var base_url = $('#base_url').val();
var idUsuario = $('#idUsuario').val();

$(document).ready(function(){
    table = $('#tabla_prospectos').DataTable();
    load();

    // Mostrar mini modal de acciones
    $('#tabla_prospectos tbody').on('click', 'a.minimodal', function () {
        var tr = $(this).closest('tr');
        var row = table.row(tr);
        var data = row.data();

        $('.modal').modal({
            dismissible: true
        });
 
        //call the specific div (modal)
        $('#modalAcciones').modal('open');
        $('#idProspectoModal').val(data.id);
        $('#empresa').val(data.empresa);
    });

    $('#lista_editar').on('click', function () {
        var id = $('#idProspectoModal').val();
        window.location = 'Prospectos/edicion/'+id;
    });
    
    // Listener para Visualizar 
    $('#lista_visualizar').on('click', function (){
        var id = $('#idProspectoModal').val();
        window.location = 'Prospectos/visualizar/'+id;
    });
    // Listener para eliminar 
    $('#lista_eliminar').on('click', function (){
        var id = $('#idProspectoModal').val();
        confirmaEliminarProspecto(id);
    });
    // Listener para cotizar
    $('#lista_cotiza').on('click', function (){   
        var id = $('#idProspectoModal').val();
        window.location = base_url+'index.php/Cotizaciones/altaCotizacion/'+id;
    });
    // Listener para listado de cotizaciones
    $('#lista_cotiza_historico').on('click', function (){   
        var id = $('#idProspectoModal').val();
        window.location = base_url+'index.php/Cotizaciones/listaCotizacionesPorProspecto/'+id;
    }); 

    // Listener para Convertir 
    $('#lista_convertir').on('click', function (){
        var id = $('#idProspectoModal').val();
        confirmaConvertirCliente(id);
    });
    // Listener para agendar llamadas 
    $('#lista_llamadas').on('click', function (){
        var id = $('#idProspectoModal').val();
        window.location = 'Crm/agendaLlamadas/'+id;
    });
    // Listener para agendar correos 
    $('#lista_correos').on('click', function (){
        var id = $('#idProspectoModal').val();
        window.location = 'Crm/agendaCorreos/'+id;
    });
    // Listener para agendar visitas 
    $('#lista_visitas').on('click', function (){
        var id = $('#idProspectoModal').val();
        window.location = 'Crm/agendaVisitas/'+id;
    });
    // Listener para revisar Agenda 
    $('#lista_agenda').on('click', function (){
        var id = $('#idProspectoModal').val();
        window.location = 'Crm/revisarCalendario/'+id;
    });
    // Listener para revisar timeline 
    $('#lista_timeline').on('click', function (){
        var id = $('#idProspectoModal').val();
        //window.location = 'Crm/revisarTimeline/'+id;
        verTimeline(id);
    });
});
function confirmaEliminarProspecto(idProspecto){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de Eliminar este Prospecto? El cambio será irreversible',
        type: 'red',
        typeAnimated: true,
        buttons: 
        {
            confirmar: function (){
                $.ajax({
                    url: base_url+"index.php/prospectos/eliminarProspecto/"+idProspecto,
                    success: function (response){
                        if(response==1){
                            fuctionalert('Éxito!','Prospecto Eliminado!');
                            setTimeout(function(){ 
                                window.location.href = base_url+"index.php/prospectos"; 
                            }, 2000);
                        }else{
                            fuctionalert('Error!','Algo salió mal, intente de nuevo o contacte al administrador del sistema'); 
                            setTimeout(function(){ 
                                window.location.href = base_url+"index.php/prospectos"; 
                            }, 2000);      
                        }
                    },
                    error: function(response){
                        fuctionalert('Error!','Algo salió mal, intente de nuevo o contacte al administrador del sistema');   
                        setTimeout(function(){ 
                                window.location.href = base_url+"index.php/prospectos";
                            }, 2000);  
                    }
                });
            },
            cancelar: function (){
                
            }
        }
    });
}

function confirmaConvertirCliente(idProspecto){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de querer convertire este Prospecto en Cliente? Una vez realizado el cambio, deberá acceder al apartado de clientes y capturar sus datos fiscales',
        type: 'blue',
        typeAnimated: true,
        buttons: 
        {
            confirmar: function (){
                $.ajax({
                    url: base_url+"index.php/prospectos/convertirProspectoCliente/"+idProspecto,
                    success: function (response){
                        if(response==1){
                            fuctionalert('Éxito!','Cliente Creado!');
                            setTimeout(function(){ 
                                window.location.href = base_url+"index.php/prospectos"; 
                            }, 2000); 
                        }else{
                            fuctionalert('Error!','Algo salió mal, intente de nuevo o contacte al administrador del sistema'); 
                            setTimeout(function(){ 
                                window.location.href = base_url+"index.php/prospectos"; 
                            }, 2000);      
                        }
                    },
                    error: function(response){
                        console.log(response);
                        fuctionalert('Error!','Algo salió mal, intente de nuevo o contacte al administrador del sistema');    
                        setTimeout(function(){ 
                                window.location.href = base_url+"index.php/prospectos";
                            }, 2000);
                    }
                });
            },
            cancelar: function (){
                
            }
        }
    });
}

function load() {
    // Destruye la tabla y la crea de nuevo con los datos cargados 
    table.destroy();
    table = $('#tabla_prospectos').DataTable({
        //pagingType: 'full_numbers',
           pagingType: 'input',
           //"sPaginationType": "listbox",
        "bProcessing": true,
        "serverSide": true,
        search: {
                return: true
            }, 
        "ajax": {
            "url": base_url+"index.php/Prospectos/getListadoProspectosPorUsuario_asic/"+idUsuario,
            type: "post"
        },
        "columns": [
            {"data": "id"},
            {"data": "empresa"},
            {"data": "puesto_contacto"},
            {"data": "email"},
            {"data": "estado"},
            {"data": "municipio"},
            {"data": "giro"},
            {"data": "direccion"},
            {"data": "observaciones"},            
            {"data": "nombre"},            
            {"data": null,
                 render:function(data,type,row){
                    $('.tooltipped').tooltip();
                    var html='<a class="btn-floating red tooltipped minimodal" data-position="top" data-delay="50" data-tooltip="Acciones">\
                                    <i class="material-icons">build</i>\
                                  </a>';
                    return html;
                }
            }
        ],
        "order": [[ 0, "desc" ]],
    }).on('draw',function(){
        $('.dataTables_filter').append('<i class="fa fa-search buttonsearch" onclick="searchtable_v()"></i>')

    });
}
function searchtable_v(){
    var contenedor = document.getElementById('tabla_prospectos_filter');

    // Selecciona el input dentro del contenedor
    var input = contenedor.querySelector('input.form-control-bmz');

    // Obtén el valor del input
    var searchv = input.value;

    // Muestra el valor en la consola
    console.log(searchv);
    //var searchv =$("input[type=search]").val();
    table.search(searchv).draw();
}
function verTimeline(idProspecto){
    $.dialog({
            theme: 'supervan',
            title: 'Timeline',
            content: 'url: '+base_url+'index.php/prospectos/getInfo/'+idProspecto,
            animation: 'scale',
            columnClass: 'medium',
            closeAnimation: 'scale',
            backgroundDismiss: true,
        });
}
function selectcotiza(){
    var selection=$('#cotizar option:selected').val(); 
      if(selection==1){
        $('.select_familia').css('display','none');
      }else if(selection==2){
        $('.select_familia').css('display','inline');
      }
}
function fuctionalert(titles,contents){
    $.alert({
        boxWidth: '30%',
        useBootstrap: false,
        title: titles,
        content: contents});
}