var base_url = $('#base_url').val();
var addtr=0;
var tableconfig=0;
$(document).ready(function(){
    $('#tablecontactos').DataTable();
    $('.chosen-select').chosen({width: "100%"});
    $('.rentarow').change(function(event) {
        calcularmontos();
    });
    $('#fechadiferentev').change(function(event) {
        if ($('#fechadiferentev').is(':checked')) {
            $('.fechadiferentev').show('slow');
        }else{
            $('.fechadiferentev').hide(1000);
            $('#fechadiferente').val('');
        }
    });
    $('.idcentroc').change(function(event) {
        var idcentroc=$(this).val();
        var rowequipo = $(this).data('rowequipo');
        var modeloid = $(this).data('modeloid');
        var serieid = $(this).data('serieid');
        //console.log(idcentroc+' '+rowequipo+' '+modeloid+' '+serieid);
        $.ajax({
            type:'POST',
            url: base_url+'index.php/Configuracionrentas_ext/addcentrocostosequiposinsertupdate',
            data: {
                idcontrato:$('#idcontrato').val(),
                idcentroc:idcentroc,
                rowequipo:rowequipo,
                modeloid:modeloid,
                serieid:serieid,
            },
            success:function(data){
            }
        });
    });
    obtenerdateinstalacionretiro();
});
function agregarcontacto() {
        var idtr=0;
        var tipo=$('#tipo').val();
        var nombre=$('#nombre').val();
        var telefono=$('#telefono').val();
        var correo=$('#correo').val();
        var ubicacion=$('#ubicacion_c').val();
        var ultima=$('#ultima').val();
        var comentario=$('#comentario').val();
        if (nombre!='') {
                //==========================================
                agregarcontactotr(idtr,tipo,nombre,telefono,correo,ubicacion,ultima,comentario);
                
                $('#tipo').val('');$('#nombre').val('');$('#telefono').val('');$('#correo').val('');$('#ubicacion_c').val('');$('#ultima').val('');$('#comentario').val('');
                //====================================
        }else{
            swal({ title: "Atención!",text: "Tienes que agregar un nombre",type: 'error',showCancelButton: false,allowOutsideClick: false,});
        }
}
function agregarcontactotr(idtr,tipo,nombre,telefono,correo,ubicacion,ultima,comentario){
    var addtre='<tr class="addtr_'+addtr+'">\
                    <td><input id="idtr" type="text"  value="'+idtr+'" readonly></td>\
                    <td><input id="servicio" type="text"  value="'+tipo+'" class="readonlys"></td>\
                    <td><input id="nombre" type="text"  value="'+nombre+'" class="readonlys"></td>\
                    <td><input id="telefono" type="text"  value="'+telefono+'" class="readonlys"></td>\
                    <td><input id="correo" type="text"  value="'+correo+'" class="readonlys"></td>\
                    <td><input id="ubicacion" type="text"  value="'+ubicacion+'" class="readonlys"></td>\
                    <td><input id="ultima_actualizacion" type="text"  value="'+ultima+'" class="readonlys"></td>\
                    <td><input id="comentarios" type="text"  value="'+comentario+'" class="readonlys"></td>\
                    <td><a class="btn-floating waves-effect waves-light red accent-2" onclick="deteletr('+addtr+','+idtr+')"><i class="material-icons">clear</i></a></td>\
              </tr>';
    $('.tabla_equiposrenta').append(addtre);
    addtr++;
}
function deteletr(id,idc){
    $('.addtr_'+id).remove();
}
function pagos_procedimientos() {
    var fechadiferentevalidar=1;
    if ($('#fechadiferentev').is(':checked')) {
        if ($('#fechadiferente').val()=='') {
            fechadiferentevalidar=0;
        }
    }
    if (fechadiferentevalidar==1) {
        //============================================================
            var DATA  = [];
            var TABLA   = $("#tablacontactos tbody > tr");
            TABLA.each(function(){         
                item = {};
                item ["idtr"] = $(this).find("input[id*='idtr']").val();
                item ["servicio"] = $(this).find("input[id*='servicio']").val();
                item ["nombre"]  = $(this).find("input[id*='nombre']").val();
                item ["telefono"] = $(this).find("input[id*='telefono']").val();
                item ["correo"] = $(this).find("input[id*='correo']").val();
                item ["ubicacion"] = $(this).find("input[id*='ubicacion']").val();
                item ["ultima_actualizacion"]  = $(this).find("input[id*='ultima_actualizacion']").val();
                item ["comentarios"] = $(this).find("input[id*='comentarios']").val();
                DATA.push(item);
            });
            datacontactos   = JSON.stringify(DATA);
        //============================================================
            var DATA2  = [];
            var TABLA2   = $("#tabla_equipos tbody > tr");
                TABLA2.each(function(){         
                    item2 = {};
                    item2 ["areid"] = $(this).find("input[id*='areid']").val();
                    item2 ["id_equipo"] = $(this).find("input[id*='id_equipo']").val();
                    item2 ["orden"] = $(this).find("input[id*='orden']").val();
                    item2 ["serieId"] = $(this).find("input[id*='serieId']").val();
                    item2 ["ubicacion"] = $(this).find("input[id*='ubicacion']").val();
                    item2 ["nuevo"]  = $(this).find("input[id*='nuevo']").val();
                    item2 ["arranque"] = $(this).find("input[id*='arranque']").val();
                    item2 ["stock_mensual"] = $(this).find("input[id*='stock_mensual']").val();
                    item2 ["contador_inicial"] = $(this).find("input[id*='contador_inicial']").val();
                    item2 ["tipo"] = $(this).find("input[id*='tipo']").val();
                    item2 ["tipoconfir"] = $(this).find("input[id*='tipocon']").val();
                    item2 ["contador_iniciale"] = $(this).find("input[id*='contador_iniciale']").val();

                    item2 ["sol_consu_cant"] = $(this).find("input[id*='sol_consu_cant']").val();
                    item2 ["sol_consu_periodo"] = $(this).find("input[id*='sol_consu_periodo']").val();
                   // item2 ["accesorios"]  = $(this).find("input[id*='accesorios']").val();
                    DATA2.push(item2);
                });
                dataequipos   = JSON.stringify(DATA2);
        //=================================================================
            var DATA3  = [];
            var TABLA3 = $(".tabla_pagos ul > li");
            TABLA3.each(function(){         
                        item3 = {};
                        item3 ["equipos"] = $(this).find("input[id*='equipos_cliks']").val();
                        item3 ["serie"] = $(this).find("input[id*='serie_cliks']").val();
                        item3 ["renta"] = $(this).find("input[id*='renta']").val();
                        item3 ["clicks_mono"] = $(this).find("input[id*='clicks_mono']").val();
                        item3 ["precio_c_e_mono"]  = $(this).find("input[id*='precio_c_e_mono']").val();
                        item3 ["clicks_color"] = $(this).find("input[id*='clicks_color']").val();
                        item3 ["precio_c_e_color"] = $(this).find("input[id*='precio_c_e_color']").val();
                        DATA3.push(item3);
                    });
                    dataequiposclicks   = JSON.stringify(DATA3);
        //===============================================
        var data = $('#form_pagos').serialize()+'&'+$('#form_procedimientos').serialize()+'&personalId='+$('#personalId').val()+'&idcontrato='+$('#idcontrato').val()+'&contactos='+datacontactos+'&equiposrow='+dataequipos+'&equiposclicks='+dataequiposclicks;
        $.ajax({
            type:'POST',
            url: base_url+"index.php/Configuracionrentas/guardarcontratopago",
            data: data,
            success: function (data){
                var idpro=parseFloat(data);
                //var idcontrato = $('#idcontrato').val();
               contratosview($('#idcontrato').val());
            }
        });
    }else{
       $.alert({boxWidth: '30%',useBootstrap: false,title: 'Atención!',
                content: 'Otra fecha de inicio no puede ser vacia'}
            ); 
    }
}
// Vista de contratos 
function selectipo2() {
    var id = $('#tipo2 option:selected').val();
    if(id==1){
        $('.centros_costos').hide('show');
        $('.inputequipo').remove(); 
        $('.row_equipo_n').css('display','block');
        $("#monto").prop("readonly", true);
        $("#montoc").prop("readonly", true);
        $('.preview_periodo').show();
    }else if(id==2){
        $('.preview_periodo').hide();
        $('.centros_costos').show('show');
        viewcc();
        $("#monto").prop("readonly", false);
        $("#montoc").prop("readonly", false);
        $('.row_equipo_n').css('display','none');
        $('.row_equipo').append('<ul class="collapsible inputequipo" data-collapsible="accordion">\
                                    <li>\
                                        <div class="collapsible-header active">Global</div>\
                                        <div class="collapsible-body" style="display: block;">\
                                            <div class="inputequipo">\
                                                <div class="row">\
                                                  <div class="col s2"><h6>Clicks incluidos mono</h6></div>\
                                                  <div class="col s2"><input name="clicks_mono" class="readonlys" type="number"></div>\
                                                  <div class="col s1"></div>\
                                                  <div class="col s2"><h6>Precio click excedente</h6></div>\
                                                  <div class="col s2"><input name="precio_c_e_mono" class="readonlys" type="number"></div>\
                                                </div>\
                                                <div class="row">\
                                                  <div class="col s2"><h6>Clicks incluidos color</h6></div>\
                                                  <div class="col s2"><input name="clicks_color" class="readonlys" type="number"></div>\
                                                  <div class="col s1"></div>\
                                                  <div class="col s2"><h6>Precio click color excedente</h6></div>\
                                                  <div class="col s2"><input name="precio_c_e_color" class="readonlys" type="number"></div>\
                                                </div>\
                                                <div class="row">\
                                                    <div class="col s2">\
                                                      <label>Descuento clicks</label><input id="descuento_g_clicks"  type="number"  class="form-control-bmz " readonly>\
                                                    </div>\
                                                    <div class="col s2">\
                                                      <label>Descuento Escaneo</label><input id="descuento_g_scaneo"  type="number"  class="form-control-bmz " readonly>\
                                                    </div>\
                                                  </div>\
                                            </div>\
                                        </div>\
                                    </li>\
                                </ul>');
        $('.row_equipo2').html('<br><div class="row">\
                                      <div class="col s2"><h6>Clicks Mono</h6><input class="exc_clicks_mono form-control-bmz" type="number" readonly></div>\
                                      <div class="col s2"><h6>Excedentes Mono</h6><input class="exc_mono form-control-bmz" type="number" readonly></div>\
                                      <div class="col s1"></div>\
                                      <div class="col s2"><h6>Clicks Color</h6><input class="exc_clicks_color form-control-bmz" type="number" readonly></div>\
                                      <div class="col s2"><h6>Excedentes Color</h6><input class="exc_color form-control-bmz readonlys" type="number" readonly></div>\
                                    </div>\
                                    <ul class="collapsible inputequipo" data-collapsible="accordion">\
                                    <li>\
                                        <div class="collapsible-header active">Global</div>\
                                        <div class="collapsible-body" style="display: block;">\
                                            <div class="inputequipo">\
                                                <div class="row">\
                                                  <div class="col s2"><h6>Clicks incluidos mono</h6></div>\
                                                  <div class="col s2"><input class="inf_clicks_mono form-control-bmz" type="number" readonly></div>\
                                                  <div class="col s1"></div>\
                                                  <div class="col s2"><h6>Precio click excedente</h6></div>\
                                                  <div class="col s2"><input class="inf_precio_c_e_mono form-control-bmz readonlys" type="number" readonly></div>\
                                                </div>\
                                                <div class="row">\
                                                  <div class="col s2"><h6>Clicks incluidos color</h6></div>\
                                                  <div class="col s2"><input  class="inf_clicks_color form-control-bmz readonlys" type="number" readonly></div>\
                                                  <div class="col s1"></div>\
                                                  <div class="col s2"><h6>Precio click color excedente</h6></div>\
                                                  <div class="col s2"><input name="" class="inf_precio_c_e_color form-control-bmz readonlys" type="number"></div>\
                                                </div>\
                                                <div class="row">\
                                                    <div class="col s2"><label>Descuento clicks</label><input  type="number"  class="inf_descuento_g_clicks form-control-bmz " readonly></div>\
                                                    <div class="col s2"><label>Descuento Escaneo</label><input  type="number"  class="inf_descuento_g_scaneo form-control-bmz " readonly></div>\
                                                  </div>\
                                            </div>\
                                        </div>\
                                    </li>\
                                </ul>');
        $('.collapsible').collapsible();
    }
}

function contratosview(id){
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Configuracionrentas/viewcontratopago",
        data: {
            idcontrato:id
        },
        success: function (data){
            $('.tabla_equiposrenta').html('');
            console.log(data);
            var array = $.parseJSON(data);
            if (array.filas>0) {
                $('#idcondicionextra').val(array.idcondicionextra);
                //console.log('entra');
                //console.log(array.id_renta);
                if(array.info_factura!=null){
                    var html_text_if='';
                    html_text_if+='<label>INSTRUCCIONES PARA FACTURACIÓN</label>';
                    html_text_if+='<textarea class="form-control-bmz" style="height: 76px !important;" readonly disabled>'+array.info_factura+'</textarea>';
                   $('.info_factura').html(html_text_if);
                    //console.log('info_factura '+array.info_factura); 
                }
                $('#id_renta').val(array.id_renta);
                $('#tipo1').val(array.tipo1);
                $('#tipo2').val(array.tipo2);
                $('#tipo3').val(array.tipo3);
                $('#tipo4').val(array.tipo4);
                $('.infoclientecontrato').html(array.nombrecliente+' / Contrato: '+array.idcontrato+' / Folio: '+array.folio);
                $('#nfclientes').html('<option value="'+array.idCliente+'">'+array.nombrecliente+'</option>');
                $('#addfactcliente').val(array.idCliente);
                var rentacolor=array.rentacolor;
                if(rentacolor>0){

                }else{
                    var rentacolor=array.precio_c_e_color;
                }
                if(array.facturacionlibre==1){
                    $('.rentarow').val(0).prop('readonly', true);
                }
                $('#nfcontratos').html('<option value="'+array.idcontrato+'" data-rentacolor="'+rentacolor+'">'+array.idcontrato+'</option>');
                selectipo2();
                $('#ordenc_numero').val(array.ordenc_numero);
                $('#ordenc_vigencia').val(array.ordenc_vigencia); //verificar
                $('#oc_vigencia').val(array.oc_vigencia);//verificar
                $('#saldo').val(array.saldo);
                if (array.clicks_mono!=null) {
                    $('input[name=clicks_mono]').val(array.clicks_mono)
                    $('input[name=precio_c_e_mono]').val(array.precio_c_e_mono)
                    $('input[name=clicks_color]').val(array.clicks_color)
                    $('input[name=precio_c_e_color]').val(array.precio_c_e_color)
                    $('#descuento_g_clicks').val(array.descuento_g_clicks);
                    $('#descuento_g_scaneo').val(array.descuento_g_scaneo);

                    $('.inf_clicks_mono').val(array.clicks_mono);
                    $('.inf_precio_c_e_mono').val(array.precio_c_e_mono);
                    $('.inf_clicks_color').val(array.clicks_color);
                    $('.inf_precio_c_e_color').val(array.precio_c_e_color);
                    $('.inf_descuento_g_clicks').val(array.descuento_g_clicks);
                    $('.inf_descuento_g_scaneo').val(array.descuento_g_scaneo);
                }
                $('#fecha_instalacion').val(array.fecha_instalacion);
                $('#periodo_ini').val(array.periodo_ini);
                $('#perioro_fin').val(array.perioro_fin);
                $('#servicio_horario').val(array.servicio_horario);
                $('#servicio_equipo').val(array.servicio_equipo);
                $('#servicio_doc').val(array.servicio_doc);
                $('#compras_portal').val(array.compras_portal);
                $('#compras_usuario').val(array.compras_usuario);
                $('#compras_contra').val(array.compras_contra);
                $('#cobranza').val(array.cobranza);
                $('#descrip_fac').val(array.descrip_fac);
                $('#monto').val(array.rentadeposito);
                $('#montoc').val(array.rentacolor);
                $('#continuarcontadores').val(array.continuarcontadores);
                //$('#ordenc_numero').val(array.ordenc_numero);
                array.contactos.forEach(function(element) {
                    agregarcontactotr(element.arcId,element.servicio,element.nombre,element.telefono,element.correo,element.ubicacion,element.ultima_actualizacion,element.comentarios);
                });
                array.equipos.forEach(function(element) {
                    //console.log(element)
                    $('.areid_'+element.id_equipo+'_'+element.serieId+'_'+element.tipo).val(element.areid);
                    //$('.orden_'+element.id_equipo+'_'+element.serieId+'_'+element.tipo).val(element.orden);
                    $('.td_orden_'+element.id_equipo+'_'+element.serieId+'_'+element.tipo).append('<span class="span_orden">'+element.orden+'</span>');

                    $('.ubicacion_'+element.id_equipo+'_'+element.serieId+'_'+element.tipo).val(element.ubicacion);
                    //$('.nuevo_'+element.id_equipo+'_'+element.serieId+'_'+element.tipo).val(element.nuevo);
                    $('.arranque_'+element.id_equipo+'_'+element.serieId+'_'+element.tipo).val(element.arranque);
                    $('.stock_mensual_'+element.id_equipo+'_'+element.serieId+'_'+element.tipo).val(element.stock_mensual);
                    $('.contador_inicial_'+element.id_equipo+'_'+element.serieId+'_'+element.tipo).val(element.contador_inicial);
                    $('.contador_iniciale_'+element.id_equipo+'_'+element.serieId+'_'+element.tipo).val(element.contador_iniciale);

                    $('.sol_consu_cant_'+element.id_equipo+'_'+element.serieId+'_'+element.tipo).val(element.sol_consu_cant);
                    $('.sol_consu_periodo_'+element.id_equipo+'_'+element.serieId+'_'+element.tipo).val(element.sol_consu_periodo);
                    //$('.accesorios_'+element.id_equipo+'_'+element.id_renta).val(element.);
                    if (element.clicks_color!=null) {
                       $('.renta_'+element.id_equipo+'_'+element.serieId+'_'+element.tipo).val(element.renta);
                       $('.clicks_mono_'+element.id_equipo+'_'+element.serieId+'_'+element.tipo).val(element.clicks_mono);
                       $('.precio_c_e_mono_'+element.id_equipo+'_'+element.serieId+'_'+element.tipo).val(element.precio_c_e_mono);
                       $('.clicks_color_'+element.id_equipo+'_'+element.serieId+'_'+element.tipo).val(element.clicks_color);
                       $('.precio_c_e_color_'+element.id_equipo+'_'+element.serieId+'_'+element.tipo).val(element.precio_c_e_color); 

                       $('.renta_'+element.id_equipo+'_'+element.serieId).val(element.renta);
                       $('.rentac_'+element.id_equipo+'_'+element.serieId).val(element.rentac);
                       $('.clicks_mono_'+element.id_equipo+'_'+element.serieId).val(element.clicks_mono);
                       $('.precio_c_e_mono_'+element.id_equipo+'_'+element.serieId).val(element.precio_c_e_mono);
                       $('.clicks_color_'+element.id_equipo+'_'+element.serieId).val(element.clicks_color);
                       $('.precio_c_e_color_'+element.id_equipo+'_'+element.serieId).val(element.precio_c_e_color); 
                    }
                });
                $('select').trigger("chosen:updated");
                $(".chosen-select").attr('disabled', true).trigger("chosen:updated");
                $(".readonlys").prop("readonly", true);
                $('input').change();
                $('.bttondisabled').attr('disabled', true);
                $("#fac_auto").prop("disabled", true);
                $('.bttondisabled').hide(); 
 
                $('.add_editar_btn').html('<button class="btn green waves-effect waves-light right bttondisabled_editar" type="button" onclick="editar_modal()">Editar<i class="material-icons right">border_color</i></button>'); 
                
                if (array.fechadiferentev==1) {
                    $('#fechadiferentev').attr({checked: true});
                    $('.fechadiferentev').show('show');
                    setTimeout(function(){ 
                        $('#fechadiferente').val(array.fechadiferente);
                    }, 1000);
                }
                if(array.tipo2==1){
                    
                    array.equipos_conextra.forEach(function(element) {
                        //console.log(element)
                        
                        if (element.renta_m>0 || element.renta_c>0) {
                            /*
                           $('.renta_'+element.id_equipo+'_'+element.serieId+'_'+element.tipo).val(element.renta);
                           $('.clicks_mono_'+element.id_equipo+'_'+element.serieId+'_'+element.tipo).val(element.clicks_mono);
                           $('.precio_c_e_mono_'+element.id_equipo+'_'+element.serieId+'_'+element.tipo).val(element.precio_c_e_mono);
                           $('.clicks_color_'+element.id_equipo+'_'+element.serieId+'_'+element.tipo).val(element.clicks_color);
                           $('.precio_c_e_color_'+element.id_equipo+'_'+element.serieId+'_'+element.tipo).val(element.precio_c_e_color); 
                           */
                            
                           $('.renta_'+element.equiporow+'_'+element.serieId).val(element.renta_m);
                           $('.rentac_'+element.equiporow+'_'+element.serieId).val(element.renta_c);
                           $('.clicks_mono_'+element.equiporow+'_'+element.serieId).val(element.click_m);
                           $('.precio_c_e_mono_'+element.equiporow+'_'+element.serieId).val(element.excedente_m);
                           $('.clicks_color_'+element.equiporow+'_'+element.serieId).val(element.click_c);
                           $('.precio_c_e_color_'+element.equiporow+'_'+element.serieId).val(element.excedente_c); 
                        }
                    });
                }
                if(array.factura_depo>0){
                    var html_fac='Factura en deposito<br>';
                        html_fac+='<a class="b-btn b-btn-primary " href="'+base_url+'index.php/Configuracionrentas/generarfacturas/'+array.factura_depo+'/1" target="_blank"><i class="fas fa-file-pdf fa-2x"></i></a>  ';
                        html_fac+='<a class="b-btn b-btn-primary " href="'+base_url+'kyocera/facturas/'+array.factura_xml+'" target="_blank"><i class="far fa-file-code fa-2x"></i></a>';
                    $('.addfacturadeposito').html(html_fac);
                }

                if(array.fac_auto==1){
                    $("#fac_auto").prop("checked", true);
                    $('.tab_test11').show();
                }
                setTimeout(function(){ 
                    verificarequiposnuevos();
                }, 1000);
            }else{
                $('.infoclientecontrato').html(array.nombrecliente+' / Contrato: '+array.idcontrato+' / Folio: '+array.folio);
                if(array.info_factura!=null){
                    var html_text_if='';
                    html_text_if+='<label>INSTRUCCIONES PARA FACTURACIÓN</label>';
                    html_text_if+='<textarea class="form-control-bmz" style="height: 76px !important;" readonly disabled>'+array.info_factura+'</textarea>';
                   $('.info_factura').html(html_text_if);
                    //console.log('info_factura '+array.info_factura); 
                }
                $('#monto').val(array.rentadeposito);
                $('#montoc').val(array.rentacolor);
                $('#fecha_instalacion').val(array.fecha_instalacion);
                $('#ordenc_vigencia').val(array.ordenc_vigencia);
                $('#oc_vigencia').val(array.oc_vigencia);
                $('#tipo1').val(array.tipo1);
                $('#tipo3').val(array.tipo3);
                $('#servicio_horario').val(array.servicio_horario);
                $('#servicio_equipo').val(array.servicio_equipo);
                $('#servicio_doc').val(array.servicio_doc);
                $('#ordenc_numero').val(array.ordenc_numero);
                setTimeout(function(){
                    $('input[name=clicks_mono]').val(array.clicks_mono);
                    $('input[name=precio_c_e_mono]').val(array.precio_c_e_mono);
                    $('input[name=clicks_color]').val(array.clicks_color);
                    $('input[name=precio_c_e_color]').val(array.precio_c_e_color);
                }, 1000);
                if(array.facturacionlibre==1){
                    $('.rentarow').val(0).prop('readonly', true);
                    $('.rentacrow').val(0).prop('readonly', true);
                }
                $('#tipo2').val(array.tipo2).change();
                traerdatoscontrato(id);
            }
            if(array.status_vigente==0){
                $('.infostatusvigente').html('<div class="statuscv">El contrato se encuentra vencido</div>');
            }
            
            $('select').trigger("chosen:updated");
            $('input').change();
            if (array.estatus==0) {
                bloquearbtn();
            }
            if(tableconfig==0){
                $('#tabla_equipos').DataTable({
                    "lengthMenu": [[-1], ["All"]],
                    "searching": false,
                    "paging": false,
                    "info": false,
                    columnDefs: [
                        { orderable: false, targets: 4 }
                        ],
                });    
            }
            tableconfig++;
            //var idpro=parseFloat(data);   
        }
    });
}
function editar_modal(){
    vericar_pass();
}
function vericar_pass(){
    /*
    pass = $('#ver_pass').val();
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Configuracionrentas/vericar_pass",
        data: {
            pass:pass
        },
        success: function (data){
        */
            var titulo = "",texto = "",tipo = "";
            //if(data >1){
                titulo = "Éxito"; 
                texto  = "Contraseña correcta";
                tipo = 'success';
                $('#modal_editor').modal('close');
                $('select').trigger("chosen:updated");
                $(".chosen-select").attr('disabled', false).trigger("chosen:updated");
                $(".readonlys").prop("readonly", false);
                $('input').change();
                $('.bttondisabled').show();
                $('.bttondisabled').attr('disabled', false);
                $('.titulo_btn').html('Editar');
                $('.bttondisabled_editar').css('display','none');
                $('#fecha_instalacion').attr('onclick','advertenciaeditfecha()');
                $("#fac_auto").prop("disabled", false);
                /*
            }else{
                titulo = "Error"; 
                texto  = "Verificar la contraseña";
                tipo = 'error';
            }
            swal({ title: titulo,
                    text: texto,
                    type: tipo,
                    showCancelButton: false,
                    allowOutsideClick: false,
                });
        }
    });   
    */
}
function traerdatoscontrato(idcontrato){
    
    $.ajax({
        type:'POST',
        url: base_url+'index.php/Rentas/traerdatoscontrato',
        data: {contrato:idcontrato},
        success:function(data){
            var array = $.parseJSON(data);
            array.equipos.forEach(function(element) {
                //console.log(element);
                $('.renta_'+element.equiposrow+'_'+element.serieId).val(element.rentacosto);
                $('.clicks_mono_'+element.equiposrow+'_'+element.serieId).val(element.pag_monocromo_incluidos);
                $('.precio_c_e_mono_'+element.equiposrow+'_'+element.serieId).val(element.excedente);
                $('.clicks_color_'+element.equiposrow+'_'+element.serieId).val(element.pag_color_incluidos);
                $('.precio_c_e_color_'+element.equiposrow+'_'+element.serieId).val(element.excedentecolor);
            });
        }
    }); 
    if ($('#tipo2 option:selected').val()==1) {
        setTimeout(function(){ 
            calcularmontos();
        }, 1000);
    }  
}
function calcularmontos(){
    var tipo2 = $('#tipo2 option:selected').val();
    var addtp = 0;
    $(".rentarow").each(function() {
        var vstotal = $(this).val()==''?0:$(this).val();
        addtp += Number(vstotal);
    });
    if(tipo2!=2){
        $('#monto').val(addtp).change();
    }
    var addtpc = 0;
    
    $(".rentacrow").each(function() {
        var vstotalc = $(this).val()==''?0:$(this).val();
        addtpc += Number(vstotalc);
    });
    if(tipo2!=2){
        $('#montoc').val(addtpc).change();
    }
}
function bloquearbtn(){
    setTimeout(function(){ 
        $('.bttondisabled').remove();
        $('.addprefactura').remove();
    }, 1000);
}
function advertenciaeditfecha(){
    $.alert({boxWidth: '30%',useBootstrap: false,title: 'Atención!',
                content: '¿Está seguro que desea cambiar la fecha de inicio?, todos los periodos capturados se eliminarán'});
}
function ppdate(){
    //console.log('ppdate');
    var html='Seleccione rango de fechas para obtener produccion promedio<br>\
             <div class="row"><div class="col s12"><label>Fecha Inicio</label><input type="date" id="pp_fi" class="form-control-bmz">\
             <label>Fecha Fin</label><input type="date" id="pp_ff" class="form-control-bmz"></div></div>';
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: html,
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                //===================================================
                var idcontrato=$('#idcontrato').val();
                var pp_fi=$('#pp_fi').val();
                var pp_ff=$('#pp_ff').val();
                window.location.href = base_url+'index.php/Configuracionrentas/ContratosPagos/'+idcontrato+'?ppfi='+pp_fi+'&ppff='+pp_ff; 
                //================================================
            },
            cancelar: function (){
                
            }
        }
    });
}
function viewcc(){
    view_centroscostos();
}
function addcentrocostos(){
    $.ajax({
        type:'POST',
        url: base_url+'index.php/Configuracionrentas_ext/addcentrocostos',
        data: {
            ccdes:$('#cc_descripcion').val(),
            idcontrado:$('#idcontrato').val()
        },
        success:function(data){
            $('#cc_descripcion').val('');
            toastr["success"]("Agregado");
            view_centroscostos();
        }
    }); 
}
function view_centroscostos(){
    $.ajax({
        type:'POST',
        url: base_url+'index.php/Configuracionrentas_ext/view_centroscostos',
        data: {
            idcontrado:$('#idcontrato').val()
        },
        success:function(data){
            $('.view_centroscostos').html('');
            $('.select_ccs').html('<option value="0">Seleccione</option>');
            var array = $.parseJSON(data);
            array.css.forEach(function(element) {
                var htmlcs='<tr><td>'+element.descripcion+'</td>\
                                <td><input type="number" class="form-control-bmz cant_cc_0 cant_cc_0_'+element.id+'" onChange="updateccv('+element.id+',0)" value="'+element.clicks+'"></td>\
                                <td><input type="number" class="form-control-bmz cant_cc_1 cant_cc_1_'+element.id+'" onChange="updateccv('+element.id+',1)" value="'+element.clicks_c+'"></td>\
                                <td><a onclick="deletecc('+element.id+')" class="btn-bmz red"><i class="fas fa-trash-alt"></i></a></td></tr>';
                $('.view_centroscostos').append(htmlcs);
                $('.select_ccs').append('<option value="'+element.id+'">'+element.descripcion+'</option>');
            });
            setTimeout(function(){ 
                array.css_quipo.forEach(function(element) {
                    $('.ccs_'+element.rowequipo+'_'+element.modeloid+'_'+element.serieid).val(element.idcentroc);
                });
            }, 1000);

        }
    }); 
}
function deletecc(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Confirma la eliminacion del centro de costos?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                //===================================================
                $.ajax({
                    type:'POST',
                    url: base_url+'index.php/Configuracionrentas_ext/centroscostos_delete',
                    data: {
                        id:id
                    },
                    statusCode:{
                        404: function(data){
                            toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema");
                        },
                        500: function(){
                            toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema");
                        }
                    },
                    success:function(data){
                        toastr["success"]("Eliminado correctamente");
                        view_centroscostos();
                    }
                }); 
                //================================================
            },
            cancelar: function () 
            {
                
            }
        }
    });
}
function updateccv(id,tipo){
    var clicksm=$('.cant_cc_0_'+id).val();
    var clicksc=$('.cant_cc_1_'+id).val();
    $.ajax({
        type:'POST',
        url: base_url+'index.php/Configuracionrentas_ext/updateccv',
        data: {
            id:id,
            cm:clicksm,
            cc:clicksc
        },
        statusCode:{
            404: function(data){
                toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema");
            },
            500: function(){
                toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema");
            }
        },
        success:function(data){
            
        }
    });
    calcularclickccv(tipo);
}
function calcularclickccv(tipo){
    var clicks_m=$('input[name="clicks_mono"]').val();
    var clicks_c=$('input[name="clicks_color"]').val();
    if(tipo==0){
        var clicks=0;
        $(".cant_cc_0").each(function() {
            var vstotalps = $(this).val();
            clicks += Number(vstotalps);
        });
        if(parseInt(clicks_m)==parseInt(clicks)){
            $('.cant_cc_0').removeClass('red');
        }else{
            $('.cant_cc_0').addClass('red');
        }
    }
    if(tipo==1){
        var clicks=0;
        $(".cant_cc_1").each(function() {
            var vstotalps = $(this).val();
            clicks += Number(vstotalps);
        });
        if(parseInt(clicks_m)==parseInt(clicks)){
            $('.cant_cc_1').removeClass('red');
        }else{
            $('.cant_cc_1').addClass('red');
        }
    }
}
function preview_periodo(){
    var idcontrato = $('#idcontrato').val();
    var p_ini=$('#periodo_inicial').val();
    var p_fin=$('#periodo_final').val();
    //=============================================================
        var permitido=1;
        var tipo2=$('#tipo2 option:selected').val();
        var DATA4  = [];
        var TABLA4   = $("#profactura tbody > tr");
            TABLA4.each(function(){         
                item2 = {};
                item2 ["proId"] = $(this).find("input[class*='fd_equipo']").val();//productoId
                item2 ["serieId"] = $(this).find("input[class*='fd_serie']").val();
                item2 ["c_c_i"] = $(this).find("input[class*='f_c_c_i']").val();
                item2 ["c_c_f"]  = $(this).find("input[class*='f_c_c_f']").val();
                item2 ["e_c_i"] = $(this).find("input[class*='f_e_c_i']").val();
                item2 ["e_c_f"] = $(this).find("input[class*='f_e_c_f']").val();
                item2 ["produc"] = $(this).find("input[class*='f_produccion']").val();//produccion
                item2 ["toco"] = $(this).find("input[class*='f_toner_consumido']").val();//toner_consumido
                item2 ["descu"] = $(this).find("input[class*='descuento']").val();//descuento
                item2 ["descue"] = $(this).find("input[class*='descuentoe']").val();//descuentoe
                item2 ["producc_tot"] = $(this).find("input[class*='produccion_total']").val();//produccion_total
                item2 ["tipo"] = $(this).find("input[class*='fd_tipo']").val();
                item2 ["tipo2"]=tipo2;
                    item2 ["exce"] = $(this).find("input[class*='excedentes']").val();//excedentes
                    item2 ["costexce"] = $(this).find("input[class*='excedentescosto']").val();//costoexcedente
                    item2 ["totexce"] = $(this).find("input[class*='excedentestotal']").val();//totalexcedente
                if (parseFloat($(this).find("input[class*='f_c_c_f']").val()) < parseFloat($(this).find("input[class*='f_c_c_i']").val())) {
                    permitido=0;
                }
                
                if ($(this).find("input[class*='f_c_c_f']").data('tomarescaner')==1) {
                    if (parseFloat($(this).find("input[class*='f_e_c_f']").val()) < parseFloat($(this).find("input[class*='f_e_c_i']").val())) {
                        permitido=0;
                    }
                }
                
                DATA4.push(item2);
            });
            dataequipos   = JSON.stringify(DATA4);

    //=============================================================
    var datos='p_ini='+p_ini+'&p_fin='+p_fin+'&eqdetalle='+dataequipos;
    var url_preview=base_url+'Reportes/rpdll_preview/0/'+idcontrato+'?'+datos;
    if (permitido==1) {
        window.open(url_preview, '_blank'); 
    }else{
        swal({title: "No permitido",text: "verifique sus contadores",timer: 2000,showConfirmButton: false});
    }
}
function modalcontactos(){
    $('#modal_contactos').modal('open');
}
function addmodalcontactos(row){
    var tip = $('.addmodalcontactos_'+row).data('puesto');
    var ate = $('.addmodalcontactos_'+row).data('atencionpara');
    var tel = $('.addmodalcontactos_'+row).data('telefono');
    var ema = $('.addmodalcontactos_'+row).data('email');
    $('#tipo').val(tip).change();
    $('#nombre').val(ate).change();
    $('#telefono').val(tel).change();
    $('#correo').val(ema).change();
    $('#modal_contactos').modal('close');
}
function obtenerdateinstalacionretiro(){

            var DATA2  = [];
            var TABLA2   = $("#tabla_equipos tbody > tr");
                TABLA2.each(function(){         
                    item2 = {};
                    item2 ["rowequipo"] = $(this).find("span[class*='fechaintalacion']").data('rowe');
                    item2 ["serieid"] = $(this).find("span[class*='fechaintalacion']").data('serieid');
                    DATA2.push(item2);
                });
                dataequipos   = JSON.stringify(DATA2);
        
        var data = 'idcontrato='+$('#idcontrato').val()+'&equiposrow='+dataequipos;
        $.ajax({
            type:'POST',
            url: base_url+"index.php/Configuracionrentas_ext/obtenerdateinstalacionretiro",
            data: data,
            success: function (data){
                console.log(data);
                var array = $.parseJSON(data);
                array.forEach(function(element) {
                    if(element.tip==1){
                        if(element.pol==17){//17 instalacion
                            $('.instala_'+element.roweq).html(element.fecha);
                        }
                    }
                });
            }
        });
}
function verificarequiposnuevos(){
    /*
    console.log('verificarequiposnuevos');
    var noti=0;
    var TABLA2   = $("#tabla_equipos tbody > tr");
        TABLA2.each(function(){  
            var areid =  $(this).find("input[id*='areid']").val();     
            if(areid>0){

            }else{
                noti=1;
            }
        });
    if(noti==1){
        $.alert({boxWidth: '30%',useBootstrap: false,title: 'Atención!',
            content: 'Tiene un equipo recién agregado, verifique los contadores iniciales y/o condiciones'}
        );
    }
    */
}