<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AsignacionesSeries extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('ModeloAsignacion');
        $this->load->model('ModeloCatalogos');
        $this->load->helper('url');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d');
        $this->submenu=43;
        if($this->session->userdata('logeado')==true){
            $this->perfilid =$this->session->userdata('perfilid');
            $this->idpersonal = $this->session->userdata('idpersonal');
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,$this->submenu);// 43 es el id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
        }else{
            redirect('login');
        }
    }
    // Listado de Asignaciones
    function index(){ 
        $data['MenusubId']=$this->submenu;
        $data['fechainicial_reg'] = date("Y-m-d",strtotime($this->fechahoy ."- 8 month")); 
        $this->load->view('header');
        $this->load->view('main');
        $this->load->view('series/asignaciones',$data);
        $this->load->view('footerl');
        $this->load->view('series/asignacionesjs');   
    }
    public function getData_asignacion_a() {
        $params = $this->input->post();
        $serie = $this->ModeloAsignacion->Getventas_accesorios($params);
        $totalRecords=$this->ModeloAsignacion->Totalventas_accesorios($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totalRecords),  
            "recordsFiltered" => intval($totalRecords),
            "data"            => $serie->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    public function getData_asignacion_e() {
        $params = $this->input->post();
        $serie = $this->ModeloAsignacion->Getventas_equipos($params);
        $totalRecords=$this->ModeloAsignacion->Totalventas_equipos($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totalRecords),  
            "recordsFiltered" => intval($totalRecords),
            "data"            => $serie->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    public function getData_asignacion_r() {
        $params = $this->input->post();
        $serie = $this->ModeloAsignacion->Getventas_refacciones($params);
        $totalRecords=$this->ModeloAsignacion->Totalventas_refacciones($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totalRecords),  
            "recordsFiltered" => intval($totalRecords),
            "data"            => $serie->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    function seriese($idsbode=0){
        $equipo = $this->input->post('equipo');
        $bodega = $this->input->post('bod');
        $cantidad = $this->input->post('cantidad');
        $idrow = $this->input->post('idrow');
        //$bodegasrow = $this->ModeloCatalogos->getselectwheren('bodegas',array('activo'=>1));
        $strq="SELECT * from bodegas where activo=1";
        $query=$this->db->query($strq);

        //$bodegasrow=$this->ModeloCatalogos->view_session_bodegas();
        $bodegasrow=$query->result();
        //$activo = array('activo' => 1,'bodegaId'=>$bodega,'productoid'=>$equipo,'status <=' => 1);
        //$seriesrows = $this->ModeloCatalogos->getselectwherenall('series_productos',$activo);
        $seriesrowsnum=0;
        if($idsbode=='xx'){
            $xx=' selected';
            $activo = array('activo' => 1,'productoid'=>$equipo,'status <=' => 1);
        }else{
            $xx='';
            $activo = array('activo' => 1,'bodegaId'=>$bodega,'productoid'=>$equipo,'status <=' => 1);
        }
        $seriesrows = $this->ModeloCatalogos->getselectwherenall('series_productos',$activo);

            $html='<div class="row">
                        <div class="input-field col s6 m3 ">
                            <input placeholder="Cantidad" id="cant_requ_e" type="text" class="validate cant_requ_div" value="'.$cantidad.'" readonly>
                            <input placeholder="Cantidad" id="equipo" type="hidden" class="validate" value="'.$idrow.'" readonly>
                            <label for="cant_requ" class="active cant_requ_div">Cantidad Requeridos</label>
                        </div>
                        <div class="input-field col s6 m4 ">
                            <select id="newbodegae" class="form-control chosen-select" onchange="asignare_new('.$idrow.','.$cantidad.','.$equipo.')">
                                <option value="0">Bodega seleccionada</option>
                                <option value="xx" '.$xx.'>Todas</option>
                                ';
                                foreach ($bodegasrow as $itemb) {
                                        if ($itemb->bodegaId==$idsbode) {
                                            $selectedb='selected';
                                        }else{
                                            $selectedb='';
                                        }
                                        $html.='<option value="'.$itemb->bodegaId.'" '.$selectedb.'>'.$itemb->bodega.'</option>';
                                    
                                    
                                }

                         $html.='
                            </select>
                        </div>

                    </div>

                    ';
        foreach ($seriesrows as $item) {
            $html.= '<div class="col s6 m3 serieIdp_ck_'.$item->serieId.'">
                    <div class="card">
                        <div class="card-content" style="padding:15px">
                          <input type="checkbox" id="serieIdp_'.$item->serieId.'" class="checkproducto bodega_'.$item->bodegaId.'" 
                          name="checkproducto"  value="'.$item->serieId.'" onclick="selectedseriev($(this))">
                          <label for="serieIdp_'.$item->serieId.'" style="padding-left:22px">'.$item->serie.'</label> 
                        </div >
                        
                      </div>
                </div>
                
                                    ';
            $seriesrowsnum=1;
        }
        if ($seriesrowsnum==0) {
            $html.='No se encuentran disponibles series disponibles';
        }
        echo $html;
    }
    // rentas
    function serieserenta(){
        $equipo = $this->input->post('equipo');
        $bodega = $this->input->post('bod');
        $cantidad = $this->input->post('cantidad');
        $idrow = $this->input->post('idrow');

        $activo = array('activo' => 1,'bodegaId'=>$bodega,'productoid'=>$equipo,'status <=' => 1);
        $seriesrows = $this->ModeloCatalogos->getselectwherenall('series_productos',$activo);
        $seriesrowsnum=0;
            $html='<div class="row">
                        <div class="input-field col s6 m3 ">
                            <input placeholder="Cantidad" id="cant_requ_e_rentas" type="text" class="validate cant_requ_div_renta" value="'.$cantidad.'" readonly>
                            <input placeholder="Cantidad" id="equipo_rentas" type="hidden" class="validate" value="'.$idrow.'" readonly>
                            <label for="cant_requ" class="active cant_requ_div_renta">Cantidad Requeridos</label>
                        </div>
                    </div>

                    ';
        foreach ($seriesrows as $item) {
            $html.= '<div class="col s6 m3">
                    <div class="card">
                        <div class="card-content" style="padding:15px">
                          <input type="checkbox" id="serieIdp_rentas_'.$item->serieId.'" class="checkproductorenta bodega_'.$item->bodegaId.'" 
                          name="checkproductorenta"  value="'.$item->serieId.'">
                          <label for="serieIdp_rentas_'.$item->serieId.'" style="padding-left:22px">'.$item->serie.'</label> 
                        </div >
                        
                      </div>
                </div>
                
                                    ';
            $seriesrowsnum=1;
        }
        if ($seriesrowsnum==0) {
            $html='No se encuentran disponibles series disponibles';
        }
        echo $html;
    } 
    //
    function seriese_renta($idsbode=0){
        $equipo = $this->input->post('equipo');
        $bodega = $this->input->post('bod');
        $cantidad = $this->input->post('cantidad');
        $idrow = $this->input->post('idrow');
        $idcliente = $this->input->post('idcliente');
        //$bodegasrow = $this->ModeloCatalogos->getselectwheren('bodegas',array('activo'=>1));
        $bodegasrow=$this->ModeloCatalogos->view_session_bodegas();
        
        $seriesrowsnum=0;
        if($idsbode=='xx'){
            $xx=' selected';
            $activo = array('activo' => 1,'productoid'=>$equipo,'status <=' => 1);
        }else{
            $xx='';
            $activo = array('activo' => 1,'bodegaId'=>$bodega,'productoid'=>$equipo,'status <=' => 1);
        }
        $seriesrows = $this->ModeloCatalogos->getselectwherenall('series_productos',$activo);

            $html='<div class="row"><input type="hidden" id="idCliente" value="'.$idcliente.'">
                        <div class="input-field col s6 m3 ">
                            <input placeholder="Cantidad" id="cant_requ_renta" type="text" class="validate cant_requ_div" value="'.$cantidad.'" readonly>
                            <input placeholder="Cantidad" id="equipo_renta" type="hidden" class="validate" value="'.$idrow.'" readonly>
                            <label for="cant_requ" class="active cant_requ_div">Cantidad Requeridos</label>
                        </div>
                        <div class="input-field col s6 m4 ">
                            <select id="newbodegaer" class="form-control chosen-select" onchange="asignarer_new('.$idrow.','.$cantidad.','.$equipo.','.$idcliente.')">
                                <option value="0">Bodega seleccionada</option>
                                <option value="xx" '.$xx.'>Todas</option>
                                ';
                                foreach ($bodegasrow as $itemb) {
                                        if ($itemb->bodegaId==$idsbode) {
                                            $selectedb='selected';
                                        }else{
                                            $selectedb='';
                                        }
                                        $html.='<option value="'.$itemb->bodegaId.'" '.$selectedb.'>'.$itemb->bodega.'</option>';  
                                }
                         $html.='
                            </select>
                        </div>
                    </div>
                    ';
        
        
        foreach ($seriesrows as $item) {
            $html.= '<div class="col s6 m3 serieIdp_renta_ck_'.$item->serieId.'" >
                    <div class="card" >
                        <div class="card-content" style="padding:15px" >
                          <input type="checkbox" id="serieIdp_renta_'.$item->serieId.'" class="checkproducto_renta bodega_'.$item->bodegaId.'" onclick="selectedserie($(this))"
                          name="checkproducto_renta"  value="'.$item->serieId.'" >
                          <label for="serieIdp_renta_'.$item->serieId.'" style="padding-left:22px">'.$item->serie.'</label> 
                        </div >
                        
                      </div>
                </div>
                
                                    ';
            $seriesrowsnum=1;
        }
        if ($seriesrowsnum==0) {
            $html.='No se encuentran disponibles series disponibles';
        }
        echo $html;
    }
    function seriesa($idsbode=0){
        $accesorio = $this->input->post('accesorio');
        $bodega = $this->input->post('bod');
        $cantidad = $this->input->post('cantidad');
        $idrow = $this->input->post('idrow');
        //$bodegasrow = $this->ModeloCatalogos->getselectwheren('bodegas',array('activo'=>1));
        //$bodegasrow=$this->ModeloCatalogos->view_session_bodegas();
        $strq="SELECT * from bodegas where activo=1";
        $query=$this->db->query($strq);
        $bodegasrow=$query->result();
        if($bodega>0){
            $activo = array('activo' => 1,'bodegaId'=>$bodega,'accesoriosid'=>$accesorio,'status <=' => 1);
        }else{
            $activo = array('activo' => 1,'accesoriosid'=>$accesorio,'status <=' => 1);
        }
        
        $seriesrows = $this->ModeloCatalogos->getselectwherenall('series_accesorios',$activo);
        $seriesrowsnum=0;
            $html='<div class="row">
                        <div class="input-field col s6 m3 ">
                            <input placeholder="Cantidad" id="cant_requ_a" type="text" class="validate cant_requ_div" value="'.$cantidad.'" readonly>
                            <input placeholder="Cantidad" id="accesorio" type="hidden" class="validate " value="'.$idrow.'" readonly>
                            <label for="cant_requ" class="active cant_requ_div">Cantidad Requeridos</label>
                        </div>
                        <div class="input-field col s6 m4 ">
                            <select id="newbodegaac" class="form-control chosen-select" onchange="asignara_new('.$idrow.','.$cantidad.','.$accesorio.')">
                                <option value="0">Bodega seleccionada</option>
                                ';
                                foreach ($bodegasrow as $itemb) {
                                        if ($itemb->bodegaId==$idsbode) {
                                            $selectedb='selected';
                                        }else{
                                            $selectedb='';
                                        }
                                        $html.='<option value="'.$itemb->bodegaId.'" '.$selectedb.'>'.$itemb->bodega.'</option>';    
                                }
                         $html.='
                            </select>
                        </div>
                    </div>

                    ';
        foreach ($seriesrows as $item) {
            if ($item->con_serie==1) {
            $html.= '<div class="col s6 m3">
                    <div class="card">
                        <div class="card-content" style="padding:15px">
                          <input type="checkbox" id="serieIdp_'.$item->serieId.'" class="checkproductocs bodega_'.$item->bodegaId.'" 
                          name="checkproducto"  value="'.$item->serieId.'" data-con="1">
                          <label for="serieIdp_'.$item->serieId.'" style="padding-left:22px">'.$item->serie.'</label> 
                        </div >
                        
                      </div>
                </div>';
            }else{
                $cant_acc=$item->cantidad-$item->resguardar_cant;
                $html.= '<div class="col s6 m3">
                    <div class="card">
                        <div class="card-content" style="padding-left: 5px;padding-right: 5px;">
                            <div class="row checkproductossrow" style="margin-bottom: 0px;">
                                <div class="col s8">
                                    <input type="checkbox" id="serieIdp_'.$item->serieId.'" class="checkproducto bodega_'.$item->bodegaId.'" 
                                        name="checkproducto"  value="'.$item->serieId.'" data-con="0">
                                    <label for="serieIdp_'.$item->serieId.'" style="padding-left:22px">'.$item->serie.'</label>
                                </div >
                                <div class="col s4">
                                    <input type="number" id="serieIdpac" class="checkproductossc" value="" min="1" max="'.$cant_acc.'" style="margin: 0px;height: 24px;" data-max='.$cant_acc.'" >
                                </div >
                            </div >
                        </div >
                        
                      </div>
                </div>';
            }    
                                    
            $seriesrowsnum=1;
        }
        if ($seriesrowsnum==0) {
            $html.='No se encuentran disponibles series disponibles';
        }
        echo $html;
    }
    // accesorios rentas
    function seriesarentas($idsbode=0){
        $accesorio = $this->input->post('accesorio');
        $bodega = $this->input->post('bod');
        $cantidad = $this->input->post('cantidad');
        $idrow = $this->input->post('idrow');
        //$bodegasrow = $this->ModeloCatalogos->getselectwheren('bodegas',array('activo'=>1));
        $bodegasrow=$this->ModeloCatalogos->view_session_bodegas();
        //$activo = array('activo' => 1,'bodegaId'=>$bodega,'accesoriosid'=>$accesorio,'status <=' => 1);
        $activo = array('activo' => 1,'accesoriosid'=>$accesorio,'status <=' => 1);
        if($idsbode>0){
            $activo['bodegaId']=$bodega;
        }
        $seriesrows = $this->ModeloCatalogos->getselectwherenall('series_accesorios',$activo);
        $seriesrowsnum=0;
            $html='<div class="row">
                        <div class="input-field col s6 m3 ">
                            <input placeholder="Cantidad" id="cant_requ_a_rentas" type="text" class="validate cant_requ_div_rentas" value="'.$cantidad.'" readonly>
                            <input placeholder="Cantidad" id="accesorio_rentas" type="hidden" class="validate " value="'.$idrow.'" readonly>
                            <label for="cant_requ" class="active cant_requ_div_rentas">Cantidad Requeridos</label>
                        </div>
                        <div class="input-field col s6 m4 ">
                            <select id="newbodegaacr" class="form-control chosen-select" onchange="asignara_renta_new('.$idrow.','.$cantidad.','.$accesorio.')">
                                <option value="0">Bodega seleccionada</option>
                                ';
                                foreach ($bodegasrow as $itemb) {
                                        if ($itemb->bodegaId==$idsbode) {
                                            $selectedb='selected';
                                        }else{
                                            $selectedb='';
                                        }
                                        $html.='<option value="'.$itemb->bodegaId.'" '.$selectedb.'>'.$itemb->bodega.'</option>';    
                                }
                         $html.='
                            </select>
                        </div>
                    </div>

                    ';
        foreach ($seriesrows as $item) {
            if ($item->con_serie==1) {
                $html.= '<div class="col s6 m3">
                        <div class="card">
                            <div class="card-content" style="padding:15px">
                              <input type="checkbox" id="serieIdp_renta_'.$item->serieId.'" class="checkproducto_rentacs bodega_'.$item->bodegaId.'" 
                              name="checkproducto_renta"  value="'.$item->serieId.'">
                              <label for="serieIdp_renta_'.$item->serieId.'" style="padding-left:22px">'.$item->serie.'</label> 
                            </div >
                            
                          </div>
                    </div>';
            }else{
                $html.= '<div class="col s6 m3 checkproductossrow" style="margin:0px">
                            <div class="card">
                                <div class="card-content" style="padding-left: 5px; padding-top: 15px;padding-bottom: 15px;padding-right: 5px;">
                                    <div class="row checkproductossrow" style="margin:0px">
                                        <div class="col s8">
                                            <input type="checkbox" id="serieIdp_renta_'.$item->serieId.'" class="checkproducto_renta bodega_'.$item->bodegaId.'" 
                                            name="checkproducto_renta"  value="'.$item->serieId.'">
                                            <label for="serieIdp_renta_'.$item->serieId.'" style="padding-left:22px">'.$item->serie.'</label> 
                                        </div>
                                        <div class="col s4">
                                            <input type="number" id="serieIdpac" class="checkproductossc" value="" min="1" max="'.$item->cantidad.'" style="margin: 0px;height: 24px;" data-max='.$item->cantidad.'" >
                                        </div>
                                    </div >
                                </div>
                            </div>
                        </div>';
            }
            $seriesrowsnum=1;
        }
        if ($seriesrowsnum==0) {
            $html.='No se encuentran disponibles series disponibles';
        }
        echo $html;
    }
    //
    function seriesr($idsbode=0){
        $refaccion = $this->input->post('refaccion');
        $bodega = $this->input->post('bod');
        $cantidad = $this->input->post('cantidad');
        $idrow = $this->input->post('idrow');
        //$bodegasrow = $this->ModeloCatalogos->getselectwheren('bodegas',array('activo'=>1));
        //$bodegasrow=$this->ModeloCatalogos->view_session_bodegas();
        $strq="SELECT * from bodegas where activo=1";
        $query=$this->db->query($strq);
        $bodegasrow=$query->result();
        if($bodega>0){
            $activo = array('activo' => 1,'bodegaId'=>$bodega,'refaccionid'=>$refaccion,'status <=' => 1);
        }else{
            $activo = array('activo' => 1,'refaccionid'=>$refaccion,'status <=' => 1);
        }
        
        $seriesrows = $this->ModeloCatalogos->getselectwherenall('series_refacciones',$activo);
        $seriesrowsnum=0;
            $html='<div class="row">
                        <div class="input-field col s6 m3 ">
                            <input placeholder="Cantidad" id="cant_requ_r" type="text" class="validate cant_requ_div" value="'.$cantidad.'" readonly>
                            <input placeholder="Cantidad" id="refaccion" type="hidden" class="validate" value="'.$idrow.'" readonly>
                            <label for="cant_requ" class="active cant_requ_div">Cantidad Requeridos</label>
                        </div>
                        <div class="input-field col s6 m4 ">
                            <select id="newbodegaarr" class="form-control chosen-select" onchange="asignarr_new('.$idrow.','.$cantidad.','.$refaccion.')">
                                <option value="0">Bodega seleccionada</option>
                                ';
                                foreach ($bodegasrow as $itemb) {
                                        if ($itemb->bodegaId==$idsbode) {
                                            $selectedb='selected';
                                        }else{
                                            $selectedb='';
                                        }
                                        $html.='<option value="'.$itemb->bodegaId.'" '.$selectedb.'>'.$itemb->bodega.'</option>';    
                                }
                         $html.='
                            </select>
                        </div>
                    </div>

                    ';
        foreach ($seriesrows as $item) {
            if ($item->con_serie==1) {
                $html.= '<div class="col s6 m4">
                    <div class="card">
                        <div class="card-content" style="padding:15px">
                          <input type="checkbox" id="serieIdp_'.$item->serieId.'" class="checkproductosss bodega_'.$item->bodegaId.'" 
                          name="checkproducto"  value="'.$item->serieId.'" data-con="1">
                          <label for="serieIdp_'.$item->serieId.'" style="padding-left:22px">'.$item->serie.'</label> 
                        </div >
                        
                      </div>
                </div>
                ';
            }else{
                $cant_ref=$item->cantidad-$item->resguardar_cant;
                $html.= '<div class="col s6 m4 checkproductossrow">
                    <div class="card">
                        <div class="card-content" style="padding-left: 5px;padding-right: 5px;padding-top: 13px;padding-bottom: 13px;">
                            <div class="row checkproductossrow" style="margin-bottom: 0px;">
                                <div class="col s8">
                                    <input type="checkbox" id="serieIdp_'.$item->serieId.'" class="checkproductoss bodega_'.$item->bodegaId.'" name="checkproducto"  value="'.$item->serieId.'" data-con="0">
                                        <label for="serieIdp_'.$item->serieId.'" style="padding-left:22px">'.$item->serie.'</label> 
                                </div>
                                <div class="col s4">
                                    <input type="number" id="serieIdpc" class="checkproductossc" value="" min="1" max="'.$cant_ref.'" style="margin: 0px;height: 24px;" data-max='.$cant_ref.'" >
                                </div>
                            </div> 
                            
                          
                        </div >
                        
                      </div>
                </div>
                ';
            }
            
            $seriesrowsnum=1;
        }
        if ($seriesrowsnum==0) {
            $html.='No se encuentran disponibles series disponibles';
        }
        echo $html;
    }
    function addaseriese(){
        $params = $this->input->post();
        $arrayreseries =$params['arrayreseries'];
        $equipo =$params['equipo'];
        //===============================================
        $resulte=$this->ModeloCatalogos->getselectwheren('ventas_has_detallesEquipos',array('id'=>$equipo));
        $idVenta=0;
        $modelo='';
        $idEquipo=0;
        foreach ($resulte->result() as $item) {
                $idVenta=$item->idVenta;
                $idEquipo=$item->idEquipo;
                $modelo=$item->modelo;
        }
        $resultv=$this->ModeloCatalogos->getselectwheren('ventas',array('id'=>$idVenta));
        $tipo=1;
        foreach ($resultv->result() as $item) {
            $tipo=$item->tipo;
            $clienteId=$item->idCliente;
        }
        if ($tipo==1) {
            $tipos=3;
        }else{
            $tipos=2;
        }
        //==============================================
        $ars = json_decode($arrayreseries);
        $ars_serie_bodega=0;
        for ($i=0;$i<count($ars);$i++) {
            $detallesequipos = array(
                'id_equipo'=>$equipo,
                'serieId'=>$ars[$i]->serie);
            $this->ModeloCatalogos->Insert('asignacion_series_equipos',$detallesequipos);
            $this->ModeloCatalogos->updateCatalogo('series_productos',array('status'=>$tipos),array('serieId'=>$ars[$i]->serie));
            $this->Configuraciones_model->registrarmovimientosseries($ars[$i]->serie,1);
            $result_inf_ser=$this->ModeloCatalogos->getselectwheren('series_productos',array('serieId'=>$ars[$i]->serie));
            foreach ($result_inf_ser->result() as $itemis) {
                $ars_serie_bodega=$itemis->bodegaId;
            }

            $this->ModeloCatalogos->Insert('bitacora_movimientos',array('entrada_salida'=>0,'descripcion_evento'=>'Salida para venta '.$idVenta.'','personal'=>$this->idpersonal,'tipo'=>1,'id_item'=>$equipo,'modeloId'=>$idEquipo,'modelo'=>$modelo,'serieId'=>$ars[$i]->serie,'bodega_o'=>$ars_serie_bodega,'clienteId'=>$clienteId));
        }
        $dataarray=array('serie_estatus'=>1);
        if ($params['newbodega']!=0 or $params['newbodega']!='xx' or $params['newbodega']!='undefined') {
           $dataarray['serie_bodega']= $params['newbodega'];
           if($params['newbodega']=='xx'){
                $dataarray['serie_bodega']=$ars_serie_bodega;     
            }
        }else{
            if($ars_serie_bodega>0){
                $dataarray['serie_bodega']=$ars_serie_bodega;     
            }
        }
        $where=array('id'=>$equipo);
        $this->ModeloCatalogos->updateCatalogo('ventas_has_detallesEquipos',$dataarray,$where);
        //$this->Modelobitacoras->Insert(array('contenido'=>'Se asigno series a equipos de venta: '.$idVenta,'nombretabla'=>'ventas','idtable'=>$idVenta,'tipo'=>'Insert','personalId'=>$this->idpersonal));
        
        
    }
    function addaseriese_vd(){//no se ocupa ya que las tablas ventasd nunca se usaron
        $params = $this->input->post();
        $arrayreseries =$params['arrayreseries'];
        $equipo =$params['equipo'];
        //===============================================
        $resulte=$this->ModeloCatalogos->getselectwheren('ventasd_has_detallesequipos',array('id'=>$equipo));
        $idVenta=0;
        foreach ($resulte->result() as $item) {
                $idVenta=$item->idVenta;
        }
        $resultv=$this->ModeloCatalogos->getselectwheren('ventasd',array('id'=>$idVenta));
        $tipo=1;
        foreach ($resultv->result() as $item) {
            $tipo=$item->tipo;
        }
        if ($tipo==1) {
            $tipos=3;
        }else{
            $tipos=2;
        }
        //==============================================
        $ars = json_decode($arrayreseries);
        $ars_serie_bodega=0;
        for ($i=0;$i<count($ars);$i++) {
            $detallesequipos = array(
                'id_equipo'=>$equipo,
                'serieId'=>$ars[$i]->serie);
            $this->ModeloCatalogos->Insert('ventasd_asignacion_series_equipos',$detallesequipos);
            $this->ModeloCatalogos->updateCatalogo('series_productos',array('status'=>$tipos),array('serieId'=>$ars[$i]->serie));
            
            $result_inf_ser=$this->ModeloCatalogos->getselectwheren('series_productos',array('serieId'=>$ars[$i]->serie));
            foreach ($result_inf_ser->result() as $itemis) {
                $ars_serie_bodega=$itemis->bodegaId;
            }


        }
        $dataarray=array('serie_estatus'=>1);
        if ($params['newbodega']!=0 or $params['newbodega']!='xx' or $params['newbodega']!='undefined') {
           $dataarray['serie_bodega']= $params['newbodega'];
           if($params['newbodega']=='xx'){
                $dataarray['serie_bodega']=$ars_serie_bodega;     
            }
        }else{
            if($ars_serie_bodega>0){
                $dataarray['serie_bodega']=$ars_serie_bodega;     
            }
        }
        $where=array('id'=>$equipo);
        $this->ModeloCatalogos->updateCatalogo('ventasd_has_detallesequipos',$dataarray,$where);
        //$this->Modelobitacoras->Insert(array('contenido'=>'Se asigno series a equipos de venta: '.$idVenta,'nombretabla'=>'ventas','idtable'=>$idVenta,'tipo'=>'Insert','personalId'=>$this->idpersonal));
        $this->ModeloCatalogos->Insert('bitacora_movimientos',array('id_item'=>$equipo,'modelo'=>'','tipo'=>1,'entrada_salida'=>0,'personal'=>$this->idpersonal,'descripcion_evento'=>'Salida para ventad '.$idVenta.''));
        
    }
    function addaseriesrentas(){
        $params=$this->input->post();
        $arrayreseries=$params['arrayreseries'];
        $equipo=$params['equipo'];
        $idcliente=$params['idcliente'];
        //===============================================
        $resulte=$this->ModeloCatalogos->getselectwheren('rentas_has_detallesEquipos',array('id'=>$equipo));
        $idVenta=0;
        $direcc_temp='';
        $idRenta=0;
        $idEquipo=0;
        $modelo='';
        foreach ($resulte->result() as $item) {
                $idVenta=$item->idRenta;
                $idRenta=$item->idRenta;
                $direcc_temp=$item->direcc_temp;
                $idEquipo=$item->idEquipo;
                $modelo=$item->modelo;
        }
        $resultv=$this->ModeloCatalogos->getselectwheren('ventas',array('id'=>$idVenta));
        $tipo=1;
        foreach ($resultv->result() as $item) {
            $tipo=$item->tipo;
        }
        if ($tipo==1) {
            $tipos=3;
            $arraystutus=array('status'=>$tipos);
        }else{
            $tipos=2;
            $arraystutus=array('status'=>$tipos,'bodegaId'=>6);
        }
        //==============================================
        $ars = json_decode($arrayreseries);
        $aux = 0;
        $ars_serie_bodega=0;
        for ($i=0;$i<count($ars);$i++) {
                $folio_equipo = $this->ModeloCatalogos->getselectwheren('asignacion_series_r_equipos',array('idCliente'=>$idcliente));
                foreach ($folio_equipo->result() as $itemf) {
                    $aux = $itemf->folio_equipo;
                }
                $result_inf_ser=$this->ModeloCatalogos->getselectwheren('series_productos',array('serieId'=>$ars[$i]->serie));
                foreach ($result_inf_ser->result() as $itemis) {
                    $ars_serie_bodega=$itemis->bodegaId;
                }
                $folio = $aux +1; 
                $detallesequipos = array(
                'id_equipo'=>$equipo,
                'serieId'=>$ars[$i]->serie,
                'folio_equipo'=>$folio,
                'idCliente'=>$idcliente,
                'bodega_or'=>$ars_serie_bodega
                );
                $this->agregaraserviciosexistentes($equipo,$ars[$i]->serie,$idRenta); //agrega el equipo a los sevicios existentes
            $this->ModeloCatalogos->Insert('asignacion_series_r_equipos',$detallesequipos);
            $this->ModeloCatalogos->updateCatalogo('series_productos',$arraystutus,array('serieId'=>$ars[$i]->serie));
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_contrato_a_e',array('serieId'=>$ars[$i]->serie),array('serieId'=>0,'idequipo'=>$equipo,'status'=>0));
            $this->Configuraciones_model->registrarmovimientosseries($ars[$i]->serie,1);
            $result_inf_ser=$this->ModeloCatalogos->getselectwheren('series_productos',array('serieId'=>$ars[$i]->serie));
            foreach ($result_inf_ser->result() as $itemis) {
                $ars_serie_bodega=$itemis->bodegaId;
            }

            $result_inf_con=$this->ModeloCatalogos->getselectwheren('contrato',array('idRenta'=>$idRenta));
            $idcontrato=0;
            foreach ($result_inf_con->result() as $itemisc) {
                $idcontrato=$itemisc->idcontrato;
            }
            if($idcontrato>0){
                $this->ModeloCatalogos->Insert('contrato_equipo_direccion',array('contrato'=>$idcontrato,'equiposrow'=>$equipo,'serieId'=>$ars[$i]->serie,'direccion'=>$direcc_temp));
            }


            $this->ModeloCatalogos->Insert('bitacora_movimientos',array('entrada_salida'=>0,'descripcion_evento'=>'Salida para rentas '.$idRenta.'','personal'=>$this->idpersonal,'tipo'=>1,'id_item'=>$equipo,'modeloId'=>$idEquipo,'modelo'=>$modelo,'serieId'=>$ars[$i]->serie,'bodega_o'=>$ars_serie_bodega,'clienteId'=>$idcliente));
        }
        $dataarray=array('serie_estatus'=>1);

        if ($params['newbodega']!=0 or $params['newbodega']!='xx' or $params['newbodega']!='undefined') {
           $dataarray['serie_bodega']= $params['newbodega'];
           if($params['newbodega']=='xx'){
                $dataarray['serie_bodega']=$ars_serie_bodega;     
            }
        }else{
            if($ars_serie_bodega>0){
                $dataarray['serie_bodega']=$ars_serie_bodega;     
            }
        }
        $where=array('id'=>$equipo);
        $this->ModeloCatalogos->updateCatalogo('rentas_has_detallesEquipos',$dataarray,$where);
        //$this->Modelobitacoras->Insert(array('contenido'=>'Se asigno series a equipos de rentas: '.$idVenta,'nombretabla'=>'ventas','idtable'=>$idVenta,'tipo'=>'Insert','personalId'=>$this->idpersonal));
        
    }
    function addaseriesa(){
        $params = $this->input->post();
        $arrayreseries =$params['arrayreseries'];
        $arrayreseriesss =$params['arrayreseriesss'];
        $accesorio =$params['accesorio'];
        $bodega_o=0;
        //===============================================
        $resulte=$this->ModeloCatalogos->getselectwheren('ventas_has_detallesEquipos_accesorios',array('id_accesoriod'=>$accesorio));
        $idVenta=0;
        $id_accesorio=0;
        foreach ($resulte->result() as $item) {
            $idVenta=$item->id_venta;
            $id_accesorio = $item->id_accesorio;
            $bodega_o=$item->serie_bodega;
        }
        $resultv=$this->ModeloCatalogos->getselectwheren('ventas',array('id'=>$idVenta));
        $tipo=1;
        foreach ($resultv->result() as $item) {
            $tipo=$item->tipo;
        }
        if ($tipo==1) {
            $tipos=3;
        }else{
            $tipos=2;
        }
        //==============================================

        $ars = json_decode($arrayreseries);
        for ($i=0;$i<count($ars);$i++) {
            $detallesequipos = array(
                'id_accesorio'=>$accesorio,
                'serieId'=>$ars[$i]->serie);
            $this->ModeloCatalogos->Insert('asignacion_series_accesorios',$detallesequipos);
            $this->ModeloCatalogos->updateCatalogo('series_accesorios',array('status'=>$tipos),array('serieId'=>$ars[$i]->serie,'con_serie'=>1));
            $this->Configuraciones_model->registrarmovimientosseries($ars[$i]->serie,2);
            $this->ModeloCatalogos->Insert('bitacora_movimientos',array('entrada_salida'=>0,'descripcion_evento'=>'Salida para venta '.$idVenta,'personal'=>$this->idpersonal,'tipo'=>2,'id_item'=>$accesorio,'modeloId'=>$id_accesorio,'serieId'=>$ars[$i]->serie,'bodega_o'=>$bodega_o,));
            
        }
        $dataarray=array('serie_estatus'=>1);
        if ($params['newbodega']!=0) {
           $dataarray['serie_bodega']= $params['newbodega'];
           $bodega_o=$params['newbodega'];
        }
        $where=array('id_accesoriod'=>$accesorio);
        $this->ModeloCatalogos->updateCatalogo('ventas_has_detallesEquipos_accesorios',$dataarray,$where);
        //$this->Modelobitacoras->Insert(array('contenido'=>'Se asigno series a accesorios de venta: '.$idVenta,'nombretabla'=>'ventas','idtable'=>$idVenta,'tipo'=>'Insert','personalId'=>$this->idpersonal));
        $arsss = json_decode($arrayreseriesss);
        for ($i=0;$i<count($arsss);$i++) {
            $detallesequipos = array(
                'id_accesorio'=>$accesorio,
                'serieId'=>$arsss[$i]->serie);
            $this->ModeloCatalogos->Insert('asignacion_series_accesorios',$detallesequipos);
            
            $this->ModeloCatalogos->updateCatalogo('series_accesorios',array('status'=>0),array('serieId'=>$arsss[$i]->serie,'con_serie'=>1));
            $this->ModeloCatalogos->updatestock('series_accesorios','cantidad','-',$arsss[$i]->cantidad,'serieId',$arsss[$i]->serie);
            $this->Configuraciones_model->registrarmovimientosseries($arsss[$i]->serie,2);
            $this->ModeloCatalogos->Insert('bitacora_movimientos',array('entrada_salida'=>0,'descripcion_evento'=>'Salida para venta '.$idVenta,'personal'=>$this->idpersonal,'tipo'=>2,'id_item'=>$accesorio,'modeloId'=>$id_accesorio,'serieId'=>$arsss[$i]->serie,'bodega_o'=>$bodega_o, 'cantidad'=>$arsss[$i]->cantidad));
            
        }
    }
    function addaseriesa_vd(){//no se ocupa ya que las tablas ventasd nunca se usaron
        $params = $this->input->post();
        $arrayreseries =$params['arrayreseries'];
        $arrayreseriesss =$params['arrayreseriesss'];
        $accesorio =$params['accesorio'];
        //===============================================
        $resulte=$this->ModeloCatalogos->getselectwheren('ventasd_has_detallesequipos_accesorios',array('id_accesoriod'=>$accesorio));
        $idVenta=0;
        foreach ($resulte->result() as $item) {
                $idVenta=$item->id_venta;
        }
        $resultv=$this->ModeloCatalogos->getselectwheren('ventasd',array('id'=>$idVenta));
        $tipo=1;
        foreach ($resultv->result() as $item) {
            $tipo=$item->tipo;
        }
        if ($tipo==1) {
            $tipos=3;
        }else{
            $tipos=2;
        }
        //==============================================

        $ars = json_decode($arrayreseries);
        for ($i=0;$i<count($ars);$i++) {
            $detallesequipos = array(
                'id_accesorio'=>$accesorio,
                'serieId'=>$ars[$i]->serie);
            $this->ModeloCatalogos->Insert('ventasd_asignacion_series_accesorios',$detallesequipos);
            $this->ModeloCatalogos->updateCatalogo('series_accesorios',array('status'=>$tipos),array('serieId'=>$ars[$i]->serie));

            $this->ModeloCatalogos->Insert('bitacora_movimientos',array('id_item'=>$accesorio,'modelo'=>'','tipo'=>2,'entrada_salida'=>0,'personal'=>$this->idpersonal,'descripcion_evento'=>'Salida para venta '.$idVenta.''));
            
        }
        $dataarray=array('serie_estatus'=>1);
        if ($params['newbodega']!=0) {
           $dataarray['serie_bodega']= $params['newbodega'];
        }
        $where=array('id_accesoriod'=>$accesorio);
        $this->ModeloCatalogos->updateCatalogo('ventasd_has_detallesequipos_accesorios',$dataarray,$where);
        //$this->Modelobitacoras->Insert(array('contenido'=>'Se asigno series a accesorios de venta: '.$idVenta,'nombretabla'=>'ventas','idtable'=>$idVenta,'tipo'=>'Insert','personalId'=>$this->idpersonal));
        $arsss = json_decode($arrayreseriesss);
        for ($i=0;$i<count($arsss);$i++) {
            $this->ModeloCatalogos->updateCatalogo('series_accesorios',array('status'=>0),array('serieId'=>$arsss[$i]->serie));
            $this->ModeloCatalogos->updatestock('series_refacciones','cantidad','-',$arsss[$i]->cantidad,'serieId',$arsss[$i]->serie);

            $this->ModeloCatalogos->Insert('bitacora_movimientos',array('id_item'=>$accesorio,'modelo'=>'','cantidad'=>$arsss[$i]->cantidad,'tipo'=>2,'entrada_salida'=>0,'personal'=>$this->idpersonal,'descripcion_evento'=>'Salida para venta '.$idVenta.''));
            
        }
    }
    // accesorios rentas
    function addaseriesarentas(){
        $params = $this->input->post();
        $arrayreseries =$params['arrayreseries'];
        $arrayreseriesss =$params['arrayreseriesss'];
        $accesorio =$params['accesorio'];
        //===============================================
        $resulte=$this->ModeloCatalogos->getselectwheren('rentas_has_detallesEquipos_accesorios',array('id_accesoriod'=>$accesorio));
        $idVenta=0;
        $modeloId=0;
        $bodega_o=0;
        $clienteId=0;
        foreach ($resulte->result() as $item) {
                $idVenta=$item->idrentas;
                $modeloId=$item->id_accesorio;
                $bodega_o=$item->serie_bodega;
        }
        $resultvr=$this->ModeloCatalogos->getselectwheren('rentas',array('id'=>$idVenta));
        foreach ($resultvr->result() as $itemr) {
            $clienteId=$itemr->idCliente;
        }
        /*
        $resultv=$this->ModeloCatalogos->getselectwheren('ventas',array('id'=>$idVenta));
        $tipo=1;
        foreach ($resultv->result() as $item) {
            $tipo=$item->tipo;
        }
        if ($tipo==1) {
        */
            $tipos=3;
        /*
        }else{
            $tipos=2;
        }
        */
        //==============================================
        $ars_serie_bodega=0;
        $ars = json_decode($arrayreseries);
        for ($i=0;$i<count($ars);$i++) {
            $result_inf_ser=$this->ModeloCatalogos->getselectwheren('series_accesorios',array('serieId'=>$ars[$i]->serie));
            foreach ($result_inf_ser->result() as $itemis) {
                $ars_serie_bodega=$itemis->bodegaId;
            }
            $detallesequipos = array(
                'id_accesorio'=>$accesorio,
                'serieId'=>$ars[$i]->serie,
                'bodega_or'=>$ars_serie_bodega
            );
            $this->ModeloCatalogos->Insert('asignacion_series_r_accesorios',$detallesequipos);
            $this->ModeloCatalogos->updateCatalogo('series_accesorios',array('status'=>$tipos),array('serieId'=>$ars[$i]->serie));
            $this->Configuraciones_model->registrarmovimientosseries($ars[$i]->serie,2);
            $result_inf_ser=$this->ModeloCatalogos->getselectwheren('series_accesorios',array('serieId'=>$ars[$i]->serie));
            foreach ($result_inf_ser->result() as $itemis) {
                $ars_serie_bodega=$itemis->bodegaId;
            }
            $this->ModeloCatalogos->Insert('bitacora_movimientos',array('entrada_salida'=>0,'descripcion_evento'=>'Salida para renta '.$idVenta,'personal'=>$this->idpersonal,'tipo'=>2,'id_item'=>$accesorio,'modeloId'=>$modeloId, 'modelo'=>'','serieId'=>$ars[$i]->serie,'bodega_o'=>$ars_serie_bodega,'clienteId'=>$clienteId));
            
        }
        $dataarray=array('serie_estatus'=>1);
        if ($params['newbodega']!=0) {
           $dataarray['serie_bodega']= $params['newbodega'];
           $bodega_o=$params['newbodega'];
        }else{
            if($ars_serie_bodega>0){
                $dataarray['serie_bodega']=$ars_serie_bodega; 
                $bodega_o= $ars_serie_bodega; 
            }
        }
        $where=array('id_accesoriod'=>$accesorio);
        $this->ModeloCatalogos->updateCatalogo('rentas_has_detallesEquipos_accesorios',$dataarray,$where);
        //$this->Modelobitacoras->Insert(array('contenido'=>'Se asigno series a accesorios de venta: '.$idVenta,'nombretabla'=>'ventas','idtable'=>$idVenta,'tipo'=>'Insert','personalId'=>$this->idpersonal));
        $arsss = json_decode($arrayreseriesss);
        for ($i=0;$i<count($arsss);$i++) {

            $detallesequipos = array(
                'id_accesorio'=>$accesorio,
                'serieId'=>$arsss[$i]->serie);
            $this->ModeloCatalogos->Insert('asignacion_series_r_accesorios',$detallesequipos);

            $this->ModeloCatalogos->updateCatalogo('series_accesorios',array('status'=>0),array('serieId'=>$arsss[$i]->serie));
            $this->ModeloCatalogos->updatestock('series_accesorios','cantidad','-',$arsss[$i]->cantidad,'serieId',$arsss[$i]->serie);
            $this->Configuraciones_model->registrarmovimientosseries($arsss[$i]->serie,2);
            $this->ModeloCatalogos->Insert('bitacora_movimientos',array('entrada_salida'=>0,'descripcion_evento'=>'Salida para renta '.$idVenta,'personal'=>$this->idpersonal,'tipo'=>2,'id_item'=>$accesorio,'modeloId'=>$modeloId,'modelo'=>'','serieId'=>$arsss[$i]->serie,'bodega_o'=>$bodega_o,'clienteId'=>$clienteId,'cantidad'=>$arsss[$i]->cantidad));
            
        }

    }
    //
    function addaseriesr(){
        $params = $this->input->post();
        $arrayreseries =$params['arrayreseries'];
        $refaccion =$params['refaccion'];
        $arrayreseriesss = $params['arrayreseriesss'];
        
        $resultvr=$this->ModeloCatalogos->getselectwheren('ventas_has_detallesRefacciones',array('id'=>$refaccion));
        $idVentas=0;
        $idRefaccion=0;
        $modelo='';
        $bodega_o='';
        $clienteId=0;
        foreach ($resultvr->result() as $itemr) {
            $idVentas=$itemr->idVentas;
            $idRefaccion=$itemr->idRefaccion;
            $modelo=$itemr->modelo;
            $bodega_o=$itemr->serie_bodega;
        }
        $resultven=$this->ModeloCatalogos->getselectwheren('ventas',array('id'=>$idVentas));
        foreach ($resultven->result() as $itemven) {
            $clienteId=$itemven->idCliente;
        }



        $ars = json_decode($arrayreseries);
        //log_message('error','xx1'.json_encode($ars));
        for ($i=0;$i<count($ars);$i++) {
            $detallesequipos = array(
                'id_refaccion'=>$refaccion,
                'serieId'=>$ars[$i]->serie);
            $this->ModeloCatalogos->Insert('asignacion_series_refacciones',$detallesequipos);
            $this->ModeloCatalogos->updateCatalogo('series_refacciones',array('status'=>3),array('serieId'=>$ars[$i]->serie));
            $this->Configuraciones_model->registrarmovimientosseries($ars[$i]->serie,3);

            $this->ModeloCatalogos->Insert('bitacora_movimientos',array('entrada_salida'=>0,'descripcion_evento'=>'Salida para venta: '.$idVentas,'personal'=>$this->idpersonal,'tipo'=>3,'id_item'=>$refaccion,'modeloId'=>$idRefaccion, 'modelo'=>$modelo,'serieId'=>$ars[$i]->serie,'bodega_o'=>$bodega_o,'clienteId'=>$clienteId));
            
        }
        $arsss = json_decode($arrayreseriesss);
        //log_message('error','xx2'.json_encode($arsss));
        for ($i=0;$i<count($arsss);$i++) {
            $detallesequiposss = array(
                'id_refaccion'=>$refaccion,
                'serieId'=>$arsss[$i]->serie,
                'cantidad'=>$arsss[$i]->cantidad
            );
            $this->ModeloCatalogos->Insert('asignacion_series_refacciones',$detallesequiposss);
            $this->ModeloCatalogos->updateCatalogo('series_refacciones',array('status'=>0),array('serieId'=>$arsss[$i]->serie));
            $this->ModeloCatalogos->updatestock('series_refacciones','cantidad','-',$arsss[$i]->cantidad,'serieId',$arsss[$i]->serie);
            $this->Configuraciones_model->registrarmovimientosseries($arsss[$i]->serie,3);

            $this->ModeloCatalogos->Insert('bitacora_movimientos',array('entrada_salida'=>0,'descripcion_evento'=>'Salida para venta: '.$idVentas,'personal'=>$this->idpersonal,'tipo'=>3,'id_item'=>$refaccion,'modeloId'=>$idRefaccion,'modelo'=>$modelo,'serieId'=>$arsss[$i]->serie,'bodega_o'=>$bodega_o,'clienteId'=>$clienteId,'cantidad'=>$arsss[$i]->cantidad));
            
        }
        $dataarray=array('serie_estatus'=>1);
        if ($params['newbodega']!=0) {
           $dataarray['serie_bodega']= $params['newbodega'];
        }
        $where=array('id'=>$refaccion);
        $this->ModeloCatalogos->updateCatalogo('ventas_has_detallesRefacciones',$dataarray,$where);
        //$this->Modelobitacoras->Insert(array('contenido'=>'Se asigno series a refacciones','nombretabla'=>'ventas','idtable'=>0,'tipo'=>'insert','personalId'=>$this->idpersonal));
    }
    function addaseriesr_vd(){
        $params = $this->input->post();
        $arrayreseries =$params['arrayreseries'];
        $refaccion =$params['refaccion'];
        $arrayreseriesss = $params['arrayreseriesss'];
        

        $ars = json_decode($arrayreseries);
        for ($i=0;$i<count($ars);$i++) {
            $detallesequipos = array(
                'id_refaccion'=>$refaccion,
                'serieId'=>$ars[$i]->serie);
            $this->ModeloCatalogos->Insert('ventasd_asignacion_series_refacciones',$detallesequipos);
            $this->ModeloCatalogos->updateCatalogo('series_refacciones',array('status'=>3),array('serieId'=>$ars[$i]->serie));

            $this->ModeloCatalogos->Insert('bitacora_movimientos',array('id_item'=>$refaccion,'modelo'=>'','tipo'=>3,'entrada_salida'=>0,'personal'=>$this->idpersonal,'descripcion_evento'=>'Salida para ventad'));
            
        }
        $arsss = json_decode($arrayreseriesss);
        for ($i=0;$i<count($arsss);$i++) {
            $detallesequiposss = array(
                'id_refaccion'=>$refaccion,
                'serieId'=>$arsss[$i]->serie,
                'cantidad'=>$arsss[$i]->cantidad
            );
            $this->ModeloCatalogos->Insert('ventasd_asignacion_series_refacciones',$detallesequiposss);
            //$this->ModeloCatalogos->updateCatalogo('series_refacciones',array('status'=>3),array('serieId'=>$ars[$i]->serie));
            $this->ModeloCatalogos->updatestock('series_refacciones','cantidad','-',$arsss[$i]->cantidad,'serieId',$arsss[$i]->serie);

            $this->ModeloCatalogos->Insert('bitacora_movimientos',array('id_item'=>$refaccion,'modelo'=>'','cantidad'=>$arsss[$i]->cantidad,'tipo'=>3,'entrada_salida'=>0,'personal'=>$this->idpersonal,'descripcion_evento'=>'Salida para renta'));
            
        }
        $dataarray=array('serie_estatus'=>1);
        if ($params['newbodega']!=0) {
           $dataarray['serie_bodega']= $params['newbodega'];
        }
        $where=array('id'=>$refaccion);
        $this->ModeloCatalogos->updateCatalogo('ventasd_has_detallesrefacciones',$dataarray,$where);
        //$this->Modelobitacoras->Insert(array('contenido'=>'Se asigno series a refacciones','nombretabla'=>'ventas','idtable'=>0,'tipo'=>'insert','personalId'=>$this->idpersonal));
    }
    function seriesasignadase(){
        $equipo = $this->input->post('equipo');
        $seriesrows = $this->ModeloCatalogos->getequiposerie($equipo);
        $html='';
        foreach ($seriesrows->result() as $item) {
            $html.= '<div class="col s6 m3">
                    <div class="card">
                        <div class="card-content" style="padding:15px">
                          <input type="checkbox" id="serieIdpo_'.$item->serieId.'" class="checkproducto_o" 
                                name="checkproducto_o" 
                                value="'.$item->serieId.'">
                          <label for="serieIdpo_'.$item->serieId.'" style="padding-left:22px">'.$item->serie.'</label> 
                        </div >
                        
                      </div>
                </div>';
        }
        echo $html;
    }
    function seriesasignadase_vd(){
        $equipo = $this->input->post('equipo');
        $seriesrows = $this->ModeloCatalogos->getequiposerie_vd($equipo);
        $html='';
        foreach ($seriesrows->result() as $item) {
            $html.= '<div class="col s6 m3">
                    <div class="card">
                        <div class="card-content" style="padding:15px">
                          <input type="checkbox" id="serieIdpo_'.$item->serieId.'" class="checkproducto_o" 
                                name="checkproducto_o" 
                                value="'.$item->serieId.'">
                          <label for="serieIdpo_'.$item->serieId.'" style="padding-left:22px">'.$item->serie.'</label> 
                        </div >
                        
                      </div>
                </div>';
        }
        echo $html;
    }
    // rentas
    function seriesasignadaserentas(){
        $equipo = $this->input->post('equipo');
        $seriesrows = $this->ModeloCatalogos->getequiposerierentas($equipo);
        $html='';
        foreach ($seriesrows->result() as $item) {
            $html.= '<div class="col s6 m3">
                    <div class="card">
                        <div class="card-content" style="padding:15px">
                          <input type="checkbox" id="serieIdpo_renta_'.$item->serieId.'" class="checkproducto_o_renta" 
                                name="checkproducto_o_renta" 
                                value="'.$item->serieId.'">
                          <label for="serieIdpo_renta_'.$item->serieId.'" style="padding-left:22px">'.$item->serie.'</label> 
                        </div >
                        
                      </div>
                </div>';
        }
        echo $html;
    }
    //
    function seriesasignadasa(){
        $accesorio = $this->input->post('accesorio');
        $seriesrows = $this->ModeloCatalogos->getaccesorioserie($accesorio);
        $html='';
        foreach ($seriesrows->result() as $item) {
            $html.= '<div class="col s6 m3">
                    <div class="card">
                        <div class="card-content" style="padding:15px">
                          <input type="checkbox" id="serieIdpo_'.$item->serieId.'" class="checkproducto_o" 
                                name="checkproducto_o" 
                                value="'.$item->serieId.'">
                          <label for="serieIdpo_'.$item->serieId.'" style="padding-left:22px">'.$item->serie.'</label> 
                        </div >
                        
                      </div>
                </div>';
        }
        echo $html;
    }
    function seriesasignadasa_vd(){
        $accesorio = $this->input->post('accesorio');
        $seriesrows = $this->ModeloCatalogos->getaccesorioserie_vd($accesorio);
        $html='';
        foreach ($seriesrows->result() as $item) {
            $html.= '<div class="col s6 m3">
                    <div class="card">
                        <div class="card-content" style="padding:15px">
                          <input type="checkbox" id="serieIdpo_'.$item->serieId.'" class="checkproducto_o" 
                                name="checkproducto_o" 
                                value="'.$item->serieId.'">
                          <label for="serieIdpo_'.$item->serieId.'" style="padding-left:22px">'.$item->serie.'</label> 
                        </div >
                        
                      </div>
                </div>';
        }
        echo $html;
    }
    //rentas accesorios
    function seriesasignadasa_rentas(){
        $accesorio = $this->input->post('accesorio');
        $seriesrows = $this->ModeloCatalogos->getrefaccionserierentas($accesorio);
        $html='';
        foreach ($seriesrows->result() as $item) {
            $html.= '<div class="col s6 m3">
                    <div class="card">
                        <div class="card-content" style="padding:15px">
                          <input type="checkbox" id="serieIdpo_rentas_'.$item->serieId.'" class="checkproducto_o_renta" 
                                name="checkproducto_o_renta" 
                                value="'.$item->serieId.'">
                          <label for="serieIdpo_rentas_'.$item->serieId.'" style="padding-left:22px">'.$item->serie.'</label> 
                        </div >
                        
                      </div>
                </div>';
        }
        echo $html;
    }
    //
    function seriesasignadasr(){
        $refaccion = $this->input->post('refaccion');
        $seriesrows = $this->ModeloCatalogos->getrefaccionserie($refaccion);
        $html='';
        foreach ($seriesrows->result() as $item) {
            if ($item->cantidad==1) {
                $cons=1;
            }else{
                $cons=0;
            }
            $html.= '<div class="col s6 m3">
                    <div class="card">
                        <div class="card-content" style="padding:15px">
                          <input type="checkbox" id="serieIdpo_'.$item->serieId.'" class="checkproducto_o" 
                                name="checkproducto_o" 
                                value="'.$item->serieId.'" data-con="'.$cons.'">
                          <label for="serieIdpo_'.$item->serieId.'" style="padding-left:22px">'.$item->serie.'</label> 
                        </div >
                        
                      </div>
                </div>';
            
        }
        echo $html;
    }
    function seriesasignadasr_vd(){
        $refaccion = $this->input->post('refaccion');
        $seriesrows = $this->ModeloCatalogos->getrefaccionserie_vd($refaccion);
        $html='';
        foreach ($seriesrows->result() as $item) {
            if ($item->cantidad==1) {
                $cons=1;
            }else{
                $cons=0;
            }
            $html.= '<div class="col s6 m3">
                    <div class="card">
                        <div class="card-content" style="padding:15px">
                          <input type="checkbox" id="serieIdpo_'.$item->serieId.'" class="checkproducto_o" 
                                name="checkproducto_o" 
                                value="'.$item->serieId.'" data-con="'.$cons.'">
                          <label for="serieIdpo_'.$item->serieId.'" style="padding-left:22px">'.$item->serie.'</label> 
                        </div >
                        
                      </div>
                </div>';
            
        }
        echo $html;
    }
    function equipocambio(){
        $regreso = $this->input->post('regreso');
        $nuevo = $this->input->post('nuevo');
        $equipotr = $this->input->post('equipotr');
        //========================================================
            $deletewhere=array('id_equipo'=>$equipotr,'serieId'=>$regreso);
            $this->ModeloCatalogos->getdeletewheren('asignacion_series_equipos',$deletewhere);
        //========================================================
            $detallesequipos = array(
                'id_equipo'=>$equipotr,
                'serieId'=>$nuevo);
            $this->ModeloCatalogos->Insert('asignacion_series_equipos',$detallesequipos);
        //========================================================
            $this->ModeloCatalogos->updateCatalogo('series_productos',array('status'=>0),array('serieId'=>$regreso));
            $this->ModeloCatalogos->updateCatalogo('series_productos',array('status'=>3),array('serieId'=>$nuevo));
    }
    function equipocambio_vd(){
        $regreso = $this->input->post('regreso');
        $nuevo = $this->input->post('nuevo');
        $equipotr = $this->input->post('equipotr');
        //========================================================
            $deletewhere=array('id_equipo'=>$equipotr,'serieId'=>$regreso);
            $this->ModeloCatalogos->getdeletewheren('ventasd_asignacion_series_equipos',$deletewhere);
        //========================================================
            $detallesequipos = array(
                'id_equipo'=>$equipotr,
                'serieId'=>$nuevo);
            $this->ModeloCatalogos->Insert('ventasd_asignacion_series_equipos',$detallesequipos);
        //========================================================
            $this->ModeloCatalogos->updateCatalogo('series_productos',array('status'=>0),array('serieId'=>$regreso));
            $this->ModeloCatalogos->updateCatalogo('series_productos',array('status'=>3),array('serieId'=>$nuevo));
    }
    // rentas
    function equipocambiorentas(){
        $regreso = $this->input->post('regreso');
        $nuevo = $this->input->post('nuevo');
        $equipotr = $this->input->post('equipotr');
        //var_dump($regreso.'|'.$nuevo.'|'.$equipotr);die; 
        $wheredetallesequipos = array(
                'id_equipo'=>$equipotr,
                'serieId'=>$regreso);   
        $folio_equipo=0; 
        $idCliente=0;            
        $resulte=$this->ModeloCatalogos->getselectwheren('asignacion_series_r_equipos',$wheredetallesequipos);
        foreach ($resulte->result() as $item) {
                $folio_equipo=$item->folio_equipo;
                $idCliente=$item->idCliente;
        }
        //========================================================
            $detallesequipos = array(
                'id_equipo'=>$equipotr,
                'serieId'=>$nuevo,
                'folio_equipo'=>$folio_equipo,
                'idCliente'=>$idCliente);   
            $this->ModeloCatalogos->Insert('asignacion_series_r_equipos',$detallesequipos);
        //========================================================
            $deletewhere=array('id_equipo'=>$equipotr,'serieId'=>$regreso);
            $this->ModeloCatalogos->getdeletewheren('asignacion_series_r_equipos',$deletewhere);
        //========================================================
            $this->ModeloCatalogos->updateCatalogo('series_productos',array('status'=>0),array('serieId'=>$regreso));
            $this->ModeloCatalogos->updateCatalogo('series_productos',array('status'=>3),array('serieId'=>$nuevo));

            //$this->ModeloCatalogos->Insert('bitacora_movimientos',array('id_item'=>$equipotr,'modelo'=>'','tipo'=>1,'entrada_salida'=>1,'personal'=>$this->idpersonal,'descripcion_evento'=>'Devolucion por cambio de serie'));
            //$this->ModeloCatalogos->Insert('bitacora_movimientos',array('id_item'=>$equipotr,'modelo'=>'','tipo'=>1,'entrada_salida'=>0,'personal'=>$this->idpersonal,'descripcion_evento'=>'Salida para renta'));
    }
    //
    function accesoriocambio(){
        $regreso = $this->input->post('regreso');
        $nuevo = $this->input->post('nuevo');
        $accesoriotr = $this->input->post('accesoriotr');
        //========================================================
            $deletewhere=array('id_accesorio'=>$accesoriotr,'serieId'=>$regreso);
            $this->ModeloCatalogos->getdeletewheren('asignacion_series_accesorios',$deletewhere);
        //========================================================
            $detallesequipos = array(
                'id_accesorio'=>$accesoriotr,
                'serieId'=>$nuevo);
            $this->ModeloCatalogos->Insert('asignacion_series_accesorios',$detallesequipos);
        //========================================================
            $this->ModeloCatalogos->updateCatalogo('series_accesorios',array('status'=>0),array('serieId'=>$regreso));
            $this->ModeloCatalogos->updateCatalogo('series_accesorios',array('status'=>3),array('serieId'=>$nuevo));

            //$this->ModeloCatalogos->Insert('bitacora_movimientos',array('id_item'=>$accesoriotr,'modelo'=>'','tipo'=>2,'entrada_salida'=>1,'personal'=>$this->idpersonal,'descripcion_evento'=>'Devolucion por cambio de serie'));
            //$this->ModeloCatalogos->Insert('bitacora_movimientos',array('id_item'=>$accesoriotr,'modelo'=>'','tipo'=>2,'entrada_salida'=>0,'personal'=>$this->idpersonal,'descripcion_evento'=>'Salida para venta'));
    }
    // accesorios rentas editar
    function accesoriocambiorentas(){
        $regreso = $this->input->post('regreso');
        $nuevo = $this->input->post('nuevo');
        $accesoriotr = $this->input->post('accesoriotr');
        var_dump($regreso.'|'.$nuevo.'|'.$accesoriotr);
        //========================================================
            $deletewhere=array('id_accesorio'=>$accesoriotr,'serieId'=>$regreso);
            $this->ModeloCatalogos->getdeletewheren('asignacion_series_r_accesorios',$deletewhere);
        //========================================================
            $detallesequipos = array(
                'id_accesorio'=>$accesoriotr,
                'serieId'=>$nuevo);
            $this->ModeloCatalogos->Insert('asignacion_series_r_accesorios',$detallesequipos);
        //========================================================
            $this->ModeloCatalogos->updateCatalogo('series_accesorios',array('status'=>0),array('serieId'=>$regreso));
            $this->ModeloCatalogos->updateCatalogo('series_accesorios',array('status'=>3),array('serieId'=>$nuevo));

            //$this->ModeloCatalogos->Insert('bitacora_movimientos',array('id_item'=>$accesoriotr,'modelo'=>'','tipo'=>2,'entrada_salida'=>1,'personal'=>$this->idpersonal,'descripcion_evento'=>'Devolucion por cambio de serie'));
            //$this->ModeloCatalogos->Insert('bitacora_movimientos',array('id_item'=>$accesoriotr,'modelo'=>'','tipo'=>2,'entrada_salida'=>0,'personal'=>$this->idpersonal,'descripcion_evento'=>'Salida para rentas'));
    }
    //
    function refaccioncambio(){$regreso = $this->input->post('regreso');
        $nuevo = $this->input->post('nuevo');
        $refacciontr = $this->input->post('refacciontr');
        $tipo=$this->input->post('tipo');
        if ($tipo==1) {
            //========================================================
                $deletewhere=array('id_refaccion'=>$refacciontr,'serieId'=>$regreso);
                $this->ModeloCatalogos->getdeletewheren('asignacion_series_refacciones',$deletewhere);
            //========================================================
                $detallesequipos = array(
                    'id_refaccion'=>$refacciontr,
                    'serieId'=>$nuevo);
                $this->ModeloCatalogos->Insert('asignacion_series_refacciones',$detallesequipos);
            //========================================================
                $this->ModeloCatalogos->updateCatalogo('series_refacciones',array('status'=>3),array('serieId'=>$nuevo));
                $this->ModeloCatalogos->updateCatalogo('series_refacciones',array('status'=>0),array('serieId'=>$regreso));

            //$this->ModeloCatalogos->Insert('bitacora_movimientos',array('id_item'=>$refacciontr,'modelo'=>'','tipo'=>3,'entrada_salida'=>1,'personal'=>$this->idpersonal,'descripcion_evento'=>'Devolucion por cambio de serie'));
            //$this->ModeloCatalogos->Insert('bitacora_movimientos',array('id_item'=>$refacciontr,'modelo'=>'','tipo'=>3,'entrada_salida'=>0,'personal'=>$this->idpersonal,'descripcion_evento'=>'Salida para venta'));
        }else{
            $where=array('id_refaccion'=>$refacciontr,'serieId'=>$regreso);
            $result=$this->ModeloCatalogos->getselectwheren('asignacion_series_refacciones',$where);
            $cantidad=1;
            foreach ($result->result() as $itemr) {
                $cantidad=$itemr->cantidad;
            }
            $this->ModeloCatalogos->updatestock('series_refacciones','cantidad',$masmeno,$value2,$idname,$id);
            //pendiente
        }       
    } 
    function refaccioncambio_vd(){$regreso = $this->input->post('regreso');
        $nuevo = $this->input->post('nuevo');
        $refacciontr = $this->input->post('refacciontr');
        $tipo=$this->input->post('tipo');
        if ($tipo==1) {
            //========================================================
                $deletewhere=array('id_refaccion'=>$refacciontr,'serieId'=>$regreso);
                $this->ModeloCatalogos->getdeletewheren('ventasd_asignacion_series_refacciones',$deletewhere);
            //========================================================
                $detallesequipos = array(
                    'id_refaccion'=>$refacciontr,
                    'serieId'=>$nuevo);
                $this->ModeloCatalogos->Insert('ventasd_asignacion_series_refacciones',$detallesequipos);
            //========================================================
                $this->ModeloCatalogos->updateCatalogo('series_refacciones',array('status'=>3),array('serieId'=>$nuevo));
                $this->ModeloCatalogos->updateCatalogo('series_refacciones',array('status'=>0),array('serieId'=>$regreso));

            //$this->ModeloCatalogos->Insert('bitacora_movimientos',array('id_item'=>$refacciontr,'modelo'=>'','tipo'=>3,'entrada_salida'=>1,'personal'=>$this->idpersonal,'descripcion_evento'=>'Devolucion por cambio de serie'));
            //$this->ModeloCatalogos->Insert('bitacora_movimientos',array('id_item'=>$refacciontr,'modelo'=>'','tipo'=>3,'entrada_salida'=>0,'personal'=>$this->idpersonal,'descripcion_evento'=>'Salida para venta'));
        }else{
            $where=array('id_refaccion'=>$refacciontr,'serieId'=>$regreso);
            $result=$this->ModeloCatalogos->getselectwheren('ventasd_asignacion_series_refacciones',$where);
            $cantidad=1;
            foreach ($result->result() as $itemr) {
                $cantidad=$itemr->cantidad;
            }
            $this->ModeloCatalogos->updatestock('series_refacciones','cantidad',$masmeno,$value2,$idname,$id);
            //pendiente
        }       
    } 
    public function getData_asignacion_ar() {
        $params = $this->input->post();
        $serie = $this->ModeloAsignacion->Getventas_accesoriosr($params);
        $totalRecords=$this->ModeloAsignacion->Totalventas_accesoriosr($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totalRecords),  
            "recordsFiltered" => intval($totalRecords),
            "data"            => $serie->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    public function getData_asignacion_er() {
        $params = $this->input->post();
        $serie = $this->ModeloAsignacion->Getventas_equiposr($params);
        $totalRecords=$this->ModeloAsignacion->Totalventas_equiposr($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totalRecords),  
            "recordsFiltered" => intval($totalRecords),
            "data"            => $serie->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    function agregaraserviciosexistentes($equipo,$serie,$idRenta){
        $idcontrato=0;
        /*
        $resultr = $this->ModeloCatalogos->getselectwheren('rentas_has_detallesEquipos',array('id'=>$equipo));
        $idRenta=0;
        
        foreach ($resultr->result() as $item) {
            $idRenta=$item->idRenta;
        }
        */
        if ($idRenta>0) {
            $resultc = $this->ModeloCatalogos->getselectwheren('contrato',array('idRenta'=>$idRenta));
            foreach ($resultc->result() as $item) {
                $idcontrato=$item->idcontrato;
                //log_message('error', 'log_2 idcontrato: '.$idcontrato);
            }
            if ($idcontrato>0) {
                $resultasc = $this->ModeloCatalogos->getselectwheren('asignacion_ser_contrato_a',array('contratoId'=>$idcontrato));
                $asignacionId=0;
                $dataeqarrayinsert=array();
                foreach ($resultasc->result() as $item) {
                    $resultasc_info = $this->ModeloCatalogos->getselectwheren('asignacion_ser_contrato_a_e',array('idequipo'=>$equipo));
                    if($resultasc_info->num_rows()==0){
                        $asignacionId=$item->asignacionId;
                        //log_message('error', 'log_3 asignacionId: '.$asignacionId);
                        $resultasignacion=$this->ModeloCatalogos->obtenerseviciosdisponibles($this->fechahoy,$asignacionId);
                        foreach ($resultasignacion->result() as $item) {
                            //log_message('error', 'log_4 asignacionId: '.$asignacionId);
                            $arrayinsert = array(
                                                'asignacionId'=>$item->asignacionId,
                                                'idequipo'=>$equipo,
                                                'serieId'=>$serie,
                                                'tecnico'=>0,
                                                'fecha'=>$item->fecha,
                                                'hora'=>$item->hora,
                                                'horafin'=>$item->horafin,
                                                );
                            $dataeqarrayinsert[]=$arrayinsert;
                            //$this->ModeloCatalogos->db4_Insert('asignacion_ser_contrato_a_e',$arrayinsert);
                        }
                    }

                }
                if(count($dataeqarrayinsert)>0){
                    $this->ModeloCatalogos->insert_batch('asignacion_ser_contrato_a_e',$dataeqarrayinsert);
                }
            }
        }
    }
    //====================================================================
    public function getData_asignacion_e_vd() {
        $params = $this->input->post();
        $serie = $this->ModeloAsignacion->Getventas_equipos_vd($params);
        $totalRecords=$this->ModeloAsignacion->Getventas_equipos_vd_total(); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totalRecords),  
            "recordsFiltered" => intval($totalRecords),
            "data"            => $serie->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    public function getData_asignacion_a_vd() {
        $params = $this->input->post();
        $serie = $this->ModeloAsignacion->Getventas_accesorios_vd($params);
        $totalRecords=$this->ModeloAsignacion->Getventas_accesorios_vd_total(); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totalRecords),  
            "recordsFiltered" => intval($totalRecords),
            "data"            => $serie->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    public function getData_asignacion_r_vd() {
        $params = $this->input->post();
        $serie = $this->ModeloAsignacion->Getventas_refacciones_vd($params);
        $totalRecords=$this->ModeloAsignacion->Totalventas_refacciones_vd(); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totalRecords),  
            "recordsFiltered" => intval($totalRecords),
            "data"            => $serie->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    function bodegasview(){
        $resul = $this->ModeloCatalogos->getselectwheren('bodegas',array('activo'=>1));
        echo json_encode($resul->result());
    }
    function asignarrmultiple(){
        $params=$this->input->post();
        $refacciones = $params['refacciones'];
        $DATAf = json_decode($refacciones);

        //=========================================
            $strq_bod="SELECT * from bodegas where activo=1";
            $bod_resul=$this->db->query($strq_bod);
        //=========================================
        $html='<table class="table bordered striped" id="t_selected_ref">
                <thead><tr><th>Refaccion</th><th>Cantidad</th><th>Bodega Solicitada</th><th></th></tr></thead><tbody>';
        $o_st_bod='';
        for ($i=0;$i<count($DATAf);$i++) {
            $idrow = $DATAf[$i]->idrow;
            $can = $DATAf[$i]->can;
            $idref = $DATAf[$i]->idref;
            $refname = $DATAf[$i]->refname;
            $bod = $DATAf[$i]->bod;
            $bodname = $DATAf[$i]->bodname;
            $emp = $DATAf[$i]->emp;

            $html.='<tr>';
                $html.='<td class="info_pro_ref" data-row="'.$idrow.'" data-idref="'.$idref.'" data-bodo="'.$bod.'" data-cansol="'.$can.'">'.$refname.'</td>';
                $html.='<td>'.$can.'</td>';
                $html.='<td>
                            <select class="browser-default  form-control-bmz selectedbod selectedbod_'.$idrow.'" onchange="m_selec_bod('.$idrow.')">';
                            foreach ($bod_resul->result() as $itemb) {
                                if($emp==$itemb->tipov){
                                    if($bod==$itemb->bodegaId){
                                        $selected=' selected ';
                                    }else{
                                        $selected='';
                                    }
                                    $html.='<option value="'.$itemb->bodegaId.'" '.$selected.'>'.$itemb->bodega.'</option>';
                                }
                                
                            }

                    $html.='</select></td>';
                $html.='<td class="se_stock_bode_'.$idrow.'"></td>';
            $html.='</tr>';

            $strq_stock="SELECT serref.*,bod.bodega 
                        FROM series_refacciones as serref
                        INNER JOIN bodegas as bod ON bod.bodegaId=serref.bodegaId
                        WHERE serref.activo=1 AND serref.con_serie=0 AND serref.cantidad>0 AND bod.tipov='$emp' AND serref.refaccionid='$idref' ";
            $bod_resul_stock=$this->db->query($strq_stock);
            foreach ($bod_resul_stock->result() as $itemsb) {
                $cant_ref=$itemsb->cantidad-$itemsb->resguardar_cant;
                $o_st_bod.='
                <div class="col s6 m4 checkproductossrow_'.$idrow.'_'.$itemsb->bodegaId.'">';
                    $o_st_bod.='<div class="card">';
                        $o_st_bod.='<div class="card-content card-padding" style="padding-left: 5px;padding-right: 5px;padding-top: 13px;padding-bottom: 13px;">';
                            $o_st_bod.='<div class="row" style="margin-bottom: 0px;">';
                                $o_st_bod.='<div class="col s8">Sin Serie ('.$cant_ref.')</div>';
                                $o_st_bod.='<div class="col s4">';
                                    $o_st_bod.='<input type="number" id="serieId_mul" class="form-control-bmz ser_mul_g serieId_mul_'.$idrow.'" min="1" max="'.$cant_ref.'" data-cantmax="'.$cant_ref.'" data-serieId="'.$itemsb->serieId.'" style="margin: 0;px;height: 24px;">';
                                $o_st_bod.='</div>';
                            $o_st_bod.='</div> ';
                        $o_st_bod.='</div>';
                    $o_st_bod.='</div>';
                $o_st_bod.='</div>';
            }
        }
        $html.='</tbody></table>';
        $html.='<div style="display:none">'.$o_st_bod.'</div>';
        echo $html;
    }

    function addaseriesr_multiple(){
        $params=$this->input->post();
        $refacciones = $params['refacciones'];
        $DATAf = json_decode($refacciones);
        for ($i=0;$i<count($DATAf);$i++) {
            $idrow=$DATAf[$i]->idrow;
            $idref=$DATAf[$i]->idref;
            $bodo=$DATAf[$i]->bodo;
            $bodd=$DATAf[$i]->bodd;
            $serieid=$DATAf[$i]->serieid;
            $cant=$DATAf[$i]->cant;
            //===================================
                $resultvr=$this->ModeloCatalogos->getselectwheren('ventas_has_detallesRefacciones',array('id'=>$idrow));
                $idVentas=0;
                $idRefaccion=0;
                $modelo='';
                $bodega_o='';
                $clienteId=0;
                foreach ($resultvr->result() as $itemr) {
                    $idVentas=$itemr->idVentas;
                    $idRefaccion=$itemr->idRefaccion;
                    $modelo=$itemr->modelo;
                    $bodega_o=$itemr->serie_bodega;
                }
                $resultven=$this->ModeloCatalogos->getselectwheren('ventas',array('id'=>$idVentas));
                foreach ($resultven->result() as $itemven) {
                    $clienteId=$itemven->idCliente;
                }
            //===================================
                $detallesequiposss = array(
                    'id_refaccion'=>$idrow,
                    'serieId'=>$serieid,
                    'cantidad'=>$cant
                );
                $this->ModeloCatalogos->Insert('asignacion_series_refacciones',$detallesequiposss);
                $this->ModeloCatalogos->updateCatalogo('series_refacciones',array('status'=>0),array('serieId'=>$serieid));
                $this->ModeloCatalogos->updatestock('series_refacciones','cantidad','-',$cant,'serieId',$serieid);
                $this->Configuraciones_model->registrarmovimientosseries($serieid,3);
                $this->ModeloCatalogos->Insert('bitacora_movimientos',array('entrada_salida'=>0,'descripcion_evento'=>'Salida para venta: '.$idVentas,'personal'=>$this->idpersonal,'tipo'=>3,'id_item'=>$idrow,'modeloId'=>$idRefaccion,'modelo'=>$modelo,'serieId'=>$serieid,'bodega_o'=>$bodd,'clienteId'=>$clienteId,'cantidad'=>$cant));
            //===================================
                $dataarray=array('serie_estatus'=>1);
                //if ($params['newbodega']!=0) {
                   $dataarray['serie_bodega']= $bodd;
                //}
                
                $this->ModeloCatalogos->updateCatalogo('ventas_has_detallesRefacciones',$dataarray,array('id'=>$idrow));
            //===================================
        }
    }
}

?>