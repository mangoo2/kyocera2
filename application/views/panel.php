<style type="text/css">
.noti{width: 10px;height: 10px;display: block;border-radius: 3px;float: left;}
#date-servicios td,#date-servicios th,#tabla_ventas_incompletas td,#tabla_ventas_incompletas th,#table_todo_pni th,#table_todo_pni td{font-size: 11px;}
/*.delete_pni{display: none;}*/
#tabla_ventas_incompletas{width: 100% !important;}
.addiframecomep iframe{width: 100%;border: 0px;min-height: 315px;}
.c_b_control_t{
   padding: 0px;
   border: 0px;
}
.c_b_control_t iframe,.c_b_acuinci_t iframe{
   width: 100%;
   height: 940px;
   border: 0px;
}
</style>
<input type="hidden" id="ejecutivoselect" value="<?php echo $this->session->userdata('idpersonal');?>">
<input type="hidden" id="fechainicial_reg" value="<?php echo $fechainicial_reg;?>">
<input  type="date" id="fechahoy" value="<?php echo date('Y-m-d')?>" style="display: none;">

<?php
   $idpersonal=$this->session->userdata('idpersonal');
   $fecha_actual = date("Y-m-d");
   $fecha_ini = date("Y-m-d",strtotime($fecha_actual."- 10 days"));
   $fecha_fin = date("Y-m-d",strtotime($fecha_actual."+ 20 days"));
   $sol_ser=$this->Configuraciones_model->serviciosdeventas_info($fecha_ini,$fecha_fin,$idpersonal);
   $result = $this->ModeloAsignacion->getlistadocalentario($fecha_ini,$fecha_fin,0,0,0,0,0,0,0,$idpersonal);
?>

<!-- START CONTENT -->
<section id="content" width="100%">
   <!--breadcrumbs start-->
   <div id="breadcrumbs-wrapper" width="100%">
</section>
   <!-- START CONTENT -->
   <section id="content" width="100%">
      <div class="row">
         <div class="col s12">
            <h4 class="card-title mb-0"><?php echo $bienvenida; ?></h4>
         </div>
      </div>
      <div class="row mt-4">
         <div class="col s12">
            <ul class="collapsible popout collapsible-accordion" data-collapsible="accordion">
               <?php if($view_paneles==1){ ?>
                  <li class="">
                     <div class="collapsible-header">
                     <i class="material-icons">subtitles</i>Servicios</div>
                     <div class="collapsible-body" style="display: none;">
                        <!------------------------------------------------------------->
                           <table class="table display no-footer dataTable dtr-inline" id="date-servicios">
                              <thead><tr><th></th><th></th><th>Clientes</th><th>Fecha de asignacion</th><th>Hora</th><th>Estatus</th><th>Tecnico</th></tr></thead>
                              <tbody>
                                 <?php foreach ($sol_ser->result() as $itemvs) {
                                 $idventas=$itemvs->idventa;
                                 if($itemvs->view==1){
                                 $idventas=$itemvs->idventac;
                                 }
                                 if ($itemvs->prasignaciontipo==4) {
                                 $titleservicio='Venta';
                                 }
                                 if ($itemvs->prasignaciontipo==2) {
                                 $titleservicio='Poliza';
                                 }
                                 if ($itemvs->view==1) {
                                 $titleservicio='Venta Combinada';
                                 }
                                 echo '<tr><td>'.$idventas.'</td><td>'.$titleservicio.'</td><td>'.$itemvs->empresa.'</td><td>Solicitud: '.$itemvs->fechaentrega.'</td><td></td><td>Pendiente por asignar</td><td></td></tr>';
                                 } ?>
                                 <?php foreach ($result->result() as $item) {
                                 //var_dump($item);
                                 $status='<span class="noti red"></span> Sin atender';
                                 if($item->confirmado==1){
                                 //$status='Confirmado';
                                 }
                                 if($item->status==1){
                                 $status='<span class="noti oraje"></span> En proceso';
                                 }
                                 if($item->status==2){
                                 $status='<span class="noti green"></span> Finalizado';
                                 }
                                 if($item->nr==1){
                                 $status='<span class="noti red"></span> Rechazado, pendiente por reagendar';
                                 }
                                 ?><tr><td></td><td></td><td><?php echo $item->empresa;?></td><td><?php echo $item->fecha;?></td><td><?php echo $item->hora.' a '.$item->horafin;?></td><td><?php echo $status;?></td><td><?php echo $item->nombre.' '.$item->apellido_paterno.' '.$item->apellido_materno;?><?php if($item->tecnico2!=''){echo '<br>'.$item->tecnico2;}?><?php if($item->tecnico3!=''){echo '<br>'.$item->tecnico3;}?><?php if($item->tecnico4!=''){echo '<br>'.$item->tecnico4;}?></td></tr><?php } ?></tbody></table>
                        <!------------------------------------------------------------->
                     </div>
                  </li>
                  <li class="">
                     <div class="collapsible-header">
                     <i class="material-icons">games</i>Últimos pagos</div>
                     <div class="collapsible-body" style="display: none;">
                        <!------------------------------------------------------------->
                           <table class="table display no-footer dataTable dtr-inline" id="tabla_ventas_incompletas"><thead><tr><th>Pre factura</th><th>Factura</th><th>Tipo</th><th>Monto</th><th>Estatus</th><th>Cliente</th><th>Fecha</th></tr></thead><tbody></tbody></table>
                        <!------------------------------------------------------------->
                     </div>
                  </li>
                  <li>
                     <div class="collapsible-header">
                     <i class="material-icons">library_books</i>Pagos no identificados</div>
                     <div class="collapsible-body">
                        <!------------------------------------------------------------->
                           <div class="row">
                              <div class="col s3">
                                 <label>Ejecutivo asignado</label>
                                  <select id="pn_personal" class="browser-default form-control-bmz" onchange="filtro_pn()">
                                    <option value="0">Todos</option><?php foreach ($personalrows->result() as $item) { 
                                      echo '<option value="'.$item->personalId.'" >'.$item->nombre.'</option>';
                                     } ?>
                                  </select>
                              </div>
                           </div>
                           <table class="table display no-footer dataTable dtr-inline" id="table_todo_pni">
                              <thead><tr><th>Fecha</th><th>Cliente</th><th>Método</th><th>Monto</th><th>Personal<br>asignado</th><th>Comentario</th><th>Comentario<br>ejecutivo</th><th></th></tr></thead>
                           <tbody class="table_pni"></tbody>
                        </table>
                        <!------------------------------------------------------------->
                     </div>
                  </li>
                  <?php  if($idpersonal==0 || $idpersonal==1 || $idpersonal==29){ 
                     $row_get_agenda=$this->ModeloCatalogos->fecha_prox_ser($fecha_pi,$fecha_pf);
                  ?>
                     <li>
                        <div class="collapsible-header">
                        <i class="fas fa-calendar"></i>Agenda de servicios</div>
                        <div class="collapsible-body">
                           <!------------------------------------------------------------->
                              
                              <table class="table display no-footer dataTable dtr-inline" >
                                 <thead>
                                    <tr><th>Poliza</th><th>Cliente</th><th>Fecha</th><th></th></tr>
                                 </thead>
                              <tbody>
                                 <?php foreach ($row_get_agenda->result() as $item) { ?>
                                    <tr><td><?php echo $item->idpoliza;?></td><td><?php echo $item->empresa;?></td><td><?php echo $item->fecha;?></td><td><a type="button" href="<?php echo base_url();?>PolizasCreadas/servicios/<?php echo $item->idpoliza;?>" class="b-btn b-btn-primary tooltipped" data-position="top" data-delay="50" data-tooltip="Ir"><i class="fas fa-external-link-alt"></i></td></tr>
                                 <?php }  ?>
                              </tbody>
                           </table>
                           <!------------------------------------------------------------->
                        </div>
                     </li>
                  <?php } ?>
               <?php } ?>
               <?php if($view_paneles==1 || $perfilid==8 || $perfilid==5 || $perfilid==12){ ?>
                  <li>
                     <div class="collapsible-header c_b_control_t_header" onclick="carga_ct()">
                     <i class="material-icons">library_books</i>Control de tareas</div>
                     <div class="collapsible-body c_b_control_t">
                        <!------------------------------------------------------------->
                           
                        <!------------------------------------------------------------->
                     </div>
                  </li>
               <?php } ?>
               <?php if($view_paneles==1 || $perfilid==8 || $perfilid==5 || $perfilid==12){ ?>
                  <li>
                     <div class="collapsible-header c_b_acuinci_t_header" onclick="carga_a_i()">
                     <i class="material-icons">library_books</i>Acuerdos / incidencias</div>
                     <div class="collapsible-body c_b_acuinci_t">
                        <!------------------------------------------------------------->
                           
                        <!------------------------------------------------------------->
                     </div>
                  </li>
               <?php } ?>
               <?php if($view_paneles==1){ ?>
                  <li>
                     <div class="collapsible-header">
                     <i class="material-icons">library_books</i>Prefacturas pendientes de fecha de entrega <?php if($list_pre_fe->num_rows()>0){ echo '<small class="notification-badge red" style="min-width: 24px;text-align: center;">'.$list_pre_fe->num_rows().'</small>';}?></div>
                     <div class="collapsible-body">
                        <!------------------------------------------------------------->
                           <table class="table table-striped" id="table_pe_fe">
                              <thead>
                                 <tr>
                                    <th>#</th>
                                    <th>Cliente</th>
                                    <th>Pre factura</th>
                                    <th>Fecha de creación</th>
                                    <th>Ejecutivo</th>
                                    <th></th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <?php 
                                    $html='';
                                    foreach ($list_pre_fe->result() as $item_fe) {
                                       if($item_fe->tipoventa==2){
                                          $idpre =$item_fe->equipo;
                                       }else{
                                          $idpre =$item_fe->idpre;
                                       }
                                       $btn_pre='';
                                       if($item_fe->tipoventa==1){
                                          $btn_pre='<a class="btn-floating green tooltipped" data-position="top" data-delay="50" data-tooltip="Prefactura" onclick="detallev('.$item_fe->idpre.',0)" data-tooltip-id="aba04afd-b79a-0cc8-8a4c-70a3ec5eadef"><i class="material-icons">assignment</i></a>';
                                          $tipoventafil=1;
                                       }
                                       if($item_fe->tipoventa==2){
                                          $btn_pre='<a class="btn-floating green tooltipped" data-position="top" data-delay="50" data-tooltip="Prefactura" onclick="detallev('.$item_fe->idpre.',1)" data-tooltip-id="aba04afd-b79a-0cc8-8a4c-70a3ec5eadef"><i class="material-icons">assignment</i></a>';
                                          $tipoventafil=2;
                                       }
                                       if($item_fe->tipoventa==4){
                                          $btn_pre='<a class="btn-floating green tooltipped" data-position="top" data-delay="50" data-tooltip="Prefactura" onclick="detallep('.$item_fe->idpre.')"><i class="material-icons">assignment</i></a>';
                                          $tipoventafil=4;
                                       }


                                       $vinc='<a class="b-btn b-btn-primary" target="_blank" href="'.base_url().'index.php/Ventas?tipoventafil='.$tipoventafil.'&idcli='.$item_fe->idCliente.'&emp='.$item_fe->empresa.'&fechaview='.$item_fe->reg.'&eje=0&viewidpre='.$idpre.'"><i class="fas fa-random"></i></a>';

                                       $html.='<tr>';
                                          $html.='<td>'.$idpre.'</td>';
                                          $html.='<td>'.$item_fe->empresa.'</td>';
                                          $html.='<td>'.$btn_pre.'</td>';
                                          $html.='<td>'.$item_fe->reg.'</td>';
                                          $html.='<td>'.$item_fe->nombre.' '.$item_fe->apellido_paterno.'</td>';
                                          $html.='<td>'.$vinc.'</td>';
                                       $html.='</tr>';
                                    }
                                    echo $html;
                                 ?>
                              </tbody>
                           </table>
                        <!------------------------------------------------------------->
                     </div>
                  </li>
                  <?php  if($idpersonal==1 || $idpersonal==17 || $idpersonal==18){ 
                     $res_p_efe = $this->ModeloGeneral->pagosefectivo(date('Y-m-d'));
                  ?>
                     <li>
                        <div class="collapsible-header carga_pagos_efe" onclick="carga_pa_ef()">
                        <i class="fas fa-dollar-sign"></i>Pagos en efectivo <?php if($res_p_efe->num_rows()>0){ echo '<small class="notification-badge red" style="min-width: 24px;text-align: center;">'.$res_p_efe->num_rows().'</small>';}?></div>
                        <div class="collapsible-body carga_pa_ef">
                           <!------------------------------------------------------------->
                              
                              
                           <!------------------------------------------------------------->
                        </div>
                     </li>
                  <?php } ?>
               <?php } ?>
            </ul>
         </div>

</div>
<div class="row ">
   <div class="col s12 l12 animate fadeRight">
      
   </div>
</div>
