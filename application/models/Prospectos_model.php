<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class Prospectos_model extends CI_Model 
{
    public function __construct() 
    {
        parent::__construct();
        $this->DB5 = $this->load->database('other5_db', TRUE);
    }
    
    public function insertar_prospecto($dato){
    $this->db->insert('clientes',$dato);   
    return $this->db->insert_id();
    } 

    public function update_prospecto($data, $id) 
    {
        $this->db->set($data);
        $this->db->where('id', $id);
        return $this->db->update('clientes');
    }
    /*
    public function getListadoProspectos()
    {
        $sql = "SELECT
                c.id,
                c.empresa,
                c.puesto_contacto,
                c.email,
                c.estado,
                c.municipio,
                c.giro,
                c.direccion,
                c.observaciones,
                p.nombre as 'vendedor'
                FROM clientes as c
                INNER JOIN usuarios AS u ON c.idUsuario = u.UsuarioID
                INNER JOIN personal AS p ON u.personalId = p.personalId
                WHERE c.tipo=2 AND c.estatus=1";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getListadoProspectosPorUsuario($idUsuario)
    {
        $sql = "SELECT
                c.id,
                c.empresa,
                c.puesto_contacto,
                c.email,
                c.estado,
                c.municipio,
                c.giro,
                c.direccion,
                c.observaciones,
                p.nombre as 'vendedor'
                FROM
                clientes AS c
                INNER JOIN usuarios AS u ON c.idUsuario = u.UsuarioID
                INNER JOIN personal AS p ON u.personalId = p.personalId
                WHERE
                c.tipo = 2 AND
                c.estatus = 1 AND
                c.idUsuario = ".$idUsuario;
        $query = $this->db->query($sql);
        return $query->result();
    }
    */
    //==============================================
        function getListadoProspectosPorUsuario_asic($params){
            //$idUsuario=$params['idUsuario'];
            $idUsuario=0;
            $columns = array( 
                    0=>'c.id',
                    1=>'c.empresa',
                    2=>'c.puesto_contacto',
                    3=>'c.email',
                    4=>'c.estado',
                    5=>'c.municipio',
                    6=>'c.giro',
                    7=>'c.direccion',
                    8=>'c.observaciones',
                    9=>'p.nombre'
            );
            $select="";
            foreach ($columns as $c) {
                $select.="$c, ";
            }
            $this->DB5->select($select);
            $this->DB5->from('clientes AS c');
            $this->DB5->join('usuarios u', 'c.idUsuario = u.UsuarioID');
            $this->DB5->join('personal p', 'u.personalId = p.personalId');
                
                $this->DB5->where('c.tipo',2);
                $this->DB5->where('c.estatus',1);
                if ($idUsuario>1) {
                    $this->DB5->where('c.idUsuario',$idUsuario);
                }
                
            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->DB5->group_start();
                foreach($columns as $c){
                    $this->DB5->or_like($c,$search);
                }
                $this->DB5->group_end();  
            }            
$this->DB5->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->DB5->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->DB5->get();
        // print_r($query); die;
        return $query;
        }
        function getListadoProspectosPorUsuario_asict($params){
            //$idUsuario=$params['idUsuario'];
            $idUsuario=0;
            $columns = array( 
                    0=>'c.id',
                    1=>'c.empresa',
                    2=>'c.puesto_contacto',
                    3=>'c.email',
                    4=>'c.estado',
                    5=>'c.municipio',
                    6=>'c.giro',
                    7=>'c.direccion',
                    8=>'c.observaciones',
                    9=>'p.nombre'
            );
            $this->DB5->select('COUNT(*) as total');
            $this->DB5->from('clientes AS c');
            $this->DB5->join('usuarios u', 'c.idUsuario = u.UsuarioID');
            $this->DB5->join('personal p', 'u.personalId = p.personalId');
                
                $this->DB5->where('c.tipo',2);
                $this->DB5->where('c.estatus',1);
                if ($idUsuario>1) {
                    $this->DB5->where('c.idUsuario',$idUsuario);
                }
            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->DB5->group_start();
                foreach($columns as $c){
                    $this->DB5->or_like($c,$search);
                }
                $this->DB5->group_end();  
            }            
            //$this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
            //$this->db->limit($params['length'],$params['start']);
            //echo $this->db->get_compiled_select();
            $query=$this->DB5->get();
            // print_r($query); die;
            return $query->row()->total;
        }
    //==============================================
    public function getdataProspectoPorId($idProspecto){
        $sql = "SELECT * FROM clientes
                WHERE tipo=2 AND id=".$idProspecto;
        $query = $this->db->query($sql);
        return $query->row();
    }
    public function getProspectoPorId($idProspecto){
        $sql = "SELECT * FROM clientes
                WHERE id=".$idProspecto;
        $query = $this->db->query($sql);
        return $query->row();
    }
    public function insertarToCatalogo($data, $catalogo) {
        $this->db->insert($catalogo,$data);   
        return $this->db->insert_id();
    } 

    public function getListadoTel_local($idCliente)    {
        $sql = "SELECT * FROM cliente_has_telefono WHERE idCliente=".$idCliente;
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function getListadoTel_celular($idCliente){
        $sql = "SELECT * FROM cliente_has_celular WHERE idCliente=".$idCliente;
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function getListadoPersona_contacto($idCliente){
        $sql = "SELECT * FROM cliente_has_persona_contacto WHERE idCliente=".$idCliente;
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function eliminar_tel_local($id) {
        $this->db->where('id', $id);
        return $this->db->delete('cliente_has_telefono');
    }
    public function eliminar_tel_celular($id) {
        $this->db->where('id', $id);
        return $this->db->delete('cliente_has_celular');
    }
    public function eliminar_persona_contacto($id) {
        $this->db->where('id', $id);
        return $this->db->delete('cliente_has_persona_contacto');
    }
    public function getListadoLlamadasPorProspecto($idProspecto)    {
        $sql = "SELECT * FROM llamadas WHERE idProspecto=".$idProspecto;
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function getListadoCorreosPorProspecto($idProspecto){
        $sql = "SELECT * FROM correos WHERE idProspecto=".$idProspecto;
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function getListadoVisitasPorProspecto($idProspecto)    {
        $sql = "SELECT * FROM visitas WHERE idProspecto=".$idProspecto;
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function insertar_llamada($dato)    {
        $this->db->insert('llamadas',$dato);   
        return $this->db->insert_id();
    } 
    public function insertar_correo($dato)    {
        $this->db->insert('correos',$dato);   
        return $this->db->insert_id();
    } 
    public function insertar_visita($dato)    {
        $this->db->insert('visitas',$dato);   
        return $this->db->insert_id();
    }
    public function getDataFamilia(){
        $sql = "SELECT * FROM familia WHERE status=1";
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function getDataEquipo($id){
        $sql = "SELECT * FROM equipos WHERE idfamilia=$id";
        $query = $this->db->query($sql);
        return $query->result();
    } 
    public function getDataEquipo2(){
        $sql = "SELECT * FROM equipos WHERE estatus=1";
        $query = $this->db->query($sql);
        return $query->result();
    } 
}

?>