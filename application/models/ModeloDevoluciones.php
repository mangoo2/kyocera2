<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloDevoluciones extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    function getdevoluciones($params){
        $tipov=$params['tipov'];
        $cliente=$params['cliente'];
        $columns = array( 
            0=>'vd.idVentas',
            1=>'vd.idVentas',
            2=>'vd.tipo',
            3=>'vd.piezas',
            4=>'vd.modelo',
            5=>'vd.parte',
            6=>'vd.nombre',
            7=>'vd.motivo_dev',
            8=>'vd.reg_dev',
            9=>'vd.serie',
            10=>'vd.apellido_paterno',
            11=>'vd.apellido_materno',
            12=>'vd.vr',
            13=>'vd.row'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('vista_devoluciones vd');
        if($tipov>0){
            $this->db->where('vd.tipo',$tipov);
        }
        if($cliente>0){
            $this->db->where('vd.idCliente',$cliente);
        }

        //$this->db->where(array('facturaabierta'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    public function getdevolucionest($params){
        $tipov=$params['tipov'];
        $cliente=$params['cliente'];
        $columns = array( 
            0=>'vd.idVentas',
            1=>'vd.piezas',
            2=>'vd.modelo',
            3=>'vd.parte',
            4=>'vd.serie',
            5=>'vd.nombre',
            6=>'vd.apellido_paterno',
            7=>'vd.apellido_materno',
            8=>'vd.motivo_dev',
            9=>'vd.reg_dev',
            10=>'vd.tipo'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(*) as total');
        $this->db->from('vista_devoluciones vd');
        if($tipov>0){
            $this->db->where('vd.tipo',$tipov);
        }
        if($cliente>0){
            $this->db->where('vd.idCliente',$cliente);
        }
        //$where = ;
        //$this->db->where(array('facturaabierta'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        //$this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        //$this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query->row()->total;
    }





}