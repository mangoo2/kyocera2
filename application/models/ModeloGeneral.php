<?php

class ModeloGeneral extends CI_Model 
{
    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
        $this->DB2 = $this->load->database('other_db', TRUE);
    }

    /**************** INSERCIONES ****************/
    /*********************************************/
    public function getinsert($dato,$table){
	    $this->db->insert($table,$dato);   
	    return $this->db->insert_id();
    } 
    function getselectwheren($table,$where){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($where);
        $query=$this->db->get(); 
        return $query;
    }
    function updateCatalogo($Tabla,$data,$where){
        $this->db->set($data);
        $this->db->where($where);
        $this->db->update($Tabla);
        return $this->db->affected_rows();
    }
    public function genSelect($tabla){
        $query = $this->DB2->get($tabla);
        return $query->result();
    }
    function stockequipo($id){
        $strq = "SELECT count(*) as total
                FROM series_productos WHERE productoid=$id and status<=1 and activo=1 and resguardar_cant=0"; 
        $query = $this->DB2->query($strq);
        $total=0;
        foreach ($query->result() as $item) {
            $total=$item->total;
        }
        if ($total>0) {
            $rtotal=$total;
        }else{
            $rtotal=0;
        }
        return $rtotal;
    }
    function stockequipobodega($id,$bodega){
        $strq = "SELECT count(*) as total
                FROM series_productos WHERE productoid=$id and status<=1 and activo=1 and bodegaId=$bodega and resguardar_cant=0"; 
        $query = $this->DB2->query($strq);
        $total=0;
        foreach ($query->result() as $item) {
            $total=$item->total;
        }
        if ($total>0) {
            $rtotal=$total;
        }else{
            $rtotal=0;
        }
        return $rtotal;
    }
    function verificarpagosventas(){
        $strq="SELECT v.id,v.statuspago,pv.pago,fv.facturaId,fcd.complementoId 
                FROM ventas as v 
                LEFT JOIN pagos_ventas as pv on pv.idventa=v.id 
                LEFT JOIN factura_venta as fv on fv.ventaId=v.id and fv.combinada=0 
                LEFT JOIN f_facturas as ff on ff.FacturasId=fv.facturaId 
                LEFT JOIN f_complementopago_documento as fcd on fcd.facturasId=ff.FacturasId 
                WHERE v.reg>DATE_SUB(NOW(),INTERVAL '3' MONTH) and v.combinada=0 and v.activo=1 AND v.estatus!=3 AND (pv.pago>0 or fcd.complementoId>0) ORDER BY RAND() LIMIT 1";
        $strq="SELECT v.id,v.tipov, v.statuspago,pv.pago,0 as facturaId,0 as complementoId,v.reg,DATE_SUB(NOW(),INTERVAL '3' MONTH)
                FROM ventas as v 
                LEFT JOIN pagos_ventas as pv on pv.idventa=v.id 
                WHERE v.reg>DATE_SUB(NOW(),INTERVAL '10' MONTH) and v.combinada=0 and v.activo=1 AND v.estatus!=3 AND pv.pago>0
                UNION
                SELECT v.id,v.tipov, v.statuspago,0 as pago,fv.facturaId,fcd.complementoId,v.reg,DATE_SUB(NOW(),INTERVAL '5' MONTH)
                                FROM ventas as v 
                                LEFT JOIN factura_venta as fv on fv.ventaId=v.id and fv.combinada=0 
                                LEFT JOIN f_facturas as ff on ff.FacturasId=fv.facturaId 
                                LEFT JOIN f_complementopago_documento as fcd on fcd.facturasId=ff.FacturasId 
                                WHERE v.reg>DATE_SUB(NOW(),INTERVAL '5' MONTH) and v.combinada=0 and v.activo=1 AND v.estatus!=3 AND fcd.complementoId>0
                ORDER BY RAND() LIMIT 1";
        $strq="SELECT v.id,v.tipov, v.statuspago,pv.pago,0 as facturaId,0 as complementoId,v.reg,v.total_general,'' as serie, '' as Folio,'' as ImpPagado
                FROM ventas as v 
                LEFT JOIN pagos_ventas as pv on pv.idventa=v.id 
                WHERE v.reg>DATE_SUB(NOW(),INTERVAL '10' MONTH) and v.combinada=0 and v.activo=1 AND v.estatus!=3 AND pv.pago>0
                UNION
                SELECT v.id,v.tipov,v.statuspago,docd.ImpPagado as pago,fac.FacturasId as facturaId,docd.complementoId,v.reg,v.total_general,fac.serie,fac.Folio,docd.ImpPagado
                FROM ventas as v
                INNER JOIN factura_venta as facv on facv.ventaId=v.id AND facv.combinada=0
                INNER JOIN f_facturas as fac on fac.FacturasId=facv.facturaId
                INNER JOIN f_complementopago_documento as docd on docd.facturasId=fac.FacturasId
                WHERE v.reg>DATE_SUB(NOW(),INTERVAL '5' MONTH) and v.activo=1 AND v.combinada=0 AND v.estatus!=3 AND docd.ImpPagado>=v.total_general";
        $query = $this->db->query($strq);
        return $query;
    }
    function verificarpagosventascombinadas(){
        $strq="SELECT cli.empresa,vc.*, vc.total_general,comp.ImpPagado 
            FROM ventacombinada as vc 
            INNER JOIN clientes as cli on cli.id=vc.idCliente
            INNER JOIN factura_venta as facv on facv.ventaId=vc.combinadaId and facv.combinada=1
            INNER JOIN f_complementopago_documento as comp on comp.facturasId=facv.facturaId
            WHERE vc.activo=1 and vc.total_general>0 AND vc.estatus<3
            GROUP BY vc.combinadaId";
        $query = $this->db->query($strq);
        return $query;
    }
    function verificarpagosventascombinadascomp(){
        $strq="SELECT p.id,p.idCliente,p.combinada,p.reg,p.estatus,p.total, fac.FacturasId,fac.serie,fac.Folio,docd.ImpPagado
            FROM polizasCreadas as p
            INNER JOIN factura_poliza as facp on facp.polizaId=p.id
            INNER JOIN f_facturas as fac on fac.FacturasId=facp.facturaId
            INNER JOIN f_complementopago_documento as docd on docd.facturasId=fac.FacturasId
            WHERE p.activo=1 AND p.combinada=0 AND p.estatus!=3 AND p.total>0 AND docd.ImpPagado>=p.total
            ORDER BY RAND() LIMIT 1";
        $query = $this->db->query($strq);
        return $query;
    }
    function verificarpagospolizas(){
        $strq="SELECT fp.* FROM factura_poliza as fp
            INNER JOIN polizasCreadas as pc on pc.id=fp.polizaId
            INNER JOIN f_complementopago_documento as fcd on fcd.facturasId=fp.facturaId 
            WHERE pc.estatus!=3 ORDER BY RAND() LIMIT 1";
        $query = $this->db->query($strq);
        return $query;
    }
    function buscarseriefolio($cliente,$search){
        $strq="SELECT * FROM(
                    SELECT 
                        con.idcontrato,
                        spro.serie as Folio 
                    FROM rentas AS r 
                    INNER JOIN contrato AS con ON con.idRenta = r.id 
                    INNER join rentas_has_detallesEquipos as req on req.idRenta =con.idRenta 
                    inner join asignacion_series_r_equipos as reqs on reqs.id_equipo=req.id 
                    inner join series_productos as spro on spro.serieId=reqs.serieId 
                    WHERE r.estatus!=0 and r.idCliente =$cliente GROUP by spro.serie

                    UNION

                    SELECT 
                        con.idcontrato,
                        fac.Folio
                    FROM factura_prefactura as facpre
                    inner JOIN f_facturas as fac on fac.FacturasId=facpre.facturaId
                    INNER JOIN contrato AS con ON con.idcontrato = facpre.contratoId 
                    inner join rentas as r on r.id=con.idRenta
                    WHERE r.idCliente =$cliente

                    UNION

                    SELECT 
                        con.idcontrato,
                        face.Folio 
                    FROM factura_prefactura as facpre
                    inner JOIN f_facturas as fac on fac.FacturasId=facpre.facturaId
                    INNER JOIN f_facturas as face on face.f_r_uuid=fac.uuid
                    INNER JOIN contrato AS con ON con.idcontrato = facpre.contratoId
                    inner join rentas as r on r.id=con.idRenta
                    WHERE r.idCliente =$cliente and face.f_relacion=1
                    
                ) as datos WHERE Folio LIKE '%$search%'";
        $query = $this->db->query($strq);
        return $query;
    }
    function infoaccesorioshistorial($idhistorial,$groupby){
        if($groupby==1){
            $groupbyw=" GROUP BY rda.rowequipo";
        }else{
            $groupbyw="";
        }
        $strq="SELECT rha.cantidad,catacc.nombre,rda.rowequipo,asre.serieId FROM rentas_historial_accesorios as rha INNER JOIN rentas_has_detallesEquipos_accesorios as rda on rda.id_accesoriod=rha.id_accesoriod inner JOIN asignacion_series_r_equipos as asre on asre.id_equipo=rda.rowequipo INNER JOIN catalogo_accesorios as catacc on catacc.id=rha.accesorio WHERE rha.historialid=$idhistorial $groupbyw";
        $query = $this->db->query($strq);
        return $query;
    }
    function vrefcoti($idcli,$idr,$fecha){
        $strq="SELECT cref.*,cot.reg 
                FROM cotizaciones_has_detallesRefacciones as cref 
                INNER JOIN cotizaciones as cot on cot.id=cref.idCotizacion 
                WHERE cot.idCliente=$idcli AND cref.idRefaccion=$idr AND cot.reg>'$fecha 00:00:00'";
        $query = $this->db->query($strq);
        return $query;
    }
    function stockconsumiblesbodega($id,$bodega){
        $strq = "SELECT sum(total-resguardar_cant) as total
                FROM consumibles_bodegas WHERE consumiblesId=$id and status=1  and bodegaId=$bodega"; 
        $query = $this->DB2->query($strq);
        $total=0;
        foreach ($query->result() as $item) {
            $total=$item->total;
        }
        if ($total>0) {
            $rtotal=$total;
        }else{
            $rtotal=0;
        }
        return $rtotal;
    }
    function stockaccesoriobodega($id,$bodega){
        $strq = "SELECT sum(cantidad-resguardar_cant) as total
                FROM series_accesorios WHERE accesoriosid=$id and activo=1  and bodegaId=$bodega"; 
        $query = $this->DB2->query($strq);
        $total=0;
        foreach ($query->result() as $item) {
            $total=$item->total;
        }
        if ($total>0) {
            $rtotal=$total;
        }else{
            $rtotal=0;
        }
        return $rtotal;
    }
    function stockrefaccionesbodega($id,$bodega){
        $strq = "SELECT sum(cantidad-resguardar_cant) as total
                FROM series_refacciones WHERE refaccionid=$id and activo=1 and status<2 and bodegaId=$bodega"; 
        $query = $this->db->query($strq);
        $total=0;
        foreach ($query->result() as $item) {
            $total=$item->total;
        }
        if ($total>0) {
            $rtotal=$total;
        }else{
            $rtotal=0;
        }
        return $rtotal;
    }
    function permisoscliente($cli,$pass){
        $verificar=0;
        //$result_vcli=$this->getselectwheren('clientes',array('id'=>$cli,'pass_estatus'=>1,'pass_vigencia >'=>$this->fechah,'pass_value'=>$pass));
        $strq = " SELECT * FROM clientes WHERE id='$cli' and pass_estatus=1 and  pass_vigencia > '$this->fechah' and pass_value='$pass' ";
        $result_vcli = $this->db->query($strq);
        if($result_vcli->num_rows()>0){
            $verificar=1;
        }
        
        return $verificar;
    }
    function fac_pre_facid($facturaId){
        $strq="SELECT facpre.*,arp.total, ROUND(arp.total*1.16,2) as totaliva
            FROM factura_prefactura as facpre 
            INNER JOIN alta_rentas_prefactura as arp on arp.prefId=facpre.prefacturaId
            WHERE facpre.facturaId='$facturaId'";
        $query = $this->db->query($strq);
        return $query;
    }
    function complemento_fac($id_comp){
        $strq="SELECT comd.*,fac.total
            FROM f_complementopago_documento as comd
            INNER JOIN f_facturas as fac on fac.FacturasId=comd.facturasId
            WHERE comd.complementoId='$id_comp'";
        $query = $this->db->query($strq);
        return $query;
    }
    function listadopersonal(){
        $strq="SELECT * FROM personal WHERE estatus=1 ORDER BY nombre ASC";
        $query = $this->db->query($strq);
        return $query;
    }
    function aleatorio_contrato(){
        $strq="SELECT * FROM `contrato` WHERE estatus=1 ORDER BY RAND() LIMIT 1";
        $query = $this->db->query($strq);
        return $query;
    }
    function obtenerpersonaldep($dep){
        if($dep>0){
            $w_dep=" and dep='$dep' ";
        }else{
            $w_dep=' and dep>0 ';
        }
        $strq="SELECT * FROM personal WHERE estatus=1 $w_dep ORDER BY RAND() LIMIT 1";
        $query = $this->db->query($strq);
        $personalId=0;
        foreach ($query->result() as $item) {
            $personalId=$item->personalId;
        }
        return $personalId;
    }
    function buscarventapoliza($search){
        $strq="SELECT 
1 as tipopre,
v.id as idventa,
v.idCliente,
cli.empresa,
v.reg,
v.total_general,
fac.serie,
fac.Folio
FROM ventas as v
INNER JOIN clientes as cli on cli.id=v.idCliente
LEFT JOIN factura_venta as facv on facv.ventaId=v.id AND facv.combinada=0
LEFT JOIN f_facturas as fac on fac.FacturasId=facv.facturaId and fac.Estado=1
WHERE v.combinada=0 and v.activo=1 AND v.reg>DATE_SUB(NOW(),INTERVAL '12' MONTH) AND (v.id='$search' OR v.total_general='$search')

union

SELECT 
1 as tipopre,
v.id as idventa,
v.idCliente,
cli.empresa,
v.reg,
v.total_general,
fac.serie,
fac.Folio
FROM ventas as v
INNER JOIN clientes as cli on cli.id=v.idCliente
INNER JOIN factura_venta as facv on facv.ventaId=v.id AND facv.combinada=0
INNER JOIN f_facturas as fac on fac.FacturasId=facv.facturaId and fac.Estado=1
WHERE v.combinada=0 and v.activo=1 AND v.reg>DATE_SUB(NOW(),INTERVAL '12' MONTH) AND fac.Folio='$search'

union 

SELECT 
2 as tipopre,
v.equipo as idventa,
v.idCliente,
cli.empresa,
v.reg,
v.total_general,
fac.serie,
fac.Folio
FROM ventacombinada as v
INNER JOIN clientes as cli on cli.id=v.idCliente
LEFT JOIN factura_venta as facv on facv.ventaId=v.combinadaId AND facv.combinada=1
LEFT JOIN f_facturas as fac on fac.FacturasId=facv.facturaId and fac.Estado=1
WHERE v.activo=1 AND v.reg>DATE_SUB(NOW(),INTERVAL '12' MONTH) AND (v.equipo='$search' OR v.poliza='$search' OR v.total_general='$search')

union

SELECT 
2 as tipopre,
v.equipo as idventa,
v.idCliente,
cli.empresa,
v.reg,
v.total_general,
fac.serie,
fac.Folio
FROM ventacombinada as v
INNER JOIN clientes as cli on cli.id=v.idCliente
INNER JOIN factura_venta as facv on facv.ventaId=v.combinadaId AND facv.combinada=1
INNER JOIN f_facturas as fac on fac.FacturasId=facv.facturaId and fac.Estado=1
WHERE v.activo=1 AND v.reg>DATE_SUB(NOW(),INTERVAL '12' MONTH) AND fac.Folio='$search'

union 

SELECT 
4 as tipopre,
v.id as idventa,
v.idCliente,
cli.empresa,
v.reg,
(SELECT IFNULL(sum(pd.cantidad*pd.precio_local),0)+ IFNULL(sum(pd.cantidad*pd.precio_semi),0)+ IFNULL(sum(pd.cantidad*pd.precio_foraneo),0)+ IFNULL(sum(pd.cantidad*pd.precio_especial),0) FROM polizasCreadas_has_detallesPoliza as pd WHERE pd.idPoliza=v.id) as total_general,
fac.serie,
fac.Folio
FROM polizasCreadas as v
INNER JOIN clientes as cli on cli.id=v.idCliente
LEFT JOIN factura_poliza as facp on facp.polizaId=v.id 
LEFT JOIN f_facturas as fac on fac.FacturasId=facp.facturaId and fac.Estado=1
WHERE v.combinada=0 AND v.activo=1 AND v.reg>DATE_SUB(NOW(),INTERVAL '12' MONTH) AND (v.id='$search' OR fac.Folio='$search')";
        $query = $this->db->query($strq);
        return $query;
    }
    function buscartimbre($search){
        $strq="SELECT fac.*,cli.empresa
        FROM f_facturas as fac 
        INNER JOIN clientes as cli on cli.id=fac.Clientes_ClientesId
        WHERE fac.activo=1 AND fac.fechatimbre>DATE_SUB(NOW(),INTERVAL '25' MONTH) and (fac.Folio='$search' or fac.total='$search')";
        $query = $this->db->query($strq);
        return $query;
    }

    function consultar_servicos_itinerario($tec,$fecha){
        $strq="SELECT 
                    GROUP_CONCAT(tiposer,'_',asignacionId SEPARATOR ',') as tsers,
                    GROUP_CONCAT(asignacionId SEPARATOR ',') as sers,
                    fecha,
                    hora_inicio,
                    GROUP_CONCAT(tipoagregado SEPARATOR ',') as tipag,
                    GROUP_CONCAT(descripcion,'_',hora_inicio SEPARATOR ',') as cliente_hora,
                    cli
                from (

                SELECT 1 as tiposer, a_s_cone.asignacionId,a_s_cone.fecha,iti.id as iditinerario,iti.hora_inicio,iti.tipoagregado,iti.descripcion,iti.cli
                FROM asignacion_ser_contrato_a_e as a_s_cone
                INNER JOIN unidades_bitacora_itinerario as iti on iti.personal='$tec' AND iti.fecha=a_s_cone.fecha AND  iti.asignacion_gen LIKE CONCAT('%1,', a_s_cone.asignacionId, '%')
                WHERE a_s_cone.nr=0 AND a_s_cone.status<2 AND a_s_cone.fecha='$fecha' AND (a_s_cone.tecnico='$tec' or a_s_cone.tecnico2='$tec' or a_s_cone.tecnico3='$tec' or a_s_cone.tecnico4='$tec')

                UNION

                SELECT 2 as tiposer, asp.asignacionId,asp.fecha,iti.id as iditinerario,iti.hora_inicio,iti.tipoagregado,iti.descripcion,iti.cli
                FROM asignacion_ser_poliza_a_e as asp
                INNER JOIN unidades_bitacora_itinerario as iti on iti.personal='$tec' AND iti.fecha=asp.fecha AND  iti.asignacion_gen LIKE CONCAT('%2,', asp.asignacionId, '%')
                WHERE asp.fecha='$fecha' AND asp.nr=0 AND asp.status<2 AND (asp.tecnico='$tec' OR asp.tecnico2='$tec' OR asp.tecnico3='$tec' OR asp.tecnico4='$tec')

                UNION


                SELECT 3 as tiposer,ascl.asignacionId,ascl.fecha,iti.id as iditinerario,iti.hora_inicio,iti.tipoagregado,iti.descripcion,iti.cli
                FROM asignacion_ser_cliente_a_d as ascle
                INNER JOIN asignacion_ser_cliente_a as ascl on ascl.asignacionId=ascle.asignacionId
                INNER JOIN unidades_bitacora_itinerario as iti on iti.personal='$tec' AND iti.fecha=ascl.fecha  AND  iti.asignacion_gen LIKE CONCAT('%3,', ascl.asignacionId, '%')
                WHERE ascl.fecha='$fecha' AND ascle.nr=0 AND ascle.status<2 AND (ascl.tecnico='$tec' OR ascl.tecnico2='$tec' OR ascl.tecnico3='$tec' OR ascl.tecnico4='$tec')

                UNION

                SELECT 4 as tiposer,asv.asignacionId,asv.fecha,iti.id as iditinerario,iti.hora_inicio,iti.tipoagregado,iti.descripcion,iti.cli
                FROM asignacion_ser_venta_a as asv
                INNER JOIN unidades_bitacora_itinerario as iti on iti.personal='$tec' AND iti.fecha=asv.fecha  AND  iti.asignacion_gen LIKE CONCAT('%4,', asv.asignacionId, '%')
                WHERE asv.fecha='$fecha' AND asv.nr=0 AND asv.status<2 AND ( asv.tecnico='$tec' or asv.tecnico2='$tec' or asv.tecnico3='$tec' or asv.tecnico4='$tec')
                ) as datos
                GROUP BY hora_inicio,tiposer  
                ORDER BY hora_inicio  ASC";
        $query = $this->db->query($strq);
        return $query;
    }
    function consultar_servicos_itinerario_2($tec,$fecha,$tipoview){
        if($tipoview==1){
            $w_status='<2';
        }else{
            $w_status='<=2';
        }
        $strq="SELECT 
                    *
                from (

                SELECT 1 as tiposer, a_s_cone.asignacionId,a_s_cone.fecha,iti.id as iditinerario,iti.hora_inicio,iti.tipoagregado
                FROM asignacion_ser_contrato_a_e as a_s_cone
                INNER JOIN unidades_bitacora_itinerario as iti on iti.personal='$tec' AND iti.fecha=a_s_cone.fecha AND  iti.asignacion_gen LIKE CONCAT('%1,', a_s_cone.asignacionId, '%')
                WHERE a_s_cone.nr=0 AND a_s_cone.status $w_status AND a_s_cone.fecha='$fecha' AND (a_s_cone.tecnico='$tec' or a_s_cone.tecnico2='$tec' or a_s_cone.tecnico3='$tec' or a_s_cone.tecnico4='$tec')

                UNION

                SELECT 2 as tiposer, asp.asignacionId,asp.fecha,iti.id as iditinerario,iti.hora_inicio,iti.tipoagregado
                FROM asignacion_ser_poliza_a_e as asp
                INNER JOIN unidades_bitacora_itinerario as iti on iti.personal='$tec' AND iti.fecha=asp.fecha AND  iti.asignacion_gen LIKE CONCAT('%2,', asp.asignacionId, '%')
                WHERE asp.fecha='$fecha' AND asp.nr=0 AND asp.status $w_status AND (asp.tecnico='$tec' OR asp.tecnico2='$tec' OR asp.tecnico3='$tec' OR asp.tecnico4='$tec')

                UNION


                SELECT 3 as tiposer,ascl.asignacionId,ascl.fecha,iti.id as iditinerario,iti.hora_inicio,iti.tipoagregado
                FROM asignacion_ser_cliente_a_d as ascle
                INNER JOIN asignacion_ser_cliente_a as ascl on ascl.asignacionId=ascle.asignacionId
                INNER JOIN unidades_bitacora_itinerario as iti on iti.personal='$tec' AND iti.fecha=ascl.fecha  AND  iti.asignacion_gen LIKE CONCAT('%3,', ascl.asignacionId, '%')
                WHERE ascl.fecha='$fecha' AND ascle.nr=0 AND ascle.status $w_status AND (ascl.tecnico='$tec' OR ascl.tecnico2='$tec' OR ascl.tecnico3='$tec' OR ascl.tecnico4='$tec')

                UNION

                SELECT 4 as tiposer,asv.asignacionId,asv.fecha,iti.id as iditinerario,iti.hora_inicio,iti.tipoagregado
                FROM asignacion_ser_venta_a as asv
                INNER JOIN unidades_bitacora_itinerario as iti on iti.personal='$tec' AND iti.fecha=asv.fecha  AND  iti.asignacion_gen LIKE CONCAT('%4,', asv.asignacionId, '%')
                WHERE asv.fecha='$fecha' AND asv.nr=0 AND asv.status $w_status AND ( asv.tecnico='$tec' or asv.tecnico2='$tec' or asv.tecnico3='$tec' or asv.tecnico4='$tec')
                ) as datos
                GROUP BY tiposer,asignacionId
                ORDER BY hora_inicio  ASC";
        $query = $this->db->query($strq);
        return $query;
    }
    function obtencion_dias_de_rango($fechaini,$fechafin){
        // Establece las fechas de inicio y fin
        $fechaInicio = new DateTime($fechaini);
        $fechaFin = new DateTime($fechafin);

        // Añadir 1 día al final para incluir el último día
        $fechaFin->modify('+1 day');

        // Crear un intervalo de 1 día
        $intervalo = new DateInterval('P1D');

        // Crear el periodo entre las dos fechas
        $periodo = new DatePeriod($fechaInicio, $intervalo, $fechaFin);
        return $periodo;
    }
    function info_ser_poliza($poliza){
        $strq="SELECT asig.asignacionId,asig.polizaId,asig.reg,pol.combinada,asigd.nr,asigd.status
            FROM asignacion_ser_poliza_a as asig
            INNER JOIN polizasCreadas as pol on pol.id=asig.polizaId
            INNER JOIN asignacion_ser_poliza_a_e as asigd on asigd.asignacionId=asig.asignacionId
            WHERE asig.polizaId='$poliza' AND asigd.nr=0 AND asigd.status=0 AND asig.activo=1 AND asigd.activo=1";
        $query = $this->db->query($strq);
        return $query;
    }
    function obtener_pres_pendientes_fe($personal){
        if($personal==1||$personal==18){
        //if($personal==1){
            $w_p='';
        }else{
            $w_p=" AND v.id_personal='$personal' ";
        }
        $strq="SELECT v.id as idpre, 1 as tipoventa,v.idCliente,cli.empresa,v.reg,v.prefactura,v.id_personal,v.tipov,v.fechaentregah,'' as equipo,per.nombre, per.apellido_paterno
            FROM ventas as v
            INNER JOIN clientes as cli on cli.id=v.idCliente
            INNER join personal as per on per.personalId=v.id_personal
            WHERE v.activo=1 AND v.combinada=0 and v.prefactura=1 $w_p AND  v.reg>DATE_SUB(NOW(),INTERVAL '3' MONTH) AND ( v.fechaentrega IS null OR v.fechaentrega='0000-00-00')  
            UNION
            SELECT v.combinadaId as idpre, 2 as tipoventa,v.idCliente,cli.empresa,v.reg,v.prefactura,v.id_personal,v.tipov,v.fechaentregah,v.equipo,per.nombre, per.apellido_paterno
            FROM ventacombinada  as v
            INNER JOIN clientes as cli on cli.id=v.idCliente
            INNER join personal as per on per.personalId=v.id_personal
            WHERE v.activo=1  and v.prefactura=1 $w_p AND  v.reg>DATE_SUB(NOW(),INTERVAL '3' MONTH)   AND (v.fechaentrega IS null OR v.fechaentrega='0000-00-00')
            UNION
            SELECT v.id as idpre, 4 as tipoventa,v.idCliente,cli.empresa,v.reg,v.prefactura,v.id_personal,v.tipov,v.fechaentregah,'' as equipo,per.nombre, per.apellido_paterno
            FROM polizasCreadas  as v
            INNER JOIN clientes as cli on cli.id=v.idCliente
            INNER join personal as per on per.personalId=v.id_personal
            WHERE v.activo=1 AND v.combinada=0 and v.prefactura=1 $w_p AND  v.reg>DATE_SUB(NOW(),INTERVAL '3' MONTH)   AND ( v.fechaentrega IS null OR v.fechaentrega='0000-00-00')";
        $query = $this->db->query($strq);
        return $query;
    }
    
    function ocultar_ult_dig($num,$serie){
        $serien = substr($serie, -$num);
        $serienew=str_replace($serien,'****',$serie);
        return $serienew;
    }
    function pagosefectivo($fecha){
        $strq="SELECT pag.idpago,cli.empresa,pag.pago,pag.fecha,pag.observacion,fp.formapago_text,concat(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno) as persona,0 as tipopago, v.id as idventa,'' as equipo
                FROM pagos_ventas as pag
                INNER JOIN f_formapago as fp on fp.id=pag.idmetodo
                INNER JOIN personal as per on per.personalId=pag.idpersonal
                INNER JOIN ventas as v ON v.id=pag.idventa
                INNER JOIN clientes as cli on cli.id=v.idCliente
                WHERE pag.idmetodo=1 AND pag.visto=0 AND pag.reg>'$fecha 00:00:00' 

                UNION

                SELECT 
                    pag.idpago,cli.empresa,pag.pago,pag.fecha,pag.observacion,fp.formapago_text,concat(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno) as persona,1 as tipopago,vc.combinadaId as idventa, vc.equipo
                FROM pagos_combinada as pag
                INNER JOIN f_formapago as fp on fp.id=pag.idmetodo
                INNER JOIN ventacombinada as vc on vc.combinadaId=pag.idventa
                INNER JOIN ventas as v on v.id=vc.equipo
                INNER JOIN clientes as cli on cli.id=v.idCliente
                INNER JOIN personal as per on per.personalId=pag.idpersonal
                WHERE pag.idmetodo=1 AND pag.visto=0 AND pag.reg>'$fecha 00:00:00' 

                UNION

                SELECT pag.idpago,cli.empresa,pag.pago,pag.fecha,pag.observacion,fp.formapago_text,concat(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno) as persona,2 as tipopago,pol.id as idventa,'' as equipo
                FROM pagos_poliza as pag
                INNER JOIN f_formapago as fp on fp.id=pag.idmetodo
                INNER JOIN polizasCreadas as pol on pol.id=pag.idpoliza
                INNER JOIN clientes as cli on cli.id=pol.idCliente
                INNER JOIN personal as per on per.personalId=pag.idpersonal
                WHERE pag.idmetodo=1 and pag.visto=0 AND pag.reg>'$fecha 00:00:00' 

                UNION 
                SELECT 
                    pag.fp_pagosId as idpago,cli.empresa,pag.pago,pag.fecha,pag.observacion,fp.formapago_text,concat(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno) as persona,3 as tipopago,con.idcontrato as idventa,'' as equipo
                    
                FROM factura_prefactura_pagos as pag
                INNER JOIN f_formapago as fp on fp.id=pag.idmetodo
                INNER JOIN factura_prefactura as fap on fap.facId=pag.facId
                INNER JOIN contrato as con on con.idcontrato=fap.contratoId
                INNER JOIN rentas as ren on ren.id=con.idRenta
                INNER JOIN clientes as cli on cli.id=ren.idCliente
                INNER JOIN personal as per on per.personalId=pag.idpersonal
                WHERE pag.idmetodo=1 AND pag.visto=0 AND pag.reg>'$fecha 00:00:00' 

                UNION 
                SELECT pag.idpago,fac.Nombre as empresa,pag.pago,pag.fecha,pag.observaciones,fp.formapago_text,concat(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno) as persona,4 as tipopago,pag.factura as idventa,'' as equipo
                FROM pagos_factura as pag
                INNER JOIN f_facturas as fac on fac.FacturasId=pag.factura
                INNER JOIN f_formapago as fp on fp.id=pag.idmetodo
                INNER JOIN personal as per on per.personalId=pag.idpersonal
                WHERE pag.idmetodo=1 AND pag.visto=0 AND pag.reg>'$fecha 00:00:00' ";
                $query = $this->db->query($strq);
            return $query;
    }
    function listadofacturasprogramadas(){
        $strq="SELECT * FROM f_facturas WHERE activo=1 and Estado=2 AND program=1 AND program_proc=0 AND program_date BETWEEN DATE_SUB(CURDATE(), INTERVAL 2 DAY) AND CURDATE() ORDER BY RAND() LIMIT 1"; 
        $query = $this->db->query($strq);
        return $query;
    }
    function update_status_periodo_fac_comple(){
        $strq="SELECT * from(
                SELECT facpre.facId, facpre.contratoId,facpre.prefacturaId,facpre.statuspago,fac.FacturasId, fac.Folio,fac.fechatimbre ,fac.total,SUM(docd.ImpPagado) as ImpPagado
                FROM factura_prefactura as facpre
                INNER JOIN f_facturas as fac on fac.FacturasId=facpre.facturaId
                INNER JOIN f_complementopago_documento as docd on docd.facturasId=fac.FacturasId
                INNER JOIN f_complementopago as doc on doc.complementoId=docd.complementoId
                WHERE facpre.statuspago=0 AND fac.Estado=1 AND doc.Estado=1
                GROUP BY facpre.facId,fac.FacturasId 
                ) as datos where  ImpPagado>=total
                ORDER BY RAND() LIMIT 1";
        $query = $this->db->query($strq);
        foreach ($query->result() as $itemp) {
            $this->updateCatalogo('factura_prefactura',array('statuspago'=>1,'update_sta'=>1),array('facId'=>$itemp->facId));
        }
    }
    function verificar_pue_pendientes_pago($personal){
        $strq="SELECT * FROM(
                    SELECT fac.FacturasId,fac.serie,fac.Folio,fac.Nombre,fac.Rfc,fac.fechatimbre,fac.total, per.personalId,per.nombre as nombre_per,per.apellido_paterno,
                    CASE 
                            WHEN facv.combinada=0 THEN (
                                SELECT ven.id FROM ventas as ven WHERE ven.id=facv.ventaId AND ven.estatus=3
                            )
                            WHEN facv.combinada=1 THEN (
                                SELECT venc.equipo FROM ventacombinada as venc WHERE venc.combinadaId=facv.ventaId AND venc.estatus=3
                            )
                            ELSE NULL
                    END AS ventas,
                    (
                        SELECT pol.id FROM polizasCreadas as pol WHERE pol.id=facp.polizaId AND pol.estatus=3
                    ) as poliza,
                    facpre.prefacturaId,
                    SUM(pgfac.pago) as pago_fac

                    FROM f_facturas as fac
                    INNER JOIN personal as per on per.personalId=fac.usuario_id
                    LEFT JOIN factura_venta as facv on facv.facturaId=fac.FacturasId
                    LEFT JOIN factura_poliza as facp on facp.facturaId=fac.FacturasId
                    LEFT JOIN factura_prefactura as facpre on facpre.facturaId=fac.FacturasId AND facpre.statuspago=1
                    LEFT JOIN pagos_factura as pgfac on pgfac.factura=fac.FacturasId AND pgfac.activo=1
                    WHERE 
                        per.personalId='$personal' AND
                    fac.FormaPago='PUE' AND
                    fac.Estado=1 AND
                    fac.fechatimbre BETWEEN DATE_SUB(CURDATE(), INTERVAL 1 MONTH) - INTERVAL DAY(DATE_SUB(CURDATE(), INTERVAL 1 MONTH)) - 1 DAY
                                  AND LAST_DAY(DATE_SUB(CURDATE(), INTERVAL 1 MONTH))
                    GROUP BY fac.FacturasId
                    ORDER BY fac.FacturasId DESC
                ) as datos 
                WHERE 
                ventas is null AND
                poliza is null AND
                prefacturaId is null AND
                pago_fac is null";
        $query = $this->db->query($strq);
        return $query;
    }
    function consultar_lectura_general($act,$per){
        if($per>0){
            $w_per =" and per.personalId='$per' and actl.id IS NOT NULL";
        }else{
            $w_per ="";
        }
        $strq="SELECT act.asig_personal as personal,per.nombre,per.apellido_paterno,actl.id,actl.reg
                FROM actividades as act
                inner join personal as per on per.personalId=act.asig_personal
                LEFT JOIN actividades_lectura as actl on actl.tipo=0 AND actl.id_act=act.id AND actl.idpersonal=act.asig_personal
                WHERE act.id='$act' $w_per
                UNION
                SELECT actp.personal,per.nombre,per.apellido_paterno, actl.id,actl.reg
                FROM actividades_otros_personal as actp
                inner join personal as per on per.personalId=actp.personal
                LEFT JOIN actividades_lectura as actl on actl.tipo=0 AND actl.id_act=actp.id_asig AND actl.idpersonal=actp.personal
                WHERE actp.id_asig='$act' $w_per";
        $query = $this->db->query($strq);
        return $query;
    }
    function consultar_lectura_detalle($act,$iddll,$per){
        if($per>0){
            $w_per =" and per.personalId='$per' and actl.id IS NOT NULL";
        }else{
            $w_per ="";
        }
        $strq="SELECT 
                    dat.id_dll,dat.persona,per.nombre,per.apellido_paterno,actl.id as id_lectura,actl.reg 
                FROM(
                    SELECT act.asig_personal as persona,actd.id as id_dll
                    FROM actividades as act
                    INNER JOIN actividades_detalle as actd on actd.id_asig=act.id and actd.id='$iddll' and actd.creo_personalId!=act.asig_personal
                    WHERE act.id='$act'
                    UNION
                    SELECT act.creo_personalId as persona,actd.id as id_dll
                    FROM actividades as act
                    INNER JOIN actividades_detalle as actd on actd.id_asig=act.id and actd.id='$iddll' and actd.creo_personalId!=act.creo_personalId
                    WHERE act.id='$act'
                    UNION
                    SELECT actp.personal,actd.id as id_dll
                    FROM actividades_otros_personal as actp
                    INNER JOIN actividades_detalle as actd on actd.id_asig=actp.id_asig and actd.id='$iddll' and actd.creo_personalId!=actp.personal
                    WHERE actp.id_asig='$act'
                ) as dat
                INNER JOIN personal as per on per.personalId=dat.persona
                LEFT JOIN actividades_lectura as actl on actl.id_act=dat.id_dll and actl.tipo=1 AND actl.idpersonal=dat.persona
                WHERE per.personalId>0 $w_per ";
        $query = $this->db->query($strq);
        return $query;
    }
    function get_verificarseriederentas($ser){
        $strq="SELECT aseq.* 
                FROM series_productos as serpro
                INNER JOIN asignacion_series_r_equipos as aseq on aseq.serieId=serpro.serieId
                WHERE  aseq.activo=1 and serpro.serie='$ser'";
        $query = $this->db->query($strq);
        return $query;
    }
    function get_verif_fac_venta_site($fac){
        $strq="SELECT v.* FROM factura_venta as facv INNER JOIN ventas as v ON v.id=facv.ventaId WHERE v.idtransaccion IS NOT null AND facv.combinada=0 AND facv.facturaId='$fac' ";
        $query = $this->db->query($strq);
        return $query;
    }
}

?>