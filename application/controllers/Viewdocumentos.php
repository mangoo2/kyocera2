<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Viewdocumentos extends CI_Controller {
	function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloResidentes');
    }
	public function index(){
		$id=$this->input->get('id');
		$data['documentos']=$this->ModeloResidentes->getfiles($id);
		$this->load->view('Reportes/documentos',$data);
	        
	}
}