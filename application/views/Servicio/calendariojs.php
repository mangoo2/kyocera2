<script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/toastr/toastr.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/plugins/toastr/toastr.css">
<script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/fileinput/fileinput.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/plugins/fileinput/fileinput.css">
<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/fullcalendar/css/fullcalendar.css">
<script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/fullcalendar/js/fullcalendar.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/fullcalendar/lang/es.js"></script>-->
</html><link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/select2.min.css">
<script src="<?php echo base_url(); ?>public/js/select2.full.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/tipped/tipped-master/dist/js/tipped.min.js" ></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/plugins/tipped/tipped-master/dist/css/tipped.css">
<style type="text/css">
	.fc-license-message{display: none;}
	.fc-agendaDay-view {display: flex; /* Hace que los eventos se muestren en una fila horizontal */
    flex-direction: row; /* Establece la dirección horizontal */
}
/*********************************************************/
.fc .fc-scrollgrid-section-header.fc-scrollgrid-section-sticky > * {
    top: 128px;/* se define para que los encabezados del fullcalendar se queden fijos a una posicion minima */
}
@media only screen and (max-width: 450px){
	.fc .fc-scrollgrid-section-header.fc-scrollgrid-section-sticky > * {
    	top: 60px;/* se define para que los encabezados del fullcalendar se queden fijos a una posicion minima */
	}
}
</style>
<script type="text/javascript" src="<?php echo base_url(); ?>public/js/calendario.js?v=<?php echo date('YmdGis');?>"></script>