
// Obtener la base_url 
var base_url = $('#base_url').val();
// Saber si es solo Visualización
var tipoVista = $("#tipoVista").val();
$(document).ready(function(){
    $('.chosen-select').chosen({width: "90%"});
	// Asignamos el formulario de envío
	var formulario_equipo = $('#equipos_form');
	// Obtenemos la URL base para poder usarla
    if(tipoVista == 2)    {
        loadTablaAccesorios($('#idEquipo').val());
    }
    if(tipoVista == 3)    {
        loadTablaAccesorios($('#idEquipo').val());
        $("input").prop('disabled', true);
        $("textarea").prop('disabled', true);
        $("select").prop('disabled', true);
        $("checkbox").prop('disabled', true);
        $("button").prop('disabled', true);
        $(".btn_agregar").attr('style', 'pointer-events: none;');
    }
	// Validamos el formulario en base a las reglas mencionadas debajo
	formulario_equipo.validate({
            ignore: "",
            rules: 
            {
				modelo: {
                    required: true
                },
                especificaciones: {
                    required: true
                },
                costo_venta: {
                    required: true
                },
                costo_renta: {
                    required: true
                },
                pag_monocromo_incluidos: {
                    required: true
                },
                excedente: {
                    required: true
                },
                pág_color_incluidos: {
                    required: true
                },
                stock: {
                    required: true
                },
                costo_total: {
                    required: true
                },
                categoriaId: {
                    required: true
                },
                idfamilia: {
                    required: true
                },
                noparte:{
                    required: true
                },
                pag_monocromo_incluidos:{
                    required: true
                },
                excedente:{
                    required: true
                },
                pag_color_incluidos:{
                    required: true
                },
                excedentecolor:{
                    required: true
                }
            },
            // Para mensajes personalizados
            messages: 
            {
                modelo:{
                    required: "Ingrese un nombre"
                },
                especificaciones:{
                    required: "Ingrese una especificación"
                },
                costo_venta:{
                    required: "Ingrese la fecha de nacimiento"
                },
                costo_renta:{
                    required: "Ingrese una dirección válida"
                },
                pag_monocromo_incluidos:{
                    required: "Ingrese una cantidad"
                },
                pág_color_incluidos:{
                    required: "Seleccione una cantidad"
                },
                stock:{
                    required: "Seleccione un perfil"
                },
                costo_total:{
                    required: "Ingrese un nombre de usuario"
                },
                categoriaId:{
                    required: "Ingrese una categoria"
                },
                idfamilia:{
                    required: "Ingrese una categoria"
                }
            },
            errorElement : 'div',
            // Si están correctos los campos, procesamos el formulario mediante Ajax
            submitHandler: function (form){
                var DATA  = [];
                var TABLA   = $(".border");
                    TABLA.each(function(){         
                         item = {};
                         item ["accesorio"]   = $(this).find("select[id*='accesorio']").val();
                         DATA.push(item);
                    });
                accesorios = JSON.stringify(DATA);

                var datos = formulario_equipo.serialize()+'&accesorios2='+accesorios;
                
                $.ajax({
                    type:'POST',
                    url: base_url+"index.php/equipos/insertaActualizaEquipo",
                    data: datos,
                    success:function(data){   

                            cargaimage(parseInt(data));
                            if ($("#ttableconsumibles tbody > tr").length>0) {
                                cargarconsumibles(parseInt(data));
                            }
                            swal({ title: "Éxito",
                                text: "El equipo se ha registrado de manera correcta",
                                type: 'success',
                                showCancelButton: false,
                                allowOutsideClick: false,
                            });                  
                            setTimeout(function(){ 
                                window.location.href = base_url+"index.php/equipos"; 
                            }, 2000);   
                    }
                });
            }
        });
        // Basic
    $('.dropify').dropify({
        messages: {
            default: 'Arrastra y suelta un archivo aquí o haz clic',
            replace: 'Arrastre y suelte un archivo o haga clic para reemplazar',
            remove:  'Eliminar',
            error:  'Lo sentimos, el archivo es demasiado grande.'
        }
    });
    $('.formsubmitsave').click(function(event) {
        $('.formsubmitsave').prop( "disabled", true );
        setTimeout(function(){ 
            $('.formsubmitsave').prop( "disabled", false);
        }, 4000);
        $( "#equipos_form" ).submit();
    });
    
    // Used events
    var drEvent = $('.dropify-event').dropify();

    drEvent.on('dropify.beforeClear', function(event, element){
        return confirm("Do you really want to delete \"" + element.filename + "\" ?");
    });

    drEvent.on('dropify.afterClear', function(event, element){
        alert('File deleted');
    });

    $(".btn_agregar").click(function(){
        $('.chosen-select').chosen('destroy');
      var contador = $('.border').length; 
      var contadoraux = contador+1; 
      var $template = $("#datos1"),
      $clone = $template
     .clone()
     .show()
     .removeAttr('id')
     .attr('id',"datos"+contadoraux)
     .insertAfter($("#datos"+contador));

     $clone.find($('#categoria'));
     $clone.find(".div_btn").html('<a class="btn btn-floating waves-effect waves-light red btn_eliminar"><i class="material-icons">delete_forever</i></a>');
     $clone.find("input").val("");
     eliminar();
     $('.chosen-select').chosen({width: "90%"});
     //$('select').material_select();
    });
    // Listener para Eliminar 
    $('#tabla_accesorios_equipo tbody').on('click', 'a.elimina', function (){
        var tr = $(this).closest('tr');
        var row = table3.row(tr);
        var data = row.data();
        confirmaEliminarAccesorio(data.id);
    });
    $('.btn_agregarconsumible').click(function(event) {
        var consumible=$('#consumible option:selected').val();
        var consumibletxt=$('#consumible option:selected').text();
        if (consumible>0) {
            var costo_toner_black=0;
            var costo_toner_cmy=0;
            var rendimiento_toner =0;
            var rendimiento_unidad_imagen =0;
            var tipo=0;
            var stylemk='';
            var costopaginamono=0;
            var costopaginacolor=0;
            params = {};
            params.consu = consumible;
            $.ajax({
                type:'POST',
                url:base_url+'Equipos/preciosaccesorios',
                data:params,
                async:false,
                statusCode:{
                        404: function(data){
                            
                        },
                        500: function(data){
                            
                        }
                    },
                success:function(data){
                    var array = $.parseJSON(data);
                    poliza = array.poliza;
                    rendimiento_toner = array.rendimiento_toner;
                    tipo = array.tipo;
                    var costopaginamono=0;
                    var costopaginacolor=0;
                    if (tipo==5 || tipo==2 || tipo==3 || tipo==4 || tipo==7) {
                        rendimiento_unidad_imagen = array.rendimiento_unidad_imagen;
                        stylemk='';
                        if (parseFloat(rendimiento_toner)>0) {
                            
                            costopaginamono=0;
                            costopaginacolor=parseFloat(poliza)/parseFloat(rendimiento_toner);
                        }
                    }
                    if (tipo==1 || tipo==6) {
                        stylemk='style="display:none"';
                        rendimiento_unidad_imagen = 0;
                        if (parseFloat(rendimiento_toner)>0) {
                            costopaginamono=parseFloat(poliza)/parseFloat(rendimiento_toner);
                            costopaginacolor=0;
                        }
                    }

                }
            });
            var trconsu= '<tr class="class_tr_'+consumible+'">\
                            <td>\
                                <div class="row">\
                                    <input id="consumibleid"  type="hidden" value="0">\
                                    <input id="consumible_selected"  type="hidden" value="'+consumible+'">\
                                    <div class="col s3"><b>'+consumibletxt+'</b></div>\
                                    <div class="col s3">\
                                        <a class="btn-floating red tooltipped elimina" data-position="top" data-delay="50" data-tooltip="Eliminar"  data-tooltip-id="fed519cf-dba7-8626-e402-306629f7d7d1" onclick="deleteconsumible(0,'+consumible+',0)" >\
                                            <i class="material-icons">delete_forever</i>\
                                        </a>\
                                    </div>\
                                </div>\
                                <div class="row">\
                                    <div class="input-field col s3">\
                                      <i class="material-icons prefix">attach_money</i>\
                                      <input id="costo_toner_black" name="costo_toner_black"  type="number" class="inputplace" value="'+poliza+'" readonly>\
                                      <label for="costo_toner_black">Precio toner</label>\
                                    </div>\
                                    <div class="input-field col s3">\
                                      <i class="material-icons prefix">invert_colors</i>\
                                      <input id="rendimiento_toner_black" name="rendimiento_toner_black"  type="number" value="'+rendimiento_toner+'" readonly>\
                                      <label for="rendimiento_toner_black">Rendimiento toner black</label>\
                                    </div>\
                                    <div class="input-field col s3 " >\
                                      <i class="material-icons prefix">invert_colors</i>\
                                      <input id="rendimiento_unidad_imagen" name="rendimiento_unidad_imagen"  type="number" value="'+rendimiento_unidad_imagen+'" readonly>\
                                      <label for="rendimiento_unidad_imagen">Rendimiento unidad imagen</label>\
                                    </div>\
                                    <div class="input-field col s3 eliminacionproxima" '+stylemk+'>\
                                      <i class="material-icons prefix">attach_money</i>\
                                      <input id="costo_unidad_imagen" name="costo_unidad_imagen"  type="number" value="">\
                                      <label for="costo_unidad_imagen">Costo unidad imagen</label>\
                                    </div>\
                                </div>\
                                <div class="row">\
                                    <div class="input-field col s3 eliminacionproxima">\
                                      <i class="material-icons prefix">attach_money</i>\
                                      <input id="costo_garantia_servicio" name="costo_garantia_servicio"  type="number" value="">\
                                      <label for="costo_garantia_servicio">Costo garantia servicio</label>\
                                    </div>\
                                    <div class="input-field col s3 eliminacionproxima">\
                                      <i class="material-icons prefix">invert_colors</i>\
                                      <input id="rendimiento_garantia_servicio" name="rendimiento_garantia_servicio"  type="number" value="">\
                                      <label for="rendimiento_garantia_servicio">Rendimiento garantia servicio</label>\
                                    </div>\
                                    <div class="input-field col s3">\
                                      <i class="material-icons prefix">attach_money</i>\
                                      <input id="costo_pagina_monocromatica" name="costo_pagina_monocromatica"  type="number" value="'+costopaginamono+'">\
                                      <label for="costo_pagina_monocromatica">Costo por página mocromática</label>\
                                    </div>\
                                    <div class="input-field col s3">\
                                      <i class="material-icons prefix">attach_money</i>\
                                      <input id="costo_pagina_color" name="costo_pagina_color"  type="number" value="'+costopaginacolor+'">\
                                      <label for="costo_pagina_color">Costo por página a color</label>\
                                    </div>\
                                </div>\
                            </td>\
                        </tr>';
            $('#tableconsumibles').append(trconsu);
            $('.inputplace').change();
        }else{
            $.alert({
                boxWidth: '30%',
                useBootstrap: false,
                title: 'Error!',
                content: 'Seleccione un consumible'}); 
        }
    });
});
function eliminar(){
    $(".btn_eliminar").click(function(){
        var $row = $(this).parents('.border');
        $row.remove();
    });
}
function confirmaEliminarAccesorio(idAccesorio){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de Eliminar este Accesorio? El cambio será irreversible',
        type: 'red',
        typeAnimated: true,
        buttons: 
        {
            confirmar: function () 
            {
                $.ajax({
                    url: base_url+"index.php/equipos/eliminarAccesorio/"+idAccesorio,
                    success: function (response) 
                    {

                        if(response==1)
                        {
                            $.alert({
                                boxWidth: '30%',
                                useBootstrap: false,
                                title: 'Éxito!',
                                content: 'Equipo Eliminado!'});
                            location.reload();
                        }
                        else
                        {
                            $.alert({
                                boxWidth: '30%',
                                useBootstrap: false,
                                title: 'Error!',
                                content: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema'}); 
                             
                        }
                    },
                    error: function(response)
                    {
                        $.alert({
                                boxWidth: '30%',
                                useBootstrap: false,
                                title: 'Error!',
                                content: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema'});   
                        
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}
function muestraDatosUsuario(){
    if($( "#tipo" ).val()==1)    {
        $('#datosUsuario').attr('style','display: none');
    }
    else{
        $('#datosUsuario').attr('style','display: inline');
    }
}
function loadTablaAccesorios(idEquipo) {
    table3 = $('#tabla_accesorios_equipo').DataTable({
            "ajax":{ "url": base_url+"index.php/equipos/getListadoAcesoriosEquipo/"+idEquipo,
        },
        "columns": [
          {"data": "id"},
          {"data": "nombre"},
          {"data": "no_parte"},
          {"data": "costo"},
          {"data": "idEquipo", visible: false},
          {"data":null,title:"Acciones",orderable:false,class:"centrado_text",
                render:function(data,type,row){
                    $('.tooltipped').tooltip();
                    if (tipoVista == 3) {
                    var html='<ul>\
                                <li>\
                                  <a class="btn-floating red soloadministradores" data-delay="50">\
                                    <i class="material-icons">delete_forever</i>\
                                  </a>\
                                </li>\
                              </ul>';
                    return html;
                    }else{
                    var html='<ul>\
                                <li>\
                                  <a class="btn-floating red tooltipped elimina soloadministradores" data-position="top" data-delay="50" data-tooltip="Eliminar">\
                                    <i class="material-icons">delete_forever</i>\
                                  </a>\
                                </li>\
                              </ul>';
                    return html;
                    }
                }
            }
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[25, 50, 100], [25, 50, 100]],
        "dom": 'Bfrtpl',
        // Muestra el botón de Excel para importar la tabla
        "buttons": [
            /*{   
                extend: 'excelHtml5',
                text: ' Descargar Excel <i class="fa fa-download"></i>',
                className: 'btn classBotonFratsa'
            }
            */
        ],
        // Cambiamos lo principal a Español
        "language": {
            "lengthMenu": "Desplegando _MENU_ elementos por página",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
              }
        }
    });
}
function cargaimage(id){
    if ($('#foto')[0].files.length > 0) {
                var inputFileImage = document.getElementById('foto');
                var file = inputFileImage.files[0];
                var data = new FormData();
                data.append('foto',file);
                data.append('equipos',id);
                $.ajax({
                    url:base_url+'index.php/Equipos/cargafiles',
                    type:'POST',
                    contentType:false,
                    data:data,
                    processData:false,
                    cache:false,
                    success: function(data) {
                        var array = $.parseJSON(data);
                            console.log(array.ok);
                            if (array.ok=true) {
                                //$(".fileinput").fileinput("clear");
                               // toastr.success('Se ha cargado el nuevo documento correctamente','Hecho!');
                                //cargafiles();
                            }else{
                               // toastr.error('Error', data.msg);
                            }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                            var data = JSON.parse(jqXHR.responseText);
                            //toastr.error('Error', data.msg);
                        }
                });
            }
}
function cargarconsumibles(id){
    var DATA  = [];
    var TABLA   = $("#ttableconsumibles tbody > tr");
        TABLA.each(function(){         
            item = {};
            item ["id"]= $(this).find("input[id*='consumibleid']").val();
            item ["idequipo"] = id;
            item ["idconsumible"]   = $(this).find("input[id*='consumible_selected']").val();
            //item ["rendimiento_toner_black"] = $(this).find("input[id*='rendimiento_toner_black']").val();
            //item ["rendimiento_toner_cmy"] = $(this).find("input[id*='rendimiento_toner_cmy']").val();
            item ["costo_unidad_imagen"] = $(this).find("input[id*='costo_unidad_imagen']").val();
            //item ["rendimiento_unidad_imagen"] = $(this).find("input[id*='rendimiento_unidad_imagen']").val();
            item ["costo_garantia_servicio"] = $(this).find("input[id*='costo_garantia_servicio']").val();
            item ["rendimiento_garantia_servicio"] = $(this).find("input[id*='rendimiento_garantia_servicio']").val();
            item ["costo_pagina_monocromatica"] = $(this).find("input[id*='costo_pagina_monocromatica']").val();
            item ["costo_pagina_color"] = $(this).find("input[id*='costo_pagina_color']").val();
            DATA.push(item);
        });
        INFO  = new FormData();
        aInfo   = JSON.stringify(DATA);
        INFO.append('data', aInfo);
        $.ajax({
            data: INFO,
            type: 'POST',
            url : base_url+'Equipos/addconstocunsumibles',
            processData: false, 
            contentType: false,
            async: false,
            statusCode:{
                404: function(data){
                    //toastr.error('Error!', 'No Se encuentra el archivo');
                },
                500: function(){
                    //console.log(data.responseText);
                    //toastr.error('Error', '500');
                }
            },
            success: function(data){
            }
        });
}
function deleteconsumible(tipo,id,name){
    if (tipo==1) {
        swal({   
                title: "Eliminar",
                text: "¿Desea eliminar el consumible "+name+"?",
                type: "warning",   
                showCancelButton: true,
                closeOnConfirm: false,
                showLoaderOnConfirm: true, 
            },
            function(){
                    setTimeout(function(){  
                        params = {};
                        params.consu = id;
                        $.ajax({
                            type:'POST',
                            url:base_url+'Equipos/deleteconsumible',
                            data:params,
                            async:false,
                            statusCode:{
                                    404: function(data){
                                        swal({type: "error", title:"Error 404", timer: 5000});
                                    },
                                    500: function(data){
                                        swal({type: "error", title:"Error 500", timer: 5000});
                                    }
                            },
                            success:function(data){
                                swal({type: "success",title:"Eliminado",timer: 2000});
                                $('.class_trs_'+id).remove();
                            }
                        });   
                      
                }, 2000);
            });
    }else{
        $('.class_tr_'+id).remove();
    }
}
function verificarmodelo(tipoequipo){
    $.ajax({
        type:'POST',
        url:base_url+'Equipos/verificarmodeloexist',
        data:{
            id:tipoequipo,
            modelo:$('#modelo').val()
        },
        async:false,
        statusCode:{
                404: function(data){
                    swal({type: "error", title:"Error 404", timer: 5000});
                },
                500: function(data){
                    swal({type: "error", title:"Error 500", timer: 5000});
                }
        },
        success:function(data){
            console.log(data);
            if (data>0) {
                toastr["error"]("Modelo existente", "Advertencia");
            }
        }
    });   
}
function verificarmodelo2(tipoequipo){
    $.ajax({
        type:'POST',
        url:base_url+'Equipos/verificarmodeloexist2',
        data:{
            id:tipoequipo,
            noparte:$('#noparte').val()
        },
        async:false,
        statusCode:{
                404: function(data){
                    swal({type: "error", title:"Error 404", timer: 5000});
                },
                500: function(data){
                    swal({type: "error", title:"Error 500", timer: 5000});
                }
        },
        success:function(data){
            console.log(data);
            if (data>0) {
                toastr["error"]("Número de parte existente", "Advertencia");
            }
        }
    });   
}
toastr.options = {
  "closeButton": false,
  "debug": false,
  "newestOnTop": false,
  "progressBar": true,
  "positionClass": "toast-top-right",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "5000",
  "timeOut": "5000",
  "extendedTimeOut": "5000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}