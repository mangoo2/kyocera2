<?php 
	
	$spreadsheet = new Spreadsheet();
	//================================================
	$fecha_inicial = new DateTime($fechainicio);
	$fecha_final = new DateTime($fechafin);
	// Necesitamos modificar la fecha final en 1 día para que aparezca en el bucle
	$fecha_final = $fecha_final ->modify('+1 day');

	$intervalo = DateInterval::createFromDateString('1 day');
	$periodo = new DatePeriod($fecha_inicial , $intervalo, $fecha_final);
	$row_h=0;
	foreach ($periodo as $dt) {
		$fecha_de_consulta=$dt->format("Y-m-d");
		if($row_h==0){
			$sheet = $spreadsheet->getActiveSheet();
		}else{
			$spreadsheet->createSheet();
			$sheet = $spreadsheet->setActiveSheetIndex($row_h);
		}
		$sheet->setTitle($fecha_de_consulta);
	    //echo $fecha_de_consulta;
	    //===========================================
	    	$sheet->setCellValue('A1', 'Datos Hoja 1');
	    //===========================================
	    	// Guardar el archivo XLSX
			$writer = new Xlsx($spreadsheet);
			$writer->save('archivo.xlsx');
		$row_h++;
	}
?>
