        </div>
      <!-- END WRAPPER -->
    </div>
    <!-- END MAIN -->
      
    <!-- START FOOTER -->
<!-- ================================================
    Scripts
    ================================================ -->
    
    <!--materialize js-->
    <script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/MaskFocus/MaskedPassword.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/js/materialize.min.js"></script>
    <!--scrollbar-->
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <!--plugins.js - Some Specific JS codes for Plugin Settings-->
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/js/plugins.js"></script>
    <!--custom-script.js - Add your own theme custom JS-->
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/js/custom-script.js"></script>
    <!-- chartist -->
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/jquery-validation/jquery.validate.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/jquery-validation/additional-methods.min.js"></script>
    <!--plugins.js - Some Specific JS codes for Plugin Settings-->
    <!--<script type="text/javascript" src="<?php echo base_url(); ?>app-assets/js/plugins.js"></script>-->
    <!--sweetalert -->
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/sweetalert/dist/sweetalert.min.js"></script>
    <!--extra-components-sweetalert.js - Some Specific JS-->
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/js/scripts/extra-components-sweetalert.js"></script>
    <!-- dropify -->
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/dropify/js/dropify.min.js"></script>
    <!-- jquery confirm -->
    <script src="<?php echo base_url(); ?>app-assets/vendors/confirm/jquery-confirm.min.js"></script>
    <!-- JS Export -->
    <script src="<?php echo base_url(); ?>app-assets/vendors/exportJS/export.js"></script>
    <script src="<?php echo base_url(); ?>app-assets/vendors/exportJS/export.min.js"></script>
    <!-- Full Calendar -->
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/js/jquery.jeditable.js"></script>
    
  </body>
</html>