<!-- START CONTENT -->
<section id="content">
  <!--start container-->
  <div class="container">
    <div class="section">
      <div class="card-panel">
          <div id="table-datatables">
            <br>
            <h4 class="header">Listado</h4>
            <div class="row">
              <div class="col s12">

                <table id="tabla_asignar" class="responsive-table display" cellspacing="0">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Cliente</th>
                      <th>Teléfono</th>
                      <th>Dirección de Instalación</th>
                      <th>Modelo</th>
                      <th>Serie</th>
                      <th>Tipo de venta</th>
                      <td>Acciones</td>
                    </tr>
                  </thead>
                  <tbody>
                    <!------------------------------------->
                    <tr>
                      <th>#</th>
                      <th>Cliente</th>
                      <th>Teléfono</th>
                      <th>Dirección de Instalación</th>
                      <th>Modelo</th>
                      <th>Serie</th>
                      <th>Tipo de venta</th>
                      <td>Acciones</td>
                      <td>
                        <a class="btn-floating red tooltipped minimodal" data-position="top" data-delay="50" data-tooltip="Asignar" data-tooltip-id="dca448ed-3a90-3ed2-3922-f974f10908a4">
                          <i class="material-icons">build</i></a>
                      </td>
                    </tr>
                    <!------------------------------------->
                  </tbody>
                </table>
              </div>
            </div>
          </div>
      </div>
    </div>  
  </div>
</section>
<div id="modalAsignacion" class="modal"  >
    <div class="modal-content ">
      <h4> <i class="material-icons">mode_edit</i> Acciones </h4>
      <div class="col s12 m12 l6">
          <div class="row" align="center">
             <div class="col s12 m12 l12">
               <label class="col s12 m2 l2">Técnico</label>
               <div class="col s12 m8 l8">
                 <select name="tecnicos" id="tecnicos">
                   <option>Names</option>
                   <option>Names</option>
                 </select>
               </div>
             </div> 
             <div class="col s12 m12 l12">
               <label class="col s12 m2 l2">Fecha</label>
               <div class="col s12 m8 l8">
                 <input type="datetime-local" name="">
               </div>
             </div>
             <div class="col s12 m12 l12">
               <label class="col s12 m2 l2">Tipo de Servicio</label>
               <div class="col s12 m8 l8">
                 <select name="tecnicos" id="servicio">
                   <option>Correctivo</option>
                   <option>Preventivo</option>
                   <option>Retiros</option>
                   <option>Instalación</option>
                   <option>Configuración</option>
                 </select>
               </div>
             </div> 
             <div class="col s12 m12 l12">
               <label class="col s12 m2 l2">Prioridad</label>
               <div class="col s12 m10 l10">
                <input name="group1" type="radio" id="test1" />
                <label for="test1">Mismo día</label>
                <input name="group1" type="radio" id="test2" />
                <label for="test2">Día siguiente</label>
                <input name="group1" type="radio" id="test3" />
                <label for="test3">15 días</label>
                <input name="group1" type="radio" id="test4" />
                <label for="test4">Siguiente Servicio</label>
               </div>
             </div>  
          </div>
      </div>  
    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Aceptar</a>
      <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
    </div>
  </div>
</div>
