<!DOCTYPE html>
<html lang="es">
  <head>
    <?php
      $perfilid=$this->session->userdata('perfilid');
      if($perfilid==11){
        $favicons=base_url().'public/img/dimpre.png';
        $title='D.Impresión';
      }else{
        $favicons=base_url().'app-assets/images/favicon/favicon_kyocera.png';
        $title='Kyocera';
      }
      
    ?>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta content="width=device-width,initial-scale=1.0" name=viewport>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-tap-highlight" content="no">
    <meta name="description" content="Sistema Kyocera">
    <meta name="keywords" content="Sistema Kyocera">
    <meta name="theme-color" content="#e31a2f" />
    <title><?php echo $title;?></title>
    <!-- Favicons-->
    <link rel="icon" href="<?php echo $favicons; ?>" sizes="32x32">
    <link rel="apple-touch-icon" href="<?php echo $favicons; ?>">
    <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
    <link href="<?php echo base_url(); ?>app-assets/vendors/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>app-assets/css/icon.css" rel="stylesheet">   
    <link href="<?php echo base_url(); ?>app-assets/vendors/sweetalert/dist/sweetalert.css" type="text/css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>app-assets/vendors/flag-icon/css/flag-icon.min.css" type="text/css" rel="stylesheet">
<!-- CORE CSS-->
    <link href="<?php echo base_url(); ?>app-assets/css/themes/horizontal-menu/materialize.css" type="text/css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>app-assets/css/themes/horizontal-menu/style.css" type="text/css" rel="stylesheet">
    <!-- Custome CSS-->
    <link href="<?php echo base_url(); ?>app-assets/css/custom/custom.css?v=2" type="text/css" rel="stylesheet">
    <!-- CSS style Horizontal Nav-->
    <link href="<?php echo base_url(); ?>app-assets/css/layouts/style-horizontal.css" type="text/css" rel="stylesheet">
    <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
    <link href="<?php echo base_url(); ?>app-assets/vendors/jvectormap/jquery-jvectormap.css" type="text/css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>app-assets/vendors/prism/prism.css" type="text/css" rel="stylesheet"> 
    <!--<link href="<?php echo base_url(); ?>app-assets/vendors/data-tables/css/jquery.dataTables.min.css" type="text/css" rel="stylesheet">-->
    <link href="<?php echo base_url(); ?>app-assets/vendors/data-tables/css/jquery.dataTables.min.1.11.4.css" type="text/css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>app-assets/vendors/data-tables/css/responsive.dataTables.min2.2.9.css" type="text/css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>app-assets/vendors/data-tables/Buttons.2.2.2/css/buttons.dataTables.min.css" type="text/css" rel="stylesheet">
    <!-- DROPIFY -->
    <link href="<?php echo base_url(); ?>app-assets/vendors/dropify/css/dropify.min.css" type="text/css" rel="stylesheet">
    <!-- JQUERY CONFIRM -->
    <link href="<?php echo base_url(); ?>app-assets/vendors/confirm/jquery-confirm.min.css" type="text/css" rel="stylesheet">
    <!-- jQuery Library -->
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/jquery-3.3.1.min.js"></script>
    <!-- DATATABLE EDITABLE -->
    <!--<link href="<?php echo base_url(); ?>app-assets/vendors/datatable-Editable/css/select.dataTables.min.css" type="text/css" rel="stylesheet">-->
    <!-- Chosen -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/chosen2.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/styleb.css">  
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/style_chat.css?v=<?php echo date('Ymd');?>">  
    <!-- Full Calendar -->
    <!--<link href="<?php echo base_url(); ?>app-assets/vendors/fullcalendar/css/fullcalendar.min.css" type="text/css" rel="stylesheet">-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/fullcalendar/css/fullcalendar.css">
    <link href="<?php echo base_url(); ?>public/css/fontawesome/css/all.css" type="text/css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>public/css/bootstrap/bootstrap.meterialize.min.css?v=2" type="text/css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>manifest.json" rel="manifest">
    <!------------>
    <script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/loader/jquery.loader.min.js"></script>
    <link href="<?php echo base_url(); ?>public/plugins/loader/jquery.loader.min.css" type="text/css" rel="stylesheet">
    <!------------>
    <script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/offline/offline.min.js"></script>
    <link href="<?php echo base_url(); ?>public/plugins/offline/offline-theme-chrome-indicator.css" type="text/css" rel="stylesheet">
    <!------------>
    <input id="base_url" type="hidden" name="base_url" value="<?php echo base_url(); ?>">
    <style type="text/css">
      .selectweb{display: none;}
      <?php
        if(isset($_SESSION['selectweb'])){
          if($this->session->userdata('selectweb')==1){
            ?>
              .selectweb{display: block !important;}
            <?php
          }
        }
      ?>
      body.dark-only {color: rgba(255, 255, 255, 0.7);background-color: #202938;}
      .dark-only .card-panel{background-color: #111727;}
      .dark-only input{color: rgb(184 186 191);}
      .dark-only input[type=text]:not(.browser-default):disabled ,.dark-only input[readonly="readonly"], .dark-only textarea[readonly="readonly"] {color: rgb(184 186 191);border-bottom: 1px dotted rgb(184 186 191);}
      .dark-only input[type=text]:not(.browser-default):disabled+label{color: rgb(184 186 191);}
      body.dark-only .collapsible-header{background-color: #202938;}
      body.dark-only table.dataTable.display tbody tr.odd {background-color: #202938;}
      body.dark-only table.dataTable tbody tr {background-color: #111727;}
      body.dark-only table.dataTable.display tbody tr.odd>.sorting_1, 
      body.dark-only table.dataTable.order-column.stripe tbody tr.odd>.sorting_1 {background-color: #202938;}
      body.dark-only table.dataTable.display tbody tr.even>.sorting_1, 
      body.dark-only table.dataTable.order-column.stripe tbody tr.even>.sorting_1 {background-color: #111727;}
      body.dark-only .modal{background-color: #202938;}
      body.dark-only .tabs{background-color: #111727;}
      body.dark-only .modal .modal-footer{background-color: #202938;}
      body.dark-only .btn-flat{color: #bcbfc3;}
      body.dark-only .dataTables_wrapper .dataTables_length, 
      body.dark-only .dataTables_wrapper .dataTables_filter, 
      body.dark-only .dataTables_wrapper .dataTables_info, 
      body.dark-only .dataTables_wrapper .dataTables_processing, 
      body.dark-only .dataTables_wrapper .dataTables_paginate {color: #bcbfc4 !important;}
      body.dark-only .white {background-color: #323e51 !important;}
      body.dark-only button.dt-button, 
      body.dark-only div.dt-button, 
      body.dark-only a.dt-button, 
      body.dark-only input.dt-button{background: linear-gradient(to bottom, rgb(32 41 56) 0%, rgb(32 41 56) 100%);color:white;}
      body.dark-only table.dataTable.hover tbody tr:hover, 
      body.dark-only table.dataTable.display tbody tr:hover {color:black;}
      body.dark-only .jconfirm .jconfirm-box{background-color: #202938;}
      body.dark-only .floatThead-container {background: #111727;}
      body.dark-only .search_serie_div{background: #111727;}
      body.dark-only .select2-results__option{color: black;}
      body.dark-only table.striped>tbody>tr:nth-child(odd){color: black;}
      body.dark-only .enviodeticket-button{background: #111727;}
      body.dark-only .tabs .tab a:hover, 
      body.dark-only .tabs .tab a.active {background-color: transparent;color: #ffffff;}
      body.dark-only .tabs .tab a{color: rgb(221 217 217 / 35%);}
      body.dark-only #horizontal-nav ul li a span {color: rgb(255 255 255 / 87%);}
      body.dark-only table.dataTable thead th, 
      body.dark-only table.dataTable thead td {border-bottom: 1px solid #5b5b5b;}
      body.dark-only table.dataTable.no-footer {border-bottom: 1px solid #5b5b5b;}
      body.dark-only .dataTables_wrapper .dataTables_paginate .paginate_button.disabled,
      body.dark-only .dataTables_wrapper .dataTables_paginate .paginate_button.disabled:hover,
      body.dark-only .dataTables_wrapper .dataTables_paginate .paginate_button.disabled:active {color: #454444 !important;}
      .dataTables_paginate{display: block !important;}
      #servicioscal .fc-view-container{height: inherit;}
      #servicioscal .fc-toolbar h2{font-size: 15px;}
      .kv-file-upload{
        display: none;
      }
      .nav-wrapper ul li .dropdown-menu{
        font-size: 14px;
      }
      .btn-filtrar::after{
        content: 'Filtrar';
      }
    </style>
  </head>