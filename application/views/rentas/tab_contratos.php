<style type="text/css">
  input[readonly],textarea[readonly]{background: #d0d0d052  !important;}
  .bordercel_top {border-top: 1px solid #d0d0d0;}
  .bordercel_button {border-bottom: 1px solid #d0d0d0;}
  .bordercel_left {border-left:  1px solid #d0d0d0;}
  .bordercel_right{border-right: 1px solid #d0d0d0;}
  .span_orden{display: none;}
  .td_mono{border-left: 5px solid black;}
  .td_color{border-left: 5px solid red;}
[data-title]:hover:after {opacity: 1;transition: all 0.1s ease 0.5s;visibility: visible;}
[data-title]:after {content: attr(data-title);background-color: #333;color: #fff;font-size: 14px;font-family: Raleway;position: absolute;padding: 3px 20px;bottom: -1.6em;left: 100%;white-space: nowrap;box-shadow: 1px 1px 3px #222222;opacity: 0;border: 1px solid #111111;z-index: 99999;visibility: hidden;border-radius: 6px;}
[data-title] {position: relative;}
.chosen-container{
  margin-left: 0px;
}
.fechaintalacion{
  font-style: italic;
  color: green;
  font-size: 11px;
}
</style>
<br>
  <!--Fixed Width Tabs-->
    <form id="form_pagos" method="post">
      <input type="hidden" name="id_renta" id="id_renta" > 
      <div class="row">
          
        <div class="col s2">
          <label>Tipo</label>
          <select id="tipo1" name="tipo1" class="browser-default chosen-select readonlys"><option value="1" >Adelantada</option><option value="2" >Vencida</option></select>
        </div>
        <div class="col s2">
          <label>Tipo</label>
          <select id="tipo2" name="tipo2" class="browser-default chosen-select readonlys" onchange="selectipo2()"><option value="0" >Seleccione</option><option value="1" >Individual</option><option value="2" >Global</option></select>
        </div>  
        <div class="col s2">
          <label>Categoría</label>
          <select id="tipo3" name="tipo3" class="browser-default chosen-select readonlys"><option value="1" >Renta en depósito</option><option value="2" >Combinada</option></select>
        </div>
        <div class="col s2">
          <label>Tipo Equipos</label>
          <select id="tipo4" name="tipo4" class="browser-default chosen-select readonlys"><option value="1">Serv.monocromático</option><option value="2">Serv.mono+color</option><option value="3">Serv.mono+color(Compra Toner)</option><option value="4">Serv.color</option></select>
        </div>
        <div class="input-field col s2">
          <input id="ordenc_vigencia" name="ordenc_vigencia" type="date" class="readonlys">
          <label for="ordenc_vigencia" class="active" title="se calcula por la fecha de solicitud mas los meses del contrato">Vigencia Contrato</label>
        </div>
        <div class="col s2">
          <div class="switch">
            <label class="">Factura auto<input type="checkbox" name="fac_auto" id="fac_auto" value="1"><span class="lever"></span></label>
          </div>
        </div>
      </div>
      <br>
      <hr>
      <div class="row">
        <div class="col 12">
          <p class="caption">Orden de compra</p>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s2">
          <input id="ordenc_numero" name="ordenc_numero" type="text" readonly>
          <label for="ordenc_numero" class="active">Número</label>
        </div>
        <div class="input-field col s2">
          <input id="oc_vigencia" type="date" class="readonlys" readonly disabled>
          <label for="oc_vigencia" class="active">Vigencia</label>
        </div>
        <div class="input-field col s2">
          <input id="monto" name="monto" type="number" class="readonlys">
          <label for="monto" class="active">Monto Monocromatico</label>
        </div>
        <div class="input-field col s2">
          <input id="montoc" name="montoc" type="number" class="readonlys">
          <label for="montoc" class="active">Monto Color</label>
        </div>
        <div class="input-field col s2">
          <input id="saldo" name="saldo" type="number" class="readonlys">
          <label for="saldo" class="active">Saldo</label>
        </div>
      </div>
      <hr>
      <!-- -->
      <div class="row_equipo">
        
      </div>  
      <div class="row_equipo_n" style="display: none">
        <div class="row tabla_pagos">
          <ul class="collapsible " data-collapsible="accordion">

            <?php foreach ($equiposrenta as $item){ 
              if ($item->estatus==1 or $item->activo==1) {
                
            ?>
              <li>
                <input id="equipos_cliks" type="hidden" value="<?php echo $item->id ?>">
                <input id="serie_cliks" type="hidden" value="<?php echo $item->serieId?>">
                <div class="collapsible-header"><?php echo $item->modelo;if($item->usado==1){ echo '-mc';} ?> / <?php echo $item->serie ?></div>
                <div class="collapsible-body">
                  <div class="row">
                    <div class="col s2">
                      <label>Renta Monocromatico</label>
                      <input id="renta" type="number" class="readonlys rentarow renta_<?php echo $item->id ?>_<?php echo $item->serieId ?>">
                    </div>
                    <div class="col s2">
                      <label>Clicks incluidos mono</label>
                      <input id="clicks_mono" type="number" class="readonlys clicks_mono_<?php echo $item->id ?>_<?php echo $item->serieId ?>">
                    </div>
                    <div class="col s2">
                      <label>Precio click excedente</label>
                      <input id="precio_c_e_mono" type="number" class="readonlys precio_c_e_mono_<?php echo $item->id ?>_<?php echo $item->serieId ?>">
                    </div>
                    <div class="col s2">
                      <label>Renta color</label>
                      <input id="rentac" type="number" class="readonlys rentacrow rentac_<?php echo $item->id ?>_<?php echo $item->serieId ?>">
                    </div>
                    <div class="col s2">
                      <label>Clicks incluidos color</label>
                    <input id="clicks_color" type="number" class="readonlys clicks_color_<?php echo $item->id ?>_<?php echo $item->serieId ?>">
                    </div>
                    <div class="col s2">
                      <label>Precio click color excedente</label>
                      <input id="precio_c_e_color" type="number" class="readonlys precio_c_equi_color_<?php echo $item->id ?> precio_c_e_color_<?php echo $item->id ?>_<?php echo $item->serieId ?>">
                    </div>
                    
                  </div>
                </div>
              </li>
            <?php } }?>  
            
          </ul>  
        </div>
      </div>  
      <div class="row">
        <div class="col s2">
          <h6>Fecha de instalación</h6>
        </div>  
        <div class="col s2">
          <input id="fecha_instalacion" name="fecha_instalacion" type="date" class="readonlys">
        </div>
        <div class="col s2">
          <input type="checkbox" name="fechadiferentev" id="fechadiferentev">
          <label for="fechadiferentev">Otra fecha de inicio</label>
        </div>
        <div class="col s3 fechadiferentev" style="display: none;">
          <input type="date" name="fechadiferente" id="fechadiferente">
        </div>
        <div class="col s2" style="display: none;">
          <h6>periodos</h6>
        </div>  
        <div class="col s2" style="display: none;">
          <input id="periodo_ini" name="periodo_ini" type="number" value="0" class="readonlys">
        </div>
        <div class="col" style="display: none;">
          <h6>Al</h6>
        </div>  
        <div class="col s2" style="display: none;">
          <input id="perioro_fin" name="perioro_fin" type="number" value="0" class="readonlys">
        </div>
      </div>
    </form>    
    <hr>
    <div class="row">
      <div class="col s12" align="center">
        <h6 class="caption">Equipos</h6>
      </div>  
    </div>  
    <div class="row">
      <div class="col s12">
        <table id="tabla_equipos" class="responsive-table display" cellspacing="0">
          <thead>
            <tr>
              <th rowspan="2">Equipo No.</th>
              <th rowspan="2">Ubicación</th>
              <th rowspan="2">Modelo</th>
              <th rowspan="2">Serie</th>
              <th rowspan="2"></th>
              <th rowspan="2">Producción promedio<br>
                <a class="waves-effect cyan btn-bmz" onclick="ppdate()"><i class="fas fa-calendar"></i></a>
              </th>
              <th colspan="5" class="bordercel_top bordercel_button bordercel_left bordercel_right" align="center" style="text-align: center;">Toner</th>
              <th rowspan="2">Escaner Contador inicial</th>
              <!--<th rowspan="2">Accesorios</th>-->
              <th rowspan="2">Consumibles sol.</th>
              <th rowspan="2">Periodicidad</th>
            </tr>
            <tr>
              <th>Modelo</th>
              <th>Rendimiento</th>
              <th>Arranque</th>
              <th>Stock mensual</th>
              <th>Contador Inicial</th>
            </tr>
          </thead>
          <tbody>
          <?php 
            $equiporow=1;
          foreach ($equiposrenta as $item){ 
            if($item->estatus==1 or $item->activo==1){
              /* if ($item->tipo==1 || $item->tipo==3) { */
                


                $infocon=$this->ModeloCatalogos->consumibleagregado_tipo($item->idEquipo,1);
                $infocon_modelo=$infocon['modelo'];
                $infocon_rendimiento=$infocon['rendimiento'];
                if($infocon_rendimiento>0){
                  $rendimiento_contu=$infocon_rendimiento;
                }else{
                  $rendimiento_contu=$this->Rentas_model->equiposrentastonerrendimiento($item->idEquipo,$item->idRenta,1);  
                }
                
              ?>
              <tr>
                <td class="td_orden td_orden_<?php echo $item->id;?>_<?php echo $item->serieId;?>_1 td_mono" data-title="Monocromatico">
                  <input type="hidden" id="areid" class="areid_<?php echo $item->id;?>_<?php echo $item->serieId;?>_1" style="width: 75px;">
                  <input type="hidden" id="tipo" class="tipo_<?php echo $item->id;?>_<?php echo $item->serieId;?>_1 readonlys" value="1" style="width: 110px;" >
                  <input type="hidden" id="tipocon" class="tipo_<?php echo $item->id;?>_<?php echo $item->serieId;?>_1 readonlys" value="1" style="width: 110px;" >
                  <input type="hidden" id="id_equipo" value="<?php echo $item->id ?>">
                  <input type="text" id="orden" class="orden_<?php echo $item->id;?>_<?php echo $item->serieId;?>_1 readonlys" value="<?php echo $item->orden;?>" style="width: 75px; display: none;"><?php echo $item->orden;?>
                </td>
                <td>
                  <input type="text" id="ubicacion" class="ubicacion_<?php echo $item->id;?>_<?php echo $item->serieId;?>_1 readonlys" style="width: 75px; display: none;" value="<?php echo $item->ubicacion;?>">
                  <?php echo $item->ubicacion;?></td>
                <td ><input type="hidden" id="idEquipo" value="<?php echo $item->id;?>" ><?php echo $item->modelo; if($item->usado==1){ echo '-mc';} ?></td>
                <td><input type="hidden" id="serieId" value="<?php echo $item->serieId?>"><?php echo $item->serie ?><br><span class="fechaintalacion instala_<?php echo $item->id;?>" data-rowe="<?php echo $item->id;?>" data-serieid="<?php echo $item->serieId;?>" data-seriename="<?php echo $item->serie;?>" title="Fecha de instalacion"></span></td>
                <td><?php echo $item->contadores ?></td>
                <td><?php 
                    if(isset($_GET['ppfi'])){
                      $ppfi=$_GET['ppfi'];
                    }else{
                      $ppfi='';
                    }
                    if(isset($_GET['ppff'])){
                      $ppff=$_GET['ppff'];
                    }else{
                      $ppff='';
                    }
                    echo $this->Rentas_model->produccionpromedio($item->id,$item->serieId,1,$ppfi,$ppff); ?></td>
                <td class="bordercel_left"><?php
                  echo '<!--('.$item->idEquipo.'-'.$item->idRenta.')--><br>';
                  //echo $this->Rentas_model->equiposrentastoner($item->idEquipo,$item->idRenta,1); 
                  //echo '------';
                  echo $infocon_modelo;
                ?></td>
                <td>
                  <input type="number" 
                  id="nuevo" 
                  class="nuevo_<?php echo $item->id;?>_<?php echo $item->serieId;?>_1 readonlys" 
                  style="width: 75px;" 
                  value="<?php echo $rendimiento_contu;?>"
                  class="readonlys">
                </td>
                <td><input type="number" id="arranque" class="arranque_<?php echo $item->id;?>_<?php echo $item->serieId;?>_1 readonlys" style="width: 75px;" value="0"></td>
                <td><input type="number" id="stock_mensual" class="stock_mensual_<?php echo $item->id;?>_<?php echo $item->serieId;?>_1 readonlys" style="width: 110px;" ></td>
                <td class="bordercel_right" data-title="Monocromatico"><input type="number" id="contador_inicial" class="contador_inicial_<?php echo $item->id;?>_<?php echo $item->serieId;?>_1 readonlys" value="0" style="width: 110px;"></td>
                <td><input type="number" id="contador_iniciale" class="contador_iniciale_<?php echo $item->id;?>_<?php echo $item->serieId;?>_1 readonlys" value="0" style="width: 110px;"></td>
                <!--<td><?php
                  //echo $this->Rentas_model->equiposrentasaccesorios($item->idEquipo,$item->idRenta); 
                ?></td>-->
                <td><input type="number" id="sol_consu_cant" class="sol_consu_cant_<?php echo $item->id;?>_<?php echo $item->serieId;?>_1 readonlys" style="width: 75px;" value="0" title="Cantidad de consumibles"></td>
                <td><input type="number" id="sol_consu_periodo" class="sol_consu_periodo_<?php echo $item->id;?>_<?php echo $item->serieId;?>_1 readonlys" style="width: 75px;" value="0" title="Periodo de meses para la solicitud"></td>
              </tr>
            <?php $equiporow++; 
                /* } */
              }
            }
            foreach ($equiposrenta as $item){ 
            if ($item->tipo==2 || $item->tipo==3) {
              $infocon=$this->ModeloCatalogos->consumibleagregado_tipo($item->idEquipo,2);
                //var_dump($infocon);
                $infocon_modelo=$infocon['modelo'];
                $infocon_rendimiento=$infocon['rendimiento'];
                if($infocon_rendimiento>0){
                  $rendimiento_contu=$infocon_rendimiento;
                }else{
                  $rendimiento_contu=$this->Rentas_model->equiposrentastonerrendimiento($item->idEquipo,$item->idRenta,2);  
                }
            ?>
            <tr>
              <td class="td_orden td_orden_<?php echo $item->id;?>_<?php echo $item->serieId;?>_2 td_color" data-title="Color">
                <input type="hidden" id="areid" class="areid_<?php echo $item->id;?>_<?php echo $item->serieId;?>_2" style="width: 75px;">
                <input type="hidden" id="tipo" class="tipo_<?php echo $item->id;?>_<?php echo $item->serieId;?>_2 readonlys" value="2" style="width: 110px;">
                <input type="hidden" id="tipocon" class="tipo_<?php echo $item->id;?>_<?php echo $item->serieId;?>_2 readonlys" value="2" style="width: 110px;">
                <input type="hidden" id="id_equipo" value="<?php echo $item->id ?>">
                <input type="text" id="orden" class="orden_<?php echo $item->id;?>_<?php echo $item->serieId;?>_2 readonlys" style="width: 75px; display: none;"><?php echo $item->orden;?>
              </td>
              <td><input type="text" id="ubicacion" class="ubicacion_<?php echo $item->id;?>_<?php echo $item->serieId;?>_2 readonlys" style="width: 75px; display: none;" value="<?php echo $item->ubicacion;?>"><?php echo $item->ubicacion;?></td>
              <td><input type="hidden" id="idEquipo" value="<?php echo $item->id;?>" ><?php echo $item->modelo;if($item->usado==1){ echo '-mc';} ?></td>
              <td><input type="hidden" id="serieId" value="<?php echo $item->serieId?>"><?php echo $item->serie ?><span class="fechaintalacion instala_<?php echo $item->id;?>" data-rowe="<?php echo $item->id;?>" data-serieid="<?php echo $item->serieId;?>" data-seriename="<?php echo $item->serie;?>"></span></td>
              <td><?php echo $item->contadores ?></td>
              <td><?php 
                      if(isset($_GET['ppfi'])){
                      $ppfi=$_GET['ppfi'];
                    }else{
                      $ppfi='';
                    }
                    if(isset($_GET['ppff'])){
                      $ppff=$_GET['ppff'];
                    }else{
                      $ppff='';
                    }
                echo $this->Rentas_model->produccionpromedio($item->id,$item->serieId,2,$ppfi,$ppff); ?></td>
              <td class="bordercel_left"><?php
                echo '<!--('.$item->idEquipo.'-'.$item->idRenta.')--><br>';
                //echo $this->Rentas_model->equiposrentastoner($item->idEquipo,$item->idRenta,2); 
                //echo '------';
                  echo $infocon_modelo;
                  //echo json_encode($infocon);
              ?></td>
              <td>
                <input type="number" 
                id="nuevo" 
                class="nuevo_<?php echo $item->id;?>_<?php echo $item->serieId;?>_2 readonlys" 
                style="width: 75px;" 
                value="<?php echo $rendimiento_contu;?>"
                class="readonlys">
                <?php echo $rendimiento_contu;?>
              </td>
              <td><input type="number" id="arranque" class="arranque_<?php echo $item->id;?>_<?php echo $item->serieId;?>_2 readonlys" style="width: 75px;" value="0"></td>
              <td><input type="number" id="stock_mensual" class="stock_mensual_<?php echo $item->id;?>_<?php echo $item->serieId;?>_2 readonlys" style="width: 110px;"></td>
              <td class="bordercel_right" data-title="Color"><input type="number" id="contador_inicial" class="contador_inicial_<?php echo $item->id;?>_<?php echo $item->serieId;?>_2 readonlys" value="0" style="width: 110px;"></td>
              <td class=""><input type="number" id="contador_iniciale" class="contador_iniciale_<?php echo $item->id;?>_<?php echo $item->serieId;?>_2 readonlys" value="0" style="width: 110px;" readonly></td>
              <!--<td><?php
                //echo $this->Rentas_model->equiposrentasaccesorios($item->idEquipo,$item->idRenta); 
              ?></td>-->
              <td><input type="number" id="sol_consu_cant" class="sol_consu_cant_<?php echo $item->id;?>_<?php echo $item->serieId;?>_2 readonlys" style="width: 75px;" value="0" title="Cantidad de consumibles"></td>
              <td><input type="number" id="sol_consu_periodo" class="sol_consu_periodo_<?php echo $item->id;?>_<?php echo $item->serieId;?>_2 readonlys" style="width: 75px;" value="0" title="Periodo de meses para la solicitud"></td>
            </tr>
          <?php $equiporow++; 
              }
            }

          ?> 
        </tbody>
        </table>
      </div> 
    </div>  

    <hr>
    <div class="row">
      <div class="col s11" align="center">
        <h6 class="caption">Contactos</h6>
      </div>
      <div class="col s1">
        <a class="b-btn b-btn-primary bttondisabled" onclick="modalcontactos()"><i class="fas fa-user-plus"></i></a>
      </div>
    </div>
    <div class="row">
      <div class="input-field col s2">
        <input id="tipo" name="tipo" type="text" class="readonlys">
        <label for="tipo" class="active">Tipo</label>
      </div>
      <div class="input-field col s2">
        <input id="nombre" name="nombre" type="text" class="readonlys">
        <label for="nombre" class="active">Nombre</label>
      </div>
      <div class="input-field col s2">
        <input id="telefono" name="telefono" type="number" class="readonlys">
        <label for="telefono" class="active">Teléfono</label>
      </div>
      <div class="input-field col s2">
        <input id="correo" name="correo" type="email" class="readonlys">
        <label for="correo" class="active">Correo</label>
      </div>
      <div class="input-field col s2">
        <input id="ubicacion_c" name="ubicacion_c" type="text" class="readonlys">
        <label for="ubicacion" class="active">Ubicación</label>
      </div>
      <div class="input-field col s2">
        <input id="ultima" name="ultima" type="date" class="readonlys">
        <label for="ultima" class="active">Última actualización</label>
      </div>
      <div class="input-field col s11">
        <input id="comentario" name="comentario" type="text" class="readonlys" autocomplete="off">
        <label for="comentario" class="active">Cometarios</label>
      </div>
      <div class="input-field col s1">
         <a class="btn-floating cyan tooltipped bttondisabled" data-position="top" data-delay="50" data-tooltip="Agregar contato" onclick="agregarcontacto()"><i class="material-icons">add</i></a>
      </div>
    </div>
    <div class="row">
        <div class="col s12">
            <table id="tablacontactos" class="striped responsive-table display" cellspacing="0">
              <thead>
                <tr>
                  <th>#</th><th>Tipo</th><th>Nombre</th><th>Teléfono</th><th>Correo</th><th>Ubicación</th><th>Última actualización</th><th>Cometarios</th>
                </tr>
              </thead>
              <tbody class="tabla_equiposrenta">
              </tbody>
            </table>
        </div>
    </div>
    <hr>
    <form id="form_procedimientos" method="post">  
      <div class="row">
        <div class="col s12" align="center">
          <h6 class="caption">Procedimientos</h6>
        </div>  
      </div>  
      <div class="row">
        <div class="col s2">
          <h6 >Servicio</h6>
        </div>
        <div class="input-field col s3">
          <input id="servicio_horario" name="servicio_horario" type="text" class="readonlys">
          <label for="servicio_horario" class="active">Horario disponible</label>
        </div>
        <div class="input-field col s3">
          <input id="servicio_equipo" name="servicio_equipo" type="text" class="readonlys">
          <label for="servicio_equipo" class="active">Equipo para acceso</label>
        </div>
        <div class="input-field col s4">
          <input id="servicio_doc" name="servicio_doc" type="text" class="readonlys">
          <label for="servicio_doc" class="active">Documentación de acceso</label>
        </div>
      </div>
      <div class="row">
        <div class="col s2">
          <h6 >Compras</h6>
        </div>
        <div class="input-field col s3">
          <input id="compras_portal" name="compras_portal" type="text" class="readonlys">
          <label for="compras_portal" class="active">Portal</label>
        </div>
        <div class="input-field col s3">
          <input id="compras_usuario" name="compras_usuario" type="text" class="readonlys">
          <label for="compras_usuario" class="active">Usuario</label>
        </div>
        <div class="input-field col s4">
          <input id="compras_contra" name="compras_contra" type="text" class="readonlys">
          <label for="compras_contra" class="active">Contraseña</label>
        </div>
      </div>
      <div class="row">
        <div class="col s2">
          <h6 >Cobranza</h6>
        </div>
        <div class="input-field col s10">
          <textarea placeholder="Horario disponible" rows="2" id="cobranza" name="cobranza" type="text" class="readonlys"></textarea>
        </div>
      </div>
      <div class="row">
        <div class="col s2">
          <h6 >Descripción de factura</h6>
        </div>
        <div class="input-field col s10">
          <input type="text" name="descrip_fac" id="descrip_fac" class="form-control-bmz readonlys" onkeypress="return ((event.charCode >= 48 && event.charCode <= 57) || (event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || event.charCode <= 32)" title="solo se admine caracteres alfanumericos">
        </div>
      </div>
    </form>  

    <div class="row centros_costos" style="display:none;">
      <div class="col s3">
        
      </div>
      <div class="col s12">
        <ul class="collapsible" data-collapsible="accordion">
          <li class="">
            <div class="collapsible-header">Centro de Costos</div>
            <div class="collapsible-body" style="display: none;">
              <!--------------------->
              <div class="row">
                <div class=" col s6">
                  <div class="col s9">
                    <input type="text" id="cc_descripcion" class="form-control-bmz">
                  </div>
                  <div class="col s3">
                    <a class="b-btn b-btn-success " onclick="addcentrocostos()" >Agregar</a>
                  </div>
                  <div class="col s12">
                    <table class="table">
                      <thead>
                        <th>Nombre</th><th>clicks Mono</th><th>clicks Color</th><th></th>
                      </thead>
                      <tbody class="view_centroscostos">
                        
                      </tbody>
                      
                    </table>
                  </div>
                </div>
                <div class=" col s6">
                  <table>
                    <thead>
                      <tr>
                        <th>Modelo</th><th>Serie</th><th>Centro de costos</th>
                      </tr>
                    </thead>
                    <tbody>
                      
                    
                  <?php foreach ($equiposrenta as $item){ ?>
                    <tr><td><?php echo $item->modelo; ?></td><td><?php echo $item->serie; ?></td><td><select class="browser-default form-control-bmz idcentroc select_ccs <?php echo 'ccs_'.$item->id.'_'.$item->idEquipo.'_'.$item->serieId; ?>" data-rowequipo="<?php echo $item->id;?>" data-modeloid="<?php echo $item->idEquipo;?>" data-serieid="<?php echo $item->serieId;?>" ></select></td></tr>
                  <?php } ?>
                  </tbody>
                  </table>
                </div>
              </div>
              <!--------------------->
            </div>
          </li>
        </ul>
      </div>
    </div>

  <!--fin1 -->
  <div class="row">
    <div class="botonfinalizar"></div>
    <div class="input-field col s12 guadarremove">
      <div class="add_editar_btn"></div>
      <div class="bttondisabled">
        <button class="btn cyan waves-effect waves-light right bttondisabled" type="button" onclick="pagos_procedimientos()"><span class="titulo_btn">Guardar</span>
        <i class="material-icons right">send</i>
        </button>
      </div>
    </div>
  </div>
  <!-- Modal finaliza -->
<div id="modal_editor" class="modal"  >
    <div class="modal-content ">
        <h4>Editar Contrato</h4> 
        <p>Ingresa contraseña</p>
        <div class="row">  
          <div class="input-field col s12">
            <input id="ver_pass" type="password">
            <label for="ver_pass" class="active">Contraseña</label>
          </div>
        </div>  
    </div>
    <div class="modal-footer">
        <button class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</button>
        <button class="waves-effect waves-red red btn-flat" style="color: white;" onclick="vericar_pass()">Aceptar</button>
    </div>
    <!-- Modal finaliza fin-->
</div>
<script type="text/javascript">
  $(document).ready(function($) {
    contratosview(<?php echo $idcontrato;?>);
  });
</script>
