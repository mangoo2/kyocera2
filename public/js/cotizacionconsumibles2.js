var base_url = $('#base_url').val();
var editor; 
var idCotizacion = $('#idCotizacion').val();
function editar_cons(id,costo,cantidad){
    var idCliente=$('#idCliente').val();
    if ($('#estatuscot').val()==1) {
        $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Editar Precio Equipo',
        content: '<input id="passwordcons" type="password" class="validate form-control-bmz" autocomplete="new-password" placeholder="Contraseña" required><br>'+
                 '<label for="newprecio">Nuevo Precio</label><br>'+
                 '<input id="newprecio" type="number" class="validate form-control-bmz" placeholder="Precio" value="'+costo+'" required>',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var precio = $('#newprecio').val()==''?0:$('#newprecio').val();
                var pass=$('#passwordcons').val();
                if (pass!='') {
                    //if (precio>0) {
                        $.ajax({
                            type:'POST',
                            url: base_url+'Cotizaciones/editarprecioConsumicles',
                            data: {
                                precio: precio,
                                equipo: id,
                                pass: pass,
                                sumtotal: precio,
                                idCliente:idCliente
                                },
                                async: false,
                                statusCode:{
                                    404: function(data){
                                        swal("Error", "404", "error");
                                    },
                                    500: function(){
                                        swal("Error", "500", "error"); 
                                    }
                                },
                                success:function(data){
                                  if (data==1) {
                                    swal("Editado!", "Precio Editado!", "success");
                                    setTimeout(function(){ window.location.href=''; }, 3000);
                                  }else{
                                    swal("Error", "No tiene permiso", "error"); 
                                  }

                                }
                            });
                    //}else{
                    //    swal("Error", "El precio no debe de ser menor o igual a cero", "error"); 
                    //}
                }else{
                    swal("Error", "Debe de ingresar una contraseña", "error"); 
                }  
            },
            cancelar: function (){
            }
        }
    });
    }
}
function editarp_consu(id,piezas){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: 'nueva cantidad a modificar<br>'+
                 '<input type="number" id="newcantidad" class="name form-control" value="'+piezas+'" required/>',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                
                     $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Cotizaciones/editarpiezasConsumicles",
                        data: {
                            id:id,
                            cantidad:$('#newcantidad').val()
                        },
                        success: function (response){
                            location.reload();
                        },
                        error: function(response){
                            $.alert({
                                    boxWidth: '30%',
                                    useBootstrap: false,
                                    title: 'Error!',
                                    content: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema'});   
                        }
                    });
            },
            cancelar: function (){
            }
        }
    });
}
function deleteconsumiblev(id){
        var html="¿Desea eliminar el consumible?";
     $.confirm({
            boxWidth: '30%',
            useBootstrap: false,
            icon: 'fa fa-warning',
            title: 'Editar condiciones!',
            content: html,
            type: 'red',
            typeAnimated: true,
            buttons:{
                confirmar: function (){
                    $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Cotizaciones/deleteconsumiblev",
                        data: {
                            id:id
                        },
                        success: function (response){
                                location.reload();
                        },
                        error: function(response){
                            $.alert({
                                    boxWidth: '30%',
                                    useBootstrap: false,
                                    title: 'Error!',
                                    content: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema'});   
                        }
                    });   
            },
                cancelar: function () {
                }
            }
        });
}