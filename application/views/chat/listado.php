<style type="text/css">
  #tabla_empleados td{
    font-size: 13px;
  }
  .info_clipro{
    float: inline-end;
  }
</style>
<!-- START CONTENT -->
    <section id="content">
    	<!--start container-->
      	<div class="container">
      		<div class="section">

      		<input id="base_url" type="hidden" name="base_url" value="<?php echo base_url(); ?>">

      		<div id="table-datatables">
      			<br>
                <h4 class="header">Listado de Chats</h4>
                <div class="row">
                  <div class="col s3">
                    <label>Ejecutivo</label>
                    <select class="browser-default form-control-bmz" id="eje" onchange="loadtable()">
                      <option value="0">Todos</option>
                      <?php foreach ($personalrows->result() as $item) { 
                          echo '<option value="'.$item->personalId.'">'.$item->nombre.' '.$item->apellido_paterno.' '.$item->apellido_paterno.'</option>';
                      } ?>
                    </select>
                  </div>
                  <div class="col s3">
                    <label>Cliente</label>
                    <select class="browser-default form-control-bmz" id="idcliente" onchange="loadtable()">
                      <option value="0">Todos</option>
                    </select>
                  </div>
                  

                  <div class="col s12">

                    <table id="tabla_chats" class="responsive-table display" cellspacing="0">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Fecha y hora</th>
                          <th>Cliente o Prosecto</th>
                          <th>Ejecutivo</th>
                          <th>Departamento</th>
                          <th>Estatus</th>
                          <th>Acciones</th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>  
               </div>
        </section>
