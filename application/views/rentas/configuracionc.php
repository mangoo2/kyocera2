<?php 
  $nuevaURL=base_url().'Gestor_archivos';
  header('Location: '.$nuevaURL);
?>
<style type="text/css">
  table td{
    font-size: 12px;
    padding: 6px 7px;
  }
</style>
<!-- START CONTENT -->
<section id="content" width="100%">
  
  <!--start container-->
  <div class="container" width="100%">
    <div class="section">
      <div class="row">
        <p class="caption col s6"></p>
      </div>
      <div class="row">        
          <div class="col s12 m12 24">
            <div class="card-panel">
              <div class="row">
              <form id="contrato-form" method="post">
                <div class="row">
                  <div class="col">
                    <h6>Seleccionar cliente</h6>
                  </div>  
                  <div class="col s4">
                    <select id="idcliente" name="idcliente" class="browser-default chosen-select" onchange="clientecontrato()">
                      <option value="" disabled selected>Selecciona una opción</option>
                      <<?php foreach ($clientes as $item){ ?>
                        <option value="<?php echo $item->id?>" ><?php echo $item->empresa?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                <br>
                <div class="row mostrar_tabla"></div>
              </form>   
              </div>
            </div>
          </div>
        
      </div>
    </div>
  </section>