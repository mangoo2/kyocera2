var base_url=$('#base_url').val();
$(document).ready(function($) {
  var validateSubmitForm = $('#validateSubmitForm');
      validateSubmitForm.validate({
            rules: 
            {
                Monto: {
                    equalTo: '#totalimportes'
                },
                NumOperacion:{
                  required:true
                }

            },
            // Para mensajes personalizados
            messages: 
            {
                Monto:{
                    required: "Ingrese un monto",
                    equalTo:'Por favor ingrece el total de los importes'
                },
               
            },
        errorElement : 'div',
            // Si están correctos los campos, procesamos el formulario mediante Ajax
        submitHandler: function (form) 
        {   
            
            
        }
    
     });
  //----------------------------------------------------------------------------------------------------------------
  var btn_savecomplemento=1;
	$('#btn_savecomplemento').click(function(event) {
    btn_savecomplemento=1;
    var TABLA0   = $("#tabledocumentosrelacionados tbody > tr");
    TABLA0.each(function(){ 
      var idfactura = $(this).find("input[id*='idfactura']").val();
      //verificarcompletado(idfactura);
        $.ajax({
          type:'POST',
          url: base_url+"Facturaslis/documentoadd",
          data: {
            factura:idfactura
          },
          success: function (data){
            var array = $.parseJSON(data);
            if (array.ImpSaldoAnt>0) {
            }else{
              toastr["error"]("Uno o mas documentos se encuentra cubiertos", "Alerta!");
              $('.documento_add_'+array.idfactura+'').addClass('intermitente');
              btn_savecomplemento=0;
            }
            console.log('entra verificar saldo');
          },
          error: function() {
             
          }
        });
    });
    $( "#btn_savecomplemento" ).prop( "disabled", true );
    setTimeout(function(){ 
         $("#btn_savecomplemento" ).prop( "disabled", false );
    }, 10000);
    
    var clientetext=$('#razonsocialreceptor').val();
    var clientetextid=$('#idclienteregresa').val();
		var form = $('#validateSubmitForm');
		if (form.valid()) {
      $('body').loading({theme: 'dark',message: 'Timbrando Complemento...'});
      //================================================
      var DATAr  = [];
      var TABLA   = $("#tabledocumentosrelacionados tbody > tr");  
            TABLA.each(function(){         
            item = {};
            
            item ["idfactura"] = $(this).find("input[id*='idfactura']").val();
            item ["IdDocumento"]   = $(this).find("input[id*='IdDocumento']").val();
            item ["NumParcialidad"]  = $(this).find("input[id*='NumParcialidad']").val();
            item ["ImpSaldoAnt"]  = $(this).find("input[id*='ImpSaldoAnt']").val();
            item ["ImpPagado"]  = $(this).find("input[id*='ImpPagado']").val();
            item ["ImpSaldoInsoluto"]  = $(this).find("input[id*='ImpSaldoInsoluto']").val();
            item ["MetodoDePagoDR"]  = $(this).find("input[id*='MetodoDePagoDR']").val();
            DATAr.push(item);

            //verificarcompletado(idfactura);
        });
        //INFO  = new FormData();
        arraydocumento   = JSON.stringify(DATAr);
        var f_r = $('#facturarelacionada').is(':checked')==true?1:0;
        var f_r_t = $('#TipoRelacion option:selected').val();
        var f_r_uuid = $('#uuid_r').val();
        setTimeout(function(){ 
          if(btn_savecomplemento==1){
            console.log('entra procesar');
      			$.ajax({
      		        type:'POST',
      		        url: base_url+"Configuracionrentas/addcomplemento",
      		        data: form.serialize()+'&arraydocumento='+arraydocumento+'&f_r='+f_r+'&f_r_t='+f_r_t+'&f_r_uuid='+f_r_uuid,
      		        success: function (response){
      		        	 var array = $.parseJSON(response);
                     console.log(array);
                              if (array.resultado=='error') {
                                  
                                  swal({    title: "Error "+array.CodigoRespuesta+"!",
                                                text: array.MensajeError,
                                                type: "warning",
                                                showCancelButton: false
                                   });
                                  $('body').loading('stop');
                                  //retimbrar(array.facturaId,0)
                              }else{
                                  swal("Éxito!", "Se ha creado la factura", "success")
                              }
                              setTimeout(function(){ 
                                  window.location.href = base_url+"index.php/Facturaslis/?cliente="+clientetext+"&clienteid="+clientetextid; 
                              }, 9000);
      		        	
      		        }
      		    });
          }else{
            toastr["error"]("Uno de los documentos se encuentra Cubierto", "Alerta!");
            $('body').loading('stop');
          }
        }, 3000);
      //=================================================================================
		}else{
			swal({    title: "Error!",
                              text: 'Completa los campos requeridos',
                              type: "warning",
                              showCancelButton: false
                 });
		}
	});
  $('#btn_savecomplemento_previe').click(function(event) {
    btn_savecomplemento_preview();
  });
  $('#facturarelacionada').click(function(event) {
        if($('#facturarelacionada').is(':checked')){
            $('.divfacturarelacionada').show('show');
            $('#ImpSaldoAnt').prop('readonly',false);
        }else{
            $('.divfacturarelacionada').hide('show');
            $('#uuid_r').val('');
            $('#ImpSaldoAnt').prop('readonly',true);
        }
        /* Act on the event */
  });
});

/*
$.ajax({
        type:'POST',
        url: base_url+"Facturaslis/obtenerventasproductos",
        data: {
        	venta:id,
        	tipo: tipo
        },
        success: function (data){
        	console.log(data); 
        	var array = $.parseJSON(data);
        	$.each(array, function(index, item) {
        		if (parseFloat(item.iva)>0) {
        			var aiva=1;
        		}else{
        			var aiva=0;
        		}
        		agregarconceptovalor(item.cantidad,'','','','',item.producto,item.preciou,aiva);
        	});
        	funcionconceptos();
        }
    });
*/
function calcularsinsoluto(factura){
  var impsa=$('.ImpSaldoAnt_'+factura).val();
  var impp=$('.ImpPagado_'+factura).val();
  var impsi=parseFloat(impsa)-parseFloat(impp);
  $('.ImpSaldoInsoluto_'+factura).val(impsi.toFixed(2));

  var addtp = 0;
  $(".ImpPagado").each(function() {
        var vstotal = $(this).val();
        addtp += Number(vstotal);
    });
  var valor =parseFloat(addtp.toFixed(2));
  console.log(valor);
  $('#totalimportes').val(valor);
  $('.totalimport').html(valor);

}
function adddocumento(cliente,fac){
  var serie=$('#Serie').val();
  $.ajax({
        type:'POST',
        url: base_url+"Facturaslis/obtenerfacturas",
        data: {
          cli:cliente,
          fact:fac,
          serie:serie
        },
        success: function (data){
          $('.modal').modal();
          $('#modaldocumentos').modal('open');
          $('.listadodocumentos').html(data);
          $('#facturascli').DataTable({
            "order": [[ 0, "desc" ]]
          });
        }
    });
}
function adddoc(id){
  $.ajax({
        type:'POST',
        url: base_url+"Facturaslis/documentoadd",
        data: {
          factura:id
        },
        success: function (data){
          var array = $.parseJSON(data);
          if (array.ImpSaldoAnt>0) {
            $('.documento_add_'+array.idfactura).remove();
            //si se hace una modificacion a esta escructura html se tiene que modificar igual la funcion refacturacion_comp
              var html ='<tr class="documento_add_'+array.idfactura+'">\
                          <td>\
                            <div class="col s3">\
                              <input type="hidden" id="idfactura" class="form-control-bmz" value="'+array.idfactura+'"  readonly required>\
                              <input type="hidden" id="MetodoDePagoDR" class="form-control-bmz" value="'+array.MetodoDePagoDR+'"  readonly required>\
                             <label>Id del documento:</label>\
                             <input type="text" id="IdDocumento" class="form-control-bmz IdDocumentos" value="'+array.IdDocumento+'" readonly required>\
                            </div>\
                            <div class="col s2">\
                             <label>Número de parcialidad:</label>\
                             <input type="text" id="NumParcialidad" class="form-control-bmz" value="'+array.NumParcialidad+'"  required>\
                            </div>\
                            <div class="col s2">\
                             <label>Importe de saldo anterior:</label>\
                             <input type="text" id="ImpSaldoAnt" class="form-control-bmz ImpSaldoAnt_'+array.idfactura+'" value="'+array.ImpSaldoAnt+'" readonly required>\
                            </div>\
                            <div class="col s2">\
                             <label>Importe de Pagado:</label>\
                             <input type="text" id="ImpPagado" class="form-control-bmz ImpPagado ImpPagado_'+array.idfactura+'" value="0" oninput="calcularsinsoluto('+array.idfactura+')">\
                            </div>\
                            <div class="col s2">\
                             <label>Importe de Pagado:</label>\
                             <input type="text" id="ImpSaldoInsoluto" class="form-control-bmz ImpSaldoInsoluto_'+array.idfactura+'" value="'+array.ImpSaldoAnt+'" readonly>\
                            </div>\
                            <div class="col s1">\
                              <a class="btn-bmz red" onclick="deletedoc('+array.idfactura+')"><i class="material-icons">delete_forever</i></a>\
                            </div>\
                          </td>\
                        </tr>';
              $('.tabletbodydr').append(html);
          }else{
            toastr["error"]("Documento Cubierto", "Alerta!");
          }
        }
    });
}
function verificarcompletado(id){
   $.ajax({
        type:'POST',
        url: base_url+"Facturaslis/documentoadd",
        data: {
          factura:id
        },
        success: function (data){
          var array = $.parseJSON(data);
          if (array.ImpSaldoAnt>0) {
          }else{
            toastr["error"]("Uno o mas documentos se encuentra cubiertos", "Alerta!");
            $('.documento_add_'+array.idfactura+'').addClass('intermitente');
            btn_savecomplemento=0;
          }
        },
        error: function() {
           
        }
    });
}
function deletedoc(id){
  $('.documento_add_'+id).remove();
}
function btn_savecomplemento_preview(){
    var form = $('#validateSubmitForm');
    if (form.valid()) {
      //================================================
      var DATAr  = [];
        var TABLA   = $("#tabledocumentosrelacionados tbody > tr");
            TABLA.each(function(){         
            item = {};
            item ["idfactura"] = $(this).find("input[id*='idfactura']").val();
            item ["IdDocumento"]   = $(this).find("input[id*='IdDocumento']").val();
            item ["NumParcialidad"]  = $(this).find("input[id*='NumParcialidad']").val();
            item ["ImpSaldoAnt"]  = $(this).find("input[id*='ImpSaldoAnt']").val();
            item ["ImpPagado"]  = $(this).find("input[id*='ImpPagado']").val();
            item ["ImpSaldoInsoluto"]  = $(this).find("input[id*='ImpSaldoInsoluto']").val();
            item ["MetodoDePagoDR"]  = $(this).find("input[id*='MetodoDePagoDR']").val();
            DATAr.push(item);
        });
        INFO  = new FormData();
        arraydocumento   = JSON.stringify(DATAr);
      
      //===
        var datos =form.serialize()+'&arraydocumento='+arraydocumento;
        $('#modal_previefactura').modal({
            dismissible: false
        });
        $('#modal_previefactura').modal('open');
        setTimeout(function(){ 
            var urlfac=base_url+"index.php/Preview/complementodoc?"+datos;
            var htmliframe='<iframe src="'+urlfac+'" title="description" class="ifrafac">';
            $('.preview_iframe').html(htmliframe);
            //window.location.href = base_url+"index.php/Preview/factura?"+datos; 
        }, 1000);
      //=================================================================================
    }else{
      swal({    title: "Error!",
                              text: 'Completa los campos requeridos',
                              type: "warning",
                              showCancelButton: false
                 });
    }
}
toastr.options = {
  "closeButton": true,
  "debug": false,
  "newestOnTop": false,
  "progressBar": true,
  "positionClass": "toast-top-right",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "4000",
  "timeOut": "5000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}
function refacturacion_comp(id){
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Facturaslis/infocomplemento",
        data: {
            idc:id
        },
        success: function (response){
            var array = $.parseJSON(response); 
            console.log(array);   
            $('#Fechatimbre').val(array.comp.FechaPago);  
            $('#FormaDePagoP').val(array.comp.FormaDePagoP);
            $('#Monto').val(array.comp.Monto);
            $('#totalimportes').val(array.comp.Monto);
            $('#NumOperacion').val(array.comp.NumOperacion);
            $('#CtaBeneficiario').val(array.comp.CtaBeneficiario);
            $('.tabletbodydr').html('');
            array.compd.forEach(function(element) {
                //si se hace una modificacion a esta escructura html se tiene que modificar igual la funcion adddoc(id)
              var html ='<tr class="documento_add_'+element.facturasId+'">\
                          <td>\
                            <div class="col s3">\
                              <input type="hidden" id="idfactura" class="form-control-bmz" value="'+element.facturasId+'"  readonly required>\
                              <input type="hidden" id="MetodoDePagoDR" class="form-control-bmz" value="'+element.MetodoDePagoDR+'"  readonly required>\
                             <label>Id del documento:</label>\
                             <input type="text" id="IdDocumento" class="form-control-bmz IdDocumentos" value="'+element.IdDocumento+'" readonly required>\
                            </div>\
                            <div class="col s2">\
                             <label>Número de parcialidad:</label>\
                             <input type="text" id="NumParcialidad" class="form-control-bmz" value="'+element.NumParcialidad+'"  required>\
                            </div>\
                            <div class="col s2">\
                             <label>Importe de saldo anterior:</label>\
                             <input type="text" id="ImpSaldoAnt" class="form-control-bmz ImpSaldoAnt_'+element.facturasId+'" value="'+element.ImpSaldoAnt+'" readonly required>\
                            </div>\
                            <div class="col s2">\
                             <label>Importe de Pagado:</label>\
                             <input type="text" id="ImpPagado" class="form-control-bmz ImpPagado ImpPagado_'+element.facturasId+'" value="'+element.ImpPagado+'" oninput="calcularsinsoluto('+element.facturasId+')">\
                            </div>\
                            <div class="col s2">\
                             <label>Importe de Pagado:</label>\
                             <input type="text" id="ImpSaldoInsoluto" class="form-control-bmz ImpSaldoInsoluto_'+element.facturasId+'" value="'+element.ImpSaldoInsoluto+'" readonly>\
                            </div>\
                            <div class="col s1">\
                              <a class="btn-bmz red" onclick="deletedoc('+element.facturasId+')"><i class="material-icons">delete_forever</i></a>\
                            </div>\
                          </td>\
                        </tr>';
              $('.tabletbodydr').append(html);
            });
            
        },
        error: function(response){
            notificacion(1);
        }
    });
}