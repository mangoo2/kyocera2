<?php
require_once dirname(__FILE__) . '/TCPDF4/examples/tcpdf_include.php'; 
require_once dirname(__FILE__) . '/TCPDF4/tcpdf.php';
$actualizardatos=1;//0 no actualiza 1 actualiza
$GLOBALS["Folio"]=$Folio;
$GLOBALS["nombreclienter"]=$nombreclienter;

$GLOBALS["periodo"]=$periodo;
$GLOBALS["periodoante"]=$periodoante;
if($idcondicionextra>0){
    $condiciones = $this->ModeloCatalogos->getselectwheren('rentas_condiciones',array('id'=>$idcondicionextra));
    foreach ($condiciones->result() as $itemrc) {
      $tiporenta = $itemrc->tipo;
    }
}
//====================================================
  class MYPDF extends TCPDF {
    //Page header
    public function Header() {
        $logos = base_url().'public/img/alta.png';
        $logosmds = base_url().'public/img/MDS.png';
        
          $tamano1='30';//30
          $tamano2='40';//40
          $tamano3='30';//30
        $html = '
            <style type="text/css">
              .info_fac{
                font-size: 9px;
              }
              .info_facd{
                font-size: 8px;
              }
              .httablelinea{
                vertical-align: center;
                border-bottom: 1px solid #9e9e9e;
              }
              
              .httableleft{
                border-left: 1px solid #9e9e9e;
              }
              .httableright{
                border-right: 1px solid #9e9e9e;
              }
              .httabletop{
                border-top: 1px solid #9e9e9e;
              }
            </style>
            <table width="100%" border="0" cellpadding="4px" class="info_fac">
              <tr>
                <td rowspan="6" width="25%"><img src="'.$logos.'"></td>
                <td width="50%" valign="top"></td>
                <td rowspan="6" width="25%"><img src="'.$logosmds.'"></td>
              </tr>
              <tr>
                <td align="center" style="font-size: 12px;"><b>'.$GLOBALS["nombreclienter"].'</b></td>
              </tr>
              <tr>
                <td align="center" style="font-size: 12px;"><b>RENTA '.$GLOBALS["periodo"].'.</b></td>
              </tr>
              <tr>
                <td></td>
              </tr>
              <tr>
                <td align="center" style="font-size: 12px;"><b>DETALLE DE PRODUCCION:</b></td>
              </tr>
              <tr>
                <td align="center" style="font-size: 12px;"><b>DEL '.$GLOBALS["periodoante"].'.</b></td>
              </tr>
            </table>

            ';
          $this->writeHTML($html, true, false, true, false, '');
    }
      // Page footer
    public function Footer() {
      $html2='';
      $styleQR = array('border' => 0, 
           'vpadding' => '0', 
           'hpadding' => '0', 
           'fgcolor' => array(0, 0, 0), 
           'bgcolor' => false, 
           'module_width' => 1, 
           'module_height' => 1);
      $html='';

      $html2 .= ' 
            <style type="text/css">
                .fontFooter10{
                  font-size: 10px;
                }
                .fontFooterp{
                  font-size: 9px;
                  margin-top:0px;
                }
                .fontFooter{
                  font-size: 9px;
                  margin-top:0px;
                }

                .fontFooterp{
                  font-size: 8px;
                }
                .fontFooterpt{
                  font-size: 7px;
                }
                .fontFooterpt6{
                  font-size: 6px;
                }
                p{
                  margin:0px;
                }
                .valign{
                  vertical-align:middle;
                }
                .httablelinea{
                  vertical-align: center;
                  border-bottom: 1px solid #9e9e9e;
                }
                .footerpage{
                  font-size: 9px;
                  color: #9e9e9e;
                }
            </style>';

      $html2 .= '
        <table width="100%" border="0" cellpadding="2" class="fontFooterp">
          <tr>
            <td align="right" class="footerpage">
            Pagina '.$this->getAliasNumPage().' de '.$this->getAliasNbPages().'</td>
          </tr>
        </table>
      ';
        $this->writeHTML($html2, true, false, true, false, '');
        
    }
  } 
  $pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

  // set document information
  $pdf->SetCreator(PDF_CREATOR);
  $pdf->SetAuthor('Kyocera');
  $pdf->SetTitle('Folio_'.$GLOBALS["Folio"]);
  $pdf->SetSubject('factura');
  $pdf->SetKeywords('factura');

  // set default header data
  $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

  // set header and footer fonts
  $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
  $pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

  // set default monospaced font
  $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

  // set margins
  $pdf->SetMargins('7', '45', '7');
  $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
  $pdf->SetFooterMargin('15');

  // set auto page breaks
  $pdf->SetAutoPageBreak(true, 16);

  // set image scale factor
  $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

  $pdf->SetFont('dejavusans', '', 7);

//====================================================
//$pdf->setPrintHeader(false);
//$pdf->setPrintFooter(false);
//$pdf->SetMargins('10', '10', '10');
//$pdf->SetFooterMargin('10');
if ($tiporenta==2) {
  $pdf->AddPage('L', 'A4');
}else{
  $pdf->AddPage('P', 'A4');
}

  $html='
          <table border="1">
            <tr>
              <td  width="10%"  align="center">Contrato</td>
              <td  width="10%" align="center">'.$foliocontrato.'</td>
              <td  width="60%" ></td>
              <td  width="10%" align="center"><b>FACTURA</b></td>
              <td width="10%" ></td>
            </tr>
            
          </table>';
if ($tiporenta==2) {
  //===============
    $valor_g_subtotal=0;
    $valor_g_subtotalcolor=0;
    $valor_g_excedente_m=0;
    $valor_g_excedente_c=0;
  //===============
  if($idcondicionextra>0){
    $condiciones = $this->ModeloCatalogos->getselectwheren('rentas_condiciones',array('id'=>$idcondicionextra));
    foreach ($condiciones->result() as $itemrc) {
      $grentadeposito = $itemrc->renta_m;
      $grentacolor = $itemrc->renta_c;
      $gclicks_mono = $itemrc->click_m;
      $gprecio_c_e_mono = $itemrc->excedente_m;
      $gclicks_color = $itemrc->click_c;
      $gprecio_c_e_color = $itemrc->excedente_c;
    }
  }
  $descuentosg=$descuentogclisck+$descuentogscaneo;

  $valor_g_subtotal=$grentadeposito;
  $valor_g_subtotalcolor=$grentacolor;

  $html.='<style type="text/css">
            .httable{
              font-size: 8px;
            }
          </style>
          <table border="1" cellpadding="3">
            <tr>
              <td align="right" style="background-color: #d8d4d4; color: black">RENTA MENSUAL</td>
              <td  >$ '.number_format($grentadeposito,2,'.',',').'</td>
              <td align="right" style="background-color: #d8d4d4; color: black">RENTA MENSUAL</td>
              <td  >$ '.number_format($grentacolor,2,'.',',').'</td>
            </tr>
            <tr>
              <td colspan="4" align="center" >VOLUMEN INCLUIDO</td>
            </tr>
            <tr>
              <td colspan="2" style="background-color: #d8d4d4; color: black" align="center">MONOCROMÁTICA</td>
              <td colspan="2" style="background-color: #d8d4d4; color: black" align="center">COLOR</td>
            </tr>
            <tr style="background-color: #d8d4d4; color: black">
              <td align="center">CLICKS</td>
              <td align="center">EXCEDENTES</td>
              <td align="center">CLICKS</td>
              <td align="center">EXCEDENTES</td>
            </tr>
            <tr>
              <td align="center">'.$gclicks_mono.'</td>
              <td align="center">$ '.number_format($gprecio_c_e_mono,3,'.',',').'</td>
              <td align="center">'.$gclicks_color.'</td>
              <td align="center">$ '.number_format($gprecio_c_e_color,3,'.',',').'</td>
            </tr>
        </table><table><tr><th></th></tr></table>';
        if($descuentosg>0){
          $html.='<table border="1" cellpadding="3" align="center">
                      <tr>
                          <td style="background-color: #d8d4d4; color: black">Descuento Clicks</td>
                          <td>'.$descuentogclisck.'</td>
                          <td style="background-color: #d8d4d4; color: black">Descuento Escaneo</td>
                          <td>'.$descuentogscaneo.'</td>
                      </tr>
                    </table>
                    <table><tr><th></th></tr></table>';
        }

          
    $contadoresmono=0;
    $contadorescolor=0;
    foreach ($detalleperido->result() as $item) {
        if($item->tipo==1){
          $contadoresmono++;
        }
    }
    $haydespueto=0;$haydespuetoe=0;
    foreach ($detalleperido->result() as $item) {
        if($item->tipo==2){
          $contadorescolor++;
        }
        if($item->descuento>0){
          $haydespueto=1;
        }
        if($item->descuentoe>0){
          $haydespuetoe=1;
        }

    }
    if ($contadoresmono>0) {
      $html.='<table border="1" align="center" cellpadding="2">
              <thead> 
                <tr valign="middle" style="background-color: #d8d4d4; color: black">
                  <th class="httable" colspan="2"><b>EQUIPO</b></th>
                  <th class="httable" colspan="3" ><b>PRODUCCIÓN COPIA/IMPRESIÓN MONO</b></th>
                  <th class="httable" colspan="3" ><b>PRODUCCIÓN ESCANEO</b></th>';
                  if($haydespueto==1){
                    $html.='<th class="httable" rowspan="2"><b>DESCUENTO CLICKS</b></th>';  
                  }
                  if($haydespuetoe==1){
                    $html.='<th class="httable" rowspan="2"><b>DESCUENTO ESCANEO</b></th>';  
                  }
                  $html.='<th class="httable" rowspan="2"><b>PRODUCCIÓN EQUIPO</b></th>
                </tr>
                <tr valign="middle" style="background-color: #d8d4d4; color: black">
                  <th class="httable"><b>MODELO</b></th>
                  <th class="httable"><b>SERIE</b></th>
                  <th class="httable"><b>CONTADOR INICIAL</b></th>
                  <th class="httable"><b>CONTADOR FINAL</b></th>
                  <th class="httable"><b>PRODUCCIÓN</b></th>
                  <th class="httable"><b>CONTADOR INCIAL</b></th>
                  <th class="httable"><b>CONTADOR FINAL</b></th>
                  <th class="httable"><b>PRODUCCIÓN</b></th>
                </tr>
              </thead>
            ';
            $producciontotal=0;
            $trrowcol=$detalleperido->num_rows();
            foreach ($detalleperido->result() as $item) {
              if($item->tipo==1){
                if($item->usado==1){$vusado='-mc';}else{$vusado='';}
                $html.=' 
                        <tr valign="middle">
                          <td class="httable">'.$item->modelo.$vusado.'</td>
                          <td class="httable">'.$item->serie.'</td>
                          <td class="httable">'.$item->c_c_i.'</td>
                          <td class="httable">'.$item->c_c_f.'</td>
                          <td class="httable">'.$tc=$item->c_c_f-$item->c_c_i.'</td>
                          <td class="httable">'.$item->e_c_i.'</td>
                          <td class="httable">'.$item->e_c_f.'</td>
                          <td class="httable">'.$tc=$item->e_c_f-$item->e_c_i.'</td>';
                          if($haydespueto==1){
                            $html.='<td class="httable">'.$item->descuento.'</td>';
                          }
                          if($haydespuetoe==1){
                            $html.='<td class="httable">'.$item->descuentoe.'</td>';
                          }
                          $html.='<td class="httable">'.$item->produccion.'</td>  
                        </tr>
                      
                    ';
                    $producciontotal=$producciontotal+$item->produccion;
              }
            }
            if($descuentosg>0){
              $producciontotal=$producciontotal-$descuentosg; 
            }
            $volumenexcenetente=$producciontotal-$gclicks_mono;
            if ($volumenexcenetente<0) {
              $volumenexcenetente=0;
            }
            $totalexcedente=$volumenexcenetente*$gprecio_c_e_mono;
            $subtotal=$totalexcedente+$grentadeposito;
            $iva=$subtotal*0.16;
            $total=$subtotal+$iva;
            $valor_g_excedente_m=$totalexcedente;
            $this->ModeloCatalogos->updateCatalogo('alta_rentas_prefactura',array('excedente'=>$totalexcedente),array('prefId'=>$Folio));
              $colspan=8;
              if($haydespueto==1){
                $colspan=$colspan+1;
              }
              if($haydespuetoe==1){
                $colspan=$colspan+1;
              }
              $html.='
                      <tr valign="middle">
                          <td class="httable" colspan="'.$colspan.'" align="right">Produccion Total</td>
                          <td class="httable">'.$producciontotal.'</td>
                          <td class="httable"></td>
                        </tr>

              </table><table><tr><th></th></tr></table>
              <table border="1" align="center" cellpadding="2">
                <tr>
                  <th>VOLUMEN EXCEDENTE</th>
                  <th>COSTO EXCEDENTE</th>
                  <th>TOTAL EXCEDENTE</th>
                  <th>RENTA FIJA</th>
                  <th>SUBTOTAL</th>
                  <th>IVA</th>
                  <th style="background-color: #d8d4d4; color: black">TOTAL</th>
                </tr>
                <tr>
                  <td>'.$volumenexcenetente.'</td>
                  <td>$ '.number_format($gprecio_c_e_mono,3,'.',',').'</td>
                  <td>$ '.number_format($totalexcedente,2,'.',',').'</td>
                  <td>$ '.number_format($grentadeposito,2,'.',',').'</td>
                  <td>$ '.number_format($subtotal,2,'.',',').'</td>
                  <td>$ '.number_format($iva,2,'.',',').'</td>
                  <td style="background-color: #d8d4d4; color: black">$ '.number_format($total,2,'.',',').'</td>
                </tr>
              </table>';
              $this->ModeloCatalogos->updateCatalogo('alta_rentas_prefactura',array('cant_ext_mono'=>$volumenexcenetente,'prec_ext_mono'=>$gprecio_c_e_mono),array('prefId'=>$Folio));
    }
    if ($contadorescolor>0) {
      $html.='<table><tr><th></th></tr></table>
            <table border="1" align="center" cellpadding="2">
              <thead> 
                <tr valign="middle" style="background-color: #d8d4d4; color: black">
                  <th class="httable" colspan="2"><b>EQUIPO</b></th>
                  <th class="httable" colspan="3" ><b>PRODUCCIÓN COPIA/IMPRESIÓN COLOR</b></th>
                  <th class="httable" colspan="3" ><b>PRODUCCIÓN ESCANEO</b></th>';
                  if($haydespueto==1){
                    $html.='<th class="httable" rowspan="2"><b>DESCUENTO</b></th>';
                  }
                  $html.='<th class="httable" rowspan="2"><b>PRODUCCIÓN EQUIPO</b></th>
                </tr>
                <tr valign="middle" style="background-color: #d8d4d4; color: black">
                  <th class="httable"><b>MODELO</b></th>
                  <th class="httable"><b>SERIE</b></th>
                  <th class="httable"><b>CONTADOR INICIAL</b></th>
                  <th class="httable"><b>CONTADOR FINAL</b></th>
                  <th class="httable"><b>PRODUCCIÓN</b></th>
                  <th class="httable"><b>CONTADOR INCIAL</b></th>
                  <th class="httable"><b>CONTADOR FINAL</b></th>
                  <th class="httable"><b>PRODUCCIÓN</b></th>
                </tr>
              </thead>
            ';
            $producciontotal=0;
            $trrowcol=$detalleperido->num_rows();
            foreach ($detalleperido->result() as $item) {
              if($item->tipo==2){
                if($item->usado==1){$vusado='-mc';}else{$vusado='';}
                $html.=' 
                        <tr valign="middle">
                          <td class="httable">'.$item->modelo.$vusado.'</td>
                          <td class="httable">'.$item->serie.'</td>
                          <td class="httable">'.$item->c_c_i.'</td>
                          <td class="httable">'.$item->c_c_f.'</td>
                          <td class="httable">'.$tc=$item->c_c_f-$item->c_c_i.'</td>
                          <td class="httable">'.$item->e_c_i.'</td>
                          <td class="httable">'.$item->e_c_f.'</td>
                          <td class="httable">'.$tc=$item->e_c_f-$item->e_c_i.'</td>';
                          if($haydespueto==1){
                            $html.='<td class="httable">'.$item->descuento.'</td> ';
                          }
                          if($haydespuetoe==1){
                            $html.='<td class="httable">'.$item->descuentoe.'</td> ';
                          }
                          $html.='<td class="httable">'.$item->produccion.'</td>  
                        </tr>
                      
                    ';
                    $producciontotal=$producciontotal+$item->produccion;
              }
            }
            
            $volumenexcenetente=$producciontotal-$gclicks_color;
            if ($volumenexcenetente<0) {
              $volumenexcenetente=0;
            }
            $totalexcedente=$volumenexcenetente*$gprecio_c_e_color;
            $subtotal=$totalexcedente+$grentacolor;
            $iva=$subtotal*0.16;
            $total=$subtotal+$iva;
            $valor_g_excedente_c=$totalexcedente;
            $this->ModeloCatalogos->updateCatalogo('alta_rentas_prefactura',array('excedentecolor'=>$totalexcedente),array('prefId'=>$Folio));
              $colspan=7;
              if($haydespueto==1){
                $colspan=$colspan+1;
              }
              if($haydespuetoe==1){
                $colspan=$colspan+1;
              }
              $html.='
                      <tr valign="middle">
                          <td class="httable" colspan="'.$colspan.'" align="right">Produccion Total</td>
                          <td class="httable">'.$producciontotal.'</td>
                          <td class="httable"></td>
                        </tr>

              </table><table><tr><th></th></tr></table>
              <table border="1" align="center" cellpadding="2">
                <tr>
                  <th>VOLUMEN EXCEDENTE</th>
                  <th>COSTO EXCEDENTE</th>
                  <th>TOTAL EXCEDENTE</th>
                  <th>RENTA FIJA</th>
                  <th>SUBTOTAL</th>
                  <th>IVA</th>
                  <th style="background-color: #d8d4d4; color: black">TOTAL</th>
                </tr>
                <tr>
                  <td>'.$volumenexcenetente.'</td>
                  <td>$ '.number_format($gprecio_c_e_color,3,'.',',').'</td>
                  <td>$ '.number_format($totalexcedente,2,'.',',').'</td>
                  <td>$ '.number_format($grentacolor,2,'.',',').'</td>
                  <td>$ '.number_format($subtotal,2,'.',',').'</td>
                  <td>$ '.number_format($iva,2,'.',',').'</td>
                  <td style="background-color: #d8d4d4; color: black">$ '.number_format($total,2,'.',',').'</td>
                </tr>
              </table>
              ';
              $this->ModeloCatalogos->updateCatalogo('alta_rentas_prefactura',array('cant_ext_color'=>$volumenexcenetente,'prec_ext_color'=>$gprecio_c_e_color),array('prefId'=>$Folio));
    }
           $valor_g_total=$valor_g_subtotal+$valor_g_subtotalcolor+$valor_g_excedente_m+$valor_g_excedente_c;
    //log_message('error','contrato:'.$idcontrato.' periodo:'.$Folio.' Renta m: '.$valor_g_subtotal.' renta c: '.$valor_g_subtotalcolor.' exmono:'.$valor_g_excedente_m.' excolor:'.$valor_g_excedente_c.' Total:'.$valor_g_total);
    $array_data_u=array(
                        'subtotal'=>$valor_g_subtotal,
                        'subtotalcolor'=>$valor_g_subtotalcolor,
                        'excedente'=>$valor_g_excedente_m,
                        'excedentecolor'=>$valor_g_excedente_c,
                        'total'=>$valor_g_total
                      );
    if($actualizardatos==1){
      $this->ModeloCatalogos->updateCatalogo('alta_rentas_prefactura',$array_data_u,array('prefId'=>$Folio));
    }
}else{
  //===============
    $valor_g_subtotal=0;
    $valor_g_subtotalcolor=0;
    $valor_g_excedente_m=0;
    $valor_g_excedente_c=0;
  //===============
  $haydespueto=0;
  $haydespuetoe=0;
        $html.='<style type="text/css">
            .httable{
              font-size: 7.5px;
            }
          </style>';
            foreach ($detalleperido->result() as $item) {
              if($item->descuento>0){
                $haydespueto=1;
              }
              if($item->descuentoe>0){
                $haydespuetoe=1;
              }
            }
            foreach ($detalleperido->result() as $item) {

              if($item->usado==1){$vusado='-mc';}else{$vusado='';}
              if($item->tipo==2){
                $title='COLOR';
                $volumenincluido =  $item->clicks_color;
                $costorenta=round($item->precio_rc,2);
                $costoexcedente  =  round($item->precio_c_e_color,2);
                if($idcondicionextra>0){
                  $condicionesdll = $this->ModeloCatalogos->getselectwheren('rentas_condiciones_detalle',array('idc'=>$idcondicionextra,'equiporow'=>$item->id,'serieId'=>$item->serieId));
                  foreach ($condicionesdll->result() as $itemcdll) {
                    $volumenincluido = $itemcdll->click_c;
                    $costorenta = $itemcdll->renta_c;
                    $costoexcedente = $itemcdll->excedente_c;
                  }
                }
                $valor_g_subtotalcolor=$valor_g_subtotalcolor+$costorenta;
              }else{
                $title='MONO';
                $volumenincluido =  $item->clicks_mono;
                $costorenta=round($item->precio_r,2);
                $costoexcedente  =  round($item->precio_c_e_mono,3);
                if($idcondicionextra>0){
                  $condicionesdll = $this->ModeloCatalogos->getselectwheren('rentas_condiciones_detalle',array('idc'=>$idcondicionextra,'equiporow'=>$item->id,'serieId'=>$item->serieId));
                  foreach ($condicionesdll->result() as $itemcdll) {
                    $volumenincluido = $itemcdll->click_m;
                    $costorenta = $itemcdll->renta_m;
                    $costoexcedente = $itemcdll->excedente_m;
                  }
                }
                $valor_g_subtotal=$valor_g_subtotal+$costorenta;
              }
              $html.='<table border="1" align="center" cellpadding="2">
              <thead> 
                <tr valign="middle" style="background-color: #d8d4d4; color: black">
                  <th class="httable" colspan="2"><b>EQUIPO</b></th>
                  <th class="httable" colspan="3" ><b>PRODUCCIÓN COPIA/IMPRESIÓN '.$title.'</b></th>
                  <th class="httable" colspan="3" ><b>PRODUCCIÓN ESCANEO</b></th>';
                  if($haydespueto==1){
                    $html.='<th class="httable" rowspan="2"><b>DESCUENTO CLICKS</b></th>';  
                  }
                  if($haydespuetoe==1){
                    $html.='<th class="httable" rowspan="2"><b>DESCUENTO ESCANEO</b></th>';  
                  }
                  
                  $html.='<th class="httable" rowspan="2"><b>PRODUCCIÓN EQUIPO</b></th>
                  <th class="httable" rowspan="2"><b>VOLUMEN INCLUIDO</b></th>
                </tr>
                <tr valign="middle" style="background-color: #d8d4d4; color: black">
                  <th class="httable"><b>MODELO</b></th>
                  <th class="httable"><b>SERIE</b></th>
                  <th class="httable"><b>CONTADOR INICIAL</b></th>
                  <th class="httable"><b>CONTADOR FINAL</b></th>
                  <th class="httable"><b>PRODUCCIÓN</b></th>
                  <th class="httable"><b>CONTADOR INCIAL</b></th>
                  <th class="httable"><b>CONTADOR FINAL</b></th>
                  <th class="httable"><b>PRODUCCIÓN</b></th>
                </tr>
              </thead>
            ';
                $excedente=$item->produccion-$volumenincluido;
                if ($excedente<0) {
                  $excedente=0;
                }
                if($item->tipo==2){
                  $valor_g_excedente_c=$valor_g_excedente_c+($excedente*$costoexcedente);
                }else{
                  $valor_g_excedente_m=$valor_g_excedente_m+($excedente*$costoexcedente);
                }
                $totalexcedente=($excedente*$costoexcedente);
                $subtotal=$costorenta+$totalexcedente;
                $subtotaliva=$subtotal*0.16;
                $totalgeneral=round($subtotal+$subtotaliva,2);
                $html.=' 
                        <tr valign="middle">
                          <td class="httable">'.$item->modelo.$vusado.'</td>
                          <td class="httable">'.$item->serie.'</td>
                          <td class="httable">'.$item->c_c_i.'</td>
                          <td class="httable">'.$item->c_c_f.'</td>
                          <td class="httable">'.$tc=$item->c_c_f-$item->c_c_i.'</td>
                          <td class="httable">'.$item->e_c_i.'</td>
                          <td class="httable">'.$item->e_c_f.'</td>
                          <td class="httable">'.$tc=$item->e_c_f-$item->e_c_i.'</td>';
                          if($haydespueto==1){
                            $html.='<td class="httable">'.$item->descuento.'</td>'; 
                            $colspan=2;
                          }else{
                            $colspan=1;
                          }
                          if($haydespuetoe==1){
                            $html.='<td class="httable">'.$item->descuentoe.'</td>'; 
                            $colspane=2;
                          }else{
                            $colspane=1;
                          }
                          $html.='<td class="httable">'.$item->produccion.'</td>  
                          <td class="httable">'.$volumenincluido.'</td>  
                        </tr>
                        <tr>
                          <td colspan="10"> </td>
                        </tr>
                        <tr valign="middle">
                          <td colspan="2">VOLUMEN EXCEDENTE</td> 
                          <td colspan="2">COSTO EXCEDENTE</td>  
                          <td colspan="2">RENTA FIJA</td> 
                          <td colspan="2">SUBTOTAL</td>
                          <td colspan="'.$colspan.'">IVA</td>  
                          <td colspan="'.$colspane.'">TOTAL</td>  
                        </tr>
                        <tr valign="middle">
                          <td colspan="2">'.$excedente.'</td> 
                          <td colspan="2">$'.$costoexcedente.'</td>  
                          <td colspan="2">$'.$costorenta.'</td> 
                          <td colspan="2">$'.$subtotal.'</td>
                          <td colspan="'.$colspan.'">$'.$subtotaliva.'</td>  
                          <td colspan="'.$colspane.'">$'.$totalgeneral.'</td>   
                        </tr>

              </table><table><tr><th></th></tr></table>';
              $array_data_u_d=array(
                                  'costoexcedente'=>$costoexcedente,
                                  'totalexcedente'=>$totalexcedente
                                    );
              if($actualizardatos==1){
                $this->ModeloCatalogos->updateCatalogo('alta_rentas_prefactura_d',$array_data_u_d,array('prefdId'=>$item->prefdId));
              }
            }
           $valor_g_total=$valor_g_subtotal+$valor_g_subtotalcolor+$valor_g_excedente_m+$valor_g_excedente_c;
    //log_message('error','contrato:'.$idcontrato.' periodo:'.$Folio.' Renta m: '.$valor_g_subtotal.' renta c: '.$valor_g_subtotalcolor.' exmono:'.$valor_g_excedente_m.' excolor:'.$valor_g_excedente_c.' Total:'.$valor_g_total);
    $array_data_u=array(
                        'subtotal'=>$valor_g_subtotal,
                        'subtotalcolor'=>$valor_g_subtotalcolor,
                        'excedente'=>$valor_g_excedente_m,
                        'excedentecolor'=>$valor_g_excedente_c,
                        'total'=>$valor_g_total
                      );
    if($actualizardatos==1){
      $this->ModeloCatalogos->updateCatalogo('alta_rentas_prefactura',$array_data_u,array('prefId'=>$Folio));
    }
}
  

$pdf->writeHTML($html, true, false, true, false, '');
//=======================================================================
if($gel_fol==1){
    $html='<br><br><br><br>
            <table border="1" align="center" cellpadding="2" style="background-color: #d8d4d4; color: black">
              <tr valign="middle">
                <th class="httable" style="font-size:12px"><b>TONER</b></th>
              </tr>
            </table>';
    $html.='<table border="1" align="center" cellpadding="2">
            <thead> 
              <tr valign="middle" style="background-color: #d88888; color: black">
                <th class="httable" ><b>TONER</b></th>
                <th class="httable" ><b>FOLIO</b></th>
                <th class="httable"  ><b>ESTATUS</b></th>
                
              </tr>
            </thead>
          ';
  foreach ($detalleconsumiblesfolios->result() as $item) {
    if ($item->status==0) {
      $status='';
    }elseif ($item->status==1) {
      $status='stock Cliente';
    }else{
      $status='Retorno';
    }
      $html.='<tr valign="middle"><td class="httable">'.$item->modelo.'</td><td class="httable">'.$item->foliotext.'</td><td class="httable">'.$status.'</td></tr>';  
  }
   $html.='</table>';
  $pdf->writeHTML($html, true, false, true, false, '');
}
  foreach ($detallecimages->result() as $item) {
    $pdf->AddPage('P', 'A4');
    if($item->tipoimg==0){
      $file = fopen(base_url()."uploads/rentas_hojasestado/".$item->nombre, "r");
      $line = fgets($file);
      $html= $line;
      fclose($file);
    }else{
      $html=base_url()."uploads/rentas_hojasestado/".$item->nombre;
    }
    $pdf->Image($html, 'C', 0, 250, 297, '', '', '', true, 200, 'C', false, false, 0, false, false, true);
    //else  
    //log_message('error', base64_decode(base_url()."uploads/rentas_hojaestado/".$line));
    //log_message('error', '<th class="httable"><img src="'.base64_decode(base_url()."uploads/rentas_hojasestado/".$line).'"></th>');
  }//for
//=======================================================================================================================================================
  $facturas=$this->ModeloCatalogos->viewprefacturaslis_general($idcontrato);
  $GLOBALS['facturasvencidas']=0;
  $logos = base_url().'public/img/alta.png';
  $logosmds = base_url().'public/img/MDS.png';
  $htmlf = '<table width="100%" border="0" cellpadding="4px" class="info_fac">
              <tr>
                <td rowspan="6" width="25%"><img src="'.$logos.'"></td>
                <td width="50%" valign="top"></td>
                <td rowspan="6" width="25%"><img src="'.$logosmds.'"></td>
              </tr></table><p></p>
              <p></p>
                <style type="text/css">
                    .stileback{
                        background-color:grey;
                        color: white;
                        font-weight: bold;
                    }
                    .stileback2{
                        background-color:#e0e0e0;
                        font-weight: bold;
                    }
                    .bodertable{
                        border: #0078d4 5px solid;
                    }
                    .table td{
                        font-size:10px;
                    }
                </style>
                <table cellpadding="5" border="1" class="table bodertable" width="100%" align="center">
                    <thead>
                        <tr>
                          <td colspan="4" class="stileback">Facturas pendientes</td>
                        </tr>
                        <tr>
                        <td width="9%" class="stileback bodertable">Folio Factura</td>
                        <td width="10%" class="stileback">Fecha Factura</td>
                        <td width="61%" class="stileback">Concepto</td>
                        <td width="10%" class="stileback">Aplicación de pago</td>
                        <td width="10%" class="stileback">Monto Factura</td>
                        <!--<td width="11%" class="stileback">Monto Pagado</td>
                        <td width="11%" class="stileback">Saldo</td>-->
                      </tr></thead>';
            $montovencido=0;
            foreach ($facturas->result() as $item) {
                $descripcion=$this->ModeloCatalogos->descripcionfactura($item->FacturasId);
                if ($item->fechamax==null) {
                    $fechapago='Pendiente';
                }else{
                    $fechapago=$item->fechamax;
                }
                if( floatval($item->pagot)>0){
                    $pagot='$ '.number_format($item->pagot,2,'.',',');
                    $pagot_num=round($item->pagot,2);
                }else{
                    $pagot='';
                    
                    $pagot_num=0;
                }
                $totalf=$item->total-$pagot_num;

                if($totalf>0){
                    $infocomplemento=$this->ModeloCatalogos->infocomplementofactura($item->FacturasId);
                    $pagot_num=$infocomplemento[0];
                    if($infocomplemento[0]>0){
                        $fechapago=date('Y-m-d',strtotime($infocomplemento[1]));
                        $pagot='$ '.number_format($pagot_num,2,'.',',');
                        $totalf=$item->total-$pagot_num;
                    }    
                }
                
                
                $montovencido=$montovencido+$totalf;
                if($totalf>0){
                    $GLOBALS['facturasvencidas']=$GLOBALS['facturasvencidas']+1;
                }
                if($totalf>0){
                $htmlf .= '<tr nobr="true">
                        <td width="9%">U'.$item->Folio.'</td>
                        <td width="10%">'.$item->fechatimbre.'</td>
                        <td width="61%">'.$descripcion.'</td>
                        <td width="10%">'.$fechapago.'</td>
                        <td width="10%">$ '.number_format($item->total,2,'.',',').'</td>
                        <!--<td width="11%">'.$pagot.'</td>
                        <td width="11%">$ '.number_format($totalf,2,'.',',').'</td>-->
                      </tr>';
                }
            }


            

        $htmlf .= '</table>';
        if($GLOBALS['facturasvencidas']>0){
          $pdf->setPrintHeader(false);
          $pdf->SetMargins(7, '10', 7);
          $pdf->AddPage('P', 'A4');
          $html=$htmlf;

          $pdf->writeHTML($html, true, false, true, false, '');
        }
//========================================================================================================================================================

//=======================================================================
$pdf->Output('Folios_.pdf', 'I');
//$pdf->Output('../../facturas/'.$GLOBALS["Folio"].'.pdf', 'F');
//$pdf->Output('/facturas/'.$GLOBALS["Folio"].'.pdf', 'F');
?>