<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Rutas_tecnicos extends CI_Controller {
    public function __construct()     {
        parent::__construct();

        $this->load->model('ModeloCatalogos');
        $this->load->model('Configuraciones_model');
        $this->load->model('ModeloAsignacion');
        $this->load->helper('url');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->submenu=81;
        if($this->session->userdata('logeado')==true){
            $this->perfilid =$this->session->userdata('perfilid');
            $this->idpersonal = $this->session->userdata('idpersonal');
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,$this->submenu);// 52 es el id del submenu
            if ($permiso==0) {
                //redirect('Sistema');
            }
            if($this->idpersonal==1 || $this->idpersonal==12 || $this->idpersonal==17 || $this->idpersonal==18 || $this->idpersonal==54){

            }else{
                redirect('Sistema');
            }
        }else{
            redirect('Sistema'); 
        }
    }

    function index(){
        $data['MenusubId']=$this->submenu;
        $data['perfilid']=$this->perfilid;
        $data['idpersonal']=$this->idpersonal;
        $data['resultstec']=$this->Configuraciones_model->view_session_searchTecnico();  
            $this->load->view('header');
            $this->load->view('main');
            $this->load->view('rutas/ubicacion',$data);
            $this->load->view('footer');
    }
    function mapaview($eje,$fecha){
        /*
        $strq="SELECT id,latitud,longitud,GROUP_CONCAT(reg SEPARATOR ', ') as reg 
              FROM `personal_ubicaciones` 
              WHERE reg>'$fecha 06:00:00' and reg<'$fecha 20:00:00' AND idpersonal='$eje'
              GROUP BY latitud,longitud
              ORDER BY id ASC";
        */
        $strq="SELECT 
                    id,
                    latitud,
                    longitud,
                    tipo,
                    GROUP_CONCAT(
                        CASE 
                           WHEN tipo = 1 THEN CONCAT('(I ', DATE_FORMAT(reg, '%H:%i:%s'), ')')
                           WHEN tipo = 2 THEN CONCAT('(F ', DATE_FORMAT(reg, '%H:%i:%s'), ')')
                           ELSE DATE_FORMAT(reg, '%H:%i:%s')
                        END  
                        SEPARATOR ', ') as reg 
              FROM `personal_ubicaciones` 
              WHERE latitud>0 and reg>'$fecha 06:00:00' and reg<'$fecha 20:00:00' AND idpersonal='$eje'
              GROUP BY latitud,longitud
              ORDER BY id ASC";
        $query = $this->db->query($strq);
        $data['result']=$query;
        //=======================================
            $arrayubicli=array();


            //$fecha=$params['fecha'];
            $idpersonal=$eje;
            
            $result = $this->ModeloCatalogos->get_detalles_itinerario($idpersonal,$fecha);
            foreach ($result->result() as $item) {
                $rest_dir = $this->ModeloAsignacion->otenerdireccion($item->asignacion,$item->tipo_asignacion);
                    $btn_coordenadas='';
                    if($rest_dir['idrow']!='0'){
                        if($rest_dir['longitud']>0 || $rest_dir['latitud']>0){
                            if($item->nr!=1){
                                $arrayubicli[]=array('longitud'=>$rest_dir['longitud'],'latitud'=>$rest_dir['latitud'],'message'=>$item->descripcion.' Hora:'.$item->hora_inicio);
                            }
                        }   
                    }
                  
            }
            if(count($arrayubicli)==0){
                $arrayubicli[]=array('latitud'=>19.08782533671139,'longitud'=>-98.22777569573624,'message'=>'Sin datos');
            }
            $data['arrayubicli']=$arrayubicli;
        //=======================================
        //$this->load->view('rutas/mapa',$data);
        //$this->load->view('rutas/mapa_free',$data);
        $this->load->view('rutas/mapa_free2',$data);
    }
    function mapaview2(){
        $data['puntoa']=$_GET['puntoa'];
        $data['puntob']=$_GET['puntob'];
        $this->load->view('rutas/mapa_a_b',$data);
    }



    
}
?>
