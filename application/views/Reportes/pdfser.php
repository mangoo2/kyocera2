<?php
require_once dirname(__FILE__) . '/TCPDF4/examples/tcpdf_include.php'; 
require_once dirname(__FILE__) . '/TCPDF4/tcpdf.php';


class MYPDF extends TCPDF {
  //Page header
  public function Header() {
      /// datos completos
      $html = '';
   
      $this->writeHTML($html, true, false, true, false, '');
  }

    // Page footer
  public function Footer() {
    $html = '';

      $html .= '<table width="100%" border="0">
                  <tr>
                    <td align="right" class="footerpage">Pagina '.$this->getAliasNumPage().' de '.$this->getAliasNbPages().'</td>
                  </tr>
                </table>';
      $this->writeHTML($html, true, false, true, false, '');
  }
} 
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Cuestionario');
$pdf->SetTitle('Cuestionario');
$pdf->SetSubject('Cuestionario');
$pdf->SetKeywords('Cuestionario');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins('5', '10', '5');
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin("15");

// set auto page breaks
$pdf->SetAutoPageBreak(true, '0');

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('dejavusans', '', 9);
// add a page
$pdf->AddPage('P', 'A4');
$html='';
$html.='<table border="1" width="100%">
        <tr style="height: 25%">
          <td style="width: 25%">
            <img src="'.base_url().'app-assets/images/'.$configuracionCotizacion->logo1.'">
          </td>
          <td style="width: 50%">
            '.$configuracionCotizacion->textoHeader.'
          </td>
          <td style="width: 25%">
            <img class="" src="'.base_url().'app-assets/images/1024_kyocera_logo_mds.png" alt="materialize logo"  >
          </td>
        </tr>
      </table>';
$html.='<table border="1" width="100%">
        <tr>
          <td class="tdstitle" style="width: 10%;">FECHA</td>
          <td style="width: 5%;">'.$dia_reg.'</td>
          <td style="width: 5%;">'.$mes_reg.'</td>
          <td style="width: 5%;">'.$ano_reg.'</td>
          <td class="tdstitle" style="width: 5%">AC</td>
          <td style="width: 10%;">'.$ini_personal.'</td>
          <td class="tdstitle" style="width: 20%">PROINVEN No.</td>
          <td style="width: 10%;">'.$proinven.'</td>
          <td class="tdstitle" style="width: 10%">CDFI</td>
          <td style="width: 20%">'.$cfd.'</td>
        </tr>
      </table>';
$html.='<table border="1" width="100%">
        <tr>
          <td style="width: 5%;">Cia.</td>
          <td style="width: 80%;" colspan="5"><output id="razon_social">'.$empresa_t.'</output></td>
          <td style="width: 5%;">RFC</td>
          <td style="width: 10%;">';
            foreach ($rfc_datos as $item) {
                if($item->id==$rfc_id){ 
                  $html.=$item->rfc;
                }
            }
            $html.='</td>
        </tr>
        <tr>
          <td style="width: 5%; font-size: 12px">VENCE</td>
          <td style="width: 12%;">'.$vence.'</td>
          <td style="width: 10%; font-size: 12px; text-align: center;">METODO DE PAGO</td>
          <td style="width: 20%; font-size: 12px; text-align: center;">';
            foreach ($metodopagorow as $item) {
              if($item->id==$metodopagoId){ 
                $html.=$item->metodopago_text;
              }
            }
          $html.='</td>
          <td style="width: 12%; font-size: 12px; text-align: center;">FORMA DE PAGO</td>
          <td style="width: 14%; font-size: 12px; text-align: center;">';
            foreach ($formapagorow->result() as $item) { 
              if($item->id==$formapago){ 
                $html.=$item->formapago_text;
              }
            }
            
          $html.='</td>
          <td style="width: 10%; font-size: 12px;">USO DE CFDI</td>
          <td style="width: 17%; font-size: 12px;">';
            foreach ($cfdirow->result() as $item) { 
              if($item->id==$cfdi){ 
                $html.=$item->uso_cfdi_text;
              } 
            }


            $html.='
          </td> 
          
        </tr>
      </table>';
      $html.='<table border="1" width="100%">
        <tr>
          <td style="font-size: 12px; width: 5%;">Calle</td>
          <td style="font-size: 12px; width: 26%;" colspan="3">'.$calle.'</td>
          <td style="font-size: 12px; width: 5%;">No.</td>
          <td style="font-size: 12px; width: 5%;">'.$num_ext.'</td>
          <td style="font-size: 12px; width: 5%;">Col.</td>
          <td style="font-size: 12px; width: 25%; " colspan="2">'.$colonia.'</td>
          <td style="font-size: 12px; width: 9%; text-align: center;">FORMA DE COBRO</td>
          <td style="font-size: 12px; width: 20%;">'.$formacobro.'</td>
        </tr>';
        
        $html.='<tr>
          <td style="font-size: 12px; width: 5%;">Cd</td>
          <td style="font-size: 12px;" colspan="4">'.$municipio.'</td>
          <td style="font-size: 12px; width: 5%;">Edo.</td>
          <td style="font-size: 12px; width: 20%;" colspan="4">';
            $estadoval='';
                foreach ($estadorow->result() as $item) { 
                  if($item->EstadoId==$estado){ 
                    $estadoval = $item->Nombre;
                  }
                }
            if($estadovals!=''){
              $html.=$estadovals;
            }else{
              $html.=$estadoval.' C.P.'.$cp;
            }
          $html.='</td>
          <td style="font-size: 12px; width: 5%;">tel.</td>
          <td style="font-size: 12px;" colspan="2">';
            foreach ($resultadoclitel->result() as $item) { 
              if($telId==$item->id){
                $html.=$item->tel_local;
              }
            }

          $html.='</td>
        </tr>';
        
        $html.='<tr>
          <td style="font-size: 12px; width: 10%;" colspan="2">Contacto</td>
          <td style="font-size: 12px; width: 26%;" colspan="2">';
            foreach ($resultadoclipcontacto->result() as $item) {
              if($contactoId==$item->id){
                $html.=$item->persona_contacto;
              }     
            } 
            foreach ($datoscontacto->result() as $item) { 
              if($contactoId==$item->datosId){
                $html.=$item->atencionpara;
              }  
              if ($contactoId==$item->datosId) {
                    $email=$item->email;
              }
            }
            
          $html.='</td>
          <td style="font-size: 12px; width: 7%;">Cargo</td>
          <td style="font-size: 12px; width: 20%;" colspan="3">'.$cargo.'</td>
          <td style="font-size: 12px; width: 7%;">email:</td>
          <td style="font-size: 12px; width: 30%;" colspan="2">'.$email.'</td>
        </tr>';
        $html.='</table>';
        $html.='<table border="1" width="100%">';
          if($resultadoconsumibles->num_rows()>0){
            $html.='<tr><th colspan="7" style="background-color:#5a5959;color:white;text-align:center;">Consumibles</th></tr>         
                    <tr>
                      <th style="font-size: 12px; " >Cantidad</th>
                      <th style="font-size: 12px; " >No. de Parte</th>
                      <th style="font-size: 12px; " >Descripcion</th>
                      <th style="font-size: 12px; " >Precio Unitario</th>
                      <th style="font-size: 12px; " >Total</th>
                    </tr>';
          } 

            $totalgeneral=0;
            $block_envio=0; 
            foreach ($resultadoconsumibles->result() as $item) { 
              $costo=$item->costo;

              $totalc=$item->cantidad*$costo;
              $totalgeneral=$totalgeneral+$totalc;
            
      $html.='<tr>
                <td style="font-size: 12px; text-align: center;">'.$item->cantidad.'</td>
                <td style="font-size: 12px; ">'.$item->parte.'</td>
                <td style="font-size: 12px; " colspan="3">
                  <table border="1" width="100%">
                    <tr>
                      <td width="30%">TIPO</td>
                      <td width="70%">CONSUMIBLE</td>   
                    </tr>
                    <tr>
                      <td>MARCA</td>
                      <td>KYOCERA</td>    
                    </tr>
                    <tr>
                      <td>MODELO</td>
                      <td>'.$item->modelo.'</td>    
                    </tr>                 
                  </table>
                </td>
                <td style="font-size: 12px; text-align: center;"  >$'.number_format($costo,2,'.',',').'</td>
                <td style="font-size: 12px; text-align: center;" >$'.number_format($totalc,2,'.',',').'</td>
              </tr>';
            }
            if($resultadorefacciones->num_rows()>0){
              $html.='<tr><th colspan="7" style="background-color:#5a5959;color:white;text-align:center;">Refacciones</th></tr>
                    <tr>
                <th style="font-size: 12px; ">Cantidad</th>
                <th style="font-size: 12px; ">No. de Parte</th>
                <th style="font-size: 12px; " colspan="3">Descripcion</th>
                <th style="font-size: 12px; ">Precio Unitario</th>
                <th style="font-size: 12px; " >Total</th>
              </tr>';
            }
            foreach ($resultadorefacciones->result() as $item) { 
              $totala=$item->cantidad*$item->costo;
              $totalgeneral=$totalgeneral+$totala;
              
              $html.='<tr>
                <td style="font-size: 12px; text-align: center;">'.$item->cantidad.'</td>
                <td style="font-size: 12px; "></td>
                <td style="font-size: 12px; " colspan="3">
                  <table border="1" width="100%">
                    <tr>
                      <td width="30%">TIPO</td>
                      <td width="70%">REFACCION</td>    
                    </tr>
                    <tr>
                      <td>MARCA</td>
                      <td>KYOCERA</td>    
                    </tr>
                    <tr>
                      <td>MODELO</td>
                      <td>'.$item->nombre.'</td>    
                    </tr>
                    
                  </table>
                </td>
                <td style="font-size: 12px; text-align: center;"  >$'.number_format($item->costo,2,'.',',').'</td>
                <td style="font-size: 12px; text-align: center;" >$'.number_format($totala,2,'.',',').'</td>
              </tr>';
            } 
            if ($resultadoaccesorios->num_rows()>0) {
              $html.='<tr><th colspan="7" style="background-color:#5a5959;color:white;text-align:center;">Accesorios</th></tr>
                      <tr>
                        <th style="font-size: 12px; ">Cantidad</th>
                        <th style="font-size: 12px; ">No. de Parte</th>
                        <th style="font-size: 12px; " colspan="3">Descripcion</th>
                        <th style="font-size: 12px; ">Precio Unitario</th>
                        <th style="font-size: 12px; " >Total</th>
                      </tr>';
            } 
            foreach ($resultadoaccesorios->result() as $item) { 
                $totala=$item->cantidad*$item->costo;
                $totalgeneral=$totalgeneral+$totala;
          
                $html.='<tr>
                  <td style="font-size: 12px; text-align: center;">'.$item->cantidad.'</td>
                  <td style="font-size: 12px; ">'.$item->no_parte.'</td>
                  <td style="font-size: 12px; " colspan="3">
                    <table border="1" width="100%">
                      <tr>
                        <td width="30%">TIPO</td>
                        <td width="70%">Accesorio</td>    
                      </tr>
                      <tr>
                        <td>MARCA</td>
                        <td>KYOCERA</td>    
                      </tr>
                      <tr>
                        <td>MODELO</td>
                        <td>'.$item->nombre.'</td>    
                      </tr>
                      
                    </table>
                  </td>
                  <td style="font-size: 12px; text-align: center;"  >$'.number_format($item->costo,2,'.',',').'</td>
                  <td style="font-size: 12px; text-align: center;" >$'.number_format($totala,2,'.',',').'</td>
                </tr>';
            }
            $totalgeneral=$totalgeneral+$t_precio;
            $html.='<tr><th colspan="7" style="background-color:#5a5959;color:white;text-align:center;">Servicios</th></tr> 
                    <tr>
                      <td style="font-size: 12px; " colspan="2">Tipo de servicio: '.$t_servicio.'</td>
                      <td style="font-size: 12px; " colspan="5">Descripcion de falla: '.$t_servicio_motivo.'</td>
                    </tr>
                    <tr>
                      <td style="font-size: 12px; " colspan="3">Equipos: '.$t_modelo.'</td>
                      <td style="font-size: 12px; " colspan="4">Costo de servicio: $'.$t_precio.'</td>
                    </tr>
                    <tr>
                      <td style="font-size: 12px; " >Subtotal</td>
                      <td style="font-size: 12px; " colspan="2">$'.number_format($totalgeneral,2,'.',',').'</td>
                      <td style="font-size: 12px; ">Iva</td>
                      <td style="font-size: 12px; " >$'.number_format($totalgeneral*0.16,2,'.',',').'</td>
                      <td style="font-size: 12px; ">Total</td>
                      <td style="font-size: 12px; " >$'.number_format(($totalgeneral*1.16),2,'.',',').'</td>
                    </tr>
                </table>';
              $html.='<label for="observaciones">Observaciones:</label>
                      <textarea name="observaciones" id="observaciones" class="form-control" <?php echo $observaciones_block;?> ><?php echo $observaciones;?></textarea>';
            if($firma!=''){
        
                $fh = fopen(base_url()."uploads/firmasservicios/".$firma, 'r') or die("Se produjo un error al abrir el archivo");
                    $linea = fgets($fh);
                    fclose($fh);     
              
                    $html.='<div align="center">
                      <h5>Firma</h5>
                    </div>
                <div style="text-align:center;margin-top:0.05cm;padding:0.1cm;">
                  <img src="'.$linea.'" width="355" height="160" style="border:dotted 1px black;">
                  </div>';
            }
        
      

$pdf->writeHTML($html, true, false, true, false, '');
//$pdf->writeHTML($html, '', 0, '', false, 0, false, false, 0);
$pdf->Output('Captura.pdf', 'I');

?>