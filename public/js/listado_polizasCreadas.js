var base_url = $('#base_url').val();
var table;
var idpago;
var fechahoy = $('#fechahoy').val();
$(document).ready(function() {	
    $('.modal').modal({
        dismissible: true
    });
    $('#cliente').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una opción',
        allowClear: true,
        ajax: {
            url: base_url+'Ventasi/searchclientes',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    var html='';
                    if(element.empresa==element.razon_social){
                        html=element.empresa;
                    }else{
                        html=element.empresa+' ('+element.razon_social+')';
                    }
                    itemscli.push({
                        id: element.id,
                        text: html

                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    });
    // Destruye la tabla y la crea de nuevo con los datos cargados 
    $('.chosen-select').chosen({width: "90%"});
    table = $('#tabla_polizasCreadas').DataTable();
    load();
    $("#pago_p").TouchSpin({
                min: 0,
                max: 9999999999999999999999999999999999,
                initval: 0.25,
                step: 0.01,
                decimals: 2,
            });
});
function loadtable(){
    load();   
}
function load(){
  var cliente= $('#cliente option:selected').val()==undefined?0:$('#cliente option:selected').val();
  var estatus= $('#tipoestatus option:selected').val();
  var personal = $('#ejecutivoselect option:selected').val();
  var fec_ini_reg=$('#fechainicial_reg').val();
  var fechafinal_reg=$('#fechafinal_reg').val();
  var emp=$('#empresaselect option:selected').val();
  var activo=$('#tipoventadelete option:selected').val();
  table.destroy();
  table = $('#tabla_polizasCreadas').DataTable({
        //pagingType: 'full_numbers',
           pagingType: 'input',
           //"sPaginationType": "listbox",
        search: {
                return: true
        }, 
        //stateSave: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url+"index.php/PolizasCreadas/getListadocontratosPolizasIncompletasasi",
            type: "post",
            "data": {
                'cliente':cliente,
                'status':estatus,
                'personal':personal,
                'fec_ini_reg':fec_ini_reg,
                'fechafinal_reg':fechafinal_reg,
                'emp':emp,
                'activo':activo,
                'viewp':1
            },
        },
        "columns": [
            {"data": "id"},
            {"data": "empresa"},
            {"data": "id",
                render:function(data,type,row){
                    var colorbtton='blue';
                    if (row.prefactura==1) {
                        colorbtton='green';
                    }
                    var html='<a class="btn-floating '+colorbtton+' tooltipped" data-position="top" data-delay="50" data-tooltip="Prefactura" onclick="detallep('+row.id+')"><i class="material-icons">assignment</i></a>';
                    if(row.viewcont==1){
                        if (row.prefactura==1) {
                            //if(row.total_pd==1){
                                html+='<a href="'+base_url+'PolizasCreadas/servicios/'+row.id+'/0" class="btn-floating tooltipped" data-position="top" data-delay="50" data-tooltip="Servicio" target="_black"><i class="fas fa-file-invoice"></i></a>';    
                            //}else{
                                //html+='<div style="display: inline-block;">';
                                //html+='<a class="btn-floating tooltipped" data-position="top" data-delay="50" data-tooltip="Servicio" onclick="detalle_pol('+row.id+')" ><i class="fas fa-file-invoice"></i></a>';
                                //html+='<i class="fas fa-plus fa_plus_div"></i>';
                                //html+='</div>';
                            //}
                            //html+=row.total_pd;
                            html+='<!--'+row.total_pd+'-->';
                        }
                    }
                    return html;
                }
            },
            {"data": "nombre"},
            {"data": "estatus",
                render:function(data,type,row){
                    var html='';
                    if(data==1){
                        html='Pendiente';
                    }else if(data==3){
                        html='Pagado';
                    }
                    return html;
                }
            },
            {"data": "tipo", visible: false},
            {"data": 'reg'},
            {"data": null,
                 render:function(data,type,row){
                    var html='';
                    if (row.prefactura==1) {
                        if(row.vencimiento!='0000-00-00'){
                           var html=row.vencimiento; 
                        }
                    }  
                    return html;
                }
            }, 
            {"data": null,
                 render:function(data,type,row){
                    var html='';
                    if (row.tipov==1) {
                        html='Kyocera';    
                    }
                    if (row.tipov==2) {
                        html='D-Impresión';    
                    }  
                    return html;

                }
            },
            {"data": "facturas",
                render:function(data,type,row){
                    var html='';
                    if(row.siniva==1){
                        var totalventa=parseFloat(row.totalpoliza).toFixed(2);
                    }else{
                        var totalventa=(parseFloat(row.totalpoliza)+(parseFloat(row.totalpoliza)*0.16)).toFixed(2);
                    }
                    
                    html+=new Intl.NumberFormat('es-MX',{style: "currency", currency: "MXN"}).format(totalventa);
                    return html;
                }
            },
            {"data": "facturas",
                render:function(data,type,row){
                    var html='';
                    if(row.facturas!='' && row.facturas!='null' && row.facturas!=null){
                        html+=row.facturas;
                        html+=' <a class="b-btn b-btn-primary tooltipped" data-position="top" data-delay="50" data-tooltip="Desvicular Factura" onclick="desvfactura('+row.id+',3)"><i class="fas fa-unlink"></i></a>';
                    }
                        
                    return html;
                }
            }, 

            {"data": null,
                 render:function(data,type,row){
                    var html='';
                    if(row.siniva==1){
                        var totalventa=parseFloat(row.totalpoliza).toFixed(2);
                    }else{
                        var totalventa=(parseFloat(row.totalpoliza)+(parseFloat(row.totalpoliza)*0.16)).toFixed(2);
                    }
                    
                    $('.tooltipped').tooltip();
                    var btn_cfdi='';
                        var btn_pago='';
                        if (row.prefactura==1) {
                           btn_cfdi='<a class="btn-floating blue tooltipped" data-position="top" data-delay="50" data-tooltip="CFDI" onclick="modal_cdfi('+row.id+')">CFDI</a>';
                            if(totalventa>0){
                                if(row.estatus==3){
                                    var colorbuton='green';
                                }else{
                                    var colorbuton='blue';
                                    //if(row.vencimiento!='0000-00-00'){
                                        console.log('Poliza '+row.id+' '+row.vencimiento+'<'+fechahoy);
                                        if(row.vencimiento<fechahoy){
                                            var colorbuton='red';
                                        }
                                    //}
                                }
                                btn_pago='<a class="btn-floating '+colorbuton+' tooltipped modal_pagos verificar_'+row.id+'" data-position="top" data-delay="50" data-tooltip="Pago" data-poliza="'+row.id+'" onclick="modal_pago('+row.id+')">\
                                        <i class="material-icons">attach_money</i></a>';
                            }
                        }
                    if(parseInt(row.activo)==1){
                            html+='<a class="btn-floating red tooltipped facturar" data-position="top" data-delay="50" data-tooltip="Eliminar" onclick="finaliza('+row.id+')">\
                                    <i class="fas fa-trash-alt"></i></a>';
                    }
                    html+=btn_pago;
                    if(parseInt(row.activo)==1){
                        if (row.prefactura==1 && totalventa>0) {
                            
                            html+='<a class="b-btn b-btn-primary tooltipped" data-position="top" data-delay="50" \
                                    data-tooltip="Agregar Factura" \
                                    data-tooltip-id="6df07467-d305-bd8f-2908-fc7be2299d57" onclick="addfactura('+row.id+','+row.idCliente+',2,'+totalventa+','+row.tipov+')"> \
                                            <i class="fas fa-folder-plus"></i></a>';
                        }
                    }
                    if(parseInt(row.activo)==1){
                        if (row.prefactura==0){
                            html+=' <a class="b-btn b-btn-primary tooltipped" data-position="top" data-delay="50" data-tooltip="Cambiar de cliente" onclick="cambiar_cliente('+row.id+',2)"><i class="fas fa-people-arrows"></i></a> ';
                        }
                        if(totalventa>0){
                            if(row.facturas!='' && row.facturas!='null' && row.facturas!=null){}else{
                                html+='<a class="b-btn b-btn-primary tooltipped" data-position="top" data-delay="50" data-tooltip="Ir a Generación de factura" onclick="irfactura('+row.idCliente+',2,'+row.id+','+row.tipov+')"><i class="fas fa-file-invoice"></i></a>';
                            }
                        }
                    }

                    return html;
                }
            }
        ],
        "order": [[ 0, "desc" ]],
        createdRow:function(row,data,dataIndex){
            $(row).find('td:eq(7)')
                .addClass('obtencionfacturas ob_fac_pol_'+data.id)
                .attr('data-poliza',data.id);
        }
        
    }).on('draw',function(){
        $('.dataTables_filter').append('<i class="fa fa-search buttonsearch" onclick="searchtable_v()"></i>');
        vericarpagos();
    });
}
function searchtable_v(){
    var contenedor = document.getElementById('tabla_polizasCreadas_filter');

    // Selecciona el input dentro del contenedor
    var input = contenedor.querySelector('input.form-control-bmz');

    // Obtén el valor del input
    var searchv = input.value;

    // Muestra el valor en la consola
    console.log(searchv);
    //var searchv =$("input[type=search]").val();
    table.search(searchv).draw();
}
function detallep(id){
    window.open(base_url+"PolizasCreadas/view/"+id, "Prefactura", "width=780, height=612");
}
function finaliza(id_v){
    
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de eliminar?<br>Se necesita permisos de administrador<br>'+
                 '<input type="text" placeholder="Contraseña" id="contrasena" name="contrasena" class="name form-control-bmz" style="width:99%" required/>',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var pass=$("input[name=contrasena]").val();
                if (pass!='') {
                    $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Sistema/solicitarpermiso",
                        data: {
                            pass:pass
                        },
                        success: function (response){
                                var respuesta = parseInt(response);
                                if (respuesta==1) {
                    //=============================================================================
                                $.ajax({
                                  type: "POST",
                                  url: base_url+"index.php/PolizasCreadas/finalizar_factura",
                                  data:{id:id_v},
                                  success: function(data){
                                    var status= parseFloat(data);
                                    if (status==1) {
                                        $.alert({boxWidth: '30%',useBootstrap: false,title: 'Atención!',content: 'Eliminado correctamente'});
                                        load(); 
                                    }else{
                                        $.alert({boxWidth: '30%',useBootstrap: false,title: 'Atención!',content: 'No es posible eliminar, Prefactura de póliza facturada'});
                                    }
                                  }
                                });
                    //=============================================================================
                                }else{
                                    notificacion(2);
                                }
                        },
                        error: function(response){  
                             notificacion(1);
                        }
                    });

                }else{
                    notificacion(0);
                }
            },
            cancelar: function () {
            }
        }
    });
    setTimeout(function(){
        new MaskedPassword(document.getElementById("contrasena"), '\u25CF');
    },1000);
}
function fecha_vencimiento(id_v){
    var result='';
    $.ajax({
      type: "POST",
      url: base_url+"index.php/PolizasCreadas/verificar_fecha_vencimiento",
      data:{id:id_v},
      async:false,
      success: function(data){
        result=data;
      }
    });   
    return result;
}
function modal_cdfi(id) {
    $('#id_venta_e').val(id);
    $('#modal_cfdi').modal('open');
}
function update_cfdi(){
    var id_venta=$('#id_venta_e').val();
    var cfdi=$('#cdfi_e').val();
    $.ajax({
      type: "POST",
      url: base_url+"index.php/PolizasCreadas/update_cfdi_registro",
      data:{id_venta:id_venta,cfdi:cfdi},
      success: function(data){
         $.alert({boxWidth: '30%',useBootstrap: false,title: 'Atención!',content: 'Guardador correctamente'});
      }
    }); 
    var cfdi=$('#cdfi_e').val('');  
}
function modal_pago(id){
    $('#id_venta_p').val(id);
    $('#fecha_p').val('');
    $('#pago_p').val('');
    $('#observacion_p').val('');
    $('.modal').modal({
        dismissible: true
    });
    $('#modal_pago').modal('open');
    $('.monto_prefactura').html('$ <span class="totalpoliza">'+monto_poliza(id)+'</span>');
    table_tipo_compra(id);
    setTimeout(function(){ 
        calcularrestante();
    }, 1000);
}
function calcularrestante(){
    var totalpoliza=parseFloat($('.totalpoliza').html());
    var pagos = 0;
    $(".montoagregado").each(function() {
        var vstotal = parseFloat($(this).html());
        pagos += Number(vstotal);
    });
    var restante = parseFloat(totalpoliza)-parseFloat(pagos);
    if(restante<=0){
        restante=0;
        cambiarestatuspago(totalpoliza);
    }
    $('.restante_prefactura').html('$ <span class="restantepoliza">'+restante.toFixed(2)+'</span>');
}
function monto_poliza(id_v){//poliza monto de prefactura
    var result='';
    $.ajax({
      type: "POST",
      url: base_url+"index.php/PolizasCreadas/montopoliza",
      data:{id:id_v},
      async:false,
      success: function(data){
        result=data;
      }
    });   
    return result;
}
function guardar_pago_compras(){
    $( ".guardar_pago_compras" ).prop( "disabled", true );
    setTimeout(function(){ 
         $(".guardar_pago_compras" ).prop( "disabled", false );
    }, 2000);
    var form_pago = $('#form_pago');
    form_pago.validate({
            rules: 
            { 
                fecha: {
                    required: true
                },
                idmetodo: {
                    required: true
                },
                pago: {
                    required: true
                }
            },
            // Para mensajes personalizados
            messages: 
            {   
                fecha:{
                    required: "Campo obligatirio"
                },
                idmetodo:{
                    required: "Campo obligatirio"
                },
                pago:{
                    required: "Campo obligatirio"
                }
            },
        errorElement : 'div'
    });
    var valid = $("#form_pago").valid();
    if(valid){
        var restante =parseFloat($('.restantepoliza').html());
        var pago_p =parseFloat($('#pago_p').val());
        if (pago_p>0 && pago_p<=restante) {
            var datos = $('#form_pago').serialize();
            $.ajax({
              type: "POST",
              url: base_url+"index.php/PolizasCreadas/guardar_pago_tipo",
              data:datos,
              success: function(data){
                 $.alert({boxWidth: '30%',useBootstrap: false,title: 'Atención!',content: 'Guardador correctamente'});
                $('#fecha_p').val('');
                $('#pago_p').val('');
                $('#observacion_p').val('');
                table_tipo_compra($('#id_venta_p').val());
                setTimeout(function(){ 
                    calcularrestante();
                }, 1000);
              }
            });
        }else{
            $.alert({boxWidth: '30%',useBootstrap: false,title: 'Atención!',content: 'Monto mayor a 0 y menor o igual al restante'});
        }
    }   
}
function table_tipo_compra(id){
    $('.text_tabla_pago').html('');
    $.ajax({
      type: "POST",
      url: base_url+"index.php/PolizasCreadas/table_get_tipo_compra",
      data:{id:id},
      success: function(data){
        $('.text_tabla_pago').html(data);
      }
    });
}
function deletepagop(id){
    $('#modal_eliminarpago').modal();
    $('#modal_eliminarpago').modal('open');
    idpago=id;
}
function eliminarpago(){
    $.ajax({
        type: "POST",
        url: base_url+"index.php/PolizasCreadas/vericar_pass",
        data:{
            pago:idpago,
            pass:$('#ver_pass').val()
        },
        success: function(data){
            $('#ver_pass').val('');
            $('#modal_eliminarpago').modal('close');
            $('#modal_pago').modal('close');
            if(parseFloat(data)==1){
                $.alert({boxWidth: '30%',useBootstrap: false,title: 'Atención!',content: 'Pago Eliminado correctamente'});
            }else{
                swal({ title: "Error",text: "Verificar la contraseña",type: 'error',showCancelButton: false,allowOutsideClick: false,});
            }
        }
    });
}
function confirmarpagov(id,tipo){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: 'Se necesita permisos de administrador<br>'+
                 '<input type="password" placeholder="Contraseña" id="contrasena" class="name form-control" required/>',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var pass=$('#contrasena').val();
                if (pass!='') {
                    $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Sistema/solicitarpermiso",
                        data: {
                            pass:pass
                        },
                        success: function (response){
                                var respuesta = parseInt(response);
                                if (respuesta==1) {
                    //=============================================================================
                        confirmarpagov2(id,tipo);
                    //=============================================================================
                                }else{
                                    $.alert({boxWidth: '30%',useBootstrap: false,title: 'Advertencia!',content: 'No tiene permisos'}); 
                                }
                        },
                        error: function(response){
                            $.alert({boxWidth: '30%',useBootstrap: false,title: 'Error!',content: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema'});   
                             
                        }
                    });

                }else{
                    $.alert({boxWidth: '30%',useBootstrap: false,title: 'Advertencia!',content: 'Ingrese una contraseña'}); 
                }
                
            },
            cancelar: function (){
                
            }
        }
    });
}
function confirmarpagov2(id,tipo){
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Generales/confirmarpagos",
        data: {
            id:id,
            tipo:tipo
        },
        success: function (response){
            $('#modal_pago').modal('close');
        },
        error: function(response){
            $.alert({boxWidth: '30%',useBootstrap: false,title: 'Error!',content: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema'});   
             
        }
    });
}
function desvfactura(id,tipo){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: 'para desvicular las facturas Se necesita permisos de administrador<br>'+
                 '<input type="password" placeholder="Contraseña" id="contrasena" class="name form-control" required/>',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var pass=$('#contrasena').val();
                if (pass!='') {
                    $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Sistema/solicitarpermiso",
                        data: {
                            pass:pass
                        },
                        success: function (response){
                                var respuesta = parseInt(response);
                                if (respuesta==1) {
                    //=============================================================================
                        desvfactura2(id,tipo);
                    //=============================================================================
                                }else{
                                    toastr["warning"]("No tiene permisos", "Advertencia");
                                }
                        },
                        error: function(response){
                            toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema"); 
                             
                        }
                    });

                }else{
                    toastr["warning"]("Ingrese una contraseña", "Advertencia");
                }
                
            },
            cancelar: function () 
            {
                
            }
        }
    });
}
function desvfactura2(id,tipo){
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Generales/desvfactura",
        data: {
            id:id,
            tipo:tipo
        },
        success: function (response){
                toastr["success"]("Facturas desvinculadas");
                load();
        },
        error: function(response){
            toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema"); 
             
        }
    });
}
function vericarpagos(){
    var DATAf  = [];
    $(".modal_pagos").each(function() {
        item = {};
        item ["poliza"] = $(this).data('poliza');
        DATAf.push(item);
    });
    aInfo   = JSON.stringify(DATAf);
    //console.log(aInfo);
    var datos=aInfo;
    
    $.ajax({
        type:'POST',
        url: base_url+"Generales/vericarpagospoliza",
        data: {
            datos:aInfo
        },
        success: function (data){
            //console.log(data);
            var array = $.parseJSON(data);
            console.log(array);
            $.each(array, function(index, item) {
                $(".verificar_"+item.idpoliza).addClass( "yellowc" );
                
            });
        }
    });
}
function loadtable(){
    var tipo=$('#tipoventa').val();
    if(tipo==4){

    }else{
        window.location.href = base_url+"index.php/Ventas?tv="+tipo;
    }
}
function detalle_pol(id){
    $('#modalpolizadetalle').modal('open');
    $.ajax({
        type:'POST',
        url: base_url+"index.php/PolizasCreadas/detalle_pol_op",
        data: {
            id:id
        },
        success: function (response){
                $('.add_info_pol_dll').html(response);
        },
        error: function(response){
            toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema"); 
             
        }
    });
}
function idmetodop(){
    var met = $('#idmetodo option:selected').val();
      var fecha = $('#fechaactual').val();
      if(met==1){
        $('#fecha_p').val(fecha).prop({'readonly':true});
      }else{
        $('#fecha_p').prop({'readonly':false});
      }
}