            <?php if ($datosCotizacion[0]->propuesta==0) { ?>
                <table>
                  <tr class="tablaBordesPrincipal">
                      <td class="tablaBordesPrincipal" width="90%" align="right" style="text-align: right;"><b>SubTotal:</b></td>
                      <td class="tablaBordesPrincipal" width="10%"><b>$ <?php echo number_format($GLOBALS["total"],2,'.',',');?></b></td> 
                  </tr>
                  <tr class="tablaBordesPrincipal">
                      <td class="tablaBordesPrincipal" align="right" style="text-align: right;"><b>I.V.A:</b></td>
                      <td class="tablaBordesPrincipal"><b>$ <?php echo number_format($GLOBALS["total"]*0.16,2,'.',',');?></b></td> 
                  </tr>
                  <tr class="tablaBordesPrincipal">
                      <td class="tablaBordesPrincipal" width="90%" align="right" style="text-align: right;"><b>TOTAL:</b></td>
                      <td class="tablaBordesPrincipal" width="10%"><b>$ <?php echo number_format($GLOBALS["total"]+($GLOBALS["total"]*0.16),2,'.',',');?></b></td> 
                  </tr>
                </table>
            <?php  } ?>
            <div align="center"><?php echo $configuracionCotizacion->leyendaPrecios; ?></div>
            
            <!-- Observaciones -->
            <div class="row" style="margin-bottom: -12px">
              <table class="centered letraTablaFooterEquipos" style="width:100%">
                  <thead>
                      <tr>
                        <th class="tablaBordesPrincipal letraTablaFooterEquiposHeader">
                          <div id="contenido1" contenteditable="<?php if($datosCotizacion[0]->estatus==1){ echo 'true';}else{ echo 'false';}?>" class="info1" oninput="saveinfo_o(1)" ><?php
                            if($datosCotizacion[0]->info1!=null){
                              echo $datosCotizacion[0]->info1;
                            }else{
                              echo $configuracionCotizacion->tituloObservaciones;
                            }
                            ?></div>
                        </th>
                        <th class="tablaBordesPrincipal letraTablaFooterEquiposHeader"><?php echo $configuracionCotizacion->titulo1Footer; ?></th>
                      </tr>
                  </thead>
                  <tbody >
                    <tr>
                        <td class="tablaBordesPrincipal contenido1" width="50%">
                          <div id="contenido2" contenteditable="<?php if($datosCotizacion[0]->estatus==1){ echo 'true';}else{ echo 'false';}?>" class="info2" oninput="saveinfo_o(2)"><?php 
                            if($datosCotizacion[0]->info2!=null){
                              echo $datosCotizacion[0]->info2;
                            }else{
                              echo $configuracionCotizacion->contenidoObservaciones;
                            }
                           ?></div>
                        </td>
                        <td class="tablaBordesPrincipal contenido2" width="50%" ><div id="contenido5" contenteditable="<?php if($datosCotizacion[0]->estatus==1){ echo 'true';}else{ echo 'false';}?>" class="info5" oninput="saveinfo_o(5)"><?php 
                            if($datosCotizacion[0]->info5!=null){
                              echo $datosCotizacion[0]->info5;
                            }else{
                              echo $configuracionCotizacion->contenido1Footer;
                            }
                           ?></div></td>
                    </tr>
                  </tbody>
                </table>
            </div>
            <br>

            
            <div class="row" style="margin-bottom: -12px">
              <table class="centered letraTablaFooterEquipos" style="width:100%">
                  <thead>
                      <tr>
                        <th class="tablaBordesPrincipal">
                          <div id="contenido3" contenteditable="<?php if($datosCotizacion[0]->estatus==1){ echo 'true';}else{ echo 'false';}?>" class="info3" oninput="saveinfo_o(3)"><?php 
                            if($datosCotizacion[0]->info3!=null){
                              echo $datosCotizacion[0]->info3;
                            }else{
                              echo $configuracionCotizacion->titulo2Footer;    
                            }
                           ?></div>
                        </th>
                        <th class="tablaBordesPrincipal"><?php echo $fechareg; ?></th>
                      </tr>
                  </thead>
                  <tbody >
                    <tr>
                        <td class="tablaBordesPrincipal " width="50%" >
                          
                          <div id="contenido4" contenteditable="<?php if($datosCotizacion[0]->estatus==1){ echo 'true';}else{ echo 'false';}?>" class="info4" onchange="saveinfo_o(4)" ><?php 
                            if($datosCotizacion[0]->info4!=null){
                              echo $datosCotizacion[0]->info4;
                            }else{
                              echo $configuracionCotizacion->contenido2Footer;  
                            }
                           ?></div>  
                        </td>
                        <td class="tablaBordesPrincipal contenido4" width="50%">
                          <?php echo $configuracionCotizacion->firma;?><b><?php echo$nombreUsuario ?></b><br><?php echo $emailUsuario ?>
                          </td>
                    </tr>
                  </tbody>
                </table>
            </div>

           

<!--columna 1---------------------------------------------------------------->

              <form class="form" id="" method="post">
                <?php
                if(isset($cliente))
                {
                ?>
                <input id="idCliente" name="idCliente" type="hidden" value="<?php echo $idCliente; ?>">
                <?php
                }
                ?>
                <div class="row">
                  <div class="input-field col s12">
                    <button class="btn-bmz btn cyan waves-effect waves-light right btn_imp" type="button" onclick="btn_guardar()">Imprimir
                    <i class="material-icons right">send</i>
                    </button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="">
  <select id="atencionselect_xx" class="browser-default form-control-bmz" style="display: none;" onchange="obtenercontactosss()">
                                      <option 
                                      data-atencionpara=""
                                      data-email=""
                                      data-telefono=""
                                      data-celular=""
                                      disabled selected
                                      >Seleccione</option>
                                  <?php foreach ($datoscontacto->result() as $item) { ?>
                                      <option 
                                        data-atencionpara="<?php echo $item->atencionpara;?>"
                                        data-email="<?php echo $item->email;?>"
                                        data-telefono="<?php echo $item->telefono;?>"
                                        data-celular="<?php echo $item->celular;?>"

                                        ><?php echo $item->atencionpara;?></option>
                                  <?php } ?>
                                </select>
</div>

<script type="text/javascript">
  var idCotizacion = $('#idCotizacion').val();
  var base_url = $('#base_url').val();
  function saveinfo_o(tipo){
    saveinfo(<?php echo $idCotizacion;?>,tipo);
  }
  $(document).ready(function($) {
    if ($('#estatuscot').val()==1) {
        console.log('entra el editor');
        
        $( '#contenido1' ).ckeditor();
        $( '#contenido2' ).ckeditor();
        $( '#contenido3' ).ckeditor();
        $( '#contenido4' ).ckeditor();
        $( '#textoHeader2' ).ckeditor();
        
    } 
  });
  function saveinfo(idcoti,tipo){
    var info=$('.info'+tipo).html();
    console.log(info);
    $.ajax({
        type:'POST',
        url: base_url+'Generales/editarinfocotizacion',
        data: {
            cotizacion: idcoti,
            tipo: tipo,
            info: info
            },
            async: false,
            statusCode:{
                404: function(data){
                    //swal("Error", "404", "error");
                },
                500: function(){
                    //swal("Error", "500", "error"); 
                }
            },
            success:function(data){
              
            }
        });
  }
  /*
  function btn_guardar(){
    //window.open(base_url+'Cotizaciones/pdf/'+idCotizacion, '_blank');
    $('.print_css').append('.cot{color: red}');
    $('.pantalla').remove();
    $('.usuario').remove();
    $('.menu').remove();
    $('.kyoc').remove();
    $('.log').append("<img class='kyoc' src='"+base_url+"app-assets/images/altakyo.png' alt='materialize logo' width='200px;' height='100px;'>");
    $('.venta_text').append("<h6 align='center' style='font-size:15px; color:black;'>Venta-Renta-Mantenimiento<br>Impresoras y Multifuncionales monocromáticos y color<br> Blvd. Norte No. 18831, San Alejandro, CP 72090, Puebla, Pue.<br> Call Center: 273 3400, 249 5393<br> Kyoceraap.com</h6>");
   // $('.venta_img').append("<img class='kyoc' src='"+base_url+"app-assets/images/1024_kyocera_logo.png' alt='materialize logo' width='200px;' height='80px;'>");
    //$('.btn_imp').remove();
    saveinfo_o(1);
    saveinfo_o(2);
    saveinfo_o(3);
    saveinfo_o(4);
    setTimeout(function() {window.print();}, 500);
    setTimeout(function() {location.reload();}, 5000);
  }
  */
  function btn_guardar(){
    saveinfo_o(1);
    saveinfo_o(2);
    saveinfo_o(3);
    saveinfo_o(4);
    saveinfo_o(5);
    window.open(base_url+'index.php/Cotizaciones/pdf/'+idCotizacion, '_blank');
  }
</script>
<script type="text/javascript" src="<?php echo base_url(); ?>public/js/cotizacionEquipos2.js?v=<?php echo date('YmdGi');?>" ></script>
<script type="text/javascript" src="<?php echo base_url(); ?>public/js/cotizaciongenerales.js?v=<?php echo date('YmdGi');?>" ></script>
<style type="text/css">
  <?php 
    if(isset($_GET['view'])){ ?>
      .button_add_cotizacion, .badgeaction{
        display: none;
      }
  <?php } ?>
  <?php if(isset($_GET['vte'])){ ?>
    .button_add_cotizacion, .badgeaction{display: none;}
  <?php }elseif ($perfilid==5 || $perfilid==12) { ?>
      .button_add_cotizacion, .badgeaction{display: none;}
    <?php } ?>
</style>
<script type="text/javascript">
  $(document).ready(function($) {
    <?php if(isset($_GET['vte'])){ ?>
      $('[onclick]').removeAttr('onclick');
    <?php }elseif ($perfilid==5 || $perfilid==12) { ?>
      $('[onclick]').removeAttr('onclick');
    <?php } ?>
  });
</script>