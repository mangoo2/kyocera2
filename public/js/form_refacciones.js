// Saber si es solo Visualización
var tipoVista = $("#tipoVista").val();
var base_url = $('#base_url').val();
$(document).ready(function(){
    $('.chosen-select').chosen({width: "90%"});
    var base_url = $('#base_url').val();
    var formulario_consumibles = $('#consumibles_form');

    // En caso de que sea solo visualizar, deshabilita todo
   
    if (tipoVista == 2) {
        var RefaccionesID = $('#idRefacciones').val(); 
        loadTabla_reacciones(RefaccionesID);
    }
    else if(tipoVista == 3)
    {   var RefaccionesID = $('#idRefacciones').val(); 
        loadTabla_reacciones(RefaccionesID);
        $("input").prop('disabled', true);
        $("textarea").prop('disabled', true);
        $("select").prop('disabled', true);
        $("checkbox").prop('disabled', true);
        $("button").prop('disabled', true);
        $(".btn_agregar").attr('style', 'pointer-events: none;');

    }


    var formulario_refacciones = $('#refacciones_form');
     formulario_refacciones.validate({
            rules: 
            {
                
                codigo: {
                    required: true
                },
                nombre: {
                    required: true
                },

                /*
                categoria: {
                    required: true
                },
                */
                stock: {
                    required: true
                },
                precio_usa: {
                    required: true
                },
                precio_unitario: {
                    required: true
                },
                precio_total: {
                    required: true
                }
            
            },
            // Para mensajes personalizados
            messages: 
            {
                 
                codigo:{
                    required: "Ingrese un codigo"
                },
                nombre:{
                    required: "Ingrese un nombre"
                },
                categoria:{
                    required: "Ingrese una categoria"
                },
                stock:{
                    required: "Ingrese un stock"
                },
                precio_usa:{
                    required: "Seleccione un precio USA"
                },
                precio_unitario:{
                    required: "Ingrese un precio unitario"
                },
                precio_total:{
                    required: "Ingrese un precio total"
                }
            
            },
        errorElement : 'div',
            // Si están correctos los campos, procesamos el formulario mediante Ajax
        submitHandler: function (form) 
      {

            var DATA  = [];
                    var TABLA   = $(".border");
                        TABLA.each(function(){         
                             item = {};
                             item ["idEquipo"]   = $(this).find("select[id*='equipos']").val();
                             DATA.push(item);
                        });
            equipos   = JSON.stringify(DATA);
    
                var datos = formulario_refacciones.serialize()+'&equipos2='+equipos;

                $.ajax({
                type:'POST',
                url: base_url+'index.php/refacciones/insertaActualizaRefaciones',
                data: datos,
                success:function(data)
                {
                    if(data=='existe'){
                        swal({ title: "Error",
                            text: "Compatibilidad de impresoras ya existe, ingrese otro diferente.",
                            type: 'error',
                            showCancelButton: false,
                            allowOutsideClick: false,
                        });
                    } 
                    // Si existe el usuario, mostramos el mensaje de entrada y redirigimos
                    else{
                        cargaimage(data);
                        swal({ title: "Éxito",
                            text: "Se insertó la refación",
                            type: 'success',
                            showCancelButton: false,
                            allowOutsideClick: false,
                        });
                        
                        setTimeout(function(){ 
                          window.location.href = base_url+"index.php/refacciones"; 
                        }, 2000);
                    
                    }
                }
            });
              
        }
    
     });
     

    $('.dropify').dropify({
        messages: {
            default: 'Arrastra y suelta un archivo aquí o haz clic',
            replace: 'Arrastre y suelte un archivo o haga clic para reemplazar',
            remove:  'Eliminar',
            error:  'Lo sentimos, el archivo es demasiado grande.'
        }
    });

    $(".btn_agregar").click(function(){
        $('.chosen-select').chosen('destroy');
      var contador = $('.border').length; 
      var contadoraux = contador+1; 
      var $template = $("#datos1"),
      $clone = $template
     .clone()
     .show()
     .removeAttr('id')
     .attr('id',"datos"+contadoraux)
     .insertAfter($("#datos"+contador));

     $clone.find($('#categoria'));
     $clone.find(".div_btn").html('<a class="btn btn-floating waves-effect waves-light red btn_eliminar"><i class="material-icons">delete_forever</i></a>');
     $clone.find("select").val("");
     $clone.find("input").val("");
     eliminar();
     $('.chosen-select').chosen({width: "90%"});

    });

               // Listener para Eliminar 
    $('#tabla_equipos tbody').on('click', 'a.elimina', function () 
    {
        var tr = $(this).closest('tr');
        var row = table3.row(tr);
        var data = row.data();
        confirmaEliminarEquipo(data.id);
    });

});
function eliminar(){
    $(".btn_eliminar").click(function(){
                    var $row = $(this).parents('.border');
   
                    // Remove element containing the option
                    $row.remove();

    });
}

function confirmaEliminarEquipo(idAccesorio)
{
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de Eliminar este Equipo? El cambio será irreversible',
        type: 'red',
        typeAnimated: true,
        buttons: 
        {
            confirmar: function () 
            {
                $.ajax({
                    url: base_url+"index.php/refacciones/eliminar_has_refacciones/"+idAccesorio,
                    success: function (response) 
                    {

                        if(response==1)
                        {
                            $.alert({
                                boxWidth: '30%',
                                useBootstrap: false,
                                title: 'Éxito!',
                                content: 'Equipo Eliminado!'});
                            location.reload();
                        }
                        else
                        {
                            $.alert({
                                boxWidth: '30%',
                                useBootstrap: false,
                                title: 'Error!',
                                content: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema'}); 
                             
                        }
                    },
                    error: function(response)
                    {
                        $.alert({
                                boxWidth: '30%',
                                useBootstrap: false,
                                title: 'Error!',
                                content: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema'});   
                        
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}
function loadTabla_reacciones(id) 
{   
    table3 = $('#tabla_equipos').DataTable({
            "ajax":{ "url": base_url+"index.php/Refacciones/getListado_reacciones/"+id,
        },
        "columns": [
          {"data": "id"},
          {"data": "modelo"},
          {"data": "id", visible: false},
          {"data":null,title:"Acciones",orderable:false,class:"centrado_text",
                render:function(data,type,row){
                    $('.tooltipped').tooltip();
                    if(tipoVista == 3){
                        var html='<ul>\
                                <li>\
                                  <a class="btn-floating red soloadministradores"  data-delay="50">\
                                    <i class="material-icons">delete_forever</i>\
                                  </a>\
                                </li>\
                              </ul>';
                        return html;
                        }else{
                        var html='<ul>\
                                    <li>\
                                      <a class="btn-floating red tooltipped elimina soloadministradores" data-position="top" data-delay="50" data-tooltip="Eliminar">\
                                        <i class="material-icons">delete_forever</i>\
                                      </a>\
                                    </li>\
                                  </ul>';
                        return html;  
                        }
                }
            }
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[25, 50, 100], [25, 50, 100]],
        "dom": 'Bfrtpl',
        // Muestra el botón de Excel para importar la tabla
        "buttons": [
            /*{   
                extend: 'excelHtml5',
                text: ' Descargar Excel <i class="fa fa-download"></i>',
                className: 'btn classBotonFratsa'
            }
            */
        ],
        // Cambiamos lo principal a Español
        "language": {
            "lengthMenu": "Desplegando _MENU_ elementos por página",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
              }
        }
    });
}

function cargaimage(id){
    if ($('#foto')[0].files.length > 0) {
                var inputFileImage = document.getElementById('foto');
                var file = inputFileImage.files[0];
                var data = new FormData();
                data.append('foto',file);
                data.append('refacciones',id);
                $.ajax({
                    url:base_url+'index.php/Refacciones/cargafiles',
                    type:'POST',
                    contentType:false,
                    data:data,
                    processData:false,
                    cache:false,
                    success: function(data) {
                        var array = $.parseJSON(data);
                            console.log(array.ok);
                            if (array.ok=true) {
                                //$(".fileinput").fileinput("clear");
                               // toastr.success('Se ha cargado el nuevo documento correctamente','Hecho!');
                                //cargafiles();
                            }else{
                               // toastr.error('Error', data.msg);
                            }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                            var data = JSON.parse(jqXHR.responseText);
                            //toastr.error('Error', data.msg);
                        }
                });
            }
}

function validarequipo(){
    $.ajax({
        type:'POST',
        url: base_url+'index.php/Refacciones/verificarequipo',
        data: {datos:$('#equipos').val(),idrefac:$('#idRefacciones').val()},
        success:function(data)
        {
            if(data == 1){
                swal({ title: "Error",
                    text: "Compatibilidad de impresoras ya existe, ingrese otro diferente.",
                    type: 'error',
                    showCancelButton: false,
                    allowOutsideClick: false,
                });
            }
        }
    });
}
function verificasiyaexiste(id){
    $.ajax({
        type:'POST',
        url: base_url+'index.php/Refacciones/verificasiyaexiste',
        data: {
            id:id,
            codigo:$('#codigo').val()},
        success:function(data){
            if(parseInt(data) == 1){
                swal({ title: "Error",
                    text: "No. de parte ya existe",
                    type: 'error',
                    showCancelButton: false,
                    allowOutsideClick: false,
                });
                $('.buttonsave').prop( "disabled", true );
            }else{
                $('.buttonsave').prop( "disabled", false );
            }
        }
    });
}