<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class Consumibles_model extends CI_Model 
{
    public function __construct() 
    {
        parent::__construct();
        $this->DB5 = $this->load->database('other5_db', TRUE);
    }
    
    public function insertar_consumibles($dato){
    $this->db->insert('consumibles',$dato);   
    return $this->db->insert_id();
    } 

    public function insertar_consumibles_costos($dato){
    $this->db->insert('consumibles_costos',$dato);   
    return $this->db->insert_id();
    } 

    public function insertar_consumibles_equipos($dato){
    $this->db->insert('equipos_has_consumibles',$dato);   
    return $this->db->insert_id();
    } 


    public function update_consumibles($data, $id) {
        $this->db->set($data);
        $this->db->where('id', $id);
        $this->db->update('consumibles');
        return $this->db->insert_id();
    }
    public function update_foto($data, $id) {
        $this->db->set($data);
        $this->db->where('id', $id);
        return $this->db->update('consumibles');
    }

    public function update_consumibles_costos($data, $id) {
        $this->db->set($data);
        $this->db->where('id', $id);
        return $this->db->update('consumibles_costos');
    }

    public function getListadoConsumibles()    {
        //SELECT c.id,e.modelo,c.idConsumibles FROM equipos_has_consumibles as c left join equipos as e on e.id = c.idEquipo where c.idConsumibles =435 
        $sql = "SELECT
                c.id,
                c.foto,
                c.modelo,
                c.parte,
                c.stock,
                c.observaciones,
                c.rendimiento_unidad_imagen,
                c.rendimiento_toner,
                GROUP_CONCAT(eq.modelo SEPARATOR ', ') as equipos,
                c.paginaweb,
                c.destacado
                FROM consumibles as c
                left join equipos_has_consumibles as ec on ec.idConsumibles=c.id
                left join equipos as eq on eq.id=ec.idEquipo
                WHERE c.status=1 group by c.id";
        $query = $this->db->query($sql);
        return $query->result();
    }


    public function getDataEquipos(){
        $sql = "SELECT * FROM equipos WHERE estatus=1";
        $query = $this->db->query($sql);
        return $query->result();
    } 
    public function getConsumiblePorId($idConsumible)    {
        $sql = "SELECT
                c.*,
                cc.general,
                cc.iva,
                cc.neto,
                cc.frecuente,
                cc.iva2,
                cc.neto2,
                cc.especial,
                cc.iva3,
                cc.neto3,
                cc.poliza,
                cc.iva4,
                cc.neto4,
                cc.id as 'idConsumiblesCostos'
                FROM consumibles as c 
                INNER JOIN consumibles_costos as cc ON cc.consumibles_id=c.id
                WHERE c.id=".$idConsumible;
        $query = $this->db->query($sql);
        return $query->row();
    }
    public function insertar_equipos_has_consumibles($dato)    {
        $this->db->insert('equipos_has_consumibles',$dato);   
        return $this->db->insert_id();
    }
    public function getListadoHas_consumibles($id)    {
        $sql = "SELECT c.id,e.modelo,c.idConsumibles FROM equipos_has_consumibles as c left join equipos as e on e.id = c.idEquipo where c.idConsumibles =".$id;
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function eliminar_has_consumibles($id) {
        $this->db->where('id', $id);
        return $this->db->delete('equipos_has_consumibles');
    }
    public function truncateTable($table) {
        $this->db->truncate($table);
    }
    public function getListaPreciosConsumibles()    {
        $sql = "SELECT
                c.id AS DT_RowId,
                c.modelo,
                cc.precioDolares,
                cc.neto4,
                cc.iva4,
                cc.neto3,
                cc.poliza,
                cc.iva3,
                cc.especial,
                cc.neto2,
                cc.iva2,
                cc.frecuente,
                cc.neto,
                cc.iva,
                cc.general
                FROM consumibles as c
                LEFT JOIN consumibles_costos as cc ON cc.consumibles_id=c.id WHERE c.status=1";
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function getPreciosConsumiblesPorId($idConsumible)    {
        $sql = "SELECT
                c.id AS DT_RowId,
                c.modelo,
                cc.precioDolares,
                cc.neto4,
                cc.iva4,
                cc.neto3,
                cc.poliza,
                cc.iva3,
                cc.especial,
                cc.neto2,
                cc.iva2,
                cc.frecuente,
                cc.neto,
                cc.iva,
                cc.general
                FROM consumibles as c
                LEFT JOIN consumibles_costos as cc ON cc.consumibles_id=c.id WHERE c.id=".$idConsumible;
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function update_precios_consumibles($data, $id)     {
        //log_message('error', 'entra al update_precios_consumibles');
        $this->db->set($data);
        $this->db->where('consumibles_id', $id);
        $this->db->update('consumibles_costos');
        $this->updatepreciosequipos($id);
        return $id;
    }
    function updatepreciosequipos($consumible){
        //log_message('error', 'entra al updatepreciosequipos');
        $sql = "SELECT * 
                FROM consumibles_costos
                WHERE consumibles_id=$consumible";
        $query = $this->db->query($sql);
        $poliza=0;
        foreach ($query->result() as $item) {
            $poliza=$item->poliza;
        }
         //log_message('error', 'poliza:'.$poliza);
        if ($poliza>0) {
            $poliza=$poliza;
        }else{
            $poliza=0;
        }
        if ($consumible==30) {
           //log_message('error', 'poliza:'.$poliza);
        }

        $sql2 = "SELECT * 
                FROM consumibles
                WHERE id=$consumible";
        $query2 = $this->db->query($sql2);
        foreach ($query2->result() as $item) {
            $rendimiento_toner=$item->rendimiento_toner;
            $tipo=$item->tipo;
        }
        if ($consumible==30) {
           //log_message('error', 'rendimiento:'.$rendimiento_toner);
           //log_message('error', 'tipo:'.$tipo);
        }

        if ($rendimiento_toner>0) {
            if ($poliza>0) {
                if ($tipo==1) {
                    $costoporpaginamonocromatica=round($poliza/$rendimiento_toner, 2);
                    $costoporpaginacolor=0;
                }else{
                    $costoporpaginamonocromatica=0;
                    $costoporpaginacolor=round($poliza/$rendimiento_toner, 2);
                }
            }else{
                $costoporpaginamonocromatica=0;
                $costoporpaginacolor=0;
            }

        }else{
            $costoporpaginamonocromatica=0;
            $costoporpaginacolor=0;
        }

        $this->db->set(array('costo_pagina_monocromatica'=>$costoporpaginamonocromatica,'costo_pagina_color'=>$costoporpaginacolor));
        $this->db->where(array('idconsumible'=>$consumible));
        $this->db->update('equipos_consumibles');
    }
    public function getTipoCambioConsumibles()    {
        $sql = "SELECT *
                FROM tiposCambio WHERE categoria='consumibles'";
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function getPorcentajeGananciaConsumibles()    {
        $sql = "SELECT *
                FROM porcentajeGanancia WHERE categoria='consumibles'";
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function getDescuentosConsumibles()    {
        $sql = "SELECT *
                FROM descuentosCategorias WHERE categoria='consumibles'";
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function getConsumiblesPorIdEquipo($idEquipo,$bodega){
            if($bodega>0){
                $wherebodega=" and cob.bodegaId=$bodega ";
            }else{
                $wherebodega="";
            }
            if($idEquipo>0){
                $where_equipo=" AND ec.idEquipo = $idEquipo";
            }else{
                $where_equipo=" AND ec.idEquipo >0 group by c.id";
            }
        $sql = "SELECT c . * ,
                (select sum(cob.total) from consumibles_bodegas as cob where cob.consumiblesId=c.id $wherebodega) as stock,
                (select sum(cob.resguardar_cant) from consumibles_bodegas as cob where cob.consumiblesId=c.id $wherebodega) as stock_resg
                FROM consumibles AS c
                LEFT JOIN equipos_has_consumibles AS ec ON ec.idConsumibles = c.id
                WHERE c.status=1 $where_equipo";
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function getconsumiblesstock($idconsu,$bodega){
            if($bodega>0){
                $wherebodega=" and cob.bodegaId=$bodega ";
            }else{
                $wherebodega="";
            }
        $sql = "SELECT c . * ,
                (select sum(cob.total) from consumibles_bodegas as cob where cob.consumiblesId=c.id $wherebodega) as stock,
                (select sum(cob.resguardar_cant) from consumibles_bodegas as cob where cob.consumiblesId=c.id $wherebodega) as stock_resg
                FROM consumibles AS c
                WHERE c.status=1 and c.id='$idconsu' ";
        $query = $this->db->query($sql);
        return $query;
    }
    public function getrefaccionesPorIdEquipo($idEquipo,$bodega){
        if($bodega>0){
            $wherebodega=" and inr.bodegaId=$bodega ";
        }else{
            $wherebodega="";
        }
        $sql = "SELECT c . *,
                (select sum(inr.cantidad) from series_refacciones as inr where inr.refaccionid=c.id and inr.activo=1 and inr.status=0 $wherebodega) as stock,
                (select sum(inr.resguardar_cant) from series_refacciones as inr where inr.refaccionid=c.id and inr.activo=1 and inr.status=0 $wherebodega) as stock_resg
                FROM refacciones AS c
                LEFT JOIN equipos_has_refacciones AS ec ON ec.idRefacciones = c.id
                WHERE c.status=1 and ec.idEquipo =".$idEquipo;
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function getConsumibleConPrecios($idConsumible)    {
        $sql = "SELECT
                c.id,
                c.foto,
                c.modelo,
                c.parte,
                c.stock,
                c.observaciones,
                c.`status`,
                cc.id as 'idConsumibleCosto',
                cc.general,
                cc.iva,
                cc.neto,
                cc.frecuente,
                cc.iva2,
                cc.neto2,
                cc.especial,
                cc.iva3,
                cc.neto3,
                cc.poliza,
                cc.iva4,
                cc.neto4,
                cc.costo_pesos,
                cc.precioDolares
                FROM consumibles as c INNER JOIN consumibles_costos as cc ON cc.consumibles_id=c.id
                WHERE c.id=".$idConsumible;
        $query = $this->DB5->query($sql);
        return $query->result();
    }
    public function ConsumibleConPrecios($idConsumible)    {
        $sql = "SELECT
                c.id,
                c.foto,
                c.modelo,
                c.parte,
                c.stock,
                c.observaciones,
                c.`status`,
                cc.id as 'idConsumibleCosto',
                cc.general,
                cc.iva,
                cc.neto,
                cc.frecuente,
                cc.iva2,
                cc.neto2,
                cc.especial,
                cc.iva3,
                cc.neto3,
                cc.poliza,
                cc.iva4,
                cc.neto4,
                cc.costo_pesos,
                cc.precioDolares
                FROM consumibles as c INNER JOIN consumibles_costos as cc ON cc.consumibles_id=c.id
                WHERE c.id=".$idConsumible;
        $query = $this->DB5->query($sql);
        return $query->result();
    }
    public function getrefaccionesConPrecios($idConsumible)    {
        $sql = "SELECT
                c.id,
                c.foto,
                c.nombre,
                c.stock,
                c.observaciones,
                c.`status`,
                cc.id as 'idConsumibleCosto',
                cc.general,
                cc.iva,
                cc.neto,
                cc.frecuente,
                cc.iva2,
                cc.neto2,
                cc.especial,
                cc.iva3,
                cc.neto3,
                cc.poliza,
                cc.iva4,
                cc.neto4,
                cc.costo_pesos,
                cc.precioDolares
                FROM refacciones as c INNER JOIN refacciones_costos as cc ON cc.refacciones_id =c.id
                WHERE c.id=".$idConsumible;
        $query = $this->DB5->query($sql);
        return $query->result();
    }
    public function verificarequipo($data,$id){
        $sql = "SELECT * FROM equipos_has_consumibles WHERE idEquipo=$data AND  idConsumibles=$id";
        $query = $this->DB5->query($sql);
        return $query->result();
    }
    function getlistconsu($params){
        $paginaweb=$params['paginaweb'];
        $columns = array(
                0=>"c.id",
                1=>"c.modelo",
                2=>"c.parte",
                3=>"c.observaciones",
                4=>"c.id",
                5=>"c.destacado",
                6=>"c.rendimiento_unidad_imagen",
                7=>"c.rendimiento_toner",
                8=>"c.paginaweb",
                9=>"GROUP_CONCAT(eq.modelo SEPARATOR ', ') as equipos"
        );
        $columnsss = array(
                0=>"c.id",
                1=>"c.modelo",
                2=>"c.parte",
                3=>"c.observaciones",
                4=>"c.id",
                5=>"c.destacado",
                6=>"c.rendimiento_unidad_imagen",
                7=>"c.rendimiento_toner",
                8=>"c.paginaweb"
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('consumibles c');
        $this->db->join('equipos_has_consumibles ec', 'ec.idConsumibles=c.id','left');
        $this->db->join('equipos eq', 'eq.id=ec.idEquipo','left');
        
        $this->db->where(array('c.status'=>1));

        if($paginaweb>1){
            $this->db->where(array('c.paginaweb'=>1));
        }
        $this->db->group_by("c.id"); 
        //$this->db->where(array('facturaabierta'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            $nosearch=0;
            foreach($columnsss as $c){
                if ($nosearch!=2) {
                    $this->db->or_like($c,$search);
                }
                $nosearch++;
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columnsss[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    public function getlistconsut($params){
        $paginaweb=$params['paginaweb'];
        $columns = array(
                0=>"c.id",
                1=>"c.modelo",
                2=>"c.parte",
                3=>"c.observaciones",
                4=>"c.id",
                5=>"c.destacado",
                6=>"c.rendimiento_unidad_imagen",
                7=>"c.rendimiento_toner",
                8=>"c.paginaweb"
        );
        
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(*) as total');
        $this->db->from('consumibles c');
        $this->db->join('equipos_has_consumibles ec', 'ec.idConsumibles=c.id','left');
        $this->db->join('equipos eq', 'eq.id=ec.idEquipo','left');
        
        $this->db->where(array('c.status'=>1));

        if($paginaweb>1){
            $this->db->where(array('c.paginaweb'=>1));
        }
        $this->db->group_by("c.id"); 
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            $nosearch=0;
            foreach($columns as $c){
                if ($nosearch!=2) {
                    $this->db->or_like($c,$search);
                }
                $nosearch++;
            }
            $this->db->group_end();  
        }            
        $query=$this->db->get();
        return $query->num_rows();
    }
}

?>