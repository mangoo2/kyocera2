<?php 

// Validamos si es creación o edición, para así cargar los datos respectivamente
if(isset($refaciones_categoria))
{
    $idRefacciones = $refaciones_categoria->id;
    $foto = $refaciones_categoria->foto;
    $codigo = $refaciones_categoria->codigo;
    $nombre = $refaciones_categoria->nombre;
    $categoria = $refaciones_categoria->categoria;
    $stock = $refaciones_categoria->stock;
    $observaciones = $refaciones_categoria->observaciones;
    $precio_usa = $refaciones_categoria->precio_usa;
    $precio_unitario = $refaciones_categoria->precio_unitario;
    $precio_total = $refaciones_categoria->precio_total;
    $rendimiento = $refaciones_categoria->rendimiento;
}
else
{

 //   $idRefacciones = 0;
    $foto = '';
    $codigo = '';
    $nombre = '';
    $categoria = '';
    $stock = '';
    $observaciones = '';
    $precio_usa = 0;
    $precio_unitario = 0;
    $precio_total = 0;
    $rendimiento=0;
} 

if($tipoVista==1)
{
  $label = 'Formulario de Alta de Refacciones';
}
else if($tipoVista==2)
{
  $label = 'Formulario de Edición de Refacciones';
}
else if($tipoVista==3)
{
  $label = 'Formulario de Visualización de Refacciones';
}
?>
<style type="text/css">
    <?php if($this->perfilid!=1){ ?>
    .soloadministradores{
      display: none;
    }
  <?php } ?>
</style>
<!-- START CONTENT --> 
        <section id="content" width="100%">
          

          <!--start container-->
          <div class="container" width="100%">
            <div class="section">
                <h4 class="caption"><?php echo $label; ?></h4>
              <div class="row">
                <input id="base_url" type="hidden" name="base_url" value="<?php echo base_url(); ?>">
                 <input id="tipoVista" type="hidden" name="tipoVista" value="<?php echo $tipoVista; ?>">
                <!-- FORMULARIO -->
               
 
                  <div class="col s12 m12 24">

                    <div class="card-panel">

                      <div class="row">
                         <form class="form" id="refacciones_form" method="post">
                          <?php 
                            if(isset($refaciones_categoria)) 
                            {
                          ?>
                            <input id="idRefacciones" name="idRefacciones" type="hidden" value="<?php echo $idRefacciones; ?>">
                          <?php
                            }
                          ?>
                          <div class="row">
                            <div class="input-field col s3">
                               <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 230px; height: 210px; border-radius: 12px;">
                                            <?php if ($foto!=''){ ?>
                                              <input type="file" id="foto" name="foto" class="dropify" data-default-file="<?php echo base_url(); ?>uploads/refacciones/<?php echo $foto; ?>">
                                               <a class="btn btn-info" href="<?php echo base_url(); ?>uploads/refacciones/<?php echo $foto; ?>" target="_blank" >visualizar</a>
                                            <?php }else{?>  
                                              <input type="file" id="foto" name="foto" class="dropify">
                                            <?php }?>
                                            </div>
                               </div>
                            </div>
                            
                             <div class="col s9">
                              <label>Código / No. de Parte</label>
                              <input id="codigo" name="codigo" type="text" class="form-control-bmz"  placeholder="Código / No. de Parte" value="<?php echo $codigo; ?>" onchange="verificasiyaexiste(<?php if(isset($idRefacciones)){ echo $idRefacciones;}else{ echo 0;} ?>)">
                             </div>
                             <br>
                             <div class="col s9">
                              <label>Nombre</label>
                              <input id="nombre" name="nombre" type="text" placeholder="Nombre" class="form-control-bmz" value="<?php echo $nombre; ?>">
                            </div>
                            <div class="col s9">
                              <label>Rendimiento</label>
                              <input id="rendimiento" name="rendimiento" type="number" class="form-control-bmz" placeholder="rendimiento" value="<?php echo $rendimiento; ?>">
                            </div>
                            <br>
                            <div class="input-field col s5" style="display: none;">
                              <i class="material-icons prefix">local_library</i>
                                <select id="categoria" name="categoria" class="browser-default form-control-bmz">
                                    <?php foreach($categoria_refaccion as $item)
                                    { 
                                    ?>
                                      <option value="<?php echo $item->id?>" <?php if($item->id==$categoria) echo "selected";   ?> ><?php echo $item->nombre; ?></option>
                                    <?php }?>
                                </select>
                              <label for="categoria" class="active">Categoria</label>
                            </div>
                            <div class="input-field col s4" style="display: none;">
                              <i class="material-icons prefix">chrome_reader_mode</i>
                              <input id="stock" name="stock"   type="number" value="<?php echo $stock; ?>">
                              <label for="stock">Stock</label>
                            </div>
                          </div> 
                          <br>
                          <div class="row">
                            <div class="input-field col s12">
                              <i class="material-icons prefix">question_answer</i>
                              <textarea id="observaciones" name="observaciones" class="materialize-textarea"><?php echo $observaciones; ?></textarea>
                              <label for="message">Observaciones</label>
                            </div>
                          </div>
                          <!--
                          <hr class="divider"><h4 class="caption">Costos</h4></hr>
                          
                          <div class="row">
                            <div class="input-field col s4">
                              <i class="material-icons prefix">attach_money</i>
                              <input id="precio_usa" name="precio_usa"   type="number" step="any" value="<?php echo $precio_usa; ?>">
                              <label for="precio_usa">Precio USA</label>
                            </div>

                            <div class="input-field col s4">
                              <i class="material-icons prefix">attach_money</i>
                              <input id="precio_unitario" name="precio_unitario"  type="number" step="any" value="<?php echo $precio_unitario; ?>">
                              <label for="precio_unitario">Precio Unitario</label>
                            </div>

                            <div class="input-field col s4">
                              <i class="material-icons prefix">monetization_on</i>
                              <input id="precio_total" name="precio_total"   type="number" step="any" value="<?php echo $precio_total; ?>">
                              <label for="precio_total">Precio Total</label>
                            </div>
                          </div>
                          -->

                          <hr class="divider"><h4 class="caption">Compatibilidad de impresoras</h4></hr>

                          <div class="row">
                              <div class="col s4">

                                <?php  
                                $i=0;
                                $size=1;
                                do{
                                ?>
                                <div  class="border" id="datos1">
                                  <div class="input-field col s10">
                                  
                                    <select id="equipos" name="equipos" class="browser-default chosen-select" onchange="validarequipo()">
                                       <option value="" disabled selected>Selecciona una opción</option>
                                        <?php foreach($equipo as $item){?> 
                                            <option value="<?php echo $item->id;?>" ><?php echo $item->modelo;?></option>
                                        <?php } ?>
                                    </select>
                                 <!-- 
                                 <input type="text" name=""> 
                                  <label for="equipos">Equipo</label>
                                  -->
                                  </div>
                                  <div class="input-field col s2 div_btn">
                                  <?php if($i==0){ ?>
                                  <a class="btn btn-floating waves-effect waves-light cyan btn_agregar"> <i class="material-icons">add</i></a>
                                  <?php } else {?>
                                  <a class="btn btn-floating waves-effect waves-light red btn_eliminar"><i class="material-icons">delete_forever</i></a>
                                  <?php }?>
                                  </div>

                                </div>
                                <?php 
                                $i++;
                                }while($i<$size);
                                ?>
                                
                              </div> 
                              <div class="col s6">
                                <?php 
                                if($tipoVista!=1)
                                {
                                ?> 
                                          <table class="table table-striped table-condensed table-hover table-responsive" id="tabla_equipos" name="tabla_equipos">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Nombre</th>
                                                    <th>idRefacciones</th>
                                                    <th></th>
                                                </tr>
                                            </thead>

                                            <tbody >
                                            </tbody>
                                            
                                          </table> 
                               
                                <?php } ?>
                              </div>   
                            </div>



                          <div class="row">
                            <div class="input-field col s12">
                              <button class="btn cyan waves-effect waves-light right buttonsave" type="submit">Guardar
                                <i class="material-icons right">send</i>
                              </button>
                            </div>
                          </div>
                        </form>
                        
                      </div>

                    </div>

                  </div>
                
              </div>
            </div>
          </div>
        </section>