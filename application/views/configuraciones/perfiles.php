<!-- START CONTENT -->
    <section id="content">
      <!--start container-->
        <div class="container">
          <div class="section">

          <input id="base_url" type="hidden" name="base_url" value="<?php echo base_url(); ?>">

          <div id="table-datatables">
            <br>
                <h4 class="header">Perfiles</h4>
                <div class="row">
                  <div class="col s4">
                    <div class="card-panel">
                      <div class="row">
                        <div class="col s12">
                          <a class="waves-effect green btn-bmz" onclick="addperfil(0,'')" style="float: right;">Nuevo</a>
                        </div>
                        <div class="col s12">
                          <table class="table table-bordered" id="table-perfil">
                            <thead>
                              <tr>
                                <th>#</th>
                                <th>Perfil</th>
                                <th></th>
                              </tr>
                            </thead>
                            <tbody class="tbody-perfil">
                              
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div> 
                  <div class="col s8">
                    <div class="card-panel">
                      <div class="row">
                        <div class="col s3">Perfil</div>
                        <div class="col s8">
                          <select class="form-control-bmz browser-default" id="selectedperfiles" onchange="viepermisos()">
                            
                          </select>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col s6">
                          <table class="table table-bordered" id="table-menu">
                            <thead>
                              <tr>
                                <th>#</th>
                                <th>Menu</th>
                                <th>Submenu</th>
                                <th></th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php 
                                $rowmenu=1;
                              foreach ($resultmenu->result() as $item) { ?>
                                <tr>
                                  <td><?php echo $rowmenu;?></td>
                                  <td><?php echo $item->menu;?></td>
                                  <td><?php echo $item->submenu;?></td>
                                  <td>
                                    <a class="waves-effect btn-bmz blue" 
                                       onclick="agregarpermiso(<?php echo $item->MenusubId;?>)"><i class="fas fa-arrow-right"></i></a>
                                  </td>
                                </tr>
                              <?php $rowmenu++;
                              } ?>
                            </tbody>
                          </table>
                        </div>
                        <div class="col s6">
                          <table class="table table-bordered" id="table-submenu">
                            <thead>
                              <tr>
                                <th>Menu</th>
                                <th>Submenu</th>
                                <th></th>
                              </tr>
                            </thead>
                            <tbody class="tbody-submenu">
                              
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>    
                </div>
                
                      
              </div>
            </div>  
               </div>
        </section>

<div id="modaladdupdateperfil" class="modal">
  <div class="modal-content ">
    <div class="row">
      <div class="col s4">
        <input type="hidden" id="perfilId">
          Perfil
      </div>
      <div class="col s8">
        <input type="text" id="nombre" class="form-control-bmz">
      </div>                
    </div>
    
    
                
  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
    <a href="#!" class="modal-action  waves-effect waves-green green btn-flat  " onclick="inserupdateperfil()">Aceptar</a>
  </div>
</div>