var base_url = $('#base_url').val();
var table;
var editarar=0;
$(document).ready(function($) {
	$('.chosen-select').chosen({width: "100%"});
	$('#pcliente').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una opción',
        allowClear: true,
        ajax: {
            url: base_url+'Ventasi/searchclientes',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.id,
                        text: element.empresa

                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function (e) {
          load();
    });	
    table = $('#tabla_asignar').DataTable({
        responsive: !0
    });
    load();
   
});

function load(){
	var serv = $('#serv option:selected').val();
	if (serv==1) {
		loadcontrato();
	}else if(serv==2){
		loadpoliza();
	}else if(serv==3){
        loadcliente();
    }else if(serv==4){
        loadventas();
    }else{
        //table.destroy();
        table = $('#tabla_asignar').DataTable();
    }
}
function loadcontrato(){
    table.destroy();
    var tiposer = $('#tiposer option:selected').val();
	var tecnico = $('#tecnico option:selected').val();
	var status = $('#status option:selected').val();
	var zona = $('#zona option:selected').val();
	var pcliente = $('#pcliente option:selected').val();
		if(pcliente==undefined){
		    pcliente=0;
		}
    var fechainicio = $('#fechainicio').val();
    var fechafin = $('#fechafin').val();
    table = $('#tabla_asignar').DataTable({
        fixedHeader: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            url: base_url + "index.php/Listaservicios/getlistaservicioc",
            type: "post",
            "data": {
                "tiposer":tiposer,
				"tecnico":tecnico,
				"status":status,
				"zona":zona,
				"pcliente":pcliente,
                "fechainicio":fechainicio,
                "fechafin":fechafin,
                'estatus_ad':1,
                'noti':0
            },
            error: function() {
                $("#tabla_asignar").css("display", "none");
            }
        },
        "columns": [
            {"data": "asignacionIde"},
            {"data": "servicio"},
            {"data": "empresa"},
            {"data": "fecha"},
            {"data": null,
                render: function(data,type,row){ 
                        var html=row.hora+' a '+row.horafin;
                    return html;
                }
            },
            {"data": "comentario"},
            {"data": "modelo"},
            {"data": "serie"},
            {"data": null,
                render: function(data,type,row){ 
                    if (row.status==0) {
                        var status='<p></p>';
                    }
                    if (row.status==1) {
                        var status='<p>En proceso</p>';
                    }
                    if (row.status==2) {
                        var status='<p style="color:green">Finalizado</p>';
                    }
                    if (row.status==3) {
                        var status='<p style="color:red">Suspendido</p>';
                    }
                        //var nombre="'"+row.modelo+"'";
                        var html=status;
                    

                    return html;
                }
            },
            {"data": null,
                render: function(data,type,row){ 
                    
                        //var nombre="'"+row.modelo+"'";
                        var color_btn='';
                        if(row.t_fecha==null){
                            color_btn='cyan';
                        }else{
                            color_btn='red';
                        }
                        var html='';
                        	
                        	
                        	
                            html+='<a class="b-btn b-btn-primary" onclick="venta('+row.asignacionId+',1)">\
                                    <i class="fas fa-cart-plus" style="font-size: 20px;"></i></a>';
                            if(row.status==1 || row.status==2){
                                html+='<a class="b-btn b-btn-primary s_status_'+row.asignacionIde+'_1"\
                                 data-h_ser_i="'+row.horaserinicio+'" data-h_ser_f="'+row.horaserfin+'"\
                                 data-h_ser_iex="'+row.horaserinicioext+'" data-h_ser_fex="'+row.horaserfinext+'"\
                                   onclick="modal_estatus('+row.asignacionIde+',1)">\
                                    <i class="fas fa-exclamation-circle" style="font-size: 20px;"></i></a>';        
                            }        
                            html+='<a class="b-btn b-btn-primary" onclick="documento('+row.asignacionId+',1,'+row.asignacionIde+')">\
                                    <i class="fas fa-file-alt" style="font-size: 20px;"></i></a>';

                            html+='<a class="b-btn b-btn-primary edit_comentario_'+row.asignacionId+'" onclick="edit_comentario('+row.asignacionId+',1,'+row.asignacionIde+')" data-comen="'+row.comentario+'"><i class="fas fa-pencil-alt"></i></a>';
 
                    

                    return html;
                }
            },
          ],
        order: [
            [0, "desc"]
          ]
        
    });
    $('.cambiartitulo_8').html('Equipo');
}
function loadpoliza(){
    table.destroy();
    var tiposer = $('#tiposer option:selected').val();
	var tecnico = $('#tecnico option:selected').val();
	var status = $('#status option:selected').val();
	var zona = $('#zona option:selected').val();
	var pcliente = $('#pcliente option:selected').val();
		if(pcliente==undefined){
		    pcliente=0;
		}
	var fechainicio = $('#fechainicio').val();
    var fechafin = $('#fechafin').val();
    table = $('#tabla_asignar').DataTable({
        fixedHeader: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            url: base_url + "index.php/Listaservicios/getlistaserviciop",
            type: "post",
            "data": {
                "tiposer":tiposer,
				"tecnico":tecnico,
				"status":status,
				"zona":zona,
				"pcliente":pcliente,
                "fechainicio":fechainicio,
                "fechafin":fechafin,
                'estatus_ad':1,
                'noti':0
            },
            error: function() {
                $("#tabla_asignar").css("display", "none");
            }
        },
        "columns": [
            {"data": "asignacionIde"},
            {"data": "servicio"},
            {"data": "empresa"},
            {"data": "fecha"},
            {"data": null,
                render: function(data,type,row){ 
                        var html=row.hora+' a '+row.horafin;
                    return html;
                }
            },
            {"data": "comentario"},
            {"data": "modelo"},
            {"data": "serie"},
            {"data": null,
                render: function(data,type,row){ 
                    if (row.status==0) {
                        var status='<p></p>';
                    }
                    if (row.status==1) {
                        var status='<p>En proceso</p>';
                    }
                    if (row.status==2) {
                        var status='<p style="color:green">Finalizado</p>';
                    }
                    if (row.status==3) {
                        var status='<p style="color:red">Suspendido</p>';
                    }
                        //var nombre="'"+row.modelo+"'";
                        var html=status;
                    

                    return html;
                }
            },
            {"data": null,
                render: function(data,type,row){ 
                        var color_btn='';
                        if(row.t_fecha==null){
                            color_btn='cyan';
                        }else{
                            color_btn='red';
                        }
                        //var nombre="'"+row.modelo+"'";
                        var html='';
                    		
                        	
                        	
                            html+='<a class="b-btn b-btn-primary" onclick="venta('+row.asignacionId+',2)">\
                                    <i class="fas fa-cart-plus" style="font-size: 20px;"></i></a>';
                            if(row.status==1 || row.status==2){
                                html+='<a class="b-btn b-btn-primary s_status_'+row.asignacionIde+'_2"\
                                 data-h_ser_i="'+row.horaserinicio+'" data-h_ser_f="'+row.horaserfin+'"\
                                  data-h_ser_iex="'+row.horaserinicioex+'" data-h_ser_fex="'+row.horaserfinex+'"\
                                   onclick="modal_estatus('+row.asignacionIde+',2)">\
                                    <i class="fas fa-exclamation-circle" style="font-size: 20px;"></i></a>';        
                            } 
                            html+='<a class="b-btn b-btn-primary" onclick="documento('+row.asignacionId+',2,'+row.asignacionIde+')">\
                                    <i class="fas fa-file-alt" style="font-size: 20px;"></i></a>';  
                            html+='<a class="b-btn b-btn-primary edit_comentario_'+row.asignacionId+'" onclick="edit_comentario('+row.asignacionId+',2,'+row.asignacionIde+')" data-comen="'+row.comentario+'"><i class="fas fa-pencil-alt"></i></a>';  
                    return html;
                }
            },
          ],
        order: [
            [0, "desc"]
          ]
        
    });
    $('.cambiartitulo_8').html('Equipo');
}
function loadcliente(){
    table.destroy();
    var tiposer = $('#tiposer option:selected').val();
    var tecnico = $('#tecnico option:selected').val();
    var status = $('#status option:selected').val();
    var zona = $('#zona option:selected').val();
    var pcliente = $('#pcliente option:selected').val();
        if(pcliente==undefined){
            pcliente=0;
        }
    var fechainicio = $('#fechainicio').val();
    var fechafin = $('#fechafin').val();
    table = $('#tabla_asignar').DataTable({
        fixedHeader: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            url: base_url + "index.php/Listaservicios/getlistaserviciocl",
            type: "post",
            "data": {
                "tiposer":tiposer,
                "tecnico":tecnico,
                "status":status,
                "zona":zona,
                "pcliente":pcliente,
                "fechainicio":fechainicio,
                "fechafin":fechafin,
                'estatus_ad':1,
                'noti':0
            },
            error: function() {
                $("#tabla_asignar").css("display", "none");
            }
        },
        "columns": [
            {"data": "asignacionId"},
            {"data": "servicio"},
            {"data": "empresa"},
            {"data": "fecha"},
            {"data": null,
                render: function(data,type,row){ 
                        var html=row.hora+' a '+row.horafin;
                    return html;
                }
            },
            {"data": "comentario"},
            {"data": "servicio"},
            {"data": 'serie'},
            {"data": null,
                render: function(data,type,row){ 
                    if (row.status==0) {
                        var status='<p></p>';
                    }
                    if (row.status==1) {
                        var status='<p>En proceso</p>';
                    }
                    if (row.status==2) {
                        var status='<p style="color:green">Finalizado</p>';
                    }
                    if (row.status==3) {
                        var status='<p style="color:red">Suspendido</p>';
                    }
                        //var nombre="'"+row.modelo+"'";
                        var html=status;
                    

                    return html;
                }
            },
            {"data": null,
                render: function(data,type,row){ 
                        var color_btn='';
                        if(row.t_fecha==null){
                            color_btn='cyan';
                        }else{
                            color_btn='red';
                        }
                        //var nombre="'"+row.modelo+"'";
                        var html='';
                            
                            
                            
                            html+='<a class="b-btn b-btn-primary" onclick="venta('+row.asignacionId+',3)">\
                                    <i class="fas fa-cart-plus" style="font-size: 20px;"></i></a>';
                            if(row.status==1 || row.status==2){
                                html+='<a class="b-btn b-btn-primary s_status_'+row.asignacionIdd+'_3"\
                                         data-h_ser_i="'+row.horaserinicio+'" data-h_ser_f="'+row.horaserfin+'"\
                                         data-h_ser_iex="'+row.horaserinicio+'" data-h_ser_fex="'+row.horaserfin+'"\
                                         onclick="modal_estatus('+row.asignacionIdd+',3)">\
                                    <i class="fas fa-exclamation-circle" style="font-size: 20px;"></i></a>';        
                            }     
                            html+='<a class="b-btn b-btn-primary" onclick="documento('+row.asignacionId+',3,0)">\
                                    <i class="fas fa-file-alt" style="font-size: 20px;"></i></a>';
                            html+='<a class="b-btn b-btn-primary edit_comentario_'+row.asignacionId+'" onclick="edit_comentario('+row.asignacionId+',3,'+row.asignacionIdd+')" data-comen="'+row.comentario+'"><i class="fas fa-pencil-alt"></i></a>';  
                    return html;
                }
            },
          ],
        order: [
            [0, "desc"]
          ]
        
    });
    $('.cambiartitulo_8').html('Equipo/Servicio');
}
function loadventas(){
    table.destroy();
    var tiposer = $('#tiposer option:selected').val();
    var tecnico = $('#tecnico option:selected').val();
    var status = $('#status option:selected').val();
    var zona = $('#zona option:selected').val();
    var pcliente = $('#pcliente option:selected').val();
        if(pcliente==undefined){
            pcliente=0;
        }
    var fechainicio = $('#fechainicio').val();
    var fechafin = $('#fechafin').val();
    table = $('#tabla_asignar').DataTable({
        fixedHeader: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            url: base_url + "index.php/Listaservicios/getlistaserviciovent",
            type: "post",
            "data": {
                "tiposer":tiposer,
                "tecnico":tecnico,
                "status":status,
                "zona":zona,
                "pcliente":pcliente,
                "fechainicio":fechainicio,
                "fechafin":fechafin,
                'estatus_ad':1,
                'noti':0
            },
            error: function() {
                $("#tabla_asignar").css("display", "none");
            }
        },
        "columns": [
            {"data": "asignacionId"},
            {"data": "servicio"},
            {"data": "empresa"},
            {"data": "fecha"},
            {"data": null,
                render: function(data,type,row){ 
                        var html=row.hora+' a '+row.horafin;
                    return html;
                }
            },
            {"data": "comentario"},
            {"data": null,"visible": false},
            {"data": null,"visible": false},
            {"data": null,
                render: function(data,type,row){ 
                    if (row.status==0) {
                        var status='<p></p>';
                    }
                    if (row.status==1) {
                        var status='<p>En proceso</p>';
                    }
                    if (row.status==2) {
                        var status='<p style="color:green">Finalizado</p>';
                    }
                    if (row.status==3) {
                        var status='<p style="color:red">Suspendido</p>';
                    }
                        //var nombre="'"+row.modelo+"'";
                        var html=status;
                    

                    return html;
                }
            },
            {"data": null,
                render: function(data,type,row){ 
                        var color_btn='';
                        if(row.t_fecha==null){
                            color_btn='cyan';
                        }else{
                            color_btn='red';
                        }
                        //var nombre="'"+row.modelo+"'";
                        var html='';
                            
                            html+='</a>';
                            
                            
                            html+='<a class="b-btn b-btn-primary" onclick="venta('+row.asignacionId+',4)">\
                                    <i class="fas fa-cart-plus" style="font-size: 20px;"></i></a>';
                            if(row.status==1 || row.status==2){
                                html+='<a class="b-btn b-btn-primary s_status_'+row.asignacionId+'_4"\
                                 data-h_ser_i="'+row.horaserinicio+'"\
                                  data-h_ser_f="'+row.horaserfin+'"\
                                 data-h_ser_iex="'+row.horaserinicioext+'"\
                                  data-h_ser_fex="'+row.horaserfinext+'"\
                                    onclick="modal_estatus('+row.asignacionId+',4)">\
                                    <i class="fas fa-exclamation-circle" style="font-size: 20px;"></i></a>';        
                            }     
                            html+='<a class="b-btn b-btn-primary" onclick="documento('+row.asignacionId+',4,0)">\
                                    <i class="fas fa-file-alt" style="font-size: 20px;"></i></a>';
                            html+='<a class="b-btn b-btn-primary edit_comentario_'+row.asignacionId+'" onclick="edit_comentario('+row.asignacionId+',4,0)" data-comen="'+row.comentario+'"><i class="fas fa-pencil-alt"></i></a>'; 
                    return html;
                }
            },
          ],
        order: [
            [0, "desc"]
          ]
        
    });
}
function ChangeDate(data){
  var parte=data.split('-');
  return parte[2]+"-"+parte[1]+"-"+parte[0];
}
function tConvert(time){
  // Check correct time format and split into components
  time = time.toString ().match (/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

  if (time.length > 1) { // If time format correct
    time = time.slice (1);  // Remove full string match value
    time[5] = +time[0] < 12 ? 'AM' : 'PM'; // Set AM/PM
    time[0] = +time[0] % 12 || 12; // Adjust hours
  }
  return time.join (''); // return adjusted time or original string
}
function viewhistorial(idrow,table){
    $.ajax({
        type:'POST',
        url: base_url+"Listaservicios/viewhistorial",
        data: {
            idrow: idrow,
            table: table
        },
        success: function (data){
            $('.historicotecnicos').html(data);
        }
    });
}
//======================================
function venta(id,tipo){
    $('.totalg').html(0);
    $('.modal').modal();
    $('#modalventa').modal('open');
    viewdatosventaservicio(id,tipo);
}
function viewdatosventaservicio(id,tipo){
    editarar=0;
    $('#vmidasignacion').val(id);
    $('#vmidasignaciontipo').val(tipo);
    $('.tabaddconsul').html('');
    $('.taaddrefaccion').html('');
    $.ajax({
        type:'POST',
        url: base_url+"Listaservicios/datosventaservicio",
        data: {
          id:id,
          tipo:tipo
        },
        success: function (data){
          console.log(data);
          var array = $.parseJSON(data);
          var datos=array.datos;          
          array.consumibles.forEach(function(element) {
                editarar=1;
                var descripcion="'"+element.modelo+" ("+element.parte+")'";
                addconsuview(element.asId,element.cantidad,element.id,descripcion,element.costo);
          });
          array.refacciones.forEach(function(element) {
                editarar=1;
                var descripcion="'"+element.nombre+" ("+element.codigo+")'";
                addrefaview(element.asId,element.cantidad,element.id,descripcion,element.costo);
          });
   
      
        }
    }); 
}
function funcionConsumiblesprecios(aux){
    var toner = $('#sconcumibles option:selected').val();
    
    $.ajax({
        url: base_url+"index.php/Cotizaciones/getDatosConsumiblesPorEquipop/"+toner,
        success: function (response){
            var array = $.parseJSON(response); 
            $('#sconcumiblesp').html('');
            $('#sconcumiblesp').html(array.select_option);
            //aux.find('.tonerp').html('');
            //aux.find('.tonerp').html(response);
            $('#sconcumiblesp').trigger("chosen:updated");
        },
        error: function(response){
            alertfunction('Atención!','Algo salió mal, intente de nuevo o contacte al administrador del sistema');
        }
    }); 
}
function selectprecior(){
    var id = $('#srefaccions option:selected').val();
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Cotizaciones/mostrarselectpreciosr",
        data: {id:id},
        success: function (data){
            var array = $.parseJSON(data); 
            $('#srefaccionp').html('');
            $('#srefaccionp').html(array.select_option);   
        },
        error: function(data){ 
            alertfunction('Atención!','Algo salió mal, intente de nuevo o contacte al administrador del sistema');
        }
    });
}
var rowventa=0;
function addconsuview(row,cant,con,cont,precio){
    var tabless ='<tr class="rowventa_'+rowventa+'_'+row+'">\
                <td><input id="addcantidadc" value="'+cant+'" style="background: transparent;border: 0px;margin: 0px;width: 92px;" >\
                <input type="hidden" id="idtable" value="'+row+'" style="">\
                </td>\
                <td><input type="hidden" id="addcantidadr" value="'+con+'" style="">'+cont+'</td>\
                <td><input id="addcantidadp" value="'+precio+'" class="precio" style="background: transparent;border: 0px;margin: 0px;width: 92px;" readonly></td>\
                <td>\
                  <a class="btn-floating red tooltipped elimina" onclick="deteteven('+rowventa+','+row+',1)"><i class="material-icons">delete_forever</i></a></td>\
              </tr>';
  $('.tabaddconsul').append(tabless);
  rowventa++;
  calculartotal();
}
function addrefaview(row,cant,ref,reft,precio){
    var tabless ='<tr class="rowventa_'+rowventa+'_'+row+'">\
                <td><input id="addrefaccionc" value="'+cant+'" style="background: transparent;border: 0px;margin: 0px;width: 92px;" >\
                <input type="hidden" id="idtable" value="'+row+'" style="">\
                </td>\
                <td><input type="hidden" id="addrefaccionr" value="'+ref+'" style="">'+reft+'</td>\
                <td><input id="addrefaccionp" value="'+precio+'" class="precio" style="background: transparent;border: 0px;margin: 0px;width: 92px;" readonly></td>\
                <td>\
                  <a class="btn-floating red tooltipped elimina" onclick="deteteven('+rowventa+','+row+',2)"><i class="material-icons">delete_forever</i></a></td>\
              </tr>';
    $('.taaddrefaccion').append(tabless);
    rowventa++;
    calculartotal();
}
function deteteven(row,rowid,tipo){
  
  
  if (rowid>0) {
    deleteacre(row,rowid,tipo);
  }else{
    $('.rowventa_'+row+'_'+rowid).remove();
    calculartotal();
  }
}
function calculartotal(){
    var addtp = 0;
    $(".precio").each(function() {
        var vstotal = $(this).val();
        addtp += Number(vstotal);
    });
    $('.totalg').html(new Intl.NumberFormat('es-MX').format(addtp));
}
function deleteacre(row,rowid,tipo){
    $.confirm({
            boxWidth: '30%',
            useBootstrap: false,
            icon: 'fa fa-warning',
            title: 'Atención!',
            content: '¿Está seguro de Eliminar?',
            type: 'red',
            typeAnimated: true,
            buttons: 
            {
                confirmar: function (){
                    //===============
                    $.ajax({
                          type:'POST',
                          url: base_url+'index.php/Listaservicios/deleteacre',
                          data: {
                            rowid:rowid,
                            tipo:tipo
                            },
                          success:function(data){
                              $.alert({
                                boxWidth: '30%',
                                useBootstrap: false,
                                title: 'Atención!',
                                content: 'Eliminado correctamente'}
                              );
                                $('.rowventa_'+row+'_'+rowid).remove();
                                calculartotal();
                          }
                      });
                    //=============
            },
                cancelar: function () 
                {
                }
            }
        });
}
function savemodalventa(){
    //=============================================
              var consumi = [];
              var TABLAc   = $("#tableaddconsulmibles tbody > tr");
                TABLAc.each(function(){  
                        itemc = {};
                        itemc['id'] = $(this).find("input[id*='idtable']").val();
                        itemc['cantidad'] = $(this).find("input[id*='addcantidadc']").val();
                        itemc['consumible'] = $(this).find("input[id*='addcantidadr']").val();
                        itemc['precio'] = $(this).find("input[id*='addcantidadp']").val();
                        
                        consumi.push(itemc);
                });
                aInfoc   = JSON.stringify(consumi);
              //=============================================
              //=============================================
              var refaccion = [];
              var TABLAc   = $("#tableaddrefacciones tbody > tr");
                TABLAc.each(function(){  
                        itemc = {};
                        itemc['id'] = $(this).find("input[id*='idtable']").val();
                        itemc['cantidad'] = $(this).find("input[id*='addrefaccionc']").val();
                        itemc['refaccion'] = $(this).find("input[id*='addrefaccionr']").val();
                        itemc['precio'] = $(this).find("input[id*='addrefaccionp']").val();
                        
                        refaccion.push(itemc);
                });
                aInfor   = JSON.stringify(refaccion);
              //=============================================
          var tasign = $('input[name=tasign]:checked').val();//1pre asignacion 2 asginacion
          var tipo = $('input[name=prasignaciontipo]:checked').val();//1 contrato 2 poliza
            $.ajax({
                  type:'POST',
                  url: base_url+'index.php/Listaservicios/editarventaservicio',
                  data: {
                    tasign:$('#vmidasignacion').val(),
                    tipo:$('#vmidasignaciontipo').val(),
                    vconsumible:aInfoc,
                    vrefaccion:aInfor
                    },
                  success:function(data){
                      $.alert({
                        boxWidth: '30%',
                        useBootstrap: false,
                        title: 'Atención!',
                        content: 'Edicion correcta'}
                      );
                      $('#modalventa').modal('close');
                  }
              });
}
function modal_estatus(idrow,tipo){
    $('.text_h_i').html('');
    $('.text_h_f').html('');
    $('.text_h_t').html('');

    $('.text_h_iex').html('');
    $('.text_h_fex').html('');
    $('.text_h_t').html('');

    var text_h_i = $('.s_status_'+idrow+'_'+tipo).data('h_ser_i');
    var text_h_f = $('.s_status_'+idrow+'_'+tipo).data('h_ser_f');
    

    var text_h_iex = $('.s_status_'+idrow+'_'+tipo).data('h_ser_iex');
    var text_h_fex = $('.s_status_'+idrow+'_'+tipo).data('h_ser_fex');
    if (text_h_iex==null || text_h_iex=='undefined') {
        $('.tiempoextras').hide();
    }else{
        $('.tiempoextras').show();
    }
    $('#modal_lis_ser').modal();
    $('#modal_lis_ser').modal('open');
    if(text_h_i!=null || text_h_i!='undefined'){
        $('.text_h_i').html(tConvert(text_h_i));
    }
    if(text_h_f!=null || text_h_f!='undefined'){
        $('.text_h_f').html(tConvert(text_h_f));
        var diferencia = diferenciastiempo(text_h_f,text_h_i);
        $('.text_h_t').html(diferencia);
    }

    if(text_h_iex!=null || text_h_iex!='undefined'){
        $('.text_h_iex').html(tConvert(text_h_iex));
    }
    if(text_h_fex!=null || text_h_fex!='undefined'){
        $('.text_h_fex').html(tConvert(text_h_fex));
        var diferenciaex = diferenciastiempo(text_h_fex,text_h_iex);
        $('.text_h_tex').html(diferenciaex);
    }
}
function documento(idasig,tipo,equipo){
    if(tipo==1){
        window.open(base_url+"index.php/Pdfview?file=Listaservicios/contrato_formato/"+idasig+"/"+tipo+"/"+equipo, "Prefactura", "width=780, height=612");
    }else if(tipo==2){
        window.open(base_url+"index.php/Pdfview?file=Listaservicios/tipo_formato/"+idasig+"/"+tipo+"/"+equipo, "Prefactura", "width=780, height=612");
    }else if(tipo==3){
        window.open(base_url+"index.php/Pdfview?file=Listaservicios/tipo_formatoc/"+idasig+"/"+tipo+"/"+equipo, "Prefactura", "width=780, height=612");
    }else if(tipo==4){
        window.open(base_url+"index.php/Pdfview?file=Listaservicios/tipo_formatovent/"+idasig+"/"+tipo+"/"+equipo, "Prefactura", "width=780, height=612");
    }
} 
function diferenciastiempo(horainicio,horafin){
    var hora1 = horainicio.split(":"),
        hora2 = horafin.split(":"),
        
        t1 = new Date(),
        t2 = new Date();
 
        t1.setHours(hora1[0], hora1[1], hora1[2]);
        t2.setHours(hora2[0], hora2[1], hora2[2]);
 
        //Aquí hago la resta
        t1.setHours(t1.getHours() - t2.getHours(), t1.getMinutes() - t2.getMinutes(), t1.getSeconds() - t2.getSeconds());
 
        //Imprimo el resultado
       return "" + (t1.getHours() ? t1.getHours() + (t1.getHours() > 1 ? " horas" : " hora") : "") + (t1.getMinutes() ? ", " + t1.getMinutes() + (t1.getMinutes() > 1 ? " minutos" : " minuto") : "") + (t1.getSeconds() ? (t1.getHours() || t1.getMinutes() ? " y " : "") + t1.getSeconds() + (t1.getSeconds() > 1 ? " segundos" : " segundo") : "");
}
function edit_comentario(asig,tipo,asigd){
    var comen=$('.edit_comentario_'+asig).data('comen');
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Desea cambiar el comentario del servicio?<br>Se necesita permisos de administrador<br>'+
                 '<input type="text" placeholder="Contraseña" id="contrasena" name="contrasena" class="name form-control-bmz" required style="width: 99%;"/><br>\
                 <label>Comentario</label><textarea id="comen_new" class="form-control-bmz" style="min-height:98px;">'+comen+'</textarea>',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var pass=$("input[name=contrasena]").val();
                var comenn=$("#comen_new").val();
                if (pass!='') {
                    $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Sistema/solicitarpermiso",
                        data: {
                            pass:pass
                        },
                        success: function (response){
                                var respuesta = parseInt(response);
                                if (respuesta==1) {
                    //=============================================================================
                                    $.ajax({
                                          type:'POST',
                                          url: base_url+'index.php/Listaserviciosh/editcomen',
                                          data: {
                                            asig:asig,
                                            tipo:tipo,
                                            asigd:asigd,
                                            coment:comenn
                                            },
                                          success:function(data){
                                              
                                              toastr.success('Comentario acualizado correctamente');
                                                load();
                                          }
                                    });
                    //=============================================================================
                                }else{
                                    notificacion(2);
                                }
                        },
                        error: function(response){  
                             notificacion(1);
                        }
                    });

                }else{
                    notificacion(0);
                }
            },
            cancelar: function (){
                
            }
        }
    });
    setTimeout(function(){
        new MaskedPassword(document.getElementById("contrasena"), '\u25CF');
    },1000);
}