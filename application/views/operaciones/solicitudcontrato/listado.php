<!-- START CONTENT -->
    <section id="content">
    	<!--start container-->
      	<div class="container">
      		<div class="section">
<div class="card-panel">
      		<input id="base_url" type="hidden" name="base_url" value="<?php echo base_url(); ?>">

      		<div id="table-datatables">
      			<br>
                <h4 class="header">Listado Solicitudes de Contrato</h4>
                <div class="row">
                  <div class="col s12">

                    <table id="tabla_solicitudcontrato" class="responsive-table display" cellspacing="0">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Cliente</th>
                          <th>Modelo</th>
                          <th>Área</th>
                          <th>Plaza</th>
                          <th>Costo por clic</th>
                          <th>Excedente</th>
                          <td>Cotización de:</td>
                          <td></td>
                          <td></td>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            </div>  
               </div>
        </section>