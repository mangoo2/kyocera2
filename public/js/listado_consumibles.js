var base_url = $('#base_url').val();
var equipo_infog;
$(document).ready(function(){
	table = $('#tabla_consumibles').DataTable();
	load();

    // Validar en la carga masiva, que el tipo del archivo sea XLSX
    // Si no lo es, no lo dejará avanzar
    $('#inputFile').change(function () {
        var ext = this.value.match(/\.(.+)$/)[1];
        switch (ext) 
        {
            case 'xlsx':
                $('#uploadButton').attr('disabled', false);
            break;
            default:
                $.alert({
                                boxWidth: '30%',
                                useBootstrap: false,
                                title: 'Atención!',
                                content: 'No es el tipo de archivo permitido.'});  
                $('#uploadButton').attr('disabled', true);
            break;
        }
    });

    // Enviar el archivo para carga masiva vía AJAX
    $("#archivo-form").on("submit", function(e){
        e.preventDefault();
        var formData = new FormData(document.getElementById("archivo-form"));
        $.ajax({
                url: base_url+"index.php/consumibles/cargaArchivo",
                type: "post",
                data: formData,
                dataType: "html",
                cache: false,
                contentType: false,
                processData: false,
                beforeSend: function() 
                {
                    document.getElementById('loading').style.visibility = "visible";
                },
                success: function (response) 
                {
                    $.alert({
                                boxWidth: '30%',
                                useBootstrap: false,
                                title: 'Éxito!',
                                content: 'Archivo cargado correctamente!'});
                    //alert("");
                            setTimeout(function(){ 
                                window.location.href = base_url+"index.php/consumibles";
                                //location.reload(); 
                            }, 2000);
                },
                error:function()
                {
                    toastr.error("Error Inesperado, intente de nuevo");
                }
        });
    });
    filesupload();
    $('.modal').modal();
});

// Eliminar consumible, usando JQUERY CONFIRM
function confirmaEliminarConsumible(idConsumible){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de Eliminar este Consumible? El cambio será irreversible',
        type: 'red',
        typeAnimated: true,
        buttons:         {
            // Si le da click al botón de "confirmar"
            confirmar: function (){
                $.ajax({
                    url: base_url+"index.php/consumibles/eliminarConsumible/"+idConsumible,
                    success: function (response){
                            $.alert({
                                boxWidth: '30%',
                                useBootstrap: false,
                                title: 'Éxito!',
                                content: 'Consumible Eliminado!'});
                            
                            setTimeout(function(){ 
                                window.location.href = base_url+"index.php/consumibles"; 
                            }, 2000);
                            
                    },
                    error: function(response){
                        $.alert({
                                boxWidth: '30%',
                                useBootstrap: false,
                                title: 'Error!',
                                content: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema'});   
                        setTimeout(function(){ 
                                window.location.href = base_url+"index.php/consumibles";
                        }, 2000);  
                        
                    }
                });
            },
            // Si le da click a "cancelar"
            cancelar: function (){                
            }
        }
    });
}
/*
function load() {
    // Destruye la tabla y la crea de nuevo con los datos cargados 
    table.destroy();
    table = $('#tabla_consumibles').DataTable({
        responsive: !0,
        "ajax": {
            "url": base_url+"index.php/Consumibles/getListadoConsumibles"
        },
        "columns": [
            {"data": "id"},
            {"data": "modelo"},
            {"data": "parte"},
            {"data": "observaciones",
                render:function(data,type,row){
                    var html =row.observaciones+'<br>';
                    if(row.rendimiento_unidad_imagen>0){
                        html+='Rendimiento unidad imagen: '+row.rendimiento_unidad_imagen+'<br>';
                    }
                    if(row.rendimiento_toner>0){
                        html+='Rendimiento toner: '+row.rendimiento_toner;
                    }
                    return html;
                }
            }, 
            {"data": "equipos"}, 
            {"data": "destacado",
                render:function(data,type,row){ 
                    var html='';
                    if(row.destacado==1){
                        var startchecked='checked';
                    }else{
                        var startchecked='';
                    }
                    html='<input class="star_f soloadministradores" type="checkbox" title="Favorito" id="star_factura_'+row.id+'" onclick="favorito('+row.id+')" '+startchecked+'>';
                    return html;
                }
            },         
            {"data": null,
                 render:function(data,type,row){
                    $('.tooltipped').tooltip();
                    var html='';
                        html+='<a href="'+base_url+'consumibles/edicion/'+row.id+'" class="btn-floating green tooltipped edit soloadministradores" data-position="top" data-delay="50" data-tooltip="Editar"><i class="material-icons">mode_edit</i></a>';
                        html+='<a href="'+base_url+'consumibles/visualizar/'+row.id+'" class="btn-floating blue tooltipped visualiza" data-position="top" data-delay="50" data-tooltip="Visualizar"><i class="material-icons">remove_red_eye</i></a>';
                        html+='<a class="btn-floating red tooltipped elimina soloadministradores" data-position="top" data-delay="50" data-tooltip="Eliminar" onclick="confirmaEliminarConsumible('+row.id+')"><i class="material-icons">delete_forever</i></a>';
                        html+=' <a class="btn-floating blue tooltipped infogen_'+row.id+'" data-modelo="'+row.modelo+'" data-position="top" data-delay="50" data-tooltip="Informacion General" onclick="infogen('+row.id+')"><i class="fa fa-laptop fa-fw"></i></a>';
                            if(row.paginaweb==1){
                                var viewweb='checked';
                            }else{
                                var viewweb='';
                            } 
                        html+='<div class="switch"><br>\
                                          <label class="soloadministradores">Mostrar\
                                            <input type="checkbox" name="bloqueo" id="mostrarweb_'+row.id+'" wtx-context="7579CA6E-0857-4E6C-B2D3-9042AE775C00" onclick="mostrarweb('+row.id+')" '+viewweb+'>\
                                            <span class="lever"></span>\
                                          </label>\
                                    </div>';
                    return html;
                }
            }
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[25, 50, 100], [25, 50, 100]],
        //"dom": 'Bfrtpl',
        // Muestra el botón de Excel para importar la tabla
        "buttons": [
            
        ],
        
    }).on('draw',function(){
        $('.dataTables_filter').append('<i class="fa fa-search buttonsearch" onclick="searchtable_v()"></i>')

    });
}
*/
function load() {
    // Destruye la tabla y la crea de nuevo con los datos cargados 
    table.destroy();
    table = $('#tabla_consumibles').DataTable({
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url+"index.php/Consumibles/getlistconsumibles",
            type: "post",
            "data": {
                'paginaweb':0
            },
        },
        "columns": [
            {"data": "id"},
            {"data": "modelo"},
            {"data": "parte"},
            {"data": "observaciones",
                render:function(data,type,row){
                    var html =row.observaciones+'<br>';
                    if(row.rendimiento_unidad_imagen>0){
                        html+='Rendimiento unidad imagen: '+row.rendimiento_unidad_imagen+'<br>';
                    }
                    if(row.rendimiento_toner>0){
                        html+='Rendimiento toner: '+row.rendimiento_toner;
                    }
                    return html;
                }
            }, 
            {"data": "equipos"}, 
            {"data": "destacado",
                render:function(data,type,row){ 
                    var html='';
                    if(row.destacado==1){
                        var startchecked='checked';
                    }else{
                        var startchecked='';
                    }
                    html='<input class="star_f soloadministradores" type="checkbox" title="Favorito" id="star_factura_'+row.id+'" onclick="favorito('+row.id+')" '+startchecked+'>';
                    return html;
                }
            },         
            {"data": null,
                 render:function(data,type,row){
                    $('.tooltipped').tooltip();
                    var html='';
                        html+='<a href="'+base_url+'consumibles/edicion/'+row.id+'" class="btn-floating green tooltipped edit soloadministradores" data-position="top" data-delay="50" data-tooltip="Editar"><i class="material-icons">mode_edit</i></a>';
                        html+='<a href="'+base_url+'consumibles/visualizar/'+row.id+'" class="btn-floating blue tooltipped visualiza" data-position="top" data-delay="50" data-tooltip="Visualizar"><i class="material-icons">remove_red_eye</i></a>';
                        html+='<a class="btn-floating red tooltipped elimina soloadministradores" data-position="top" data-delay="50" data-tooltip="Eliminar" onclick="confirmaEliminarConsumible('+row.id+')"><i class="material-icons">delete_forever</i></a>';
                        html+=' <a class="btn-floating blue tooltipped infogen_'+row.id+'" data-modelo="'+row.modelo+'" data-position="top" data-delay="50" data-tooltip="Informacion General" onclick="infogen('+row.id+')"><i class="fa fa-laptop fa-fw"></i></a>';
                            if(row.paginaweb==1){
                                var viewweb='checked';
                            }else{
                                var viewweb='';
                            } 
                        html+='<div class="switch"><br>\
                                          <label class="soloadministradores">Mostrar\
                                            <input type="checkbox" name="bloqueo" id="mostrarweb_'+row.id+'" wtx-context="7579CA6E-0857-4E6C-B2D3-9042AE775C00" onclick="mostrarweb('+row.id+')" '+viewweb+'>\
                                            <span class="lever"></span>\
                                          </label>\
                                    </div>';
                    return html;
                }
            }
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[25, 50, 100], [25, 50, 100]],
        //"dom": 'Bfrtpl',
        // Muestra el botón de Excel para importar la tabla
        "buttons": [
            /*
            {   
                extend: 'excelHtml5',
                text: ' Descargar Excel <i class="fa fa-download"></i>',
                className: 'btn classBotonFratsa'
            }
            */
        ],
        
    }).on('draw',function(){
        $('.dataTables_filter').append('<i class="fa fa-search buttonsearch" onclick="searchtable_v()"></i>')

    });
}
function searchtable_v(){
    var contenedor = document.getElementById('tabla_consumibles_filter');

    // Selecciona el input dentro del contenedor
    var input = contenedor.querySelector('input.form-control-bmz');

    // Obtén el valor del input
    var searchv = input.value;

    // Muestra el valor en la consola
    console.log(searchv);
    //var searchv =$("input[type=search]").val();
    table.search(searchv).draw();
}
//========================
$(document).ready(function($) {
    $('#save_fc').click(function(event) {
        var datos = $('#form_catacteristicas').serialize();
                
        $.ajax({
            type:'POST',
            url: base_url+"index.php/Consumibles/insertaActualizacaracteristicas",
            data: datos,
            success:function(data){  
                catacte_data_view(equipo_infog);
                toastr["success"]("Se ha Procesado con exito", "éxito");
                fc_limpiar();
                
            },
            error: function(response){
                toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema", "Advertencia");
            }
        });
    });   
});
function favorito(id){
    setTimeout(function(){ 
        var statusfav=$('#star_factura_'+id).is(':checked')==true?1:0;
        $.ajax({
            type:'POST',
            url: base_url+'Consumibles/favorito',
            data: {
                equipo:id,
                status:statusfav
            },
            statusCode:{
                404: function(data){
                    toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema", "Advertencia");
                },
                500: function(){
                    toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema", "Advertencia");
                }
            },
            success:function(data){
                
               
            }
        });
    }, 1000);
}
function mostrarweb(id){
    setTimeout(function(){ 
        var paginaweb = $('#mostrarweb_'+id).is(':checked')==true?1:0;
        $.ajax({
            type:'POST',
            url: base_url+"index.php/Consumibles/mostrarweb",
            data: {
                    idequipo:id,
                    mostrar:paginaweb
                },
            success: function (response){
                
            },
            error: function(response){
                toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema", "Advertencia");
            }
        });
    }, 1000);
}
function infogen(id){
    $('#modalinfoequipo').modal('open');
    var modelo=$('.infogen_'+id).data('modelo');
    $('#idequipofc').val(id);
    $('.equipo_modelo').html(modelo);
    equipo_infog=id;
    upload_data_view(equipo_infog);
    catacte_data_view(equipo_infog);
}
function upload_data_view(id){
    console.log(id);
    $.ajax({
        type:'POST',
        url: base_url+'index.php/Consumibles/viewimages',
        data: {idequipo:id},
        success:function(data){
            $('.galeriimg').html(data);
            $('.materialboxed').materialbox();
        }
    });
}
function catacte_data_view(id){
    $.ajax({
        type:'POST',
        url: base_url+'index.php/Consumibles/viewcaracte',
        data: {idequipo:id},
        success:function(data){
            $('.lis_catacter').html(data);
        }
    });
}
function filesupload(){
    $("#files").fileinput({
        showCaption: true,
        showUpload: true,// quita el boton de upload
        //rtl: true,
        allowedFileExtensions: ["jpg","png","webp","jpeg"],
        browseLabel: 'Seleccionar documentos',
        uploadUrl: base_url+'Consumibles/imagenes_multiple',
        maxFilePreviewSize: 5000,
        previewFileIconSettings: {
            'docx': '<i class="fa fa-file-word-o text-primary"></i>',
            'xlsx': '<i class="fa fa-file-excel-o text-success"></i>',
            'pptx': '<i class="fa fa-file-powerpoint-o text-danger"></i>',
            'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
            'pdf': '<i class="fa fa-file-pdf-o text-danger"></i>',
            'zip': '<i class="fa fa-file-archive-o text-muted"></i>',
            'cer': '<i class="fas fa-file-invoice"></i>',
        },
        uploadExtraData: function (previewId, index) {
        var info = {
                    idequipo:equipo_infog
                };
        return info;
      }
    }).on('filebatchuploadcomplete', function(event, files, extra) {
      //location.reload();
        upload_data_view(equipo_infog);
    }).on('filebatchuploadsuccess', function(event, files, extra) {
      //location.reload();
      //toastr.success('Se cargo el Certificado Correctamente','Hecho!');
        upload_data_view(equipo_infog);
    });
}
function deleteimg(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de Eliminar la imagen? El cambio será irreversible',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Consumibles/deleteimg/",
                    data: {idequipo:id},
                    success: function (response){
                        toastr["success"]("Se ha eliminado con exito", "éxito");
                        upload_data_view(equipo_infog);
                    },
                    error: function(response){
                        toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema", "Advertencia");
                    }
                });
            },
            cancelar: function (){
                
            }
        }
    });
}
function fc_limpiar(){
    $('#idfc').val(0);
    $('#descripcion').val('');
    $('#name').val('');
    $('#general').prop('checked',false);
    $('#orden').val('');
    $('#cancelar_fc').hide('slow');
}
function delete_ec(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de Eliminar la caracteristica? El cambio será irreversible',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Consumibles/deleteec",
                    data: {idequipo:id},
                    success: function (response){
                        toastr["success"]("Se ha eliminado con exito", "éxito");
                        catacte_data_view(equipo_infog);
                    },
                    error: function(response){
                        toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema", "Advertencia");
                    }
                });
            },
            cancelar: function (){
                
            }
        }
    });
}