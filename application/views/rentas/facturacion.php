<!-- START CONTENT -->
<section id="content" width="100%">
  
  <!--start container-->
  <div class="container" width="100%">
    <div class="section">
      <div class="row">
        <p class="caption col s6">Factura</p>
      </div>
      <div class="row">
        <div class="col s12 m12 24">
            <div class="card-panel">
              <div class="row">
                <div class="col s12 m6">
                  <div class="col s3 m3">
                    <label>TIPO</label>
                  </div>
                  <div class="col s3 m3">
                    <input type="text" value="Adelantada" readonly> 
                  </div>
                  <div class="col s3 m3">
                    <input type="text" value="1000.00" readonly> 
                  </div>
                  <div class="col s3 m3">
                    <input type="text" value="Individual" readonly> 
                  </div>
                </div>
                <div class="col s12 m6">
                  <div class="col s3 m3">
                    <input type="text" value="Renta en desposito" readonly> 
                  </div>
                  <div class="col s3 m3">
                    <input type="text" value="1000.00" readonly> 
                  </div>
                  <div class="col s3 m3">
                    <input type="text" value="serv. Monocromatico" readonly> 
                  </div>
                  <div class="col s3 m3">
                    
                  </div>
                </div>
              </div> 
              <div class="row">
                <div class="col s12 m6">
                  <div class="col s3 m3">
                    <label>CLICKS INCLUIDOS MONO</label>
                  </div>
                  <div class="col s3 m3">
                    <input type="text" value="5000" readonly> 
                  </div>
                  <div class="col s3 m3">
                    <label>PRECIO CLICK RENTA</label>
                  </div>
                  <div class="col s3 m3">
                    <input type="text" value="0.2" readonly> 
                  </div>
                </div>
                <div class="col s12 m6">
                  <div class="col s3 m3">
                    <label>PRECIO CLICK EXCEDENTE</label>
                  </div>
                  <div class="col s3 m3">
                    <input type="text" value="0.25" readonly> 
                  </div>
                  <div class="col s6 m6">
                    <div class="col s12 m12">
                      <label>ORDEN DE COMPRA</label>
                    </div>
                    <div class="col s6 m6">
                      <label>NÚMERO</label>
                      <input type="text" value="" readonly> 
                    </div>
                    <div class="col s6 m6">
                      <label>VIGENCIA</label>
                      <input type="text" value="" readonly> 
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col s3 m1">
                    <label>FECHA DE INSTALACIÓN</label>
                </div>
                <div class="col s3 m2">
                    <input type="date" value="2019-08-01" readonly>
                </div>
              </div> 
              <div class="row">
                <div class="col s3 m1">
                    <label>PERIODO</label>
                </div>
                <div class="col s2 m1">
                    <input type="text" value="1" readonly>
                </div>
                <div class="col s2 m1">
                    <input type="text" value="AGO" readonly>
                </div>
                <div class="col s2 m1">
                    <input type="text" value="2019" readonly>
                </div>
                <div class="col s2 m1">
                    <label>AL</label>
                </div>
                <div class="col s2 m1">
                    <input type="text" value="31" readonly>
                </div>
                <div class="col s2 m1">
                    <input type="text" value="AGO" readonly>
                </div>
                <div class="col s2 m1">
                    <input type="text" value="2019" readonly>
                </div>
              </div>
              <div class="row">
                <div class="col s12 m12">
                    <label>INFORMACIÓN EQUIPO</label>
                </div>
                <div class="col s12 m12">
                    <label>RENDIMINETO</label>
                </div>
                <div class="col s12 m12">
                  <table class="responsive-table display">
                    <thead>
                      <tr>
                        <th>EQUIPO No.</th>
                        <th>UBICACIÓN</th>
                        <th>MODELO</th>
                        <th>SERIE</th>
                        <th>PRODUCCIÓN PROMEDIO</th>
                        <th>MODELO TONER</th>
                        <th>NUEVO</th>
                        <th>ARRANQUE</th>
                        <th>STOCK MENSUAL</th>
                        <th>CONTADOR INICIAL</th>
                        <th>ACCESORIOS</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>1</td>
                        <td></td>
                        <td>M2040</td>
                        <td>VUR69890</td>
                        <td></td>
                        <td>TK-1175</td>
                        <td>12000</td>
                        <td>1000</td>
                        <td>1</td>
                        <td>0</td>
                        <td>STAND,REGULADOR</td>
                      </tr>
                    </tbody>
                    
                  </table>
                </div>
                <div class="col s12 m12">
                    <label>FACTURACIÓN</label>
                </div>
              </div>
              <div class="row">
                <div class="col s3 m1">
                    <label>PERIODO RENTA</label>
                </div>
                <div class="col s3 m3">
                  <input type="text" name="" value="RENTA DEL 1 AL 30 DE SEPTIEMBRE DE 2019" readonly>
                </div>
                <div class="col s3 m1">
                  <input type="text" name="" value="1000" readonly>
                </div>
                <div class="col s3 m2">
                  <label>TOTAL FACTURA</label>
                </div>
                <div class="col s3 m2">
                  <input type="text" name="" value="1000" readonly>
                </div>
              </div> 
              <div class="row">
                <div class="col s3 m1">
                    <label>EXCEDENTES</label>
                </div>
                <div class="col s3 m3">
                  <input type="text" name="" value="RENTA DEL 1 AL 30 DE SEPTIEMBRE DE 2019" readonly>
                </div>
                <div class="col s3 m1">
                  <input type="text" name="" value="227.25" readonly>
                </div>
              </div> 
              <div class="row">
                <div class="col s12 m12">
                  <table class="responsive-table display centered">
                    <thead>
                      <tr>
                        <th rowspan="2">TIPO</th>
                        <th rowspan="2">MODELO</th>
                        <th rowspan="2">SERIE</th>
                        <th colspan="2">COPIA</th>
                        <th colspan="2">ESCANEO</th>
                        <th rowspan="2">MODELO TONER</th>
                        <th rowspan="2">PRODUCCIÓN</th>
                        <th rowspan="2">TONER CONSUMIDO</th>
                        <th rowspan="2">PRODUCCIÓN MONO</th>
                      </tr>
                      <tr>
                        <th>CONTADOR INICIAL</th>
                        <th>CONTADOR FINAL</th>
                        <th>CONTADOR INICIAL</th>
                        <th>CONTADOR FINAL</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                
              </div>
            </div>
        </div>
        

        
      </div>
    </div>
  </div>
</section>