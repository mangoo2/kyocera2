<input type="hidden" id="mesactual" value="<?php echo date('m');?>">
<input type="hidden" id="anioactual" value="<?php echo date('Y');?>">

<style type="text/css">
  .error{    color: red;  }
  .test5{    display: none !important;  }
  .table_pg td{    padding: 5px;  }
  <?php if($perfilid==11){ ?> 
    .test5{
      display: block !important;
    }
    .test1,.test2,.test3,.test4{
      display: none !important;
    }
  <?php } ?>
  <?php
                              if(isset($_SESSION['selectweb'])){
                                if($this->session->userdata('selectweb')==1){
                                  $empresa=2;
                                }else{
                                  $empresa=1;
                                }
                              }else{
                                $empresa=1;
                              }
                              if(isset($_GET['g_tipov'])){
                                $empresa=$_GET['g_tipov'];
                              }
                            ?>
</style>
<section id="content" width="100%">
  
  <!--start container-->
  <div class="container" width="100%">
    <div class="section">
      <div class="row">
        <div class="col s6"><p class="caption ">Facturar</p></div>
        
        
          <div class="col s6" style="text-align: right;">
              <input type="checkbox" class="filled-in" id="infoanticipo" >
              <label for="infoanticipo">Anticipo</label>
            <?php if($idpersonal==1 or $idpersonal==9){ ?>
              <a class="waves-effect green btn-bmz" onclick="mpublicogeneral()">Obtener Publico General</a>
            <?php } ?>
          </div>

        
      </div>
      <div class="row">
        <!-- FORMULARIO  <?php ?>-->
          <div class="col s12 m12 24">
            <div class="card-panel">
              <div class="row"> 
                <form id="validateSubmitForm" method="post" autocomplete="off"> 
                  <div class="row selectweb">
                    <div class="col s6">
                      <label for="">Empresa</label>
                      <select name="tipov" id="tipov" class="browser-default form-control-bmz">
                        <option value="0">Seleccione</option>
                        <option value="1" <?php if($empresa==1){ echo 'selected';}?> >Alta Productividad</option>
                        <option value="2" <?php if($empresa==2){ echo 'selected';}?> >D-Impresión</option>
                      </select>
                    </div>
                  </div>
                  <div class="row">
                   <div class="col s6">
                    <label for="">Cliente</label> <!--Llenar -->
                    <!--<input id="idcliente" name="idcliente" type="text" value="" >-->
                    <select class="browser-default" name="idcliente" id="idcliente" required>
                      <?php if(isset($_GET['g_cli'])){
                            if($_GET['g_cli']>0){
                               $row_cli=$this->ModeloCatalogos->getselectwheren('clientes',array('id'=>$_GET['g_cli']));
                               foreach ($row_cli->result() as $item_g_cli) {
                                 echo '<option value="'.$item_g_cli->id.'">'.$item_g_cli->empresa.'<option>';
                               }
                            }
                          }
                      ?>
                    </select>
                    
                   </div> 
                   <div class="col s6">
                    <label for="">RFC</label> <!--Llenar -->
                    <select class="browser-default " name="rfc" id="rfc" required onchange="verficartiporfc()">
                      
                    </select>
                   </div>
                   <div class="col s6"></div><div class="col s6 infodatosfiscales"></div>
                  </div>
                  <div class="row agregardatospublicogeneral">
                  
                  </div>
                  <div class="row">
                    <div class="col s6">
                        
                        <label class="active">Método de pago</label>
                        <select id="FormaPago" name="FormaPago" class="browser-default form-control-bmz" required>
                          <?php foreach ($metodo as $key) { ?>
                            <option value="<?php echo $key->metodopago; ?>"><?php echo $key->metodopago_text; ?></option>
                          <?php } ?>
                        </select>
                        
                      </div> 
                      <div class="col s6">
                        <label class="active">Forma de pago</label>
                        <select  id="MetodoPago" name="MetodoPago" class="browser-default" required>
                          <option value="" disabled selected>Selecciona una opción</option>
                          <?php foreach ($forma as $key) { ?>
                            <option value="<?php echo $key->formapago; ?>"><?php echo $key->formapago_text; ?></option>
                          <?php } ?>
                        </select>
                        
                      </div>
                  </div>
                  <div class="row">
                   <div class="input-field col s6">
                    <input id="" disabled="" name="" type="text" value="">
                    <label for="">Fecha Aplicación</label> <!--Llenar -->
                   </div> 
                   <div class="input-field col s6">
                    <input id="" disabled="" name="" type="text" value="">
                    <label for="">Lugar Expedición</label> <!--Llenar -->
                   </div> 
                  </div>
                  <div class="row">
                    <div class="col s6">
                      <label for="" class="active">Tipo Moneda</label> <!--Llenar -->
                      <select id="moneda" name="moneda" class="span12 form-control browser-default" required>
                          <option value="pesos">MXN Peso Mexicano</option>
                          <option value="USD">Dólares Norteamericanos</option>
                          <option value="EUR">Euro</option>
                      </select>
                      
                    </div> 
                    <div class="col s6">
                        <label class="active">Uso CFDI</label>
                        <select id="uso_cfdi" name="uso_cfdi" class="browser-default " required>
                          <option value="" disabled selected>Selecciona una opción</option>
                          <?php foreach ($cfdi as $key) { ?>
                            <option value="<?php echo $key->uso_cfdi; ?>" class="option_<?php echo $key->uso_cfdi; ?> pararf <?php echo str_replace(',',' ',$key->pararf) ?>" disabled><?php echo $key->uso_cfdi_text; ?></option>
                          <?php } ?>
                        </select>
                        
                    </div>
                    <div class="col s6">
                      <label for="CondicionesDePago">Condicion de Pago</label>
                      <input type="text" name="CondicionesDePago" id="CondicionesDePago" class="browser-default form-control-bmz" onpaste="return false;">
                    </div>
                    <div class="col s6 divtipoComprobante">
                      <label for="CondicionesDePago">Tipo Comprobante</label>
                      <select id="TipoComprobante" name="TipoComprobante" class="browser-default form-control-bmz" required>
                          <option value="I" >Ingreso</option>
                          <option value="E" >Egreso</option>

                        </select>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col s12">
                      
                      
                      <input type="checkbox" class="filled-in" id="facturarelacionada" >
                      <label for="facturarelacionada">Factura Relacionada</label>

                        
                      <div class="form-group divfacturarelacionada" style="display:none;">
                        <div class="col s4">
                          <div class="form-group">
                            <label>Tipo Relacion</label>
                            <select class="browser-default form-control-bmz" id="TipoRelacion" onchange="selecttiporelacion()">
                              
                              <option value="01">01 Nota de crédito de los documentos relacionados</option>
                              <!--<option value="02">02 Nota de débito de los documentos relacionados</option>
                              <option value="03">03 Devolución de mercancía sobre facturas o traslados previos</option>
                              -->
                              <option value="04">04 Sustitución de los CFDI previos</option>
                              <!--
                              <option value="05">05 Traslados de mercancias facturados previamente</option>
                              <option value="06">06 Factura generada por los traslados previos</option>-->
                              <option value="07">07 CFDI por aplicación de anticipo</option>
                              
                            </select>
                          </div>
                        </div>
                        <div class="col s4">
                          <div class="form-group">
                            <label>Folio Fiscal</label>
                            <input type="text" id="uuid_r" class="form-control-bmz" placeholder="111AAA1A-1AA1-1A11-11A1-11A1AA111A11">
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row tarjeta_datos" style="display: none;">
                    <div class="input-field col s6">
                      <i class="material-icons prefix">credit_card</i>
                      <input id="tarjeta" name="tarjeta" type="number" value="">
                      <label for="">Últimos Dígitos:</label> <!--Llenar -->
                    </div>  
                  </div>
                   <div class="row">
                     <div class="col s12">
                       <hr>
                     </div>
                     <div>
                       <ul class="collapsible" data-collapsible="accordion">
                        <li>
                         <div class="collapsible-header lipedimento">Pedimento</div>
                         <div class="collapsible-body row">
                           <div class="input-field col s6">
                            <i class="material-icons prefix"></i>
                            <input id="numproveedor" name="numproveedor" type="text" value="">
                            <label for="">Número de proveedor</label> <!--Llenar -->
                           </div>
                           <div class="input-field col s6">
                            <i class="material-icons prefix"></i>
                            <input id="numordencompra" name="numordencompra" type="text" value="">
                            <label for="">Número de orden de compra</label> <!--Llenar -->
                           </div>
                           <div class="input-field col s6">
                            <textarea id="observaciones" name="observaciones" class="materialize-textarea" placeholder=" "></textarea>
                            <label for="">Observaciones</label>
                            </div>
                         </div>
                        </li>
                       </ul>
                     </div>

                   </div>
                   </form>
                   <div class="row divrequerimentos">
                     <div>
                       <ul class="collapsible" data-collapsible="accordion">
                        <li>
                         <div class="collapsible-header">Requerimentos</div>
                         <div class="collapsible-body">
                            <div class="row">
                              <div class="col s12">
                                Tipo de Relación
                              </div>
                              <div class="col s12">
                                  <input class="with-gap" name="tiporelacion" type="radio" id="test0" value="0" checked>
                                  <label for="test0">Ninguna</label>
                                  <input class="with-gap test1" name="tiporelacion" type="radio" id="test1" value="1">
                                  <label for="test1" class="test1">Ventas</label>
                                  <input class="with-gap test2" name="tiporelacion" type="radio" id="test2" value="2">
                                  <label for="test2" class="test2">Ventas combinadas</label>
                                  
                                  <input class="with-gap test3" name="tiporelacion" type="radio" id="test3" value="3">
                                  <label for="test3" class="test3">Póliza</label>
                                  
                                  <input class="with-gap test4" name="tiporelacion" type="radio" id="test4" value="4">
                                  <label for="test4" class="test4">Servicio</label>

                                  <input class="with-gap test5" name="tiporelacion" type="radio" id="test5" value="5">
                                  <label for="test5" class="test5">Ventas</label>

                                  <input type="hidden" id="ventaviculada">
                                  <input type="hidden" id="ventaviculadatipo">
                
                              </div>
                              <div class="col s12 infoventas">
                                
                              </div>
                              <div class="col s12 infoventasrelaciones">
                                <table id="vrtable">
                                  <thead>
                                    <tr>
                                      <th>Tipo Relacion</th>
                                      <th></th>
                                      <th>#</th>
                                    </tr>
                                  </thead>
                                  <tbody class="vrtabletbody">
                                    
                                  </tbody>
                                </table>
                              </div>
                            </div>
                         </div>
                        </li>
                       </ul>
                     </div>

                   </div>
                
                <!-- inicio -->
                <div class="row">
                  <div class="col s12 addpermisoseditcon" style="text-align:end;">
                    
                  </div>
                  <div class="col s12">
                    <table id="table_conceptos" class="striped">
                      <thead>
                        <tr>
                          <th width="8%">Cantidad</th>
                          <th width="14%">Unidad SAT</th>
                          <th width="14%">Concepto SAT</th>
                          <th width="23%">Descripción</th>
                          <th width="8%">Precio</th>
                          <th width="8%">Descuento</th>
                          <th width="10%">Monto</th>
                          <th width="7%">Aplicar iva</th>
                          <th width="10%"></th>
                        </tr>
                        <tr>
                          <th >
                            <input type="number" name="scantidad" id="scantidad" class="form-control-bmz" value="1" onchange="calculartotal()">
                          </th>
                          <th >
                            <select name="ssunidadsat" id="sunidadsat" class="browser-default"></select>
                          </th>
                          <th >
                            <select name="sconseptosat" id="sconseptosat" class="browser-default">
                              <?php foreach ($fservicios->result() as $item) { ?>
                                <option value="<?php echo $item->Clave; ?>" ><?php echo $item->Clave.' / '.$item->nombre; ?></option>
                              <?php } ?>
                            </select>
                          </th>
                          <th >
                            <input type="text" name="sdescripcion" id="sdescripcion" class="form-control-bmz">
                          </th>
                          <th >
                            <input type="number" name="sprecio" id="sprecio" class="form-control-bmz" value="0" onchange="calculartotal()">
                          </th>
                          <th class="" style="text-align: center;"></th>
                          <th class="montototal" style="text-align: center;"></th>
                          
                          <th >
                            <input type="checkbox" class="filled-in" id="aplicariva" checked="checked">
                            <label for="aplicariva"></label>
                          </th>
                          <th >
                            <a class="waves-effect green btn-bmz agregarconcepto" onclick="agregarconcepto()">Agregar</a>
                          </th>
                        </tr>
                      </thead>
                      <tbody class="addcobrar">
                        
                      </tbody>
                    </table>
                    
                  </div>
                </div>
                <div class="row">
                  <div class="col s8">
                    
                  </div>
                  <div class="col s4">
                    <div class="row">
                      <div class="col s7" style="text-align: right;">
                        Subtotal
                      </div>
                      <div class="col s5">
                        <span style="float: left;">$</span>
                        <input type="" name="Subtotalinfo" id="Subtotalinfo" readonly style="background: transparent; border:0; width: 90%;" value="0.00">
                        <input type="hidden" name="Subtotal" id="Subtotal" readonly style="background: transparent; border:0; width: 90%;" value="0.00">
                      </div>
                    </div>
                    <div class="row">
                      <div class="col s7" style="text-align: right;">
                        Descuento
                      </div>
                      <div class="col s5">
                        <span style="float: left;">$</span>
                        <input type="" name="descuentof" id="descuentof" readonly style="background: transparent; border:0; width: 90%;" value="0.00">
                      </div>
                    </div> 
                    <div class="row">
                      <div class="col s7" style="text-align: right;">
                        I.V.A
                      </div>
                      <div class="col s5">
                        <span style="float: left;">$</span>
                        <input type="" name="iva" id="iva" readonly style="background: transparent; border:0; width: 90%;" value="0.00">
                      </div>
                    </div>
                    <div class="row">
                      <div class="col s7" style="text-align: right;">
                        <input type="checkbox" id="risr" />
                        <label for="risr" onclick="calculartotales_set(1000)">10 % Retencion I.S.R.</label>
                      </div>
                      <div class="col s5">
                        <span style="float: left;">$</span>
                        <input type="" name="isr" id="isr" readonly style="background: transparent; border:0; width: 90%;" value="0.00">
                      </div>
                    </div> 
                    <div class="row">
                      <div class="col s7" style="text-align: right;">
                        <input type="checkbox" id="riva" />
                        <label for="riva" onclick="calculartotales_set(1000)">10.67 % Retencion I.V.A.</label>
                      </div>
                      <div class="col s5">
                        <span style="float: left;">$</span>
                        <input type="" name="ivaretenido" id="ivaretenido" readonly style="background: transparent; border:0; width: 90%;" value="0.00">
                      </div>
                    </div>
                    <div class="row">
                      <div class="col s7" style="text-align: right;">
                        <input type="checkbox" id="5almillar" />
                        <label for="5almillar" onclick="calculartotales_set(1000)">5 al millar.</label>
                      </div>
                      <div class="col s5">
                        <span style="float: left;">$</span>
                        <input type="" name="5almillarval" id="5almillarval" readonly style="background: transparent; border:0; width: 90%;" value="0.00">
                      </div>
                    </div>
                    <div class="row">
                      <div class="col s7" style="text-align: right;">
                        <input type="checkbox" id="aplicaout" />
                        <label for="aplicaout" onclick="calculartotales_set(1000)">6% Retencion servicios de personal.</label>
                      </div>
                      <div class="col s5">
                        <span style="float: left;">$</span>
                        <input type="" name="outsourcing" id="outsourcing" readonly style="background: transparent; border:0; width: 90%;" value="0.00">
                      </div>
                    </div>  
                    <div class="row">
                      <div class="col s7" style="text-align: right;">
                        Total
                      </div>
                      <div class="col s5">
                        <span style="float: left;">$</span>
                        <input type="" name="total" id="total" readonly style="background: transparent; border:0; width: 90%;" value="0.00">
                      </div>
                    </div> 
                    
                  </div>
                </div>
                <!-- fin --> 
                <div class="row">
                  <div class="botonfinalizar"></div>
                  <div class="input-field col s9">
                  </div>
                  <div class="input-field col s3 guadarremove">
                    <button class="btn cyan waves-effect waves-light right registrofac_preview" type="button">Facturar<i class="material-icons right">send</i></button>
                    <button class="btn cyan waves-effect waves-light right registrofac_save" type="button" style="margin-top: 10px;" onclick="registrofac(0,<?php echo $facturaId;?>)">Guardar<i class="material-icons right">save</i></button>
                  </div>
                </div>

              </div>
            </div>
          </div>
        
      </div>
    </div>
  </section>
<style type="text/css">
  .ifrafac{
    width: 100%;
    height: 412px;
    border: 0;
  }
</style>
<div id="modal_previefactura" class="modal modal-fixed-footer" style="width: 80%;">
    <div class="modal-content" style="padding: 0px;">
      <div class="row">
        <!--<div class="col s12">
          Favor de validar los siguientes datos del receptor:
        </div>
        <div class="col s4">
          <b>Nombre Fiscal: </b><br><span class="v_nomfical"></span>
        </div>
        <div class="col s4">
          <b>Direccion Fiscal(C.P.): </b><br><span class="v_direccionfiscal"></span>
        </div>
        <div class="col s4">
          <b>Regimen Fiscal Receptor: </b><br><span class="v_regimenfiscal"></span>
        </div>-->
        <div class="col-md-12 preview_iframe">
          
        </div>
      </div>
    </div>
    <div class="modal-footer">
      <a class="modal-close waves-effect waves-green btn-flat">Cancelar</a>
      <button class="modal-close waves-effect waves-green btn-flat registrofac">Aceptar</button>
    </div>
  </div>

  <div id="mpublicogeneral" class="modal modal-fixed-footer" style="width: 80%;">
    <div class="modal-content">
      <div class="row">
        <div class="col s12">
          <h5>Obtener Ventas Publico En General</h5>
        </div>
      </div>
      <div class="row">
        <?php
          $fecha = new DateTime();
          $fecha2 = new DateTime();
          $fecha->modify('first day of this month');
          $pg_fechai = $fecha->format('Y-m-d'); 
          $fecha2->modify('last day of this month');
          $pg_fechaf = $fecha2->format('Y-m-d');
        ?>
        <div class="col s2">
          <label>Fecha inicio</label>
          <input type="date" id="pg_fechai" class="form-control-bmz" value="<?php echo $pg_fechai;?>">
        </div>
        <div class="col s2">
          <label>Fecha Final</label>
          <input type="date" id="pg_fechaf" class="form-control-bmz" value="<?php echo $pg_fechaf;?>">
        </div>
        <div class="col s2">
          <label>Personal</label>
          <select id="pg_personal" class="browser-default form-control-bmz">
            <option value="0">Todos</option>
            <?php foreach ($personalrows->result() as $item) { ?>
              <option value="<?php echo $item->personalId; ?>"  ><?php echo $item->nombre; ?> <?php echo $item->apellido_paterno; ?> <?php echo $item->apellido_materno; ?></option>
            <?php } ?>
          </select>
        </div >
        <div class="col s2">
          <a class="waves-effect green btn-bmz" onclick="obtenerpg()">Obtener</a>
        </div>
      </div>
      <div class="row">
        <div class="col s12 infoventaspg">
          
        </div>
      </div>
    </div>
    <div class="modal-footer">
      <a class="modal-close waves-effect waves-green btn-flat">Cancelar</a>
      <button class="modal-close waves-effect waves-green btn-flat pgok">Aceptar</button>
    </div>
  </div>