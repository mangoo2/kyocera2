<!-- START CONTENT -->
<section id="content" width="100%">
  
  <!--start container-->
  <div class="container" width="100%">
    <div class="section">
      <div class="row">
        <p class="caption col s6">Nuevo contrato</p>
      </div>
      <div class="row">
        <input id="base_url" type="hidden" name="base_url" value="<?php echo base_url(); ?>">
        <!-- FORMULARIO -->
        
          <div class="col s12 m12 24">
            <div class="card-panel">
              <div class="row">
              <form id="contrato-form" method="post">
                <input type="hidden" name="idcotizacionAprobada" id="idcotizacionAprobada" value="<?php echo $idcotizacionAprobada ?>">
                <div class="row">
                  <div class="input-field col s5">
                    <i class="material-icons prefix">description</i>
                    <select id="tipocontrato" name="tipocontrato">
                      <option value="" disabled selected>Selecciona una opción</option>
                      <option value="1" >12 meses</option>
                      <option value="2" >24 meses</option>
                      <option value="2" >36 meses</option>
                    </select>
                    <label>Tipo de contrato</label>
                  </div>
                  <div class="input-field col s2"></div>
                  <div class="input-field col s5">
                    <i class="material-icons prefix">account_box</i>
                    <select id="tipopersona" name="tipopersona">
                      <option value="" disabled selected>Selecciona una opción</option>
                      <option value="1" >Fisica</option>
                      <option value="2" >Moral</option>
                    </select>
                    <label>Tipo de persona</label>
                  </div>
                </div>
                <div class="divider"></div>
                <h5 class="header">Datos de Contrato</h5>
                <div class="row">
                  <div class="input-field col s5">
                    <i class="material-icons prefix"></i>
                    <select id="vigencia" name="vigencia">
                      <option value="" disabled selected>Selecciona una opción</option>
                      <option value="1" >12 meses</option>
                      <option value="2" >24 meses</option>
                      <option value="2" >36 meses</option>
                    </select>
                    <label>Vigencia</label>
                  </div>
                  <div class="input-field col s2"></div>
                  <div class="input-field col s5">
                    <i class="material-icons prefix">account_circle</i>
                    <input type="text" readonly value="<?php echo $vendedor; ?>">
                    <input id="personalId" name="personalId" type="hidden" value="<?php echo $vendedorid; ?>">
                    <label for="personalId">Vendedor</label>
                  </div>
                </div>
                <div class="row">
                  <div class="input-field col s4">
                    <i class="material-icons prefix">event</i>
                    <input placeholder="" id="fechasolicitud" name="fechasolicitud" type="date" class="validate">
                    <label for="fechasolicitud" class="active">Fecha de solicitud</label>
                  </div>
                  <div class="input-field col s4"></div>
                  <div class="input-field col s4">
                    <i class="material-icons prefix">event</i>
                    <input placeholder="" id="fechainicio" name="fechainicio" type="date" class="validate">
                    <label for="fechainicio" class="active">Fecha de inicio</label>
                  </div>
                </div>
                <div class="divider"></div>
                <h5 class="header">Datos de cliente</h5>
                <div class="row">
                  <div class="input-field col s8">
                    <i class="material-icons prefix"></i>
                    <input id=" arrendataria" name=" arrendataria"  type="text" autocomplete="off" value="">
                    <label for="arrendataria">Arrendataria</label>
                  </div>
                  <div class="input-field col s4">
                    <i class="material-icons prefix"></i>
                    <input id="rfc" name="rfc" type="text" value="">
                    <label for="rfc">RFC</label>
                  </div>
                </div>  
                <div class="row">  
                  <div class="input-field col s8">
                    <i class="material-icons prefix">description</i>
                    <input id=" actaconstitutiva" name=" actaconstitutiva" type="text" value="">
                    <label for="actaconstitutiva">Acta Constitutiva</label>
                  </div>
                  <div class="input-field col s4">
                    <i class="material-icons prefix">event</i>
                    <input id="fecha" name="fecha" type="date" value="">
                    <label for="fecha" class="active">Fecha</label>
                  </div>
                </div>
                <div class="row">
                  <div class="input-field col s4">
                    <i class="material-icons prefix">#</i>
                    <input id="notariapublicano" name="notariapublicano" type="text" value="">
                    <label for="notariapublicano">Notaria Publica No.</label>
                  </div>
                  <div class="input-field col s8">
                    <i class="material-icons prefix">person</i>
                    <input id=" titular" name=" titular"  type="text" autocomplete="off" value="">
                    <label for="titular">Titular</label>
                  </div>
                </div>
                <div class="row">
                  <div class="input-field col s12">
                    <i class="material-icons prefix"></i>
                    <input id="representantelegal" name="representantelegal"  type="text" autocomplete="off" value="">
                    <label for="representantelegal">Representante legal</label>
                  </div>
                </div>  
                <div class="row">
                  <div class="input-field col s4">
                    <i class="material-icons prefix"></i>
                    <input id="instrumentonotarial" name="instrumentonotarial" type="text" value="">
                    <label for="instrumentonotarial">Instrumento notarial</label>
                  </div>
                  <div class="input-field col s4">
                    <i class="material-icons prefix">event</i>
                    <input placeholder="" id="instrumentofechasolicitud" name="instrumentofechasolicitud" type="date" class="validate">
                    <label for="instrumentofechasolicitud" class="active">Fecha de solicitud</label>
                  </div>
                  <div class="input-field col s4">
                    <i class="material-icons prefix">#</i>
                    <input id=" instrumentonotariapublicano" name=" instrumentonotariapublicano"  type="text" autocomplete="off" value="">
                    <label for="instrumentonotariapublicano">Notaria Pública No.</label>
                  </div>
                </div>
                <div class="row">
                  <div class="input-field col s12">
                    <i class="material-icons prefix">place</i>
                    <input id="domiciliofiscal" name="domiciliofiscal"  type="text" autocomplete="off" value="">
                    <label for="domiciliofiscal">Domicilio fiscal</label>
                  </div>
                </div> 
                <div class="row">
                  <div class="input-field col s12">
                    <i class="material-icons prefix">place</i>
                    <input id="domicilioinstalacion" name="domicilioinstalacion"  type="text" autocomplete="off" value="">
                    <label for="domicilioinstalacion">Domicilio de instalación</label>
                  </div>
                </div> 
                <div class="row">
                  <div class="input-field col s4">
                    <i class="material-icons prefix">account_box</i>
                    <input id="contacto" name="contacto" type="text" value="">
                    <label for="contacto">Contacto</label>
                  </div>
                  <div class="input-field col s4">
                    <input id="cargoarea" name="cargoarea" type="text" value="">
                    <label for="cargoarea">Cargo/Área</label>
                  </div>
                  <div class="input-field col s4">
                    <i class="material-icons prefix">smartphone</i>
                    <input id="telefono" name="telefono"  type="text" autocomplete="off" value="">
                    <label for="telefono">Teléfono</label>
                  </div>
                </div>
                <div class="divider"></div>
                <h5 class="header">Clausulas</h5>
                <div class="row">
                  <div class="input-field col s6">
                    <i class="material-icons prefix"></i>
                    <input id="rentadeposito" name="rentadeposito" type="text" value="">
                    <label for="rentadeposito">Renta en deposito</label>
                  </div>
                  <div class="input-field col s6">
                    <i class="material-icons prefix"></i>
                    <input id="rentaadelantada" name="rentaadelantada"  type="text" autocomplete="off" value="">
                    <label for="rentaadelantada">Renta en adelantada</label>
                  </div>
                </div>
                <div class="divider"></div>
                <h5 class="header">Páginas</h5>
                <div class="row">
                  <div class="input-field col s4">
                    <i class="material-icons prefix"></i>
                    <input id="tipo" name="tipo" type="text" value="">
                    <label for="tipo">Tipo</label>
                  </div>
                  <div class="input-field col s4">
                    <i class="material-icons prefix">#</i>
                    <input id="cantidad" name="cantidad"  type="number" autocomplete="off" value="">
                    <label for="cantidad">Cantidad</label>
                  </div>
                  <div class="input-field col s4">
                    <i class="material-icons prefix">#</i>
                    <input id="exendente" name="exendente"  type="number" autocomplete="off" value="">
                    <label for="exendente">Excendente</label>
                  </div>
                </div>
                <div class="divider"></div>
                <h5 class="header">Observaciones</h5>
                <div class="row">
                  <div class="input-field col s12">
                    <i class="material-icons prefix">question_answer</i>
                    <textarea id="observaciones" name="observaciones" class="materialize-textarea"></textarea>
                    <label for="observaciones">Observaciones</label>
                  </div>
                </div>

              </form>
            <form id="contrato-form" method="post">
                <div class="divider"></div>
                <h5 class="header">Equipo</h5>
                <div class="row">
                  <div class="col s12">
                    <table id="tabla_equipos" class="responsive-table display" cellspacing="0">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Cliente</th>
                          <th>Modelo</th>
                          <th>Área</th>
                          <th>Plaza</th>
                          <th>Costo por clic</th>
                          <th>Excedente</th>
                          <td></td>
                        </tr>
                      </thead>
                      <tbody id="class_equipos">
                         <?php foreach ($equipo as $item){ ?>
                            <tr>
                              <td><?php echo $item->id ?></td>
                              <td><?php echo $item->empresa ?></td>
                              <td><?php echo $item->modelo ?></td>
                              <td><?php echo $item->area ?></td>
                              <td><?php echo $item->plaza ?></td>
                              <td><?php echo $item->costoporclick ?></td>
                              <td><?php echo $item->excedente ?></td>
                            </tr>
                         <?php } ?>
                      </tbody>
                    </table>    
                </div>
            </form>    
                <div class="row">
                  <div class="input-field col s12">
                    <button class="btn cyan waves-effect waves-light right registro" type="button">Guardar
                      <i class="material-icons right">send</i>
                    </button>
                  </div>
                </div>

              </div>
            </div>
          </div>
        
      </div>
    </div>
  </section>