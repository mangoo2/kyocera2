function notificacion(tipo){
    if(tipo==0){//notificacion(0);
        $.alert({
            boxWidth: '30%',useBootstrap: false,
            title: 'Advertencia!',
            content: 'Ingrese una contraseña'}); 
    }
    if(tipo==1){//notificacion(1);
        $.alert({
            boxWidth: '30%',useBootstrap: false,
            title: 'Atención!',
            content: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema'}); 
    }
    if(tipo==2){//notificacion(2);
        $.alert({
            boxWidth: '30%',useBootstrap: false,
            title: 'Advertencia!',
            content: 'No tiene permisos'});
    }
    if(tipo==3){//notificacion(3);
        $.alert({boxWidth: '30%',useBootstrap: false,
            title: 'Advertencia!',
            content: 'Ingrese una cantidad mayor o/y a cero'}); 
    }
    if(tipo==4){//notificacion(4);
        $.alert({boxWidth: '30%',useBootstrap: false,
            title: 'Advertencia!',
            content: 'Seleccione Fisica o Moral'}); 
    }
    if(tipo==5){//notificacion(5);
        $.alert({boxWidth: '30%',useBootstrap: false,
            title: 'Advertencia!',
            content: 'Seleccione una bodega'}); 
    }
    if(tipo==6){//notificacion(6);
        $.alert({boxWidth: '30%',useBootstrap: false,
            title: 'Advertencia!',
            content: 'No hay suficiente stock'}); 
    }
    if(tipo==7){//notificacion(7);
        $.alert({boxWidth: '30%',useBootstrap: false,
            title: 'Advertencia!',
            content: 'PIN incorrecto'}); 
    }
    if(tipo==8){//notificacion(8);
        $.alert({boxWidth: '30%',useBootstrap: false,
            title: 'Advertencia!',
            content: 'Ingrese un PIN'}); 
    }
    if(tipo==9){//notificacion(9);
        toastr.error('No Se encuentra el archivoa','Error!');
    }
    if(tipo==10){//notificacion(10);
        toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema');
    }
}