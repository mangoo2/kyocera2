<style type="text/css">
  .kv-file-upload{
    display: none;
  }
  .error{
    color: red;
  }
</style>
<!-- START CONTENT --> 
<section id="content" width="100%">
  <!--start container-->
  <div class="container" width="100%">
    <div class="section">
      <h4 class="caption">Configuración de facturación.</h4>
      <div class="row">
                    <div class="col s6">
                      <label for="">Regimen</label>
                      <select name="tipov" id="tipov" class="browser-default form-control-bmz" onchange="view_tipov()">
                        <option value="1" <?php if($tipov==1){ echo 'selected';}?>>Kyocera</option>
                        <option value="2" <?php if($tipov==2){ echo 'selected';}?>>D-Impresión</option>
                      </select>
                    </div>
      </div>
      <div class="row">
                    <div class="col s12 m6 l2">
                      <div class="card gradient-45deg-blue-grey-blue-grey gradient-shadow min-height-100 white-text">
                        <div class="padding-4">
                          <div class="col s7 m6">
                            <i class="material-icons background-round mt-5">assignment</i>
                            <p>Timbres</p>
                          </div>
                          <div class="col s5 m6 right-align">
                            <h5 class="mb-0"><?php echo number_format($timbresvigentes,0,'.',',');?></h5>
                            
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col s12 m6 l2">
                      <div class="card gradient-45deg-green-teal gradient-shadow min-height-100 white-text">
                        <div class="padding-4">
                          <div class="col s7 m6">
                            <i class="material-icons background-round mt-5">done</i>
                            <p>Facturadas</p>
                          </div>
                          <div class="col s5 m6 right-align">
                            <h5 class="mb-0"><?php echo number_format($facturastimbradas,0,'.',',');?></h5>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col s12 m6 l2">
                      <div class="card gradient-45deg-purple-deep-orange gradient-shadow min-height-100 white-text">
                        <div class="padding-4">
                          <div class="col s7 m6">
                            <i class="material-icons background-round mt-5">block</i>
                            <p>Canceladas</p>
                          </div>
                          <div class="col s5 m6 right-align">
                            <h5 class="mb-0"><?php echo number_format($facturascanceladas,0,'.',',');?></h5>
                            
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col s12 m6 l2">
                      <div class="card gradient-45deg-green-teal gradient-shadow min-height-100 white-text">
                        <div class="padding-4">
                          <div class="col s7 m6">
                            <i class="material-icons background-round mt-5">done</i>
                            <p>Complementos</p>
                          </div>
                          <div class="col s5 m6 right-align">
                            <h5 class="mb-0"><?php echo number_format($complementotimbradas,0,'.',',');?></h5>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col s12 m6 l2">
                      <div class="card gradient-45deg-blue-grey-blue-grey gradient-shadow min-height-100 white-text">
                        <div class="padding-4">
                          <div class="col s7 m6">
                            <i class="material-icons background-round mt-5">assignment</i>
                            <p>Consumidos</p>
                          </div>
                          <div class="col s5 m6 right-align">
                            <h5 class="mb-0"><?php 
                              $consumidostimbre=$timbresvigentes-$facturasdisponibles;
                            echo number_format($consumidostimbre,0,'.',',');?></h5>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col s12 m6 l2">
                      <div class="card gradient-45deg-blue-grey-blue-grey gradient-shadow min-height-100 white-text">
                        <div class="padding-4">
                          <div class="col s7 m6">
                            <i class="material-icons background-round mt-5">assignment</i>
                            <p>Disponibles</p>
                          </div>
                          <div class="col s5 m6 right-align">
                            <h5 class="mb-0"><?php echo number_format($facturasdisponibles,0,'.',',');?></h5>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col s12 m6 l2">
                      <div class="card gradient-45deg-blue-grey-blue-grey gradient-shadow min-height-100 white-text">
                        <div class="padding-4">
                          
                          <div class="col s12 m12 right-align">
                            <h5 class="mb-0"><?php echo $fechafin;?></h5>
                          </div>
                          <div class="col s12 m12">
                            <p>Fin de vigencia</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
      <div class="row">
        <div class="col s6">
          <a class="btn waves-effect waves-light gradient-45deg-green-teal gradient-shadow col s4" href="<?php echo base_url();?>Conf_facturacion/unidades_servicios">servicios/unidades</a>
        </div>
        <div class="col s6">
          <a class="btn waves-effect waves-light gradient-45deg-green-teal gradient-shadow col s5" href="<?php echo base_url();?>Conf_facturacion/unidades_servicios_default">servicios/unidades Default</a>
        </div>
      </div>
      <div class="row">
        <div class="col s12">
          <ul class="collapsible collapsible-accordion" data-collapsible="accordion">
            <li class="active">
              <div class="collapsible-header active">
                Datos Generales</div>
              <div class="collapsible-body" style="display: block;">
                <form class="row" id="formdatosgenerales"  >
                  <input type="hidden" name="ConfiguracionesId"  value="<?php echo $ConfiguracionesId;?>" id="ConfiguracionesId">
                  <div class="row">
                    <div class="col s3">
                      <label for="Nombre" class="active">RASON SOCIAL</label>
                      <input type="text" id="Nombre" name="Nombre" class="form-control-bmz" value="<?php echo $Nombre;?>">          
                    </div>
                    <div class="col s3">
                      <label for="Curp" class="active">CURP</label>
                      <input type="text" id="Curp" name="Curp" class="form-control-bmz" value="<?php echo $Curp;?>">          
                    </div>
                    <div class="col s3">
                      <label for="Rfc" class="active">RFC</label>
                      <input type="text" id="Rfc" name="Rfc" class="form-control-bmz" value="<?php echo $Rfc;?>" required>          
                    </div>
                    <div class="col s3">
                      <label for="Rfc" class="active">RÉGIMEN FISCAL</label>
                      <select id="Regimen" name="Regimen" class="form-control-bmz browser-default">';
                        <option value="601" <?php if($Regimen==601){ echo 'selected';} ?> >601 General de Ley Personas Morales</option>
                        <option value="603" <?php if($Regimen==603){ echo 'selected';} ?> >603 Personas Morales con Fines no Lucrativos</option>
                        <option value="605" <?php if($Regimen==605){ echo 'selected';} ?> >605 Sueldos y Salarios e Ingresos Asimilados a Salarios</option>
                        <option value="606" <?php if($Regimen==606){ echo 'selected';} ?> >606 Arrendamiento</option>
                        <option value="607" <?php if($Regimen==607){ echo 'selected';} ?> >607 Régimen de Enajenación o Adquisición de Bienes</option>
                        <option value="608" <?php if($Regimen==608){ echo 'selected';} ?> >608 Demás ingresos</option>
                        <option value="609" <?php if($Regimen==609){ echo 'selected';} ?> >609 Consolidación</option>
                        <option value="610" <?php if($Regimen==610){ echo 'selected';} ?> >610 Residentes en el Extranjero sin Establecimiento Permanente en México</option>
                        <option value="611" <?php if($Regimen==611){ echo 'selected';} ?> >611 Ingresos por Dividendos (socios y accionistas)</option>
                        <option value="612" <?php if($Regimen==612){ echo 'selected';} ?> >612 Personas Físicas con Actividades Empresariales y Profesionales</option>
                        <option value="614" <?php if($Regimen==614){ echo 'selected';} ?> >614 Ingresos por intereses</option>
                        <option value="615" <?php if($Regimen==615){ echo 'selected';} ?> >615 Régimen de los ingresos por obtención de premios</option>
                        <option value="616" <?php if($Regimen==616){ echo 'selected';} ?> >616 Sin obligaciones fiscales</option>
                        <option value="620" <?php if($Regimen==620){ echo 'selected';} ?> >620 Sociedades Cooperativas de Producción que optan por diferir sus ingresos</option>
                        <option value="621" <?php if($Regimen==621){ echo 'selected';} ?> >621 Incorporación Fiscal</option>
                        <option value="622" <?php if($Regimen==622){ echo 'selected';} ?> >622 Actividades Agrícolas, Ganaderas, Silvícolas y Pesqueras</option>
                        <option value="623" <?php if($Regimen==623){ echo 'selected';} ?> >623 Opcional para Grupos de Sociedades</option>
                        <option value="624" <?php if($Regimen==624){ echo 'selected';} ?> >624 Coordinados</option>
                        <option value="625" <?php if($Regimen==625){ echo 'selected';} ?> >625 Régimen de las Actividades Empresariales con ingresos a través de Plataformas Tecnológicas</option>
                        <option value="626" <?php if($Regimen==626){ echo 'selected';} ?> >626 Régimen Simplificado de Confianza </option>
                        <option value="628" <?php if($Regimen==628){ echo 'selected';} ?> >628 Hidrocarburos</option>
                        <option value="629" <?php if($Regimen==629){ echo 'selected';} ?> >629 De los Regímenes Fiscales Preferentes y de las Empresas Multinacionales</option>
                        <option value="630" <?php if($Regimen==630){ echo 'selected';} ?> >630 Enajenación de acciones en bolsa de valores</option>
                                     
                      </select>         
                    </div>
                  </div>
                  <div class="row">
                    <div class="col s3">
                      <label for="Calle" class="active">CALLE</label>
                      <input type="text" id="Calle" name="Calle" class="form-control-bmz" value="<?php echo $Calle;?>">          
                    </div>
                    <div class="col s3">
                      <label for="localidad" class="active">LOCALIDAD</label>
                      <input type="text" id="localidad" name="localidad" class="form-control-bmz" value="<?php echo $localidad;?>">          
                    </div>
                    <div class="col s3">
                      <label for="Noexterior" class="active">N° EXT.</label>
                      <input type="text" id="Noexterior" name="Noexterior" class="form-control-bmz" value="<?php echo $Noexterior;?>">          
                    </div>
                    <div class="col s3">
                      <label for="Nointerior" class="active">N° INT.</label>
                      <input type="text" id="Nointerior" name="Nointerior" class="form-control-bmz" value="<?php echo $Nointerior;?>">          
                    </div>
                    <div class="col s3">
                      <label for="Municipio" class="active">MUNICIPIO</label>
                      <input type="text" id="Municipio" name="Municipio" class="form-control-bmz" value="<?php echo $Municipio;?>">          
                    </div>
                    <div class="col s3">
                      <label for="Colonia" class="active">COLONIA</label>
                      <input type="text" id="Colonia" name="Colonia" class="form-control-bmz" value="<?php echo $Colonia;?>">          
                    </div>
                    <div class="col s3">
                      <label for="Estado" class="active">ESTADO</label>
                      <input type="text" id="Estado" name="Estado" class="form-control-bmz" value="<?php echo $Estado;?>">          
                    </div>
                    <div class="col s3">
                      <label for="CodigoPostal" class="active">CÓDIGO POSTAL</label>
                      <input type="text" id="CodigoPostal" name="CodigoPostal" class="form-control-bmz" value="<?php echo $CodigoPostal;?>">          
                    </div>
                    <div class="col s3">
                      <label for="PaisExpedicion" class="active">PAIS</label>
                      <input type="text" id="PaisExpedicion" name="PaisExpedicion" class="form-control-bmz" value="<?php echo $PaisExpedicion;?>">          
                    </div>



                  </div>
                </form>
                <div class="row">
                  <div class="col s12">
                    <a  class="waves-effect green btn-bmz" onclick="actualizardatos()" >Actualizar</a>
                  </div>
                </div>
                
              </div>
            </li>
            <li>
              <div class="collapsible-header">
                Instalación de certificado de sello digital</div>
              <div class="collapsible-body">
                <!--<form action="<?php echo base_url();?>Conf_facturacion/procesar" method="post" class="row" id="formarchivos"   >-->
                  <?php echo form_open_multipart('Conf_facturacion/procesar');?>
                  
                  
                  
                  <div class="row">
                    <div class="col s3">
                      
                    </div>
                    <div class="col s3">
                      <!--<a type="submit" class="waves-effect green btn-bmz" onclick="procesar()">Instalar</a>-->
                      
                    </div>
                    
                  </div>
                  
                </form>
                <div class="row">
                  <div class="col s6">
                    <input type="file" id="cerdigital" name="cerdigital" class="form-control-bmz" required >
                  </div>
                  <div class="col s6">
                    <input type="file" id="claveprivada" name="claveprivada" class="form-control-bmz" required>
                    <div class="row" style="margin-top: 5px;">
                      <label for="passprivada" class="active col s3" style="color: black; font-size: 14px;">Contraseña de clave privada:</label>
                      <div class="col s9">
                        <input type="password" id="passprivada" name="passprivada" class="form-control-bmz" required>          
                      </div>
                    </div>
                  </div>
                  <div class="col s12">
                    <a  class="waves-effect green btn-bmz" onclick="instalararchivos()" >Instalar</a>
                  </div>
                </div>
                <div class="col s12 iframeinstalacion" style="display:none;">
                  
                </div>
              </div>
            </li>
          </ul>
        </div>
       

          
        
      </div>
    </div>
  </div>
</section>