<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Pdfview extends CI_Controller {
    function __construct()    {
        parent::__construct();
        if($this->session->userdata('logeado')==true){
        }else{
            redirect('Sistema'); 
        }
    }
	function index(){
  
        $this->load->view('pdfview');

  	}

}