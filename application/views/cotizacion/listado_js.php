<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/plugins/toastr/toastr.css">
<script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/toastr/toastr.min.js" ></script>

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/plugins/touchspin/jquery.bootstrap-touchspin.min.css">
<script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/touchspin/jquery.bootstrap-touchspin.min.js" ></script>

<script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/loader/jquery.loadingModal.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/plugins/loader/jquery.loadingModal.css">

<link href="<?php echo base_url(); ?>public/plugins/select2/select2.min.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/select2/select2.full.min.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>public/js/listado_cotizaciones.js?v=<?php echo date('YmdGis');?>" ></script>

<script type="text/javascript">
        $(document).ready(function($) {
<?php if($idCliente==0){ ?>
    
    		$('.header').html('Listado de Cotizaciones');
    		$('.btn-new-cot').remove();
    	
<?php } ?>
        <?php 
            if (isset($_GET['cot'])) {
                ?>
                setTimeout(function(){ 
                    table.search('<?php echo $_GET['cot'];?>').draw();
                }, 1300);
                <?php
            }
        ?>
        });
</script>