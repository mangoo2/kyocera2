<style type="text/css">
  table td{font-size: 12px;padding: 6px 7px;}
  .btns-factura{min-width: 178px;}
  .btn-retimbrar{/*display: none; */}
  .select2-container{width: 100% !important;}
  .no_tienerelacion{max-width: 33px;}
  .consultar_fac_pue.comp-success{background-color: #4caf50;border-color: #4caf50;}
  .docfileacuse iframe{width: 100%;border: 0px;height: 400px;}
  .info_pni{display: none;}
  .email.enviado{background-color: #1e710a;border-color: #1e710a;}
  .minheaderinput{
    min-height: 59px !important;
  }
  .fa-stack{
  width: auto;
}
.btns-factura .btn-retimbrar{
  min-width: 16px;
  padding: 5px 11px;
  text-align: left;
}
</style>
<div  clas="row" style="display: none;">
 <input type="password" name="" >
  <input type="search" name="" >
  <input id="perfilid" type="hidden" name="perfilid" value="<?php echo $perfilid; ?>">
  <input type="hidden" id="codigocarga" value="<?php echo date('YmdGis');?>" readonly> 
  <input id="fechahoy" type="hidden" value="<?php echo date('Y-m-d'); ?>">
</div>

<!-- START CONTENT -->
<section id="content">
  <!--start container-->
    <div class="container">
      <div class="section">
        <div class="">
          <h4 class="header">Facturación</h4>
          <div class="row">
            <div class="col s12">
              <ul class="tabs tab-demo z-depth-1">
                <li class="tab col s3"><a class="active" href="#test1">Facturas</a></li>
                <li class="tab col s3"><a href="#test3" onclick="loadtable_c()">Complementos</a></li>
                <li class="tab col s3" style="display:none;"><a href="#test2" >Pendientes</a></li>
                <li class="tab col s3" ><a href="#test5" onclick="loadtable_save()">Guardadas</a></li>
              </ul>
            </div>
            <div class="col s12 ">
              <div id="test1" class="col s12 card-panel">
                <!------------->
                <div class="row">
                  <div class="col s6 m10 l10"></div>
                  <div class="col s6 m2 l2">
                    <a class="waves-effect green btn-bmz" href="<?php echo base_url();?>Facturaslis/add">Nuevo</a>
                    <?php if($viewidpersonal!=0){ ?><a class="waves-effect red btn-bmz" onclick="cancelarfacturas()">Cancelar</a><?php } ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col s6 m2 l2 minheaderinput" style="display: <?php echo $vendedor; ?>;">
                    <label>Vendedor</label>
                    <select id="personals" class="browser-default form-control-bmz filtrosdatatable" >
                      <option value="0">Todos</option>
                      <?php foreach ($personalrows->result() as $item) { ?>
                        <option value="<?php echo $item->personalId; ?>" <?php if($item->personalId==$idpersonal){ echo 'selected';}?> ><?php echo $item->nombre; ?> <?php echo $item->apellido_paterno; ?> <?php echo $item->apellido_materno; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                  <div class="col s6 m2 l2 minheaderinput">
                    <label>Cliente</label>
                    <select id="idcliente" class="browser-default form-control-bmz filtrosdatatable" >
                      <?php if(isset($_GET['clienteid'])){ ?>
                        <option value="<?php echo $_GET['clienteid'];?>"><?php echo $_GET['cliente'];?></option>
                      <?php } ?><option value="0">Todos</option></select>
                  </div>
                  <div class="col s6 m2 l2 minheaderinput">
                    <label>RFC</label>
                    <select id="idcliente_rfc" class="browser-default form-control-bmz filtrosdatatable" ><option value="0">Todos</option></select>
                  </div>
                  <div class="col s6 m2 l2 minheaderinput">
                    <label>Fecha Inicio</label>
                    <input type="date" id="finicial" class="form-control-bmz filtrosdatatable" value="<?php echo $fechainicial_reg;?>" >
                  </div>
                  <div class="col s6 m2 l2 minheaderinput">
                    <label>Fecha Fin</label>
                    <input type="date" id="ffinal" class="form-control-bmz filtrosdatatable" >
                  </div>
                  
                  <div class="col s6 m2 l2 minheaderinput ">
                    <label>Razón social</label>
                    <select id="rs_folios" class="browser-default form-control-bmz filtrosdatatable" onchange="loadtable()">
                      <option value="U" <?php if(isset($_GET['tipov'])){ if($_GET['tipov']==1){ echo 'selected';} } ?> >Kyocera</option>
                      <option value="D" <?php if(isset($_GET['tipov'])){ if($_GET['tipov']==2){ echo 'selected';} } ?> >D-impresión</option>
                    </select>
                  </div>
                  
                </div>
                <div class="row">
                  <div class="col s6 m2 l2 minheaderinput ">
                    <label>Método de pago</label>
                    <select id="rs_metodopago" class="browser-default form-control-bmz filtrosdatatable" >
                      <option value="0">Todas</option>
                      <option value="1">Pago en una sola exhibición</option>
                      <option value="2">Pago en parcialidades o diferido</option>
                    </select>
                  </div>
                  <div class="col s6 m2 l2 minheaderinput ">
                    <label>Tipo CFDI</label>
                    <select id="rs_tcfdi" class="browser-default form-control-bmz filtrosdatatable" >
                      <option value="0">Todas</option>
                      <option value="1">Facturas</option>
                      <option value="2">Factura 01 Nota Credito</option>
                      <option value="3">Factura 04 Sustitucion</option>
                      <option value="4">Factura de Anticipo recibido</option>
                      <option value="5">Factura con relacion de Anticipo</option>
                      <option value="6">Factura Egreso de Anticipo</option>
                    </select>
                  </div>
                  <div class="col s6 m2 l2 minheaderinput ">
                    <label>Tipo Documento</label>
                    <select id="rs_pendsave" class="browser-default form-control-bmz filtrosdatatable" >
                      <option value="0">Todas</option>
                      <option value="1">Pendientes</option>
                      <option value="2">Guardadas</option>
                    </select>
                  </div>
                  <div class="col s6 m2 l2 form-check form-check-inline" ><!--Solo se oculta si lo requieren se activa nuevamente solo se oculto por temas de ergonomia-->
                    <input type="checkbox" name="checkbox1" id="tipoview" class="form-check-input" value="<?php echo $viewidpersonal;?>" onchange="loadtableswitch()">
                  </div>
                  <div class="col s4 m1  l1">
                        <button class="b-btn b-btn-primary" onclick="loadtable()" style="margin-top: 20px;" title="Realizar consulta"><i class="fas fa-filter"></i> <span class="btn-filtrar"></span></button>
                      </div>
                </div>
                <div class="row">
                  <div class="col s12 m12 l12" style="text-align: end;">
                    <a class="waves-effect green btn-bmz" title="Registrar pagos no reconocidos" onclick="r_pago_n_reconocidos()">Registrar pago</a>
                    <button type="button" class="b-btn b-btn-primary email " onclick="enviarmailfacall()"><i class="fas fa-mail-bulk"></i></button>
                    <a class="b-btn b-btn-primary tooltipped" data-position="top" data-delay="50" data-tooltip="Buscar factura" onclick="msearchpre()"  ><i class="fas fa-search"></i></a>
            
                  </div>
                </div>
                <table id="tabla_facturacion" name="tabla_facturacion_cliente" class="responsive-table display" cellspacing="0">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Folio</th>
                      <th>Cliente</th>
                      <th>RFC</th>
                      <th>Monto</th>
                      <th>Estatus</th><!--sin timbrar timbrada cancelada-->
                      <th>Fecha</th>
                      <th>Método de pago</th>
                      <th>Personal</th>
                      <th>Tipo</th>
                      <th></th>
                      
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
                <!------------->
              </div>
              <div id="test2" class="col s12 card-panel">
                 <!------------->
                  <div class="row">
                    <div class="col s12" style="text-align: end;">
                      <a class="waves-effect green btn-bmz" onclick="loadtable_fpp()" >Consultar</a>
                    </div>
                  </div>
                <div class="row">
                  <div class="col s2" style="display: <?php echo $vendedor; ?>;">
                    <label>Vendedor</label>
                    <select id="personals_pp" class="browser-default form-control-bmz" onchange="loadtable_fpp()">
                      <option value="0">Todos</option>
                      <?php foreach ($personalrows->result() as $item) { ?>
                        <option value="<?php echo $item->personalId; ?>" <?php if($item->personalId==$idpersonal){ echo 'selected';}?> ><?php echo $item->nombre; ?> <?php echo $item->apellido_paterno; ?> <?php echo $item->apellido_materno; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                  <div class="col s2">
                    <label>Cliente</label>
                    <select id="idcliente_pp" class="browser-default form-control-bmz" onchange="loadtable_fpp()">
                      <?php if(isset($_GET['clienteid'])){ ?>
                        <option value="<?php echo $_GET['clienteid'];?>"><?php echo $_GET['cliente'];?></option>
                      <?php } ?>
                      <option value="0">Todos</option>
                    </select>
                  </div>
                  <div class="col s2">
                    <label>RFC</label>
                    <select id="idcliente_rfc_pp" class="browser-default form-control-bmz" onchange="loadtable_fpp()">
                      <option value="0">Todos</option>
                    </select>
                  </div>
                  <div class="col s2">
                    <label>Fecha Inicio</label>
                    <input type="date" id="finicial_pp" class="form-control-bmz" value="<?php echo $fechainicial_reg;?>" onchange="loadtable_fpp()">
                  </div>
                  <div class="col s2">
                    <label>Fecha Fin</label>
                    <input type="date" id="ffinal_pp" class="form-control-bmz" onchange="loadtable_fpp()">
                  </div>
                  
                  <div class="col s2 selectweb">
                    <label>Razón social</label>
                    <select id="rs_folios_pp" class="browser-default form-control-bmz" onchange="loadtable_fpp()">
                      <option value="U">Kyocera</option>
                      <option value="D">D-impresión</option>
                    </select>
                  </div>

                  <div class="col s2 form-check form-check-inline">
                    <input type="checkbox" name="checkbox1" id="tipoview_pp" class="form-check-input" value="<?php echo $viewidpersonal;?>" onchange="loadtableswitch()">
                  </div>
                </div>
                <table id="tabla_facturacion_pp" class="responsive-table display" cellspacing="0">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Folio</th>
                      <th>Cliente</th>
                      <th>RFC</th>
                      <th>Monto</th>
                      <th>Estatus</th><!--sin timbrar timbrada cancelada-->
                      <th>Fecha</th>
                      <th>Personal</th>
                      <th></th>
                      
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
                <!------------->
              </div>
              <div id="test4" class="col s12 card-panel" style="display:none;">
                <div class="row">
                  <div class="col s3">
                    <label>Tipo</label>
                    <select id="ptipo" class="browser-default form-control-bmz" onchange="loadtablep()">
                      <option value="0">Seleccione</option>
                      <option value="1">Venta</option>
                      <option value="2">Venta Combinada</option>
                      <option value="3">Poliza</option>
                      <!--<option value="4">Servicio</option>-->
                      <option value="5">Renta</option>

                    </select>
                  </div>
                  <div class="col s3" style="display: <?php echo $vendedor; ?>;">
                    <label>Vendedor</label>
                    <select id="personalp" class="browser-default form-control-bmz" onchange="loadtablep()">
                      <option value="0">Todos</option>
                      <?php foreach ($personalrows->result() as $item) { ?>
                        <option value="<?php echo $item->personalId; ?>" <?php if($item->personalId==$idpersonal){ echo 'selected';}?> ><?php echo $item->nombre; ?> <?php echo $item->apellido_paterno; ?> <?php echo $item->apellido_materno; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                <div class="row">
                  <table id="tablependientesf" class="responsive-table display">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Cliente</th>
                        <th>vendedor</th>
                        <th></th>
                      </tr>
                    </thead>
                  </table>
                </div>
                
              </div>
              <div id="test3" class="col s12 card-panel">
                <div class="row">
                  <div class="col s10">
                    
                  </div>
                  <div class="col s2">
                    <?php if($viewidpersonal!=1){ ?>
                      <a class="waves-effect red btn-bmz" onclick="cancelarcomplementos()">Cancelar</a>
                    <?php } ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col s4 m2 l2" style="display: <?php echo $vendedor; ?>;">
                    <label>Vendedor</label>
                    <select id="personalsc" class="browser-default form-control-bmz" >
                      <option value="0">Todos</option>
                      <?php foreach ($personalrows->result() as $item) { ?>
                        <option value="<?php echo $item->personalId; ?>" <?php if($item->personalId==$idpersonal){ echo 'selected';}?> ><?php echo $item->nombre; ?> <?php echo $item->apellido_paterno; ?> <?php echo $item->apellido_materno; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                  <div class="col s4 m2 l2">
                    <label>Cliente</label>
                    <select id="idclientec" class="browser-default form-control-bmz" >
                      <option value="0">Todos</option>
                    </select>
                  </div>
                  <div class="col s4 m2 l2">
                    <label>Fecha Inicio</label>
                    <input type="date" id="finicial_c" class="form-control-bmz" value="<?php echo $fechainicial_reg;?>" >
                  </div>
                  <div class="col s4 m2 l2">
                    <label>Fecha Fin</label>
                    <input type="date" id="ffinal_c" class="form-control-bmz" >
                  </div>
                  <div class="col s4 m2 l2 selectweb">
                    <label>Razón social</label>
                    <select id="rs_foliosc" class="browser-default form-control-bmz" >
                      <option value="U">Kyocera</option>
                      <option value="D">D-impresión</option>
                    </select>
                  </div>
                  <div class="col s4 m1  l1">
                        <button class="b-btn b-btn-primary" onclick="loadtable_c()" style="margin-top: 20px;" title="Realizar consulta"><i class="fas fa-filter"></i> <span class="btn-filtrar"></span></button>
                      </div>
                </div>
                <table id="tabla_complementos" class="table display" cellspacing="0">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Cliente</th>
                      <th>Folios</th>
                      <th>Monto</th>
                      <th>Estatus</th><!--sin timbrar timbrada cancelada-->
                      <th>Fecha</th>
                      <th>creado</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
              <div id="test5" class="col s12 card-panel">
                <!------------->
                
                <div class="row">
                  <div class="col s6 m2 l2 minheaderinput" style="display: <?php echo $vendedor; ?>;">
                    <label>Vendedor</label>
                    <select id="personals_save" class="browser-default form-control-bmz filtrosdatatable_s" >
                      <option value="0">Todos</option>
                      <?php foreach ($personalrows->result() as $item) { ?>
                        <option value="<?php echo $item->personalId; ?>" <?php if($item->personalId==$idpersonal){ echo 'selected';}?> ><?php echo $item->nombre; ?> <?php echo $item->apellido_paterno; ?> <?php echo $item->apellido_materno; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                  <div class="col s6 m2 l2 minheaderinput">
                    <label>Cliente</label>
                    <select id="idcliente_save" class="browser-default form-control-bmz filtrosdatatable_s" >
                      
                  </div>
                  <div class="col s6 m2 l2 minheaderinput">
                    <label>RFC</label>
                    <select id="idcliente_rfc_save" class="browser-default form-control-bmz filtrosdatatable_s" ><option value="0">Todos</option></select>
                  </div>
                  <div class="col s6 m2 l2 minheaderinput">
                    <label>Fecha Inicio</label>
                    <input type="date" id="finicial_save" class="form-control-bmz filtrosdatatable_s" value="<?php echo $fechainicial_reg;?>" >
                  </div>
                  <div class="col s6 m2 l2 minheaderinput">
                    <label>Fecha Fin</label>
                    <input type="date" id="ffinal_save" class="form-control-bmz filtrosdatatable_s" >
                  </div>
                  
                  <div class="col s6 m2 l2 minheaderinput ">
                    <label>Razón social</label>
                    <select id="rs_folios_save" class="browser-default form-control-bmz filtrosdatatable_s" onchange="loadtable_save()">
                      <option value="U" <?php if(isset($_GET['tipov'])){ if($_GET['tipov']==1){ echo 'selected';} } ?> >Kyocera</option>
                      <option value="D" <?php if(isset($_GET['tipov'])){ if($_GET['tipov']==2){ echo 'selected';} } ?> >D-impresión</option>
                    </select>
                  </div>
                  
                </div>
                <div class="row">
                  <div class="col s6 m2 l2 minheaderinput ">
                    <label>Método de pago</label>
                    <select id="rs_metodopago_save" class="browser-default form-control-bmz filtrosdatatable_s" >
                      <option value="0">Todas</option>
                      <option value="1">Pago en una sola exhibición</option>
                      <option value="2">Pago en parcialidades o diferido</option>
                    </select>
                  </div>
                  <div class="col s6 m2 l2 minheaderinput ">
                    <label>Tipo CFDI</label>
                    <select id="rs_tcfdi_save" class="browser-default form-control-bmz filtrosdatatable_s" >
                      <option value="0">Todas</option>
                      <option value="1">Facturas</option>
                      <option value="2">Factura 01 Nota Credito</option>
                      <option value="3">Factura 04 Sustitucion</option>
                      <option value="4">Factura de Anticipo recibido</option>
                      <option value="5">Factura con relacion de Anticipo</option>
                      <option value="6">Factura Egreso de Anticipo</option>
                    </select>
                  </div>
                  <div class="col s6 m2 l2 minheaderinput" style="display:none;">
                    <label>Tipo Documento</label>
                    <select id="rs_pendsave_save" class="browser-default form-control-bmz filtrosdatatable_s" >
                      <!--<option value="0">Todas</option>
                      <option value="1">Pendientes</option>-->
                      <option value="2">Guardadas</option>
                    </select>
                  </div>
                  <div class="col s4 m1  l1">
                        <button class="b-btn b-btn-primary" onclick="loadtable_save()" style="margin-top: 20px;" title="Realizar consulta"><i class="fas fa-filter"></i> <span class="btn-filtrar"></span></button>
                      </div>
                </div>
                
                <table id="tabla_facturacion_save" name="tabla_facturacion_cliente" class="responsive-table display" cellspacing="0" style="width:100%">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Folio</th>
                      <th>Cliente</th>
                      <th>RFC</th>
                      <th>Monto</th>
                      <th>Estatus</th><!--sin timbrar timbrada cancelada-->
                      <th>Fecha</th>
                      <th>Método de pago</th>
                      <th>Personal</th>
                      <th>Tipo</th>
                      <th>Programación</th>
                      <th></th>
                      
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
                <!------------->
              </div>
            </div>
          </div>
        </div>
      </div> 
    </div>
</section>
<div id="modalcomplementos" class="modal" >
  <div class="modal-content" style="padding: 5px">
    <div class="row">
      <div class="col s12">
        <a class="waves-effect green btn-bmz" onclick="addcomplemento()" >Nuevo</a>
      </div>
    </div>
    <div class="row"><div class="col s12 listadocomplementos"></div></div>
  </div>  
  <div class="modal-footer">
    <a class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
  </div>
</div>





<div id="modal_pago_s" class="modal"  > 
    <div class="modal-content "> 
      <div class="row"> 
        <div class="col s6"> 
          <h5>Registro de nuevo pago</h5> 
        </div> 
        <div class="col s6" align="right"> 
          <div class="row" style="margin-bottom: 0px;">
            <div class="col s7">Monto de prefactura</div>
            <div style="color: red" class="col s5 monto_prefactura_s"></div>
          </div>
          <div class="row" style="margin-bottom: 0px;">
            <div class="col s7">Restante</div>
            <div class="col s5 restante_prefactura_s" style="color: red"></div>
          </div>
        </div> 
      </div>   
      <h6>Vendedor: <?php echo($_SESSION['usuario']); ?></h6> 
      <form class="form" id="form_pago_servicio" method="post">  
        <input type="hidden" id="idservicio" name="idservicio"><!--id de la factura --> 
        <div class="row"> 
          <div class="input-field col s3"> 
            <input type="date" name="fecha" id="fecha_p_s" class="form-control-bmz"> 
            <label for="fecha" class="active">Fecha</label> 
          </div> 
          <div class="col s4"> 
            <label for="idmetodo" class="active">Método de pago</label><br> 
            <select name="idmetodo" class="browser-default form-control-bmz"> 
               <option value="1">01 Efectivo</option><option value="2">02 Cheque nominativo</option><option value="3">03 Transferencia electrónica de fondos</option><option value="4">04 Tarjetas de Crédito</option><option value="5">05 Monedero Electrónico</option><option value="6">06 Dinero Electrónico</option><option value="7">08 Vales de despensa</option><option value="8">12 Dación en pago</option><option value="9">13 Pago por subrogación</option><option value="10">14 Pago por consignación</option><option value="11">15 Condonación</option><option value="12">17 Compensación</option><option value="13">23 Novación</option><option value="14">24 Confusión</option><option value="15">25 Remisión de deuda</option><option value="16">26 Prescripción o caducidad</option><option value="17">27 A satisfacción del acreedor</option><option value="18">28 Tarjeta de Débito</option><option value="19">29 Tarjeta de Servicio</option><option value="20">30 Aplicación de anticipos</option><option value="21">99 Por definir</option><option value="31">31 Intermediario pagos</option> 
            </select>   
          </div> 
          <div class="input-field col s4"> 
            <input type="number" name="pago" id="pago_p_s" class="active form-control-bmz" placeholder="0.0"> 
            <label for="pago" class="active">Monto</label> 
          </div> 
        </div> 
      
        <div class="row"> 
          <div class="input-field col s9"> 
            <textarea name="observacion" ></textarea> 
            <label for="observacion_p" class="active">Comentario</label> 
          </div>
          <div class="col s3">
            <a class="waves-effect waves-light btn modal-triggert" onclick="guardar_pago_servicios()">Guardar</a> 
          </div> 
        </div> 
      </form>
      <div class="row">
        <div class="col s12"> 
          <span class="text_tabla_pago_servicios"></span> 
        </div> 
      </div>
    </div>     
  </div> 


<div id="modalnotascredito" class="modal" >
  <div class="modal-content" style="padding: 5px">
    <div class="row">
      <div class="col s12">
        <h4>Notas credito</h4>
      </div>
    </div>
    <div class="row">
      <div class="col s12 ">
        <table class="table" id="table_notacredito">
          <thead><tr><th>Folio</th><th>Fecha</th><th>Monto</th><th></th></tr></thead>
          <tbody class="listadonotascredito"></tbody>
          
        </table>
      </div>
    </div>
  </div>  
  <div class="modal-footer">
    <a class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
  </div>
</div>

<div id="modaldocacuse" class="modal" >
  <div class="modal-content" style="padding: 5px">
    <div class="row">
      <div class="col s12">
        <h4>Acuse de cancelacion</h4>
      </div>
    </div>
    <div class="row">
      <div class="col s12 ">
        <input type="file" name="fileacuse" id="fileacuse">
      </div>
      <div class="col s12 docfileacuse">
        
      </div>
    </div>
  </div>  
  <div class="modal-footer">
    <a class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
  </div>
</div>




<div id="modal_enviomail" class="modal">
    <div class="modal-content">
      <div class="col s12">
        <label>Firma del ejecutivo</label>
        <select class="browser-default form-control-bmz" id="firma_m_f">
          <?php foreach ($perfirma->result() as $itemf) {
            echo '<option value="'.$itemf->personalId.'">'.$itemf->nombre.' '.$itemf->apellido_paterno.' '.$itemf->apellido_materno.' / '.$itemf->email.' / '.$itemf->celular.'</option>';
          }?>
        </select>
      </div>
      <div class="col s12">
        <label>Copia oculta (BBC)</label>
        <select class="browser-default form-control-bmz" id="bbc_m_f"><option value="0"></option>
          <?php foreach ($perfirma->result() as $itemf) {
            echo '<option value="'.$itemf->personalId.'">'.$itemf->nombre.' '.$itemf->apellido_paterno.' '.$itemf->apellido_materno.' / '.$itemf->email.' / '.$itemf->celular.'</option>';
          }?>
        </select>
      </div>
      <div class="col s12">
          <label>Correo</label>
          <div class="m_s_contac"></div>
        </div>
      <div class="row">
        <div class="col s12">
          <label>Asunto</label>
          <input class="form-control-bmz" id="ms_asucto2" value="Factura">
        </div>
        <div class="col s12">
          <label>Comentario</label>
          <textarea class="form-control-bmz" id="ms_coment2" style="min-height: 150px;"></textarea>
        </div>
        <div class="col s12 input_envio_s">
        </div>
        <div class="col s12 table_mcr_files_s"></div>
      </div>
      <div class="row"><div class="col s12 iframefac"></div></div>
      <div class="row"><div class="col s12 addbtnservicio"></div></div>
      
 
    </div>
    <div class="row">
      <div class="col m12" align="right">
        <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
      </div>
    </div>        
</div>

<div id="modal_enviomailcomp" class="modal">
  <div class="modal-content">
    <div class="col s12">
      <label>Firma del ejecutivo</label>
      <select class="browser-default form-control-bmz" id="firma_m_c">
        <?php foreach ($perfirma->result() as $itemf) {
          echo '<option value="'.$itemf->personalId.'">'.$itemf->nombre.' '.$itemf->apellido_paterno.' '.$itemf->apellido_materno.' / '.$itemf->email.' / '.$itemf->celular.'</option>';
        }?>
      </select>
    </div>
    <div class="col s12">
      <label>Copia oculta (BBC)</label>
      <select class="browser-default form-control-bmz" id="bbc_m_c"><option value="0"></option>
        <?php foreach ($perfirma->result() as $itemf) {
          echo '<option value="'.$itemf->personalId.'">'.$itemf->nombre.' '.$itemf->apellido_paterno.' '.$itemf->apellido_materno.' / '.$itemf->email.' / '.$itemf->celular.'</option>';
        }?>
      </select>
    </div>
    <div class="col s12">
        <label>Correo</label>
        <div class="m_s_contac_c"></div>
      </div>
    <div class="row">
      <div class="col s12">
        <label>Asunto</label>
        <input class="form-control-bmz" id="ms_asucto2_c" value="Complemento">
      </div>
      <div class="col s12">
        <label>Comentario</label>
        <textarea class="form-control-bmz" id="ms_coment2_c" style="min-height: 150px;"></textarea>
      </div>
      <div class="col s12 input_envio_s_c">
      </div>
      <div class="col s12 table_mcr_files_s_c"></div>
    </div>
    <div class="row"><div class="col s12 iframefac_c"></div></div>
    <div class="row"><div class="col s12 addbtnservicio_c"></div></div>
  </div>
  <div class="row">
    <div class="col m12" align="right">
      <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
    </div>
  </div>        
</div>

<div id="modal_enviomail_all" class="modal">
  <div class="modal-content">
    <div class="col s12">
      <label>Firma del ejecutivo</label>
      <select class="browser-default form-control-bmz" id="firma_m_c_all">
        <?php foreach ($perfirma->result() as $itemf) {
          echo '<option value="'.$itemf->personalId.'">'.$itemf->nombre.' '.$itemf->apellido_paterno.' '.$itemf->apellido_materno.' / '.$itemf->email.' / '.$itemf->celular.'</option>';
        }?>
      </select>
    </div>
    <div class="col s12">
      <label>Copia oculta (BBC)</label>
      <select class="browser-default form-control-bmz" id="bbc_m_c_all"><option value="0"></option>
        <?php foreach ($perfirma->result() as $itemf) {
          echo '<option value="'.$itemf->personalId.'">'.$itemf->nombre.' '.$itemf->apellido_paterno.' '.$itemf->apellido_materno.' / '.$itemf->email.' / '.$itemf->celular.'</option>';
        }?>
      </select>
    </div>
    <div class="col s12">
        <label>Correo</label>
        <div class="m_s_contac_c_all"></div>
      </div>
    <div class="row">
      <div class="col s12">
        <label>Asunto</label>
        <input class="form-control-bmz" id="ms_asucto2_c_all" value="Facturas">
      </div>
      <div class="col s12">
        <label>Comentario</label>
        <textarea class="form-control-bmz" id="ms_coment2_c_all" style="min-height: 150px;"></textarea>
      </div>
      <div class="col s12 input_envio_s_all"></div>
      <div class="col s12 table_mcr_files_s_all"></div>
    </div>
    <div class="row"><div class="col s12 iframefac_c_all"></div></div>
    <div class="row"><div class="col s12 addbtnservicio_all"></div></div>
  </div>
  <div class="row">
    <div class="col m12" align="right">
      <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
    </div>
  </div>        
</div>

<div id="modalsearchpre" class="modal" style="z-index: 1003; display: none; opacity: 0; transform: scaleX(0.7); top: 4%;">
  <div class="modal-content ">
    <h5>Búsqueda por factura, folio o monto</h5>
    <div class="row">
      <div class="col s10">
        <div class="col s6">
          <label>Ingresa parametros de búsqueda</label>
          <div class="b-input-group mb-3">
            <input type="text" class="form-control-bmz" id="searchpre" required>
            <div class="b-input-group-append">
              <button class="b-btn b-btn-primary"  type="button" onclick="searchpre()">Buscar</button>
            </div>
          </div>
        </div>
      </div>
      <div class="col s2"><a class="b-btn b-btn-primary " onclick="searchprelimpiar()" style="margin-top: 20px;" title="limpiar"><i class="fas fa-eraser"></i></a></div>
      <div class="col s12 table_result_search"></div>
    </div>
  </div>
  <div class="modal-footer">
    <button class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</button>
  </div>
</div>
