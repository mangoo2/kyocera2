var base_url = $('#base_url').val();
var table;
var idpago;
var tipopago;
var fechahoy = $('#fechahoy').val();
$(document).ready(function(){ 
    $('.modal').modal({dismissible: true});
    $('.chosen-select').chosen({width: "90%"});
    table = $('#tabla_ventas_incompletas').DataTable();
	loadtable();
    $('#cliente').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una opción',
        allowClear: true,
        ajax: {
            url: base_url+'Ventasi/searchclientes',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.id,
                        text: element.empresa

                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    });
    $("#pago_p").TouchSpin({
                min: 0,
                max: 9999999999999999999999999999999999,
                initval: 0.25,
                step: 0.01,
                decimals: 2,
            });
    $.switcher('#tipoview');
    /*
    new DG.OnOffSwitch({
        el: '#tipoview',
        textOn: 'Usuario',
        textOff: 'Todas',
        trackColorOn:'#F57C00',
        trackColorOff:'#df3039',
        listener:function(name, checked){
            setTimeout(function(){ 
                load();
            }, 500);
            
        }
    });
    */
}); 
function loadtableswitch(){
    setTimeout(function(){ 
        load();
    }, 500);
}
function loadtable(){
        load();
}
function load() {
    var cliente= $('#cliente option:selected').val()==undefined?0:$('#cliente option:selected').val();
    var estatus= $('#tipoestatus option:selected').val();
    //var tipoventa= $('#tipoventa option:selected').val();//1 venta 2 combinada 3 servicios
    var estatusventa= $('#tipoventadelete option:selected').val();
    var tipoview = $('#tipoview').is(':checked')==true?1:0;
    var idpersonal = $('#ejecutivoselect option:selected').val();
    var fechainicial_reg = $('#fechainicial_reg').val();
    
    // Destruye la tabla y la crea de nuevo con los datos cargados 
    table.destroy();
    table = $('#tabla_ventas_incompletas').DataTable({
           
        //stateSave: true,
        responsive: !0,
        "bProcessing": true,
         search: {
                return: true
            }, 
        "serverSide": true,
        "ajax": {
            "url": base_url+"index.php/Ventasd/getListaVentasIncompletasasi/",
            type: "post",
            "data": {
                'cliente':cliente,
                'status':estatus,
                'tipoventa':0,
                'estatusventa':estatusventa,
                'tipoview':tipoview,
                'idpersonal':idpersonal,
                'fechainicial_reg':fechainicial_reg
            },
        },
        "columns": [
            {"data": "id",
                render:function(data,type,row){
                    var html='';
                    if (row.combinada==1) {
                        if(row.equipo>0){
                            html=row.equipo;
                        }else if(row.consumibles>0){
                            html=row.consumibles;
                        }else if(row.refacciones>0){
                            html=row.refacciones;
                        }
                    }else{
                        html=row.id;
                    }
                    return html;
                }
            },
            {"data": "empresa"},
            {"data": "id",
                render:function(data,type,row){
                    if (row.prefactura==1) {
                        var colorbtton='green';
                    }else{
                        var colorbtton='blue';
                    }
                    var html='<a class="btn-floating '+colorbtton+' tooltipped" data-position="top" data-delay="50" data-tooltip="Prefactura" onclick="detalle('+row.id+','+row.combinada+')">\
                                <i class="material-icons">assignment</i>\
                                </a>';
                        if(row.totalcon>0){
                            var totalcon=row.totalcon;
                        }else{
                            var totalcon= 0;
                        }
                        if(row.totalref>0){
                            var totalref=row.totalref;
                        }else{
                            var totalref= 0;
                        }
                        if(row.totalequ>0){
                            var totalequ=row.totalequ;
                        }else{
                            var totalequ= 0;
                        }
                        if(row.totalacc>0){
                            var totalacc=row.totalacc;
                        }else{
                            var totalacc= 0;
                        }

                    var totalventa=parseFloat(totalcon)+parseFloat(totalref)+parseFloat(totalequ)+parseFloat(totalacc);
                    html+='<!--'+parseFloat(totalcon)+'+'+parseFloat(totalref)+'+'+parseFloat(totalequ)+'='+totalventa+' '+row.totalref+'-->';
                    //if(totalventa==0){
                    //    html+=' Renta';
                    //}
                    return html;
                }
            },
            {"data": "vendedor"},
            {"data": "estatus",
                 render:function(data,type,row){
                    if(data==1){
                        var html='Pendiente';    
                    } 
                    else if(data==2){
                        var html='';    
                    }
                    else if(data==3){
                        var html='Pagado';    
                    }  
                        return html;
                }
            },
            {"data": "reg"}, 
            {"data": null,
                 render:function(data,type,row){
                    var html='';
                    if (row.prefactura==1) {
                        if(row.vencimiento!='0000-00-00'){
                            var html=row.vencimiento;    
                        }
                        
                    }  
                        return html;

                }
            }, 
            {"data": null,
                 render:function(data,type,row){
                    var html='';
                    if (row.prefactura==1) {
                        if(row.fechaentrega!='0000-00-00'){
                            var html=row.fechaentrega;    
                        }
                        
                    }  
                        return html;

                }
            }, 
            {"data": "combinada",
                 render:function(data,type,row){
                    var html='';
                    /*
                    if(data==1){
                        var html='Venta combinada';    
                    } else{
                        var html='Venta'; 
                    }
                    */
                    if(row.totalcon>0){
                        var totalcon=row.totalcon;
                    }else{
                        var totalcon= 0;
                    }
                    if(row.totalref>0){
                        var totalref=row.totalref;
                    }else{
                        var totalref= 0;
                    }
                    if(row.totalequ>0){
                        var totalequ=row.totalequ;
                    }else{
                        var totalequ= 0;
                    }
                    if(row.totalacc>0){
                        var totalacc=row.totalacc;
                    }else{
                        var totalacc= 0;
                    }
                    if(row.totalcon2>0){
                        var totalcon2=row.totalcon2;
                    }else{
                        var totalcon2= 0;
                    }
                    var totalventa=parseFloat(totalcon)+parseFloat(totalref)+parseFloat(totalequ)+parseFloat(totalacc)+parseFloat(totalcon2);
                    if (row.combinada==1) {
                        if(row.totalpoli>0){
                            var totalpoli=row.totalpoli;
                        }else{
                            var totalpoli= 0;
                        }
                        totalventa=parseFloat(totalventa)+parseFloat(totalpoli);
                    }
                    totalventa=totalventa+(parseFloat(totalventa)*0.16);
                    html+='<!--consumibles:'+parseFloat(totalcon)+'-->\
                            <!--consumibles:'+parseFloat(totalcon2)+'-->\
                            <!--refacciones'+parseFloat(totalref)+'-->\
                            <!--equipos'+parseFloat(totalequ)+'-->\
                            <!--accesorios'+parseFloat(totalacc)+'-->\
                            <!--Total:'+totalventa+'-->';
                    html+=new Intl.NumberFormat('es-MX',{style: "currency", currency: "MXN"}).format(totalventa);
                        return html;
                }
            }, 
            //{"data": "idCotizacion"}, 
            {"data": "facturas",
                render:function(data,type,row){
                    var html='';
                    if(row.facturas!='' && row.facturas!='null' && row.facturas!=null){
                        html+=row.facturas;
                        html+=' <a class="b-btn b-btn-primary tooltipped" data-position="top" data-delay="50" data-tooltip="Desvicular Factura" onclick="desvfactura('+row.id+','+row.combinada+')"><i class="fas fa-unlink"></i></a>';
                    }
                        
                    return html;
                }
            },
            {"data": "id",
                 render:function(data,type,row){
                    var html='';
                    if(row.totalcon>0){
                            var totalcon=row.totalcon;
                        }else{
                            var totalcon= 0;
                        }
                        if(row.totalref>0){
                            var totalref=row.totalref;
                        }else{
                            var totalref= 0;
                        }
                        if(row.totalequ>0){
                            var totalequ=row.totalequ;
                        }else{
                            var totalequ= 0;
                        }
                        if(row.totalacc>0){
                            var totalacc=row.totalacc;
                        }else{
                            var totalacc= 0;
                        }
                        if(row.totalcon2>0){
                            var totalcon2=row.totalcon2;
                        }else{
                            var totalcon2= 0;
                        }
                        var totalventa=parseFloat(totalcon)+parseFloat(totalref)+parseFloat(totalequ)+parseFloat(totalacc)+parseFloat(totalcon2);
                        totalventa=(totalventa+(parseFloat(totalventa)*0.16)).toFixed(2);
                    $('.tooltipped').tooltip();
                    if(row.e_entrega==1){
                        var disabledbtn='disabled';
                    }else{
                        var disabledbtn='';
                    }
                    if(row.comentario!='' && row.comentario!='null' && row.comentario!=null){
                        var btn_com='green';
                    }else{
                        var btn_com='blue';
                    }
                    html+='<button class="btn-floating '+btn_com+' tooltipped coment_'+row.id+'_'+row.combinada+'" data-comentario="'+row.comentario+'" data-position="top" data-delay="50" data-tooltip="Comentario" onclick="comentarioventa('+row.id+',5)">\
                                    <i class="fas fa-comment fa-fw"></i>\
                                  </button>';

                    html+='<button class="btn-floating red tooltipped" data-position="top" data-delay="50" data-tooltip="Eliminar" onclick="eliminarventa('+row.id+','+row.combinada+')" '+disabledbtn+'>\
                                    <i class="fas fa-trash-alt"></i>\
                                  </button>';

                    //if(parseInt(row.estatus)==1){
                        
                        if (row.prefactura==1 && totalventa>0) {
                            if(row.estatus==3){
                                var colorbuton='green';
                            }else{
                                var colorbuton='blue';
                                //if(row.vencimiento!='0000-00-00'){
                                    if(row.vencimiento<fechahoy){
                                        var colorbuton='red';
                                    }
                                //}
                            }
                            html+='<a class="btn-floating '+colorbuton+' tooltipped modal_pagos modal_pagos_'+row.id+' verificar_'+row.id+'_'+row.combinada+'" \
                                    data-position="top" data-delay="50" \
                                    data-tooltip="Pago" \
                                    data-idventa="'+row.id+'" \
                                    data-tipoventa="'+row.combinada+'" \
                                    onclick="modal_pago('+row.id+','+row.combinada+')">\
                                        <i class="material-icons">attach_money</i>\
                                      </a><!--'+totalventa+'-->';
                        }
                        
                    //}
                    if(parseInt(row.activo)==1){
                        if (row.prefactura==1) {
                            html+='<a class="b-btn b-btn-primary tooltipped" \
                                    data-position="top" \
                                    data-delay="50" \
                                    data-tooltip="Agregar Factura" \
                                    data-tooltip-id="6df07467-d305-bd8f-2908-fc7be2299d57" onclick="addfactura('+row.id+','+row.idCliente+','+row.combinada+','+totalventa+')"> \
                                            <i class="fas fa-folder-plus"></i></a>';
                        }
                    }
                        html+='<a class="b-btn b-btn-primary tooltipped solounosusuarios" \
                                    data-position="top" \
                                    data-delay="50" \
                                    data-tooltip="Clonar Venta" \
                                    data-tooltip-id="6df07467-d305-bd8f-2908-fc7be2299d57" onclick="v_ext_copy('+row.id+','+row.combinada+')"> \
                                            <i class="fas fa-copy fa-fw"></i></a>';

                    
                    return html;
                }
            }
        ],
        "order": [[ 5, "desc" ]],
        "language": {
            "processing": "<div class='row'><div class='col s12 procesandoclass'><img src='"+base_url+"public/img/tenor.gif' width='50px;' height='50px;'>Procesando</div></div>"
        },
        createdRow:function(row,data,dataIndex){
            $(row).find('td:eq(8)')
                .addClass('obtencionfacturas venta_tipo_'+data.combinada+'_id_'+data.id)
                .attr('data-tipo',data.combinada).attr('data-idv',data.id);
        }
    }).on('draw',function(){
        $('.dataTables_filter').append('<i class="fa fa-search buttonsearch" onclick="searchtable_v()"></i>')
        vericarpagosventas();
    });

}
function searchtable_v(){
    var searchv =$("input[type=search]").val();
    table.search(searchv).draw();
}
/*
function loadservicio(){
    var cliente= $('#cliente option:selected').val()==undefined?0:$('#cliente option:selected').val();
    var tipo_servicio= $('#tipo_servicio option:selected').val();
    var estatusventa= $('#tipoventadelete option:selected').val();
    // Destruye la tabla y la crea de nuevo con los datos cargados 
    var idpersonal = $('#ejecutivoselect option:selected').val();
    var fechainicial_reg = $('#fechainicial_reg').val();
    table.destroy();
    table = $('#tabla_ventas_incompletas').DataTable({
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        search: {
                return: true
            },
        "ajax": {
            "url": base_url+"index.php/Ventas/getlistaservicios",
            type: "post",
            "data": {
                'cliente':cliente,
                'tipo_servicio':tipo_servicio,
                'activo':estatusventa,
                'idpersonal':idpersonal,
                'fechainicial_reg':fechainicial_reg
            },
        },
        "columns": [
            {"data": "asignacionId"},
            {"data": "empresa"},
            {"data": null,
                render:function(data,type,row){
                    var prefactura=0;
                    if (row.prefacturaId>0) {
                        var colorbtton='green';
                    }else{
                        var colorbtton='blue';
                    }
                    var html='<a class="btn-floating '+colorbtton+' tooltipped"\
                                data-position="top" data-delay="50" data-tooltip="Prefactura"\
                                onclick="detalleservicio('+row.asignacionId+','+row.tipo+')">\
                                <i class="material-icons">assignment</i>\
                                </a>';
                    return html;
                }
            },
            {"data": null,"visible": false},
            {"data": null,"visible": false},
             
            {"data": "reg"}, 
            {"data": null,"visible": false},
            {"data": null,
                render:function(data,type,row){
                    var html='';
                    if (row.tipo==1) {
                        html='Contrato';
                    }
                    if (row.tipo==2) {
                        html='Poliza';
                    }
                    if (row.tipo==3) {
                        html='Evento';
                    }
                    return html;
                }

            }, 
            {"data": null,"visible": false},
            {"data": null,"visible": false},
            {"data": "id",
                 render:function(data,type,row){
                    $('.tooltipped').tooltip();
                    var html='';
                    if (row.activo==1) {
                        if (row.prefacturaId>0) {
                            html+='<a class="btn-floating blue tooltipped" data-position="top" data-delay="50" data-tooltip="CFDI" onclick="modal_cdfimser('+row.asignacionId+','+row.tipo+')">\
                                            CFDI\
                                          </a>';
                            html+='<a class="btn-floating green tooltipped" data-position="top" data-delay="50" data-tooltip="Pago" onclick="modal_pagoser('+row.asignacionId+','+row.tipo+')">\
                                            <i class="material-icons">attach_money</i>\
                                          </a>';
                        }else{
                            html+='<a class="btn-floating red tooltipped" data-position="top" data-delay="50" data-tooltip="Eliminar" onclick="eliminarser('+row.asignacionId+','+row.tipo+')">\
                                        <i class="fas fa-trash-alt"></i>\
                                      </a>';
                        }
                    }else{
                        //motivo de la eliminacion
                        html+='<a class="btn-floating red  eliminadoservicio_'+row.asignacionId+'_'+row.tipo+'" data-motivo="'+row.motivoeliminado+'" onclick="mensajedelete('+row.asignacionId+','+row.tipo+')">\
                                        <i class="material-icons">description</i>\
                                      </a>';
                    }
                    return html;
                }
            }
        ],
        "order": [[ 0, "desc" ]],
        "language": {
            "processing": "<div class='row'><div class='col s12 procesandoclass'><img src='"+base_url+"public/img/tenor.gif' width='50px;' height='50px;'>Procesando</div></div>"
        },
    }).on('draw',function(){
        $('.dataTables_filter').append('<i class="fa fa-search buttonsearch" onclick="searchtable_v()"></i>')

    });
}
*/
function obtencionfacturas(tipo,id){
    $.ajax({
      type:'POST',
      url: base_url+'index.php/Ventas/obtencionfacturas',
      data: {
        tipo:tipo,
        id:id
        },
      success:function(data){
          $('.venta_tipo_'+tipo+'_id_'+id).html(data);
      }
  });
}
function detalle(id,tipo){
    //if (tipo==0) {
        window.open(base_url+"Prefactura/viewd/"+id, "Prefactura", "width=780, height=612");
    //}else{
      //  window.open(base_url+"Prefactura/viewc/"+id, "Prefactura", "width=780, height=612");
    //}
}
/*
function detalleservicio(id,tipo){
    window.open(base_url+"Prefactura/viewser/"+id+"/"+tipo, "Prefactura", "width=780, height=612");    
}
*/
function finaliza(id,tipo){
    $('#id_venta').val(id);
    $('#tipoventa').val(tipo);
    $('#modalFinaliza').modal('open');
}
function finalizar_factura(){
    var id_v = $('#id_venta').val(); 
    var tipo = $('#tipoventa').val(); 
    // tipo 0 es la venta combinada
    if(tipo==0) {
        $.ajax({
          type: "POST",
          url: base_url+"index.php/Ventas/finalizar_factura",
          data:{id:id_v},
          success: function(data){
            $.alert({
                boxWidth: '30%',
                useBootstrap: false,
                title: 'Atención!',
                content: 'Finalizado correctamente'}
            );
            table.ajax.reload(); 
          }
        });
    }else{
        $.ajax({
          type: "POST",
          url: base_url+"index.php/Ventas/finalizar_facturac",
          data:{id:id_v},
          success: function(data){
            $.alert({
                boxWidth: '30%',
                useBootstrap: false,
                title: 'Atención!',
                content: 'Finalizado correctamente'}
            );
            table.ajax.reload(); 
          }
        });
    }
}
//////////////////////
function fecha_vencimiento(id_v){
    var result='';
    $.ajax({
      type: "POST",
      url: base_url+"index.php/Ventas/verificar_fecha_vencimiento",
      data:{id:id_v},
      async:false,
      success: function(data){
        result=data;
      }
    });   
    return result;
}
function modal_cdfi(id,tipo) {
    $('#id_venta_e').val(id);
    $('#tipo_e').val(tipo);
    $('#modal_cfdi').modal('open');
}
function update_cfdi(){
    var id_venta=$('#id_venta_e').val();
    var tipo=$('#tipo_e').val();
    var cfdi=$('#cdfi_e').val();
    $.ajax({
      type: "POST",
      url: base_url+"index.php/Ventas/update_cfdi_registro",
      data:{id_venta:id_venta,tipo:tipo,cfdi:cfdi},
      success: function(data){
         $.alert({
            boxWidth: '30%',
            useBootstrap: false,
            title: 'Atención!',
            content: 'Guardado correctamente'}
        );
      }
    }); 
    var tipo=$('#tipo_e').val('');
    var cfdi=$('#cdfi_e').val('');  
}
function modal_pago(id,tipo){
    $('#id_venta_p').val(id);
    $('#tipo_p').val(tipo);
    $('#fecha_p').val('');
    $('#pago_p').val('');
    $('#observacion_p').val('');
    $('#modal_pago').modal('open');
    if(tipo==1){//Combinada 
       $('.monto_prefactura').html('$ <span class="totalpoliza">'+monto_combinada(id)+'</span>');
    }else{// ventas
       $('.monto_prefactura').html('$ <span class="totalpoliza">'+monto_ventas_vd(id)+'</span>');
    }
    table_tipo_compra(id,tipo);
    setTimeout(function(){ 
        calcularrestante();
    }, 1000);
}
function calcularrestante(){
    var totalpoliza=parseFloat($('.totalpoliza').html());
    var pagos = 0;
    $(".montoagregado").each(function() {
        var vstotal = parseFloat($(this).html());
        pagos += Number(vstotal);
    });
    pagos=pagos.toFixed(2);
    var restante = parseFloat(totalpoliza)-parseFloat(pagos);
    if(restante<=0){
        restante=0;
        cambiarestatuspago(totalpoliza);
    }
    $('.restante_prefactura').html('$ <span class="restantepoliza">'+restante.toFixed(2)+'</span>');
}
function monto_ventas_vd(id_v){//ventas monto de prefactura
    var result='';
    $.ajax({
      type: "POST",
      url: base_url+"index.php/Ventasd/montoventas",
      data:{id:id_v},
      async:false,
      success: function(data){
        result=data;
      }
    });   
    return result;
}
function monto_combinada(id_v){//Combinada monto de prefactura
    var result='';
    $.ajax({
      type: "POST",
      url: base_url+"index.php/Ventas/montocombinada",
      data:{id:id_v},
      async:false,
      success: function(data){
        result=data;
      }
    });   
    return result;
}
function guardar_pago_compras(){
    var form_pago = $('#form_pago');
    form_pago.validate({
            rules: 
            { 
                fecha: {
                    required: true
                },
                idmetodo: {
                    required: true
                },
                pago: {
                    required: true
                }
            },
            // Para mensajes personalizados
            messages: 
            {   
                fecha:{
                    required: "Campo obligatirio"
                },
                idmetodo:{
                    required: "Campo obligatirio"
                },
                pago:{
                    required: "Campo obligatirio"
                }
            },
        errorElement : 'div'
    });
    var valid = $("#form_pago").valid();
    if(valid){
        var restante =parseFloat($('.restantepoliza').html());
        var pago_p =parseFloat($('#pago_p').val());
        if (pago_p>0 && pago_p<=restante) {
            var datos = $('#form_pago').serialize();
            $.ajax({
              type: "POST",
              url: base_url+"index.php/Ventasd/guardar_pago_tipo",
              data:datos,
              success: function(data){
                 $.alert({
                    boxWidth: '30%',
                    useBootstrap: false,
                    title: 'Atención!',
                    content: 'Guardado correctamente'}
                );
                $('#fecha_p').val('');
                $('#pago_p').val('');
                $('#observacion_p').val('');
                table_tipo_compra($('#id_venta_p').val(),$('#tipo_p').val());
                setTimeout(function(){ 
                    calcularrestante();
                }, 1000);
              }
            });
        }else{
            $.alert({
                    boxWidth: '30%',
                    useBootstrap: false,
                    title: 'Atención!',
                    content: 'Monto mayor a 0 y menor o igual al restante'}
                );
        }
    }   
}
function table_tipo_compra(id,tipo){
    $('.text_tabla_pago').html('');
    $.ajax({
      type: "POST",
      url: base_url+"index.php/Ventasd/table_get_tipo_compra",
      data:{id:id,tip:tipo},
      success: function(data){
        $('.text_tabla_pago').html(data);
      }
    });
}
function deletepagov(id,tipo){
    $('#modal_eliminarpago').modal('open');
    idpago=id;
    tipopago=tipo;
}
function eliminarpago(){
    $.ajax({
        type: "POST",
        url: base_url+"index.php/Ventasd/vericar_pass",
        data:{
            pago:idpago,
            tipo:tipopago,
            pass:$('#ver_pass').val()
        },
        success: function(data){
            $('#ver_pass').val('');
            $('#modal_eliminarpago').modal('close');
            $('#modal_pago').modal('close');
            if(parseFloat(data)==1){
                $.alert({
                    boxWidth: '30%',
                    useBootstrap: false,
                    title: 'Atención!',
                    content: 'Pago Eliminado correctamente'}
                );
            }else{
                swal({ title: "Error",
                    text: "Verificar la contraseña",
                    type: 'error',
                    showCancelButton: false,
                    allowOutsideClick: false,
                });
            }
        }
    });
}
function  eliminarventa(id,tipo) {
    $('#motivoeliminacion').val('');
    $('#deleteventa').val(id);
    $('#deletetipo').val(tipo);
    $('#modaleliminar').modal('open');
}
function  ventadelete() {
    var motivo=$('#motivoeliminacion').val();
    $.ajax({
        type: "POST",
        url: base_url+"index.php/Ventasd/ventadelete",
        data:{
            ventaid:$('#deleteventa').val(),
            ventatipo:$('#deletetipo').val(),
            motivo:$('#motivoeliminacion').val()
        },
        success: function(data){
            var status= parseFloat(data);
            if (status==1) {
                $.alert({
                    boxWidth: '30%',
                    useBootstrap: false,
                    title: 'Atención!',
                    content: 'Eliminado correctamente'}
                );
                $('#modaleliminar').modal('close');
                loadtable(); 
            }else{
                $.alert({
                    boxWidth: '30%',
                    useBootstrap: false,
                    title: 'Atención!',
                    content: 'No es posible eliminar, Venta facturada'}
                );
            }
        }
    });
}
function  eliminarser(id,tipo) {
    $('#motivoeliminacionservicio').val('');
    $('#deleteservicio').val(id);
    $('#deleteserviciotipo').val(tipo);
    $('#modaleliminarservicio').modal('open');
}
function  serviciodelete() {
    var motivo=$('#motivoeliminacionservicio').val();
    $.ajax({
        type: "POST",
        url: base_url+"index.php/Ventas/serviciodelete",
        data:{
            servicioid:$('#deleteservicio').val(),
            serviciotipo:$('#deleteserviciotipo').val(),
            motivo:$('#motivoeliminacionservicio').val()
        },
        success: function(data){
            loadtable();
            $('#modaleliminarservicio').modal('close');
        }
    });
}
function mensajedelete(id,tipo){
    var motivo=$('.eliminadoservicio_'+id+'_'+tipo).data('motivo');
    swal(motivo);
}
function modal_cdfimser(id,tipo) {
    $('#cdfi_ser').val('');
    $('#id_cfdi_servicio').val(id);
    $('#tipo_cfdi_servicio').val(tipo);
    $('#modal_cfdi_ser').modal('open');
}
function update_cfdi_ser(){
    var id=$('#id_cfdi_servicio').val();
    var tipo=$('#tipo_cfdi_servicio').val();
    var cfdi=$('#cdfi_ser').val();
    $.ajax({
      type: "POST",
      url: base_url+"index.php/Ventas/update_cfdi_servicio",
      data:{id:id,tipo:tipo,cfdi:cfdi},
      success: function(data){
         $.alert({
            boxWidth: '30%',
            useBootstrap: false,
            title: 'Atención!',
            content: 'Guardado correctamente'}
        );
      }
    }); 
      
}
//===========================================
function modal_pagoser(id,tipo){
    $('#idservicio').val(id);
    $('#tipos').val(tipo);
    $('#fecha_p_s').val('');
    $('#pago_p_s').val('');
    $('#observacion_p').val('');
    $('#modal_pago_s').modal('open');
    if(tipo==1){//Combinada 
       $('.monto_prefactura_s').html('$ <span class="totalpolizas">'+monto_servicios(id,1)+'</span>');
    }else if(tipo==2){// ventas
       $('.monto_prefactura_s').html('$ <span class="totalpolizas">'+monto_servicios(id,2)+'</span>');
    }else if(tipo==3){// ventas
       $('.monto_prefactura_s').html('$ <span class="totalpolizas">'+monto_servicios(id,3)+'</span>');
    }
    table_tipo_servicio(id,tipo);
    setTimeout(function(){ 
        calcularrestantes();
    }, 1000);
}
function guardar_pago_servicios(){
    var form_pago = $('#form_pago_servicio');
    form_pago.validate({
            rules: 
            { 
                fecha: {
                    required: true
                },
                idmetodo: {
                    required: true
                },
                pago: {
                    required: true
                }
            },
            // Para mensajes personalizados
            messages: 
            {   
                fecha:{
                    required: "Campo obligatirio"
                },
                idmetodo:{
                    required: "Campo obligatirio"
                },
                pago:{
                    required: "Campo obligatirio"
                }
            },
        errorElement : 'div'
    });
    var valid = $("#form_pago_servicio").valid();
    if(valid){
        var restante =parseFloat($('.totalpolizas').html());
        var pago_p =parseFloat($('#pago_p_s').val());
        if (pago_p>0 && pago_p<=restante) {
            var datos = $('#form_pago_servicio').serialize();
            $.ajax({
              type: "POST",
              url: base_url+"index.php/Ventas/guardar_pago_tipo_servicio",
              data:datos,
              success: function(data){
                 $.alert({
                    boxWidth: '30%',
                    useBootstrap: false,
                    title: 'Atención!',
                    content: 'Guardado correctamente'}
                );
                $('#fecha_p').val('');
                $('#pago_p').val('');
                $('#observacion_p').val('');
                table_tipo_servicio($('#idservicio').val(),$('#tipos').val());
                setTimeout(function(){ 
                    calcularrestante();
                }, 1000);
              }
            });
        }else{
            $.alert({
                    boxWidth: '30%',
                    useBootstrap: false,
                    title: 'Atención!',
                    content: 'Monto mayor a 0 y menor o igual al restante'}
                );
        }
    }   
}
function table_tipo_servicio(id,tipo){
    $('.text_tabla_pago').html('');
    $.ajax({
      type: "POST",
      url: base_url+"index.php/Ventas/table_get_pagos_servicios",
      data:{id:id,tip:tipo},
      success: function(data){
        $('.text_tabla_pago_servicios').html(data);
      }
    });
}
function calcularrestantes(){
    var totalpoliza=parseFloat($('.totalpolizas').html());
    var pagos = 0;
    $(".montoagregados").each(function() {
        var vstotal = parseFloat($(this).html());
        pagos += Number(vstotal);
    });
    var restante = parseFloat(totalpoliza)-parseFloat(pagos);
    if(restante<0){
        restante=0;
    }
    $('.restante_prefactura_s').html('$ <span class="restantepoliza">'+restante.toFixed(2)+'</span>');
}
function monto_servicios(id_v,tipos){//Combinada monto de prefactura
    var result='';
    $.ajax({
      type: "POST",
      url: base_url+"index.php/Ventas/montoservicio",
      data:{
        id:id_v,
        tipo:tipos
        },
      async:false,
      success: function(data){
        result=data;
      }
    });   
    return result;
}
function deletepagoser(id){
    $.ajax({
      type: "POST",
      url: base_url+"index.php/Ventas/deletepagoser",
      data:{
        id:id
        },
      async:false,
      success: function(data){
        $('.remove_table_pagos_ser_'+id).remove();
        calcularrestantes();
      }
    }); 
}
function confirmarpagov(id,tipo){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: 'Se necesita permisos de administrador<br>'+
                 '<input type="password" placeholder="Contraseña" id="contrasena" class="name form-control" required/>',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var pass=$('#contrasena').val();
                if (pass!='') {
                    $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Sistema/solicitarpermiso",
                        data: {
                            pass:pass
                        },
                        success: function (response){
                                var respuesta = parseInt(response);
                                if (respuesta==1) {
                    //=============================================================================
                        confirmarpagov2(id,tipo);
                    //=============================================================================
                                }else{
                                    $.alert({
                                    boxWidth: '30%',
                                    useBootstrap: false,
                                    title: 'Advertencia!',
                                    content: 'No tiene permisos'}); 
                                }
                        },
                        error: function(response){
                            $.alert({
                                    boxWidth: '30%',
                                    useBootstrap: false,
                                    title: 'Error!',
                                    content: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema'});   
                             
                        }
                    });

                }else{
                    $.alert({
                                    boxWidth: '30%',
                                    useBootstrap: false,
                                    title: 'Advertencia!',
                                    content: 'Ingrese una contraseña'}); 
                }
                
            },
            cancelar: function () 
            {
                
            }
        }
    });
}
function confirmarpagov2(id,tipo){
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Generales/confirmarpagos",
        data: {
            id:id,
            tipo:5
        },
        success: function (response){
            $('#modal_pago').modal('close');
        },
        error: function(response){
            $.alert({
                    boxWidth: '30%',
                    useBootstrap: false,
                    title: 'Error!',
                    content: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema'});   
             
        }
    });
}
function desvfactura(id,tipo){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: 'para desvicular las facturas Se necesita permisos de administrador<br>'+
                 '<input type="password" placeholder="Contraseña" id="contrasena" class="name form-control" required/>',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var pass=$('#contrasena').val();
                if (pass!='') {
                    $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Sistema/solicitarpermiso",
                        data: {
                            pass:pass
                        },
                        success: function (response){
                                var respuesta = parseInt(response);
                                if (respuesta==1) {
                    //=============================================================================
                        desvfactura2(id,tipo);
                    //=============================================================================
                                }else{
                                    toastr["warning"]("No tiene permisos", "Advertencia");
                                }
                        },
                        error: function(response){
                            toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema"); 
                             
                        }
                    });

                }else{
                    toastr["warning"]("Ingrese una contraseña", "Advertencia");
                }
                
            },
            cancelar: function () 
            {
                
            }
        }
    });
}
function desvfactura2(id,tipo){
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Generales/desvfactura",
        data: {
            id:id,
            tipo:tipo
        },
        success: function (response){
                toastr["success"]("Facturas desvinculadas");
                loadtable();
        },
        error: function(response){
            toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema"); 
             
        }
    });
}
function vericarpagosventas(){
    var DATAf  = [];
    $(".modal_pagos").each(function() {
        item = {};
        item ["idventa"] = $(this).data('idventa');
        item ["tipoventa"] = $(this).data('tipoventa');
        DATAf.push(item);
    });
    aInfo   = JSON.stringify(DATAf);
    //console.log(aInfo);
    var datos=aInfo;
    
    $.ajax({
        type:'POST',
        url: base_url+"Generales/vericarpagosventas",
        data: {
            datos:aInfo
        },
        success: function (data){
            //console.log(data);
            var array = $.parseJSON(data);
            console.log(array);
            $.each(array, function(index, item) {
                    $(".verificar_"+item.idventa+"_"+item.tipo).addClass( "yellowc" );
                
            });
        }
    });
}
function v_ext_copy(id,tipo){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Desea realizar un clonado de la venta seleccionada?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Ventasd/v_ext",
                    data: {
                        id:id,
                        tipo:tipo
                    },
                    success: function (data){
                        console.log(data);
                        var array = $.parseJSON(data);
                        console.log(array.equipos.length);
                        if(array.equipos.length==0){
                            toastr["success"]("Venta Clonada");
                            loadtable();
                        }else{
                            var html='Verifique Existencia de los siguientes productos';
                                html+='<table class="table"><thead><tr><th>Modelo</th><th>Bodega</th></tr></thead><tbody>';
                            $.each(array.equipos, function(index, item) {
                                 html+='<tr><th>'+item.modelo+'</th><th>'+item.bodega+'</th></tr>';
                                
                            });
                                html+='</tbody></table>';
                            $.alert({
                                boxWidth: '30%',
                                useBootstrap: false,
                                title: 'Atención!',
                                content: html}
                            );
                        }
                        /*
                        var array = $.parseJSON(data);
                        console.log(array);
                        $.each(array, function(index, item) {
                                $(".verificar_"+item.idventa+"_"+item.tipo).addClass( "yellowc" );
                            
                        });
                        */
                    }
                });
        },
            cancelar: function () 
            {
                
            }
        }
    });
}
function comentarioventa(id,tipo){
    var co=$('.coment_'+id+'_'+tipo).data('comentario')==null?'':$('.coment_'+id+'_'+tipo).data('comentario');
    if(co=='null'){
        co='';
    }
    $.confirm({
        boxWidth: '40%',
        useBootstrap: false,
        icon: 'fa fa-success',
        title: 'Comentario',
        content: '<textarea id="comentarioventa" class="form-control-bmz">'+co+'</textarea>',
        type: 'green',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var comt=$('#comentarioventa').val();
                $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Generales/agregarcomentario",
                    data: {
                        id:id,
                        tipo:tipo,
                        com:comt
                    },
                    success: function (response){
                        toastr["success"]("Comentario agregado");    
                    },
                    error: function(response){
                        toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema"); 
                         
                    }
                }); 
            },
            cancelar: function (){
                
            }
        }
    });
}
