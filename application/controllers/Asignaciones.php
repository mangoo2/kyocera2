<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Asignaciones extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Configuraciones_model');
        $this->load->model('ModeloCatalogos');
        $this->load->model('Clientes_model');
        $this->load->model('Rentas_model');
        $this->load->model('Polizas_model');
        $this->load->model('Configuraciones_model');
        $this->load->model('ConfiguracionDocumentos_Model');
        $this->load->model('Estados_model', 'estados');
        $this->load->model('Modelonotify');
        $this->fechaactual=date('Y-m-d');
        $this->submenu=34;
        $this->idpersonal = $this->session->userdata('idpersonal');
        if($this->session->userdata('logeado')==true){
        }else{
            redirect('login');
        }
    }

    // Listado de Asignaciones
    function index(){    
        $data['MenusubId']=$this->submenu;
        //$data['resultstec']=$this->Configuraciones_model->searchTecnico();
        $data['resultstec']=$this->Configuraciones_model->view_session_searchTecnico();   
        //$data['resultzonas']=$this->ModeloCatalogos->db10_getselectwheren('rutas',array('status'=>1));
        $data['resultzonas']=$this->ModeloCatalogos->view_session_rutas();
        //$data['resultpolizas']=$this->ModeloCatalogos->getselectwheren('servicio_evento',array('status'=>1)); 
        $data['resultpolizas']=$this->ModeloCatalogos->db10_getselectwheren('polizas',array('estatus'=>1));   

        $data['consumiblesresult']=$this->ModeloCatalogos->db10_getselectwheren('consumibles',array('status'=>1)); 
        $data['refaccionesresult']=$this->ModeloCatalogos->db10_getselectwheren('refacciones',array('status'=>1));
        $data['accesoriosresult']=$this->ModeloCatalogos->db10_getselectwheren('catalogo_accesorios',array('status'=>1));  
        
        $data['serviciosdeventas']=$this->Configuraciones_model->serviciosdeventasvvv();
        $data['serviciosdepolizas']=$this->Configuraciones_model->serviciosdeventaspolizasvvv(); 

        $data['contrsinservicios']=$this->ModeloCatalogos->contratossinservicios($this->fechaactual);
        if(isset($_GET['idpolizaser'])){
            $data['idpolizaser']=$_GET['idpolizaser'];
        }else{
            $data['idpolizaser']=0;
        }
        if(isset($_GET['tipopc'])){
            $data['tipopc']=$_GET['tipopc'];
        }else{
            $data['tipopc']=0;
        }
        $this->load->view('header');
        $this->load->view('main');
        $this->load->view('Servicio/listado_asignaciones',$data);
        $this->load->view('footer');
        $this->load->view('Servicio/listado_asignaciones_js');
    }

    function datoscontrato(){
        $id=$this->input->post('id');
        $contrato = $this->Rentas_model->contratoc($id); 
        $consumibles = $this->Rentas_model->contratocconsumibles($id); 
        $arraydatos = array(
                            'contrato'=>$contrato[0],
                            'consumibles'=>$consumibles->result()
                            );
        echo json_encode($arraydatos);
    }
    function datoscliente(){
        $id=$this->input->post('id');
        $datoscliente = $this->ModeloCatalogos->db2_getselectwheren('clientes',array('id'=>$id));
        $datoscliente=$datoscliente->result();

        $datosclientedireccion = $this->ModeloCatalogos->db2_getselectwheren('clientes_direccion',array('idcliente'=>$id,'status'=>1));
        $datosclientedatosfiscales = $this->ModeloCatalogos->db2_getselectwheren('cliente_has_datos_fiscales',array('idCliente'=>$id,'activo'=>1));

        $datosclientecontacto = $this->ModeloCatalogos->db2_getselectwheren('cliente_datoscontacto',array('clienteId'=>$id,'activo'=>1));

        

        $array =array(
                'datos'=>$datoscliente[0],
                'direccion'=>$datosclientedireccion->result(),
                'datosfiscales'=>$datosclientedatosfiscales->result(),
                'contactos'=>$datosclientecontacto->result(),
                );
        echo json_encode($array);
    }
    public function datospoliza(){
        $id=$this->input->post('id');
        $contrato = $this->Polizas_model->datospolizap($id); 
        //=========================================================
            $sql_num_ser_add="SELECT * FROM `polizascreadas_ext_ser_add` WHERE idpoliza='$id' AND activo=1";
            $query_num_ser_add = $this->db->query($sql_num_ser_add);
        //==========================================================
        $count_row_servicios=0;
        $count_row_servicios2=0;
        /*
        if($contrato[0]->viewcont==1){
            $row_servicios=$this->Configuraciones_model->getserviospolizarealizados($id);
            //log_message('error',json_encode($row_servicios));
            if($row_servicios->num_rows()>0){
                $count_row_servicios=$row_servicios->num_rows();
            }else{
                $count_row_servicios=0;
            }
            $n_ser=0;
        }else{
            $n_ser=0;
        }
        */
        $vcont_ser = $this->obtenerultimosservicios($contrato[0]->idCliente,3);
        $rows_ser_eve = $this->Polizas_model->servicioseventocliente($contrato[0]->idCliente,$id);
        $count_row_servicios=0;
        foreach ($rows_ser_eve->result() as $itemsev1) {

              if($itemsev1->status==2){
                $count_row_servicios=$count_row_servicios+1;
              }elseif($itemsev1->status<2){
                //$servicios_f++;
              }
          }
        $rows_ser_pol = $this->Polizas_model->serviciospolizacliente($contrato[0]->idCliente,$id,$id);
        foreach ($rows_ser_pol->result() as $itemsev) {
              if($itemsev->status==2){
                $count_row_servicios2=$count_row_servicios2+1;
              }elseif($itemsev->status<2){
                //$servicios_f++;
              }
        }
        //======================================
        
            $res_vc = $this->ModeloCatalogos->getselectwheren('ventacombinada',array('poliza'=>$contrato[0]->id,'activo'=>1));
            $id_com=0;
            foreach ($res_vc->result() as $itemvc) {
                $id_com = $itemvc->combinadaId;
            }
        //======================================
        $array = array(
                    'pol'=>$contrato[0],
                    'n_ser'=>$count_row_servicios,
                    'n_ser_ex'=>$count_row_servicios2,
                    'vcont_ser'=>$vcont_ser,
                    'cli_pol'=>$contrato[0]->idCliente,
                    'id_com'=>$id_com
                        );
        echo json_encode($array);
    }
    function searchRutas(){
        $cli = $this->input->get('search');
        $results=$this->Configuraciones_model->searchRutas();
        echo json_encode($results->result());
    }

    function searchTecnicos(){
        $cli = $this->input->get('search');
        $results=$this->Configuraciones_model->searchTecnico();
        echo json_encode($results->result());
    }

    public function getAsignaciones() {
        $params = $this->input->post();
        $serie = $this->Configuraciones_model->getAsignaServ($params);
        $totalRecords= $this->Configuraciones_model->TotalAsignaServ($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totalRecords),  
            "recordsFiltered" => intval($totalRecords),
            "data"            => $serie->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }

    function get_DatosAsigna(){
        $tasign = $this->input->post('tasign');
        $tipo = $this->input->post('tipo');
        $idrow = $this->input->post('id');
        if ($tasign==1) {
            if ($tipo==1) {
                $table='asignacion_ser_contrato';
                $idname='contratoId';
            }else{
                $table='asignacion_ser_poliza';
                $idname='polizaId';
            }
        }else{
            if ($tipo==1) {
                $table='asignacion_ser_contrato_t';
                $idname='contratoId';
            }else{
                $table='asignacion_ser_poliza_t';
                $idname='polizaId';
            }
        }

        $array = $this->ModeloCatalogos->getselectwheren($table,array($idname=>$idrow));
        if($array->num_rows()>0){
            $array=$array->result();
        }else{
            $array=[];
        }
        echo json_encode($array);
    }

    public function view($id=0){
        $data['ventaId'] = $id;
        $data['dia_reg']=date('d');
        $data['mes_reg']=date('m');
        $data['ano_reg']=date('y');
        $data['proinven']=''; // es el numero de folio de la factura aparecera cuando se guarden los campos restantes o se finalise la edicion
        $data['cfd']='';
        $data['vence']='';// sera la fecha de vencimiento sera remplazada una ves guardad
        $data['vence_block']='';//el campo de vencimiento sera bloqueado una ves guardado
        $data['formapago']=0;
        $data['formapago_block']='';
        $data['cfdi']=0;
        $data['cfdi_block']='';
        $data['formacobro']='';
        $data['formacobro_block']='';
        $data['estado']=0;
        $data['cargo']='';
        $data['cargo_block']='';
        $data['metodopagoId']=0;
        $data['metodopagoId_block']='';

        $data['telId']=0;
        $data['telId_block']='';        
        $data['contactoId']=0;
        $data['contactoId_block']='';            
        $data['cargo']=0;
        $data['cargo_block']='';
        $data['block_button']=0;
        $data['observaciones']='';
        $data['observaciones_block']='';
        $data['rfc_id']=0;
        $data['rfc_id_button']='';
        //=================================================================
        $where_ventas=array('id'=>$id);
        $resultadov = $this->ModeloCatalogos->getselectwheren('polizasCreadas',$where_ventas);
        $id_personal=0;
        foreach ($resultadov->result() as $item) {
            $id_personal=$item->id_personal;
            $idCliente=$item->idCliente;
        }
        $data['rfc_datos']=$this->Clientes_model->getListadoDatosFiscales($idCliente); 
        //==========================================================================
        $where_personal=array('personalId'=>$id_personal);
        $resultadop = $this->ModeloCatalogos->getselectwheren('personal',$where_personal);
        $personal_nombre='';
        $personal_apellidop='';
        $personal_apellidom='';
        foreach ($resultadop->result() as $item) {
            $personal_nombre= $item->nombre;
            $personal_apellidop= $item->apellido_paterno;
            $personal_apellidom= $item->apellido_materno;
        }
        $data['ini_personal']=substr($personal_nombre, 0, 1).substr($personal_apellidop, 0, 1).substr($personal_apellidom, 0, 1);
        //============================================
            $resultadocli = $this->ModeloCatalogos->db6_getselectwheren('clientes',array('id'=>$idCliente));
            foreach ($resultadocli->result() as $item) {
                $data['empresa']=$item->empresa;
                $data['razon_social']=$item->razon_social;
                $data['rfc']=$item->rfc;
                $data['calle']=$item->calle;
                $data['num_ext']=$item->num_ext;
                $data['colonia']=$item->colonia;
                $data['estado']=$item->estado;
                $data['cp']=$item->cp;
                $data['municipio']=$item->municipio;
                $data['email']=$item->email;
            }
            $resultadoclidf = $this->ModeloCatalogos->db6_getselectwheren('cliente_has_datos_fiscales',array('idCliente'=>$idCliente));
            foreach ($resultadoclidf->result() as $item) {
                $data['razon_social']=$item->razon_social;
                $data['rfc']=$item->rfc;
                $data['calle']=$item->calle;
                $data['num_ext']=$item->num_ext;
                $data['colonia']=$item->colonia;
                $data['cp']=$item->cp;
            }


            $data['resultadoclitel'] = $this->ModeloCatalogos->db6_getselectwheren('cliente_has_telefono',array('idCliente'=>$idCliente));
            $data['resultadoclipcontacto'] = $this->ModeloCatalogos->db6_getselectwheren('cliente_has_persona_contacto',array('idCliente'=>$idCliente));

            $data['polizaventadetalles'] = $this->ModeloCatalogos->polizaventadetalles2($id);
        //===========================================
            $data['metodopagorow']=$this->ModeloCatalogos->db6_getselectwheren('f_metodopago',array('activo'=>1));
            $data['formapagorow']=$this->ModeloCatalogos->db6_getselectwheren('f_formapago',array('activo'=>1));
            $data['cfdirow']=$this->ModeloCatalogos->db6_getselectwheren('f_uso_cfdi',array('activo'=>1));
            $data['estadorow']=$this->ModeloCatalogos->db6_getselectwheren('estado',array('activo'=>1));
            $data['configuracionCotizacion'] = $this->ConfiguracionDocumentos_Model->getConfiguracion(1);
        //============================================================================================
        $resultprefactura = $this->ModeloCatalogos->db6_getselectwheren('prefactura',array('ventaId'=>$id,'tipo'=>1));
        foreach ($resultprefactura->result() as $item) {
            $data['proinven']=$item->prefacturaId;
            $data['vence']=$item->vencimiento;
            $data['vence_block']='disabled';
            $data['metodopagoId']=$item->metodopagoId;
            $data['metodopagoId_block']='disabled';
            $data['formapago']=$item->formapagoId;
            $data['formapago_block']='disabled';
            $data['cfdi']=$item->usocfdiId;
            $data['cfdi_block']='disabled';
            $data['formacobro']=$item->formadecobro;
            $data['formacobro_block']='disabled';

        
            $data['telId']=$item->telId;
            $data['telId_block']='disabled';
        
            $data['contactoId']=$item->contactoId;
            $data['contactoId_block']='disabled';
            
            $data['cargo']=$item->cargo;
            $data['cargo_block']='disabled';
            $data['block_button']=1;
            $fechareg=$item->reg;
            $data['dia_reg']=date('d',strtotime($fechareg));
            $data['mes_reg']=date('m',strtotime($fechareg));
            $data['ano_reg']=date('y',strtotime($fechareg));
            $data['observaciones']=$item->observaciones;
            $data['observaciones_block']='disabled'; 
            $data['rfc_id']=$item->rfc_id;
            $data['rfc_id_button']='disabled';
        }
        //============================================================================================
        $this->load->view('Servicio/prefactura',$data);
    }
    function polizas(){
        $id = $this->input->post('id'); 
        $result=$this->Polizas_model->getlistadopolizasc($id);
        echo '<option value="0"></option>';
        foreach ($result as $item) {
            echo '<option value="'.$item->id.'">'.$item->id.'</option>';
        }
    }
    function get_savefecha(){
        $tasign=$this->input->post('tasign');
        $tipo=$this->input->post('tipo');
        $idconpol=$this->input->post('idconpol');
        $serviciorow=$this->input->post('serviciorow');
        $fecha=$this->input->post('fecha');
        $hora=$this->input->post('hora');
        $horafin=$this->input->post('horafin');
        $hora_comida=$this->input->post('hora_comida');
        $horafin_comida=$this->input->post('horafin_comida');

        $hora = $this->ModeloCatalogos->verificarhora($hora);
        $horafin = $this->ModeloCatalogos->verificarhora($horafin);

        if ($serviciorow>0) {
            if ($tasign==1) {
                if ($tipo==1) {
                    $this->ModeloCatalogos->updateCatalogo('asignacion_ser_contrato',array('fecha'=>$fecha,'hora'=>$hora,'horafin'=>$horafin,'hora_comida'=>$hora_comida,'horafin_comida'=>$horafin_comida),array('asignacionId'=>$serviciorow));
                }else{
                    $this->ModeloCatalogos->updateCatalogo('asignacion_ser_poliza',array('fecha'=>$fecha,'hora'=>$hora,'horafin'=>$horafin,'hora_comida'=>$hora_comida,'horafin_comida'=>$horafin_comida),array('asignacionId'=>$serviciorow));
                }
            }else{
                if ($tipo==1) {
                    $this->ModeloCatalogos->updateCatalogo('asignacion_ser_contrato_t',array('fecha'=>$fecha,'hora'=>$hora,'horafin'=>$horafin,'hora_comida'=>$hora_comida,'horafin_comida'=>$horafin_comida),array('asignacionId'=>$serviciorow));
                }else{
                    $this->ModeloCatalogos->updateCatalogo('asignacion_ser_poliza_t',array('fecha'=>$fecha,'hora'=>$hora,'horafin'=>$horafin,'hora_comida'=>$hora_comida,'horafin_comida'=>$horafin_comida),array('asignacionId'=>$serviciorow));
                }
            }
        }else{
            if ($tasign==1) {
                if ($tipo==1) {
                    $this->ModeloCatalogos->Insert('asignacion_ser_contrato',array('fecha'=>$fecha,'hora'=>$hora,'horafin'=>$horafin,'hora_comida'=>$hora_comida,'horafin_comida'=>$horafin_comida,'contratoId'=>$idconpol));
                }else{
                    $this->ModeloCatalogos->Insert('asignacion_ser_poliza',array('fecha'=>$fecha,'hora'=>$hora,'horafin'=>$horafin,'hora_comida'=>$hora_comida,'horafin_comida'=>$horafin_comida,'polizaId'=>$idconpol));
                }
            }else{
                if ($tipo==1) {
                    $this->ModeloCatalogos->Insert('asignacion_ser_contrato_t',array('fecha'=>$fecha,'hora'=>$hora,'horafin'=>$horafin,'hora_comida'=>$hora_comida,'horafin_comida'=>$horafin_comida,'contratoId'=>$idconpol));
                }else{
                    $this->ModeloCatalogos->Insert('asignacion_ser_poliza_t',array('fecha'=>$fecha,'hora'=>$hora,'horafin'=>$horafin,'hora_comida'=>$hora_comida,'horafin_comida'=>$horafin_comida,'polizaId'=>$idconpol));
                }
            }
        }
    }
    function get_savetecnico(){
        $tasign=$this->input->post('tasign');
        $tipo=$this->input->post('tipo');
        $idconpol=$this->input->post('idconpol');
        $serviciorow=$this->input->post('serviciorow');
        $tecnico=$this->input->post('tecnico');
        if ($serviciorow>0) {
            if ($tasign==1) {
                if ($tipo==1) {
                    $this->ModeloCatalogos->updateCatalogo('asignacion_ser_contrato',array('tecnico'=>$tecnico),array('asignacionId'=>$serviciorow));
                }else{
                    $this->ModeloCatalogos->updateCatalogo('asignacion_ser_poliza',array('tecnico'=>$tecnico),array('asignacionId'=>$serviciorow));
                }
            }else{
                if ($tipo==1) {
                    $this->ModeloCatalogos->updateCatalogo('asignacion_ser_contrato_t',array('tecnico'=>$tecnico),array('asignacionId'=>$serviciorow));
                }else{
                    $this->ModeloCatalogos->updateCatalogo('asignacion_ser_poliza_t',array('tecnico'=>$tecnico),array('asignacionId'=>$serviciorow));
                }
            }
        }else{
            if ($tasign==1) {
                if ($tipo==1) {
                    $this->ModeloCatalogos->Insert('asignacion_ser_contrato',array('fecnico'=>$tecnico,'contratoId'=>$idconpol));
                }else{
                    $this->ModeloCatalogos->Insert('asignacion_ser_poliza',array('tecnico'=>$tecnico,'polizaId'=>$idconpol));
                }
            }else{
                if ($tipo==1) {
                    $this->ModeloCatalogos->Insert('asignacion_ser_contrato_t',array('tecnico'=>$tecnico,'contratoId'=>$idconpol));
                }else{
                    $this->ModeloCatalogos->Insert('asignacion_ser_poliza_t',array('tecnico'=>$tecnico,'polizaId'=>$idconpol));
                }
            }
        }
    }
    function get_saveprioridad(){
        $tasign=$this->input->post('tasign');
        $tipo=$this->input->post('tipo');
        $idconpol=$this->input->post('idconpol');
        $serviciorow=$this->input->post('serviciorow');
        $priodidad=$this->input->post('priodidad');
        $priodidad2=$this->input->post('priodidad2');
        $hora_cri_ser=$this->input->post('hora_cri_ser');
        if ($serviciorow>0) {
            if ($tasign==1) {
                if ($tipo==1) {
                    $this->ModeloCatalogos->updateCatalogo('asignacion_ser_contrato',array('prioridad'=>$priodidad,'prioridad2'=>$priodidad2,'hora_cri_ser'=>$hora_cri_ser),array('asignacionId'=>$serviciorow));
                }else{
                    $this->ModeloCatalogos->updateCatalogo('asignacion_ser_poliza',array('prioridad'=>$priodidad,'prioridad2'=>$priodidad2,'hora_cri_ser'=>$hora_cri_ser),array('asignacionId'=>$serviciorow));
                }
            }else{
                if ($tipo==1) {
                    $this->ModeloCatalogos->updateCatalogo('asignacion_ser_contrato_t',array('prioridad'=>$priodidad,'prioridad2'=>$priodidad2,'hora_cri_ser'=>$hora_cri_ser),array('asignacionId'=>$serviciorow));
                }else{
                    $this->ModeloCatalogos->updateCatalogo('asignacion_ser_poliza_t',array('prioridad'=>$priodidad,'prioridad2'=>$priodidad2,'hora_cri_ser'=>$hora_cri_ser),array('asignacionId'=>$serviciorow));
                }
            }
        }else{
            if ($tasign==1) {
                if ($tipo==1) {
                    $this->ModeloCatalogos->Insert('asignacion_ser_contrato',array('prioridad'=>$priodidad,'contratoId'=>$idconpol,'prioridad2'=>$priodidad2,'hora_cri_ser'=>$hora_cri_ser));
                }else{
                    $this->ModeloCatalogos->Insert('asignacion_ser_poliza',array('prioridad'=>$priodidad,'polizaId'=>$idconpol,'prioridad2'=>$priodidad2,'hora_cri_ser'=>$hora_cri_ser));
                }
            }else{
                if ($tipo==1) {
                    $this->ModeloCatalogos->Insert('asignacion_ser_contrato_t',array('prioridad'=>$priodidad,'contratoId'=>$idconpol,'prioridad2'=>$priodidad2,'hora_cri_ser'=>$hora_cri_ser));
                }else{
                    $this->ModeloCatalogos->Insert('asignacion_ser_poliza_t',array('prioridad'=>$priodidad,'polizaId'=>$idconpol,'prioridad2'=>$priodidad2,'hora_cri_ser'=>$hora_cri_ser));
                }
            }
        }
    }
    function get_savezona(){
        $tasign=$this->input->post('tasign');
        $tipo=$this->input->post('tipo');
        $idconpol=$this->input->post('idconpol');
        $serviciorow=$this->input->post('serviciorow');
        $zona=$this->input->post('zona');
        if ($serviciorow>0) {
            if ($tasign==1) {
                if ($tipo==1) {
                    $this->ModeloCatalogos->updateCatalogo('asignacion_ser_contrato',array('zonaId'=>$zona),array('asignacionId'=>$serviciorow));
                }else{
                    $this->ModeloCatalogos->updateCatalogo('asignacion_ser_poliza',array('zonaId'=>$zona),array('asignacionId'=>$serviciorow));
                }
            }else{
                if ($tipo==1) {
                    $this->ModeloCatalogos->updateCatalogo('asignacion_ser_contrato_t',array('zonaId'=>$zona),array('asignacionId'=>$serviciorow));
                }else{
                    $this->ModeloCatalogos->updateCatalogo('asignacion_ser_poliza_t',array('zonaId'=>$zona),array('asignacionId'=>$serviciorow));
                }
            }
        }else{
            if ($tasign==1) {
                if ($tipo==1) {
                    $this->ModeloCatalogos->Insert('asignacion_ser_contrato',array('zonaId'=>$zona,'contratoId'=>$idconpol));
                }else{
                    $this->ModeloCatalogos->Insert('asignacion_ser_poliza',array('zonaId'=>$zona,'polizaId'=>$idconpol));
                }
            }else{
                if ($tipo==1) {
                    $this->ModeloCatalogos->Insert('asignacion_ser_contrato_t',array('zonaId'=>$zona,'contratoId'=>$idconpol));
                }else{
                    $this->ModeloCatalogos->Insert('asignacion_ser_poliza_t',array('zonaId'=>$zona,'polizaId'=>$idconpol));
                }
            }
        }
    }
    function get_savemotivos(){
        $tasign=$this->input->post('tasign');
        $tipo=$this->input->post('tipo');
        $idconpol=$this->input->post('idconpol');
        $serviciorow=$this->input->post('serviciorow');
        $tservicio=$this->input->post('tservicio');
        $tserviciomotivo=$this->input->post('tserviciomotivo');
        $tpoliza = $this->input->post('tpoliza');
        $ttipo = $this->input->post('ttipo');
        if ($serviciorow>0) {
            if ($tasign==1) {
                if ($tipo==1) {
                    $this->ModeloCatalogos->updateCatalogo('asignacion_ser_contrato',array('tserviciomotivo'=>$tserviciomotivo,'tservicio'=>$tservicio,'tpoliza'=>$tpoliza,'ttipo'=>$ttipo),array('asignacionId'=>$serviciorow));
                }else{
                    $this->ModeloCatalogos->updateCatalogo('asignacion_ser_poliza',array('tserviciomotivo'=>$tserviciomotivo,'tservicio'=>$tservicio,'tpoliza'=>$tpoliza,'ttipo'=>$ttipo),array('asignacionId'=>$serviciorow));
                }
            }else{
                if ($tipo==1) {
                    $this->ModeloCatalogos->updateCatalogo('asignacion_ser_contrato_t',array('tserviciomotivo'=>$tserviciomotivo,'tservicio'=>$tservicio,'tpoliza'=>$tpoliza,'ttipo'=>$ttipo),array('asignacionId'=>$serviciorow));
                }else{
                    $this->ModeloCatalogos->updateCatalogo('asignacion_ser_poliza_t',array('tserviciomotivo'=>$tserviciomotivo,'tservicio'=>$tservicio,'tpoliza'=>$tpoliza,'ttipo'=>$ttipo),array('asignacionId'=>$serviciorow));
                }
            }
        }else{
            if ($tasign==1) {
                if ($tipo==1) {
                    $this->ModeloCatalogos->Insert('asignacion_ser_contrato',array('tserviciomotivo'=>$tserviciomotivo,'tservicio'=>$tservicio,'contratoId'=>$idconpol,'tpoliza'=>$tpoliza,'ttipo'=>$ttipo));
                }else{
                    $this->ModeloCatalogos->Insert('asignacion_ser_poliza',array('tserviciomotivo'=>$tserviciomotivo,'tservicio'=>$tservicio,'polizaId'=>$idconpol,'tpoliza'=>$tpoliza,'ttipo'=>$ttipo));
                }
            }else{
                if ($tipo==1) {
                    $this->ModeloCatalogos->Insert('asignacion_ser_contrato_t',array('tserviciomotivo'=>$tserviciomotivo,'tservicio'=>$tservicio,'contratoId'=>$idconpol,'tpoliza'=>$tpoliza,'ttipo'=>$ttipo));
                }else{
                    $this->ModeloCatalogos->Insert('asignacion_ser_poliza_t',array('tserviciomotivo'=>$tserviciomotivo,'tservicio'=>$tservicio,'polizaId'=>$idconpol,'tpoliza'=>$tpoliza,'ttipo'=>$ttipo));
                }
            }
        }
    }
    function generar(){
        $tasign=$this->input->post('tasign');
        $tipo=$this->input->post('tipo');
        $idconpol=$this->input->post('idconpol');
        $serviciorow=$this->input->post('serviciorow');
        $equipos=$this->input->post('equipos');

        $vconsumible=$this->input->post('vconsumible');
        $vrefaccion=$this->input->post('vrefaccion');
        $vaccesorio=$this->input->post('vaccesorio');
        $cfconsuf=$this->input->post('cfconsuf');
        $stock_toner=$this->input->post('stock_toner');
        $servicios=$this->input->post('servicios');

        $idpolizaser=$this->input->post('idpolizaser');
        $tipopc=$this->input->post('tipopc');

        $genera=1;
        if ($serviciorow>0) {
            if ($tasign==1) {// pre-asignacion
                if ($tipo==1) {
                    
                    //===========================================================
                    $this->ModeloCatalogos->updateCatalogo('asignacion_ser_contrato',array('generado'=>1),array('asignacionId'=>$serviciorow));
                    $respuesta=$this->ModeloCatalogos->getselectwheren('asignacion_ser_contrato',array('asignacionId'=>$serviciorow));
                    foreach ($respuesta->result() as $item) {
                        if ($item->fecha==null) {
                            $genera=0;
                            log_message('error', 'contrato falta informacion de fecha');
                        }
                        if ($item->hora==null) {
                            $genera=0;
                            log_message('error', 'contrato falta informacion de hora');
                        }
                       
                        if ($item->prioridad==null) {
                            $genera=0;
                            log_message('error', 'contrato falta informacion de prioridad');
                        }
                        
                        if ($item->zonaId==null) {
                            //$genera=0;
                            //log_message('error', 'contrato falta informacion de zona');
                            $item->zonaId=2;
                        }
                        
                        $contratoId=$item->contratoId;
                        $fecha=$item->fecha;
                        $hora=$item->hora;
                        $horafin=$item->horafin;
                        
                        $hora = $this->ModeloCatalogos->verificarhora($hora);
                        $horafin = $this->ModeloCatalogos->verificarhora($horafin);
                        
                        $hora_comida=$item->hora_comida;
                        $horafin_comida=$item->horafin_comida;
                        $tecnico=$item->tecnico;
                        $prioridad=$item->prioridad;
                        $prioridad2=$item->prioridad2;
                        $hora_cri_ser=$item->hora_cri_ser;
                        $zonaId=$item->zonaId;
                        
                        $tpoliza=$item->tpoliza;
                        $ttipo=$item->ttipo;
                        $tservicio=$item->tservicio;
                        $tserviciomotivo = $item->tserviciomotivo;
                    }
                    /*
                    $resvisiosrow=$this->ModeloCatalogos->getselectwheren('asignacion_ser_contrato_a',array('contratoId'=>$contratoId,'tiposervicio'=>0,'activo'=>1));
                    if ($resvisiosrow->num_rows()>0) {
                        $genera=2;
                    }
                    */
                    if ($genera==1) {
                        $respuestacont=$this->ModeloCatalogos->getselectwheren('contrato',array('idcontrato'=>$contratoId));
                        $idRenta=0;
                        $tipocontrato=0;
                        foreach ($respuestacont->result() as $item) {
                            $idRenta=$item->idRenta;
                            if($item->tipocontrato==1){
                                //$tipocontrato=12;
                                $tipocontrato=1;
                            }elseif ($item->tipocontrato==2) {
                                //$tipocontrato=24;
                                $tipocontrato=1;
                            }elseif ($item->tipocontrato==3) {
                                //$tipocontrato=35;
                                $tipocontrato=1;
                            }else{
                                $tipocontrato=1;
                            }
                            //log_message('error', 'servicios0:'.$tipocontrato );
                            //$tipocontrato=$tipocontrato-$this->serviciosgenerados($contratoId,$this->fechaactual);
                            //log_message('error', 'servicios1:'.$tipocontrato );
                        }

                        //===========================================================
                            $registro=1;
                            $fechainicial=$fecha;
                            while ($registro <= $tipocontrato){
                                if($ttipo>0){}else{ $ttipo=1;}
                                $datainfo=array(
                                            'contratoId'=>$contratoId,
                                            'prioridad'=>$prioridad,
                                            'prioridad2'=>$prioridad2,
                                            'hora_cri_ser'=>$hora_cri_ser,
                                            'zonaId'=>$zonaId,
                                            'tpoliza'=>$tpoliza,
                                            'ttipo'=>$ttipo,
                                            'tservicio_a'=>$tservicio,
                                            'tserviciomotivo'=>$tserviciomotivo,
                                            'creado_personal'=>$this->idpersonal,
                                            'stock_toner'=>$stock_toner
                                            );
                                $asignacionId=$this->ModeloCatalogos->db4_Insert('asignacion_ser_contrato_a',$datainfo);
                                $dataeqarray=array();
                                $DATAeq = json_decode($equipos);
                                for ($j=0;$j<count($DATAeq);$j++){
                                //$respuestarentae=$this->ModeloCatalogos->getselectwheren('rentas_has_detallesEquipos',array('id'=>$idRenta));
                                //foreach ($respuestarentae->result() as $item) {
                                  //  $respuestarentaess=$this->ModeloCatalogos->getselectwheren('asignacion_series_r_equipos',array('id_equipo'=>$item->id));
                                    //foreach ($respuestarentaess->result() as $itemss) {
                                        $datainfoeq=array(
                                                'asignacionId'=>$asignacionId,
                                                'idequipo'=>$DATAeq[$j]->equipos,
                                                'serieId'=>$DATAeq[$j]->serie,
                                                'tecnico'=>$DATAeq[$j]->tecnico,
                                                'tecnico2'=>$DATAeq[$j]->tecnico2,
                                                'tecnico3'=>$DATAeq[$j]->tecnico3,
                                                'tecnico4'=>$DATAeq[$j]->tecnico4,
                                                'fecha'=>$fechainicial,
                                                'hora'=>$hora,
                                                'horafin'=>$horafin,
                                                'hora_comida'=>$hora_comida,
                                                'horafin_comida'=>$horafin_comida
                                            );
                                        $dataeqarray[]=$datainfoeq;
                                        //$this->ModeloCatalogos->db4_Insert('asignacion_ser_contrato_a_e',$datainfoeq);
                                   // }
                                    
                                }
                                if (count($DATAeq)>0) {
                                    $this->ModeloCatalogos->db4_insert_batch('asignacion_ser_contrato_a_e',$dataeqarray);
                                }
                                $fechainicial = strtotime ( '+ 1 month' , strtotime ( $fechainicial ) ) ;
                                $fechainicial = date ( 'Y-m-d' , $fechainicial );
                                $registro++;
                                $this->verificardirecciones($asignacionId);
                            }
                        //===========================================================

                    }
                }else{
                    //$this->ModeloCatalogos->updateCatalogo('asignacion_ser_poliza',array('zonaId'=>$zona),array('asignacionId'=>$serviciorow));
                    $this->ModeloCatalogos->updateCatalogo('asignacion_ser_poliza',array('generado'=>1),array('asignacionId'=>$serviciorow));
                    $respuesta=$this->ModeloCatalogos->getselectwheren('asignacion_ser_poliza',array('asignacionId'=>$serviciorow));
                    foreach ($respuesta->result() as $item) {
                        if ($item->fecha==null) {
                            $genera=0;
                            log_message('error', 'tipo0 poliza falta informacion de fecha');
                        }
                        if ($item->hora==null) {
                            $genera=0;
                            log_message('error', 'tipo0 poliza falta informacion de hora');
                        }
                        
                        if ($item->prioridad==null) {
                            $genera=0;
                            
                            log_message('error', 'tipo0 poliza falta informacion prioridad');
                        }
                        
                        if ($item->zonaId==null) {
                            //$genera=0;  
                            //log_message('error', 'tipo0 poliza falta informacion de zona');
                            $item->zonaId=2;
                        }
                        
                        $polizaId=$item->polizaId;
                        $fecha=$item->fecha;
                        $hora=$item->hora;
                        $horafin=$item->horafin;

                        $hora = $this->ModeloCatalogos->verificarhora($hora);
                        $horafin = $this->ModeloCatalogos->verificarhora($horafin);

                        $hora_comida=$item->hora_comida;
                        $horafin_comida=$item->horafin_comida;
                        $tecnico=$item->tecnico;
                        $prioridad=$item->prioridad;
                        $prioridad2=$item->prioridad2;
                        $hora_cri_ser=$item->hora_cri_ser;
                        $zonaId=$item->zonaId;
                        //==============================
                        $tpoliza=$item->tpoliza;
                        $ttipo=$item->ttipo;
                        $tservicio=$item->tservicio;
                        $tserviciomotivo = $item->tserviciomotivo;
                        //==============================
                    }
                    //========================================= se define si se cobra o no
                        $totaldeasignaciones= $this->numerodepolizasasignadas($polizaId);
                        $totaldeasignaciones=$totaldeasignaciones+1;
                    //=========================================
                    //$resvisiosrow=$this->ModeloCatalogos->getselectwheren('asignacion_ser_poliza_a',array('polizaId'=>$polizaId,'tiposervicio'=>0,'activo'=>1));
                    //if ($resvisiosrow->num_rows()>0) {
                      //  $genera=2;
                    //}
                    //if ($genera==1) {
                        $tipocontrato=1;
                        //===========================================================
                            $registro=1;
                            $fechainicial=$fecha;
                            while ($registro <= $tipocontrato){

                                $datainfo=array(
                                            'polizaId'=>$polizaId,
                                            'prioridad'=>$prioridad,
                                            'prioridad2'=>$prioridad2,
                                            'hora_cri_ser'=>$hora_cri_ser,
                                            'zonaId'=>$zonaId,
                                            'tpoliza'=>null,
                                            'ttipo'=>null,
                                            'tservicio'=>null,
                                            'tserviciomotivo'=>null,
                                            'creado_personal'=>$this->idpersonal,
                                            'tipo_pre_corr'=>$tipopc
                                            );
                                $asignacionId=$this->ModeloCatalogos->Insert('asignacion_ser_poliza_a',$datainfo);
                                $this->ModeloCatalogos->updateCatalogo('polizasCreadas',array('alert'=>0,'alert_date'=>''),array('id' => $polizaId));
                                if($idpolizaser>0){
                                    $this->ModeloCatalogos->updateCatalogo('polizascreadas_sercontr',array('activo'=>0),array('idpolizaser' => $idpolizaser));
                                }
                                //$respuestarentae=$this->ModeloCatalogos->getselectwheren('polizasCreadas_has_detallesPoliza',array('idPoliza'=>$polizaId));
                                //foreach ($respuestarentae->result() as $item) {
                                    //$respuestarentaess=$this->ModeloCatalogos->getselectwheren('polizasCreadas_has_detallesPoliza_series',array('iddetalle'=>$item->id));
                                    $rowseries=0;
                                    //foreach ($respuestarentaess->result() as $itemss) {
                                    $DATAeq = json_decode($equipos);
                                    $dataeqarray=array();
                                    for ($j=0;$j<count($DATAeq);$j++){
                                        //===================================================================
                                        $fechappasig=$this->fechadeprimerapolizasasignadas($polizaId);
                                        $vigencia_meses=$this->obtenervigenciapolizavm($polizaId,$DATAeq[$j]->equipos);

                                        //log_message('error', 'vigencia_meses '.$vigencia_meses.' fechappasig: '.$fechappasig);
                                        $fechafinal = strtotime ( '+'.$vigencia_meses.' month' , strtotime ( $fechappasig ) ) ;
                                        $fechafinal = date ( 'Y-m-d' , $fechafinal );

                                        if ($this->fechaactual>$fechafinal) {
                                            $cobrar=1;
                                            //log_message('error', 'cobrar 1 '.$this->fechaactual.' '.$fechafinal);
                                        }else{
                                            $vigencia_clicks=$this->obtenervigenciapolizavc($polizaId,$DATAeq[$j]->equipos);

                                            if (intval($totaldeasignaciones)<=intval($vigencia_clicks)) {
                                                $cobrar=0;
                                                //log_message('error', 'cobrar 2 ');
                                            }else{
                                                $cobrar=1;
                                                //log_message('error', 'cobrar 3 ');
                                            }
                                        }



                                        
                                        //==========================================================================


                                        $datainfoequipo2=array(
                                                'asignacionId'=>$asignacionId,
                                                'idequipo'=>$DATAeq[$j]->equipos,
                                                'serie'=>$DATAeq[$j]->serie,
                                                'tecnico'=>$DATAeq[$j]->tecnico,
                                                'tecnico2'=>$DATAeq[$j]->tecnico2,
                                                'tecnico3'=>$DATAeq[$j]->tecnico3,
                                                'tecnico4'=>$DATAeq[$j]->tecnico4,
                                                'fecha'=>$fechainicial,
                                                'hora'=>$hora,
                                                'horafin'=>$horafin,
                                                'hora_comida'=>$hora_comida,
                                                'horafin_comida'=>$horafin_comida,
                                                'cobrar'=>$cobrar,
                                                'idvinculo'=>$DATAeq[$j]->idvinculo
                                            );
                                        $dataeqarray[]=$datainfoequipo2;
                                        //$this->ModeloCatalogos->Insert('asignacion_ser_poliza_a_e',$datainfoequipo2);
                                        $rowseries=1;
                                    
                                    
                                    }
                                    if (count($DATAeq)>0) {
                                        $this->ModeloCatalogos->insert_batch('asignacion_ser_poliza_a_e',$dataeqarray);
                                    }
                                $fechainicial = strtotime ( '+ 1 month' , strtotime ( $fechainicial ) ) ;
                                $fechainicial = date ( 'Y-m-d' , $fechainicial );
                                $registro++;
                            }
                        //===========================================================
                    //}
                }
            }else{// asgnacion del dia
                if ($tipo==1) {
                    //$this->ModeloCatalogos->updateCatalogo('asignacion_ser_contrato_t',array('zonaId'=>$zona),array('asignacionId'=>$serviciorow));
                    $respuesta=$this->ModeloCatalogos->getselectwheren('asignacion_ser_contrato_t',array('asignacionId'=>$serviciorow));
                    foreach ($respuesta->result() as $item) {
                        if ($item->fecha==null) {
                            $genera=0;
                            log_message('error', 'contrato falta informacion de fecha');
                        }
                        if ($item->hora==null) {
                            $genera=0;
                            log_message('error', 'contrato falta informacion de hora');
                        }
                        
                        if ($item->prioridad==null) {
                            $genera=0;
                            log_message('error', 'contrato falta informacion prioridad');
                        }
                        
                        if ($item->zonaId==null) {
                            //$genera=0;
                            //log_message('error', 'contrato falta informacion de zona');
                            $item->zonaId=2;
                        }
                        
                        if ($item->tservicio==null) {
                            $genera=0;
                            log_message('error', 'contrato falta informacion de tipo servicio');
                        }
                        
                        $contratoId=$item->contratoId;
                        $fecha=$item->fecha;
                        $hora=$item->hora;
                        $horafin=$item->horafin;

                        $hora = $this->ModeloCatalogos->verificarhora($hora);
                        $horafin = $this->ModeloCatalogos->verificarhora($horafin);

                        $hora_comida=$item->hora_comida;
                        $horafin_comida=$item->horafin_comida;
                        $tecnico=$item->tecnico;
                        $prioridad=$item->prioridad;
                        $prioridad2=$item->prioridad2;
                        $hora_cri_ser=$item->hora_cri_ser;
                        $zonaId=$item->zonaId;
                        $tservicio=$item->tservicio;
                        $tserviciomotivo=$item->tserviciomotivo;

                        $ttipo=$item->ttipo;
                        $tpoliza=$item->tpoliza;
                    }
                    if ($genera==1) {
                        $fechainicial=$fecha;
                        if ($prioridad==2) {
                            //$fechainicial = strtotime ( '+ 1 day' , strtotime ( $fechainicial ) ) ;
                            //$fechainicial = date ( 'Y-m-d' , $fechainicial );
                        }
                        if ($prioridad==3) {
                            //$fechainicial = strtotime ( '+ 2 week' , strtotime ( $fechainicial ) ) ;
                            //$fechainicial = date ( 'Y-m-d' , $fechainicial );
                        }

                        if($ttipo>0){}else{ $ttipo=1;}
                        $datainfo=array(
                                    'contratoId'=>$contratoId,
                                    'prioridad'=>$prioridad,
                                    'prioridad2'=>$prioridad2,
                                    'hora_cri_ser'=>$hora_cri_ser,
                                    'zonaId'=>$zonaId,
                                    'tiposervicio'=>1,
                                    'tservicio_a'=>$tservicio,
                                    'tserviciomotivo'=>$tserviciomotivo,
                                    'ttipo'=>$ttipo,
                                    'tpoliza'=>$tpoliza,
                                    'creado_personal'=>$this->idpersonal
                                    );
                        $asignacionId=$this->ModeloCatalogos->db4_Insert('asignacion_ser_contrato_a',$datainfo);
                        $dataeqarray=array();
                        $DATAeq = json_decode($equipos);
                        for ($j=0;$j<count($DATAeq);$j++){
                            $datainfoequipo3=array(
                                    'asignacionId'=>$asignacionId,
                                    'idequipo'=>$DATAeq[$j]->equipos,
                                    'serieId'=>$DATAeq[$j]->serie,
                                    'tecnico'=>$DATAeq[$j]->tecnico,
                                    'tecnico2'=>$DATAeq[$j]->tecnico2,
                                    'tecnico3'=>$DATAeq[$j]->tecnico3,
                                    'tecnico4'=>$DATAeq[$j]->tecnico4,
                                    'fecha'=>$fechainicial,
                                    'hora'=>$hora,
                                    'horafin'=>$horafin,
                                    'hora_comida'=>$hora_comida,
                                    'horafin_comida'=>$horafin_comida
                                );
                                //$this->ModeloCatalogos->db4_Insert('asignacion_ser_contrato_a_e',$datainfoequipo3);
                            $dataeqarray[]=$datainfoequipo3;
                        }
                        if (count($DATAeq)>0) {
                            $this->ModeloCatalogos->db4_insert_batch('asignacion_ser_contrato_a_e',$dataeqarray);
                        }
                        $DATAvc = json_decode($vconsumible);
                        for ($s=0;$s<count($DATAvc);$s++){
                            $datavca =array(
                                'asignacionId'=>$asignacionId,
                                'cantidad'=>$DATAvc[$s]->cantidad,
                                'consumible'=>$DATAvc[$s]->consumible,
                                'costo'=>$DATAvc[$s]->precio,
                                'tipo'=>1
                            );
                            $this->ModeloCatalogos->db4_Insert('asignacion_ser_avc',$datavca);
                        }
                        $DATAvr = json_decode($vrefaccion);
                        for ($c=0;$c<count($DATAvr);$c++){
                            $datavcra =array(
                                'asignacionId'=>$asignacionId,
                                'cantidad'=>$DATAvr[$c]->cantidad,
                                'refacciones'=>$DATAvr[$c]->refaccion,
                                'costo'=>$DATAvr[$c]->precio,
                                'tipo'=>1
                            );
                            $this->ModeloCatalogos->db4_Insert('asignacion_ser_avr',$datavcra);
                        }
                        $DATAva = json_decode($vaccesorio);
                        for ($x=0;$x<count($DATAva);$x++){
                            $datavcrac =array(
                                'asignacionId'=>$asignacionId,
                                'cantidad'=>$DATAva[$x]->cantidad,
                                'accessorio'=>$DATAva[$x]->accesorio,
                                'costo'=>$DATAva[$x]->precio,
                                'tipo'=>1
                            );
                            $this->ModeloCatalogos->db4_Insert('asignacion_ser_ava',$datavcrac);
                        }
                        $DATAcf = json_decode($cfconsuf);
                        for ($z=0;$z<count($DATAcf);$z++){
                            $this->ModeloCatalogos->updateCatalogo('contrato_folio',array('serviciocId'=>$asignacionId),array('idcontratofolio'=>$DATAcf[$z]->cffolioid));
                        }
                        $this->verificardirecciones($asignacionId);

                    }
                    
                }else{
                    //$this->ModeloCatalogos->updateCatalogo('asignacion_ser_poliza_t',array('zonaId'=>$zona),array('asignacionId'=>$serviciorow));
                    $respuesta=$this->ModeloCatalogos->getselectwheren('asignacion_ser_poliza_t',array('asignacionId'=>$serviciorow));
                    foreach ($respuesta->result() as $item) {
                        if ($item->fecha==null) {
                            $genera=0;
                            log_message('error', 'tipo1 Poliza falta informacion de fecha');
                        }
                        if ($item->hora==null) {
                            $genera=0;
                            log_message('error', 'tipo1 Poliza falta informacion de hora');
                        }
                        
                        if ($item->prioridad==null) {
                            $genera=0;
                            log_message('error', 'tipo1 Poliza falta informacion prioridad');
                        }
                        
                        if ($item->zonaId==null) {
                            //$genera=0;
                            //log_message('error', 'tipo1 Poliza falta informacion de zona');
                            $item->zonaId=2;
                        }
                        
                        if ($item->tservicio==null) {
                            //$genera=0;
                            //log_message('error', 'tipo1 Poliza falta informacion de tipo servicio');
                        }
                        if ($item->tserviciomotivo==null) {
                            //$genera=0;
                            //log_message('error', 'tipo1 Poliza falta informacion de motivo');
                        }
                        $polizaId=$item->polizaId;
                        $fecha=$item->fecha;
                        $hora=$item->hora;
                        $horafin=$item->horafin;

                        $hora = $this->ModeloCatalogos->verificarhora($hora);
                        $horafin = $this->ModeloCatalogos->verificarhora($horafin);
                        
                        $hora_comida=$item->hora_comida;
                        $horafin_comida=$item->horafin_comida;
                        $tecnico=$item->tecnico;
                        $prioridad=$item->prioridad;
                        $prioridad2=$item->prioridad2;
                        $hora_cri_ser=$item->hora_cri_ser;
                        $zonaId=$item->zonaId;
                        $tservicio=$item->tservicio;
                        $tserviciomotivo=$item->tserviciomotivo;
                        $tpoliza=$item->tpoliza;
                        $ttipo=$item->ttipo;
                    }
                    //========================================= se define si se cobra o no
                        $totaldeasignaciones= $this->numerodepolizasasignadas($polizaId);
                        $totaldeasignaciones=$totaldeasignaciones+1;
                    //=========================================
                    if ($genera==1) {
                        $fechainicial=$fecha;
                        if ($prioridad==2) {
                            //$fechainicial = strtotime ( '+ 1 day' , strtotime ( $fechainicial ) ) ;
                            //$fechainicial = date ( 'Y-m-d' , $fechainicial );
                        }
                        if ($prioridad==3) {
                            //$fechainicial = strtotime ( '+ 2 week' , strtotime ( $fechainicial ) ) ;
                            //$fechainicial = date ( 'Y-m-d' , $fechainicial );
                        }
                        $datainfo=array(
                                    'polizaId'=>$polizaId,
                                    'prioridad'=>$prioridad,
                                    'prioridad2'=>$prioridad2,
                                    'hora_cri_ser'=>$hora_cri_ser,
                                    'zonaId'=>$zonaId,
                                    'tiposervicio'=>1,
                                    'tservicio'=>null,
                                    'tserviciomotivo'=>null,
                                    'tpoliza'=>null,
                                    'ttipo'=>null,
                                    'creado_personal'=>$this->idpersonal,
                                    'tipo_pre_corr'=>$tipopc
                                    );
                        $asignacionId=$this->ModeloCatalogos->Insert('asignacion_ser_poliza_a',$datainfo);
                        $this->ModeloCatalogos->updateCatalogo('polizasCreadas',array('alert'=>0,'alert_date'=>''),array('id' => $polizaId));
                        if($idpolizaser>0){
                            $this->ModeloCatalogos->updateCatalogo('polizascreadas_sercontr',array('activo'=>0),array('idpolizaser' => $idpolizaser));
                        }
                        $dataeqarray=array();
                        $DATAeq = json_decode($equipos);
                        for ($j=0;$j<count($DATAeq);$j++){
                            //===================================================================
                                $fechappasig=$this->fechadeprimerapolizasasignadas($polizaId);
                                $vigencia_meses=$this->obtenervigenciapolizavm($polizaId,$DATAeq[$j]->equipos);

                                //log_message('error', 'vigencia_meses '.$vigencia_meses.' fechappasig: '.$fechappasig);
                                $fechafinal = strtotime ( '+'.$vigencia_meses.' month' , strtotime ( $fechappasig ) ) ;
                                $fechafinal = date ( 'Y-m-d' , $fechafinal );

                                if ($this->fechaactual>$fechafinal) {
                                    $cobrar=1;
                                    //log_message('error', 'cobrar 1 '.$this->fechaactual.' '.$fechafinal);
                                }else{
                                    $vigencia_clicks=$this->obtenervigenciapolizavc($polizaId,$DATAeq[$j]->equipos);

                                    if (intval($totaldeasignaciones)<=intval($vigencia_clicks)) {
                                        $cobrar=0;
                                        //log_message('error', 'cobrar 2 ');
                                    }else{
                                        $cobrar=1;
                                        //log_message('error', 'cobrar 3 ');
                                    }
                                }
    
                            //==========================================================================
                                if(isset($DATAeq[$j]->idvinculo)){
                                    $idvinculo=$DATAeq[$j]->idvinculo;
                                }else{
                                    $idvinculo=0;
                                }
                            $datainfoequipo4=array(
                                    'asignacionId'=>$asignacionId,
                                    'idequipo'=>$DATAeq[$j]->equipos,
                                    'serie'=>$DATAeq[$j]->serie,
                                    'tecnico'=>$DATAeq[$j]->tecnico,
                                    'tecnico2'=>$DATAeq[$j]->tecnico2,
                                    'tecnico3'=>$DATAeq[$j]->tecnico3,
                                    'tecnico4'=>$DATAeq[$j]->tecnico4,
                                    'fecha'=>$fechainicial,
                                    'hora'=>$hora,
                                    'horafin'=>$horafin,
                                    'hora_comida'=>$hora_comida,
                                    'horafin_comida'=>$horafin_comida,
                                    'cobrar'=>$cobrar,
                                    'idvinculo'=>$idvinculo
                                );
                                $dataeqarray[]=$datainfoequipo4;
                                //$this->ModeloCatalogos->Insert('asignacion_ser_poliza_a_e',$datainfoequipo4);
                        }
                        if (count($DATAeq)>0) {
                            $this->ModeloCatalogos->insert_batch('asignacion_ser_poliza_a_e',$dataeqarray);
                        }

                        $DATAvc = json_decode($vconsumible);
                        for ($s=0;$s<count($DATAvc);$s++){
                            $datavca =array(
                                'asignacionId'=>$asignacionId,
                                'cantidad'=>$DATAvc[$s]->cantidad,
                                'consumible'=>$DATAvc[$s]->consumible,
                                'costo'=>$DATAvc[$s]->precio,
                                'tipo'=>2
                            );
                            $this->ModeloCatalogos->Insert('asignacion_ser_avc',$datavca);
                        }
                        $DATAvr = json_decode($vrefaccion);
                        for ($c=0;$c<count($DATAvr);$c++){
                            $datavcra =array(
                                'asignacionId'=>$asignacionId,
                                'cantidad'=>$DATAvr[$c]->cantidad,
                                'refacciones'=>$DATAvr[$c]->refaccion,
                                'costo'=>$DATAvr[$c]->precio,
                                'tipo'=>2
                            );
                            $this->ModeloCatalogos->Insert('asignacion_ser_avr',$datavcra);
                        }
                        $DATAva = json_decode($vaccesorio);
                        for ($x=0;$x<count($DATAva);$x++){
                            $datavcrac =array(
                                'asignacionId'=>$asignacionId,
                                'cantidad'=>$DATAva[$x]->cantidad,
                                'accessorio'=>$DATAva[$x]->accesorio,
                                'costo'=>$DATAva[$x]->precio,
                                'tipo'=>2
                            );
                            $this->ModeloCatalogos->Insert('asignacion_ser_ava',$datavcrac);
                        }
                    }
                }
            }
            //log_message('error',json_encode($servicios));
            $DATAser = json_decode($servicios);
            $dataeqarrays=array();
            for ($a=0;$a<count($DATAser);$a++){
                /*
                $datainfoequipo2=array(
                        'o_tipo'=>$tipo,
                        'o_idser'=>$asignacionId,
                        'g_tipo'=>$DATAser[$a]->tipo,
                        'g_idser'=>$DATAser[$a]->servicio
                    );
                    */
                $datainfoequipo2=array(
                        'id_ser'=>$DATAser[$a]->servicio,
                        'id_tipo'=>$DATAser[$a]->tipo,
                        'id_ser_garantia'=>$asignacionId,
                        'id_tipo_garantia'=>$tipo,
                        'personal'=>$this->idpersonal
                    );
                $dataeqarrays[]=$datainfoequipo2;
            }

            if (count($DATAser)>0) {
                //$this->ModeloCatalogos->db4_insert_batch('asignacion_ser_garantia',$dataeqarrays);
                $this->ModeloCatalogos->db4_insert_batch('polizascreadas_ext_servisios',$dataeqarrays);
            }
        }
        echo $genera;
    }
    public function clientes_datos(){
        $html='';    
        $html.='<div class="row">
                    <div class="col s4">
                        <div class="switch"><br>
                              <label>AAA
                                <input type="checkbox" name="aaa" id="aaa">
                                <span class="lever"></span>
                              </label>
                        </div>
                    </div>
                <div class="col s12">
                <input id="idclientenew" type="hidden">
                <ul class="collapsible collapsible-accordion" data-collapsible="accordion">
                    <li class="active">
                      <div class="collapsible-header active">
                        Formulario de Cliente
                      </div>
                      <div class="collapsible-body" style="display: block;">';
                      ///////////////////////////////////////////////////////
                      //<!-- FORMULARIO -->
            
                  $html.='<p>

                            <div class="row">
                                <form class="form" id="clientes_from" method="post">
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <i class="material-icons prefix">location_city</i>
                                            <input id="empresa" name="empresa" type="text" placeholder="Empresa" value="">
                                        </div>
                                        <div class="input-field col s6"></div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s3">
                                            <div class="col s12 margenTopDivChosen" >
                                                <i class="material-icons prefix">local_library</i>
                                                <label for="estado" class="active">Estado</label>
                                                <select id="estado" name="estado" class="browser-default chosen-select">
                                                    <option></option>';
                                                    $get_estados = $this->estados->getEstados();
                                                    foreach($get_estados as $estado_elegido){
                                                    $html.='<option value="'.$estado_elegido->EstadoId.'">'.$estado_elegido->Nombre.'</option>';
                                                    }
                                        $html.='</select>
                                            </div>   
                                        </div>
                                            <input id="giro" name="giro"  type="hidden" value="">
                                            <input id="condicion" name="condicion"  type="hidden" value="">
                                        <div class="input-field col s3">
                                            <i class="material-icons prefix">local_library</i>
                                            <input id="municipio" name="municipio"  type="text" value="">
                                            <label for="municipio">Municipio</label>
                                        </div>
                                        <div class="input-field col s3">
                                            <i class="material-icons prefix">local_offer</i>
                                            <input id="antiguedad" name="antiguedad"  type="date" value="">
                                            <label for="antiguedad" class="active">Antiguedad</label>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="input-field col s10">
                                          <i class="material-icons prefix">directions</i>
                                          <input type="text" id="direcc">
                                          <label for="direcc">Dirección de entrega/instalaciones</label>
                                        </div>
                                        <div class="col s2"><br>
                                          <a class="btn-floating waves-effect waves-light cyan" onclick="add_address()"><i class="material-icons prefix">add</i></a>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col s12 tabla_direcc">
                                            <table class="responsive-table display tabla_dir" cellspacing="0">
                                              <thead>
                                                <tr>
                                                  <th>#</th>
                                                  <th>Dirrección</th>
                                                  <th></th>
                                                </tr>
                                              </thead>
                                              <tbody class="tbody_direccion">
                                              </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12">
                                          <i class="material-icons prefix">question_answer</i>
                                          <textarea id="observaciones" name="observaciones" class="materialize-textarea"></textarea>
                                          <label for="message">Observaciones</label>
                                        </div>
                                    </div>
                                </form>

                                <div class="row">
                                    <div class="input-field col s2">
                                      <input id="atencionpara" name="atencionpara"  type="text" class="inputdatoscontacto">
                                      <label for="atencionpara">Atencion para:</label>
                                    </div>
                                    <div class="input-field col s2">
                                      <input id="puesto_contacto" name="puesto_contacto" type="text" class="inputdatoscontacto">
                                      <label for="puesto_contacto">Puesto de contacto</label>
                                    </div>
                                    <div class="input-field col s2">
                                      <input id="email" name="email"  type="email" class="inputdatoscontacto">
                                      <label for="email">Correo electrónico</label>
                                    </div>
                                    <div class="input-field col s2">
                                      <input id="tel_local" name="tel_local"  type="text" class="inputdatoscontacto">
                                      <label for="tel_local">Telefono</label>
                                    </div>
                                    <div class="input-field col s2">
                                      <input id="celular" name="celular"  type="text" class="inputdatoscontacto">
                                      <label for="celular">celular</label>
                                    </div>
                                    <div class="col s1">
                                      <a class="waves-effect waves-light btn-bmz " onclick="agregar_pc()">Agregar</a>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col s12">
                                      <table class="table table-striped table-condensed table-hover table-responsive" id="tabledatoscontacto">
                                        <thead>
                                            <tr>
                                                <th>Atencion para</th>
                                                <th>Puesto</th>
                                                <th>Email</th>
                                                <th>Telefono</th>
                                                <th>Celular</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody class="tabledatoscontactotb">
                                        </tbody>
                                      </table>
                                    </div>
                                </div>  

                                  <!-- datosfiscales -->
                                  <!-- fin datosfiscales -->
                                  <div class="row">
                                    <div class="col s12">
                                        <h4 class="caption">Tabla de Datos Fiscales 
                                        <a class="waves-effect waves-light btn-bmz modal-trigger" onclick="agregar_d_f()">Datos Fiscales</a></h4>
                                        <div class="row">   
                                           <div class="col s12">
                                                <table class="table table-striped table-condensed table-hover table-responsive border_datos" id="border_datos">
                                                  <thead>
                                                      <tr>
                                                          <th># Registro</th>
                                                          <th>Razón Social</th>
                                                          <th>RFC</th>
                                                          <th>CP</th>
                                                          <th>Calle</th>
                                                          <th># Ext</th>
                                                          <th># Int</th>
                                                          <th>Colonia</th>
                                                          <th>Acciones</th>
                                                      </tr>
                                                  </thead>
                                                  <tbody >
                                                    
                                                  </tbody>
                                              </table> 
                                            </div>
                                        </div>
                                        <br>
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="input-field col s12">
                                      <button class="btn cyan waves-effect waves-light right guardar_cliente" type="button" onclick="guardar_cliente()">Guardar
                                        <i class="material-icons right">send</i>
                                      </button>
                                    </div>
                                  </div>
                                </form>      
                              </div>
                          </p>';
                      ///////////////////////////////////////////////////////     
              $html.='</div>
                    </li>
                  </ul>
                </div>
              </div>
               ';
        echo $html;       
    }
    function savesolicitudcliente(){
        $datoscli=$this->input->post();
        $clienteId=$datos['clienteId'];
        $fecha=$datos['fecha'];
        $hora=$datos['hora'];
        $tecnico=$datos['tecnico'];
        $prioridad=$datos['prioridad'];
        $zonaId=$datos['zonaId'];
        $tservicio=$datos['tservicio'];
        $tserviciomotivo=$datos['tserviciomotivo'];
        $tiposervicio=$datos['tiposervicio'];

        $datosarray=array(
            'clienteId'=>$clienteId,
            'fecha'=>$fecha,
            'hora'=>$hora,
            'tecnico'=>$tecnico,
            'prioridad'=>$prioridad,
            'zonaId'=>$zonaId,
            'tservicio'=>$tservicio,
            'tserviciomotivo'=>$tserviciomotivo,
            'tiposervicio'=>$tiposervicio
        );
        $this->ModeloCatalogos->Insert('asignacion_ser_poliza',$datosarray);
    }
    function obtenerequipos(){
        $datos=$this->input->post();
        $id=$datos['id'];
        $idconpoli=$datos['idconpoli'];
        $cli=$datos['cli'];
        $idpolizaser=$datos['idpolizaser'];
        //$resultstec=$this->Configuraciones_model->searchTecnico();
        $resultstec=$this->Configuraciones_model->view_session_searchTecnico();
        $html='';
        $fil_dir='';
        $fil_dir_row=array();
        $rowtr=1;
        //log_message('error','$idpolizaser 0:'.$idpolizaser);
        if ($idconpoli==1) {//idconpoli 0 contratos 1 polizas
            $respuestarentae=$this->ModeloCatalogos->db10_getselectwheren('polizasCreadas_has_detallesPoliza',array('idPoliza'=>$id,'suspender'=>0));
                foreach ($respuestarentae->result() as $item) {
                    $direccionequipo=$item->direccionservicio;
                    $comeninfo=$item->comeninfo;
                    $respuestarentaess=$this->ModeloCatalogos->getselectwheren('polizasCreadas_has_detallesPoliza_series',array('iddetalle'=>$item->id));
                    $rowseries=0;
                    $modeloeq=$this->obtenerequiposserie2($item->idEquipo);
                    foreach ($respuestarentaess->result() as $itemss) {
                        $serierow="'".$itemss->serie."'";
                        if($item->viewser==1){
                            $disabled='';
                            $trclass='serie_check';
                        }else{
                            $disabled=' disabled ';
                            $trclass='';
                        }
                        //log_message('error','$idpolizaser 1:'.$idpolizaser);
                        if($idpolizaser>0){
                            //log_message('error','$idpolizaser 2:'.$idpolizaser);
                            $pol_ser_ext=$this->ModeloCatalogos->getselectwheren('polizascreadas_sercontr_has_detallespoliza',array('idpolizaser'=>$idpolizaser,'idequipopol'=>$item->id));
                            if($pol_ser_ext->num_rows()>0){
                                $disabled='';
                                $trclass='serie_check';
                                foreach ($pol_ser_ext->result() as $item_pse) {
                                    $comeninfo=$item_pse->comeninfo;
                                }
                            }else{
                                $disabled=' disabled ';
                            }
                        }
                        $html.='
                            <tr>
                                <td>
                                                
                                    <input type="checkbox" class="'.$trclass.' serie_check_'.$itemss->iddetalle.'_'.$itemss->serie.'" id="serie_check_'.$rowtr.'" value="'.$rowtr.'" onclick="v_ulimos_ser('.$rowtr.','.$serierow.',2)" '.$disabled.'>
                                    <label for="serie_check_'.$rowtr.'"></label>
                                                
                                              
                                    <input type="hidden" value="'.$itemss->iddetalle.'" id="equipoid">
                                    <input type="hidden" value="'.$itemss->serie.'" id="serieid">
                                    <input type="hidden" value="'.$item->id.'" id="idvinculo">
                                </td>
                                <td>
                                    '.$modeloeq.'
                                </td>
                                <td>
                                    '.$itemss->serie.'
                                </td>
                                <td>
                                    <select name="tecnico" id="tecnico" class="browser-default form-control-bmz tecnicoselect nuevometodo coun_tec_1">';
                                      
                                        foreach ($resultstec as $item) {
                                          $html.='<option value="'.$item->personalId.'">'.$item->tecnico.'</option>';
                                        }
                                       
                                     $html.='</select>
                                     <select name="tecnico2" id="tecnico2" class="browser-default form-control-bmz nuevometodo coun_tec_2" style="display:none"><option value="0"></option>';
                                        foreach ($resultstec as $item) {
                                          $html.='<option value="'.$item->personalId.'">'.$item->tecnico.'</option>';
                                        }
                                       
                                     $html.='</select>
                                     <select name="tecnico3" id="tecnico3" class="browser-default form-control-bmz nuevometodo coun_tec_3" style="display:none"><option value="0"></option>';
                                        foreach ($resultstec as $item) {
                                          $html.='<option value="'.$item->personalId.'">'.$item->tecnico.'</option>';
                                        }
                                       
                                     $html.='</select>
                                     <select name="tecnico4" id="tecnico4" class="browser-default form-control-bmz nuevometodo coun_tec_4" style="display:none"><option value="0"></option>';
                                        foreach ($resultstec as $item) {
                                          $html.='<option value="'.$item->personalId.'">'.$item->tecnico.'</option>';
                                        }
                                       
                                     $html.='</select>
                                    <input type="text" name="serietext" id="serietext" readonly style="display:none">
                                </td>
                                <td ><table><tr><td>'.$direccionequipo.'</td><td>'.$comeninfo.'</td></tr></table></td>
                            </tr>
                            ';
                        $rowseries=1;
                        $rowtr++;
                    }
                    if ($rowseries==0) {
                        $serierow="'".$item->serie."'";

                        if($item->viewser==1){
                            $disabled='';
                            $trclass='serie_check';
                        }else{
                            $disabled=' disabled ';
                            $trclass='';
                        }
                        if($idpolizaser>0){
                            //log_message('error','$idpolizaser 2:'.$idpolizaser);
                            $pol_ser_ext=$this->ModeloCatalogos->getselectwheren('polizascreadas_sercontr_has_detallespoliza',array('idpolizaser'=>$idpolizaser,'idequipopol'=>$item->id));
                            if($pol_ser_ext->num_rows()>0){
                                $disabled='';
                                $trclass='serie_check';
                                foreach ($pol_ser_ext->result() as $item_pse) {
                                    $comeninfo=$item_pse->comeninfo;
                                }
                            }else{
                                $disabled=' disabled ';
                            }
                        }
                        $html.='
                            <tr>
                                <td>
                                    <input type="checkbox" class="'.$trclass.'" id="serie_check_'.$rowtr.'" value="'.$rowtr.'" onclick="v_ulimos_ser('.$rowtr.','.$serierow.',2)" '.$disabled.'>
                                    <label for="serie_check_'.$rowtr.'"></label>
                                    <input type="hidden" value="'.$item->id.'" id="equipoid">
                                    <input type="hidden" value="'.$item->serie.'" id="serieid">
                                    <input type="hidden" value="'.$item->id.'" id="idvinculo">
                                </td>
                                <td>'.$modeloeq.'</td>
                                <td>'.$item->serie.'</td>
                                <td>
                                    <select name="tecnico" id="tecnico" class="browser-default tecnicoselect form-control-bmz coun_tec_1">';
                                        foreach ($resultstec as $item) {
                                          $html.='<option value="'.$item->personalId.'">'.$item->tecnico.'</option>';
                                        }
                                       
                                     $html.='</select>
                                     <select name="tecnico2" id="tecnico2" class="browser-default form-control-bmz coun_tec_2" style="display:none"><option value="0"></option>';
                                      
                                        foreach ($resultstec as $item) {
                                          $html.='<option value="'.$item->personalId.'">'.$item->tecnico.'</option>';
                                        }
                                       
                                     $html.='</select>
                                     <select name="tecnico3" id="tecnico3" class="browser-default form-control-bmz coun_tec_3" style="display:none"><option value="0"></option>';
                                      
                                        foreach ($resultstec as $item) {
                                          $html.='<option value="'.$item->personalId.'">'.$item->tecnico.'</option>';
                                        }
                                       
                                     $html.='</select>
                                     <select name="tecnico4" id="tecnico4" class="browser-default form-control-bmz coun_tec_4" style="display:none"><option value="0"></option>';
                                      
                                        foreach ($resultstec as $item) {
                                          $html.='<option value="'.$item->personalId.'">'.$item->tecnico.'</option>';
                                        }
                                       
                                     $html.='</select>
                                    <input type="text" name="serietext" id="serietext" readonly style="display:none">
                                </td>
                                <td><table><tr><td>'.$direccionequipo.'</td><td>'.$comeninfo.'</td></tr></table></td>
                                
                            </tr>
                            ';
                            $rowtr++;
                    }
                }
        }else{
            $respuestarentae=$this->ModeloCatalogos->db10_getselectwheren('rentas_has_detallesEquipos',array('idRenta'=>$id));
            foreach ($respuestarentae->result() as $item) {
                $respuestarentaess=$this->ModeloCatalogos->db2_getselectwheren('asignacion_series_r_equipos',array('id_equipo'=>$item->id,'activo'=>1));
                    $modeloe=$item->modelo;
                foreach ($respuestarentaess->result() as $itemss) {
                    $poreliminar = $itemss->poreliminar;
                    $estatus_suspendido = $itemss->estatus_suspendido;
                    $serie=$this->obtenerequiposserie($itemss->serieId);
                    $direccionequipo= $this->getdireccionequipos($itemss->id_equipo,$itemss->serieId);
                    $dir_id= $this->getdireccionequiposiddir($itemss->id_equipo,$itemss->serieId);
                    //$r_sol_cot_r=$this->ModeloCatalogos->db2_getselectwheren('asignacion_ser_contrato_a_e',array('idequipo'=>$itemss->id_equipo,'serieId'=>$itemss->serieId,'cot_status'=>1));
                    //$r_sol_cot_r=$this->ModeloCatalogos->db2_getselectwheren('asignacion_ser_contrato_a_e',array('idequipo'=>$item->id_equipo,'serieId'=>$item->serieId,'cot_status'=>1));
                    $r_sol_cot_r=$this->ModeloCatalogos->get_info_pendiente_solicitud_refaccion($itemss->id_equipo,$itemss->serieId,$serie);
                    $r_info_cot_cot=$this->ModeloCatalogos->info_get_serie_cotizacion($serie,$cli);
                    $r_info_cot_cot_html='';
                    $r_sol_cot_r_icon='';
                    if($r_sol_cot_r->num_rows() >0){
                        //$r_sol_cot_r_icon='<i class="far fa-clipboard fa-fw" title="Solicitud de refaccion pendiente de cotizar"></i>';
                        $r_sol_cot_r_icon.='<br>';
                        foreach ($r_sol_cot_r->result() as $item_as) {
                        /*
                            1 contrato
                            2 poliza
                            3 evento
                            4 venta
                        */
                        $r_sol_cot_r_icon.='<i class="far fa-clipboard fa-fw notificacionsercot" title="Solicitud de refaccion pendiente de cotizar" ';
                        $r_sol_cot_r_icon.='data-asignacionide="'.$item_as->asignacionIde.'"  ';
                        $r_sol_cot_r_icon.='data-asignacionid="'.$item_as->asignacionId.'" ';
                        $r_sol_cot_r_icon.='data-cotrefaccion="'.$item_as->cot_refaccion.'" ';
                        $r_sol_cot_r_icon.='data-cotrefaccion2="'.$item_as->cot_refaccion2.'" ';
                        $r_sol_cot_r_icon.='data-cotrefaccion3="'.$item_as->cot_refaccion3.'" ';
                        $r_sol_cot_r_icon.='data-tiposer="'.$item_as->tiposer.'" ';
                        $r_sol_cot_r_icon.='></i>';
                        }
                    }
                    if($r_info_cot_cot->num_rows() >0){
                        $r_info_cot_cot_html.='<br>';
                    }
                    foreach ($r_info_cot_cot->result() as $item_ic) {
                        // code...
                        $r_info_cot_cot_html.='<a class="b-btn b-btn-primary iconcot " onclick="link_cot('.$item_ic->id.','.$item_ic->idCliente.')" title="Cotizacion '.$item_ic->id.'"><i class="fas fa-file-invoice"></i></a>';
                    }
                    $r_e_s_disabled='';
                    $fil_dir_row[]=array('id'=>$dir_id,'name'=>$direccionequipo);
                    if($estatus_suspendido==1){
                        $r_e_s_disabled=' disabled ';
                        $class_s_c='';
                    }else{
                        $class_s_c=' serie_check_selected serie_check ';
                    }
                   
                    if($poreliminar==1){
                        $r_e_s_disabled=' disabled ';
                        $class_s_c='';
                        $class_s_label='<br><span style="color:red">Equipo Por Eliminar</span>';
                    }else{
                        $class_s_c=' serie_check_selected serie_check ';
                        $class_s_label='';
                    }
                    $html.='<tr class="dir_row dir_'.$dir_id.'"><td style="width: 95px;">';
                                                
                                    $html.='<input type="checkbox" class="'.$class_s_c.' serie_check_'.$itemss->id_equipo.'_'.$itemss->serieId.'" id="serie_check_'.$itemss->serieId.'" value="'.$itemss->serieId.'" onchange="verificarservicios('.$itemss->serieId.')" data-serie="'.$serie.'" '.$r_e_s_disabled.' data-ren="'.$id.'" data-ideq="'.$itemss->id_equipo.'">';
                                    $html.='<label for="serie_check_'.$itemss->serieId.'"></label>';
                                    $html.='<a class="waves-effect cyan btn-bmz consultar_fechas_agenda_'.$itemss->id_equipo.'_'.$itemss->serieId.'"  onclick="consultarfechasagenda('.$itemss->id_equipo.')" style="display:none"><i class="fas fa-calendar-alt" style="font-size: 20px;"></i></a>';

                                    $html.='<input type="hidden" class="ss" value="'.$itemss->id_equipo.'" id="equipoid">';
                                    $html.='<input type="hidden" value="'.$itemss->serieId.'" id="serieid">';
                                    $html.='<input type="hidden" value="'.$dir_id.'" id="dir_id">';
                                    $html.='<input type="hidden" value="0" id="idvinculo">'.$class_s_label.'';
                                $html.='</td>';
                                $html.='<td>'.$modeloe.'</td>';
                                $html.='<td>'.$serie.''.$r_sol_cot_r_icon.$r_info_cot_cot_html.'</td>';
                                $html.='<td>';
                                    $html.='<select name="tecnico" id="tecnico" class="browser-default tecnicoselect form-control-bmz coun_tec_1">';
                                      
                                        foreach ($resultstec as $item) {
                                          $html.='<option value="'.$item->personalId.'">'.$item->tecnico.'</option>';
                                        }
                                       
                                     $html.='</select>';
                                     $html.='<select name="tecnico2" id="tecnico2" class="browser-default form-control-bmz coun_tec_2" style="display:none"><option value="0"></option>';
                                      
                                        foreach ($resultstec as $item) {
                                          $html.='<option value="'.$item->personalId.'">'.$item->tecnico.'</option>';
                                        }
                                       
                                     $html.='</select>';
                                     $html.='<select name="tecnico3" id="tecnico3" class="browser-default form-control-bmz coun_tec_3" style="display:none"><option value="0"></option>';
                                      
                                        foreach ($resultstec as $item) {
                                          $html.='<option value="'.$item->personalId.'">'.$item->tecnico.'</option>';
                                        }
                                       
                                     $html.='</select>';
                                     $html.='<select name="tecnico4" id="tecnico4" class="browser-default form-control-bmz coun_tec_4" style="display:none"><option value="0"></option>';
                                      
                                        foreach ($resultstec as $item) {
                                          $html.='<option value="'.$item->personalId.'">'.$item->tecnico.'</option>';
                                        }
                                       
                                     $html.='</select>';
                                    $html.='<input type="text" name="serietext" id="serietext" readonly style="display:none">';
                                $html.='</td>';
                                $html.='<td>'.$direccionequipo.'</td>';
                            $html.='</tr>';
                }
                
            }
        }
        //===========================================
            $unique = array();

            // Recorrer el array original y usar serialización para identificar valores únicos
            foreach ($fil_dir_row as $element) {
                $serialized = serialize($element);
                if (!array_key_exists($serialized, $unique)) {
                    $unique[$serialized] = $element;
                }
            }

            // Convertir el array de vuelta a índices numéricos
            $fil_dir_row_unique = array_values($unique);

            // Imprimir el resultado
            //print_r($fil_dir_row_unique);
                $fil_dir='<option value="xxx">Todas</option>';
            foreach ($fil_dir_row_unique as $itemd) {
                $fil_dir.='<option value="dir_'.$itemd['id'].'">'.$itemd['name'].'</option>';
            }
        //==========================================
        $html_g='<table class="table bordered" id="tableequipos">';
                $html_g.='<thead>';
                  $html_g.='<tr>';
                    $html_g.='<th style="width:7%">';
                      $html_g.='<input type="checkbox" id="serie_check_0">';
                      $html_g.='<label for="serie_check_0"></label>';
                    $html_g.='</th>';
                    $html_g.='<th colspan="2"></th>';
                    $html_g.='<th colspan="1"><label>Tecnicos</label><select id="coun_tec" class="browser-default form-control-bmz" onchange="coun_tec()"><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option></select> </th>';
                    $html_g.='<th colspan="1">';
                      $html_g.='<input type="checkbox" class="filled-in intoner_checked" id="intoner"><label for="intoner" class="intoner_label">Solicitar inventario de toner</label>';
                      $html_g.='<a class="b-btn b-btn-primary" href="'.base_url().'index.php/Cotizaciones/listaCotizacionesPorCliente/'.$cli.'" target="_blank" style="float: inline-end;margin-top: 17px;margin-left: 18px;">Ver Cotizaciones</a>';
                      $html_g.='<label style="width: 280px;display: block;float: inline-end;">Filtrar direccion<select class="form-control-bmz" id="filtro_dir" onchange="filtro_dir()">'.$fil_dir.'</select></label>';
                    $html_g.='</th>';
                  $html_g.='</tr>';
                $html_g.='</thead>';
                $html_g.='<tbody>';
                $html_g.=$html;
                $html_g.='</tbody>';
                
              $html_g.='</table>';
        echo $html_g;
    }
    function obtenerequiposcontratos(){
        $idcontrato = $this->input->post('id');
        $equiposresult=$this->ModeloCatalogos->obtenerequiposcontratos($idcontrato,$this->fechaactual);

        echo json_encode($equiposresult->result());
    }
    function obtenerequiposserie($id){
        $respuestarentae=$this->ModeloCatalogos->db2_getselectwheren('series_productos',array('serieId'=>$id));
        $serie='';
        foreach ($respuestarentae->result() as $item) {
            $serie=$item->serie;
        }
        return $serie;
    }
    function obtenerequiposserie2($id){
        $respuestarentae=$this->ModeloCatalogos->db3_getselectwheren('equipos',array('id'=>$id));
        $modelo='';
        foreach ($respuestarentae->result() as $item) {
            $modelo=$item->modelo;
        }
        return $modelo;
    }
    function obtenerservicios(){
        $tpoliza=$this->input->post('tpoliza');
        $resultoption=$this->ModeloCatalogos->getselectwheren('polizas_detalles',array('activo'=>1,'idPoliza'=>$tpoliza)); 
        $html='';
        foreach ($resultoption->result() as $item) {
            $equipomodelo=$this->obtenerequiposserie2($item->modelo);
            $html.='<option value="'.$item->id.'" 
            data-local="'.$item->precio_local.'"
            data-semi="'.$item->precio_semi.'"
            data-foraneo="'.$item->precio_foraneo.'"
            data-especial="'.$item->precio_especial.'"
            data-modelo="'.$item->modelo.'"
            class="modelo_poliza_'.$item->modelo.'"
            >'.$equipomodelo.''.$item->newmodelo.'</option>';
        }
        echo $html;
    }
    function get_savecliente(){
        $idconpol=$this->input->post('idconpol');
        $serviciorow=$this->input->post('serviciorow');
        //$tservicio=$this->input->post('tservicio');
        $tserviciomotivo=$this->input->post('tserviciomotivo');
        //$tpoliza = $this->input->post('tpoliza');
        //$ttipo = $this->input->post('ttipo');
        $cliente = $this->input->post('cliente');
        $fecha = $this->input->post('fecha');
        $hora = $this->input->post('hora');
        $horafin = $this->input->post('horafin');
        $hora_comida = $this->input->post('hora_comida');
        $horafin_comida = $this->input->post('horafin_comida');
        $zona = $this->input->post('zona');
        $priodidad = $this->input->post('priodidad');
        $priodidad2 = $this->input->post('priodidad2');
        $hora_cri_ser = $this->input->post('hora_cri_ser');
        $tecnico = $this->input->post('tecnico');
        $tecnico2 = $this->input->post('tecnico2');
        $tecnico3 = $this->input->post('tecnico3');
        $tecnico4 = $this->input->post('tecnico4');
        $servicios = $this->input->post('servicios');
        $ncequipos = '';
        $documentaccesorio = $this->input->post('documentaccesorio');
        
        $vconsumible=$this->input->post('vconsumible');
        $vrefaccion=$this->input->post('vrefaccion');
        $vaccesorio=$this->input->post('vaccesorio');
        $datoservicio = $this->input->post('datoservicio');

        //log_message('error', 'entra al controlador');
        $hora = $this->ModeloCatalogos->verificarhora($hora);
        $horafin = $this->ModeloCatalogos->verificarhora($horafin);
        
        $datainser = array(
            'clienteId'=>$cliente,
            'fecha'=>$fecha,
            'hora'=>$hora,
            'horafin'=>$horafin,
            'hora_comida'=>$hora_comida,
            'horafin_comida'=>$horafin_comida,
            'tecnico'=>$tecnico,
            'tecnico2'=>$tecnico2,
            'tecnico3'=>$tecnico3,
            'tecnico4'=>$tecnico4,
            'prioridad'=>$priodidad,
            'prioridad2'=>$priodidad2,
            'hora_cri_ser'=>$hora_cri_ser,
            'zonaId'=>$zona,
            'tserviciomotivo'=>$tserviciomotivo,
            'tiposervicio'=>1,
            'equipostext'=>$ncequipos,
            'documentaccesorio'=>$documentaccesorio,
            'creado_personal'=>$this->idpersonal
        );
        $asignacionId=$this->ModeloCatalogos->Insert('asignacion_ser_cliente_a',$datainser);

         $DATAvc = json_decode($vconsumible);
        for ($s=0;$s<count($DATAvc);$s++){
            $datavca =array(
                'asignacionId'=>$asignacionId,
                'cantidad'=>$DATAvc[$s]->cantidad,
                'consumible'=>$DATAvc[$s]->consumible,
                'costo'=>$DATAvc[$s]->precio,
                'tipo'=>3
            );
            $this->ModeloCatalogos->Insert('asignacion_ser_avc',$datavca);
        }
        $DATAvr = json_decode($vrefaccion);
        for ($c=0;$c<count($DATAvr);$c++){
            $datavcra =array(
                'asignacionId'=>$asignacionId,
                'cantidad'=>$DATAvr[$c]->cantidad,
                'refacciones'=>$DATAvr[$c]->refaccion,
                'costo'=>$DATAvr[$c]->precio,
                'tipo'=>3
            );
            $this->ModeloCatalogos->Insert('asignacion_ser_avr',$datavcra);
        }
        $DATAva = json_decode($vaccesorio);
        for ($x=0;$x<count($DATAva);$x++){
            $datavcrac =array(
                'asignacionId'=>$asignacionId,
                'cantidad'=>$DATAva[$x]->cantidad,
                'accessorio'=>$DATAva[$x]->accesorio,
                'costo'=>$DATAva[$x]->precio,
                'tipo'=>3
            );
            $this->ModeloCatalogos->Insert('asignacion_ser_ava',$datavcrac);
        }

        $DATAvs = json_decode($datoservicio);
        $DATAvsarray=array();
        for ($z=0;$z<count($DATAvs);$z++){
            $datavcrac =array(
                'asignacionId'=>$asignacionId,
                'tpoliza'=>$DATAvs[$z]->poliza,
                'tservicio_a'=>$DATAvs[$z]->equipos,
                'serie'=>$DATAvs[$z]->serie,
                'direccion'=>$DATAvs[$z]->direccion,
                'iddir'=>$DATAvs[$z]->iddir,
                'ttipo'=>$DATAvs[$z]->tipo,
                'tecnico'=>$tecnico,
                'tecnico2'=>$tecnico2,
                'tecnico3'=>$tecnico3,
                'tecnico4'=>$tecnico4  
            );
            $DATAvsarray[]=$datavcrac;
            //$this->ModeloCatalogos->Insert('asignacion_ser_cliente_a_d',$datavcrac);
        }
        if(count($DATAvs)>0){
            $this->ModeloCatalogos->insert_batch('asignacion_ser_cliente_a_d',$DATAvsarray);
        }
        //log_message('error',json_encode($servicios));
            $DATAser = json_decode($servicios);
            $dataeqarrays=array();
            $dataeqarrays2=array();
            for ($a=0;$a<count($DATAser);$a++){
                /*
                $datainfoequipo2=array(
                        'o_tipo'=>3,
                        'o_idser'=>$asignacionId,
                        'g_tipo'=>$DATAser[$a]->tipo,
                        'g_idser'=>$DATAser[$a]->servicio
                    );
                $dataeqarrays[]=$datainfoequipo2;
                */

                $datainfoequipo0=array(
                        'id_ser'=>$DATAser[$a]->servicio,
                        'id_tipo'=>$DATAser[$a]->tipo,
                        'id_ser_garantia'=>$asignacionId,
                        'id_tipo_garantia'=>3,
                        'personal'=>$this->idpersonal
                    );
                $dataeqarrays2[]=$datainfoequipo0;
            }
            /*
            if (count($DATAser)>0) {
                $this->ModeloCatalogos->db4_insert_batch('asignacion_ser_garantia',$dataeqarrays);
            }*/
            if (count($DATAser)>0) {
                //$this->ModeloCatalogos->db4_insert_batch('asignacion_ser_garantia',$dataeqarrays);
                $this->ModeloCatalogos->db4_insert_batch('polizascreadas_ext_servisios',$dataeqarrays2);
            }
    }
    function get_optenerequipospoliza($id){
        $respuestarentae=$this->ModeloCatalogos->db6_getselectwheren('polizas_detalles',array('id'=>$id));
        $equipo=0;
        foreach ($respuestarentae->result() as $item) {
            $equipo=$item->modelo;
        }
        $modeloequipo=$this->obtenerequiposserie2($equipo);
        return $modeloequipo;
    }
    function numerodepolizasasignadas($id){
        $resultp=$this->ModeloCatalogos->db6_getselectwheren('asignacion_ser_poliza_a',array('polizaId'=>$id));
        $total=0;
        foreach ($resultp->result() as $item) {
            $total++;
        }
        return $total;
    }
    function fechadeprimerapolizasasignadas($id){
        //$resultp=$this->ModeloCatalogos->getselectwheren('asignacion_ser_poliza_a',array('polizaId'=>$id));
        $strq = "SELECT ape.fecha FROM asignacion_ser_poliza_a as ap 
                inner JOIN asignacion_ser_poliza_a_e as ape on ape.asignacionId=ap.asignacionId
                WHERE ap.polizaId=".$id." ORDER BY `ape`.`fecha` ASC limit 1";
        $query = $this->db->query($strq);

        $fecha=$this->fechaactual;
        //log_message('error', 'fechaconsulta actual: '.$fecha);
        foreach ($query->result() as $item) {
            $fecha = $item->fecha;
            //log_message('error', 'fechaconsulta: '.$fecha);
        }
        return $fecha;
    }
    function obtenervigenciapolizavm($poliza,$equipo){
        $resultp=$this->ModeloCatalogos->db6_getselectwheren('polizasCreadas_has_detallesPoliza',array('id'=>$equipo));
        $idcotizacion=0;
        foreach ($resultp->result() as $item) {
            $vigencia_meses=$item->vigencia_meses;
            $vigencia_clicks=$item->vigencia_clicks;
        }
        /*
        $array = array(
            'vigencia_meses'=>$vigencia_meses,
            'vigencia_clicks'=>$vigencia_clicks
            );
        */
        return $vigencia_meses; 
    }
    function obtenervigenciapolizavc($poliza,$equipo){
        $resultp=$this->ModeloCatalogos->db6_getselectwheren('polizasCreadas_has_detallesPoliza',array('id'=>$equipo));
        $idcotizacion=0;
        foreach ($resultp->result() as $item) {
            $vigencia_meses=$item->vigencia_meses;
            $vigencia_clicks=$item->vigencia_clicks;
        }
        /*
        $array = array(
            'vigencia_meses'=>$vigencia_meses,
            'vigencia_clicks'=>$vigencia_clicks
            );
        */
        return $vigencia_clicks; 
    }
    function obtenerserviciosventas(){
        $serventa=$this->ModeloCatalogos->traeserviciosventas(0);
        $serventac=$this->ModeloCatalogos->traeserviciosventas(1);
        $html='<table class="table striped" id="tableventassolicitud">
                    <thead>
                        <tr>
                            <th>Fecha de venta</th>
                            <th>Vendedor</th>
                            <th>Cliente</th>
                            <th>Pre-factura</th>
                            <th>Acciones</th>
                            <th></th> 
                        </tr>
                    </thead>
                    <tbody>
                ';
                foreach ($serventa->result() as $item) {
            $html.='
                    <tr class="ventaservicio_'.$item->id.'_0_'.$item->idCliente.' ventaserviciocheck">
                        <td>'.$item->reg.'</td>
                        <td>'.$item->nombre.' '.$item->apellido_paterno.' '.$item->apellido_materno.'</td>
                        <td>'.$item->empresa.'</td>
                        <td>
                            <a class="btn-floating blue tooltipped" data-position="top" data-delay="50" data-tooltip="Prefactura" onclick="prefacturav('.$item->id.',0)" data-tooltip-id="8277bbe3-f84a-cafe-247c-e81127cceae6">
                                <i class="material-icons">assignment</i>
                            </a>
                        </td>
                        <td>
                            <a class="waves-effect btn-bmz deplegaracciones_'.$item->id.'_0_'.$item->idCliente.' " 
                            style="background: green;" 
                            onclick="deplegaracciones('.$item->id.',0,'.$item->idCliente.')">Solicitar</a></td>
                        <td>
                            <div class="acciones_ventas_'.$item->id.'_0 acciones_ventas" style="width: 345px;"></div>
                        </thd 
                    </tr>
                ';
                }
                foreach ($serventac->result() as $item) {
            $html.='
                    <tr class="ventaservicio_'.$item->combinadaId.'_1_'.$item->idCliente.' ventaserviciocheck">
                        <td>'.$item->reg.'</td>
                        <td>'.$item->nombre.' '.$item->apellido_paterno.' '.$item->apellido_materno.'</td>
                        <td>'.$item->empresa.'</td>
                        <td>
                            <a class="btn-floating blue tooltipped" data-position="top" data-delay="50" data-tooltip="Prefactura" onclick="prefacturav('.$item->combinadaId.',1)" data-tooltip-id="10cb80fe-9410-7876-8074-e0493b031368">
                                <i class="material-icons">assignment</i>
                            </a>
                        </td>
                        <td>
                            <a class="waves-effect btn-bmz deplegaracciones_'.$item->combinadaId.'_1_'.$item->idCliente.'" 
                            style="background: green;" 
                            onclick="deplegaracciones('.$item->combinadaId.',1,'.$item->idCliente.')">Solicitar</a></td>
                        <td>
                            <div class="acciones_ventas_'.$item->combinadaId.'_1 acciones_ventas"></div>
                        </thd 
                    </tr>
                ';
                }
                $html.='</tbody></table>';
        echo $html;
    }
    function obtenerserviciospolizas(){
        $serventa=$this->Configuraciones_model->serviciosdeventas_info('','',0);
        $html='<table class="table striped" id="tableventassolicitud">';
                    $html.='<thead>';
                        $html.='<tr>';
                            $html.='<th>Fecha de venta</th>';
                            $html.='<th>Vendedor</th>';
                            $html.='<th>Cliente</th>';
                            $html.='<th></th>';
                            $html.='<th>Acciones</th>';
                            $html.='<th></th> ';
                        $html.='</tr>';
                    $html.='</thead>';
                    $html.='<tbody>';

                foreach ($serventa->result() as $item) {
                    $viewcont_cant=$item->viewcont_cant;
                    $ser_rest=$item->ser_rest;
                    if($ser_rest>0){
                        $viewcont_cant=$ser_rest;
                    }
                    if ($item->prasignaciontipo==2) {
                        if($item->viewcont==1){
                            /*
                            $row_servicios=$this->Configuraciones_model->getserviospolizarealizados($item->idventa);
                            //log_message('error',json_encode($row_servicios));
                            if($row_servicios->num_rows()>0){
                                $count_row_servicios=$row_servicios->num_rows();
                            }else{
                                $count_row_servicios=0;
                            }
                            */
                            /*
                            $sql_num_ser_add="SELECT * FROM `polizascreadas_ext_ser_add` WHERE idpoliza='$item->idventa' AND activo=1";
                            $query_num_ser_add = $this->db->query($sql_num_ser_add);
                            if($query_num_ser_add->num_rows()>0){
                                $count_row_servicios=$count_row_servicios+$query_num_ser_add->num_rows();
                            }*/
                            $rows_ser_eve = $this->Polizas_model->servicioseventocliente($item->idCliente,$item->idventa);
                            $count_row_servicios=0;
                            foreach ($rows_ser_eve->result() as $itemsev1) {

                                  if($itemsev1->status==2){
                                    $count_row_servicios=$count_row_servicios+1;
                                  }elseif($itemsev1->status<2){
                                    //$servicios_f++;
                                  }
                              }
                            $rows_ser_pol = $this->Polizas_model->serviciospolizacliente($item->idCliente,$item->idventa,$item->idventa);
                            foreach ($rows_ser_pol->result() as $itemsev) {
                                  if($itemsev->status==2){
                                    $count_row_servicios=$count_row_servicios+1;
                                  }elseif($itemsev->status<2){
                                    //$servicios_f++;
                                  }
                            }
                            $viewcont='('.$count_row_servicios.' /'.$viewcont_cant.') <a href="'.base_url().'PolizasCreadas/servicios/'.$item->idventa.'" target="_blank"><i class="fas fa-retweet" title="Poliza recurente x"></i></a>';
                            if($count_row_servicios >= $viewcont_cant){
                                $deplegaraccionesp = ' onclick="limitedeservicios()" ';
                            }else{
                                $deplegaraccionesp = ' onclick="deplegaraccionesp('.$item->idventa.')" ';
                            }
                        }else{
                            $viewcont='';
                            $deplegaraccionesp = ' onclick="deplegaraccionesp('.$item->idventa.')" ';
                        }
                        $html.='<tr class="ventaservicio_'.$item->idventa.'_0_'.$item->idCliente.' ventaserviciocheck">';
                            $html.='<td>'.$item->reg.'</td>';
                            $html.='<td>'.$item->vendedor.'</td>';
                            $html.='<td>'.$item->empresa.'</td>';
                            $html.='<td>'.$item->idventa.$viewcont.'</td>';
                            $html.='<td><a class="waves-effect btn-bmz deplegaracciones_'.$item->idventa.'_0_'.$item->idCliente.'" style="background: green;" '.$deplegaraccionesp.'>Solicitar</a></td>';
                            $html.='<td><div class="acciones_poliza acciones_poliza_'.$item->idventa.'_0 acciones_ventas" style="width: 345px;"></div></td>';
                        $html.='</tr>';
                    }
                }
                
                $html.='</tbody></table>';
        echo $html;
    }
    function get_savesventa(){
        $serviciorow=$this->input->post('serviciorow');
        //$tservicio=$this->input->post('tservicio');
        //$tserviciomotivo=$this->input->post('tserviciomotivo');
        //$tpoliza = $this->input->post('tpoliza');
        //$ttipo = $this->input->post('ttipo');
        $ventaId = $this->input->post('ventaId');
        $cliente = $this->input->post('cliente');
        $tipov = $this->input->post('tipov');
        $fecha = $this->input->post('fecha');
        $hora = $this->input->post('hora');
        $horafin = $this->input->post('horafin');
        $hora_comida = $this->input->post('hora_comida');
        $horafin_comida = $this->input->post('horafin_comida');
        $zona = $this->input->post('zona');
        $priodidad = $this->input->post('priodidad');
        $prioridad2 = $this->input->post('prioridad2');
        $hora_cri_ser = $this->input->post('hora_cri_ser');
        $tecnico = $this->input->post('tecnico');
        $servicios = $this->input->post('servicios');
        $ncequipos = '';
        $documentaccesorio = $this->input->post('documentaccesorio');
        
        $vconsumible=$this->input->post('vconsumible');
        $vrefaccion=$this->input->post('vrefaccion');
        $datoservicio = $this->input->post('datoservicio');
        $vaccesorio = $this->input->post('vaccesorio');
        //================
            $resultventa=$this->ModeloCatalogos->getselectwheren('ventas',array('id'=>$ventaId));
            foreach ($resultventa->result() as $item) {
                $servicio_poliza    = $item->servicio_poliza;
                $servicio_servicio  = $item->servicio_servicio;
                $servicio_direccion = $item->servicio_direccion;
                $servicio_tipo      = $item->servicio_tipo;
                $tserviciomotivo    = $item->servicio_motivo;
            }
        //================
        //log_message('error', 'entra al controlador');
        $datainser = array(
            'ventaId'=>$ventaId,
            'clienteId'=>$cliente,
            'tipov'=>$tipov,
            'fecha'=>$fecha,
            'hora'=>$hora,
            'horafin'=>$horafin,
            'hora_comida'=>$hora_comida,
            'horafin_comida'=>$horafin_comida,
            'tecnico'=>$tecnico,
            'prioridad'=>$priodidad,
            'prioridad2'=>$prioridad2,
            'hora_cri_ser'=>$hora_cri_ser,
            'zonaId'=>$zona,
            'tserviciomotivo'=>$tserviciomotivo,
            'tiposervicio'=>1,
            'equipostext'=>$ncequipos,
            'documentaccesorio'=>$documentaccesorio,
            'creado_personal'=>$this->idpersonal
        );
        $asignacionId=$this->ModeloCatalogos->Insert('asignacion_ser_venta_a',$datainser);

         $DATAvc = json_decode($vconsumible);
        for ($s=0;$s<count($DATAvc);$s++){
            $datavca =array(
                'asignacionId'=>$asignacionId,
                'cantidad'=>$DATAvc[$s]->cantidad,
                'consumible'=>$DATAvc[$s]->consumible,
                'costo'=>$DATAvc[$s]->precio,
                'tipo'=>4
            );
            $this->ModeloCatalogos->Insert('asignacion_ser_avc',$datavca);
        }
        $DATAvr = json_decode($vrefaccion);
        for ($c=0;$c<count($DATAvr);$c++){
            $datavcra =array(
                'asignacionId'=>$asignacionId,
                'cantidad'=>$DATAvr[$c]->cantidad,
                'refacciones'=>$DATAvr[$c]->refaccion,
                'costo'=>$DATAvr[$c]->precio,
                'tipo'=>4
            );
            $this->ModeloCatalogos->Insert('asignacion_ser_avr',$datavcra);
        }
        $DATAva = json_decode($vaccesorio);
        for ($x=0;$x<count($DATAva);$x++){
            $datavcrac =array(
                'asignacionId'=>$asignacionId,
                'cantidad'=>$DATAva[$x]->cantidad,
                'accessorio'=>$DATAva[$x]->accesorio,
                'costo'=>$DATAva[$x]->precio,
                'tipo'=>4
            );
            $this->ModeloCatalogos->Insert('asignacion_ser_ava',$datavcrac);
        }
        
        /*
        $DATAvs = json_decode($datoservicio);
        for ($z=0;$z<count($DATAvs);$z++){
            $datavcrac =array(
                'asignacionId'=>$asignacionId,
                'tpoliza'=>$DATAvs[$z]->poliza,
                'tservicio'=>$DATAvs[$z]->equipos,
                'serie'=>$DATAvs[$z]->serie,
                'direccion'=>$DATAvs[$z]->direccion,
                'ttipo'=>$DATAvs[$z]->tipo,    
            );
          */$infoseries='';
            $serconsu=$this->ModeloServicio->obtenerseriesconsumibles($ventaId);
            foreach ($serconsu->result() as $itemsc) {
                $infoseries.=$itemsc->serie.' ';
            }
            
            $datavcrac =array(
                'asignacionId'=>$asignacionId,
                'tpoliza'=>$servicio_poliza,
                'tservicio_a'=>$servicio_servicio,
                'serie'=>$infoseries,
                'direccion'=>$servicio_direccion,
                'ttipo'=>$servicio_tipo,    
            );
            $this->ModeloCatalogos->Insert('asignacion_ser_venta_a_d',$datavcrac);
        //}
            //log_message('error',json_encode($servicios));
            $DATAser = json_decode($servicios);
            $dataeqarrays=array();
            for ($a=0;$a<count($DATAser);$a++){
                $datainfoequipo2=array(
                        'o_tipo'=>4,
                        'o_idser'=>$asignacionId,
                        'g_tipo'=>$DATAser[$a]->tipo,
                        'g_idser'=>$DATAser[$a]->servicio
                    );
                $dataeqarrays[]=$datainfoequipo2;
            }

            if (count($DATAser)>0) {
                $this->ModeloCatalogos->db4_insert_batch('asignacion_ser_garantia',$dataeqarrays);
            }
    }
    function getdireccionequipos($equipo,$serie){
        $resultdir=$this->ModeloCatalogos->db2_getselectwheren('contrato_equipo_direccion',array('equiposrow'=>$equipo,'serieId'=>$serie));
        $iddireccionget='g_0';
        $direccion='';
        foreach ($resultdir->result() as $itemdir) {
            if($itemdir->direccion!=''){
                $iddireccionget=$itemdir->direccion;
            }
        }
        if($iddireccionget!='g_0'){
            $tipodireccion=explode('_',$iddireccionget);

            $tipodir = $tipodireccion[0];
            $iddir   = $tipodireccion[1];
            if ($tipodir=='g') {
                $resultdirc=$this->ModeloCatalogos->db2_getselectwheren('clientes_direccion',array('idclientedirecc'=>$iddir));
                foreach ($resultdirc->result() as $item) {
                    $direccion=$item->direccion;
                }
            }
            if ($tipodir=='f') {
                $resultdirc=$this->ModeloCatalogos->db2_getselectwheren('cliente_has_datos_fiscales',array('id'=>$iddir));
                foreach ($resultdirc->result() as $item) {
                    $direccion='Razon social: '.$item->razon_social.' Colonia:'.$item->colonia.' Calle:'.$item->calle.' Numero:'.$item->num_ext;
                }
            }
        }
        return $direccion;
    }
    function getdireccionequiposiddir($equipo,$serie){
        $resultdir=$this->ModeloCatalogos->db2_getselectwheren('contrato_equipo_direccion',array('equiposrow'=>$equipo,'serieId'=>$serie));
        $iddireccionget='g_0';
        $direccion='';
        foreach ($resultdir->result() as $itemdir) {
            $iddireccionget=$itemdir->direccion;
        }
        
        return $iddireccionget;
    }
    function informacionservicioventa(){
        $idventa = $this->input->post('idventa');
        $result=$this->ModeloCatalogos->ventaservicio($idventa);
        $html='';
        $arrayservicios=array();
        foreach ($result->result() as $item) {
            $arrayservicios[]=array(
                                    'poliza'=>$item->nombre,
                                    'servicio'=>$item->modelo.' '.$item->newmodelo,
                                    'direccon'=>$item->servicio_direccion,
                                    'motivo'=>$item->servicio_motivo
                                    );
            $html.='<div class="row">';
                $html.='<div class="col m3">';
                    $html.='Póliza: '.$item->nombre;
                $html.='</div>';
                $html.='<div class="col m3">';
                    $html.='Servicio: '.$item->modelo.' '.$item->newmodelo;
                $html.='</div>';
                $html.='<div class="col m3">';
                    $html.='Dirección: '.$item->servicio_direccion;
                $html.='</div>';
                $html.='<div class="col m3">';
                    $html.='Motivo: '.$item->servicio_motivo;
                $html.='</div>';
            $html.='</div>';
        }
        echo json_encode($arrayservicios);
    }
    function serviciosgenerados($contrato,$fecha){
        $result=$this->ModeloCatalogos->serviciosgenerados($contrato,$fecha);
        $servicios=0;
        foreach ($result->result() as $item) {
            $servicios++;
        }
        return $servicios;
    }
    function fechasagenda(){
        $params = $this->input->post();
        $idcontrato= $params['idcontrato'];
        $idequipo= $params['idequipo'];
        $result=$this->ModeloCatalogos->viewagenda($idcontrato,$idequipo,$this->fechaactual);
        $html='<table class="table" id="table-agenda">
                <thead>
                    <tr>
                        <th>Fechas Agendadas</th>
                    </tr>
                </thead>
                <tbody>
              ';
            foreach ($result->result() as $item) {
                $html.='<tr>
                            <td><a href="'.base_url().'Listaservicios?servicio=1&fecha='.$item->fecha.'&idcliente='.$item->id.'&cliente='.$item->empresa.'" target="_blank">'.$item->fecha.'</a></td>
                        </tr>';
            }

        $html.='</tbody></table>';
        echo $html;
    }
    function verificarservicios(){
        $params = $this->input->post();
        $idcontrato= $params['idcontrato'];
        $serie= $params['serie'];
        $ideq = $params['ideq'];

        $fechaw = strtotime ( '+ 25 day' , strtotime ( $this->fechaactual ) ) ;
            $fechaw = date ( 'Y-m-d' , $fechaw );
        

        $result=$this->Rentas_model->nextservicio($idcontrato,$serie,$fechaw);
        $pasaser=1;
        $fechanext=$this->fechaactual;
        $speriocidad=0;
        foreach ($result->result() as $item) {
            $fechanext=$item->fechanext;
            $speriocidad=$item->speriocidad;
        }
        if($fechanext>$this->fechaactual){
            $pasaser=0;
        }
        //====================================================
        /*
        $strq="SELECT eqcons.id,eqcons.idrentas,eqcons.idequipo,eqcons.rowequipo,eqcons.cantidad,eqcons.id_consumibles,cf.idcontratofolio,cf.foliotext,cf.fechagenera,cf.status,
                DATEDIFF(NOW(), cf.fechagenera) AS dias_diferencia,
                areq.sol_consu_cant,areq.sol_consu_periodo,
                IF(areq.sol_consu_periodo > 0, areq.sol_consu_periodo * 30, 0) AS periodo_dias
                FROM rentas_has_detallesequipos_consumible AS eqcons 
                LEFT JOIN contrato_folio as cf on cf.idrelacion=eqcons.id
                INNER JOIN alta_rentas_equipos as areq on areq.id_equipo=eqcons.rowequipo
                WHERE eqcons.rowequipo='$ideq' AND cf.idcontratofolio>0
                ORDER BY cf.idcontratofolio DESC LIMIT 1";
        $query_c = $this->db->query($strq);
        $consu_alert=0;
        $consu_cant=0;
        foreach ($query_c->result() as $item) {
            $periodo_dias=$item->periodo_dias;
            if($periodo_dias>0){
                $periodo_dias=$periodo_dias-10;
               if($item->dias_diferencia>$periodo_dias){
                $consu_alert=1;
                $consu_cant=$item->sol_consu_cant;
               }
            }
            
        }
        */
        $v_ufp=$this->Rentas_model->verificar_ultimifolios_periodo($ideq);
        $consu_alert = $v_ufp['consu_alert'];
        $consu_cant = $v_ufp['consu_cant'];



        $arraydatos = array(
                            'pasaser'=>$pasaser,
                            'fechanext'=>$fechanext,
                            'speriocidad'=>$speriocidad,
                            'consu_alert'=>$consu_alert,
                            'consu_cant'=>$consu_cant
                            );
        echo json_encode($arraydatos);
    }
    
    function verificarventaserivicio(){
        $params = $this->input->post();
        $idventa= $params['idventa'];
        $tipo= $params['tipo'];

        $mes_actual=date('m',strtotime($this->fechaactual));
        $ano_actual=date('Y',strtotime($this->fechaactual));

        $result=$this->Rentas_model->datosventaser($idventa,$tipo);
        $pasa=1;
        foreach ($result->result() as $item) {
            $idCliente  = $item->idCliente;
            $idserie    = $item->idserie;
            
            $resultser=$this->Rentas_model->datosventaservis($idCliente,$idserie);
            foreach ($resultser->result() as $itemser) {
                $mes_reg=date('m',strtotime($itemser->fecha));
                $ano_reg=date('Y',strtotime($itemser->fecha));

                if($ano_actual==$ano_reg){
                    if($mes_actual==$mes_reg){
                        $pasa=0;
                    }
                }
            }

        }
        echo $pasa;
    }
    function verificardirecciones($idasignacion){
        $resasig=$this->ModeloCatalogos->getselectwheren('asignacion_ser_contrato_a',array('asignacionId'=>$idasignacion));
        $resasig=$resasig->row();
        unset($resasig->asignacionId);

        $strq="SELECT asige.asignacionIde,asige.asignacionId,asige.idequipo,asige.serieId,coned.direccion 
                FROM asignacion_ser_contrato_a_e as asige 
                LEFT JOIN contrato_equipo_direccion as coned on coned.equiposrow=asige.idequipo AND coned.serieId=asige.serieId
                WHERE asige.asignacionId='$idasignacion'  
                ORDER BY `coned`.`direccion`  DESC";
        $query = $this->db->query($strq);
        $result_row=$query->row();
        $direccion = $result_row->direccion;
        $idasignacion_new=$idasignacion;
        foreach ($query->result() as $item) {
            if($direccion==$item->direccion){
                $direccion=$item->direccion;
                $this->ModeloCatalogos->updateCatalogo('asignacion_ser_contrato_a_e',array('asignacionId'=>$idasignacion_new),array('asignacionIde'=>$item->asignacionIde));
            }else{
                $direccion=$item->direccion;
                $idasignacion_new=$this->ModeloCatalogos->Insert('asignacion_ser_contrato_a',$resasig);
                $this->ModeloCatalogos->updateCatalogo('asignacion_ser_contrato_a_e',array('asignacionId'=>$idasignacion_new),array('asignacionIde'=>$item->asignacionIde));
            }
        }

    }
    function obtenerultimosservicios($cli,$tipo,$serie=''){
        //echo $serie;
        $params=$this->input->post();
        //$cli = $params['cli'];
        //$tipo = $params['tipo'];
            $fechamenos10 = date("Y-m-d",strtotime($this->fechaactual."- 10 day")); 
        //==================================================
            $fecha10 = date("Y-m-d",strtotime($this->fechaactual."- 30 day")); 
            $fecha1 = date("Y-m-d",strtotime($this->fechaactual)); 
            if($serie!=''){
                $where3=" and clid.serie='$serie' ";
                $where1=" and serp.serie='$serie' ";
                $where2=" and apole.serie='$serie' ";
                $where4=" and acld.serie='$serie' ";
            }else{
                $where1='';
                $where2='';
                $where3='';
                $where4='';
            }
            $strq3="SELECT cli.asignacionId,pol.nombre as poliza,per.nombre as nom_per, per.apellido_paterno, per.apellido_materno, cli.fecha, GROUP_CONCAT(clid.serie SEPARATOR ' ') as series
                    FROM asignacion_ser_cliente_a as cli
                    INNER JOIN asignacion_ser_cliente_a_d as clid on clid.asignacionId=cli.asignacionId
                    INNER JOIN polizas as pol on pol.id=clid.tpoliza
                    LEFT JOIN personal as per on per.personalId=cli.per_fin
                    WHERE cli.clienteId='$cli' AND cli.fecha>='$fecha10' AND cli.fecha<='$fecha1' AND clid.status=2 AND clid.nr=0 $where3
                    GROUP BY cli.asignacionId";
                $query3 = $this->db->query($strq3);
            $strq1="SELECT conser.asignacionId, pol.nombre as poliza,per.nombre as nom_per, per.apellido_paterno, per.apellido_materno, consera.fecha, GROUP_CONCAT(serp.serie SEPARATOR ' ') as series
                    FROM asignacion_ser_contrato_a as conser
                    INNER JOIN contrato as con on con.idcontrato=conser.contratoId
                    INNER JOIN rentas as ren on ren.id=con.idRenta
                    INNER JOIN asignacion_ser_contrato_a_e as consera on consera.asignacionId=conser.asignacionId
                    INNER JOIN polizas as pol on pol.id=conser.tpoliza
                    LEFT JOIN personal as per on per.personalId=consera.per_fin
                    LEFT JOIN series_productos as serp on serp.serieId=consera.serieId
                    WHERE ren.idCliente='$cli' AND consera.fecha>='$fecha10' AND consera.fecha<='$fecha1' AND consera.status=2 AND consera.nr=0 $where1
                    GROUP BY conser.asignacionId";
                $query1 = $this->db->query($strq1);
            $strq2="SELECT apol.asignacionId,pocde.nombre as poliza,per.nombre as nom_per, per.apellido_paterno, per.apellido_materno, apole.fecha, GROUP_CONCAT(apole.serie SEPARATOR ' ') as series
                    FROM asignacion_ser_poliza_a as apol
                    INNER JOIN asignacion_ser_poliza_a_e as apole on apole.asignacionId=apol.asignacionId
                    INNER JOIN polizasCreadas as polc on polc.id=apol.polizaId
                    INNER JOIN polizasCreadas_has_detallesPoliza as pocde ON pocde.id=apole.idequipo
                    LEFT JOIN personal as per on per.personalId=apole.per_fin
                    WHERE polc.idCliente='$cli' AND apole.fecha>='$fecha10' AND apole.fecha<='$fecha1' AND apole.status=2 AND apole.nr=0 $where2
                    GROUP BY apol.asignacionId";
                $query2 = $this->db->query($strq2);

            $strq4="SELECT acl.asignacionId, pol.nombre as poliza, per.nombre as nom_per, per.apellido_paterno, per.apellido_materno, acl.fecha,acld.serie ,acld.serie as series
                    FROM asignacion_ser_venta_a as acl
                    JOIN `clientes` `cli` ON `cli`.`id`=`acl`.`clienteId`
                    JOIN `rutas` `zo` ON `zo`.`id`=`acl`.`zonaId`
                    JOIN `asignacion_ser_venta_a_d` `acld` ON `acld`.`asignacionId` = `acl`.`asignacionId`
                    left JOIN `servicio_evento` `sereve` ON `sereve`.`id` = `acld`.`tservicio`
                    left join prefactura as pre on pre.ventaId=acl.ventaId and pre.tipo=0
                    left JOIN polizas as pol on pol.id=acld.tpoliza
                    left join cliente_datoscontacto as clidc on clidc.datosId=pre.contactoId
                    left join cliente_has_persona_contacto as clidca on clidca.id=pre.contactoId
                    LEFT JOIN personal as per on per.personalId=acl.per_fin
                    INNER JOIN ventas as ven on ven.id=acl.ventaId
                    where ven.idCliente='$cli' AND acl.fecha>='$fecha10' AND acl.fecha<='$fecha1' AND acl.status=2 $where4";
                $query4 = $this->db->query($strq4);
        //======================================================================
        if($tipo==0){
            $tot3=$query3->num_rows();
            $tot1=$query1->num_rows();
            $tot2=$query2->num_rows();
            $tot4=$query4->num_rows();
            $total =$tot1+$tot2+$tot3+$tot4;

            echo $total; 
        }
        if($tipo==1){
            $html='';
            if(isset($params['tasign'])) {
                $html.=$this->seriesagregadas($params['idcpv'],$params['tasign']);
            }
            foreach ($query3->result() as $item) {
                //log_message('error',' fecha ('.date("Y-m-d",strtotime($item->fecha)).') > fechamenos10 ('.date("Y-m-d",strtotime($fechamenos10)).') ');
                if(date("Y-m-d",strtotime($item->fecha))>date("Y-m-d",strtotime($fechamenos10)) ){
                    $m_a_dies_b='';
                    $m_a_dies_b_onclik='';
                }else{
                    $m_a_dies_b=' disabled ';
                    $m_a_dies_b_onclik=' onclick="desbloquearservicios()" ';
                }
                $check='<input type="checkbox" class="filled-in servicios_ckeck '.$m_a_dies_b.'" id="servicios_ckeck_3_'.$item->asignacionId.'" data-ser="'.$item->asignacionId.'" data-sertipo="3" '.$m_a_dies_b.'>';
                $check.='<label for="servicios_ckeck_3_'.$item->asignacionId.'" '.$m_a_dies_b_onclik.' class="servicios_ckeck_label '.$m_a_dies_b.'"></label>';
                $html.='<tr class="view_series '.$item->series.'"><td>'.$item->asignacionId.'</td><td>'.$item->poliza.'</td><td>'.$item->series.'</td><td>'.$item->nom_per.' '.$item->apellido_paterno.' '.$item->apellido_materno.'</td><td>'.$item->fecha.'</td><td>'.$check.'</td></tr>';
            }
            foreach ($query1->result() as $item) {
                if(date("Y-m-d",strtotime($item->fecha))>date("Y-m-d",strtotime($fechamenos10)) ){
                    $m_a_dies_b='';
                    $m_a_dies_b_onclik='';
                }else{
                    $m_a_dies_b=' disabled ';
                    $m_a_dies_b_onclik=' onclick="desbloquearservicios()" ';
                }
                $check='<input type="checkbox" class="filled-in servicios_ckeck '.$m_a_dies_b.'" id="servicios_ckeck_3_'.$item->asignacionId.'" data-ser="'.$item->asignacionId.'" data-sertipo="1" '.$m_a_dies_b.'>';
                $check.='<label for="servicios_ckeck_3_'.$item->asignacionId.'" '.$m_a_dies_b_onclik.' class="servicios_ckeck_label '.$m_a_dies_b.'"></label>';
                $html.='<tr class="view_series '.$item->series.'"><td>'.$item->asignacionId.'</td><td>'.$item->poliza.'</td><td>'.$item->series.'</td><td>'.$item->nom_per.' '.$item->apellido_paterno.' '.$item->apellido_materno.'</td><td>'.$item->fecha.'</td><td>'.$check.'</td></tr>';
            }
            foreach ($query2->result() as $item) {
                if(date("Y-m-d",strtotime($item->fecha))>date("Y-m-d",strtotime($fechamenos10)) ){
                    $m_a_dies_b='';
                    $m_a_dies_b_onclik='';
                }else{
                    $m_a_dies_b=' disabled ';
                    $m_a_dies_b_onclik=' onclick="desbloquearservicios()" ';
                }
                $check='<input type="checkbox" class="filled-in servicios_ckeck '.$m_a_dies_b.'" id="servicios_ckeck_3_'.$item->asignacionId.'" data-ser="'.$item->asignacionId.'" data-sertipo="2" '.$m_a_dies_b.' >';
                $check.='<label for="servicios_ckeck_3_'.$item->asignacionId.'" '.$m_a_dies_b_onclik.' class="servicios_ckeck_label '.$m_a_dies_b.'"></label>';

                $html.='<tr class="view_series '.$item->series.'"><td>'.$item->asignacionId.'</td><td>'.$item->poliza.'</td><td>'.$item->series.'</td><td>'.$item->nom_per.' '.$item->apellido_paterno.' '.$item->apellido_materno.'</td><td>'.$item->fecha.'</td><td>'.$check.'</td></tr>';
            }
            foreach ($query4->result() as $item) {
                if(date("Y-m-d",strtotime($item->fecha))>date("Y-m-d",strtotime($fechamenos10)) ){
                    $m_a_dies_b='';
                    $m_a_dies_b_onclik='';
                }else{
                    $m_a_dies_b=' disabled ';
                    $m_a_dies_b_onclik=' onclick="desbloquearservicios()" ';
                }
                $check='<input type="checkbox" class="filled-in servicios_ckeck '.$m_a_dies_b.'" id="servicios_ckeck_3_'.$item->asignacionId.'" data-ser="'.$item->asignacionId.'" data-sertipo="4" '.$m_a_dies_b.' >';
                $check.='<label for="servicios_ckeck_3_'.$item->asignacionId.'" '.$m_a_dies_b_onclik.' class="servicios_ckeck_label '.$m_a_dies_b.'"></label>';

                $html.='<tr class="view_series '.$item->series.'"><td>'.$item->asignacionId.'</td><td>'.$item->poliza.'</td><td>'.$item->series.'</td><td>'.$item->nom_per.' '.$item->apellido_paterno.' '.$item->apellido_materno.'</td><td>'.$item->fecha.'</td><td>'.$check.'</td></tr>';
            }
            echo $html;
        }
        if($tipo==3){
            $tot1=$query1->num_rows();
            $tot2=$query2->num_rows();
            $tot3=$query3->num_rows();
            $tot4=$query4->num_rows();
            $total =$tot1+$tot2+$tot3+$tot4;

            return $total; 
        }
    }
    function seriesagregadas($ids,$tipo){
        $series_array=[];
        //log_message('error','$topo : '.$tipo);
        if($tipo==1){
            $respuestarentae=$this->ModeloCatalogos->db10_getselectwheren('rentas_has_detallesEquipos',array('idRenta'=>$ids));
            foreach ($respuestarentae->result() as $item) {
                $respuestarentaess=$this->ModeloCatalogos->db2_getselectwheren('asignacion_series_r_equipos',array('id_equipo'=>$item->id,'activo'=>1));
                    $modeloe=$item->modelo;
                foreach ($respuestarentaess->result() as $itemss) {
                    $serie=$this->obtenerequiposserie($itemss->serieId);
                    //log_message('error',$serie);
                    $series_array[]=$serie;
                }
                
            }
        }
        if($tipo==2){
            //=====================================================
            $rowtr=0;
                $respuestarentae=$this->ModeloCatalogos->db10_getselectwheren('polizasCreadas_has_detallesPoliza',array('idPoliza'=>$ids,'suspender'=>0));
                foreach ($respuestarentae->result() as $item) {
                    $direccionequipo=$item->direccionservicio;
                    $comeninfo=$item->comeninfo;
                    $respuestarentaess=$this->ModeloCatalogos->db10_getselectwheren('polizasCreadas_has_detallesPoliza_series',array('iddetalle'=>$item->id));
                    $rowseries=0;
                    $modeloeq=$this->obtenerequiposserie2($item->idEquipo);
                    foreach ($respuestarentaess->result() as $itemss) {
                        $series_array[]=$itemss->serie;
                        $rowseries=1;
                    }
                    if ($rowseries==0) {
                        $series_array[]=$item->serie;
                    }
                }
                //==================================================================
                    $sql_0="SELECT vc.equipo,vc.consumibles,vc.refacciones,vc.poliza 
                            FROM polizasCreadas as pol
                            INNER JOIN ventacombinada as vc on vc.poliza=pol.id
                            WHERE pol.id='$ids' AND pol.combinada=1";
                            $query_0 = $this->db->query($sql_0);
                    foreach ($query_0->result() as $item_0) {
                        $sql_1="SELECT ser.serie FROM ventas_has_detallesConsumibles as vdc INNER JOIN series_productos as ser ON ser.serieId=vdc.idserie WHERE vdc.idVentas='$item_0->equipo'";
                        $query_1 = $this->db->query($sql_1);
                        foreach ($query_1->result() as $item_1) {
                            $series_array[]=$item_1->serie;
                        }
                        $sql_2="SELECT ser.serie FROM ventas_has_detallesEquipos as vdeq INNER JOIN asignacion_series_equipos as ase on ase.id_equipo=vdeq.id INNER JOIN series_productos as ser on ser.serieId=ase.serieId WHERE vdeq.idVenta='$item_0->equipo'";
                        $query_2 = $this->db->query($sql_2);
                        foreach ($query_2->result() as $item_2) {
                            $series_array[]=$item_2->serie;
                        }
                        $sql_3="SELECT * FROM ventas_has_detallesRefacciones WHERE idVentas='$item_0->equipo'";
                        $query_3 = $this->db->query($sql_3);
                        foreach ($query_3->result() as $item_3) {
                            $series_array[]=$item_3->serie;
                        }
                    }
                //==================================================================
            //=====================================================
        }
        if($tipo==4){
            $sql_1="SELECT ser.serie FROM ventas_has_detallesConsumibles as vdc INNER JOIN series_productos as ser ON ser.serieId=vdc.idserie WHERE vdc.idVentas='$ids'";
            $query_1 = $this->db->query($sql_1);
            foreach ($query_1->result() as $item_1) {
                $series_array[]=$item_1->serie;
            }
            $sql_2="SELECT ser.serie FROM ventas_has_detallesEquipos as vdeq INNER JOIN asignacion_series_equipos as ase on ase.id_equipo=vdeq.id INNER JOIN series_productos as ser on ser.serieId=ase.serieId WHERE vdeq.idVenta='$ids'";
            $query_2 = $this->db->query($sql_2);
            foreach ($query_2->result() as $item_2) {
                $series_array[]=$item_2->serie;
            }
            $sql_3="SELECT * FROM `ventas_has_detallesRefacciones` WHERE idVentas='$ids'";
            $query_3 = $this->db->query($sql_3);
            foreach ($query_3->result() as $item_3) {
                $series_array[]=$item_3->serie;
            }
        }
        return '<div class="seriesagregadas" data-series="'.implode(",", $series_array).'"></div>';
    }
    
}

?>