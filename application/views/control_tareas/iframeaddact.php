<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicons-->
    <link rel="icon" href="<?php echo base_url().'app-assets/images/favicon/favicon_kyocera.png' ?>" sizes="32x32">
    <link rel="apple-touch-icon" href="<?php echo base_url().'app-assets/images/favicon/favicon_kyocera.png' ?>">
    <title>Agregar actividad</title>
    <link href="<?php echo base_url(); ?>public/plugins/note/bootstrap.min.css" rel="stylesheet">
    <script src="<?php echo base_url(); ?>public/plugins/note/jquery-3.5.1.min.js"></script>
    <script src="<?php echo base_url(); ?>public/plugins/note/bootstrap.min.js"></script>
    <!-- include summernote css/js -->
    <link href="<?php echo base_url(); ?>public/plugins/note/summernote.min.css" rel="stylesheet">
    <script src="<?php echo base_url(); ?>public/plugins/note/summernote.min.js"></script>

    <link href="<?php echo base_url(); ?>public/css/fontawesome/css/all.css" type="text/css" rel="stylesheet">
    <script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/fileinput/fileinput_bos.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/plugins/fileinput/fileinput_bos.css">

    <script type="text/javascript" src="<?php echo base_url(); ?>public/js/control_tareas_iframe.js?v=<?php echo date('YmdGis');?>" ></script>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/plugins/toastr/toastr.css">
    <script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/toastr/toastr.min.js" ></script>
  </head>
  <body>
    <input type="hidden" id="base_url" value="<?php echo base_url();?>">
    <input type="hidden" id="viewtipo" value="<?php echo $viewtipo;?>">
    <div class="container">
      <div class="row">
        <div class="col-xs-10 col-sm-10 col-md-10 ">
          <label>Personal</label>
          <select class="form-control" id="personal">
               <?php 
                foreach ($res_personal->result() as $item) {
                  echo '<option value="'.$item->personalId.'">'.$item->nombre.' '.$item->apellido_paterno.' '.$item->apellido_materno.'</option>';
                }
               ?>
             </select>
        </div>
        <div class="col-xs-2 col-sm-2 col-md-2">
          <a class="btn btn-primary" onclick="addperplus()" style="margin-top: 22px;"><i class="fas fa-user-plus"></i></a>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <table class="table" id="tablepersonal">
            <tbody class="tb_tablepersonal"></tbody>
          </table>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <label>Titulo</label>
          <input type="text" class="form-control notetitle" autocomplete="off">
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <label>Fecha limite</label>
          <input type="date" class="form-control" id="vigencia">
        </div>
      </div>
      <div class="row">
        <div class="col-md-12 summernotetext">
          <label>Descripción</label>
          <textarea class="form-control summernote" style="min-height:200px !important"></textarea>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <input type="file" id="files" name="files[]" multiple > 
        </div>
      </div>
      <div class="row">
        <div class="input-field col-md-12">
          <button class="btn btn-success right enviodeticket"  name="action">Enviar</button>
        </div>
      </div>
    </div>
    
  </body>
</html>