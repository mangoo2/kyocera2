<style type="text/css">
  #profactura input{margin-bottom: 0px;height: 2rem;}
  .search_serie_div{position: fixed;top: 86%;width: 279px;background: white;z-index: 9999;}
  .import_cont_delete{display: none !important;}
  .tr_suspendido{
    background-color: #ff000026 !important;
  }
  .date-ser{
    font-size: 11px;
    color: #9e9e9e;
    font-style: italic;
  }
  .serie_input.cont_not_exit{
    background-color: #4293e9 !important;
  }
</style>
<br> 
<!--<input id="id_renta" name="id_renta" type="hidden" value="0" readonly>-->
<div class="row">
  <div class="col s12 infostatusvigente"></div>
  <div class="col s3 m1"><label>PERIODO</label></div>
  <div class="col s6 m3"><input id="periodo_inicial" name="periodo_inicial" type="date" readonly></div>
  <div class="col s2 m1"><label>AL</label></div>
  <div class="col s6 m3"><input id="periodo_final" name="periodo_final" type="date" readonly></div>
  <div class="col s6 m4" style="text-align: end;"><a class="btn-bmz waves-effect waves-light gradient-45deg-green-teal gradient-shadow" onclick="cargafilecontadores()" >Contadores</a></div>
</div>
<div class="row">
  <div class="col s12 m12" style="display: none;"><label>INFORMACIÓN EQUIPO</label></div>
  <div class="col s12 m12" style="display: none;"><label>RENDIMINETO</label></div>
  <div class="col s12 m12">
    <table class="responsive-table display" style="display: none;" id="profactura0">
      <thead><tr><th>EQUIPO No.</th><th>UBICACIÓN</th><th>MODELO</th><th>SERIE</th><th>PRODUCCIÓN PROMEDIO</th><th>MODELO TONER</th><th>RENDIMIENTO</th><th>ARRANQUE</th><th>STOCK MENSUAL</th><th>CONTADOR INICIAL</th><th>ACCESORIOS</th></tr></thead><tbody>
        <?php 
            $equiporow=1;
          foreach ($equiposrenta as $item){ ?><tr><td><?php echo $equiporow ?></td><td><input type="text" class="ubicacion_<?php echo $item->id;?>_<?php echo $item->serieId;?> readonlys" style="width: 75px;" class="readonlys" readonly></td><td><?php echo $item->modelo;if($item->usado==1){ echo '-mc';} ?></td><td><?php echo $item->serie ?></td><td><?php 
                    if(isset($_GET['ppfi'])){
                      $ppfi=$_GET['ppfi'];
                    }else{
                      $ppfi='';
                    }
                    if(isset($_GET['ppff'])){
                      $ppff=$_GET['ppff'];
                    }else{
                      $ppff='';
                    }
              //echo $this->Rentas_model->produccionpromedio($item->id,$item->serieId,1,$ppfi,$ppff); ?></td><td><?php //echo $this->Rentas_model->equiposrentastoner($item->idEquipo,$item->idRenta,1); ?></td><td><input type="number" class="nuevo_<?php echo $item->id;?>_<?php echo $item->serieId;?> readonlys" style="width: 75px;" class="readonlys" readonly></td><td><input type="number"  class="arranque_<?php echo $item->id;?>_<?php echo $item->serieId;?> readonlys" style="width: 75px;" class="readonlys" readonly></td><td><input type="number" class="stock_mensual_<?php echo $item->id;?>_<?php echo $item->serieId;?> readonlys" style="width: 110px;" class="readonlys" readonly></td><td><input type="number" class="contador_inicial_<?php echo $item->id;?>_<?php echo $item->serieId;?> readonlys" style="width: 110px;" class="readonlys" readonly></td><td><?php //echo $this->Rentas_model->equiposrentasaccesorios($item->idEquipo,$item->idRenta); ?></td></tr><?php $equiporow++; 
              } 
          ?> 
      </tbody>
      
    </table>
  </div>
  <div class="col s12 m12"><label>FACTURACIÓN</label></div>
</div>
<div class="row">
  <div class="col s3 m2"><label>PERIODO RENTA MONOCROMATICO</label></div>
  <div class="col s3 m5"><input type="text" id="periodon" class="periodon" name="" value="RENTA DEL 1 AL 30 DE SEPTIEMBRE DE 2019" readonly></div>
  <div class="col s3 m1"><input type="text" id="costoperiodo" value="0" readonly></div>
  <div class="col s3 m2"><label>TOTAL FACTURA</label></div>
  <div class="col s3 m2"><input type="text" id="totalperiodo" value="0" readonly></div>
</div> 
<div class="row removecostoperiodocolor">
  <div class="col s3 m2"><label>PERIODO RENTA COLOR</label></div>
  <div class="col s3 m5"><input type="text" id="periodon" class="periodon" name="" value="RENTA DEL 1 AL 30 DE SEPTIEMBRE DE 2019" readonly></div>
  <div class="col s3 m1"><input type="text" id="costoperiodocolor" value="0" readonly></div>
</div> 
<div class="row excedentesmono">
  <div class="col s3 m2"><label>EXCEDENTES MONOCROMATICO</label></div>
  <div class="col s3 m5"><input type="text" id="periodoex" value="EXCEDENTE DEL 1 AL 30 DE SEPTIEMBRE DE 2019" readonly></div>
  <div class="col s3 m1"><input type="text" id="costoperiodoexcedente"  name="" value="0" readonly></div>
</div> 
<div class="row excedentescolor">
  <div class="col s3 m2"><label>EXCEDENTES COLOR</label></div>
  <div class="col s3 m5"><input type="text" id="periodoex_e" value="EXCEDENTE DEL 1 AL 30 DE SEPTIEMBRE DE 2019" readonly></div>
  <div class="col s3 m1"><input type="text" id="costoperiodoexcedente_c"  name="" value="0" readonly></div>
</div> 

<div class="row indiglob_des">
  <div class="col s2">
    <label class="active">Descuento clicks</label>
    <input id="descuento_g_clicks2" type="number" class="form-control-bmz descuento_readonly descuento_0_0_0" onchange="caldesglobal()" data-equipo="0" data-serie="0" data-moco="0" readonly>
  </div>
  <div class="col s2">
    <label class="active">Descuento Escaneo</label>
    <input id="descuento_g_scaneo2" type="number" class="form-control-bmz descuento_readonly descuento_0_0_0" onchange="caldesglobal()" data-equipo="0" data-serie="0" data-moco="0" readonly>
  </div>
  <div class="col s2"></div>
  <div class="col s6 info_factura"></div>
</div>
<br>
<div class="row search_serie_div">
  <div class="col s3 m3 " align="right"><label>Serie</label></div>
  <div class="col s6 m6 "><input type="text" id="search_serie" class="form-control-bmz"></div>
  <div class="col s3 m3 "><button class="btn-bmz cyan waves-effect" type="button" onclick="buscar_serie()">Buscar</button></div>
</div>    
<div class="row">
  <div class="col s12 m12">
    <table class="table  display centered" id="profactura">
      <thead>
        <tr>
          <th rowspan="2"></th>
          <th rowspan="2"></th>
          <th rowspan="2">UBICACION</th>
          <th rowspan="2">TIPO</th>
          <th rowspan="2">MODELO</th>
          <th rowspan="2">SERIE</th>
          <th rowspan="2"></th>
          <th colspan="3">COPIA / IMPRESIÓN</th>
          <th colspan="3">ESCANEO</th>
          
          <th rowspan="2" class="removeproduccion">PRODUCCIÓN</th>
          <th rowspan="2">TONER CONSUMIDO</th>
          <th rowspan="2" title="descuento de clicks">DESCUENTO Cliks</th>
          <th rowspan="2" title="descuento de clicks">DESCUENTO Escaneo</th>
          <th rowspan="2">PRODUCCIÓN TOTAL</th>
          
          <th rowspan="2"># TONER CONSUMIDO</th>
          <th rowspan="2" class="removetipo2">EXCEDENTES</th>
          <th rowspan="2" class="removetipo2">COSTO EXCEDENTES</th>
          <th rowspan="2" class="removetipo2">TOTAL EXCEDENTE</th>
        </tr>
        <tr>
          <th>CONTADOR INICIAL</th>
          <th>CONTADOR FINAL</th>
          <th>PRODUCCIÓN</th>
          <th>CONTADOR INICIAL</th>
          <th>CONTADOR FINAL</th>
          <th>PRODUCCIÓN</th>
        </tr>
      </thead>
      <tbody>
        <?php 
            $equiporow=1;
          foreach ($equiposrenta as $item){ 
            if ($item->fecha_fin_susp!=null) {

              if (date('Y-m-d',strtotime($item->fecha_fin_susp)) >= date('Y-m-d')) {
                $equiposuspendido=1;
              }else{
                $equiposuspendido=0;
              }
            }else{
              $equiposuspendido=0;
            }
            if ($item->estatus==0) {
              $equiposuspendido=1;
            }
            if ($item->activo==0) {
              $equiposuspendido=1;
            }
            if($equiposuspendido==1){
              $class_sus=' equiposuspendido ';
              $title_sus=' title="Equipo Suspendido" ';
            }else{
              $class_sus='';
              $title_sus='';
            }
            //log_message('error', 'resultado final: '.$equiposuspendido);
            /*if ($item->tipo==1||$item->tipo==3) { */ ?>
              <tr class="trcsss_<?php echo $item->id;?>_<?php echo $item->serieId?>_1 <?php echo $class_sus;?>" <?php echo $title_sus;?> >
                <td class="img_captura_<?php echo $item->id;?>_<?php echo $item->serieId?> td_orden td_orden_<?php echo $item->id;?>_<?php echo $item->serieId;?>_1 td_mono"><?php echo $item->orden;?>
                </td>
                <td><?php echo $item->centro_costos_inf;?></td>
                <td><?php echo $item->ubicacion;?></td>
                <td>
                    <span title="Monocromatico">Mono</span>
                    <input type="hidden" class="f_c_cont_fin_<?php echo $item->id;?>_<?php echo $item->serieId?>_1_s" value="<?php echo $equiposuspendido;?>" readonly>
                    <input type="hidden" class="fd_equipo" value="<?php echo $item->id;?>" readonly>
                    <input type="hidden" class="fd_serie" value="<?php echo $item->serieId?>" readonly data-serie="<?php echo $item->serie;?>">
                    <input type="hidden" class="fd_tipo" value="1" readonly>
                    
                </td>
                <td><?php echo $item->modelo; if($item->usado==1){ echo '-mc';}?></td>
                <td class="color_serie serie_<?php echo $item->serieId?>" id="select_serie_<?php echo $item->serieId?>"><?php 
                    echo $item->serie;
                    echo '<br><span class="date-ser ser_'.$item->serie.'"></span>'; 
                    ?></td>
                <td><?php echo $item->contadores?></td>
                <td><input type="number" class=" f_c_c_i f_c_cont_ini_<?php echo $item->id;?>_<?php echo $item->serieId?>_1" readonly data-bodegaor="<?php echo $item->bodega_or;?>"></td>
                <td>
                  <input type="number" 
                    class=" f_c_c_f f_c_cont_fin_<?php echo $item->id;?>_<?php echo $item->serieId?>_1 add_cont_<?php echo $item->serie ?>_mono prfprocontador serie_input serie_input_<?php echo $item->serie;?> " 
                    data-equipo="<?php echo $item->id;?>" 
                    data-serie="<?php echo $item->serieId?>" 
                    data-moco="1" data-tomarescaner="1" data-tipo="1" value="0" 
                    data-suspendido="<?php echo $equiposuspendido;?>"
                    >
                </td>
                <td><input type="number" class=" f_c_c_p f_c_cont_pro_<?php echo $item->id;?>_<?php echo $item->serieId?>_1" readonly></td>
                <td><input type="number" class=" f_e_c_i f_e_cont_ini_<?php echo $item->id;?>_<?php echo $item->serieId?>_1 " readonly></td>
                <td>
                  <input type="number" 
                    class=" f_e_c_f f_e_cont_fin_<?php echo $item->id;?>_<?php echo $item->serieId?>_1 prfprocontador add_cont_<?php echo $item->serie ?>_escaner" 
                    data-equipo="<?php echo $item->id;?>" 
                    data-serie="<?php echo $item->serieId?>" 
                    data-moco="1" data-tomarescaner="1" data-tipo="1" value="0" 
                    data-suspendido="<?php echo $equiposuspendido;?>"
                    >
                </td>
                <td><input type="number" class=" f_e_c_p f_e_cont_pro_<?php echo $item->id;?>_<?php echo $item->serieId?>_1" readonly></td>
                <td class="removeproduccion"><input type="number" class="f_produccion f_produccion_<?php echo $item->id;?>_<?php echo $item->serieId?>_1" readonly></td>
                <td><input type="number" placeholder="%" class="f_toner_consumido f_toner_consumido_<?php echo $item->id;?>_<?php echo $item->serieId?>_1"  readonly></td>
                <td>
                  <input type="number" class="descuento descuento_readonly descuento_mono descuento_<?php echo $item->id;?>_<?php echo $item->serieId?>_1 prfprocontador" 
                    data-equipo="<?php echo $item->id;?>" 
                    data-serie="<?php echo $item->serieId?>" 
                    data-moco="1" data-tomarescaner="1" data-tipo="1"
                  value="<?php if($item->des_click>0){ echo $item->des_click;}else{ echo 0;} ?>" readonly>
                </td>
                <td>
                  <input type="number" class="descuentoe descuento_readonly descuentoe_mono descuentoe_<?php echo $item->id;?>_<?php echo $item->serieId?>_1 prfprocontador" 
                    data-equipo="<?php echo $item->id;?>" 
                    data-serie="<?php echo $item->serieId?>" 
                    data-moco="1" data-tomarescaner="1" data-tipo="1"
                  value="<?php if($item->des_escaneo>0){ echo $item->des_escaneo;}else{ echo 0;} ?>" readonly>
                </td>
                <td><input type="number" class="produccion_total produccion_mono produccion_total_<?php echo $item->id;?>_<?php echo $item->serieId?>_1" readonly></td>
                <td><input type="number" class="total_toner produccion_monot total_toner_<?php echo $item->id;?>_<?php echo $item->serieId?>_1" readonly></td>
                <td class="removetipo2"><input type="number" class="excedentes exc_mono excedentes_<?php echo $item->id;?>_<?php echo $item->serieId?>_1" readonly></td>
                <td class="removetipo2"><input type="number" class="excedentescosto excedentescosto_<?php echo $item->id;?>_<?php echo $item->serieId?>_1" readonly></td>
                <td class="removetipo2"><input type="number" class="excedente_mono excedentestotal excedentetotal_<?php echo $item->id;?>_<?php echo $item->serieId?>_1" value="0" readonly></td>
              </tr>
        <?php $equiporow++; 
              /* } */
            } ?> 
        <?php foreach ($equiposrenta as $item){ 
            if ($item->tipo==2||$item->tipo==3) { 

              if ($item->fecha_fin_susp!=null) {
              
                if (date('Y-m-d',strtotime($item->fecha_fin_susp))>=date('Y-m-d')) {
                  $equiposuspendido=1;
                }else{
                  $equiposuspendido=0;
                }
              }else{
                $equiposuspendido=0;
              }


              if ($item->tipo==3) {
                $readonlyh='readonly';
                $display='style="display:none"';
                $tomarescaner=0;
              }else{
                $display='';
                $tomarescaner=1;
                $readonlyh='';
              }
              if($equiposuspendido==1){
                $class_sus=' equiposuspendido ';
                $title_sus=' title="Equipo Suspendido" ';
              }else{
                $class_sus='';
                $title_sus='';
              }
        ?>
              <tr class="trcsss_<?php echo $item->id;?>_<?php echo $item->serieId?>_2 <?php echo $class_sus;?>" <?php echo $title_sus;?>>
                <td class="img_captura_<?php echo $item->id;?>_<?php echo $item->serieId?> td_orden td_orden_<?php echo $item->id;?>_<?php echo $item->serieId;?>_2 td_color" data-poreliminar="<?php echo $item->poreliminar; ?>"><?php echo $item->orden;?>
                </td>
                <td><?php echo $item->centro_costos_inf;?></td>
                <td><?php echo $item->ubicacion;?></td>
                <td>
                    Color
                    <input type="hidden" class="fd_equipo" value="<?php echo $item->id;?>" readonly>
                    <input type="hidden" class="fd_serie" value="<?php echo $item->serieId?>" readonly data-serie="<?php echo $item->serie;?>">
                    <input type="hidden" class="fd_tipo" value="2" readonly>
                </td>
                <td><?php echo $item->modelo ?></td>
                <td class="color_serie serie_<?php echo $item->serieId?>" id="select_serie_<?php echo $item->serieId?>"><?php 
                    echo $item->serie;
                    echo '<br><span class="date-ser ser_'.$item->serie.'"></span>'; 
                  ?></td>
                <td><?php echo $item->contadores?></td>
                <td><input type="number" class=" f_c_c_i f_c_cont_ini_<?php echo $item->id;?>_<?php echo $item->serieId?>_2" readonly data-bodegaor="<?php echo $item->bodega_or;?>"></td>
                <td>
                  <input type="number" 
                    class=" f_c_c_f f_c_cont_fin_<?php echo $item->id;?>_<?php echo $item->serieId?>_2 prfprocontador add_cont_<?php echo $item->serie ?>_color serie_input serie_input_<?php echo $item->serie;?>" 
                    data-equipo="<?php echo $item->id;?>" 
                    data-serie="<?php echo $item->serieId?>" 
                    data-moco="2" data-tipo="2" 
                    data-tomarescaner="<?php echo $tomarescaner;?>" 
                    value="0" 
                    data-suspendido="<?php echo $equiposuspendido;?>"
                    >
                </td>
                <td><input type="number" class=" f_c_c_p f_c_cont_pro_<?php echo $item->id;?>_<?php echo $item->serieId?>_2" readonly></td>
                <td><input type="number" class=" f_e_c_i f_e_cont_ini_<?php echo $item->id;?>_<?php echo $item->serieId?>_2" readonly <?php echo $display;?>></td>
                <td>
                  <input type="number" 
                    class=" f_e_c_f f_e_cont_fin_<?php echo $item->id;?>_<?php echo $item->serieId?>_2 prfprocontador" 
                    data-equipo="<?php echo $item->id;?>" 
                    data-serie="<?php echo $item->serieId?>" 
                    data-moco="2" data-tipo="2" 
                    data-tomarescaner="<?php echo $tomarescaner;?>" 
                    value="0" <?php echo $readonlyh;?> <?php echo $display;?>>
                </td>
                <td><input type="number" class=" f_e_c_p f_e_cont_pro_<?php echo $item->id;?>_<?php echo $item->serieId?>_2" <?php echo $display;?>  readonly></td>
                <td class="removeproduccion"><input type="number" class="f_produccion f_produccion_<?php echo $item->id;?>_<?php echo $item->serieId?>_2" readonly></td>
                <td><input type="number" placeholder="%" class="f_toner_consumido f_toner_consumido_<?php echo $item->id;?>_<?php echo $item->serieId?>_2"  readonly></td>
                <td>
                  <input type="number" class="descuento descuento_readonly descuento_color descuento_<?php echo $item->id;?>_<?php echo $item->serieId?>_2 prfprocontador" 
                    data-equipo="<?php echo $item->id;?>" 
                    data-serie="<?php echo $item->serieId?>" 
                    data-moco="2" data-tipo="2" 
                    data-tomarescaner="<?php echo $tomarescaner;?>" 
                  value="<?php if($item->des_click>0){ echo $item->des_click;}else{ echo 0;} ?>" readonly>
                </td>
                <td>
                  <input type="number" class="descuentoe descuento_readonly descuentoe_color descuentoe_<?php echo $item->id;?>_<?php echo $item->serieId?>_2 prfprocontador" 
                    data-equipo="<?php echo $item->id;?>" 
                    data-serie="<?php echo $item->serieId?>" 
                    data-moco="2" data-tipo="2" 
                    data-tomarescaner="<?php echo $tomarescaner;?>" 
                  value="<?php if($item->des_escaneo>0){ echo $item->des_escaneo;}else{ echo 0;} ?>" readonly>
                </td>
                <td><input type="number" class="produccion_total produccion_color produccion_total_<?php echo $item->id;?>_<?php echo $item->serieId?>_2" readonly></td>
                <td><input type="number" class="total_toner produccion_colort total_toner_<?php echo $item->id;?>_<?php echo $item->serieId?>_2" readonly></td>
                <td class="removetipo2"><input type="number" class="excedentes exc_color excedentes_<?php echo $item->id;?>_<?php echo $item->serieId?>_2" readonly></td>
                <td class="removetipo2"><input type="number" class="excedentescosto excedentescosto_<?php echo $item->id;?>_<?php echo $item->serieId?>_2" readonly></td>
                <td class="removetipo2"><input type="number" class="excedente_color  excedentestotal excedentetotal_<?php echo $item->id;?>_<?php echo $item->serieId?>_2" value="0" readonly></td>
              </tr>
        <?php $equiporow++;}} ?> 
          
      </tbody>
    </table>
  </div>
</div>
<div class="row row_equipo2">
  
</div>
<div class="row">
    <div class="botonfinalizar"></div>
    <div class="input-field col s2" style="margin-bottom:80px">
      <button class="b-btn b-btn-cyan preview_periodo" type="button" onclick="preview_periodo()">Preview<i class="material-icons right">send</i></button>
    </div>
    <div class="input-field col s11 guadarremove">
      <button class="btn cyan waves-effect waves-light right addprefactura" type="button" onclick="addprefactura()">Guardar <i class="material-icons right">send</i></button>
    </div>
  </div>
<script type="text/javascript">
  function verificaimg(){
    <?php foreach ($equiposrenta as $item){ ?>
      verificaimgrow(<?php echo $item->id;?>,<?php echo $item->serieId?>);
    <?php } ?>

  }
  function verificaimgrow(equipo,serie){
    var fechaini = $('#periodo_inicial').val();
    var fechafin = $('#periodo_final').val();
    $.ajax({
            url:'<?php echo base_url(); ?>index.php/Configuracionrentas/consultaimg',
            type:'POST',
            data:{
             equipo:equipo,
             serie:serie,
             fechaini:fechaini,
             fechafin:fechafin 
            },
            success: function(data) {
              console.log(data+'dsfsd');
                  if(data == 0){
                    //if($('.img_captura_'+equipo+'_'+serie).html()==''){
                      $('.img_captura_'+equipo+'_'+serie+' a').remove();
                      var span=$('.img_captura_'+equipo+'_'+serie).html();
                      $('.img_captura_'+equipo+'_'+serie).html(span+'<a class="btn-bmz darken-4" style="background-color: red"><i class="material-icons">photo_camera</i></a>');
                    //}
                  }else{
                    //if($('.img_captura_'+equipo+'_'+serie).html()==''){
                      $('.img_captura_'+equipo+'_'+serie+' a').remove();
                      var span=$('.img_captura_'+equipo+'_'+serie).html();
                      $('.img_captura_'+equipo+'_'+serie).html(span+'<a class="btn-bmz darken-4" style="background-color: #1d7d74" onclick="detalleimg('+equipo+','+serie+');"><i class="material-icons">photo_camera</i></a>');
                    //}
                  }
            }
        });
    
  }
</script>
<div id="modal_file" class="modal"  >
    <div class="modal-content ">
        <h5>Contadores registrados</h5>
        
        <div class="row">  
          <div class="col s12">
            <table class="table">
              <thead><tr><th>Fecha de carga</th><th></th><th></th></tr></thead>
              <tbody class="table_cont_reg"></tbody>
            </table>
          </div>
        </div>  
    </div>
    <div class="modal-footer">
        <button class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</button>
    </div>
    <!-- Modal finaliza fin-->
</div>
<div id="modal_file_info" class="modal"  >
    <div class="modal-content ">
        <h5>Contadores registrados</h5>
        
        <div class="row">  
          <div class="col s12">
            <table class="table">
              <thead><tr><th>Modelo</th><th>Serie</th><th>Ubicacion</th><th>Fecha</th><th>Mono</th><th>Color</th><th>Escaner</th><th>Contrato</th><th>Folio</th><th>Empresa</th></tr></thead>
              <tbody class="table_cont_reg_inf"></tbody>
            </table>
          </div>
        </div>  
    </div>
    <div class="modal-footer">
        <button class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</button>
    </div>
    <!-- Modal finaliza fin-->
</div>