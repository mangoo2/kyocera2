<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Facturacion extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('menus');
        $this->load->model('Facturacion_model', 'model');
        $this->load->model('Clientes_model');
    }

    // Listado de Equipos
    function index() {
        if($this->session->userdata('logeado')==true){
            //$menus = $this->menus->getMenus($this->session->userdata('perfilid'));
            //$data['menus'] = $menus['menus'];
            //$data['submenusArray'] = $menus['submenusArray'];
            $data['clientes'] = $this->Clientes_model->getListadoClientes();
            
            $this->load->view('header');
            $this->load->view('main',$data);
            $this->load->view('facturaciones/seleccionCliente');
            $this->load->view('facturaciones/seleccionCliente_js');
            $this->load->view('footer');
        }else{
            redirect('login');
        }
    }

    // Listado de Equipos
    function facturacionPorCliente($idCliente) 
    {
        if($this->session->userdata('logeado')==true)
        {
            $menus = $this->menus->getMenus($this->session->userdata('perfilid'));
            $data['menus'] = $menus['menus'];
            $data['submenusArray'] = $menus['submenusArray'];
            $data['idCliente'] = $idCliente;
            $data['facturasPorCliente'] = $this->model->getfacturasPorCliente($idCliente);

            $this->load->view('header');
            $this->load->view('main',$data);
            $this->load->view('facturaciones/listado');
            $this->load->view('facturaciones/listado_js');
            $this->load->view('footer');
        }
        else
        {
            redirect('login');
        }
    }

    function listadoFacturaCliente($idCliente)
    {
        $datos = $this->model->getfacturasPorCliente($idCliente);
        $json_data = array("data" => $datos);
        echo json_encode($json_data);
    }
}