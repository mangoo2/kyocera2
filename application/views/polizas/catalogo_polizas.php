<?php
// Validamos si es creación o edición, para así cargar los datos respectivamente
if(isset($poliza)){
  $idPoliza=$poliza->id;
  $nombre = $poliza->nombre;
  $cubre = $poliza->cubre;
  $vigencia_meses = $poliza->vigencia_meses;
  $vigencia_clicks = $poliza->vigencia_clicks;
  $titulo = 'Edición de Póliza';
  $descuento = $poliza->descuento;
}else{
  $idPoliza=0;
  $nombre = '';
  $cubre = '';
  $vigencia_meses = '';
  $vigencia_clicks = '';
  $titulo = 'Alta de Póliza';
  $descuento=0;
}
if($tipoVista==1){
  $label = 'Formulario de Alta de Póliza';
}else if($tipoVista==2){
  $label = 'Formulario de Edición de Póliza';
}else if($tipoVista==3){
  $label = 'Formulario de Visualización de Póliza';
} ?>
<style type="text/css">
.chosen-container{margin-left: 0px;}
<?php if($this->perfilid!=1){ ?>
.soloadministradores{display: none;}
<?php } ?>
.duplicado{
  box-shadow: 0px 0px 20px rgba(250,10,10,1);
  animation: infinite resplandorAnimation 2s;
}
@keyframes resplandorAnimation {
  0%,100%{
    box-shadow: 0px 0px 20px rgba(250,10,10,1);
  }
  50%{
    box-shadow: 0px 0px 0px;
  }
}
</style>
<!-- START CONTENT -->
<section id="content" width="100%">
  
  <!--start container-->
  <div class="container" width="100%">
    <input id="base_url" type="hidden" name="base_url" value="<?php echo base_url(); ?>">
    <input id="tipoVista" type="hidden" name="tipoVista" value="<?php echo $tipoVista ?>">
    <div class="section">
      <div class="row">
        <div class="col s12 m12 l12">
          <h4 class="header2"><?php echo $label; ?></h4>
          <div class="card-panel">
            <div class="row">
              <form class="form" id="polizas_form" method="post">
                <input id="idPoliza" name="idPoliza" type="hidden" value="<?php echo $idPoliza ?>">
                
                <div class="row">
                  <div class="input-field col s4">
                    <i class="material-icons prefix">text_format</i>
                    <input id="nombre" name="nombre" type="text" value="<?php echo $nombre; ?>">
                    <label for="nombre">Nombre de Póliza</label>
                  </div>
                  <div class="input-field col s4">
                    <i class="material-icons prefix">report</i>
                    <input id="cubre" name="cubre"  type="text" value="<?php echo $cubre; ?>">
                    <label for="cubre">Incluye</label>
                  </div>
                  
                </div>
                <div class="row">
                  <div class="input-field col s4">
                    <i class="material-icons prefix">date_range</i>
                    <input id="vigencia_meses" name="vigencia_meses"  type="text" value="<?php echo $vigencia_meses; ?>" title="solo numero de mes">
                    <label for="vigencia_meses">Vigencia</label>
                  </div>
                  <div class="input-field col s4">
                    <i class="material-icons prefix">print</i>
                    <input id="vigencia_clicks" name="vigencia_clicks" type="text" value="<?php echo $vigencia_clicks; ?>" title="solo numero de visitas total ó 0 para ilimitado">
                    <label for="vigencia_clicks">Visitas</label>
                  </div>
                  <div class=" col s4">
                    <label for="descuento">Descuento</label>
                    <select id="descuento" name="descuento" class="form-control-bmz browser-default">
                      <option value="0" <?php if($descuento==0){ echo 'selected';};?> >0 %</option>
                      <option value="10" <?php if($descuento==10){ echo 'selected';};?> >10 %</option>
                      <option value="20" <?php if($descuento==20){ echo 'selected';};?> >20 %</option>
                      <option value="30" <?php if($descuento==30){ echo 'selected';};?> >30 %</option>
                    </select>
                    
                  </div>
                </div>
              </form>
              <div class="row soloadministradores">
                <div class="col s12">
                  <button class="btn waves-effect waves-light cyan" onclick="adddetalle()">Agregar detalle</button>
                </div>
              </div>
              <div class="row">
                <div class="col s12" style="margin-top:25px">
                  <table class="table" id="poliza_detalle">
                    <tbody class="poliza_detalle_tbody">
                      
                    </tbody>
                  </table>
                </div>
              </div>
              <div class="row">
                <div class="input-field col s12">
                  <button class="btn cyan waves-effect waves-light right savepoliza" type="submit" name="action">Guardar
                  <i class="material-icons right">send</i>
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
  </div>
</div>
</div>
</section>
<select id="equiposbases" class="browser-default" style="display: none;">
<?php foreach ($equipos as $item) { ?>
<option value="<?php echo $item->id;?>"><?php echo $item->modelo;?></option>
<?php } ?>
</select>