<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Rentas extends CI_Controller {
    public function __construct()     {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('login_model');
        $this->load->model('Rentas_model');
        $this->load->model('Clientes_model');
        $this->load->library('menus');
        $this->load->model('Equipos_model');
        $this->load->model('ModeloCatalogos');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d');
        $this->submenu=7;
        if($this->session->userdata('logeado')==true){
            $this->perfilid =$this->session->userdata('perfilid');
            $this->idpersonal = $this->session->userdata('idpersonal');
            //$permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,7);// 7 es el id del submenu
            //if ($permiso==0) {
              //  redirect('Login');
            //}
        }else{
            redirect('login');
        }
    }
    // Listado de Empleados
    function index() {
        $data['MenusubId']=$this->submenu;
            // Obtiene los menús disponibles del sistema dependiendo el perfil, ayudado de la libreria "MENUS"
            $this->load->view('header');
            $this->load->view('main',$data);
            $this->load->view('rentas/listado');
            $this->load->view('footer');
            $this->load->view('rentas/jslistcontrato');   
    }
    function contrato($id){ 
        $data['MenusubId']=$this->submenu; 
            $data['tipo3']=0;
            $data['idRenta'] = $id; 
            // = Consultas de datos del cliente
            $data['contratofin'] = 0 ;
            $existeC = 0; // existe contrato
            $cliente_id = 0; // id del cliente
            $direcc = ''; // direccion del cliente
            $cont = ''; // contacto
            $cargoarea = ''; // cargo o area
            $tel = ''; // telefono
            $nombre = ''; // nombre
            $id_persona = 0; // id persona 
            $horario_disponible='';
            $equipo_acceso='';
            $documentacion_acceso='';
            $id_cliente = $this->Rentas_model->idcliente_idrenta($id); 
            foreach ($id_cliente as $item) {
                $cliente_id = $item->idCliente;// id del cliente
            }
            $data['cliente_id']=$cliente_id;
            $data['datoscontacto']=$this->ModeloCatalogos->getselectwheren('cliente_datoscontacto',array('clienteId'=>$cliente_id,'activo'=>1));
            //$data['direccioncliente']=$this->ModeloCatalogos->getselectwherenall('clientes_direccion',array('idcliente'=>$cliente_id,'status'=>1));
            $data['direccioncliente']=$this->Rentas_model->datosdireccioncontacto($cliente_id);
            //$data['cliente_rfc']= $this->Rentas_model->cliente_has_datos_fiscales($cliente_id);
            //Domicilio fiscal
            $data['fiscalcontacto'] = $this->Clientes_model->getListadoDatosFiscales($cliente_id);
            //Domicilio de instalacion
            $direccion = $this->Rentas_model->direccioncliente($cliente_id);
            foreach ($direccion as $item) { $direcc = $item->direccion; }
            //Contacto 
            //$contac = $this->Rentas_model->contactocliente($cliente_id);
            //foreach ($contac as $item) { $cont = $item->persona_contacto; }
            //Cargo o Area
            //$cargo = $this->Rentas_model->puestocontacto($cliente_id);
             //foreach ($cargo as $item) { $cargoarea = $item->puesto_contacto; }
            //Telefono 
            //$telefono = $this->Rentas_model->telefonocliente($cliente_id);
            //foreach ($telefono as $item) { $tel = $item->tel_local;}
            $data['speriocidad']=1;
            $resultcliente=$this->ModeloCatalogos->getselectwheren('clientes',array('id'=>$cliente_id));
            foreach ($resultcliente->result() as $item) {
                $horario_disponible=$item->horario_disponible;
                $equipo_acceso=$item->equipo_acceso;
                $documentacion_acceso=$item->documentacion_acceso;
            }
            // Existe un contrato
            $contratoexiste = $this->Rentas_model->existecontrato($id);             
            foreach ($contratoexiste as $item){
                    $existeC = 1;  
            }
            // Se ferifica si existe, si hay se pintan los datos del contrato ya existente 
            if($existeC == 1){
                $data['contratoexiste'] = $existeC; // existe contrato
                foreach ($contratoexiste as $item){
                    $data['idcontrato'] = $item->idcontrato;
                    $data['folio'] = $item->folio;
                    $data['tipocontrato'] = $item->tipocontrato;
                    $data['tipopersona'] = $item->tipopersona;
                    $data['vigencia'] = $item->vigencia;
                    $personalId_p = $item->personalId;
                    $data['fechasolicitud'] = $item->fechasolicitud;
                    $data['fechainicio'] = $item->fechainicio;
                    $data['arrendataria'] = $item->arrendataria;
                    $data['rfc'] = $item->rfc;
                    $data['actaconstitutiva'] = $item->actaconstitutiva;
                    $data['fecha'] = $item->fecha;
                    $data['notariapublicano'] = $item->notariapublicano;
                    $data['titular'] = $item->titular;
                    $data['representantelegal'] = $item->representantelegal;
                    $data['instrumentonotarial'] = $item->instrumentonotarial;
                    $data['instrumentofechasolicitud'] = $item->instrumentofechasolicitud;
                    $data['instrumentonotariapublicano'] = $item->instrumentonotariapublicano ;
                    $data['domiciliofiscal'] = $item->domiciliofiscal;
                    $data['domicilioinstalacion'] = $item->domicilioinstalacion;
                    $data['contacto'] = $item->contacto;
                    $data['cargoarea'] = $item->cargoarea;
                    $data['telefono'] = $item->telefono;
                    $data['rentadeposito'] = $item->rentadeposito;
                    $data['rentacolor'] = $item->rentacolor;
                    $data['rentaadelantada'] = $item->rentaadelantada;
                    $data['tiporenta'] = $item->tiporenta;
                    $data['tipo'] = $item->tipo;
                    $data['cantidad'] = $item->cantidad;
                    $data['exendente'] = $item->exendente;
                    $data['observaciones'] = $item->observaciones;
                    $data['idRenta'] = $item->idRenta;
                    $data['estatus'] = $item->estatus;
                    $data['clicks_mono']=$item->clicks_mono;
                    $data['precio_c_e_mono']=$item->precio_c_e_mono;
                    $data['clicks_color']=$item->clicks_color;
                    $data['precio_c_e_color']=$item->precio_c_e_color;
                    $data['servicio_horario']=$item->servicio_horario;
                    $data['servicio_equipo']=$item->servicio_equipo;
                    $data['servicio_doc']=$item->servicio_doc;
                    $data['ordencompra']=$item->ordencompra;
                    $data['horaentregainicio']=$item->horaentregainicio;
                    $data['horaentregafin']=$item->horaentregafin;
                    $data['facturacionlibre']=$item->facturacionlibre;
                    $data['speriocidad']=$item->speriocidad;

                    $data['descuento_g_clicks']=$item->descuento_g_clicks;
                    $data['descuento_g_scaneo']=$item->descuento_g_scaneo;
                    $data['oc_vigencia']=$item->oc_vigencia;
                    $data['oc_comentario']=$item->oc_comentario;
                    $data['info_factura']=$item->info_factura;
                    if($item->tipo3>0){
                        $data['tipo3']=$item->tipo3;
                    }else{
                        $datarenta=$this->ModeloCatalogos->db13_getselectwheren('alta_rentas',array('idcontrato'=>$item->idcontrato));
                        foreach ($datarenta->result() as $itemcr) {
                            $data['tipo3']=$itemcr->tipo3;  
                        }
                    }
                }
                $persona = $this->Rentas_model->getPersona($personalId_p);
                foreach ($persona as $item) {
                     $nombre = $item->nombre;
                     $id_persona = $item->personalId;
                }
                $data['vendedorid'] = $id_persona;
                $data['vendedor'] = $nombre;
                
            }else{
                $data['idcontrato'] = '';
                $data['folio'] = '';
                $data['tipocontrato'] = '';
                $data['tipopersona'] = '';
                $data['vigencia'] = '';
                $data['personalId'] = '';
                $data['fechasolicitud'] = '';
                $data['fechainicio'] = '';
                $data['arrendataria'] = '';
                $data['rfc'] = '';
                $data['actaconstitutiva'] = '';
                $data['fecha'] = '';
                $data['notariapublicano'] = '';
                $data['titular'] = '';
                $data['representantelegal'] = '';
                $data['instrumentonotarial'] = '';
                $data['instrumentofechasolicitud'] = '';
                $data['instrumentonotariapublicano'] = '';
                $data['domiciliofiscal'] = '';
                $data['rentadeposito'] = '';
                $data['rentacolor'] = '';
                $data['rentaadelantada'] = '';
                $data['tiporenta'] = 2;
                $data['tipo'] = '';
                $data['cantidad'] = '';
                $data['exendente'] = '';
                $data['observaciones'] = '';
                $data['idRenta'] = $id;
                $data['estatus'] = 1;
                // Variables de las consultas del cliente
                $data['contratoexiste'] = $existeC; // existe contrato
                $data['domicilioinstalacion'] = $direcc; // direccion del cliente
                $data['contacto'] = $cont; // contato 
                $data['cargoarea'] = $cargoarea;// carogo o area
                $data['telefono'] = $tel; // telefono 
                $data['clicks_mono']='';
                $data['precio_c_e_mono']='';
                $data['clicks_color']='';
                $data['precio_c_e_color']='';
                $data['servicio_horario']=$horario_disponible;
                $data['servicio_equipo']=$equipo_acceso;
                $data['servicio_doc']=$documentacion_acceso;
                $data['ordencompra']='';
                $data['horaentregainicio']='';
                $data['horaentregafin']='';
                $data['facturacionlibre']='';

                $data['descuento_g_clicks']=0;
                $data['descuento_g_scaneo']=0;
                $data['oc_vigencia']='';
                $data['oc_comentario']='';
                $data['info_factura']='';
                // id del quien esta iniciando sesion
                $datos = $this->Rentas_model->getPersona($this->session->userdata('idpersonal'));
                foreach ($datos as $item) {
                     $nombre = $item->nombre;
                }
                $data['vendedorid'] = $this->session->userdata('idpersonal');
                $data['vendedor'] = $nombre;

                $rentasanteriores = $this->Rentas_model->concultarcontratoscliente($cliente_id);
                foreach ($rentasanteriores->result() as $item) {
                    $data['arrendataria'] = $item->arrendataria;
                    $data['rfc'] = $item->rfc;
                    $data['actaconstitutiva'] = $item->actaconstitutiva;
                    $data['fecha'] = $item->fecha;
                    $data['notariapublicano'] = $item->notariapublicano;
                    $data['titular'] = $item->titular;
                    $data['representantelegal'] = $item->representantelegal;
                    $data['instrumentonotarial'] = $item->instrumentonotarial;
                    $data['instrumentonotariapublicano'] = $item->instrumentonotariapublicano;
                }
            }
            $data['equiposrow']=$this->ModeloCatalogos->getselectwherenall('equipos',array('estatus'=>1));
            //$data['bodegasrow']=$this->ModeloCatalogos->getselectwherenall('bodegas',array('activo'=>1));
            $data['restbodegas']=$this->ModeloCatalogos->view_session_bodegas();
            $data['existeC']=$existeC;
            $data['equipo'] = $this->Rentas_model->getcontratos_id($id);
            $data['equiposerie'] = $this->Rentas_model->equiposrentas2($id);
            $this->load->view('header');
            $this->load->view('main',$data);
            $this->load->view('rentas/contrato');
            $this->load->view('footerl');
            $this->load->view('rentas/jscontrato');
            $this->load->view('info_m_servisios');
    }
    function factura($idRegistro){
        $data['MenusubId']=$this->submenu;
            $menus = $this->menus->getMenus($this->session->userdata('perfilid'));
            $data['menus'] = $menus['menus'];
            $data['submenusArray'] = $menus['submenusArray'];

            $this->load->view('header');
            $this->load->view('main',$data);
            $this->load->view('operaciones/facturacion');
            $this->load->view('footer');    
    }
    function registrar(){
        $data = $this->input->post();
        $datosAct = array('estatus' => 2);
        $equiposclicks=$data['equipos'];
        $tiporenta=$data['tiporenta'];
        $direcciones=$data['direcciones'];
        unset($data['equipos']);
        unset($data['direcciones']);
        // Actualizamos la renta a estatus 2, lo cual querrá decir que se completó y se generó contrato
        $this->Rentas_model->actualizaRenta($datosAct, $data['idRenta']);
        $result = $this->Rentas_model->alta('contrato',$data);
        $this->ModeloCatalogos->updateuuid($result,6);
        //============================================================================
            $array_generar=array(
            'renta'=>$data['idRenta'],
            'tipo'=>$data['tiporenta'],
            'fechainicial'=>$data['fechainicio'],
            'fechafinal'=>'',
            'renta_m'=>$data['rentadeposito'],
            'click_m'=>$data['clicks_mono'],
            'excedente_m'=>$data['precio_c_e_mono'],
            'renta_c'=>$data['rentacolor'],
            'click_c'=>$data['clicks_color'],
            'excedente_c'=>$data['precio_c_e_color']);
            $con_id=$this->ModeloCatalogos->Insert('rentas_condiciones',$array_generar);
        //============================================================================
        echo $result;
        if ($tiporenta==1) {
            $equiposclicks = json_decode($equiposclicks);
            $dataarrayinsert_eq=array();
            $dataarrayinsert_eq_cond=array();
            for ($i=0;$i<count($equiposclicks);$i++) { 
                  $datae['contrato'] = $result;
                  $datae['equiposrow'] = $equiposclicks[$i]->equiposrow;
                  $datae['serieId'] = $equiposclicks[$i]->serieId;
                  $datae['rentacosto'] = $equiposclicks[$i]->rentacosto;
                  $datae['pag_monocromo_incluidos'] = $equiposclicks[$i]->pag_monocromo_incluidos;
                  $datae['excedente'] = $equiposclicks[$i]->excedente;
                  $datae['rentacostocolor'] = $equiposclicks[$i]->rentacostocolor;
                  $datae['pag_color_incluidos'] = $equiposclicks[$i]->pag_color_incluidos;
                  $datae['excedentecolor'] = $equiposclicks[$i]->excedentecolor;
                  $dataarrayinsert_eq[]=$datae;
                  //$this->ModeloCatalogos->Insert('contrato_equipos',$datae);
                  //=================================================================
                    $datasi['idc']=$con_id;
                    $datasi['equiporow']=$equiposclicks[$i]->equiposrow;
                    $datasi['serieId']=$equiposclicks[$i]->serieId;
                    $datasi['renta_m']=$equiposclicks[$i]->rentacosto;
                    $datasi['click_m']=$equiposclicks[$i]->pag_monocromo_incluidos;
                    $datasi['excedente_m']=$equiposclicks[$i]->excedente;
                    $datasi['renta_c']=$equiposclicks[$i]->rentacostocolor;
                    $datasi['click_c']=$equiposclicks[$i]->pag_color_incluidos;
                    $datasi['excedente_c']=$equiposclicks[$i]->excedentecolor;
                    $dataarrayinsert_eq_cond[]=$datasi;
                    //$this->ModeloCatalogos->Insert('rentas_condiciones_detalle',$datasi);
                  //=================================================================
            }
            if(count($equiposclicks)>0){
                $this->ModeloCatalogos->insert_batch('contrato_equipos',$dataarrayinsert_eq);
            }
            if(count($dataarrayinsert_eq_cond)>0){
                $this->ModeloCatalogos->insert_batch('rentas_condiciones_detalle',$dataarrayinsert_eq_cond);
            }
        }
        //==============================================
            $direcciones = json_decode($direcciones);
            $datadirarray=array();
            for ($i=0;$i<count($direcciones);$i++) { 
                  $datad['contrato'] = $result;
                  $datad['equiposrow'] = $direcciones[$i]->equiposrow;
                  $datad['serieId'] = $direcciones[$i]->serieId;
                  $datad['direccion'] = $direcciones[$i]->direccion;
                  $datadirarray[]=$datad;

                  //$this->ModeloCatalogos->Insert('contrato_equipo_direccion',$datad);
                  $this->ModeloCatalogos->db5_updateCatalogo('asignacion_series_r_equipos',array('orden'=>$direcciones[$i]->orden,'ubicacion'=>$direcciones[$i]->ubicacion,'des_click'=>$direcciones[$i]->des_click,'des_escaneo'=>$direcciones[$i]->des_escaneo),array('id_equipo'=>$direcciones[$i]->equiposrow,'serieId'=>$direcciones[$i]->serieId)); 
            }
            if (count($direcciones)>0) {
                $this->ModeloCatalogos->insert_batch('contrato_equipo_direccion',$datadirarray);
            }
        //==============================================
        /*
            $contrato_renta = $this->ModeloCatalogos->contrato_renta($data['idRenta']);
            foreach ($contrato_renta as $item) {
                $clienteid_renta = $item->idCliente;
            }
            $dataarray = array('idrentas' => $data['idRenta']);
            $resulthdconsumibles = $this->ModeloCatalogos->getselectwheren('rentas_has_detallesequipos_consumible',$dataarray);
            //=======================================================================
                $folio = 0;
                $contr_folio = $this->ModeloCatalogos->contratofolio($clienteid_renta);
                foreach ($contr_folio as $itemc) {
                    $folio = $itemc->folio;
                }
                $datafoliosrray=array();
            //=======================================================================


            foreach ($resulthdconsumibles->result() as $item) {
                
                $cantidad = $item->cantidad;
                for ($i=1; $i <= $cantidad; $i++) { 
                    
                    $folio = $folio +1; 
                    /// $idretashdc = $item->id;
                    $id_rentas_c = $item->idrentas;
                    $id_consumibles = $item->id_consumibles;
                    $idequipo = $item->idequipo;
                    //===========================================
                    // aqui se sacara la serie del equipo para agregarla a la tabla de control de folio
                    // despues en la consulta se ara la consulta por el id se serie para desagrupar los folios de los consumibles
                    // el id de la serie se sacara de la tabla "asignacion_series_r_equipos" dependiendo del $idequipo
                    // si en dado caso es un equipo que su cantidad sea de 2 o mas por obvias razones va atener mas series y en este caso
                    //===========================================

                    $foliotext = $id_rentas_c.'T'.str_pad($folio, 3, "0", STR_PAD_LEFT);   
                    $data_folio = array(
                                        'idrenta'=>$id_rentas_c,
                                        'idCliente'=>$clienteid_renta,
                                        'idconsumible'=>$id_consumibles,
                                        'folio'=>$folio,
                                        'foliotext'=>$foliotext,
                                    );
                    //$this->ModeloCatalogos->Insert('contrato_folio',$data_folio);
                    $datafoliosrray[]=$data_folio;
                }
            }
            if(count($datadirarray)>0){
                $this->ModeloCatalogos->insert_batch('contrato_folio',$datafoliosrray);
            }
        */
        //================================================================================================================
        //$this->Modelobitacoras->Insert(array('contenido'=>'Se Genero contrato de rentas','nombretabla'=>'contrato','idtable'=>$result,'tipo'=>'Insert','personalId'=>$this->idpersonal));
        //============================== generacion de servicio de instalacion =========================================
            /*
            $serviciodata= array(
                                    'contratoId'=>$result,
                                    'prioridad'=>1,
                                    'zonaId'=>1,//sin zona
                                    'tpoliza'=>17,//Servicios de instalación
                                    'tservicio_a'=>876,//sin modelo
                                    'ttipo'=>1,
                                    'tserviciomotivo'=>'Servicio de instalación',
                                    'servicio_auto_inst'=>1,
                                    'creado_personal'=>$this->idpersonal
                                );
            $asignacionId=$this->ModeloCatalogos->Insert('asignacion_ser_contrato_a',$serviciodata);
            //===================================
                $fechainicial = strtotime ( '+ 1 day' , strtotime ($this->fechahoy));
                $fechainicial = date ( 'Y-m-d' , $fechainicial);
            //===================================
            $dataeqarray=array();
            for ($i=0;$i<count($direcciones);$i++) { 
                $datainfoeq=array(
                                    'asignacionId'=>$asignacionId,
                                    'idequipo'=>$direcciones[$i]->equiposrow,
                                    'serieId'=>$direcciones[$i]->serieId,
                                    'tecnico'=>49,
                                    'fecha'=>$fechainicial,
                                    'hora'=>'09:00:00',
                                    'horafin'=>'18:00:00',
                                    'hora_comida'=>null,
                                    'horafin_comida'=>null
                                );
                            $dataeqarray[]=$datainfoeq;
            }
            if(count($direcciones)>0){
                $this->ModeloCatalogos->insert_batch('asignacion_ser_contrato_a_e',$dataeqarray);
            }
            */
        //==============================================================================================================
    }
    function solicitudinstalacion(){
        $data = $this->input->post();
        $idRenta=$data['idRenta'];
        $equiposerie = $this->Rentas_model->equiposrentas2($idRenta);

        $result=$this->ModeloCatalogos->getselectwheren('contrato',array('idRenta'=>$idRenta,'estatus'=>1));
        $idcontrato=0;
        foreach ($result->result() as $item) {
            $idcontrato=$item->idcontrato;
        }
        if($idcontrato>0){
            //==================================================================================
                $serviciodata= array(
                                    'contratoId'=>$idcontrato,
                                    'prioridad'=>1,
                                    'zonaId'=>1,//sin zona
                                    'tpoliza'=>17,//Servicios de instalación
                                    'tservicio_a'=>876,//sin modelo
                                    'ttipo'=>1,
                                    'tserviciomotivo'=>'Servicio de instalación',
                                    'servicio_auto_inst'=>1,
                                    'creado_personal'=>$this->idpersonal
                                );
            $asignacionId=$this->ModeloCatalogos->Insert('asignacion_ser_contrato_a',$serviciodata);
            //==================================================================================
            //===================================
                $fechainicial = strtotime ( '+ 1 day' , strtotime ($this->fechahoy));
                $fechainicial = date ( 'Y-m-d' , $fechainicial);
            //===================================
            $dataeqarray=array();
            foreach ($equiposerie as $iteme) {
                $datainfoeq=array(
                                    'asignacionId'=>$asignacionId,
                                    'idequipo'=>$iteme->id,
                                    'serieId'=>$iteme->serieId,
                                    'tecnico'=>49,
                                    'fecha'=>$fechainicial,
                                    'hora'=>'09:00:00',
                                    'horafin'=>'18:00:00',
                                    'hora_comida'=>null,
                                    'horafin_comida'=>null
                                );
                            $dataeqarray[]=$datainfoeq;
            }
            if(count($dataeqarray)>0){
                $this->ModeloCatalogos->insert_batch('asignacion_ser_contrato_a_e',$dataeqarray);
            }
        }
    }
    function update_registrar(){
        $data = $this->input->post();
        $direcciones=$data['direcciones'];
        $equiposclicks=$data['equipos'];
        $tiporenta=$data['tiporenta'];
        unset($data['direcciones']);
        unset($data['equipos']);
        $rentadeposito = $data['rentadeposito'];
        $rentacolor    = $data['rentacolor'];
        $idRenta       = $data['idRenta'];
        $vigencia      = $data['vigencia'];
        $fechainicio   = $data['fechainicio'];
        $servicio_horario=$data['servicio_horario'];
        $servicio_equipo=$data['servicio_equipo'];
        $servicio_doc=$data['servicio_doc'];
        if(!isset($data['facturacionlibre'])){
            $data['facturacionlibre']=0;
        }
        //================================================
        $resultdatosc = $this->ModeloCatalogos->getselectwheren('contrato',array('idRenta'=>$idRenta));
        $idcontrato=0;
        foreach ($resultdatosc->result() as $item) {
            $idcontrato=$item->idcontrato;
        }
        //==============================================

        $result = $this->Rentas_model->update('contrato',$data,'idRenta',$data['idRenta']);
        if($idcontrato>0){
            $this->ModeloCatalogos->updateCatalogo('alta_rentas',array('tipo2'=>$tiporenta),array('idcontrato'=>$idcontrato));
        }

        if ($tiporenta==1) {
            $equiposclicks = json_decode($equiposclicks);
            for ($i=0;$i<count($equiposclicks);$i++) { 
                  $dataew['equiposrow'] = $equiposclicks[$i]->equiposrow;
                  $dataew['serieId'] = $equiposclicks[$i]->serieId;
                  $datae['rentacosto'] = $equiposclicks[$i]->rentacosto;
                  $datae['pag_monocromo_incluidos'] = $equiposclicks[$i]->pag_monocromo_incluidos;
                  $datae['excedente'] = $equiposclicks[$i]->excedente;
                  $datae['rentacostocolor'] = $equiposclicks[$i]->rentacostocolor;
                  $datae['pag_color_incluidos'] = $equiposclicks[$i]->pag_color_incluidos;
                  $datae['excedentecolor'] = $equiposclicks[$i]->excedentecolor;

                  $existeequipo=$this->ModeloCatalogos->getselectwheren('contrato_equipos',$dataew);
                  if($existeequipo->num_rows()==0){
                    $datae['equiposrow']=$equiposclicks[$i]->equiposrow;
                    $datae['serieId']=$equiposclicks[$i]->serieId;
                    $datae['contrato']=$idcontrato;
                    $this->ModeloCatalogos->Insert('contrato_equipos',$datae);
                  }else{
                    unset($datae['equiposrow']);
                    unset($datae['serieId']);
                    unset($datae['contrato']);
                    $this->ModeloCatalogos->updateCatalogo('contrato_equipos',$datae,$dataew);
                  }
                  $datos_renta_equipos = array(
                                                'renta'=>$equiposclicks[$i]->rentacosto,
                                                'clicks_mono'=>$equiposclicks[$i]->pag_monocromo_incluidos,
                                                'rentac'=>$equiposclicks[$i]->rentacostocolor,
                                                'clicks_color'=>$equiposclicks[$i]->pag_color_incluidos,
                                                'precio_c_e_mono'=>$equiposclicks[$i]->excedente,
                                                'precio_c_e_color'=>$equiposclicks[$i]->excedentecolor
                                            );
                  $datos_renta_equiposwhere=array(
                                            'id_equipo'=>$equiposclicks[$i]->equiposrow,
                                            'serieId'=>$equiposclicks[$i]->serieId
                                            );
                  $this->ModeloCatalogos->updateCatalogo('alta_rentas_equipos',$datos_renta_equipos,$datos_renta_equiposwhere);
            }
            //$this->ModeloCatalogos->Insert('contrato_equipos',$datae);
        }
        
            $direcciones = json_decode($direcciones);
            $datainsertarray_cqd=array();
            for ($i=0;$i<count($direcciones);$i++) { 
                  $datadw['equiposrow'] = $direcciones[$i]->equiposrow;
                  $datadw['serieId'] = $direcciones[$i]->serieId;
                  $datad['direccion'] = $direcciones[$i]->direccion;
                  $resultdireccion=$this->ModeloCatalogos->db2_getselectwheren('contrato_equipo_direccion',$datadw);
                  if($resultdireccion->num_rows()==0){
                        $datadw['direccion'] = $direcciones[$i]->direccion;
                        $datadw['contrato'] = $idcontrato;
                        $datainsertarray_cqd[]=$datadw;
                        //$this->ModeloCatalogos->db4_Insert('contrato_equipo_direccion',$datadw);
                  }else{
                        $this->ModeloCatalogos->db5_updateCatalogo('contrato_equipo_direccion',$datad,$datadw);  
                  }
                  $this->ModeloCatalogos->db5_updateCatalogo('asignacion_series_r_equipos',array('orden'=>$direcciones[$i]->orden,'ubicacion'=>$direcciones[$i]->ubicacion,'des_click'=>$direcciones[$i]->des_click,'des_escaneo'=>$direcciones[$i]->des_escaneo),array('id_equipo'=>$direcciones[$i]->equiposrow,'serieId'=>$direcciones[$i]->serieId));  
                  
            }
            if(count($datainsertarray_cqd)>0){
                $this->ModeloCatalogos->insert_batch('contrato_equipo_direccion',$datainsertarray_cqd);
            }
        //=============================================================================
            switch ($vigencia) {
              case 1:
                $meses=12;
                break;
              case 2:
                $meses=24;
                break;
              case 3:
                $meses=36;
                break;
              
              default:
                $meses=0;
                break;
            }
            $ordenc_vigencia = strtotime ( '+ '.$meses.' month', strtotime ($fechainicio) ) ;
            $ordenc_vigencia = date ( 'Y-m-d' , $ordenc_vigencia );

            $this->ModeloCatalogos->updateCatalogo('alta_rentas',array('monto'=>$rentadeposito,'montoc'=>$rentacolor,'ordenc_vigencia'=>$ordenc_vigencia,'servicio_horario'=>$servicio_horario,'servicio_equipo'=>$servicio_equipo,'servicio_doc'=>$servicio_doc),array('idcontrato'=>$idcontrato));
        //=============================================================================
            //$this->Modelobitacoras->Insert(array('contenido'=>'Se Modifico contrato de rentas','nombretabla'=>'contrato','idtable'=>$result,'tipo'=>'Insert','personalId'=>$this->idpersonal));

        echo $result;
        //==============================================
    }
    public function getListadocontratosRentasIncompletas() {
        $params = $this->input->post();
        $getdata = $this->Rentas_model->getListadocontratosRentasIncompletas($params);
        $totaldata= $this->Rentas_model->total_getListadocontratosRentasIncompletas($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    function traerdatoscontrato(){
        $idcontrato = $this->input->post('contrato');
        $contratos_equipos = $this->ModeloCatalogos->getselectwheren('contrato_equipos',array('contrato'=>$idcontrato));
        //$contratos_equiposdir = $this->ModeloCatalogos->getselectwheren('contrato_equipo_direccion',array('contrato'=>$idcontrato));
        $filasrow= array(
                        'equipos'=>$contratos_equipos->result()
                        );
        echo json_encode($filasrow);
    }
    function traerdatosrenta(){
        $idrenta = $this->input->post('renta');
        $rowrentas = $this->ModeloCatalogos->getselectwheren('rentas',array('id'=>$idrenta));
        $rowrentasequipos = $this->ModeloCatalogos->getselectwheren('rentas_has_detallesEquipos',array('idRenta'=>$idrenta));
        $tiporenta=0;
        $eg_rm=0;
        $eg_rvm=0;
        $eg_rpem=0;
        $eg_rc=0;
        $eg_rvc=0;
        $eg_rpec=0;
        foreach ($rowrentas->result() as $item) {
            $tiporenta=$item->tiporenta;
            $eg_rm=$item->eg_rm;
            $eg_rvm=$item->eg_rvm;
            $eg_rpem=$item->eg_rpem;
            $eg_rc=$item->eg_rc;
            $eg_rvc=$item->eg_rvc;
            $eg_rpec=$item->eg_rpec;
        }
        $filasrow= array(
            'tiporenta'=>$tiporenta,
            'eg_rm'=>$eg_rm,
            'eg_rvm'=>$eg_rvm,
            'eg_rpem'=>$eg_rpem,
            'eg_rc'=>$eg_rc,
            'eg_rvc'=>$eg_rvc,
            'eg_rpec'=>$eg_rpec,
            'equipos'=>$rowrentasequipos->result()
        );
        echo json_encode($filasrow);
    }
    function obtenerconsumiblesrefacciones(){
        $equipo = $this->input->post('equipo');
        $accesorios = $this->Equipos_model->getListadoAcesoriosEquipo($equipo,0);
        $consumibles = $this->ModeloCatalogos->consumiblesequipos($equipo,0);//en el sero puede ser sustituido para consultar stock de bodegas
        $optionaccesorios='';
        foreach ($accesorios as $item) {
            $optionaccesorios.='<option value="'.$item->idcatalogo_accesorio.'">'.$item->nombre.'</option>';
        }
        $optionconsumibles='';
        foreach ($consumibles->result() as $item) {
            $optionconsumibles.='<option value="'.$item->idconsumible.'">'.$item->modelo.'</option>';        
        }
         $array = array("accesorios"=>$optionaccesorios,
                        "consumibles"=>$optionconsumibles
                    );
        echo json_encode($array);
    }
    /*
    function agregarnuevoequipo(){
        $data = $this->input->post();
        //log_message('error', $data);
        $renta = $data['renta'];
        $rentaedita = $data['rentaedita'];
        $idRentEdit = $data['rentaRenEdita'];
        $cantidad = $data['cantidad'];
        $equipoInd = $data['equipo'];
        $bodega = $data['bodega'];
        $temp = $data['temp'];
        $fechatemp = $data['fechatemp'];
        $arrayconsumible = $data['arrayconsumible'];
        $arrayaccesorios = $data['arrayaccesorios'];
        unset($data['arrayconsumible']);
        unset($data['arrayaccesorios']);
        $contrato_renta = $this->ModeloCatalogos->contrato_renta($data['renta']);
        foreach ($contrato_renta as $item) {
            $clienteid_renta = $item->idCliente;
        }
        //=======================================
            $costo_unidad_imagen=0;
            $rendimiento_unidad_imagen=0;
            $costo_garantia_servicio=0;
            $rendimiento_garantia_servicio=0;
            $costo_pagina_monocromatica=0;
            $costo_pagina_color=0;
            $tipo=0;
            $costo_toner=0;
            $rendimiento_toner=0;

            $historialid=$this->ModeloCatalogos->Insert('rentas_historial',array('rentas'=>$renta));

            $equipo = $this->Equipos_model->getEquipoPorId($equipoInd); 
            $precio = $equipo->costo_renta;
        //=======================================
            if($rentaedita==0){
                $renta = $renta;
            }else{
                $renta = $idRentEdit;
            }
            $detallesCotizacion = array('idRenta' => $renta,
                                            'idEquipo' => $equipoInd,
                                            'cantidad' => $cantidad,
                                            'serie_bodega' => $bodega,
                                            'modelo' => $equipo->modelo,
                                            'especificaciones_tecnicas' => $equipo->especificaciones_tecnicas,
                                            'precio' => $precio,
                                            'costo_unidad_imagen' => $costo_unidad_imagen,
                                            'rendimiento_unidad_imagen' => $rendimiento_unidad_imagen,
                                            'costo_garantia_servicio' => $costo_garantia_servicio,
                                            'rendimiento_garantia_servicio' => $rendimiento_garantia_servicio,
                                            'costo_pagina_monocromatica' => $costo_pagina_monocromatica,
                                            'costo_pagina_color' => $costo_pagina_color,
                                            'excedente' => $equipo->excedente,
                                            'tipotoner' => $tipo,
                                            'costo_toner' => $costo_toner,
                                            'rendimiento_toner' => $rendimiento_toner,
                                            'estatus_suspendido'=>$temp, 
                                            'fecha_fin_susp'=>$fechatemp,  
                                            'serie_estatus'=>0         
                                );
            // Se insertan en la BD
            if($rentaedita==0){
                $idhistorialequipos=$this->ModeloCatalogos->db5_Insert('rentas_has_detallesEquipos',$detallesCotizacion);
            }else{
                $idhistorialequipos=$this->ModeloCatalogos->db5_Insert('rentas_has_detallesEquipos',$detallesCotizacion);// se agrego esta linea para insertar un nuevo registro
                $this->ModeloCatalogos->db4_updateCatalogo('rentas_has_detallesEquipos',array('estatus'=>0),array('id'=>$rentaedita));   //se le cambia el status para conservarlo pero por el estatus hinabilitarlo
                //log_message('error', 'verificar eliminar renta:'.$renta.' rentaedita:'.$rentaedita);
                //log_message('error', 'asignacion_series_r_equipos activo=0 id_equipo:'.$rentaedita);
                //$this->ModeloCatalogos->updateCatalogo('asignacion_series_r_equipos',array('activo'=>0),array('id_equipo'=>$rentaedita));
                $rowseries=$this->ModeloCatalogos->getselectwheren('asignacion_series_r_equipos',array('id_equipo'=>$rentaedita));
                foreach ($rowseries->result() as $itemse) {
                    
                    $this->ModeloCatalogos->db4_updateCatalogo('series_productos',array('status'=>0),array('serieId'=>$itemse->serieId));
                    //$this->ModeloCatalogos->updateCatalogo('asignacion_ser_contrato_a_e',array('status'=>3,'motivosuspencion'=>'Eliminacion y/o Cambio'),array('serieId'=>$itemse->serieId,'idequipo'=>$rentaedita,'fecha >='=>$this->fechahoy));
                }
            }
            $this->ModeloCatalogos->db5_Insert('rentas_historial_equipos',array('historialid'=>$historialid,'equipo'=>$idhistorialequipos));
        //=======================================
            
                        
            $DATAc = json_decode($arrayconsumible);
            for ($i=0;$i<count($DATAc);$i++) {

                    $idconsumuble=$DATAc[$i]->toner;
                    //log_message('error', 'idconsumible '.$idconsumuble);
                    $resultadoaddcon=$this->ModeloCatalogos->consumibleagregadoselected($idconsumuble,$equipo->id);
                    foreach ($resultadoaddcon->result() as $item2) {
                        $tipo = $item2->tipo;
                        $costo_toner = $item2->poliza;
                        $rendimiento_toner = $item2->rendimiento_toner;
                        $costo_unidad_imagen = $item2->costo_unidad_imagen;
                        $rendimiento_unidad_imagen = $item2->rendimiento_unidad_imagen;
                        $costo_garantia_servicio = $item2->costo_garantia_servicio;
                        $rendimiento_garantia_servicio = $item2->rendimiento_garantia_servicio;
                        $costo_pagina_monocromatica = $item2->costo_pagina_monocromatica;
                        $costo_pagina_color = $item2->costo_pagina_color;
                    }
                    if($rentaedita==0){
                        $renta = $renta;
                    }else{
                        $renta = $idRentEdit;  
                    }
                    //$datachfc["idventa"]=$idCotizacion;
                    $datachfc["idequipo"]=$equipoInd;
                    $datachfc["rowequipo"]=$idhistorialequipos;
                    $datachfc["id_consumibles"]=$idconsumuble;
                    $datachfc["costo_toner"]=$costo_toner;
                    $datachfc["rendimiento_toner"]=$rendimiento_toner;
                    $datachfc["rendimiento_unidad_imagen"]=$rendimiento_unidad_imagen;
                    $datachfc["idrentas"]=$renta;
                    $datachfc["cantidad"]=$DATAc[$i]->piezas;
                    $cantidad=$DATAc[$i]->piezas;
                    if($rentaedita==0){
                        $this->ModeloCatalogos->db5_Insert('rentas_has_detallesequipos_consumible',$datachfc);  
                    }else{
                        $this->ModeloCatalogos->db4_updateCatalogo('rentas_has_detallesequipos_consumible',$datachfc,array('idrentas'=>$renta,'idequipo'=>$equipoInd));   
                    }
                    $folios=$this->addcontrolfolio($renta,$cantidad,$clienteid_renta,$idconsumuble);
                    $this->ModeloCatalogos->db5_Insert('rentas_historial_consumibles',array('cantidad'=>$cantidad,'historialid'=>$historialid,'consumible'=>$idconsumuble,'folios'=>$folios));
            }
        //=======================================
            
        //=======================================
        $DATAa = json_decode($arrayaccesorios);
           // log_message('error', 'accesorios:acc'.$accesorios);
            //log_message('error', 'accesorios: data '.$DATAa);
            for ($i=0;$i<count($DATAa);$i++) {
                if($rentaedita==0){
                    $renta = $renta;
                }else{
                    $renta = $idRentEdit;  
                }
                $dataas["idrentas"]=$renta;
                $dataas['id_equipo']=$equipoInd;
                $dataas["rowequipo"]=$idhistorialequipos;
                $dataas['id_accesorio']=$DATAa[$i]->accesorio;
                $dataas["cantidad"]=$DATAa[$i]->piezas;
                if($rentaedita==0){
                    $id_accesoriod=$this->ModeloCatalogos->db5_Insert('rentas_has_detallesEquipos_accesorios',$dataas);
                }else{
                    $id_accesoriod=0;
                    $this->ModeloCatalogos->updateCatalogo('rentas_has_detallesEquipos_accesorios',$dataas,array('idrentas'=>$renta,'id_equipo'=>$equipoInd));   
                }
                $this->ModeloCatalogos->db5_Insert('rentas_historial_accesorios',array('cantidad'=>$DATAa[$i]->piezas,'historialid'=>$historialid,'accesorio'=>$DATAa[$i]->accesorio,'id_accesoriod'=>$id_accesoriod));
            //log_message('error', 'Equipo: '.$equiposelected.' accesorio: '.$accesorioselected);
            }   
        //=======================================
        //$this->eliminarserviciosposteriores($renta);
    }
    */
    function agregarnuevoequipo(){
        $data = $this->input->post();

        $renta = $data['renta'];
        $rentaedita = $data['rentaedita'];
        $idRentEdit = $data['rentaRenEdita'];

        $arrayequipos =$data['arrayequipos'];

        $contrato_renta = $this->ModeloCatalogos->contrato_renta($data['renta']);
        foreach ($contrato_renta as $item) {
            $clienteid_renta = $item->idCliente;
        }
        //=======================================
            $costo_unidad_imagen=0;
            $rendimiento_unidad_imagen=0;
            $costo_garantia_servicio=0;
            $rendimiento_garantia_servicio=0;
            $costo_pagina_monocromatica=0;
            $costo_pagina_color=0;
            $tipo=0;
            $costo_toner=0;
            $rendimiento_toner=0;

            $historialid=$this->ModeloCatalogos->Insert('rentas_historial',array('rentas'=>$renta));
        //=======================================
            $DATAe = json_decode($arrayequipos);
            for ($i=0;$i<count($DATAe);$i++) {
                $equipo = $this->Equipos_model->getEquipoPorId($DATAe[$i]->equipo); 
                //$precio = $equipo->costo_renta;
                $equipoInd=$DATAe[$i]->equipo;
                $direcc=$DATAe[$i]->dic;
                $detallesCotizacion = array('idRenta' => $renta,
                                            'idEquipo' => $DATAe[$i]->equipo,
                                            'cantidad' => 1,
                                            'serie_bodega' => $DATAe[$i]->bodega,
                                            'modelo' => $equipo->modelo,
                                            'especificaciones_tecnicas' => $equipo->especificaciones_tecnicas,
                                            'precio' => $equipo->costo_renta,
                                            'costo_unidad_imagen' => $costo_unidad_imagen,
                                            'rendimiento_unidad_imagen' => $rendimiento_unidad_imagen,
                                            'costo_garantia_servicio' => $costo_garantia_servicio,
                                            'rendimiento_garantia_servicio' => $rendimiento_garantia_servicio,
                                            'costo_pagina_monocromatica' => $costo_pagina_monocromatica,
                                            'costo_pagina_color' => $costo_pagina_color,
                                            'excedente' => $equipo->excedente,
                                            'tipotoner' => $tipo,
                                            'costo_toner' => $costo_toner,
                                            'rendimiento_toner' => $rendimiento_toner,
                                            'estatus_suspendido'=>0, 
                                            'fecha_fin_susp'=>null,  
                                            'serie_estatus'=>0,
                                            'direcc_temp'=>$direcc        
                                );

                $idhistorialequipos=$this->ModeloCatalogos->Insert('rentas_has_detallesEquipos',$detallesCotizacion);

                $this->ModeloCatalogos->Insert('rentas_historial_equipos',array('historialid'=>$historialid,'equipo'=>$idhistorialequipos));
                
                $DATAc=$DATAe[$i]->consumibles;
                for ($j=0;$j<count($DATAc);$j++) {

                    $idconsumuble=$DATAc[$j]->cons;
                    $idbodega=$DATAc[$j]->bode_c;
                    //log_message('error', 'idconsumible '.$idconsumuble);
                    $resultadoaddcon=$this->ModeloCatalogos->consumibleagregadoselected($idconsumuble,$equipo->id);
                    foreach ($resultadoaddcon->result() as $item2) {
                        $modelo = $item2->modelo; 
                        $tipo = $item2->tipo;
                        $costo_toner = $item2->poliza;
                        $rendimiento_toner = $item2->rendimiento_toner;
                        $costo_unidad_imagen = $item2->costo_unidad_imagen;
                        $rendimiento_unidad_imagen = $item2->rendimiento_unidad_imagen;
                        $costo_garantia_servicio = $item2->costo_garantia_servicio;
                        $rendimiento_garantia_servicio = $item2->rendimiento_garantia_servicio;
                        $costo_pagina_monocromatica = $item2->costo_pagina_monocromatica;
                        $costo_pagina_color = $item2->costo_pagina_color;
                    }

                    $datachfc["idequipo"]=$equipoInd;
                    $datachfc["rowequipo"]=$idhistorialequipos;
                    $datachfc["id_consumibles"]=$idconsumuble;
                    $datachfc["costo_toner"]=$costo_toner;
                    $datachfc["rendimiento_toner"]=$rendimiento_toner;
                    $datachfc["rendimiento_unidad_imagen"]=$rendimiento_unidad_imagen;
                    $datachfc["idrentas"]=$renta;
                    $datachfc["cantidad"]=1;

                    $idrentaconsumible=$this->ModeloCatalogos->Insert('rentas_has_detallesequipos_consumible',$datachfc);

                    $this->ModeloCatalogos->updatestock2('consumibles_bodegas','total','-',1,'consumiblesId',$idconsumuble,'bodegaId',$idbodega);
                    $this->ModeloCatalogos->Insert('bitacora_movimientos',array('entrada_salida'=>0,'descripcion_evento'=>'Solicitud de consumible para rentas: '.$renta,'personal'=>$this->idpersonal,'tipo'=>4,'id_item'=>$idconsumuble,'modeloId'=>$idconsumuble,'modelo'=>$modelo,'bodega_o'=>$idbodega,'clienteId'=>$clienteid_renta));

                    $folios=$this->addcontrolfolio($renta,1,$clienteid_renta,$idconsumuble,$idrentaconsumible);

                    $this->ModeloCatalogos->Insert('rentas_historial_consumibles',array('cantidad'=>1,'historialid'=>$historialid,'consumible'=>$idconsumuble,'folios'=>$folios));
                }
                $DATAa=$DATAe[$i]->accesorios;
                for ($k=0;$k<count($DATAa);$k++) {
                    $dataas["idrentas"]=$renta;
                    $dataas['id_equipo']=$equipoInd;
                    $dataas["rowequipo"]=$idhistorialequipos;
                    $dataas['id_accesorio']=$DATAa[$k]->acce;
                    $dataas["cantidad"]=1;
                    $id_accesoriod=$this->ModeloCatalogos->Insert('rentas_has_detallesEquipos_accesorios',$dataas);

                    $this->ModeloCatalogos->Insert('rentas_historial_accesorios',array('cantidad'=>1,'historialid'=>$historialid,'accesorio'=>$DATAa[$k]->acce,'id_accesoriod'=>$id_accesoriod));
                }
            }
            echo $historialid;
    }
    function actualizaproductostable(){
        $id = $this->input->post('id');
        $resulte = $this->Rentas_model->getcontratos_id($id);
        $html='';
        $cont=0;
        foreach ($resulte as $item){ 
            $cont++;
        }
        foreach ($resulte as $item){ 
            $html.='<tr>';
            $html.='<td>'.$item->id.'</td>';
            $html.='<td>'.$item->empresa.'</td>';
            $html.='<td>'.$item->modelo.'</td>';
            $html.='<td>'.$item->cantidad.'</td>';
            $html.='<td>';
                    //if($item->estatus_suspendido==0){
                        $html.='<button type="button" class="btn-floating waves-effect waves-light cyan btn tooltipped suspender" data-position="bottom" data-delay="50" data-tooltip="Suspender" onclick="suspender('.$item->idRD.')";><i class="material-icons">hourglass_empty</i></button>';
                    //}
                    /*
                    //if($item->estatus_suspendido==1){
                        $html.='<button type="button" class="btn-floating waves-effect waves-light cyan btn tooltipped suspender" data-position="bottom" data-delay="50" data-tooltip="Activar" onclick="activar('.$item->idRD.')";>
                          <i class="material-icons">check_box</i>
                        </button>';
                    //}
                    */
                    if($cont>1){
                     $html.='<a class="btn-floating waves-effect waves-light red accent-2 btn tooltipped deletee" data-position="bottom" data-delay="50" data-tooltip="Eliminar" onclick="eliminar('.$item->idRD.')";>                          <i class="material-icons">delete_forever</i></a>';
                    }else{
                        $html.='<a class="btn-floating waves-effect  waves-light red accent-2 btn tooltipped deletee" data-position="bottom" data-delay="50" data-tooltip="Eliminar" onclick="eliminar('.$item->idRD.')";><i class="material-icons">delete_forever</i></a>';    
                    }
                    /*
                    $html.='<button type="button" class="btn-floating waves-effect waves-light green darken-1 btn tooltipped cambiar" data-position="bottom" data-delay="50" data-tooltip="Cambio" onclick="cambiar('.$item->idRD.')";>
                          <i class="material-icons">swap_horiz</i>
                        </button>';
                    */
            $html.='</td>';
            $html.='</tr>';
            
        }
        echo $html;
    }
    function cancelar(){
        $id = $this->input->post('contratoId');
        $this->ModeloCatalogos->updateCatalogo('rentas',array('estatus'=>0),array('id'=>$id));
        $this->ModeloCatalogos->updateCatalogo('contrato',array('estatus'=>0,'personalelimino'=>$this->idpersonal),array('idRenta'=>$id));
        
        $result=$this->ModeloCatalogos->getselectwheren('rentas_has_detallesEquipos',array('idRenta'=>$id));
        foreach ($result->result() as $item) {
            $result2=$this->ModeloCatalogos->getselectwheren('asignacion_series_r_equipos',array('id_equipo'=>$item->id));
            foreach ($result2->result() as $item2) {
                $this->ModeloCatalogos->updateCatalogo('series_productos',array('status'=>0,'bodegaId'=>2,'usado'=>1),array('serieId'=>$item2->serieId));
                $this->ModeloCatalogos->updateCatalogo('asignacion_ser_contrato_a_e',array('status'=>3,'motivosuspencion'=>'Eliminacion y/o Cambio'),array('serieId'=>$item2->serieId,'idequipo'=>$item->id,'fecha >='=>$this->fechahoy,'status'=>0));
                $this->Configuraciones_model->registrarmovimientosseries($item2->serieId,1);
                //1 equipo, 2 acesorio, 3 refaccion,4 consumible    
                $this->ModeloCatalogos->Insert('bitacora_movimientos',array('id_item'=>0,'cantidad'=>1,'modeloId'=>$item->idEquipo,'tipo'=>1,'entrada_salida'=>1,'personal'=>$this->idpersonal,'descripcion_evento'=>'devolucion por cancelacion renta '.$id,'serieId'=>$item2->serieId));
            }
            
        }
        $result=$this->ModeloCatalogos->getselectwheren('rentas_has_detallesEquipos_accesorios',array('idrentas'=>$id));
        foreach ($result->result() as $item) {
            $result2=$this->ModeloCatalogos->getselectwheren('asignacion_series_r_accesorios',array('id_accesorio'=>$item->id_accesorio));
            foreach ($result2->result() as $item2) {
                $this->ModeloCatalogos->updateCatalogo('series_accesorios',array('status'=>0),array('serieId'=>$item2->serieId));
                $this->Configuraciones_model->registrarmovimientosseries($item2->serieId,2);
                //1 equipo, 2 acesorio, 3 refaccion,4 consumible    
                $this->ModeloCatalogos->Insert('bitacora_movimientos',array('id_item'=>0,'cantidad'=>1,'modeloId'=>$item->id_accesorio,'tipo'=>2,'entrada_salida'=>1,'personal'=>$this->idpersonal,'descripcion_evento'=>'devolucion por cancelacion renta '.$id,'serieId'=>$item2->serieId));
            }
            
        }
        /*
        $result=$this->ModeloCatalogos->getselectwheren('rentas_has_detallesEquipos',array('idRenta'=>$id));
        foreach ($result->result() as $item) {
            $result2=$this->ModeloCatalogos->getselectwheren('asignacion_series_r_refacciones',array('id_refaccion'=>$item->id));
            foreach ($result2->result() as $item2) {
                $this->ModeloCatalogos->updateCatalogo('series_refacciones',array('status'=>0),array('serieId'=>$item2->serieId));
                $this->Configuraciones_model->registrarmovimientosseries($item2->serieId,3);
            }
            
        }
        */
    }
    function historialprefacturas(){
        $params = $this->input->post();
        $idrentas=$params['idrenta'];
        $result=$this->ModeloCatalogos->getselectwheren('rentas_historial',array('rentas'=>$idrentas,'activo'=>1));
         $html='<table class="striped" style="width:100%;" id="historialprefacturas">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Fecha</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>';
                foreach ($result->result() as $item) {
                    if($item->e_entrega==1){
                        $btndisabled='disabled';
                    }else{
                        $btndisabled='';
                    }
                    $html.='<tr>
                        <td>'.$item->id.'</td>
                        <td>'.$item->reg.'</td>
                        <td>
                            <a class="btn-floating amber tooltipped" data-position="top" data-delay="50" 
                                data-tooltip="Prefactura" onclick="prefacturah('.$item->rentas.','.$item->id.')">
                                <i class="material-icons">assignment</i>\
                            </a>
                            <button class="btn-floating red tooltipped" '.$btndisabled.'><i class="material-icons" onclick="eliminarpreh('.$item->id.')">clear</i></button>
                        </td>
                    </tr>';
                }
        $html.='</tbody></table>';
        echo $html;
    }
    function eliminarserviciosposteriores($idrenta){// esta funcion tiene que se la misma a la que se encuentra en el controlador de contratos
        $rowcontrato=$this->ModeloCatalogos->getselectwheren('contrato',array('idRenta'=>$idrenta));
        $idcontrato=0;
        foreach ($rowcontrato->result() as $item) {
            $idcontrato=$item->idcontrato;
        }
        $rowcontratoservicios=$this->ModeloCatalogos->servicioscontratosactivos($idcontrato);
        foreach ($rowcontratoservicios->result() as $item) {
            //==============================================
                $this->ModeloCatalogos->db4_updateCatalogo('asignacion_ser_contrato_a_e',array('activo'=>0,'infodelete'=>$this->idpersonal),array('asignacionId'=>$item->asignacionId,'status'=>0,'fecha >'=>$this->fechahoy));
            //==============================================
        }
    }
    function addcontrolfolio($idrenta,$cantidad,$clienteid_renta,$id_consumibles,$idrelacion){
        $foliostext='';
        $aux = 0;
        for ($i=1; $i <= $cantidad; $i++) { 
            $contr_folio = $this->ModeloCatalogos->contratofolio($clienteid_renta);
            foreach ($contr_folio as $itemc) {
                $aux = $itemc->folio;
            }
            /*
            $folio = $aux +1; 
            /// $idretashdc = $item->id;
            $foliotext = $idrenta.'T'.str_pad($folio, 3, "0", STR_PAD_LEFT);
            $result_ft=$this->ModeloCatalogos->getselectwheren('contrato_folio',array('foliotext'=>$foliotext));
            if($result_ft->num_rows()>0){
                $folio+1;
                $foliotext = $idrenta.'T'.str_pad($folio, 3, "0", STR_PAD_LEFT);
            }
            */
            $ft_result=$this->Rentas_model->generadorfolio($aux,$idrenta);
            $foliotext=$ft_result['foliotext'];
            $folio=$ft_result['folio'];

            $foliostext .= $foliotext.' ';
            $data_folio = array('idrenta'=>$idrenta,'idCliente'=>$clienteid_renta,'idconsumible'=>$id_consumibles,'folio'=>$folio,'foliotext'=>$foliotext,'idrelacion'=>$idrelacion,'creado_personal'=>$this->idpersonal);
            $this->ModeloCatalogos->Insert('contrato_folio',$data_folio);
        }
        return $foliostext;
    }
    function solicitudentregatoner(){
        $params = $this->input->post();
        $idRenta=$params['idRenta'];
        $tipo=$params['tipo'];
        $contrato_renta = $this->ModeloCatalogos->contrato_renta($idRenta);
        foreach ($contrato_renta as $item) {
            $clienteid_renta = $item->idCliente;
        }
        $dataarray = array('idrentas' => $idRenta);
        $resulthdconsumibles = $this->ModeloCatalogos->getselectwheren('rentas_has_detallesequipos_consumible',$dataarray);
        //=======================================================================
            $folio = 0;
            $contr_folio = $this->ModeloCatalogos->contratofolio($clienteid_renta);
            foreach ($contr_folio as $itemc) {
                $folio = $itemc->folio;
            }
            $datafoliosrray=array();
        //=======================================================================

        $row=0;
        $idroweq=0;
        foreach ($resulthdconsumibles->result() as $item) {
            
            $cantidad       = $item->cantidad;
            $id_consumibles = $item->id_consumibles;
            $bodega         = $item->bodega;
            //log_message('error', 'cantidad :'.$cantidad);
            for ($i=1; $i <= $cantidad; $i++) { 
                
                $folio = $folio +1; 
                /// $idretashdc = $item->id;
                $id_rentas_c = $item->idrentas;
                
                $idequipo = $item->idequipo;
                //===========================================
                // aqui se sacara la serie del equipo para agregarla a la tabla de control de folio
                // despues en la consulta se ara la consulta por el id se serie para desagrupar los folios de los consumibles
                // el id de la serie se sacara de la tabla "asignacion_series_r_equipos" dependiendo del $idequipo
                // si en dado caso es un equipo que su cantidad sea de 2 o mas por obvias razones va atener mas series y en este caso
                //===========================================
                if($idroweq!=$item->id){
                    $folioinicial='I';
                    $idroweq=$item->id;
                }else{
                    $folioinicial='';
                }
                $foliotext = $id_rentas_c.'T'.str_pad($folio, 3, "0", STR_PAD_LEFT).$folioinicial;   
                $data_folio = array(
                                    'idrenta'=>$id_rentas_c,
                                    'idCliente'=>$clienteid_renta,
                                    'idconsumible'=>$id_consumibles,
                                    'folio'=>$folio,
                                    'foliotext'=>$foliotext,
                                    'status'=>$tipo,
                                    'idrelacion'=>$item->id,
                                    'creado_personal'=>$this->idpersonal
                                );
                //$this->ModeloCatalogos->Insert('contrato_folio',$data_folio);
                $datafoliosrray[]=$data_folio;

            }
            $this->ModeloCatalogos->updatestock2('consumibles_bodegas','total','-',$cantidad,'consumiblesId',$id_consumibles,'bodegaId',$bodega);
            //1 equipo, 2 acesorio, 3 refaccion,4 consumible    
            $this->ModeloCatalogos->Insert('bitacora_movimientos',array('id_item'=>0,'cantidad'=>$cantidad,'modeloId'=>$id_consumibles,'tipo'=>4,'entrada_salida'=>0,'personal'=>$this->idpersonal,'descripcion_evento'=>'salida renta '.$id_rentas_c,'serieId'=>0));
            $row++;
        }
        if(count($datafoliosrray)>0){
            $this->ModeloCatalogos->insert_batch('contrato_folio',$datafoliosrray);
        }
    }
    function agregarnuevoequipoext(){
        $data = $this->input->post();
        
        $arrayaccesorios = $data['arrayaccesorios'];
        unset($data['arrayaccesorios']);

        //=======================================
        $DATAa = json_decode($arrayaccesorios);
           // log_message('error', 'accesorios:acc'.$accesorios);
            //log_message('error', 'accesorios: data '.$DATAa);
            $renta=0;
            for ($j=0;$j<count($DATAa);$j++) {
                $renta=$DATAa[$j]->renta;
            }
            $historialid=$this->ModeloCatalogos->Insert('rentas_historial',array('rentas'=>$renta));
            for ($i=0;$i<count($DATAa);$i++) {
                $cantidad=$DATAa[$i]->piezas;
                $dataas["idrentas"]=$DATAa[$i]->renta;
                $dataas['id_equipo']=$DATAa[$i]->equipo;
                $dataas['id_accesorio']=$DATAa[$i]->accesorio;
                $dataas["cantidad"]=1;
                $dataas['rowequipo']=$DATAa[$i]->equiporow;
                for ($c=1; $c <= $cantidad; $c++) { 
                    $id_accesoriod=$this->ModeloCatalogos->db5_Insert('rentas_has_detallesEquipos_accesorios',$dataas);

                    $this->ModeloCatalogos->Insert('rentas_historial_accesorios',array('cantidad'=>1,'historialid'=>$historialid,'accesorio'=>$DATAa[$i]->accesorio,'id_accesoriod'=>$id_accesoriod));
                }
                
            }   
        //=======================================
        //$this->eliminarserviciosposteriores($renta);
            echo $historialid;
    }
    function deletehistoricopre($historialid){

        $result_rha = $this->ModeloCatalogos->getselectwheren('rentas_historial_accesorios',array('historialid'=>$historialid));
        foreach ($result_rha->result() as $itemha) {
            $id_accesoriod=$itemha->id_accesoriod;
            //rentas_has_detallesequipos_accesorios
        }
    }
    function datosordenar(){
        $idRenta = $this->input->post('idRenta');
        //log_message('error', 'idRenta :'.$idRenta);
        $rentaequipos = $this->Rentas_model->equiposrentas2($idRenta);
        $html='';
        $idequiporow=0;
        foreach ($rentaequipos as $itemeq) {
            $idequiporow_con=$itemeq->id;
            $id_serie = $itemeq->serieId;
            //ondragover ondragenter ondraglave
            $html.='<ul class="collapsible collapsible-accordion equipo_selected_'.$itemeq->id.'" data-collapsible="accordion" style="margin:0px;" ondrop="soltar(event)" ondragover="permitirSoltar(event,'.$itemeq->id.','.$itemeq->idEquipo.','.$id_serie.')"  data-equipose="'.$itemeq->modelo.' '.$itemeq->serie.'">
                        <li class="">
                            <div class="collapsible-header">
                                <i class="material-icons">print</i> '.$itemeq->modelo.' '.$itemeq->serie.'</div>
                            <div class="collapsible-body" style="display: none; padding:10px;">
                                <ul id="issues-collection" class="collection z-depth-1">
                                ';
                             
                                $rentaequipos_accesorios = $this->ModeloCatalogos->rentaequipos_accesorios1($itemeq->idRenta,$itemeq->idEquipo,$itemeq->id);
                                foreach ($rentaequipos_accesorios->result() as $itemacc) {
                                      $html.='
                                        <li class="collection-item accesorio_row_'.$itemacc->id_accesoriod.'" 
                                                draggable="true" ondragstart="arrastrar(event,'.$itemacc->id_accesoriod.',2)" data-acce="'.$itemacc->nombre.' '.$itemacc->series.'">
                                          <div class="row" style="margin: 0px;">
                                            <div class="col s12">
                                              <p class="collections-title" style="margin: 0px;"><i class="fa fa-solid fa-wrench"></i> '.$itemacc->cantidad.' <b>'.$itemacc->nombre.'</b> '.$itemacc->series.'</p>
                                            </div>
                                            
                                          </div>
                                        </li>
                                      ';
                                }
                                $idcontratofolio=0;
                                $rentaequipos_consumible = $this->ModeloCatalogos->rentaequipos_consumible1($itemeq->idRenta,$itemeq->idEquipo,$itemeq->id);
                                foreach ($rentaequipos_consumible->result() as $item) {
                                    $contrato_folio2 = $this->ModeloCatalogos->obtenerfoliosconsumiblesrenta2($itemeq->idRenta,$item->id_consumibles,$item->id,$id_serie); 
                                    foreach ($contrato_folio2->result() as $itemx) {
                                        if($idcontratofolio!=$itemx->idcontratofolio){
                                            $idcontratofolio=$itemx->idcontratofolio;
                                            $html.='
                                                <li class="collection-item consumible_row_'.$item->id.'" 
                                                        draggable="true" ondragstart="arrastrar(event,'.$item->id.',1)" data-consu="'.$item->modelo.' '.$itemx->foliotext.'" data-idcontrolfolio="'.$itemx->idcontratofolio.'">
                                                  <div class="row" style="margin: 0px;">
                                                    <div class="col s12">
                                                      <p class="collections-title" style="margin: 0px;"><i class="fas fa-palette fa-fw"></i> '.$item->parte.' <b>'.$item->modelo.'</b> '.$itemx->foliotext.'</p>
                                                    </div>
                                                    
                                                  </div>
                                                </li>
                                              ';
                                        }
                                          
                                          
                                    }
                                }
                              
                            

            $html.='</ul>       </div>
                        </li>
                    </ul>';

              $idequiporow=$itemeq->id;
        }
        $html.='<ul class="collapsible collapsible-accordion" data-collapsible="accordion" style="margin:0px;">
                        <li class="">
                            <div class="collapsible-header">
                                <i class="material-icons">print</i> Sin Relacion</div>
                            <div class="collapsible-body" style="display: none; padding:10px;">
                                <ul id="issues-collection" class="collection z-depth-1">
                                ';
                             
                                $rentaequipos_accesorios = $this->ModeloCatalogos->rentaequipos_accesorios0($idRenta,0,0);
                                foreach ($rentaequipos_accesorios->result() as $itemacc) {
                                      $html.='
                                        <li class="collection-item accesorio_row_'.$itemacc->id_accesoriod.'" 
                                                draggable="true" ondragstart="arrastrar(event,'.$itemacc->id_accesoriod.',2)" data-acce="'.$itemacc->nombre.' '.$itemacc->series.'">
                                          <div class="row" style="margin: 0px;">
                                            <div class="col s12">
                                              <p class="collections-title" style="margin: 0px;"><i class="fa fa-solid fa-wrench"></i>'.$itemacc->cantidad.' <b>'.$itemacc->nombre.'</b> '.$itemacc->series.'</p>
                                            </div>
                                            
                                          </div>
                                        </li>
                                      ';
                                }
                                $rentaequipos_consumible = $this->ModeloCatalogos->rentaequipos_consumible0($idRenta,0,0);
                                foreach ($rentaequipos_consumible->result() as $item) {
                                    $contrato_folio2 = $this->ModeloCatalogos->obtenerfoliosconsumiblesrenta($idRenta,$item->id_consumibles,$item->id); 
                                    foreach ($contrato_folio2->result() as $itemx) {
                                          if($itemx->idrelacion>0){
                                            $html.='
                                                <li class="collection-item consumible_row_'.$item->id.'" 
                                                        draggable="true" ondragstart="arrastrar(event,'.$item->id.',1)" data-consu="'.$item->modelo.' '.$itemx->foliotext.'" data-idcontrolfolio="'.$itemx->idcontratofolio.'">
                                                  <div class="row" style="margin: 0px;">
                                                    <div class="col s12">
                                                      <p class="collections-title" style="margin: 0px;"><i class="fas fa-palette fa-fw"></i> '.$item->parte.' <b>'.$item->modelo.'</b> '.$itemx->foliotext.'</p>
                                                    </div>
                                                    
                                                  </div>
                                                </li>
                                              ';
                                          }
                                          
                                    }
                                }
                              
                            

            $html.='</ul>       </div>
                        </li>
                    </ul>';
        
        echo $html;
    }
    function ordenaraccesorio(){
        $params=$this->input->post();
        $idequipo = $params['idequipo'];
        $modeloeq = $params['modeloeq'];
        $idacce = $params['idacce'];
        $this->ModeloCatalogos->updateCatalogo('rentas_has_detallesEquipos_accesorios',array('rowequipo'=>$idequipo,'id_equipo'=>$modeloeq),array('id_accesoriod'=>$idacce));
    }
    function ordenarconsumible(){
        $params=$this->input->post();
        $idequipo = $params['idequipo'];
        $modeloeq = $params['modeloeq'];
        $serieid = $params['serieid'];
        $idtoner = $params['idtoner'];
        $idcontrolfolio = $params['idcontrolfolio'];
        $this->ModeloCatalogos->updateCatalogo('rentas_has_detallesequipos_consumible',array('rowequipo'=>$idequipo,'idequipo'=>$modeloeq),array('id'=>$idtoner));
        $this->ModeloCatalogos->updateCatalogo('contrato_folio',array('idrelacion'=>$idtoner,'serieId'=>$serieid),array('idcontratofolio'=>$idcontrolfolio));
    }
    function savecondiciones(){
        $params=$this->input->post();
        if(isset($params['con_preid'])){
            $con_preid=$params['con_preid'];
        }else{
            $con_preid=0;
        }
        $con_id = $params['con_id'];
        $equipos = $params['equipos'];

        $array_generar=array(
            'renta'=>$params['con_renta'],
            'tipo'=>$params['con_tipo'],
            'fechainicial'=>$params['con_fechainicial'],
            'fechafinal'=>$params['con_fechafinal'],
            'renta_m'=>$params['con_renta_m'],
            'click_m'=>$params['con_click_m'],
            'excedente_m'=>$params['con_excedente_m'],
            'renta_c'=>$params['con_renta_c'],
            'click_c'=>$params['con_click_c'],
            'excedente_c'=>$params['con_excedente_c'],
            'comentario'=>$params['comentario']
        );
        if($con_id>0){
            $this->ModeloCatalogos->updateCatalogo('rentas_condiciones',$array_generar,array('id'=>$con_id));
        }else{
            $con_id=$this->ModeloCatalogos->Insert('rentas_condiciones',$array_generar);
        }
        $DATA = json_decode($equipos);
        for ($i=0;$i<count($DATA);$i++) {
            if($DATA[$i]->id>0){
                $datased['renta_m']=$DATA[$i]->renta_m;
                $datased['click_m']=$DATA[$i]->click_m;
                $datased['excedente_m']=$DATA[$i]->excedente_m;
                $datased['renta_c']=$DATA[$i]->renta_c;
                $datased['click_c']=$DATA[$i]->click_c;
                $datased['excedente_c']=$DATA[$i]->excedente_c;
                $this->ModeloCatalogos->updateCatalogo('rentas_condiciones_detalle',$datased,array('id'=>$DATA[$i]->id));
            }else{
                $datasi['idc']=$con_id;
                $datasi['equiporow']=$DATA[$i]->equiporow;
                $datasi['serieId']=$DATA[$i]->serieId;
                $datasi['renta_m']=$DATA[$i]->renta_m;
                $datasi['click_m']=$DATA[$i]->click_m;
                $datasi['excedente_m']=$DATA[$i]->excedente_m;
                $datasi['renta_c']=$DATA[$i]->renta_c;
                $datasi['click_c']=$DATA[$i]->click_c;
                $datasi['excedente_c']=$DATA[$i]->excedente_c;
                $this->ModeloCatalogos->Insert('rentas_condiciones_detalle',$datasi);
            }
            
        }
        if($con_preid>0){
            $this->ModeloCatalogos->updateCatalogo('alta_rentas_prefactura',array('idcondicionextra'=>$con_id),array('prefId'=>$con_preid));
        }
    }
    function listcondiciones(){
        $params=$this->input->post();
        $idRenta = $params['idRenta'];
        $list_cont=$this->ModeloCatalogos->getselectwheren('rentas_condiciones',array('renta'=>$idRenta,'activo'=>1));
        $html='';

        $html.='<table class="striped" id="table_individual">
                    <thead><tr><th>Fecha de registro</th><th>Tipo</th><th>Fecha Inicial</th><th>Acciones</th></tr></thead><tbody>';
        foreach ($list_cont->result() as $item) {
            $ltipo='';
            if($item->tipo==2){
                $ltipo='Global';
            }
            if($item->tipo==1){
                $ltipo='Individual';
            }
            $html.='<tr><th>'.$item->reg.'</th><th>'.$ltipo.'</th><th>'.$item->fechainicial.'</th><th><a class="btn-floating green tooltipped edit" onclick="viewcondi('.$item->id.')"><i class="material-icons">format_list_numbered</i></a></th></tr>';
        }
        $html.='</tbody></table>';
        echo $html;
    }
    function viewcondi(){
        $params=$this->input->post();
        $id = $params['id'];
        $list_cont=$this->ModeloCatalogos->getselectwheren('rentas_condiciones',array('id'=>$id,'activo'=>1));
        $list=$this->ModeloCatalogos->getselectwheren('rentas_condiciones_detalle',array('idc'=>$id));

        $array = array(
                'datos'=>$list_cont->row(),
                'datosdll'=>$list->result(),
           );
        echo json_encode($array);
    }
    function viewcondi2(){
        $params=$this->input->post();
        $id = $params['id'];
        $prefId = $params['prefId'];

        $list_cont=$this->ModeloCatalogos->getselectwheren('rentas_condiciones',array('id'=>$id,'activo'=>1));
        $strq="SELECT rcd.*, serp.serie,eq.modelo
                FROM rentas_condiciones_detalle as rcd
                INNER JOIN series_productos as serp ON serp.serieId=rcd.serieId
                INNER JOIN equipos as eq on eq.id=serp.productoid
                WHERE rcd.idc='$id'";
        $list = $this->db->query($strq);


        if($id>0){
            $datos=$list_cont->row();
            $datosdll=$list->result();
        }else{
            $strq1="SELECT alre.* FROM alta_rentas_prefactura as arp INNER JOIN alta_rentas as alre on alre.id_renta=arp.id_renta WHERE arp.prefId='$prefId'";
            $list_cont = $this->db->query($strq1);
            $list_cont=$list_cont->row();
            $datos=array(
                        'tipo'=>$list_cont->tipo2,
                        'renta_m'=>$list_cont->monto,
                        'click_m'=>$list_cont->clicks_mono,
                        'excedente_m'=>$list_cont->precio_c_e_mono,
                        'renta_c'=>$list_cont->montoc,
                        'click_c'=>$list_cont->precio_c_e_color,
                        'excedente_c'=>$list_cont->precio_c_e_color,
                        'comentario'=>''
                        );
            //=================================================
            $strq2="SELECT arpd.productoId as equiporow,arpd.serieId,arp.id_renta,areq.renta as renta_m,areq.clicks_mono as click_m,areq.precio_c_e_mono as excedente_m,areq.rentac as renta_c,areq.clicks_color as click_c, areq.precio_c_e_color as excedente_c,eq.modelo,serpr.serie
                FROM alta_rentas_prefactura_d as arpd
                INNER JOIN alta_rentas_prefactura as arp on arp.prefId=arpd.prefId
                INNER JOIN alta_rentas_equipos as areq on areq.id_renta=arp.id_renta AND areq.id_equipo=arpd.productoId AND areq.serieId=arpd.serieId
                INNER JOIN series_productos as serpr on serpr.serieId=arpd.serieId
                INNER JOIN equipos as eq on eq.id=serpr.productoid
                WHERE arpd.prefId='$prefId'
                GROUP BY arpd.productoId,arpd.serieId";
            $list = $this->db->query($strq2);
            $datosdll=$list->result();

        }


        $array = array(
                'datos'=>$datos,
                'datosdll'=>$datosdll,
           );
        echo json_encode($array);
    }
    function obtenerequipos_de_contratos(){
        $params = $this->input->post();
        $idRenta = $params['idRenta'];

        $html='<table class="table bordered" id="tableequipos">';
        //===============================================
            //$respuestarentae=$this->ModeloCatalogos->db10_getselectwheren('rentas_has_detallesEquipos',array('idRenta'=>$idRenta));
            $respuestarentae=$this->Rentas_model->get_euipos_renta_historial($idRenta);
            foreach ($respuestarentae->result() as $item) {
                $respuestarentaess=$this->ModeloCatalogos->db2_getselectwheren('asignacion_series_r_equipos',array('id_equipo'=>$item->id,'activo'=>1));
                    $modeloe=$item->modelo;
                foreach ($respuestarentaess->result() as $itemss) {
                    $serie=$this->obtenerequiposserie($itemss->serieId);
                    if($itemss->ser_instala==1 || $item->id_rhe_eq>0){
                        $disabled=' disabled ';
                    }else{
                        $disabled='';
                    }
                    
                    $html.='<tr class="dir_row"><td style="width: 95px;">            
                                <input type="checkbox" class="serie_check serie_check_'.$itemss->id_equipo.'_'.$itemss->serieId.'" id="serie_check_'.$itemss->serieId.'" value="'.$itemss->serieId.'"  data-serie="'.$serie.'" '.$disabled.'>
                                    <label for="serie_check_'.$itemss->serieId.'"></label>
                                    <a class="waves-effect cyan btn-bmz consultar_fechas_agenda_'.$itemss->id_equipo.'_'.$itemss->serieId.'"  onclick="consultarfechasagenda('.$itemss->id_equipo.')" style="display:none"><i class="fas fa-calendar-alt" style="font-size: 20px;"></i></a>
                                    <input type="hidden" class="ss" value="'.$itemss->id_equipo.'" id="equipoid">
                                    <!--<input type="hidden" class="ss" value="'.$itemss->id_equipo.'" id="equiporow">-->
                                    <input type="hidden" value="'.$itemss->serieId.'" id="serieid">
                                </td>
                                <td>'.$modeloe.'</td>
                                <td>'.$serie.'</td>
                            </tr>';
                }
                
            }
        //===============================================
        $html.='</table>';

        echo $html;
    }
    function obtenerequiposserie($id){
        $respuestarentae=$this->ModeloCatalogos->db2_getselectwheren('series_productos',array('serieId'=>$id));
        $serie='';
        foreach ($respuestarentae->result() as $item) {
            $serie=$item->serie;
        }
        return $serie;
    }
    function generacionpreequipos(){
        $params = $this->input->post();
        $equipos = $params['equipos'];
        $idRenta = $params['idRenta'];

        $historialid=$this->ModeloCatalogos->Insert('rentas_historial',array('rentas'=>$idRenta));

        $DATAe = json_decode($equipos);
        for ($i=0;$i<count($DATAe);$i++) {
            $equipoid=$DATAe[$i]->equipoid;
            $serieid=$DATAe[$i]->serieid;

            $this->ModeloCatalogos->Insert('rentas_historial_equipos',array('historialid'=>$historialid,'equipo'=>$equipoid));
            $result_acc=$this->ModeloCatalogos->getselectwheren('rentas_has_detallesEquipos_accesorios',array('idrentas'=>$idRenta,'rowequipo'=>$equipoid));
            $result_con=$this->ModeloCatalogos->getselectwheren('rentas_has_detallesequipos_consumible',array('idrentas'=>$idRenta,'rowequipo'=>$equipoid));
            foreach ($result_acc->result() as $itemac) {
                $this->ModeloCatalogos->Insert('rentas_historial_accesorios',array('cantidad'=>$itemac->cantidad,'historialid'=>$historialid,'accesorio'=>$itemac->id_accesorio,'id_accesoriod'=>$itemac->id_accesoriod));
            }
            foreach ($result_con->result() as $itemcon) {
                $idrow_consu=$itemcon->id;
                $result_cf=$this->ModeloCatalogos->getselectwheren('contrato_folio',array('idrelacion'=>$idrow_consu));
                $folios='';
                foreach ($result_cf->result() as $item_cf) {
                    $folios=$item_cf->foliotext;
                }

                $this->ModeloCatalogos->Insert('rentas_historial_consumibles',array('cantidad'=>$itemcon->cantidad,'historialid'=>$historialid,'consumible'=>$itemcon->id_consumibles,'folios'=>$folios));
            }
            $this->ModeloCatalogos->updateCatalogo('asignacion_series_r_equipos',array('ser_instala'=>1),array('id_equipo'=>$equipoid,'serieId'=>$serieid));

        }

        $array=array('historialid'=>$historialid);
        echo json_encode($array);
    }
    function agregarequipomanualmente($idequipo,$idserie,$idrenta){
        $equipo = $this->Equipos_model->getEquipoPorId($idequipo); 
                //$precio = $equipo->costo_renta;
            //==========================================
                    $costo_unidad_imagen=0;
                    $rendimiento_unidad_imagen=0;
                    $costo_garantia_servicio=0;
                    $rendimiento_garantia_servicio=0;
                    $costo_pagina_monocromatica=0;
                    $costo_pagina_color=0;
                    $tipo=0;
                    $costo_toner=0;
                    $rendimiento_toner=0;
            //=================================
                $detallesCotizacion = array('idRenta' => $idrenta,
                                            'idEquipo' => $idequipo,
                                            'cantidad' => 1,
                                            'serie_bodega' => 1,
                                            'modelo' => $equipo->modelo,
                                            'especificaciones_tecnicas' => $equipo->especificaciones_tecnicas,
                                            'precio' => $equipo->costo_renta,
                                            'costo_unidad_imagen' => $costo_unidad_imagen,
                                            'rendimiento_unidad_imagen' => $rendimiento_unidad_imagen,
                                            'costo_garantia_servicio' => $costo_garantia_servicio,
                                            'rendimiento_garantia_servicio' => $rendimiento_garantia_servicio,
                                            'costo_pagina_monocromatica' => $costo_pagina_monocromatica,
                                            'costo_pagina_color' => $costo_pagina_color,
                                            'excedente' => $equipo->excedente,
                                            'tipotoner' => $tipo,
                                            'costo_toner' => $costo_toner,
                                            'rendimiento_toner' => $rendimiento_toner,
                                            'estatus_suspendido'=>0, 
                                            'fecha_fin_susp'=>null,  
                                            'serie_estatus'=>0,
                                            'direcc_temp'=>''        
                                );

                $idhistorialequipos=$this->ModeloCatalogos->Insert('rentas_has_detallesEquipos',$detallesCotizacion);

                //=====================================================================================
                $id_cliente = $this->Rentas_model->idcliente_idrenta($idrenta); 
                foreach ($id_cliente as $item) {
                    $cliente_id = $item->idCliente;// id del cliente
                }
                $detallesequipos = array(
                'id_equipo'=>$idhistorialequipos,
                'serieId'=>$idserie,
                'folio_equipo'=>0,
                'idCliente'=>$cliente_id,
                'poreliminar'=>1,'comenext'=>'....','ubicacion'=>'....');



                $this->ModeloCatalogos->Insert('asignacion_series_r_equipos',$detallesequipos);
    }
}    
