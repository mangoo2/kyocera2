var base_url = $('#base_url').val();
var codigocarga = $('#codigocarga').val();
$(document).ready(function(){
    $('.chosen-select').chosen({width: "90%"});
    $('#tipofacturasstatus').select2({width: '100%'});
	/*$('#cancelaF').click(function(event) {
		console.log("cancelar click");
		$("#modal_cancelar").modal({
	        show:true,
	        backdrop: 'static',
	        keyboard: false
	    });
	});*/

});
/*
function cancelar(idfact,idcont){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de Cancelar Factura?',
        type: 'red',
        typeAnimated: true,
        buttons: 
        {
            confirmar: function () 
            {
                $.ajax({
                    url: base_url+"index.php/Configuracionrentas/cancelarFactura/",
                    type: 'post',
                    data: { idfactura:idfact, idcontrato:idcont },
                    success: function (response) 
                    {
                    	console.log(response);
                    	listfacturas(idcont);
                    },
                    error: function(response)
                    {
                        $.alert({
                                boxWidth: '30%',
                                useBootstrap: false,
                                title: 'Error!',
                                content: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema'});   
                            setTimeout(function(){ 
                            //location.reload();
                         }, 2000);
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}
*/
function cancelar(idfact,idcont){
    var html='<div class="row">\
                <div class="col s12 m12">\
                    ¿Está seguro de realizar la Cancelacion de la factura?<br>Se necesita permisos de administrador\
                </div>\
                <div class="col s12 m12">\
                    <input type="password" placeholder="Contraseña" id="contrasena" class="name form-control" required/>\
                </div>\
              </div>\
              <div class="row">\
                <div class="col s6 m6">\
                    <label>Motivo Cancelación</label>\
                    <select id="motivocancelacion" class="form-control-bmz" onchange="mocance('+idfact+')">\
                        <option value="00" disabled selected> Seleccione una opción</option>\
                        <option value="01" >01 Comprobante emitido con errores con relación</option>\
                        <option value="02" >02 Comprobante emitido con errores sin relación</option>\
                        <option value="03" >03 No se llevó a cabo la operación</option>\
                        <option value="04" >04 Operación nominativa relacionada en una factura global</option>\
                    </select>\
                </div>\
                <div class="col s6 m6 class_foliorelacionado" style="display:none">\
                    <label>Folio Relacionado</label>\
                    <input type="text" id="foliorelacionado" class="form-control-bmz" placeholder="111AAA1A-1AA1-1A11-11A1-11A1AA111A11">\
                </div>\
              </div>';
    $.confirm({
        boxWidth: '55%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: html,
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var pass=$('#contrasena').val();
                var motivo = $('#motivocancelacion option:selected').val();
                var uuidrelacionado=$('#foliorelacionado').val();
                if (pass!='') {
                     $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Sistema/solicitarpermiso",
                        data: {
                            pass:pass
                        },
                        success: function (response){
                                var respuesta = parseInt(response);
                                if (respuesta==1) {
                //===================================================
                    //var facturaslis = $("#tabla_facturacion tbody > tr");
                    var DATAa  = [];
                    var num_facturas_selected=0;
                    //facturaslis.each(function(){  
                        //if ($(this).find("input[class*='facturas_checked']").is(':checked')) {
                            num_facturas_selected++;
                            item = {};                    
                            item ["FacturasIds"]  = idfact;
                            
                            DATAa.push(item);
                        //}       
                        
                    //});
                    INFOa  = new FormData();
                    aInfoa   = JSON.stringify(DATAa);
                    //console.log(aInfoa);
                    if (num_facturas_selected==1) {
                        $.ajax({
                            type:'POST',
                            url: base_url+"index.php/Configuracionrentas/cancelarCfdi",
                            data: {
                                facturas:aInfoa,
                                motivo:motivo,
                                uuidrelacionado:uuidrelacionado
                            },
                            success:function(response){  
                                console.log(response);
                                var array = $.parseJSON(response);
                                if (array.resultado=='error') {
                                    swal({    title: "Error "+array.CodigoRespuesta+"!",
                                                  text: array.MensajeError,
                                                  type: "warning",
                                                  showCancelButton: false
                                     });
                                }else{
                                    swal("Éxito!", "Se ha Cancelado la factura", "success");
                                    listfacturas(idcont);
                                }

                            }
                        });
                    }else{
                        alertfunction('Atención!','Seleccione solo una factura');
                    }
                //================================================
                                }else{
                                    notificacion(2);
                                }
                        },
                        error: function(response){
                            notificacion(1); 
                        }
                    });
                    
                }else{
                    notificacion(0);
                }
            },
            cancelar: function (){
                
            }
        }
    });
}
function alertfunction(titlea,contenta){
    $.alert({boxWidth: '30%',useBootstrap: false,title: titlea,theme: 'bootstrap',content: contenta});  
}
function mocance(idfac){
    var motivo=$('#motivocancelacion option:selected').val();
    if(motivo=='01'){
        $('.class_foliorelacionado').show('show');
        setTimeout(function(){ 
            $.ajax({
                url: base_url+"index.php/Facturas/obtenerfoliouuid/",
                type: 'post',
                data: { 
                    idfactura:idfac
                },
                success: function (response) {
                    console.log(response);
                    $('#foliorelacionado').val(response);
                },
                error: function(response){
                    notificacion(1); 
                        setTimeout(function(){ 
                        //location.reload();
                     }, 2000);
                }
            });
        }, 1000);
        
    }else{
        $('.class_foliorelacionado').hide('show');
        $('#foliorelacionado').val('');
    }
}
function refacturar(idfact,idcont){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de refacturar? Se creará un nuevo documento!',
        type: 'red',
        typeAnimated: true,
        buttons: 
        {
            confirmar: function (){
                $.ajax({
                    url: base_url+"index.php/Configuracionrentas/reFactura/",
                    type: 'post',
                    data: { idfactura:idfact, idcontrato:idcont },
                    success: function (response){
                    	console.log(response);
                    	listfacturas(idcont);
                    },
                    error: function(response){
                        notificacion(1);  
                            setTimeout(function(){ 
                            //location.reload();
                         }, 2000);
                    }
                });
            },
            cancelar: function (){
                
            }
        }
    });
}

function retimbrar(idfactura,contrato){
    
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de generar el retimbrado?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $('body').loading({theme: 'dark',message: 'Timbrando factura...'});
                $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Configuracionrentas/retimbrar",
                    data: {
                        factura:idfactura
                    },
                    success:function(response){ 
                        $('body').loading('stop'); 
                        console.log(response);
                        var array = $.parseJSON(response);
                        if (array.resultado=='error') {
                            swal({    title: "Error "+array.CodigoRespuesta+"!",
                                          text: array.MensajeError,
                                          type: "warning",
                                          showCancelButton: false
                             });
                        }else{
                            swal("Éxito!", "Se ha creado la factura", "success");
                            listfacturas(contrato);
                        }

                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}
function listfacturas(idcontrato){
	var id_renta = $('#id_renta').val();
    var tipo=$('#tipofacturasstatus option:selected').val();
	$.ajax({
	    type:'POST',
	    url: base_url+"index.php/Configuracionrentas_ext/listfacturas",
	    data: {
	    	idcontrato:idcontrato,
            tipo:tipo
	    },
	    success:function(data){  
	    	$('.listfacturas').html(data);
	    	var tablefaclist=$('#tablefacturaslis').DataTable({
                            "order": [[ 1, "desc" ]]
                            }).on('draw',function(){
                                vericarpagosventas();
                            });
	    	$('.tooltipped').tooltip({delay: 50});

            $('#tipofacturasstatus').attr('disabled', false);
            tablefaclist.columns( [0] ).visible( false );
            vericarpagosventas();

	    }
    });
}
function facturapago(factura,total){
    $('#id_factura').val(factura);
    $('#fecha_p').val('');
    $('#pago_p').val('');
    $('#observacion_p').val('');
    $('.modal').modal({
        dismissible: true
    });
    $('#modal_pago').modal('open');
    $('#idmetodo').attr('disabled', false);
    $('#idmetodo').trigger("chosen:updated"); 
    $('.monto_prefactura').html('$ <span class="totalpoliza">'+total+'</span>');
    
    table_pagos_factura(factura);
    setTimeout(function(){ 
        calcularrestante();
    }, 1000);
}
function table_pagos_factura(id){
    $('.text_tabla_pago').html('');
    $.ajax({
      type: "POST",
      url: base_url+"index.php/Configuracionrentas/table_get_pagos_factura",
      data:{id:id},
      success: function(data){
        $('.text_tabla_pago').html(data);
      }
    });
}
function calcularrestante(){
    var totalpoliza=parseFloat($('.totalpoliza').html());
    var pagos = 0;
    $(".montoagregado").each(function() {
        var vstotal = parseFloat($(this).html());
        pagos += Number(vstotal);
    });
    var restante = parseFloat(totalpoliza)-parseFloat(pagos);
    if(restante<=0){
        restante=0;
        cambiarestatuspago($('#id_factura').val());
    }
    $('.restante_prefactura').html('$ <span class="restantepoliza">'+restante.toFixed(2)+'</span>');
}
function cambiarestatuspago(id){
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Generales/cambiarestatuspagoperiodo",
        data: {
            facId:id
        },
        success:function(response){  
         
        }
    });
}
function monto_ventas(id_v){//ventas monto de prefactura
    var result='';
    $.ajax({
      type: "POST",
      url: base_url+"index.php/Ventas/montoventas",
      data:{id:id_v},
      async:false,
      success: function(data){
        result=data;
      }
    });   
    return result;
}
function guardar_pago_compras(){
    var form_pago = $('#form_pago');
    form_pago.validate({
            rules: 
            { 
                fecha: {
                    required: true
                },
                idmetodo: {
                    required: true
                },
                pago: {
                    required: true
                }
            },
            // Para mensajes personalizados
            messages: 
            {   
                fecha:{
                    required: "Campo obligatirio"
                },
                idmetodo:{
                    required: "Campo obligatirio"
                },
                pago:{
                    required: "Campo obligatirio"
                }
            },
        errorElement : 'div'
    });
    var valid = $("#form_pago").valid();
    if(valid){
        var restante =parseFloat($('.restantepoliza').html());
        var pago_p =parseFloat($('#pago_p').val());
        if (pago_p>0 && pago_p<=restante) {
            var datos = $('#form_pago').serialize();
            $.ajax({
              type: "POST",
              url: base_url+"index.php/Configuracionrentas/guardar_pago_factura",
              data:datos,
              success: function(data){
                 $.alert({
                    boxWidth: '30%',
                    useBootstrap: false,
                    title: 'Atención!',
                    content: 'Guardador correctamente'}
                );
                $('#fecha_p').val('');
                $('#pago_p').val('');
                $('#observacion_p').val('');
                table_pagos_factura($('#id_factura').val());
                setTimeout(function(){ 
                    calcularrestante();
                    var idcontrato = $('#idcontrato').val();
                    listfacturas(idcontrato);
                }, 1000);
              }
            });
        }else{
            $.alert({
                    boxWidth: '30%',
                    useBootstrap: false,
                    title: 'Atención!',
                    content: 'Monto mayor a 0 y menor o igual al restante'}
                );
        }
    }   
}
function deletepagov(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de eliminar el pago?<br>Se necesita permisos de administrador<br>'+
                 '<input type="password" placeholder="Contraseña" id="contrasena" class="name form-control" required/>',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var pass=$('#contrasena').val();
                if (pass!='') {
                     $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Sistema/solicitarpermiso",
                        data: {
                            pass:pass
                        },
                        success: function (response){
                                var respuesta = parseInt(response);
                                if (respuesta==1) {
                                    $.ajax({
                                        type:'POST',
                                        url: base_url+"index.php/Configuracionrentas/eliminarpago",
                                        data: {
                                            pagoId:id
                                        },
                                        success: function (response){
                                                $.alert({
                                                    boxWidth: '30%',
                                                    useBootstrap: false,
                                                    title: 'Éxito!',
                                                    content: 'Pago eliminado!'});
                                                table_pagos_factura($('#id_factura').val());
                                                setTimeout(function(){ 
                                                    calcularrestante();
                                                }, 1000);
                                                
                                        },
                                        error: function(response){
                                            notificacion(1); 
                                        }
                                    });
                                }else{
                                    notificacion(2);
                                }
                        },
                        error: function(response){
                            notificacion(1); 
                        }
                    });
                    
                }else{
                    notificacion(0); 
                }
            },
            cancelar: function (){
                
            }
        }
    });
}
function confirmarpagov(id,tipo){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: 'Se necesita permisos de administrador<br>'+
                 '<input type="password" placeholder="Contraseña" id="contrasena" class="name form-control" required/>',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var pass=$('#contrasena').val();
                if (pass!='') {
                    $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Sistema/solicitarpermiso",
                        data: {
                            pass:pass
                        },
                        success: function (response){
                                var respuesta = parseInt(response);
                                if (respuesta==1) {
                    //=============================================================================
                        confirmarpagov2(id,tipo);
                    //=============================================================================
                                }else{
                                    notificacion(2);
                                }
                        },
                        error: function(response){
                            notificacion(1);   
                             
                        }
                    });

                }else{
                    notificacion(0);
                }
                
            },
            cancelar: function (){
                
            }
        }
    });
}
function confirmarpagov2(id,tipo){
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Generales/confirmarpagos",
        data: {
            id:id,
            tipo:tipo
        },
        success: function (response){
            $('#modal_pago').modal('close');
            $('.modal_pagos').removeClass('yellowc');
            vericarpagosventas();
        },
        error: function(response){
            notificacion(1);
        }
    });
}
function vericarpagosventas(){
    var DATAf  = [];
    $(".modal_pagos").each(function() {
        item = {};
        item ["idpre"] = $(this).data('idpre');
        DATAf.push(item);
    });
    aInfo   = JSON.stringify(DATAf);
    //console.log(aInfo);
    var datos=aInfo;
    
    $.ajax({
        type:'POST',
        url: base_url+"Generales/vericarpagosrentas",
        data: {
            datos:aInfo
        },
        success: function (data){
            //console.log(data);
            var array = $.parseJSON(data);
            console.log(array);
            $.each(array, function(index, item) {
                    $(".verificar_"+item.idpre).addClass( "yellowc" );
                
            });
        }
    });
}
function enviarmailfac(idfac){
    var cliente = $('#idCliente').val();
    obtenercontacto(cliente);
    $('.input_envio_c').html('');
    $('.table_mcr_files_c').html('');
    $('.input_envio_s').html('<input type="file" id="files" name="files[]" multiple accept="*">');
    $('.table_mcr_files_s').html('<table class="table bordered striped" id="table_mcr_files"><tbody class="tabbodycr_files"></tbody></table>');
    $('.addbtnservicio').html('<button type="button" class="waves-effect btn-bmz  red btn-envioser" onclick="enviarmailfac_mail('+idfac+')">Enviar</button>');
    $('.iframefac').html('<iframe src="'+base_url+'Configuracionrentas/generarfacturas/'+idfac+'/1?savefac=1">');
    inputfileadd();

    $('#modal_enviomail').modal();
    $('#modal_enviomail').modal('open');
    $('#firma_m_f').val(56)
}
function obtenercontacto(cliente){
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Calendario/obtenercontactos",
        data: {
            cliente:cliente
        },
        success: function (response){
            $('.m_s_contac').html('<select class="browser-default form-control-bmz" id="ms_contacto">'+response+'</select>');
                
        },
        error: function(response){
            notificacion(1);
        }
    });
    //=====================================================================
    
}
function inputfileadd(){
    $("#files").fileinput({
        showCaption: true,
        dropZoneEnabled: false,
        showUpload: true,// quita el boton de upload
        //rtl: true,
        //allowedFileExtensions: ["jpg","png","webp","jpeg"],
        browseLabel: 'Seleccionar documentos',
        uploadUrl: base_url+'Calendario/file_multiple',
        maxFilePreviewSize: 5000,
        previewFileIconSettings: {
            'docx': '<i class="fa fa-file-word-o text-primary"></i>',
            'xlsx': '<i class="fa fa-file-excel-o text-success"></i>',
            'pptx': '<i class="fa fa-file-powerpoint-o text-danger"></i>',
            'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
            'pdf': '<i class="fa fa-file-pdf-o text-danger"></i>',
            'zip': '<i class="fa fa-file-archive-o text-muted"></i>',
            'cer': '<i class="fas fa-file-invoice"></i>',
        },
        uploadExtraData: function (previewId, index) {
        var info = {
                    code:codigocarga
                };
        return info;
      }
    }).on('filebatchuploadcomplete', function(event, files, extra) {
      //location.reload();
        upload_data_view(codigocarga);
        $('#files').fileinput('clear');
    }).on('filebatchuploadsuccess', function(event, files, extra) {
      //location.reload();
      //toastr.success('Se cargo el Certificado Correctamente','Hecho!');
        upload_data_view(codigocarga);
        $('#files').fileinput('clear');
    });
}
var idrowcc=0;
function upload_data_view(codigo){
    $.ajax({
            type:'POST',
            url: base_url+"index.php/Calendario/upload_data_view",
            data: {
                code : codigo
            },
            success: function (response){
                console.log(response);
                var array = $.parseJSON(response);
                array.forEach(function(r){
                    var html ='<tr class="idrowcc_'+idrowcc+'">';
                        html+='<td><input type="hidden" id="ec_file" value="'+r.id+'">'+r.file+'</td>';
                        html+='<td><a class="b-btn b-btn-danger" onclick="delete_mcr('+idrowcc+')"><i class="fas fa-trash-alt"></i></a></td>';
                        html+='</tr>';
                        $('.tabbodycr_files').append(html);
                        idrowcc++;

                });
                
            },
            error: function(response){
                toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema");
                
            }
        });
}
function delete_mcr(id){
    $('.idrowcc_'+id).remove();
}
function enviarmailfac_mail(idfac){
    var email =$('#ms_contacto option:selected').data('email');
    $.ajax({
        type:'POST',
        url: base_url+'index.php/Mailenvio/envio_fac',
        data: {
            idfac:idfac,
            code:codigocarga,
            email:email,
            asunto:$('#ms_asucto2').val(),
            comen:$('#ms_coment2').val(),
            per:$('#firma_m_f option:selected').val(),
            perbbc:$('#bbc_m_f option:selected').val()
        },
        success:function(data){
            console.log(data);
            var array = $.parseJSON(data);
            codigocarga=array.newcodigo;
            $('#codigocarga').val(codigocarga);
            toastr["success"]("Correo enviado");
        }
    });
}