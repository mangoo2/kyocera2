<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class Cambios_model extends CI_Model 
{
    public function __construct() 
    {
        parent::__construct();
    }

    public function actualizaTipoCambio($id, $data)
    {
    	$this->db->set($data);
	    $this->db->where('id', $id);
	    return $this->db->update('tiposCambio');
    }

   	public function getTiposCambio()
   	{
   		$sql = "SELECT * FROM tiposCambio";
        $query = $this->db->query($sql);
        return $query->result();
   	}
}

?>