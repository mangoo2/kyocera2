// Saber si es solo Visualización 
var tipoVista = $("#tipoVista").val();
var base_url = $('#base_url').val();
var addtr=0;
$(document).ready(function(){
    $('.modal').modal();
    $('#condicion').ckeditor();
    $('.chosen-select').chosen({width: "95%"});
    $('#modelocliente').chosen();
    var base_url = $('#base_url').val();
    $('.submitform').click(function(event) {
        $(".submitform").prop("disabled", true);
        setTimeout(function(){ 
            $("#clientes_from").submit();
        }, 1000);
        setTimeout(function(){ 
            $(".submitform").prop("disabled", false);
        }, 3000);
      
    });
   
    var planta_sucursal_v = $('#planta_sucursal_v').val();
    if(planta_sucursal_v == 2){
      $('.planta_sucursal_dir').css('display','block');
    }else{
      $('.planta_sucursal_dir').css('display','none'); 
      $('#planta_sucursal_dir').val('');
    }
     // En caso de que sea solo visualizar, deshabilita todo
    if(tipoVista == 2){
        var clienteID = $('#idCliente').val();
        //loadTabla_Tel_local(clienteID);
        //loadTabla_Tel_celular(clienteID);
        //loadTabla_Tel_persona_contacto(clienteID);
      //  loadTabla_datos_fiscales(clienteID);
    } 
    else if(tipoVista == 3)
    {   var clienteID = $('#idCliente').val();
        //loadTabla_Tel_local(clienteID);
        //loadTabla_Tel_celular(clienteID);
        //loadTabla_Tel_persona_contacto(clienteID);
        $("input").prop('disabled', true);
        $("textarea").prop('disabled', true);
        $("select").prop('disabled', true);
        $("checkbox").prop('disabled', true);
        $("button").prop('disabled', true);
        $(".btn_agregar_tel").attr('style', 'pointer-events: none;');
        $(".btn_agregar_cel").attr('style', 'pointer-events: none;');
        $(".eliminar").attr('style', 'pointer-events: none;');
    }

    var formulario_cliente = $('#clientes_from');
     formulario_cliente.validate({
            rules: 
            {
                empresa: {
                    required: true
                },
                municipio: {
                    required: true
                },
                giro_empresa: {
                    required: true
                },
                direccion: {
                    required: true
                },
                observaciones: {
                    required: true
                }

            },
            // Para mensajes personalizados
            messages: 
            {
                empresa:{
                    required: "Ingrese una empresa"
                },
                /*
                persona_contacto:{
                    required: "Ingrese un contacto"
                },
                
                correo:{
                    required: "Ingrese un correo"
                },
                tel_local:{
                    required: "Ingrese un telefono local"
                },
                celular:{
                    required: "Ingrese un número celular"
                },
                */
                estado:{
                    required: "Seleccione un estado"
                },
                municipio:{
                    required: "Ingrese un municipio"
                },
                giro_empresa:{
                    required: "Ingrese el giro de la empresa"
                },
                direccion:{
                    required: "Ingrese una dirección"
                },
                observaciones:{
                    required: "Ingrese una observación"
                },
                email:{
                    required: "Ingrese un email"
                }
            },
        errorElement : 'div',
            // Si están correctos los campos, procesamos el formulario mediante Ajax
        submitHandler: function (form) {   
            var DATA3  = [];
                    var TABLA3   = $("#tabledatoscontacto tbody > tr");
                        TABLA3.each(function(){         
                              items = {};
                              items['datosId'] = $(this).find("input[id*='datosId']").val();
                              items['atencionpara'] = $(this).find("input[id*='atencionpara']").val();
                              var puesto=$(this).find("input[id*='puesto']").val();
                                  puesto=puesto.replace('&', '---');
                              items['puesto'] = puesto;
                              items['email'] = $(this).find("input[id*='email']").val();
                              items['telefono'] = $(this).find("input[id*='telefono']").val();
                              items['celular'] = $(this).find("input[id*='celular']").val();
                              items['dir'] = $(this).find("select[id*='dir_contac'] option:selected").val();
                             DATA3.push(items);
                        });
            persona_contacto = JSON.stringify(DATA3);
            var DATA4  = [];
                    var TABLA4   = $(".border_datos tbody > tr");
                        TABLA4.each(function(){         
                             item = {};
                             item ["id"]            = $(this).find("input[id*='id_df']").val();
                             var razon_social = $(this).find("input[id*='razon_social']").val();
                             razon_social=razon_social.replace('&','----');
                             razon_social=razon_social.replace('+','****');
                             razon_social=razon_social.replace('"','....');
                             razon_social=razon_social.replace('"','....');
                             item ["razon_social"]  = razon_social; 
                             //$(this).find("input[id*='rfc']").change();
                             item ["rfc"]           = $(this).find("input[id*='rfc']").val().replace('&','----');
                             item ["cp"]            = $(this).find("input[id*='cp']").val();
                             item ["calle"]         = $(this).find("textarea[id*='calle']").val();
                             item ["num_ext"]       = $(this).find("input[id*='num_ext']").val();
                             item ["num_int"]       = $(this).find("input[id*='num_int']").val();
                             item ["colonia"]       = $(this).find("textarea[id*='colonia']").val();

                             item ["estado"]        = $(this).find("select[id*='df_estado'] option:selected").val();
                             item ["municipio"]     = $(this).find("input[id*='df_minicipio']").val();
                             item ["localidad"]     = $(this).find("input[id*='df_localidad']").val();
                             item ["RegimenFiscalReceptor"]     = $(this).find("select[id*='RegimenFiscalReceptor'] option:selected").val();
                             
                             DATA4.push(item);
                        });
            datos_fiscales = JSON.stringify(DATA4);
            
            var DATA5  = [];
                    var TABLA5   = $(".tabla_dir tbody > tr");
                        TABLA5.each(function(){         
                             item = {};
                             item ["id"] = $(this).find("input[id*='idclientedirecc']").val();
                             item ["direccion"] = $(this).find("input[id*='direccion']").val();
                             item ["pro_acc"] = $(this).find("input[id*='pro_acc']").val();
                             item ["longitud"] = $(this).find("input[id*='longitud']").val();
                             item ["latitud"] = $(this).find("input[id*='latitud']").val();
                             DATA5.push(item);
                        });
            datos_direccion = JSON.stringify(DATA5);

            var DATA6  = [];
                    var TABLA6   = $("#table_modelos_cliente tbody > tr");
                        TABLA6.each(function(){         
                             item = {};
                             item ["id"] = $(this).find("input[id*='cliequId']").val();
                             item ["modelo"] = $(this).find("input[id*='modeloclienteid']").val();
                             item ["serie"] = $(this).find("input[id*='seriecliente']").val();
                             DATA6.push(item);
                        });
            datos_equipos = JSON.stringify(DATA6);

            var aaa_x = 0; 
            if($('#aaa').is(':checked')){
                aaa_x=1;   
            }
            var bloqueo = 0; 
            if($('#bloqueo').is(':checked')){
                bloqueo=1;   
            }
            var soc=0;
            if($('#sol_orden_com').is(':checked')){
                soc=1;
            }
            var fm=$('input:radio[name=fisicamoral]:checked').val();
            //var datos = formulario_cliente.serialize()+'&aaa='+aaa_x+'&telefono_local='+telefono_local+'&telefono_celular='+telefono_celular+'&persona_contacto='+persona_contacto+'&datos_fiscales='+datos_fiscales+'&datos_direccion='+datos_direccion;
            var condicion=$('#condicion').val();
            var horario_disponible = $('#horario_disponible').val();
            var equipo_acceso = $('#equipo_acceso').val();
            var documentacion_acceso = $('#documentacion_acceso').val();

            var mrow = $('#m_row').val();
            var mcorreo = $('#m_correo').val();
            var mpass = $('#m_pass').val();


            var datos = formulario_cliente.serialize()+'&aaa='+aaa_x+'&datos_fiscales='+datos_fiscales+'&datos_direccion='+datos_direccion+'&persona_contacto='+persona_contacto+'&condicion='+condicion+'&horario_disponible='+horario_disponible+'&equipo_acceso='+equipo_acceso+'&documentacion_acceso='+documentacion_acceso+'&datos_equipos='+datos_equipos+'&bloqueo='+bloqueo+'&fm='+fm+'&mrow='+mrow+'&mcorreo='+mcorreo+'&mpass='+mpass+'&soc='+soc;

            if (TABLA5.length>0) {
              $.ajax({
                  type:'POST',
                  url: base_url+'index.php/clientes/insertaActualizaClientes',
                  data: datos,
                  success:function(data){
                        var array = $.parseJSON(data);
                        console.log(array);
                      // Si existe el usuario, mostramos el mensaje de entrada y redirigimos
                      if(array.result>=1){
                        redirigiropcion(array.result);
                      }
                      // En caso contrario, se notifica
                      else{
                          swal({ title: "Error",
                              text: "No se pudo guardar la información, intente de nuevo o contacte al Administrador del Sistema",
                              type: 'error',
                              showCancelButton: false,
                              allowOutsideClick: false,
                          });
                      }
                  }
              });
            }else{
              swal({ title: "Error",
                    text: "Agregar una direccion den entrega",
                    type: 'error',
                    showCancelButton: false,
                    allowOutsideClick: false,
                });
            }
            
        }
    
     });
    $(".btn_agregar_tel").click(function(){
      var contador = $('.border_tel').length; 
      var contadoraux = contador+1; 
      var $template = $("#tel_dato1"),
      $clone = $template
     .clone()
     .show()
     .removeAttr('id')
     .attr('id',"tel_dato"+contadoraux)
     .insertAfter($("#tel_dato"+contador));

     $clone.find($('#categoria'));
     $clone.find(".div_btn_tel").html('<a class="btn btn-floating waves-effect waves-light red btn_eliminar_tel"><i class="material-icons">delete_forever</i></a>');
     $clone.find("select").val("");
     $clone.find("input").val("");
     eliminar_tel();
     $('select').material_select();

    });

    $(".btn_agregar_cel").click(function(){
      var contador = $('.border_cel').length; 
      var contadoraux = contador+1; 
      var $template = $("#cel_datos1"),
      $clone = $template
     .clone()
     .show()
     .removeAttr('id')
     .attr('id',"cel_datos"+contadoraux)
     .insertAfter($("#cel_datos"+contador));

     $clone.find($('#categoria'));
     $clone.find(".div_btn_cel").html('<a class="btn btn-floating waves-effect waves-light red btn_eliminar_cel"><i class="material-icons">delete_forever</i></a>');
     $clone.find("select").val("");
     $clone.find("input").val("");
     eliminar_cel();
     $('select').material_select();

    });
    
    $(".btn_agregar_contacto").click(function(){
      var contador = $('.border_contacto').length; 
      var contadoraux = contador+1; 
      var $template = $("#contacto_datos1"),
      $clone = $template
     .clone()
     .show()
     .removeAttr('id')
     .attr('id',"contacto_datos"+contadoraux)
     .insertAfter($("#contacto_datos"+contador));

     $clone.find($('#categoria'));
     $clone.find(".div_btn_contacto").html('<a class="btn btn-floating waves-effect waves-light red btn_eliminar_contacto"><i class="material-icons">delete_forever</i></a>');
     $clone.find("input").val("");
     eliminar_contacto();
     

    });  
    $('.class_readonly').click(function(event) {
        if($(".class_readonly").prop("readonly")){
            $.confirm({
                boxWidth: '30%',
                useBootstrap: false,
                icon: 'fa fa-warning',
                title: 'Atención!',
                content: 'Se necesita permisos de administrador<br>'+
                         '<input type="password" placeholder="Contraseña" id="contrasena" class="name form-control" required/>',
                type: 'red',
                typeAnimated: true,
                buttons:{
                    confirmar: function (){
                        var pass=$('#contrasena').val();
                        if (pass!='') {
                            $.ajax({
                                type:'POST',
                                url: base_url+"index.php/Sistema/solicitarpermiso",
                                data: {
                                    pass:pass
                                },
                                success: function (response){
                                        var respuesta = parseInt(response);
                                        if (respuesta==1) {
                            //=============================================================================
                                
                                $(".class_readonly").prop("readonly",false)
                            //=============================================================================
                                        }else{
                                            notificacion(2); 
                                        }
                                },
                                error: function(response){
                                    notificacion(1);  
                                     
                                }
                            });

                        }else{
                            notificacion(0);
                        }
                        
                    },
                    cancelar: function (){
                        
                    }
                }
            });
        }
    });  
    $('#empresa').on("input", function() {
        // Captura el valor actual del elemento de entrada
        var valorInput = $(this).val();

        // Reemplaza los caracteres deseados (por ejemplo, reemplaza "mundo" con "amigos")
        var valorInput = valorInput.replace(/\\/g, "");
        var valorInput = valorInput.replace("'", "");

        // Asigna el valor modificado de vuelta al elemento de entrada
        $(this).val(valorInput);
    });
    $('#establecerpass').click(function(event) {
        var fech_vigencia = $('#fecha_vencimiento_info').val();
        var cli = $('#idCli').val();
        $.confirm({
            boxWidth: '30%',
            useBootstrap: false,
            icon: 'fa fa-warning',
            title: 'Atención!',
            content: 'Desea establecer una contraseña para este cliente?<br>'+
                     '<label>Establecer contraseña</label><input type="text" placeholder="Contraseña" id="contrasena" name="contrasena" class="name form-control"  onpaste="return false;" required/><br>'+
                     '<label>Fecha vencimiento</label><input type="date" id="fech_vigencia"  value="'+fech_vigencia+'" class="name form-control"readonly/>',
            type: 'red',
            typeAnimated: true,
            buttons:{
                confirmar: function (){
                    //var pass=$('#contrasena').val();
                    var pass=$("input[name=contrasena]").val();
                    if (pass!='') {
                         $.ajax({
                            type:'POST',
                            url: base_url+"index.php/Clientes/establecerpass",
                            data: {
                                pass:pass,
                                fecha:fech_vigencia,
                                cli:cli
                            },
                            success: function (response){
                                toastr["success"]("Contraseña establecida", "Hecho")
                            },
                            error: function(response){
                                notificacion(1);
                                setTimeout(function(){
                                            location.reload();
                                },2000);
                            }
                        });
                        
                    }else{
                        notificacion(0);
                        
                    }
                },
                cancelar: function (){
                    
                }
            }
        });
        setTimeout(function(){
            new MaskedPassword(document.getElementById("contrasena"), '\u25CF');
            
        },1000);
    });

});
function redirigiropcion(id){
    var empresa = $('#empresa').val();
    $.confirm({
        icon:'fa fa-check',
        boxWidth: '40%',
        useBootstrap: false,
        icon: 'fa fa-success',
        title: 'Cliente Guardado',
        content: 'Seleccione una opcion a redirigir',
        type: 'success',
        animation: 'scale',
        theme:'modern',
        typeAnimated: true,
        buttons:{
            Venta: function (){
                window.location.href = base_url+"index.php/Ventas/add/"+id; 
            },
            Cotizacion: function (){
                window.location.href = base_url+"index.php/Cotizaciones/altaCotizacion/"+id; 
            },
            'Listado Clientes': function (){
                window.location.href = base_url+"index.php/clientes"; 
            },
            'Listado Ventas': function (){
                window.location.href = base_url+"index.php/Ventas?idcli="+id+"&emp="+empresa; 
            }
        }
    });
}
function mostraratencion() {
    var nombre = $('#atencionpara').val();
    $('#persona_contacto').val(nombre);
}
function eliminar_tel(){
    $(".btn_eliminar_tel").click(function(){
        var $row = $(this).parents('.border_tel');
        // Remove element containing the option
        $row.remove();
    });
}

function eliminar_cel(){
    $(".btn_eliminar_cel").click(function(){
        var $row = $(this).parents('.border_cel');
        // Remove element containing the option
        $row.remove();
    });
}
function eliminar_contacto(){
    $(".btn_eliminar_contacto").click(function(){
        var $row = $(this).parents('.border_contacto');
        // Remove element containing the option
        $row.remove();
    });
}
function Eliminar_Tabla_DatosF(idDato){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de eliminar estos datos fiscales? El cambio será irreversible',
        type: 'red',
        typeAnimated: true,
        buttons: 
        {
            confirmar: function () 
            {
                $.ajax({
                    url: base_url+"index.php/clientes/eliminar_datos_fiscales/"+idDato,
                    success: function (response) {
                            $.alert({boxWidth: '30%',useBootstrap: false,
                                title: 'Éxito!',
                                content: 'Datos Fiscales Eliminado!'});
                            setTimeout(function(){ 
                            location.reload();
                            }, 2000);

                    },
                    error: function(response) {
                        notificacion(1);  
                            setTimeout(function(){ 
                            location.reload();
                            }, 2000);
                    }
                });
            },
            cancelar: function () {
                
            }
        }
    });
}

function selectsucursal(){
    var ps = $('#planta_sucursal option:selected').val();
    if(ps == 2){
      $('.planta_sucursal_dir').css('display','block');
    }else{
      $('.planta_sucursal_dir').css('display','none'); 
      $('#planta_sucursal_dir').val('');
    }
}
var datos_df = 1;
function agregar_d_f() {
    var idpersonal = $('#idpersonal').val();
    if(idpersonal==1 || idpersonal==63){
        var onpaste='';
    }else{
        var onpaste=' onpaste="return false;" ';
    }
      var tablerow='<tr class="polizaselecteddd_row_'+datos_df+'">\
                    <td>\
                        <input readonly="" id="id_df" type="text" value="0" style="display: none;">\
                        <div class="row">\
                          <div class="col s6">\
                            <label>Razón Social<span class="datosrequeridos">*</span></label>\
                            <input id="razon_social" style="min-height: 50px;" '+onpaste+' value="" onchange="validarrazonsocial()">\
                          </div>\
                          <div class="col s2">\
                            <label class="active">R.F.C<span class="datosrequeridos">*</span></label>\
                            <input id="rfc" type="text" value="" onchange="validarrfc(this)">\
                          </div>\
                          <div class="col s2">\
                            <label for="df_estado" class="active">Estado<span class="datosrequeridos">*</span></label>\
                              <select id="df_estado" class="browser-default form-control-bmz">\
                                <option></option>\
                                <option value="AGUASCALIENTES">AGUASCALIENTES</option>\
                                <option value="BAJA CALIFORNIA">BAJA CALIFORNIA</option>\
                                <option value="BAJA CALIFORNIA SUR">BAJA CALIFORNIA SUR</option>\
                                <option value="CAMPECHE">CAMPECHE</option>\
                                <option value="COAHUILA">COAHUILA</option>\
                                <option value="COLIMA">COLIMA</option>\
                                <option value="CHIAPAS">CHIAPAS</option>\
                                <option value="CHIHUAHUA">CHIHUAHUA</option>\
                                <option value="CDMX">CDMX</option>\
                                <option value="DURANGO">DURANGO</option>\
                                <option value="GUANAJUATO">GUANAJUATO</option>\
                                <option value="GUERRERO">GUERRERO</option>\
                                <option value="HIDALGO">HIDALGO</option>\
                                <option value="JALISCO">JALISCO</option>\
                                <option value="ESTADO DE MEXICO">ESTADO DE MEXICO</option>\
                                <option value="MICHOACAN">MICHOACAN</option>\
                                <option value="MORELOS">MORELOS</option>\
                                <option value="NAYARIT">NAYARIT</option>\
                                <option value="NUEVO LEON">NUEVO LEON</option>\
                                <option value="OAXACA">OAXACA</option>\
                                <option value="PUEBLA">PUEBLA</option>\
                                <option value="QUERETARO">QUERETARO</option>\
                                <option value="QUINTANA ROO">QUINTANA ROO</option>\
                                <option value="SAN LUIS POTOSI">SAN LUIS POTOSI</option>\
                                <option value="SINALOA">SINALOA</option>\
                                <option value="SONORA">SONORA</option>\
                                <option value="TABASCO">TABASCO</option>\
                                <option value="TAMAULIPAS">TAMAULIPAS</option>\
                                <option value="TLAXCALA">TLAXCALA</option>\
                                <option value="VERACRUZ">VERACRUZ</option>\
                                <option value="YUCATAN">YUCATAN</option>\
                                <option value="ZACATECAS">ZACATECAS</option>\
                          </select>\
                          </div>\
                          <div class="col s2">\
                            <label>Municipio<span class="datosrequeridos">*</span></label>\
                            <input id="df_minicipio" type="text" value="" class="">\
                          </div>\
                        </div>\
                        <div class="row">\
                          <div class="col s2">\
                            <label>Localidad</label>\
                            <input id="df_localidad" type="text" value="" class="">\
                          </div>\
                          <div class="col s1">\
                            <label class="active">C.P.<span class="datosrequeridos">*</span></label>\
                            <input id="cp" type="text" value="">\
                          </div>\
                          <div class="col s2">\
                            <label class="active">Calle<span class="datosrequeridos">*</span></label>\
                            <textarea id="calle" style="min-height: 50px;"></textarea>\
                          </div>\
                          <div class="col s1">\
                            <label class="active"># Ext</label>\
                            <input id="num_ext" type="text" value="">\
                          </div>\
                          <div class="col s1">\
                            <label># Int</label>\
                            <input id="num_int" type="text" value="">\
                          </div>\
                          <div class="col s2">\
                            <label class="active">Colonia</label>\
                            <textarea id="colonia" style="min-height: 50px;"></textarea>\
                          </div>\
                          <div class="col s2">\
                            <label>RegimenFiscalReceptor <span class="datosrequeridos">*</span></label>\
                            <select id="RegimenFiscalReceptor" name="RegimenFiscalReceptor" class="browser-default form-control-bmz" required>\
                              <option value="0" ></option>\
                              <option value="601" >601 General de Ley Personas Morales</option>\
                              <option value="603" >603 Personas Morales con Fines no Lucrativos</option>\
                              <option value="605" >605 Sueldos y Salarios e Ingresos Asimilados a Salarios</option>\
                              <option value="606" >606 Arrendamiento</option>\
                              <option value="607" >607 Régimen de Enajenación o Adquisición de Bienes</option>\
                              <option value="608" >608 Demás ingresos</option>\
                              <option value="609" >609 Consolidación</option>\
                              <option value="610" >610 Residentes en el Extranjero sin Establecimiento Permanente en México</option>\
                              <option value="611" >611 Ingresos por Dividendos (socios y accionistas)</option>\
                              <option value="612" >612 Personas Físicas con Actividades Empresariales y Profesionales</option>\
                              <option value="614" >614 Ingresos por intereses</option>\
                              <option value="615" >615 Régimen de los ingresos por obtención de premios</option>\
                              <option value="616" >616 Sin obligaciones fiscales</option>\
                              <option value="620" >620 Sociedades Cooperativas de Producción que optan por diferir sus ingresos</option>\
                              <option value="621" >621 Incorporación Fiscal</option>\
                              <option value="622" >622 Actividades Agrícolas, Ganaderas, Silvícolas y Pesqueras</option>\
                              <option value="623" >623 Opcional para Grupos de Sociedades</option>\
                              <option value="624" >624 Coordinados</option>\
                              <option value="625" >625 Régimen de las Actividades Empresariales con ingresos a través de Plataformas Tecnológicas</option>\
                              <option value="626" >626 Régimen Simplificado de Confianza</option>\
                              <option value="628" >628 Hidrocarburos</option>\
                              <option value="629" >629 De los Regímenes Fiscales Preferentes y de las Empresas Multinacionales</option>\
                              <option value="630" >630 Enajenación de acciones en bolsa de valores</option>\
                            </select>\
                          </div>\
                          <div class="col s1">\
                                <a class="btn-floating red tooltipped" onclick="deletepolisadd_df('+datos_df+')" data-position="top" data-delay="50" data-tooltip="Eliminar" data-tooltip-id="8863a6d3-4452-b359-f327-022de81628a3">\
                                  <i class="material-icons">delete_forever</i>\
                                </a>\
                          </div>\
                        </div>\
                    </td>\
                  </tr>';
    
    
    if($(".border_datos tbody > tr").length<1){
    $('#border_datos tbody').append(tablerow);
    }
    datos_df++;
}
function deletepolisadd_df(id) {
  $('.polizaselecteddd_row_'+id).remove();
}
function add_address(){ 
    var dirrecc = $('#direcc').val();
    var spro_acc = $('#spro_acc').val();
    if(dirrecc!= ""){
      var html_text = '<tr class="addtr_'+addtr+'">\
                          <td><input id="idclientedirecc" type="hidden"  value="0" readonly></td>\
                          <td><input class="form-control-bmz" type="text" id="direccion" value="'+dirrecc+'"></td>\
                          <td><input class="form-control-bmz" type="text" id="pro_acc" value="'+spro_acc+'"></td>\
                          <td><input class="form-control-bmz" type="number"  id="longitud" value=""></td>\
                          <td><input class="form-control-bmz" type="number"  id="latitud" value=""></td>\
                          <td><a class="btn-floating waves-effect waves-light red accent-2" onclick="deteletr('+addtr+')"><i class="material-icons">clear</i></a></td>\
                          </tr>';
      $('.tbody_direccion').append(html_text);   
      addtr++; 
      $('#direcc').val('');
    }else{
      swal({   
        title: "Atención",
        text: "Tienes que ingresar una dirección",
        timer: 2000,
        showConfirmButton: false
      });
    }
}
function deteletr(id){
  $('.addtr_'+id).remove();
}
function delete_direcc(id)
{
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de eliminar esta dirreción',
        type: 'red',
        typeAnimated: true,
        buttons: 
        {
            confirmar: function () {
                $.ajax({
                    url: base_url+"index.php/clientes/delete_direccion/"+id,
                    success: function (response) {
                        $.alert({boxWidth: '30%',useBootstrap: false,
                                title: 'Éxito!',
                                content: 'Dirección Eliminada!'});
                        setTimeout(function(){ 
                        location.reload();
                        }, 2000);

                    },
                    error: function(response){
                        notificacion(1);
                            setTimeout(function(){ 
                            location.reload();
                            }, 2000);
                    }
                });
            },
            cancelar: function () {
                
            }
        }
    });
}

function agregar_pc(){
  var atencionpara = $('#atencionpara').val();
  var puesto_contacto = $('#puesto_contacto').val();
  var email = $('#email').val();
  var tel_local = $('#tel_local').val();
  var celular = $('#celular').val();
  if(atencionpara!=''){
  datoscontactoadd(0,atencionpara,puesto_contacto,email,tel_local,celular);
  }
  $('.inputdatoscontacto').val('');
}
var rowpc=0;
function datoscontactoadd(id,atencionpara,puesto_contacto,email,tel_local,celular){
    var htmloption=$('.optiondireccion').html();
  var addtrpc =  '<tr class="rowpc_'+rowpc+'_'+id+'">\
                    <td>\
                      <input type="hidden" id="datosId" value="'+id+'" readonly>\
                      <input type="hidden" id="atencionpara" value="'+atencionpara+'" readonly>\
                      '+atencionpara+'\
                    </td>\
                    <td>\
                      <input type="hidden" id="puesto" value="'+puesto_contacto+'" readonly>'+puesto_contacto+'\
                    </td>\
                    <td>\
                      <input type="hidden" id="email" value="'+email+'" readonly>'+email+'\
                    </td>\
                    <td><input type="hidden" id="telefono" value="'+tel_local+'" readonly>'+tel_local+'\
                    </td>\
                    <td><input type="hidden" id="celular" value="'+celular+'" readonly>'+celular+'\
                    </td>\
                    <td><select class="browser-default form-control-bmz" id="dir_contac">'+htmloption+'</select></td>\
                    <td>\
                      <a class="btn-floating red tooltipped" onclick="Eliminar_pc('+id+','+rowpc+')" \
                        data-position="top" data-delay="50" data-tooltip="Eliminar">\
                        <i class="material-icons">delete_forever</i>\
                      </a>\
                    </td>\
                </tr>';
  $('.tabledatoscontactotb').append(addtrpc);
  rowpc++;
}
function Eliminar_pc(id,row){
  if (id==0) {
    $('.rowpc_'+row+'_'+id).remove();
  }else{
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de Eliminar el contacto? El cambio será irreversible',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    url: base_url+"index.php/clientes/eliminar_persona_contacto/"+id,
                    success: function (response){

                        if(response==1){
                            $.alert({boxWidth: '30%',useBootstrap: false,
                                title: 'Éxito!',
                                content: 'Contacto Eliminado!'});
                                $('.rowpc_'+row+'_'+id).remove();
                        }else{
                            notificacion(1); 
                            setTimeout(function(){ 
                            location.reload();
                            }, 2000);
                        }
                    },
                    error: function(response){
                        notificacion(1);  
                            setTimeout(function(){ 
                            location.reload();
                            }, 2000);
                    }
                });
            },
            cancelar: function (){
                
            }
        }
    });
  }
}
function obtienedatosclientes(id){

}
/*
function rfcValido(rfc) {
    var re = /^([ A-ZÑ&]?[A-ZÑ&]{3}) ?(?:- ?)?(\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])) ?(?:- ?)?([A-Z\d]{2})([A\d])$/,
        validado = rfc.match(re);
  
    if (!validado)  //Coincide con el formato general?
      return false;
    
    //Separar el dígito verificador del resto del RFC
    var digitoVerificador = validado.pop(),
        rfcSinDigito = validado.slice(1).join('')
        
    //Obtener el digito esperado
    var diccionario  = "0123456789ABCDEFGHIJKLMN&OPQRSTUVWXYZ Ñ",
        lngSuma      = 0.0,
        digitoEsperado;

    if (rfcSinDigito.length == 11) rfc = " " + rfc; //Ajustar a 12
    for(var i=0; i<13; i++)
        lngSuma = lngSuma + diccionario.indexOf(rfcSinDigito.charAt(i)) * (13 - i);
    digitoEsperado = 11 - lngSuma % 11;
    if (digitoEsperado == 11) digitoEsperado = 0;
    if (digitoEsperado == 10) digitoEsperado = "A";
  
  //El dígito verificador coincide con el esperado?
    return digitoVerificador == digitoEsperado;
}
*/
function validarrfc(input) {
    var rfc = input.value.toUpperCase();

    if (rfcValido1(rfc) || rfc=='XAXX010101000' || rfc=='SEP210905778'|| rfc=='GE98501011S6' || rfc=='GEP8501011S6' || rfc=='USE9205217S8') { // ⬅️ Acá se comprueba
      rfcexiste(rfc);
      toastr["success"](rfc+" válido", "Correcta!");
    } else {
      toastr["error"](rfc+" No válido", "Alerta!");
    }
        
}
function rfcValido1(rfcStr) {
    
    $('.help-block').remove();
    
    var strCorrecta;
    
    strCorrecta = rfcStr;
        
    if (rfcStr.length == 12){
       
       //var valid = '^(([A-Z]|[a-z]){3})([0-9]{6})((([A-Z]|[a-z]|[0-9]){3}))';
       var valid = '^(([A-Z,Ñ,&]|[a-z,ñ,&]){3})([0-9]{6})((([A-Z]|[a-z]|[0-9]){3}))';
    
    } else {
       
       var valid = '^(([A-Z]|[a-z]|\s){1})(([A-Z]|[a-z]){3})([0-9]{6})((([A-Z]|[a-z]|[0-9]){3}))';
    }
    
    var validRfc=new RegExp(valid);
    
    var matchArray=strCorrecta.match(validRfc);
    
    if (matchArray==null) {        
        return false;        
    } else {
        return true;
    }
}
function rfcexiste(rfc){
  $.ajax({
        type:'POST',
        url: base_url+"index.php/Clientes/rfcexiste",
        data: {
          cli: $('#idCli').val(),
          vrfc:rfc
        },
        success: function (response){
          var array = $.parseJSON(response);
          if (array.resul>0) {
            //toastr["error"]("Clientes"+array.datos, "Existe!");
            $.confirm({
                boxWidth: '50%',
                useBootstrap: false,
                icon: 'fa fa-warning',
                title: 'Atención!',
                content: 'El RFC ya existe en los siguientes clientes:<br>'+
                         '<table class="striped">'+array.datos+'<table/>',
                type: 'red',
                typeAnimated: true,
                buttons:{
                    continuar: function () {
                        
                    }
                }
            });
            //$(".submitform").prop('disabled', true);
          }else{
            $(".submitform").prop('disabled', false);
          }
        },
        error: function(response){
            alertfunction('Atención!','Algo salió mal, intente de nuevo o contacte al administrador del sistema'); 
        }
    });
}
function verificarexis(){
    var idCliente=$('#idCliente').val()==undefined?0:$('#idCliente').val();
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Clientes/existecliente",
        data: {
          nombre: $('#empresa').val(),
          idCliente:idCliente,
        },
        success: function (response){
            $('.listadoclientes').html('');
          var array = $.parseJSON(response);
          if(array.length>0){
            $('#modalclientesexit').modal({
                    dismissible: false
                });
            $('#modalclientesexit').modal('open');
            $.each(array, function(index, item) {
                var html ='<tr>\
                                <td>'+item.empresa+'</td>\
                                <td>\
                                    <a class="btn-floating green tooltipped edit" \
                                    data-position="top" \
                                    data-delay="50" \
                                    data-tooltip="Editar" \
                                    href="'+base_url+'Clientes/edicion/'+item.id+'" >\
                                      <i class="material-icons">mode_edit</i> \
                                  </a>\
                                <a class="btn-floating light-blue darken-4 tooltipped" data-position="top" data-delay="50" data-tooltip="Venta" href="'+base_url+'Ventas/add/'+item.id+'"><i class="material-icons">add_shopping_cart</i></a>\
                                <a class="btn-floating light-blue darken-4 tooltipped" data-position="top" data-delay="50" data-tooltip="Cotizar" href="'+base_url+'Cotizaciones/altaCotizacion/'+item.id+'"><i class="material-icons">monetization_on</i></a>\
                                <a class="btn-floating light-blue darken-4 tooltipped" data-position="top" data-delay="50" data-tooltip="Prefacturas" onclick="infodell('+item.id+')" ><i class="material-icons">assignment</i></a>\
                                </td>\
                            </tr>';
                $('.listadoclientes').append(html);
            });
            $('.tooltipped').tooltip();
          }else{
            toastr.success('El cliente no existe', "Correcto!");
          }
          
          
        },
        error: function(response){
            toastr["error"]('Algo salió mal, intente de nuevo o contacte al administrador del sistema', "tención!");
        }
    });
}
toastr.options = {
  "closeButton": true,
  "debug": false,
  "newestOnTop": false,
  "progressBar": true,
  "positionClass": "toast-top-right",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "4000",
  "timeOut": "5000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}
function sinmodelo(){
    if($('#sinmodelo').is(':checked')){
        $('#modelocliente').html('<option value="113">Sin Modelo</option>');
    }else{
        var modeloshtml=$('#modeloclientexxx').html();
        $('#modelocliente').html(modeloshtml);
    }
    $('#modelocliente').trigger("chosen:updated");
}
var modeloclirow=0;
function addequipo(){
    var modeloid=$('#modelocliente option:selected').val();
    var modelotext=$('#modelocliente option:selected').text();
    var seriecliente=$('#seriecliente').val();
    var html='<tr class="modeloclirow_'+modeloclirow+'">\
                <td>\
                    <input type="hidden" id="cliequId" value="0">\
                    <input type="hidden" id="modeloclienteid" value="'+modeloid+'">\
                    '+modelotext+'\
                </td>\
                <td>\
                    <input type="hidden" id="seriecliente" value="'+seriecliente+'">\
                    '+seriecliente+'\
                </td>\
                <td>\
                    <a class="btn-floating waves-effect waves-light red accent-2" onclick="deleteequipo('+modeloclirow+',0)">\
                        <i class="material-icons">clear</i>\
                    </a>\
                </td>\
              </tr>';
    $('.tbody_modelos_cliente').append(html);
    modeloclirow++;
}
function deleteequipo(row,tipo){
    if(tipo==0){
        $('.modeloclirow_'+row).remove();
    }else{
        $.confirm({
            boxWidth: '30%',
            useBootstrap: false,
            icon: 'fa fa-warning',
            title: 'Atención!',
            content: '¿Desea eliminar el equipo?',
            type: 'red',
            typeAnimated: true,
            buttons:{
                confirmar: function (){
                    
                    $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Clientes/deleteequipo",
                        data: {
                            id:row
                        },
                        success: function (response){
                            $('.modeloclirowe_'+row).remove();
                        },
                        error: function(response){
                            notificacion(1); 
                             
                        }
                    });
                },
                cancelar: function (){
                }
            }
        });
    }
}
function validarrazonsocial(){
    var razon_social=$('#razon_social').val();
        razon_social = razon_social.replaceAll(' ', '_');
    $.alert({boxWidth: '30%',useBootstrap: false,
            title: 'Confirmación',
            content: 'Confirma que la razon social es correcta?<br><span class="spannota">los espacios en blanco se muestran con guiones para hacerlo mas visual</span><br>'+razon_social});  
}
function fismo(){
    var fm=$('input:radio[name=fisicamoral]:checked').val();
    if(fm==1){
        $('#credito').val(1);
    }else{
        $('#credito').val(15);
    }
}
function creditofun(){
    var cre =$('#credito').val();
    var fm=$('input:radio[name=fisicamoral]:checked').val();
    if(fm==1){
        if(cre>1){
            //===================================
                $.confirm({
                    boxWidth: '30%',
                    useBootstrap: false,
                    icon: 'fa fa-warning',
                    title: 'Atención!',
                    content: 'Se necesita permisos de administrador<br>'+
                             '<input type="password" placeholder="Contraseña" id="contrasena" class="name form-control" required/>',
                    type: 'red',
                    typeAnimated: true,
                    buttons:{
                        confirmar: function (){
                            var pass=$('#contrasena').val();
                            if (pass!='') {
                                 $.ajax({
                                    type:'POST',
                                    url: base_url+"index.php/Sistema/solicitarpermiso",
                                    data: {
                                        pass:pass
                                    },
                                    success: function (response){
                                            var respuesta = parseInt(response);
                                            if (respuesta==1) {
                                            }else{
                                                $('#credito').val(1);
                                            }
                                    },
                                    error: function(response){
                                        notificacion(1);
                                    }
                                });
                                
                            }else{
                                notificacion(0);
                                $('#credito').val(1);
                            }
                        },
                        cancelar: function (){
                            $('#credito').val(1);
                        }
                    }
                });
            //====================================
        }
    }
    if(fm==2){
        if(cre>15){
            //===================================
                $.confirm({
                    boxWidth: '30%',
                    useBootstrap: false,
                    icon: 'fa fa-warning',
                    title: 'Atención!',
                    content: 'Se necesita permisos de administrador<br>'+
                             '<input type="password" placeholder="Contraseña" id="contrasena" class="name form-control" required/>',
                    type: 'red',
                    typeAnimated: true,
                    buttons:{
                        confirmar: function (){
                            var pass=$('#contrasena').val();
                            if (pass!='') {
                                 $.ajax({
                                    type:'POST',
                                    url: base_url+"index.php/Sistema/solicitarpermiso",
                                    data: {
                                        pass:pass
                                    },
                                    success: function (response){
                                            var respuesta = parseInt(response);
                                            if (respuesta==1) {
                                            }else{
                                                $('#credito').val(15);
                                            }
                                    },
                                    error: function(response){
                                        notificacion(1);
                                    }
                                });
                                
                            }else{
                                notificacion(0);
                                $('#credito').val(15);
                            }
                        },
                        cancelar: function (){
                            $('#credito').val(15);
                        }
                    }
                });
            //====================================
        }
    }
    if(fm==undefined){
        $('#credito').val(1);
        alertfunction('Advertencia!','Seleccione Fisica o Moral');
    }
}
function alertfunction(titlea,contenta){
    $.alert({boxWidth: '30%',useBootstrap: false,title: titlea,theme: 'bootstrap',content: contenta});  
}
function addaccesoweb(idCliente){
    var html='';
        html+='¿Está agregar acceso al cliente?';
        html+='<br>';
        html+='<label>Correo</label><input type="mail" id="m_correo" class="form-control-bmz">';
        html+='<label>Contraseña</label><input type="text" id="m_pass" class="form-control-bmz">';
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: html,
        type: 'red',
        typeAnimated: true,
        buttons: {
            confirmar: function (){
                var m_correo = $('#m_correo').val();
                var m_pass = $('#m_pass').val();
                $.ajax({
                    type:'POST',
                    url: base_url+'index.php/Clientes/addaccesoweb',
                    data: {
                        correo:m_correo,
                        pass:m_pass,
                        idcli:idCliente
                    },
                    success:function(data){
                        toastr.success('Acceso Agregado');
                    }
                });
            },
            cancelar: function (){
            }
        }
    });
}
function infodell(idcli){
    $('#modalinfopre').modal('open');
    $.ajax({
        type:'POST',
        url: base_url+'index.php/Clientes/presscli',
        data: {
            cli:idcli
        },
        success:function(data){
            $('.infos_press').html(data);
        },
        error: function(response){
            notificacion(1);
        }
    });
}
function usardatosf(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: 'Se necesita permisos de administrador<br>'+
                 '<input type="password" placeholder="Contraseña" id="contrasena" class="name form-control" required/>',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var pass=$('#contrasena').val();
                if (pass!='') {
                     $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Sistema/solicitarpermiso",
                        data: {
                            pass:pass
                        },
                        success: function (response){
                                var respuesta = parseInt(response);
                                if (respuesta==1) {
                                    //===================================
                                    var razon = $('.datosf_'+id).data('razon');
                                    var rfc = $('.datosf_'+id).data('rfc');
                                    var cp = $('.datosf_'+id).data('cp');
                                    var estado = $('.datosf_'+id).data('estado');
                                    var municipio = $('.datosf_'+id).data('municipio');
                                    var localidad = $('.datosf_'+id).data('localidad');
                                    var calle = $('.datosf_'+id).data('calle');
                                    var num_ext = $('.datosf_'+id).data('num_ext');
                                    var num_int = $('.datosf_'+id).data('num_int');
                                    var rfr = $('.datosf_'+id).data('rfr');
                                    var col = $('.datosf_'+id).data('colonia');

                                    $('#razon_social').val(razon);
                                    $('#rfc').val(rfc);
                                    $('#df_estado').val(estado);
                                    $('#df_minicipio').val(municipio);
                                    $('#df_localidad').val(localidad);
                                    $('#cp').val(cp);
                                    $('#calle').val(calle);
                                    $('#num_ext').val(num_ext);
                                    $('#num_int').val(num_int);
                                    $('#colonia').val(col);
                                    $('#RegimenFiscalReceptor').val(rfr);
                                    //===================================
                                }else{
                                    $('#credito').val(1);
                                }
                        },
                        error: function(response){
                            notificacion(1);
                        }
                    });
                    
                }else{
                    notificacion(0);
                    $('#credito').val(1);
                }
            },
            cancelar: function (){
                $('#credito').val(1);
            }
        }
    });
}