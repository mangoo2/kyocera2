<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Listapreciosconsumibles extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->model('consumibles_model');
        $this->load->model('login_model');
        $this->load->helper('url');
        $this->load->library('menus');
        $this->load->model('ModeloCatalogos');
        $this->submenu=20;
        if($this->session->userdata('logeado')==true){
            $this->perfilid =$this->session->userdata('perfilid');
            $this->idpersonal = $this->session->userdata('idpersonal');
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,$this->submenu);// 20 es el id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
        }else{
            redirect('login');
        }
    }

    // Listado de Precios de Equipos
    function index(){
        $data['MenusubId']=$this->submenu;
            // Obtiene los menús disponibles del sistema dependiendo el perfil, ayudado de la libreria "MENUS"
            $menus = $this->menus->getMenus($this->session->userdata('perfilid'));
            $data['menus'] = $menus['menus'];
            $data['submenusArray'] = $menus['submenusArray'];
            // Cargamos las vistas
            $this->load->view('header');
            $this->load->view('main',$data);
            $this->load->view('preciosconsumibles/listado');
            $this->load->view('preciosconsumibles/listado_js');
            $this->load->view('footer');
        
    }

    // Obtener el JSON del listado de equipos para enviar a la tabla
    function getListaPreciosConsumibles(){
        $precios = $this->consumibles_model->getListaPreciosConsumibles();
        $json_data = array("data" => $precios);
        echo json_encode($json_data);
    }

    // Actualizar un precio que venga de la tabla
    function actualizaPrecio(){
        $data = $this->input->post();
        // La variable KEY traerá el ID del equipo a actualizar
        $key = key($data["data"]);
        
        // Comparamos si lo que trae es el precio en Dólares, para hacer los cálculos
        if(key($data["data"][$key])=='precioDolares'){
            // Obtenemos el Tipo de Cambio
            $cambio = $this->consumibles_model->getTipoCambioConsumibles();
            // Convertimos a Pesos 
            $tipoCambio = $cambio[0]->cambio;
            $precio = $data["data"][$key]['precioDolares'];
            $descuentos = $this->consumibles_model->getDescuentosConsumibles();
            $descuentoConsumibles = $descuentos[0]->descuento;
            //======================================================================
                $precioPesos1=$precio*$tipoCambio;
                $precioPesos2=100-$descuentoConsumibles;
            //======================================================================
            //log_message('error', '(('.$precio.' * '.$tipoCambio.')*(100-'.$descuentos[0]->descuento.'))/100');
            $precioPesos = (($precioPesos1)*($precioPesos2))/100;
            //log_message('error', '(('.$precioPesos1.')*('.$precioPesos2.'))/100');
            //log_message('error', $precioPesos);
            // Obtenemos los porcentajes de Ganancia para Refacciones
            $porcentajesGanancia = $this->consumibles_model->getPorcentajeGananciaConsumibles();

            // Guardamos cada uno de los porcentajes correspondientes
            // VENTA = GENERAL; RENTA = ESPECIAL; POLIZA = FRECUENTE; REVENDEDOR = POLIZA
            $porcentajeGeneral = $porcentajesGanancia[0]->gananciaVenta;
            $porcentajeEspecial = $porcentajesGanancia[0]->gananciaPoliza;
            $porcentajeFrecuente = $porcentajesGanancia[0]->gananciaRenta;
            $porcentajePoliza = $porcentajesGanancia[0]->gananciaRevendedor;

            // Obtenemos el total de ganancia para cada tarea
            //$gananciaGeneral = ($porcentajeGeneral * $precioPesos) / 100;
            //$gananciaEspecial = ($porcentajeEspecial * $precioPesos) / 100;
            //$gananciaFrecuente = ($porcentajeFrecuente * $precioPesos) / 100;
            //$gananciaPoliza = ($porcentajePoliza * $precioPesos) / 100;

            // Sumamos las ganancias al precio
            //$precioConGananciaGeneral = $precioPesos + $gananciaGeneral;
            //$precioConGananciaEspecial = $precioPesos + $gananciaEspecial;
            //$precioConGananciaFrecuente = $precioPesos + $gananciaFrecuente;
            //$precioConGananciaPoliza = $precioPesos + $gananciaPoliza;

            // Obtenemos los descuentos
            //$descuentos = $this->consumibles_model->getDescuentosConsumibles();
            //$descuentoConsumibles = $descuentos[0]->descuento;

            // Hacemos los descuentos sobre los precios con Ganancia
            $totalDescuento = ($descuentoConsumibles * $precioPesos) / 100;

            $precioConDescuentoGeneral = $precioPesos/((100-$porcentajeGeneral)/100);
            //log_message('error', $precioPesos.'/((100-'.$porcentajeGeneral.')/100)');
            $precioConDescuentoFrecuente = $precioPesos/(1-($porcentajeFrecuente/100));
            log_message('error', 'frecuente: '.$precioPesos.'/(1-('.$porcentajeFrecuente.'/100))');
            $precioConDescuentoEspecial = $precioPesos/(1-($porcentajeEspecial/100));
            
            $precioConDescuentoPoliza = $precioPesos/(1-($porcentajePoliza/100));

            $data["data"][$key]['general'] = $precioConDescuentoGeneral;
            $data["data"][$key]['especial'] = $precioConDescuentoEspecial;
            $data["data"][$key]['frecuente'] = $precioConDescuentoFrecuente;
            $data["data"][$key]['poliza'] = $precioConDescuentoPoliza;

            $data["data"][$key]['costo_pesos'] = $precioPesos;
            $data["data"][$key]['porcentaje_ganancia'] = $porcentajeGeneral;
            $data["data"][$key]['descuento'] = $totalDescuento;
        } 

        // Se actualiza elregistro        
        $idActualizado = $this->consumibles_model->update_precios_consumibles($data["data"][$key], $key);
        $this->Modelobitacoras->Insert(array('contenido'=>'Se Actualizo Lista de precios consumibles','nombretabla'=>'consumibles_costos','idtable'=>0,'tipo'=>'Update','personalId'=>$this->idpersonal)); 
        // Sí se actualizó bien, se regresan los datos a la tabla
        if($idActualizado==$key){
            // Se ibtienene todos los datos del listado
            $preciosActualizados = $this->consumibles_model->getPreciosConsumiblesPorId($idActualizado);
            $preciosActualizadosAux = (array) $preciosActualizados[0];
            
            // Se arma el array
            $dataRetorno = array($preciosActualizadosAux ); 
            
            // Se crea el data y se regresa en JSON
            $datosRetorno = array("data" => $dataRetorno);
            echo json_encode($datosRetorno);
        }
    }
}