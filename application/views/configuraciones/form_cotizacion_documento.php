<?php 


$label = 'Formulario de Edición de Documento de Cotización';

$foto = $configuracionCotizacion->logo1;
$foto2 = $configuracionCotizacion->logo2;
?>

<!-- START CONTENT --> 
        <section id="content" width="100%">
          

          <!--start container-->
          <div class="container" width="100%">
            <div class="section">
               <h4 class="caption"><?php echo $label; ?>.</h4>
              <div class="row">
                <input id="base_url" type="hidden" name="base_url" value="<?php echo base_url(); ?>">

                <!-- FORMULARIO -->
               

                  <div class="col s12 m12 24">

                    <div class="card-panel">

                      <div class="row">
                         <div class="row">
                          <div class="col s6">
                            <select name="tipov" id="tipov" class="browser-default form-control-bmz" onchange="view_tipov()">
                              <option value="1" <?php if($tipov==1){ echo 'selected';}?> >Kyocera</option>
                              <option value="2" <?php if($tipov==2){ echo 'selected';}?> >D-Impresión</option>
                            </select>
                          </div>
                        </div>
            
                         <div class="row">
                            <div class="input-field col s6" align="center">
                              <h4>Logo 1:</h4>
                               <div class="fileinput fileinput-new" data-provides="fileinput">

                                  <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 230px; height: 210px; border-radius: 12px;">

                                    <input type="file" id="logo1" name="logo1" class="dropify" data-default-file="<?php echo base_url(); ?>app-assets/images/<?php echo $foto; ?>">

                                  </div>

                               </div>
                            </div>
                            <div class="input-field col s6" align="center">
                              <h4>Logo 2:</h4>
                               <div class="fileinput fileinput-new" data-provides="fileinput">
                                   <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 230px; height: 210px; border-radius: 12px;">
                                  
                                  <input type="file" id="logo2" name="logo2" class="dropify" data-default-file="<?php echo base_url(); ?>app-assets/images/<?php echo $foto2; ?>">
                                  </div>

                               </div>
                            </div>
                          </div>
                          <br>
                        <form class="form" id="configuracion_cotizacion_form" method="post">
                          <div class="row" align="center">
                             <div class="input-field col s6" >
                              <h5>Encabezado</h5>
                              <textarea id="textoHeader" name="textoHeader" ><?php echo $configuracionCotizacion->textoHeader; ?></textarea>

                            </div>
                            <div class="input-field col s6" >
                              <h5>Sub encabezado</h5>
                              <textarea id="textoHeader2" name="textoHeader2" ><?php echo $configuracionCotizacion->textoHeader2; ?></textarea>
                            </div>
                          </div>
                          <br>

                          <div class="row" align="center">
                             <div class="input-field col s6">
                                <h5>1er Pie de página</h5>
                                <textarea id="leyendaPrecios" name="leyendaPrecios" ><?php echo $configuracionCotizacion->leyendaPrecios; ?></textarea>
                              </div>
                          </div>

                          <br>
                          <div class="row" align="center">
                             <div class="input-field col s6">
                                <h5>Titulo Observaciones</h5>
                                <textarea id="tituloObservaciones" name="tituloObservaciones" ><?php echo $configuracionCotizacion->tituloObservaciones; ?></textarea>
                              </div>
                              <div class="input-field col s6">
                                <h5>Contenido Observaciones</h5>
                                <textarea id="contenidoObservaciones" name="contenidoObservaciones" ><?php echo $configuracionCotizacion->contenidoObservaciones; ?></textarea>
                              </div>
                          </div>

                          <br>
                          <div class="row" align="center">
                             <div class="input-field col s6">
                                <h5>Titulo 2do Pie de página</h5>
                                <textarea id="titulo1Footer" name="titulo1Footer" ><?php echo $configuracionCotizacion->titulo1Footer; ?></textarea>
                              </div>
                              <div class="input-field col s6">
                                <h5>Contenido 2do Pie de página</h5>
                                <textarea id="contenido1Footer" name="contenido1Footer"> <?php echo $configuracionCotizacion->contenido1Footer; ?></textarea>
                              </div>
                          </div>

                          <br>
                          <div class="row" align="center">
                             <div class="input-field col s6">
                                <h5>Titulo 3er Pie de página</h5>
                                <textarea id="titulo2Footer" name="titulo2Footer" ><?php echo $configuracionCotizacion->titulo2Footer; ?></textarea>
                              </div>
                              <div class="input-field col s6">
                                <h5>Contenido 3er Pie de página</h5>
                                <textarea id="contenido2Footer" name="contenido2Footer" ><?php echo $configuracionCotizacion->contenido2Footer; ?></textarea>
                              </div>
                          </div>

                          <br>
                          <div class="row" align="center">
                             <div class="input-field col s6">
                                <h5>Firma</h5>
                                <textarea id="firma" name="firma" ><?php echo $configuracionCotizacion->firma; ?></textarea>
                              </div>
                              <div class="input-field col s6">
                                <h5>Color de ecabezado</h5>
                                <input type="color" name="colorencabezado" id="colorencabezado" value="<?php echo $configuracionCotizacion->color; ?>" style="border: 50px;height: 78px;width: 159px;">
                              </div>
                          </div>

                          <br>
                          <div class="row" align="center">
                            <div class="input-field col s12">
                              <button class="btn cyan waves-effect waves-light center" type="submit">Guardar
                                <i class="material-icons right">send</i>
                              </button>
                            </div>
                          </div>
                               
                        </form>                   
                      </div>

                    </div>

                  </div>
                
              </div>
            </div>
          </div>
        </section>