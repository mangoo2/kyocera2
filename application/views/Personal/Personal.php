<div class="row">
                <div class="col-md-12">
                  <h2>Personal</h2>
                </div>
                <div class="col-md-12">
                  <div class="col-md-11"></div>
                  <div class="col-md-1">
                    <a href="Personal/Personaladd" class="btn btn-raised gradient-ibiza-sunset white sidebar-shadow"><i class="fa fa-plus"></i></span> Nuevo</a>
                  </div>
                </div>
              </div>

              <!--Statistics cards Ends-->
              <!--Line with Area Chart 1 Starts-->
              <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Listado de personal</h4>
                        </div>
                        <div class="card-body">
                            <div class="card-block form-horizontal">
                	<!-------------->
                	<div class="col-md-12">
                                    <table class="table table-striped" id="data-tables">
                                      <thead>
                                        <tr>
                                          <th>Nombre</th>
                                          <th></th>
                                        </tr>
                                      </thead>
                                      <tbody>

                                        <?php foreach ($personal->result() as $item){ ?>
                                         <tr id="trper_<?php echo $item->personalId; ?>">
                                                  <td><?php echo $item->nombre; ?></td>
                                                  <td>
                                            
                                                    <div class="btn-group mr-1 mb-1">
                                                      <button type="button" class="btn btn-raised btn-icon btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="icon-settings"></i></button>
                                                      <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">
                                                        <a class="dropdown-item" href="personal/Personaladd?id=<?php echo $item->personalId; ?>">Editar</a>
                                                        <a class="dropdown-item" href="#" onclick="personaldelete(<?php echo $item->personalId; ?>);">Eliminar</a>
                                        
                                                      </div>
                                                  </td>
                                             
                                                </tr>
                                          
                                        <?php } ?>
                                            
                                      </tbody>
                                    </table>
                                </div>
                	<!-------------->
                </div>
              </div>
          </div>
      </div>
    </div>
<script type="text/javascript">
        $(document).ready(function() {
                $('#data-tables').dataTable();
        } );
        
</script>