<style type="text/css">
  .etiqueta{
    text-align: center;
    color: #fff;
    padding: 1px;
    border-radius: 6px;
  }
  .etiquetasalida{
    background-color: #26d744; text-align: center;
  }
  .etiquetaentrada{
    background-color: #00e5ff; text-align: center;
  }
  .chosen-container{
    margin-left: 0px;
  }
  table td{
    font-size: 12px;
    padding: 6px 7px;
  }

</style>
<input type="hidden" id="foliotext" value="<?php if(isset($_GET['foliotext'])){echo $_GET['foliotext'];}?>">
<!-- START CONTENT -->
    <section id="content">
      <!--start container-->
        <div class="container">
          <div class="section">
          <div id="table-datatables">
            <br>
                <h4 class="header">Listado de Folios</h4>
                <div class="row">
                  <input id="base_url" type="hidden" name="base_url" value="<?php echo base_url(); ?>">
                  <div class="col s3">
                    <a class="btn waves-effect waves-light gradient-45deg-green-teal gradient-shadow" onclick="modalretorno()">Registrar retorno</a>
                  </div>
                  <div class="col s2">
                    <label>Seleccionar cliente</label>
                    <select id="idcliente" class="browser-default chosen-select" onchange="load()">
                          <option value="0">Selecciona una opción</option>
                          <?php 
                            foreach ($clientes as $item) {
                              echo '<option value="'.$item->id.'">'.$item->empresa.'</option>';
                            }
                          ?>
                        </select>
                  </div>
                  <div class="col s2">
                    <label>Estatus</label>
                    <select id="cf_estatus" class="browser-default form-control-bmz" onchange="load()">
                      <option value="0">Todos</option>
                      <option value="1">Stock Cliente</option>
                      <option value="2">Retorno</option>
                    </select>
                  </div>
                  <div class="col s2">
                    <a class="btn-bmz waves-effect waves-light gradient-45deg-green-teal gradient-shadow" onclick="exportar()">Exportar</a>
                  </div>
                </div>
                <div class="row">
                  <div class="col s2">
                    <label>Fecha inicial</label>
                    <input type="date" class="form-control-bmz" id="finicial" value="<?php if(isset($_GET['foliotext'])){}else{ echo $fechainicial_reg;}?>" onchange="load()">
                  </div>
                  <div class="col s2">
                    <label>Fecha final</label>
                    <input type="date" class="form-control-bmz" id="ffinal" onchange="load()">
                  </div>
                  <div class="col s2">
                    <label>Toner</label>
                    <select class="form-control-bmz browser-default" id="select_toner" onchange="load()">
                      <option value="x">Seleccione</option>
                      <option value="0">Todos</option>
                      <?php foreach ($con_row->result() as $item) {
                        echo '<option value="'.$item->id.'">'.$item->modelo.'</option>';
                      }

                      ?>
                    </select>
                  </div>
                  <div class="col s12">
                    <table id="tabla_con" class="responsive-table display" cellspacing="0">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Modelo</th>
                          <th>Parte</th>
                          <th>Vendedor</th>
                          <th>Fecha</th>
                          <th>Cliente</th>
                          <th>Folio</th>
                          <th>Estatus</th>
                          <th>Porcentaje<br>Retorno</th>
                          <th>Fecha Retorno</th>
                          <th></th>
                          <th></th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
          </div> 
        </div>
     </div>
    </section>

<div id="modalRetorno" class="modal"  style="min-width: 80%;">
  <div class="modal-content" style="padding: 5px">
    <div class="row">
      <div class="col s9 m9">  
          <div class="input-field">
            <i class="material-icons prefix">view_week</i>
            <input id="codigo" type="text" placeholder="Código">
          </div>
      </div>
      <div class="col s3"><br>
        <a class="btn waves-effect waves-light"  style="background-color: #26d744; text-align: center" onclick="btnretorno()">Buscar</a>
      </div>
    </div> 
    <div class="row">
          <div class="mregistro col s12" style="display: none">
            <table id="tabla_folio" class="responsive-table display" cellspacing="0">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Modelo</th>
                  <th>Parte</th>
                  <th>Folio</th>
                  <th>Cliente</th>
                  <th>Fecha</th>
                  <th></th>
                  <th>Comentario</th>
                  <th></th>
                </tr>
              </thead>
              <tbody class="etiquetacodigo">
              </tbody>
            </table>
          </div>
    </div> 
  </div>  
  <div class="modal-footer">
    <a class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
    <a class="btn waves-effect waves-light cyan" onclick="updateretorno()">Registrar retorno</a>
  </div>
</div>