<!DOCTYPE html>
<html>
<head>
  <title>Mapa con Google Maps</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <style>
    body{
      margin: 0px;
    }
    #map {
      height: 98vh;
      width: 100%;
    }
  </style>
  <!--
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyABSaHvdLATWTIeJuvYs2Xv4utVc70gR1Y"></script>-->
  <script loading="async" async src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDD2MlZRITOyVSZ8GtlEhmNEFWpw3_73CE"></script>
  
  <!--<script src="script3.js?v=3"></script>-->
  <script type="text/javascript">
    <?php $pintarsegundaruta=1;?>
    /*
    const locations = [
      { lat: 19.006563, lng: -98.149418, message: 'Mensaje 1' },
      { lat: 19.014234, lng: -98.137846, message: 'Mensaje 2' },
      { lat: 19.024810, lng: -98.140679, message: 'Mensaje 3' },
    ];
    */
    //====================================v1
      /*
      const locations = [
        <?php 
            if($result->num_rows()==0){
              echo "{ lat: 19.08782533671139, lng: -98.22777569573624, message: 'Sin registros' }";
            }
          foreach ($result->result() as $item) { 
          echo "{ lat: ".$item->latitud.", lng: ".$item->longitud.", message: '".$item->reg."' },";
        }?>
      ];
      const locations2 = [
        <?php foreach ($arrayubicli as $itemc) {
          echo "{ lat: ".$itemc['latitud'].", lng: ".$itemc['longitud'].", message: '".str_replace("'", "", $itemc['message'])."' },";
        }

        ?>
      ];
      */
    //=========================================
    //=========================================v2
        var DATA1  = [];
        var DATA2  = [];
        <?php if($result->num_rows()==0){ ?>
              item1 = {};
              item1 ["lat"]   = 19.08782533671139;
              item1 ["lng"]  = -98.22777569573624;
              item1 ["message"]  = 'Sin registros';
              DATA1.push(item1);
        <?php }
          foreach ($result->result() as $item) { ?>
          item1 = {};
          item1 ["lat"]   = <?php echo $item->latitud; ?>;item1 ["lng"]  = <?php echo $item->longitud; ?>;item1 ["message"]  = '<?php echo $item->reg; ?>';
          DATA1.push(item1);
      <?php } 
          foreach ($arrayubicli as $itemc) { ?>
          item2 = {};
          item2 ["lat"]   = <?php echo $itemc['latitud'] ?>;item2 ["lng"]  = <?php echo $itemc['longitud'] ?>;item2 ["message"]  = '<?php echo str_replace("'", "", $itemc['message']) ?>';
          DATA2.push(item2);
        <?php }
      ?>
      const locations=DATA1;
      const locations2=DATA2;
    //=========================================
    function initMap() {
  // Create a map centered on the first location
  const map = new google.maps.Map(document.getElementById('map'), {
    center: { lat: locations[0].lat, lng: locations[0].lng },
    zoom: 13
  });

  // Define custom icons for each set of locations
  const customIcon1 = {
    url: '<?php echo base_url();?>public/img/file-info_40446.png', // URL of the icon image for the first set
    scaledSize: new google.maps.Size(20, 20),
    origin: new google.maps.Point(0, 0),
    anchor: new google.maps.Point(0, 10)
  };

  const customIcon2 = {
    url: '<?php echo base_url();?>public/img/edificio.png', // URL of the icon image for the second set
    scaledSize: new google.maps.Size(35, 35),
    origin: new google.maps.Point(0, 0),
    anchor: new google.maps.Point(0, 10)
  };

  // Add markers to the map with info windows for the first set of locations
  locations.forEach(location => {
    const marker = new google.maps.Marker({
      position: { lat: location.lat, lng: location.lng },
      map,
      icon: customIcon1
    });

    // Create an info window with the message
    const infowindow = new google.maps.InfoWindow({
      content: `<p>${location.message}</p>` // Display message as HTML
    });

    // Attach a click event listener to the marker
    marker.addListener('click', () => {
      infowindow.open(map, marker); // Open the info window when clicked
    });
  });

  // Add markers to the map with info windows for the second set of locations
  locations2.forEach(location => {
    const marker = new google.maps.Marker({
      position: { lat: location.lat, lng: location.lng },
      map,
      icon: customIcon2
    });

    // Create an info window with the message
    const infowindow = new google.maps.InfoWindow({
      content: `<p>${location.message}</p>` // Display message as HTML
    });

    // Attach a click event listener to the marker
    marker.addListener('click', () => {
      infowindow.open(map, marker); // Open the info window when clicked
    });
  });

  // Create a DirectionsService and DirectionsRenderer for the first route
  const directionsService = new google.maps.DirectionsService();
  const directionsRenderer = new google.maps.DirectionsRenderer({
    map,
    polylineOptions: {
      strokeColor: 'blue'
    }
  });

  // Configure the first route
  const waypoints = locations.slice(1, -1).map(location => ({
    location: new google.maps.LatLng(location.lat, location.lng),
    stopover: true
  }));

  const request = {
    origin: new google.maps.LatLng(locations[0].lat, locations[0].lng),
    destination: new google.maps.LatLng(locations[locations.length - 1].lat, locations[locations.length - 1].lng),
    waypoints,
    travelMode: google.maps.TravelMode.DRIVING
  };

  // Calculate and display the first route
  directionsService.route(request, (result, status) => {
    if (status === google.maps.DirectionsStatus.OK) {
      directionsRenderer.setDirections(result);
    } else {
      console.error('Error getting directions:', status);
    }
  });

  // Create a DirectionsService and DirectionsRenderer for the second route
  const directionsService2 = new google.maps.DirectionsService();
  const directionsRenderer2 = new google.maps.DirectionsRenderer({
    map,
    polylineOptions: {
      strokeColor: 'red'
    }
  });

  // Configure the second route
  const waypoints2 = locations2.slice(1, -1).map(location => ({
    location: new google.maps.LatLng(location.lat, location.lng),
    stopover: true
  }));

  const request2 = {
    origin: new google.maps.LatLng(locations2[0].lat, locations2[0].lng),
    destination: new google.maps.LatLng(locations2[locations2.length - 1].lat, locations2[locations2.length - 1].lng),
    waypoints: waypoints2,
    travelMode: google.maps.TravelMode.DRIVING
  };

  // Calculate and display the second route
  directionsService2.route(request2, (result, status) => {
    if (status === google.maps.DirectionsStatus.OK) {
      directionsRenderer2.setDirections(result);
    } else {
      console.error('Error getting directions:', status);
    }
  });
}

// Load the map when the page is fully loaded
window.onload = initMap;
  </script>
</head>
<body>
  <div id="map"></div>
</body>
</html>