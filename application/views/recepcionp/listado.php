<style type="text/css">
  #tabla_equipos th,#tabla_equipos td{
    font-size: 12px;
  }

</style>

<!-- START CONTENT -->
    <section id="content">
      <!--start container-->
        <div class="container">
          <div class="section">


          <div id="table-datatables">
            <br>
                <h4 class="header">Listado de Equipos</h4>
                <div class="row">
                  <div class="col s6" style="display:<?php echo $display;?>">
                    <label>Personal</label>
                    <select id="idpersonal" class="browser-default form-control-bmz" onchange="loadtable()">
                      <option value="0">Todos</option>
                      <?php foreach ($personalrow->result() as $item) { ?>
                          <option value="<?php echo $item->personalId;?>" <?php if($idpersonal==$item->personalId){ echo 'selected';} ?>><?php echo $item->nombre.' '.$item->apellido_paterno.' '.$item->apellido_materno;?></option>
                      <?php } ?>
                    </select>
                  </div>
                  

                  <div class="col s12">
                
                    <table id="tabla_equipos" class="table bordered striped" cellspacing="0">
                      <thead>
                        <tr>
                          <th>Cant.</th>
                          <th>Modelo</th>
                          <th>No Parte</th>
                          <th></th>
                          <th></th>
                          <th></th>
                          <th>Acciones</th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>  