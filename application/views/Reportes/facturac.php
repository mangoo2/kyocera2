<?php
require_once dirname(__FILE__) . '/TCPDF3/examples/tcpdf_include.php'; 
require_once dirname(__FILE__) . '/TCPDF3/tcpdf.php';
$GLOBALS["Folio"]=$Folio;
$GLOBALS["Nombrerasonsocial"]=$Nombrerasonsocial;
$GLOBALS["folio_fiscal"]=$folio_fiscal;
$GLOBALS["rrfc"]=$rrfc;
$GLOBALS["rdireccion"]=$rdireccion;
$GLOBALS["nocertificadosat"]=$nocertificadosat;
$GLOBALS['regimenf']=$regimenf;
$GLOBALS["certificado"]=$certificado;
$GLOBALS["cfdi"]=$cfdi;
$GLOBALS["fechatimbre"]=$fechatimbre;
$GLOBALS["Estado"]=1;//en dado caso de que se encuentre cancelado
$GLOBALS["cp"]=$cp;
$GLOBALS["cliente"]=$cliente;
$GLOBALS["clirfc"]=$clirfc;
$GLOBALS["clidireccion"]=$clidireccion.' C.P. '.$cp;
$GLOBALS["isr"]=$isr;
$GLOBALS['numordencompra']=$numordencompra;
$GLOBALS["ivaretenido"]=$ivaretenido;
$GLOBALS["cedular"]=$cedular;
$GLOBALS["cincoalmillarval"]=$cincoalmillarval;
$GLOBALS["outsourcing"]=$outsourcing;
$GLOBALS["RegimenFiscalReceptor"]=$RegimenFiscalReceptor;

$GLOBALS['f_relacion'] = $f_relacion;
$GLOBALS['f_r_tipo'] = $f_r_tipo;
$GLOBALS['f_r_uuid'] = $f_r_uuid;

if ($numproveedor!='') {
    $GLOBALS['numproveedor']=$numproveedor;
    $GLOBALS['numproveedorv']='block';
}else{
    $GLOBALS['numproveedor']='';
    $GLOBALS['numproveedorv']='none';
}

if ($numordencompra!='') {
    $GLOBALS['numordencompra']=$numordencompra;
    $GLOBALS['numordencomprav']='block';
}else{
    $GLOBALS['numordencompra']='';
    $GLOBALS['numordencomprav']='none';
}
$GLOBALS['observaciones']=$observaciones;
$GLOBALS["total"]=$total;
$GLOBALS["moneda"]=$moneda;
if ($moneda=='pesos') {
  $GLOBALS['abreviaturamoneda']='MXN';
}else{
  $GLOBALS['abreviaturamoneda']='USD';
}
$GLOBALS["subtotal"]=$subtotal;
$GLOBALS["iva"]=$iva;
$GLOBALS["tarjeta"]=$tarjeta;
$GLOBALS["selloemisor"]=$selloemisor;
$GLOBALS["sellosat"]=$sellosat;
$GLOBALS["cadenaoriginal"]=$cadenaoriginal;
if ($cadenaoriginal=='') {
  $serie='';
}else{
  $series=explode("|", $cadenaoriginal);
  $serie=$series[3];
}
$GLOBALS["serie"]=$serie;

$GLOBALS["FormaPago"]=$FormaPago;
$GLOBALS["FormaPagol"]=$FormaPagol;
$GLOBALS["MetodoPago"]=$MetodoPago;
$GLOBALS["MetodoPagol"] =$MetodoPagol; 
$GLOBALS["tipoComprobante"]=$tipoComprobante;
$GLOBALS["facturadetalles"]=$facturadetalles;
if($clirfc=='XAXX010101000'){
  $GLOBALS["nombrecliente"]='<b>Cliente: </b>'.$empresa;
}else{
  $GLOBALS["nombrecliente"]='';
}
//====================================================
class MYPDF extends TCPDF {
  //===========================================================
      var $Void = ""; 
      var $SP = " "; 
      var $Dot = "."; 
      var $Zero = "0"; 
      var $Neg = "Menos";
      function ValorEnLetras($x, $Moneda ){ 
        $s=""; 
        $Ent=""; 
        $Frc=""; 
        $Signo=""; 
             
        if(floatVal($x) < 0) 
         $Signo = $this->Neg . " "; 
        else 
         $Signo = ""; 
         
        if(intval(number_format($x,2,'.','') )!=$x) //<- averiguar si tiene decimales 
          $s = number_format($x,2,'.',''); 
        else 
          $s = number_format($x,2,'.',''); 
            
        $Pto = strpos($s, $this->Dot); 
             
        if ($Pto === false) 
        { 
          $Ent = $s; 
          $Frc = $this->Void; 
        } 
        else 
        { 
          $Ent = substr($s, 0, $Pto ); 
          $Frc =  substr($s, $Pto+1); 
        } 

        if($Ent == $this->Zero || $Ent == $this->Void) 
           $s = "Cero "; 
        elseif( strlen($Ent) > 7) 
        { 
           $s = $this->SubValLetra(intval( substr($Ent, 0,  strlen($Ent) - 6))) .  
                 "Millones " . $this->SubValLetra(intval(substr($Ent,-6, 6))); 
        } 
        else 
        { 
          $s = $this->SubValLetra(intval($Ent)); 
        } 

        if (substr($s,-9, 9) == "Millones " || substr($s,-7, 7) == "Millón ") 
           $s = $s . "de "; 

        $s = $s . $Moneda; 
        if ($Moneda=='pesos') {
          $abreviaturamoneda='M.N.';
        }else{
          $abreviaturamoneda='U.S.D';
        }

        if($Frc != $this->Void) 
        { 
           $s = $s . " " . $Frc. "/100"; 
           //$s = $s . " " . $Frc . "/100"; 
        } 
        $letrass=$Signo . $s . " ".$abreviaturamoneda; 
        return ($Signo . $s . " ".$abreviaturamoneda);    
      } 
      function SubValLetra($numero) { 
          $Ptr=""; 
          $n=0; 
          $i=0; 
          $x =""; 
          $Rtn =""; 
          $Tem =""; 

          $x = trim("$numero"); 
          $n = strlen($x); 

          $Tem = $this->Void; 
          $i = $n; 
           
          while( $i > 0) 
          { 
             $Tem = $this->Parte(intval(substr($x, $n - $i, 1).  
                                 str_repeat($this->Zero, $i - 1 ))); 
             If( $Tem != "Cero" ) 
                $Rtn .= $Tem . $this->SP; 
             $i = $i - 1; 
          } 

           
          //--------------------- GoSub FiltroMil ------------------------------ 
          $Rtn=str_replace(" Mil Mil", " Un Mil", $Rtn ); 
          while(1) 
          { 
             $Ptr = strpos($Rtn, "Mil ");        
             If(!($Ptr===false)) 
             { 
                If(! (strpos($Rtn, "Mil ",$Ptr + 1) === false )) 
                  $this->ReplaceStringFrom($Rtn, "Mil ", "", $Ptr); 
                Else 
                 break; 
             } 
             else break; 
          } 

          //--------------------- GoSub FiltroCiento ------------------------------ 
          $Ptr = -1; 
          do{ 
             $Ptr = strpos($Rtn, "Cien ", $Ptr+1); 
             if(!($Ptr===false)) 
             { 
                $Tem = substr($Rtn, $Ptr + 5 ,1); 
                if( $Tem == "M" || $Tem == $this->Void) 
                   ; 
                else           
                   $this->ReplaceStringFrom($Rtn, "Cien", "Ciento", $Ptr); 
             } 
          }while(!($Ptr === false)); 

          //--------------------- FiltroEspeciales ------------------------------ 
          $Rtn=str_replace("Diez Un", "Once", $Rtn ); 
          $Rtn=str_replace("Diez Dos", "Doce", $Rtn ); 
          $Rtn=str_replace("Diez Tres", "Trece", $Rtn ); 
          $Rtn=str_replace("Diez Cuatro", "Catorce", $Rtn ); 
          $Rtn=str_replace("Diez Cinco", "Quince", $Rtn ); 
          $Rtn=str_replace("Diez Seis", "Dieciseis", $Rtn ); 
          $Rtn=str_replace("Diez Siete", "Diecisiete", $Rtn ); 
          $Rtn=str_replace("Diez Ocho", "Dieciocho", $Rtn ); 
          $Rtn=str_replace("Diez Nueve", "Diecinueve", $Rtn ); 
          $Rtn=str_replace("Veinte Un", "Veintiun", $Rtn ); 
          $Rtn=str_replace("Veinte Dos", "Veintidos", $Rtn ); 
          $Rtn=str_replace("Veinte Tres", "Veintitres", $Rtn ); 
          $Rtn=str_replace("Veinte Cuatro", "Veinticuatro", $Rtn ); 
          $Rtn=str_replace("Veinte Cinco", "Veinticinco", $Rtn ); 
          $Rtn=str_replace("Veinte Seis", "Veintiseís", $Rtn ); 
          $Rtn=str_replace("Veinte Siete", "Veintisiete", $Rtn ); 
          $Rtn=str_replace("Veinte Ocho", "Veintiocho", $Rtn ); 
          $Rtn=str_replace("Veinte Nueve", "Veintinueve", $Rtn ); 

          //--------------------- FiltroUn ------------------------------ 
          If(substr($Rtn,0,1) == "M") $Rtn = "Un " . $Rtn; 
          //--------------------- Adicionar Y ------------------------------ 
          for($i=65; $i<=88; $i++) 
          { 
            If($i != 77) 
               $Rtn=str_replace("a " . Chr($i), "* y " . Chr($i), $Rtn); 
          } 
          $Rtn=str_replace("*", "a" , $Rtn); 
          return($Rtn); 
      } 
      function ReplaceStringFrom(&$x, $OldWrd, $NewWrd, $Ptr) { 
        $x = substr($x, 0, $Ptr)  . $NewWrd . substr($x, strlen($OldWrd) + $Ptr); 
      } 
      function Parte($x) { 
          $Rtn=''; 
          $t=''; 
          $i=''; 
          Do 
          { 
            switch($x) 
            { 
               Case 0:  $t = "Cero";break; 
               Case 1:  $t = "Un";break; 
               Case 2:  $t = "Dos";break; 
               Case 3:  $t = "Tres";break; 
               Case 4:  $t = "Cuatro";break; 
               Case 5:  $t = "Cinco";break; 
               Case 6:  $t = "Seis";break; 
               Case 7:  $t = "Siete";break; 
               Case 8:  $t = "Ocho";break; 
               Case 9:  $t = "Nueve";break; 
               Case 10: $t = "Diez";break; 
               Case 20: $t = "Veinte";break; 
               Case 30: $t = "Treinta";break; 
               Case 40: $t = "Cuarenta";break; 
               Case 50: $t = "Cincuenta";break; 
               Case 60: $t = "Sesenta";break; 
               Case 70: $t = "Setenta";break; 
               Case 80: $t = "Ochenta";break; 
               Case 90: $t = "Noventa";break; 
               Case 100: $t = "Cien";break; 
               Case 200: $t = "Doscientos";break; 
               Case 300: $t = "Trescientos";break; 
               Case 400: $t = "Cuatrocientos";break; 
               Case 500: $t = "Quinientos";break; 
               Case 600: $t = "Seiscientos";break; 
               Case 700: $t = "Setecientos";break; 
               Case 800: $t = "Ochocientos";break; 
               Case 900: $t = "Novecientos";break; 
               Case 1000: $t = "Mil";break; 
               Case 1000000: $t = "Millón";break; 
            } 

            If($t == $this->Void) 
            { 
              $i = floatval($i) + 1; 
              $x = $x / 1000; 
              If($x== 0) $i = 0; 
            } 
            else 
               break; 
                  
          }while($i != 0); 
          
          $Rtn = $t; 
          Switch($i) 
          { 
             Case 0: $t = $this->Void;break; 
             Case 1: $t = " Mil";break; 
             Case 2: $t = " Millones";break; 
             Case 3: $t = " Billones";break; 
          } 
          return($Rtn . $t); 
      }  
  //===========================================================
  //Page header
  public function Header() {
      $logos = base_url().'public/img/alta.png';
      $logos2 = base_url().'public/img/kyocera.png';
      if ($GLOBALS["Estado"]==0) {
        $cancelado='<span  style="color:red; font-size:20px"><b>Cancelado</b></span>';
      }else{
        $cancelado='';
      }
        $tamano1='30';//30
        $tamano2='40';//40
        $tamano3='30';//30
      $html = '
          <style type="text/css">
            .info_fac{
              font-size: 9px;
            }
            .info_facd{
              font-size: 8px;
            }
            .httablelinea{
              vertical-align: center;
              border-bottom: 1px solid #9e9e9e;
            }
            
            .httableleft{
              border-left: 1px solid #9e9e9e;
            }
            .httableright{
              border-right: 1px solid #9e9e9e;
            }
            .httabletop{
              border-top: 1px solid #9e9e9e;
            }
          </style>
          <style type="text/css">
        .httable{
          vertical-align: center;
          border-bottom: 2px solid #9e9e9e;
          border-top: 1px solid #9e9e9e;

        }
        .httablepro{
            vertical-align: center;
            border-bottom: 1px solid #9e9e9e;
          }
        .magintablepro{
            margin-top:0px;
            margin-bottom:0px;
            margin: 0px;
        }
      </style>
          <table width="100%" border="0" cellpadding="4px" class="info_fac">
            <tr>
              <td width="23%"><img src="'.$logos.'"></td>
              <td width="54%" rowspan="2" align="center" class="httablepro">
                <span style="margin:0px; font-size:16px;">ALTA PRODUCTIVIDAD</span><br>
                <span style="margin:0px; font-size:11px;"><b>Domicilio fiscal:</b></span><br>
                <span style="margin:0px; font-size:11px;">Calle: BOULEVARD NORTE No. 1831, Col. VILLAS SAN ALEJANDRO</span>
                <span style="margin:0px; font-size:11px;">PUEBLA, PUEBLA, MÉXICO, CP:72090</span><br>
                <span style="margin:0px; font-size:11px;">RFC: APR980122KZ6</span><br><br>
                <span style="margin:0px; font-size:11px;">Tel: 2733400</span>
              </td>
              <td width="23%"><img src="'.$logos2.'" width="150px"></td>
            </tr>
            <tr >
              <td class="httablepro"></td>
              <td class="httablepro"></td>
            </tr>
            </table>
            <table width="100%" border="0" cellpadding="4px" class="info_fac">
              <tr>
                <td colspan="2" align="center">
                  <span style="margin:0px; font-size:11px;"><b>Almacen General</b></span>
                </td>
              </tr>
              <tr>
                <td><b>Lugar de expedición:</b> 72090 - PUEBLA</td>
                <td><b>Tipo de comprobante:</b> '.$GLOBALS["tipoComprobante"].'</td>
              </tr>
              <tr>
                <td>'.$GLOBALS["nombrecliente"].'</td>
                <td><b>Fecha:</b> '.$GLOBALS["fechatimbre"].'</td>
              </tr>
              <tr>
                <td><b>Facturado a:</b> '.$GLOBALS["cliente"].'</td>
                <td><b>R.F.C:</b> '.$GLOBALS["clirfc"].'</td>
              </tr>';
              if($GLOBALS["RegimenFiscalReceptor"]!=''){
        $html.='<tr>
                <td><b>Residencia fiscal:</b> '.$GLOBALS["clidireccion"].'</td>
                <td><b>Uso de CFDI:</b> '.$GLOBALS["cfdi"].'</td>
              </tr>';
        $html.='<tr>
                  <td class="httablepro"><b>Domicilio fiscal:</b> '.$GLOBALS["cp"].'</td>
                  <td class="httablepro"><b>Régimen Fiscal Receptor:</b> '.$GLOBALS["RegimenFiscalReceptor"].'</td>
                </tr>';
              }else{
        $html.='<tr>
                <td class="httablepro"><b>Residencia fiscal:</b> '.$GLOBALS["clidireccion"].'</td>
                <td class="httablepro"><b>Uso de CFDI:</b> '.$GLOBALS["cfdi"].'</td>
              </tr>';
              }
      $html.='<tr>
                <td><b>Serie:</b> '.$GLOBALS["serie"].'</td>
                <td><b>Folio:</b> '.$GLOBALS["Folio"].'</td>
              </tr>
              <tr>
                <td class="httablepro"><b>Folio fiscal:</b> '.$GLOBALS["folio_fiscal"].' <br> '.$cancelado.'</td>
                <td class="httablepro"></td>
              </tr>
              <tr>
                <td><b>Régimen fiscal:</b> '.$GLOBALS['regimenf'].'</td>
                <td><b>NO. DE SERIE DEL CERTIFICADO DEL SAT: </b>'.$GLOBALS["nocertificadosat"].'</td>
              </tr>
              <tr>
                <td class="httablepro"><b>Moneda: </b>'.$GLOBALS['abreviaturamoneda'].'</td>
                <td class="httablepro"><b>NO. DE SERIE DEL CERTIFICADO DEL EMISOR: </b>'.$GLOBALS["certificado"].'</td>
              </tr>
            </table>
          ';
        $this->writeHTML($html, true, false, true, false, '');
  }
    // Page footer
  public function Footer() {
    $html2='';
    $styleQR = array('border' => 0, 
         'vpadding' => '0', 
         'hpadding' => '0', 
         'fgcolor' => array(0, 0, 0), 
         'bgcolor' => false, 
         'module_width' => 1, 
         'module_height' => 1);
          $urlfolio_fe=substr($GLOBALS["selloemisor"], -8); 
          $urlfolio='https://verificacfdi.facturaelectronica.sat.gob.mx/default.aspx?&id='.$GLOBALS["folio_fiscal"].'&re=APR980122KZ6&rr='.$GLOBALS["clirfc"].'&tt='.$GLOBALS["total"].'&fe='.$urlfolio_fe;
          $params = $this->serializeTCPDFtagParameters(array($urlfolio, 'QRCODE,L', '', '', 45, 45, $styleQR, 'N'));
    $html='';

    $html2 .= ' 
          <style type="text/css">
              .fontFooter10{
                font-size: 10px;
              }
              .fontFooterp{
                font-size: 9px;
                margin-top:0px;
              }
              .fontFooter{
                font-size: 9px;
                margin-top:0px;
              }

              .fontFooterp{
                font-size: 8px;
              }
              .fontFooterpt{
                font-size: 7px;
              }
              .fontFooterpt6{
                font-size: 6px;
              }
              p{
                margin:0px;
              }
              .valign{
                vertical-align:middle;
              }
              .httablelinea{
                vertical-align: center;
                border-bottom: 1px solid #9e9e9e;
              }
              .footerpage{
                font-size: 9px;
                color: #9e9e9e;
              }
          </style>';
          $html2 .= '<table width="100%" border="0" class="fontFooter10">
                      <tr >
              <td cellpadding="2" style="display:'.$GLOBALS['numproveedorv'].'"><b>Numero de Proveedor:</b> '.$GLOBALS['numproveedor'].'</td>
              <td cellpadding="2"style="display:'.$GLOBALS['numordencomprav'].'"><b>Numero de orden de compra:</b> '.$GLOBALS['numordencompra'].'</td>
            </tr>';
            if ($GLOBALS['observaciones']!='') {
              $html2 .= '<tr>
                          <td cellpadding="4"><b>Observaciones:</b> '.$GLOBALS['observaciones'].'</td>
                        </tr>';
            }
            $descuentog=0;
            foreach ($GLOBALS["facturadetalles"] as $item) {
              $descuentog=$descuentog+$item->descuento;
            }
            $rowspan=6;
            if ($GLOBALS["isr"]>0) {
              $rowspan=$rowspan+1;
            }
            if ($GLOBALS["ivaretenido"]>0) {
              $rowspan=$rowspan+1;
            }
            if ($GLOBALS["cincoalmillarval"]>0) {
              $rowspan=$rowspan+1;
            }
            if ($GLOBALS["outsourcing"]>0) {
              $rowspan=$rowspan+1;
            }
            $html2 .= '<tr>
              <td  width="48%" rowspan="'.$rowspan.'" valign="top"><b>CANTIDAD EN LETRA::</b> '.($this->ValorEnLetras($GLOBALS["total"],$GLOBALS["moneda"])).'';
              if($GLOBALS['f_relacion']==1){
            $html2.=' <br><b>Motivo Cancelacion:</b> 01 Comprobante emitido con errores con relación<br>
                      <b>Tipo Relación:</b>04 Sustitución de los CFDI previos<br>
                      <b>Folio relacionado:</b> '.$GLOBALS['f_r_uuid'];
              }
               $html2 .= '</td>
              <td width="20%" align="left"></td>
              <td width="17%" align="right" class="httablelinea"><b>SUBTOTAL</b></td>
              <td width="15%" align="center" class="httablelinea">$ '.number_format(($GLOBALS["subtotal"]),2,'.',',').'</td>
            </tr>
            <tr>
              <td >&nbsp;</td>
              <td align="right" class="httablelinea"><b>DESCUENTO</b></td>
              <td align="center" class="httablelinea">$ '.number_format($descuentog,2,'.',',').'</td>
            </tr>
        <tr>
          <td ></td>
          <td align="right" class="httablelinea"><b>IVA</b></td>
          <td align="center" class="httablelinea">$ '.number_format($GLOBALS["iva"],2,'.',',').'</td>
        </tr>';

        if ($GLOBALS["isr"]>0) {
            $html2 .= '<tr>
                        <td></td>
                        <td align="right" class="httablelinea"><b>10% Retención ISR</b></td>
                        <td align="center"class="httablelinea">$ '.number_format($GLOBALS["isr"],2,'.',',').'</td>
                      </tr>';
        }
        if ($GLOBALS["ivaretenido"]>0) {
            $html2.='<tr>
                      <td >&nbsp;</td>
                      <td align="right" class="httablelinea"><b>10.67% Retención IVA</b></td>
                      <td align="center"class="httablelinea">$ '.number_format($GLOBALS["ivaretenido"],2,'.',',').'</td>
                    </tr>';
        }
        if ($GLOBALS["cincoalmillarval"]>0) {
            $html2.='<tr >
                      <td >&nbsp;</td>
                      <td align="right" class="httablelinea"><b>5 al millar</b></td>
                      <td align="center"class="httablelinea">$ '.number_format($GLOBALS["cincoalmillarval"],2,'.',',').'</td>
                    </tr>';
        }
        if ($GLOBALS["outsourcing"]>0) {
            $html2.='<tr >
                      <td >&nbsp;</td>
                      <td align="right" class="httablelinea"><b>6% Retención servicios de personal</b></td>
                      <td align="center"class="httablelinea">$ '.number_format($GLOBALS["outsourcing"],2,'.',',').'</td>
                    </tr>';
        }
        $html2 .= '<tr>
                      <td >&nbsp;</td>
                      <td align="right" class="httablelinea"><b>TOTAL</b></td>
                      <td align="center"class="httablelinea">$ '.number_format($GLOBALS["total"],2,'.',',').'</td>
                    </tr>';
      $html2 .= '</table>';
    $html2 .= '
      <table><tr><td></td></tr></table>
      <table width="100%" border="0" class="fontFooter">
        <tr>
          <td width="50%"><b>MÉTODO DE PAGO: </b>'.$GLOBALS["MetodoPagol"].'</td>
          <td width="50%"><b>ÚLTIMOS 4 DIGITOS DE LA CUENTA O TARJETA: </b>'.$GLOBALS["tarjeta"].'</td>
        </tr>
        <tr>
          <td width="50%"><b>FORMA DE PAGO: </b>'.$GLOBALS["FormaPagol"].'</td>
          <td width="50%"></td>
        </tr>
      </table>';
    $html2 .= '
      <table><tr><td></td></tr></table>
      <table width="100%" border="0" class="fontFooterpt" cellpadding="3">
        <tr>
          <td rowspan="6" width="20%" align="center" style="text-align: center;" ><tcpdf method="write2DBarcode" params="' . $params . '" /></td>
          <td width="80%" class="httablelinea">SELLO DIGITAL DEL EMISOR</td>
        </tr>
        <tr>
          <td height="35px" class="httablelinea fontFooterpt">'.$GLOBALS["selloemisor"].'</td>
        </tr>
        <tr>
          <td class="httablelinea">SELLO DIGITAL SAT</td>
        </tr>
        <tr>
          <td height="35px" valign="top" class="httablelinea fontFooterpt">'.$GLOBALS["sellosat"].'
          </td>
        </tr>
        <tr>
          <td class="httablelinea">CADENA ORIGINAL DEL COMPLEMENTO DE CERTIFICACIÓN DIGITAL DEL SAT</td>
        </tr>
        <tr>
          <td height="35px" class="httablelinea fontFooterpt6" colspan="1">'.substr(utf8_encode($GLOBALS["cadenaoriginal"]), 0, 635).'</td>
        </tr>
      </table>';
    $html2 .= '
      <table width="100%" border="0" cellpadding="2" class="fontFooterp">
        <tr>
          <td align="center">La reproducción apócrifa de este comprobante constituye un delito en los términos de las disposiciones fiscales.</td>
        </tr>
        <tr>
          <td  valign="middle" align="center">Este documento es una representación impresa de un CFDI v.4.0</td>
        </tr>
        <tr>
          <td  valign="middle" class="valign" align="center">Ofrecemos la mejor solución en Facturación Electrónica: www.adminfactura.com.mx</td>
        </tr>
        <tr>
          <td align="center"></td>
        </tr>
        <tr>
          <td align="right" class="footerpage">
          Pagina '.$this->getAliasNumPage().' de '.$this->getAliasNbPages().'</td>
        </tr>
      </table>
    ';
    //<img src="http://facturacion33.adminfactura.com.mx/view/image/viamex_logob100.png" width="100px">
      $this->writeHTML($html2, true, false, true, false, '');
      
  }
} 
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Kyocera');
$pdf->SetTitle('Folio_'.$GLOBALS["Folio"]);
$pdf->SetSubject('factura');
$pdf->SetKeywords('factura');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins('7', '100', '7');
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin('102');

// set auto page breaks
$pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('dejavusans', '', 7);
// add a page
$pdf->AddPage('P', 'A4');
$pdf->setPrintHeader(true);
$pdf->setPrintFooter(true);
$htmlp='
      <style type="text/css">
        .httable{
          vertical-align: center;
          border-bottom: 2px solid #9e9e9e;
          border-top: 1px solid #9e9e9e;

        }
        .httablepro{
            vertical-align: center;
            border-bottom: 1px solid #9e9e9e;
          }
        .magintablepro{
            margin-top:0px;
            margin-bottom:0px;
            margin: 0px;
        }
      </style>
  <table border="0" align="center" cellpadding="2">

    <tr valign="middle">
      <th width="10%" class="httable"><b>CANTIDAD</b></th>
      <th width="10%" class="httable"><b>CLAVE DE UNIDAD</b></th>
      <th width="24%" class="httable"><b>CLAVE DEL PRODUCTO Y/O SERVICIO</b></th>
      <th width="26%" class="httable"><b>DESCRIPCION</b></th>
      <th width="10%" class="httable"><b>VALOR UNITARIO</b></th>
      <th width="10%" class="httable"><b>Descuento</b></th>
      <th width="10%" class="httable"><b>SUBTOTAL</b></th>
      
    </tr>
 
';
foreach ($facturadetalles as $item) {
  $htmlp.='
      <tr class="magintablepro">
        <td class="httablepro" align="center">'.$item->Cantidad.'</td>
        <td class="httablepro" align="center">'.$item->Unidad.' / '.$item->nombre.'</td>
        <td class="httablepro">'.$item->servicioId.' / '.$item->Descripcion.'</td>
        <td class="httablepro">'.$item->Descripcion2.'</td>
        <td class="httablepro" align="center">$ '.$item->Cu.'</td>
        <td class="httablepro" align="center">$ '.$item->descuento.'</td>
        <td class="httablepro" align="center">$ '.number_format(($item->Cantidad*$item->Cu)-$item->descuento,2,'.',',').'</td>
      </tr>
  ';
}
$htmlp.='</table>';
$pdf->writeHTML($htmlp, true, false, true, false, '');
//====================================================
//$pdf->setPrintHeader(false);
//$pdf->setPrintFooter(false);
//$pdf->SetMargins('10', '10', '10');
//$pdf->SetFooterMargin('10');

//$pdf->setPrintHeader(false);
$pdf->SetMargins('10', '10', '10');
//$pdf->AddPage('P', 'A4');

//$pdf->setPrintFooter(false);
$html='';
foreach ($detalleperidofactura->result() as $item) {

      $periodo =$item->prefacturaId;
      $contrato=$item->contratoId;
      $idcontrato=$item->contratoId;

        $Folio=$periodo;
        $prefacturaId=$periodo;
        $detalleperido= $this->Rentas_model->detalleperido($periodo);
        $detalleconsumiblesfolios= $this->Rentas_model->detalleconsumiblesfoliosp($contrato,$periodo);
        $detallecimages= $this->Rentas_model->detalleimagesRentap($periodo);
        //===========================================
            $detalleresult=$this->ModeloCatalogos->getselectwheren('alta_rentas_prefactura',array('prefId'=>$periodo));
                $periodo='';
              foreach ($detalleresult->result() as $itemsr) {
                $periodo=$itemsr->periodo_inicial.' al '.$itemsr->periodo_final;


                $periodo_inicial_ante =date("d-m-Y",strtotime($itemsr->periodo_inicial.' -1 month'));
                //$periodo_inicial_ante =date("d-m-Y",strtotime($periodo_inicial_ante.' -1 day'));

                $periodo_final_ante =date("d-m-Y",strtotime($itemsr->periodo_final.' -1 month'));
                //$periodo_final_ante =date("d-m-Y",strtotime($periodo_final_ante.' -1 day'));


                $periodoante=$periodo_inicial_ante.' al '.$periodo_final_ante;
              }
              //$data['periodo']=$periodo;
              //$data['periodoante']=$periodoante;
        //============================================================================================
            $contrato = $this->ModeloCatalogos->getselectwheren('contrato',array('idcontrato'=>$contrato));
            $contrato=$contrato->row();

            $idRentac=$contrato->idRenta;
            $foliocontrato=$contrato->folio;

            $contrato = $this->ModeloCatalogos->getselectwheren('alta_rentas',array('idcontrato'=>$idcontrato));
            $contrato=$contrato->row();

            $tiporenta=$contrato->tipo2;

            $grentadeposito     =$contrato->monto;
            $grentacolor        =$contrato->montoc;

            $gclicks_mono       =$contrato->clicks_mono;
            $gprecio_c_e_mono   =$contrato->precio_c_e_mono;
            $gclicks_color      =$contrato->clicks_color;
            $gprecio_c_e_color  =$contrato->precio_c_e_color;
            if ($contrato->tipo1==2) {
                    $periodoante=$periodo;
            }
            $datosrentas = $this->ModeloCatalogos->getselectwheren('rentas',array('id'=>$idRentac));
            $datosrentas=$datosrentas->row();
            $datosrentas->idCliente;

            $datoscliente= $this->ModeloCatalogos->getselectwheren('clientes',array('id'=>$datosrentas->idCliente));
            $nombreclienter='';
            foreach ($datoscliente->result() as $itemc) {
              $nombreclienter=$itemc->empresa;
            }
        //===============================================================================================
            $logos = base_url().'public/img/alta.png';
      $logosmds = base_url().'public/img/MDS.png';
      
        $tamano1='30';//30
        $tamano2='40';//40
        $tamano3='30';//30
          
          $pdf->setPrintHeader(false);
        if ($tiporenta==2) {
            $pdf->AddPage('L', 'A4');
          }else{
            $pdf->AddPage('P', 'A4');
          }
$pdf->setPrintFooter(false);
      $html .= '
          <style type="text/css">
            .info_fac{
              font-size: 9px;
            }
            .info_facd{
              font-size: 8px;
            }
            .httablelinea{
              vertical-align: center;
              border-bottom: 1px solid #9e9e9e;
            }
            
            .httableleft{
              border-left: 1px solid #9e9e9e;
            }
            .httableright{
              border-right: 1px solid #9e9e9e;
            }
            .httabletop{
              border-top: 1px solid #9e9e9e;
            }
          </style>
          <table width="100%" border="0" cellpadding="4px" class="info_fac">
            <tr>
              <td rowspan="6" width="25%"><img src="'.$logos.'"></td>
              <td width="50%" valign="top"></td>
              <td rowspan="6" width="25%"><img src="'.$logosmds.'"></td>
            </tr>
            <tr>
              <td align="center" style="font-size: 12px;"><b>'.$nombreclienter.'</b></td>
            </tr>
            <tr>
              <td align="center" style="font-size: 12px;"><b>RENTA '.$periodo.'.</b></td>
            </tr>
            <tr>
              <td></td>
            </tr>
            <tr>
              <td align="center" style="font-size: 12px;"><b>DETALLE DE PRODUCCION:</b></td>
            </tr>
            <tr>
              <td align="center" style="font-size: 12px;"><b>DEL '.$periodoante.'.</b></td>
            </tr>
          </table>

          ';
      $html.='
          <table border="1">
            <tr>
              <td  width="10%"  align="center">Contrato</td>
              <td  width="10%" align="center">'.$foliocontrato.'</td>
              <td  width="60%" ></td>
              <td  width="10%" align="center"><b>FACTURA</b></td>
              <td width="10%" ></td>
            </tr>
            
          </table>';
if ($tiporenta==2) {
  $html.='<style type="text/css">
            .httable{
              font-size: 8px;
            }
          </style>
          <table border="1" cellpadding="3">
            <tr>
              <td align="right" style="background-color: #d8d4d4; color: black">RENTA MENSUAL</td>
              <td  >$ '.number_format($grentadeposito,2,'.',',').'</td>
              <td align="right" style="background-color: #d8d4d4; color: black">RENTA MENSUAL</td>
              <td  >$ '.number_format($grentacolor,2,'.',',').'</td>
            </tr>
            <tr>
              <td colspan="4" align="center" >VOLUMEN INCLUIDO</td>
            </tr>
            <tr>
              <td colspan="2" style="background-color: #d8d4d4; color: black" align="center">MONOCROMÁTICA</td>
              <td colspan="2" style="background-color: #d8d4d4; color: black" align="center">COLOR</td>
            </tr>
            <tr style="background-color: #d8d4d4; color: black">
              <td align="center">CLICKS</td>
              <td align="center">EXCEDENTES</td>
              <td align="center">CLICKS</td>
              <td align="center">EXCEDENTES</td>
            </tr>
            <tr>
              <td align="center">'.$gclicks_mono.'</td>
              <td align="center">$ '.number_format($gprecio_c_e_mono,2,'.',',').'</td>
              <td align="center">'.$gclicks_color.'</td>
              <td align="center">$ '.number_format($gprecio_c_e_color,2,'.',',').'</td>
            </tr>
        </table><table><tr><th></th></tr></table>';

          
    $contadoresmono=0;
    $contadorescolor=0;
    foreach ($detalleperido->result() as $item) {
        if($item->tipo==1){
          $contadoresmono++;
        }
    }
    foreach ($detalleperido->result() as $item) {
        if($item->tipo==2){
          $contadorescolor++;
        }
    }
    if ($contadoresmono>0) {
      $html.='<table border="1" align="center" cellpadding="2">
              <thead> 
                <tr valign="middle" style="background-color: #d8d4d4; color: black">
                  <th class="httable" colspan="2"><b>EQUIPO</b></th>
                  <th class="httable" colspan="3" ><b>PRODUCCIÓN COPIA/IMPRESIÓN MONO</b></th>
                  <th class="httable" colspan="3" ><b>PRODUCCIÓN ESCANEO</b></th>
                  <th class="httable" rowspan="2"><b>PRODUCCIÓN EQUIPO</b></th>
                </tr>
                <tr valign="middle" style="background-color: #d8d4d4; color: black">
                  <th class="httable"><b>MODELO</b></th>
                  <th class="httable"><b>SERIE</b></th>
                  <th class="httable"><b>CONTADOR INICIAL</b></th>
                  <th class="httable"><b>CONTADOR FINAL</b></th>
                  <th class="httable"><b>PRODUCCIÓN</b></th>
                  <th class="httable"><b>CONTADOR INCIAL</b></th>
                  <th class="httable"><b>CONTADOR FINAL</b></th>
                  <th class="httable"><b>PRODUCCIÓN</b></th>
                </tr>
              </thead>
            ';
            $producciontotal=0;
            $trrowcol=$detalleperido->num_rows();
            foreach ($detalleperido->result() as $item) {
              if($item->tipo==1){
                $html.=' 
                        <tr valign="middle">
                          <td class="httable">'.$item->modelo.'</td>
                          <td class="httable">'.$item->serie.'</td>
                          <td class="httable">'.$item->c_c_i.'</td>
                          <td class="httable">'.$item->c_c_f.'</td>
                          <td class="httable">'.$tc=$item->c_c_f-$item->c_c_i.'</td>
                          <td class="httable">'.$item->e_c_i.'</td>
                          <td class="httable">'.$item->e_c_f.'</td>
                          <td class="httable">'.$tc=$item->e_c_f-$item->e_c_i.'</td>
                          <td class="httable">'.$item->produccion.'</td>  
                        </tr>
                      
                    ';
                    $producciontotal=$producciontotal+$item->produccion;
              }
            }
            $volumenexcenetente=$producciontotal-$gclicks_mono;
            if ($volumenexcenetente<0) {
              $volumenexcenetente=0;
            }
            $totalexcedente=$volumenexcenetente*$gprecio_c_e_mono;
            $subtotal=$totalexcedente+$grentadeposito;
            $iva=$subtotal*0.16;
            $total=$subtotal+$iva;
              $html.='
                      <tr valign="middle">
                          <td class="httable" colspan="8" align="right">Produccion Total</td>
                          <td class="httable">'.$producciontotal.'</td>
                          <td class="httable"></td>
                        </tr>

              </table><table><tr><th></th></tr></table>
              <table border="1" align="center" cellpadding="2">
                <tr>
                  <th>VOLUMEN EXCEDENTE</th>
                  <th>COSTO EXCEDENTE</th>
                  <th>TOTAL EXCEDENTE</th>
                  <th>RENTA FIJA</th>
                  <th>SUBTOTAL</th>
                  <th>IVA</th>
                  <th style="background-color: #d8d4d4; color: black">TOTAL</th>
                </tr>
                <tr>
                  <td>'.$volumenexcenetente.'</td>
                  <td>$ '.number_format($gprecio_c_e_mono,2,'.',',').'</td>
                  <td>$ '.number_format($totalexcedente,2,'.',',').'</td>
                  <td>$ '.number_format($grentadeposito,2,'.',',').'</td>
                  <td>$ '.number_format($subtotal,2,'.',',').'</td>
                  <td>$ '.number_format($iva,2,'.',',').'</td>
                  <td style="background-color: #d8d4d4; color: black">$ '.number_format($total,2,'.',',').'</td>
                </tr>
              </table>
              ';
    }
    if ($contadorescolor>0) {
      $html.='<table><tr><th></th></tr></table>
            <table border="1" align="center" cellpadding="2">
              <thead> 
                <tr valign="middle" style="background-color: #d8d4d4; color: black">
                  <th class="httable" colspan="2"><b>EQUIPO</b></th>
                  <th class="httable" colspan="3" ><b>PRODUCCIÓN COPIA/IMPRESIÓN COLOR</b></th>
                  <th class="httable" colspan="3" ><b>PRODUCCIÓN ESCANEO</b></th>
                  <th class="httable" rowspan="2"><b>PRODUCCIÓN EQUIPO</b></th>
                </tr>
                <tr valign="middle" style="background-color: #d8d4d4; color: black">
                  <th class="httable"><b>MODELO</b></th>
                  <th class="httable"><b>SERIE</b></th>
                  <th class="httable"><b>CONTADOR INICIAL</b></th>
                  <th class="httable"><b>CONTADOR FINAL</b></th>
                  <th class="httable"><b>PRODUCCIÓN</b></th>
                  <th class="httable"><b>CONTADOR INCIAL</b></th>
                  <th class="httable"><b>CONTADOR FINAL</b></th>
                  <th class="httable"><b>PRODUCCIÓN</b></th>
                </tr>
              </thead>
            ';
            $producciontotal=0;
            $trrowcol=$detalleperido->num_rows();
            foreach ($detalleperido->result() as $item) {
              if($item->tipo==2){
                $html.=' 
                        <tr valign="middle">
                          <td class="httable">'.$item->modelo.'</td>
                          <td class="httable">'.$item->serie.'</td>
                          <td class="httable">'.$item->c_c_i.'</td>
                          <td class="httable">'.$item->c_c_f.'</td>
                          <td class="httable">'.$tc=$item->c_c_f-$item->c_c_i.'</td>
                          <td class="httable">'.$item->e_c_i.'</td>
                          <td class="httable">'.$item->e_c_f.'</td>
                          <td class="httable">'.$tc=$item->e_c_f-$item->e_c_i.'</td>
                          <td class="httable">'.$item->produccion.'</td>  
                        </tr>
                      
                    ';
                    $producciontotal=$producciontotal+$item->produccion;
              }
            }
            $volumenexcenetente=$producciontotal-$gclicks_color;
            $totalexcedente=$volumenexcenetente*$gprecio_c_e_color;
            $subtotal=$totalexcedente+$grentacolor;
            $iva=$subtotal*0.16;
            $total=$subtotal+$iva;
              $html.='
                      <tr valign="middle">
                          <td class="httable" colspan="8" align="right">Produccion Total</td>
                          <td class="httable">'.$producciontotal.'</td>
                          <td class="httable"></td>
                        </tr>

              </table><table><tr><th></th></tr></table>
              <table border="1" align="center" cellpadding="2">
                <tr>
                  <th>VOLUMEN EXCEDENTE</th>
                  <th>COSTO EXCEDENTE</th>
                  <th>TOTAL EXCEDENTE</th>
                  <th>RENTA FIJA</th>
                  <th>SUBTOTAL</th>
                  <th>IVA</th>
                  <th style="background-color: #d8d4d4; color: black">TOTAL</th>
                </tr>
                <tr>
                  <td>'.$volumenexcenetente.'</td>
                  <td>$ '.number_format($gprecio_c_e_color,2,'.',',').'</td>
                  <td>$ '.number_format($totalexcedente,2,'.',',').'</td>
                  <td>$ '.number_format($grentacolor,2,'.',',').'</td>
                  <td>$ '.number_format($subtotal,2,'.',',').'</td>
                  <td>$ '.number_format($iva,2,'.',',').'</td>
                  <td style="background-color: #d8d4d4; color: black">$ '.number_format($total,2,'.',',').'</td>
                </tr>
              </table>
              ';
    }
}else{
        $html.='<style type="text/css">
            .httable{
              font-size: 8px;
            }
          </style>';
            foreach ($detalleperido->result() as $item) {
              if($item->tipo==2){
                $title='COLOR';
                $volumenincluido =  $item->clicks_color;
                $costorenta=round($item->precio_rc,2);
                $costoexcedente  =  round($item->precio_c_e_color,2);
              }else{
                $title='MONO';
                $volumenincluido =  $item->clicks_mono;
                $costorenta=round($item->precio_r,2);
                $costoexcedente  =  round($item->precio_c_e_mono,2);
              }
              $html.='<table border="1" align="center" cellpadding="2">
              <thead> 
                <tr valign="middle" style="background-color: #d8d4d4; color: black">
                  <th class="httable" colspan="2"><b>EQUIPO</b></th>
                  <th class="httable" colspan="3" ><b>PRODUCCIÓN COPIA/IMPRESIÓN '.$title.'</b></th>
                  <th class="httable" colspan="3" ><b>PRODUCCIÓN ESCANEO</b></th>
                  <th class="httable" rowspan="2"><b>PRODUCCIÓN EQUIPO</b></th>
                  <th class="httable" rowspan="2"><b>VOLUMEN INCLUIDO</b></th>
                </tr>
                <tr valign="middle" style="background-color: #d8d4d4; color: black">
                  <th class="httable"><b>MODELO</b></th>
                  <th class="httable"><b>SERIE</b></th>
                  <th class="httable"><b>CONTADOR INICIAL</b></th>
                  <th class="httable"><b>CONTADOR FINAL</b></th>
                  <th class="httable"><b>PRODUCCIÓN</b></th>
                  <th class="httable"><b>CONTADOR INCIAL</b></th>
                  <th class="httable"><b>CONTADOR FINAL</b></th>
                  <th class="httable"><b>PRODUCCIÓN</b></th>
                  
                </tr>
              </thead>
            ';
            $excedente=$item->produccion-$volumenincluido;
                if ($excedente<0) {
                  $excedente=0;
                }
                $subtotal=$costorenta+($excedente*$costoexcedente);
                $subtotaliva=$subtotal*0.16;
                $totalgeneral=round($subtotal+$subtotaliva,2);
                $html.=' 
                        <tr valign="middle">
                          <td class="httable">'.$item->modelo.'</td>
                          <td class="httable">'.$item->serie.'</td>
                          <td class="httable">'.$item->c_c_i.'</td>
                          <td class="httable">'.$item->c_c_f.'</td>
                          <td class="httable">'.$tc=$item->c_c_f-$item->c_c_i.'</td>
                          <td class="httable">'.$item->e_c_i.'</td>
                          <td class="httable">'.$item->e_c_f.'</td>
                          <td class="httable">'.$tc=$item->e_c_f-$item->e_c_i.'</td>
                          <td class="httable">'.$item->produccion.'</td>  
                          <td class="httable">'.$volumenincluido.'</td>  
                          
                        </tr>
                      
                    <tr>
                          <td colspan="10"> </td>
                        </tr>
                        <tr valign="middle">
                          <td colspan="2">VOLUMEN EXCEDENTE</td> 
                          <td colspan="2">COSTO EXCEDENTE</td>  
                          <td colspan="2">RENTA FIJA</td> 
                          <td colspan="2">SUBTOTAL</td>
                          <td>IVA</td>  
                          <td>TOTAL</td>  
                        </tr>
                        <tr valign="middle">
                          <td colspan="2">'.$excedente.'</td> 
                          <td colspan="2">$'.$costoexcedente.'</td>  
                          <td colspan="2">$'.$costorenta.'</td> 
                          <td colspan="2">$'.$subtotal.'</td>
                          <td>$'.$subtotaliva.'</td>  
                          <td>$'.$totalgeneral.'</td>   
                        </tr>

              </table><table><tr><th></th></tr></table>';
              
            }
}
  

$pdf->writeHTML($html, true, false, true, false, '');
  //=======================================================================
$pdf->AddPage('P', 'A4');
  $html='<br><br><br><br>
          <table border="1" align="center" cellpadding="2" style="background-color: #d8d4d4; color: black">
            <tr valign="middle">
              <th class="httable" style="font-size:12px"><b>TONER</b></th>
            </tr>
          </table>';
  $html.='<table border="1" align="center" cellpadding="2">
          <thead> 
            <tr valign="middle" style="background-color: #d88888; color: black">
              <th class="httable" ><b>TONER</b></th>
              <th class="httable" ><b>FOLIO</b></th>
              <th class="httable"  ><b>ESTATUS</b></th>
              
            </tr>
          </thead>
        ';
foreach ($detalleconsumiblesfolios->result() as $item) {
  if ($item->status==0) {
    $status='';
  }elseif ($item->status==1) {
    $status='stock Cliente';
  }else{
    $status='Retorno';
  }
    $html.=' 
            <tr valign="middle">
              <td class="httable">'.$item->modelo.'</td>
              <td class="httable">'.$item->foliotext.'</td>
              <td class="httable">'.$status.'</td>
            </tr>
          
        ';  
}
 $html.='</table>';
$pdf->writeHTML($html, true, false, true, false, '');
  foreach ($detallecimages->result() as $item) {
    $pdf->AddPage('P', 'A4');
    if($item->tipoimg==0){
      $file = fopen(base_url()."uploads/rentas_hojasestado/".$item->nombre, "r");
      $line = fgets($file);
      $html= $line;
      fclose($file);
    }else{
      $html=base_url()."uploads/rentas_hojasestado/".$item->nombre;
    }
    $pdf->Image($html, 'C', 0, 250, 297, '', '', '', true, 200, 'C', false, false, 0, false, false, true);
    //else  
    //log_message('error', base64_decode(base_url()."uploads/rentas_hojaestado/".$line));
    //log_message('error', '<th class="httable"><img src="'.base64_decode(base_url()."uploads/rentas_hojasestado/".$line).'"></th>');
  }//for
}
  










$pdf->Output('Folios_.pdf', 'I');
//$pdf->Output('../../facturas/'.$GLOBALS["Folio"].'.pdf', 'F');
//$pdf->Output('/facturas/'.$GLOBALS["Folio"].'.pdf', 'F');
?>