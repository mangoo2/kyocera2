<style type="text/css">
  .chosen-container{margin-left: 0px;}
  .cserienew .cserienewi{height: 2rem !important;margin: 0px !important;}
  .cserienew td{padding: 5px !important;}
  table td{
    font-size: 12px !important;
  }
  #modal_series_e{
    width: 70%;
  }
</style>
<!-- START CONTENT -->
    <section id="content">
      <!--start container-->
        <div class="container">
          <div class="section">
          <div id="table-datatables">
            <br>
                <h4 class="header">Listado de Compras</h4>
                <div class="row">
                    <div class="col s5">
                      <a class="btn-bmz waves-effect waves-light gradient-45deg-green-teal gradient-shadow" href="<?php echo base_url(); ?>index.php/Compras/add">Nuevo</a>
                    </div>
                    <div class="col s7" style="text-align: end;">
                      <a class="btn-bmz waves-effect waves-light gradient-45deg-green-teal gradient-shadow" href="<?php echo base_url(); ?>index.php/Compras/Solicitudes">Solicitudes</a>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col s2">
                    <input type="date" id="fecha" class="form-control-bmz" onchange="datos_compras()">
                  </div>
                  <div class="col s2">
                    <select class="form-control-bmz browser-default" id="tipoc" onchange="datos_compras()">
                      <option value="0">Todas</option>
                      <option value="1">Completadas</option>
                      <option value="2">En proceso</option>
                    </select>
                  </div>
                </div>
                <div class="row">

                  <div class="col s12">
                    <table id="tabla_compras" class="responsive-table tabla_compras" cellspacing="0">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Folio</th>
                          <th>Personal</th>
                          <th></th>
                          <th></th>
                          <th>Fecha</th>
                          <th>File</th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
          </div>  
        </div>
     </div>
    <div class="modal fade modal-lg" id="modal_series_e" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="width: 70%">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <div class="row">
              <div class="col s10">
                <h5 class="modal-title" id="exampleModalLabel">Ingresar series</h5>
              </div>
              <div class="col s2">
                <a onclick="agregaritems()" class="btn-bmz green gradient-shadow">
                                <i class="fas fa-plus"></i> Agregar</a>
              </div>
            </div>
            
          </div>
          <div class="modal-body">
            <div class="row">   
                <input type="hidden" id="id_compra_c">
                <div class="col s12 text_label" style="padding: 0px;">
                </div>
              </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="modal-action modal-close waves-effect waves-red gray btn-flat">Cancelar</button>
            <button type="button" class="btn blue aceptar_series" onclick="aceptar_series()">Aceptar</button>
          </div>
        </div>
      </div>
    </div>
    <div class="modal fade" id="modal_series_e_file" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Cargar series</h5>
          </div>
          <div class="modal-body">
            <div class="row">   
                <input type="hidden" id="id_compra_c_f">
                <div class="col s12">
                  <input type="file" name="inputFile" id="inputFile" class="form-control dropify" value="">
                </div>
              </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="modal-action modal-close waves-effect waves-red gray btn-flat">cerrar</button>
          </div>
        </div>
      </div>
    </div>
    <div class="modal fade" id="modal_series_editar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Editar serie</h5>
          </div>
          <div class="modal-body">
            <div class="row">   
                <input type="hidden" id="id_compra_edit">
                <input type="hidden" id="id_row_edit">
                <input type="hidden" id="id_tipo_edit">
                <div class="col s12">
                  <input type="text" name="editserie" id="editserie" class="form-control" value="">
                </div>
              </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="modal-action modal-close waves-effect waves-red gray btn-flat">cerrar</button>
            <button type="button" class="btn blue" onclick="aceptar_editar_series()">Aceptar</button>
          </div>
        </div>
      </div>
    </div>
    <div class="modal fade" id="modal_series_c" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Consulta compra</h5>
          </div>
          <div class="modal-body">
            <div class="row class_consultacompra">   
                
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="modal-action modal-close waves-effect waves-red gray btn-flat">Cancelar</button>
          </div>
        </div>
      </div>
    </div>
    <div class="classbodegas" style="display: none;">
      <select id="idbodeganone" name="idbodega" class="browser-default chosen">
        <?php foreach ($bodegas as $itemb) { 
              if($itemb->bodegaId==8){
                $selected='selected';
              }else{
                $selected='';
              }
            echo '<option value="'.$itemb->bodegaId.'" '.$selected.'>'.$itemb->bodega.'</option>';
           } ?>
      </select>
    </div>
    </section>       