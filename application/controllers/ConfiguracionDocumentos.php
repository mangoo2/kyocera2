<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class ConfiguracionDocumentos extends CI_Controller {
    public function __construct()     {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ConfiguracionDocumentos_Model','model');
        $this->load->model('ModeloCatalogos');
        $this->submenu=25;
        if($this->session->userdata('logeado')==true){
            $this->perfilid =$this->session->userdata('perfilid');
            $this->idpersonal = $this->session->userdata('idpersonal');
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,$this->submenu);// 22 es el id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
        }else{
            redirect('login');
        }
    }

    // Listado de Equipos
    function index(){
        $data['MenusubId']=$this->submenu;
            // Obtiene los menús disponibles del sistema dependiendo el perfil, ayudado de la libreria "MENUS"
        $tipov=1;
        if(isset($_GET['tipov'])){
            if($tipov>0){
                $tipov=$_GET['tipov'];
            }
        }
        $data['tipov']=$tipov;
        $data['configuracionCotizacion'] = $this->model->getConfiguracion($tipov);
        $this->load->view('header');
        $this->load->view('main',$data);
        $this->load->view('configuraciones/form_cotizacion_documento');
        $this->load->view('configuraciones/form_cotizacion_documento_js');
        $this->load->view('footer');
    }

    public function actualizaDatosConfiguracionCotizacion() {
        $data = $this->input->post();
        $json_decode = json_decode($data['data']);
        $tipov=$json_decode->tipov;
        $datosActualizados = array(
            'textoHeader' => $json_decode->textoHeader,
            'textoHeader2'=> $json_decode->textoHeader2,
            'leyendaPrecios'=> $json_decode->leyendaPrecios,
            'tituloObservaciones'=> $json_decode->tituloObservaciones,
            'contenidoObservaciones' => $json_decode->contenidoObservaciones,
            'titulo1Footer'=> $json_decode->titulo1Footer,
            'contenido1Footer'=> $json_decode->contenido1Footer,
            'titulo2Footer'=> $json_decode->titulo2Footer,
            'contenido2Footer'=> $json_decode->contenido2Footer,
            'firma' => $json_decode->firma,
            'color' => $json_decode->color
        );
        $actualizado = $this->model->actualizaDatosConfiguracionCotizacion($datosActualizados, $tipov);
        
        echo $actualizado;
        //$this->Modelobitacoras->Insert(array('contenido'=>'Se realizo una actializacion de configuracion de documentos','nombretabla'=>'configuracionCotizaciones','idtable'=>0,'tipo'=>'Update','personalId'=>$this->idpersonal));
    }

    function cargaArchivos()    {
        $upload_folder ='uploads/configuraciones';
        
        if(empty($_FILES))        {
            echo "ninguna imagen se escogió";
        }else{

        }

        var_dump($_FILES); die();
        
        $nombre_archivo = $_FILES['foto']['name'];
        $tipo_archivo = $_FILES['foto']['type'];
        $tamano_archivo = $_FILES['foto']['size'];
        $tmp_archivo = $_FILES['foto']['tmp_name'];
        //$archivador = $upload_folder . '/' . $nombre_archivo;
        $fecha=date('ymd-His');
        $newfile='r_-'.$fecha.'-'.$nombre_archivo;        
        $archivador = $upload_folder . '/'.$newfile;
        if (!move_uploaded_file($tmp_archivo, $archivador)) {
            $return = Array('ok' => FALSE, 'msg' => 'Ocurrió un error al subir el archivo. No pudo guardarse.', 'status' => 'error');
        }else{
            $arrayfoto = array('foto' => $newfile, );
            $this->model->update_foto($arrayfoto,$consumibles);
            $return = Array('ok'=>TRUE);
        }
        echo json_encode($return);
    }
    function cargaArchivos1()    {
        $configuracion = $_POST['configuracion'];
        $upload_folder ='app-assets/images/';
        
        if(empty($_FILES))        {
            echo "ninguna imagen se escogió";
        }else{

        }

        
        $nombre_archivo = $_FILES['foto']['name'];
        $tipo_archivo = $_FILES['foto']['type'];
        $tamano_archivo = $_FILES['foto']['size'];
        $tmp_archivo = $_FILES['foto']['tmp_name'];
        //$archivador = $upload_folder . '/' . $nombre_archivo;
        $fecha=date('ymd-His');
        $newfile='r_-'.$fecha.'-'.$nombre_archivo;        
        $archivador = $upload_folder . '/'.$newfile;
        if (!move_uploaded_file($tmp_archivo, $archivador)) {
            $return = Array('ok' => FALSE, 'msg' => 'Ocurrió un error al subir el archivo. No pudo guardarse.', 'status' => 'error');
        }else{
            $arrayfoto = array('foto' => $newfile, );
            //$this->model->update_foto($arrayfoto,$consumibles);
            $this->ModeloCatalogos->updateCatalogo('configuracioncotizaciones',array('logo1'=>$newfile),array('id'=>$configuracion));
            $return = Array('ok'=>TRUE);
        }
        echo json_encode($return);
    }
    function cargaArchivos2()    {
        $configuracion = $_POST['configuracion'];
        $upload_folder ='app-assets/images/';
        
        if(empty($_FILES))        {
            echo "ninguna imagen se escogió";
        }else{

        }

        
        $nombre_archivo = $_FILES['foto']['name'];
        $tipo_archivo = $_FILES['foto']['type'];
        $tamano_archivo = $_FILES['foto']['size'];
        $tmp_archivo = $_FILES['foto']['tmp_name'];
        //$archivador = $upload_folder . '/' . $nombre_archivo;
        $fecha=date('ymd-His').'dos';
        $newfile='r_-'.$fecha.'-'.$nombre_archivo;        
        $archivador = $upload_folder . '/'.$newfile;
        if (!move_uploaded_file($tmp_archivo, $archivador)) {
            $return = Array('ok' => FALSE, 'msg' => 'Ocurrió un error al subir el archivo. No pudo guardarse.', 'status' => 'error');
        }else{
            $arrayfoto = array('foto' => $newfile, );
            //$this->model->update_foto($arrayfoto,$consumibles);
            $this->ModeloCatalogos->updateCatalogo('configuracioncotizaciones',array('logo2'=>$newfile),array('id'=>$configuracion));
            $return = Array('ok'=>TRUE);
        }
        echo json_encode($return);
    }

}