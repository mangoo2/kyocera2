<style type="text/css">
  hr{
    border-width: 2px;
  }
</style>
<!-- START CONTENT -->
<section id="content" width="100%">
  <input type="hidden" id="equipo" value="<?php echo $id;?>">
  <input type="hidden" id="idserie" value="<?php echo $idserie ?>" name="">
  <!--start container-->
  <div class="container" width="100%">
    <div class="section">
      <div class="row">
        <p class="caption col s6">Contadores</p>
      </div>
       
      <div class="row">        
          <div class="col s12 m6">
            <a class="btn waves-effect waves-light amber darken-4" onclick="normal()"><i class="material-icons">camera_front</i></a>
            <a class="btn waves-effect waves-light amber darken-4" onclick="frontal()"><i class="material-icons">camera_rear</i></a>
            <a class="btn waves-effect waves-light amber darken-4" onclick="campurar()"><i class="material-icons">camera_enhance</i></a>
            <div class="input-field col s3">
              <select class="chosen-select" id="tipo">
                <option value="1">Hoja de estado</option>
                <option value="2">Refacción</option>
              </select>
              <label>Tipo</label>
            </div>
            <div class="css_guardar" style="display: none;">
              <a class="btn darken-4" style="background-color: blue" onclick="guardar()">Guardar</a>
            </div>
          </div> 
      </div>
      <div class="row">
    
      </div>
      <div class="row">
        <div class="col s12">
          
          
        </div>
      </div>
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header"><i class="material-icons">videocam</i>Camara</div>
          <div class="collapsible-body" style="padding: 0">
            <video playsinline autoplay style="width: 100%"></video>
          </div>
        </li>
        <li>
          <div class="collapsible-header"><i class="material-icons">center_focus_strong</i>Captura</div>
          <div class="collapsible-body" style="padding: 0">
            <canvas></canvas>
            <textarea id="comentario" placeholder="Comentarios"></textarea>
          </div>
        </li>
        
      </ul>
    </div>
  </section>