<style type="text/css" class="print_css">
  .datosClienteCotizacion{
    background: <?php echo $configuracionCotizacion->color ?> !important;
    -webkit-print-color-adjust:exact;
  }
  html{
        -webkit-print-color-adjust: exact;
  }
  .tablaBordesPrincipal p{
    margin: 0px
  }
  .badgeaction{
    border-radius: 13px !important;
    min-width: 24px !important;
    cursor: pointer;
  }
  @media print {
    html{
        -webkit-print-color-adjust: exact;
    }
    .btn_imp{
      display: none;
    }
    td,th{
      font-size: 12px;
      padding: 0px;
    }
    .contenido4 h1{
      font-size: 10px;
    }
    .img_equipos{
      width: 90px !important;
    }
    .badgeaction,.button_add_cotizacion{
      display: none;
    }
  }
</style>
<?php 
  $rowitems=0;
?>
<input type="hidden" id="estatuscot" value="<?php echo $datosCotizacion[0]->estatus;?>">
<!-- START CONTENT -->
<section id="content" width="100%">
  <!--start container-->
  <div class="container" width="100%">
    <div class="section" style="margin-top: -53px;">
     <br>
      <div class="row">
        <input id="base_url" type="hidden" name="base_url" value="<?php echo base_url(); ?>">
        <input id="idCotizacion" type="hidden" name="idCotizacion" value="<?php echo $datosCotizacion[0]->id; ?>">
        
        <!-- FORMULARIO  card-panel-->
        <div class="col s12">
          <div class="">

            <!-- ENCABEZADO PRINCIPAL PDF -->
            <div class="row"> 
              <div class="col s3">
                <img src="<?php echo base_url(); ?>app-assets/images/<?php echo $configuracionCotizacion->logo1; ?>">
              </div>
              <div class="col s6">
                <p class="headerPDF"><?php echo $configuracionCotizacion->textoHeader; ?></p>
              </div> 
              <div class="col s3">
                <p align="center"><img class="" src="<?php echo base_url(); ?>app-assets/images/1024_kyocera_logo_mds.png" alt="materialize logo"  style="max-width:200px; width: 100%;"></p>
              </div>
            </div>

            <!-- APARTADO DE INFORMACIÓN DE RECEPTOR DE COTIZACIÓN-->
            
            
            <div class="row">
              <table class="centered datosClienteCotizacion tablaBordesHeader" style="width:100%">
                  <thead>
                    
                  </thead>
                  <tbody class="tablaBordes">
                    <tr >
                      <td class="tablaBordesHeader" >
                        Cotización Especial Para: <br>
                        <?php echo $datosCliente->empresa ?>
                      </td>
                      <td class="tablaBordesHeader">
                        Atención para: <br>
                        <?php echo $datosCotizacion[0]->atencion ?>
                      </td>
                      <td class="tablaBordesHeader">
                        Correo: <?php echo $datosCotizacion[0]->correo ?> <br>
                        Teléfono: <?php echo $datosCotizacion[0]->telefono ?>
                      </td>
                      <td class="tablaBordesHeader">
                        Cotización No. <?php echo $datosCotizacion[0]->id?> 
                      </td>
                    </tr>
                  </tbody>
                </table>
            </div>
            <div class="row button_add_cotizacion">
              <div class="col s3">
                <!--<a class="btn-bmz btn cyan waves-effect waves-light" type="button" href="<?php echo base_url(); ?>Cotizaciones/altaCotizacion/<?php echo $button_add_cotizacion;?>" >Agregar nuevo</a>-->
              </div>
            </div>
            <div align="center"><div class="" id="textoHeader2" contenteditable="true"><?php echo $configuracionCotizacion->textoHeader2; ?></div></div>
            <!------------ TABLA DE INFORMACIÓN DE EQUIPOS ------------>
            <div class="row">
                <table class="centered bordered letraTablaImpresionEquipos tablaBordesPrincipal" id="tablaHorizontal" style="width:100%">
                  <thead>
                    <tr class="tablaBordesPrincipal">
                        <th class="tablaBordesPrincipal">Cantidad</th>
                        <th class="tablaBordesPrincipal">Modelo</th>
                        
                        <th class="tablaBordesPrincipal" width="82px"><?php if($tipo==1){echo 'Precio';}elseif ($tipo==2){echo 'Renta Mensual';} ?></th>
                        <?php if ($datosCotizacion[0]->propuesta==0) { ?>
                          <!--<th class="tablaBordesPrincipal" width="82px">SubTotal</th>-->
                        <?php } ?>
                        <?php if ($datosCotizacion[0]->propuesta==1) { ?>
                          <!--<th class="tablaBordesPrincipal" width="82px">SubTotal</th>-->
                          <!--<th class="tablaBordesPrincipal" width="82px">Iva</th>-->
                          <th class="tablaBordesPrincipal" width="82px">Total</th>
                        <?php } ?>
                    </tr>
                  </thead>
                  <tbody >
                   <?php 
                          $total=0;
                          foreach ($detallesCotizacion->result() as $item) {
                            $total=$total+($item->costo*$item->cantidad);     
                    ?>
                     <tr class="tablaBordesPrincipal" >
                        <td class="tablaBordesPrincipal" onclick="editarp(<?php echo $item->id_accesoriod;?>,<?php echo $item->cantidad ?>)"><?php echo $item->cantidad;?></td>
                        <td class="tablaBordesPrincipal"><?php echo $item->nombre;?></td>
                        
                        
                        <td class="tablaBordesPrincipal contardato" onclick="editar(<?php echo $item->id_accesoriod;?>,<?php echo $item->costo;?>)">$ <?php echo number_format($item->costo,2,'.',',');?></td>
                        <?php if ($datosCotizacion[0]->propuesta==0) { ?>
                          <!--<td class="tablaBordesPrincipal">$ <?php echo number_format($item->precio*$item->cantidad,2,'.',',');?></td>-->
                        <?php } ?>
                        <?php if ($datosCotizacion[0]->propuesta==1) { ?>
                          <!--<td class="tablaBordesPrincipal">$ <?php echo number_format(($item->precio*$item->cantidad)*0.16,2,'.',',');?></td>-->
                          <td class="tablaBordesPrincipal">$ <?php echo number_format(($item->precio*$item->cantidad),2,'.',',');?></td>
                        <?php } ?>
                    </tr>
                   



                    <?php } ?> 
                    
                    
                  </tbody>
                </table>
                <?php if ($datosCotizacion[0]->propuesta==0) { ?>
                <table>
                  <tr class="tablaBordesPrincipal">
                      <td class="tablaBordesPrincipal" width="90%" align="right" style="text-align: right;"><b>SubTotal:</b></td>
                      <td class="tablaBordesPrincipal" width="10%"><b>$ <?php echo number_format($total,2,'.',',');?></b></td> 
                  </tr>
                  <tr class="tablaBordesPrincipal">
                      <td class="tablaBordesPrincipal" align="right" style="text-align: right;"><b>I.V.A:</b></td>
                      <td class="tablaBordesPrincipal"><b>$ <?php echo number_format($total*0.16,2,'.',',');?></b></td> 
                  </tr>
                  <tr class="tablaBordesPrincipal">
                      <td class="tablaBordesPrincipal" width="90%" align="right" style="text-align: right;"><b>TOTAL:</b></td>
                      <td class="tablaBordesPrincipal" width="10%"><b>$ <?php echo number_format($total+($total*0.16),2,'.',',');?></b></td> 
                  </tr>
                </table>
                <?php  } ?>
                
                
            </div>
           


            <div align="center"><?php echo $configuracionCotizacion->leyendaPrecios; ?></div>
            
            <!-- Observaciones -->
            <div class="row" style="margin-bottom: -12px">
              <table class="centered letraTablaFooterEquipos" style="width:100%">
                  <thead>
                      <tr>
                        <th class="tablaBordesPrincipal letraTablaFooterEquiposHeader">
                          <div id="contenido1" contenteditable="true" class="info1" oninput="saveinfo_o(1)" ><?php
                            if($datosCotizacion[0]->info1!=null){
                              echo $datosCotizacion[0]->info1;
                            }else{
                              echo $configuracionCotizacion->tituloObservaciones;
                            }
                            ?></div>
                        </th>
                        <th class="tablaBordesPrincipal letraTablaFooterEquiposHeader"><?php echo $configuracionCotizacion->titulo1Footer; ?></th>
                      </tr>
                  </thead>
                  <tbody >
                    <tr>
                        <td class="tablaBordesPrincipal contenido1" width="50%">
                          <div id="contenido2" contenteditable="true" class="info2" oninput="saveinfo_o(2)"><?php 
                            if($datosCotizacion[0]->info2!=null){
                              echo $datosCotizacion[0]->info2;
                            }else{
                              echo $configuracionCotizacion->contenidoObservaciones;
                            }
                           ?></div>
                        </td>
                        <td class="tablaBordesPrincipal contenido2" width="50%" ><?php echo $configuracionCotizacion->contenido1Footer; ?></td>
                    </tr>
                  </tbody>
                </table>
            </div>
            <br>

            
            <div class="row" style="margin-bottom: -12px">
              <table class="centered letraTablaFooterEquipos" style="width:100%">
                  <thead>
                      <tr>
                        <th class="tablaBordesPrincipal">
                          <div id="contenido3" contenteditable="true" class="info3" oninput="saveinfo_o(3)"><?php 
                            if($datosCotizacion[0]->info3!=null){
                              echo $datosCotizacion[0]->info3;
                            }else{
                              echo $configuracionCotizacion->titulo2Footer;    
                            }
                           ?></div>
                        </th>
                        <th class="tablaBordesPrincipal"><?php echo $fechareg; ?></th>
                      </tr>
                  </thead>
                  <tbody >
                    <tr>
                        <td class="tablaBordesPrincipal " width="50%" >
                          
                          <div id="contenido4" contenteditable="true" class="info4" onchange="saveinfo_o(4)" ><?php 
                            if($datosCotizacion[0]->info4!=null){
                              echo $datosCotizacion[0]->info4;
                            }else{
                              echo $configuracionCotizacion->contenido2Footer;  
                            }
                           ?></div>  
                        </td>
                        <td class="tablaBordesPrincipal contenido4" width="50%">
                          <?php echo $configuracionCotizacion->firma;?><b><?php echo$nombreUsuario ?></b><br><?php echo $emailUsuario ?>
                          </td>
                    </tr>
                  </tbody>
                </table>
            </div>

           

<!--columna 1---------------------------------------------------------------->

              <form class="form" id="" method="post">
                <?php
                if(isset($cliente))
                {
                ?>
                <input id="idCliente" name="idCliente" type="hidden" value="<?php echo $idCliente; ?>">
                <?php
                }
                ?>
                <div class="row">
                  <div class="input-field col s12">
                    <button class="btn-bmz btn cyan waves-effect waves-light right btn_imp" type="button" onclick="btn_guardar()">Imprimir
                    <i class="material-icons right">send</i>
                    </button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<div id="modaleditor" class="modal">
  <div class="modal-content ">
    <h4>Editar Precio</h4>
    <div class="border_datos" id="fiscales_dato1">
      <div class="col s24 m24 l12">
        <div class="row ">
            <div class="row">
              <input id="idEquipon" type="hidden" value="">
              <div class="input-field col s12">
                <input id="password" type="password" class="validate" autocomplete="new-password" required>
                <label for="password">Password</label>
              </div>
            </div>
            <div class="row">
              <div class="input-field col s12">
                <input id="newprecio" type="number" class="validate" placeholder="Precio" required>
                <label for="newprecio">Nuevo Precio</label>
              </div>
            </div>
                                        

        </div>                          
      </div>
    </div>                    
  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
    <a href="#!" class="modal-action modal-close waves-effect waves-green green btn-flat editarcosto">Aceptar</a>
  </div>
</div>

<script type="text/javascript">
  function saveinfo_o(tipo){
    saveinfo(<?php echo $idCotizacion;?>,tipo);
  }
  function editarprecioconsumible(idrow,precio){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Editar',
        content: 'Editar costo toner<br>'+
                 '<input type="number" placeholder="monto" id="costo_toner" class="name form-control-bmz" value="'+precio+'" required/>',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var costo_toner_new=$('#costo_toner').val();
                
                     $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Cotizaciones/editarcostotoner",
                        data: {
                            id:idrow,
                            costo_toner:costo_toner_new
                        },
                        success: function (response){
                                location.reload();
                                
                        },
                        error: function(response){
                            $.alert({
                                    boxWidth: '30%',
                                    useBootstrap: false,
                                    title: 'Error!',
                                    content: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema'});   
                             
                        }
                    });
                    
                
                
            },
            cancelar: function () 
            {
                
            }
        }
    });
  }
</script>