<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicons-->
    <link rel="icon" href="<?php echo base_url().'app-assets/images/favicon/favicon_kyocera.png' ?>" sizes="32x32">
    <link rel="apple-touch-icon" href="<?php echo base_url().'app-assets/images/favicon/favicon_kyocera.png' ?>">
    <title>Agregar Respuesta</title>
    <link href="<?php echo base_url(); ?>public/plugins/note/bootstrap.min.css" rel="stylesheet">
    <!--<link href="<?php echo base_url(); ?>public/plugins/bootstrap4_3_1/bootstrap.min.css" rel="stylesheet">-->
    <script src="<?php echo base_url(); ?>public/plugins/note/jquery-3.5.1.min.js"></script>
    <script src="<?php echo base_url(); ?>public/plugins/note/bootstrap.min.js"></script>
    <!--<script src="<?php echo base_url(); ?>public/plugins/bootstrap4_3_1/bootstrap.min.js"></script>-->
    <!-- include summernote css/js -->
    <link href="<?php echo base_url(); ?>public/plugins/note/summernote.min.css" rel="stylesheet">
    <script src="<?php echo base_url(); ?>public/plugins/note/summernote.min.js"></script>

    <link href="<?php echo base_url(); ?>public/css/fontawesome/css/all.css" type="text/css" rel="stylesheet">
    <script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/fileinput/fileinput_bos.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/plugins/fileinput/fileinput_bos.css">

    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/js/materialize.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>public/js/control_tareas_info_ac.js?v=<?php echo date('YmdGis');?>" ></script>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/plugins/toastr/toastr.css">
    <script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/toastr/toastr.min.js" ></script>
    <style type="text/css">
      .obtenerinfo img{width: 200px;}
      .materialboxed {display: block;cursor: zoom-in;}
      .materialboxed:hover:not(.active) {     opacity: .8;   }
      .materialboxed.active { cursor: zoom-out;}
   
 #materialbox-overlay {position: fixed;top: 0;background: #6b6b6b75;z-index: 1;   }
   
  .materialbox-caption {
     position: fixed;
     display: none;
 
   }
   .obtenerinfo{
    max-height: 400px;
    overflow: auto;
   }
   
    </style>
    <style type="text/css">
      .card {
          position: relative;
          display: -ms-flexbox;
          display: flex;
          -ms-flex-direction: column;
          flex-direction: column;
          min-width: 0;
          word-wrap: break-word;
          background-color: #fff;
          background-clip: border-box;
          border: 1px solid rgba(0, 0, 0, .125);
          border-radius: .25rem;
          margin-bottom: 10px;
      }
      .card-body {
    -ms-flex: 1 1 auto;
    flex: 1 1 auto;
    min-height: 1px;
    padding: 1.25rem;
}
.card-title {
    margin-bottom: .75rem;
}
.card-text:last-child {
    margin-bottom: 0;
}
.card-footer:last-child {
    border-radius: 0 0 calc(.25rem - 1px) calc(.25rem - 1px);
}
.card-footer {
    padding: .75rem 1.25rem;
    background-color: rgba(0, 0, 0, .03);
    border-top: 1px solid rgba(0, 0, 0, .125);
    text-align: end;
}
.card-header:first-child {
    border-radius: calc(.25rem - 1px) calc(.25rem - 1px) 0 0;
}
.card-header {
    padding: .75rem 1.25rem;
    margin-bottom: 0;
    background-color: rgba(0, 0, 0, .03);
    border-bottom: 1px solid rgba(0, 0, 0, .125);
    text-align: center;
    font-weight: bold;
}
.img_leido{
  width: 20px !important;
    margin-left: auto;
}
.material-tooltip {
  padding: 10px 8px;
  font-size: 1rem;
  z-index: 2000;
  background-color: transparent;
  border-radius: 2px;
  color: #fff;
  min-height: 36px;
  line-height: 120%;
  opacity: 0;
  position: absolute;
  text-align: center;
  max-width: calc(100% - 4px);
  overflow: hidden;
  left: 0;
  top: 0;
  pointer-events: none;
  visibility: hidden;
}
.campodescripcion{
  width: 85%;
  margin-left: auto;
    margin-right: auto;
    float: unset;
}
.btn-codeview{
  display: none;
}
.viewmensajes{
  width: 75%;
  float: right;
}
.personal_<?php echo $idpersonal;?>{
  float: left !important;
}
.card-footer{
  padding: 3px 12px;
  font-size: 12px;
}

@media only screen and (max-width: 500px){
  .campodescripcion{
    width: 100%;
  }
  .viewmensajes{
    width: 100%;
  }
}
    </style>
  </head>
  <body>
    <input type="hidden" id="idreg" value="<?php echo $idreg;?>">
    <input type="hidden" id="base_url" value="<?php echo base_url();?>">
    <div class="container">
      <div class="row">
        <div class="col-md-12 obtenerinfo" id="obtenerinfo">
          
        </div>
      </div>
      <div class="row">
        <div class="col-md-10 campodescripcion summernotetext">
          <label>Descripción</label>
          <textarea class="form-control summernote" style="min-height:200px !important"></textarea>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <input type="file" id="files" name="files[]" multiple > 
        </div>
      </div>
      <div class="row">
        <div class="input-field col-md-12">
          <button class="btn btn-success right enviodeticket"  name="action">Enviar</button>
        </div>
      </div>
    </div>
    
  </body>
</html>