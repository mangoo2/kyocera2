var base_url = $('#base_url').val();
$(document).ready(function() {
  summernote();
  $('.enviodeticket').click(function(event) {
    var gurl= $('#base_url').val()+'Generales/ticket';
    console.log(gurl);
    $.ajax({
        type:'POST',
        url: gurl,
        data: {
            title: $('.notetitle').val(),
            contenido: $('.summernote').val()
        },
        async: false,
        statusCode:{
            404: function(data){
                swal("Error", "404", "error");
            },
            500: function(){
                swal("Error", "500", "error"); 
            }
        },
        success:function(data){
          console.log(data)
          toastr["success"]("Solicitud Enviada", "Hecho!");
          $('.summernote').val('');$('.notetitle').val('');
          $('.notetitle').val('');
        }
    });
  });
  $('.enviocomentario').click(function(event) {
        var gurl= $('#base_url').val()+'Generales/comen_pni';
        console.log(gurl);
        $.ajax({
            type:'POST',
            url: gurl,
            data: {
                id: $('#idp').val(),
                comen: $('.summernote').val()
            },
            async: false,
            statusCode:{
                404: function(data){
                    swal("Error", "404", "error");
                },
                500: function(){
                    swal("Error", "500", "error"); 
                }
            },
            success:function(data){
              console.log(data)
              toastr["success"]("Comentario Enviado", "Hecho!");
              $('.summernote').val('');
                setTimeout(function(){ 
                    location.reload();
                }, 2000);
            }
        });
  });
});
function summernote(){
  $('.summernote').summernote({toolbar: [ ['style', ['bold', 'italic', 'underline', 'clear']],['insert', ['link', 'picture', 'video']]]});
}