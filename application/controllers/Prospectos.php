<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Prospectos extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->model('Prospectos_model', 'model');
        $this->load->model('Clientes_model');
        $this->load->model('Estados_model', 'estados');
        $this->load->model('Cotizaciones_model');
        $this->load->model('ModeloCatalogos');
        $this->load->helper('url');
        $this->idpersonal = $this->session->userdata('idpersonal');
        $this->submenu=4;
    }
    // Listado de Empleados
    function index(){
        $data['MenusubId']=$this->submenu;
        if($this->session->userdata('logeado')==true){
            // Obtiene los menús disponibles del sistema dependiendo el perfil, ayudado de la libreria "MENUS"
            $data['familia'] = $this->model->getDataFamilia();
            $this->load->view('header');
            $this->load->view('main',$data);
            $this->load->view('prospectos/listado');
            $this->load->view('prospectos/listado_js');
            $this->load->view('footerl');
        }else{
            redirect('login');
        }
    }

    // Listado de Empleados
    function alta(){
        $data['MenusubId']=$this->submenu;
        if($this->session->userdata('logeado')==true){
            // Obtiene los menús disponibles del sistema dependiendo el perfil, ayudado de la libreria "MENUS"
            $data['tipoVista'] = 1;
            $data['estados'] = $this->estados->getEstados();
            $data['cliente_dire'] = array();
            $data['datoscontacto']=$this->ModeloCatalogos->getselectwheren('cliente_datoscontacto',array('clienteId'=>0,'activo'=>1));
            $this->load->view('header');
            $this->load->view('main',$data);
            $this->load->view('prospectos/form_prospectos');
            $this->load->view('prospectos/form_prospectos_js');
            $this->load->view('footer');
        }else{
            redirect('login');
        }
    }
        // Edicion de Equipo
    function edicion($idProspecto){
        $data['MenusubId']=$this->submenu;
        if($this->session->userdata('logeado')==true){
            // Obtiene los menús disponibles del sistema dependiendo el perfil, ayudado de la libreria "MENUS"
            $data['prospecto'] = $this->model->getProspectoPorId($idProspecto);
            $data['tipoVista'] = 2;
            $data['estados'] = $this->estados->getEstados();
            $data['cliente_dire'] = $this->Clientes_model->getClientedirecPorId($idProspecto);
            $data['datoscontacto']=$this->ModeloCatalogos->getselectwheren('cliente_datoscontacto',array('clienteId'=>$idProspecto,'activo'=>1));
            $this->load->view('header');
            $this->load->view('main',$data);
            $this->load->view('prospectos/form_prospectos');
            $this->load->view('prospectos/form_prospectos_js');
            $this->load->view('footer');
        }else{
            redirect('login');
        }
    }


        // Visualización de Equipo
    function visualizar($idProspecto){
        $data['MenusubId']=$this->submenu;
        if($this->session->userdata('logeado')==true){
            // Obtiene los menús disponibles del sistema dependiendo el perfil, ayudado de la libreria "MENUS"
            $data['prospecto'] = $this->model->getProspectoPorId($idProspecto);
            $data['tipoVista'] = 3;
            $data['estados'] = $this->estados->getEstados();
            $data['cliente_dire'] = $this->Clientes_model->getClientedirecPorId($idProspecto);
            $data['datoscontacto']=$this->ModeloCatalogos->getselectwheren('cliente_datoscontacto',array('clienteId'=>$idProspecto,'activo'=>1));
            $this->load->view('header');
            $this->load->view('main',$data);
            $this->load->view('prospectos/form_prospectos');
            //$this->load->view('panel');
            $this->load->view('prospectos/form_prospectos_js');
            $this->load->view('footer');
        }
        else
        {
            redirect('login');
        }
    }
    function convertirProspectoCliente($idProspecto){
        $data = array('tipo' => 1);
        $actualizado = $this->model->update_prospecto($data, $idProspecto);
        if($actualizado==true)
        echo 1;
    }
    function insertaActualizaProspectos(){
        $result = 0; 
        $data = $this->input->post();

        $persona_contacto = $this->input->post('persona_contacto');
        $datos_direccion = $this->input->post('datos_direccion');
        
               // Ordenamos los datos principales del cliente
        $datosProspectos = array(
                            'empresa' => $data["empresa"],
                            'estado' => $data["estado"],
                            'municipio' => $data["municipio"],
                            'giro' => $data["giro"],
                            'observaciones' => $data["observaciones"],
                            'tipo' => 2,
                            'estatus' => 1,
                            'antiguedad'=>$data['antiguedad'],
                            'idUsuario' => $this->session->userdata["usuarioid"],
                            'aaa'=>$data['aaa'],

                            );
        
        if(!isset($data['idProspecto'])){
           $id = $this->model->insertar_prospecto($datosProspectos); 
           //$this->ModeloCatalogos->Insert('clientes_direccion',array('idcliente'=>$id,'direccion'=>$data["direccion"]));
           $result = $id;
            //$this->Modelobitacoras->Insert(array('contenido'=>'Se Inserto Prospecto','nombretabla'=>'clientes','idtable'=>$id,'tipo'=>'Insert','personalId'=>$this->idpersonal));
        }
        else{
        // update
           $id=$data['idProspecto'];
           unset($data['idProspecto']);
           $result = $this->model->update_prospecto($datosProspectos,$id); 
           //$this->ModeloCatalogos->getdeletewheren3('clientes_direccion',array('idcliente'=>$id));
           //$this->ModeloCatalogos->Insert('clientes_direccion',array('idcliente'=>$id,'direccion'=>$data["direccion"]));
           //$this->Modelobitacoras->Insert(array('contenido'=>'Se Actualizo Prospecto','nombretabla'=>'clientes','idtable'=>$id,'tipo'=>'Insert','personalId'=>$this->idpersonal)); 
        }
        $DATA_CONTACTO = json_decode($persona_contacto);        
        for ($i = 0; $i < count($DATA_CONTACTO); $i++) {
            if ($DATA_CONTACTO[$i]->datosId==0) {
                
                $puesto=$DATA_CONTACTO[$i]->puesto;
                $puesto = str_replace("---", "&", $puesto);

                $email=$DATA_CONTACTO[$i]->email;
                $email = str_replace("---", "&", $email);

                $temp=array(
                    'clienteId'=>$id,
                    'atencionpara'=>$DATA_CONTACTO[$i]->atencionpara,
                    'puesto'=>$puesto,
                    'email'=>$email,
                    'telefono'=>$DATA_CONTACTO[$i]->telefono,
                    'celular'=>$DATA_CONTACTO[$i]->celular
                );
                $this->ModeloCatalogos->Insert('cliente_datoscontacto',$temp);
            }
        } 

        $datos_direcc = json_decode($datos_direccion);
        for ($i = 0; $i < count($datos_direcc); $i++) {
            $idclientedirecc = $datos_direcc[$i]->id;
            $direccion = $datos_direcc[$i]->direccion;
                if($direccion!=''){   
                    $idarrayd = array('idclientedirecc' => $idclientedirecc);    
                    $tempd=array(
                        "idcliente"=>$id,
                        "direccion"=>$direccion
                    );
                    if($idclientedirecc==0){
                        $this->model->insertarToCatalogo($tempd,"clientes_direccion");
                    }else{
                        $this->ModeloCatalogos->updateCatalogo("clientes_direccion",$tempd,$idarrayd);
                    }
                    
                }
        }   
     
         
        echo $result;  
    }

    public function getListadoProspectos()
    {
        $prospectacion = $this->model->getListadoProspectos();
        $json_data = array("data" => $prospectacion);
        echo json_encode($json_data);
    }
    /*
    public function getListadoProspectosPorUsuario($idUsuario) {
        if($idUsuario==1)
        {   
            $prospectacion = $this->model->getListadoProspectos($idUsuario);
        }
        else
        {
            $prospectacion = $this->model->getListadoProspectosPorUsuario($idUsuario);
        }
        $json_data = array("data" => $prospectacion);
        echo json_encode($json_data);
    }
    */
    public function getListadoProspectosPorUsuario_asic($idUsuario) {
        $params = $this->input->post();
        
        $params['idUsuario']=$idUsuario;
        $productos = $this->model->getListadoProspectosPorUsuario_asic($params);
        $totalRecords=$this->model->getListadoProspectosPorUsuario_asict($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totalRecords),  
            "recordsFiltered" => intval($totalRecords),
            "data"            => $productos->result(),
            "query"           =>$this->db->last_query()   
        );
        
        echo json_encode($json_data);
    }

    function eliminarProspecto($idProspecto) 
    {
        $datosProspecto = array('estatus' => 0);
        $eliminado = $this->model->update_prospecto($datosProspecto, $idProspecto);
        echo $eliminado;
        //$this->Modelobitacoras->Insert(array('contenido'=>'Se Elimino Prospecto','nombretabla'=>'clientes','idtable'=>$eliminado,'tipo'=>'Delete','personalId'=>$this->idpersonal)); 
    }

    function getListadoTel_local($idTelefono)
    {
        $tel_local = $this->model->getListadoTel_local($idTelefono);
        $json_data = array("data" => $tel_local);
        echo json_encode($json_data);
    }

    function getListadoTel_celular($idTelefono)
    {
        $tel_celular = $this->model->getListadoTel_celular($idTelefono);
        $json_data = array("data" => $tel_celular);
        echo json_encode($json_data);
    }

    function getListadoPersona_contacto($idContacto)
    {
        $contacto = $this->model->getListadoPersona_contacto($idContacto);
        $json_data = array("data" => $contacto);
        echo json_encode($json_data);
    }

    function eliminar_tel_local($id_tel) 
    {
        $eliminado = $this->model->eliminar_tel_local($id_tel);
        echo $eliminado;
    }


    function eliminar_tel_celular($id_tel) 
    {
        $eliminado = $this->model->eliminar_tel_celular($id_tel);
        echo $eliminado;
    }

    function eliminar_persona_contacto($id_contacto) 
    {
        $eliminado = $this->model->eliminar_persona_contacto($id_contacto);
        echo $eliminado;
    }

    function getInfo($idProspectoCliente)
    {
        $data['estatus'] = 1;
        // Verificamos primero el tipo para saber si es Prospecto o Cliente
        $datosProspectoCliente = $this->model->getProspectoPorId($idProspectoCliente);
        $datosCotizacion = $this->Cotizaciones_model->getDatosCotizacionPorIdProspectoCliente($idProspectoCliente);

        if($datosProspectoCliente->tipo==1)
        {  
            $data['estatus'] = 3;
        }
        // Debemos saber si tiene ya creada al menos una cotización
        else if(!empty($datosCotizacion))
        {
            $data['estatus'] = 2;
        }
        $data['datosProspectoCliente'] = $datosProspectoCliente;

        $this->load->view('prospectos/table',$data);
    }

    function cotizar($idProspecto,$tipo,$cotizar,$idfamilia) 
    {   
        $menus = $this->getMenus($this->session->userdata('perfilid'));
        $data['menus'] = $menus['menus'];
        $data['submenusArray'] = $menus['submenusArray'];
        $data['prospecto'] = $this->model->getProspectoPorId($idProspecto);
        $data['equipos'] = $this->model->getDataEquipo($idfamilia);
        if($tipo==1){
        $data['tipo'] = "Venta";
        }else if($tipo==2){
        $data['tipo'] = "Renta";
        }else if($tipo==3){
        $data['tipo'] = "Póliza";
        }
        $data['cotizar'] = "Familiar";
        $this->load->view('header');
        $this->load->view('main',$data);
        $this->load->view('prospectos/cotizar');
        $this->load->view('prospectos/jscotizar');
        $this->load->view('footer'); 
    }
    function cotizacion($idProspecto,$tipo,$cotizar) 
    {   
        $menus = $this->getMenus($this->session->userdata('perfilid'));
        $data['menus'] = $menus['menus'];
        $data['submenusArray'] = $menus['submenusArray'];
        $data['prospecto'] = $this->model->getProspectoPorId($idProspecto);
        $data['equipos'] = $this->model->getDataEquipo2();
        if($tipo==1){
        $data['tipo'] = "Venta";
        }else if($tipo==2){
        $data['tipo'] = "Renta";
        }else if($tipo==3){
        $data['tipo'] = "Póliza";
        }
        $data['cotizar'] = "Manual";
        $this->load->view('header');
        $this->load->view('main',$data);
        $this->load->view('prospectos/cotizar');
        $this->load->view('prospectos/jscotizar');
        $this->load->view('footer'); 
    }

}

?>