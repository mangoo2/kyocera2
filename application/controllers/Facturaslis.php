<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Facturaslis extends CI_Controller {
    public function __construct()     {
        parent::__construct();
        $this->load->model('ModeloGeneral');
        $this->load->model('ModeloCatalogos');
        $this->load->model('Modelofacturas');
        $this->load->model('ModeloComplementos');
        $this->load->model('Ventas_model');
        $this->load->model('ConfiguracionDocumentos_Model');
        $this->load->helper('url');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fechaactual=date('Y-m-d H:i:s');
        $this->mesactual = date('n');//m con ceros iniciales n sin ceros iniciales
        $this->submenu=59;
        if($this->session->userdata('logeado')==true){
            $this->perfilid =$this->session->userdata('perfilid');
            $this->idpersonal = $this->session->userdata('idpersonal');
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,$this->submenu);// 52 es el id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
        }else{
            redirect('login'); 
        }
    }
    function index(){
        $data['MenusubId']=$this->submenu;
            if ($this->perfilid==1) {
                $data['idpersonal']=0;
                $data['vendedor']='block';
            }else{
                $data['vendedor']='none';
                //$data['idpersonal']=$this->idpersonal;
                $data['idpersonal']=0;
            }
            $data['perfilid']=$this->perfilid;
            $data['viewidpersonal']=$this->idpersonal;
            //$data['personalrows']=$this->ModeloCatalogos->getselectwheren('personal',array('estatus'=>1));
            $data['personalrows']=$this->ModeloGeneral->listadopersonal();
            $data['fechainicial_reg'] = date("Y-m-d",strtotime($this->fechaactual."- 1 year")); 
            $data['metodorow']=$this->ModeloCatalogos->getselectwheren('f_formapago',array('activo'=>1));
            $data['perfirma']=$this->ModeloCatalogos->seleccionejecutivofirma();
            $data['lis_fac_prog']=$this->ModeloGeneral->listadofacturasprogramadas();
            $this->load->view('header');
            $this->load->view('main');
            $this->load->view('facturaciones/Facturaslis',$data);
            $this->load->view('footer');
            $this->load->view('facturaciones/modalpagosi');
            $this->load->view('facturaciones/Facturaslisjs');
    }
    public function getlistfacturas() {
        $params = $this->input->post();
        $getdata = $this->Modelofacturas->getfacturas($params);
        $totaldata= $this->Modelofacturas->total_facturas($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    public function getlistfacturas_pp() {
        $params = $this->input->post();
        $getdata = $this->Modelofacturas->getfacturas_pp($params);
        $totaldata= $this->Modelofacturas->total_facturas_pp($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    function add($facturaId=0){
        $data['MenusubId']=$this->submenu;
            $data['cfdi']=$this->ModeloGeneral->genSelect('f_uso_cfdi'); 
            $data['metodo']=$this->ModeloGeneral->genSelect('f_metodopago'); 
            $data['forma']=$this->ModeloGeneral->genSelect('f_formapago');
            $data['fservicios']=$this->ModeloCatalogos->getselectwheren('f_servicios',array('activo'=>1));
            $data['funidades']=$this->ModeloCatalogos->getselectwheren('f_unidades',array('activo'=>1)); 
            $data['facturaId']=$facturaId;
            $data['perfilid']=$this->perfilid;
            $data['idpersonal']=$this->idpersonal;
            $data['personalrows']=$this->ModeloCatalogos->getselectwheren('personal',array('estatus'=>1));
            $data['fac_pue_pend']=$this->ModeloGeneral->verificar_pue_pendientes_pago($this->idpersonal);
            $this->load->view('header');
            $this->load->view('main');
            $this->load->view('facturaciones/Facturasadd',$data);
            $this->load->view('footer');
            $this->load->view('facturaciones/Facturasaddjs',$data);
    }
    function obtenerrfc(){
        $params = $this->input->post();
        $idcliente = $params['id'];
        $rfc = $params['rfc'];
        $result=$this->ModeloCatalogos->getselectwheren('cliente_has_datos_fiscales',array('idCliente'=>$idcliente,'activo'=>1));
        $html='';
        foreach ($result->result() as $item) {
            if($rfc==$item->rfc){
                $rfcselected=' selected';
            }else{
                $rfcselected='';
            }
            $html.='<option value="'.$item->id.'" '.$rfcselected.'>'.$item->rfc.'</option>';
        }
        echo $html;
    }
    function obtenerventas(){
        $params = $this->input->post();
        $tipo =$params['tipo'];
        $cliente = $params['cliente'];
        $tipov =$params['tipov'];
        if ($this->perfilid==1) {
            $ejecutivo =0;
        }else{
            //$ejecutivo =$this->idpersonal;
            $ejecutivo =0;
        }
        if ($tipo==1) {
                if ($ejecutivo>0) {
                    $whereejecutivo=" v.id_personal=$ejecutivo and ";
                }else{
                    $whereejecutivo="";
                }
                /*
                $this->db->select('v.estatus,v.id,v.prefactura,v.telefono,v.correo,cli.empresa, v.reg,fac.facturaId');
                $this->db->from('ventas v ');
                $this->db->join('clientes cli', 'cli.id=v.idCliente');
                $this->db->join('factura_venta fac', 'fac.ventaId=v.id','left');
                $where = array('v.prefactura'=>1,'fac.combinada'=>0);
                $where['v.idCliente']=$cliente;
                $this->db->where($where);
                $this->db->order_by('v.id', 'DESC');
                $this->db->limit(10);
                */
                $select="SELECT v.estatus, v.id, v.prefactura, v.telefono, v.correo, cli.empresa, v.reg, facf.FacturasId,v.siniva,v.tipov,
                         (SELECT sum(vhdc.piezas*vhdc.precioGeneral) as total FROM ventas_has_detallesConsumibles as vhdc WHERE vhdc.idVentas=v.id) as totalcon, 
                        (SELECT sum(vhdr.piezas*vhdr.precioGeneral) as total FROM ventas_has_detallesRefacciones as vhdr WHERE vhdr.idVentas=v.id) as totalref,
                        (SELECT sum(vhde.cantidad*vhde.precio) AS total FROM ventas_has_detallesEquipos AS vhde WHERE vhde.idVenta=v.id ) as totalequ,
                        (SELECT sum(vhdac.cantidad*vhdac.costo) AS total FROM ventas_has_detallesEquipos_accesorios AS vhdac WHERE vhdac.id_venta=v.id ) as totalacc
                        FROM ventas as v
                        JOIN clientes as cli ON cli.id=v.idCliente
                        LEFT JOIN factura_venta as fac ON fac.ventaId=v.id AND fac.combinada = 0
                        LEFT join f_facturas as facf on facf.FacturasId=fac.facturaId and facf.Estado=1
                        WHERE v.prefactura = 1 and v.requ_fac=1
                        and v.activo=1
                        AND $whereejecutivo v.idCliente = $cliente and v.total_general>0 and v.tipov='$tipov' AND facf.FacturasId IS NULL
                        ORDER BY v.id DESC
                         LIMIT 95 ";
                $query=$this->db->query($select);

            $html='<table class="responsive-table display no-footer">
                        <thead>
                            <tr><th>#</th><th>Cliente</th><th></th><th>Prefactura</th><th>Fecha</th><th></th>
                            </tr>
                        </thead>
                        <tbody>
                    ';
            foreach ($query->result() as $item) {
                $verificarseries=$this->ModeloCatalogos->venta_verificarseries($item->id,0);
                if ($item->FacturasId>0) {
                    $styledisplay='style="display:none"';
                }else{
                    $styledisplay='';
                    if($item->siniva==1){
                        $styledisplay='style="display:none"';
                    }
                }
                $totalventa=$item->totalcon+$item->totalref+$item->totalequ+$item->totalacc;
                if($totalventa==0){
                    $styledisplay='style="display:none"';
                }$view_tipov='';
                if($item->tipov==1){
                    $view_tipov='Alta Productividad';
                }
                if($item->tipov==2){
                    $view_tipov='D-Impresión';
                }
                if($verificarseries==1){
                    $styledisplay='sinserie style="display:none"';
                }
                $html.='<tr '.$styledisplay.' class="totalventa_'.$totalventa.'">
                            <td>'.$item->id.'</td><td>'.$item->empresa.'</td><td>'.$view_tipov.'</td>
                            <td>
                                <a class="btn-floating green tooltipped" data-position="top" data-delay="50" data-tooltip="Prefactura" onclick="detalle('.$item->id.',0)"><i class="material-icons">assignment</i></a>
                            </td>
                            <td>'.$item->reg.'</td>
                            <td>';
                            if ($item->FacturasId>0) {
                                $html.='<a class="waves-effect red btn-bmz"  >Facturada</a>';
                            }else{
                                if(date("n",strtotime($item->reg))==date('n')){
                                    $im_onclick='importarproductos('.$item->id.',0,1)';
                                }else{
                                    $im_onclick='importarproductos_pass('.$item->id.',0,1)';
                                }
                                $html.='<button class="waves-effect green btn-bmz import_'.$item->id.'" onclick="'.$im_onclick.'" >Importar</button>';
                            }
                        
                        $html.='</td>
                        </tr>';
            }
            $html.='</tbody></table>';
            echo $html;
        }
        if ($tipo==2) {
            if ($ejecutivo>0) {
                    $whereejecutivo=" vc.id_personal=$ejecutivo and ";
                }else{
                    $whereejecutivo="";
                }
            /*
            $this->db->select('vc.estatus,vc.combinadaId id,vc.prefactura,vc.telefono,vc.correo,cli.empresa, vc.reg,fac.facturaId');
            $this->db->from('ventacombinada vc ');
            $this->db->join('clientes cli', 'cli.id=vc.idCliente');
            $this->db->join('factura_venta fac', 'fac.ventaId=vc.combinadaId','left');
            $where = array('vc.prefactura'=>1,'fac.combinada'=>1);
            $where['vc.idCliente']=$cliente;
            $this->db->where($where);
            $this->db->order_by('vc.combinadaId', 'DESC');
            $this->db->limit(10);
            $query=$this->db->get();
            */
            $select="SELECT vc.estatus, vc.combinadaId as id, vc.prefactura, vc.telefono, vc.correo, cli.empresa, vc.reg, facf.FacturasId,vc.siniva,vc.tipov,vc.equipo,vc.consumibles,vc.refacciones,vc.poliza,vc.total_general
                FROM `ventacombinada` `vc`
                JOIN `clientes` `cli` ON `cli`.`id`=`vc`.`idCliente`
                LEFT JOIN `factura_venta` `fac` ON `fac`.`ventaId`=`vc`.`combinadaId` AND `fac`.`combinada` = 1
                LEFT join f_facturas as facf on facf.FacturasId=fac.facturaId and facf.Estado=1
                WHERE $whereejecutivo vc.prefactura = 1 and vc.activo=1
                AND `vc`.`idCliente` = $cliente and vc.tipov='$tipov' and vc.requ_fac=1
                ORDER BY `vc`.`combinadaId` DESC";
            $query=$this->db->query($select);
            $html='<table class="responsive-table display no-footer">
                        <thead>
                            <tr><th>#</th><th>Cliente</th><th></th><th>Prefactura</th><th>Fecha</th><th></th></tr>
                        </thead>
                        <tbody>';
            foreach ($query->result() as $item) {
                $idpre=$item->id;
                $verificarseries=$this->ModeloCatalogos->venta_verificarseries($item->id,1);
                if ($item->FacturasId>0) {
                    $styledisplay='style="display:none"';
                }else{
                    $styledisplay='';
                    if($item->siniva==1){
                        $styledisplay='style="display:none"';
                    }
                }
                if($item->total_general==0){
                    $styledisplay='style="display:none"';
                }
                $view_tipov='';
                if($item->tipov==1){
                    $view_tipov='Alta Productividad';
                }
                if($item->tipov==2){
                    $view_tipov='D-Impresión';
                }
                if($item->equipo>0){
                    $idpre=$item->equipo;

                }elseif($item->consumibles>0){
                    $idpre=$item->consumibles;

                }elseif($item->refacciones>0){
                    $idpre=$item->refacciones;
                    
                }elseif($item->poliza>0){
                    $idpre=$item->poliza;
                }
                if($verificarseries==1){
                    $styledisplay='sinserie style="display:none"';
                }
                $html.='<tr '.$styledisplay.'>
                            <td>'.$idpre.'</td><td>'.$item->empresa.'</td><td>'.$view_tipov.'</td>
                            <td><a class="btn-floating green tooltipped" data-position="top" data-delay="50" data-tooltip="Prefactura" onclick="detalle('.$item->id.',1)"><i class="material-icons">assignment</i></a></td>
                            <td>'.$item->reg.'</td>
                            <td>';
                            if ($item->FacturasId>0) {
                                $html.='<a class="waves-effect red btn-bmz"  >Facturada</a>';
                            }else{
                                if(date("n",strtotime($item->reg))==date('n')){
                                    $im_onclick='importarproductos('.$item->id.',1,2)';
                                }else{
                                    $im_onclick='importarproductos_pass('.$item->id.',1,2)';
                                }
                                $html.='<button class="waves-effect green btn-bmz import_'.$item->id.'" onclick="'.$im_onclick.'">Importar</button>';
                            }
                                
                            $html.='</td>
                        </tr>';
            }
            $html.='</tbody></table>';


            echo $html;
        }
        if ($tipo==3) {//poliza
            
                $this->db->select('v.id,v.estatus,v.prefactura,v.telefono,v.correo,cli.empresa,fac.facturaId,v.siniva,v.tipov,v.reg,v.subtotal');
                $this->db->from('polizasCreadas v ');
                $this->db->join('clientes cli', 'cli.id=v.idCliente');
                $this->db->join('factura_poliza fac', 'fac.polizaId=v.id','left');
                $where = array(
                            'v.idCliente'=>$cliente,
                            'v.prefactura'=>1,
                            'v.activo'=>1,
                            'v.combinada'=>0,
                            'v.tipov'=>$tipov,
                            'v.requ_fac'=>1
                        );
                
                $this->db->where($where);
                if ($ejecutivo>0) {
                    $this->db->where(array('v.id_personal' =>$ejecutivo));
                    //$whereejecutivo=" v.id_personal=$ejecutivo and ";
                }
                $this->db->order_by('v.id', 'DESC');
                $this->db->limit(12);
                $query=$this->db->get();

                $html='<table class="responsive-table display no-footer">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Cliente</th>
                                <th></th>
                                <th>Prefactura</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                    ';
            foreach ($query->result() as $item) {
                $polizaventadetalles = $this->ModeloCatalogos->polizaventadetalles3($item->id);
                $pre_gene=0;
                foreach ($polizaventadetalles->result() as $itempd) {
                    $pre_d=0;
                    if($itempd->precio_local >0) {
                        $pre_d=$itempd->precio_local;
                    }elseif($itempd->precio_semi >0) {
                        $pre_d=$itempd->precio_semi;
                    }elseif($itempd->precio_foraneo >0) {
                        $pre_d=$itempd->precio_foraneo;
                    }elseif($itempd->precio_especial >0) {
                        $pre_d=$itempd->precio_especial;
                    }
                    $pre_gene+=$pre_d;
                }
                $styledisplay='';
                    if($item->siniva==1){
                        $styledisplay='style="display:none"';
                    }
                    $view_tipov='';
                    if($item->tipov==1){
                        $view_tipov='Alta Productividad';
                    }
                    if($item->tipov==2){
                        $view_tipov='D-Impresión';
                    }
                    if($pre_gene==0){
                        $styledisplay='style="display:none"';
                    }
                $html.='<tr '.$styledisplay.' class="pre_gene_'.$pre_gene.'">
                            <td>'.$item->id.'</td><td>'.$item->empresa.'</td><td>'.$view_tipov.'</td>
                            <td>
                                <a class="btn-floating green tooltipped" data-position="top" data-delay="50" data-tooltip="Prefactura" onclick="detallep('.$item->id.')" >
                                    <i class="material-icons">assignment</i>
                                </a>
                            </td>
                            <td>';
                            if ($item->facturaId>0) {
                                $html.='<a class="waves-effect red btn-bmz"  >Facturada</a>';
                            }else{
                                if(date("n",strtotime($item->reg))==date('n')){
                                    $im_onclick='importarproductos('.$item->id.',2,3)';
                                }else{
                                    $im_onclick='importarproductos_pass('.$item->id.',3,3)';
                                }
                                $html.='<button class="waves-effect green btn-bmz import_'.$item->id.'" onclick="importarproductos('.$item->id.',2,3)" >Importar</button>';
                            }
                        
                        $html.='</td>
                        </tr>';
            }
            $html.='</tbody></table>';


            echo $html;
        }
        if ($tipo==4) {//servicio
            $querysql1 ="SELECT asig.asignacionId, cli.empresa, asig.fecha, asig.hora, asig.horafin, fac.facturaId
                    FROM asignacion_ser_cliente_a as asig
                    JOIN clientes as cli ON cli.id=asig.clienteId
                    LEFT JOIN factura_servicio as fac ON fac.servicioId=asig.asignacionId AND fac.tipo = 3
                    WHERE asig.clienteid = '$cliente'
                    ORDER BY asig.asignacionId DESC
                    LIMIT 12 ";
            $query1 = $this->db->query($querysql1);

            $querysql2 ="SELECT asig.asignacionId,cli.empresa,fac.facturaId
                        FROM asignacion_ser_contrato_a as asig
                        inner join contrato as cont on cont.idcontrato=asig.contratoId
                        inner JOIN rentas as ren on ren.id=cont.idRenta
                        inner join clientes as cli on cli.id=ren.idCliente
                        LEFT JOIN factura_servicio as fac ON fac.servicioId=asig.asignacionId AND fac.tipo = 1
                        WHERE asig.tiposervicio=1  
                              and ren.idCliente='$cliente'
                        ORDER BY asig.asignacionId DESC
                        LIMIT 12 

                        ";
            $query2 = $this->db->query($querysql2);

            $querysql3 ="SELECT asig.asignacionId, asig.polizaId,cli.empresa,fac.facturaId
                        FROM asignacion_ser_poliza_a as asig
                        inner JOIN polizasCreadas as pol on pol.id=asig.polizaId
                        inner join clientes as cli on cli.id=pol.idCliente
                        LEFT JOIN factura_servicio as fac ON fac.servicioId=asig.asignacionId AND fac.tipo = 2
                        WHERE asig.cobrar=1
                            and cli.id='$cliente'
                        ORDER BY asig.asignacionId DESC
                        LIMIT 12

                        ";
            $query3 = $this->db->query($querysql3);

                $html='<table class="responsive-table display no-footer">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Cliente</th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                    ';
                foreach ($query1->result() as $item) {
                    $html.='<tr>
                                <td>'.$item->asignacionId.'</td><td>'.$item->empresa.'</td><td>Alta Productividad</td>
                                <td>Evento</td>
                                <td>';
                                if ($item->facturaId>0) {
                                    $html.='<a class="waves-effect red btn-bmz"  >Facturada</a>';
                                }else{
                                    $html.='<button class="waves-effect green btn-bmz import_'.$item->asignacionId.'" onclick="importarproductoss('.$item->asignacionId.',3,4)" >Importar</button>';
                                }
                            $html.='</td>
                            </tr>';
                }
                foreach ($query2->result() as $item) {
                    $html.='<tr>
                                <td>'.$item->asignacionId.'</td><td>'.$item->empresa.'</td><td>Alta Productividad</td>
                                <td>Contrato</td>';
                                if ($item->facturaId>0) {
                                    $html.='<a class="waves-effect red btn-bmz"  >Facturada</a>';
                                }else{
                                    $html.='<button class="waves-effect green btn-bmz import_'.$item->asignacionId.'" onclick="importarproductoss('.$item->asignacionId.',1,4)" >Importar</button>';
                                }
                            $html.='</td>
                            </tr>';
                }
                foreach ($query3->result() as $item) {
                    $html.='<tr>
                                <td>'.$item->asignacionId.'</td><td>'.$item->empresa.'</td><td>Alta Productividad</td>
                                <td>Poliza</td>
                                <td>
                                ';
                                if ($item->facturaId>0) {
                                    $html.='<a class="waves-effect red btn-bmz"  >Facturada</a>';
                                }else{
                                    $html.='<button class="waves-effect green btn-bmz import_'.$item->asignacionId.'" onclick="importarproductoss('.$item->asignacionId.',2,4)" >Importar</button>';
                                }
                            $html.='</td>
                            </tr>';
                }
            $html.='</tbody></table>';


            echo $html;
        }
        /*
        if ($tipo==5) {
                if ($ejecutivo>0) {
                    $whereejecutivo=" v.id_personal=$ejecutivo and ";
                }else{
                    $whereejecutivo="";
                }
                
                $select="SELECT v.estatus, v.id, v.prefactura, v.telefono, v.correo, cli.empresa, v.reg, fac.facturaId,v.siniva,
                         (SELECT sum(vhdc.piezas*vhdc.precioGeneral) as total FROM ventasd_has_detallesconsumibles as vhdc WHERE vhdc.idVentas=v.id) as totalcon, 
                        (SELECT sum(vhdr.piezas*vhdr.precioGeneral) as total FROM ventasd_has_detallesrefacciones as vhdr WHERE vhdr.idVentas=v.id) as totalref,
                        (SELECT sum(vhde.cantidad*vhde.precio) AS total FROM ventasd_has_detallesequipos AS vhde WHERE vhde.idVenta=v.id ) as totalequ,
                        (SELECT sum(vhdac.cantidad*vhdac.costo) AS total FROM ventasd_has_detallesequipos_accesorios AS vhdac WHERE vhdac.id_venta=v.id ) as totalacc
                        FROM ventasd as v
                        JOIN clientes as cli ON cli.id=v.idCliente
                        LEFT JOIN ventasd_factura as fac ON fac.ventaId=v.id
                        WHERE v.prefactura = 1
                        and v.activo=1
                        AND $whereejecutivo v.idCliente = $cliente
                        ORDER BY v.id DESC
                         LIMIT 80 ";
                $query=$this->db->query($select);

            $html='<table class="responsive-table display no-footer">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Cliente</th>
                                <th>Prefactura</th>
                                <th>Fecha</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                    ';
            foreach ($query->result() as $item) {
                if ($item->facturaId>0) {
                    $styledisplay='style="display:none"';
                }else{
                    $styledisplay='';
                    if($item->siniva==1){
                        $styledisplay='style="display:none"';
                    }
                }
                $totalventa=$item->totalcon+$item->totalref+$item->totalequ+$item->totalacc;
                if($totalventa==0){
                    $styledisplay='style="display:none"';
                }
                $html.='<tr '.$styledisplay.' class="totalventa_'.$totalventa.'">
                            <td>'.$item->id.'</td>
                            <td>'.$item->empresa.'</td>
                            <td>
                                <a class="btn-floating green tooltipped" data-position="top" data-delay="50" data-tooltip="Prefactura" onclick="detalle('.$item->id.',5)" data-tooltip-id="d660c2d9-e6e6-18a7-21f6-be86654bf354">
                                    <i class="material-icons">assignment</i>
                                </a>
                            </td>
                            <td>'.$item->reg.'</td>
                            <td>';
                            if ($item->facturaId>0) {
                                $html.='<a class="waves-effect red btn-bmz"  >Facturada</a>';
                            }else{
                                $html.='<button class="waves-effect green btn-bmz import_'.$item->id.'" onclick="importarproductos('.$item->id.',5)" >Importar</button>';
                            }
                        
                        $html.='</td>
                        </tr>';
            }
            $html.='</tbody></table>';
            echo $html;
        }
        */
    }
    function obtenerventasproductos($tipopg=0){
        $params = $this->input->post();
        $tipo =$params['tipo'];
        $venta = $params['venta'];
        $folio_venta='';$orden_comp='';
        //log_message('error', 'venta '.$venta.' tipo'.$tipo);
        $tipov=1;
        //$prefactura=$this->ModeloCatalogos->getselectwheren('prefactura',array('ventaId'=>$venta,'tipo'=>$tipo));
        //$prefacturadatos=$prefactura->row();
        //=============================================
            $option_unidad_ve_value = $this->ModeloCatalogos->obtenerunidadserviciovt(2,1,1);
            $option_unidad_ve_text = $this->ModeloCatalogos->obtenerunidadserviciovt(2,1,2);
            $option_concepto_ve_value = $this->ModeloCatalogos->obtenerunidadserviciovt(2,2,1);
            $option_concepto_ve_text = $this->ModeloCatalogos->obtenerunidadserviciovt(2,2,2);

            $option_unidad_vt_value = $this->ModeloCatalogos->obtenerunidadserviciovt(3,1,1);
            $option_unidad_vt_text = $this->ModeloCatalogos->obtenerunidadserviciovt(3,1,2);
            $option_concepto_vt_value = $this->ModeloCatalogos->obtenerunidadserviciovt(3,2,1);
            $option_concepto_vt_text = $this->ModeloCatalogos->obtenerunidadserviciovt(3,2,2);

            $option_unidad_vr_value = $this->ModeloCatalogos->obtenerunidadserviciovt(4,1,1);
            $option_unidad_vr_text = $this->ModeloCatalogos->obtenerunidadserviciovt(4,1,2);
            $option_concepto_vr_value = $this->ModeloCatalogos->obtenerunidadserviciovt(4,2,1);
            $option_concepto_vr_text = $this->ModeloCatalogos->obtenerunidadserviciovt(4,2,2);

            $option_unidad_va_value = $this->ModeloCatalogos->obtenerunidadserviciovt(5,1,1);
            $option_unidad_va_text = $this->ModeloCatalogos->obtenerunidadserviciovt(5,1,2);
            $option_concepto_va_value = $this->ModeloCatalogos->obtenerunidadserviciovt(5,2,1);
            $option_concepto_va_text = $this->ModeloCatalogos->obtenerunidadserviciovt(5,2,2);

            $option_unidad_se_value = $this->ModeloCatalogos->obtenerunidadserviciovt(6,1,1);
            $option_unidad_se_text = $this->ModeloCatalogos->obtenerunidadserviciovt(6,1,2);
            $option_concepto_se_value = $this->ModeloCatalogos->obtenerunidadserviciovt(6,2,1);
            $option_concepto_se_text = $this->ModeloCatalogos->obtenerunidadserviciovt(6,2,2);
        //=============================================
        $arrayproductos=array();
        if ($tipo==0) {
            $r_venta=$this->ModeloCatalogos->getselectwheren('ventas',array('id'=>$venta));
            $r_venta=$r_venta->row();
            $tipov=$r_venta->tipov;
            $folio_venta=$r_venta->folio_venta;
            if($r_venta->orden_comp==null){
                $orden_comp ='';
            }else{
                $orden_comp =$r_venta->orden_comp;    
            }
            

            if ($r_venta->folio_venta==null) {
                    $folio_venta='';
            }
            $prefactura=$this->ModeloCatalogos->getselectwheren('prefactura',array('ventaId'=>$venta,'tipo'=>$tipo));
            $prefacturadatos=$prefactura->row();
            $resultadosequipos=$this->ModeloCatalogos->getselectwheren('ventas_has_detallesEquipos',array('idVenta'=>$venta));
            foreach ($resultadosequipos->result() as $item) {
                $series= $this->ModeloCatalogos->seriesenventas($item->id);
                $arrayproductos[]=array(
                    'unidadsat'=>$option_unidad_ve_value,
                    'unidadsat_text'=>$option_unidad_ve_text,
                    'conceptosat'=>$option_concepto_ve_value,
                    'conceptosat_text'=>$option_concepto_ve_text,
                    'cantidad'=>$item->cantidad,
                    'producto'=>$item->modelo.' '.$series,
                    'preciou'=>$item->precio,
                    'iva'=>($item->cantidad*$item->precio)*0.16
                );
            }
            $resultadoaccesorios = $this->ModeloCatalogos->accesoriosventa($venta);
            foreach ($resultadoaccesorios->result() as $item) {
                $item->id_accesoriod;
                $series= $this->ModeloCatalogos->seriesenventasaccesorio($item->id_accesoriod);
                $arrayproductos[]=array(
                    'unidadsat'=>$option_unidad_va_value,
                    'unidadsat_text'=>$option_unidad_va_text,
                    'conceptosat'=>$option_concepto_va_value,
                    'conceptosat_text'=>$option_concepto_va_text,
                    'cantidad'=>$item->cantidad,
                    'producto'=>$item->nombre.' '.$series,
                    'preciou'=>$item->costo,
                    'iva'=>($item->cantidad*$item->costo)*0.16
                );
            }
            $resultadoconsumibles = $this->ModeloCatalogos->consumiblesventa($venta);
            foreach ($resultadoconsumibles->result() as $item) {
                $arrayproductos[]=array(
                    'unidadsat'=>$option_unidad_vt_value,
                    'unidadsat_text'=>$option_unidad_vt_text,
                    'conceptosat'=>$option_concepto_vt_value,
                    'conceptosat_text'=>$option_concepto_vt_text,
                    'cantidad'=>$item->cantidad,
                    'producto'=>$item->modelo,
                    'preciou'=>$item->costo_toner,
                    'iva'=>($item->cantidad*$item->costo_toner)*0.16
                );
            }
            $consumiblesventadetalles = $this->ModeloCatalogos->consumiblesventadetalles($venta);
            foreach ($consumiblesventadetalles->result() as $item) {
                $arrayproductos[]=array(
                    'unidadsat'=>$option_unidad_vt_value,
                    'unidadsat_text'=>$option_unidad_vt_text,
                    'conceptosat'=>$option_concepto_vt_value,
                    'conceptosat_text'=>$option_concepto_vt_text,
                    'cantidad'=>$item->cantidad,
                    'producto'=>$item->modelo,
                    'preciou'=>$item->costo_toner,
                    'iva'=>($item->cantidad*$item->costo_toner)*0.16
                );
            }
            $ventadetallesrefacion = $this->ModeloCatalogos->ventas_has_detallesRefacciones($venta);
            foreach ($ventadetallesrefacion->result() as $item) {
                
                $resuleserie=$this->ModeloCatalogos->getrefaccionserie($item->id);
                if($resuleserie->num_rows()>0){
                    $arrayseries=array();
                    foreach ($resuleserie->result() as $itemse) {
                        $arrayseries[]=$itemse->serie;
                    }
                    $series= implode(" / ", $arrayseries);
                }else{
                    $series='';
                }
                

                $arrayproductos[]=array(
                    'unidadsat'=>$option_unidad_vr_value,
                    'unidadsat_text'=>$option_unidad_vr_text,
                    'conceptosat'=>$option_concepto_vr_value,
                    'conceptosat_text'=>$option_concepto_vr_text,
                    'cantidad'=>$item->cantidad,
                    'producto'=>$item->modelo.' '.$series,
                    'preciou'=>$item->precioGeneral,
                    'iva'=>($item->cantidad*$item->precioGeneral)*0.16
                );
            }
        }
        if ($tipo==1) {
            $prefactura=$this->ModeloCatalogos->getselectwheren('prefactura',array('ventaId'=>$venta,'tipo'=>3));
            $prefacturadatos=$prefactura->row();
            $resultadov = $this->ModeloCatalogos->getselectwheren('ventacombinada',array('combinadaId'=>$venta));
            $id_personal=0;
            foreach ($resultadov->result() as $item) {
                $id_personal=$item->id_personal;
                $idCliente=$item->idCliente;
                $equipo=$item->equipo;
                $consumibles=$item->consumibles;
                $refacciones=$item->refacciones;
                $poliza=$item->poliza;
                $tipov=$item->tipov;
                $folio_venta=$item->folio_venta;
                if($item->orden_comp==null){
                    $orden_comp ='';
                }else{
                    $orden_comp =$item->orden_comp;    
                }
                if ($item->folio_venta==null) {
                    $folio_venta='';
                }
            }
            $resultadosequipos = $this->ModeloCatalogos->getselectwheren('ventas_has_detallesEquipos',array('idVenta'=>$equipo));
            $resultadoaccesorios = $this->ModeloCatalogos->accesoriosventa($equipo);
            $resultadoconsumibles = $this->ModeloCatalogos->consumiblesventa($equipo);
            $consumiblesventadetalles = $this->ModeloCatalogos->consumiblesventadetalles($consumibles);
            $polizaventadetalles = $this->ModeloCatalogos->polizaventadetalles3($poliza);
            $ventadetallesrefacion = $this->ModeloCatalogos->ventas_has_detallesRefacciones($refacciones);
            foreach ($resultadosequipos->result() as $item) {
                $series= $this->ModeloCatalogos->seriesenventas($item->id);
                $arrayproductos[]=array(
                    'unidadsat'=>$option_unidad_ve_value,
                    'unidadsat_text'=>$option_unidad_ve_text,
                    'conceptosat'=>$option_concepto_ve_value,
                    'conceptosat_text'=>$option_concepto_ve_text,
                    'cantidad'=>$item->cantidad,
                    'producto'=>$item->modelo.' '.$series,
                    'preciou'=>$item->precio,
                    'iva'=>($item->cantidad*$item->precio)*0.16
                );
            }
            foreach ($resultadoaccesorios->result() as $item) {
                $series= $this->ModeloCatalogos->seriesenventasaccesorio($item->id_accesoriod);
                $arrayproductos[]=array(
                    'unidadsat'=>$option_unidad_va_value,
                    'unidadsat_text'=>$option_unidad_va_text,
                    'conceptosat'=>$option_concepto_va_value,
                    'conceptosat_text'=>$option_concepto_va_text,
                    'cantidad'=>$item->cantidad,
                    'producto'=>$item->nombre.' '.$series,
                    'preciou'=>$item->costo,
                    'iva'=>($item->cantidad*$item->costo)*0.16
                );
            }
            foreach ($resultadoconsumibles->result() as $item) {
                $arrayproductos[]=array(
                    'unidadsat'=>$option_unidad_vt_value,
                    'unidadsat_text'=>$option_unidad_vt_text,
                    'conceptosat'=>$option_concepto_vt_value,
                    'conceptosat_text'=>$option_concepto_vt_text,
                    'cantidad'=>$item->cantidad,
                    'producto'=>$item->modelo,
                    'preciou'=>$item->costo_toner,
                    'iva'=>($item->cantidad*$item->costo_toner)*0.16
                );
            }
            foreach ($consumiblesventadetalles->result() as $item) {
                $arrayproductos[]=array(
                    'unidadsat'=>$option_unidad_vt_value,
                    'unidadsat_text'=>$option_unidad_vt_text,
                    'conceptosat'=>$option_concepto_vt_value,
                    'conceptosat_text'=>$option_concepto_vt_text,
                    'cantidad'=>$item->cantidad,
                    'producto'=>$item->modelo,
                    'preciou'=>$item->costo_toner,
                    'iva'=>($item->cantidad*$item->costo_toner)*0.16
                );
            }
            foreach ($ventadetallesrefacion->result() as $item) {
                $resuleserie=$this->ModeloCatalogos->getrefaccionserie($item->id);
                if($resuleserie->num_rows()>0){
                    $arrayseries=array();
                    foreach ($resuleserie->result() as $itemse) {
                        $arrayseries[]=$itemse->serie;
                    }
                    $series= implode(" / ", $arrayseries);
                }else{
                    $series='';
                }

                $arrayproductos[]=array(
                    'unidadsat'=>$option_unidad_vr_value,
                    'unidadsat_text'=>$option_unidad_vr_text,
                    'conceptosat'=>$option_concepto_vr_value,
                    'conceptosat_text'=>$option_concepto_vr_text,
                    'cantidad'=>$item->cantidad,
                    'producto'=>$item->modelo.' '.$series,
                    'preciou'=>$item->precioGeneral,
                    'iva'=>($item->cantidad*$item->precioGeneral)*0.16
                );
            }
            foreach ($polizaventadetalles->result() as $item) {
                $preciogeneral=0;
                if ($item->precio_local>0) {
                    $preciogeneral=$item->precio_local;
                }
                if ($item->precio_semi>0) {
                    $preciogeneral=$item->precio_semi;
                }
                if ($item->precio_foraneo>0) {
                    $preciogeneral=$item->precio_foraneo;
                }
                if ($item->precio_especial>0) {
                    $preciogeneral=$item->precio_especial;
                }
                if($preciogeneral>0){
                    $arrayproductos[]=array(
                        'unidadsat'=>$option_unidad_se_value,
                        'unidadsat_text'=>$option_unidad_se_text,
                        'conceptosat'=>$option_concepto_se_value,
                        'conceptosat_text'=>$option_concepto_se_text,
                        'cantidad'=>$item->cantidad,
                        'producto'=>$item->nombre,
                        'preciou'=>$preciogeneral,
                        'iva'=>($item->cantidad*$preciogeneral)*0.16
                    );
                }
            }
        }
        if ($tipo==2) {
            $r_venta=$this->ModeloCatalogos->getselectwheren('polizasCreadas',array('id'=>$venta));
            $r_venta=$r_venta->row();
            $tipov=$r_venta->tipov;
            if($r_venta->orden_comp==null){
                $orden_comp ='';
            }else{
                $orden_comp =$r_venta->orden_comp;    
            }

            $prefactura=$this->ModeloCatalogos->getselectwheren('prefactura',array('ventaId'=>$venta,'tipo'=>1));
            $prefacturadatos=$prefactura->row();
            $polizaventadetalles = $this->ModeloCatalogos->polizaventadetalles3($venta);
            foreach ($polizaventadetalles->result() as $item) {
                $preciogeneral=0;
                if ($item->precio_local>0) {
                    $preciogeneral=$item->precio_local;         
                }
                if ($item->precio_semi>0) {
                    $preciogeneral=$item->precio_semi;     
                }
                if ($item->precio_foraneo>0) {
                    $preciogeneral=$item->precio_foraneo;
                }
                if ($item->precio_especial>0) {
                    $preciogeneral=$item->precio_especial;
                }
                if($preciogeneral>0){
                    $arrayproductos[]=array(
                        'unidadsat'=>$option_unidad_se_value,
                        'unidadsat_text'=>$option_unidad_se_text,
                        'conceptosat'=>$option_concepto_se_value,
                        'conceptosat_text'=>$option_concepto_se_text,
                        'cantidad'=>$item->cantidad,
                        'producto'=>$item->nombre.' '.$item->modelo.' '.$item->modeloe,
                        'preciou'=>$preciogeneral,
                        'iva'=>($item->cantidad*$preciogeneral)*0.16
                    );
                }
            }
        }
        if ($tipo==3) {
            # code...
        }
        /*
        if ($tipo==5) {
            $prefactura=$this->ModeloCatalogos->getselectwheren('ventasd_prefactura',array('ventaId'=>$venta));
            $prefacturadatos=$prefactura->row();
            $resultadosequipos=$this->ModeloCatalogos->getselectwheren('ventasd_has_detallesequipos',array('idVenta'=>$venta));
            foreach ($resultadosequipos->result() as $item) {
                $series= $this->ModeloCatalogos->seriesenventas_vd($item->id);
                $arrayproductos[]=array(
                    'unidadsat'=>$option_unidad_ve_value,
                    'unidadsat_text'=>$option_unidad_ve_text,
                    'conceptosat'=>$option_concepto_ve_value,
                    'conceptosat_text'=>$option_concepto_ve_text,
                    'cantidad'=>$item->cantidad,
                    'producto'=>$item->modelo.' '.$series,
                    'preciou'=>$item->precio,
                    'iva'=>($item->cantidad*$item->precio)*0.16
                );
            }
            $resultadoaccesorios = $this->ModeloCatalogos->accesoriosventasd($venta);
            foreach ($resultadoaccesorios->result() as $item) {
                $item->id_accesoriod;
                $series= $this->ModeloCatalogos->seriesenventasaccesorio_vd($item->id_accesoriod);
                $arrayproductos[]=array(
                    'unidadsat'=>$option_unidad_va_value,
                    'unidadsat_text'=>$option_unidad_va_text,
                    'conceptosat'=>$option_concepto_va_value,
                    'conceptosat_text'=>$option_concepto_va_text,
                    'cantidad'=>$item->cantidad,
                    'producto'=>$item->nombre.' '.$series,
                    'preciou'=>$item->costo,
                    'iva'=>($item->cantidad*$item->costo)*0.16
                );
            }
            $resultadoconsumibles = $this->ModeloCatalogos->consumiblesventasd($venta);
            foreach ($resultadoconsumibles->result() as $item) {
                $arrayproductos[]=array(
                    'unidadsat'=>$option_unidad_vt_value,
                    'unidadsat_text'=>$option_unidad_vt_text,
                    'conceptosat'=>$option_concepto_vt_value,
                    'conceptosat_text'=>$option_concepto_vt_text,
                    'cantidad'=>$item->cantidad,
                    'producto'=>$item->modelo,
                    'preciou'=>$item->costo_toner,
                    'iva'=>($item->cantidad*$item->costo_toner)*0.16
                );
            }
            $consumiblesventadetalles = $this->ModeloCatalogos->consumiblesventasddetalles($venta);
            foreach ($consumiblesventadetalles->result() as $item) {
                $arrayproductos[]=array(
                    'unidadsat'=>$option_unidad_vt_value,
                    'unidadsat_text'=>$option_unidad_vt_text,
                    'conceptosat'=>$option_concepto_vt_value,
                    'conceptosat_text'=>$option_concepto_vt_text,
                    'cantidad'=>$item->cantidad,
                    'producto'=>$item->modelo,
                    'preciou'=>$item->costo_toner,
                    'iva'=>($item->cantidad*$item->costo_toner)*0.16
                );
            }
            $ventadetallesrefacion = $this->ModeloCatalogos->ventasd_has_detallesrefacciones($venta);
            foreach ($ventadetallesrefacion->result() as $item) {
                
                $resuleserie=$this->ModeloCatalogos->getrefaccionserie_vd($item->id);
                if($resuleserie->num_rows()>0){
                    $arrayseries=array();
                    foreach ($resuleserie->result() as $itemse) {
                        $arrayseries[]=$itemse->serie;
                    }
                    $series= implode(" / ", $arrayseries);
                }else{
                    $series='';
                }
                

                $arrayproductos[]=array(
                    'unidadsat'=>$option_unidad_vr_value,
                    'unidadsat_text'=>$option_unidad_vr_text,
                    'conceptosat'=>$option_concepto_vr_value,
                    'conceptosat_text'=>$option_concepto_vr_text,
                    'cantidad'=>$item->cantidad,
                    'producto'=>$item->modelo.' '.$series,
                    'preciou'=>$item->precioGeneral,
                    'iva'=>($item->cantidad*$item->precioGeneral)*0.16
                );
            }
        }
        */
        if($tipopg==1){
            $cantidad_pg=0;
            $preciou_pg=0;
            foreach ($arrayproductos as $itempg) {
                $cantidad_pg=$cantidad_pg+$itempg['cantidad'];
                $preciou_pg=$preciou_pg+$itempg['preciou'];
            }
            $totalpg=round($cantidad_pg*$preciou_pg,2);
            $totaliva=round($totalpg*0.16,2);
            $arrayproductos=array();
            $arrayproductos[]=array(
                    'unidadsat'=>'ACT',
                    'unidadsat_text'=>'ACT / Actividad',
                    'conceptosat'=>'01010101',
                    'conceptosat_text'=>'No existe en el catalogo',
                    'cantidad'=>1,
                    'producto'=>'Venta',
                    'preciou'=>$totalpg,
                    'iva'=>$totaliva
                );
        }
        $fac_pend=array();
        if($tipo<=2){
            if($tipo<2){
                $strq="SELECT fv.*,fac.FacturasId,fac.serie,fac.Folio,fac.Estado
                    FROM factura_venta as fv
                    INNER JOIN f_facturas as fac on fac.FacturasId=fv.facturaId  
                    WHERE fv.ventaId='$venta' AND fv.combinada='$tipo'";
            }else{
                $strq="SELECT fp.*, fac.FacturasId,fac.serie,fac.Folio,fac.Estado 
                        FROM factura_poliza as fp 
                        INNER JOIN f_facturas as fac on fac.FacturasId=fp.facturaId
                        WHERE fp.polizaId='$venta'
                        ";
            }
            $query=$this->db->query($strq);
            foreach ($query->result() as $item) {
                if($item->Estado==2){
                    $fac_pend[]=array('facid'=>$item->FacturasId,'serie'=>$item->serie,'folio'=>$item->Folio,'estado'=>$item->Estado);
                }
            }
        }

        echo json_encode(array('folio_venta'=>$folio_venta,'tipov'=>$tipov,'arrayproductos'=>$arrayproductos,'prefacturadatos'=>$prefacturadatos,'fac_pend'=>$fac_pend,'orden_comp'=>$orden_comp));
    }
    function obtenerventasproductoss(){
        $params = $this->input->post();
        $tipo =$params['tipo'];
        $venta = $params['venta'];
        //log_message('error', 'venta '.$venta.' tipo'.$tipo);
        $option_unidad_vt_value = $this->ModeloCatalogos->obtenerunidadserviciovt(3,1,1);
        $option_unidad_vt_text = $this->ModeloCatalogos->obtenerunidadserviciovt(3,1,2);
        $option_concepto_vt_value = $this->ModeloCatalogos->obtenerunidadserviciovt(3,2,1);
        $option_concepto_vt_text = $this->ModeloCatalogos->obtenerunidadserviciovt(3,2,2);

        $option_unidad_vr_value = $this->ModeloCatalogos->obtenerunidadserviciovt(4,1,1);
        $option_unidad_vr_text = $this->ModeloCatalogos->obtenerunidadserviciovt(4,1,2);
        $option_concepto_vr_value = $this->ModeloCatalogos->obtenerunidadserviciovt(4,2,1);
        $option_concepto_vr_text = $this->ModeloCatalogos->obtenerunidadserviciovt(4,2,2);

        $option_unidad_va_value = $this->ModeloCatalogos->obtenerunidadserviciovt(5,1,1);
        $option_unidad_va_text = $this->ModeloCatalogos->obtenerunidadserviciovt(5,1,2);
        $option_concepto_va_value = $this->ModeloCatalogos->obtenerunidadserviciovt(5,2,1);
        $option_concepto_va_text = $this->ModeloCatalogos->obtenerunidadserviciovt(5,2,2);

        $option_unidad_se_value = $this->ModeloCatalogos->obtenerunidadserviciovt(6,1,1);
        $option_unidad_se_text = $this->ModeloCatalogos->obtenerunidadserviciovt(6,1,2);
        $option_concepto_se_value = $this->ModeloCatalogos->obtenerunidadserviciovt(6,2,1);
        $option_concepto_se_text = $this->ModeloCatalogos->obtenerunidadserviciovt(6,2,2);
        
        $arrayproductos=array();
        if ($tipo==3) {
            $querysql1 ="SELECT asigd.serie,asigd.direccion,asigd.ttipo,sere.nombre,sere.local,sere.semi,sere.foraneo,sere.especial
                        from asignacion_ser_cliente_a_d as asigd
                        inner join servicio_evento as sere on sere.id=asigd.tservicio
                        WHERE asigd.asignacionId=$venta";
            $query1 = $this->db->query($querysql1);


            foreach ($query1->result() as $item) {
                $precio=0;
                if ($item->ttipo==1) {
                    $precio=$item->local;
                }
                if ($item->ttipo==2) {
                    $precio=$item->semi;
                }
                if ($item->ttipo==3) {
                    $precio=$item->foraneo;
                }
                if ($item->ttipo==4) {
                    $precio=$item->especial;
                }
                $arrayproductos[]=array(
                    'unidadsat'=>$option_unidad_se_value,
                    'unidadsat_text'=>$option_unidad_se_text,
                    'conceptosat'=>$option_concepto_se_value,
                    'conceptosat_text'=>$option_concepto_se_text,
                    'cantidad'=>1,
                    'producto'=>$item->serie.' '.$item->direccion.' '.$item->nombre,
                    'preciou'=>$precio,
                    'iva'=>($precio)*0.16
                );
            }
            
        }
        if ($tipo==1) {
            $querysql1 ="SELECT rene.modelo,sepro.serie,asig.ttipo,asig.tserviciomotivo,sere.local,sere.semi,sere.foraneo,sere.especial
                    FROM asignacion_ser_contrato_a_e as asigd
                    inner join rentas_has_detallesequipos as rene on rene.id=asigd.idequipo
                    inner join series_productos as sepro on sepro.serieId=asigd.serieId
                    inner join asignacion_ser_contrato_a as asig on asig.asignacionId=asigd.asignacionId
                    inner join servicio_evento as sere on sere.id=asig.tservicio
                    WHERE asig.asignacionId=$venta";
            $query1 = $this->db->query($querysql1);


            foreach ($query1->result() as $item) {
                $precio=0;
                if ($item->ttipo==1) {
                    $precio=$item->local;
                }
                if ($item->ttipo==2) {
                    $precio=$item->semi;
                }
                if ($item->ttipo==3) {
                    $precio=$item->foraneo;
                }
                if ($item->ttipo==4) {
                    $precio=$item->especial;
                }
                $arrayproductos[]=array(
                    'unidadsat'=>$option_unidad_se_value,
                    'unidadsat_text'=>$option_unidad_se_text,
                    'conceptosat'=>$option_concepto_se_value,
                    'conceptosat_text'=>$option_concepto_se_text,
                    'cantidad'=>1,
                    'producto'=>$item->modelo.' '.$item->serie.' '.$item->tserviciomotivo,
                    'preciou'=>$precio,
                    'iva'=>($precio)*0.16
                );
            }
        }
        if ($tipo==2) {
            $querysql1 ="SELECT equ.modelo,asigd.serie,assp.ttipo,assp.tserviciomotivo,asigd.asignacionId,sere.local,sere.semi,sere.foraneo,sere.especial
                        FROM asignacion_ser_poliza_a_e as asigd
                        inner join polizascreadas_has_detallespoliza as pold on pold.id=asigd.idequipo
                        inner join equipos as equ on equ.id=pold.idEquipo
                        inner join asignacion_ser_poliza_a as assp on assp.asignacionId=asigd.asignacionId
                        inner join servicio_evento as sere on sere.id=assp.tservicio
                        WHERE asigd.asignacionId=$venta";
            $query1 = $this->db->query($querysql1);


            foreach ($query1->result() as $item) {
                $precio=0;
                if ($item->ttipo==1) {
                    $precio=$item->local;
                }
                if ($item->ttipo==2) {
                    $precio=$item->semi;
                }
                if ($item->ttipo==3) {
                    $precio=$item->foraneo;
                }
                if ($item->ttipo==4) {
                    $precio=$item->especial;
                }
                $arrayproductos[]=array(
                    'unidadsat'=>$option_unidad_se_value,
                    'unidadsat_text'=>$option_unidad_se_text,
                    'conceptosat'=>$option_concepto_se_value,
                    'conceptosat_text'=>$option_concepto_se_text,
                    'cantidad'=>1,
                    'producto'=>$item->modelo.' '.$item->serie.' '.$item->tserviciomotivo,
                    'preciou'=>$precio,
                    'iva'=>($precio)*0.16
                );
            }
        }
        //==================================================================
            $resultadoconsumibles=$this->Ventas_model->datosserviciosconsumibles($venta,$tipo);
            $resultadorefacciones=$this->Ventas_model->datosserviciosrefacciones($venta,$tipo);
            $resultadoaccesorios=$this->Ventas_model->datosserviciosaccesorios($venta,$tipo);
            foreach ($resultadoconsumibles->result() as $item) {
                $arrayproductos[]=array(
                    'unidadsat'=>$option_unidad_vt_value,
                    'unidadsat_text'=>$option_unidad_vt_text,
                    'conceptosat'=>$option_concepto_vt_value,
                    'conceptosat_text'=>$option_concepto_vt_text,
                    'cantidad'=>$item->cantidad,
                    'producto'=>$item->modelo,
                    'preciou'=>$item->costo,
                    'iva'=>($item->costo)*0.16
                );
            }
            foreach ($resultadorefacciones->result() as $item) {
                $arrayproductos[]=array(
                    'unidadsat'=>$option_unidad_vr_value,
                    'unidadsat_text'=>$option_unidad_vr_text,
                    'conceptosat'=>$option_concepto_vr_value,
                    'conceptosat_text'=>$option_concepto_vr_text,

                    'cantidad'=>$item->cantidad,
                    'producto'=>$item->nombre,
                    'preciou'=>$item->costo,
                    'iva'=>($item->costo)*0.16
                );
            }
            foreach ($resultadoaccesorios->result() as $item) {

                $arrayproductos[]=array(
                    'unidadsat'=>$option_unidad_va_value,
                    'unidadsat_text'=>$option_unidad_va_text,
                    'conceptosat'=>$option_concepto_va_value,
                    'conceptosat_text'=>$option_concepto_va_text,
                    'cantidad'=>$item->cantidad,
                    'producto'=>$item->nombre,
                    'preciou'=>$item->costo,
                    'iva'=>($item->costo)*0.16
                );
            }
        //==================================================================

        echo json_encode($arrayproductos);
    }
    function complemento($idfactura){
        $data['cfdi']=$this->ModeloGeneral->genSelect('f_uso_cfdi'); 
        $data['metodo']=$this->ModeloGeneral->genSelect('f_metodopago'); 
        $data['forma']=$this->ModeloGeneral->genSelect('f_formapago'); 
        $idconfi=1;
        //======================================================================
            $datosfactura=$this->ModeloCatalogos->getselectwheren('f_facturas',array('FacturasId'=>$idfactura));
            $datosfactura=$datosfactura->result();
            $datosfactura=$datosfactura[0];
            if($datosfactura->serie=='D'){
                $idconfi=2;
            }
            $data['serie']=$datosfactura->serie;
        //======================================================================
        $datosconfiguracion=$this->ModeloCatalogos->getselectwheren('f_configuraciones',array('ConfiguracionesId'=>$idconfi));
        $datosconfiguracion=$datosconfiguracion->result();
        $datosconfiguracion=$datosconfiguracion[0];
        $data['rFCEmisor']=$datosconfiguracion->Rfc;
        $data['Nombre']=$datosconfiguracion->Nombre;
        $data['Regimen']=$datosconfiguracion->Regimen;
        $data['LugarExpedicion']=$datosconfiguracion->CodigoPostal;

        

        $direccion=$this->ModeloCatalogos->db14_getselectwheren('cliente_has_datos_fiscales',array('rfc'=>$datosfactura->Rfc,'idCliente'=>$datosfactura->Clientes_ClientesId,'activo'=>1));
        foreach ($direccion->result() as $item) {
            $razon_social = $item->razon_social;
            $cp = $item->cp;
            
            $RegimenFiscalReceptor=$item->RegimenFiscalReceptor;
        }
        $data['razonsocialreceptor']=$razon_social;
        $data['rfcreceptor']=$datosfactura->Rfc;
        $data['Fecha']=date('Y-m-d').'T'.date('H:i:s');
        $data['uuid']=$datosfactura->uuid;
        $data['Folio']=$datosfactura->Folio;
        $data['FacturasId']=$datosfactura->FacturasId;
        $data['Fechafa']=date('Y-m-d',strtotime($datosfactura->fechatimbre)).'T'.date('H:i:s',strtotime($datosfactura->fechatimbre));
        $data['formapago']=$datosfactura->MetodoPago;
        
        $saldo=$this->ModeloComplementos->saldocomplemento($idfactura);
        $datoscopnum=$this->ModeloComplementos->saldocomplementonum($idfactura);

        $data['copnum']=$datoscopnum+1;
        //log_message('error', 'validar saldo: '.$datosfactura->total.'-'.$saldo);
        $data['saldoanterior']=$datosfactura->total-$saldo;
        $data['cliente']=$datosfactura->Clientes_ClientesId;
        $data['MetodoDePagoDR']=$datosfactura->FormaPago;


        $this->load->view('header');
        $this->load->view('main');
        $this->load->view('facturaciones/complementodd',$data);
        $this->load->view('footer');
        $this->load->view('facturaciones/complementojs');
    }
    function listasdecomplementos(){
        $factura=$this->input->post('facturas');
        //$resultado=$this->ModeloCatalogos->getselectwheren('f_complementopago',array('FacturasId'=>$factura,'Estado'=>1));
        $resultado=$this->ModeloCatalogos->complementofacturas($factura);
        $html='<table class="responsive-table display no-footer dataTable" id="tableliscomplementos">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Fecha</th>
                        <th>Numero de parcialidad</th>
                        <th>Monto</th>
                        <th>Archivo</th>
                    </tr>
                </thead>
                <tbody>';
        foreach ($resultado->result() as $item) {
            $html.='<tr>
                        <td>'.$item->complementoId.'</td>
                        <td>'.$item->FechaPago.'</td>
                        <td>'.$item->NumParcialidad.'</td>
                        <td>'.$item->ImpPagado.'</td>
                        <td>
                            <a class="b-btn b-btn-primary tooltipped" href="'.base_url().$item->rutaXml.'" target="_blank" data-position="top" data-delay="50" data-tooltip="XML" download="" data-tooltip-id="54ee1647-5011-f5dd-45f2-7aa729169609"><i class="far fa-file-code fa-2x"></i></a>
                            <a class="b-btn b-btn-primary tooltipped" href="'.base_url().'Facturaslis/complementodoc/'.$item->complementoId.'" target="_blank" data-position="top" data-delay="50" data-tooltip="XML"  data-tooltip-id="54ee1647-5011-f5dd-45f2-7aa729169609">
                                <i class="fas fa-file-pdf fa-2x"></i>
                                </a>
                        </td>
                    </tr>';
        }
        $html.='</tbody></table>';
        echo $html;
    }
    function complementodoc($id){
        $resultscp=$this->ModeloCatalogos->getselectwheren('f_complementopago',array('complementoId'=>$id));
        $resultscp=$resultscp->row();
        //===================================
            $idconfig=1;
            if($resultscp->Serie=='D'){
                $idconfig=2;
            }
        //===================================
        $datosconfiguracion=$this->ModeloCatalogos->getselectwheren('f_configuraciones',array('ConfiguracionesId'=>$idconfig));
        $datosconfiguracion=$datosconfiguracion->row();
        $data['confif']=$datosconfiguracion;
        $data['rfcemisor']=$datosconfiguracion->Rfc;
        $data['nombreemisor']=$datosconfiguracion->Nombre;
        $data['regimenf']='601 General de Ley Personas Morales';
        //==========================================
            $data['configuracionCotizacion'] = $this->ConfiguracionDocumentos_Model->getConfiguracion($idconfig); 

        //==========================================
            $data['idcomplemento']=$id;
            
            $data['resultscpd']=$this->ModeloCatalogos->getselectwheren('f_complementopago_documento',array('complementoId'=>$id));
            
            $FacturasId=$resultscp->FacturasId;
            $data['uuid']=$resultscp->uuid;
            $data['nocertificadosat']=$resultscp->nocertificadosat;
            $data['LugarExpedicion']=$resultscp->LugarExpedicion;
            $data['fechatimbre']=$resultscp->fechatimbre; 
            $data['fechapago']=$resultscp->FechaPago;
            $data['monto']=$resultscp->Monto;
            $data['IdDocumento']=$resultscp->IdDocumento;
            //$data['Folio']=$resultscp->Folio;
            $data['ImpSaldoAnt']=$resultscp->ImpSaldoAnt;
            $data['NumParcialidad']=$resultscp->NumParcialidad;
            $data['ImpSaldoInsoluto']=$resultscp->ImpSaldoInsoluto;
            $data['Sello']=$resultscp->Sello;
            $data['sellosat']=$resultscp->sellosat;
            $data['cadenaoriginal']=$resultscp->cadenaoriginal;
            $data['nocertificadosat']=$resultscp->nocertificadosat;
            $data['rfcreceptor']=$resultscp->R_rfc;
            $data['nombrereceptorr']=$resultscp->R_nombre;
            $data['NumOperacion']=$resultscp->NumOperacion;
            $docrelac = $this->Modelofacturas->documentorelacionado($id);
            $data['docrelac']=$docrelac->result();
        //====================================================================
            $resultsfp=$this->ModeloCatalogos->getselectwheren('f_formapago',array('id'=>$resultscp->FormaDePagoP));
            $resultsfp=$resultsfp->row();
            $data['formapago']=$resultsfp->formapago_text;
        //==========================================
            //$resultsfc=$this->ModeloCatalogos->getselectwheren('f_facturas',array('FacturasId'=>$FacturasId));
            //$resultsfc=$resultsfc->row();
            /*
            if ($resultsfc->moneda=='pesos') {
                $data['moneda']='Peso Mexicano';
            }else{
                $data['moneda']='Dolar';
            }
            */
        //====================================================================
            //$resultsmp=$this->ModeloCatalogos->getselectwheren('f_metodopago',array('metodopago'=>$resultsfc->FormaPago));
            //$resultsmp=$resultsmp->row();
            //$data['metodopago']=$resultsmp->metodopago_text;
        //==========================================
        $this->load->view('Reportes/complemento',$data);
    }
    function obtenerfacturas(){
        $datos = $this->input->post();
        $cli=$datos['cli'];
        $fact=$datos['fact'];
        $serie=$datos['serie'];
        $rowsfacturas=$this->ModeloCatalogos->getselectwheren('f_facturas',array('Estado'=>1,'Clientes_ClientesId'=>$cli,'Folio !='=>$fact,'FormaPago'=>'PPD','serie'=>$serie));
        $html='<table id="facturascli" class="responsive-table display no-footer dataTable">
                <thead>
                    <tr>
                        <th>Folio</th>
                        <th>IdDocumento(uuid)</th>
                        <th>Monto</th>
                        <th>Fecha Timbre</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
        ';
        foreach ($rowsfacturas->result() as $item) {
            $html.='<tr>
                        <td>'.$item->Folio.'</td>
                        <td class="facturasuuid">'.$item->uuid.'</td>
                        <td>'.$item->total.'</td>
                        <td>'.$item->fechatimbre.'</td>
                        <td>
                            <a class="btn-bmz waves-effect waves-light cyan" 
                                onclick="adddoc('.$item->FacturasId.')"
                                >Agregar</a></td>
                    </tr>';
        }
        $html.='</tbody></table>';
        echo $html;
    }
    function documentoadd(){
        $datos = $this->input->post();
        $idfactura=$datos['factura'];
        $datosfactura=$this->ModeloCatalogos->getselectwheren('f_facturas',array('FacturasId'=>$idfactura));
        $datosfactura=$datosfactura->result();
        $datosfactura=$datosfactura[0];

        //$datoscop=$this->ModeloCatalogos->getselectwheren('f_complementopago_documento',array('FacturasId'=>$idfactura,'Estado'=>1));
        $datoscop=$this->ModeloCatalogos->consultarcomplementosfactura($idfactura);
        $datoscopnum=$datoscop->num_rows();
        $saldo=0;
        foreach ($datoscop->result() as $item) {
            $saldo=$saldo+$item->ImpPagado;
        }
        $NumParcialidad=$datoscopnum+1;
        $array = array(
            'idfactura'=>$idfactura,
            'MetodoDePagoDR'=>$datosfactura->FormaPago,
            'IdDocumento'=>$datosfactura->uuid,
            'NumParcialidad'=>$NumParcialidad,
            'ImpSaldoAnt'=>$datosfactura->total-$saldo
        );
        echo json_encode($array);
    }
    public function getlistpendientesv() {
        $params = $this->input->post();
        $getdata = $this->Modelofacturas->getlistpendientesv($params);
        $totaldata= $this->Modelofacturas->getlistpendientesvt($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    public function getlistpendientesvc() {
        $params = $this->input->post();
        $getdata = $this->Modelofacturas->getlistpendientesvc($params);
        $totaldata= $this->Modelofacturas->getlistpendientesvct($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    public function getlistpendientesp() {
        $params = $this->input->post();
        $getdata = $this->Modelofacturas->getlistpendientesp($params);
        $totaldata= $this->Modelofacturas->getlistpendientespt($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    public function getlistpendientess() {
        $params = $this->input->post();
        $getdata = $this->Modelofacturas->getlistpendientess($params);
        $totaldata= $this->Modelofacturas->getlistpendientesst($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    public function getlistpendientesr() {
        $params = $this->input->post();
        $getdata = $this->Modelofacturas->getlistpendientesr($params);
        $totaldata= $this->Modelofacturas->getlistpendientesrt($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    public function getlistcomplementos() {
        $params = $this->input->post();
        $getdata = $this->ModeloComplementos->getcomplementos($params);
        $totaldata= $this->ModeloComplementos->total_complementos($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    function obtenerdatosfactura(){
        $params = $this->input->post();
        $idfactura = $params['factura'];
        $datosfactura=$this->ModeloCatalogos->getselectwheren('f_facturas',array('FacturasId'=>$idfactura));
        $datosfacturad=$this->ModeloCatalogos->getselectwheren('f_facturas_servicios',array('FacturasId'=>$idfactura));
        $array = array(
            'facturas'=>$datosfactura->result(),
            'facturasd'=>$datosfacturad->result()
        );
        echo json_encode($array);
    }
    function exportar($fechaini,$fechafin){
        //http://localhost/kyocera2/index.php/Facturaslis/exportar/2022-02-01/2022-02-28
        $params['personal']=0;
        $params['cliente']=0;
        $params['finicio']=$fechaini;
        $params['ffin']=$fechafin;
        $params['order'][0]['column']=1;
        $params['order'][0]['dir']='desc';
        $params['length']=500;
        $params['start']=0;
        $getdata = $this->Modelofacturas->getfacturas($params);
        echo json_encode($getdata->result());
    }
    function viewdatosfiscalesinfo(){
        $params = $this->input->post();
        $cliente = $params['cliente'];
        $rfc = $params['rfc'];
        
        $razon_social='';
        $cp='';
        $RegimenFiscalReceptor=0;
        $direccion=$this->ModeloCatalogos->db14_getselectwheren('cliente_has_datos_fiscales',array('rfc'=>$rfc,'idCliente'=>$cliente,'activo'=>1));
        foreach ($direccion->result() as $item) {
            $razon_social = $item->razon_social;
            $rfc = $item->rfc;
            $cp = $item->cp;
            $num_ext = $item->num_ext;
            $num_int = $item->num_int;
            $colonia = $item->colonia;
            $calle = $item->calle;
            if ($item->estado!=null) {
              $estado=$item->estado;
            }
            if ($item->municipio!=null) {
              $municipio=$item->municipio;
            }
            $localidad=$item->localidad;
            $RegimenFiscalReceptor=$item->RegimenFiscalReceptor;
        }
        $razon_social=str_replace(array(' '), array('-'), $razon_social);
        $razon_social='<span title="los guiones representan los espacios">'.$razon_social.'</span>';
        $datos=array(
                    'nombrefiscal'=>$razon_social,
                    'cp'=>$cp,
                    'regimennumber'=>$RegimenFiscalReceptor,
                    'regimen'=>$this->Modelofacturas->regimenf($RegimenFiscalReceptor),
                    'regimenclave'=>$RegimenFiscalReceptor
                    );
        echo json_encode($datos);
    }
    public function table_get_pagos_servicios(){ 
        $id = $this->input->post('id'); 
        $html='<table class="responsive-table display striped" cellspacing="0"> 
                    <thead> 
                      <tr> 
                        <th>Fecha</th> 
                        <th>Método de pago</th> 
                        <th>Monto</th>
                        <th>Comentario</th> 
                        <!--<th>Comprobante</th>-->
                        <th></th> 
                      </tr> 
                    </thead> 
                    <tbody>'; 
        
           
            $strq = "SELECT p.idpago,p.fecha,f.formapago_text,p.pago,p.observaciones
                FROM pagos_factura as p
                inner JOIN f_formapago as f on f.id=p.idmetodo
                WHERE p.factura=$id and p.activo=1"; 

            $query = $this->db->query($strq);
            foreach ($query->result() as $item) { 
            $html.='<tr class="remove_table_pagos_ser_'.$item->idpago.'"> 
                     <td>'.$item->fecha.'</td> 
                     <td>'.$item->formapago_text.'</td> 
                     <td class="montoagregados">'.$item->pago.'</td> 
                     <td>'.$item->observaciones.'</td> 
                     <td>
                        <span 
                            class="new badge red" 
                            style="cursor: pointer;" 
                            onclick="deletepagoser('.$item->idpago.')">
                            <i class="material-icons">delete</i>
                        </span>
                    </td> 
                    </tr>'; 
            } 
        
        $html.='</tbody> 
                  </table>'; 
        echo $html; 
    } 
    function guardar_pago_factura(){
        $params = $this->input->post();
        $data['factura']=$params['idservicio'];
        $data['pago']=$params['pago'];
        $data['fecha']=$params['fecha'];
        $data['observaciones']=$params['observacion'];
        $data['idmetodo']=$params['idmetodo'];
        $data['idpersonal']=$this->idpersonal;
        $this->ModeloCatalogos->Insert('pagos_factura',$data);
    }
    function deletepagofac(){
        $params = $this->input->post();
        $this->ModeloCatalogos->updateCatalogo('pagos_factura',array('activo'=>0),array('idpago'=>$params['idpago']));
    }
    function actualizaestatuspagado(){
        $params = $this->input->post();
        $facturaid=$params['factura'];
        $this->ModeloCatalogos->updateCatalogo('f_facturas',array('pagada'=>1),array('FacturasId'=>$facturaid));
    }
    function obtenerrfc2(){
        $id = $this->input->post('id');
        $results=$this->ModeloCatalogos->getselectwheren('cliente_has_datos_fiscales',array('idCliente'=>$id,'activo'=>1));
        //echo $results;
        echo json_encode($results->result());
    }
    function obtenerrfc3(){
        $pro = $this->input->get('search');

        $strq = "SELECT * from cliente_has_datos_fiscales WHERE activo=1 and (razon_social like '%".$pro."%' or rfc like '%".$pro."%') group by rfc,razon_social ";
        $query = $this->db->query($strq);

        echo json_encode($query->result());
    }
    function obtenerpg(){
        $params=$this->input->post();
        $faci = $params['faci'];
        $facf = $params['facf'];
        $ejecutivo = $params['per'];
        $empresa = $params['empresa'];
        $html='<div class="col s9"></div><div class="col s3"><button class="waves-effect green btn-bmz" onclick="import_pg()" >Importar Todos</button></div>';
        $html.='<table class="responsive-table display no-footer table_pg"><thead><tr><th>#</th><th>Cliente</th><th></th><th>Prefactura</th><th>Fecha</th><th></th></tr></thead><tbody>';
        //===========================================================================================
            if ($ejecutivo>0) {
                $whereejecutivo="and  v.id_personal=$ejecutivo ";
                $whereejecutivo2="and  vc.id_personal=$ejecutivo ";
            }else{
                $whereejecutivo="";
                $whereejecutivo2="";
            }
            if($faci!=''){
                $fechaiwhere=" and v.reg>='".$faci." 00:00:00' ";
                $fechaiwhere2=" and vc.reg>='".$faci." 00:00:00' ";
            }else{
                $fechaiwhere="";
                $fechaiwhere2="";
            }
            if($facf!=''){
                $fechafwhere=" and v.reg<='".$facf." 23:59:59' ";
                $fechafwhere2=" and vc.reg<='".$facf." 23:59:59' ";
            }else{
                $fechafwhere="";
                $fechafwhere2="";
            }
               
                $select="SELECT v.estatus, v.id, v.prefactura, v.telefono, v.correo, cli.empresa, v.reg, facf.FacturasId,v.siniva,v.tipov,
                         (SELECT sum(vhdc.piezas*vhdc.precioGeneral) as total FROM ventas_has_detallesConsumibles as vhdc WHERE vhdc.idVentas=v.id) as totalcon, 
                        (SELECT sum(vhdr.piezas*vhdr.precioGeneral) as total FROM ventas_has_detallesRefacciones as vhdr WHERE vhdr.idVentas=v.id) as totalref,
                        (SELECT sum(vhde.cantidad*vhde.precio) AS total FROM ventas_has_detallesEquipos AS vhde WHERE vhde.idVenta=v.id ) as totalequ,
                        (SELECT sum(vhdac.cantidad*vhdac.costo) AS total FROM ventas_has_detallesEquipos_accesorios AS vhdac WHERE vhdac.id_venta=v.id ) as totalacc,
                        fac.facturaId
                        FROM ventas as v
                        inner JOIN clientes as cli ON cli.id=v.idCliente
                        inner join prefactura as pre on pre.ventaId=v.id and pre.tipo=0
                        inner join cliente_has_datos_fiscales as clidf on clidf.id=pre.rfc_id
                        LEFT JOIN factura_venta as fac ON fac.ventaId=v.id AND fac.combinada = 0
                        LEFT join f_facturas as facf on facf.FacturasId=fac.facturaId and facf.Estado=1
                        WHERE v.tipov=$empresa and v.prefactura = 1
                        and v.activo=1 and clidf.rfc='XAXX010101000' AND fac.facturaId IS null
                        $whereejecutivo $fechaiwhere $fechafwhere
                        GROUP BY v.id ORDER BY v.id DESC
                         LIMIT 90 ";
                $query=$this->db->query($select);

            
            foreach ($query->result() as $item) {
                $styledisplay=1;
                if ($item->FacturasId>0) {
                    $styledisplay=0;
                }else{
                    if($item->siniva==1){
                        $styledisplay=0;
                    }
                }
                $totalventa=$item->totalcon+$item->totalref+$item->totalequ+$item->totalacc;
                if($totalventa==0){
                    $styledisplay='style="display:none"';
                }$view_tipov='';
                if($item->tipov==1){
                    $view_tipov='Alta Productividad';
                }
                if($item->tipov==2){
                    $view_tipov='D-Impresión';
                }
                if($styledisplay==1){
                $html.='<tr  class="totalventa_'.$totalventa.'">
                            <td>'.$item->id.'</td><td>'.$item->empresa.'</td><td>'.$view_tipov.'</td>
                            <td>
                                <a class="btn-floating green tooltipped" data-position="top" data-delay="50" data-tooltip="Prefactura" onclick="detalle('.$item->id.',0)" ><i class="material-icons">assignment</i></a>
                            </td>
                            <td>'.$item->reg.'</td>
                            <td>';
                            if ($item->FacturasId>0) {
                                $html.='<a class="waves-effect red btn-bmz"  >Facturada</a>';
                            }else{
                                $html.='<button class="waves-effect green btn-bmz import_'.$item->id.' import_pg import_pg_'.$item->id.'_0" onclick="importarproductospg('.$item->id.',0,1)" data-id="'.$item->id.'" data-tipo="0">Importar</button>';
                            }
                        
                        $html.='</td>
                        </tr>';
                }
            }
        //======================================================================
            $select="SELECT vc.estatus, vc.combinadaId as id, vc.prefactura, vc.telefono, vc.correo, cli.empresa, vc.reg, facf.FacturasId,vc.siniva,vc.tipov,vc.equipo,vc.consumibles,vc.refacciones,vc.poliza
                FROM `ventacombinada` `vc`
                JOIN `clientes` `cli` ON `cli`.`id`=`vc`.`idCliente`
                inner join prefactura as pre on pre.ventaId=vc.combinadaId and pre.tipo=3
                inner join cliente_has_datos_fiscales as clidf on clidf.id=pre.rfc_id
                LEFT JOIN `factura_venta` `fac` ON `fac`.`ventaId`=`vc`.`combinadaId` AND `fac`.`combinada` = 1
                LEFT join f_facturas as facf on facf.FacturasId=fac.facturaId and facf.Estado=1
                WHERE 
                    vc.tipov=$empresa and
                    vc.prefactura = 1 and
                    clidf.rfc='XAXX010101000'
                    $whereejecutivo2 $fechaiwhere2 $fechafwhere2 
                
                ORDER BY `vc`.`combinadaId` DESC";
            $query=$this->db->query($select);
            foreach ($query->result() as $item) {
                $styledisplay=1;
                $idpre=$item->id;
                if ($item->FacturasId>0) {
                    $styledisplay=0;
                }else{
                    
                    if($item->siniva==1){
                        $styledisplay=0;
                    }
                }
                $view_tipov='';
                if($item->tipov==1){
                    $view_tipov='Alta Productividad';
                }
                if($item->tipov==2){
                    $view_tipov='D-Impresión';
                }
                if($item->equipo>0){
                    $idpre=$item->equipo;

                }elseif($item->consumibles>0){
                    $idpre=$item->consumibles;

                }elseif($item->refacciones>0){
                    $idpre=$item->refacciones;
                    
                }elseif($item->poliza>0){
                    $idpre=$item->poliza;
                    
                }
                if($styledisplay==1){
                $html.='<tr '.$styledisplay.'>
                            <td>'.$idpre.'</td><td>'.$item->empresa.'</td><td>'.$view_tipov.'</td>
                            <td>
                                <a class="btn-floating green tooltipped" data-position="top" data-delay="50" data-tooltip="Prefactura" onclick="detalle('.$item->id.',1)" >
                                    <i class="material-icons">assignment</i>
                                </a>
                            </td>
                            <td>'.$item->reg.'</td>
                            <td>';
                            if ($item->FacturasId>0) {
                                $html.='<a class="waves-effect red btn-bmz"  >Facturada</a>';
                            }else{
                                $html.='<button class="waves-effect green btn-bmz import_'.$item->id.' import_pg import_pg_'.$item->id.'_1"" onclick="importarproductospg('.$item->id.',1,2)" data-id="'.$item->id.'" data-tipo="1">Importar</button>';
                            }
                                
                            $html.='</td>
                        </tr>';
                }
            }
        //======================================================================
            $this->db->select('v.id,v.estatus,v.prefactura,v.telefono,v.correo,cli.empresa,fac.facturaId,v.siniva,v.tipov,v.reg,(SELECT IFNULL(sum(pd.precio_local),0)+ IFNULL(sum(pd.precio_semi),0)+ IFNULL(sum(pd.precio_foraneo),0)+ IFNULL(sum(pd.precio_especial),0) FROM `polizasCreadas_has_detallesPoliza` as pd WHERE pd.idPoliza=v.id) as totalpoliza');
            $this->db->from('polizasCreadas v ');
            $this->db->join('clientes cli', 'cli.id=v.idCliente');
            $this->db->join('prefactura pre', 'pre.ventaId=v.id and pre.tipo=1');
            $this->db->join('cliente_has_datos_fiscales clidf', 'clidf.id=pre.rfc_id');
            $this->db->join('factura_poliza fac', 'fac.polizaId=v.id','left');
            $where = array(
                        'v.tipo'=>$empresa,
                        'v.prefactura'=>1,
                        'v.activo'=>1,
                        'v.combinada'=>0,
                        'clidf.rfc'=>'XAXX010101000'
                    );
            
            $this->db->where($where);
            if ($ejecutivo>0) {
                $this->db->where(array('v.id_personal' =>$ejecutivo));
                //$whereejecutivo=" v.id_personal=$ejecutivo and ";
            }
             if($faci!=''){
                $this->db->where(array('v.reg>=' =>$faci.' 00:00:00'));
            }
            if($facf!=''){
                $this->db->where(array('v.reg<=' =>$facf.' 23:59:59'));
            }
            $query=$this->db->get();
            foreach ($query->result() as $item) {
                $styledisplay=1;
                    if($item->siniva==1){
                        $styledisplay=0;
                    }
                    if ($item->facturaId>0) {
                        $styledisplay=0;
                    }
                    if($item->totalpoliza>0){

                    }else{
                        $styledisplay=0;
                    }
                    $view_tipov='';
                    if($item->tipov==1){
                        $view_tipov='Alta Productividad';
                    }
                    if($item->tipov==2){
                        $view_tipov='D-Impresión';
                    }
                    if($styledisplay==1){
                $html.='<tr>
                            <td>'.$item->id.'</td><td>'.$item->empresa.'</td><td>'.$view_tipov.'</td>
                            <td>
                                <a class="btn-floating green tooltipped" data-position="top" data-delay="50" data-tooltip="Prefactura" onclick="detallep('.$item->id.')" >
                                    <i class="material-icons">assignment</i>
                                </a>
                            </td>
                            <td>'.$item->reg.'</td>
                            <td>';
                            if ($item->facturaId>0) {
                                $html.='<a class="waves-effect red btn-bmz"  >Facturada</a>';
                            }else{
                                $html.='<button class="waves-effect green btn-bmz import_'.$item->id.' import_pg import_pg_'.$item->id.'_2"" onclick="importarproductospg('.$item->id.',2,3)" data-id="'.$item->id.'" data-tipo="1">Importar</button>';
                            }
                        
                        $html.='</td>
                        </tr>';
                    }
            }
        //======================================================================
            $html.='</tbody></table>';
            echo $html;
    }
    function fileacuse(){
        $idfaccomp=$_POST['idfaccomp'];
        $tipofaccomp=$_POST['tipofaccomp'];

        

        $configUpload['upload_path'] = FCPATH.'kyocera/acuses/';
        $configUpload['allowed_types'] = '*';
        $configUpload['max_size'] = '9000';
        $configUpload['file_name'] = date('Y-m-d_H_i_s');
        $this->load->library('upload', $configUpload);
        $this->upload->do_upload('fileacuse');  
        $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
        $file_name = $upload_data['file_name']; //uploded file name
        $extension=$upload_data['file_ext'];    // uploded file extension
        if($tipofaccomp==0){
            $this->ModeloCatalogos->updateCatalogo('f_facturas',array('doc_acuse_cancelacion'=>$file_name),array('FacturasId'=>$idfaccomp));
        }
        if($tipofaccomp==1){
            $this->ModeloCatalogos->updateCatalogo('f_complementopago',array('doc_acuse_cancelacion'=>$file_name),array('complementoId'=>$idfaccomp));
        }
        

        $output = [];
        echo json_encode($output);
    }
    function eliminarfac(){
        $params=$this->input->post();
        $id = $params['id'];
        $factura=$id;
        $this->ModeloCatalogos->updateCatalogo('f_facturas',array('activo'=>0,'delete_personal'=>$this->idpersonal),array('FacturasId'=>$id,'Estado!='=>1));

        $this->ModeloCatalogos->getdeletewheren('factura_venta',array('facturaId'=>$factura));
        $this->ModeloCatalogos->getdeletewheren('factura_poliza',array('facturaId'=>$factura));
        //===============Liberar periodo=============
            $result=$this->ModeloCatalogos->getselectwheren('factura_prefactura',array('facturaId'=>$factura));
            foreach ($result->result() as $item) {
              $prefId=$item->prefacturaId;
              $this->ModeloCatalogos->updateCatalogo('alta_rentas_prefactura',array('factura_excedente'=>0,'factura_renta'=>0),array('prefId'=>$prefId));
              $this->ModeloCatalogos->updateCatalogo('alta_rentas_prefactura_d',array('statusfacturar'=>0,'statusfacturae'=>0),array('prefId'=>$prefId));
            }
            $strq="SELECT fap.*,fapp.fp_pagosId 
                FROM factura_prefactura as fap
                LEFT JOIN factura_prefactura_pagos as fapp on fapp.facId=fap.facId
                WHERE fap.facturaId='$factura' AND fapp.fp_pagosId IS null";
             $query_facp=$this->db->query($strq);
             foreach ($query_facp->result() as $item) {
                $facId = $item->facId;
                $this->ModeloCatalogos->getdeletewheren('factura_prefactura',array('facId'=>$facId));
             }
        //================================================================================
    }
    function infocomplemento(){
        $params = $this->input->post();
        $idp = $params['idc'];

        $results=$this->ModeloCatalogos->getselectwheren('f_complementopago',array('complementoId'=>$idp));
        $results_d=$this->ModeloCatalogos->getselectwheren('f_complementopago_documento',array('complementoId'=>$idp));
        //echo $results;

        $array = array(
                        'comp'=>$results->row(),
                        'compd'=>$results_d->result(),
                        'compdr'=>$results_d->row()
                    );
        echo json_encode($array);
    }
    function buscartimbre(){
        $params = $this->input->post();
        $pre = $params['pre'];
        $result = $this->ModeloGeneral->buscartimbre($pre);
        $html='<table id="table_search_pre"><thead>
                    <tr>
                        <th>Folio</th>
                        <th>Razon social</th>
                        <th>Rfc</th>
                        <th>Fecha Timbre</th>
                        <th>Monto</th>
                        <th></th>
                    </tr>
                </thead><tbody>';
        $row=0;
        foreach ($result->result() as $item) {
            $html.='<tr>';
                $html.='<td>'.$item->serie.''.$item->Folio.'</td>';
                $html.='<td>'.$item->Nombre.'</td>';
                $html.='<td>'.$item->Rfc.'</td>';
                $html.='<td>'.$item->fechatimbre.'</td>';
                $html.='<td>$ '.number_format($item->total,2,'.',',').'</td>';
                $html.='<td>';
                    $html.='<a class="b-btn b-btn-primary viewpreir_'.$row.'" onclick="viewpreir('.$row.')" ';
                    $html.=' data-fecha="'.date("Y-m-d",strtotime($item->fechatimbre)).'" ';
                    $html.=' data-tipopre="'.$item->serie.'" ';

                    $html.=' data-idcliente="'.$item->Clientes_ClientesId.'" ';
                    $html.=' data-empresa="'.$item->empresa.'" ';



                    $html.=' data-idfac="'.$item->FacturasId.'" ';
                    $html.=' data-folio="'.$item->Folio.'" ';
                    $html.='><i class="fas fa-random"></i></a>';
                $html.='</td>';
            $html.='</tr>';
            $row++;
        }
        $html.='</tbody></table>';
        echo $html;
    }
    function edit_program(){
        $params = $this->input->post();
        $idfac = $params['idfac'];
        $date = $params['date'];
        if($date==''){
            $program=0;
        }else{
            $program=1;
        }
        $this->ModeloCatalogos->updateCatalogo('f_facturas',array('program_date'=>$date,'program'=>$program),array('FacturasId'=>$idfac));
    }
    function exportarlist(){
        $tipo=$_GET['tipo'];


        $params['personal']=$_GET['personal'];
        $params['cliente']=$_GET['cliente'];
        $params['finicio']=$_GET['finicio'];
        $params['ffin']=$_GET['ffin'];

        $params['idclienterfc']=$_GET['idclienterfc'];
        $params['rs_folios']=$_GET['rs_folios'];
        $params['metodopago']=$_GET['metodopago'];
        $params['tcfdi']=$_GET['tcfdi'];
        $params['pensav']=$_GET['pensav'];

        $params['order'][0]['column']=1;
        $params['order'][0]['dir']='desc';
        $params['length']=500;
        $params['start']=0;
        $getdata = $this->Modelofacturas->getfacturas($params);
        //echo json_encode($getdata->result());
        if($tipo=='pdf'){
            $col_w_1=' width="5%" ';
            $col_w_2='width="31%"';
            $col_w_3='width="9%"';
            $col_w_4='width="8%"';
            $col_w_5='width="8%"';
            $col_w_6='width="10%"';
            $col_w_7='width="9%"';
            $col_w_8='width="15%"';
            $col_w_9='width="5%"';
        }else{
            $col_w_1='';$col_w_2='';$col_w_3='';$col_w_4='';$col_w_5='';$col_w_6='';$col_w_7='';$col_w_8='';$col_w_9='';
        }




        $html='<table border="0" cellpadding="2">';
        $html.='<thead >';
        $html.='<tr>';
        $html.='<th '.$col_w_1.' class="th_head">Folio</th>';
        $html.='<th '.$col_w_2.' class="th_head">Cliente</th>';
        $html.='<th '.$col_w_3.' class="th_head">RFC</th>';
        $html.='<th '.$col_w_4.' class="th_head">Monto</th>';
        $html.='<th '.$col_w_5.' class="th_head">Estatus</th>';
        $html.='<th '.$col_w_6.' class="th_head">Fecha</th>';
        $html.='<th '.$col_w_7.' class="th_head">Metodo de pago</th>';
        $html.='<th '.$col_w_8.' class="th_head">Personal</th>';
        $html.='<th '.$col_w_9.' class="th_head">Tipo</th>';
        $html.='</tr>';
        $html.='</thead>';
        $html.='<tbody>';
        if($tipo=='pdf'){
            $html.='<tr>';
                $html.='<td '.$col_w_1.'></td>';
                $html.='<td '.$col_w_2.'></td>';
                $html.='<td '.$col_w_3.'></td>';
                $html.='<td '.$col_w_4.'></td>';
                $html.='<td '.$col_w_5.'></td>';
                $html.='<td '.$col_w_6.'></td>';
                $html.='<td '.$col_w_7.'></td>';
                $html.='<td '.$col_w_8.'></td>';
                $html.='<td '.$col_w_9.'></td>';
            $html.='</tr>';
        }
        foreach ($getdata->result() as $item) {
            $html.='<tr>';
                $html.='<td>'.$item->Folio.'</td>';
                $html.='<td>';
                    if($item->Rfc=='XAXX010101000'){ 
                        $html.=$item->Nombre.'<br>'.$item->empresa;
                    }else{
                        $html.=$item->Nombre;
                    }
                $html.='</td>';
                $html.='<td>'.$item->Rfc.'</td>';
                $html.='<td>$'.number_format($item->total,2,'.',',').'</td>';
                $html.='<td>';
                    $html_s='';
                    if($item->Estado==0){
                        $html_s='Cancelada';
                    }else if($item->Estado==1){
                        $html_s='Facturado';
                        if($item->fechatimbre=='0000-00-00 00:00:00'){
                            $html_s='Sin Facturar';
                        }
                    }else if($item->Estado==2){
                        $html_s='Sin Facturar';
                    }else{
                        $html_s='';
                    }
                    $html.=$html_s;
                $html.='</td>';
                $html.='<td>'.$item->fechatimbre.'</td>';
                $html.='<td>'.$item->FormaPago.'</td>';
                $html.='<td>'.$item->nombre.' '.$item->apellido_paterno.' '.$item->apellido_materno.'</td>';
                $html.='<td>';
                    $html_t='Factura';
                    
                    if($item->f_relacion==1 && $item->f_r_tipo=='01'){
                        $html_t='Factura 01 Nota Credito';
                    }
                    if($item->f_relacion==1 && $item->f_r_tipo=='04'){
                        $html_t='Factura 04 Sustitucion';
                    }
                    if($item->f_relacion==0 && $item->f_r_tipo=='07'){
                        $html_t='Factura de Anticipo recibido';
                    }
                    if($item->f_relacion==1 && $item->f_r_tipo=='07' && $item->TipoComprobante=='I'){
                        $html_t='Factura con relacion de Anticipo';
                    }
                    if($item->f_relacion==1 && $item->f_r_tipo=='07' && $item->TipoComprobante=='E'){
                        $html_t='Factura Egreso de Anticipo';
                    }
                    $html.=$html_t;
                $html.='</td>';
            $html.='</tr>';
        }
        $html.='</tbody></table>';

        $data['tabla']=$html;
        if($tipo=='excel'){
            $this->load->view('Reportes/export_excel_gen',$data);
        }
        if($tipo=='pdf'){
            $this->load->view('Reportes/export_pdf_gen',$data);
        }
    }
}
?>
