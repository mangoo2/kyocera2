<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<title>Accessible markers - Leaflet</title>
	
	<link rel="shortcut icon" type="image/x-icon" href="docs/images/favicon.ico" />

    <!--<link rel="stylesheet" href="https://unpkg.com/leaflet@1.9.4/dist/leaflet.css" integrity="sha256-p4NxAoJBhIIN+hmNHrzRCf9tD/miZyoHS5obTRR9BMY=" crossorigin=""/>
    <script src="https://unpkg.com/leaflet@1.9.4/dist/leaflet.js" integrity="sha256-20nQCchB9co0qIjJZRGuk2/Z9VM+kNiyxNV1lvTlZBo=" crossorigin=""></script>-->

    <link rel="stylesheet" href="<?php echo base_url();?>public/plugins/leaflet/leaflet.css"  crossorigin=""/>
    <script src="<?php echo base_url();?>public/plugins/leaflet/leaflet.js"  crossorigin=""></script>

	<style>
		html, body {
			height: 100%;
			margin: 0;
		}
		.leaflet-container {
			height: 100vh;
			width: 100%;
		}
	</style>

	
</head>
<body>

<div id='map'></div>
19.036723645153856, -98.21511944434279
<?php 
	$latitud=19.036723645153856;
	$longitud=-98.21511944434279;
?>
<script>
	const map = L.map('map').setView([<?php echo $latitud;?>, <?php echo $longitud;?>], 13);

	const tiles = L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
		maxZoom: 19,
		attribution: '<a href="http://mangoo.mx/" target="_blank">Mangoo.mx</a>'
	}).addTo(map);

	

	

	


	const popup = L.popup()
		.setLatLng([<?php echo $latitud;?>, <?php echo $longitud;?>])
		.setContent('Dar click en cualquier parte para obtener las coordenadas.')
		.openOn(map);

	function onMapClick(e) {
		popup
			.setLatLng(e.latlng)
			.setContent(`Latitud ${e.latlng.lat} <br>Longitud ${e.latlng.lng}`)
			.openOn(map);
	}

	map.on('click', onMapClick);
</script>



</body>
</html>