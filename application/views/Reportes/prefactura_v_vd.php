<?php
require_once dirname(__FILE__) . '/TCPDF3/examples/tcpdf_include.php'; 
require_once dirname(__FILE__) . '/TCPDF3/tcpdf.php';

  $logos = base_url().'public/img/alta.jpg';
  $logos2 = base_url().'public/img/kyocera.jpg';
  //=========================================================================
    $rfc_selected='';
    $razon_social_selected='';
    $metodopago_text='';
    $formapago_text='';
    $uso_cfdi_text='';
    $telefono='';
    $estadoval='';
    $puesto_t='';
    $email_t='';
    $atencionpara_t='';

    foreach ($rfc_datos as $item) { 
      if($item->id==$rfc_id){
        $razon_social_selected=$item->razon_social;
        $rfc_selected=$item->rfc; 
        $cp =$item->cp;
        $calle =$item->calle;
        $num_ext=$item->num_ext;
        $colonia=$item->colonia;
        $municipio=$item->municipio;
        $estadoval=$item->estado;
        if($item->rfc=='XAXX010101000'){
          $razon_social_selected.='<br>'.$empresars;
        }
      }      
    }
    foreach ($metodopagorow as $item) {
      if($item->id==$metodopagoId){ 
        $metodopago_text=$item->metodopago_text;
      }
    }
    foreach ($formapagorow->result() as $item) {
      if($item->id==$formapago){ 
        $formapago_text=$item->formapago_text;
      }
    }
    foreach ($cfdirow->result() as $item) {
      if($item->id==$cfdi){ 
        $uso_cfdi_text=$item->uso_cfdi_text;
      }
    } 
    foreach ($resultadoclitel->result() as $item) {
      if($telId==$item->id){
        $telefono=$item->tel_local;
      }
    } 
    foreach ($datoscontacto->result() as $item) { 
      if($telId==$item->datosId){
        $telefono=$item->telefono;
      }
    }
    
    foreach ($estadorow->result() as $item) { 
        if($item->EstadoId==$estado){ 
          $estadoval = $item->Nombre;
        }
    }
    foreach ($datoscontacto->result() as $item) { 
      if($contactoId==$item->datosId){
        $puesto_t=$item->puesto;
        $email_t=$item->email;
        $atencionpara_t=$item->atencionpara;
      }
    }
    if($combinada==1){
      $domicilio_entrega='';
      foreach ($polizaventadetalles->result() as $item) { 
        if($item->comentario!=''){
          $domicilio_entrega=$item->comentario;
        }
      }
      foreach ($resultadoequipos->result() as $item) { 
        if($item->comentario!=''){
          $domicilio_entrega=$item->comentario;
        }
        
      }
      foreach ($resultadoaccesorios->result() as $item) { 
        if($item->comentario!=''){
          $domicilio_entrega=$item->comentario;
        }
      }
      foreach ($resultadoconsumibles->result() as $item) { 
        if($item->comentario!=''){
          $domicilio_entrega=$item->comentario;
        }
      }
      foreach ($consumiblesventadetalles->result() as $item) { 
        if($item->comentario!=''){
          $domicilio_entrega=$item->comentario;
        }
      }
      foreach ($ventadetallesrefacion->result() as $item) {
        if($item->comentario!=''){
          $domicilio_entrega=$item->comentario;
        } 
      }
    }
  //=========================================================================
//====================================================
class MYPDF extends TCPDF {
 
  public function Header() {

      
      //$this->writeHTML($html, true, false, true, false, '');
  }
    // Page footer
  public function Footer() {
    $html2='';
    
      //<img src="http://facturacion33.adminfactura.com.mx/view/image/viamex_logob100.png" width="100px">
      //$this->writeHTML($html2, true, false, true, false, '');
      
  }
} 
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Kyocera');
$pdf->SetTitle('Venta');
$pdf->SetSubject('factura');
$pdf->SetKeywords('factura');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins('7', '10', '7');
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin('10');

// set auto page breaks
$pdf->SetAutoPageBreak(true, 10);//PDF_MARGIN_BOTTOM

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('dejavusans', '', 7);
// add a page
$pdf->AddPage('P', 'A4');
$html='<style type="text/css">
          .t_c{
            text-align:center;
          }
          .t_b{
            font-weight: bold;
          }
          .bor_b{
            border-bottom: 1px solid black;
          }
          .bor_l{
            border-left: 1px solid black;
          }
          .bor_r{
            border-right: 1px solid black;
          }
          .bor_t{
            border-top: 1px solid black;
          }
          .tableb th{
              background-color:rgb(217,217,217);          
          }
        </style>';

  $html.='<table border="0">
            <tr>
              <td width="20%" class="t_c"><img src="'.$logos.'" width="80px"></td>
              <td width="60%" class="t_c">
                "ALTA PRODUCTIVIDAD, S.A DE C.V."<br>
                RFC: APR980122KZ6<br>
                Impresoras y Multifuncionales, Venta, Renta y Mantenimiento.<br>
                Domicilio Fiscal: Blvd. Norte 1831 Col. Villas San Alejandro. Puebla, Pue. Mexico C.P.72090<br>
                Banco BANAMEX, Cuenta 7347132, Sucursal 826, CLABE 2668082673471320<br>
                Call Center 273 3400, 249 5393. www.kyoceraap.com

              </td>
              <td width="20%" class="t_c"><img src="'.$logos2.'" width="100px"></td>
            </tr>
          </table>
          <p></p>
          <table border="0" class="tableb" cellpadding="2">
            <tr>
              <th class="bor_b bor_l  bor_t" >Fecha: </th>
              <td class="bor_b  bor_t" >'.$dia_reg.'/'.$mes_reg.'/'.$ano_reg.'</td>
              <th class="bor_b  bor_t" >AC</th>
              <td class="bor_b  bor_t" >'.$ini_personal.'</td>
              <th class="bor_b  bor_t" >PROINVEN No.</th>
              <td class="bor_b  bor_t" >'.$proinven.'</td>
              <th class="bor_b  bor_t" >ENTREGA</th>
              <td class="bor_b  bor_r bor_t" >'.$fechaentrega.'</td>
            </tr>
            <tr>
              <th class="bor_b bor_l">Cia.</th>
              <td class="bor_b  " colspan="5">'.$razon_social_selected.'</td>
              <th class="bor_b  ">RFC</th>
              <td class="bor_b  bor_r">'.$rfc_selected.'</td>
            </tr>
          </table>
          <table border="0" class="tableb" cellpadding="2">
            <tr>
              <td class="bor_b bor_l bor_r " >Vence:</td>
              <td class="bor_b  bor_r " >'.$vence.'</td>
              <td class="bor_b  bor_r" >Método de pago:</td>
              <td class="bor_b  bor_r " >'.$metodopago_text.'</td>
              <td class="bor_b  bor_r" >Forma de pago:</td>
              <td class="bor_b  bor_r " >'.$formapago_text.'</td>
              <td class="bor_b  bor_r" >Uso de CFDI:</td>
              <td class="bor_b  bor_r " >'.$uso_cfdi_text.'</td>
            </tr>
            <tr>
              <td class="bor_b bor_l  " >Tel:</td>
              <td class="bor_b   " colspan="4">'.$telefono.'</td>
              <td class="bor_b  " >Forma de cobro:</td>
              <td class="bor_b  bor_r " colspan="2">'.$formacobro.'</td>
            </tr>
            <tr>
              <td class="bor_b bor_l bor_r " colspan="4">
                <span class="t_b">Dirección de Entrega/Instalación:</span><br>
                '.$domicilio_entrega.'
              </td>
              <td class="bor_b  bor_r " colspan="4">
                <table >
                  <tr>
                    <td colspan="2" class="t_b">Dirección Fiscal:</td>
                  </tr>
                  <tr>
                    <td>Calle:</td><td>'.$calle.'</td>
                  </tr>
                  <tr>
                    <td>No. </td><td>'.$num_ext.'</td>
                  </tr>
                  <tr>
                    <td>Col.</td><td>'.$colonia.'</td>
                  </tr>
                  <tr>
                    <td>CD.</td><td>'.$municipio.'</td>
                  </tr>
                  <tr>
                    <td>Estado.</td><td>'.$estadoval.'</td>
                  </tr>
                  <tr>
                    <td>C.P.</td><td>'.$cp.'</td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
              <td class="bor_b bor_l  bor_r" colspan="3"><span class="t_b">Contacto:</span>'.$atencionpara_t.'</td>
              <td class="bor_b  bor_r" colspan="2"><span class="t_b">Cargo:</span>'.$puesto_t.'</td>
              <td class="bor_b  bor_r "colspan="3" ><span class="t_b">Email:</span>'.$email_t.'</td>
            </tr>
          </table>
          <p></p>';
          $totalgeneral=0;
          if($combinada==1){
            $html.='<table class="tableb" cellpadding="2" align="center">
                      <tr>
                        <th class="bor_b bor_l  bor_r bor_t">DESCRIPCION</th>
                        <th class="bor_b bor_l  bor_r bor_t">TIPO</th>
                        <th class="bor_b bor_l  bor_r bor_t">MODELO</th>
                        <th class="bor_b bor_l  bor_r bor_t">SERIE</th>
                        <th class="bor_b bor_l  bor_r bor_t">INCLUYE</th>
                        <th class="bor_b bor_l  bor_r bor_t">PRECIO UNITARIO </th>
                        <th class="bor_b bor_l  bor_r bor_t">TOTAL</th>
                      </tr>';
                    foreach ($polizaventadetalles->result() as $item) { 
                      $totalc = 0;
                      if($item->precio_local !==NULL) {
                        $totalc=$item->precio_local;
                      }elseif($item->precio_semi !==NULL) {
                        $totalc=$item->precio_semi;
                      }elseif($item->precio_foraneo !==NULL) {
                        $totalc=$item->precio_foraneo;
                      }elseif($item->precio_especial !==NULL) {
                        $totalc=$item->precio_especial;
                      }else{
                        $tipoprecio=1;
                      }
                      $totalgeneral=$totalgeneral+$totalc;
                      $html.='
                      <tr>
                        <td class="bor_b bor_l  bor_r bor_t">Servicio</td>
                        <td class="bor_b bor_l  bor_r bor_t">'.$item->nombre.'</td>
                        <td class="bor_b bor_l  bor_r bor_t">'.$item->modeloe.'</td>
                        <td class="bor_b bor_l  bor_r bor_t">'.$item->serie.'</td>
                        <td class="bor_b bor_l  bor_r bor_t">'.$item->cubre.'</td>
                        <td class="bor_b bor_l  bor_r bor_t">$ '.number_format($totalc,2,'.',',').'</td>
                        <td class="bor_b bor_l  bor_r bor_t">$ '.number_format($totalc,2,'.',',').'</td>
                      </tr>';
                      if($item->comentario!=''){
                        $html.='
                            <tr>
                              <td class="bor_b bor_l  bor_r bor_t" colspan="7">'.$item->comentario.'</td>
                            </tr>';
                      }
                    }
            $html.='</table> <p></p>';   
          }
          $html.='<table class="tableb" cellpadding="2" align="center">
            <tr>
              <th class="bor_b bor_l  bor_r bor_t">Cant.</th>
              <th class="bor_b bor_l  bor_r bor_t">Marca</th>
              <th class="bor_b bor_l  bor_r bor_t">Tipo</th>
              <th class="bor_b bor_l  bor_r bor_t">Modelo</th>
              <th class="bor_b bor_l  bor_r bor_t">Serie</th>
              <th class="bor_b bor_l  bor_r bor_t">Bodega</th>
              <th class="bor_b bor_l  bor_r bor_t">No. De Parte</th>
              <th class="bor_b bor_l  bor_r bor_t">Equipo de Referencia</th>
              <th class="bor_b bor_l  bor_r bor_t">Serie de Referencia</th>
              <th class="bor_b bor_l  bor_r bor_t">Precio Unitario</th>
              <th class="bor_b bor_l  bor_r bor_t">Total</th>
            </tr>';
            
            foreach ($resultadoequipos->result() as $item) { 
              $totale=$item->cantidad*$item->precio;
              $totalgeneral=$totalgeneral+$totale;
              $seriee='';
              $serieb='';
              
              $numeroparte=$this->ModeloCatalogos->obtenernumeroserieequipo($item->idEquipo);
              $resuleserie=$this->ModeloCatalogos->getequiposerie_vd($item->id);
                foreach ($resuleserie->result() as $itemse) { 
                  $seriee=$itemse->serie;
                  $serieb=$itemse->bodega;
                  $html.='<tr>
                        <td class="bor_b bor_l  bor_r">1</td>
                        <td class="bor_b bor_l  bor_r">KYOCERA</td>
                        <td class="bor_b bor_l  bor_r">IMPRESORA</td>
                        <td class="bor_b bor_l  bor_r">'.$item->modelo.'</td>
                        <td class="bor_b bor_l  bor_r">'.$seriee.'</td>
                        <td class="bor_b bor_l  bor_r">'.$serieb.'</td>
                        <td class="bor_b bor_l  bor_r">'.$numeroparte.'</td>
                        <td class="bor_b bor_l  bor_r"></td>
                        <td class="bor_b bor_l  bor_r"></td>
                        <td class="bor_b bor_l  bor_r">$ '.number_format($item->precio,2,'.',',').'</td>
                        <td class="bor_b bor_l  bor_r">$ '.number_format($item->precio,2,'.',',').'</td>
                      </tr>';
                }
                /*
              $html.='<tr>
                        <td class="bor_b bor_l  bor_r">'.$item->cantidad.'</td>
                        <td class="bor_b bor_l  bor_r">KYOCERA</td>
                        <td class="bor_b bor_l  bor_r">IMPRESORA</td>
                        <td class="bor_b bor_l  bor_r">'.$item->modelo.'</td>
                        <td class="bor_b bor_l  bor_r">'.$seriee.'</td>
                        <td class="bor_b bor_l  bor_r">'.$serieb.'</td>
                        <td class="bor_b bor_l  bor_r">'.$numeroparte.'</td>
                        <td class="bor_b bor_l  bor_r"></td>
                        <td class="bor_b bor_l  bor_r"></td>
                        <td class="bor_b bor_l  bor_r">$ '.number_format($item->precio,2,'.',',').'</td>
                        <td class="bor_b bor_l  bor_r">$ '.number_format($totale,2,'.',',').'</td>
                      </tr>';
                */
            }
            foreach ($resultadoaccesorios->result() as $item) { 
              $totala=$item->cantidad*$item->costo;
              $totalgeneral=$totalgeneral+$totala;
              $seriea='';
              $serieb='';
              $resuleserie=$this->ModeloCatalogos->getaccesorioserie_vd($item->id_accesoriod);
                $arrayseries=array();
                foreach ($resuleserie->result() as $itemse) { 
                  $seriea.=$itemse->serie.'<br>';
                  $seriebl=$this->ModeloCatalogos->obtenerbodega($item->serie_bodega);
                  $serieb.=$seriebl.'<br>';
                }
                $html.='<tr>
                        <td class="bor_b bor_l  bor_r">'.$item->cantidad.'</td>
                        <td class="bor_b bor_l  bor_r">KYOCERA</td>
                        <td class="bor_b bor_l  bor_r">ACCESORIO</td>
                        <td class="bor_b bor_l  bor_r">'.$item->nombre.'</td>
                        <td class="bor_b bor_l  bor_r">'.$seriea.'</td>
                        <td class="bor_b bor_l  bor_r">'.$serieb.'</td>
                        <td class="bor_b bor_l  bor_r">'.$item->no_parte.'</td>
                        <td class="bor_b bor_l  bor_r"></td>
                        <td class="bor_b bor_l  bor_r"></td>
                        <td class="bor_b bor_l  bor_r">$ '.number_format($item->costo,2,'.',',').'</td>
                        <td class="bor_b bor_l  bor_r">$ '.number_format($totala,2,'.',',').'</td>
                      </tr>';
            }
            foreach ($resultadoconsumibles->result() as $item) { 
              $costo=$item->costo_toner;

              $totalc=$item->cantidad*$costo;
              $totalgeneral=$totalgeneral+$totalc;

              $html.='<tr>
                        <td class="bor_b bor_l  bor_r">'.$item->cantidad.'</td>
                        <td class="bor_b bor_l  bor_r">KYOCERA</td>
                        <td class="bor_b bor_l  bor_r">Consumible</td>
                        <td class="bor_b bor_l  bor_r">'.$item->modelo.'</td>
                        <td class="bor_b bor_l  bor_r"></td>
                        <td class="bor_b bor_l  bor_r"></td>
                        <td class="bor_b bor_l  bor_r">'.$item->parte.'</td>
                        <td class="bor_b bor_l  bor_r">'.$item->modeloeq.'</td>
                        <td class="bor_b bor_l  bor_r"></td>
                        <td class="bor_b bor_l  bor_r">$ '.number_format($costo,3,'.',',').'</td>
                        <td class="bor_b bor_l  bor_r">$ '.number_format($totalc,3,'.',',').'</td>
                      </tr>';
            }
            foreach ($consumiblesventadetalles->result() as $item) { 
              $costo=$item->costo_toner;
              $totalc=$item->cantidad*$costo;
              $totalgeneral=$totalgeneral+$totalc;
              $equiporelacionado='';
              //====================================================================
                $resultse=$this->ModeloCatalogos->db3_getselectwheren('series_productos',array('serieId'=>$item->idserie));
                $seriee='';
                foreach ($resultse->result() as $itemse) {
                  $seriee=$itemse->serie;
                }
                //$resultseu=$this->ModeloCatalogos->db3_getselectwheren('alta_rentas_equipos',array('serieId'=>$item->idserie,'id_equipo'=>$item->idEquipo));
                $ubicacione='';
                //foreach ($resultseu->result() as $itemseu) {
                //  $ubicacione=$itemseu->ubicacion;
                //}
                //if($item->idserie>0){
                //  $resultser=$this->ModeloCatalogos->db3_getselectwheren('rentas_has_detallesEquipos',array('id'=>$item->idEquipo));
                //  foreach ($resultser->result() as $itemser) {
                //    $equiporelacionado=$itemser->modelo;
                //  }
                //}
                //$bodegaselect o $ubicacione
                $bodegaselect = $this->ModeloCatalogos->obtenerbodega($item->bodegas);
              //===========================================================================
              $html.='<tr>
                        <td class="bor_b bor_l  bor_r">'.$item->cantidad.'</td>
                        <td class="bor_b bor_l  bor_r">KYOCERA</td>
                        <td class="bor_b bor_l  bor_r">Consumible.</td>
                        <td class="bor_b bor_l  bor_r">'.$item->modelo.'</td>
                        <td class="bor_b bor_l  bor_r">'.$item->foliotext.'</td>
                        <td class="bor_b bor_l  bor_r">'.$bodegaselect.'</td>
                        <td class="bor_b bor_l  bor_r">'.$item->parte.'</td>
                        <td class="bor_b bor_l  bor_r">'.$item->modeloeq.'</td>
                        <td class="bor_b bor_l  bor_r">'.$seriee.'</td>
                        <td class="bor_b bor_l  bor_r">$ '.number_format($costo,3,'.',',').'</td>
                        <td class="bor_b bor_l  bor_r">$ '.number_format($totalc,2,'.',',').'</td>
                      </tr>';
            }
            foreach ($ventadetallesrefacion->result() as $item) { 
              $totalc=round($item->cantidad*$item->precioGeneral,2);
              $totalgeneral=$totalgeneral+$totalc;

              //==========================================================
                $serier='';
                $serieb='';
                $resuleserie=$this->ModeloCatalogos->getrefaccionserie_vd($item->id);
                foreach ($resuleserie->result() as $itemse) { 
                  $serier.=$itemse->serie.'<br>';
                  $serieb.=$itemse->bodega.'<br>';
                }
              //==========================================================
              

              $html.='<tr>
                        <td class="bor_b bor_l  bor_r">'.$item->cantidad.'</td>
                        <td class="bor_b bor_l  bor_r">KYOCERA</td>
                        <td class="bor_b bor_l  bor_r">Refacción</td>
                        <td class="bor_b bor_l  bor_r">'.$item->modelo.'</td>
                        <td class="bor_b bor_l  bor_r">'.$serier.'</td>
                        <td class="bor_b bor_l  bor_r">'.$serieb.'</td>
                        <td class="bor_b bor_l  bor_r">'.$item->codigo.'</td>
                        <td class="bor_b bor_l  bor_r">'.$item->modeloeq.'</td>
                        <td class="bor_b bor_l  bor_r">'.$item->serieeq.'</td>
                        <td class="bor_b bor_l  bor_r">$ '.number_format($item->precioGeneral,2,'.',',').'</td>
                        <td class="bor_b bor_l  bor_r">$ '.number_format($totalc,2,'.',',').'</td>
                      </tr>';
            }
            if($combinada==1){

            }
          $html.='<tr><td  colspan="9"></td><td class="bor_b bor_l  bor_r">Subtotal</td><td class="bor_b bor_l  bor_r">$ '.number_format($totalgeneral,2,'.',',').'</td></tr>';
          $html.='<tr><td  colspan="9"></td><td class="bor_b bor_l  bor_r">IVA</td><td class="bor_b bor_l  bor_r">$ '.number_format($totalgeneral*0.16,2,'.',',').'</td></tr>';
          $html.='<tr><td  colspan="9"></td><td class="bor_b bor_l  bor_r">Total</td><td class="bor_b bor_l  bor_r">$ '.number_format(round($totalgeneral*0.16, 2)+round($totalgeneral, 2),2,'.',',').'</td></tr>';
          $html.='</table>';
          $html.='<p></p><p></p>';

            if($combinada==1){
              $html.='<table class="tableb" cellpadding="2" align="center">
                        <tr>
                          <th class="bor_b bor_l  bor_r bor_t">Vigencia</th>
                          <th class="bor_b bor_l  bor_r bor_t">Visitas</th>
                        </tr>';
                foreach ($polizaventadetalles->result() as $item) { 
                $html.='<tr>
                          <td class="bor_b bor_l  bor_r bor_t">'.$item->vigencia_meses.'</td>
                          <td class="bor_b bor_l  bor_r bor_t">'.$item->cubre.'</td>
                        </tr>';
                }
                   $html.='</table>';
            }

          $html.='<table class="bor_b bor_l  bor_r bor_t" cellpadding="2">
                  <tr><td>Observaciones</td></tr>
                  <tr><td>'.$observaciones.'</td></tr>
                  </table>';
$pdf->writeHTML($html, true, false, true, false, '');



$pdf->IncludeJS('print(true);');
$pdf->Output('Folios_.pdf', 'I');

?>