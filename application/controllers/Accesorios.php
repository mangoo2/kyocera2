<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Accesorios extends CI_Controller {
    public function __construct()     {
        parent::__construct();

        $this->load->model('ModeloCatalogos');
        $this->load->model('Modeloaccesorios');
        $this->load->helper('url');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->submenu=52;
        if($this->session->userdata('logeado')==true){
            $this->perfilid =$this->session->userdata('perfilid');
            $this->idpersonal = $this->session->userdata('idpersonal');
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,$this->submenu);// 52 es el id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
        }else{
            redirect('login'); 
        }
    }

    function index(){
        $data['MenusubId']=$this->submenu;
        $data['perfilid']=$this->perfilid;
            $this->load->view('header');
            $this->load->view('main');
            $this->load->view('Catalogos/accesorios',$data);
            $this->load->view('footer');
            $this->load->view('Catalogos/accesoriosjs');
    }
    public function getlistaccesorios() {
        $params = $this->input->post();
        $getdata = $this->Modeloaccesorios->getaccesorios($params);
        $totaldata= $this->Modeloaccesorios->total_accesorios($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    //========================================================================
    function imagenes_multiple(){
        $idequipo=$_POST['idequipo'];
      $data = [];
   
      $count = count($_FILES['files']['name']);
      $output = [];
      for($i=0;$i<$count;$i++){
    
        if(!empty($_FILES['files']['name'][$i])){
    
          $_FILES['file']['name'] = $_FILES['files']['name'][$i];
          $_FILES['file']['type'] = $_FILES['files']['type'][$i];
          $_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
          $_FILES['file']['error'] = $_FILES['files']['error'][$i];
          $_FILES['file']['size'] = $_FILES['files']['size'][$i];
          $DIR_SUC=FCPATH.'uploads/equipos';

          $config['upload_path'] = $DIR_SUC; 
          //$config['allowed_types'] = 'docx|xlsx|pdf|doc|xls|pptx';
          $config['allowed_types'] = '*';

          //$config['allowed_types'] = 'docx|xlsx|pdf|jpg|jpeg|png|gif';
          $config['max_size'] = 5000;
          $file_names=date('Ymd_His').'_'.rand(0, 500);
          //$config['file_name'] = $file_names.'_'.str_replace(' ', '_', $_FILES['files']['name'][$i]);
          $config['file_name'] = $file_names;
          //$config['file_name'] = $file_names;
   
          $this->load->library('upload',$config); 
    
          if($this->upload->do_upload('file')){
            $uploadData = $this->upload->data();
            $filename = $uploadData['file_name'];
            $filenametype = $uploadData['file_ext'];
   
            $data['totalFiles'][] = $filename;
            //$this->ModeloGeneral->tabla_inserta('equipo_imgs',array('idequipo'=>$idequipo,'imagen'=>$filename,'filetype'=>$filenametype,'usuario'=>$this->idpersonal));
            $this->ModeloCatalogos->Insert('catalogo_accesorios_imgs',array('idaccesorio'=>$idequipo,'imagen'=>$filename));
          }else{
            $data = array('error' => $this->upload->display_errors());
            log_message('error', json_encode($data)); 
          }
        }
   
      }
      echo json_encode($output);
    }
    function favorito(){
        $params = $this->input->post();
        $id=$params['equipo'];
        $mostrar = $params['status'];
        $this->ModeloCatalogos->updateCatalogo('catalogo_accesorios',array('destacado'=>$mostrar),array('id'=>$id));
        echo $mostrar;
    }
    function mostrarweb(){
        $params = $this->input->post();
        $id=$params['idequipo'];
        $mostrar = $params['mostrar'];
        $this->ModeloCatalogos->updateCatalogo('catalogo_accesorios',array('paginaweb'=>$mostrar),array('id'=>$id));
    }
    function viewimages(){
        $params = $this->input->post();
        $id=$params['idequipo'];
        $resultados=$this->ModeloCatalogos->getselectwheren('catalogo_accesorios_imgs',array('idaccesorio'=>$id,'activo'=>1));
        $html='';
                foreach ($resultados->result() as $item) {
                    $html.='<div class="card_img"><div class="card_img_c"><img class="materialboxed" src="'.base_url().'uploads/equipos/'.$item->imagen.'"></div><div class="card_img_a">
                                <a class="btn-floating red tooltipped" data-position="top" data-delay="50" data-tooltip="Eliminar" onclick="deleteimg('.$item->id.')"><i class="material-icons">delete_forever</i></a></div></div>';
                }
        echo $html;
    }
    function viewcaracte(){
        $params = $this->input->post();
        $id=$params['idequipo'];
        //$resultados=$this->ModeloCatalogos->getselectwheren('equipos_caracteristicas',array('idequipo'=>$id,'activo'=>1));
        
        $strq = "SELECT * FROM catalogo_accesorios_caracteristicas where idaccesorios=$id and activo=1 ORDER BY orden ASC";
        $query = $this->db->query($strq);

        $html='<table class="table striped"><thead><tr><th>Tipo</th><th>Descripcion</th><th>General</th><th>Orden</th><th></th></tr></thead><tbody>';
        foreach ($query->result() as $item) {
            if($item->general==1){
                $general='SI';
            }else{
                $general='';
            }
            $html.='<tr><td>'.$item->name.'</td><td>'.$item->descripcion.'</td><td>'.$general.'</td><td>'.$item->orden.'</td>
                        <td>
                            <a class="btn-floating green tooltipped edit soloadministradores eq_cact_'.$item->id.'" 
                                data-name="'.$item->name.'"
                                data-descripcion="'.$item->descripcion.'"
                                data-general="'.$item->general.'"
                                data-orden="'.$item->orden.'"
                                onclick="editar_ec('.$item->id.')" data-position="top" data-delay="50" data-tooltip="Editar" data-tooltip-id="f5aed0ec-5db2-aa68-1319-23959ebc0465"><i class="material-icons">mode_edit</i></a>
                            <a class="btn-floating red tooltipped elimina soloadministradores" data-position="top" data-delay="50" data-tooltip="Eliminar" data-tooltip-id="7817ec6e-15c1-ab9a-76f3-cff8bc1c9ab4" onclick="delete_ec('.$item->id.')"><i class="material-icons">delete_forever</i></a>
                        </td>
                    </tr>';
        }
        $html.='</tbody></table>';
        echo $html;
    }
    function deleteimg(){
        $params = $this->input->post();
        $id=$params['idequipo'];
        $this->ModeloCatalogos->updateCatalogo('catalogo_accesorios_imgs',array('activo'=>0),array('id'=>$id));
    }
    function insertaActualizacaracteristicas(){
        $params=$this->input->post();
        $id = $params['id'];
        unset($params['id']);
        if($id>0){
            $this->ModeloCatalogos->updateCatalogo('catalogo_accesorios_caracteristicas',$params,array('id'=>$id));
        }else{
            $this->ModeloCatalogos->Insert('catalogo_accesorios_caracteristicas',$params);
        }
    }
    function deleteec(){
        $params = $this->input->post();
        $id=$params['idequipo'];
        $this->ModeloCatalogos->updateCatalogo('catalogo_accesorios_caracteristicas',array('activo'=>0),array('id'=>$id));
    }


    
}
?>
