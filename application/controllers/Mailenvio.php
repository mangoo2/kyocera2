<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Mailenvio extends CI_Controller {
    public function __construct()     {
        parent::__construct();
        //$this->load->model('Clientes_model', 'model');
        date_default_timezone_set('America/Mexico_City');
        $this->load->model('ModeloCatalogos');
        $this->load->model('Configuraciones_model');
        $this->load->model('ModeloAsignacion');
        $this->load->model('Ventas_model');
        $this->load->model('Rentas_model');
        $this->load->helper('url');
        $this->fechahoy = date('Y-m-d');
        $this->horaactual = date('H:i:s');
        $this->version = date('YmdHi');
        $this->fechahoy = date('Y-m-d G:i:s');
        if($this->session->userdata('logeado')==true){
            $this->perfilid =$this->session->userdata('perfilid');
            $this->idpersonal = $this->session->userdata('idpersonal');
        }else{
            //redirect('login');
        }
        $this->enviosproductivo=1;//0 demo 1 productivo

            //Indicamos el protocolo a utilizar
            $this->cf_protocol = 'smtp';
             
            //El servidor de correo que utilizaremos
            $this->cf_smtp_host ='mail.altaproductividadapr.com'; 
             
            //Nuestro usuario
            $this->cf_smtp_user = 'info@altaproductividadapr.com';

            //Nuestra contraseña
            $this->cf_smtp_pass = 'yUs&J=T4*V$J';

            //Puerto
            $this->cf_smtp_port = '465';

            $this->cf_smtp_crypto = 'ssl';
    }

    function index(){
        
    }
    
    function envio_s(){
        $bcc_array[]='info@altaproductividadapr.com';
        $params = $this->input->post();
        $asi=$params['asi'];
        $tipo=$params['tipo'];
        $per=$params['per'];
        $perbbc=$params['perbbc'];
        $code=$params['code'];
        $asunto=$params['asunto'];
        $comen=$params['comen'];
        $email=$params['email'];
        $idCliente=0;

        $strqf="SELECT im.file FROM img_temp as im WHERE im.code='$code'";
                $query_f = $this->db->query($strqf);

        $strqp="SELECT * FROM personal as per WHERE per.personalId='$per'";
        $query_p = $this->db->query($strqp);
        $query_p = $query_p->row();

        $strqpbbc="SELECT * FROM personal as per WHERE per.personalId='$perbbc'";
        $query_pbbc = $this->db->query($strqpbbc);
        foreach ($query_pbbc->result() as $itembbc) {
            $bcc_array[]=$itembbc->email;
        }


        $atencionpara='';
        //================================
            //cargamos la libreria email
            $this->load->library('email');

            /*
            * Configuramos los parámetros para enviar el email,
            * las siguientes configuraciones es recomendable
            * hacerlas en el fichero email.php dentro del directorio config,
            * en este caso para hacer un ejemplo rápido lo hacemos 
            * en el propio controlador
            */
            
            //Indicamos el protocolo a utilizar
            $config['protocol'] = 'smtp';
             
            //El servidor de correo que utilizaremos
            $config["smtp_host"] ='mail.altaproductividadapr.com'; 
             
            //Nuestro usuario
            $config["smtp_user"] = 'info@altaproductividadapr.com';

            //Nuestra contraseña
            $config["smtp_pass"] = 'yUs&J=T4*V$J';

            //Puerto
            $config["smtp_port"] = '465';

            $config["smtp_crypto"] = 'ssl';
                    
            //El juego de caracteres a utilizar
            $config['charset'] = 'utf-8'; 
     
            //Permitimos que se puedan cortar palabras
            $config['wordwrap'] = TRUE;
             
            //El email debe ser valido  
            $config['validate'] = true;

            $config['mailtype'] = 'html';

            //Establecemos esta configuración
            $this->email->initialize($config);
            //$asunto='Servicio';
            //Ponemos la dirección de correo que enviará el email y un nombre
            $this->email->from('info@altaproductividadapr.com',$asunto);
             
              /*
               * Ponemos el o los destinatarios para los que va el email
               * en este caso al ser un formulario de contacto te lo enviarás a ti
               * mismo
               */
        //======================
            //$this->email->bcc('andres@mangoo.mx');
            $this->email->bcc($bcc_array);
            $this->email->reply_to($query_p->email, $query_p->nombre.' '.$query_p->apellido_paterno.' '.$query_p->apellido_materno);
        
        
            
            $correo='ag-gerardo@hotmail.com';
            if($this->enviosproductivo==1){
                $correo = $email;
            }
            //Definimos el asunto del mensaje
                $this->email->subject($asunto);
            $empresa='';
            $contacto='';
            $telefono='';
            $servicio='';
            $fecha='';
            $tecnico='';
            $equipos='';
            log_message('error','asignacion: '.$asi.' tipo: '.$tipo);
            $contactosarray=array();
            if($tipo==1){
                $strq="SELECT `ace`.`asignacionIde`, `per`.`personalId`, 
                            CONCAT(per.nombre, ' ', `per`.`apellido_paterno`, ' ', per.apellido_materno) as tecnico, `cli`.`id`, `cli`.`empresa`, 
                        ace.fecha, 
                        ace.hora,
                        ace.horafin, 
                        `rde`.`modelo`, `serp`.`serie`, 
                        ace.status, 
                        ac.tiposervicio, 
                        `ace`.`horafin`, `ac`.`asignacionId`, `sereve`.`nombre` as `servicio`,
                        ac.prioridad,
                        rde.id as id_equipo,
                        serp.serieId,
                        con.contacto,
                        con.telefono,
                        ac.comentariocal,
                        con.servicio_equipo,
                        con.servicio_doc,
                        ac.tservicio_a,
                        ace.confirmado,
                        pol.nombre as poliza,
                        ren.idCliente,
                        ac.contacto_text,
                        cli.equipo_acceso,
                        cli.documentacion_acceso,
                        ac.equipo_acceso_info,
                        ac.documentacion_acceso_info,
                        ac.tserviciomotivo,
                        ac.tpoliza,
                        con.idRenta,
                        rde.idEquipo,
                        asre.comentario,
                        asre.ubicacion,
                        con.idRenta,
                        asre.contadores,
                        ace.nr,
                        ren.correo,
                        CONCAT(per2.nombre, ' ', per2.apellido_paterno, ' ', per2.apellido_materno) as tecnico2,
                        CONCAT(per3.nombre, ' ', per3.apellido_paterno, ' ', per3.apellido_materno) as tecnico3
                        FROM asignacion_ser_contrato_a_e ace
                        JOIN asignacion_ser_contrato_a ac ON ac.asignacionId=ace.asignacionId
                        LEFT JOIN personal per ON per.personalId=ace.tecnico
                        LEFT JOIN personal per2 ON per2.personalId=ace.tecnico2
                        LEFT JOIN personal per3 ON per3.personalId=ace.tecnico3
                        JOIN contrato con ON con.idcontrato=ac.contratoId
                        JOIN rentas ren ON ren.id=con.idRenta
                        JOIN clientes cli ON cli.id=ren.idCliente
                        JOIN rutas ru ON ru.id=ac.zonaId
                        JOIN rentas_has_detallesEquipos rde ON rde.id=ace.idequipo
                        JOIN series_productos serp ON serp.serieId=ace.serieId
                        left JOIN polizas as pol on pol.id=ac.tpoliza
                        left JOIN servicio_evento sereve ON sereve.id=ac.tservicio
                        left JOIN polizas_detalles as pold on pold.id=ac.tservicio_a
                        JOIN asignacion_series_r_equipos asre ON rde.id=asre.id_equipo and asre.serieId=serp.serieId
                        where ace.activo=1  and ac.asignacionId=$asi";
                        $query = $this->db->query($strq);
                        foreach ($query->result() as $item) {
                            $empresa = $item->empresa;
                            $contacto=$item->contacto;
                            $telefono=$item->telefono;
                            $servicio = $item->poliza;
                            $fecha = $item->fecha;
                            $idCliente = $item->idCliente;
                            //$correo = $item->correo;
                            $ubicacion = $item->ubicacion;
                            $tecnico = $item->tecnico.', '.$item->tecnico2.', '.$item->tecnico3;
                            $direccionequipo= $this->getdireccionequipos($item->id_equipo,$item->serieId,$fecha);
                            $equipos .='<tr><td>'.$item->modelo.' </td><td>'.$item->serie.'</td><td style="font-size:10px;">'.$direccionequipo.' '.$ubicacion.'</td></tr>';
                        }
            }
            if($tipo==2){
                $resultado=$this->ModeloAsignacion->getdatospoliza($asi,0,0);
                foreach ($resultado->result() as $item) {
                    $empresa = $item->empresa;
                    $contacto=$item->atencion;
                    $telefono=$item->telefono;
                    $servicio = $item->servicio;
                    $fecha = $item->fecha;
                    $tecnico = $item->tecnico.', '.$item->tecnico2;
                    $idCliente = $item->idCliente;
                    
                    $equipos .='<tr><td>'.$item->modelo.'</td><td>'.$item->serie.'</td><td style="font-size:10px;">'.$item->direccionservicio.'</td></tr>';
                }
            }
            if($tipo==3){
                $resultado=$this->ModeloAsignacion->getdatoscliente($asi);
                foreach ($resultado->result() as $item) {
                    $empresa = $item->empresa;
                    $servicio = $item->servicio;
                    $contacto=$item->contacto_text;
                    $servicio = $item->poliza;
                    $fecha = $item->fecha;
                    $tecnico = $item->tecnico.', '.$item->tecnico2;
                    $idCliente = $item->clienteId;
                    
                    if($item->retiro==1){
                        $modelo_eq=$item->modeloretiro;
                    }else{
                        $modelo_eq=$item->modelo;
                    }
                    $equipos .='<tr><td>'.$modelo_eq.'</td><td>'.$item->serie.'</td><td style="font-size:10px;">'.$item->direccion.'</td></tr>';
                }
            }
            if ($tipo==4) {
                $resultado=$this->ModeloAsignacion->getdatosventas($asi);
                foreach ($resultado->result() as $item) {
                    $empresa = $item->empresa;
                    $servicio = $item->servicio;
                    $contacto=$item->atencionpara;
                    $telefono=$item->telefono;
                    $tecnico = $item->tecnico;
                    $fecha = $item->fecha;
                    $tecnico = $item->tecnico;
                    $ventaId = $item->ventaId;
                    $ventaIdd = $item->ventaIdd;
                    $tipov = $item->tipov;
                    $direccion = $item->direccion;
                    $idCliente = $item->clienteId;
                    //$correo = $item->email;
                    if($this->enviosproductivo==1){
                        $correo=$email;
                    }
                    //==============================================
                        if ($tipov==1) {
                            $reesequi = $this->ModeloCatalogos->db13_getselectwheren('ventacombinada',array('combinadaId'=>$ventaIdd));
                            foreach ($reesequi->result() as $item) {
                                $ventaIdd=$item->equipo;
                            }
                        }
                        $resultadoequipos = $this->ModeloCatalogos->db13_getselectwheren('ventas_has_detallesEquipos',array('idVenta'=>$ventaIdd));
                        $resultadoaccesorios = $this->ModeloCatalogos->accesoriosventa($ventaIdd);
                        $resultadoconsumibles = $this->ModeloCatalogos->consumiblesventa($ventaIdd);
                        $consumiblesventadetalles = $this->ModeloCatalogos->consumiblesventadetalles($ventaIdd);
                        $polizaventadetalles = $this->ModeloCatalogos->polizaventadetalles($ventaIdd);
                        $ventadetallesrefacion = $this->ModeloCatalogos->ventas_has_detallesRefacciones($ventaIdd);

                        foreach ($resultadoequipos->result() as $item) { 
                            $model=$this->ModeloCatalogos->obtenernumeroserieequipo($item->idEquipo);
                            $resuleserie=$this->ModeloCatalogos->getequiposerie($item->id);

                            foreach ($resuleserie->result() as $itemse) { 
                                $equipos .='<tr><td>'.$item->modelo.'</td><td>'.$itemse->serie.'</td><td style="font-size:10px;">'.$direccion.'</td></tr>';
                            }
                            
                        }
                        foreach ($resultadoaccesorios->result() as $item) { 
                            $resuleserie=$this->ModeloCatalogos->getaccesorioserie($item->id_accesoriod);
                            foreach ($resuleserie->result() as $itemse) { 
                                $equipos .='<tr><td>'.$item->nombre.'</td><td>'.$itemse->serie.'</td><td style="font-size:10px;">'.$direccion.'</td></tr>';
                            }
                        }
                        foreach ($resultadoconsumibles->result() as $item) { 
                            $equipos .='<tr><td>'.$estatuseq.''.$item->modelo.'</td><td></td><td style="font-size:10px;">'.$direccion.'</td></tr>';
                        }
                        foreach ($consumiblesventadetalles->result() as $item) { 
                            $equipos .='<tr><td>'.$item->modelo.'</td><td><table>
                                                                            <tr>
                                                                                <td width="50%"></td>
                                                                                <td width="50%" style="color: #625e5e; font-size: 11px;">Equipo referencia<br>'.$item->modeloeq.'</td></tr></table></td><td style="font-size:10px;">'.$direccion.'</td></tr>';
                        }
                        foreach ($ventadetallesrefacion->result() as $item) {
                            $resuleserie=$this->ModeloCatalogos->getrefaccionserie($item->id);
                            foreach ($resuleserie->result() as $itemse) {
                                $equipos .='<tr><td>'.$item->modelo.'</td><td><table><tr><td rowspan="2">'.$itemse->serie.'</td><td style="color: #625e5e; font-size: 11px;">Equipo referencia<br>'.$item->modeloeq.'</td></tr><tr><td style="color: #625e5e; font-size: 11px;">'.$item->serieeq.'</td></tr></table> </td><td style="font-size:10px;">'.$direccion.'</td></tr>';
                            }
                        }
                    //==============================================
                }
            }
            $result_mc = $this->ModeloCatalogos->getselectwheren('cliente_datoscontacto',array('activo'=>1,'email'=>$correo));
            foreach ($result_mc->result() as $itemmc) {
                $contacto=$itemmc->atencionpara;
                $telefono=$itemmc->telefono;
            }
            $this->email->to($correo);


        $message  = '<div style="background: url(https://altaproductividadapr.com/public/img/fondo.jpg) #eaeaea;width: 100%;height: 94%;min-height: 752px;padding-top: 25px;margin: 0px;">
                    <div style="width: 10%; height: 10px; float: left;">
                        
                    </div>
                    <div style="background: white; border-top: 3px solid #ff0101; padding: 25px 25px 0px 25px; width: 80%; max-width: 825px;margin-left: auto; margin-right: auto; float: left; -webkit-box-shadow: 10px 10px 42px -6px rgba(0,0,0,0.75);-moz-box-shadow: 10px 10px 42px -6px rgba(0,0,0,0.75);box-shadow: 10px 10px 42px -6px rgba(0,0,0,0.75);">
                        <div style="width: 100%; float: left;">
                            <div style="width: 30%;float: left;">
                                <img src="https://altaproductividadapr.com/public/img/cotheader2.png" style="max-height: 100px;">
                            </div>
                            <div style="width: 40%;float: left; min-height:10px; font-style: italic; font-size: 14px; color:#2e4064;">
                                Impresoras y Multifunciones, Venta, Renta y Mantenimiento. Blvd. Norte 1831 Col. Villas San Alejandro. Puebla, Pue. <br>Call Center 273 3400, 249 5393. www.kyoceraap.com
                            </div>
                            <div style="width: 30%;float: left;">
                                <img src="https://altaproductividadapr.com/public/img/alta.jpg" style="max-height: 100px;">
                            </div>
                        </div>
                        <div style="width: 100%; float: left;">
                            <div style="width: 50%;float: left;height: 10px;"></div>
                            <div style="width: 50%;float: left; border-bottom: 3px solid #ff0101; font-size:17px"><b>Confirmación Servicio</b></div>
                        </div>
                        <div style="width: 100%; float: left;font-size: 14px; color:#2e4064;">
                            <div style="width: 30%;float: left;">Atención para:</div>
                            <div style="width: 70%;float: left;">'.$empresa.'</div>
                        </div>
                        <div style="width: 100%; float: left;font-size: 14px; color:#2e4064;">
                            <div style="width: 30%;float: left;">Contacto:</div>
                            <div style="width: 70%;float: left;">'.$contacto.'</div>
                        </div>
                        <div style="width: 100%; float: left;font-size: 14px; color:#2e4064;">
                            <div style="width: 30%;float: left;">Teléfono:</div>
                            <div style="width: 70%;float: left;">'.$telefono.'</div>
                        </div>
                        <div style="width: 100%; float: left;font-size: 14px; color:#2e4064; margin-bottom: 10px;">
                            <div style="width: 30%;float: left;">Tipo servicio:</div>
                            <div style="width: 70%;float: left;">'.$servicio.'</div>
                        </div>
                        <div style="width: 100%; float: left;font-size: 14px; ">
                            <div style="width: 30%;float: left;color:#2e4064;">Día programado de servicio:</div>
                            <div style="width: 70%;float: left; color: red;">'.$fecha.'</div>
                        </div>
                        <div style="width: 100%; float: left;font-size: 14px; margin-bottom: 10px;">
                            <div style="width: 30%;float: left;color:#2e4064;">Técnicos programados:</div>
                            <div style="width: 70%;float: left; color: red;">'.$tecnico.'</div>
                        </div>
                        <div style="width: 100%; float: left;font-size: 14px; margin-bottom: 10px;color:#2e4064; margin-bottom: 10px;">
                            <table style="width:100%">
                                <thead>
                                    <tr ><th style="border-bottom: 3px solid #ff0101;">Modelo</th><th style="border-bottom: 3px solid #ff0101;">Serie</th><th style="border-bottom: 3px solid #ff0101;">Dirección</th></tr>
                                </thead>
                                <tbody>
                                    '.$equipos.'
                                </tbody>
                            </table>
                        </div>
                        <div style="width: 100%; float: left;font-size: 14px; margin-bottom: 10px;color:#2e4064; margin-bottom: 10px; text-align:justify">
                            '.$comen.'
                        </div>
                        <div style="width: 100%; float: left;font-size: 14px; margin-bottom: 10px;color:#2e4064; margin-bottom: 10px;">
                            
                        </div>
                        <div style="width: 100%; float: left;font-size: 14px; margin-bottom: 10px;color:#2e4064; margin-bottom: 10px;">
                            <table style="width: 100%;">
                                <tr>
                                    <td></td>
                                    <td style="text-decoration: none;color: #455a64;font-weight: bold;">'.$query_p->nombre.' '.$query_p->apellido_paterno.' '.$query_p->apellido_materno.'</td>
                                    <td></td>
                                    <td><a href="mailto:'.$query_p->email.'" style="text-decoration: none;color: #455a64;font-weight: bold;">'.$query_p->email.'</a></td>
                                    <td></td>
                                </tr>
                                
                            </table>
                        </div>
                        <div style="width: 100%; float: left;font-size: 14px; margin-bottom: 10px;color:#2e4064; margin-bottom: 10px;">
                            <img src="https://altaproductividadapr.com/public/img/cot_footer.jpg" width="100%">
                        </div>

                        
                    </div>
                </div>';

        $this->email->message($message);
        foreach ($query_f->result() as $itemf) {
            $urlfile=FCPATH.'uploads/filestemp/'.$itemf->file;
            $this->email->attach($urlfile);
        }
        //Enviamos el email y si se produce bien o mal que avise con una flasdata
        if($this->email->send()){
            //$this->session->set_flashdata('envio', 'Email enviado correctamente');
            $this->ModeloCatalogos->updateCatalogo('clientes',array('bodymail'=>$comen),array('id'=>$idCliente));
        }else{
            //$this->session->set_flashdata('envio', 'No se a enviado el email');
        }
        $array=array(
                    'newcodigo'=>date('YmdGis')
                );
        echo json_encode($array);
        //==================
    }
    function getdireccionequipos($equipo,$serie,$fecha){
        //hay una version de esta funcion directamente en ModeloCatalogos->getdireccionequipos2 por si se va a hacer algun ajuste a este
        $resultdir=$this->ModeloCatalogos->db2_getselectwheren('contrato_equipo_direccion',array('equiposrow'=>$equipo,'serieId'=>$serie));
        $iddireccionget='g_0';
        $direccion='';
        foreach ($resultdir->result() as $itemdir) {
            if($itemdir->direccion!=''){
                $iddireccionget=$itemdir->direccion;
            }
        }
        //log_message('error','iddireccionget '.$iddireccionget);
        if($iddireccionget!='g_0'){
            $tipodireccion=explode('_',$iddireccionget);

            $tipodir = $tipodireccion[0];
            $iddir   = $tipodireccion[1];
            if ($tipodir=='g') {
                $resultdirc_where['idclientedirecc']=$iddir;
                
                if($fecha>=$this->fechahoy){
                    //log_message('error','entra direccion g');
                    $resultdirc_where['status']=1;
                }
                $resultdirc=$this->ModeloCatalogos->db2_getselectwheren('clientes_direccion',$resultdirc_where);
                foreach ($resultdirc->result() as $item) {
                    $direccion=$item->direccion;
                }
            }
            if ($tipodir=='f') {
                $resultdirc_where['id']=$iddir;
                
                if($fecha>=$this->fechahoy){
                    //log_message('error','entra direccion f');
                    $resultdirc_where['activo']=1;
                }
                $resultdirc=$this->ModeloCatalogos->db2_getselectwheren('cliente_has_datos_fiscales',$resultdirc_where);
                foreach ($resultdirc->result() as $item) {
                    $direccion='Razon social: '.$item->razon_social.' Colonia:'.$item->colonia.' Calle:'.$item->calle.' Numero:'.$item->num_ext;
                }
            }
        }
        return $direccion;
    }
    function envio_c($idenvio,$asi,$tipo,$per,$perbbc,$contratos){
                $bbcarray[]='info@altaproductividadapr.com';
        //==================================================
                 $strq = "SELECT * FROM envio_mail where id='$idenvio' ";
                
                $query_a = $this->db->query($strq);
                foreach ($query_a->result() as $item) {
                    $folio=$item->folio;
                    //$tipo=$item->tipo;
                    $comen=$item->comen;
                    $asunto=$item->asunto;
                }
                $strqcont = "SELECT emc.contratoid,con.idRenta,cli.empresa,ren.idCliente FROM envio_mail_contrato as emc 
                            INNER JOIN contrato as con on con.idcontrato=emc.contratoid
                            INNER JOIN rentas as ren on ren.id=con.idRenta
                            INNER JOIN clientes as cli on cli.id=ren.idCliente
                            WHERE emc.idenvio='$idenvio'";
                $query_cont = $this->db->query($strqcont);
                $empresa='';
                foreach ($query_cont->result() as $item) {
                    $empresa=$item->empresa;
                    $idCliente = $item->idCliente;
                }
        //===================================================================================
                $strqc="SELECT cli.* FROM envio_mail_contacto as emc INNER JOIN cliente_datoscontacto as cli on cli.datosId=emc.idcontacto WHERE emc.idenvio='$idenvio'";
                $query_c = $this->db->query($strqc);
                $contactosarray = array();
                if($this->enviosproductivo==0){
                    $contactosarray[]='ag-gerardo@hotmail.com';
                }
                foreach ($query_c->result() as $item) {
                    if($this->enviosproductivo==1){
                        $contactosarray[]=$item->email; //se comento para que no se le envien correos al cliente
                    }
                    
                }
        //==================================================
                $strqf="SELECT im.file FROM envio_mail_file as emf INNER JOIN img_temp as im on im.id=emf.idfile WHERE emf.idenvio='$idenvio'";
                $query_f = $this->db->query($strqf);
        //===================================================
                $strqp="SELECT * FROM personal as per WHERE per.personalId='$per'";
                $query_p = $this->db->query($strqp);
                $query_p = $query_p->row();
        //=====================================================
        //===================================================
                $strqpbbc="SELECT * FROM personal as per WHERE per.personalId='$perbbc'";
                $query_pbbc = $this->db->query($strqpbbc);
                foreach ($query_pbbc->result() as $itembbc) {
                    $bbcarray[]=$itembbc->email;
                }
        //=====================================================
        if($idenvio>0){
            
            $atencionpara='';
            //================================
                //cargamos la libreria email
                $this->load->library('email');

                /*
                * Configuramos los parámetros para enviar el email,
                * las siguientes configuraciones es recomendable
                * hacerlas en el fichero email.php dentro del directorio config,
                * en este caso para hacer un ejemplo rápido lo hacemos 
                * en el propio controlador
                */
                
                //Indicamos el protocolo a utilizar
                $config['protocol'] = 'smtp';
                 
                //El servidor de correo que utilizaremos
                $config["smtp_host"] ='mail.altaproductividadapr.com'; 
                 
                //Nuestro usuario
                $config["smtp_user"] = 'info@altaproductividadapr.com';

                //Nuestra contraseña
                $config["smtp_pass"] = 'yUs&J=T4*V$J';

                //Puerto
                $config["smtp_port"] = '465';

                $config["smtp_crypto"] = 'ssl';
                        
                //El juego de caracteres a utilizar
                $config['charset'] = 'utf-8'; 
         
                //Permitimos que se puedan cortar palabras
                $config['wordwrap'] = TRUE;
                 
                //El email debe ser valido  
                $config['validate'] = true;

                $config['mailtype'] = 'html';

                //Establecemos esta configuración
                $this->email->initialize($config);
                //$asunto='Consumibles';
                //Ponemos la dirección de correo que enviará el email y un nombre
                $this->email->from('info@altaproductividadapr.com',$asunto);
                 
                  /*
                   * Ponemos el o los destinatarios para los que va el email
                   * en este caso al ser un formulario de contacto te lo enviarás a ti
                   * mismo
                   */
            //======================
            //$this->email->bcc('andres@mangoo.mx');
            $this->email->bcc($bbcarray);
            $this->email->to($contactosarray);
            $this->email->reply_to($query_p->email, $query_p->nombre.' '.$query_p->apellido_paterno.' '.$query_p->apellido_materno);
            //$this->email->to($correo, $empresa);
            //==========================================================================
                $empresa='';
                $contacto='';
                $telefono='';
                $servicio='';
                $fecha='';
                $tecnico='';
                $equipos='';
                //log_message('error','asignacion: '.$asi.' tipo: '.$tipo);
                //$contactosarray=array();
                if($tipo==1){
                    $strq_uni="SELECT `ace`.`asignacionIde`, `per`.`personalId`, 
                                CONCAT(per.nombre, ' ', `per`.`apellido_paterno`, ' ', per.apellido_materno) as tecnico, `cli`.`id`, `cli`.`empresa`, 
                            ace.fecha, 
                            ace.hora,
                            ace.horafin, 
                            `rde`.`modelo`, `serp`.`serie`, 
                            ace.status, 
                            ac.tiposervicio, 
                            `ac`.`asignacionId`, `sereve`.`nombre` as `servicio`,
                            ac.prioridad,
                            rde.id as id_equipo,
                            serp.serieId,
                            con.contacto,
                            con.telefono,
                            ac.comentariocal,
                            con.servicio_equipo,
                            con.servicio_doc,
                            ac.tservicio_a,
                            ace.confirmado,
                            pol.nombre as poliza,
                            ren.idCliente,
                            ac.contacto_text,
                            cli.equipo_acceso,
                            cli.documentacion_acceso,
                            ac.equipo_acceso_info,
                            ac.documentacion_acceso_info,
                            ac.tserviciomotivo,
                            ac.tpoliza,
                            con.idRenta,
                            rde.idEquipo,
                            asre.comentario,
                            asre.ubicacion,
                            asre.contadores,
                            ace.nr,
                            ren.correo,
                            CONCAT(per2.nombre, ' ', per2.apellido_paterno, ' ', per2.apellido_materno) as tecnico2,
                            CONCAT(per3.nombre, ' ', per3.apellido_paterno, ' ', per3.apellido_materno) as tecnico3
                            FROM asignacion_ser_contrato_a_e ace
                            JOIN asignacion_ser_contrato_a ac ON ac.asignacionId=ace.asignacionId
                            LEFT JOIN personal per ON per.personalId=ace.tecnico
                            LEFT JOIN personal per2 ON per2.personalId=ace.tecnico2
                            LEFT JOIN personal per3 ON per3.personalId=ace.tecnico3
                            JOIN contrato con ON con.idcontrato=ac.contratoId
                            JOIN rentas ren ON ren.id=con.idRenta
                            JOIN clientes cli ON cli.id=ren.idCliente
                            JOIN rutas ru ON ru.id=ac.zonaId
                            JOIN rentas_has_detallesEquipos rde ON rde.id=ace.idequipo
                            JOIN series_productos serp ON serp.serieId=ace.serieId
                            left JOIN polizas as pol on pol.id=ac.tpoliza
                            left JOIN servicio_evento sereve ON sereve.id=ac.tservicio
                            left JOIN polizas_detalles as pold on pold.id=ac.tservicio_a
                            JOIN asignacion_series_r_equipos asre ON rde.id=asre.id_equipo and asre.serieId=serp.serieId ";
                            //===================================================
                                $sql_cons=$strq_uni." where ace.activo=1  and ac.asignacionId=$asi ";
                                $query_cons = $this->db->query($sql_cons);
                                $fecha='';
                                foreach ($query_cons->result() as $item) {
                                    $fecha=$item->fecha;
                                }
                            //=====================================================







                            $strq='Select * FROM (';
                            $strq.='(';
                                $strq.=$strq_uni;
                                $strq.=" where ace.activo=1  and ac.asignacionId=$asi ";
                            $strq.=')';
                                $DATAc = json_decode($contratos);
                                for ($i=0;$i<count($DATAc);$i++) {
                                    $idcon=$DATAc[$i]->con;
                                    $strq.=' UNION ';
                                    $strq.='(';
                                    $strq.=$strq_uni;
                                    $strq.=" where ace.activo=1 and ac.contratoId='$idcon' and  ace.fecha='$fecha' ";
                                    $strq.=')';
                                }
                            $strq.=') AS datos';





                            $query = $this->db->query($strq);
                            foreach ($query->result() as $item) {
                                $empresa = $item->empresa;
                                $contacto=$item->contacto;
                                $telefono=$item->telefono;
                                $servicio = $item->poliza;
                                $fecha = $item->fecha;
                                //$correo = $item->correo;
                                $ubicacion = $item->ubicacion;
                                $tecnico = $item->tecnico.', '.$item->tecnico2.', '.$item->tecnico3;
                                $direccionequipo= $this->getdireccionequipos($item->id_equipo,$item->serieId,$fecha);
                                $equipos .='<tr><td>'.$item->modelo.' </td><td>'.$item->serie.'</td><td style="font-size:10px;">'.$direccionequipo.' '.$ubicacion.'</td></tr>';
                            }
                }
                if($tipo==2){
                    $resultado=$this->ModeloAsignacion->getdatospoliza($asi,0,0);
                    foreach ($resultado->result() as $item) {
                        $empresa = $item->empresa;
                        $contacto=$item->atencion;
                        $telefono=$item->telefono;
                        $servicio = $item->servicio;
                        $fecha = $item->fecha;
                        $tecnico = $item->tecnico.', '.$item->tecnico2;
                        //$correo =$item->correo;
                        $equipos .='<tr><td>'.$item->modelo.'</td><td>'.$item->serie.'</td><td style="font-size:10px;">'.$item->direccionservicio.'</td></tr>';
                    }
                }
                if($tipo==3){
                    $resultado=$this->ModeloAsignacion->getdatoscliente($asi);
                    foreach ($resultado->result() as $item) {
                        $empresa = $item->empresa;
                        $servicio = $item->servicio;
                        $contacto=$item->contacto_text;
                        $servicio = $item->poliza;
                        $fecha = $item->fecha;
                        $tecnico = $item->tecnico.', '.$item->tecnico2;
                        //$correo=$email;
                        if($item->retiro==1){
                            $modelo_eq=$item->modeloretiro;
                        }else{
                            $modelo_eq=$item->modelo;
                        }
                        $equipos .='<tr><td>'.$modelo_eq.'</td><td>'.$item->serie.'</td><td style="font-size:10px;">'.$item->direccion.'</td>'.$td_comen.'</tr>';
                    }
                }
                if ($tipo==4) {
                    $resultado=$this->ModeloAsignacion->getdatosventas($asi);
                    foreach ($resultado->result() as $item) {
                        $empresa = $item->empresa;
                        $servicio = $item->servicio;
                        $contacto=$item->atencionpara;
                        $telefono=$item->telefono;
                        $tecnico = $item->tecnico;
                        $fecha = $item->fecha;
                        $tecnico = $item->tecnico;
                        $ventaId = $item->ventaId;
                        $ventaIdd = $item->ventaIdd;
                        $tipov = $item->tipov;
                        $direccion = $item->direccion;
                        //$correo = $item->email;
                        //==============================================
                            if ($tipov==1) {
                                $reesequi = $this->ModeloCatalogos->db13_getselectwheren('ventacombinada',array('combinadaId'=>$ventaIdd));
                                foreach ($reesequi->result() as $item) {
                                    $ventaIdd=$item->equipo;
                                }
                            }
                            $resultadoequipos = $this->ModeloCatalogos->db13_getselectwheren('ventas_has_detallesEquipos',array('idVenta'=>$ventaIdd));
                            $resultadoaccesorios = $this->ModeloCatalogos->accesoriosventa($ventaIdd);
                            $resultadoconsumibles = $this->ModeloCatalogos->consumiblesventa($ventaIdd);
                            $consumiblesventadetalles = $this->ModeloCatalogos->consumiblesventadetalles($ventaIdd);
                            $polizaventadetalles = $this->ModeloCatalogos->polizaventadetalles($ventaIdd);
                            $ventadetallesrefacion = $this->ModeloCatalogos->ventas_has_detallesRefacciones($ventaIdd);

                            foreach ($resultadoequipos->result() as $item) { 
                                $model=$this->ModeloCatalogos->obtenernumeroserieequipo($item->idEquipo);
                                $resuleserie=$this->ModeloCatalogos->getequiposerie($item->id);

                                foreach ($resuleserie->result() as $itemse) { 
                                    $equipos .='<tr><td>'.$item->modelo.'</td><td>'.$itemse->serie.'</td><td style="font-size:10px;">'.$direccion.'</td></tr>';
                                }
                                
                            }
                            foreach ($resultadoaccesorios->result() as $item) { 
                                $resuleserie=$this->ModeloCatalogos->getaccesorioserie($item->id_accesoriod);
                                foreach ($resuleserie->result() as $itemse) { 
                                    $equipos .='<tr><td>'.$item->nombre.'</td><td>'.$itemse->serie.'</td><td style="font-size:10px;">'.$direccion.'</td></tr>';
                                }
                            }
                            foreach ($resultadoconsumibles->result() as $item) { 
                                $equipos .='<tr><td>'.$estatuseq.''.$item->modelo.'</td><td></td><td style="font-size:10px;">'.$direccion.'</td></tr>';
                            }
                            foreach ($consumiblesventadetalles->result() as $item) { 
                                $equipos .='<tr><td>'.$item->modelo.'</td><td><table>
                                                                                <tr>
                                                                                    <td width="50%"></td>
                                                                                    <td width="50%" style="color: #625e5e; font-size: 11px;">Equipo referencia<br>'.$item->modeloeq.'</td></tr></table></td><td style="font-size:10px;">'.$direccion.'</td></tr>';
                            }
                            foreach ($ventadetallesrefacion->result() as $item) {
                                $resuleserie=$this->ModeloCatalogos->getrefaccionserie($item->id);
                                foreach ($resuleserie->result() as $itemse) {
                                    $equipos .='<tr><td>'.$item->modelo.'</td><td><table><tr><td rowspan="2">'.$itemse->serie.'</td><td style="color: #625e5e; font-size: 11px;">Equipo referencia<br>'.$item->modeloeq.'</td></tr><tr><td style="color: #625e5e; font-size: 11px;">'.$item->serieeq.'</td></tr></table> </td><td style="font-size:10px;">'.$direccion.'</td></tr>';
                                }
                            }
                        //==============================================
                    }
                }
            //==========================================================================
            //===================================================================================
                $strqc="SELECT cli.* FROM envio_mail_contacto as emc INNER JOIN cliente_datoscontacto as cli on cli.datosId=emc.idcontacto WHERE emc.idenvio='$idenvio'";
                $query_c = $this->db->query($strqc);
                $contactosarray = array();
                foreach ($query_c->result() as $item) {
                    $contacto=$item->atencionpara;
                    $telefono=$item->telefono;
                }
            //==================================================

            //Definimos el asunto del mensaje
                $this->email->subject($asunto);
            
            //$codigocon=$this->encript($contratoId);


            $url="https://altaproductividadapr.com/consumibles_sol?id=$folio";

            $message  = '<div style="background: url(https://altaproductividadapr.com/public/img/fondo.jpg) #eaeaea;width: 100%;height: 94%;min-height: 752px;padding-top: 25px;margin: 0px;">
                    <div style="width: 10%; height: 10px; float: left;">
                        
                    </div>
                    <div style="background: white; border-top: 3px solid #ff0101; padding: 25px 25px 0px 25px; width: 80%; max-width: 825px;margin-left: auto; margin-right: auto; float: left; -webkit-box-shadow: 10px 10px 42px -6px rgba(0,0,0,0.75);-moz-box-shadow: 10px 10px 42px -6px rgba(0,0,0,0.75);box-shadow: 10px 10px 42px -6px rgba(0,0,0,0.75);">
                        <div style="width: 100%; float: left;">
                            <div style="width: 30%;float: left;">
                                <img src="https://altaproductividadapr.com/public/img/cotheader2.png" style="max-height: 100px;">
                            </div>
                            <div style="width: 40%;float: left; min-height:10px; font-style: italic; font-size: 14px; color:#2e4064;">
                                Impresoras y Multifunciones, Venta, Renta y Mantenimiento. Blvd. Norte 1831 Col. Villas San Alejandro. Puebla, Pue. <br>Call Center 273 3400, 249 5393. www.kyoceraap.com
                            </div>
                            <div style="width: 30%;float: left;">
                                <img src="https://altaproductividadapr.com/public/img/alta.jpg" style="max-height: 100px;">
                            </div>
                        </div>
                        <div style="width: 100%; float: left;">
                            <div style="width: 50%;float: left;height: 10px;"></div>
                            <div style="width: 50%;float: left; border-bottom: 3px solid #ff0101; font-size:17px"><b>Confirmación Servicio</b></div>
                        </div>
                        <div style="width: 100%; float: left;font-size: 14px; color:#2e4064;">
                            <div style="width: 30%;float: left;">Atención para:</div>
                            <div style="width: 70%;float: left;">'.$empresa.'</div>
                        </div>
                        <div style="width: 100%; float: left;font-size: 14px; color:#2e4064;">
                            <div style="width: 30%;float: left;">Contacto:</div>
                            <div style="width: 70%;float: left;">'.$contacto.'</div>
                        </div>
                        <div style="width: 100%; float: left;font-size: 14px; color:#2e4064;">
                            <div style="width: 30%;float: left;">Teléfono:</div>
                            <div style="width: 70%;float: left;">'.$telefono.'</div>
                        </div>
                        <div style="width: 100%; float: left;font-size: 14px; color:#2e4064; margin-bottom: 10px;">
                            <div style="width: 30%;float: left;">Tipo servicio:</div>
                            <div style="width: 70%;float: left;">'.$servicio.'</div>
                        </div>
                        <div style="width: 100%; float: left;font-size: 14px; ">
                            <div style="width: 30%;float: left;color:#2e4064;">Día programado de servicio:</div>
                            <div style="width: 70%;float: left; color: red;">'.$fecha.'</div>
                        </div>
                        <div style="width: 100%; float: left;font-size: 14px; margin-bottom: 10px;">
                            <div style="width: 30%;float: left;color:#2e4064;">Técnicos programados:</div>
                            <div style="width: 70%;float: left; color: red;">'.$tecnico.'</div>
                        </div>
                        <div style="width: 100%; float: left;font-size: 14px; margin-bottom: 10px;color:#2e4064; margin-bottom: 10px;">
                            <table style="width:100%">
                                <thead>
                                    <tr ><th style="border-bottom: 3px solid #ff0101;">Modelo</th><th style="border-bottom: 3px solid #ff0101;">Serie</th><th style="border-bottom: 3px solid #ff0101;">Dirección</th></tr>
                                </thead>
                                <tbody>
                                    '.$equipos.'
                                </tbody>
                            </table>
                        </div>
                        <div style="width: 100%; float: left;">
                            <div style="width: 50%;float: left;height: 10px;"></div>
                            <div style="width: 50%;float: left; border-bottom: 3px solid #ff0101; font-size:18px"><b>Solicitud de consumibles</b></div>
                        </div>
                        <!--<div style="width: 100%; float: left;font-size: 15px; color:#2e4064;">
                            <p><b>Estimado '.$empresa.'</b></p>
                        </div>-->
                        <div style="width: 100%; float: left;font-size: 14px; color:#2e4064; text-align: justify;">
                            <p>Por favor solicitamos de su apoyo para indicarnos el número de botes vacíos de tóner en su stock, para realizar su canje por nuevos. Mediante el siguiente link podrá capturar el número y tipo de consumibles:</p>
                        </div>
                        <div style="width: 100%; float: left;font-size: 14px; color:#2e4064; text-align: center;">
                            <a href="'.$url.'" style="background-color:red;color: white;text-decoration: none;border-radius: 24px;padding-top: 12px;padding-bottom: 12px;padding-left: 24px;padding-right: 24px;"><b>Ir a solicitar</b></a>
                        </div>
                        <div style="width: 100%; float: left;font-size: 14px; color:#2e4064; text-align: justify;">

                        </div>
                        <div style="width: 100%; float: left;font-size: 14px; margin-bottom: 10px;color:#2e4064; margin-bottom: 10px; text-align: justify;">
                            '.$comen.'
                        </div>
                        <div style="width: 100%; float: left;font-size: 14px; margin-bottom: 10px;color:#2e4064; margin-bottom: 10px;">
                            <table style="width: 100%;">
                                <tr>
                                    <td></td>
                                    <td style="text-decoration: none;color: #455a64;font-weight: bold;">'.$query_p->nombre.' '.$query_p->apellido_paterno.' '.$query_p->apellido_materno.'</td>
                                    <td></td>
                                    <td><a href="mailto:'.$query_p->email.'" style="text-decoration: none;color: #455a64;font-weight: bold;">'.$query_p->email.'</a></td>
                                    <td></td>
                                </tr>
                                
                            </table>
                        </div>
                        <div style="width: 100%; float: left;font-size: 14px; margin-bottom: 10px;color:#2e4064; margin-bottom: 10px;">
                            <img src="https://altaproductividadapr.com/public/img/cot_footer.jpg" width="100%">
                        </div>

                        
                    </div>
                </div>';

            $this->email->message($message);
            //$this->email->attach(base_url().$datoscomplemento->rutaXml);
            //$this->email->attach(base_url().'files_sat/facturaspdf/Complemento_'.$serie.'_'.$Folio.'.pdf');
            foreach ($query_f->result() as $itemf) {
                $urlfile=FCPATH.'uploads/filestemp/'.$itemf->file;
                $this->email->attach($urlfile);
            }
            //Enviamos el email y si se produce bien o mal que avise con una flasdata
            if($this->email->send()){
                //$this->session->set_flashdata('envio', 'Email enviado correctamente');
                $this->ModeloCatalogos->updateCatalogo('clientes',array('bodymail'=>$comen),array('id'=>$idCliente));
            }else{
                //$this->session->set_flashdata('envio', 'No se a enviado el email');
            }

            //==================
        }
    }
    function encript($id){
        $this->load->library('encryption');
        $codigocon=$this->encryption->encrypt($id);
        
        $conteo = substr_count($codigocon, '+');
        if($conteo>0){
            //echo '<br>1<br>';
            $codigocon=$this->encryption->encrypt($id);

            $conteo = substr_count($codigocon, '+');
            if($conteo>0){
                //echo '<br>2<br>';
                $codigocon=$this->encryption->encrypt($id);

                $conteo = substr_count($codigocon, '+');
                if($conteo>0){
                    //echo '<br>3<br>';
                    $codigocon=$this->encryption->encrypt($id);

                    $conteo = substr_count($codigocon, '+');
                    if($conteo>0){
                        //echo '<br>4<br>';
                        $codigocon=$this->encryption->encrypt($id);

                        $conteo = substr_count($codigocon, '+');
                        if($conteo>0){
                            //echo '<br>5<br>';
                            $codigocon=$this->encryption->encrypt($id);

                            $conteo = substr_count($codigocon, '+');
                            if($conteo>0){
                                //echo '<br>6<br>';
                                $codigocon=$this->encryption->encrypt($id);
                            }
                        }
                    }
                }
            }
        }
        
        /*
        $codigocond=$this->encryption->decrypt($codigocon);
        if($codigocond>0){
            $codigocon=$this->encryption->encrypt($id);
            //=================================
                $codigocond=$this->encryption->decrypt($codigocon);
                if($codigocond>0){
                    $codigocon=$this->encryption->encrypt($id);
                    //=================================
                        $codigocond=$this->encryption->decrypt($codigocon);
                        if($codigocond>0){
                            $codigocon=$this->encryption->encrypt($id);
                            //=================================
                                $codigocond=$this->encryption->decrypt($codigocon);
                                if($codigocond>0){
                                    $codigocon=$this->encryption->encrypt($id);
                                    //=================================
                                        $codigocond=$this->encryption->decrypt($codigocon);
                                        if($codigocond>0){
                                            $codigocon=$this->encryption->encrypt($id);
                                            //=================================
                                                $codigocond=$this->encryption->decrypt($codigocon);
                                                if($codigocond>0){
                                                    $codigocon=$this->encryption->encrypt($id);
                                                    
                                                }
                                            //=================================
                                        }
                                    //=================================
                                }
                            //=================================
                        }
                    //=================================
                }
            //=================================
        }
        */
        //echo $codigocon;
        //echo '<br>';
        //$codigocond=$this->encryption->decrypt($codigocon);
        //echo $codigocond;
        return $codigocon;
    }
    function decrypt(){
        $codigocon=$_GET['cod'];
        https://altaproductividadapr.com/Mailenvio/decrypt?cod=435823205ef7a1d7d36d0a674d13e9e855785596efb920434429213fbf86a41ae7a4a17474f372f586616fb2498ef68020307fb2d63a9c45b30adc04d519f597GPa4UTQnENtqH5Qu/u3TotiVY+zUnqsziqFKeWHjDP0=
                                        //   $codigocon='435823205ef7a1d7d36d0a674d13e9e855785596efb920434429213fbf86a41ae7a4a17474f372f586616fb2498ef68020307fb2d63a9c45b30adc04d519f597GPa4UTQnENtqH5Qu/u3TotiVY+zUnqsziqFKeWHjDP0=';
        echo $codigocon;

        $this->load->library('encryption');
        $codigocond=$this->encryption->decrypt($codigocon);
        echo $codigocond;
    }
    function generalenvio(){
        $params = $this->input->post();
        $op = $params['op'];
        $tipo = $params['tipo'];
        $asunto = $params['asunto'];
        $comen = $params['comen'];
        $contratos = $params['contratos'];
        $contactos = $params['contactos'];
        $files = $params['files'];

        $asi = $params['asi'];
        $tipos = $params['tipos'];
        $per = $params['per'];
        $perbbc = $params['perbbc'];

        $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $codigo=date('Ymd-His').'-'.substr(str_shuffle($permitted_chars), 0, 16);

        $datainter['folio']=$codigo;
        $datainter['tipo']=$op;
        $datainter['comen']=$comen;
        $datainter['asunto']=$asunto;
        $idenvio=$this->ModeloCatalogos->Insert('envio_mail',$datainter);
        
        $DATAc = json_decode($contratos);
        for ($i=0;$i<count($DATAc);$i++) {
            $dataic['idenvio']=$idenvio;
            $dataic['contratoid']=$DATAc[$i]->con;
            $this->ModeloCatalogos->Insert('envio_mail_contrato',$dataic);
        }
        $DATAcc = json_decode($contactos);
        for ($j=0;$j<count($DATAcc);$j++) {
            $dataicc['idenvio']=$idenvio;
            $dataicc['idcontacto']=$DATAcc[$j]->cont;
            $this->ModeloCatalogos->Insert('envio_mail_contacto',$dataicc);
        }
        $DATAfi = json_decode($files);
        for ($l=0;$l<count($DATAfi);$l++) {
            $dataifi['idenvio']=$idenvio;
            $dataifi['idfile']=$DATAfi[$l]->fil;
            $this->ModeloCatalogos->Insert('envio_mail_file',$dataifi);
        }
        if($tipo==0){
            $this->envio_c($idenvio,$asi,$tipos,$per,$perbbc,$contratos);
        }
        $array=array(
                    'codigo'=>$codigo,
                    'idenvio'=>$idenvio,
                    'tipo'=>$tipo,
                    'newcodigo'=>date('YmdGis')
                );
        echo json_encode($array);

    }
    function envio_fac(){
        $bbcarray[]='info@altaproductividadapr.com';
        $params = $this->input->post();
        //log_message('error',json_encode($params));
        $idfac=$params['idfac'];

        $code=$params['code'];
        $asunto=$params['asunto'];
        $comen=$params['comen'];
        $email=$params['email'];
        $per=$params['per'];
        $perbbc=$params['perbbc'];

        $strqf="SELECT im.file FROM img_temp as im WHERE im.code='$code'";
                $query_f = $this->db->query($strqf);

        $strqfac="SELECT * FROM f_facturas  WHERE FacturasId='$idfac'";
                $query_fac = $this->db->query($strqfac);
                $query_fac=$query_fac->row();
        //===================================================
                $strqp="SELECT * FROM personal as per WHERE per.personalId='$per'";
                $query_p = $this->db->query($strqp);
                $query_p = $query_p->row();
        //=====================================================
        //===================================================
                $strqpbbc="SELECT * FROM personal as per WHERE per.personalId='$perbbc'";
                $query_pbbc = $this->db->query($strqpbbc);
                foreach ($query_pbbc->result() as $itembbc) {
                    $bbcarray[]=$itembbc->email;
                }
        //=====================================================
        $atencionpara='';
        //================================
            //cargamos la libreria email
            $this->load->library('email');

            /*
            * Configuramos los parámetros para enviar el email,
            * las siguientes configuraciones es recomendable
            * hacerlas en el fichero email.php dentro del directorio config,
            * en este caso para hacer un ejemplo rápido lo hacemos 
            * en el propio controlador
            */
            
            //Indicamos el protocolo a utilizar
            $config['protocol'] = 'smtp';
             
            //El servidor de correo que utilizaremos
            $config["smtp_host"] ='mail.altaproductividadapr.com'; 
             
            //Nuestro usuario
            $config["smtp_user"] = 'info@altaproductividadapr.com';

            //Nuestra contraseña
            $config["smtp_pass"] = 'yUs&J=T4*V$J';

            //Puerto
            $config["smtp_port"] = '465';

            $config["smtp_crypto"] = 'ssl';
                    
            //El juego de caracteres a utilizar
            $config['charset'] = 'utf-8'; 
     
            //Permitimos que se puedan cortar palabras
            $config['wordwrap'] = TRUE;
             
            //El email debe ser valido  
            $config['validate'] = true;

            $config['mailtype'] = 'html';

            //Establecemos esta configuración
            $this->email->initialize($config);
            //$asunto='Servicio';
            //Ponemos la dirección de correo que enviará el email y un nombre
            $this->email->from('info@altaproductividadapr.com',$asunto);
             
              /*
               * Ponemos el o los destinatarios para los que va el email
               * en este caso al ser un formulario de contacto te lo enviarás a ti
               * mismo
               */
            //======================
            //$this->email->bcc('andres@mangoo.mx');
            $this->email->bcc($bbcarray);
            $this->email->reply_to($query_p->email, $query_p->nombre.' '.$query_p->apellido_paterno.' '.$query_p->apellido_materno);
            
            if($this->enviosproductivo==1){
                $correo=json_decode($email);
            }else{
                $correo=array('ag-gerardo@hotmail.com');
                
            }
        
            
            
            

            //Definimos el asunto del mensaje
            $this->email->subject($asunto);
            
            $this->email->to($correo);


        $message  = '<div style="background: url(https://altaproductividadapr.com/public/img/fondo.jpg) #eaeaea;width: 100%;height: 94%;min-height: 752px;padding-top: 25px;margin: 0px;">
                    <div style="width: 10%; height: 10px; float: left;">
                        
                    </div>
                    <div style="background: white; border-top: 3px solid #ff0101; padding: 25px 25px 0px 25px; width: 80%; max-width: 825px;margin-left: auto; margin-right: auto; float: left; -webkit-box-shadow: 10px 10px 42px -6px rgba(0,0,0,0.75);-moz-box-shadow: 10px 10px 42px -6px rgba(0,0,0,0.75);box-shadow: 10px 10px 42px -6px rgba(0,0,0,0.75);">
                        <div style="width: 100%; float: left;">
                            <div style="width: 30%;float: left;">
                                <img src="https://altaproductividadapr.com/public/img/cotheader2.png" style="max-height: 100px;">
                            </div>
                            <div style="width: 40%;float: left; min-height:10px; font-style: italic; font-size: 14px; color:#2e4064;">
                                Impresoras y Multifunciones, Venta, Renta y Mantenimiento. Blvd. Norte 1831 Col. Villas San Alejandro. Puebla, Pue. <br>Call Center 273 3400, 249 5393. www.kyoceraap.com
                            </div>
                            <div style="width: 30%;float: left;">
                                <img src="https://altaproductividadapr.com/public/img/alta.jpg" style="max-height: 100px;">
                            </div>
                        </div>
                        <div style="width: 100%; float: left;">
                            <div style="width: 50%;float: left;height: 10px;"></div>
                            <div style="width: 50%;float: left; border-bottom: 3px solid #ff0101; font-size:17px"><b>Facturación </b></div>
                        </div>
                        <div style="width: 100%; float: left;font-size: 14px; color:#2e4064;">
                            <div style="width: 30%;float: left;">Facturado:</div>
                            <div style="width: 70%;float: left;">'.$query_fac->Nombre.'</div>
                        </div>
                        <div style="width: 100%; float: left;font-size: 14px; color:#2e4064;">
                            <div style="width: 30%;float: left;">Folio:</div>
                            <div style="width: 70%;float: left;">'.$query_fac->serie.''.$query_fac->Folio.'</div>
                        </div>
                        
                        
                        <div style="width: 100%; float: left;font-size: 14px; margin-bottom: 10px;color:#2e4064; margin-bottom: 10px; text-align:justify">
                            '.$comen.'
                        </div>
                        <div style="width: 100%; float: left;font-size: 14px; margin-bottom: 10px;color:#2e4064; margin-bottom: 10px;">
                            
                        </div>
                        <div style="width: 100%; float: left;font-size: 14px; margin-bottom: 10px;color:#2e4064; margin-bottom: 10px;">
                            <table style="width: 100%;">
                                <tr>
                                    <td></td>
                                    <td style="text-decoration: none;color: #455a64;font-weight: bold;">'.$query_p->nombre.' '.$query_p->apellido_paterno.' '.$query_p->apellido_materno.'</td>
                                    <td></td>
                                    <td><a href="mailto:'.$query_p->email.'" style="text-decoration: none;color: #455a64;font-weight: bold;">'.$query_p->email.'</a></td>
                                    <td></td>
                                </tr>
                                
                            </table>
                        </div>
                        <div style="width: 100%; float: left;font-size: 14px; margin-bottom: 10px;color:#2e4064; margin-bottom: 10px;">
                            <img src="https://altaproductividadapr.com/public/img/cot_footer.jpg" width="100%">
                        </div>

                        
                    </div>
                </div>';

        $this->email->message($message);

        $urlfac=FCPATH.'kyocera/facturaspdf/'.$query_fac->serie.''.$query_fac->Folio.'.pdf';
        $this->email->attach($urlfac);
        
        $urlxml=FCPATH.$query_fac->rutaXml;
        $this->email->attach($urlxml);
        
        foreach ($query_f->result() as $itemf) {
            $urlfile=FCPATH.'uploads/filestemp/'.$itemf->file;
            $this->email->attach($urlfile);
        }
        //Enviamos el email y si se produce bien o mal que avise con una flasdata
        if($this->email->send()){
            //$this->session->set_flashdata('envio', 'Email enviado correctamente');
            $this->ModeloCatalogos->updateCatalogo('f_facturas',array('envio_m_p'=>$this->idpersonal,'envio_m_d'=>$this->fechahoy),array('FacturasId'=>$idfac));
        }else{
            //$this->session->set_flashdata('envio', 'No se a enviado el email');
        }
        $array=array(
                    'newcodigo'=>date('YmdGis')
                );
        echo json_encode($array);
        //==================
    }
    function envio_comp(){
        $bbcarray[]='info@altaproductividadapr.com';
        $params = $this->input->post();

        $idfac=$params['idfac'];

        $code=$params['code'];
        $asunto=$params['asunto'];
        $comen=$params['comen'];
        $email=$params['email'];
        $per=$params['per'];
        $perbbc=$params['perbbc'];

        $strqf="SELECT im.file FROM img_temp as im WHERE im.code='$code'";
                $query_f = $this->db->query($strqf);

        $strqfac="SELECT * FROM f_complementopago  WHERE complementoId='$idfac'";
                $query_fac = $this->db->query($strqfac);
                $query_fac=$query_fac->row();
        //===================================================
                $strqp="SELECT * FROM personal as per WHERE per.personalId='$per'";
                $query_p = $this->db->query($strqp);
                $query_p = $query_p->row();
        //=====================================================
        //===================================================
                $strqpbbc="SELECT * FROM personal as per WHERE per.personalId='$perbbc'";
                $query_pbbc = $this->db->query($strqpbbc);
                foreach ($query_pbbc->result() as $itembbc) {
                    $bbcarray[]=$itembbc->email;
                }
        //=====================================================
        $atencionpara='';
        //================================
            //cargamos la libreria email
            $this->load->library('email');

            /*
            * Configuramos los parámetros para enviar el email,
            * las siguientes configuraciones es recomendable
            * hacerlas en el fichero email.php dentro del directorio config,
            * en este caso para hacer un ejemplo rápido lo hacemos 
            * en el propio controlador
            */
            
            //Indicamos el protocolo a utilizar
            $config['protocol'] = 'smtp';
             
            //El servidor de correo que utilizaremos
            $config["smtp_host"] ='mail.altaproductividadapr.com'; 
             
            //Nuestro usuario
            $config["smtp_user"] = 'info@altaproductividadapr.com';

            //Nuestra contraseña
            $config["smtp_pass"] = 'yUs&J=T4*V$J';

            //Puerto
            $config["smtp_port"] = '465';

            $config["smtp_crypto"] = 'ssl';
                    
            //El juego de caracteres a utilizar
            $config['charset'] = 'utf-8'; 
     
            //Permitimos que se puedan cortar palabras
            $config['wordwrap'] = TRUE;
             
            //El email debe ser valido  
            $config['validate'] = true;

            $config['mailtype'] = 'html';

            //Establecemos esta configuración
            $this->email->initialize($config);
            //$asunto='Servicio';
            //Ponemos la dirección de correo que enviará el email y un nombre
            $this->email->from('info@altaproductividadapr.com',$asunto);
             
              /*
               * Ponemos el o los destinatarios para los que va el email
               * en este caso al ser un formulario de contacto te lo enviarás a ti
               * mismo
               */
            //======================
            //$this->email->bcc('andres@mangoo.mx');
            $this->email->bcc($bbcarray);
            $this->email->reply_to($query_p->email, $query_p->nombre.' '.$query_p->apellido_paterno.' '.$query_p->apellido_materno);
            
            if($this->enviosproductivo==1){
                $correo=json_decode($email);
            }else{
                $correo=array('ag-gerardo@hotmail.com');
                
            }
        
            
            
            

            //Definimos el asunto del mensaje
            $this->email->subject($asunto);
            
            $this->email->to($correo);


        $message  = '<div style="background: url(https://altaproductividadapr.com/public/img/fondo.jpg) #eaeaea;width: 100%;height: 94%;min-height: 752px;padding-top: 25px;margin: 0px;">
                    <div style="width: 10%; height: 10px; float: left;">
                        
                    </div>
                    <div style="background: white; border-top: 3px solid #ff0101; padding: 25px 25px 0px 25px; width: 80%; max-width: 825px;margin-left: auto; margin-right: auto; float: left; -webkit-box-shadow: 10px 10px 42px -6px rgba(0,0,0,0.75);-moz-box-shadow: 10px 10px 42px -6px rgba(0,0,0,0.75);box-shadow: 10px 10px 42px -6px rgba(0,0,0,0.75);">
                        <div style="width: 100%; float: left;">
                            <div style="width: 30%;float: left;">
                                <img src="https://altaproductividadapr.com/public/img/cotheader2.png" style="max-height: 100px;">
                            </div>
                            <div style="width: 40%;float: left; min-height:10px; font-style: italic; font-size: 14px; color:#2e4064;">
                                Impresoras y Multifunciones, Venta, Renta y Mantenimiento. Blvd. Norte 1831 Col. Villas San Alejandro. Puebla, Pue. <br>Call Center 273 3400, 249 5393. www.kyoceraap.com
                            </div>
                            <div style="width: 30%;float: left;">
                                <img src="https://altaproductividadapr.com/public/img/alta.jpg" style="max-height: 100px;">
                            </div>
                        </div>
                        <div style="width: 100%; float: left;">
                            <div style="width: 50%;float: left;height: 10px;"></div>
                            <div style="width: 50%;float: left; border-bottom: 3px solid #ff0101; font-size:17px"><b>Complemento </b></div>
                        </div>
                        <div style="width: 100%; float: left;font-size: 14px; color:#2e4064;">
                            <div style="width: 30%;float: left;">Facturado:</div>
                            <div style="width: 70%;float: left;">'.$query_fac->R_nombre.'</div>
                        </div>
                        <div style="width: 100%; float: left;font-size: 14px; margin-bottom: 10px;color:#2e4064; margin-bottom: 10px; text-align:justify">
                            '.$comen.'
                        </div>
                        <div style="width: 100%; float: left;font-size: 14px; margin-bottom: 10px;color:#2e4064; margin-bottom: 10px;"></div>
                        <div style="width: 100%; float: left;font-size: 14px; margin-bottom: 10px;color:#2e4064; margin-bottom: 10px;">
                            <table style="width: 100%;">
                                <tr>
                                    <td></td>
                                    <td style="text-decoration: none;color: #455a64;font-weight: bold;">'.$query_p->nombre.' '.$query_p->apellido_paterno.' '.$query_p->apellido_materno.'</td>
                                    <td></td>
                                    <td><a href="mailto:'.$query_p->email.'" style="text-decoration: none;color: #455a64;font-weight: bold;">'.$query_p->email.'</a></td>
                                    <td></td>
                                </tr>
                                
                            </table>
                        </div>
                        <div style="width: 100%; float: left;font-size: 14px; margin-bottom: 10px;color:#2e4064; margin-bottom: 10px;">
                            <img src="https://altaproductividadapr.com/public/img/cot_footer.jpg" width="100%">
                        </div>

                        
                    </div>
                </div>';

        $this->email->message($message);

        $urlfac=FCPATH.'kyocera/facturaspdf/complemento_'.$query_fac->complementoId.'.pdf';
        $this->email->attach($urlfac);
        
        $urlxml=FCPATH.$query_fac->rutaXml;
        $this->email->attach($urlxml);
        
        foreach ($query_f->result() as $itemf) {
            $urlfile=FCPATH.'uploads/filestemp/'.$itemf->file;
            $this->email->attach($urlfile);
        }
        //Enviamos el email y si se produce bien o mal que avise con una flasdata
        if($this->email->send()){
            //$this->session->set_flashdata('envio', 'Email enviado correctamente');
            $this->ModeloCatalogos->updateCatalogo('f_complementopago',array('envio_m_p'=>$this->idpersonal,'envio_m_d'=>$this->fechahoy),array('complementoId'=>$idfac));
        }else{
            //$this->session->set_flashdata('envio', 'No se a enviado el email');
        }
        $array=array(
                    'newcodigo'=>date('YmdGis')
                );
        echo json_encode($array);
        //==================
    }
    function envio_fac_all(){
        $bbcarray[]='info@altaproductividadapr.com';
        $params = $this->input->post();

        $idfac=$params['idfac'];

        $code=$params['code'];
        $asunto=$params['asunto'];
        $comen=$params['comen'];
        $email=$params['email'];
        $per=$params['per'];
        $perbbc=$params['perbbc'];

        $strqf="SELECT im.file FROM img_temp as im WHERE im.code='$code'";
                $query_f = $this->db->query($strqf);
        /*
        $strqfac="SELECT * FROM f_facturas  WHERE FacturasId='$idfac'";
                $query_fac = $this->db->query($strqfac);
                $query_fac=$query_fac->row();
        */

                $this->db->select('*');
                $this->db->from('f_facturas fac');
                $this->db->group_start();
        $DATAf = json_decode($idfac);
        for ($i=0;$i<count($DATAf);$i++) {
                $this->db->or_like('fac.FacturasId',$DATAf[$i]->FacturasIds);
        }
                $this->db->group_end(); 
                $query_fac=$this->db->get();
                $query_fac_row=$query_fac->row();
        //===================================================
                $strqp="SELECT * FROM personal as per WHERE per.personalId='$per'";
                $query_p = $this->db->query($strqp);
                $query_p = $query_p->row();
        //=====================================================
        //===================================================
                if($perbbc>0){
                    $strqpbbc="SELECT * FROM personal as per WHERE per.personalId='$perbbc'";
                    $query_pbbc = $this->db->query($strqpbbc);
                    foreach ($query_pbbc as $itembbc) {
                        $bbcarray[]=$itembbc->email;
                    }
                }
        //=====================================================
        $atencionpara='';
        //================================
            //cargamos la libreria email
            $this->load->library('email');

            /*
            * Configuramos los parámetros para enviar el email,
            * las siguientes configuraciones es recomendable
            * hacerlas en el fichero email.php dentro del directorio config,
            * en este caso para hacer un ejemplo rápido lo hacemos 
            * en el propio controlador
            */
            
            //Indicamos el protocolo a utilizar
            $config['protocol'] = 'smtp';
             
            //El servidor de correo que utilizaremos
            $config["smtp_host"] ='mail.altaproductividadapr.com'; 
             
            //Nuestro usuario
            $config["smtp_user"] = 'info@altaproductividadapr.com';

            //Nuestra contraseña
            $config["smtp_pass"] = 'yUs&J=T4*V$J';

            //Puerto
            $config["smtp_port"] = '465';

            $config["smtp_crypto"] = 'ssl';
                    
            //El juego de caracteres a utilizar
            $config['charset'] = 'utf-8'; 
     
            //Permitimos que se puedan cortar palabras
            $config['wordwrap'] = TRUE;
             
            //El email debe ser valido  
            $config['validate'] = true;

            $config['mailtype'] = 'html';

            //Establecemos esta configuración
            $this->email->initialize($config);
            //$asunto='Servicio';
            //Ponemos la dirección de correo que enviará el email y un nombre
            $this->email->from('info@altaproductividadapr.com',$asunto);
             
              /*
               * Ponemos el o los destinatarios para los que va el email
               * en este caso al ser un formulario de contacto te lo enviarás a ti
               * mismo
               */
            //======================
            //$this->email->bcc('andres@mangoo.mx');
            $this->email->bcc($bbcarray);
            $this->email->reply_to($query_p->email, $query_p->nombre.' '.$query_p->apellido_paterno.' '.$query_p->apellido_materno);
            
            if($this->enviosproductivo==1){
                $correo=json_decode($email);
            }else{
                $correo=array('ag-gerardo@hotmail.com');
                
            }
        
            
            
            

            //Definimos el asunto del mensaje
            $this->email->subject($asunto);
            
            $this->email->to($correo);


        $message  = '<div style="background: url(https://altaproductividadapr.com/public/img/fondo.jpg) #eaeaea;width: 100%;height: 94%;min-height: 752px;padding-top: 25px;margin: 0px;">
                    <div style="width: 10%; height: 10px; float: left;">
                        
                    </div>
                    <div style="background: white; border-top: 3px solid #ff0101; padding: 25px 25px 0px 25px; width: 80%; max-width: 825px;margin-left: auto; margin-right: auto; float: left; -webkit-box-shadow: 10px 10px 42px -6px rgba(0,0,0,0.75);-moz-box-shadow: 10px 10px 42px -6px rgba(0,0,0,0.75);box-shadow: 10px 10px 42px -6px rgba(0,0,0,0.75);">
                        <div style="width: 100%; float: left;">
                            <div style="width: 30%;float: left;">
                                <img src="https://altaproductividadapr.com/public/img/cotheader2.png" style="max-height: 100px;">
                            </div>
                            <div style="width: 40%;float: left; min-height:10px; font-style: italic; font-size: 14px; color:#2e4064;">
                                Impresoras y Multifunciones, Venta, Renta y Mantenimiento. Blvd. Norte 1831 Col. Villas San Alejandro. Puebla, Pue. <br>Call Center 273 3400, 249 5393. www.kyoceraap.com
                            </div>
                            <div style="width: 30%;float: left;">
                                <img src="https://altaproductividadapr.com/public/img/alta.jpg" style="max-height: 100px;">
                            </div>
                        </div>
                        <div style="width: 100%; float: left;">
                            <div style="width: 50%;float: left;height: 10px;"></div>
                            <div style="width: 50%;float: left; border-bottom: 3px solid #ff0101; font-size:17px"><b>Facturación </b></div>
                        </div>
                        <div style="width: 100%; float: left;font-size: 14px; color:#2e4064;">
                            <div style="width: 30%;float: left;">Facturado:</div>
                            <div style="width: 70%;float: left;">'.$query_fac_row->Nombre.'</div>
                        </div>';
                        foreach ($query_fac->result() as $itemf) {
        $message  .= ' <div style="width: 100%; float: left;font-size: 14px; color:#2e4064;">
                            <div style="width: 30%;float: left;">Folio:</div>
                            <div style="width: 70%;float: left;">'.$itemf->serie.''.$itemf->Folio.'</div>
                        </div>';
                        }
        
                                    
        $message  .= '<div style="width: 100%; float: left;font-size: 14px; margin-bottom: 10px;color:#2e4064; margin-bottom: 10px; text-align:justify">
                            '.$comen.'
                        </div>
                        <div style="width: 100%; float: left;font-size: 14px; margin-bottom: 10px;color:#2e4064; margin-bottom: 10px;">
                            
                        </div>
                        <div style="width: 100%; float: left;font-size: 14px; margin-bottom: 10px;color:#2e4064; margin-bottom: 10px;">
                            <table style="width: 100%;">
                                <tr>
                                    <td></td>
                                    <td style="text-decoration: none;color: #455a64;font-weight: bold;">'.$query_p->nombre.' '.$query_p->apellido_paterno.' '.$query_p->apellido_materno.'</td>
                                    <td></td>
                                    <td><a href="mailto:'.$query_p->email.'" style="text-decoration: none;color: #455a64;font-weight: bold;">'.$query_p->email.'</a></td>
                                    <td></td>
                                </tr>
                                
                            </table>
                        </div>
                        <div style="width: 100%; float: left;font-size: 14px; margin-bottom: 10px;color:#2e4064; margin-bottom: 10px;">
                            <img src="https://altaproductividadapr.com/public/img/cot_footer.jpg" width="100%">
                        </div>

                        
                    </div>
                </div>';

        $this->email->message($message);
        foreach ($query_fac->result() as $itemf) {
            $urlfac=FCPATH.'kyocera/facturaspdf/'.$itemf->serie.''.$itemf->Folio.'.pdf';
            $this->email->attach($urlfac);
            
            $urlxml=FCPATH.$itemf->rutaXml;
            $this->email->attach($urlxml);
        }
        
        foreach ($query_f->result() as $itemf) {
            $urlfile=FCPATH.'uploads/filestemp/'.$itemf->file;
            $this->email->attach($urlfile);
        }
        //Enviamos el email y si se produce bien o mal que avise con una flasdata
        if($this->email->send()){
            //$this->session->set_flashdata('envio', 'Email enviado correctamente');
            $this->ModeloCatalogos->updateCatalogo('f_facturas',array('envio_m_p'=>$this->idpersonal,'envio_m_d'=>$this->fechahoy),array('FacturasId'=>$idfac));
        }else{
            //$this->session->set_flashdata('envio', 'No se a enviado el email');
        }
        $array=array(
                    'newcodigo'=>date('YmdGis')
                );
        echo json_encode($array);
        //==================
    }
    function passservicio(){
        $params = $this->input->post();
        $mail = $params['mail'];
        $pin = $params['pin'];
        $asig = $params['asig'];
        $tipo = $params['tipo'];
        if($tipo==1){//renta
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_contrato_a',array('pass'=>$pin,'pass_personal'=>$this->idpersonal),array('asignacionId'=>$asig));
        }
        if($tipo==2){//poliza
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_poliza_a',array('pass'=>$pin,'pass_personal'=>$this->idpersonal),array('asignacionId'=>$asig));
        }
        if($tipo==3){//eventi cliente
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_cliente_a',array('pass'=>$pin,'pass_personal'=>$this->idpersonal),array('asignacionId'=>$asig));
        }
        $this->envio_pin($pin,$mail,$asig,$tipo);
    }
    function envio_pin($pin,$mail,$asig,$tipo){
        $bbcarray[]='info@altaproductividadapr.com';
        $pass='';
        if($tipo==1){
            $resultado = $this->ModeloAsignacion->getdatosrenta($asig,'',0,0);
            foreach ($resultado->result() as $item) {
                $pass=$item->pass;
                $fecha=$item->fecha;
                $servicio = $item->servicio;
                $empresa =$item->empresa;
                if($item->tservicio_a>0){
                    $servicio = $item->poliza;
                }
            }
        }
        if($tipo==2){
            $resultado=$this->ModeloAsignacion->getdatospoliza($asig,0,0);
            foreach ($resultado->result() as $item) {
                $pass=$item->pass;
                $fecha=$item->fecha;
                $servicio = $item->servicio;
                $empresa =$item->empresa;
            }
        }
        if($tipo==3){
            $resultado=$this->ModeloAsignacion->getdatoscliente($asig);
            foreach ($resultado->result() as $item) {
                $pass=$item->pass;
                $fecha=$item->fecha;
                $servicio = $item->servicio;
                $empresa =$item->empresa;
                if($item->tservicio_a>0){
                    $servicio = $item->poliza;
                }
            }
        }
        
        $atencionpara='';
        //================================
            //cargamos la libreria email
            $this->load->library('email');

            /*
            * Configuramos los parámetros para enviar el email,
            * las siguientes configuraciones es recomendable
            * hacerlas en el fichero email.php dentro del directorio config,
            * en este caso para hacer un ejemplo rápido lo hacemos 
            * en el propio controlador
            */
            
            //Indicamos el protocolo a utilizar
            $config['protocol'] = 'smtp';
             
            //El servidor de correo que utilizaremos
            $config["smtp_host"] ='mail.altaproductividadapr.com'; 
             
            //Nuestro usuario
            $config["smtp_user"] = 'info@altaproductividadapr.com';

            //Nuestra contraseña
            $config["smtp_pass"] = 'yUs&J=T4*V$J';

            //Puerto
            $config["smtp_port"] = '465';

            $config["smtp_crypto"] = 'ssl';
                    
            //El juego de caracteres a utilizar
            $config['charset'] = 'utf-8'; 
     
            //Permitimos que se puedan cortar palabras
            $config['wordwrap'] = TRUE;
             
            //El email debe ser valido  
            $config['validate'] = true;

            $config['mailtype'] = 'html';

            //Establecemos esta configuración
            $this->email->initialize($config);
            $asunto='PIN de verificación';
            //Ponemos la dirección de correo que enviará el email y un nombre
            $this->email->from('info@altaproductividadapr.com',$asunto);
             
              /*
               * Ponemos el o los destinatarios para los que va el email
               * en este caso al ser un formulario de contacto te lo enviarás a ti
               * mismo
               */
            //======================
            //$this->email->bcc('andres@mangoo.mx');
            $this->email->bcc($bbcarray);
            //$this->email->reply_to($query_p->email, $query_p->nombre.' '.$query_p->apellido_paterno.' '.$query_p->apellido_materno);
            
            if($this->enviosproductivo==1){
                //$correo=json_decode($email);
                $correo=array($mail);
            }else{
                $correo=array('ag-gerardo@hotmail.com');
                
            }
        
            
            
            

            //Definimos el asunto del mensaje
            $this->email->subject($asunto);
            
            $this->email->to($correo);


            $message  = '<table border="0" cellpadding="0" cellspacing="0" id="m_3473502346079385518m_-1195920292316544843body" style="text-align:center;min-width:640px;width:100%;margin:0;padding:0" bgcolor="#fafafa">
            <tbody>
                <tr>
                    <td style="font-family:&quot;Helvetica Neue&quot;,Helvetica,Arial,sans-serif;height:4px;font-size:4px;line-height:4px" bgcolor="red"></td>
                </tr>
                <tr>
                    <td style="font-family:&quot;Helvetica Neue&quot;,Helvetica,Arial,sans-serif;font-size:13px;line-height:1.6;color:#5c5c5c;padding:25px 0">
                        <img alt="GitLab" src="https://altaproductividadapr.com/public/img/alta.jpg"  height="76" class="CToWUd" data-bit="iit">
                    </td>
                </tr>
                <tr>
                    <td style="font-family:&quot;Helvetica Neue&quot;,Helvetica,Arial,sans-serif">
                        <table border="0" cellpadding="0" cellspacing="0" style="width:640px;border-collapse:separate;border-spacing:0;margin:0 auto">
                            <tbody>
                                <tr>
                                    <td style="font-family:&quot;Helvetica Neue&quot;,Helvetica,Arial,sans-serif;border-radius:3px;overflow:hidden;padding:18px 25px;border:1px solid #ededed" align="left" bgcolor="#fff">
                                        <table border="0" cellpadding="0" cellspacing="0" style="width:100%;border-collapse:separate;border-spacing:0">
                                            <tbody>
                                                <tr><td><div style="color:#1f1f1f;line-height:1.25em;max-width:400px;margin:0 auto" align="center">
                                                    <h3>PIN para finalización de servicio</h3>
                                                    <p style="font-size:1em">'.$empresa.'</p>
                                                    <p style="font-size:0.9em">Favor de proporcionar el siguiente PIN al técnico que le proporciona el servicio para poder finalizar.</p>
                                                    <p style="font-size:0.9em">
                                                        Servicio : '.$servicio.'
                                                    </p>
                                                    <p style="font-size:0.9em">
                                                        Fecha de servicio : '.$fecha.'
                                                    </p>
                                                    <div style="width:207px;height:53px;background-color:#f0f0f0;line-height:53px;font-weight:700;font-size:1.5em;color:#303030;margin:26px 0">'.$pass.'</div>
                                                    <p style="font-size:0.75em">Su código de verificación solo sera valido para el servicio actual.</p>
                                                </div>
                                            </td></tr></tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="font-family:&quot;Helvetica Neue&quot;,Helvetica,Arial,sans-serif;font-size:13px;line-height:1.6;color:#5c5c5c;padding:25px 0">
                        <img alt="kyocera" src="https://altaproductividadapr.com/app-assets/images/1024_kyocera_logo.png" style="display:block;width:90px;margin:0 auto 1em" class="CToWUd" data-bit="iit">
                        <div>Estás recibiendo este correo electrónico para ayudar a la validación del servicio.</a></div>
                    </td>
                </tr>
                <tr>
                    <td style="font-family:&quot;Helvetica Neue&quot;,Helvetica,Arial,sans-serif;font-size:13px;line-height:1.6;color:#5c5c5c;padding:25px 0"></td>
                </tr>
            </tbody>
        </table>';

        $this->email->message($message);
        /*
        foreach ($query_fac->result() as $itemf) {
            $urlfac=FCPATH.'kyocera/facturaspdf/'.$itemf->serie.''.$itemf->Folio.'.pdf';
            $this->email->attach($urlfac);
            
            $urlxml=FCPATH.$itemf->rutaXml;
            $this->email->attach($urlxml);
        }
        
        foreach ($query_f->result() as $itemf) {
            $urlfile=FCPATH.'uploads/filestemp/'.$itemf->file;
            $this->email->attach($urlfile);
        }
        */
        //Enviamos el email y si se produce bien o mal que avise con una flasdata
        if($this->email->send()){
            //$this->session->set_flashdata('envio', 'Email enviado correctamente');
            $enviomail=1;
        }else{
            //$this->session->set_flashdata('envio', 'No se a enviado el email');
            $enviomail=0;
        }
        $array=array(
                    'enviomail'=>$enviomail
                );
        echo json_encode($array);
        //==================
    }
    function envio_cotizacion(){
        $bcc_array[]='info@altaproductividadapr.com';
        $params = $this->input->post();
        $idcotizacion=$params['asi'];
        //$tipo=$params['tipo'];
        $per=$params['per'];
        $perbbc=$params['perbbc'];
        //$code=$params['code'];
        $asunto=$params['asunto'];
        $comen=$params['comen'];
        $email=$params['email'];
        $idCliente=0;

        
        $strqp="SELECT * FROM personal as per WHERE per.personalId='$per'";
        $query_p = $this->db->query($strqp);
        $query_p = $query_p->row();

        $strqpbbc="SELECT * FROM personal as per WHERE per.personalId='$perbbc'";
        $query_pbbc = $this->db->query($strqpbbc);
        foreach ($query_pbbc->result() as $itembbc) {
            $bcc_array[]=$itembbc->email;
        }


        $atencionpara='';
        //================================
            //cargamos la libreria email
            $this->load->library('email');

            /*
            * Configuramos los parámetros para enviar el email,
            * las siguientes configuraciones es recomendable
            * hacerlas en el fichero email.php dentro del directorio config,
            * en este caso para hacer un ejemplo rápido lo hacemos 
            * en el propio controlador
            */
            
            //Indicamos el protocolo a utilizar
            $config['protocol'] = 'smtp';
             
            //El servidor de correo que utilizaremos
            $config["smtp_host"] ='mail.altaproductividadapr.com'; 
             
            //Nuestro usuario
            $config["smtp_user"] = 'info@altaproductividadapr.com';

            //Nuestra contraseña
            $config["smtp_pass"] = 'yUs&J=T4*V$J';

            //Puerto
            $config["smtp_port"] = '465';

            $config["smtp_crypto"] = 'ssl';
                    
            //El juego de caracteres a utilizar
            $config['charset'] = 'utf-8'; 
     
            //Permitimos que se puedan cortar palabras
            $config['wordwrap'] = TRUE;
             
            //El email debe ser valido  
            $config['validate'] = true;

            $config['mailtype'] = 'html';

            //Establecemos esta configuración
            $this->email->initialize($config);
            //$asunto='Servicio';
            //Ponemos la dirección de correo que enviará el email y un nombre
            $this->email->from('info@altaproductividadapr.com',$asunto);
             
              /*
               * Ponemos el o los destinatarios para los que va el email
               * en este caso al ser un formulario de contacto te lo enviarás a ti
               * mismo
               */
        //======================
            //$this->email->bcc('andres@mangoo.mx');
            $this->email->bcc($bcc_array);
            $this->email->reply_to($query_p->email, $query_p->nombre.' '.$query_p->apellido_paterno.' '.$query_p->apellido_materno);
        
        
            
            $correo='ag-gerardo@hotmail.com';
            if($this->enviosproductivo==1){
                $correo = $email;
            }
            //Definimos el asunto del mensaje
                $this->email->subject($asunto);
            $empresa='';
            $contacto='';
            $telefono='';
            $servicio='';
            $fecha='';
            $tecnico='';
            $equipos='';
            //log_message('error','asignacion: '.$asi.' tipo: '.$tipo);
            
            
            $result_mc = $this->ModeloCatalogos->getselectwheren('cliente_datoscontacto',array('activo'=>1,'email'=>$correo));
            foreach ($result_mc->result() as $itemmc) {
                $contacto=$itemmc->atencionpara;
                $telefono=$itemmc->telefono;
            }
            $result_cot = $this->ModeloCatalogos->getselectwheren('cotizaciones',array('id'=>$idcotizacion));
            $idCliente=0;
            foreach ($result_cot->result() as $itemcot) {
                $idCliente=$itemcot->idCliente;
            }
            $result_cli = $this->ModeloCatalogos->getselectwheren('clientes',array('id'=>$idCliente));
            $empresa=0;
            foreach ($result_cli->result() as $itemcli) {
                $empresa=$itemcli->empresa;
            }

            $this->email->to($correo);


        $message  = '<div style="background: url(https://altaproductividadapr.com/public/img/fondo.jpg) #eaeaea;width: 100%;height: 94%;min-height: 752px;padding-top: 25px;margin: 0px;">
                    <div style="width: 10%; height: 10px; float: left;">
                        
                    </div>
                    <div style="background: white; border-top: 3px solid #ff0101; padding: 25px 25px 0px 25px; width: 80%; max-width: 825px;margin-left: auto; margin-right: auto; float: left; -webkit-box-shadow: 10px 10px 42px -6px rgba(0,0,0,0.75);-moz-box-shadow: 10px 10px 42px -6px rgba(0,0,0,0.75);box-shadow: 10px 10px 42px -6px rgba(0,0,0,0.75);">
                        <div style="width: 100%; float: left;">
                            <div style="width: 30%;float: left;">
                                <img src="https://altaproductividadapr.com/public/img/cotheader2.png" style="max-height: 100px;">
                            </div>
                            <div style="width: 40%;float: left; min-height:10px; font-style: italic; font-size: 14px; color:#2e4064;">
                                Impresoras y Multifunciones, Venta, Renta y Mantenimiento. Blvd. Norte 1831 Col. Villas San Alejandro. Puebla, Pue. <br>Call Center 273 3400, 249 5393. www.kyoceraap.com
                            </div>
                            <div style="width: 30%;float: left;">
                                <img src="https://altaproductividadapr.com/public/img/alta.jpg" style="max-height: 100px;">
                            </div>
                        </div>
                        <div style="width: 100%; float: left;">
                            <div style="width: 50%;float: left;height: 10px;"></div>
                            <div style="width: 50%;float: left; border-bottom: 3px solid #ff0101; font-size:17px"><b>Solicitud de cotización</b></div>
                        </div>
                        <div style="width: 100%; float: left;font-size: 14px; color:#2e4064;">
                            <div style="width: 30%;float: left;">Atención para:</div>
                            <div style="width: 70%;float: left;">'.$empresa.'</div>
                        </div>
                        <div style="width: 100%; float: left;font-size: 14px; color:#2e4064;">
                            <div style="width: 30%;float: left;">Contacto:</div>
                            <div style="width: 70%;float: left;">'.$contacto.'</div>
                        </div>
                        <div style="width: 100%; float: left;font-size: 14px; color:#2e4064;">
                            <div style="width: 30%;float: left;">Teléfono:</div>
                            <div style="width: 70%;float: left;">'.$telefono.'</div>
                        </div>
                        <div style="width: 100%; float: left;font-size: 14px; color:#2e4064; margin-bottom: 10px;">
                            <div style="width: 30%;float: left;">Cotización:</div>
                            <div style="width: 70%;float: left;">'.$idcotizacion.'</div>
                        </div>
                        
                        
                        
                        <div style="width: 100%; float: left;font-size: 14px; margin-bottom: 10px;color:#2e4064; margin-bottom: 10px; text-align:justify">
                            '.$comen.'
                        </div>
                        <div style="width: 100%; float: left;font-size: 14px; margin-bottom: 10px;color:#2e4064; margin-bottom: 10px;">
                            
                        </div>
                        <div style="width: 100%; float: left;font-size: 14px; margin-bottom: 10px;color:#2e4064; margin-bottom: 10px;">
                            <table style="width: 100%;">
                                <tr>
                                    <td></td>
                                    <td style="text-decoration: none;color: #455a64;font-weight: bold;">'.$query_p->nombre.' '.$query_p->apellido_paterno.' '.$query_p->apellido_materno.'</td>
                                    <td></td>
                                    <td><a href="mailto:'.$query_p->email.'" style="text-decoration: none;color: #455a64;font-weight: bold;">'.$query_p->email.'</a></td>
                                    <td></td>
                                </tr>
                                
                            </table>
                        </div>
                        <div style="width: 100%; float: left;font-size: 14px; margin-bottom: 10px;color:#2e4064; margin-bottom: 10px;">
                            <img src="https://altaproductividadapr.com/public/img/cot_footer.jpg" width="100%">
                        </div>

                        
                    </div>
                </div>';

        $this->email->message($message);
        /*
        foreach ($query_f->result() as $itemf) {
            $urlfile=FCPATH.'uploads/filestemp/'.$itemf->file;
            $this->email->attach($urlfile);
        }
        */
        $urlfile=FCPATH.'doccotizacion/cotizacion_'.$idcotizacion.'.pdf';
        $this->email->attach($urlfile);
        //Enviamos el email y si se produce bien o mal que avise con una flasdata
        if($this->email->send()){
            //$this->session->set_flashdata('envio', 'Email enviado correctamente');
            $envio=1;
        }else{
            $envio=0;
            //$this->session->set_flashdata('envio', 'No se a enviado el email');
        }
        $array=array(
                    'envio'=>$envio
                );
        echo json_encode($array);
        //==================
    }
    function envio_estadocuenta(){
        $bcc_array[]='info@altaproductividadapr.com';
        $params = $this->input->post();
        $idCliente=$params['cliente'];
        $codi=$params['codi'];
        //$tipo=$params['tipo'];
        $per=$params['per'];
        $perbbc=$params['perbbc'];
        //$code=$params['code'];
        $asunto=$params['asunto'];
        $comen=$params['comen'];
        $email=$params['email'];
        //$idCliente=0;

        
        $strqp="SELECT * FROM personal as per WHERE per.personalId='$per'";
        $query_p = $this->db->query($strqp);
        $query_p = $query_p->row();

        $strqpbbc="SELECT * FROM personal as per WHERE per.personalId='$perbbc'";
        $query_pbbc = $this->db->query($strqpbbc);
        foreach ($query_pbbc->result() as $itembbc) {
            $bcc_array[]=$itembbc->email;
        }


        $atencionpara='';
        //================================
            //cargamos la libreria email
            $this->load->library('email');

            /*
            * Configuramos los parámetros para enviar el email,
            * las siguientes configuraciones es recomendable
            * hacerlas en el fichero email.php dentro del directorio config,
            * en este caso para hacer un ejemplo rápido lo hacemos 
            * en el propio controlador
            */
            
            //Indicamos el protocolo a utilizar
            $config['protocol'] = 'smtp';
             
            //El servidor de correo que utilizaremos
            $config["smtp_host"] ='mail.altaproductividadapr.com'; 
             
            //Nuestro usuario
            $config["smtp_user"] = 'info@altaproductividadapr.com';

            //Nuestra contraseña
            $config["smtp_pass"] = 'yUs&J=T4*V$J';

            //Puerto
            $config["smtp_port"] = '465';

            $config["smtp_crypto"] = 'ssl';
                    
            //El juego de caracteres a utilizar
            $config['charset'] = 'utf-8'; 
     
            //Permitimos que se puedan cortar palabras
            $config['wordwrap'] = TRUE;
             
            //El email debe ser valido  
            $config['validate'] = true;

            $config['mailtype'] = 'html';

            //Establecemos esta configuración
            $this->email->initialize($config);
            //$asunto='Servicio';
            //Ponemos la dirección de correo que enviará el email y un nombre
            $this->email->from('info@altaproductividadapr.com',$asunto);
             
              /*
               * Ponemos el o los destinatarios para los que va el email
               * en este caso al ser un formulario de contacto te lo enviarás a ti
               * mismo
               */
        //======================
            //$this->email->bcc('andres@mangoo.mx');
            $this->email->bcc($bcc_array);
            $this->email->reply_to($query_p->email, $query_p->nombre.' '.$query_p->apellido_paterno.' '.$query_p->apellido_materno);
        
        
            
            $correo='ag-gerardo@hotmail.com';
            if($this->enviosproductivo==1){
                $correo = $email;
            }
            //Definimos el asunto del mensaje
                $this->email->subject($asunto);
            $empresa='';
            $contacto='';
            $telefono='';
            $servicio='';
            $fecha='';
            $tecnico='';
            $equipos='';
            //log_message('error','asignacion: '.$asi.' tipo: '.$tipo);
            
            
            $result_mc = $this->ModeloCatalogos->getselectwheren('cliente_datoscontacto',array('activo'=>1,'email'=>$correo));
            foreach ($result_mc->result() as $itemmc) {
                $contacto=$itemmc->atencionpara;
                $telefono=$itemmc->telefono;
            }
            

            $result_cli = $this->ModeloCatalogos->getselectwheren('clientes',array('id'=>$idCliente));
            $empresa=0;
            foreach ($result_cli->result() as $itemcli) {
                $empresa=$itemcli->empresa;
            }

            $this->email->to($correo);


        $message  = '<div style="background: url(https://altaproductividadapr.com/public/img/fondo.jpg) #eaeaea;width: 100%;height: 94%;min-height: 752px;padding-top: 25px;margin: 0px;">
                    <div style="width: 10%; height: 10px; float: left;">
                        
                    </div>
                    <div style="background: white; border-top: 3px solid #ff0101; padding: 25px 25px 0px 25px; width: 80%; max-width: 825px;margin-left: auto; margin-right: auto; float: left; -webkit-box-shadow: 10px 10px 42px -6px rgba(0,0,0,0.75);-moz-box-shadow: 10px 10px 42px -6px rgba(0,0,0,0.75);box-shadow: 10px 10px 42px -6px rgba(0,0,0,0.75);">
                        <div style="width: 100%; float: left;">
                            <div style="width: 30%;float: left;">
                                <img src="https://altaproductividadapr.com/public/img/cotheader2.png" style="max-height: 100px;">
                            </div>
                            <div style="width: 40%;float: left; min-height:10px; font-style: italic; font-size: 14px; color:#2e4064;">
                                Impresoras y Multifunciones, Venta, Renta y Mantenimiento. Blvd. Norte 1831 Col. Villas San Alejandro. Puebla, Pue. <br>Call Center 273 3400, 249 5393. www.kyoceraap.com
                            </div>
                            <div style="width: 30%;float: left;">
                                <img src="https://altaproductividadapr.com/public/img/alta.jpg" style="max-height: 100px;">
                            </div>
                        </div>
                        <div style="width: 100%; float: left;">
                            <div style="width: 50%;float: left;height: 10px;"></div>
                            <div style="width: 50%;float: left; border-bottom: 3px solid #ff0101; font-size:17px"><b>Solicitud de cotización</b></div>
                        </div>
                        <div style="width: 100%; float: left;font-size: 14px; color:#2e4064;">
                            <div style="width: 30%;float: left;">Atención para:</div>
                            <div style="width: 70%;float: left;">'.$empresa.'</div>
                        </div>
                        <div style="width: 100%; float: left;font-size: 14px; color:#2e4064;">
                            <div style="width: 30%;float: left;">Contacto:</div>
                            <div style="width: 70%;float: left;">'.$contacto.'</div>
                        </div>
                        <div style="width: 100%; float: left;font-size: 14px; color:#2e4064;">
                            <div style="width: 30%;float: left;">Teléfono:</div>
                            <div style="width: 70%;float: left;">'.$telefono.'</div>
                        </div>
                        
                        
                        
                        
                        <div style="width: 100%; float: left;font-size: 14px; margin-bottom: 10px;color:#2e4064; margin-bottom: 10px; text-align:justify">
                            '.$comen.'
                        </div>
                        <div style="width: 100%; float: left;font-size: 14px; margin-bottom: 10px;color:#2e4064; margin-bottom: 10px;">
                            
                        </div>
                        <div style="width: 100%; float: left;font-size: 14px; margin-bottom: 10px;color:#2e4064; margin-bottom: 10px;">
                            <table style="width: 100%;">
                                <tr>
                                    <td></td>
                                    <td style="text-decoration: none;color: #455a64;font-weight: bold;">'.$query_p->nombre.' '.$query_p->apellido_paterno.' '.$query_p->apellido_materno.'</td>
                                    <td></td>
                                    <td><a href="mailto:'.$query_p->email.'" style="text-decoration: none;color: #455a64;font-weight: bold;">'.$query_p->email.'</a></td>
                                    <td></td>
                                </tr>
                                
                            </table>
                        </div>
                        <div style="width: 100%; float: left;font-size: 14px; margin-bottom: 10px;color:#2e4064; margin-bottom: 10px;">
                            <img src="https://altaproductividadapr.com/public/img/cot_footer.jpg" width="100%">
                        </div>

                        
                    </div>
                </div>';

        $this->email->message($message);
        /*
        foreach ($query_f->result() as $itemf) {
            $urlfile=FCPATH.'uploads/filestemp/'.$itemf->file;
            $this->email->attach($urlfile);
        }
        */
        $urlfile=FCPATH.'doccotizacion/EC_'.$idCliente.'_'.$codi.'.pdf';
        $this->email->attach($urlfile);
        //Enviamos el email y si se produce bien o mal que avise con una flasdata
        if($this->email->send()){
            //$this->session->set_flashdata('envio', 'Email enviado correctamente');
            $envio=1;
        }else{
            $envio=0;
            //$this->session->set_flashdata('envio', 'No se a enviado el email');
        }
        $array=array(
                    'envio'=>$envio
                );
        echo json_encode($array);
        //==================
    }
    function envio_reporte_excedentes(){
        $bcc_array[]='info@altaproductividadapr.com';
        $params = $this->input->post();
        $fichero=$params['asi'];
        //$tipo=$params['tipo'];
        $per=$params['per'];
        $perbbc=$params['perbbc'];
        //$code=$params['code'];
        $asunto=$params['asunto'];
        $comen=$params['comen'];
        $email=$params['email'];
        $idCliente=$params['idcli'];

        
        $strqp="SELECT * FROM personal as per WHERE per.personalId='$per'";
        $query_p = $this->db->query($strqp);
        $query_p = $query_p->row();

        $strqpbbc="SELECT * FROM personal as per WHERE per.personalId='$perbbc'";
        $query_pbbc = $this->db->query($strqpbbc);
        foreach ($query_pbbc->result() as $itembbc) {
            $bcc_array[]=$itembbc->email;
        }


        $atencionpara='';
        //================================
            //cargamos la libreria email
            $this->load->library('email');

            /*
            * Configuramos los parámetros para enviar el email,
            * las siguientes configuraciones es recomendable
            * hacerlas en el fichero email.php dentro del directorio config,
            * en este caso para hacer un ejemplo rápido lo hacemos 
            * en el propio controlador
            */
            
            //Indicamos el protocolo a utilizar
            $config['protocol'] = 'smtp';
             
            //El servidor de correo que utilizaremos
            $config["smtp_host"] ='mail.altaproductividadapr.com'; 
             
            //Nuestro usuario
            $config["smtp_user"] = 'info@altaproductividadapr.com';

            //Nuestra contraseña
            $config["smtp_pass"] = 'yUs&J=T4*V$J';

            //Puerto
            $config["smtp_port"] = '465';

            $config["smtp_crypto"] = 'ssl';
                    
            //El juego de caracteres a utilizar
            $config['charset'] = 'utf-8'; 
     
            //Permitimos que se puedan cortar palabras
            $config['wordwrap'] = TRUE;
             
            //El email debe ser valido  
            $config['validate'] = true;

            $config['mailtype'] = 'html';

            //Establecemos esta configuración
            $this->email->initialize($config);
            //$asunto='Servicio';
            //Ponemos la dirección de correo que enviará el email y un nombre
            $this->email->from('info@altaproductividadapr.com',$asunto);
             
              /*
               * Ponemos el o los destinatarios para los que va el email
               * en este caso al ser un formulario de contacto te lo enviarás a ti
               * mismo
               */
        //======================
            //$this->email->bcc('andres@mangoo.mx');
            $this->email->bcc($bcc_array);
            $this->email->reply_to($query_p->email, $query_p->nombre.' '.$query_p->apellido_paterno.' '.$query_p->apellido_materno);
        
        
            
            $correo='ag-gerardo@hotmail.com';
            if($this->enviosproductivo==1){
                $correo = $email;
            }
            //Definimos el asunto del mensaje
                $this->email->subject($asunto);
            $empresa='';
            $contacto='';
            $telefono='';
            $servicio='';
            $fecha='';
            $tecnico='';
            $equipos='';
            //log_message('error','asignacion: '.$asi.' tipo: '.$tipo);
            
            
            $result_mc = $this->ModeloCatalogos->getselectwheren('cliente_datoscontacto',array('activo'=>1,'email'=>$correo));
            foreach ($result_mc->result() as $itemmc) {
                $contacto=$itemmc->atencionpara;
                $telefono=$itemmc->telefono;
            }
            
            $result_cli = $this->ModeloCatalogos->getselectwheren('clientes',array('id'=>$idCliente));
            $empresa=0;
            foreach ($result_cli->result() as $itemcli) {
                $empresa=$itemcli->empresa;
            }

            $this->email->to($correo);


        $message  = '<div style="background: url(https://altaproductividadapr.com/public/img/fondo.jpg) #eaeaea;width: 100%;height: 94%;min-height: 752px;padding-top: 25px;margin: 0px;">
                    <div style="width: 10%; height: 10px; float: left;">
                        
                    </div>
                    <div style="background: white; border-top: 3px solid #ff0101; padding: 25px 25px 0px 25px; width: 80%; max-width: 825px;margin-left: auto; margin-right: auto; float: left; -webkit-box-shadow: 10px 10px 42px -6px rgba(0,0,0,0.75);-moz-box-shadow: 10px 10px 42px -6px rgba(0,0,0,0.75);box-shadow: 10px 10px 42px -6px rgba(0,0,0,0.75);">
                        <div style="width: 100%; float: left;">
                            <div style="width: 30%;float: left;">
                                <img src="https://altaproductividadapr.com/public/img/cotheader2.png" style="max-height: 100px;">
                            </div>
                            <div style="width: 40%;float: left; min-height:10px; font-style: italic; font-size: 14px; color:#2e4064;">
                                Impresoras y Multifunciones, Venta, Renta y Mantenimiento. Blvd. Norte 1831 Col. Villas San Alejandro. Puebla, Pue. <br>Call Center 273 3400, 249 5393. www.kyoceraap.com
                            </div>
                            <div style="width: 30%;float: left;">
                                <img src="https://altaproductividadapr.com/public/img/alta.jpg" style="max-height: 100px;">
                            </div>
                        </div>
                        <div style="width: 100%; float: left;">
                            <div style="width: 50%;float: left;height: 10px;"></div>
                            <div style="width: 50%;float: left; border-bottom: 3px solid #ff0101; font-size:17px"><b>Solicitud de cotización</b></div>
                        </div>
                        <div style="width: 100%; float: left;font-size: 14px; color:#2e4064;">
                            <div style="width: 30%;float: left;">Atención para:</div>
                            <div style="width: 70%;float: left;">'.$empresa.'</div>
                        </div>
                        <div style="width: 100%; float: left;font-size: 14px; color:#2e4064;">
                            <div style="width: 30%;float: left;">Contacto:</div>
                            <div style="width: 70%;float: left;">'.$contacto.'</div>
                        </div>
                        <div style="width: 100%; float: left;font-size: 14px; color:#2e4064;">
                            <div style="width: 30%;float: left;">Teléfono:</div>
                            <div style="width: 70%;float: left;">'.$telefono.'</div>
                        </div>
                        
                        
                        
                        
                        <div style="width: 100%; float: left;font-size: 14px; margin-bottom: 10px;color:#2e4064; margin-bottom: 10px; text-align:justify">
                            '.$comen.'
                        </div>
                        <div style="width: 100%; float: left;font-size: 14px; margin-bottom: 10px;color:#2e4064; margin-bottom: 10px;">
                            
                        </div>
                        <div style="width: 100%; float: left;font-size: 14px; margin-bottom: 10px;color:#2e4064; margin-bottom: 10px;">
                            <table style="width: 100%;">
                                <tr>
                                    <td></td>
                                    <td style="text-decoration: none;color: #455a64;font-weight: bold;">'.$query_p->nombre.' '.$query_p->apellido_paterno.' '.$query_p->apellido_materno.'</td>
                                    <td></td>
                                    <td><a href="mailto:'.$query_p->email.'" style="text-decoration: none;color: #455a64;font-weight: bold;">'.$query_p->email.'</a></td>
                                    <td></td>
                                </tr>
                                
                            </table>
                        </div>
                        <div style="width: 100%; float: left;font-size: 14px; margin-bottom: 10px;color:#2e4064; margin-bottom: 10px;">
                            <img src="https://altaproductividadapr.com/public/img/cot_footer.jpg" width="100%">
                        </div>

                        
                    </div>
                </div>';

        $this->email->message($message);
        /*
        foreach ($query_f->result() as $itemf) {
            $urlfile=FCPATH.'uploads/filestemp/'.$itemf->file;
            $this->email->attach($urlfile);
        }
        */
        $urlfile=FCPATH.$fichero;
        $this->email->attach($urlfile);
        //Enviamos el email y si se produce bien o mal que avise con una flasdata
        if($this->email->send()){
            //$this->session->set_flashdata('envio', 'Email enviado correctamente');
            $envio=1;
        }else{
            $envio=0;
            //$this->session->set_flashdata('envio', 'No se a enviado el email');
        }
        $array=array(
                    'envio'=>$envio
                );
        echo json_encode($array);
        //==================
    }
    function envio_fac_error(){
        $bbcarray[]='info@altaproductividadapr.com';
        $params = $this->input->post();

        $idfac=$params['idfac'];
        /*
        $code=$params['code'];
        $asunto=$params['asunto'];
        $comen=$params['comen'];
        $per=$params['per'];
        $perbbc=$params['perbbc'];

        $strqf="SELECT im.file FROM img_temp as im WHERE im.code='$code'";
                $query_f = $this->db->query($strqf);
        

                $this->db->select('*');
                $this->db->from('f_facturas fac');
                $this->db->group_start();
        $DATAf = json_decode($idfac);
        for ($i=0;$i<count($DATAf);$i++) {
                $this->db->or_like('fac.FacturasId',$DATAf[$i]->FacturasIds);
        }
                $this->db->group_end(); 
                $query_fac=$this->db->get();
                $query_fac_row=$query_fac->row();
        //===================================================
                $strqp="SELECT * FROM personal as per WHERE per.personalId='$per'";
                $query_p = $this->db->query($strqp);
                $query_p = $query_p->row();
        //=====================================================
        //===================================================
                if($perbbc>0){
                    $strqpbbc="SELECT * FROM personal as per WHERE per.personalId='$perbbc'";
                    $query_pbbc = $this->db->query($strqpbbc);
                    foreach ($query_pbbc as $itembbc) {
                        $bbcarray[]=$itembbc->email;
                    }
                }
        //=====================================================
        $atencionpara='';
        */
        $rfc='';
        $Nombre='';
        $Cp='';
        $r_regimenfiscal='';$mensajeerror='';
        $res_fac = $this->ModeloCatalogos->getselectwheren('f_facturas',array('FacturasId'=>$idfac));
        foreach ($res_fac->result() as $item_fac) {
            $rfc=$item_fac->Rfc;
            $Nombre=$item_fac->Nombre;
            $Cp=$item_fac->Cp;
            $r_regimenfiscal=$item_fac->r_regimenfiscal;
            $mensajeerror=$item_fac->mensajeerror;
        }
        //================================
            //cargamos la libreria email
            $this->load->library('email');

            /*
            * Configuramos los parámetros para enviar el email,
            * las siguientes configuraciones es recomendable
            * hacerlas en el fichero email.php dentro del directorio config,
            * en este caso para hacer un ejemplo rápido lo hacemos 
            * en el propio controlador
            */
            
            //Indicamos el protocolo a utilizar
            $config['protocol'] = $this->cf_protocol;
             
            //El servidor de correo que utilizaremos
            $config["smtp_host"] = $this->cf_smtp_host;
             
            //Nuestro usuario
            $config["smtp_user"] = $this->cf_smtp_user;

            //Nuestra contraseña
            $config["smtp_pass"] = $this->cf_smtp_pass;

            //Puerto
            $config["smtp_port"] = $this->cf_smtp_port;

            $config["smtp_crypto"] = $this->cf_smtp_crypto;
                    
            //El juego de caracteres a utilizar
            $config['charset'] = 'utf-8'; 
     
            //Permitimos que se puedan cortar palabras
            $config['wordwrap'] = TRUE;
             
            //El email debe ser valido  
            $config['validate'] = true;

            $config['mailtype'] = 'html';

            //Establecemos esta configuración
            $this->email->initialize($config);
            //$asunto='Servicio';
            //Ponemos la dirección de correo que enviará el email y un nombre
            $this->email->from('info@altaproductividadapr.com','Error timbrado');
             
              /*
               * Ponemos el o los destinatarios para los que va el email
               * en este caso al ser un formulario de contacto te lo enviarás a ti
               * mismo
               */
        //======================
            $this->email->bcc('info@altaproductividadapr.com');
            //$this->email->bcc($bbcarray);
            //$this->email->reply_to($query_p->email, $query_p->nombre.' '.$query_p->apellido_paterno.' '.$query_p->apellido_materno);
            
            $correo=array();
            //$correo[]='ag-gerardo@hotmail.com';
            $strq_to="SELECT * FROM `personal` WHERE personalId=1 or personalId=18 or personalId=56 or personalId=74";
            $query_to = $this->db->query($strq_to);
            foreach ($query_to->result() as $item_to) {
                $correo[]=$item_to->email;
            }
            

            //Definimos el asunto del mensaje
            $this->email->subject('Error al timbrar');
            
            $this->email->to($correo);


        $message  = '<div style="background: url(https://altaproductividadapr.com/public/img/fondo.jpg) #eaeaea;width: 100%;height: 94%;min-height: 752px;padding-top: 25px;margin: 0px;">';
                    $message  .= '<div style="width: 10%; height: 10px; float: left;"></div>';
                    $message  .= '<div style="background: white; border-top: 3px solid #ff0101; padding: 25px 25px 0px 25px; width: 80%; max-width: 825px;margin-left: auto; margin-right: auto; float: left; -webkit-box-shadow: 10px 10px 42px -6px rgba(0,0,0,0.75);-moz-box-shadow: 10px 10px 42px -6px rgba(0,0,0,0.75);box-shadow: 10px 10px 42px -6px rgba(0,0,0,0.75);">';
                        $message  .= '<div style="width: 100%; float: left;">';
                            $message  .= '<div style="width: 30%;float: left;">';
                                $message  .= '<img src="https://altaproductividadapr.com/public/img/cotheader2.png" style="max-height: 100px;">';
                            $message  .= '</div>';
                            $message  .= '<div style="width: 40%;float: left; min-height:10px; font-style: italic; font-size: 14px; color:#2e4064;">';
                                $message  .= 'Impresoras y Multifunciones, Venta, Renta y Mantenimiento. Blvd. Norte 1831 Col. Villas San Alejandro. Puebla, Pue. <br>Call Center 273 3400, 249 5393. www.kyoceraap.com';
                            $message  .= '</div>';
                            $message  .= '<div style="width: 30%;float: left;">';
                                $message  .= '<img src="https://altaproductividadapr.com/public/img/alta.jpg" style="max-height: 100px;">';
                            $message  .= '</div>';
                        $message  .= '</div>';
                        $message  .= '<div style="width: 100%; float: left;">';
                            $message  .= '<div style="width: 50%;float: left;height: 10px;"></div>';
                            $message  .= '<div style="width: 50%;float: left; border-bottom: 3px solid #ff0101; font-size:17px"><b>Se genero un error al timbrar </b></div>';
                        $message  .= '</div>';
                        
                        $message  .= '<div style="width: 100%; float: left;font-size: 14px; color:#2e4064;">';
                            $message  .= '<div style="width: 30%;float: left;">RFC:</div>';
                            $message  .= '<div style="width: 70%;float: left;">'.$rfc.'</div>';
                        $message  .= '</div>';

                        $message  .= '<div style="width: 100%; float: left;font-size: 14px; color:#2e4064;">';
                            $message  .= '<div style="width: 30%;float: left;">Razon social:</div>';
                            $message  .= '<div style="width: 70%;float: left;">'.$Nombre.'</div>';
                        $message  .= '</div>';

                        $message  .= '<div style="width: 100%; float: left;font-size: 14px; color:#2e4064;">';
                            $message  .= '<div style="width: 30%;float: left;">Codigo postal:</div>';
                            $message  .= '<div style="width: 70%;float: left;">'.$Cp.'</div>';
                        $message  .= '</div>';

                        $message  .= '<div style="width: 100%; float: left;font-size: 14px; color:#2e4064;">';
                            $message  .= '<div style="width: 30%;float: left;">Regimen Fiscal:</div>';
                            $message  .= '<div style="width: 70%;float: left;">'.$r_regimenfiscal.'</div>';
                        $message  .= '</div>';
        
                       $codigo=json_decode($mensajeerror);
                       $mensajeerror_l='';
                        if(isset($codigo->TimbrarCFDIResult->MensajeError)){
                            $mensajeerror_l.='<b>MensajeError:</b> '.$codigo->TimbrarCFDIResult->MensajeError;
                        }
                        if(isset($codigo->TimbrarCFDIResult->MensajeErrorDetallado)){
                            if($codigo->TimbrarCFDIResult->MensajeErrorDetallado!=''){
                                $mensajeerror_l.='<br><b>MensajeErrorDetallado:</b> '.$codigo->TimbrarCFDIResult->MensajeErrorDetallado;
                            }
                            
                        }             
        $message  .= '<div style="width: 100%; float: left;font-size: 14px; margin-bottom: 10px;margin-top: 10px;color:#2e4064; margin-bottom: 10px; ">
                            '.$mensajeerror_l.'
                        </div>
                        <div style="width: 100%; float: left;font-size: 14px; margin-bottom: 10px;color:#2e4064; margin-bottom: 10px;">
                            
                        </div>
                        <div style="width: 100%; float: left;font-size: 14px; margin-bottom: 10px;color:#2e4064; margin-bottom: 10px;">
                            <table style="width: 100%;">
                                <tr>
                                    <td></td>
                                    <td style="text-decoration: none;color: #455a64;font-weight: bold;"></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                
                            </table>
                        </div>
                        <div style="width: 100%; float: left;font-size: 14px; margin-bottom: 10px;color:#2e4064; margin-bottom: 10px;">
                            <img src="https://altaproductividadapr.com/public/img/cot_footer.jpg" width="100%">
                        </div>

                        
                    </div>
                </div>';

        $this->email->message($message);
        
        
        
        //Enviamos el email y si se produce bien o mal que avise con una flasdata
        if($this->email->send()){
            //$this->session->set_flashdata('envio', 'Email enviado correctamente');
            $send='Email enviado correctamente';
        }else{
            //$this->session->set_flashdata('envio', 'No se a enviado el email');
            $send='No se a enviado el email';
        }
        $array=array(
                    'newcodigo'=>date('YmdGis'),
                    'send'=>$send
                );
        echo json_encode($array);
        //==================
    }
}