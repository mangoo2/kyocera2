<?php
if(isset($cliente))
{
$idCliente = $cliente->id;
$empresa = $cliente->empresa;
}
?>
<style type="text/css" class="print_css">
  
</style>
<!-- START CONTENT -->
<section id="content" width="100%">
  <!--start container-->
  <div class="container" width="100%">
    <div class="section" style="margin-top: -53px;">
     <br>
      <div class="row">
        <input id="base_url" type="hidden" name="base_url" value="<?php echo base_url(); ?>">
        <!-- FORMULARIO  card-panel-->
        <div class="col s12">
          <div class="">

            <!-- ENCABEZADO PRINCIPAL PDF -->
            <div class="row"> 
              <div class="col s3">
                <img src="<?php echo base_url(); ?>app-assets/images/alta.png">
              </div>
              <div class="col s6">
                <p class="headerPDF"><b>Impresoras y Multifuncionales, Venta, Renta y Mantenimiento.<br>Blvd. Norte 1831 V. San Alejandro. Puebla, Pue. Call Center 273 3400, 249 5393<br>www.kyoceraapp.com</b></p>
              </div> 
              <div class="col s3">
                <p align="center"><img class="" src="<?php echo base_url(); ?>app-assets/images/1024_kyocera_logo_mds.png" alt="materialize logo" width="200px;" height="50px;"></p>
              </div>
            </div>

            <!-- APARTADO DE INFORMACIÓN DE RECEPTOR DE COTIZACIÓN-->
            
            
            <div class="row">
              <table class="centered datosClienteCotizacion">
                  <thead>
                    
                  </thead>
                  <tbody >
                    <tr>
                        <td>
                          Cotización Especial Para: <br>
                          Mangoo Software
                        </td>
                        <td>
                          Atención para: <br>
                          Raúl Trujeque Martínez
                        </td>
                        <td>
                          Correo raul@mangoo.com.mx <br>
                          Teléfono: 2224578961
                        </td>
                        <td>
                          Cotización No. <?php echo rand ( 1000, 5000 );?> 
                        </td>
                    </tr>
                  </tbody>
                </table>
            </div>

            <!------------ TABLA DE INFORMACIÓN DE EQUIPOS ------------>
            <div class="row">
                <table class="centered bordered letraTablaImpresion">
                  <thead>
                    <tr>
                        <th style="font-size:10px;"><b>Multifuncional Laser Monocromático </b></th>
                        <th>Equipo 1</th>
                        <th>Equipo 2</th>
                        <th>Equipo 3</th>
                    </tr>
                  </thead>
                  <tbody >
                    <tr>
                      <td>
                        <img src="<?php echo base_url(); ?>app-assets/images/kyoventa.png" class="onMouseOverDisabled">
                      </td>
                      <td >
                        <img src="<?php echo base_url(); ?>uploads/equipos/r_-19100.png" class="onMouseOverDisabled">
                      </td>
                      <td>
                        <img src="<?php echo base_url(); ?>uploads/equipos/r_-19100.png" class="onMouseOverDisabled">
                      </td>
                      <td>
                        <img src="<?php echo base_url(); ?>uploads/equipos/r_-19100.png" class="onMouseOverDisabled">
                      </td>
                    </tr>
                    <tr>
                      <td class="letraTablaFooter"><b>Precios: </b></td>
                      <td><b>$36,417.00</b></td>
                      <td><b>$58,459.00</b></td>
                      <td><b>$58,690.00</b></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td colspan="3">Costos de Impresión</td>
                    </tr>
                    <tr>
                      <td style="font-size:8px;">Costo Toner Black</td>
                      <td>$1,037.00</td>
                      <td>$1,378.00</td>
                      <td>$1,1670.00</td>
                    </tr>
                    <tr>
                      <td>Rendimiento Toner Black</td>
                      <td>12,000</td>
                      <td>20,000</td>
                      <td>25,000</td>
                    </tr>
                    <tr>
                      <td>Costo Toner CMY</td>
                      <td>$3,996.00</td>
                      <td>$3,996.00</td>
                      <td>$4,842.00</td>
                    </tr>
                    <tr>
                      <td>Rendimiento Toner CMY</td>
                      <td>6,000</td>
                      <td>12,000</td>
                      <td>15,000</td>
                    </tr>
                    <tr>
                      <td>Costo Unidad Imagen</td>
                      <td colspan="3">Inicial sin cargo</td>
                    </tr>
                    <tr>
                      <td>Rendimiento Unidad Imagen</td>
                      <td>200,000</td>
                      <td>200,000</td>
                      <td>200,000</td>
                    </tr>
                    <tr>
                      <td>Costo Garantia y Servicio</td>
                      <td colspan="3">Inicial sin cargo</td>
                    </tr>
                    <tr>
                      <td>Rendimiento Garantia y Servicio</td>
                      <td colspan="3">Inicial sin cargo</td>
                    </tr>
                    <tr>
                      <td>Costo por Página Mono</td>
                      <td>$0.09</td>
                      <td>$0.07</td>
                      <td>$0.07</td>
                    </tr>
                    <tr>
                      <td>Costo por Página Mono</td>
                      <td>$0.67</td>
                      <td>$0.33</td>
                      <td>$0.32</td>
                    </tr>
                    <tr>
                      <td width="19%" class="letraTablaFooter"><b>Especificaciones</b></td>
                      <td width="27%" height="100px">Velocidad: 24 ppm. Reducción-Ampliación: 25-400%. Procesa hasta: Doble Carta. Bypass: p/100 hojas. Alimentador: Rev 50. Resolución: 1200 x 1200 dpi. Procesador: 1,2. Memoria: 1.5 GB. Control de Usuarios: 100 Claves. Requerimientos eléctricos: 120V, 60Hz, 12A. Dimensiones: 59 x 59 x 75. Peso: 77 Kg. Capacidad Mensual: 30k. Aplicaciones HyPAS: Impresión Móvil, Conector a Google Drive Evernote, Monitoreo y Reporteador</td>
                      <td width="27%" height="100px">Velocidad: 24 ppm. Reducción-Ampliación: 25-400%. Procesa hasta: Doble Carta. Bypass: p/100 hojas. Alimentador: Rev 50. Resolución: 1200 x 1200 dpi. Procesador: 1,2. Memoria: 1.5 GB. Control de Usuarios: 100 Claves. Requerimientos eléctricos: 120V, 60Hz, 12A. Dimensiones: 59 x 59 x 75. Peso: 77 Kg. Capacidad Mensual: 30k. Aplicaciones HyPAS: Impresión Móvil, Conector a Google Drive Evernote, Monitoreo y Reporteador</td>
                      <td width="27%" height="100px">Velocidad: 24 ppm. Reducción-Ampliación: 25-400%. Procesa hasta: Doble Carta. Bypass: p/100 hojas. Alimentador: Rev 50. Resolución: 1200 x 1200 dpi. Procesador: 1,2. Memoria: 1.5 GB. Control de Usuarios: 100 Claves. Requerimientos eléctricos: 120V, 60Hz, 12A. Dimensiones: 59 x 59 x 75. Peso: 77 Kg. Capacidad Mensual: 30k. Aplicaciones HyPAS: Impresión Móvil, Conector a Google Drive Evernote, Monitoreo y Reporteador</td>
                    </tr>

                  </tbody>
                </table>
            </div>

            <!-- Condiciones de Equipos -->
            <div class="row" style="margin-bottom: -10px">
              <table class="centered letraTablaFooter">
                  <thead>
                      <tr>
                        <th>Condiciones</th>
                        <th>Certificaciones</th>
                        <th></th>
                      </tr>
                  </thead>
                  <tbody >
                    <tr>
                        <td width="33%"><b>Precios: </b>Sujetos a I.V.A. y a cambios.  <b>Garantía: </b>Por 12 meses contra defectos de fabricación. <b>Incluye: </b>Unidad de imagen iniciales.  <b>Opción: </b>Aumento de memoria. <b>Tiempo de entrega: </b>Equipo en existencia.  </b></td>
                        <td width="34%">Calidad ISO9001, responsabilidad ambiental ISO14001, rendimiento de suministros ISO19752, seguridad comprobada TUV-UE, eficiente consumo Energy Star y equipo nuevo NOM-019</td>
                        <td width="33%"><b>Enero de 2018</b><br><b><b>Guillermina Alvarez. </b>guille.a@kyoceraap.com</td>
                    </tr>
                  </tbody>
                </table>
            </div>

           

<!--columna 1---------------------------------------------------------------->

              <form class="form" id="" method="post">
                <?php
                if(isset($cliente))
                {
                ?>
                <input id="idCliente" name="idCliente" type="hidden" value="<?php echo $idCliente; ?>">
                <?php
                }
                ?>
                <div class="row">
                  <div class="input-field col s12">
                    <button class="btn cyan waves-effect waves-light right btn_imp" type="button" onclick="btn_guardar()">Imprimir
                    <i class="material-icons right">send</i>
                    </button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>