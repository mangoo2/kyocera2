
$(document).ready(function(){

	// Asignamos el formulario de envío
	var formulario_registro = $('#login-form');
	// Obtenemos la URL base para poder usarla
	var base_url = $('#base_url').val();

	// Validamos el formulario en base a las reglas mencionadas debajo
	formulario_registro.validate({
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: 
            {
				username: {
                    required: true
                },				
                password: {
                    required: true,
					minlength: 5
                },
				
            },
            // Si están correctos los campos, procesamos el formulario mediante Ajax
            submitHandler: function (form) 
            {
                var username = $('#username').val();
				var password = 	$('#password').val();				
                $.ajax({
                	type:'POST',
                	url: base_url+'index.php/Login/session',
                	data:
                	{
                		username:username,password:password
                	},
                	async:false,
                	success:function(data)
                	{
                		// Si existe el usuario, mostramos el mensaje de entrada y redirigimos
                		if(data==1)
                		{
                			swal({ title: "Bienvenido",
							    text: "A continuación, será redirigido",
							    type: 'success',
	                            showCancelButton: false,
	                            allowOutsideClick: false,
	                        });
	                        
							setTimeout(function(){ 
								window.location.href = base_url+"index.php/Sistema"; 
							}, 2000);
                		}
                		// En caso contrario, se notifica
                		else
                		{
                			swal({ title: "Error",
							    text: "El usuario y/o la contraseña son erroneos",
							    type: 'error',
	                            showCancelButton: false,
	                            allowOutsideClick: false,
	                        });
                		}
                	}
                });
                
            }
        });
});