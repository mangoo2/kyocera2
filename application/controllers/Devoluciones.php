<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Devoluciones extends CI_Controller {
    public function __construct()     {
        parent::__construct();

        $this->load->model('ModeloCatalogos');
        $this->load->model('Modeloaccesorios');
        $this->load->model('ModeloDevoluciones');
        $this->load->helper('url');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        if($this->session->userdata('logeado')==true){
            $this->perfilid =$this->session->userdata('perfilid');
            $this->idpersonal = $this->session->userdata('idpersonal');
            
        }else{
            redirect('login'); 
        }
    }

    function index(){
            $this->load->view('header');
            $this->load->view('main');
            $this->load->view('Devoluciones/listado');
            $this->load->view('footer');
            $this->load->view('Devoluciones/listadojs');
    }
    public function getlistaccesorios() {
        $params = $this->input->post();
        $getdata = $this->ModeloDevoluciones->getdevoluciones($params);
        $totaldata= $this->ModeloDevoluciones->getdevolucionest($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    
    


    
}
?>
