<?php 

header("Pragma: public");
header("Expires: 0");
$filename = "Facturas.xls";
header("Content-type: application/x-msdownload");
header("Content-Disposition: attachment; filename=$filename");
header("Pragma: no-cache");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");


?>
<table id="tabla_facturacion" name="tabla_facturacion_cliente" class="responsive-table display" cellspacing="0" border="1">
  <thead>
    <tr>
      <th>Serie</th>
      <th>Folio</th>
      <th>Cliente</th>
      <th>RFC</th>
      <th>Monto</th>
      <th>Estatus</th><!--sin timbrar timbrada cancelada-->
      <th>Fecha</th>
      <th>Personal</th>
      <th>Folio Fiscal</th>
      <th>Metodo de pagos</th>
      <th></th>
      <th>Folio Fiscal complemento</th>
      <th>Fecha timbre complemento</th>
      <th>ImpPagado</th>
      <th>Pago Registrado(PUE)</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($getdata->result() as $item) { 

        
        if($item->Estado_comp==1){
          $ImpPagado=$item->ImpPagado;
        }else{
          $ImpPagado=0;
        }
        $pagosmanual='';
        $montototalfactura=0;
        if($item->FormaPago=='PUE'){
            $resultrp = $this->ModeloCatalogos->obtencionpagosrentasfactura($item->FacturasId);
            foreach ($resultrp->result() as $itemrp) {
              $pagosmanual.= '$ '.number_format($itemrp->pago,2,'.',',').' '.$itemrp->fecha.'<br>';
              $montototalfactura=$montototalfactura+$itemrp->pago;
            }
            $resultvcp = $this->ModeloCatalogos->obtencionventapolizafactura($item->FacturasId);
            foreach ($resultvcp->result() as $itemvcp) {
              if($itemvcp->combinada==0){
                $result = $this->ModeloCatalogos->pagos_ventas($itemvcp->ventaId); 
                foreach ($result as $itemvv) { 
                  $pagosmanual.= '$ '.number_format($itemvv->pago,2,'.',',').' '.$itemvv->fecha.'<br>';
                  $montototalfactura=$montototalfactura+$itemvv->pago;
                }
              }elseif($itemvcp->combinada==1){
                $result = $this->ModeloCatalogos->pagos_combinada($itemvcp->ventaId); 
                foreach ($result as $itemv) { 
                  $pagosmanual.= '$ '.number_format($itemv->pago,2,'.',',').' '.$itemv->fecha.'<br>';
                  $montototalfactura=$montototalfactura+$itemv->pago;
                }
              }elseif($itemvcp->combinada==2){
                $result = $this->ModeloCatalogos->pagos_poliza($itemvcp->ventaId); 
                foreach ($result as $itemvc) { 
                  $pagosmanual.= '$ '.number_format($itemvc->pago,2,'.',',').' '.$itemvc->fecha.'<br>';
                  $montototalfactura=$montototalfactura+$itemvc->pago;
                }
              }
            }

          }else{
             $montototalfactura=$montototalfactura+floatval($ImpPagado);
          }
    
      $html_tr='<tr>';
        $html_tr.='<td>'.$item->serie.'</td>';
        $html_tr.='<td>'.$item->Folio.'</td>';
        $html_tr.='<td>'.$item->Nombre.'</td>';
        $html_tr.='<td>'.$item->Rfc.'</td>';
        $html_tr.='<td>$ '.number_format($item->total,2,'.',',').'</td>';
        
                  $estatus='';
                  if($item->Estado==0){
                    $estatus = 'Cancelada';
                  }elseif($item->Estado==1){
                    $estatus = 'Facturado';
                      if($item->fechatimbre=='0000-00-00 00:00:00'){
                            $estatus='Sin Facturar';
                        }
                  }elseif($item->Estado==2){
                        $estatus='Sin Facturar';
                  }else{
                      $estatus='';
                  }
                  $html_tr.='<td>'.$estatus.'</td>';
        
        $html_tr.='<td>'.$item->fechatimbre.'</td>';
        $html_tr.='<td>'.$item->nombre.' '.$item->apellido_paterno.' '.$item->apellido_materno.'</td>';
        $html_tr.='<td>'.$item->uuid.'</td>';
        $html_tr.='<td>'.$item->FormaPago.'</td>';
        $html_tr.='<td></td>';
        $html_tr.='<td>'.$item->uuid_compl.'</td>';
        $html_tr.='<td>'.$item->fechatimbre_compl.'</td>';
        $html_tr.='<td>'.$ImpPagado.'</td>';
        $html_tr.='<td>'.$pagosmanual.'<!--------'.$montototalfactura.'--></td>';
      $html_tr.='</tr>';
        if($tpago==0){//todo
          echo $html_tr;
        }
        if($tpago==1){//pagados
          if($item->total<=$montototalfactura){
            echo $html_tr;
          }
        }
        if($tpago==2){//pendientes
          if( floatval($item->total)>floatval($montototalfactura)){
            echo $html_tr;
            //echo '<tr><td> colspan="14" </td> </tr>':
          }
        }
      
    } ?>
  </tbody>
</table>