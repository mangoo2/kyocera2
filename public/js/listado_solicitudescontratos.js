
var base_url = $('#base_url').val();
var idCliente = $('#idCliente').val();

$(document).ready(function(){ 

	table = $('#tabla_facturacion_cliente').DataTable();
	load();

	$('#generarFacturaEquipos').on('click', function () {

		$('.modal').modal({
            dismissible: true
        });
    
	    //call the specific div (modal)
	    $('#modalAcciones').modal('open');
	});
	
}); 

function facturar(idRegistro)
{
	window.location.href=base_url+'/index.php/Rentas/factura/'+idRegistro;
}


function load() {
    // Destruye la tabla y la crea de nuevo con los datos cargados 
    table.destroy();
    table = $('#tabla_facturacion_cliente').DataTable({
        "ajax": {
            "url": base_url+"index.php/facturacion/listadoFacturaCliente/"+idCliente
        },
        "columns": [
            {"data": "id"},
            {"data": "idCliente"},
            {"data": "idEquipo"},
            {"data": "fotoequipo",
            	// si trae informacion la foto se mostrara
                render:function(data,type,row)
                { 
                    if(data!="")
                    {
                        return '<a class="btn_img" target="_blank" href="'+base_url+'uploads/equipos/'+data+'"><img src="'+base_url+'uploads/equipos/'+data+'" style="height:50px;width:50px;" class="imgpro"></a>';
                    }
                    else
                    {
                        return '<img src="'+base_url+'app-assets/images/kyocera_mini.png" style="height:50px;width:50px;" class="imgpro">';
                    }
                }
        	},
            {"data": "modelo"},
            {"data": "area"},
            {"data": "plazo"},           
            {"data": "costoporclick"},  
            {"data": "excedente"},  
            {"data": "id",
                 render:function(data,type,row)
                 {
                    var html='<button class="btn waves-effect waves-light facturar" type="button" onclick="facturar('+row.id+')" >Facturar\
                    <i class="material-icons right">send</i>\
                    </button><br>\
                    <button class="btn waves-effect waves-light cyan " type="button" id="'+row.id+'" name="'+row.id+'">Excedentes\
                    <i class="material-icons right">send</i>\
                    </button>';
                    return html;
                }
            }
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[25, 50, 100], [25, 50, 100]],
        "dom": 'frtpl',
        
        // Cambiamos lo principal a Español
        "language": {
            "lengthMenu": "Desplegando _MENU_ elementos por página",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
              }
        }
    });
}