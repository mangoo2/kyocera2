<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Rchat extends CI_Controller {
    public function __construct()     {
        parent::__construct();
        //$this->load->model('Clientes_model', 'model');
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloChat');
        $this->load->model('ModeloGeneral');
        $this->load->helper('url');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->submenu=80;
        if($this->session->userdata('logeado')==true){
            $this->perfilid =$this->session->userdata('perfilid');
            $this->idpersonal = $this->session->userdata('idpersonal');
            /*
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,$this->submenu);// 45 es el id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
            */
        }else{
            redirect('login'); 
        }
    }

    // Listado de series
    function index(){
        $data['MenusubId']=$this->submenu;
        $data['personalrows']=$this->ModeloGeneral->listadopersonal();
            $this->load->view('header');
            $this->load->view('main',$data);
            $this->load->view('chat/listado');   
            $this->load->view('footer');
            $this->load->view('chat/listadojs');
    }
    public function getlistchat() {
        $params = $this->input->post();
        $getdata = $this->ModeloChat->get_listado_chat($params);
        $totaldata= $this->ModeloChat->get_listado_chat_t($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    function coun_solicitu_chat(){
        $params = $this->input->post();
        $dep = $params['dep'];
        $per = $params['per'];
        $dataw['codigo']=date('Ymd');
        $dataw['status']=0;
        if($dep>0){
            $dataw['departamento']=$dep;
        }
        $result = $this->ModeloCatalogos->getselectwheren('chat_ini',$dataw);

        $asig_per_actual=0;

        foreach ($result->result() as $item_chat) {
            log_message('error','log_1');
            $idper = $this->ModeloGeneral->obtenerpersonaldep($dep);
            if($item_chat->reg_temp=='' || $item_chat->reg_temp==null){
                log_message('error','log_2');
                $this->ModeloCatalogos->updateCatalogo('chat_ini',array('id_per_temp'=>$idper,'reg_temp'=>$this->fechahoy),array('id'=>$item_chat->id));
                if($idper==$this->idpersonal){
                    log_message('error','log_3');
                    $asig_per_actual=1;
                }
            }else{
                log_message('error','log_4');
                $min_agregado = intval(date("i", strtotime($item_chat->reg_temp)));
                $min_actual = intval(date("i", strtotime($this->fechahoy)));
                if($min_agregado==$min_actual){
                    log_message('error','log_5');
                    if(intval($item_chat->id_per_temp)==intval($this->idpersonal) ){
                        log_message('error','log_6');
                        $asig_per_actual=1;
                    }else{
                        log_message('error','log_7');
                        $this->ModeloCatalogos->updateCatalogo('chat_ini',array('id_per_temp'=>$idper,'reg_temp'=>$this->fechahoy),array('id'=>$item_chat->id));
                        if($idper==$this->idpersonal){
                            log_message('error','log_8');
                            $asig_per_actual=1;
                        }
                    }
                }else{
                    log_message('error','log_9');
                    $this->ModeloCatalogos->updateCatalogo('chat_ini',array('id_per_temp'=>$idper,'reg_temp'=>$this->fechahoy),array('id'=>$item_chat->id));
                    if($idper==$this->idpersonal){
                        log_message('error','log_10');
                        $asig_per_actual=1;
                    }
                }

            }
        }
        $array = array(
                    'num'=>$result->num_rows(),
                    'asig_per_actual'=>$asig_per_actual
                    );
        echo json_encode($array);
    }
    function view($dep,$per){
        if(isset($_SESSION['url_actual_k'])){
            redirect($_SESSION['url_actual_k']);
        }
        $data['dep']=$dep;
        $data['per']=$per;
        $dataw['codigo']=date('Ymd');
        $dataw['status']=0;
        if($dep>0){
            $dataw['departamento']=$dep;
        }
        $result = $this->ModeloCatalogos->getselectwheren('chat_ini',$dataw);
        $data['result']=$result;

        $dataw['status']=1;
        $dataw['idpersonal_inicio']=$per;
        $result1 = $this->ModeloCatalogos->getselectwheren('chat_ini',$dataw);
        $data['result1']=$result1;

        $this->load->view('chat/solicitudes',$data);
    }
    function confirm($id,$dep,$per){
        $result = $this->ModeloCatalogos->getselectwheren('chat_ini',array('id'=>$id));
        if($result->num_rows()>0){
            $this->ModeloCatalogos->updateCatalogo('chat_ini',array('status'=>1,'idpersonal_inicio'=>$this->idpersonal),array('id'=>$id,'status'=>0));
            foreach ($result->result() as $item) {
                $c_t=$item->departamento;
                $idc = $item->id;
                $codigol= $item->codigol;
                $request_uri="https://chat.altaproductividadapr.com?c_t=$c_t&idc=$idc&test=$codigol&idpersonal=".$this->idpersonal;
                $_SESSION['url_actual_k']=$request_uri;
                redirect($request_uri);
            }
        }else{
            redirect('Rchat/view/'.$dep.'/'.$per);
        }
    }
    function finalizar($id){
        $this->ModeloCatalogos->updateCatalogo('chat_ini',array('status'=>2),array('id'=>$id));
        $result = $this->ModeloCatalogos->getselectwheren('chat_ini',array('id'=>$id));
        unset($_SESSION['url_actual_k']);
        foreach ($result->result() as $item) {
            $dep=$item->departamento;
            $per=$item->idpersonal_inicio;
            redirect('Rchat/view/'.$dep.'/'.$per);
        }
    }
    function viewchat($id){
        $resutl = $this->ModeloCatalogos->getselectwheren('chat_ini',array('id'=>$id));
        $resutld = $this->ModeloCatalogos->getselectwheren('chat_ini_msn',array('id_chat'=>$id));
        $data['resutld']=$resutld;
        $idpersonal_inicio=0;
                foreach ($resutl->result() as $item_c) {
                    $data['nombre']=$item_c->nombre;
                    $data['status']=$item_c->status;
                    $data['motivo']=$item_c->motivo;
                    $idpersonal_inicio=$item_c->idpersonal_inicio;
                    if($item_c->departamento==1){ $tipo_dep='Ventas';}
                    if($item_c->departamento==2){ $tipo_dep='Contratos / Arrendamiento';}
                    if($item_c->departamento==3){ $tipo_dep='Pólizas';}
                    if($item_c->departamento==4){ $tipo_dep='Servicio Técnico';}
                }
                $data['tipo_dep']=$tipo_dep;
                if($idpersonal_inicio>0){
                    $resutl_eje = $this->ModeloCatalogos->getselectwheren('personal',array('personalId'=>$idpersonal_inicio));
                    foreach ($resutl_eje->result() as $item_e) {
                        $data['ejecutivo']=$item_e->nombre;
                    }
                }else{
                    $data['ejecutivo']='';
                }

        $this->load->view('chat/viewchat',$data);
    }
}
?>
