<style type="text/css">
  .display_refacciones{
    display: none;
  }
</style>
<section id="content" width="100%">
          

          <!--start container-->
          <div class="container" width="100%">
            <div class="section">
              <div class="row">
                <p class="caption col s6">Configuración de Porcentajes de Ganancia.</p>
                
              </div>

              <div class="row" >
                <input id="base_url" type="hidden" name="base_url" value="<?php echo base_url(); ?>">
                
                <!-- FORMULARIO -->
                <form class="col s12 " id="porcentajes-form" method="post">

                  <div class="col s12 m12 24" >

                    <div class="card-panel" align="center">
                      <!-- PORCENTAJES DE UTILIDAD PARA VENTA -->
                      <div class="row">
                        <div class="row col s12" >
                          
                          <div class="row col s12">
                            <p class="caption col s6">PORCENTAJES VENTA EQUIPOS / COSTO GENERAL REFACCIONES Y CONSUMIBLES</p>  
                            <br>
                          </div>

                          <div class="input-field col s2">
                            <label>Porcentajes de Ganancia Equipos</label>
                          </div>
                          <div class="input-field col s4">
                            <i class="material-icons prefix"> % </i>
                            <input id="cambio_equipos" name="porcentaje_equipos_venta" type="text" placeholder="Equipos" value="<?php echo $porcentajes[0]->gananciaVenta; ?>">
                          </div>                          

                          <div class="input-field col s2">
                            <label>Porcentajes de Ganancia Refacciones</label>
                          </div>
                          <div class="input-field col s4">
                            <i class="material-icons prefix">% </i>
                            <input id="cambio_refacciones" name="porcentaje_refacciones_venta" type="text" placeholder="Refacciones" value="<?php echo $porcentajes[1]->gananciaVenta; ?>">
                          </div>  

                        </div>

                        <div class="row col s12" >

                          <div class="input-field col s2">
                            <label>Porcentajes de Ganancia Consumibles</label>
                          </div>
                          <div class="input-field col s4">
                            <i class="material-icons prefix">% </i>
                            <input id="cambio_consumibles" name="porcentaje_consumibles_venta" type="text" placeholder="Consumibles" value="<?php echo $porcentajes[2]->gananciaVenta; ?>">
                          </div>                          

                          <div class="input-field col s2">
                            <label>Porcentajes de Ganancia Accesorios</label>
                          </div>
                          <div class="input-field col s4">
                            <i class="material-icons prefix">% </i>
                            <input id="cambio_accesorios" name="porcentaje_accesorios_venta" type="text" placeholder="Accesorios" value="<?php echo $porcentajes[3]->gananciaVenta; ?>">
                          </div>

                        </div>

                      </div>
                      <!-- PORCENTAJES DE UTILIDAD PARA VENTA -->

                      <!-- PORCENTAJES DE UTILIDAD PARA RENTA -->
                      <div class="row">
                        <div class="row col s12" >
                          
                          <div class="row col s12">
                            <p class="caption col s6">PORCENTAJES RENTA / COSTO FRECUENTE REFACCIONES Y CONSUMIBLES</p>  
                            <br>
                          </div>

                          <div class="input-field col s2">
                            <label>Porcentajes de Ganancia Equipos</label>
                          </div>
                          <div class="input-field col s4">
                            <i class="material-icons prefix">% </i>
                            <input id="cambio_equipos" name="porcentaje_equipos_renta" type="text" placeholder="Equipos" value="<?php echo $porcentajes[0]->gananciaRenta; ?>">
                          </div>                          

                          <div class="input-field col s2" >
                            <label class="display_refacciones">Porcentajes de Ganancia Refacciones</label>
                          </div>
                          <div class="input-field col s4" >
                            <i class="material-icons prefix display_refacciones">% </i>
                            <input id="cambio_refacciones" name="porcentaje_refacciones_renta" type="text" placeholder="Refacciones" value="<?php echo $porcentajes[1]->gananciaRenta; ?>" class="display_refacciones">
                          </div>  

                        </div>

                        <div class="row col s12" >

                          <div class="input-field col s2">
                            <label>Porcentajes de Ganancia Consumibles</label>
                          </div>
                          <div class="input-field col s4">
                            <i class="material-icons prefix">% </i>
                            <input id="cambio_consumibles" name="porcentaje_consumibles_renta" type="text" placeholder="Consumibles" value="<?php echo $porcentajes[2]->gananciaRenta; ?>">
                          </div>                          

                          <div class="input-field col s2">
                            <label>Porcentajes de Ganancia Accesorios</label>
                          </div>
                          <div class="input-field col s4">
                            <i class="material-icons prefix">% </i>
                            <input id="cambio_accesorios" name="porcentaje_accesorios_renta" type="text" placeholder="Accesorios" value="<?php echo $porcentajes[3]->gananciaRenta; ?>">
                          </div>

                        </div>

                      </div>
                      <!-- PORCENTAJES DE UTILIDAD PARA RENTA -->

                      <!-- PORCENTAJES DE UTILIDAD PARA POLIZA -->
                      <div class="row">
                        <div class="row col s12" >
                          
                          <div class="row col s12">
                            <p class="caption col s6">PORCENTAJES PÓLIZA / COSTO ESPECIAL REFACCIONES Y CONSUMIBLES</p>  
                            <br>
                          </div>

                          <div class="input-field col s2">
                            <label>Porcentajes de Ganancia Equipos</label>
                          </div>
                          <div class="input-field col s4">
                            <i class="material-icons prefix">% </i>
                            <input id="cambio_equipos" name="porcentaje_equipos_poliza" type="text" placeholder="Equipos" value="<?php echo $porcentajes[0]->gananciaPoliza; ?>">
                          </div>                          

                          <div class="input-field col s2">
                            <label class="display_refacciones">Porcentajes de Ganancia Refacciones</label>
                          </div>
                          <div class="input-field col s4">
                            <i class="material-icons prefix display_refacciones">% </i>
                            <input id="cambio_refacciones" name="porcentaje_refacciones_poliza" type="text" placeholder="Refacciones" value="<?php echo $porcentajes[1]->gananciaPoliza; ?>" class="display_refacciones">
                          </div>  

                        </div>

                        <div class="row col s12" >

                          <div class="input-field col s2">
                            <label>Porcentajes de Ganancia Consumibles</label>
                          </div>
                          <div class="input-field col s4">
                            <i class="material-icons prefix">% </i>
                            <input id="cambio_consumibles" name="porcentaje_consumibles_poliza" type="text" placeholder="Consumibles" value="<?php echo $porcentajes[2]->gananciaPoliza; ?>">
                          </div>                          

                          <div class="input-field col s2">
                            <label>Porcentajes de Ganancia Accesorios</label>
                          </div>
                          <div class="input-field col s4">
                            <i class="material-icons prefix">% </i>
                            <input id="cambio_accesorios" name="porcentaje_accesorios_poliza" type="text" placeholder="Accesorios" value="<?php echo $porcentajes[3]->gananciaPoliza; ?>">
                          </div>

                        </div>

                      </div>
                      <!-- PORCENTAJES DE UTILIDAD PARA POLIZA -->

                      <!-- PORCENTAJES DE UTILIDAD PARA REVENDEDOR -->
                      <div class="row">
                        <div class="row col s12" >
                          
                          <div class="row col s12">
                            <p class="caption col s6">PORCENTAJES REVENDEDOR / COSTO CON PÓLIZA REFACCIONES Y CONSUMIBLES</p>  
                            <br>
                          </div>

                          <div class="input-field col s2">
                            <label>Porcentajes de Ganancia Equipos</label>
                          </div>
                          <div class="input-field col s4">
                            <i class="material-icons prefix">% </i>
                            <input id="cambio_equipos" name="porcentaje_equipos_revendedor" type="text" placeholder="Equipos" value="<?php echo $porcentajes[0]->gananciaRevendedor; ?>">
                          </div>                          

                          <div class="input-field col s2">
                            <label class="display_refacciones">Porcentajes de Ganancia Refacciones</label>
                          </div>
                          <div class="input-field col s4">
                            <i class="material-icons prefix display_refacciones">% </i>
                            <input id="cambio_refacciones" name="porcentaje_refacciones_revendedor" type="text" placeholder="Refacciones" value="<?php echo $porcentajes[1]->gananciaRevendedor; ?>" class="display_refacciones">
                          </div>  

                        </div>

                        <div class="row col s12" >

                          <div class="input-field col s2">
                            <label>Porcentajes de Ganancia Consumibles</label>
                          </div>
                          <div class="input-field col s4">
                            <i class="material-icons prefix">% </i>
                            <input id="cambio_consumibles" name="porcentaje_consumibles_revendedor" type="text" placeholder="Consumibles" value="<?php echo $porcentajes[2]->gananciaRevendedor; ?>">
                          </div>                          

                          <div class="input-field col s2">
                            <label>Porcentajes de Ganancia Accesorios</label>
                          </div>
                          <div class="input-field col s4">
                            <i class="material-icons prefix">% </i>
                            <input id="cambio_accesorios" name="porcentaje_accesorios_revendedor" type="text" placeholder="Accesorios" value="<?php echo $porcentajes[3]->gananciaRevendedor; ?>">
                          </div>

                        </div>

                      </div>
                      <!-- PORCENTAJES DE UTILIDAD PARA REVENDEDOR -->

                      <div class="row">
                        <div class="row" >
                          <div class="input-field col s2">
                            <button id="button" name="actualiza_porcentajes_ganancia" class="btn waves-effect waves-light green darken-1"> Actualizar</button> 
                          </div>
                        </div>
                      </div>

                    </div>

                  </div>

                </form>
              </div>

          </div>
        </div>
      </section>