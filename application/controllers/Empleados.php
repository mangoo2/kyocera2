<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Empleados extends CI_Controller {
    public function __construct()     {
        parent::__construct();
        $this->load->model('Empleados_model', 'model');
        $this->load->model('Estados_model', 'estados');
        $this->load->model('login_model');
        $this->load->helper('url');
        $this->load->model('ModeloCatalogos');
        $this->submenu=1;
        $this->idpersonal = $this->session->userdata('idpersonal');
        if($this->session->userdata('logeado')==true){
            $this->perfilid =$this->session->userdata('perfilid');
            $this->idpersonal = $this->session->userdata('idpersonal');
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,$this->submenu);// 1 es el id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
        }else{
            redirect('login');
        }
    }

    // Listado de Empleados
    function index(){
        $data['MenusubId']=$this->submenu;
            $this->load->view('header');
            $this->load->view('main',$data);
            $this->load->view('empleados/listado');   
            $this->load->view('footer');
            $this->load->view('empleados/listado_js');
    }

    // Listado de Empleados
    function alta(){
        $data['MenusubId']=$this->submenu;
        $data['estados'] = $this->estados->getEstados();
        $data['tipoVista'] = 1;
        $data['perfiles'] = $this->model->getPerfiles();
        $data['idpersonal']=$this->idpersonal;
        $this->load->view('header');
        $this->load->view('main',$data);
        $this->load->view('empleados/form_empleados');
        
        $this->load->view('footer');
        $this->load->view('empleados/form_empleados_js');
    }

    // Listado de Empleados
    function edicion($idEmpleado){ 
        $data['MenusubId']=$this->submenu;  
        $data['empleado'] = $this->model->getEmpleadoPorId($idEmpleado);
        $data['estados'] = $this->estados->getEstados();
        $data['tipoVista'] = 2;
        $data['perfiles'] = $this->model->getPerfiles();
        $data['idpersonal']=$this->idpersonal;
        $this->load->view('header');
        $this->load->view('main',$data);
        $this->load->view('empleados/form_empleados');
        $this->load->view('footer');
        $this->load->view('empleados/form_empleados_js');
    }

    // Listado de Empleados
    function visualizar($idEmpleado){
        $data['MenusubId']=$this->submenu;
        $data['empleado'] = $this->model->getEmpleadoPorId($idEmpleado);
        $data['perfiles'] = $this->model->getPerfiles();
        $data['estados'] = $this->estados->getEstados();
        $data['tipoVista'] = 3;
        $data['idpersonal']=$this->idpersonal;
        $this->load->view('header');
        $this->load->view('main',$data);
        $this->load->view('empleados/form_empleados');
        
        $this->load->view('footer');
        $this->load->view('empleados/form_empleados_js');
    }

    // Eliminar empleados
    function eliminarEmpleado($idEmpleado){
        $datosEmpleado = array('estatus' => 0);
        $eliminado = $this->model->actualizaEmpleado($datosEmpleado, $idEmpleado);
        echo $eliminado;
        //$this->Modelobitacoras->Insert(array('contenido'=>'Se Elimino Empleado','nombretabla'=>'personal','idtable'=>$idEmpleado,'tipo'=>'Delete','personalId'=>$this->idpersonal));
    }

    // Restablecer Contrasena
    function restableceContrasena(){
        $datos = $this->input->post();
        $contrasena=$datos['contrasenaRestablecida'];
        $datosUsuario = array('contrasena' => password_hash($contrasena, PASSWORD_BCRYPT),'texuser'=>$contrasena);
        $restablecida = $this->model->actualizaUsuario($datosUsuario, $datos['idUsuario']);
        echo $restablecida;
    }

    function getListadoEmpleados(){
        // Obtenemos el JSON para la lista de empleados de la tabla
        $empleados = $this->model->getListadoEmpleados();
        $json_data = array("data" => $empleados);
        echo json_encode($json_data);
    }

    function insertaActualizaEmpleado(){
        //$idEmpleado = 0;
        // Recupera los datos que vienen del formukario
        $data = $this->input->post();
                
        // Si no existe el id del Empleado, será un nuevo empleado
        if(!isset($data['idEmpleado'])){
            if(isset($data['usuario'])) { $usuario = $data['usuario']; unset($data['usuario']);}
            if(isset($data['contrasena'])) { $contrasena = $data['contrasena']; unset($data['contrasena']);}
            if(isset($data['perfilId'])) { $perfilId = $data['perfilId']; unset($data['perfilId']);}
            unset($data['action']);
            
            $idEmpleado = $this->model->insertaEmpleado($data);

            // Si trae el id de empleado, y es de tipo usuario, ingresaremos los datos restantes
            if(isset($idEmpleado) && $data['tipo']==2){

                $datosUsuario = array('perfilId' => $perfilId,
                                      'personalId' => $idEmpleado,
                                      'usuario' => $usuario,
                                      'contrasena' => password_hash($contrasena, PASSWORD_BCRYPT),
                                      'texuser'=>$contrasena
                                      );
                $idUsuario = $this->model->insertaUsuario($datosUsuario);
                //$this->Modelobitacoras->Insert(array('contenido'=>'Se Inserto Empleado','nombretabla'=>'personal','idtable'=>$idUsuario,'tipo'=>'Insert','personalId'=>$this->idpersonal));
            }
        }else{   
            if(isset($data['usuario'])) { $usuario = $data['usuario']; unset($data['usuario']);}
            if(isset($data['contrasena'])) { $contrasena = $data['contrasena']; unset($data['contrasena']);}
            if(isset($data['perfilId'])) { $perfilId = $data['perfilId']; unset($data['perfilId']);}
            if(isset($data['usuarioId'])) { $usuarioId = $data['usuarioId']; unset($data['usuarioId']);}

            $idEmpleadoAux = $data['idEmpleado']; 
            unset($data['idEmpleado']);
            unset($data['action']);
            
            $idEmpleado = $this->model->actualizaEmpleado($data,$idEmpleadoAux);

            // Si es de tipo usuario y trae nombre de usuario, procedemos a guardar
            if(isset($usuario) && $data['tipo']==2){
                // Si trae id de usuario se actualiza
                if($usuarioId!=''){
                    $datosUsuario = array('perfilId' => $perfilId,
                                      'personalId' => $idEmpleadoAux,
                                      'usuario' => $usuario); 

                    $this->model->actualizaUsuario($datosUsuario,$usuarioId);
                    //$this->Modelobitacoras->Insert(array('contenido'=>'Se Actualizo Empleado','nombretabla'=>'personal','idtable'=>$usuarioId,'tipo'=>'Insert','personalId'=>$this->idpersonal)); 
                }
                
            }
        }
        echo $idEmpleado;
    }
    function export(){
        $strq = "SELECT per.*,est.Nombre as estados,usu.Usuario FROM personal as per 
                inner join estado as est on est.EstadoId=per.estado
                left join usuarios as usu on usu.personalId=per.personalId
                where per.estatus=1";
        $data['query'] = $this->db->query($strq);
        $this->load->view('empleados/export',$data); 
    }
    function pagoefe(){
        $params = $this->input->post();
        $per = $params['per'];
        $pfectivo = $params['pfectivo'];
        $this->ModeloCatalogos->updateCatalogo('personal',array('p_efectivo'=>$pfectivo),array('personalId'=>$per));
    }
}