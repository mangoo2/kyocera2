var base_url = $('#base_url').val();
var trunquearredondear=0;//0 redondeo 1 trunquear (dos dijitos)
//var save_facturasid=0;
$(document).ready(function(){
	$('.collapsible').collapsible();
	$('.chosen-select').chosen({width: "91%"});
	$('.registrofac').click(function(event) {
        
	});
    $('#MetodoPago').select2({width: 'resolve'});
    $('.registrofac_preview').click(function(event) {

        registrofac_preview();
    });
	$('#FormaPago').change(function(event) {
		$(this).val();
		if ($(this).val()=='TarjetasDeCredito') {
			$(".tarjeta_datos").show( "slow" );
		}else{
			$(".tarjeta_datos").hide( "slow" );
			$('#tarjeta').val('');
		}
	});
    $('.produccionlibe').click(function(event) {
        $('#modal_produccionlibre').modal();
        $('#modal_produccionlibre').modal('open');
    });
    $('#facturarelacionada').click(function(event) { //agregado version 4.0
        if($('#facturarelacionada').is(':checked')){
            $('.divfacturarelacionada').show('show');
        }else{
            $('.divfacturarelacionada').hide('show');
            $('#uuid_r').val('');
        }
        /* Act on the event */
    });
    $('#FormaPago').change(function(event) {
        if($('#FormaPago').val()=='PPD'){
            $('#MetodoPago option').prop('disabled',true);
            $('#MetodoPago').val('PorDefinir').trigger('change');
            $('#MetodoPago option[value=PorDefinir]').prop('disabled',false);
            $('#MetodoPago').select2();
        }
        if($('#FormaPago').val()=='PUE'){
            $('#MetodoPago option').prop('disabled',false);
            $('#MetodoPago option[value=PorDefinir]').prop('disabled',true);
            $('#MetodoPago').select2();
        }
    });
    /*
    $('.info_ren_exce_value').change(function(event) {
        var info_renta_mono = $('#info_renta_mono').val();
        var info_renta_color = $('#info_renta_color').val();
        var info_excede_mono = $('#info_excede_mono').val();
        var info_excede_color = $('#info_excede_color').val();
        var montototal=parseFloat(info_renta_mono)+parseFloat(info_renta_color)+parseFloat(info_excede_mono)+parseFloat(info_excede_color);
        console.log(montototal);
        var preciossub=0;
        $(".info_ren_exce_value").each(function() {
            var vstotalps = $(this).val();
            preciossub += Number(vstotalps);
        });
        if(montototal==preciossub){
            $('.btn-agrega_ccs').prop('disabled',false);
        }else{
            $('.btn-agrega_ccs').prop('disabled',true);
        }

        var idcentroc = $(this).data('idcentroc');
        var prefid = $(this).data('prefid');
        var montoc = $(this).val();
        console.log(idcentroc+' '+prefid+' '+montoc);
        $.ajax({
            type:'POST',
            url: base_url+'index.php/Configuracionrentas_ext/iucentrocostosfac',
            data: {
                idcentroc:idcentroc,
                prefid:prefid,
                montoc:montoc
            },
            success:function(data){
            }
        });
    });
    */
    view_ccs_montos();
    contratosview();
});
function checkfactura() {
    //==============================================
        var equipos = $("#equipos_facturar tbody > tr");
        var DATAe  = [];
        equipos.each(function(){     
            if($(this).find("input[class*='testadd']").is(':checked')){    
                            item = {};
                            item ["equipos"]   = $(this).find("input[class*='testadd']").val();
                            item ["renta"]   = $(this).find("input[class*='testadd']").data('renta');
                            item ["serie"]   = $(this).find("input[class*='testadd']").data('serie');
                            item ["tipo"]   = $(this).find("input[class*='testadd']").data('tipo');
                            item ["excedente"]   = $(this).find("input[class*='testadd']").data('excedente');
                            DATAe.push(item);
            }
                        });
        aInfoe   = JSON.stringify(DATAe);
    //==============================================
	var tipo = 0 ;
	if($('#checkfact1').is(":checked")){
        tipo = 1;
	}
	if($('#checkfact2').is(":checked")){
        tipo = 2;
	}
	if($('#checkfact3').is(":checked")){
        tipo = 3;
	}
    if(tipo==1){
        $('.div_fac_pen').show('show');
    }else{
        $('.div_fac_pen').hide('show');
        $('#fac_pen').prop('checked',false);
    }
	$('.datos_principal').remove(); 
    var mono  =  $('#tipomc_m').is(':checked')==true?1:0;
    var color =  $('#tipomc_c').is(':checked')==true?2:0;
    // anteriormente era index.php/Configuracionrentas/factura_requerimientos pero se cambio para evitar cambios en ese controlador
	$.ajax({
	        type:'POST',
	        url: base_url+"index.php/Configuracionrentas_ext/factura_requerimientos",
	        data: {
                id:$('#idfactura').val(),
                tip:tipo,
                tiporenta:$('#tiporenta').val(),
                equipos:aInfoe,
                mon:mono,
                col:color
            },
	        success: function (data){
	        	//$('.datos_principal').remove(); 
                $('.addcobrar').append(data); 
                unidadconceptosat();
                calcularmontos();
                movertr();
                deletetrrow();
	        }
    });
}
function view_factura(){
	if($('#testadd').is(":checked")){
        $('.view_factura').css('display','block');
	}else{
		$('.view_factura').css('display','none');
	}
}
function clientecontrato() {
	var idcliente = $('#idclientec option:selected').val();
	$.ajax({
        type:'POST',
        url: base_url+"index.php/Configuracionrentas/selectcontratos",
        data: {id:idcliente},
        success: function (data){
        	$('.mostrar_tabla').html(data); 
        	$('.chosen-select').chosen({width: "100%"});
        }
    });
    $('.mostrar_factura').html(''); 
    $('.check_renta').css('display','none');
}
function facturaselect() {
    $('input[name="radio1"]').prop('checked', false);
	var idcontrato = $('#idcontrato_f option:selected').val();
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Configuracionrentas/selectfacturas",
        data: {id:idcontrato},
        success: function (data){
        	$('.mostrar_factura').html(data); 
        	$('.chosen-select').chosen({width: "100%"});
        }
    });
}
function mostrar_check_rentas() {
	$('.check_renta').css('display','block');
    var idscontrato=$('#idcontrato_f option:selected').val();
    var idsperiodo=$('#idfactura_f option:selected').val();
    $('.addinsercionprefacturas').html('<iframe src="'+base_url+'index.php/Reportes/rentadetalles/'+idsperiodo+'/'+idscontrato+'"></iframe>');
}
function checkfactura2(){
    //$('.datos_agregagados').remove();
	var idfactura_f = $('#idfactura_f option:selected').val();
    var idcontrato_f = $('#idcontrato_f option:selected').val();
    
    var idfactura_f_text = $('#idfactura_f option:selected').text();
    var idcontrato_f_text = $('#idcontrato_f option:selected').text();

	var tipo = 0 ;
	if($('#checkfact21').is(":checked")){
        tipo = 1;
	}
	if($('#checkfact22').is(":checked")){
        tipo = 2;
	}
	if($('#checkfact23').is(":checked")){
        tipo = 3;
	}
    var rowcobrartabl = $('#table_conceptos tbody > tr').length;
    $('.datos_agregagados_periodo_'+idfactura_f).remove();
	$.ajax({
	        type:'POST',
	        url: base_url+"index.php/Configuracionrentas/factura_requerimientos2",
	        data: {
                id:idfactura_f,
                tip:tipo,
                tiporenta:$('#tiporenta').val(),
                rowcobrartable:rowcobrartabl
            },
	        success: function (data){
                  $('.addcobrar').append(data); 
                  unidadconceptosat();
                  calcularmontos();
                  //================================================
                    $('.daperiodo_'+idfactura_f).remove();
                    var htmlperiodosext='<tr class="daperiodo_'+idfactura_f+'">\
                                            <td>\
                                            <input type="hidden" value="'+idcontrato_f+'" id="contrato2" >\
                                            <input type="hidden" value="'+idfactura_f+'" id="prefacturaId2" >\
                                            '+idcontrato_f+' ('+idcontrato_f_text+')</td>\
                                            <td>'+idfactura_f+' ('+idfactura_f_text+')</td>\
                                            <td>\
                                                <a class="btn-floating waves-effect waves-light purple lightrn-1" onclick="deleteperiodoall('+idfactura_f+')"><i class="material-icons">delete_forever</i></a>\
                                            </td>\
                                          </tr>';
                    $('.tablepediodosextrastbody').append(htmlperiodosext);
                  //================================================
	        }
    });
    setTimeout(function(){ 
        calcularmontos();
    }, 1000);
}
function deleteprefactura(id) {
	$('.datos_agregagados_'+id).remove();
    setTimeout(function(){ 
        calcularmontos();
    }, 1000);
}
function unidadconceptosat(){
	$('.unidadsat').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un Unidad',
          ajax: {
            url: base_url+'Configuracionrentas/searchunidadsat',
            dataType: "json",
            data: function (params) {
              var query = {
                search: params.term,
                type: 'public'
              }
              return query;
          },
          processResults: function(data){
            var clientes=data;
            var itemscli = [];
            data.forEach(function(element) {
                    itemscli.push({
                        id: element.Clave,
                        text: element.Clave+' / '+element.nombre
                    });
                });
                return {
                    results: itemscli
                };        
          },  
        }
      });
	$('.conseptosat').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un Concepto',
          ajax: {
            url: base_url+'Configuracionrentas/searchconceptosat',
            dataType: "json",
            data: function (params) {
              var query = {
                search: params.term,
                type: 'public'
              }
              return query;
          },
          processResults: function(data){
            var clientes=data;
            var itemscli = [];
            data.forEach(function(element) {
                    itemscli.push({
                        id: element.Clave,
                        text: element.nombre
                    });
                });
                return {
                    results: itemscli
                };        
          },  
        }
      });
}
function calcularmontos(){
	//var subtotal1=0;
    //$(".precio").each(function() {
    //    var vstotal = $(this).val();
    //    subtotal1 += Number(vstotal);
    //});
    var subtotal1 =0;
    var TABLApr   = $("#table_conceptos tbody > tr");
        TABLApr.each(function(){ 
            var vtotales = parseFloat($(this).find("input[id*='cantidad']").val())*parseFloat($(this).find("input[id*='precio']").val());
                //vtotales = redondear(vtotales,2);
                //console.log(redondear(vtotales,2));
                //vtotales = vtotales.toFixed(2);
                //console.log(vtotales);
                //console.log(Math.ceil(vtotales));
                subtotal1 += Number(vtotales);
        });
    var descuento1=0;
    $(".cdescuento").each(function() {
        var vdescuento = $(this).val();
        descuento1 += Number(vdescuento);
    });
    //==================================
    var caliva=0;
    $(".precio").each(function() {
        var vcaliva = $(this).val()*0.16;
            //vcaliva=vcaliva.toFixed(2);
            if(trunquearredondear==0){
                vcaliva = vcaliva.toFixed(4);
            }else{
                vcaliva=Math.floor(vcaliva * 100) / 100;
            }
        caliva += Number(vcaliva);
    });
    console.log(caliva.toFixed(2));
    caliva=caliva.toFixed(2);
    //================================
    //var subtotal = parseFloat(subtotal1)-parseFloat(descuento1);
    var subtotal = parseFloat(subtotal1);
    var iva =(parseFloat(subtotal)-parseFloat(descuento1))*0.16;
        if(trunquearredondear==0){
            iva = iva.toFixed(2);
        }else{
            iva=Math.floor(iva * 100) / 100;
        }
    //var iva =caliva;
    var total = parseFloat(subtotal)-parseFloat(descuento1)+parseFloat(iva);
        total=parseFloat(total.toFixed(2));
    

    if ($('#risr').is(':checked')) {
        var v_isr=subtotal*0.1;
            v_isr =v_isr.toFixed(2);
            $('#isr').val(v_isr);
            total=total-v_isr;
            total=parseFloat(total.toFixed(2));
    }else{
        var v_isr=0;
        $('#isr').val(0.00);
    }
    if ($('#riva').is(':checked')) {
        var v_riva=subtotal*0.106666;
            v_riva =v_riva.toFixed(2);
            $('#ivaretenido').val(v_riva);
            total=total-v_riva;
            total=parseFloat(total.toFixed(2));
    }else{
        $('#ivaretenido').val(0.00);
    }
    if ($('#5almillar').is(':checked')) {
        var v_5millar=(subtotal/1000)*5;
            v_5millar =v_5millar.toFixed(2);
            $('#5almillarval').val(v_5millar);
            total=total-v_5millar;
            total=parseFloat(total.toFixed(2));
    }else{
        $('#5almillarval').val(0.00);
    }
    if ($('#aplicaout').is(':checked')) {
        var aplicaout = subtotal*0.06;
            aplicaout = aplicaout.toFixed(2);
        
            $('#outsourcing').val(aplicaout);
            totales=totales-aplicaout;
            totales=totales.toFixed(2);
    }else{
        $('#outsourcing').val(0.00);
    }
    total=parseFloat(total.toFixed(2));

    $('#Subtotalinfo').val(subtotal1.toFixed(2));
    $('#descuentof').val(descuento1);
    $('#Subtotal').val(subtotal);
    
    $('#iva').val(iva);
    
    $('#total').val(total);
}
function alertfunction(titlea,contenta){
    $.alert({boxWidth: '30%',useBootstrap: false,title: titlea,theme: 'bootstrap',content: contenta});  
}
function obtenerdatosrentas(){console.log('obtenerdatosrentas');
	var prefactura=$('#idfactura').val();
	var contrato=$('#idcontrato').val();
	$.ajax({
        type:'POST',
        url: base_url+"index.php/Configuracionrentas/obtenerdatosrentas",
        data: {
        	prefac:prefactura,
        	contra:contrato
        },
        success: function (response){
           
           
        },
        error: function(response){
            notificacion(10);
        }
    });
}
function  plgenerar() {
    var prefactura=$('#idfactura').val();
    var valorpl=$('#valorpl').val();
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Configuracionrentas/produccionlibre",
        data: {
            prefac:prefactura,
            valor:valorpl
        },
        success: function (data){
           $('.addcobrar').html(data); 
                unidadconceptosat();
                calcularmontos();
           
        },
        error: function(response){
            notificacion(10);
        }
    });
}
function activardescuento(idrow){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: 'Se necesita permisos de administrador<br>'+
                 '<input type="password" placeholder="Contraseña" id="contrasena" class="name form-control-bmz" required/>',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var pass=$('#contrasena').val();
                if (pass!='') {
                     $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Sistema/solicitarpermiso",
                        data: {
                            pass:pass
                        },
                        success: function (response){
                                var respuesta = parseInt(response);
                                if (respuesta==1) {
                                    $('.cdescuento_'+idrow).attr({'readonly':false});
                                    $('.cdescuento_'+idrow).removeAttr("onclick");
                                    //$('.cdescuento_'+idrow).removeAttr("onclick");
                                    $('.cdescuento_'+idrow).attr('onchange', 'calcularmontos();');
                                }else{
                                    notificacion(2); 
                                }
                        },
                        error: function(response){
                            notificacion(10);  
                             
                        }
                    });
                    
                }else{
                    notificacion(0);
                }
                
            },
            cancelar: function () 
            {
                
            }
        }
    });
}
function deleteperiodoall(rowperiodo){
    $('.datos_agregagados_periodo_'+rowperiodo).remove();
    $('.daperiodo_'+rowperiodo).remove();
    calcularmontos();
}
function calculartotales_set(tiempo){
    setTimeout(function(){ 
        calcularmontos();
    }, tiempo);
}
function registrofac_preview(){
    var FormaPago = $('#FormaPago option:selected').val();
    if(FormaPago=='PPD'){
        $('#MetodoPago').val('PorDefinir').select2();
    }
    //$( ".registrofac" ).prop( "disabled", true );
        var form =$('#validateSubmitForm');
        var valid =form.valid();
        if (valid) {
            var datos = form.serialize();
            var productos = $("#table_conceptos tbody > tr");
            var blockearfact=1;
            var mono  =  $('#tipomc_m').is(':checked')==true?1:0;
            var color =  $('#tipomc_c').is(':checked')==true?2:0;
            //==============================================
                var DATAa  = [];
                productos.each(function(){         
                    item = {};
                    //item ["selectedequipo"]   = $(this).find("input[id*='selectedequipo']").val();
                    //item ["selectedprefactura"]   = $(this).find("input[id*='selectedprefactura']").val();
                    //item ["facturarenta"]   = $(this).find("input[id*='facturarenta']").val();
                    //item ["facturaexcedente"]   = $(this).find("input[id*='facturaexcedente']").val();
                    item ["Cantidad"]   = $(this).find("input[id*='cantidad']").val();
                    item ["Unidad"]  = $(this).find("select[id*='unidadsat'] option:selected").val();
                    if ($(this).find("select[id*='unidadsat'] option:selected").val()==null) {
                        blockearfact=0;
                    }
                    item ["servicioId"]  = $(this).find("select[id*='conseptosat'] option:selected").val();
                    if ($(this).find("select[id*='conseptosat'] option:selected").val()==null) {
                        blockearfact=0;
                    }
                    item ["Descripcion"]  = $(this).find("select[id*='conseptosat'] option:selected").text();
                    item ["Descripcion2"]  = $(this).find("input[id*='descripcion']").val();
                    item ["Cu"]  = $(this).find("input[id*='precio']").val();
                    item ["descuento"]  = $(this).find("input[id*='descuento']").val()==''?0:$(this).find("input[id*='descuento']").val();
                    item ["Importe"]  = $(this).find("input[id*='precio']").val();
                    DATAa.push(item);
                });
                INFOa  = new FormData();
                aInfoa   = JSON.stringify(DATAa);
            //==============================================
                var equipos = $("#equipos_facturar tbody > tr");
                var DATAe  = [];
                equipos.each(function(){     
                    if($(this).find("input[class*='testadd']").is(':checked')){    
                                    item = {};
                                    item ["equipos"]   = $(this).find("input[class*='testadd']").val();
                                    item ["renta"]   = $(this).find("input[class*='testadd']").data('renta');
                                    item ["excedente"]   = $(this).find("input[class*='testadd']").data('excedente');
                                    var tipomonocolor = parseInt($(this).find("input[class*='testadd']").data('tipo'));
                                    if (mono==tipomonocolor) {
                                        DATAe.push(item);
                                    }
                                    if (color==tipomonocolor) {
                                        DATAe.push(item);
                                    }
                                    
                    }
                                });
                //aInfoe   = JSON.stringify(DATAe);
                aInfoe   = 'x';
            //==============================================
            //==============================================
                var equipospe = $("#tablepediodosextras tbody > tr");
                var DATApe  = [];
                equipospe.each(function(){     
                        itempe = {};
                        itempe ["contrato"]   = $(this).find("input[id*='contrato2']").val();
                        itempe ["periodo"]   = $(this).find("input[id*='prefacturaId2']").val();
                        DATAe.push(itempe);
                    
                    });
                aInfope   = JSON.stringify(DATApe);
            //==============================================
            var productoslength=productos.length;
            if (blockearfact==0) {
                productoslength=0;
            }



            if (productoslength>0) {
                var Subtotal=$('#Subtotal').val();
                var iva=$('#iva').val();
                var total=$('#total').val();
                var contrato=$('#idcontrato').val();
                var prefactura=$('#idfactura').val();
                var tipof=$("input[name='radio0']:checked").val();
                var tiporenta=$('#tiporenta').val();
                var visr= $('#isr').val();
                var vriva= $('#ivaretenido').val();
                var v5millar= $('#5almillarval').val();
                var outsourcing= $('#outsourcing').val();
                //var contrato2=$('#idcontrato_f option:selected').val();
                //var prefactura2=$('#idfactura_f option:selected').val();
                var f_r = $('#facturarelacionada').is(':checked')==true?1:0; //agregados version 4.0
                var f_r_t = $('#TipoRelacion option:selected').val();
                var f_r_uuid = $('#uuid_r').val();
                

                datos=datos+'&subtotal='+Subtotal+'&iva='+iva+'&visr='+visr+'&vriva='+vriva+'&v5millar='+v5millar+'&outsourcing='+outsourcing+'&total='+total+'&conceptos='+aInfoa+'&contratoId='+contrato+'&prefacturaId='+prefactura+'&periodosextras='+aInfope+'&equipos='+aInfoe+'&tipof='+tipof+'&tiporenta='+tiporenta+'&mono='+mono+'&color='+color+'&f_relacion='+f_r+'&f_r_tipo='+f_r_t+'&f_r_uuid='+f_r_uuid;;
                //===
                $('#modal_previefactura').modal({
                    dismissible: false
                });
                $('#modal_previefactura').modal('open');
                obtenerinfofiscal();
                /*
                setTimeout(function(){ 
                    var urlfac=base_url+"index.php/Preview/facturar?"+datos;
                    var htmliframe="<iframe src='"+urlfac+"' title='description' class='ifrafac'>";
                    $('.preview_iframe').html(htmliframe);
                    //window.location.href = base_url+"index.php/Preview/factura?"+datos; 
                }, 1000);
                */
                $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Preview/gen_facturar",
                    data: datos,
                    success: function (response){
                        var array = $.parseJSON(response);
                        if(array.idcont>0){
                            var urlfac=base_url+"index.php/Preview/facturar/"+array.idcont;
                            var htmliframe="<iframe src='"+urlfac+"' title='description' class='ifrafac'>";
                            $('.preview_iframe').html(htmliframe);
                        }
                    },
                    error: function(response){
                        notificacion(10);
                        $('body').loading('stop');
                    }
                });
            }else{
                alertfunction('Atención!','Agregar por lo menos un concepto');
            }
        }else{
            alertfunction('Atención!','Faltan campos obligatorios');
        }
}
//====================================================
function verficartiporfc(){
    var rfc = $('#rfc').val();
    if(rfc=='XAXX010101000'){
        var html='<div class="col s4">\
                    <label>Periodicidad</label>\
                    <select class="browser-default form-control-bmz" name="pg_periodicidad" id="pg_periodicidad" onchange="v_periocidad()">\
                      <option value="01">01 Diario</option>\
                      <option value="02">02 Semanal</option>\
                      <option value="03">03 Quincenal</option>\
                      <option value="04">04 Mensual</option>\
                      <option value="05">05 Bimestral</option>\
                    </select>\
                  </div>\
                  <div class="col s4">\
                    <label>Mes</label>\
                    <select class="browser-default form-control-bmz" name="pg_meses" id="pg_meses">\
                      <option value="01" class="select_no_bimestral">01 Enero</option>\
                      <option value="02" class="select_no_bimestral">02 Febrero</option>\
                      <option value="03" class="select_no_bimestral">03 Marzo</option>\
                      <option value="04" class="select_no_bimestral">04 Abril</option>\
                      <option value="05" class="select_no_bimestral">05 Mayo</option>\
                      <option value="06" class="select_no_bimestral">06 Junio</option>\
                      <option value="07" class="select_no_bimestral">07 Julio</option>\
                      <option value="08" class="select_no_bimestral">08 Agosto</option>\
                      <option value="09" class="select_no_bimestral">09 Septiembre</option>\
                      <option value="10" class="select_no_bimestral">10 Octubre</option>\
                      <option value="11" class="select_no_bimestral">11 Noviembre</option>\
                      <option value="12" class="select_no_bimestral">12 Diciembre</option>\
                      <option value="13" class="select_bimestral">13 Enero-Febrero</option>\
                      <option value="14" class="select_bimestral">14 Marzo-Abril</option>\
                      <option value="15" class="select_bimestral">15 Mayo-Junio</option>\
                      <option value="16" class="select_bimestral">16 Julio-Agosto</option>\
                      <option value="17" class="select_bimestral">17 Septiembre-Octubre</option>\
                      <option value="18" class="select_bimestral">18 Noviembre-Diciembre</option>\
                    </select>\
                  </div>\
                  <div class="col s4">\
                    <label>Año</label>\
                    <input type="number" name="pg_anio" id="pg_anio" class="form-control-bmz" readonly>\
                  </div>';
        $('.agregardatospublicogeneral').html(html);
        setTimeout(function(){ 
            var mesactual = $('#mesactual').val();
            var anioactual = $('#anioactual').val();
            console.log(mesactual);
            console.log(anioactual);
            $('#pg_meses').val(mesactual);
            $('#pg_anio').val(anioactual);
            $('.select_bimestral').hide('show');
        }, 1000);
    }else{
        $('.agregardatospublicogeneral').html('');
    }
    obtenerinfofiscal();
}
function v_periocidad(){
    var mesactual = $('#mesactual').val();
    var pg_periodicidad = $('#pg_periodicidad').val();
    if(pg_periodicidad=='05'){
        $('.select_no_bimestral').hide('show');
        $('.select_bimestral').show('show');
        if(mesactual=='01' || mesactual=='02'){
            var mesactual_n=13;
        }
        if(mesactual=='03' || mesactual=='04'){
            var mesactual_n=14;
        }
        if(mesactual=='05' || mesactual=='06'){
            var mesactual_n=15;
        }
        if(mesactual=='07' || mesactual=='08'){
            var mesactual_n=16;
        }
        if(mesactual=='09' || mesactual=='10'){
            var mesactual_n=17;
        }
        if(mesactual=='11' || mesactual=='12'){
            var mesactual_n=18;
        }
        $('#pg_meses').val(mesactual_n);
    }else{
        $('#pg_meses').val(mesactual);
        $('.select_no_bimestral').show('show');
        $('.select_bimestral').hide('show');
    }
    
}
function obtenerinfofiscal(){
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Facturaslis/viewdatosfiscalesinfo",
        data: {
            cliente:$('#idcliente').val(),
            rfc:$('#rfc').val(),
        },
        success: function (response){
            console.log(response);
            var array = $.parseJSON(response);
            //$('.v_nomfical').html(array.nombrefiscal);
            if(array.cp!=''){
                var cp=array.cp;
            }else{
                var cp='<span style="color:red"><b>Sin Direccion Fiscal</b></span>';
            }
            //$('.v_direccionfiscal').html(cp);
            if(array.regimen!=''){
                var regimen=array.regimen;
            }else{
                var regimen='<span style="color:red"><b>Sin regimen fiscal</b></span>';
            }
            //$('.v_regimenfiscal').html(regimen);
            var infodf='<div class="col s4">\
                          <b>Nombre Fiscal: </b><br><span >'+array.nombrefiscal+'</span>\
                        </div>\
                        <div class="col s4">\
                          <b>Direccion Fiscal(C.P.): </b><br><span >'+cp+'</span>\
                        </div>\
                        <div class="col s4">\
                          <b>Regimen Fiscal Receptor: </b><br><span >'+regimen+'</span>\
                        </div>';
            $('.infodatosfiscales').html(infodf);
            $( "#uso_cfdi ."+array.regimennumber ).prop( "disabled", false ).trigger('change');
            $( "#uso_cfdi" ).trigger("chosen:updated");
            selectusocfdi(array.regimenclave);
        },
        error: function(response){
            notificacion(10); 
        }
    });
}
function selectusocfdi(clave){
    clave=parseInt(clave);
    switch (clave) {
      case 601:
            $('#uso_cfdi').val('G03');
        break;
      case 603:
        $('#uso_cfdi').val('G03');
        break;
      case 606:
        $('#uso_cfdi').val('G03');
        break;
      case 612:
        $('#uso_cfdi').val('G03');
        break;
      case 620:
        $('#uso_cfdi').val('G03');
        break;
      case 621:
        $('#uso_cfdi').val('G03');
        break;
      case 622:
        $('#uso_cfdi').val('G03');
        break;
      case 623:
        $('#uso_cfdi').val('G03');
        break;
      case 624:
        $('#uso_cfdi').val('G03');
        break;
      case 625:
        $('#uso_cfdi').val('G03');
        break;
      case 626:
        $('#uso_cfdi').val('G03');
        break;
      default:
        console.log(clave);
    }
    $( "#uso_cfdi" ).trigger("chosen:updated");
}
function movertr(){
    $( "#table_conceptos tbody" ).sortable();
}
function cameu(){
    var excedente = $('.excedente.info');
    excedente.each(function(){
        var cantidad = $(this).data("cantidad");
        var precioexc = $(this).data("precioexc");
        var preid = $(this).data("preid");
        console.log(cantidad+'_'+precioexc+'_'+preid);
        $('.cant_'+preid).val(cantidad);
        $('.precio_'+preid).val(precioexc);
    });
    setTimeout(function(){ 
        calcularmontos();
    }, 1000);
}
var rowcobrar=0;
function agregarex(tipo){
    var idfactura = $('#idfactura').val();
    var idcontrato = $('#idcontrato').val();
    var descripcion = $('.btn-agrega_'+tipo).data('agregado');
    var monto = $('.btn-agrega_'+tipo).data('monto');
    var html='';
    if(tipo==0){
        html='<tr class="datos_principal tr_5">\
                      <td>\
                        <input id="selectedequipo" name="selectedequipo" type="hidden" value="0" readonly>\
                        <input id="selectedprefactura" name="selectedprefactura" type="hidden" value="'+idfactura+'" readonly>\
                        <input id="facturarenta" name="facturarenta" type="hidden" value="0" readonly>\
                        <input id="facturaexcedente" name="facturaexcedente" type="hidden" value="1" readonly>\
                        <input id="cantidad" name="cantidad" type="text" value="1" class="excedente info cant_'+rowcobrar+'" data-cantidad="'+monto+'" data-precioexc="'+monto+'" data-preid="'+rowcobrar+'" readonly></td>\
                      <td>\
                        <div style="width:250px">\
                          <select id="unidadsat" class="unidadsat browser-default">\
                          <option value="E48">E48 / Unidad de servicio</option>\
                          </select>\
                        <div>\
                      </td>\
                      <td>\
                        <div style="width:250px">\
                          <select id="conseptosat" class="conseptosat browser-default">\
                          <option value="80161801">80161801 / Servicio de alquiler o leasing de fotocopiadoras</option>\
                          </select>\
                        <div>\
                      </td>\
                      <td><input id="descripcion" name="descripcion" type="text" value="'+descripcion+'"></td>\
                      <td><input id="precio" name="precio" class="precio excedente precio_'+rowcobrar+'" type="text" value="'+monto+'" readonly></td>\
                      <td>\
                          <input type="number"\
                            name="descuento"\
                            id="descuento"\
                            class="form-control-bmz cdescuento cdescuento_'+rowcobrar+'"\
                            onclick="activardescuento('+rowcobrar+')"\
                            value="0" readonly>\
                      </td>\
                    </tr>';
                    rowcobrar++;
    }
    if(tipo==1){
        html='<tr class="datos_principal tr_6">\
                      <td>\
                        <input id="selectedequipo" name="selectedequipo" type="hidden" value="0" readonly>\
                        <input id="selectedprefactura" name="selectedprefactura" type="hidden" value="'+idfactura+'" readonly>\
                        <input id="facturarenta" name="facturarenta" type="hidden" value="0" readonly>\
                        <input id="facturaexcedente" name="facturaexcedente" type="hidden" value="1" readonly>\
                        <input id="cantidad" name="cantidad" type="text" value="1" class="excedente info cant_'+rowcobrar+'" data-cantidad="'+monto+'" data-precioexc="'+monto+'" data-preid="'+rowcobrar+'" readonly></td>\
                      <td><div style="width:250px">\
                          <select id="unidadsat" class="unidadsat browser-default">\
                          <option value="E48">E48 / Unidad de servicio</option>\
                          </select>\
                        <div>\
                      </td>\
                      <td>\
                          <div style="width:250px">\
                            <select id="conseptosat" class="conseptosat browser-default">\
                            <option value="80161801">80161801 / Servicio de alquiler o leasing de fotocopiadoras</option>\
                            </select>\
                          <div>\
                      </td>\
                      <td><input id="descripcion" name="descripcion" type="text" value="'+descripcion+'"></td>\
                      <td><input id="precio" name="precio" class="precio excedente precio_'+rowcobrar+'" type="text" value="'+monto+'" readonly></td>\
                      <td>\
                          <input type="number"\
                            name="descuento"\
                            id="descuento"\
                            class="form-control-bmz cdescuento cdescuento_'+rowcobrar+'"\
                            onclick="activardescuento('+rowcobrar+')"\
                            value="0" readonly>\
                      </td>\
                    </tr>'; 
                    rowcobrar++;
    }
    $('.addcobrar').append(html); 
    calcularmontos();
                movertr();
}
function view_ccs_montos(){
    $.ajax({
            type:'POST',
            url: base_url+'index.php/Configuracionrentas_ext/view_ccs_montos',
            data: {
                prefid:$('#idfactura').val()
            },
            success:function(data){
                var array = $.parseJSON(data);
                setTimeout(function(){ 
                array.ccsmontos.forEach(function(element) {
                    $('.info_cc_'+element.idcentroc).val(element.monto);
                });
            }, 1000);
            }
        });
}
function agregarequiposccs(idccs,idpre,idcontrato,fechai,fechaf,tipo){
    $.ajax({
            type:'POST',
            url: base_url+"index.php/Configuracionrentas_ext/agregarequiposccs",
            data: {
                idccs:idccs,
                idpre:idpre,
                idcontrato:idcontrato,
                fechai:fechai,
                fechaf:fechaf,
                tipo:tipo
            },
            success: function (data){
                //$('.datos_principal').remove(); 
                $('.addcobrar').append(data); 
                unidadconceptosat();
                calcularmontos();
                movertr();
                deletetrrow();
                console.log('muestra datos');
            }
    });
}
function deletetrrow(){
    $('.deletetrrow').click(function(event) {
        $(this).closest('tr').remove();
        calcularmontos();
    });
}
function registrofac(tipofac){
    $( ".registrofac" ).prop( "disabled", true );
        setTimeout(function(){ 
            $(".registrofac" ).prop( "disabled", false );
        }, 10000);
        var form =$('#validateSubmitForm');
        var valid =form.valid();
        if (valid) {
            var datos = form.serialize();
            var productos = $("#table_conceptos tbody > tr");
            var blockearfact=1;
            var mono  =  $('#tipomc_m').is(':checked')==true?1:0;
            var color =  $('#tipomc_c').is(':checked')==true?2:0;
            //==============================================
                var DATAa  = [];
                productos.each(function(){         
                    item = {};
                    item ["selectedequipo"]   = $(this).find("input[id*='selectedequipo']").val();
                    item ["selectedprefactura"]   = $(this).find("input[id*='selectedprefactura']").val();
                    item ["facturarenta"]   = $(this).find("input[id*='facturarenta']").val();
                    item ["facturaexcedente"]   = $(this).find("input[id*='facturaexcedente']").val();
                    item ["Cantidad"]   = $(this).find("input[id*='cantidad']").val();
                    item ["Unidad"]  = $(this).find("select[id*='unidadsat'] option:selected").val();
                    if ($(this).find("select[id*='unidadsat'] option:selected").val()==null) {
                        blockearfact=0;
                    }
                    item ["servicioId"]  = $(this).find("select[id*='conseptosat'] option:selected").val();
                    if ($(this).find("select[id*='conseptosat'] option:selected").val()==null) {
                        blockearfact=0;
                    }
                    item ["Descripcion"]  = $(this).find("select[id*='conseptosat'] option:selected").text();
                    item ["Descripcion2"]  = $(this).find("input[id*='descripcion']").val();
                    item ["Cu"]  = $(this).find("input[id*='precio']").val();
                    if ($(this).find("input[id*='precio']").val()>0) { }else{
                        blockearfact=0;
                    }
                    item ["descuento"]  = $(this).find("input[id*='descuento']").val()==''?0:$(this).find("input[id*='descuento']").val();
                    item ["Importe"]  = $(this).find("input[id*='precio']").val();
                    DATAa.push(item);
                });
                INFOa  = new FormData();
                aInfoa   = JSON.stringify(DATAa);
            //==============================================
                var equipos = $("#equipos_facturar tbody > tr");
                var DATAe  = [];
                equipos.each(function(){     
                    if($(this).find("input[class*='testadd']").is(':checked')){    
                        item = {};
                        item ["equipos"]   = $(this).find("input[class*='testadd']").val();
                        item ["renta"]   = $(this).find("input[class*='testadd']").data('renta');
                        item ["excedente"]   = $(this).find("input[class*='testadd']").data('excedente');
                        var tipomonocolor = parseInt($(this).find("input[class*='testadd']").data('tipo'));
                        if (mono==tipomonocolor) {
                            DATAe.push(item);
                        }
                        if (color==tipomonocolor) {
                            DATAe.push(item);
                        }
                                    
                    }
                                });
                aInfoe   = JSON.stringify(DATAe);
            //==============================================
            //==============================================
                var equipospe = $("#tablepediodosextras tbody > tr");
                var DATApe  = [];
                equipospe.each(function(){     
                        itempe = {};
                        itempe ["contrato"]   = $(this).find("input[id*='contrato2']").val();
                        itempe ["periodo"]   = $(this).find("input[id*='prefacturaId2']").val();
                        DATApe.push(itempe);
                    
                    });
                aInfope   = JSON.stringify(DATApe);
            //==============================================
            var productoslength=productos.length;
            if (blockearfact==0) {
                productoslength=0;
            }



            if (productoslength>0) {
                if(tipofac==1){
                    $('body').loading({theme: 'dark',message: 'Timbrando factura...'});
                }else{
                    $('body').loading({theme: 'dark',message: 'Guardado factura...'});
                }
                
                var Subtotal=$('#Subtotal').val();
                var iva=$('#iva').val();
                var total=$('#total').val();
                var contrato=$('#idcontrato').val();
                var prefactura=$('#idfactura').val();
                var tipof=$("input[name='radio0']:checked").val();
                var tiporenta=$('#tiporenta').val();
                var visr= $('#isr').val();
                var vriva= $('#ivaretenido').val();
                var v5millar= $('#5almillarval').val();
                var outsourcing= $('#outsourcing').val();
                //var contrato2=$('#idcontrato_f option:selected').val();
                //var prefactura2=$('#idfactura_f option:selected').val();
                var f_r = $('#facturarelacionada').is(':checked')==true?1:0; //agregados version 4.0
                var f_r_t = $('#TipoRelacion option:selected').val();
                var f_r_uuid = $('#uuid_r').val();
                var des=$('#descuentof').val();
                var fac_pen = $('#fac_pen').is(':checked')==true?1:0;

                datos=datos+'&subtotal='+Subtotal+'&iva='+iva+'&visr='+visr+'&vriva='+vriva+'&v5millar='+v5millar+'&outsourcing='+outsourcing+'&total='+total+'&conceptos='+aInfoa+'&contratoId='+contrato+'&prefacturaId='+prefactura+'&periodosextras='+aInfope+'&equipos='+aInfoe+'&tipof='+tipof+'&tiporenta='+tiporenta+'&mono='+mono+'&color='+color+'&f_r='+f_r+'&f_r_t='+f_r_t+'&f_r_uuid='+f_r_uuid+'&des='+des+'&fac_pen='+fac_pen+'&tipofac='+tipofac;
                $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Configuracionrentas/generafacturar",
                    data: datos,
                    success: function (response){
                        console.log(response);
                        var array = $.parseJSON(response);
                        if (array.resultado=='error') {
                            swal({    title: "Error "+array.CodigoRespuesta+"!",
                                          text: array.MensajeError,
                                          type: "warning",
                                          showCancelButton: false
                             });
                            redir_con(contrato);
                        }else{
                            if (array.resultado=='save') {
                                //save_facturasid = array.facturasid;
                                if(array.facturasid>0){
                                    confirm_save(array.facturasid,contrato);
                                }else{
                                    swal("Éxito!", "Se ha guardado la factura", "success");
                                    redir_con(contrato);
                                }
                                
                            }else{
                                swal("Éxito!", "Se ha creado la factura", "success");
                                redir_con(contrato);
                            }
                            
                        }
                        $('body').loading('stop');
                        /*
                        alertfunction('Éxito!','Se ha creado la factura: '); 
                        setTimeout(function(){ 
                            //window.open(base_url+"index.php/Cotizaciones/documentoHorizontalConBordes/"+response.trim()); 
                        }, 2000);*/
                        
                        
                    },
                    error: function(response){
                        notificacion(10);
                        $('body').loading('stop');
                    }
                });
            }else{
                alertfunction('Atención!','Agregar por lo menos un concepto');
            }
        }else{
            alertfunction('Atención!','Faltan campos obligatorios');
        }
}
function redir_con(con){
    setTimeout(function(){ 
        window.location.href = base_url+"index.php/Configuracionrentas/ContratosPagos/"+con; 
    }, 3000);
}
function confirm_save(idfac,con){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Guardado',
        content: 'Se ha guardado la factura.<br>¿Desea programar timbrado?',
        type: 'green',
        typeAnimated: true,
        buttons:{
            Programar: function (){
                confirm_save_program(idfac,con);
            },
            Cancelar: function () {
                window.location.href = base_url+"index.php/Configuracionrentas/ContratosPagos/"+con; 
            }
        }
    });
}
function confirm_save_program(idfac,con){
    var html='Fecha de Timbrado<br>';
        html+='<input type="date" placeholder="Contraseña" id="fecha_tim" class="form-control-bmz" required/>'
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Programar',
        content: html,
        type: 'green',
        typeAnimated: true,
        buttons:{
            Programar: function (){
                var fechat=$('#fecha_tim').val();
                
                     $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Configuracionrentas_ext/save_program",
                        data: {
                            fecha:fechat,
                            idfac:idfac
                        },
                        success: function (response){
                            toastr.success('Se a programado el Timbrando');
                            redir_con(con);
                        },
                        error: function(response){
                            notificacion(10); 
                        }
                    });
            },
            Cancelar: function () {
                window.location.href = base_url+"index.php/Configuracionrentas/ContratosPagos/"+con; 
            }
        }
    });
}


function contratosview(){
    var contrato=$('#idcontrato').val();
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Configuracionrentas/viewcontratopago",
        data: {
            idcontrato:contrato
        },
        success: function (data){
            var array = $.parseJSON(data);

            if(array.ordenc_numero!=''){
                $('#numordencompra').val(array.ordenc_numero).change();
                //$('#ordenc_vigencia').val(array.ordenc_vigencia);
                $('.li_pedimentos').click();
            }


           
            
               
        }
    });
}
function fac_pen_pago_pue(num,idcon){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: 'Tiene facturas PUE del mes anterior sin completar Pagos<br>Se necesita permisos de administrador para continuar<br>'+
                '<input type="text" placeholder="Contraseña" id="contrasena4" name="contrasena4" class="name form-control-bmz" required/>',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var pass=$("input[name=contrasena4]").val();
                if (pass!='') {
                     $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Sistema/solicitarpermiso",
                        data: {
                            pass:pass
                        },
                        success: function (response){
                                var respuesta = parseInt(response);
                                if (respuesta==1) {
                                    //==================================
                                        
                                    //=================================
                                }else{
                                    fac_pen_pago_pue(num);
                                    notificacion(2);
                                    
                                }
                        },
                        error: function(response){
                            notificacion(1);
                        }
                    });
                    
                }else{
                    notificacion(0);
                }
            },
            cancelar: function () {
                window.location.href = base_url+"index.php/Configuracionrentas/ContratosPagos/"+idcon;
            }
        }
    });
    setTimeout(function(){
            new MaskedPassword(document.getElementById("contrasena4"), '\u25CF');
    },500);
}