<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class Login_model extends CI_Model 
{
    public function __construct() 
    {
        parent::__construct();
    }

    function login($usuario) 
    {
    	$strq = "CALL SP_GET_SESSION('".$usuario."');";
        $query = $this->db->query($strq);
        return $query->result();
    }

    function getMenus($perfil)
    {
        $strq ="SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.PerfilId='$perfil' ORDER BY men.MenuId ASC";
        $query = $this->db->query($strq);
        return $query->result();
    } 

    function submenus($perfil,$menu){
        $strq ="SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon from menu_sub as menus, perfiles_detalles as perfd WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='$perfil' and menus.MenuId='$menu' ORDER BY menus.MenusubId ASC";
        $query = $this->db->query($strq);
        return $query->result();
    }
}