<?php
require_once dirname(__FILE__) . '/TCPDF3/examples/tcpdf_include.php'; 
require_once dirname(__FILE__) . '/TCPDF3/tcpdf.php';

  $logos = base_url().'public/img/alta.jpg';
  $logos2 = base_url().'public/img/kyocera.jpg';
  //=========================================================================
    $GLOBALS['configuracionCotizacion']=$configuracionCotizacion;
    $GLOBALS["fecha_crea"]=$fecha_crea;
    $GLOBALS["fecha_asig"]=$fecha_asig;
    $GLOBALS["tipo_serv"]=$tipo_serv;
    $GLOBALS["responsable_asig"]=$responsable_asig;
    $GLOBALS["hora_asig"]=$hora_asig;
    $GLOBALS["prioridad"]=$prioridad;
    $GLOBALS["descripcion_serv_fall"]=$descripcion_serv_fall;
    $GLOBALS["modelo"]=$modelo;
    $GLOBALS["serie"]=$serie;
    $GLOBALS["direcc_inst_serv"]=$direcc_inst_serv;
    $GLOBALS["tel"]=$tel;
    $GLOBALS["cel"]=$cel;
    $GLOBALS["docum_acce"]=$docum_acce;
    $GLOBALS["observa_edi"]=$observa_edi;
    $GLOBALS["lectu_ini"]=$lectu_ini;
    $GLOBALS["km_veh_i"]=$km_veh_i;
    $GLOBALS["lectur_fin"]=$lectur_fin;
    $GLOBALS["km_veh_f"]=$km_veh_f;
    $GLOBALS["asesor_ser"]=$asesor_ser;
    $GLOBALS["gerencia"]=$gerencia;
    $GLOBALS["firma"]=$firma;
    $GLOBALS["comentarioejecutiva"]=$comentarioejecutiva;
    $GLOBALS["comentariotecnico"]=$comentariotecnico;
    $GLOBALS["nombre_empresa"]=$nombre_empresa;
    $GLOBALS["bodega_or_name"]=$bodega_or_name;
    

    if($GLOBALS["firma"]!=''){  
       
      $fh = fopen(base_url()."uploads/firmasservicios/".$GLOBALS["firma"], 'r') or die("Se produjo un error al abrir el archivo");
            $linea = fgets($fh);
            fclose($fh);   
          $linea=preg_replace('#^data:image/[^;]+;base64,#', '', $linea) ;    
    } 
    if($GLOBALS["firma"]!=''){
      $imgtext_title='Firma del cliente'; 
      $firmaimg='<img src="@'.$linea.'" width="335"  height="160" style="border:dotted 1px black;">';
    }else{
      $imgtext_title='Nombre de quien validó';
      $firmaimg=$qrecibio;
    } 
  //=========================================================================
//====================================================
class MYPDF extends TCPDF {
 
  public function Header() {

      
      //$this->writeHTML($html, true, false, true, false, '');
  }
    // Page footer
  public function Footer() {
    $html2='';
    
      //<img src="http://facturacion33.adminfactura.com.mx/view/image/viamex_logob100.png" width="100px">
      //$this->writeHTML($html2, true, false, true, false, '');
      
  }
} 
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Kyocera');
$pdf->SetTitle('Servicio');
$pdf->SetSubject('factura');
$pdf->SetKeywords('factura');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins('7', '10', '7');
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin('10');

// set auto page breaks
$pdf->SetAutoPageBreak(true, 10);//PDF_MARGIN_BOTTOM

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('dejavusans', '', 7);
// add a page
$pdf->AddPage('P', 'A4');
$html='<style type="text/css">
          .t_c{text-align:center;}
          .t_b{font-weight: bold;}
          .bor_b{border-bottom: 1px solid black;}
          .bor_l{border-left: 1px solid black;}
          .bor_r{border-right: 1px solid black;}
          .bor_t{border-top: 1px solid black;}
          .tableb th{background-color:rgb(217,217,217);}
        </style>';

  $html.='<table border="0">
            <tr>
              <td width="20%" class="t_c"><img src="'.$logos.'" width="80px"></td>
              <td width="60%" class="t_c">
                "ALTA PRODUCTIVIDAD, S.A DE C.V."<br>
                RFC: APR980122KZ6<br>
                Impresoras y Multifuncionales, Venta, Renta y Mantenimiento.<br>
                Domicilio Fiscal: Blvd. Norte 1831 Col. Villas San Alejandro. Puebla, Pue. Mexico C.P.72090<br>
                Banco BANAMEX, Cuenta 7347132, Sucursal 826, CLABE 2668082673471320<br>
                Call Center 273 3400, 249 5393. www.kyoceraap.com

              </td>
              <td width="20%" class="t_c"><img src="'.$logos2.'" width="100px"></td>
            </tr>
          </table>
          <p></p>';
          $datosconsumiblesfolio = $this->ModeloServicio->getfoliosserviciosserie($idpdf,$serieId);
            if ($datosconsumiblesfolio->num_rows()>0) {
              $html.='<table class="tableb">
                        <tr>
                          <th>Cantidad</th>
                          <th>Marca</th>
                          <th>Modelo</th>
                          <th>parte</th>
                          <th>Folio</th>
                        </tr>
                      ';
                foreach ($datosconsumiblesfolio->result() as $item) {
                   $html.='
                        <tr>
                          <td>1</td>
                          <td>KYOCERA</td>
                          <td>'.$item->modelo.'</td>
                          <td>'.$item->parte.'</td>
                          <td>'.$item->foliotext.'</td>
                        </tr>
                      ';
                }
              $html.='</table>';
            }
    
              


    $html.='<table class="tableb" cellpadding="4">
              <tr>
                <th class="t_c t_b bor_b bor_l bor_r bor_t">INFORMACIÓN GENERAL DEL SERVICIO</th>
              </tr>
            </table>
            <table class="tableb" cellpadding="4">
              <tr>
                <th class="bor_l bor_t">FECHA DE CREACION </th>
                <td class="  bor_t">'.date('d-m-Y',strtotime($GLOBALS["fecha_crea"])).'</td>
                <th class="  bor_t">RESPONSABLE ASIGNADO</th>
                <td class=" bor_r bor_t">'.$GLOBALS["responsable_asig"].'</td>
              </tr>
              <tr>
                <th class="bor_l ">FECHA DE ASIGNACION </th>
                <td class="  ">'.date('d-m-Y',strtotime($GLOBALS["fecha_asig"])).'</td>
                <th class="  ">HORA DE ASIGNACION</th>
                <td class=" bor_r ">'.date('g:i a',strtotime($GLOBALS["hora_asig"])).'</td>
              </tr>
              <tr>
                <th class="bor_l bor_b">TIPO DE SERVICIO</th>
                <td class="  bor_b">'.$GLOBALS["tipo_serv"].'</td>
                <th class="  bor_b">PRIORIDAD</th>
                <td class=" bor_r bor_b">'.$GLOBALS["prioridad"].'</td>
              </tr>
            </table>
            <p></p>
            <table class="tableb" cellpadding="4">
              <tr>
                <th class="t_c t_b bor_b bor_l bor_r bor_t">INFORMACIÓN DEL EQUIPO</th>
              </tr>
            </table>
            <table class="tableb" cellpadding="4">
              <tr>
                <th width="8%" class="bor_b bor_l bor_t">Modelo</th>
                <td width="13%" class="bor_b bor_t">'.$GLOBALS["modelo"].'</td>
                <th width="9%" class="bor_b bor_t">Serie</th>
                <td width="10%" class="bor_b bor_t">'.$GLOBALS["serie"].'</td>
                <th width="22%" class="bor_b bor_t">Descripción del servicio ó falla : </th>
                <td width="38%" class="bor_b bor_r bor_t">'.$GLOBALS["descripcion_serv_fall"].'</td>
              </tr>';
              if($comentarioinfo!=''){
                $html.='<tr><td colspan="6" class="bor_l bor_r">'.$comentarioinfo.'</td></tr>';
              }
                if($tpoliza==22){
                  $rentaequipos_accesorios = $this->ModeloCatalogos->rentaequipos_accesorios1($idRenta,$idEquipo,$id_equipo);
                    if($rentaequipos_accesorios->num_rows()>0){
                      //$html.='<tr><th class="t_c t_b bor_b bor_l bor_r bor_t" colspan="6">INFORMACIÓN DE ACCESORIOS</th></tr>';
                    }
                    foreach ($rentaequipos_accesorios->result() as $itemacc) {
                        //$equipos .='<tr><td>'.$itemacc->cantidad.' '.$itemacc->nombre.'</td><td>'.$itemacc->series.'</td><td></td></tr>';
                        $html.='<tr>
                                <th class="bor_b bor_l bor_t">Modelo Accesorio</th>
                                <td class="bor_b bor_t">'.$itemacc->nombre.'</td>
                                <th class="bor_b bor_t">Serie</th>
                                <td class="bor_b bor_t bor_r" colspan="3">'.$itemacc->series.'</td>
                              </tr>';
                    }
                    $arrayconsumibles=array();
                    $rentaequipos_consumible = $this->ModeloCatalogos->rentaequipos_consumible1($idRenta,$idEquipo,$id_equipo);
                    foreach ($rentaequipos_consumible->result() as $item0) {
                        $contrato_folio2 = $this->ModeloCatalogos->obtenerfoliosconsumiblesrenta3($idRenta,$item0->id_consumibles,$item0->id,$serieId); 
                        foreach ($contrato_folio2->result() as $itemx) {
                          $arrayconsumibles[$itemx->idcontratofolio]=array('modelo'=>$item0->modelo,'parte'=>$item0->parte,'folio'=>$itemx->foliotext);
                            //if($idcontratofolio!=$itemx->idcontratofolio){
                                //$equipos .='<tr><td>'.$item0->modelo.' No° parte '.$item0->parte.'</td><td>'.$itemx->foliotext.'</td><td></td></tr>';
                                /*
                                $html.='<tr>
                                          <th class="bor_b bor_l bor_t">Modelo Consumible</th>
                                          <td class="bor_b bor_t">'.$item0->modelo.'</td>
                                          <th class="bor_b bor_t">Folio</th>
                                          <td class="bor_b bor_t bor_r" colspan="3">'.$itemx->foliotext.'</td>
                                        </tr>';
                                */
                            //}
                              
                              
                        }
                    }
                    foreach ($arrayconsumibles as $itemu) {
                        //$equipos .='<tr><td>'.$itemu['modelo'].' No° parte '.$itemu['parte'].'</td><td>'.$itemu['folio'].'</td><td></td></tr>';
                        $html.='<tr>
                                          <th class="bor_b bor_l bor_t">Modelo Consumible</th>
                                          <td class="bor_b bor_t">'.$itemu['modelo'].'</td>
                                          <th class="bor_b bor_t">Folio</th>
                                          <td class="bor_b bor_t bor_r" colspan="3">'.$itemu['folio'].'</td>
                                        </tr>';
                    }
                }
              
              
            $html.='</table>
            <table class="tableb" cellpadding="4">
              <tr>
                <th class="t_c t_b bor_b bor_l bor_r bor_t">INFORMACION DEL CLIENTE </th>
              </tr>
            </table>
            <table class="tableb" cellpadding="4">
              <tr>
                <td width="30%" class=" bor_l bor_t">Nombre</td>
                <td width="70%" class=" bor_r bor_t">'.$GLOBALS["nombre_empresa"].'</td>
              </tr>
              <tr>
                <td class=" bor_l ">Dirección de instalación/servicio</td>
                <td class=" bor_r ">'.$GLOBALS["direcc_inst_serv"].'</td>
              </tr>
              <tr>
                <td class=" bor_l ">Contacto</td>
                <td class=" bor_r ">'.$contacto.'</td>
              </tr>
              <tr>
                <td class=" bor_l ">Puesto</td>
                <td class=" bor_r ">'.$puesto.'</td>
              </tr>
              <tr>
                <td class=" bor_l ">Teléfono</td>
                <td class=" bor_r ">'.$GLOBALS["tel"].'</td>
              </tr>
              <tr>
                <td class=" bor_l ">Celular</td>
                <td class=" bor_r ">'.$GLOBALS["cel"].'</td>
              </tr>
              <tr>
                <td class="bor_b bor_l ">Documentos y accesorios para el ingreso</td>
                <td class="bor_b bor_r ">'.$GLOBALS["docum_acce"].'</td>
              </tr>
            </table>
            <p></p>
            <table class="tableb" cellpadding="4">
              <tr>
                <th class="t_c t_b bor_b bor_l bor_r bor_t">Observaciones Técnico</th>
                <th class="t_c t_b bor_b bor_l bor_r bor_t">Observaciones Ejecutiva</th>
              </tr>
              <tr>
                <td class="t_c bor_b  bor_l bor_r bor_t">'.$GLOBALS["comentariotecnico"].'</td>
                <td class="t_c  bor_b bor_l bor_r bor_t">'.$GLOBALS["comentarioejecutiva"].'</td>
              </tr>
              <tr>
                <th class="t_c t_b bor_b bor_l bor_r bor_t">'.$imgtext_title.'</th>
                <th class="t_c t_b bor_b bor_l bor_r bor_t">Asesor de servicio</th>
              </tr>
              <tr>
                <td class="t_c  bor_b bor_l bor_r ">'.$firmaimg.'</td>
                <td class="t_c  bor_b bor_l bor_r ">'.$GLOBALS["asesor_ser"].'</td>
              </tr>
              

            </table>
            ';


$pdf->writeHTML($html, true, false, true, false, '');



$pdf->IncludeJS('print(true);');
$pdf->Output('Folios_.pdf', 'I');

?>