<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Fac_pen_ren extends CI_Controller {
    public function __construct()     {
        parent::__construct();

        $this->load->model('ModeloCatalogos');
        $this->load->model('Rentas_model');
        $this->load->helper('url');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fecha = date('Y-m-d');
        $this->submenu=64;
        if($this->session->userdata('logeado')==true){
            $this->perfilid =$this->session->userdata('perfilid');
            $this->idpersonal = $this->session->userdata('idpersonal');
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,$this->submenu);// 52 es el id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
        }else{
            redirect('login'); 
        }
    }

    function index(){
        $data['MenusubId']=$this->submenu;
        $data['idpersonal']=$this->idpersonal;
        $this->load->view('header');
        $this->load->view('main');
        $this->load->view('rentas/pendientefac',$data);
        $this->load->view('footer');
        $this->load->view('rentas/pendientefacjs');
    }
    
    public function getlistpendientes() {
        $params = $this->input->post();
        //log_message('error',json_encode($params));
        $f1 = $this->input->post('fechas');
        $f2 = $this->input->post('fechas2');
        $tipo = $this->input->post('tipo');
        $tipov = $this->input->post('tipov');
        $cli = $this->input->post('cli');
        $fechafinal = strtotime ( '- 3 days' , strtotime ( $this->fecha ) ) ;
        $fechafinal = date ( 'Y-m-d' , $fechafinal ); //log_message('error', 'cli: '.$cli);
        
        $result_temp = $this->ModeloCatalogos->getselectwheren('temp_periodos',array('activo'=>1));

        $getdata = $this->Rentas_model->getListado_fac_pen_ren($f1,$f2,$fechafinal,$tipo,$this->idpersonal,$cli);
        if($tipov==1){
            $getdata=array();
        }

        if($tipo<2){//diferente de facturadas
            $getdata_temp = $this->Rentas_model->getListado_fac_pen_ren2($cli);
            foreach ($getdata_temp->result() as $item) {
                $periodo_inicial=$item->periodo_inicial;
                $periodo_final=$item->periodo_final;
                
                $date1 = new DateTime($this->fecha);
                $date2 = new DateTime($periodo_final);
                $diff = $date1->diff($date2);

                for ($i = 1; $i <= $diff->m; $i++) {
                    $periodo_inicial=$this->sumarummes($periodo_inicial);
                    $periodo_final=$this->sumarummes($periodo_final);

                    $estatus=1;
                    $asignadas=0;
                    $fechaasignadas='';
                    $estatus_motivo='';
                    $estatus_confirm=0;
                    $prioridad=0;
                    $comentario='';
                    foreach ($result_temp->result() as $itemt) {
                        $itemtfechaini=$itemt->fechaini;
                        if($itemt->contrato==$item->idcontrato and $itemtfechaini==$periodo_inicial ){
                            $estatus=$itemt->estatus;
                            $asignadas=$itemt->asignadas;
                            $fechaasignadas=$itemt->fechaasignada;
                            $estatus_motivo=$itemt->estatus_motivo;
                            $estatus_confirm=$itemt->estatus_confirm;
                            $prioridad=$itemt->prioridad;
                            $comentario=$itemt->comen;
                        }
                        
                    }
                    //if($this->idpersonal==50 and $asignadas==1){
                    if($this->idpersonal==13 and $asignadas==1){
                        $getdata[]=array('idcontrato'=>$item->idcontrato,'empresa'=>$item->empresa,'periodo_inicial'=>$periodo_inicial,'periodo_final'=>$periodo_final,'nombre'=>'','apellido_paterno'=>'','apellido_materno'=>'','fechatimbre'=>'','nombref'=>'','apellido_paternof'=>'','apellido_maternof'=>'','estatus'=>$estatus,'asignadas'=>$asignadas,'fechaasignada'=>$fechaasignadas,'prefId'=>0,'cargadas'=>0,'estatus_motivo'=>$estatus_motivo,'estatus_confirm'=>$estatus_confirm,'prioridad'=>$prioridad,'comen'=>$comentario);
                    }else{
                        //if($this->idpersonal!=50){
                        if($this->idpersonal!=13){
                        if($estatus==$tipo and $estatus==0 and $tipov!=2){
                            //log_message('error','descartardas ');
                            $getdata[]=array('idcontrato'=>$item->idcontrato,'empresa'=>$item->empresa,'periodo_inicial'=>$periodo_inicial,'periodo_final'=>$periodo_final,'nombre'=>'','apellido_paterno'=>'','apellido_materno'=>'','fechatimbre'=>'','nombref'=>'','apellido_paternof'=>'','apellido_maternof'=>'','estatus'=>$estatus,'asignadas'=>$asignadas,'fechaasignada'=>$fechaasignadas,'prefId'=>0,'cargadas'=>0,'estatus_motivo'=>$estatus_motivo,'estatus_confirm'=>$estatus_confirm,'prioridad'=>$prioridad,'comen'=>$comentario);
                        }
                        if($estatus==$tipo and $estatus==1 and $tipov!=2){
                            //log_message('error','vigentes'.$this->idpersonal);
                            $getdata[]=array('idcontrato'=>$item->idcontrato,'empresa'=>$item->empresa,'periodo_inicial'=>$periodo_inicial,'periodo_final'=>$periodo_final,'nombre'=>'','apellido_paterno'=>'','apellido_materno'=>'','fechatimbre'=>'','nombref'=>'','apellido_paternof'=>'','apellido_maternof'=>'','estatus'=>$estatus,'asignadas'=>$asignadas,'fechaasignada'=>$fechaasignadas,'prefId'=>0,'cargadas'=>0,'estatus_motivo'=>$estatus_motivo,'estatus_confirm'=>$estatus_confirm,'prioridad'=>$prioridad,'comen'=>$comentario);
                        }
                        }
                        

                    }

                    //$getdata[]=array('idcontrato'=>$item->idcontrato,'empresa'=>$item->empresa,'periodo_inicial'=>$periodo_inicial,'periodo_final'=>$periodo_final,'nombre'=>'','apellido_paterno'=>'','apellido_materno'=>'','fechatimbre'=>'','nombref'=>'','apellido_paternof'=>'','apellido_maternof'=>'','estatus'=>$estatus,'asignadas'=>$asignadas,'fechaasignada'=>$fechaasignadas,'prefId'=>0,'cargadas'=>0 );
                }
            }

        }



















        /*
        if($this->idpersonal!=50){
            if($tipo==1 and $tipov<2){
                $getdata_temp = $this->Rentas_model->getListado_fac_pen_ren2();
                foreach ($getdata_temp->result() as $item) {
                    $periodo_inicial=$item->periodo_inicial;
                    $periodo_final=$item->periodo_final;
                    
                    $date1 = new DateTime($this->fecha);
                    $date2 = new DateTime($periodo_final);
                    $diff = $date1->diff($date2);

                    for ($i = 1; $i <= $diff->m; $i++) {
                        $periodo_inicial=$this->sumarummes($periodo_inicial);
                        $periodo_final=$this->sumarummes($periodo_final);

                        $estatus=1;
                        $asignadas=0;
                        $fechaasignadas='';

                        foreach ($result_temp->result() as $itemt) {
                            $itemtfechaini=$itemt->fechaini;
                            if($itemt->contrato==$item->idcontrato and $itemtfechaini==$periodo_inicial ){
                                $estatus=$itemt->estatus;
                                $asignadas=$itemt->asignadas;
                                $fechaasignadas=$itemt->fechaasignada;
                            }
                            
                        }

                        $getdata[]=array('idcontrato'=>$item->idcontrato,'empresa'=>$item->empresa,'periodo_inicial'=>$periodo_inicial,'periodo_final'=>$periodo_final,'nombre'=>'','apellido_paterno'=>'','apellido_materno'=>'','fechatimbre'=>'','nombref'=>'','apellido_paternof'=>'','apellido_maternof'=>'','estatus'=>$estatus,'asignadas'=>$asignadas,'fechaasignada'=>$fechaasignadas,'prefId'=>0,'cargadas'=>0 );
                    }
                }
                
            }
        }else{

        }
        */
        //log_message('error',$getdata);
        
        $json_data = array("data" => $getdata);
        echo json_encode($json_data);
    }
    
    function eliminar_reg(){
        $idreg = $this->input->post('idreg');
        $mot = $this->input->post('mot');
        $this->ModeloCatalogos->updateCatalogo('alta_rentas_prefactura',array('estatus'=>0,'estatus_motivo'=>$mot),array('prefId'=>$idreg));
    }

    function asignar_reg(){
        $idreg = $this->input->post('idreg');
        $check = $this->input->post('check');
        $fecha = $this->input->post('fecha');
        $prio = $this->input->post('prio');
        $comen = $this->input->post('comen');
        $this->ModeloCatalogos->updateCatalogo('alta_rentas_prefactura',array('asignadas'=>$check,'fechaasignada'=>$fecha,'prioridad'=>$prio,'comen'=>$comen),array('prefId'=>$idreg));
    }
    function sumarummes($fecha){
        $messuma = date("Y-m-d",strtotime($fecha.'+1 month')); 
        return $messuma;
    }

    function asignar_reg_t(){
        $check = $this->input->post('check');
        $fecha = $this->input->post('fecha');

        $idcont =   $this->input->post('idcont');
        $fini   =   $this->input->post('fini');
        $ffin   =   $this->input->post('ffin');

        $prio = $this->input->post('prio');
        $comen = $this->input->post('comen');

        $result = $this->ModeloCatalogos->getselectwheren('temp_periodos',array('contrato'=>$idcont,'fechaini'=>$fini));
        if($result->num_rows()==0){
            $this->ModeloCatalogos->Insert('temp_periodos',array('contrato'=>$idcont,'fechaini'=>$fini,'fechafin'=>$ffin,'asignadas'=>$check,'fechaasignada'=>$fecha,'prioridad'=>$prio,'comen'=>$comen));
        }else{
            $result=$result->row();
            $this->ModeloCatalogos->updateCatalogo('temp_periodos',array('asignadas'=>$check,'fechaasignada'=>$fecha,'prioridad'=>$prio,'comen'=>$comen),array('id'=>$result->id));
        }
    }
    function eliminar_reg_t(){
        
        $mot = $this->input->post('mot');
        $idcont =   $this->input->post('idcont');
        $fini   =   $this->input->post('fini');
        $ffin   =   $this->input->post('ffin');

        $result = $this->ModeloCatalogos->getselectwheren('temp_periodos',array('contrato'=>$idcont,'fechaini'=>$fini));
        if($result->num_rows()==0){
            $this->ModeloCatalogos->Insert('temp_periodos',array('contrato'=>$idcont,'fechaini'=>$fini,'fechafin'=>$ffin,'estatus'=>0,'estatus_motivo'=>$mot));
        }else{
            $result=$result->row();
            $this->ModeloCatalogos->updateCatalogo('temp_periodos',array('estatus'=>0,'estatus_motivo'=>$mot),array('id'=>$result->id));
        }

    }
    function confirmardes(){
        $prefId = $this->input->post('prefId');

        $this->ModeloCatalogos->updateCatalogo('alta_rentas_prefactura',array('estatus_confirm'=>1),array('prefId'=>$prefId));
    }
    function confirmardes_t(){

        $idcont =   $this->input->post('con');
        $fini   =   $this->input->post('fini');
        $ffin   =   $this->input->post('ffin');

        $result = $this->ModeloCatalogos->getselectwheren('temp_periodos',array('contrato'=>$idcont,'fechaini'=>$fini));
        if($result->num_rows()==0){
            $this->ModeloCatalogos->Insert('temp_periodos',array('contrato'=>$idcont,'fechaini'=>$fini,'fechafin'=>$ffin,'estatus_confirm'=>1));
        }else{
            $result=$result->row();
            $this->ModeloCatalogos->updateCatalogo('temp_periodos',array('estatus_confirm'=>1),array('id'=>$result->id));
        }
    }
}
?>
