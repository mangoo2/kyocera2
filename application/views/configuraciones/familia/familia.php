<!-- START CONTENT -->
<section id="content" width="100%">
  <!--start container-->
  <div class="container" width="100%">
    <div class="section">
      <h4 class="caption">Formulario de registro de familiares.</h4>
      <div class="row">
        <input id="base_url" type="hidden" name="base_url" val="<?php echo base_url();?>">
        <!-- FORMULARIO -->
        <div class="col s12 m12 24">
          <div id="basic-form" class="section">
            <div class="row">
              <div class="col s12 m12 l6">
                <div class="card-panel">
                  <h4 class="header2">Familias en Cotizaciones</h4>
                  <div class="row">
                    <form class="form" id="form_familia" method="post">
                      <div class="row">
                        <div class="input-field col s10">
                          <i class="material-icons prefix">account_circle</i>
                          <input id="nombre" name="nombre" type="text" >
                          <label for="nombre">Nombre</label>
                        </div>
                        <div class="input-field col s2">
                          <button type="submit" class="btn-floating waves-effect waves-light cyan"><i class="material-icons">add</i></button>
                        </div>
                      </div>
                    </form>
                  </div>
                  <div class="row">
                      <div class="input-field col s12" style="overflow-y: scroll; height: 250px;">  
                        <table id="tabla_familia">
                          <thead>
                            <tr>
                              <th> </th>
                              <th>Nombre</th>
                              <th>Acciones</th>
                              <th> </th>
                            </tr>
                          </thead>
                          <tbody >
                            <?php
                            foreach ($familia as $f) {
                            echo " <tr>
                              <td></td>
                              <td style='display:none'>$f->id</td>
                              <td>$f->nombre</td>
                              <td><a class='btn btn-floating waves-effect waves-light green editar'><i class='material-icons'>edit</i></a></td>
                            </tr>";
                            }
                            ?>
                          </tbody>
                        </table>
                      </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<div id="modalFamilia" class="modal"  >
    <div class="modal-content ">
        <div class="modal-header">
            <h4 class="modal-title">Editar Familiar</h4>
        </div>
        <div class="modal-body">
          <input type="hidden" id="id_familia">
          <div class="row">
              <div class="input-field col s10">
                <i class="material-icons prefix">account_circle</i>
                <input id="name" name="name" type="text" >
              </div>
          </div>
        </div>
        <div class="modal-footer">
          <a class="btn waves-effect waves-light cyan" onclick="editar()">Guardar</a>
          <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
        </div>
    </div>
</div>                