<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Ventasd extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloSerie');
        $this->load->model('ModeloGeneral');//insert generales
        $this->load->model('Cotizaciones_model');
        $this->load->model('Equipos_model');
        $this->load->model('Polizas_model');
        $this->load->model('Clientes_model');
        $this->load->model('Prospectos_model');
        $this->load->model('Consumibles_model');
        $this->load->model('Refacciones_model');
        $this->load->model('ConfiguracionDocumentos_Model');
        $this->load->model('Ventas_model');
        $this->load->model('Ventasd_model');
        $this->load->model('Rentas_model');
        $this->load->model('Personal/ModeloPersonal');
        $this->load->model('Modelodimpresion');
        date_default_timezone_set('America/Mexico_City');
        $this->fechaactual=date('Y-m-d H:i:s');
        if($this->session->userdata('logeado')==true){
            $this->perfilid =$this->session->userdata('perfilid');
            $this->idpersonal = $this->session->userdata('idpersonal');
            /*
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,28);// 20 es el id del submenu
            if ($permiso==0) {
                redirect('Login');
            }*/
        }else{
            redirect('login');
        }
    }

    public function index(){

        /*
        $where = array('personalId' =>$this->idpersonal);   
        $result=$this->ModeloCatalogos->db10_getselectwheren('personal',$where); 
        foreach ($result->result() as $item) { 
           $data['nombre']=$item->nombre; 
           $data['apellido_paterno']=$item->apellido_paterno; 
           $data['apellido_materno']=$item->apellido_materno; 
        } 
        */
        //$where_m = array('activo' =>1);   
        //$data['get_f_formapago']=$this->ModeloCatalogos->db10_getselectwheren('f_formapago',$where_m); 
        $data['fechainicial_reg'] = date("Y-m-d",strtotime($this->fechaactual."- 1 year")); 
        
        $data['perfilid']=$this->perfilid;
        $data['personalrows']=$this->ModeloCatalogos->getselectwheren('personal',array('estatus'=>1));
        $data['idpersonal']=$this->idpersonal;
        $data['lispagosvp']=$this->ModeloGeneral->verificarpagosventas();
        $this->load->view('header');
        $this->load->view('main');
        $this->load->view('ventasd/listado',$data);
        
        $this->load->view('footer'); 
        $this->load->view('ventasd/listado_js');
	}
    public function getListaVentasIncompletasasi() {
        $params = $this->input->post();
        //$params['idpersonal']=$this->idpersonal;
        $params['perfilid']=$this->perfilid;
        
            $productos = $this->Ventasd_model->getlistadosoloventas($params);
            $totalRecords=$this->Ventasd_model->getlistadosoloventast($params); 
          
        
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totalRecords),  
            "recordsFiltered" => intval($totalRecords),
            "data"            => $productos->result(),
            "query"           =>$this->db->last_query()   
        );
        
        echo json_encode($json_data);
    }
    function getlistaservicios(){
        $params = $this->input->post();
        $productos = $this->Ventas_model->getlistaservicios($params);
        $totalRecords=$this->Ventas_model->getlistaserviciost($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totalRecords),  
            "recordsFiltered" => intval($totalRecords),
            "data"            => $productos->result(),
            "query"           =>$this->db->last_query()   
        );
        
        echo json_encode($json_data);
    }
    // Vista para crear Cotización
    function add($idCliente)    {
            $this->ModeloCatalogos->updateCatalogo('consumibles_bodegas',array('total'=>0),array('total <'=>0));
            $this->ModeloCatalogos->updateCatalogo('series_accesorios',array('cantidad'=>0),array('cantidad <'=>0));
            $data['datoscontacto']=$this->ModeloCatalogos->db10_getselectwheren('cliente_datoscontacto',array('clienteId'=>$idCliente,'activo'=>1));
            $data['idCliente']          = $idCliente;
            $data['equipos']            = $this->Equipos_model->getEquiposParaSelect();  
            //$data['polizas']            = $this->Polizas_model->getListadoPolizas();      
            //$data['familia']            = $this->Clientes_model->getDataFamilia();
            $data['equiposimg']         = $this->Modelodimpresion->getDataEquipo2();
            $datosCliente               = $this->Clientes_model->getClientePorId($idCliente);
            $data['nombreCliente']      = $datosCliente->empresa;
            $data['horario_disponible'] = $datosCliente->horario_disponible;
            $data['equipo_acceso']      = $datosCliente->equipo_acceso;
            $data['documentacion_acceso'] = $datosCliente->documentacion_acceso;
            $data['condicion']          = $datosCliente->condicion;
            $data['emailCliente']       = '';
            $data['atencionCliente']    = $datosCliente->puesto_contacto;
            $data['atencionpara']       = '';
            $data['aaa']                = $datosCliente->aaa;
            $data['bloqueo']            = $datosCliente->bloqueo;
            $where  = array('idCliente' => $idCliente,'status'=>1);
            $datosClientet = $this->ModeloCatalogos->db10_getselectwheren('cliente_has_telefono',$where);
            $telefono='';
            foreach ($datosClientet->result() as $item) {
                $telefono=$item->tel_local;
            }
            $data['telefonoCliente'] = $telefono;
            //$data['restbodegas']=$this->ModeloCatalogos->db10_getselectwheren('bodegas',array('activo' => 1));
            $data['restbodegas']=$this->ModeloCatalogos->view_session_bodegas();
            $data['perfilid']=$this->perfilid;
            $data['idpersonal']=$this->idpersonal;
            $data['resultpolizas']=$this->ModeloCatalogos->db10_getselectwheren('polizas',array('estatus'=>1));
            $this->load->view('header');
            $this->load->view('main',$data);
            $this->load->view('ventasd/form_ventasd',$data);
            $this->load->view('ventasd/form_ventasd_js');
            $this->load->view('footerck');         
    }
    // Función para registrar una nueva ventas

    function nuevaventa(){
        $data = $this->input->post();
        
        // Saber que se va a cotizar
        $eleccionCotizar = $data['eleccionCotizar'];
        unset($data['eleccionCotizar']);
        
        if(isset($data['accesorios'])){
            $accesorios = $data['accesorios'];
            unset($data['accesorios']);
        }

        if(isset($data['consumibles'])){
            $consumibles = $data['consumibles'];
            unset($data['consumibles']);
        }

        // Según sea el tipo de Cotización que se va a elegir, se procesará de diferente manera
        switch ($eleccionCotizar){   
            // Equipos
            case 1:
                
            break;
            // Consumibles
            case 2:
                
            break;
            // Refacciones
            case 3:
                $equiposSeleccionados = $data['arrayEquipos'];
                unset($data['arrayEquipos']);

                $arrayRefaccion = $data['arrayRefaccion'];
                unset($data['arrayRefaccion']);

                $piezasSeleccionadas = $data['arrayPiezas'];
                unset($data['arrayPiezas']);


                $equiposArreglo = explode(',', $equiposSeleccionados);
                $refaccionesArreglo = explode(',', $arrayRefaccion);
                $piezasArreglo = explode(',', $piezasSeleccionadas);
                
                $datosCotizacion = array('atencion' => $data['atencion'],
                                            'correo' => $data['correo'],
                                            'telefono' => $data['telefono'],
                                            'idCliente' => $data['idCliente'],
                                            'tipo' => $eleccionCotizar,
                                            //'rentaVentaPoliza' => 1,
                                            'estatus' => 1,
                                            'reg'=>$this->fechaactual
                                            );
                
                $idCotizacion = $this->ModeloGeneral->getinsert($datosCotizacion,'ventas');
                //$idCotizacion = $this->Cotizaciones_model->insertarCotizacion($datosCotizacion);
                /*
                for ($i=0; $i < count($refaccionesArreglo); $i++){

                    $datosRefaccion = $this->Refacciones_model->getRefaccionConPrecios($refaccionesArreglo[$i]);
                    
                    $detallesCotizacion = array(
                                            'idCotizacion' => $idCotizacion,
                                            'idEquipo' => $equiposArreglo[$i],
                                            'piezas' => $piezasArreglo[$i], 
                                            'modelo' => $datosRefaccion[0]->nombre,
                                            'descripcion' => $datosRefaccion[0]->observaciones,
                                            'precioGeneral' => $datosRefaccion[0]->general,
                                            'totalGeneral' => ($piezasArreglo[$i]*$datosRefaccion[0]->general),
                                            'idRefaccion' => $refaccionesArreglo[$i]
                                        );
                    $this->ModeloGeneral->getinsert($detallesCotizacion,'cotizaciones_has_detallesRefacciones');                   
                    //$this->Cotizaciones_model->insertarDetallesCotizacionRefacciones($detallesCotizacion);
                }*/
                echo $idCotizacion;
            break;
            // Pólizas
            case 4:
                
            break;
            
            default:
                
            break;
        }
    }
    function detallesventas(){
        $id = $this->input->post('id');
        $datos = $this->Ventas_model->detallesventas($id);
        $datosconsu = $this->Ventas_model->detallesventasconsumibles($id);
        $datosaccesorio = $this->Ventas_model->detallesaccesoriosv($id);
        $html='
            <table class="responsive-table display tabledetallesventas" cellspacing="0">
              <thead>
                <tr>
                  <th>Producto</th>
                  <th>Num. Serie</th>
                </tr>
              </thead>
              <tbody>';
                   foreach ($datos as $item) {
                    $html.='<tr>';
                    $html.='<td>'.$item->modelo.'</td>';
                    $html.='<td>'.$item->serie.'</td>';
                    $html.='</tr>';
                   }
              $html.='</tbody>
                   </table>';
        
        if($datosconsu->num_rows()>=1){
            $html.='
            <table class="responsive-table display tabledetallesventas" cellspacing="0">
              <thead>
                <tr>
                  <th>Consumible</th>
                  <th>Toner</th>
                </tr>
              </thead>
              <tbody>';
                   foreach ($datosconsu->result() as $item) {
                    $htmlc='';
                    $html.='<tr>';
                    $html.='<td>'.$item->modelo.'</td>';
                        if ($item->tblack==1) {
                            $htmlc='Toner Black:'.$item->costo_toner_black;
                        }else{
                            $cmy=$item->costo_toner_cmy/3;
                            $cmy =round($cmy, 2);
                            if ($item->tc=1) {
                               $htmlc.='<p>Toner C:'.$cmy.'</p>';
                            }
                            if ($item->tm=1) {
                                $htmlc.='<p>Toner M:'.$cmy.'</p>';
                            }
                            if ($item->ty=1) {
                               $htmlc.='<p>Toner Y:'.$cmy.'</p>';
                            }
                        }



                    $html.='<td>'.$htmlc.'</td>';
                    $html.='</tr>';
                   }
              $html.='</tbody>
                   </table>';
        }
        if($datosaccesorio->num_rows()>=1){
            $html.='
            <table class="responsive-table display tabledetallesventas" cellspacing="0">
              <thead>
                <tr>
                  <th>Accesoio</th>
                  <th>Serie</th>
                </tr>
              </thead>
              <tbody>';
                   foreach ($datosaccesorio->result() as $item) {
                    $html.='<tr>';
                    $html.='<td>'.$item->nombre.'</td>';
                    $html.='<td>'.$item->serie.'</td>';
                    $html.='</tr>';
                   }
              $html.='</tbody>
                   </table>';
        }
        echo $html;
    }
    function finalizar_factura(){
        $id = $this->input->post('id');
        $data = array('estatus' => 3);
        $resul = $this->Ventas_model->updateCatalogo($data,'id',$id,'ventas');
        echo $resul;
    }
    function finalizar_facturac(){
        $id = $this->input->post('id');
        $data = array('estatus' => 3);
        $resul = $this->Ventas_model->updateCatalogo($data,'combinadaId',$id,'ventacombinada');
        echo $resul;
    }
    function nuevaCotizacionpoliza(){
        $data = $this->input->post();
        $datosCotizacion = array('atencion' => $data['atencion'],
                                            'correo' => $data['correo'],
                                            'telefono' => $data['telefono'],
                                            'idCliente' => $data['idCliente'],
                                            'tipo' => 4,
                                            //'rentaVentaPoliza' => 3,
                                            'id_personal' => $this->idpersonal,
                                            'estatus' => 1,
                                        'combinada' => $data['combinada']
                                            );

        $idCotizacion = $this->ModeloCatalogos->Insert('polizasCreadas',$datosCotizacion);
        $arrayPolizas = $data['arrayPolizas'];
        $dataeqarrayinsert=array();
        unset($data['arrayPolizas']);
        $DATAc = json_decode($arrayPolizas);
        for ($i=0;$i<count($DATAc);$i++) {
            $datosPoliza = $this->Polizas_model->getPolizaConUnDetalle($DATAc[$i]->poliza,$DATAc[$i]->polizad);
            $precio_local=null;
            $precio_semi=null;
            $precio_foraneo=null;
            $precio_especial=null;
            //log_message('error', 'costo poliza: '.$DATAc[$i]->costo);
            if ($DATAc[$i]->costo=='local') {
                        $precio_local=$datosPoliza[0]->precio_local;
                        $precio_semi=null;
                        $precio_foraneo=null;
                        $precio_especial=null;
            }elseif ($DATAc[$i]->costo=='foraneo') {
            //}elseif ($polizasArregloDetallescosto[$i]=='foraneo') {
                        $precio_local=null;
                        $precio_semi=null;
                        $precio_especial=null;
                        $precio_foraneo=$datosPoliza[0]->precio_foraneo;
            }elseif($DATAc[$i]->costo=='semi'){
                        $precio_local=null;
                        $precio_semi=$datosPoliza[0]->precio_semi;
                        $precio_foraneo=null;
                        $precio_especial=null;
            }elseif($DATAc[$i]->costo=='especial'){
                        $precio_local=null;
                        $precio_semi=null;
                        $precio_foraneo=null;
                        $precio_especial=$datosPoliza[0]->precio_especial;
            }

            $detallesCotizacion = array('idPoliza' => $idCotizacion,
                                            'idEquipo' => $DATAc[$i]->equipo,
                                            'serie'=> $DATAc[$i]->serie,
                                            'idCotizacion' => 0,
                                            'nombre' => $datosPoliza[0]->nombre, 
                                            'cubre' => $datosPoliza[0]->cubre,
                                            'vigencia_meses' => $datosPoliza[0]->vigencia_meses,
                                            'vigencia_clicks' => $datosPoliza[0]->vigencia_clicks,
                                            'modelo' => $datosPoliza[0]->modelo,
                                            'precio_local' => $precio_local,
                                            'precio_semi' => $precio_semi,
                                            'precio_foraneo' => $precio_foraneo,
                                            'precio_especial' => $precio_especial,
                                            'comentario' => $DATAc[$i]->motivo,

                                        );
            $dataeqarrayinsert[]=$detallesCotizacion;
            //$this->Cotizaciones_model->insertarDetallesCotizacionPoliza($detallesCotizacion);
            //$this->ModeloCatalogos->Insert('polizasCreadas_has_detallesPoliza',$detallesCotizacion);
        }
        if(count($DATAc)>0){
            $this->ModeloCatalogos->insert_batch('polizasCreadas_has_detallesPoliza',$dataeqarrayinsert);
        }
        echo $idCotizacion;
    }
    function editarpoliza(){
        $idpoliza = $this->input->post('idpoliza');
        $polizacol = $this->input->post('polizacol');
        $pass = $this->input->post('pass');
        $valor = $this->input->post('valor');
        $permiso=0;
        $permisos=$this->Login_model->permisoadmin();
        foreach ($permisos as $item) {
            
            $verificar = password_verify($pass,$item->contrasena);
            if ($verificar) {
                $permiso=1;
            }
        }
        if ($permiso==1) {
            $data[$polizacol]=$valor;
            $where = array('id' => $idpoliza );
            $this->ModeloCatalogos->updateCatalogo('polizasCreadas_has_detallesPoliza',$data,$where);
        }

        echo $permiso;
    }
    function viewventa(){
        $idventa = $this->input->post('idventa');
        $where_vde=array('idVenta'=>$idventa);
        $result_vde=$this->ModeloCatalogos->db10_getselectwheren('ventas_has_detallesEquipos',$where_vde);
        $result_vdacc=$this->ModeloCatalogos->ventasaccesorios($idventa);
        $result_vdcon=$this->ModeloCatalogos->ventasconsumibles($idventa);
        //$resultbodegas=$this->ModeloCatalogos->db10_getselectwheren('bodegas',array('activo' => 1));
        $resultbodegas=$this->ModeloCatalogos->view_session_bodegas();
        $html='<table class="highlight" id="venta_equipos">
                      <thead>
                        <tr>
                          <th>Cantidad</th>
                          <th>Equipo</th>
                          <th>Bodega</th>
                        </tr>
                      </thead>
                      <tbody>
                        
                      ';
        foreach ($result_vde->result() as $item) {
            $stockequipo=$this->ModeloGeneral->stockequipo($item->idEquipo);
            $html.='<tr>
                          <td>
                            <input type="hidden" name="id" id="id" value="'.$item->id.'">
                            <input type="number" name="cantidad" id="cantidad" value="'.$item->cantidad.'" min="1" class="validstockequedit_'.$item->id.'"oninput="validstockequedit($(this),'.$stockequipo.')">
                          </td>
                          <td>
                            '.$item->modelo.'
                          </td>
                          <td>
                            <select id="serie_bodega" name="serie_bodega" class="browser-default chosen-select form-control-bmz" >';
                              foreach ($resultbodegas as $itemb) {
                                  $html.='<option value="'.$itemb->bodegaId.'">'.$itemb->bodega.'</option>';
                               }  
                            $html.='</select>  
                            
                          </td>
                        </tr>';

        }
        $html.='</tbody>
            </table>';
        $html.='<table class="highlight" id="venta_accesorios">
                      <thead>
                        <tr>
                          <th>Cantidad</th>
                          <th>Accesorio</th>
                          <th>Bodega</th>
                        </tr>
                      </thead>
                      <tbody>';
        foreach ($result_vdacc->result() as $item) {
                $html.='<tr>
                          <td>
                            <input type="hidden" name="id_accesorio" id="id_accesorio" value="'.$item->id_accesorio.'">
                            <input type="number" name="cantidad" id="cantidad" value="'.$item->cantidad.'" min="1">
                          </td>
                          <td>
                            '.$item->nombre.'
                          </td>
                          <td>
                            <select id="serie_bodega" name="serie_bodega" class="browser-default chosen-select form-control-bmz" >';
                              foreach ($resultbodegas as $itemb) {
                                  $html.='<option value="'.$itemb->bodegaId.'">'.$itemb->bodega.'</option>';
                               }  
                            $html.='</select> 
                          </td>
                        </tr>';
        }
        $html.='</tbody>
            </table>';
        $html.='<table class="highlight" id="venta_consumibles">
                      <thead>
                        <tr>
                          <th>Cantidad</th>
                          <th>Consumible</th>
                          <th>Bodega</th>
                        </tr>
                      </thead>
                      <tbody>';
        foreach ($result_vdcon->result() as $item) {
            

            $html.='<tr>
                          <td>
                            <input type="hidden" name="id_accesorio" id="id_accesorio" value="'.$item->id.'">
                            <input type="hidden" name="id_consu" id="id_consu" value="'.$item->id_consumibles.'">
                            <input type="number" name="cantidad" id="cantidad" value="'.$item->cantidad.'" min="1">
                          </td>
                          <td>
                            '.$item->modelo.'
                          </td>
                          <td>
                            <select id="serie_bodega" name="serie_bodega" class="browser-default chosen-select form-control-bmz" >';
                              foreach ($resultbodegas as $itemb) {
                                  $html.='<option value="'.$itemb->bodegaId.'">'.$itemb->bodega.'</option>';
                               }  
                            $html.='</select> 
                          </td>
                          

                        </tr>';
        }
        $html.='</tbody>
            </table>';
        $html.='<div class="row">
                    <div class="col s12 m12 12">
                        <input type="hidden" id="ventaservicioc">
                        <div class="switch">
                            <label style="font-size: 20px;">
                                ¿Desea incluir instalación y/o servicio o póliza de cortesia?
                                <input type="checkbox" id="aceptareservicio" value="'.$idventa.'" onchange="aceptareservicio()">
                                <span class="lever"></span>
                            </label>
                        </div>
                    </div>                
                </div>';
        echo $html;
    }
    function editarventacs(){
        $data = $this->input->post();
        $arrayequipos = $data['arrayequipos'];
        $arrayaccesorios = $data['arrayaccesorios'];
        $arrayconsumibles = $data['arrayconsumibles'];
        $servicio = $data['servicio'];

        $polizae = $data['polizae'];
        $servicioe = $data['servicioe'];
        $direccione = $data['direccione'];
        $tipoe = $data['tipoe'];
        $motivoe = $data['motivoe'];
        $fechae = $data['fechae'];
        $sidventa = $data['sidventa'];

        $DATAe = json_decode($arrayequipos);
        for ($i=0;$i<count($DATAe);$i++) {
            $datae['cantidad']=$DATAe[$i]->cantidad;
            $datae['serie_bodega']=$DATAe[$i]->serie;
            $this->ModeloCatalogos->updateCatalogo('ventas_has_detallesEquipos',$datae,array('id'=>$DATAe[$i]->id));
        }
        $DATAa = json_decode($arrayaccesorios);
        for ($i=0;$i<count($DATAa);$i++) {
            $dataa['cantidad']=$DATAa[$i]->cantidad;
            $dataa['serie_bodega']=$DATAa[$i]->serie;
            $this->ModeloCatalogos->updateCatalogo('ventas_has_detallesEquipos_accesorios',$dataa,array('id_accesorio'=>$DATAa[$i]->id_accesorio));
        }
        $DATAc = json_decode($arrayconsumibles);
        for ($i=0;$i<count($DATAc);$i++) {
            $cantidad=$DATAc[$i]->cantidad;
            $datacc['cantidad']=$cantidad;
            $bodega=$DATAc[$i]->bodega;
            $datacc['bodega']=$bodega;
            $id_consu=$DATAc[$i]->id_consu;
            $this->ModeloCatalogos->updateCatalogo('ventas_has_detallesequipos_consumible',$datacc,array('id'=>$DATAc[$i]->id));
            $this->ModeloCatalogos->updatestock2('consumibles_bodegas','total','-',$cantidad,'consumiblesId',$id_consu,'bodegaId',$bodega);
        }
        if ($servicio>0) {
            $idventa=intval($servicio);
            $this->ModeloCatalogos->updateCatalogo('ventas',array('servicio'=>1,'servicio_poliza'=>$polizae,'servicio_servicio'=>$servicioe,'servicio_direccion'=>$direccione,'servicio_tipo'=>$tipoe,'servicio_motivo'=>$motivoe),array('id'=>$idventa));
            $this->ModeloCatalogos->updateCatalogo('ventacombinada',array('servicio'=>1),array('equipo'=>$idventa));
            $this->ModeloCatalogos->updateCatalogo('ventacombinada',array('servicio'=>1),array('consumibles'=>$idventa));
            $this->ModeloCatalogos->updateCatalogo('ventacombinada',array('servicio'=>1),array('refacciones'=>$idventa));
            $this->ModeloCatalogos->updateCatalogo('ventacombinada',array('servicio'=>1),array('poliza'=>$idventa));
        }else{
            $idventa=intval($sidventa);
            $this->ModeloCatalogos->updateCatalogo('ventas',array('fechaentrega'=>$fechae),array('id'=>$idventa));
            $this->ModeloCatalogos->updateCatalogo('ventacombinada',array('fechaentrega'=>$fechae),array('equipo'=>$idventa,'fechaentrega'=>NULL));
            $this->ModeloCatalogos->updateCatalogo('ventacombinada',array('fechaentrega'=>$fechae),array('consumibles'=>$idventa,'fechaentrega'=>NULL));
            $this->ModeloCatalogos->updateCatalogo('ventacombinada',array('fechaentrega'=>$fechae),array('refacciones'=>$idventa,'fechaentrega'=>NULL));
            $this->ModeloCatalogos->updateCatalogo('ventacombinada',array('fechaentrega'=>$fechae),array('poliza'=>$idventa,'fechaentrega'=>NULL));
        }
    }
    function editarventacsrenta(){
        $data = $this->input->post();
        $arrayequipos = $data['arrayequipos'];
        $arrayaccesorios = $data['arrayaccesorios'];
        $arrayconsumibles = $data['arrayconsumibles'];
        $DATAe = json_decode($arrayequipos);
        for ($i=0;$i<count($DATAe);$i++) {
            $datae['cantidad']=$DATAe[$i]->cantidad;
            $datae['serie_bodega']=$DATAe[$i]->serie;
            $this->ModeloCatalogos->updateCatalogo('rentas_has_detallesEquipos',$datae,array('id'=>$DATAe[$i]->id));
           
            $data_rentas = $this->ModeloCatalogos->getselectwherenall('rentas_has_detallesEquipos',array('id'=>$DATAe[$i]->id));
            foreach ($data_rentas as $item) {
                $this->ModeloCatalogos->updateCatalogo('rentas_has_detallesEquipos_accesorios',$datae,array('idrentas'=> $item->idRenta,'id_equipo'=>$item->idEquipo));
            }
        }
        $DATAa = json_decode($arrayaccesorios);
        for ($i=0;$i<count($DATAa);$i++) {
            $dataa['cantidad']=$DATAa[$i]->cantidad;
            $dataa['serie_bodega']=$DATAa[$i]->serie;
            $this->ModeloCatalogos->updateCatalogo('rentas_has_detallesEquipos_accesorios',$dataa,array('id_accesorio'=>$DATAa[$i]->id_accesorio));
        }
        $DATAc = json_decode($arrayconsumibles);
        for ($i=0;$i<count($DATAc);$i++) {
            $datacc['cantidad']=$DATAc[$i]->cantidad;
            $bodega=$DATAc[$i]->bodega;
            $datacc['bodega']=$bodega;
            $this->ModeloCatalogos->updateCatalogo('rentas_has_detallesequipos_consumible',$datacc,array('id'=>$DATAc[$i]->id));
        }

    }
   
    function nuevaventaequipos(){
        $data = $this->input->post();
        $eleccionCotizar = 1;
        $tpeq=$data['tipoprecioequipo'];
        unset($data['tipoprecioequipo']);
        unset($data['eleccionCotizar']);
        if(isset($data['accesorios'])){
            $accesorios = $data['accesorios'];
            unset($data['accesorios']);
        }
        
        if(isset($data['consumibles'])){
            $consumibles = $data['consumibles'];
            unset($data['consumibles']);
        }
        //===============================================
        $tipoCotizacion = $data["tipo"];
                unset($data['tipo']);
                //$data['rentaVentaPoliza'] = $tipoCotizacion;
                // Saber si es cotización MANUAL o por FAMILIA
                $tipoCotizacionEquipos = $data["cotizar"];
                unset($data['cotizar']);
                // Agisnamos lo que se va a cotizar
                $data['tipo'] = $eleccionCotizar;
                // Se inserta el registro principal
                if ($tipoCotizacion==1) {
                    $datosCotizacion = array('atencion' => $data['atencion'],
                                        'correo' => $data['correo'],
                                        'telefono' => $data['telefono'],
                                        'idCliente' => $data['idCliente'],
                                        'tipo' => $data['tipo'],
                                        //'rentaVentaPoliza' => $tipoCotizacion,
                                        'id_personal'=>$this->idpersonal,
                                        'estatus' => 1,
                                        'reg'=>$this->fechaactual,
                                        'combinada' => $data['combinada']);
                    // Se inserta la cotización principal
                    $idCotizacion = $this->ModeloCatalogos->Insert('ventas',$datosCotizacion);
                }elseif ($tipoCotizacion==2) {
                    $datosCotizacion = array('atencion' => $data['atencion'],
                                        'correo' => $data['correo'],
                                        'telefono' => $data['telefono'],
                                        'idCliente' => $data['idCliente'],
                                        //'rentaVentaPoliza' => $tipoCotizacion,
                                        'id_personal'=>$this->idpersonal,
                                        'estatus' => 1,
                                        'reg'=>$this->fechaactual,
                                        'combinada' => $data['combinada']);
                    // Se inserta la cotización principal
                    $idCotizacion = $this->ModeloCatalogos->Insert('rentas',$datosCotizacion);
                    //log_message('error', 'idrentas a: '.$idCotizacion);
                }
                
                // Si trae el tipo de cotización de equipos con valor 1, es MANUAL
                //if($tipoCotizacionEquipos==1){
                    //$arregloEquipos = explode(',',$data["equiposIndividuales"]);
                    $equiposIndividuales=$data["equiposIndividuales"];
                    $DATAeq = json_decode($equiposIndividuales);
                    //foreach ($arregloEquipos as $equipoInd) {
                    $dataarrayinsert_eq_r=array();
                    for ($j=0;$j<count($DATAeq);$j++){
                        $equipoInd=$DATAeq[$j]->equipos;
                        $cantidadeq=$DATAeq[$j]->cantidad;
                        $equipo = $this->Equipos_model->getEquipoPorId($equipoInd);
                        // Dependiendo si es VENTA, RENTA o PÓLIZA, es el precio que se va a tomar
                        if($tipoCotizacion==1){
                            if ($tpeq==1) {
                                $precio = $equipo->costo_venta;
                            }elseif ($tpeq==2) {
                                $precio = $equipo->costo_poliza;
                            }else{
                                $precio = $equipo->costo_renta;
                            }
                            
                        }
                        else if ($tipoCotizacion==2){
                            $precio = $equipo->costo_renta;
                        }
                        else if ($tipoCotizacion==3){
                            $precio = $equipo->costo_poliza;
                        }
                        if ($tpeq==3) {
                                $precio = 0;
                        }
                        //========================================
                            //$costo_toner_black = $equipo->costo_toner_black;
                            //$costo_toner_cmy = $equipo->costo_toner_cmy;
                            //$rendimiento_toner_black = $equipo->rendimiento_toner_black;
                            //$rendimiento_toner_cmy = $equipo->rendimiento_toner_cmy;
                            //$costo_unidad_imagen = $equipo->costo_unidad_imagen;
                            //$rendimiento_unidad_imagen = $equipo->rendimiento_unidad_imagen;
                            //$costo_garantia_servicio = $equipo->costo_garantia_servicio;
                            //$rendimiento_garantia_servicio = $equipo->rendimiento_garantia_servicio;
                            //$costo_pagina_monocromatica = $equipo->costo_pagina_monocromatica;
                            //$costo_pagina_color = $equipo->costo_pagina_color;
                        //========================================
                            $tipo = 1;
                            $costo_toner = 0;
                            $rendimiento_toner = 0;
                            $costo_unidad_imagen = 0;
                            $rendimiento_unidad_imagen = 0;
                            $costo_garantia_servicio = 0;
                            $rendimiento_garantia_servicio = 0;
                            $costo_pagina_monocromatica = 0;
                            $costo_pagina_color = 0;
                        //========================================
                            $DATAc = json_decode($consumibles);
                            for ($i=0;$i<count($DATAc);$i++) {
                                if ($equipo->id==$DATAc[$i]->equiposelected) {
                                    $idconsumuble=$DATAc[$i]->consumibleselected;
                                    //log_message('error', 'idconsumible '.$idconsumuble);
                                    $resultadoaddcon=$this->ModeloCatalogos->consumibleagregadoselected($idconsumuble,$equipo->id);
                                    foreach ($resultadoaddcon->result() as $item2) {
                                        $tipo = $item2->tipo;
                                        $costo_toner = $item2->poliza;
                                        $rendimiento_toner = $item2->rendimiento_toner;
                                        $costo_unidad_imagen = $item2->costo_unidad_imagen;
                                        $rendimiento_unidad_imagen = $item2->rendimiento_unidad_imagen;
                                        $costo_garantia_servicio = $item2->costo_garantia_servicio;
                                        $rendimiento_garantia_servicio = $item2->rendimiento_garantia_servicio;
                                        $costo_pagina_monocromatica = $item2->costo_pagina_monocromatica;
                                        $costo_pagina_color = $item2->costo_pagina_color;


                                    }
                                    $datachfc["idventa"]=$idCotizacion;
                                        $datachfc["idequipo"]=$DATAc[$i]->equiposelected;
                                        $datachfc["cantidad"]=$cantidadeq;
                                        $datachfc["id_consumibles"]=$idconsumuble;
                                        $datachfc["costo_toner"]=$costo_toner;
                                        $datachfc["rendimiento_toner"]=$rendimiento_toner;
                                        $datachfc["rendimiento_unidad_imagen"]=$rendimiento_unidad_imagen;
                                        if ($tipoCotizacion==1) {
                                            $this->ModeloCatalogos->Insert('ventas_has_detallesequipos_consumible',$datachfc);
                                            
                                        }elseif ($tipoCotizacion==2) {
                                            unset($datachfc["idventa"]);
                                            $datachfc["idrentas"]=$idCotizacion;
                                            $this->ModeloCatalogos->Insert('rentas_has_detallesequipos_consumible',$datachfc);

                                        }
                                }
                            }
                        //=========================================================
                            $DATAa = json_decode($accesorios);
                            for ($l=0;$l<count($DATAa);$l++) {
                                if ($equipo->id==$DATAa[$l]->equiposelected) {
                                    if ($tipoCotizacion==1) {
                                        $dataas['id_venta']=$idCotizacion;
                                        $dataas['id_equipo']=$DATAa[$l]->equiposelected;
                                        $dataas['id_accesorio']=$DATAa[$l]->accesorioselected;
                                        $dataas['cantidad']=$cantidadeq;
                                        $dataas['costo']=$this->ModeloCatalogos->costoaccesorio($DATAa[$l]->accesorioselected);
                                        $this->ModeloCatalogos->Insert('ventas_has_detallesEquipos_accesorios',$dataas);
                                    }elseif ($tipoCotizacion==2) {
                                        //log_message('error', 'idrentas Accesorio: '.$idCotizacion);
                                        $dataas["idrentas"]=$idCotizacion;
                                        $dataas['id_equipo']=$DATAa[$l]->equiposelected;
                                        $dataas['id_accesorio']=$DATAa[$l]->accesorioselected;
                                        $dataas['cantidad']=$cantidadeq;
                                        $this->ModeloCatalogos->Insert('rentas_has_detallesEquipos_accesorios',$dataas);
                                    }
                                }
                            }   
                        // Se arma el array para insertar los datos
                        if ($tipoCotizacion==1) {
                            $detallesCotizacion = array('idVenta' => $idCotizacion,
                                                        'idEquipo' => $equipo->id,
                                                        'cantidad'=>$cantidadeq,
                                                        'modelo' => $equipo->modelo,
                                                        'especificaciones_tecnicas' => $equipo->especificaciones_tecnicas,
                                                        'precio' => $precio,
                                                        'costo_unidad_imagen' => $costo_unidad_imagen,
                                                        'rendimiento_unidad_imagen' => $rendimiento_unidad_imagen,
                                                        'costo_garantia_servicio' => $costo_garantia_servicio,
                                                        'rendimiento_garantia_servicio' => $rendimiento_garantia_servicio,
                                                        'costo_pagina_monocromatica' => $costo_pagina_monocromatica,
                                                        'costo_pagina_color' => $costo_pagina_color,
                                                        'excedente' => $equipo->excedente,
                                                        'tipotoner' => $tipo,
                                                        'costo_toner' => $costo_toner,
                                                        'rendimiento_toner' => $rendimiento_toner,           
                                            );
                            // Se insertan en la BD
                            $this->ModeloCatalogos->Insert('ventas_has_detallesEquipos',$detallesCotizacion);
                        }elseif ($tipoCotizacion==2) {
                            $detallesCotizacion = array('idRenta' => $idCotizacion,
                                                        'idEquipo' => $equipo->id,
                                                        'cantidad'=>$cantidadeq,
                                                        'modelo' => $equipo->modelo,
                                                        'especificaciones_tecnicas' => $equipo->especificaciones_tecnicas,
                                                        'precio' => $precio,
                                                        'costo_unidad_imagen' => $costo_unidad_imagen,
                                                        'rendimiento_unidad_imagen' => $rendimiento_unidad_imagen,
                                                        'costo_garantia_servicio' => $costo_garantia_servicio,
                                                        'rendimiento_garantia_servicio' => $rendimiento_garantia_servicio,
                                                        'costo_pagina_monocromatica' => $costo_pagina_monocromatica,
                                                        'costo_pagina_color' => $costo_pagina_color,
                                                        'excedente' => $equipo->excedente,
                                                        'tipotoner' => $tipo,
                                                        'costo_toner' => $costo_toner,
                                                        'rendimiento_toner' => $rendimiento_toner,           
                                            );
                            // Se insertan en la BD
                            $dataarrayinsert_eq_r[]=$detallesCotizacion;
                            //$this->ModeloCatalogos->Insert('rentas_has_detallesEquipos',$detallesCotizacion);
                        }
                        //$this->Cotizaciones_model->insertarDetallesCotizacionEquipos($detallesCotizacion);
                        
                    }
                    if(count($dataarrayinsert_eq_r)>0){
                        $this->ModeloCatalogos->insert_batch('rentas_has_detallesEquipos',$dataarrayinsert_eq_r);
                    }
                //}
                 /*
                $DATAa = json_decode($accesorios);
               // log_message('error', 'accesorios:acc'.$accesorios);
                //log_message('error', 'accesorios: data '.$DATAa);
                for ($l=0;$l<count($DATAa);$l++) {
                    
                    
                    if ($tipoCotizacion==1) {
                        $dataas['id_venta']=$idCotizacion;
                        $dataas['id_equipo']=$DATAa[$l]->equiposelected;
                        $dataas['id_accesorio']=$DATAa[$l]->accesorioselected;
                        $this->ModeloCatalogos->Insert('ventas_has_detallesEquipos_accesorios',$dataas);
                    }elseif ($tipoCotizacion==2) {
                        log_message('error', 'idrentas Accesorio: '.$idCotizacion);
                        $dataas["idrentas"]=$idCotizacion;
                        $dataas['id_equipo']=$DATAa[$l]->equiposelected;
                        $dataas['id_accesorio']=$DATAa[$l]->accesorioselected;
                        $this->ModeloCatalogos->Insert('rentas_has_detallesEquipos_accesorios',$dataas);
                    }
                    //log_message('error', 'Equipo: '.$equiposelected.' accesorio: '.$accesorioselected);
                }   
                */
                // Se regresa el ID de la cotización
                echo $idCotizacion;
                //==================================================================
    }
    function nuevaventaequiposr(){
        $data = $this->input->post();
        $eleccionCotizar = 1;
        $tpeq=$data['tipoprecioequipo'];
        unset($data['tipoprecioequipo']);
        unset($data['eleccionCotizar']);
        if(isset($data['accesorios'])){
            $accesorios = $data['accesorios'];
            unset($data['accesorios']);
        }
        
        if(isset($data['consumibles'])){
            $consumibles = $data['consumibles'];
            unset($data['consumibles']);
        }
        //===============================================
        $tipoCotizacion = $data["tipo"];
                unset($data['tipo']);
                //$data['rentaVentaPoliza'] = $tipoCotizacion;
                // Saber si es cotización MANUAL o por FAMILIA
                $tipoCotizacionEquipos = $data["cotizar"];
                unset($data['cotizar']);
                // Agisnamos lo que se va a cotizar
                $data['tipo'] = $eleccionCotizar;
                // Se inserta el registro principal
                if ($tipoCotizacion==1) {
                    /*
                    $datosCotizacion = array('atencion' => $data['atencion'],
                                        'correo' => $data['correo'],
                                        'telefono' => $data['telefono'],
                                        'idCliente' => $data['idCliente'],
                                        'tipo' => $data['tipo'],
                                        //'rentaVentaPoliza' => $tipoCotizacion,
                                        'id_personal'=>$this->idpersonal,
                                        'estatus' => 1,
                                        'reg'=>$this->fechaactual,
                                        'combinada' => $data['combinada']);
                    // Se inserta la cotización principal
                    $idCotizacion = $this->ModeloCatalogos->Insert('ventas',$datosCotizacion);
                    */
                }elseif ($tipoCotizacion==2) {
                    $datosCotizacion = array('atencion' => $data['atencion'],
                                        'correo' => $data['correo'],
                                        'telefono' => $data['telefono'],
                                        'idCliente' => $data['idCliente'],
                                        //'rentaVentaPoliza' => $tipoCotizacion,
                                        'id_personal'=>$this->idpersonal,
                                        'estatus' => 1,
                                        'reg'=>$this->fechaactual,
                                        'combinada' => $data['combinada']);
                    // Se inserta la cotización principal
                    $idCotizacion = $this->ModeloCatalogos->Insert('rentas',$datosCotizacion);
                    //log_message('error', 'idrentas a: '.$idCotizacion);
                }
                
                // Si trae el tipo de cotización de equipos con valor 1, es MANUAL
                //if($tipoCotizacionEquipos==1){
                    //$arregloEquipos = explode(',',$data["equiposIndividuales"]);
                    $equiposIndividuales=$data["equiposIndividuales"];
                    $DATAeq = json_decode($equiposIndividuales);
                    //foreach ($arregloEquipos as $equipoInd) {
                    $dataarrayinsert_eq_r=array();
                    for ($j=0;$j<count($DATAeq);$j++){
                        $equipoInd=$DATAeq[$j]->equipos;
                        $cantidadeq=$DATAeq[$j]->cantidad;
                        $equipo = $this->Equipos_model->getEquipoPorId($equipoInd);
                        // Dependiendo si es VENTA, RENTA o PÓLIZA, es el precio que se va a tomar
                        if($tipoCotizacion==1){
                            if ($tpeq==1) {
                                $precio = $equipo->costo_venta;
                            }elseif ($tpeq==2) {
                                $precio = $equipo->costo_poliza;
                            }else{
                                $precio = $equipo->costo_renta;
                            }
                            
                        }
                        else if ($tipoCotizacion==2){
                            $precio = $equipo->costo_renta;
                        }
                        else if ($tipoCotizacion==3){
                            $precio = $equipo->costo_poliza;
                        }
                        if ($tpeq==3) {
                                $precio = 0;
                        }
                        //========================================
                            //$costo_toner_black = $equipo->costo_toner_black;
                            //$costo_toner_cmy = $equipo->costo_toner_cmy;
                            //$rendimiento_toner_black = $equipo->rendimiento_toner_black;
                            //$rendimiento_toner_cmy = $equipo->rendimiento_toner_cmy;
                            //$costo_unidad_imagen = $equipo->costo_unidad_imagen;
                            //$rendimiento_unidad_imagen = $equipo->rendimiento_unidad_imagen;
                            //$costo_garantia_servicio = $equipo->costo_garantia_servicio;
                            //$rendimiento_garantia_servicio = $equipo->rendimiento_garantia_servicio;
                            //$costo_pagina_monocromatica = $equipo->costo_pagina_monocromatica;
                            //$costo_pagina_color = $equipo->costo_pagina_color;
                        //========================================
                            $tipo = 1;
                            $costo_toner = 0;
                            $rendimiento_toner = 0;
                            $costo_unidad_imagen = 0;
                            $rendimiento_unidad_imagen = 0;
                            $costo_garantia_servicio = 0;
                            $rendimiento_garantia_servicio = 0;
                            $costo_pagina_monocromatica = 0;
                            $costo_pagina_color = 0;
                        //========================================
                            $DATAc = json_decode($consumibles);
                            for ($i=0;$i<count($DATAc);$i++) {
                                if ($equipo->id==$DATAc[$i]->equiposelected) {
                                    $idconsumuble=$DATAc[$i]->consumibleselected;
                                    //log_message('error', 'idconsumible '.$idconsumuble);
                                    $resultadoaddcon=$this->ModeloCatalogos->consumibleagregadoselected($idconsumuble,$equipo->id);
                                    foreach ($resultadoaddcon->result() as $item2) {
                                        $tipo = $item2->tipo;
                                        $costo_toner = $item2->poliza;
                                        $rendimiento_toner = $item2->rendimiento_toner;
                                        $costo_unidad_imagen = $item2->costo_unidad_imagen;
                                        $rendimiento_unidad_imagen = $item2->rendimiento_unidad_imagen;
                                        $costo_garantia_servicio = $item2->costo_garantia_servicio;
                                        $rendimiento_garantia_servicio = $item2->rendimiento_garantia_servicio;
                                        $costo_pagina_monocromatica = $item2->costo_pagina_monocromatica;
                                        $costo_pagina_color = $item2->costo_pagina_color;


                                    }
                                        $datachfc["idventa"]=$idCotizacion;
                                        $datachfc["idequipo"]=$DATAc[$i]->equiposelected;
                                        $datachfc["cantidad"]=1;
                                        $datachfc["id_consumibles"]=$idconsumuble;
                                        $datachfc["costo_toner"]=$costo_toner;
                                        $datachfc["rendimiento_toner"]=$rendimiento_toner;
                                        $datachfc["rendimiento_unidad_imagen"]=$rendimiento_unidad_imagen;
                                        
                                        if ($tipoCotizacion==2) {
                                            unset($datachfc["idventa"]);
                                            $datachfc["idrentas"]=$idCotizacion;
                                            for ($a=0;$a<$DATAc[$i]->cantidad;$a++) {
                                                $this->ModeloCatalogos->Insert('rentas_has_detallesequipos_consumible',$datachfc);
                                            }

                                        }
                                }
                            }
                        //=========================================================
                            $DATAa = json_decode($accesorios);
                            for ($l=0;$l<count($DATAa);$l++) {
                                if ($equipo->id==$DATAa[$l]->equiposelected) {
                                    if ($tipoCotizacion==2) {
                                        //log_message('error', 'idrentas Accesorio: '.$idCotizacion);
                                        $dataas["idrentas"]=$idCotizacion;
                                        $dataas['id_equipo']=$DATAa[$l]->equiposelected;
                                        $dataas['id_accesorio']=$DATAa[$l]->accesorioselected;
                                        $dataas['cantidad']=1;
                                        for ($a=0;$a<$DATAa[$l]->cantidad;$a++) {
                                            $this->ModeloCatalogos->Insert('rentas_has_detallesEquipos_accesorios',$dataas);
                                        }
                                    }
                                }
                            }   
                        // Se arma el array para insertar los datos
                        if ($tipoCotizacion==2) {
                            $detallesCotizacion = array('idRenta' => $idCotizacion,
                                                        'idEquipo' => $equipo->id,
                                                        'cantidad'=>1,
                                                        'modelo' => $equipo->modelo,
                                                        'especificaciones_tecnicas' => $equipo->especificaciones_tecnicas,
                                                        'precio' => $precio,
                                                        'costo_unidad_imagen' => $costo_unidad_imagen,
                                                        'rendimiento_unidad_imagen' => $rendimiento_unidad_imagen,
                                                        'costo_garantia_servicio' => $costo_garantia_servicio,
                                                        'rendimiento_garantia_servicio' => $rendimiento_garantia_servicio,
                                                        'costo_pagina_monocromatica' => $costo_pagina_monocromatica,
                                                        'costo_pagina_color' => $costo_pagina_color,
                                                        'excedente' => $equipo->excedente,
                                                        'tipotoner' => $tipo,
                                                        'costo_toner' => $costo_toner,
                                                        'rendimiento_toner' => $rendimiento_toner,           
                                            );
                            // Se insertan en la BD
                            //$dataarrayinsert_eq_r[]=$detallesCotizacion;
                            for ($b=0;$b<$cantidadeq;$b++) {
                                $this->ModeloCatalogos->Insert('rentas_has_detallesEquipos',$detallesCotizacion);
                            }
                        }
                        //$this->Cotizaciones_model->insertarDetallesCotizacionEquipos($detallesCotizacion);
                        
                    }
                    //if(count($dataarrayinsert_eq_r)>0){
                    //    $this->ModeloCatalogos->insert_batch('rentas_has_detallesEquipos',$dataarrayinsert_eq_r);
                    //}
                //}
                 /*
                $DATAa = json_decode($accesorios);
               // log_message('error', 'accesorios:acc'.$accesorios);
                //log_message('error', 'accesorios: data '.$DATAa);
                for ($l=0;$l<count($DATAa);$l++) {
                    
                    
                    if ($tipoCotizacion==1) {
                        $dataas['id_venta']=$idCotizacion;
                        $dataas['id_equipo']=$DATAa[$l]->equiposelected;
                        $dataas['id_accesorio']=$DATAa[$l]->accesorioselected;
                        $this->ModeloCatalogos->Insert('ventas_has_detallesEquipos_accesorios',$dataas);
                    }elseif ($tipoCotizacion==2) {
                        log_message('error', 'idrentas Accesorio: '.$idCotizacion);
                        $dataas["idrentas"]=$idCotizacion;
                        $dataas['id_equipo']=$DATAa[$l]->equiposelected;
                        $dataas['id_accesorio']=$DATAa[$l]->accesorioselected;
                        $this->ModeloCatalogos->Insert('rentas_has_detallesEquipos_accesorios',$dataas);
                    }
                    //log_message('error', 'Equipo: '.$equiposelected.' accesorio: '.$accesorioselected);
                }   
                */
                // Se regresa el ID de la cotización
                $this->ordenarrentas($idCotizacion);
                echo $idCotizacion;
                //==================================================================
    }
    function ordenarrentas($idrenta){
        $pendiente=0;
        $resultequipos=$this->ModeloCatalogos->getselectwheren('rentas_has_detallesEquipos',array('idRenta'=>$idrenta,'estatus'=>1));
        foreach ($resultequipos->result() as $iteme) {
            $idrow_equipo=$iteme->id;
            $equipo=$iteme->idEquipo;
            $racce=$this->ModeloCatalogos->getselectwherenlimit('rentas_has_detallesEquipos_accesorios',array('idrentas'=>$idrenta,'id_equipo'=>$equipo,'rowequipo'=>null),1);
            foreach ($racce->result() as $itema) {
                $this->ModeloCatalogos->updateCatalogo('rentas_has_detallesEquipos_accesorios',array('rowequipo'=>$idrow_equipo),array('id_accesoriod'=>$itema->id_accesoriod));
            }
            $raccc=$this->ModeloCatalogos->getselectwherenlimit('rentas_has_detallesequipos_consumible',array('idrentas'=>$idrenta,'idequipo'=>$equipo,'rowequipo'=>null),1);
            foreach ($raccc->result() as $itemc) {
                $this->ModeloCatalogos->updateCatalogo('rentas_has_detallesequipos_consumible',array('rowequipo'=>$idrow_equipo),array('id'=>$itemc->id));
            }
        }

        $raccenum=$this->ModeloCatalogos->getselectwheren('rentas_has_detallesEquipos_accesorios',array('idrentas'=>$idrenta,'rowequipo'=>null));
        if($raccenum->num_rows()>0){
            $pendiente=1;
        }
        $racccnum=$this->ModeloCatalogos->getselectwheren('rentas_has_detallesequipos_consumible',array('idrentas'=>$idrenta,'rowequipo'=>null));
        if($racccnum->num_rows()>0){
            $pendiente=1;
        }
        if($pendiente==1){
            $this->ordenarrentas($idrenta);
        }

    }
    /*
    function nuevaventaconsumibles(){
        $data = $this->input->post();

        $equiposSeleccionados = $data['arrayEquipos'];
                unset($data['arrayEquipos']);

                $tonersSeleccionados = $data['arrayToner'];
                unset($data['arrayToner']);

                $piezasSeleccionadas = $data['arrayPiezas'];
                unset($data['arrayPiezas']);

                $bodegasc = $data['bodegasc'];
                unset($data['bodegasc']);

                $presiosSeleccionadas = $data['arrayPrecio'];
                unset($data['arrayPrecio']);

                unset($data['equipoListaConsumibles']);
                unset($data['toner']);

                $data['tipo'] = 2;

                $equiposArreglo = explode(',', $equiposSeleccionados);
                $tonersArreglo = explode(',', $tonersSeleccionados);
                $piezasArreglo = explode(',', $piezasSeleccionadas);
                $precioArreglo = explode(',', $presiosSeleccionadas);
                $bodegascArreglo = explode(',', $bodegasc);
                
                $datosCotizacion = array('atencion' => $data['atencion'],
                                            'correo' => $data['correo'],
                                            'telefono' => $data['telefono'],
                                            'idCliente' => $data['idCliente'],
                                            'tipo' => $data['tipo'],
                                            //'rentaVentaPoliza' => 1,
                                            'id_personal'=>$this->idpersonal,
                                            'estatus' => 1,
                                            'reg'=>$this->fechaactual,
                                        'combinada' => $data['combinada']
                                            );
                $idCotizacion = $this->ModeloGeneral->getinsert($datosCotizacion,'ventas');
                //$idCotizacion = $this->Cotizaciones_model->insertarCotizacion($datosCotizacion);
                $dataconsumiblesarray=array();
                for ($i=0; $i < count($equiposArreglo); $i++){
                    $bodegaselect=$bodegascArreglo[$i];
                    if($tonersArreglo[$i]>0){
                        $datosConsumible = $this->Consumibles_model->getConsumibleConPrecios($tonersArreglo[$i]);
                        $total=$precioArreglo[$i]*$piezasArreglo[$i];
                        $detallesCotizacion = array('idVentas' => $idCotizacion,
                                                'idEquipo' => $equiposArreglo[$i],
                                                'idConsumible' => $datosConsumible[0]->id,
                                                'piezas' => $piezasArreglo[$i], 
                                                'modelo' => $datosConsumible[0]->modelo,
                                                'rendimiento' =>'',
                                                'descripcion' => $datosConsumible[0]->observaciones,
                                                'precioGeneral' => $precioArreglo[$i],
                                                'totalGeneral' => $total,
                                                'bodegas' => $bodegaselect,
                                            );
                        $this->ModeloCatalogos->updatestock2('consumibles_bodegas','total','-',$piezasArreglo[$i],'consumiblesId',$datosConsumible[0]->id,'bodegaId',$bodegaselect);
                        $this->ModeloCatalogos->Insert('bitacora_movimientos',array('id_item'=>$datosConsumible[0]->id,'modelo'=>$datosConsumible[0]->modelo,'tipo'=>4,'entrada_salida'=>0,'personal'=>$this->idpersonal,'descripcion_evento'=>'Salida para venta '.$idCotizacion.''));
                        $dataconsumiblesarray[]=$detallesCotizacion;
                        //$this->ModeloGeneral->getinsert($detallesCotizacion,'ventas_has_detallesConsumibles');
                        //$this->Cotizaciones_model->insertarDetallesCotizacionConsumibles($detallesCotizacion);
                    }
                }
                if(count($dataconsumiblesarray)>0){
                    $this->ModeloCatalogos->insert_batch('ventas_has_detallesConsumibles',$dataconsumiblesarray);
                }
                echo $idCotizacion;
    }
    */
    /*
    function nuevaventarefacciones(){
        $data = $this->input->post();

        $arrayRefaccion = $data['arrayRefaccion'];
        unset($data['arrayRefaccion']);
        
        $datosCotizacion = array('atencion' => $data['atencion'],
                                    'correo' => $data['correo'],
                                    'telefono' => $data['telefono'],
                                    'idCliente' => $data['idCliente'],
                                    'tipo' => 3,
                                    //'rentaVentaPoliza' => 1,
                                    'estatus' => 1,
                                    'reg'=>$this->fechaactual,
                                'combinada' => $data['combinada'],
                                'id_personal'=> $this->idpersonal
                                    );
                
        $idVenta = $this->ModeloGeneral->getinsert($datosCotizacion,'ventas');
        $datarefaccionesarray=array();
        $DATAc = json_decode($arrayRefaccion);       
        for ($i=0;$i<count($DATAc);$i++) {
            // insertar 
            if ($DATAc[$i]->toner>0) {
                
                $datosConsumible = $this->Consumibles_model->getrefaccionesConPrecios($DATAc[$i]->toner);
                    $total=$DATAc[$i]->tonerp*$DATAc[$i]->piezas;
                    $detallesCotizacion = array('idVentas' => $idVenta,
                                            'idEquipo' => $DATAc[$i]->equipo,
                                            'serie' => $DATAc[$i]->serie,
                                            'idRefaccion' => $datosConsumible[0]->id,
                                            'piezas' => $DATAc[$i]->piezas, 
                                            'modelo' => $datosConsumible[0]->nombre,
                                            'rendimiento' =>'',
                                            'descripcion' => $datosConsumible[0]->observaciones,
                                            'precioGeneral' => $DATAc[$i]->tonerp,
                                            'totalGeneral' => $total,
                                            'serie_bodega' => $DATAc[$i]->bodega
                                        );
                    $datarefaccionesarray[] =$detallesCotizacion;
                    
            }
        }
        if(count($datarefaccionesarray)>0){
            $this->ModeloCatalogos->insert_batch('ventas_has_detallesRefacciones',$datarefaccionesarray);
        }
        echo $idVenta;
    }
    */
    function nuevaventacombinada(){
        $data = $this->input->post();
        $data['id_personal']=$this->idpersonal;
        $id=$this->ModeloCatalogos->Insert('ventacombinada',$data);
        echo $id;
    }
    // funcion para editar los datos de rentas cuando ya se a aceptado la venta de tipo renta
    function viewventarenta(){
        $idventa = $this->input->post('idventa');
        $where_vde=array('idRenta'=>$idventa);
        $result_vde=$this->ModeloCatalogos->db10_getselectwheren('rentas_has_detallesEquipos',$where_vde);
        //$resultbodegas=$this->ModeloCatalogos->db10_getselectwheren('bodegas',array('activo' => 1));

        $result_vdacc=$this->ModeloCatalogos->ventasaccesoriosrentas($idventa);
        $result_vdcon=$this->ModeloCatalogos->ventasconsumiblesrenta($idventa);
        //$resultbodegas=$this->ModeloCatalogos->db10_getselectwheren('bodegas',array('activo' => 1));
        $resultbodegas=$this->ModeloCatalogos->view_session_bodegas();

        $html='<table class="highlight" id="venta_equiposrenta">
                      <thead>
                        <tr>
                          <th>Cantidad</th>
                          <th>Equipo</th>
                          <th>Bodega</th>
                        </tr>
                      </thead>
                      <tbody>
                        
                      ';
        foreach ($result_vde->result() as $item) {
            $html.='<tr>
                          <td>
                            <input type="hidden" name="id" id="id" value="'.$item->id.'">
                            <input type="number" name="cantidad" id="cantidad" value="'.$item->cantidad.'" min="1">
                          </td>
                          <td>
                            '.$item->modelo.'
                          </td>
                          <td>
                            <select id="serie_bodega" name="serie_bodega" class="browser-default chosen-select form-control-bmz" >';
                              foreach ($resultbodegas as $itemb) {
                                    $stockbe=$this->ModeloGeneral->stockequipobodega($item->idEquipo,$itemb->bodegaId);
                                  $html.='<option value="'.$itemb->bodegaId.'" data-stock="'.$stockbe.'">'.$itemb->bodega.' ('.$stockbe.')</option>';
                               }  
                            $html.='</select> 
                          </td>
                        </tr>';

        }
        $html.='</tbody>
            </table>';
        $html.='<table class="highlight" id="venta_accesorios">
                      <thead>
                        <tr>
                          <th>Cantidad</th>
                          <th>Accesorio</th>
                          <th>Bodega</th>
                        </tr>
                      </thead>
                      <tbody>';
        foreach ($result_vdacc->result() as $item) {
                $html.='<tr>
                          <td>
                            <input type="hidden" name="id_accesorio" id="id_accesorio" value="'.$item->id_accesorio.'">
                            <input type="number" name="cantidad" id="cantidad" value="'.$item->cantidad.'" min="1">
                          </td>
                          <td>
                            '.$item->nombre.'
                          </td>
                          <td>
                            <select id="serie_bodega" name="serie_bodega" class="browser-default chosen-select form-control-bmz" >';
                              foreach ($resultbodegas as $itemb) {
                                    $stockacc=$this->ModeloSerie->stockaccesoriosbodega($item->id_accesorio,$itemb->bodegaId);
                                  $html.='<option value="'.$itemb->bodegaId.'" data-stock="'.$stockacc.'">'.$itemb->bodega.'('.$stockacc.')</option>';
                               }  
                            $html.='</select> 
                          </td>
                        </tr>';
        }
        $html.='</tbody>
            </table>';
        $html.='<table class="highlight" id="venta_consumibles">
                      <thead>
                        <tr>
                          <th>Cantidad</th>
                          <th>Consumible</th>
                          <th>Bodega</th>
                        </tr>
                      </thead>
                      <tbody>';
        foreach ($result_vdcon->result() as $item) {
            

            $html.='<tr>
                          <td>
                            <input type="hidden" name="id_accesorio" id="id_accesorio" value="'.$item->id.'">
                            <input type="number" name="cantidad" id="cantidad" value="'.$item->cantidad.'" min="1">
                          </td>
                          <td>
                            '.$item->modelo.'
                          </td>
                          <td>
                            <select id="serie_bodega" name="serie_bodega" class="browser-default chosen-select form-control-bmz" >';
                              foreach ($resultbodegas as $itemb) {
                                $stocktoner=$this->ModeloSerie->stocktonerbodegas($item->id_consumibles,$itemb->bodegaId);
                                  $html.='<option value="'.$itemb->bodegaId.'" data-stock="'.$stocktoner.'">'.$itemb->bodega.' ('.$stocktoner.')</option>';
                               }  
                            $html.='</select> 
                          </td>

                        </tr>';
        }
        $html.='</tbody>
            </table>';
        echo $html;
    }
        ////////////////7 
    public function verificar_fecha_vencimiento(){ 
        $id = $this->input->post('id'); 
        $ven=''; 
        $aux=''; 
        $arraywhere = array('ventaId'=>$id); 
        $result=$this->ModeloCatalogos->db3_getselectwheren_2('vencimiento','prefactura',$arraywhere); 
        foreach ($result as $item) { 
           $ven=$item->vencimiento; 
        } 
        if($ven=='0000-00-00'){ 
           $aux=''; 
        }else{ 
           $aux=$ven; 
        } 
        echo $aux; 
    } 
    public function update_cfdi_registro(){ 
        $id_venta = $this->input->post('id_venta'); 
 
        //$data[''] = $this->input->post('tipo'); 
        $cfdi = $this->input->post('cfdi'); 
        $arraydata = array('ventaId' =>$id_venta); 
        $data = array('cfdi'=>$cfdi); 
        $this->ModeloCatalogos->updateCatalogo('prefactura',$data,$arraydata); 
    } 
    function montoventas(){//ventas 
        $id=$this->input->post('id'); 
        $totalgeneral=0; 

        $resultfactura=$this->ModeloCatalogos->ontenerdatosfacturaventa_vd($id,0);
        
        if($resultfactura->num_rows()==0){

            $rowequipos=$this->ModeloCatalogos->db10_getselectwheren('ventasd_has_detallesequipos',array('idVenta'=>$id)); 
            foreach ($rowequipos->result() as $item) { 
                $totalgeneral=$totalgeneral+($item->cantidad*$item->precio); 
            } 
            $resultadoaccesorios = $this->ModeloCatalogos->accesoriosventasd($id); 
            foreach ($resultadoaccesorios->result() as $item) { 
                $totalgeneral=$totalgeneral+($item->cantidad*$item->costo); 
            } 
            $resultadoconsumibles = $this->ModeloCatalogos->consumiblesventasd($id); 
            foreach ($resultadoconsumibles->result() as $item) {  
                $totalgeneral=$totalgeneral+($item->cantidad*$item->costo_toner); 
            } 
            $consumiblesventadetalles = $this->ModeloCatalogos->consumiblesventasddetalles($id); 
            foreach ($consumiblesventadetalles->result() as $item) {  
                $totalgeneral=$totalgeneral+($item->cantidad*$item->costo_toner); 
            } 
            $ventadetallesrefacion = $this->ModeloCatalogos->ventasd_has_detallesrefacciones($id); 
            foreach ($ventadetallesrefacion->result() as $item) {  
                $totalgeneral=$totalgeneral+($item->cantidad*$item->precioGeneral); 
            } 
            $totalgeneral=$totalgeneral+($totalgeneral*0.16); 

        }else{
            foreach ($resultfactura->result() as $item) {
                $totalgeneral=$totalgeneral+$item->total;
            }
        }
        echo round($totalgeneral, 2);  
    } 
    /*
    function montocombinada(){// convinadas 
        $id=$this->input->post('id'); 

        $resultfactura=$this->ModeloCatalogos->ontenerdatosfacturaventa($id,1);
        $totalgeneral=0; 

        if($resultfactura->num_rows()==0){

            $where_ventas=array('combinadaId'=>$id); 
            $resultadov = $this->ModeloCatalogos->db10_getselectwheren('ventacombinada',$where_ventas); 
            $equipo=0; 
            $consumibles=0; 
            $refacciones=0; 
            $poliza=0; 
            foreach ($resultadov->result() as $item) { 
                    $equipo=$item->equipo; 
                    $consumibles=$item->consumibles; 
                    $refacciones=$item->refacciones; 
                    $poliza=$item->poliza; 
            } 
            
            $rowequipos=$this->ModeloCatalogos->db10_getselectwheren('ventas_has_detallesEquipos',array('idVenta'=>$equipo)); 
            foreach ($rowequipos->result() as $item) { 
                $totalgeneral=$totalgeneral+($item->cantidad*$item->precio); 
            } 
            $resultadoaccesorios = $this->ModeloCatalogos->accesoriosventa($equipo); 
            foreach ($resultadoaccesorios->result() as $item) { 
                $totalgeneral=$totalgeneral+($item->cantidad*$item->costo); 
            } 
            $resultadoconsumibles = $this->ModeloCatalogos->consumiblesventa($equipo); 
            foreach ($resultadoconsumibles->result() as $item) {  
                $totalgeneral=$totalgeneral+($item->cantidad*$item->costo_toner); 
            } 
            $consumiblesventadetalles = $this->ModeloCatalogos->consumiblesventadetalles($consumibles); 
            foreach ($consumiblesventadetalles->result() as $item) {  
                $totalgeneral=$totalgeneral+($item->cantidad*$item->costo_toner); 
            } 
            $ventadetallesrefacion = $this->ModeloCatalogos->ventas_has_detallesRefacciones($refacciones); 
            foreach ($ventadetallesrefacion->result() as $item) {  
                $totalgeneral=$totalgeneral+($item->cantidad*$item->precioGeneral); 
            } 
            $polizaventadetalles = $this->ModeloCatalogos->polizaventadetalles3($poliza); 
            foreach ($polizaventadetalles->result() as $item) {  
                if($item->precio_local !==NULL) { 
                    $totalc=$item->precio_local; 
                } 
                if($item->precio_semi !==NULL) { 
                    $totalc=$item->precio_semi; 
                } 
                if($item->precio_foraneo !==NULL) { 
                    $totalc=$item->precio_foraneo; 
                } 
                $totalgeneral=$totalgeneral+($totalc); 
                //$totalgeneral=$totalgeneral+($totalc*item->cantidad); 
                     
            } 
            $totalgeneral=$totalgeneral+($totalgeneral*0.16);
        }else{
            foreach ($resultfactura->result() as $item) {
                $totalgeneral=$totalgeneral+$item->total;
            }
        }
        echo round($totalgeneral, 2);
    } 
    */
    function montoventas2($id){//ventas 
        //misma estructura en ModeloCatalogos->montoventas2
        $totalgeneral=0; 
        $rowequipos=$this->ModeloCatalogos->db10_getselectwheren('ventasd_has_detallesequipos',array('idVenta'=>$id)); 
        foreach ($rowequipos->result() as $item) { 
            $totalgeneral=$totalgeneral+($item->cantidad*$item->precio); 
        } 
        $resultadoaccesorios = $this->ModeloCatalogos->accesoriosventasd($id); 
        foreach ($resultadoaccesorios->result() as $item) { 
            $totalgeneral=$totalgeneral+($item->cantidad*$item->costo); 
        } 
        $resultadoconsumibles = $this->ModeloCatalogos->consumiblesventasd($id); 
        foreach ($resultadoconsumibles->result() as $item) {  
            $totalgeneral=$totalgeneral+($item->cantidad*$item->costo_toner); 
        } 
        $consumiblesventadetalles = $this->ModeloCatalogos->consumiblesventasddetalles($id); 
        foreach ($consumiblesventadetalles->result() as $item) {  
            $totalgeneral=$totalgeneral+($item->cantidad*$item->costo_toner); 
        } 
        $ventadetallesrefacion = $this->ModeloCatalogos->ventasd_has_detallesrefacciones($id); 
        foreach ($ventadetallesrefacion->result() as $item) {  
            $totalgeneral=$totalgeneral+($item->cantidad*$item->precioGeneral); 
        } 
        $totalgeneral=$totalgeneral+($totalgeneral*0.16); 
 
        return round($totalgeneral, 2);  
    } 
    /*
    function montocombinada2($id){// convinadas 
        //misma estructura en ModeloCatalogos->montocombinada2
        $where_ventas=array('combinadaId'=>$id); 
        $resultadov = $this->ModeloCatalogos->db10_getselectwheren('ventacombinada',$where_ventas); 
        $equipo=0; 
        $consumibles=0; 
        $refacciones=0; 
        $poliza=0; 
        foreach ($resultadov->result() as $item) { 
                $equipo=$item->equipo; 
                $consumibles=$item->consumibles; 
                $refacciones=$item->refacciones; 
                $poliza=$item->poliza; 
        } 
        $totalgeneral=0; 
        $rowequipos=$this->ModeloCatalogos->db10_getselectwheren('ventas_has_detallesEquipos',array('idVenta'=>$equipo)); 
        foreach ($rowequipos->result() as $item) { 
            $totalgeneral=$totalgeneral+($item->cantidad*$item->precio); 
        } 
        $resultadoaccesorios = $this->ModeloCatalogos->accesoriosventa($equipo); 
        foreach ($resultadoaccesorios->result() as $item) { 
            $totalgeneral=$totalgeneral+($item->cantidad*$item->costo); 
        } 
        $resultadoconsumibles = $this->ModeloCatalogos->consumiblesventa($equipo); 
        foreach ($resultadoconsumibles->result() as $item) {  
            $totalgeneral=$totalgeneral+($item->cantidad*$item->costo_toner); 
        } 
        $consumiblesventadetalles = $this->ModeloCatalogos->consumiblesventadetalles($consumibles); 
        foreach ($consumiblesventadetalles->result() as $item) {  
            $totalgeneral=$totalgeneral+($item->cantidad*$item->costo_toner); 
        } 
        $ventadetallesrefacion = $this->ModeloCatalogos->ventas_has_detallesRefacciones($refacciones); 
        foreach ($ventadetallesrefacion->result() as $item) {  
            $totalgeneral=$totalgeneral+($item->cantidad*$item->precioGeneral); 
        } 
        $polizaventadetalles = $this->ModeloCatalogos->polizaventadetalles2($poliza); 
        foreach ($polizaventadetalles->result() as $item) {  
            if($item->precio_local !==NULL) { 
                $totalc=$item->precio_local; 
            } 
            if($item->precio_semi !==NULL) { 
                $totalc=$item->precio_semi; 
            } 
            if($item->precio_foraneo !==NULL) { 
                $totalc=$item->precio_foraneo; 
            } 
            $totalgeneral=$totalgeneral+($totalc); 
            //$totalgeneral=$totalgeneral+($totalc*item->cantidad); 
            
 
             
        } 
        $totalgeneral=$totalgeneral+($totalgeneral*0.16); 
        return round($totalgeneral, 2); 
    } 
    */
    public function guardar_pago_tipo(){ 
        $data = $this->input->post(); 
        $tipo = $data['tipo']; 
        $idcompra = $data['idcompra']; 
        $data['idpersonal']=$this->idpersonal; 
        unset($data['idcompra']); 
        unset($data['tipo']); 
        if($this->idpersonal==18){// si diana es la que deposito se confirma en automatico
            $data['confirmado']=1;
        }
        /*
        if($tipo==1){//combinada 
            $data['idventa']=$idcompra; 
            $this->ModeloCatalogos->Insert('pagos_combinada',$data); 
            //===================================================================
            $rowpagos=$this->ModeloCatalogos->db10_getselectwheren('pagos_combinada',array('idventa'=>$idcompra));
            $totalpagos=0;
            foreach ($rowpagos->result() as $item) {
                $totalpagos=$totalpagos+$item->pago;
            }
            $totalpre=$this->montocombinada2($idcompra);
            if ($totalpagos>=$totalpre) {
                $this->ModeloCatalogos->updateCatalogo('ventacombinada',array('statuspago'=>1),array('combinadaId'=>$idcompra));
            }

        }else{//venta  
        */
            $data['idventa']=$idcompra; 
            $this->ModeloCatalogos->Insert('ventasd_pagos',$data); 
            //===================================================================
            $rowpagos=$this->ModeloCatalogos->db10_getselectwheren('ventasd_pagos',array('idventa'=>$idcompra));
            $totalpagos=0;
            foreach ($rowpagos->result() as $item) {
                $totalpagos=$totalpagos+$item->pago;
            }
            
            $totalpre=$this->montoventas2($idcompra);
            if ($totalpagos>=$totalpre) {
                $this->ModeloCatalogos->updateCatalogo('ventasd',array('statuspago'=>1),array('id'=>$idcompra));
            }
            

        //}    
    } 
    public function table_get_tipo_compra(){ 
        $id = $this->input->post('id'); 
        $tip = $this->input->post('tip'); 
        $resultfactura=$this->ModeloCatalogos->ontenerdatosfacturaventa_vd($id,$tip);
        $html='<table class="responsive-table display striped" cellspacing="0" id="tablepagosg"> 
                    <thead> 
                      <tr> 
                        <th>Fecha</th> 
                        <th>Método de pago</th> 
                        <th>Monto</th>
                        <th>Comentario</th> 
                        <!--<th>Comprobante</th>-->
                        <th></th> 
                      </tr> 
                    </thead> 
                    <tbody>
                        <tr style="display: none;">
                            <td><input type="hidden" id="idventapoliza" value="'.$id.'" data-tipovp="3" ></td>
                             <td></td>
                             <td></td>
                             <td></td> 
                             <!--<td></td>-->
                             <td></td> 
                        </tr>
                    '; 
        
            if($resultfactura->num_rows()==0){
               $result = $this->ModeloCatalogos->pagos_ventas_vd($id); 
               foreach ($result as $item) {
               if($item->confirmado==1){
                    $itemconfirmado='Confirmado <i class="material-icons" style="color:green;">done_all</i>';
               }else{
                    $itemconfirmado='Sin Confirmar <a class="new badge yellow" style="padding: 9px 2px 0px;" onclick="confirmarpagov('.$item->idpago.',0)"><i class="material-icons">warning</i> </a>';
               } 
                $html.='<tr> 
                         <td>
                            <input type="hidden" id="idventapoliza" value="'.$id.'" data-tipovp="3" >
                            '.$item->fecha.'</td> 
                         <td>'.$item->formapago_text.'</td> 
                         <td class="montoagregado">'.$item->pago.'</td> 
                         <td>'.$item->observacion.'</td> 
                         <!--<td>'.$item->comprobante.'</td>--> 
                         <td>
                            <span 
                                class="new badge red" 
                                style="cursor: pointer;" 
                                onclick="deletepagov('.$item->idpago.',0)">
                                <i class="material-icons">delete</i>
                            </span>
                            '.$itemconfirmado.'
                        </td>
                        </tr>'; 
                } 
            }
        
                foreach($resultfactura->result() as $item){
                    $FacturasIdselected=$item->FacturasId;
                    $resultvcp = $this->ModeloCatalogos->obtencionventapolizafactura_vd($item->FacturasId);

                    foreach ($resultvcp->result() as $itemvcp) {

                                    $html.='<tr style="display: none;">
                                            <td><input type="hidden" id="idventapoliza" value="'.$itemvcp->ventaId.'" data-tipovp="3" ></td>
                                            <td></td><td></td><td></td><!--<td></td>--><td></td></tr>';
                            //=======================================
                                 $result = $this->ModeloCatalogos->pagos_ventas_vd($itemvcp->ventaId); 
                                   foreach ($result as $item) { 
                                    if($item->confirmado==1){
                                        $itemconfirmado='Confirmado <i class="material-icons" style="color:green;">done_all</i>';
                                   }else{
                                        $itemconfirmado='Sin Confirmar <a class="new badge yellow" style="padding: 9px 2px 0px;" onclick="confirmarpagov('.$item->idpago.',0)"><i class="material-icons">warning</i> </a>';
                                   } 
                                    $html.='<tr> 
                                             <td>
                                                <input type="hidden" id="idventapoliza" value="'.$itemvcp->ventaId.'" data-tipovp="0">
                                                '.$item->fecha.'</td> 
                                             <td>'.$item->formapago_text.'</td> 
                                             <td class="montoagregado">'.$item->pago.'</td> 
                                             <td>'.$item->observacion.'</td> 
                                             <!--<td>'.$item->comprobante.'</td>--> 
                                             <td>
                                                <span 
                                                    class="new badge red" 
                                                    style="cursor: pointer;" 
                                                    onclick="deletepagov('.$item->idpago.',0)">
                                                    <i class="material-icons">delete</i>
                                                </span>
                                                '.$itemconfirmado.'
                                            </td>
                                            </tr>'; 
                                    } 
                            //=======================================
                        
                    }
                    $resultvcp_cp = $this->ModeloCatalogos->obteciondecomplememtofactura($FacturasIdselected);
                    foreach ($resultvcp_cp->result() as $itemcp) {
                        $html.='<tr> 
                                     <td>
                                        <input type="hidden" id="idventapoliza" value="'.$id.'" data-tipovp="3" class="pagodecomplemento">
                                        '.$itemcp->Fecha.'
                                    </td> 
                                     <td></td> 
                                     <td class="montoagregado">'.$itemcp->ImpPagado.'</td> 
                                     <td>Complemento</td> 
                                     <!--<td></td>--> 
                                     <td>
                                        
                                    </td>
                                    </tr>'; 
                    }
                }
        $html.='</tbody> 
                  </table>'; 
        echo $html; 
    } 
    function vericar_pass(){
      $pass = $this->input->post('pass');
      $pago = $this->input->post('pago');
      $tipo = $this->input->post('tipo'); 
      $verifica = $this->Rentas_model->verificar_pass2($pass);
      //log_message('error', '------ '.$verifica);
      $aux = 0;
      if($verifica == 1){
         $aux=1;
        //if ($tipo==1) {
            //combinada
            //log_message('error', ' pagos combinada ');
          //  $this->ModeloCatalogos->getdeletewheren2('pagos_combinada',$pago);
            //$this->Modelobitacoras->Insert(array('contenido'=>'Se Elimino pago de venta combinada','nombretabla'=>'pagos_combinada','idtable'=>$pago,'tipo'=>'Delete','personalId'=>$this->idpersonal));
        //}
        //if ($tipo==0) {
            //venta normal
            //log_message('error', ' pagos ventas ');
            $this->ModeloCatalogos->getdeletewheren2('ventasd_pagos',$pago);
            $this->Modelobitacoras->Insert(array('contenido'=>'Se Elimino pago de ventad','nombretabla'=>'pagos_ventas','idtable'=>$pago,'tipo'=>'Delete','personalId'=>$this->idpersonal));
        //}
      }
      echo $aux;
    }
    function addseriviciovd(){
        $data = $this->input->post(); 
        $idventa=intval($data['idventa']);
        $poliza = $data['poliza'];
        $servicio = $data['servicio'];
        $direccion = $data['direccion'];
        $tipo = $data['tipo'];
        $motivo = $data['motivo'];
        $fechaentrega=$data['fecharecoleccion'];

        $this->ModeloCatalogos->updateCatalogo('ventas',array('servicio'=>1,'servicio_poliza'=>$poliza,'servicio_servicio'=>$servicio,'servicio_direccion'=>$direccion,'servicio_tipo'=>$tipo,'servicio_motivo'=>$motivo,'fechaentrega'=>$fechaentrega),array('id'=>$idventa));
    }
    function addseriviciovd2(){
        $data = $this->input->post(); 
        $idventa=intval($data['idventa']);
        $fechaentrega=$data['fecharecoleccion'];

        $this->ModeloCatalogos->updateCatalogo('ventas',array('fechaentrega'=>$fechaentrega),array('id'=>$idventa));
    }
    ///////////////////////////
    public function por_evento(){
        $this->load->view('header');
        $this->load->view('ventas/porevento');
        $this->load->view('footer');
        $this->load->view('ventas/poreventojs');
    }
    public function tipo_formato($id){
        $data['configuracionCotizacion'] = $this->ConfiguracionDocumentos_Model->getConfiguracion(1);
        if($id==1){ 
            $this->load->view('Reportes/porevento',$data); 
        }else{
            $this->load->view('Reportes/contrato',$data);
        }

    }
    function ventadelete(){
        $data = $this->input->post();
        $ventaid = $data['ventaid'];
        $ventatipo = $data['ventatipo'];
        $motivo = $data['motivo'];
        if (isset($this->idpersonal)) {
            $idpersonal=$this->idpersonal;
        }else{
            $idpersonal=0;
        }
        if ($ventatipo==0) {//normal
            //$resultfarventa=$this->ModeloCatalogos->db10_getselectwheren('factura_venta',array('ventaId'=>$ventaid,'combinada'=>0));
            $resultfarventa=$this->Ventas_model->validarfactura($ventaid,0);
            if ($resultfarventa->num_rows()>0) {
                $eliminacion=0;
            }else{
                $eliminacion=1;
                $this->ModeloCatalogos->updateCatalogo('ventasd',array('motivo'=>$motivo,'activo'=>0,'personaldelete'=>$idpersonal),array('id'=>$ventaid));
                //====================== consumibles 
                    $datosconsumibles=$this->ModeloCatalogos->db10_getselectwheren('ventasd_has_detallesconsumibles',array('idVentas'=>$ventaid));
                    foreach ($datosconsumibles->result() as $item) {
                        if($item->bodegas>0){
                            $itembodegas=$item->bodegas;
                        }else{
                            $itembodegas=1;//lo manda directo a boule
                        }
                        $this->ModeloCatalogos->updatestock2('consumibles_bodegas','total','+',$item->piezas,'consumiblesId',$item->idConsumible,'bodegaId',$itembodegas);
                        //$this->ModeloCatalogos->getdeletewheren('contrato_folio',array('foliotext'=>$item->foliotext));
                        //1 equipo, 2 acesorio, 3 refaccion,4 consumible    
                        $this->ModeloCatalogos->Insert('bitacora_movimientos',array('id_item'=>$item->id,'cantidad'=>$item->piezas,'modeloId'=>$item->idConsumible,'tipo'=>4,'entrada_salida'=>1,'personal'=>$this->idpersonal,'descripcion_evento'=>'devolucion por eliminacion para ventad '.$ventaid.''));
                    }
                //======================
                //====================== equipos 
                    $datosequipos=$this->ModeloCatalogos->db10_getselectwheren('ventasd_has_detallesequipos',array('idVenta'=>$ventaid));
                    foreach ($datosequipos->result() as $item) {

                        $datosseriese=$this->ModeloCatalogos->db10_getselectwheren('ventasd_asignacion_series_equipos',array('id_equipo'=>$item->id));
                        foreach ($datosseriese->result() as $items) {
                            
                            $this->ModeloCatalogos->updateCatalogo('series_productos',array('status'=>0),array('serieId'=>$items->serieId));

                        //1 equipo, 2 acesorio, 3 refaccion,4 consumible    
                        $this->ModeloCatalogos->Insert('bitacora_movimientos',array('id_item'=>$item->id,'cantidad'=>1,'modeloId'=>$item->idEquipo,'tipo'=>1,'entrada_salida'=>1,'personal'=>$this->idpersonal,'descripcion_evento'=>'devolucion serie por eliminacion para ventad '.$ventaid,'serieId'=>$items->serieId));
                        }
                    }
                //======================
                //====================== accesorios 
                    $datosaccesorios=$this->ModeloCatalogos->db10_getselectwheren('ventasd_has_detallesequipos_accesorios',array('id_venta'=>$ventaid));
                    foreach ($datosaccesorios->result() as $item) {

                        $datosseriese=$this->ModeloCatalogos->db10_getselectwheren('ventasd_asignacion_series_accesorios',array('id_accesorio'=>$item->id_accesorio));
                        foreach ($datosseriese->result() as $items) {
                            
                            $this->ModeloCatalogos->updateCatalogo('series_accesorios',array('status'=>0),array('serieId'=>$items->serieId));
                            //1 equipo, 2 acesorio, 3 refaccion,4 consumible    
                            $this->ModeloCatalogos->Insert('bitacora_movimientos',array('id_item'=>$item->id_accesoriod,'cantidad'=>1,'modeloId'=>$item->id_accesorio,'tipo'=>2,'entrada_salida'=>1,'personal'=>$this->idpersonal,'descripcion_evento'=>'devolucion serie por eliminacion para ventad '.$ventaid,'serieId'=>$items->serieId));
                        }
                    }
                //======================
                //====================== refacciones 
                    $datosrefacciones=$this->ModeloCatalogos->db10_getselectwheren('ventasd_has_detallesrefacciones',array('idVentas'=>$ventaid));
                    foreach ($datosrefacciones->result() as $item) {
                        $bodega=$item->serie_bodega;
                        $piezas=$item->piezas;
                        //log_message('error', 'idRefaccion '.$item->idRefaccion);
                        //log_message('error', 'id '.$item->id);
                        $datosseriesr=$this->ModeloCatalogos->db10_getselectwheren('ventasd_asignacion_series_refacciones',array('id_refaccion'=>$item->id));
                        foreach ($datosseriesr->result() as $items) {
                            $serieId=$items->serieId;
                            $cantidad=$items->cantidad;
                            if($items->serieId>0){
                                $datosseriesrss=$this->ModeloCatalogos->db10_getselectwheren('series_refacciones',array('serieId'=>$items->serieId));
                                foreach ($datosseriesrss->result() as $itemm) {
                                    if ($itemm->con_serie==1) {
                                        $cantidad=1;
                                        $this->ModeloCatalogos->updateCatalogo('series_refacciones',array('status'=>0,'cantidad'=>$cantidad),array('serieId'=>$items->serieId));
                                    }else{
                                        //log_message('error', 'serieId '.$items->serieId);
                                        $this->ModeloCatalogos->updatestock('series_refacciones','cantidad','+',$piezas,'serieId',$items->serieId);
                                        //$cantidad=$cantidad+$itemm->cantidad;
                                    }
                                    //1 equipo, 2 acesorio, 3 refaccion,4 consumible    
                                    $this->ModeloCatalogos->Insert('bitacora_movimientos',array('id_item'=>$item->id,'cantidad'=>$piezas,'modeloId'=>$item->idRefaccion,'tipo'=>3,'entrada_salida'=>1,'personal'=>$this->idpersonal,'descripcion_evento'=>'devolucion serie por eliminacion para ventad '.$ventaid,'serieId'=>$items->serieId));
                                }
                            }
                            ///=================================================
                            
                        }
                    }
                //======================
            }
        }
        echo $eliminacion;
    }
    /*
    function serviciodelete(){
        $data = $this->input->post();
        $servicioid = $data['servicioid'];
        $serviciotipo = $data['serviciotipo'];
        $motivo = $data['motivo'];
        
        if ($serviciotipo==1) {
            $table='asignacion_ser_contrato_a';
        }elseif ($serviciotipo==2) {
            $table='asignacion_ser_poliza_a';
        }elseif ($serviciotipo==3) {
            $table='asignacion_ser_cliente_a';
        }  
        $this->ModeloCatalogos->updateCatalogo($table,array('activo'=>0,'motivoeliminado'=>$motivo),array('asignacionId'=>$servicioid));
    }
    */
    /*
    function update_cfdi_servicio(){
        $data = $this->input->post();
        $servicioid = $data['id'];
        $serviciotipo = $data['tipo'];
        $cfdi = $data['cfdi'];
        
        if ($serviciotipo==1) {
            $table='asignacion_ser_contrato_a';
        }elseif ($serviciotipo==2) {
            $table='asignacion_ser_poliza_a';
        }elseif ($serviciotipo==3) {
            $table='asignacion_ser_cliente_a';
        }  
        $this->ModeloCatalogos->updateCatalogo($table,array('cdfi'=>$cfdi),array('asignacionId'=>$servicioid));
    }
    */
    /*
    function guardar_pago_tipo_servicio(){
        $data = $this->input->post();
        $data['idpersonal']=$this->idpersonal;
        $this->ModeloCatalogos->Insert('pagos_servicios',$data);
    }
    */
    /*
    function montoservicio(){
        $data = $this->input->post();
        $idcpe=$data['id'];
        $tipo=$data['tipo'];

        $tprecio=0;
        $totalgeneral=0;
        if ($tipo==1) {
            $ventacon=$this->ModeloCatalogos->db10_getselectwheren('asignacion_ser_contrato_a',array('asignacionId'=>$idcpe));
            foreach ($ventacon->result() as $item) {
                $tservicio=$item->tservicio;
            }
            $ventats=$this->ModeloCatalogos->db10_getselectwheren('servicio_evento',array('id'=>$tservicio));
            foreach ($ventats->result() as $item) {
                $tprecio=$item->local;
            }
        }elseif ($tipo==2) {
            $ventacon=$this->ModeloCatalogos->db10_getselectwheren('asignacion_ser_poliza_a',array('asignacionId'=>$idcpe));
            foreach ($ventacon->result() as $item) {
                $tservicio=$item->tservicio;
            }
            $ventats=$this->ModeloCatalogos->db10_getselectwheren('servicio_evento',array('id'=>$tservicio));
            foreach ($ventats->result() as $item) {
                $t_servicio=$item->nombre;
                $tprecio=$item->semi;
            }
        }elseif ($tipo==3) {
            $ventacon=$this->ModeloCatalogos->db10_getselectwheren('asignacion_ser_cliente_a',array('asignacionId'=>$idcpe));
            foreach ($ventacon->result() as $item) {
                $idCliente=$item->clienteId;
                $t_modelo=$item->equipostext;
                $firma=$item->firma;
                $t_servicio_motivo=$item->tserviciomotivo;
                $data['cdfif']=$item->cdfi;
            }
            $ventacon2=$this->ModeloCatalogos->db10_getselectwheren('asignacion_ser_cliente_a_d',array('asignacionId'=>$idcpe));
            $t_modelo='';
            foreach ($ventacon2->result() as $item) {
                $tservicio=$item->tservicio;
                $t_modelo.=$item->serie.' ';
            }
            $ventats=$this->ModeloCatalogos->db10_getselectwheren('servicio_evento',array('id'=>$tservicio));
            foreach ($ventats->result() as $item) {
                $t_servicio=$item->nombre;
                $tprecio=$item->foraneo;
            }
        }

        $resultadoconsumibles=$this->Ventas_model->datosserviciosconsumibles($idcpe,$tipo);
        $resultadorefacciones=$this->Ventas_model->datosserviciosrefacciones($idcpe,$tipo);
        $resultadoaccesorios=$this->Ventas_model->datosserviciosaccesorios($idcpe,$tipo);
        foreach ($resultadoconsumibles->result() as $item) { 
            $costo=$item->costo;
            $totalc=$item->cantidad*$costo;
            $totalgeneral=$totalgeneral+$totalc;
        }
        foreach ($resultadorefacciones->result() as $item) { 
            $totala=$item->cantidad*$item->costo;
            $totalgeneral=$totalgeneral+$totala;
        }
        foreach ($resultadoaccesorios->result() as $item) { 
            $totala=$item->cantidad*$item->costo;
            $totalgeneral=$totalgeneral+$totala;
        }

        $tprecio=($tprecio+$totalgeneral)*1.16;
        echo round($tprecio, 2);
    }
    */
    public function table_get_pagos_servicios(){ 
        $id = $this->input->post('id'); 
        $tip = $this->input->post('tip'); 
        $html='<table class="responsive-table display striped" cellspacing="0"> 
                    <thead> 
                      <tr> 
                        <th>Fecha</th> 
                        <th>Método de pago</th> 
                        <th>Monto</th>
                        <th>Comentario</th> 
                        <!--<th>Comprobante</th>-->
                        <th></th> 
                      </tr> 
                    </thead> 
                    <tbody>'; 
        
           
            $strq = "SELECT p.idpago,p.fecha,f.formapago_text,p.pago,p.comprobante,p.observacion
                FROM pagos_servicios as p
                inner JOIN f_formapago as f on f.id=p.idmetodo
                WHERE p.idservicio=".$id." and p.tipos=".$tip; 

            $query = $this->db->query($strq);
            foreach ($query->result() as $item) { 
            $html.='<tr class="remove_table_pagos_ser_'.$item->idpago.'"> 
                     <td>'.$item->fecha.'</td> 
                     <td>'.$item->formapago_text.'</td> 
                     <td class="montoagregados">'.$item->pago.'</td> 
                     <td>'.$item->observacion.'</td> 
                     <!--<td>'.$item->comprobante.'</td>-->

                     <td>
                        <span 
                            class="new badge red" 
                            style="cursor: pointer;" 
                            onclick="deletepagoser('.$item->idpago.')">
                            <i class="material-icons">delete</i>
                        </span>
                    </td> 
                    </tr>'; 
            } 
        
        $html.='</tbody> 
                  </table>'; 
        echo $html; 
    } 
    function deletepagoser(){
        $id = $this->input->post('id'); 
        $this->ModeloCatalogos->getdeletewheren3('pagos_servicios',array('idpago'=>$id));
    }
    function prefacturaspedientes(){
        $datos = $this->input->post();
        $idcliente=$datos['idcliente'];
        $datosarray= array(
                            'prefactura'=>0,
                            'activo'=>1,
                            'idCliente'=>$idcliente
                            );
        $result= $this->ModeloCatalogos->db10_getselectwheren('vista_ventas',$datosarray);
        echo json_encode($result->result());
    }
    function cliente_equipo_historial_group(){
        $datos = $this->input->post();
        $idcliente=$datos['idcliente'];
        
        $result= $this->ModeloCatalogos->cliente_has_equipo_historial_group($idcliente);
        echo json_encode($result->result());
    }
    function obtencionfacturas(){
        $tipo = $this->input->post('tipo');
        $id = $this->input->post('id');
        $result = $this->ModeloCatalogos->obtencionfacturasventas($tipo,$id);
        $folio='';
        foreach ($result->result() as $item) {
            $folio=$item->Folio;
        }
        echo $folio;
    }
    function cliente_refacciones_cotizacion(){
        $datos = $this->input->post();
        $idcliente=$datos['idcliente'];
        $cotizaciones=$datos['cotizaciones'];
        
        $result= $this->ModeloCatalogos->cliente_refacciones_cotizacion($idcliente,$cotizaciones);
        echo json_encode($result->result());
    }
    /*
    function nuevaCotizacionaccesorios(){
        $data = $this->input->post();

        $arrayac = $data['arrayac'];
        $idventatablee = $data['idventatablee'];
        unset($data['arrayac']);
        if($idventatablee>0){
            $idVenta=intval($idventatablee);
        }else{
            $datosCotizacion = array('atencion' => $data['atencion'],
                                    'correo' => $data['correo'],
                                    'telefono' => $data['telefono'],
                                    'idCliente' => $data['idCliente'],
                                    'tipo' => 3,
                                    //'rentaVentaPoliza' => 1,
                                    'estatus' => 1,
                                    'reg'=>$this->fechaactual,
                                'combinada' => $data['combinada'],
                                'id_personal'=> $this->idpersonal
                                    );
                
            $idVenta = $this->ModeloGeneral->getinsert($datosCotizacion,'ventas');
        }
        $datareaccesorioarray=array();
        $DATAc = json_decode($arrayac);       
        for ($i=0;$i<count($DATAc);$i++) {
            
                
                
                    $detallesCotizacion = array(
                                            'id_venta'=>$idVenta,
                                            'id_equipo'=>$DATAc[$i]->equipo,
                                            'id_accesorio'=>$DATAc[$i]->accesorio,
                                            'serie_bodega'=>$DATAc[$i]->bodega,
                                            'costo'=>$DATAc[$i]->costo,
                                            'cantidad'=>$DATAc[$i]->cantidad
                                        );
                    $datareaccesorioarray[] =$detallesCotizacion;
                    
            
        }
        if(count($datareaccesorioarray)>0){
            $this->ModeloCatalogos->insert_batch('ventas_has_detallesEquipos_accesorios',$datareaccesorioarray);
        }
        echo $idVenta;
    }
    */
    function stockporbodegas(){
        $params = $this->input->post();
        $equipo = $params['equipo'];
        //log_message('error', 'equipo: '.$equipo);

        if($equipo>0){
            $result=$this->Clientes_model->consultarporbodega($equipo);
            $html='';
            foreach ($result->result() as $item) {
                $html.=$item->bodega.': '.$item->total.'<br>';
            }
            
        }else{
            $html='Consultando';
        }
        echo $html;
    }
    function v_ext(){
        $params=$this->input->post();
        $id=$params['id'];
        $tipo=$params['tipo'];
        

        $verifequipos=array();
        $insert_eq=array();
        $insert_acc=array();
        $insert_cone=array();
        $insert_con=array();
        $insert_ref=array();
        $insert_pol=array();

        $equipo=$id;
        $consumibles=$id;
        $refacciones=$id;
        $poliza=0;
        
            
        $resultadoequipos = $this->ModeloCatalogos->db6_getselectwheren('ventasd_has_detallesequipos',array('idVenta'=>$equipo));
        foreach ($resultadoequipos->result() as $item) {
            $cantidad=$item->cantidad;
            $idEquipo=$item->idEquipo;
            $bodega=$this->ModeloCatalogos->obtenerbodega($item->serie_bodega);
            $idbodega=$item->serie_bodega;
            $modelo=$item->modelo;

            //=====================================================
            $stock=$this->Clientes_model->getexistenciaequipos($idEquipo,$idbodega);
            if($stock<$cantidad){
                $verifequipos[]=array('modelo'=>$modelo,'bodega'=>$bodega);
            }

        }
        $resultadoaccesorios = $this->ModeloCatalogos->accesoriosventasd($equipo);
        foreach ($resultadoaccesorios->result() as $item) {
            $cantidad=$item->cantidad;
            $id_accesorio=$item->id_accesorio;
            $bodega=$item->bodega;
            $idbodega=$item->serie_bodega;
            $modelo=$item->nombre;
            //==========================================
            $stock=$this->ModeloGeneral->stockaccesoriobodega($id_accesorio,$idbodega);
            if($stock<$cantidad){
                $verifequipos[]=array('modelo'=>$modelo,'bodega'=>$bodega);
            }

        }
        $resultadoconsumibles = $this->ModeloCatalogos->consumiblesventasd($equipo);
        foreach ($resultadoconsumibles->result() as $item) {
            $cantidad=$item->cantidad;
            $id_consumibles=$item->id_consumibles;
            $idbodega=$item->serie_bodega;
            $modelo=$item->modelo;
            
            //==========================================
            $stock=$this->ModeloGeneral->stockconsumiblesbodega($id_consumibles,$idbodega);
            if($stock<$cantidad){
                $verifequipos[]=array('modelo'=>$modelo,'bodega'=>$bodega);
            }
        }
        $consumiblesventadetalles = $this->ModeloCatalogos->consumiblesventasddetalles($consumibles);
        foreach ($consumiblesventadetalles->result() as $item) {
            $cantidad=$item->cantidad;
            $id_consumibles=$item->idConsumible;
            $bodega=$item->bodega;
            $idbodega=$item->bodegas;
            $modelo=$item->modelo;
            //==========================================
            $stock=$this->ModeloGeneral->stockconsumiblesbodega($id_consumibles,$idbodega);
            if($stock<$cantidad){
                $verifequipos[]=array('modelo'=>$modelo,'bodega'=>$bodega);
            }
        }
        $ventadetallesrefacion = $this->ModeloCatalogos->ventasd_has_detallesrefacciones($refacciones);
        foreach ($ventadetallesrefacion->result() as $item) {
            $cantidad=$item->cantidad;
            $idRefaccion=$item->idRefaccion;
            $modelo=$item->modelo;
            $bodega=$item->bodega;
            $idbodega=$item->serie_bodega;
            
            //==========================================
            $stock=$this->ModeloGeneral->stockrefaccionesbodega($idRefaccion,$idbodega);
            if($stock<$cantidad){
                $verifequipos[]=array('modelo'=>$modelo,'bodega'=>$bodega);
            }
        }
            /*
            $resultadopoliza = $this->ModeloCatalogos->db6_getselectwheren('polizasCreadas',array('id'=>$poliza));
            $resultadopoliza=$resultadopoliza->row();
            $polizaventadetalles = $this->ModeloCatalogos->polizaventadetalles3($poliza);
            */

        
        //===========================================================================
        if(count($verifequipos)==0){
            $resultventa = $this->ModeloCatalogos->getselectwheren('ventasd',array('id'=>$id));
            $resultventa = $resultventa->row();
            unset($resultventa->id);
            $resultventa->reg=$this->fechaactual;
            $resultventa->prefactura=0;
            $resultventa->id_personal=$this->idpersonal;
            $resultventa->e_entrega=0;
            $resultventa->e_entrega_p=null;
            $idVenta_new=$this->ModeloCatalogos->Insert('ventasd',$resultventa);

            $resultadoequipos = $this->ModeloCatalogos->getselectwheren('ventasd_has_detallesequipos',array('idVenta'=>$equipo));
            foreach ($resultadoequipos->result() as $item) {
                    unset($item->id);
                    unset($item->idVenta);
                    $item->idVenta=$idVenta_new;
                    $item->serie_estatus=0;
                    $insert_eq[]=$item;
            }
            $resultadoaccesorios = $this->ModeloCatalogos->getselectwheren('ventasd_has_detallesequipos_accesorios',array('id_venta'=>$equipo));
            foreach ($resultadoaccesorios->result() as $item) {
                    unset($item->id_accesoriod);
                    unset($item->id_venta);
                    $item->id_venta=$idVenta_new;
                    $item->serie_estatus=0;
                    $insert_acc[]=$item;

                    
            }
            $resultadoconsumibles = $this->ModeloCatalogos->getselectwheren('ventasd_has_detallesequipos_consumible',array('idventa'=>$equipo));
            foreach ($resultadoconsumibles->result() as $item) {
                    unset($item->id);
                    unset($item->idventa);
                    $item->idventa=$idVenta_new;
                    $insert_cone[]=$item;

                    $this->ModeloCatalogos->updatestock2('consumibles_bodegas','total','-',$item->cantidad,'consumiblesId',$item->id_consumibles,'bodegaId',$item->bodega);

                    //1 equipo, 2 acesorio, 3 refaccion,4 consumible    
                    $this->ModeloCatalogos->Insert('bitacora_movimientos',array('id_item'=>0,'cantidad'=>$item->cantidad,'modeloId'=>$item->id_consumibles,'tipo'=>4,'entrada_salida'=>0,'personal'=>$this->idpersonal,'descripcion_evento'=>'salida para ventad '.$idVenta_new,'serieId'=>0));
            }
            $consumiblesventadetalles = $this->ModeloCatalogos->getselectwheren('ventasd_has_detallesconsumibles',array('idVentas'=>$consumibles));
            foreach ($consumiblesventadetalles->result() as $item) {
                    unset($item->id);
                    unset($item->idVentas);
                    $item->idVentas=$idVenta_new;
                    $insert_con[]=$item;

                    $this->ModeloCatalogos->updatestock2('consumibles_bodegas','total','-',$item->piezas,'consumiblesId',$item->idConsumible,'bodegaId',$item->bodegas);

                    //1 equipo, 2 acesorio, 3 refaccion,4 consumible    
                    $this->ModeloCatalogos->Insert('bitacora_movimientos',array('id_item'=>0,'cantidad'=>$item->piezas,'modeloId'=>$item->idConsumible,'tipo'=>4,'entrada_salida'=>0,'personal'=>$this->idpersonal,'descripcion_evento'=>'salida para ventad '.$idVenta_new,'serieId'=>0));
                
            }
            $ventadetallesrefacion = $this->ModeloCatalogos->getselectwheren('ventasd_has_detallesrefacciones',array('idVentas'=>$refacciones));
            foreach ($ventadetallesrefacion->result() as $item) {
                
                    unset($item->id);
                    unset($item->idVentas);
                    $item->idVentas=$idVenta_new;
                    $item->serie_estatus=0;
                    $insert_ref[]=$item;
            }
            if(count($insert_eq)>0){
                $this->ModeloCatalogos->insert_batch('ventasd_has_detallesequipos',$insert_eq);
            }
            if(count($insert_acc)>0){
                $this->ModeloCatalogos->insert_batch('ventasd_has_detallesequipos_accesorios',$insert_acc);
            }
            if(count($insert_cone)>0){
                $this->ModeloCatalogos->insert_batch('ventasd_has_detallesequipos_consumible',$insert_cone);
            }
            if(count($insert_con)>0){
                $this->ModeloCatalogos->insert_batch('ventasd_has_detallesconsumibles',$insert_con);
            }
            if(count($insert_ref)>0){
                $this->ModeloCatalogos->insert_batch('ventasd_has_detallesrefacciones',$insert_ref);
            }
            /*
            $resultpoliza = $this->ModeloCatalogos->getselectwheren('polizasCreadas',array('id'=>$poliza));
            if($resultpoliza->num_rows()>0){
                $resultpoliza=$resultpoliza->row();
                unset($resultpoliza->id);
                $idpoliza_new=$this->ModeloCatalogos->Insert('polizasCreadas',$resultpoliza);

                $resultpolizadll = $this->ModeloCatalogos->getselectwheren('polizasCreadas_has_detallesPoliza',array('idPoliza'=>$poliza));
                foreach ($resultpolizadll->result() as $item) {
                
                    unset($item->id);
                    unset($item->idPoliza);
                    $item->idPoliza=$idpoliza_new;
                    $insert_pol[]=$item;
                }

                if(count($insert_pol)>0){
                    $this->ModeloCatalogos->insert_batch('polizasCreadas_has_detallesPoliza',$insert_pol);
                }


                $datavc=array('equipo'=>$idVenta_new,'consumibles'=>$idVenta_new,'refacciones'=>$idVenta_new,'poliza'=>$idpoliza_new,'telefono'=>$resultventa->telefono,'correo'=>$resultventa->correo,'idCliente'=>$resultventa->idCliente,'id_personal'=>$this->idpersonal);
                        $this->ModeloCatalogos->Insert('ventacombinada',$datavc);
                        $this->ModeloCatalogos->updateCatalogo('ventas',array('combinada'=>1),array('id'=>$idVenta_new));

            }
            */




        }

        $datos = array('equipos'=>$verifequipos);
        echo json_encode($datos);
    }

    //====================================================== nuevas funciones ===============================================================
    function nuevaventag(){
        $data = $this->input->post();

        $arrayac = $data['arrayac'];
        unset($data['arrayac']);

        $arrayr = $data['arrayr'];
        unset($data['arrayr']);

        $arrayco = $data['arrayco'];
        unset($data['arrayco']);

        $equiposIndividuales=$data["equiposIndividuales"];
        unset($data["equiposIndividuales"]);

        $tipoCotizacion = $data["tipo"];
                unset($data['tipo']);
        if(isset($data['accesorios'])){
            $accesorios = $data['accesorios'];
            unset($data['accesorios']);
        }
        
        if(isset($data['consumibles'])){
            $consumibles = $data['consumibles'];
            unset($data['consumibles']);
        }
        $datosCotizacion = array('atencion' => $data['atencion'],'correo' => $data['correo'],'telefono' => $data['telefono'],'idCliente' => $data['idCliente'],'tipo' => 0,'estatus' => 1,'reg'=>$this->fechaactual,'id_personal'=> $this->idpersonal);
                
        $idVenta = $this->ModeloGeneral->getinsert($datosCotizacion,'ventasd');
        //===============================================================================
            $datareaccesorioarray=array();
            $DATAc = json_decode($arrayac);       
            for ($i=0;$i<count($DATAc);$i++) {

                        $detallesCotizacion = array(
                                                'id_venta'=>$idVenta,
                                                'id_equipo'=>$DATAc[$i]->equipo,
                                                'id_accesorio'=>$DATAc[$i]->accesorio,
                                                'serie_bodega'=>$DATAc[$i]->bodega,
                                                'costo'=>$DATAc[$i]->costo,
                                                'cantidad'=>$DATAc[$i]->cantidad
                                            );
                        $datareaccesorioarray[] =$detallesCotizacion;

            }
            if(count($datareaccesorioarray)>0){
                $this->ModeloCatalogos->insert_batch('ventasd_has_detallesequipos_accesorios',$datareaccesorioarray);
            }
        //=====================================================================================
            $datarefaccionesarray=array();
            $DATAcr = json_decode($arrayr);       
            for ($i=0;$i<count($DATAcr);$i++) {
                // insertar 
                if ($DATAcr[$i]->toner>0) {
                    
                    $datosConsumible = $this->Consumibles_model->getrefaccionesConPrecios($DATAcr[$i]->toner);
                        $total=$DATAcr[$i]->tonerp*$DATAcr[$i]->piezas;
                        $detallesCotizacion = array('idVentas' => $idVenta,
                                                'idEquipo' => $DATAcr[$i]->equipo,
                                                'serie' => $DATAcr[$i]->serie,
                                                'idRefaccion' => $datosConsumible[0]->id,
                                                'piezas' => $DATAcr[$i]->piezas, 
                                                'modelo' => $datosConsumible[0]->nombre,
                                                'rendimiento' =>'',
                                                'descripcion' => $datosConsumible[0]->observaciones,
                                                'precioGeneral' => $DATAcr[$i]->tonerp,
                                                'totalGeneral' => $total,
                                                'serie_bodega' => $DATAcr[$i]->bodega
                                            );
                        $datarefaccionesarray[] =$detallesCotizacion;
                        
                }
            }
            if(count($datarefaccionesarray)>0){
                $this->ModeloCatalogos->insert_batch('ventasd_has_detallesrefacciones',$datarefaccionesarray);
            }
        //=====================================================================================
            $dataconsumiblesarray=array();
            $DATAcon = json_decode($arrayco);       
            for ($i=0;$i<count($DATAcon);$i++) {

                $datosConsumible = $this->Consumibles_model->getConsumibleConPrecios($DATAcon[$i]->toner);
                        $total=$DATAcon[$i]->piezas*$DATAcon[$i]->precio;
                        $detallesCotizacion = array('idVentas' => $idVenta,
                                                'idEquipo' => $DATAcon[$i]->equipo,
                                                'idConsumible' => $datosConsumible[0]->id,
                                                'piezas' => $DATAcon[$i]->piezas, 
                                                'modelo' => $datosConsumible[0]->modelo,
                                                'rendimiento' =>'',
                                                'descripcion' => $datosConsumible[0]->observaciones,
                                                'precioGeneral' => $DATAcon[$i]->precio,
                                                'totalGeneral' => $total,
                                                'bodegas' => $DATAcon[$i]->bodega,
                                            );
                        $dataconsumiblesarray[]=$detallesCotizacion;
                $this->ModeloCatalogos->updatestock2('consumibles_bodegas','total','-',$DATAcon[$i]->piezas,'consumiblesId',$datosConsumible[0]->id,'bodegaId',$DATAcon[$i]->bodega);
                $this->ModeloCatalogos->Insert('bitacora_movimientos',array('id_item'=>$datosConsumible[0]->id,'modelo'=>$datosConsumible[0]->modelo,'tipo'=>4,'entrada_salida'=>0,'personal'=>$this->idpersonal,'descripcion_evento'=>'Salida para venta '.$idVenta.''));
            }
            if(count($dataconsumiblesarray)>0){
                $this->ModeloCatalogos->insert_batch('ventasd_has_detallesconsumibles',$dataconsumiblesarray);
            }
        //=====================================================================================
            
            
            $DATAeq = json_decode($equiposIndividuales);
                    for ($j=0;$j<count($DATAeq);$j++){
                        $equipoInd=$DATAeq[$j]->equipos;
                        $cantidadeq=$DATAeq[$j]->cantidad;
                        $equipo = $this->Equipos_model->getEquipoPorId($equipoInd);
                        // Dependiendo si es VENTA, RENTA o PÓLIZA, es el precio que se va a tomar
                        $precio = 0;
                        if($tipoCotizacion==1){
                            
                            $precio = $equipo->costo_venta;
                            
                            
                        }
                        else if ($tipoCotizacion==2){
                            $precio = $equipo->costo_renta;
                        }
                        else if ($tipoCotizacion==3){
                            $precio = $equipo->costo_poliza;
                        }
                        
                        
                        //========================================
                            $tipo = 1;
                            $costo_toner = 0;
                            $rendimiento_toner = 0;
                            $costo_unidad_imagen = 0;
                            $rendimiento_unidad_imagen = 0;
                            $costo_garantia_servicio = 0;
                            $rendimiento_garantia_servicio = 0;
                            $costo_pagina_monocromatica = 0;
                            $costo_pagina_color = 0;
                        //========================================
                            $DATAc = json_decode($consumibles);
                            for ($i=0;$i<count($DATAc);$i++) {
                                if ($equipo->id==$DATAc[$i]->equiposelected) {
                                    $idconsumuble=$DATAc[$i]->consumibleselected;
                                    //log_message('error', 'idconsumible '.$idconsumuble);
                                    $resultadoaddcon=$this->ModeloCatalogos->consumibleagregadoselected($idconsumuble,$equipo->id);
                                    foreach ($resultadoaddcon->result() as $item2) {
                                        $tipo = $item2->tipo;
                                        $costo_toner = $item2->poliza;
                                        $rendimiento_toner = $item2->rendimiento_toner;
                                        $costo_unidad_imagen = $item2->costo_unidad_imagen;
                                        $rendimiento_unidad_imagen = $item2->rendimiento_unidad_imagen;
                                        $costo_garantia_servicio = $item2->costo_garantia_servicio;
                                        $rendimiento_garantia_servicio = $item2->rendimiento_garantia_servicio;
                                        $costo_pagina_monocromatica = $item2->costo_pagina_monocromatica;
                                        $costo_pagina_color = $item2->costo_pagina_color;


                                    }
                                    $datachfc["idventa"]=$idVenta;
                                        $datachfc["idequipo"]=$DATAc[$i]->equiposelected;
                                        $datachfc["cantidad"]=$cantidadeq;
                                        $datachfc["id_consumibles"]=$idconsumuble;
                                        $datachfc["costo_toner"]=$costo_toner;
                                        $datachfc["rendimiento_toner"]=$rendimiento_toner;
                                        $datachfc["rendimiento_unidad_imagen"]=$rendimiento_unidad_imagen;
                                        if ($tipoCotizacion==1) {
                                            $this->ModeloCatalogos->Insert('ventasd_has_detallesequipos_consumible',$datachfc);
                                            
                                        }
                                }
                            }
                        //=========================================================
                            $DATAa = json_decode($accesorios);
                            for ($l=0;$l<count($DATAa);$l++) {
                                if ($equipo->id==$DATAa[$l]->equiposelected) {
                                    if ($tipoCotizacion==1) {
                                        $dataas['id_venta']=$idVenta;
                                        $dataas['id_equipo']=$DATAa[$l]->equiposelected;
                                        $dataas['id_accesorio']=$DATAa[$l]->accesorioselected;
                                        $dataas['cantidad']=$cantidadeq;
                                        $dataas['costo']=$this->ModeloCatalogos->costoaccesorio($DATAa[$l]->accesorioselected);
                                        $this->ModeloCatalogos->Insert('ventasd_has_detallesequipos_accesorios',$dataas);
                                    }
                                }
                            }   
                        // Se arma el array para insertar los datos
                        if ($tipoCotizacion==1) {
                            $detallesCotizacion = array('idVenta' => $idVenta,
                                                        'idEquipo' => $equipo->id,
                                                        'cantidad'=>$cantidadeq,
                                                        'modelo' => $equipo->modelo,
                                                        'especificaciones_tecnicas' => $equipo->especificaciones_tecnicas,
                                                        'precio' => $precio,
                                                        'costo_unidad_imagen' => $costo_unidad_imagen,
                                                        'rendimiento_unidad_imagen' => $rendimiento_unidad_imagen,
                                                        'costo_garantia_servicio' => $costo_garantia_servicio,
                                                        'rendimiento_garantia_servicio' => $rendimiento_garantia_servicio,
                                                        'costo_pagina_monocromatica' => $costo_pagina_monocromatica,
                                                        'costo_pagina_color' => $costo_pagina_color,
                                                        'excedente' => $equipo->excedente,
                                                        'tipotoner' => $tipo,
                                                        'costo_toner' => $costo_toner,
                                                        'rendimiento_toner' => $rendimiento_toner,           
                                            );
                            // Se insertan en la BD
                            $this->ModeloCatalogos->Insert('ventasd_has_detallesequipos',$detallesCotizacion);
                        }
                        //$this->Cotizaciones_model->insertarDetallesCotizacionEquipos($detallesCotizacion);
                        
                    }
                    
        //=====================================================================================
        //=====================================================================================
        //=====================================================================================
        //=====================================================================================
        //=====================================================================================
        //=====================================================================================
        echo $idVenta;
    }
}