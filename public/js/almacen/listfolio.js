
var base_url = $('#base_url').val();
var addtr = 0;
$(document).ready(function(){ 
    $('.modal').modal({
            dismissible: true
        });
    $('.chosen-select').chosen({width: "100%"});
    table = $('#tabla_con').DataTable();
    load();

    $('#codigo').keypress(function(event){
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if(keycode == '13'){
            btnretorno();
        }
    });
    $('#select_toner').select2();
}); 
function load(){
    var select_toner =$('#select_toner option:selected').val();
    if(select_toner!='x'){
        load_table();
    }
}
function load_table() {
    var cliente=$('#idcliente option:selected').val();
    var estatus=$('#cf_estatus option:selected').val();
    var finicial=$('#finicial').val();
    var ffinal=$('#ffinal').val();
    var foliotext=$('#foliotext').val();
    var select_toner =$('#select_toner option:selected').val();
    // Destruye la tabla y la crea de nuevo con los datos cargados 
    table.destroy();
    table = $('#tabla_con').DataTable({
        pagingType: 'input',
        "bProcessing": true,
        "serverSide": true,
        search: {
                return: true
            }, 
        "ajax": {
            "url": base_url+"index.php/ListadoFolio/getlistfoliostatus",
            type: "post",
            "data": {
                'cliente':cliente,
                'estatus':estatus,
                'fi':finicial,
                'ff':ffinal,
                'foliotext':foliotext,
                'toner':select_toner
            },
        },
        "columns": [
            {"data": "idcontratofolio"},
            {"data": "modelo"},
            {"data": "parte"},
            {"data": "nombre"},
            {"data": "fechagenera"},
            {"data": "empresa"},
            {"data": "foliotext"},
            {"data": "status",      
                 render:function(data,type,row){  
                    var html= '';
                    if(row.status==1){
                        html='<p class="etiqueta etiquetasalida">Stock Cliente</p>';
                    }else{    
                        html='<p class="etiqueta etiquetaentrada">Retorno</p>';
                    }    
                    return html;
                }
            },
            {"data": "porcentajeretorno",
                render:function(data,type,row){  
                    var html= '';
                    if (row.porcentajeretorno!=null) {
                        html=row.porcentajeretorno+' %';
                    }
                    return html;
                }
            },
            {"data": "fecharetorno"},
            {"data": "comentario"},
            {"data": "porcentajeretorno",
                render:function(data,type,row){  
                    var html= '';
                    if(row.status==1){
                        html='<a class="btn-bmz waves-effect waves-light gradient-45deg-green-teal gradient-shadow agregarretorno_'+row.idcontratofolio+' foliotext_'+row.foliotext+'" data-folio="'+row.foliotext+'" onclick="agregarretorno('+row.idcontratofolio+')"><i class="far fa-arrow-alt-circle-up fa-fw"></i></a>';
                    }
                    return html;
                }
            },
            {"data": "idVentas",
                render:function(data,type,row){
                    var html='';
                   if(row.idVentas>0 || row.idVentasd>0){
                        if(row.idVentas>0){
                            html='<a class="btn-floating green tooltipped" data-position="top" data-delay="50" data-tooltip="Prefactura" onclick="detalle('+row.idVentas+',0)"><i class="material-icons">assignment</i></a>';
                            html+='PROINVEN: '+row.idVentas;
                        }else if(row.idVentasd>0){
                            html='<a class="btn-floating green tooltipped" data-position="top" data-delay="50" data-tooltip="Prefactura" onclick="detalle('+row.idVentasd+',0)"><i class="material-icons">assignment</i></a>';
                            html+='PROINVEN: '+row.idVentasd;
                        }
                        
                        
                    }else{
                        if(row.preren>0){
                            html='<a class="btn-floating amber tooltipped" data-position="top" data-delay="50" data-tooltip="Prefactura" onclick="prefacturah('+row.idrenta+','+row.preren+')"><i class="material-icons">assignment</i></a>'; 
                            if(row.proinren>0){
                                html+='PROINREN: '+row.proinren;     
                            }else{
                                html+='PROINREN: ';  
                            }
                             
                        }else{
                            html='<a class="btn-floating green tooltipped" data-position="top" data-delay="50" data-tooltip="Prefactura" onclick="detaller('+row.idrenta+')"><i class="material-icons">assignment</i></a>';
                        }
                    }
                    if(row.status==2){
                        html+='<a onclick="edit_re('+row.idcontratofolio+')" class="btn-bmz blue edit_re_'+row.idcontratofolio+'" data-folio="'+row.foliotext+'"><i class="fas fa-pencil-alt"></i></a>';
                    }
                    return html; 
                }
            },
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[20,50,100],[20,50,100]]
        
    }).on('draw',function(){
        $('.dataTables_filter').append('<i class="fa fa-search buttonsearch" onclick="searchtable_v()"></i>');
        var foliotext=$('#foliotext').val();
        if(foliotext!=''){
            $('.foliotext_'+foliotext).click();
        }

    });
}
function searchtable_v(){
    var contenedor = document.getElementById('tabla_con_filter');

    // Selecciona el input dentro del contenedor
    var input = contenedor.querySelector('input.form-control-bmz');

    // Obtén el valor del input
    var searchv = input.value;

    // Muestra el valor en la consola
    console.log(searchv);
    //var searchv =$("input[type=search]").val();
    table.search(searchv).draw();
}
function modalretorno(){
    $('#modalRetorno').modal({
      dismissible: false});
    $('#modalRetorno').modal('open');
    $('#codigo').focus();
    $('.mregistro').css('display','block'); 
    //$('.etiquetacodigo').html('');    
}
function btnretorno(){
    
    codigo = $('#codigo').val();
    $.ajax({
        type:'POST',
        url: base_url+"index.php/ListadoFolio/getcodigo",
        data: {codigo:codigo},
        success:function(data){
            array = $.parseJSON(data);  
            if(array.aux == 0){
               swal({   title: "Atención",
                    text: "No se encontro ningun folio.",
                    timer: 2000,
                    showConfirmButton: false
                });
            }else{
                $('.mregistro').css('display','block'); 
                var v_folio =$(".toner_f").hasClass("toner_f_"+array.folio);//verifica si ya existe si no lo agrega
                if(v_folio==false){
                    var html_text = '<tr class="addtr_'+addtr+' toner_f toner_f_'+array.folio+'">\
                                        <td>'+array.idcontratofolio+'<input id="idcontratofolio" type="hidden"  value="'+array.idcontratofolio+'" readonly><input id="idconsumible" type="hidden"  value="'+array.idconsumible+'" readonly></td>\
                                        <td>'+array.modelo+'</td>\
                                        <td>'+array.parte+'</td>\
                                        <td>'+array.folio+'<input id="folioselected" type="hidden"  value="'+array.folio+'" readonly></td>\
                                        <td>'+array.empresa+'</td>\
                                        <td>'+array.reg+'</td>\
                                        <td>\
                                            <p class="range-field">\
                                                <input type="range" id="porcentajeretorno" min="0" max="100" />\
                                            </p>\
                                        </td>\
                                        <td>\
                                        <textarea class="form-control-bmz" id="comentario"></textarea></td>\
                                        <td><a class="btn-floating waves-effect waves-light red accent-2" onclick="deteletr('+addtr+')"><i class="material-icons">clear</i></a></td>\
                                        </tr>';
                    $('.etiquetacodigo').append(html_text);   
                    addtr++; 
                }
            } 
        }
    });
    $('#codigo').val('');
}
function deteletr(id){
    $('.addtr_'+id).remove();
}
function updateretorno(){
    var DATA  = [];
    var TABLA   = $("#tabla_folio tbody > tr");
    TABLA.each(function(){         
        item = {};
        item ["idcontratofolio"] = $(this).find("input[id*='idcontratofolio']").val();
        item ["folioselected"] = $(this).find("input[id*='folioselected']").val();
        item ["idconsumible"] = $(this).find("input[id*='idconsumible']").val();
        item ["porcentajeretorno"] = $(this).find("input[id*='porcentajeretorno']").val();
        item ["comentario"] = $(this).find("textarea[id*='comentario']").val();
        DATA.push(item);
    });
    folios   = JSON.stringify(DATA);
    var data = 'folios='+folios;
    $.ajax({
        type:'POST',
        url: base_url+"index.php/ListadoFolio/updateretorno",
        data: data,
        success: function (data){
            $('#foliotext').val('');
            $('#modalRetorno').modal('close');
            table.ajax.reload();
            swal({ title: "Éxito",
                text: "Se registro correctamente",
                type: 'success',
                showCancelButton: false,
                allowOutsideClick: false,
            });   
            $('.etiquetacodigo').html(''); 
        }
    });
}
function exportar(){
    var cliente=$('#idcliente option:selected').val();
    var estatus=$('#cf_estatus option:selected').val();
    window.location.href = base_url+"index.php/ListadoFolio/exportar/"+cliente+"/"+estatus;
}
function agregarretorno(id){
    var folio=$('.agregarretorno_'+id).data('folio');
    $('#modalRetorno').modal('open');
    $('#codigo').val(folio);
    btnretorno();
}
function detalle(id,tipo){
    if (tipo==0) {
        window.open(base_url+"Prefactura/view/"+id, "Prefactura", "width=780, height=612");
    }else{
        window.open(base_url+"Prefactura/viewc/"+id, "Prefactura", "width=780, height=612");
    }
}
function prefacturah(idrenta,idhistorial){
    window.open(base_url+"RentasCompletadas/viewh/"+idrenta+'/'+idhistorial, "Prefactura", "width=780, height=612");
}
function detaller(id){
    window.open(base_url+"RentasCompletadas/view/"+id, "Prefactura", "width=780, height=612");
}
function edit_re(idc){
    var fol=$('.edit_re_'+idc).data('folio');
    var html='<br><label>indicar el nuevo porcentaje para el folio '+fol+'</label>';
        html+='<p class="range-field"><input type="range" id="porcentajeretorno_new" min="0" max="100" /></p>';
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Desea editar el porcentaje?'+html,
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var por=$('#porcentajeretorno_new').val();
              $.ajax({
                type:'POST',
                url: base_url+"index.php/ListadoFolio/updateretorno_edit",
                data: {
                    idc:idc,
                    por:por
                },
                success: function (data){
                    table.ajax.reload();
                    swal({ title: "Éxito",
                        text: "Se registro correctamente",
                        type: 'success',
                        showCancelButton: false,
                        allowOutsideClick: false,
                    });
                }
            });
              
            },
            cancelar: function () {
                
            }
        }
    });
}