 // Obtener la BASE URL
var base_url = $('#base_url').val();
var formulario_cotizacion;
var tipocosto=0;
var continuar_sin_ser=0;
$(document).ready(function(){
    $('.modal').modal();
	$('.chosen-select').chosen({width: "100%"});
    // Acción del botón Cotizar
    $('#btn_cotizar').on('click', function (){
        $("#btn_cotizar").prop("disabled", true);
        setTimeout(function(){ 
            $("#btn_cotizar").prop("disabled", false);
        }, 5000);
        btn_cotizar();
    });
    /*
    $('#btn_cotizar').on('click', function ()     {   
        $("#btn_cotizar").prop("disabled", true);
        setTimeout(function(){ 
            $("#btn_cotizar").prop("disabled", false);
        }, 5000);
        formulario_cotizacion = $('#formulario_cotizacion');    
        // Validamos que haya elegido que va a cotizar
        if($('#eleccionCotizar').val()=='' || $('#eleccionCotizar').val()==null){
            alertfunction('Atención!','Debe seleccionar que desea Cotizar');  
        }
        // Tipo Equipo
        else if($('#eleccionCotizar').val()==1){   
            var validado = validaciones(1);
            if(validado == 1){
                creacotizacionEquipos();
            }     
        }
        // Tipo Consumible
        else if($('#eleccionCotizar').val()==2){   
            var validado = validaciones(2);
            if(validado == 1){
                var equipoListaConsumibles = [];
                $(".equipoListaConsumibles").each(function() {
                    equipoListaConsumibles.push($(this).val()); 
                });

                var toner = [];
                $(".toner").each(function() {
                    toner.push($(this).val()); 
                });
                var precio = [];
                $(".tonerp").each(function() {
                    precio.push($(this).val()); 
                });

                var piezas = [];
                $(".piezas").each(function() {
                    piezas.push($(this).val()); 
                });

                var datos = formulario_cotizacion.serialize()+'&arrayEquipos='+equipoListaConsumibles+'&arrayToner='+toner+'&arrayPiezas='+piezas+'&arrayPrecio='+precio;
                creaCotizacion(datos);
            }
        }
        // Tipo Refacción
        else if($('#eleccionCotizar').val()==3){
            var validado = validaciones(3);
            if(validado == 1) 
            {
                var equipoListaRefacciones = [];
                $(".equipoListaRefacciones").each(function() {
                    equipoListaRefacciones.push($(this).val()); 
                });

                var seriee = [];
                $(".seriee").each(function() {
                    seriee.push($(this).val()); 
                });

                var refaccion = [];
                $(".refaccion").each(function() {
                    refaccion.push($(this).val()); 
                });

                var piezasRefacion = [];
                $(".piezasRefacion").each(function() {
                    piezasRefacion.push($(this).val()); 
                });
                var refaccionesp =[]
                $(".refaccionesp").each(function() {
                    refaccionesp.push($(this).val()); 
                });

                var datos = formulario_cotizacion.serialize()+'&arrayEquipos='+equipoListaRefacciones+'&seriee='+seriee+'&arrayRefaccion='+refaccion+'&arrayPiezas='+piezasRefacion+'&arrayrprecio='+refaccionesp;
                creaCotizacion(datos);
            }
        }
        // Tipo Póliza
        else if($('#eleccionCotizar').val()==4){
            var validado = validaciones(4);
            if(validado == 1){
            creacotizacionpoliza();
            }
        }else if($('#eleccionCotizar').val()==5){
            var validado = validaciones(5);
            if(validado == 1){
                creacotizacionaccesorio();
            }
        }
        
    });
    */

    // Dependiendo de lo que vaya a cotizar, es lo que mostraremos como opciones
    

    // Acciones para cuando use el SELECT de los equipos para los consumibles
    $("#selectEquiposConsumibles").change(function()    {
        if($("#eleccionCotizar").val()==2)        {
            $('#selectToner').attr('style','display: block');
            $('#selectTonerp').attr('style','display: block');
            $('#inputCantidad').attr('style','display: block');   
        }
    });

    // Acciones para cuando use el SELECT de los equipos para los consumibles
    $("#selectEquiposRefacciones").change(function()    {
        if($("#eleccionCotizar").val()==3)
        {
            $('#selectRefaccion').attr('style','display: block');
            $('#inputCantidadRefaccion').attr('style','display: block');   
        }
    });
    // Agrega mas equipos para poliza
    $('#btn_add_equipo_poliza').click(function(event) {
        seleccionpoliza();
    });

    // Botón para agregar un nuevo consumible a la cotizacion
    $('#botonAgregarConsumible').on('click', function () { 
        agregarConsumible();
    });

    // Botón para eliminar un consumible de la cotizacion
    $('#botonEliminarConsumible').on('click', function (){ 
        eliminarConsumible();
    });

    // Botón para agregar una nueva refaccion a la cotizacion
    $('#botonAgregarRefaccion').on('click', function () { 
        agregarRefaccion();
    });

    // Botón para eliminar una refaccion de la cotizacion
    $('#botonEliminarRefaccion').on('click', function () { 
        eliminarRefaccion();
    });


          // Basic
    $('.dropify').dropify({
        messages: {
            default: 'Arrastra y suelta un archivo aquí o haz clic',
            replace: 'Arrastre y suelte un archivo o haga clic para reemplazar',
            remove:  'Eliminar',
            error:  'Lo sentimos, el archivo es demasiado grande.'
        }
    });
    // Used events
    var drEvent = $('.dropify-event').dropify();
    drEvent.on('dropify.beforeClear', function(event, element){
        return confirm("Do you really want to delete \"" + element.filename + "\" ?");
    });
    drEvent.on('dropify.afterClear', function(event, element){
        alert('File deleted');
    });
    $('#tables_equipos_selected').dataTable( {
            "pageLength": 170,
            "lengthChange": false,
            "columns": [
                { "searchable": false },
                { "searchable": false },
                null,
                { "searchable": false },
                { "searchable": false },
                { "searchable": false },
                { "searchable": false }
                ] 
            });
    //=====================================================
    $('#costo_monocromatico').click(function(event) {
        if ($('#costo_monocromatico').is('[readonly]')) {
            tipocosto=1;
            $('#modaleditor').modal('open');
        }
    });
    $('#costo_color').click(function(event) {
        if ($('#costo_color').is('[readonly]')) {
            tipocosto=2;
            $('#modaleditor').modal('open');
        }
    });
    $('.editarcosto').click(function(event) {
        $.ajax({
            type:'POST',
            url: base_url+'Sistema/solicitarpermiso',
            data: {
                pass: $('#password').val()
                },
                async: false,
                statusCode:{
                    404: function(data){
                        swal("Error", "404", "error");
                    },
                    500: function(){
                        swal("Error", "500", "error"); 
                    }
                },
                success:function(data){
                  if (data==1) {
                    //swal("Editado!", "Precio Editado!", "success");
                    if (tipocosto==1) {
                        $("#costo_monocromatico").attr("readonly", false);
                        $("#costo_monocromatico").trigger("touchspin.updatesettings", {max: 999999999999});
                        $("#costo_monocromatico").trigger("touchspin.updatesettings", {min: 0.01});
                    }else{
                        $("#costo_color").attr("readonly", false);
                        $("#costo_color").trigger("touchspin.updatesettings", {max: 999999999999});
                        $("#costo_color").trigger("touchspin.updatesettings", {min: 0.01});
                    }


                  }else{
                    swal("Error", "No tiene permiso", "error"); 
                  }

                }
            });
        $('#password').val('');
    });
    $('.addtabuladorrendimiento').click(function(event) {
        var equipo = $('#tiporendimientoselectede').val();
        var htmlval='';
        if ($('#tiporendimientoselectedm').val()==1) {
            var vm =$('#volumen_monocromatico').val();
            var tm =$('#total_monocromatico').val();
            htmlval+='<div class="row" style="width: 220px">\
                        <div class="col s6">\
                            <label>Volumen Monocromatico</label>\
                            <input type="number" value="'+vm+'" id="equipo_r_volm" class="form-control-b" style="width: 100px" readonly>\
                        </div>\
                        <div class="col s6">\
                            <label>Costo Monocromatico</label>\
                            <input type="number" value="'+tm+'" id="equipo_r_costm" class="form-control-b" style="width: 100px" readonly>\
                        </div>\
                        </div>';
        }
        if ($('#tiporendimientoselectedc').val()==1) {
            var vc= $('#volumen_color').val();
            var tc= $('#total_color').val();
            var nccc=$('#eg_compra_consumible').is(':checked')==true?1:0;
            if(nccc==1){
                var nccc_l='No compra comcumible color';
            }else{
                var nccc_l='';
            }
            htmlval+='<div class="row" style="width: 220px">\
                        <div class="col s6">\
                            <label>Volumen Color</label>\
                            <input type="number" value="'+vc+'" id="equipo_r_volc" class="form-control-b" style="width: 100px" readonly>\
                        </div>\
                        <div class="col s6">\
                            <label>Costo Color</label>\
                            <input type="number" value="'+tc+'" id="equipo_r_costc" class="form-control-b" style="width: 100px" readonly>\
                        </div>\
                        <div class="col s6">\
                            '+nccc_l+'\
                            <input type="hidden" value="'+nccc+'" id="equipo_r_nccc" readonly>\
                        </div>\
                        </div>';
        }
        if ($('#tiporenta').val()==1) {
          $('.equipo_r_'+equipo).html(htmlval);  
        }
        
    });
    $('#tipo').change(function(event) {
        $('#tiporenta').val(0);
        if($('#tipo option:selected').val()==2){
            $('.solopararentas').show();
            $('.tiporentaselect').show();

            $('.nopararentas').hide();
            $('#ventac').prop('checked',false).change();
            $('#ventar').prop('checked',false).change();
            $('#ventap').prop('checked',false).change();
            $('#ventaa').prop('checked',false).change();

        }else{
            $('.nopararentas').show();
            $('.solopararentas').hide();
            $('.tiporentaselect').hide();
            $('#tiporenta').val(0);
        }
    });
    $('#tiporenta').change(function(event) {
        var tipocoti=$('#tiporenta option:selected').val();
        if (tipocoti==2) {
            $('.costoglobalvaldiv').show();
        }else{
            $('.costoglobalvaldiv').hide();
            $('.costoglobalvaldiv input').val(0);
        }
    });
    $("#eg_rm").TouchSpin({
        min: 0,
        max: 9999999999999999999999999999999999,
        initval: 0.25,
        step: 0.01,
        decimals: 2,
    });
    $("#eg_rvm").TouchSpin({
        min: 0,
        max: 9999999999999999999999999999999999,
        initval: 0,
        step: 1,
        decimals: 0,
    });
    $("#eg_rpem").TouchSpin({
        min: 0,
        max: 9999999999999999999999999999999999,
        initval: 0.25,
        step: 0.01,
        decimals: 2,
    });
    $("#eg_rc").TouchSpin({
        min: 0,
        max: 9999999999999999999999999999999999,
        initval: 0.25,
        step: 0.01,
        decimals: 2,
    });
    $("#eg_rvc").TouchSpin({
        min: 0,
        max: 9999999999999999999999999999999999,
        initval: 0,
        step: 1,
        decimals: 0,
    });
    $("#eg_rpec").TouchSpin({
        min: 0,
        max: 9999999999999999999999999999999999,
        initval: 0.25,
        step: 0.01,
        decimals: 2,
    });
    $(".equipo_cantidad").TouchSpin({
        min: 1
    });
}); 

// Crear la cotización
function creaCotizacion(datos){
    var tipoDocumento = '';
    var idCliente = $('#idCliente').val();
    console.log('idCliente: '+idCliente);
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Cotizaciones/nuevaCotizacion",
        data: datos,
        success: function (response){
            alertfunction('Éxito!','Se ha creado la cotización: '+response);

            if($("#eleccionCotizar").val()==1){
                tipoDocumento = 'documentoHorizontalConBordes';
            }
            else if($("#eleccionCotizar").val()==2){
                tipoDocumento = 'documentoConsumiblesBordes';
            }
            else if($("#eleccionCotizar").val()==3){
                tipoDocumento = 'documentoRefaccionesBordes';
            }
            else if($("#eleccionCotizar").val()==4){
                tipoDocumento = 'documentoPolizas';
            }   
            
            setTimeout(function(){ 
                window.open(base_url+"index.php/Cotizaciones/"+tipoDocumento+"/"+response.trim()); 
            }, 2000);
            
            
            setTimeout(function(){ 
                window.location.href = base_url+"index.php/Cotizaciones/listaCotizacionesPorCliente/"+idCliente; 
            }, 2000);
            
        },
        error: function(response){
            notificacion(1);
        }
    });
}

// Llamar a la función para cargar datos de detalles de la Póliza seleccionada
/*
function funcionDetalles(selectPreDetalle){
    var aux = selectPreDetalle.closest('.rowPolizas');
    cargaPreciosPoliza(aux);
}
*/
// Llamar a la función para cargar datos de detalles de la Póliza seleccionada costos
/*
function funcionDetalles2(selectPreDetalle){
    var aux = selectPreDetalle.closest('.rowPolizas');
    cargaPreciosPoliza2(aux);
}*/


// Llamar a la función para cargar datos de los consumibles del equipo seleccionado
function funcionRefacciones(selectRefaccionDetalle){
    var aux = $('#equipoListaRefacciones').val();
    cargaRefaccionesPorEquipo(aux);
}
function selectcotiza(){
    var selection=$('#cotizar option:selected').val(); 
      if(selection==1){
        $('.select_familia').css('display','none');
        $('.equipos_select').css('display','block');
        //buscador();
      }else if(selection==2){
        $('.select_familia').css('display','block');
        $('.equipos_select').css('display','none');
      }
}

// Saber si una Familia tiene los 3 integrantes correspondientes o no
function verificaFamilia(selectFamilias){
    $.ajax({
        url: base_url+"index.php/Cotizaciones/verificaFamilia/"+selectFamilias.value,
        success: function (response){
            if(response==0){
                var mensaje = 'La familia seleccionada se encuentra vacia, seleccione otra con los 3 integrantes exactos.';
                $('#btn_cotizar').attr('style', 'display: none');
            }
            else if(response==1){
                $('#btn_cotizar').attr('style', 'display: inline-block');
            }
            else if(response==2){
                var mensaje = 'La familia seleccionada tiene registrados mas de 3 integrantes, seleccione otra con los 3 integrantes exactos.';
                $('#btn_cotizar').attr('style', 'display: none');
            }
            else if(response==3){
                var mensaje = 'La familia seleccionada tiene registrados menos de 3 integrantes, seleccione otra con los 3 integrantes exactos.';
                $('#btn_cotizar').attr('style', 'display: none');
            }
            if(response!=1){
                alertfunction('Atención!',mensaje); 
            }
        },
        error: function(response){ 
            notificacion(1);
        }
    }); 
}

// Clonar el DIV de la selección de un Consumible
function agregarConsumible(){
    var contadorConsumibles = $('#contadorConsumibles').val();
    var contadorConsumiblesAux = contadorConsumibles;
    contadorConsumibles++;

    $('#contadorConsumibles').val(contadorConsumibles);
    $('.chosen-select').chosen('destroy');

    var $template = $('#rowConsumibles1'),
                        $clone = $template
                        .clone()
                        .show()
                        .attr('data-conrow', contadorConsumibles)
                        .removeAttr('id')
                        .attr('id',"rowConsumibles"+contadorConsumibles)
                        .insertAfter("#rowConsumibles"+contadorConsumiblesAux)
                        //.append('')
                        $clone.find("select").val("");
    $('.chosen-select').chosen({width: "100%"});
    //=============================================
     var rowcom = $('.collapsible_venta_c .collapsible-body-c > .rowConsumibles');;
     rowcom.each(function(){  
        var conrow = $(this).data('conrow');
        $(this).find("a[id*='botonEliminarConsumible']").attr("onclick","deletevconsu("+conrow+")");
     });
}
function deletevconsu(id){
    $('#rowConsumibles'+id).remove();
 }
// Elimiinar un registro clonado de un consumible
function eliminarConsumible(){
    var contadorConsumibles = $('#contadorConsumibles').val();
    if(contadorConsumibles>1){
        $('#rowConsumibles'+contadorConsumibles).remove();
        contadorConsumibles--;
        $('#contadorConsumibles').val(contadorConsumibles);
    }
    else if(contadorConsumibles==1){
        alertfunction('Atención!','Debe tener por lo menos un consumible seleccionado'); 
    }
}
// Clonar el DIV de la selección de una refaccion
function agregarRefaccion(){
    var contadorRefacciones = $('#contadorRefacciones').val();
    var contadorRefaccionesAux = contadorRefacciones;
    contadorRefacciones++;

    $('#contadorRefacciones').val(contadorRefacciones);
    $('.chosen-select').chosen('destroy');

    var $template = $('#rowRefacciones1'),
                        $clone = $template
                        .clone()
                        .show()
                        .removeAttr('id')
                        .attr('id',"rowRefacciones"+contadorRefacciones)
                        .insertAfter("#rowRefacciones"+contadorRefaccionesAux)
                        //.append('')
                        $clone.find("select").val("");
    $('.chosen-select').chosen({width: "100%"});
}
// Elimiinar un registro clonado de una refaccion
function eliminarRefaccion(){
    var contadorRefacciones = $('#contadorRefacciones').val();
    if(contadorRefacciones>1){
        $('#rowRefacciones'+contadorRefacciones).remove();
        contadorRefacciones--;
        $('#contadorRefacciones').val(contadorRefacciones);
    }
    else if(contadorRefacciones==1){
        alertfunction('Atención!','Debe tener por lo menos una refacción seleccionado'); 
    }
}

// Cargar por AJAX los consumibles que pertenencen a un equipo
function cargaRefaccionesPorEquipo(idEquipo){
    
    
    $.ajax({
        url: base_url+"index.php/Cotizaciones/getDatosRefaccionesPorEquipo/"+idEquipo,
        success: function (response){
            $('.refaccion').html('');
            $('.refaccion').html(response);
            $('.refaccion').trigger("chosen:updated");
        },
        error: function(response){  
            notificacion(1);
        }
    });   
}
function validaciones(eleccionCotizar){
    var retorno = 1;

    if($('#atencion').val().trim()=='' || $('#correo').val().trim()=='' || $('#telefono').val().trim()==''){ 
        alertfunction('Atención!','Debe llenar los campos de "Atención", "Correo" y "Teléfono". De lo contrario no podrá seguir creando una Cotización.'); 
        retorno = 0;
    }else{
        switch (eleccionCotizar) {
            case 1:
                console.log('Validaciones de equipos');
                if($('#tipo').val()=='' || $('#tipo').val()==null){
                    alertfunction('Atención!','Seleccione que tipo de cotización de equipos se realizará.');   
                    retorno = 0;
                }else if($('#cotizar').val()=='' || $('#cotizar').val()==null){
                    alertfunction('Atención!','Seleccione que forma de cotización de equipos se realizará.'); 
                    retorno = 0;
                }else if($('#cotizar').val()==2 && $('#idfamilia').val()==null){
                    retorno = 0;
                }else if($('#cotizar').val()==1){
                    var addtp = 0;
                    $(".nombre:checked").each(function() {
                        $(this).val();
                        addtp++; 
                    });
                    if(addtp==0){
                        alertfunction('Atención!','Favor de selecionar por lo menos un equipos');
                        retorno = 0;
                    }else if(addtp>3){
                       alertfunction('Atención!','Favor de selecionar tres equipos');
                        retorno = 0;
                       
                    }else if(addtp == 3){
                        console.log('Éxito');
                    }
                    var tipo = new Array();
                    //===========================
                    $(".nombre:checked").each(function() {
                       var tipo2 = tipo.push($(this).val());
                    });     
                    console.log(tipo);
                    Array.prototype.unique=function(a){
                    return function(){return this.filter(a)}}(function(a,b,c){return c.indexOf(a,b+1)<0
                    });
                    var resultado = tipo.unique();
                    console.log(resultado.length);   
                    if(resultado.length != 1){
                        alertfunction('Atención!','Seleccione equipos de la misma categoría');
                        retorno = 0;
                    }
                }   
            break;
            case 2:
                console.log('Validaciones de consumibles');
                if($('#opcionesConsumiblesRefacciones').find('.tipoConsumibles').val()=='' || $('#opcionesConsumiblesRefacciones').find('.tipoConsumibles').val()==null){
                    alertfunction('Atención!','Seleccione que tipo de cotización de consumibles se realizará.');
                    retorno = 0;
                }
                else if($('.equipoListaConsumibles').val()=='' || $('.equipoListaConsumibles').val()==null){
                    alertfunction('Atención!','Seleccione para que equipo se busca el consumible.');
                    retorno = 0;
                }
                else if($('.toner').val()=='' || $('.toner').val()==null){
                    alertfunction('Atención!','Seleccione el consumible deseado.');
                    retorno = 0;
                }
                else if($('.piezas').val()=='' || $('.piezas').val()==null || $('.piezas').val()==0){
                    alertfunction('Atención!','Ingrese la cantidad de piezas del consumible.');
                    retorno = 0;
                }
            break;
            case 3:
                console.log('Validaciones de refacciones');
                if($('#opcionesConsumiblesRefacciones').find('.tipoConsumibles').val()=='' || $('#opcionesConsumiblesRefacciones').find('.tipoConsumibles').val()==null){
                    alertfunction('Atención!','Seleccione que tipo de cotización de Refacciones se realizará.');
                    retorno = 0;
                }
                else if($('.equipoListaRefacciones').val()=='' || $('.equipoListaRefacciones').val()==null){
                    alertfunction('Atención!','Seleccione para que equipo se busca la refaccion.');
                    retorno = 0;
                }
                else if($('.refaccion').val()=='' || $('.refaccion').val()==null){
                    alertfunction('Atención!','Seleccione la refacción deseada.');
                    retorno = 0;
                }
                else if($('.piezasRefacion').val()=='' || $('.piezasRefacion').val()==null || $('.piezasRefacion').val()==0){
                    alertfunction('Atención!','Ingrese la cantidad de piezas de la refacción.');
                    retorno = 0;
                }
            break;
            case 4:
                console.log('Validaciones de polizas');
            break;
            case 5:
                 if($("#table_venta_accesorios tbody > tr").length==0){
                    retorno = 0;
                 }
                 break;
            default: 
            break;
        }
    }
    return retorno;
}
function addaccesorios(id){
    $('#modalaccesorios').modal('open');
    $("#costo_monocromatico").TouchSpin({
                min: 0.25,
                max: 0.25,
                initval: 0.25,
                step: 0.01,
                decimals: 2,
            });
    $("#costo_color").TouchSpin({
                min: 2.5,
                max:2.5,
                initval: 2.5,
                step: 0.01,
                decimals: 2,
            });
    

    $.ajax({
        type:'POST',
        url: $('#base_url').val()+'Equipos/accesorios',
        data: {
            id: id,  
        },
        async: false,
        statusCode:{
            404: function(data){
                alertfunction('Atención!','Error 404');
            },
            500: function(data){
                alertfunction('Atención!','Error 500');
            }
        },
        success:function(data){
            console.log(data);
            var array = $.parseJSON(data);
            $('.modaltableaccesorios').html(array.accesorios);
            $('.modaltableconsu').html(array.consumibles);
            $('.tablemaccesorios').DataTable();
            $('.tableconsu').DataTable();
            console.log(array.rendimiento.r_mono)
            $("#volumen_monocromatico").TouchSpin({
                min: array.rendimiento.r_mono,
                max:99999999999999999999999999,
                initval: array.rendimiento.r_mono,
                //step: 0.1,
                decimals: 2,
                boostat: 5,
                maxboostedstep: 10
            }).change(function(event) {
                var vol_mon = $('#volumen_monocromatico').val();
                var costo_mon = $('#costo_monocromatico').val();
                var total_mon = parseFloat(vol_mon)*parseFloat(costo_mon);
                $('#total_monocromatico').val(total_mon);
            });
            $("#volumen_monocromatico").trigger("touchspin.updatesettings", {min: array.rendimiento.r_mono});
            $("#volumen_color").TouchSpin({
                min: 0,
                max:99999999999999999999999999,
                initval: array.rendimiento.r_color,
                //step: 0.1,
                decimals: 2,
                boostat: 5,
                maxboostedstep: 10
            }).change(function(event) {
                var vol_mon = $('#volumen_color').val();
                var costo_mon = $('#costo_color').val();
                var total_mon = parseFloat(vol_mon)*parseFloat(costo_mon);
                $('#total_color').val(total_mon);
            });
            setTimeout(function(){ 
                $("#volumen_color").val(array.rendimiento.r_color);
                $("#volumen_color").trigger("touchspin.updatesettings", {min: 0,initval: array.rendimiento.r_color});
            }, 1000);
            $('#tiporendimientoselectedm').val(array.rendimiento.mono);
            $('#tiporendimientoselectedc').val(array.rendimiento.color);
            $('#tiporendimientoselectede').val(id);
            
        }
    });
}
function addaccesorio(ide,ida,name,bloqueo){
    var accesorio = '<div class="row accesorioadd_'+ide+'_'+ida+'">\
                        <div class="col s8">\
                                  <input type="hidden" id="equiposelected" value="'+ide+'">\
                                  <input type="hidden" id="accesorioselected" value="'+ida+'">\
                                  '+name+'\
                        </div>\
                        <div class="col s2">\
                        <a onclick="deleteaccesorio('+ide+','+ida+')"><i class="material-icons">delete_forever</i></a>\
                        </div>\
                    </div>';
    $('.equipo_s_'+ide).append(accesorio);
}
function addconsu(ide,id,name){
    var accesorio = '<div class="row consumibleadd_'+ide+'_'+id+'">\
                        <div class="col s8">\
                                  <input type="hidden" id="equiposelectedc" value="'+ide+'">\
                                  <input type="hidden" id="consumibleselected" value="'+id+'">\
                                  '+name+'\
                        </div>\
                        <div class="col s2">\
                        <a onclick="deleteconsu('+ide+','+id+')"><i class="material-icons">delete_forever</i></a>\
                        </div>\
                    </div>';
    $('.equipo_c_'+ide).append(accesorio);
}
function deleteaccesorio(ide,ida){
    $('.accesorioadd_'+ide+'_'+ida).remove();
}
function deleteconsu(ide,ida){
    $('.consumibleadd_'+ide+'_'+ida).remove();
}
/*
function buscador(){ 
    var $container = $('.isotope').isotope({ 
        itemSelector: '.gallery-item', 
        layoutMode: 'fitRows' 
      }); 
    $('#filter-text').bind('keyup', function() { 
        var filterValue = this.value.toLowerCase(); 
        isotopeFilter(); 
    }); 
    var filterFns = function() {          
      var kwd = $('#filter-text').val().toLowerCase(); 
      var re = new RegExp(kwd, "gi"); 
      var name = $(this).find('.filter-name').text(); 
      return name.match( re );         
    } 
    function isotopeFilter(){ 
          $container.isotope({ filter: filterFns });     
    } 
} */

// ================================= nuevas funciones ===============================================================================================
var spoliza=1;
function seleccionpoliza(){
    $('#selectEquipos #equipoLista').html();
        var equipospoliza='<div class="col s12 m12 12 polizaselected_row_'+spoliza+'">\
                                <div class="col s10 " style="margin-top:10px;">\
                                    <div class="col s2" style="margin-left: -12px;">\
                                        <label style="position:absolute;left: 43px;">Equipos registrados</label>\
                                        <a class="btn-bmz material-icons prefix" onClick="addserieshisto('+spoliza+')" title="Equipos registrados" style="margin-top: 18px;">format_list_bulleted</a>\
                                        <label for="estado">Equipos</label>\
                                    </div>\
                                    <div class="col s3 margenTopDivChosen" >\
                                        <select id="equipoListas_'+spoliza+'" class="browser-default equipoListaselected chosen-select">\
                                            '+$('#selectEquipos #equipoLista').html()+'\
                                        </select>\
                                    </div>\
                                    <div class="col s2" style="height: 40px;">\
                                        <p><input type="checkbox" id="sinmodelo_'+spoliza+'"  value="1" onclick="sinmodelo('+spoliza+')">\
                                        <label for="sinmodelo_'+spoliza+'">Sin modelo</label></p>\
                                    </div>\
                                    <div class="input-field col s3">\
                                        <input id="polizaserie_'+spoliza+'" type="text" class="validate">\
                                        <label for="polizaserie_'+spoliza+'">Serie</label>\
                                    </div>\
                                    <div class="col s1">\
                                        <a class="btn-floating waves-effect waves-light red lightrn-1 btn tooltipped" data-position="top" data-tooltip="Eliminar"  data-tooltip-id="4f233d2f-8677-72fd-0179-9a198bed22f6" onclick="deleteequipo('+spoliza+')">\
                                            <i class="material-icons">delete</i>\
                                        </a>\
                                    </div>\
                                </div>\
                                <div class="col s12 m12 12">\
                                    <div class="input-field col s4">\
                                        <i class="material-icons prefix">format_list_bulleted</i>\
                                        <label for="estado">Pólizas Existentes</label><br>\
                                        <div class="col s12 margenTopDivChosen">\
                                            <select id="polizasLista_'+spoliza+'" class="browser-default chosen-select " onchange="polizaseleccionada('+spoliza+')" >\
                                                '+$('#selectPolizas #polizasLista').html()+'\
                                            </select>\
                                        </div>\
                                    </div>\
                                    <div class="col s4"  id="selectDetallesPolizas" style="display:none">\
                                        <i class="material-icons prefix">person</i>\
                                        <label for="estado">Modelos de Póliza</label><br>\
                                        <div class="col s12 margenTopDivChosen">\
                                            <select id="detallesPoliza_'+spoliza+'" name="detallesPoliza" class="form-control-bmz browser-default"  onchange="polizalfm('+spoliza+')"    >\
                                            </select>\
                                        </div>\
                                    </div>\
                                    <div class="input-field col s3"  id="selectDetallesPolizas">\
                                        <label for="estado">Cantidad</label><br>\
                                        <div class="col s12 margenTopDivChosen">\
                                            <input type="number" id="detallesPoliza_cantidad_'+spoliza+'" name="detallesPoliza_cantidad" class="form-control-bmz"  value="1" >\
                                        </div>\
                                    </div>\
                                    <div class=" col s4" id="selectDetallesPolizas2">\
                                        <i class="material-icons prefix">$</i>\
                                        <label for="estado">Costo</label><br>\
                                        <div class="col s10 margenTopDivChosen">\
                                            <select id="detallesPolizacosto_'+spoliza+'" name="detallesPolizacosto" class="form-control-bmz browser-default detallesPolizacosto">\
                                                <option value="local">Local</option>\
                                                <option value="semi">Semi</option>\
                                                <option value="foraneo">Foraneo</option>\
                                                <option value="especial">Especial</option>\
                                                <option value="0" class="costocero" style="display:none">0</option>\
                                            </select>\
                                        </div>\
                                    </div>\
                                    <div class="col s1"><br>\
                                        <a class="btn-bmz waves-effect waves-light purple lightrn-1 tooltipped addtablepolizas" data-position="top" data-tooltip="Agregar" onclick="addpolizatable('+spoliza+')">Agregar <i class="far fa-arrow-alt-circle-down"></i></a>\
                                    </div>\
                                </div>\
                                <div class="col s12 m12 12 equipotablepoliza__'+spoliza+'">\
                                    <table class="col s12 m12 12 tablepolizas Highlight striped">\
                                        <tbody>\
                                        </tbody>\
                                    </table>\
                                </div>\
                            </div>';
    $('.addpolizas').append(equipospoliza);
    $('.chosen-select').chosen({width: "100%"});
    spoliza++;
}
function polizalfm(idrow){
    var local = $('#detallesPoliza_'+idrow+' option:selected').data('local');
    var semi = $('#detallesPoliza_'+idrow+' option:selected').data('semi');
    var foraneo  = $('#detallesPoliza_'+idrow+' option:selected').data('foraneo');
    var especial  = $('#detallesPoliza_'+idrow+' option:selected').data('especial');

    $("#detallesPolizacosto_"+idrow+" [value='local']").text('Local ('+local+')');
    $("#detallesPolizacosto_"+idrow+" [value='semi']").text('Semi ('+semi+')');
    $("#detallesPolizacosto_"+idrow+" [value='foraneo']").text('Foraneo ('+foraneo+')');
    $("#detallesPolizacosto_"+idrow+" [value='especial']").text('Especial ('+especial+')');
    $("#detallesPolizacosto_"+idrow).trigger("chosen:updated");
}
function deleteequipo(id){
    $('.polizaselected_row_'+id).remove();
}
function deletepolisadd(id){
    $('.polizaselecteddd_row_'+id).remove();
}
function polizaseleccionada(id){
    $('#detallesPoliza_'+id).html('');
    var polizaid=$('#polizasLista_'+id+' option:selected').val();
    var poliza_visitas=$('#polizasLista_'+id+' option:selected').data('visitas');
    $.ajax({
        url: base_url+"index.php/Cotizaciones/getDatosPolizaporId/"+polizaid,
        success: function (response){
            console.log(response);
            
            $('#detallesPoliza_'+id).html(response);
            //$('#detallesPoliza_'+id).trigger("chosen:updated");
            var equipo = $('#equipoListas_'+id+' option:selected').val();
            setTimeout(function(){ 
                $('#detallesPoliza_'+id+' [data-modelo='+equipo+']').attr('selected',true); 
                //$('#detallesPoliza_'+id).trigger("chosen:updated");
                polizalfm(id);
                setTimeout(function(){
                    var polizaselected=$('#detallesPoliza_'+id+' option:selected').text(); 
                    if (parseInt(equipo)!=parseInt(polizaselected)) {
                        alertfunction('Atención!','No existe poliza para el equipo seleccionado'); 
                    }
                }, 800);
                verificarseriederentas(id);
            }, 800);
            
        },
        error: function(response){
            notificacion(1);
        }
    });
    if(poliza_visitas>1){
        $('#detallesPoliza_cantidad_'+id).prop('readonly',true);
        $('#detallesPoliza_cantidad_'+id).val(1);
    }else{
        $('#detallesPoliza_cantidad_'+id).prop('readonly',false);
    }
}
var spolizadd=1;
function addpolizatable(id){
    var equipo = $('#equipoListas_'+id+' option:selected').val();
    var equipo_text = $('#equipoListas_'+id+' option:selected').text();
    var serie = $('#polizaserie_'+id).val();
    var polizaval = $('#polizasLista_'+id+' option:selected').val();
    var polizatext = $('#polizasLista_'+id+' option:selected').text();
    var polizadval = $('#detallesPoliza_'+id+' option:selected').val();
    var polizadtext = $('#detallesPoliza_'+id+' option:selected').text();
    var polizacval = $('#detallesPolizacosto_'+id+' option:selected').val();
    var polizactext = $('#detallesPolizacosto_'+id+' option:selected').text();
    var polizacantidad =  $('#detallesPoliza_cantidad_'+id+'').val();
    if(polizadval>0){
    var tablerow='<tr class="polizaselecteddd_row_'+spolizadd+'">\
                    <td>\
                        <input type="hidden" id="equipoposelected" value="'+equipo+'">\
                        <input type="hidden" id="serieselected" value="'+serie+'">\
                        <input type="hidden" id="polizaval" value="'+polizaval+'">\
                        '+polizatext+'\
                    </td>\
                    <td>\
                        <input type="hidden" id="polizadval" value="'+polizadval+'">\
                    </td>\
                    <td>\
                        <input type="hidden" id="polizadcant" value="'+polizacantidad+'">\
                        '+polizacantidad+'\
                    </td>\
                    <td>\
                        <input type="hidden" id="polizacval" value="'+polizacval+'">\
                        '+polizactext+'\
                    </td>\
                    <td>\
                        <a onclick="deletepolisadd('+spolizadd+')"style="cursor: pointer;"><i class="material-icons" style="color: red;">delete_forever</i></a>\
                    </td>\
                  </tr>';
        $('.equipotablepoliza__'+id+' .tablepolizas tbody').append(tablerow);
        spolizadd++;
    }
}
/*
function creacotizacionpoliza(){
    var idCliente = $('#idCliente').val();
    //================================
        var DATAp  = [];
        var TABLA   = $(".tablepolizas tbody > tr");
            TABLA.each(function(){         
            item = {};
            item ["equipo"] = $(this).find("input[id*='equipoposelected']").val();
            item ["serie"] = $(this).find("input[id*='serieselected']").val();
            item ["poliza"]   = $(this).find("input[id*='polizaval']").val();
            item ["polizad"]  = $(this).find("input[id*='polizadval']").val();
            item ["cantidad"]  = $(this).find("input[id*='polizadcant']").val();
            item ["costo"]  = $(this).find("input[id*='polizacval']").val();
            DATAp.push(item);
        });
        INFO  = new FormData();
        arrayPolizas   = JSON.stringify(DATAp);
        INFO.append('data', arrayPolizas);
    //================================
    if(TABLA.length>0){
        var datos = $('#formulario_cotizacion').serialize()+'&arrayPolizas='+arrayPolizas
        $.ajax({
            type:'POST',
            url: base_url+"index.php/Cotizaciones/nuevaCotizacionpoliza",
            data: datos,
            success: function (response){
                $.alert({boxWidth: '30%',useBootstrap: false,title: 'Éxito!',content: 'Se ha creado la cotización: '+response});           
                setTimeout(function(){ 
                    window.open(base_url+"index.php/Cotizaciones/documentoPolizas/"+response.trim()); 
                }, 2000);
                setTimeout(function(){ 
                    window.location.href = base_url+"index.php/Cotizaciones/listaCotizacionesPorCliente/"+idCliente; 
                }, 2000);
                
            },
            error: function(response){
                notificacion(1); 
            }
        });
    }else{
        alertfunction('Atención!','agrega una poliza'); 
        $( ".addtablepolizas" ).fadeOut(100).fadeIn(100).fadeOut(100).fadeIn(100).fadeOut(100).fadeIn(100).fadeOut(100).fadeIn(100);
    }
}
/*
function creacotizacionEquipos(){
    var datos = formulario_cotizacion.serialize();
    var tipoprecioequipo = $('#tipoprecioequipo option:selected').val();
    var pasa=1;
    var nombre = [];
    var TABLA   = $("#tables_equipos_selected tbody > tr");
    TABLA.each(function(){  
        if ($(this).find("input[class*='equipo_check']").is(':checked')) {
            item = {};
            item['equipos'] = $(this).find("input[id*='equipo_selected_all']").val();
            item['cantidad'] = $(this).find("input[id*='equipo_cantidad']").val();

            item['volm'] = $(this).find("input[id*='equipo_r_volm']").val()==undefined?0:$(this).find("input[id*='equipo_r_volm']").val();
            item['costm'] = $(this).find("input[id*='equipo_r_costm']").val()==undefined?0:$(this).find("input[id*='equipo_r_costm']").val();
            item['volc'] = $(this).find("input[id*='equipo_r_volc']").val()==undefined?0:$(this).find("input[id*='equipo_r_volc']").val();
            item['costc'] = $(this).find("input[id*='equipo_r_costc']").val()==undefined?0:$(this).find("input[id*='equipo_r_costc']").val();
            item['nccc'] = $(this).find("input[id*='equipo_r_nccc']").val();
             

            var equiposs =  $(this).find("input[id*='equipo_selected_all']").val();
            var equiposconsumiblelength=$('.equipo_c_'+equiposs+' > div').length;
            if (equiposconsumiblelength==0) {
                pasa=0;
            }
            nombre.push(item);
        }else{
            var equiposs = $(this).find("input[class*='equipo_check']").val();
            $('.equipo_c_'+equiposs).html('');
            $('.equipo_s_'+equiposs).html('');   
        }
    });
    aInfoe   = JSON.stringify(nombre);
    //===========================================
    var TABLAa   = $(".equipo_accessoriosselected > div");
    var DATAa  = [];
        TABLAa.each(function(){         
            item = {};
            item ["equiposelected"]   = $(this).find("input[id*='equiposelected']").val();
            item ["accesorioselected"]  = $(this).find("input[id*='accesorioselected']").val();
            DATAa.push(item);
        });
        INFOa  = new FormData();
        aInfoa   = JSON.stringify(DATAa);

    var TABLAc   = $(".equipo_consumibleselected > div");
    var DATAc  = [];
        TABLAc.each(function(){         
            item = {};
            item ["equiposelected"]   = $(this).find("input[id*='equiposelectedc']").val();
            item ["consumibleselected"]  = $(this).find("input[id*='consumibleselected']").val();
            DATAc.push(item);
        });
        INFOc  = new FormData();
        aInfoc   = JSON.stringify(DATAc);
    //===========================================

    datos = datos+'&equiposIndividuales='+aInfoe+'&accesorios='+aInfoa+'&consumibles='+aInfoc+'&tipoprecioequipo='+tipoprecioequipo;
    var tipoDocumento = '';
    var idCliente = $('#idCliente').val();
    console.log('idCliente: '+idCliente);
    if(pasa == 1){
        $.ajax({
            type:'POST',
            url: base_url+"index.php/Cotizaciones/nuevaCotizacionequipos",
            data: datos,
            success: function (response){
                alertfunction('Éxito!','Se ha creado la cotización: '+response); 
                setTimeout(function(){ 
                    window.open(base_url+"index.php/Cotizaciones/documentoHorizontalConBordes/"+response.trim()); 
                }, 2000);
                setTimeout(function(){ 
                    window.location.href = base_url+"index.php/Cotizaciones/listaCotizacionesPorCliente/"+idCliente; 
                }, 2000);
            },
            error: function(response){
                notificacion(1);
            }
        });
    }else{
        alertfunction('Atención!','Falta seleccionar al menos un consumible de un equipo.');
    }
}
*/
function alertfunction(titlea,contenta){
    $.alert({boxWidth: '30%',useBootstrap: false,title: titlea,theme: 'bootstrap',content: contenta});  
}
function obtenercontactos(){
    //$('#atencion').hide(1000);
    $('#atencionselect').show("slow");
    //$('#atencionselect').chosen({width: "100%"});
}
function obtenercontactosss(){
    var atencionpara = $('#atencionselect option:selected').data('atencionpara');
    var email = $('#atencionselect option:selected').data('email');
    var telefono = $('#atencionselect option:selected').data('telefono');
    $('#atencionselect').hide(1000);
    //$('#atencion').show("slow");
    $('#atencion').val(atencionpara).change();
    $('#correo').val(email).change();
    $('#telefono').val(telefono).change();
}
function selectprecior(data){
    var id = $('#refaccion').val();
    //var id = $('#refaccion option:selected').val();
    var aaa = $('#aaa').val();
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Cotizaciones/mostrarselectpreciosr",
        data: {id:id},
        success: function (data){
            var array = $.parseJSON(data); 
             $('.refaccionesp_mostrar').html('');
             $('.refaccionesp_mostrar').html(array.select_option);   
             $('.chosen-select').chosen({width: "100%"}); 

            if(aaa==1){
                 $('.refaccionesp').val(array.espe).trigger('chosen:updated');
            }else{
                 $('.refaccionesp').trigger("chosen:updated");    
            }
            verificarseriederentas();
        },
        error: function(data){ 
            notificacion(1);
        }
    });
}
function sinmodelo(row) {
    setTimeout(function(){
        var sinmodelov = $('#sinmodelo_'+row).is(':checked')==true?1:0; 
        if(sinmodelov==1){
            $('#equipoListas_'+row).html('<option value="113" selected>Sin modelo</option>');
            //$('.chosen-select').select2({width: 'resolve'});

            $('.chosen-select').chosen({width: '100%'});
            setTimeout(function(){
                $('#equipoListas_'+row).val(113);
                $('#equipoListas_'+row).trigger("chosen:updated");
            }, 500);
        }else{
            //$('.select_newmodelo_'+row).show('show');
            //$('.newmodelo_'+row).hide('show');

            var select =$('#selectEquipos #equipoLista').html();
            $('#equipoListas_'+row).html(select);
            //$('.chosen-select').select2({width: 'resolve'});
            $('3equipoListas_'+row).chosen({width: '100%'});
            $('#equipoListas_'+row).trigger("chosen:updated");
        }
    }, 500);
}
function addserieshisto(row){
    $('#modalserieshistorial').modal('open');
    $.ajax({
        type:'POST',
        url: base_url+'index.php/Ventas/cliente_equipo_historial_group',
        data: {
            idcliente:$('#idCliente').val()
        },
        success:function(data){
            var array = $.parseJSON(data);
            var html_table='';
            var infocliente=$('#cliente').val();
            html_table+='<table class="table" id="table-h-e-s">\
                  <thead>\
                    <tr>\
                      <th>Modelo</th>\
                      <th>Serie</th>\
                      <th></th>\
                      <th></th>\
                      <th></th>\
                      <th></th>\
                    </tr>\
                  </thead>\
                <tbody>';
                var seriehiqrow=0;
                array.forEach(function(element) {
                    var contrato='';
                    if(element.tipo==2){
                        var contrato='Contrato: '+element.idcontrato;
                    }
                    html_table+='<tr>\
                                  <td style="font-size:12px;">'+element.modelot+'</td>\
                                  <td style="font-size:12px;">'+element.serie+'</td>\
                                  <td style="font-size:12px;">'+infocliente+'</td>\
                                  <td style="font-size:12px;">'+contrato+'</td>\
                                  <td>\
                                    <a class="btn-bmz dataseries_historico_'+seriehiqrow+'" onclick="m_s_historico('+seriehiqrow+','+row+')" \
                                        data-modelo="'+element.modelot+'" \
                                        data-modeloid="'+element.modelo+'" \
                                        data-serie="'+element.serie+'" \
                                        data-tipo="'+element.tipo+'" \
                                        data-id="'+element.id+'" \
                                    >Agregar</a>\
                                  </td>\
                                  <td>';
                                    if(element.tipo>0){
                                        html_table+='<a class="btn-floating blue tooltipped " onclick="m_s_historico_pre('+seriehiqrow+')" ><i class="material-icons">assignment</i></a>';
                                    }
                        html_table+='</td>\
                                </tr>';
                    seriehiqrow++;
                });
                html_table+='</tbody>\
                </table>';  

            $('.table-h-e-s').html(html_table);
            $('#table-h-e-s').DataTable();

        }
    });
    
}
function m_s_historico(seriehiqrow,row) {
    var modelo  = $('.dataseries_historico_'+seriehiqrow).data('modelo');
    var modeloid= $('.dataseries_historico_'+seriehiqrow).data('modeloid');
    var serie   = $('.dataseries_historico_'+seriehiqrow).data('serie');
    if(modeloid==113){
        $('#sinmodelo_'+row).prop('checked',true);
    }else{
        $('#sinmodelo_'+row).prop('checked',false);
    }
    $('#equipoListas_'+row).html('<option value="'+modeloid+'">'+modelo+'</option>').trigger("chosen:updated");
    $('#polizaserie_'+row).val(serie).change();
    $('#modalserieshistorial').modal('close');
}
function m_s_historico_pre(seriehiqrow) {
    var tipo   = $('.dataseries_historico_'+seriehiqrow).data('tipo');
    var id     = $('.dataseries_historico_'+seriehiqrow).data('id');
    if(tipo==1){
        window.open(base_url+"Prefactura/view/"+id, "Prefactura", "width=780, height=612");
        //window.open(base_url+"Prefactura/viewc/"+id, "Prefactura", "width=780, height=612");
    }
    if(tipo==2){
        window.open(base_url+"RentasCompletadas/view/"+id, "Prefactura", "width=780, height=612");
    }
}

function addh_refacciones_cotizacion(selectRefaccionDetalle) {
    $('#modalrefaccionescotizacion').modal('open');
    $.ajax({
        type:'POST',
        url: base_url+'index.php/Ventas/cliente_refacciones_cotizacion',
        data: {
            idcliente:$('#idCliente').val(),
            cotizaciones:0
        },
        success:function(data){
            var array = $.parseJSON(data);
            var html_table='';
            var infocliente=$('#cliente').val();
            html_table+='<table class="table" id="table-h-r-c">\
                  <thead>\
                    <tr>\
                      <th>Cotizacion</th>\
                      <th>Equipo</th>\
                      <th>Serie</th>\
                      <th>Cantidad</th>\
                      <th>Refaccion</th>\
                      <th></th>\
                      <th></th>\
                    </tr>\
                  </thead>\
                <tbody>';
                var seriehiqrow=0;
                array.forEach(function(element) {
                    html_table+='<tr>\
                                  <td style="font-size:12px;">'+element.idCotizacion+'</td>\
                                  <td style="font-size:12px;">'+element.equipo+'</td>\
                                  <td style="font-size:12px;">'+element.serieequipo+'</td>\
                                  <td style="font-size:12px;">'+element.piezas+'</td>\
                                  <td style="font-size:12px;">'+element.refaccion+'</td>\
                                  <td>\
                                    <a class="btn-bmz datasrefaccion_historico_'+seriehiqrow+'" onclick="m_r_historico('+seriehiqrow+')" \
                                        data-idequipo="'+element.idEquipo+'" \
                                        data-serieequipo="'+element.serieequipo+'" \
                                        data-idrefaccion="'+element.idRefaccion+'" \
                                        data-piezas="'+element.piezas+'" \
                                        data-precio="'+element.precioGeneral+'" \
                                        data-tipo="'+element.tipo+'" \
                                    >Agregar</a>\
                                  </td>\
                                  <td>';
                                    if(element.tipo==0){
                                        html_table+='<a href="'+base_url+'Cotizaciones/documentoRefaccionesBordes/'+element.idCotizacion+'" class="btn-floating blue tooltipped" target="_blank"><i class="material-icons">assignment</i></a>';
                                    }
                                    
                        html_table+='</td>\
                                </tr>';
                    seriehiqrow++;
                });
                html_table+='</tbody>\
                </table>';  

            $('.table-h-r-c').html(html_table);
            $('#table-h-r-c').DataTable({
                  columnDefs: [
                    { targets: [ 0 ],
                      "visible": false
                    }
                  ]
                });

        }
    });
}
function m_r_historico(row){
    var idequipo    = $('.datasrefaccion_historico_'+row).data('idequipo');
    var idrefaccion = $('.datasrefaccion_historico_'+row).data('idrefaccion');
    var piezas      = $('.datasrefaccion_historico_'+row).data('piezas');
    var precio      = $('.datasrefaccion_historico_'+row).data('precio');
    var idDiv      = $('.datasrefaccion_historico_'+row).data('iddiv');
    var serie      = $('.datasrefaccion_historico_'+row).data('serieequipo');
    var tipo      = $('.datasrefaccion_historico_'+row).data('tipo');

    $('#equipoListaRefacciones').val(idequipo).trigger("chosen:updated").change();
    $('#seriee').val(serie);
    setTimeout(function(){
        console.log(idrefaccion);
        if(tipo==0){
            $('#refaccion').val(idrefaccion).trigger("chosen:updated").change(); 
            $('#piezasRefacion').val(piezas); 
        }
    }, 1000);
    setTimeout(function(){
        if(tipo==0){
            $('#refaccionesp').html('<option value="'+precio+'">'+precio+'</option>').trigger("chosen:updated").change(); 
        }
    }, 1250);
    setTimeout(function(){
            //addrefaccion();
    }, 1300);
    $('#modalrefaccionescotizacion').modal('close');
}
function obtenerabsesorios(){
    var equipo = $('#equipo_acce option:selected').val();
    $('#accesorio_acce').html('');
    $.ajax({
        type:'POST',
        url: base_url+'index.php/equipos/getListadoAcesoriosEquipo/'+equipo,
        success:function(data){
            var array = $.parseJSON(data);
            console.log(array);
            var accesorioselected='<option></option>';
            array.data.forEach(function(element) {
                accesorioselected+='<option value="'+element.idcatalogo_accesorio+'" data-costo="'+element.costo+'">'+element.nombre+'</option>';
            });
            $('#accesorio_acce').html(accesorioselected).trigger("chosen:updated");
        }
    });
}
function obtenerabsesorioscosto(){
    var costo = $('#accesorio_acce option:selected').data('costo');
    $('#precio_acce').val(costo);
}
var acc_row=0;
function addaccesoriov(){
    var equipo_acce=$('#equipo_acce option:selected').val();
    var equipo_acce_text=$('#equipo_acce option:selected').text();
    var accesorio_acce=$('#accesorio_acce option:selected').val();
    var accesorio_acce_text=$('#accesorio_acce option:selected').text();
    var precio_acce = $('#precio_acce').val();
    var cantidad_acc = $('#cantidad_acc').val();
    var gara = $('#s_acce_garantia').is(':checked')==true?1:0;
    if(gara==1){
        var tgarantia='<br>Garantia';
    }else{
        var tgarantia='';
    }
    if(equipo_acce>0 && accesorio_acce>0 && cantidad_acc>0 ){
    var html='<tr class="add_venta_acc_'+acc_row+'">\
                <td>\
                  <input type="hidden" id="equi_acce_selected" value="'+equipo_acce+'" class="form-control-bmz" readonly>\
                  '+equipo_acce_text+'\
                </td>\
                <td>\
                  <input type="hidden" id="acce_acce_selected" value="'+accesorio_acce+'" class="form-control-bmz" readonly>\
                  '+accesorio_acce_text+'\
                </td>\
                <td>\
                  <input type="hidden" id="costo_acce_selected" value="'+precio_acce+'" class="form-control-bmz" readonly>\
                  '+precio_acce+'\
                </td>\
                <td>\
                  <input type="hidden" id="cantidad_acce_selected" value="'+cantidad_acc+'" class="form-control-bmz">\
                  '+cantidad_acc+'\
                </td>\
                <td><input type="hidden" id="garantia_acce_selected" value="'+gara+'" class="form-control-bmz">'+tgarantia+'</td>\
                <td>\
                  <a class="btn-floating waves-effect waves-light red accent-2"><i class="material-icons" onclick="deleteventaacce('+acc_row+')">clear</i></a>\
                </td>\
              </tr>';
        $('.tbody_acce').append(html);
    }
    acc_row++;
}
function deleteventaacce(row){
    $('.add_venta_acc_'+row).remove();
}
/*
function creacotizacionaccesorio(vtipo){
    var idCliente = $('#idCliente').val();
    //================================
        var DATAac  = [];
        var TABLAac   = $("#table_venta_accesorios  tbody > tr");
            TABLAac.each(function(){         
            item = {};
            item ["equipo"] = $(this).find("input[id*='equi_acce_selected']").val();
            item ["accesorio"]   = $(this).find("input[id*='acce_acce_selected']").val();
            item ["costo"]   = $(this).find("input[id*='costo_acce_selected']").val();
            item ["cantidad"]  = $(this).find("input[id*='cantidad_acce_selected']").val();
            item ["bodega"]  = $(this).find("input[id*='bodega_acce_selected']").val();
            DATAac.push(item);
        });
        INFO  = new FormData();
        arrayac   = JSON.stringify(DATAac);
        
    //================================
    var datos = $('#formulario_cotizacion').serialize()+'&arrayac='+arrayac+'&combinada='+vtipo;
    if(TABLAac.length>0){
        $.ajax({
            type:'POST',
            url: base_url+"index.php/Cotizaciones/nuevaCotizacionaccesorios",
            data: datos,
            success: function (response){
                idventatablee=parseInt(response);
                        tipoDocumento = 'documentoHorizontalConBordes';
                      
                    
                    setTimeout(function(){ 
                        window.open(base_url+"index.php/Cotizaciones/"+tipoDocumento+"/"+idventatablee); 
                    }, 2000);
                    
                    
                    setTimeout(function(){ 
                        window.location.href = base_url+"index.php/Cotizaciones/listaCotizacionesPorCliente/"+idCliente; 
                    }, 2000);         
            },
            error: function(response){
                notificacion(1);
            }
        });
    }else{
        alertfunction('Atención!','Agregue un concepto');
    }
}
*/
//================================nueva funciones de cotizacion combinada=============================================================
//================================nueva funciones de cotizacion combinada=============================================================
//================================nueva funciones de cotizacion combinada=============================================================

// Llamar a la función para cargar datos de los consumibles del equipo seleccionado
function funcionConsumibles(){
    var equiporow=$('#equipoListaConsumibles').val();
    cargaConsumiblesPorEquipo(equiporow);
}
// Cargar por AJAX los consumibles que pertenencen a un equipo
function cargaConsumiblesPorEquipo(idEquipo){
    //var idDiv = aux.attr("id");
    //var idEquipo = aux.find('.equipoListaConsumibles').val();
    
    $.ajax({
        url: base_url+"index.php/Cotizaciones/getDatosConsumiblesPorEquipo/"+idEquipo,
        success: function (response){
            $('#toner').html('');
            $('#toner').html(response);
            $('#toner').trigger("chosen:updated");
        },
        error: function(response){
            notificacion(1); 
        }
    }); 
}
function funcionConsumiblesprecios(){
    var toner =$('#toner').val();
    cargaConsumiblesPorEquipop(toner);
}
function cargaConsumiblesPorEquipop(toner){
    var aaa = $('#aaa').val();
    $.ajax({
        url: base_url+"index.php/Cotizaciones/getDatosConsumiblesPorEquipop/"+toner,
        success: function (response) 
        {   var array = $.parseJSON(response); 
            $('.tonerp').html('');
            $('.tonerp').html(array.select_option);
            if(aaa==1){
               $('.tonerp').val(array.espe).trigger('chosen:updated');
            }else{
               $('.tonerp').trigger("chosen:updated");    
            }
        },
        error: function(response){
            notificacion(1); 
        }
    }); 
}
var trrowitem=0;
function addcosumibles(){
    trrowitem++;
    var equipo =$('#equipoListaConsumibles option:selected').val();
    var equipo_text =$('#equipoListaConsumibles option:selected').text();
    var toner =$('#toner option:selected').val();
    var toner_text =$('#toner option:selected').text();
    var tonerp =$('#tonerp option:selected').val();
    var tonerp_text =$('#tonerp option:selected').text();
    var piezasc =$('#piezasc').val();
    if(piezasc>0  && toner>0){
        var html='<tr class="tr_deleteitem_'+trrowitem+'">\
                    <td>\
                        <input type="hidden" id="c_equipo" value="'+equipo+'">\
                        <input type="hidden" id="c_toner" value="'+toner+'">\
                        <input type="hidden" id="c_precio" value="'+tonerp+'">\
                        <input type="hidden" id="c_cantidad" value="'+piezasc+'">\
                        '+equipo_text+'\
                    </td>\
                    <td>'+toner_text+'</td>\
                    <td>'+tonerp_text+'</td>\
                    <td>'+piezasc+'</td>\
                    <td><a class="btn-floating red elimina"  onclick="deleteitem('+trrowitem+')" ><i class="material-icons">delete_forever</i></a></td>\
                    </tr>';
        $('.tbody-addconsumibles').append(html);
    }
}
function addrefaccion(){
    trrowitem++;
    
    var equipo =$('#equipoListaRefacciones option:selected').val();
    var equipo_text =$('#equipoListaRefacciones option:selected').text();
    
    var serie =$('#seriee').val();
    
    var refaccion =$('#refaccion option:selected').val();
    var refaccion_text =$('#refaccion option:selected').text();
    
    var piezasr =$('#piezasRefacion').val();


    var precio =$('.selectEquiposRefacciones #refaccionesp option:selected').val();
    var precio_text =$('.selectEquiposRefacciones #refaccionesp option:selected').text();
    var gara = $('#s_ref_garantia').is(':checked')==true?1:0;
    if(gara==1){
        var tgarantia='<br>Garantia';
    }else{
        var tgarantia='';
    }
    if(piezasr>0 && equipo>0 && refaccion>0){
        var html='<tr class="tr_deleteitem_'+trrowitem+'">\
                    <td>\
                        <input type="hidden" id="r_equipo" value="'+equipo+'">\
                        <input type="hidden" id="r_serie" value="'+serie+'">\
                        <input type="hidden" id="r_refaccion" value="'+refaccion+'">\
                        <input type="hidden" id="r_cantidad" value="'+piezasr+'">\
                        <input type="hidden" id="r_precio" value="'+precio+'">\
                        <input type="hidden" id="r_gara" value="'+gara+'">\
                        '+equipo_text+'\
                    </td>\
                    <td>'+serie+'</td>\
                    <td>'+refaccion_text+'</td>\
                    <td>'+piezasr+'</td>\
                    <td>'+precio_text+'</td>\
                    <td>'+tgarantia+'</td>\
                    <td><a class="btn-floating red elimina"  onclick="deleteitem('+trrowitem+')" ><i class="material-icons">delete_forever</i></a></td>\
                    </tr>';
        $('.tbody-addrefacciones').append(html);
    }
}
function deleteitem(item){
    $('.tr_deleteitem_'+item).remove();
}
function btn_cotizar(){
    formulario_cotizacion = $('#formulario_cotizacion'); 
    var datos = formulario_cotizacion.serialize();
    var tipoprecioequipo = $('#tipoprecioequipo option:selected').val();
    var pasa=1;
    var itemsseleccionados=0;
    //===========================================
        var nombre = [];
        var TABLA   = $("#tables_equipos_selected tbody > tr");
        TABLA.each(function(){  
            if ($(this).find("input[class*='equipo_check']").is(':checked')) {
                item = {};
                item['equipos'] = $(this).find("input[id*='equipo_selected_all']").val();
                item['cantidad'] = $(this).find("input[id*='equipo_cantidad']").val();

                item['volm'] = $(this).find("input[id*='equipo_r_volm']").val()==undefined?0:$(this).find("input[id*='equipo_r_volm']").val();
                item['costm'] = $(this).find("input[id*='equipo_r_costm']").val()==undefined?0:$(this).find("input[id*='equipo_r_costm']").val();
                item['volc'] = $(this).find("input[id*='equipo_r_volc']").val()==undefined?0:$(this).find("input[id*='equipo_r_volc']").val();
                item['costc'] = $(this).find("input[id*='equipo_r_costc']").val()==undefined?0:$(this).find("input[id*='equipo_r_costc']").val();
                item['nccc'] = $(this).find("input[id*='equipo_r_nccc']").val();
                item['gara'] = $(this).find("input[id*='s_equipo_garantia']").is(':checked')==true?1:0;
                 

                var equiposs =  $(this).find("input[id*='equipo_selected_all']").val();
                var equiposconsumiblelength=$('.equipo_c_'+equiposs+' > div').length;
                if (equiposconsumiblelength==0) {
                    pasa=0;
                }
                nombre.push(item);
                itemsseleccionados++;
            }else{
                var equiposs = $(this).find("input[class*='equipo_check']").val();
                $('.equipo_c_'+equiposs).html('');
                $('.equipo_s_'+equiposs).html('');   
            }
        });
        aInfoe   = JSON.stringify(nombre);
    //===========================================
        var TABLAa   = $(".equipo_accessoriosselected > div");
        var DATAa  = [];
            TABLAa.each(function(){         
                item = {};
                item ["equiposelected"]   = $(this).find("input[id*='equiposelected']").val();
                item ["accesorioselected"]  = $(this).find("input[id*='accesorioselected']").val();
                DATAa.push(item);
                itemsseleccionados++;
            });
            INFOa  = new FormData();
            aInfoa   = JSON.stringify(DATAa);

        var TABLAc   = $(".equipo_consumibleselected > div");
        var DATAc  = [];
            TABLAc.each(function(){         
                item = {};
                item ["equiposelected"]   = $(this).find("input[id*='equiposelectedc']").val();
                item ["consumibleselected"]  = $(this).find("input[id*='consumibleselected']").val();
                DATAc.push(item);
                itemsseleccionados++;
            });
            INFOc  = new FormData();
            aInfoc   = JSON.stringify(DATAc);
    //===========================================
        var consutable = [];
        var TABLAcon   = $("#table-addconsumibles tbody > tr");
        TABLAcon.each(function(){  
            item = {};
            item['equipos'] = $(this).find("input[id*='c_equipo']").val();
            item['toners'] = $(this).find("input[id*='c_toner']").val();
            item['precio'] = $(this).find("input[id*='c_precio']").val();
            item['cantidad'] = $(this).find("input[id*='c_cantidad']").val();
            consutable.push(item);
            itemsseleccionados++;
        });
        aInfotc   = JSON.stringify(consutable);
    //===========================================
        var refatable = [];
        var TABLAref   = $("#table-addrefacciones tbody > tr");
        TABLAref.each(function(){  
            item = {};
            item['equipos'] = $(this).find("input[id*='r_equipo']").val();
            item['serie'] = $(this).find("input[id*='r_serie']").val();
            item['refa'] = $(this).find("input[id*='r_refaccion']").val();
            item['cantidad'] = $(this).find("input[id*='r_cantidad']").val();
            item['precio'] = $(this).find("input[id*='r_precio']").val();
            item['gara'] = $(this).find("input[id*='r_gara']").val();
            refatable.push(item);
            itemsseleccionados++;
        });
        aInfotre   = JSON.stringify(refatable);
    //===========================================
        var DATAp  = [];
        var TABLAp   = $(".tablepolizas tbody > tr");
        TABLAp.each(function(){         
            item = {};
            item ["equipo"] = $(this).find("input[id*='equipoposelected']").val();
            item ["serie"] = $(this).find("input[id*='serieselected']").val();
            item ["poliza"]   = $(this).find("input[id*='polizaval']").val();
            item ["polizad"]  = $(this).find("input[id*='polizadval']").val();
            item ["cantidad"]  = $(this).find("input[id*='polizadcant']").val();
            item ["costo"]  = $(this).find("input[id*='polizacval']").val();
            DATAp.push(item);
            itemsseleccionados++;
        });
        INFO  = new FormData();
        arrayPolizas   = JSON.stringify(DATAp);
    //================================
        var DATAac  = [];
        var TABLAac   = $("#table_venta_accesorios  tbody > tr");
            TABLAac.each(function(){         
            item = {};
            item ["equipo"] = $(this).find("input[id*='equi_acce_selected']").val();
            item ["accesorio"]   = $(this).find("input[id*='acce_acce_selected']").val();
            item ["costo"]   = $(this).find("input[id*='costo_acce_selected']").val();
            item ["cantidad"]  = $(this).find("input[id*='cantidad_acce_selected']").val();
            item ["bodega"]  = $(this).find("input[id*='bodega_acce_selected']").val();
            item ['gara'] = $(this).find("input[id*='garantia_acce_selected']").val();
            DATAac.push(item);
            itemsseleccionados++;
        });
        INFO  = new FormData();
        arrayac   = JSON.stringify(DATAac);
        
    //================================

    datos = datos+'&arrayequipos='+aInfoe+'&arrayequipoacce='+aInfoa+'&arrayequipoconsu='+aInfoc+'&tipoprecioequipo='+tipoprecioequipo+'&arrayconsu='+aInfotc+'&arrayrefa='+aInfotre+'&arrayPolizas='+arrayPolizas+'&arrayac='+arrayac;
    var tipoDocumento = '';
    var idCliente = $('#idCliente').val();
    console.log('idCliente: '+idCliente);
    if(validar_refaccion_servicio()==1){
        if(formulario_cotizacion.valid()){
            if(pasa == 1){
                if(itemsseleccionados>0){
                    $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Cotizaciones/nuevaCotizaciongeneral",
                        data: datos,
                        success: function (response){
                            alertfunction('Éxito!','Se ha creado la cotización: '+response); 
                            setTimeout(function(){ 
                                window.open(base_url+"index.php/Cotizaciones/documento/"+response.trim()); 
                            }, 2000);
                            setTimeout(function(){ 
                                window.location.href = base_url+"index.php/Cotizaciones/listaCotizacionesPorCliente/"+idCliente; 
                            }, 2000);
                        },
                        error: function(response){
                            notificacion(1); 
                        }
                    });
                }else{
                  alertfunction('Atención!','Falta seleccionar al menos un producto.');  
                }
            }else{
                alertfunction('Atención!','Falta seleccionar al menos un consumible de un equipo.');
            }
        }
    }else{
        var html='Favor de seleccionar un Servicio para la Refacción<br>';
            html+='Para continuar sin servicio se necesita permisos de administrador';
            html+='<input type="text" placeholder="Contraseña" id="contrasena" name="contrasena" class="name form-control"  onpaste="return false;" required/>';
        $.confirm({
                boxWidth: '55%',
                useBootstrap: false,
                icon: 'fa fa-warning',
                title: 'Atención!',
                content: html,
                type: 'red',
                typeAnimated: true,
                buttons:{
                    'Aceptar': function (){
                        var pass=$("input[name=contrasena]").val();
                        if (pass!='') {
                             $.ajax({
                                type:'POST',
                                url: base_url+"index.php/Sistema/solicitarpermiso/",
                                data: {
                                    pass:pass
                                },
                                success: function (response){
                                        var respuesta = parseInt(response);
                                        if (respuesta==1) {
                                            continuar_sin_ser=1;
                                            btn_cotizar();
                                        }else{
                                            notificacion(2);
                                        }
                                },
                                error: function(response){
                                    notificacion(1);
                                }
                            });
                        }else{
                            notificacion(0);
                        }
                    },
                    'Cancelar': function (){
                    }
                }
            });
        setTimeout(function(){
            new MaskedPassword(document.getElementById("contrasena"), '\u25CF');
        },900);
    }
}
/*
$("#eleccionCotizar").change(function()    {
        if($("#eleccionCotizar").val()==2){
            $('#opcionesEquipos').attr('style','display: none');
            //$('#tipoCotizacion').attr('style','display: none');
            $('#select_familia').attr('style','display: none');
            $('#selectPolizas').attr('style','display: none');
            $('#selectDetallesPolizas').attr('style','display: none');
            $('#selectDetallesPolizas2').attr('style','display: none');
            $('#selectEquipos').attr('style','display: none');
            $('#opcionesConsumiblesRefacciones').attr('style','display: block');
            $('.tipoConsumibles').val(1).change();
            $('.tipoConsumibles').trigger("chosen:updated"); 
            $('.selectEquiposConsumibles').attr('style','display: block');/////////////////////////////////////
            $('#selectEquiposRefacciones').attr('style','display: none');
            $('#selectRefaccion').attr('style','display: none');
            $('#inputCantidadRefaccion').attr('style','display: none'); 
            $('.equipos_select').css('display','none');
            //==============================================
            $('.addpolizass').attr('style','display: none');
            $('.addpolizas').attr('style','display: none');
        }
        if($("#eleccionCotizar").val()==3){
            $('#opcionesEquipos').attr('style','display: none');
            //$('#tipoCotizacion').attr('style','display: none');
            $('#select_familia').attr('style','display: none');
            $('#selectPolizas').attr('style','display: none');
            $('#selectDetallesPolizas').attr('style','display: none');
            $('#selectDetallesPolizas2').attr('style','display: none');
            $('#selectEquipos').attr('style','display: none');
            $('#opcionesConsumiblesRefacciones').attr('style','display: block');
            $('.tipoConsumibles').val(1).change();
            $('.tipoConsumibles').trigger("chosen:updated"); 
            $('#selectEquiposConsumibles').attr('style','display: none');
            $('.selectEquiposRefacciones').attr('style','display: block');
            $('#selectToner').attr('style','display: none');
            $('#selectTonerp').attr('style','display: none');
            $('#inputCantidad').attr('style','display: none');
            $('.equipos_select').css('display','none');
            //==============================================
            $('.addpolizass').attr('style','display: none');
            $('.addpolizas').attr('style','display: none');
        }
        if($("#eleccionCotizar").val()==4){
            $('#opcionesEquipos').attr('style','display: none');
            //$('#tipoCotizacion').attr('style','display: none');
            $('#opcionesConsumiblesRefacciones').attr('style','display: none');
            //$('#selectPolizas').attr('style','display: block');
            //$('#selectEquipos').attr('style','display: block');
            $('#selectDetallesPolizas').attr('style','display: block');
            $('#selectDetallesPolizas2').attr('style','display: block');
            $('.equipos_select').css('display','none');
            $('#selectRefaccion').attr('style','display: none');
            $('#inputCantidadRefaccion').attr('style','display: none'); 
            $('#selectToner').attr('style','display: none');
            $('#selectTonerp').attr('style','display: none');
            $('#inputCantidad').attr('style','display: none');
            $('#selectEquiposConsumibles').attr('style','display: none');
            //==============================================
            $('.addpolizass').attr('style','display: block');
            seleccionpoliza();
        }
        if($("#eleccionCotizar").val()==1){
            $('#opcionesConsumiblesRefacciones').attr('style','display: none');
            $('#selectEquipos').attr('style','display: none');
            $('#selectToner').attr('style','display: none');
            $('#inputCantidad').attr('style','display: none');
            $('#selectEquiposConsumibles').attr('style','display: none');
            $('#selectPolizas').attr('style','display: none');
            $('#selectDetallesPolizas').attr('style','display: none');
            $('#selectDetallesPolizas2').attr('style','display: none');
            $('#opcionesEquipos').attr('style','display: block');
            //$('#tipoCotizacion').attr('style','display: block'); 
            $('#selectRefaccion').attr('style','display: none');
            $('#inputCantidadRefaccion').attr('style','display: none'); 
            $('#selectToner').attr('style','display: none');
            $('#selectTonerp').attr('style','display: none');
            $('#inputCantidad').attr('style','display: none'); 
            $('#cotizar').val(1).change();
            $('#cotizar').trigger("chosen:updated"); 
            //==============================================
            $('.addpolizass').attr('style','display: none');
            $('.addpolizas').attr('style','display: none');
        }
        if($("#eleccionCotizar").val()==5){
            $('#opcionesEquipos').attr('style','display: none');
            //$('#tipoCotizacion').attr('style','display: none');
            $('#select_familia').attr('style','display: none');
            $('#selectPolizas').attr('style','display: none');
            $('#selectDetallesPolizas').attr('style','display: none');
            $('#selectDetallesPolizas2').attr('style','display: none');
            $('#selectEquipos').attr('style','display: none');
            
            $('.tipoConsumibles').val(1).change();
            $('.tipoConsumibles').trigger("chosen:updated"); 
            $('#selectEquiposConsumibles').attr('style','display: none');
            
            $('#selectToner').attr('style','display: none');
            $('#selectTonerp').attr('style','display: none');
            $('#inputCantidad').attr('style','display: none');
            $('.equipos_select').css('display','none');
            //==============================================
            $('.addpolizass').attr('style','display: none');
            $('.addpolizas').attr('style','display: none');

            $('.accesorios_selected').attr('style','display: block');
            $('#opcionesConsumiblesRefacciones').attr('style','display: block');
        } 
    });
*/
function eleccionCotizar2(tipo){
    setTimeout(function(){ 
        if(tipo==1){
            if($('#ventae').is(':checked')){
                $('.equipos_select').attr('style','display: block');
            }else{
                $('.equipos_select').attr('style','display: none');
            }
        }
        if(tipo==2){
            if($('#ventac').is(':checked')){
                $('.selectEquiposConsumibles').attr('style','display: block');
            }else{
                $('.selectEquiposConsumibles').attr('style','display: none');
                $('.tbody-addconsumibles').html('');
            }
        }
        if(tipo==3){
            if($('#ventar').is(':checked')){
                //$('#opcionesConsumiblesRefacciones').attr('style','display: block');
                $('.selectEquiposRefacciones').attr('style','display: block');
            }else{
                $('#opcionesConsumiblesRefacciones').attr('style','display: none');
                $('.selectEquiposRefacciones').attr('style','display: none');
                $('.tbody-addrefacciones').html('');
            }
        }
        if(tipo==4){
            if($('#ventap').is(':checked')){
                $('#selectDetallesPolizas').attr('style','display: block');
                $('#selectDetallesPolizas2').attr('style','display: block');
                $('.addpolizass').attr('style','display: block');
                seleccionpoliza();
            }else{
                $('#selectDetallesPolizas').attr('style','display: none');
                $('#selectDetallesPolizas2').attr('style','display: none'); 
                $('.addpolizass').attr('style','display: none'); 
                $('.addpolizas').html('');
            }
        }
        if(tipo==5){
            if($('#ventaa').is(':checked')){
                $('.accesorios_selected').attr('style','display: block');
                //$('#opcionesConsumiblesRefacciones').attr('style','display: block');
            }else{
                $('.accesorios_selected').attr('style','display: none');
                $('#opcionesConsumiblesRefacciones').attr('style','display: none');
                $('.tbody_acce').html('');
            }
        }
    }, 900);
    setTimeout(function(){ 
        var vc_e= $('#ventae').is(':checked');
        var vc_c= $('#ventac').is(':checked');
        var vc_r= $('#ventar').is(':checked');
        var vc_p= $('#ventap').is(':checked');
        var vc_a= $('#ventaa').is(':checked');
        /*
        if(tipo==4){
            if(vc_p){
                $('#ventae').prop({'disabled':true});
                $('#ventac').prop({'disabled':true});
                $('#ventar').prop({'disabled':true});
                //$('#ventap').prop({'disabled':true});
                $('#ventaa').prop({'disabled':true});
            }else{
                $('#ventae').prop({'disabled':false});
                $('#ventac').prop({'disabled':false});
                $('#ventar').prop({'disabled':false});
                //$('#ventap').prop({'disabled':true});
                $('#ventaa').prop({'disabled':false});
            }
        }else{
            if(vc_e || vc_c || vc_r || vc_a){
                $('#ventap').prop({'disabled':true});
            }else{
                $('#ventap').prop({'disabled':false});
            }
        }
        */
    }, 500);
}
function desbloqueocostosceros(){
    swal({   title: "Advertencia!",
          text: "Se necesita contraseña de administrador:",
          type: "input",   
          showCancelButton: true,
          closeOnConfirm: false,
          showLoaderOnConfirm: true,
          animation: "slide-from-top",
          inputPlaceholder: "********",
          inputType:'password' },
          function(inputValue){
            if (inputValue === false) return false;
            if (inputValue === "") {
                swal.showInputError("No se permite campos vacíos!");
                return false
            }else{
                $.ajax({
                    type:'POST',
                    url: base_url+'Cotizaciones/permisoscostocero',
                    data: {
                        pass: inputValue
                    },
                    async: false,
                    statusCode:{
                        404: function(data){
                                swal("Error", "404", "error");
                        },
                        500: function(){
                            swal("Error", "500", "error"); 
                        }
                    },
                    success:function(data){
                      if (data==1) {
                        swal({
                            title: "Confirmación",
                            text: "Contraseña correcta.",
                            timer: 3000,
                            showConfirmButton: false
                        });
                        $('.newstyles').html('<style type="text/css">.costocero{display: block !important;}</style>');
                        $('.desbloqueocostosceros').html('Costos Cero <i class="fa fa-lock-open fa-fw" style="color: green;"></i>');
                      }else{
                        swal.showInputError("No tiene permisos!");
                      }

                    }
                });
            }
            
          });
}
//==================================================================
function tipo2selected(){
    var tipo2=$('.tipo2selected option:selected').val();
    $('#v_ser_tipo').val(0);
    $('#v_ser_id').val(0);
    $('.addinfoser').html('');
    if(tipo2==1){
        $('.soloventas').show('show');
    }else{
        $('.soloventas').hide('show');
    }
}
function servicioaddcliente(){
    $('#modalservicioadd').modal('open');
    var idCliente = $('#idCliente').val();
    $.ajax({
        type:'POST',
        url: base_url+'Generales/servicioaddcliente',
        data: {
            cli: idCliente
        },
        async: false,
        statusCode:{
            404: function(data){
                    swal("Error", "404", "error");
            },
            500: function(){
                swal("Error", "500", "error"); 
            }
        },
        success:function(data){
          $('.servicioaddcliente').html(data);

            if (typeof serieselected   !== 'undefined') {
                //console.log('La variable está definida.');
                serieselected = serieselected.replace(" ", "");
                serieselected = serieselected.replace(" ", "");
                if(serieselected!=''){
                    $('.series_hide_show').hide();
                    $('.'+serieselected).show();
                }
                
            } else {
                //console.log('La variable no está definida.');
            }

        }
    });
}
function selectservent(ser,tipo){
    var ser_fecha=$('.seri_'+tipo+'_'+ser).data('fecha');
    $('#v_ser_tipo').val(tipo);
    $('#v_ser_id').val(ser);
    var htmlser='Servicio: '+ser+' Fecha:'+ser_fecha+' ';
        htmlser+='<button class="b-btn b-btn-danger" onclick="deleteser()"><i class="fas fa-trash"></i></button>';
    $('.addinfoser').html(htmlser);
    $('#modalservicioadd').modal('close');
}
function deleteser(){
    $('#v_ser_tipo').val(0);
    $('#v_ser_id').val(0);
    $('.addinfoser').html('');
}
function validar_refaccion_servicio(){
    var solo_ref=0;
    var pasa=0;
    if($('#ventar').is(':checked')){
        solo_ref=1;
    }
    if($('#ventae').is(':checked')){
        solo_ref=2;
    }
    if($('ventac').is(':checked')){
        solo_ref=2;
    }
    if($('ventap').is(':checked')){
        //solo_ref=2;
    }
    if($('ventaa').is(':checked')){
        solo_ref=2;
    }
    if(solo_ref==1){
        var TABLAp   = $(".tablepolizas tbody > tr");
            
        if(TABLAp.length>0){
            pasa=1;
        }else{
            pasa=0;
        }
    }else{
        pasa=1;
    }
    if(continuar_sin_ser==1){
        pasa=1;
    }

    return pasa;
}
function verificarseriederentas(aux){
    var ser =$('#seriee').val();
    var solo_ref=0;
    if($('#ventar').is(':checked')){
        solo_ref=1;
    }
    if($('#ventae').is(':checked')){
        solo_ref=0;
    }
    if($('ventac').is(':checked')){
        solo_ref=0;
    }
    if($('ventap').is(':checked')){
        //solo_ref=2;
    }
    if($('ventaa').is(':checked')){
        solo_ref=0;
    }
    if(solo_ref==1){
        $.ajax({
            type:'POST',
            url: base_url+"index.php/Generales/verificarseriederentas",
            data: {
                ser:ser
            },
            success: function (response){
                var array = $.parseJSON(response);
                console.log(array);
                if(array.length>0){
                    //$("#refaccionesp option.esp").val(0).text("Especial(0)").prop("selected", true);
                    $('#detallesPolizacosto_'+aux+' .costocero').show();
                    $('#detallesPolizacosto_'+aux+' option.costocero').prop("selected", true);
                }
            },
            error: function(response){
                
            }
        });
                                        
    }
}