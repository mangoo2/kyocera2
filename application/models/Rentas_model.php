<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class Rentas_model extends CI_Model 
{
    public function __construct() 
    {
        parent::__construct();
        $this->DB4 = $this->load->database('other4_db', TRUE); 
        $this->DB9 = $this->load->database('other9_db', TRUE);
        $this->DB10 = $this->load->database('other10_db', TRUE);
        //$this->DB11 = $this->load->database('other11_db', TRUE);
        $this->DB18 = $this->load->database('other18_db', TRUE);
        $this->DB15 = $this->load->database('other15_db', TRUE);
        $this->fechahoy = date('Y-m-d');
    }

    public function alta($tabla,$dato){
    $this->db->insert($tabla,$dato);   
    return $this->db->insert_id();
    } 

    public function update($tabla,$data,$value,$id) {
    $this->db->set($data);
    $this->db->where($value, $id);
    $this->db->update($tabla);
    return $id;
    }
    public function getPersona($id){
        $sql = "SELECT * FROM personal WHERE personalId=$id";
        $query = $this->db->query($sql);
        return $query->result();
    } 
    /*
    public function getListadocontratosRentasIncompletas(){
        $sql = "SELECT
                r.id, 
                c.empresa, 
                r.atencion,
                r.telefono,
                r.area,
                r.plazo, 
                r.estatus, 
                r.idCotizacion,
                r.prefactura,r.estatus
                FROM rentas as r
                INNER JOIN clientes as c on c.id = r.idCliente 
                WHERE r.estatus!=0 and 
                (SELECT COUNT(*) from rentas_has_detallesEquipos as re WHERE re.idRenta = r.id )=(SELECT COUNT(*) from rentas_has_detallesEquipos as re WHERE re.idRenta = r.id and re.serie_estatus = 1 )";
        $query = $this->db->query($sql);
        return $query->result();
    }
    */
    //========================
    function getListadocontratosRentasIncompletas($params){
        $tipo=$params['tipo'];
        $columns = array( 
            0=>'r.id', 
            1=>'co.idcontrato', 
            2=>'c.empresa', 
            3=>'r.atencion',
            4=>'co.folio',
            5=>'r.area',
            6=>'r.plazo', 
            7=>'r.estatus', 
            8=>'r.idCotizacion',
            9=>'r.prefactura',
            10=>'r.estatus'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->DB4->select($select);
        $this->DB4->from('rentas r');
        $this->DB4->join('clientes c', 'c.id = r.idCliente ');
        $this->DB4->join('contrato co', 'co.idRenta = r.id and co.estatus=1','left');
        
        
        if($tipo==1){
            $this->DB4->where('r.estatus !=0');
            //$this->DB4->where('(co.ejecutivo =0 or co.ejecutivo is null )');
            $this->DB4->where('co.idcontrato is null');
        }elseif($tipo==2){
            $this->DB4->where('r.estatus !=0');
        }else{
            $this->DB4->where('r.estatus =0');
        }
            
        //$this->db->where('r.estatus =0');
        if($tipo==2){
            $this->DB4->where('(SELECT COUNT(*) from rentas_has_detallesEquipos as re WHERE re.idRenta = r.id )!=(SELECT COUNT(*) from rentas_has_detallesEquipos as re WHERE re.idRenta = r.id and re.serie_estatus = 1 )');
        }else{
            $this->DB4->where('(SELECT COUNT(*) from rentas_has_detallesEquipos as re WHERE re.idRenta = r.id )=(SELECT COUNT(*) from rentas_has_detallesEquipos as re WHERE re.idRenta = r.id and re.serie_estatus = 1 )');
        }
        

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->DB4->group_start();
            foreach($columns as $c){
                $this->DB4->or_like($c,$search);
            }
            $this->DB4->group_end();  
        }            
        $this->DB4->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->DB4->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->DB4->get();
        // print_r($query); die;
        return $query;
    }
    public function total_getListadocontratosRentasIncompletas($params){
        $tipo=$params['tipo'];
        $columns = array( 
            0=>'r.id', 
            1=>'c.empresa', 
            2=>'r.atencion',
            3=>'co.folio',
            4=>'r.area',
            5=>'r.plazo', 
            6=>'r.estatus', 
            7=>'r.idCotizacion',
            8=>'r.prefactura',
            9=>'r.estatus',
            10=>'co.idcontrato'
        );
        
        $this->DB4->select('COUNT(*) as total');
        $this->DB4->from('rentas r');
        $this->DB4->join('clientes c', 'c.id = r.idCliente ');
        $this->DB4->join('contrato co', 'co.idRenta = r.id and co.estatus=1','left');
        if($tipo==1){
            $this->DB4->where('r.estatus !=0');
            //$this->DB4->where('(co.ejecutivo =0 or co.ejecutivo is null )');
            $this->DB4->where('co.idcontrato is null');
        }elseif($tipo==2){
            $this->DB4->where('r.estatus !=0');
        }else{
            $this->DB4->where('r.estatus =0');
        }
            
        //$this->db->where('r.estatus =0');
        if($tipo==2){
            $this->DB4->where('(SELECT COUNT(*) from rentas_has_detallesEquipos as re WHERE re.idRenta = r.id )!=(SELECT COUNT(*) from rentas_has_detallesEquipos as re WHERE re.idRenta = r.id and re.serie_estatus = 1 )');
        }else{
            $this->DB4->where('(SELECT COUNT(*) from rentas_has_detallesEquipos as re WHERE re.idRenta = r.id )=(SELECT COUNT(*) from rentas_has_detallesEquipos as re WHERE re.idRenta = r.id and re.serie_estatus = 1 )');
        }

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->DB4->group_start();
            foreach($columns as $c){
                $this->DB4->or_like($c,$search);
            }
            $this->DB4->group_end();  
        }            
        
        $query=$this->DB4->get();
        return $query->row()->total;
    }
    //========================================================================
    public function getregistro($tabla,$value){
        $sql = "SELECT * FROM $tabla WHERE $value";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getcontratos_id($id){
        $sql = "SELECT r.id, c.empresa, re.id as idRD, re.idequipo,re.modelo,r.area,r.plazo,re.costo_pagina_monocromatica as 'costoporclick',
                re.excedente, re.cantidad, re.fecha_fin_susp,re.estatus_suspendido
                FROM rentas as r
                INNER JOIN clientes as c on c.id = r.idCliente 
                INNER JOIN rentas_has_detallesEquipos as re ON re.idRenta=r.id
                WHERE r.id=$id and re.estatus=1";
        $query = $this->db->query($sql);
        return $query->result();
    } 

    public function insertarRenta($dato){
        $this->db->insert('rentas',$dato);   
        return $this->db->insert_id();
    } 

    public function insertarDetallesRentasEquipos($dato){
        $this->db->insert('rentas_has_detallesEquipos',$dato);   
        return $this->db->insert_id();
    }
    public function actualizaRenta($data,$id){
        $this->db->set($data);
        $this->db->where('id', $id);
        return $this->db->update('rentas');
    }
    public function idcliente_idrenta($id){
        $sql = "SELECT idCliente FROM rentas WHERE id=$id";
        $query = $this->db->query($sql);
        return $query->result();
    }
    
    public function puestocontacto($id){
       $sql = "SELECT puesto_contacto FROM clientes WHERE id=$id";
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function telefonocliente($id){
       $sql = "SELECT tel_local FROM cliente_has_telefono WHERE idCliente = $id LIMIT 1";
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function contactocliente($id){
       $sql = "SELECT persona_contacto FROM cliente_has_persona_contacto WHERE idCliente = $id LIMIT 1";
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function direccioncliente($id){
       $sql = "SELECT direccion FROM clientes WHERE id = $id";
        $query = $this->DB10->query($sql);
        return $query->result();
    }
    public function existecontrato($id){
        $sql = "SELECT * FROM contrato WHERE idRenta = '$id' and estatus>0 ";
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function personacontrato($id){
        $sql = "SELECT * FROM contrato WHERE idRenta = $id";
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function contratoc($id){
        $sql = "SELECT con.*,per.nombre as ejecutivo,c.empresa
                FROM rentas AS r
                INNER JOIN clientes AS c ON c.id = r.idCliente
                INNER JOIN contrato AS con ON con.idRenta = r.id 
                INNER JOIN personal AS per ON per.personalId = con.personalId
                WHERE con.idcontrato=$id";
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function contratocconsumibles($contrato){
        $sql = "SELECT contf.idcontratofolio,cons.modelo,contf.foliotext 
                FROM contrato as cont 
                INNER JOIN contrato_folio as contf on contf.idrenta=cont.idRenta 
                inner join consumibles as cons on cons.id=contf.idconsumible 
                WHERE 
                cont.idcontrato=$contrato and (contf.serviciocId=0 or contf.serviciocId is null )
                ";
        $query = $this->db->query($sql);
        return $query;
    }
    public function clientesrenta(){
        $sql = "SELECT c.id,c.empresa FROM rentas AS a
                INNER JOIN clientes AS c ON c.id = a.idCliente
                WHERE a.estatus = 2
                GROUP BY a.idCliente      
                ";
        $query = $this->db->query($sql);
        return $query->result();        
    }
    public function contratorentaclientex($idCliente)    {
        $sql = "SELECT con.idcontrato,per.nombre,con.fechainicio,con.vigencia,con.folio,rhd.id AS id_equipo,rhd.modelo,asre.serieId,
                    sep.serie FROM rentas AS r
                INNER JOIN clientes AS c ON c.id = r.idCliente
                INNER JOIN contrato AS con ON con.idRenta = r.id 
                INNER JOIN personal AS per ON per.personalId = con.personalId
                INNER JOIN rentas_has_detallesEquipos AS rhd ON rhd.idRenta = r.id 
                inner join asignacion_series_r_equipos as asre on asre.id_equipo=rhd.id
                inner JOIN series_productos as sep on sep.serieId=asre.serieId
                WHERE r.idCliente = $idCliente";
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function contratorentacliente($idCliente)    {
        $sql = "SELECT con.idcontrato,per.nombre,con.fechainicio,con.vigencia,con.folio,con.estatus,perde.nombre as personald, perde.apellido_paterno,con.rentadeposito,con.rentacolor,con.precio_c_e_color,
                ar.continuarcontadores,ar.conti_cont_fecha,con.idRenta,con.permt_continuar
                FROM rentas AS r
                INNER JOIN clientes AS c ON c.id = r.idCliente
                INNER JOIN contrato AS con ON con.idRenta = r.id 
                INNER JOIN personal AS per ON per.personalId = con.personalId
                left join personal AS perde ON perde.personalId = con.personalelimino
                left join alta_rentas as ar on ar.idcontrato=con.idcontrato
                WHERE r.idCliente = $idCliente and con.estatus=1 group by con.idcontrato ";
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function nextservicio($idcontrato,$serieId,$fecha)    {
        $sql = "SELECT 
                    asigd.asignacionIde,
                    asigd.asignacionId,
                    asigd.fecha,
                    cont.speriocidad,
                    DATE_ADD(asigd.fecha, interval cont.speriocidad month) as fechanext 
                FROM asignacion_ser_contrato_a as asig 
                INNER JOIN asignacion_ser_contrato_a_e as asigd on asigd.asignacionId=asig.asignacionId 
                INNER JOIN contrato as cont on cont.idcontrato=asig.contratoId 
                WHERE asig.activo=1 AND asigd.activo=1 AND asig.contratoId=$idcontrato and asigd.serieId=$serieId and asigd.fecha<='$fecha'
                GROUP BY asigd.fecha ORDER BY asigd.fecha DESC LIMIT 1";
        $query = $this->db->query($sql);
        return $query;
    }
    public function contratorentaclienteeje($idCliente,$ejecutivo,$estatus,$con){
        if ($ejecutivo>0) {
            $whereeje=" and con.ejecutivo=".$ejecutivo;
        }else{
            $whereeje="";
        }
        if($estatus==1){ //vigentes
            $whereestatus="and con.estatus=1 and ar.ordenc_vigencia >= '$this->fechahoy'";   
        }else if($estatus==2){ //concluidos
            $whereestatus="and con.estatus=1 and ar.ordenc_vigencia <'$this->fechahoy'"; 
        }else if($estatus==3){//finalizados
            $whereestatus="and con.estatus=2";    
        }else{
            $whereestatus="";
        }
        if($con>0){
            $w_con=" and con.idcontrato='$con' ";
        }else{
            $w_con="";
        }
        $sql = "SELECT con.idcontrato,per.nombre,con.fechainicio,con.vigencia,con.folio,con.estatus,arpex.id as idarpex
                FROM rentas AS r
                INNER JOIN clientes AS c ON c.id = r.idCliente
                INNER JOIN contrato AS con ON con.idRenta = r.id 
                INNER JOIN personal AS per ON per.personalId = con.personalId
                left join alta_rentas as ar on ar.idcontrato=con.idcontrato
                left join alta_rentas_prefactura_cex as arpex on arpex.idcontrato=con.idcontrato and arpex.activo=1
                WHERE /*r.estatus>0 and*/ con.estatus>0 $whereestatus  $w_con and r.idCliente = $idCliente $whereeje /**/group by con.idcontrato";
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function equiposrentas($idcontrato){
        /* original
        $sql = "SELECT 
                    con.idcontrato,
                    con.idRenta,
                    rhde.id,
                    rhde.idEquipo,
                    rhde.modelo,
                    asre.serieId,
                    sep.serie,
                    rhde.cantidad,
                    equi.categoriaId,
                    cteq.tipo,
                    rhde.fecha_fin_susp,
                    rhde.estatus,
                    asre.activo
                FROM contrato as con
                inner join rentas_has_detallesEquipos as rhde on rhde.idRenta=con.idRenta
                inner join asignacion_series_r_equipos as asre on asre.id_equipo=rhde.id
                inner JOIN series_productos as sep on sep.serieId=asre.serieId
                inner join equipos as equi on equi.id=rhde.idEquipo
                inner join categoria_equipos as cteq on cteq.id=equi.categoriaId
                WHERE con.idcontrato=$idcontrato ";
        */
        $sql = "SELECT * from ( 
                    SELECT 
                        con.idcontrato,
                        con.idRenta,
                        rhde.id,
                        rhde.idEquipo,
                        rhde.modelo,
                        asre.serieId,
                        sep.serie,
                        sep.usado,
                        rhde.cantidad,
                        equi.categoriaId,
                        cteq.tipo,
                        asre.fecha_fin_susp,
                        rhde.estatus,
                        asre.activo,
                        asre.contadores,
                        asre.orden,
                        asre.ubicacion,
                        ren.idCliente,
                        (
                            select are.precio_c_e_color 
                            from alta_rentas_equipos as are  
                            where are.id_equipo=rhde.id and are.serieId=sep.serieId
                            GROUP by are.serieId limit 1
                        ) as precio_c_e_color,
                        asre.des_click,
                        asre.des_escaneo,
                        (
                            SELECT cc.descripcion FROM centro_costos_equipos as cceq
                            inner join centro_costos as cc on cc.id=cceq.idcentroc
                            WHERE cceq.rowequipo=rhde.id and cceq.serieid=asre.serieId and cceq.activo=1 limit 1
                        ) as centro_costos_inf,
                        asre.poreliminar,
                        asre.bodega_or,
                        DATE_ADD(con.fechainicio, INTERVAL (CASE 
                            WHEN con.vigencia = 1 THEN 12
                            WHEN con.vigencia = 2 THEN 24
                            WHEN con.vigencia = 3 THEN 36
                            ELSE con.vigencia -- Mantener el valor original si no coincide
                        END) MONTH) AS fecha_final,
                        con.permt_continuar
                    FROM contrato as con
                    inner join rentas_has_detallesEquipos as rhde on rhde.idRenta=con.idRenta
                    inner join asignacion_series_r_equipos as asre on asre.id_equipo=rhde.id
                    inner JOIN series_productos as sep on sep.serieId=asre.serieId
                    inner join equipos as equi on equi.id=rhde.idEquipo
                    inner join categoria_equipos as cteq on cteq.id=equi.categoriaId
                    inner join rentas as ren on ren.id=con.idRenta
                    WHERE 
                        con.idcontrato=$idcontrato and 
                        asre.activo=1 
                        GROUP BY rhde.idEquipo,asre.serieId
                ) as datos 
                ORDER BY orden ASC
                ";
                //log_message('error', $sql);
        $query = $this->db->query($sql);
        return $query->result();
    }
    function produccionpromedio($equipo,$serie,$tipo,$ppfi,$ppff){ //checar este si se puede quitar
        if($ppfi!=''){
            $where_ppfi=" and arp.periodo_inicial >='$ppfi' ";
        }else{
            $where_ppfi='';
        }
        if($ppff!=''){
            $where_ppff=" and arp.periodo_final <='$ppff' ";
        }else{
            $where_ppff='';
        }
        $sql = "SELECT 
                AVG(arpd.produccion) as produccion 
                FROM alta_rentas_prefactura_d as arpd
                inner join alta_rentas_prefactura as arp on arp.prefId=arpd.prefId
                where arpd.productoId=$equipo and arpd.serieId=$serie and arpd.tipo=$tipo $where_ppfi $where_ppff";
        $query = $this->db->query($sql);
        $produccion='';
        foreach ($query->result() as $item) {
            $produccion=round($item->produccion);
        }
        return $produccion;
    }
    public function equiposrentas2($idrenta){
        $sql = "SELECT rhde.id, 
                       rhde.idEquipo, 
                       rhde.modelo, 
                       asre.serieId, 
                       sep.serie,
                       sep.usado, 
                       sep.bodegaId,
                       rhde.cantidad, 
                       equi.categoriaId, 
                       cteq.tipo, 
                       equi.pag_monocromo_incluidos, 
                       equi.pag_color_incluidos, 
                       equi.excedente, 
                       equi.excedentecolor,
                       equi.costo_renta,
                       rhde.idRenta,
                       asre.estatus_suspendido,
                       asre.fecha_fin_susp,
                       asre.contadores,
                       asre.orden,
                       asre.ubicacion,
                       asre.comenext,
                       asre.contadorinfo,
                       asre.id_asig,
                       asre.des_click,
                       asre.des_escaneo,
                       asre.hoja_estado,
                       ceq.rentacostocolor,
                       ceq.excedentecolor,
                       dir.direccion as dir_c,
                       asre.poreliminar
                       FROM rentas_has_detallesEquipos as rhde 
                       inner join asignacion_series_r_equipos as asre on asre.id_equipo=rhde.id 
                       inner JOIN series_productos as sep on sep.serieId=asre.serieId 
                       inner join equipos as equi on equi.id=rhde.idEquipo 
                       inner join categoria_equipos as cteq on cteq.id=equi.categoriaId
                       left JOIN contrato_equipos  as ceq on ceq.equiposrow = rhde.id AND ceq.serieId = sep.serieId
                       left join contrato_equipo_direccion as dir on dir.equiposrow=rhde.id and dir.serieId=asre.serieId
                       WHERE rhde.idRenta=$idrenta and asre.activo=1 GROUP BY idEquipo,SerieId ORDER BY asre.orden ASC";
        $query = $this->db->query($sql);
        return $query->result();
    }
    function equiposrentastoner($equipo,$renta,$tipo){
        if ($tipo==1) {
            $wheretipo='and cons.tipo=1'; 
        }elseif($tipo==2){
            $wheretipo='and cons.tipo!=1';
        }else{
            $wheretipo='';
        }
        $sql="SELECT rdc.id_consumibles,cons.modelo 
        FROM rentas_has_detallesequipos_consumible as rdc
        inner join consumibles as cons on cons.id=rdc.id_consumibles
        WHERE rdc.idrentas=$renta and rdc.idequipo=$equipo $wheretipo  group by cons.modelo";
        //log_message('error', $sql);
        $query = $this->DB10->query($sql);
        $modelo='';
        foreach ($query->result() as $item) {
            $modelo.=$item->modelo.' ';
        }
        return $modelo;
    }
    function equiposrentastonerrendimiento($equipo,$renta,$tipo){
        if ($tipo==1) {
            //monocromatico
            $wheretipo='and cons.tipo=1'; 
        }else{
            //color
            $wheretipo='and cons.tipo!=1';
        }
        $sql="SELECT rdc.id_consumibles,cons.rendimiento_toner 
        FROM rentas_has_detallesequipos_consumible as rdc
        inner join consumibles as cons on cons.id=rdc.id_consumibles
        WHERE rdc.idrentas=$renta and rdc.idequipo=$equipo and cons.rendimiento_toner>0
        $wheretipo
        limit 1";
        $query = $this->DB9->query($sql);
        $rendimiento=0;
        foreach ($query->result() as $item) {
            $rendimiento=$item->rendimiento_toner;
        }
        return $rendimiento;
    }
    function equiposrentasaccesorios($equipo,$renta){
        /*
        $sql="SELECT idEquipo FROM rentas_has_detallesEquipos WHERE id=$equipo";
        $query = $this->db->query($sql);
        $idEquipo=0;
        foreach ($query->result() as $item) {
            $idEquipo=$item->idEquipo;
        }
        */
        $sql="SELECT rda.id_accesorio,acc.nombre 
        FROM rentas_has_detallesEquipos_accesorios as rda
        inner join catalogo_accesorios as acc on acc.id=rda.id_accesorio
        WHERE rda.idrentas=$renta and rda.id_equipo=$equipo";
        $query = $this->DB9->query($sql);
        $modelo='';
        foreach ($query->result() as $item) {
            $modelo.=$item->nombre.' ';
        }
        return $modelo;
    }
    public function hojasestado($idcontrato,$fechainicio,$fechafin,$tipo){
        $sql = "SELECT asre.*,rhde.modelo
                FROM contrato as con
                inner join rentas_has_detallesEquipos as rhde on rhde.idRenta=con.idRenta
                inner join alta_rentas_captura as asre on asre.id_equipo=rhde.id
                inner JOIN series_productos as sep on sep.serieId=asre.serieId
                WHERE con.idcontrato=$idcontrato AND asre.reg  BETWEEN '$fechainicio 00:00:00' AND '$fechafin 23:59:59' AND asre.tipo = $tipo AND asre.status=1";
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function hojasestado_id($id)    {   
        $sql = "SELECT * FROM alta_rentas_captura WHERE idestado=$id";
        $query = $this->DB10->query($sql);
        return $query->result(); 
    }
    public function hojasestado_id_factura($idequipo,$idserie,$fechainicio,$fechafin){   
        $sql = "SELECT * FROM alta_rentas_captura WHERE status=1 AND id_equipo=$idequipo AND serieId = $idserie AND reg BETWEEN '$fechainicio 00:00:00' AND '$fechafin 23:59:59'";
        $query = $this->DB10->query($sql);
        return $query->result(); 
    }
    public function cliente_has_datos_fiscales($id){
        $sql = "SELECT id AS id_datos_fiscales,rfc,razon_social FROM cliente_has_datos_fiscales WHERE idCliente = $id and activo=1";
        $query = $this->DB9->query($sql);
        return $query->result();
    }
    function verificar_pass($pass) {
        $strq = "SELECT * FROM usuarios WHERE perfilId=1";
        $count = 0;
        $passwo =0;
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $passwo =$row->contrasena;
            $verificar = password_verify($pass,$passwo);
            if ($verificar){
                $count=1;

            }
        } 
        echo $count;
    }
    function verificar_pass2($pass) {
        $strq = "SELECT * FROM usuarios WHERE perfilId=1";
        $count = 0;
        $passwo =0;
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $passwo =$row->contrasena;
            $verificar = password_verify($pass,$passwo);
            if ($verificar){
                $count=1;

            }
        } 
        return $count;
    }
    public function clientefactura($id){
        $sql = "SELECT cl.id,cl.empresa,ch.rfc,c.idRenta,c.tiporenta,c.ordencompra
                FROM contrato AS c
                INNER JOIN rentas AS r ON r.id = c.idRenta
                INNER JOIN clientes AS cl ON cl.id = r.idCliente
                LEFT JOIN cliente_has_datos_fiscales AS ch ON ch.id = c.rfc
                WHERE c.idcontrato = $id";
        $query = $this->DB10->query($sql);
        return $query->result();
    }
    public function contratofacturas($idCliente){
        $sql = "SELECT con.idcontrato,per.nombre,con.fechainicio,con.vigencia,con.folio FROM rentas AS r
                INNER JOIN clientes AS c ON c.id = r.idCliente
                INNER JOIN contrato AS con ON con.idRenta = r.id 
                INNER JOIN personal AS per ON per.personalId = con.personalId
                WHERE r.idCliente = $idCliente";
        $query = $this->DB10->query($sql);
        return $query->result();
    }
    public function contratorentafactura($idcontrato){
        $sql = "SELECT arp.prefId, arp.periodo_inicial, arp.periodo_final FROM alta_rentas AS ar
        INNER JOIN alta_rentas_prefactura AS arp ON arp.id_renta = ar.id_renta
        WHERE ar.idcontrato = $idcontrato";
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function contratorentafactura2($idcontrato){
        $sql = "SELECT 
                arp.prefId, 
                arp.periodo_inicial, 
                arp.periodo_final,
                arp.excedente,
                arp.excedentecolor 

                FROM alta_rentas AS ar
                INNER JOIN alta_rentas_prefactura AS arp ON arp.id_renta = ar.id_renta
                WHERE ar.idcontrato = $idcontrato and ar.tipo2=2 and arp.periodo IS null";
        $query = $this->DB10->query($sql);
        return $query->result();
    }
    public function equiposfactura($idfactura){
        $sql = "SELECT arp.prefdId, rhd.modelo,sp.serie,arp.statusfacturar,arp.statusfacturae,arp.serieId,arp.tipo
        FROM alta_rentas_prefactura_d AS arp
        INNER JOIN rentas_has_detallesEquipos AS rhd ON rhd.id = arp.productoId
        INNER JOIN series_productos AS sp ON sp.serieId = arp.serieId
        /*INNER JOIN alta_rentas AS ar ON ar.id_renta = rhd.idRenta
        */
        WHERE arp.prefId = $idfactura order by arp.serieId,arp.tipo ASC";
        $query = $this->DB10->query($sql);
        return $query->result();
    }
    public function getseleclikeunidadsat($search){
        $strq = "SELECT * from f_unidades WHERE activo=1 and (Clave like '%".$search."%' or nombre like '%".$search."%' )";
        $query = $this->DB10->query($strq);
        //$this->db->close();
        return $query;
    }
    public function getseleclikeconceptosa($search){
        $strq = "SELECT * from f_servicios WHERE activo=1 and (Clave like '%".$search."%' or nombre like '%".$search."%' )";
        $query = $this->DB10->query($strq);
        //$this->db->close();
        return $query;
    }
    function ultimoFolio($serie) {
        //$strq = "SELECT Folio FROM f_facturas WHERE activo=1 and Folio!=0 ORDER BY FacturasId DESC limit 1";
        $strq = "SELECT max(Folio) as Folio FROM f_facturas WHERE serie='$serie' and (Estado=1 or Estado=0) and activo=1";
        $Folio = 0;
        $query = $this->db->query($strq);
        foreach ($query->result() as $row) {
            $Folio =$row->Folio;
        } 
        return $Folio;
    }
    function facturadetalle($facturaId){
        $strq = "SELECT * FROM `f_facturas_servicios` as fas 
                inner join f_unidades as uni on uni.Clave=fas.Unidad
                WHERE fas.FacturasId=".$facturaId;
        $query = $this->DB10->query($strq);
        return $query; 
    }
    function detalleperidofactura($facturaId){
        $strq="SELECT rde.id,
                      rde.idEquipo,
                      rde.modelo,
                      sepro.serie,
                      arpd.c_c_i,
                      arpd.c_c_f,
                      arpd.e_c_i,
                      arpd.e_c_f,
                      arpd.descuento,
                      arpd.produccion,
                      arpd.toner_consumido,
                      arpd.excedentes,
                      arpd.costoexcedente,
                      arpd.totalexcedente,
                      fapr.prefacturaId,
                      fapr.contratoId
                FROM `factura_prefactura` as fapr 
                inner JOIN alta_rentas_prefactura_d as arpd on arpd.prefId=fapr.prefacturaId
                inner JOIN rentas_has_detallesEquipos as rde on rde.id =arpd.productoId
                inner JOIN series_productos as sepro on sepro.serieId=arpd.serieId
                WHERE fapr.facturaId=".$facturaId." 
                group by fapr.prefacturaId,
                      fapr.contratoId
                ";
        $query = $this->DB15->query($strq);
        return $query; 
    }
    function detalleperido($periodo){
        $strq="
                SELECT * from (
                SELECT rde.id,
                      rde.idEquipo,
                      rde.modelo,
                      sepro.serie,
                      sepro.usado,
                      sepro.serieId,
                      arpd.prefdId,
                      arpd.c_c_i,
                      arpd.c_c_f,
                      arpd.e_c_i,
                      arpd.e_c_f,
                      arpd.descuento,
                      arpd.descuentoe,
                      arpd.produccion,
                      arpd.toner_consumido,
                      arpd.excedentes,
                      arpd.costoexcedente,
                      arpd.totalexcedente,
                      arpd.tipo,
                      (select are.clicks_mono       from alta_rentas_equipos as are where are.id_equipo=rde.id and are.serieId=sepro.serieId and are.tipo=arpd.tipo GROUP by are.clicks_mono) as clicks_mono,
                      (select are.clicks_color      from alta_rentas_equipos as are where are.id_equipo=rde.id and are.serieId=sepro.serieId and are.tipo=arpd.tipo GROUP by are.clicks_color) as clicks_color,
        (select are.precio_c_e_mono   from alta_rentas_equipos as are where are.id_equipo=rde.id and are.serieId=sepro.serieId and are.tipo=arpd.tipo GROUP by are.precio_c_e_mono) as precio_c_e_mono,
        (select are.precio_c_e_color  from alta_rentas_equipos as are where are.id_equipo=rde.id and are.serieId=sepro.serieId and are.tipo=arpd.tipo GROUP by are.precio_c_e_color ) as precio_c_e_color,
        (select are.renta   from alta_rentas_equipos as are where are.id_equipo=rde.id and are.serieId=sepro.serieId and are.tipo=arpd.tipo GROUP by are.renta) as precio_r,
        (select are.rentac  from alta_rentas_equipos as are where are.id_equipo=rde.id and are.serieId=sepro.serieId and are.tipo=arpd.tipo GROUP by are.rentac ) as precio_rc,

                      (select are.orden             from asignacion_series_r_equipos as are where are.id_equipo=rde.id and are.serieId=sepro.serieId GROUP by are.orden) as orden
                FROM `alta_rentas_prefactura_d` as arpd 
                inner JOIN rentas_has_detallesEquipos as rde on rde.id =arpd.productoId
                inner JOIN series_productos as sepro on sepro.serieId=arpd.serieId
                WHERE arpd.prefId=$periodo ) as datos ORDER BY orden,prefdId ASC";
        $query = $this->db->query($strq);
        return $query; 
    }
    function detalleconsumiblesfolios($facturaId){
        $strq="SELECT con.modelo, confo.idconsumible,confo.folio,confo.foliotext,confo.status
                from factura_prefactura as facpre
                inner JOIN contrato as cont on cont.idcontrato=facpre.contratoId 
                inner join contrato_folio as confo on confo.idrenta=cont.idRenta
                inner join consumibles as con on con.id=confo.idconsumible
                inner join alta_rentas_prefactura as arp on arp.prefId=facpre.prefacturaId
                WHERE facpre.facturaId=$facturaId and 
                        ((confo.fechagenera BETWEEN arp.periodo_inicial and arp.periodo_final) or (confo.fecharetorno BETWEEN arp.periodo_inicial and arp.periodo_final))";
                $query = $this->DB15->query($strq);
        return $query; 
    }
    function detalleconsumiblesfoliosp($contrato,$periodo){
        $strq="SELECT con.modelo, confo.idconsumible,confo.folio,confo.foliotext,confo.status
                from contrato as cont
                inner join contrato_folio as confo on confo.idrenta=cont.idRenta
                inner join consumibles as con on con.id=confo.idconsumible
                inner join alta_rentas_prefactura as arp on arp.prefId=$periodo
                WHERE cont.idcontrato=$contrato and 
                        ((confo.fechagenera BETWEEN arp.periodo_inicial and arp.periodo_final) or (confo.fecharetorno BETWEEN arp.periodo_inicial and arp.periodo_final))";
                $query = $this->DB18->query($strq);
        return $query; 
    }
    public function detalleimagesRenta($idFact){
        /*$this->db->select('ff.*');
        $this->db->from('factura_prefactura ff');
        $this->db->join('alta_rentas_prefactura_d arpd','arpd.prefId=ff.prefacturaId');
        $this->db->join('alta_rentas_prefactura arp','arp.prefId=ff.prefacturaId');
        $this->db->join('alta_rentas_captura arc','arc.id_equipo=arpd.productoId');
        //$this->db->where('arc.reg>=','arp.periodo_inicial');
        $this->db->where("arc.reg between arp.periodo_inicial AND arp.periodo_final ");
        $this->db->where("arc.status",1);
        $query = $this->db->get();
        return $query->result();*/

        $strq="SELECT rde.id,
                      rde.idEquipo,
                      rde.modelo,
                      arc.nombre,
                      arc.status,
                      arc.tipoimg
                FROM factura_prefactura as fapr 
                inner JOIN alta_rentas_prefactura_d as arpd on arpd.prefId=fapr.prefacturaId
                inner JOIN rentas_has_detallesEquipos as rde on rde.id =arpd.productoId
                inner JOIN series_productos as sepro on sepro.serieId=arpd.serieId
                left JOIN alta_rentas_captura as arc on arc.id_equipo=rde.id
                inner join alta_rentas_prefactura as arp on arp.prefId=fapr.prefacturaId
                WHERE arc.status=1 and fapr.facturaId=$idFact and 
                        (arc.reg BETWEEN arp.periodo_inicial and arp.periodo_final)";

        $query = $this->DB15->query($strq);
        return $query; 
    }
    public function detalleimagesRentap($periodo){
        $strq="SELECT rde.id,
                      rde.idEquipo,
                      rde.modelo,
                      arc.nombre,
                      arc.status,
                      arc.tipoimg
                FROM alta_rentas_prefactura as arp 
                inner JOIN alta_rentas_prefactura_d as arpd on arpd.prefId=arp.prefId
                inner JOIN rentas_has_detallesEquipos as rde on rde.id =arpd.productoId
                inner JOIN series_productos as sepro on sepro.serieId=arpd.serieId
                left JOIN alta_rentas_captura as arc on arc.id_equipo=rde.id
                WHERE arc.status=1 and arp.prefId=$periodo and 
                        (arc.reg BETWEEN arp.periodo_inicial and arp.periodo_final)";

        $query = $this->DB18->query($strq);
        return $query; 
    }
    public function getFactura($idFact){
        $this->db->select('*');
        $this->db->from('f_facturas');
        $this->db->where('FacturasId',$idFact);
        $query = $this->db->get();
        return $query->result();
    }
    public function getFacturaContrato($idFact,$idfCont){
        $this->db->select('*');
        $this->db->from('factura_prefactura');
        $this->db->where('contratoId',$idfCont);
        $this->db->where('facturaId',$idFact);
        $query = $this->db->get();
        return $query->result();
    }
    function getRentasPrefDet($params){
        $this->DB9->select('sum(toner_consumido) as toner_consumido');
        $this->DB9->from('alta_rentas_prefactura_d');
        $this->DB9->where('productoId',$params['idequipo']);
        $this->DB9->where('serieId',$params['serie']);
        $this->DB9->where('tipo',$params['tipoT']);
        $this->DB9->group_by(array("productoId", "serieId", "tipo"));
        $query=$this->DB9->get(); 
        return $query->result();
    }
    function getdatosdeestadodecuenta($contrato){
         $strq="SELECT con.folio,cli.empresa,ffac.Direccion,cli.credito
                FROM factura_prefactura as fapr
                inner JOIN contrato as con on con.idcontrato=fapr.contratoId
                inner join rentas as ren on ren.id=con.idRenta
                inner join clientes as cli on cli.id=ren.idCliente
                inner join f_facturas as ffac on ffac.FacturasId=fapr.facturaId
                WHERE fapr.contratoId=$contrato";
                $query = $this->db->query($strq);
        return $query;
    }
    function listservicios($idcontrato,$inicio,$fin){
        $strq=" SELECT asige.asignacionIde,asige.asignacionId,rut.nombre,asige.fecha,asige.hora,asige.horafin,rene.modelo ,ser.serie 
                FROM `asignacion_ser_contrato_a` as asig 
                INNER JOIN asignacion_ser_contrato_a_e as asige on asige.asignacionId=asig.asignacionId 
                inner join series_productos as ser on ser.serieId=asige.serieId 
                inner join rentas_has_detallesequipos as rene on rene.id=asige.idequipo 
                inner JOIN rutas as rut on rut.id=asig.zonaId 
                WHERE asig.contratoId=$idcontrato AND asige.fecha BETWEEN '$inicio' and '$fin'";
                $query = $this->DB10->query($strq);
        return $query;
    }
    function servicioscontrato($contrato,$fecha){
        $strq="SELECT 
                    `ac`.`asignacionId`,
                    `ac`.`prioridad`,
                    `cli`.`empresa`,
                    `ace`.`fecha`,
                    `ace`.`hora`,
                    `ace`.`horafin`,
                    `ace`.`status`,
                    `ace`.`confirmado`,
                    1 as tipo
                    FROM `asignacion_ser_contrato_a_e` `ace`
                    JOIN `asignacion_ser_contrato_a` `ac` ON `ac`.`asignacionId`=`ace`.`asignacionId`
                    JOIN `personal` `per` ON `per`.`personalId`=`ace`.`tecnico`
                    JOIN `contrato` `con` ON `con`.`idcontrato`=`ac`.`contratoId`
                    JOIN `rentas` `ren` ON `ren`.`id`=`con`.`idRenta`
                    JOIN `clientes` `cli` ON `cli`.`id`=`ren`.`idCliente`
                    JOIN `rutas` `ru` ON `ru`.`id`=`ac`.`zonaId`
                    JOIN `rentas_has_detallesEquipos` `rde` ON `rde`.`id`=`ace`.`idequipo`
                    JOIN `series_productos` `serp` ON `serp`.`serieId`=`ace`.`serieId`
                    left JOIN `servicio_evento` `sereve` ON `sereve`.`id`=`ac`.`tservicio`
                    where 
                        ac.activo=1 and 
                        ace.activo=1 and 
                        ace.status<3 and 
                        con.idcontrato=$contrato and 
                        ace.fecha>='$fecha' 
                        GROUP BY ac.asignacionId,ace.fecha,ace.hora,ace.horafin 
                    ORDER BY ace.fecha  ASC
                    ";
                $query = $this->DB9->query($strq);
        return $query;
    }
    public function polizascliente($idCliente)    {
        $sql = "SELECT *
                FROM vista_polizascreadas
                WHERE idCliente = $idCliente and activo=1 ORDER BY id DESC";
        $query = $this->db->query($sql);
        return $query->result();
    }
    function serviciospolizas($poliza,$fecha){
        $strq="SELECT 
                    apol.asignacionId,
                    apole.fecha,
                    apole.hora,
                    apole.horafin 
                    FROM asignacion_ser_poliza_a as apol 
                    INNER JOIN asignacion_ser_poliza_a_e as apole on apole.asignacionId=apol.asignacionId 
                    INNER JOIN polizasCreadas as pol on pol.id=apol.polizaId AND pol.estatus=1 
                    WHERE 
                    apol.polizaId=$poliza and 
                    apole.fecha>='$fecha' and
                    apol.activo=1
                    GROUP BY apol.asignacionId
                    ORDER BY apole.fecha  ASC
                    ";
                $query = $this->db->query($strq);
        return $query;
    }
    //=======================================
    function getListado_fac_pen_ren($f1,$f2,$fechafinal,$tipo,$personal,$cli){
        $wfechaselected="";
        if($f1!='' && $f2!=''){
            //$fechaselected = strtotime ( '- 3 days' , strtotime ( $fechaselected) ) ;
            //$fechaselected = date ( 'Y-m-d' , $fechaselected );
            //$wfechaselected =" per.periodo_final='$fechaselected' and ";
            //$wfechaselected=" per.periodo_final BETWEEN '$f1' and '$f2' and ";
            //$wfechaselected=" (per.periodo_final BETWEEN '$f1' and '$f2' OR per.fechaasignada BETWEEN '$f1' and '$f2') and ";
            $wfechaselected=" (per.fechaasignada BETWEEN '$f1' and '$f2') and ";
        }else{
            //$wfechaselected="";
            $wfechaselected=" per.periodo_final<='$fechafinal' and ";
        }
        if($personal==50){
            $wherepersonal=" per.asignadas=1 and";
        }else{
            $wherepersonal="";
        }
        
        $tipo_fact='';
        $factura_tipo='';
        if($tipo==2){
            $tipo_fact=" ff.fechatimbre!='0000-00-00 00:00:00' and ff.Estado=1 and fac.facturaId >0 ";
            $factura_tipo='';
        }else{
            $factura_tipo=" per.estatus=$tipo and per.factura_renta=0 and
            per.factura_excedente=0 and ";
            $tipo_fact=' fac.facturaId IS null';
        }
        if($cli>0){
            $where_cli=" cli.id ='$cli' and ";
        }else{
            $where_cli="";
        }

        $strq="SELECT 
                aren.idcontrato,
                cli.empresa,
                per.periodo_inicial,
                per.periodo_final,
                fac.facturaId,
                pers.nombre,
                pers.apellido_paterno,
                pers.apellido_materno,
                per.prefId,
                per.estatus,
                per.asignadas,
                per.fechaasignada,
                ff.fechatimbre,
                persff.nombre AS nombref,
                persff.apellido_paterno AS apellido_paternof,
                persff.apellido_materno AS apellido_maternof,
                per.estatus_motivo,
                per.estatus_confirm,
                per.prioridad,
                per.comen,
                1 as cargadas
            FROM alta_rentas_prefactura as per
            INNER JOIN alta_rentas as aren on aren.id_renta=per.id_renta
            INNER JOIN contrato as cont on cont.idcontrato=aren.idcontrato
            INNER JOIN rentas as ren on ren.id=cont.idRenta
            INNER JOIN clientes as cli on cli.id=ren.idCliente
            LEFT JOIN personal as pers on pers.personalId=cont.ejecutivo
            LEFT JOIN factura_prefactura as fac on fac.prefacturaId=per.prefId
            LEFT JOIN f_facturas as ff on ff.FacturasId=fac.facturaId
            LEFT JOIN personal as persff on persff.personalId=ff.usuario_id
            WHERE 
            $factura_tipo $wherepersonal $where_cli
            cont.estatus=1 and $wfechaselected $tipo_fact";//se actualizo el etatus a 1 por que solo tiene que traer la informacion de los vigentes, descartando los eliminado y los finalizados 

        $this->db->query($strq);
        $query = $this->db->query($strq);
        return $query->result();
    }
    function getListado_fac_pen_ren2($cli){
        if($cli>0){
            $where_cli=" and cli.id ='$cli' ";
        }else{
            $where_cli="";
        }

        $strq="SELECT idcontrato,empresa,max_prefid,arp2.* 
               from( 
                    SELECT max(arp.prefId) as max_prefid,con.idcontrato,cli.empresa
                    FROM alta_rentas as ar 
                    INNER JOIN alta_rentas_prefactura as arp on arp.id_renta=ar.id_renta 
                    INNER JOIN contrato as con on con.idcontrato=ar.idcontrato 
                    INNER JOIN rentas as ren on ren.id=con.idRenta
                    INNER JOIN clientes as cli on cli.id=ren.idCliente
                    WHERE con.estatus=1 $where_cli
                    GROUP BY con.idcontrato
                ) as dp 
               INNER join alta_rentas_prefactura as arp2 on arp2.prefId=max_prefid 
               WHERE arp2.periodo_inicial<DATE_SUB(NOW(),INTERVAL '1' MONTH)";
        $this->db->query($strq);
        $query = $this->db->query($strq);
        return $query;
    }
    //========================================
    function concultarcontratoscliente($idCliente){
        $strq="SELECT con.* 
                FROM rentas as ren 
                INNER JOIN contrato as con on con.idRenta=ren.id 
                WHERE ren.idCliente=$idCliente";
        $this->db->query($strq);
        $query = $this->db->query($strq);
        return $query;
    }
    function rentas_equipos_list($idRenta){
        $strq="SELECT 
                    rde.id as rowequipo,
                    rde.idEquipo,
                    eq.noparte,
                    eq.modelo,
                    asre.serieId,
                    sp.serie,
                    asre.folio_equipo,
                    bod.bodega,
                    rde.serie_bodega,
                    bod.bodegaId,
                    asre.ubicacion,
                    asre.comenext
                FROM rentas_has_detallesEquipos as rde 
                INNER JOIN equipos as eq on eq.id=rde.idEquipo 
                INNER JOIN asignacion_series_r_equipos as asre on asre.id_equipo=rde.id 
                INNER JOIN series_productos as sp on sp.serieId=asre.serieId 
                left JOIN bodegas as bod on bod.bodegaId=rde.serie_bodega
                WHERE rde.estatus=1 AND asre.activo=1 and rde.idRenta=$idRenta";
        $this->db->query($strq);
        $query = $this->db->query($strq);
        return $query;
    }
    function rentas_equipos_resumen($idRenta){
        $strq="SELECT COUNT(*) as total,eq.modelo 
                FROM rentas_has_detallesEquipos as rde 
                INNER JOIN equipos as eq on eq.id=rde.idEquipo 
                INNER JOIN asignacion_series_r_equipos as asre on asre.id_equipo=rde.id 
                INNER JOIN series_productos as sp on sp.serieId=asre.serieId 
                WHERE rde.estatus=1 AND asre.activo=1 and rde.idRenta=$idRenta GROUP by eq.id";
        $this->db->query($strq);
        $query = $this->db->query($strq);
        return $query;
    }
    function rentas_toner_resumen($idRenta){
        $strq="SELECT 
                    sum(rhdc.cantidad) as cantidad,
                    c.modelo 
                    FROM rentas_has_detallesequipos_consumible AS rhdc 
                    INNER JOIN consumibles AS c ON c.id = rhdc.id_consumibles  
                WHERE rhdc.idrentas=$idRenta GROUP BY c.id";
        $this->db->query($strq);
        $query = $this->db->query($strq);
        return $query;
    }
    public function datosventaser($idventa,$tipo)    {
        if($tipo==0){
            $sql = "SELECT vn.idCliente,vc.idserie FROM ventas as vn
                INNER JOIN ventas_has_detallesConsumibles as vc on vc.idVentas=vn.id
                WHERE vn.activo=1 and vn.id=$idventa and vc.idserie>0";
        }else{
            $sql ="SELECT vn.idCliente,vc.idserie 
            FROM ventacombinada as vnc
            INNER JOIN ventas as vn on vn.id=vnc.consumibles
            INNER JOIN ventas_has_detallesConsumibles as vc on vc.idVentas=vn.id
            WHERE vnc.activo=1 AND vnc.combinadaId=$idventa and vc.idserie>0";
        }
        
        $query = $this->db->query($sql);
        return $query;
    }
    public function datosventaservis($cliente,$serie)    {
        
            $sql ="SELECT 
                    av.asignacionId,
                    av.ventaId,
                    av.fecha,
                    vn.idCliente,
                    vc.idserie
                FROM asignacion_ser_venta_a as av
                INNER JOIN ventas as vn on vn.id=av.ventaId and av.tipov=vn.combinada
                INNER JOIN ventas_has_detallesConsumibles as vc on vc.idVentas =vn.id

                WHERE av.activo=1 AND av.status=2  and vn.idCliente=$cliente and vc.idserie=$serie
                ORDER BY av.fecha DESC LIMIT 1";
        
        
        $query = $this->db->query($sql);
        return $query;
    }
    public function datosdireccioncontacto($cliente){
        $sql ="SELECT clid.*,GROUP_CONCAT(clidc.atencionpara) as atencionpara 
                FROM clientes_direccion as clid
                LEFT JOIN cliente_datoscontacto as clidc on clidc.dir=clid.idclientedirecc
                WHERE clid.status=1 AND clid.idcliente='$cliente' GROUP BY clid.idclientedirecc";
        
        
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function getobtenescentrocostosgroup($contrato){
        $sql ="SELECT cceq.idcentroc,cc.idcontrato,cc.descripcion,cc.clicks,cc.clicks_c,cceq.rowequipo,cceq.modeloid,cceq.serieid
                FROM centro_costos as cc
                INNER JOIN centro_costos_equipos as cceq on cceq.idcentroc=cc.id AND cceq.activo=1
                WHERE cc.idcontrato='$contrato' AND cc.activo=1  
                GROUP BY cceq.idcentroc";
        
        
        $query = $this->db->query($sql);
        return $query;
    }
    public function get_info_prefactura($idcontrato){
        $sql ="SELECT 
                arp.prefId, ar.tipo2, arp.periodo_inicial,arp.periodo_final,arp.idcondicionextra,
                ar.clicks_mono as g_clicks_mono,
                ar.precio_c_e_mono as g_precio_c_e_mono,
                ar.clicks_color as g_clicks_color,
                ar.precio_c_e_color as g_precio_c_e_color,
                ar.clicks_mono,
                ar.clicks_color,
                ar.precio_c_e_mono,
                ar.precio_c_e_color,
                arp.subtotal,
                arp.excedente,
                arp.subtotalcolor,
                arp.excedentecolor,
                arp.total,
                fac.FacturasId,
                fac.Estado,
                ar.monto,
                ar.montoc,
                facpre.statuspago


                FROM alta_rentas as ar
                INNER JOIN alta_rentas_prefactura as arp on arp.id_renta=ar.id_renta
                LEFT JOIN factura_prefactura as facpre on facpre.prefacturaId=arp.prefId
                LEFT JOIN f_facturas as fac on fac.FacturasId=facpre.facturaId and fac.Estado=1
                WHERE ar.idcontrato='$idcontrato' /*and (fac.FacturasId is null or arp.total>fac.subtotal) */
                GROUP by arp.prefId
                ";
        
        
        $query = $this->db->query($sql);
        return $query;
    }
    function get_info_pre_dll_pro_ex_indi($idpre,$tipo){
        $sql="SELECT 
                SUM(arpd.produccion_total) as produccion_total, 
                SUM(arpd.excedentes) as excedentes, 
                arpd.costoexcedente as costoexcedente 
                FROM `alta_rentas_prefactura_d` as arpd 
                WHERE arpd.prefId='$idpre' and arpd.tipo='$tipo'";
        $query = $this->db->query($sql);
        return $query;
    }
    function titlehtmlformatocc($tipo,$info){
        if($tipo==1){
            $title='Mono'.$info;
        }else{
            $title='color'.$info;
        }
        $html_title='<table border="1" align="center" cellpadding="2"><thead>';
                  $html_title.='<tr valign="middle" class="styletitles">';
                    $html_title.='<th class="httable" colspan="2" class="styletitles"><b>EQUIPO</b></th>';
                    $html_title.='<th class="httable" colspan="3"><b>PRODUCCIÓN COPIA/IMPRESIÓN '.$title.'</b></th>';
                    $html_title.='<th class="httable" colspan="3"><b>PRODUCCIÓN ESCANEO</b></th>';
                    $html_title.='<th class="httable" rowspan="2"><b>PRODUCCIÓN EQUIPO</b></th>';
                    $html_title.='<th class="httable" rowspan="2"><b>SubTotal</b></th>';
                  $html_title.='</tr>';
                  $html_title.='<tr valign="middle" class="styletitles">';
                    $html_title.='<th class="httable"><b>MODELO</b></th>';
                    $html_title.='<th class="httable"><b>SERIE</b></th>';
                    $html_title.='<th class="httable"><b>CONTADOR INICIAL</b></th>';
                    $html_title.='<th class="httable"><b>CONTADOR FINAL</b></th>';
                    $html_title.='<th class="httable"><b>PRODUCCIÓN</b></th>';
                    $html_title.='<th class="httable"><b>CONTADOR INCIAL</b></th>';
                    $html_title.='<th class="httable"><b>CONTADOR FINAL</b></th>';
                    $html_title.='<th class="httable"><b>PRODUCCIÓN</b></th>';
                    
                  $html_title.='</tr>';
                $html_title.='</thead>';
        return $html_title;
    }
    function get_info_pred_contadores_indi($contrato,$pre,$tipo){
        $sql="SELECT 
                are.*,
                arp.excedentes 
            FROM alta_rentas as ar
            INNER JOIN alta_rentas_equipos as are on are.id_renta=ar.id_renta
            inner join alta_rentas_prefactura_d as arp on arp.productoId=are.id_equipo and arp.serieId=are.serieId AND arp.tipo=are.tipo
            WHERE ar.idcontrato='$contrato' AND are.tipo='$tipo' AND arp.prefId='$pre'";
        $query = $this->db->query($sql);
        return $query;
    }
    function get_cont_vigencia(){
        $sql="SELECT 
                con.idcontrato,
                con.folio,
                cli.empresa,
                con.oc_vigencia,
                CURDATE() as fechahoy,
                DATE_SUB(con.oc_vigencia, INTERVAL 3 MONTH) AS nueva_fecha,
                con.oc_vigencia_new
            FROM contrato as con
            INNER JOIN rentas as ren on ren.id=con.idRenta
            INNER JOIN clientes as cli on cli.id=ren.idCliente
            WHERE 
                con.estatus>0 AND 
                con.oc_vigencia IS NOT NULL AND
                DATE_SUB(con.oc_vigencia, INTERVAL 3 MONTH) < CURDATE()
                ORDER BY con.oc_vigencia  ASC";

        $query = $this->db->query($sql);
        return $query;
    }
    function get_euipos_renta_historial($idRenta){
        /*
        $sql="SELECT rde.*
                FROM rentas_has_detallesEquipos as rde
                LEFT JOIN rentas_historial_equipos as rhe ON rde.id=rhe.equipo and rhe.activo=1
                WHERE rde.idRenta='$idRenta' AND rhe.id IS null
                GROUP by rde.id";
                */
        $sql="SELECT rde.*,rhe.id as id_rhe_eq
                FROM rentas_has_detallesEquipos as rde
                LEFT JOIN rentas_historial_equipos as rhe ON rde.id=rhe.equipo and rhe.activo=1
                WHERE rde.idRenta='$idRenta'
                GROUP by rde.id";

        $query = $this->db->query($sql);
        return $query;
    }
    function get_equipos_contrato($idcontrato,$ser){
        if($ser==1){
            $w_ac=" AND ase.activo=1 ";
        }else{
            $w_ac="";
        }
        $sql="SELECT arpd.productoId,arpd.serieId,rde.modelo,serp.serie,ase.activo
            FROM alta_rentas as ar
            INNER JOIN alta_rentas_prefactura as arp on arp.id_renta=ar.id_renta
            INNER JOIN alta_rentas_prefactura_d as arpd on arpd.prefId=arp.prefId
            INNER join rentas_has_detallesEquipos as rde on rde.id=arpd.productoId
            INNER JOIN series_productos as serp on serp.serieId=arpd.serieId
            INNER JOIN asignacion_series_r_equipos as ase on ase.id_equipo=rde.id AND ase.serieId=arpd.serieId
            WHERE ar.idcontrato='$idcontrato' $w_ac
            GROUP BY arpd.productoId,arpd.serieId";

        $query = $this->db->query($sql);
        return $query;
    }
    function get_info_prefactura_2($idcontrato,$fi,$ff){
        if($fi==''){
            $w_fi="";
        }else{
            $w_fi=" and arp.periodo_inicial>='$fi' ";
        }
        if($ff==''){
            $w_ff="";
        }else{
            $w_ff=" and arp.periodo_inicial<='$ff' ";
        }   
        $sql="SELECT arp.*
            FROM alta_rentas as ar
            INNER JOIN alta_rentas_prefactura as arp on arp.id_renta=ar.id_renta
            WHERE ar.idcontrato='$idcontrato' $w_fi $w_ff
            ORDER BY arp.periodo_inicial  ASC";

        $query = $this->db->query($sql);
        return $query;
    }
    function get_info_prefactura_d($idcontrato,$fi,$ff){
        if($fi==''){
            $w_fi="";
        }else{
            $w_fi=" and arp.periodo_inicial>='$fi' ";
        }
        if($ff==''){
            $w_ff="";
        }else{
            $w_ff=" and arp.periodo_inicial<='$ff' ";
        }  
        $sql="SELECT arp.periodo_inicial,arp.periodo_final,DATE_FORMAT(arp.periodo_inicial, '%m_%Y') as fecha_form,arpd.productoId,arpd.serieId,arpd.c_c_i,arpd.c_c_f,arpd.e_c_i,arpd.e_c_f,arpd.tipo
                FROM alta_rentas as ar
                INNER JOIN alta_rentas_prefactura as arp on arp.id_renta=ar.id_renta
                INNER JOIN alta_rentas_prefactura_d as arpd on arpd.prefId=arp.prefId
                WHERE ar.idcontrato='$idcontrato' $w_fi $w_ff ";

        $query = $this->db->query($sql);
        return $query;
    }
    function verificar_ultimifolios_periodo($ideq){
        $strq="SELECT eqcons.id,eqcons.idrentas,eqcons.idequipo,eqcons.rowequipo,eqcons.cantidad,eqcons.id_consumibles,cf.idcontratofolio,cf.foliotext,cf.fechagenera,cf.status,
                DATEDIFF(NOW(), cf.fechagenera) AS dias_diferencia,
                areq.sol_consu_cant,areq.sol_consu_periodo,
                IF(areq.sol_consu_periodo > 0, areq.sol_consu_periodo * 30, 0) AS periodo_dias
                FROM rentas_has_detallesequipos_consumible AS eqcons 
                LEFT JOIN contrato_folio as cf on cf.idrelacion=eqcons.id
                INNER JOIN alta_rentas_equipos as areq on areq.id_equipo=eqcons.rowequipo
                WHERE eqcons.rowequipo='$ideq' AND cf.idcontratofolio>0
                ORDER BY cf.idcontratofolio DESC LIMIT 1";
        $query_c = $this->db->query($strq);
        $consu_alert=0;
        $consu_cant=0;
        foreach ($query_c->result() as $item) {
            $periodo_dias=$item->periodo_dias;
            if($periodo_dias>0){
                $periodo_dias=$periodo_dias-10;
               if($item->dias_diferencia>$periodo_dias){
                $consu_alert=1;
                $consu_cant=$item->sol_consu_cant;
               }
            }
            
        }




        $arraydatos = array(
                            'consu_alert'=>$consu_alert,
                            'consu_cant'=>$consu_cant
                            );
        //return json_encode($arraydatos);
        return $arraydatos;
    }
    function generadorfolio($aux,$idRenta){
        $folio = $aux +1; 
        $disponible = false;
        
        while (!$disponible) {
            // Generar el foliotext
            $foliotext = $idRenta . 'T' . str_pad($folio, 3, "0", STR_PAD_LEFT);
            //log_message('error','aux:'.$aux);
            // Consultar si ya existe el foliotext
            $result_ft = $this->ModeloCatalogos->getselectwheren('contrato_folio', array('foliotext' => $foliotext));

            // Si no existe en la base de datos, entonces lo consideramos disponible
            if ($result_ft->num_rows() == 0) {
                $disponible = true; // Salimos del ciclo porque el foliotext está disponible
            } else {
                $folio++; // Incrementamos el folio y seguimos buscando
            }
        }
        return array('folio'=>$folio,'foliotext'=>$foliotext);
    }
    function ultimacargacontador($serie){
        $sql="SELECT * FROM `carga_contadores` WHERE serie='$serie' AND activo=1 ORDER BY `id` DESC LIMIT 1";
        $query = $this->db->query($sql);
        $fecha_serie='';
        foreach ($query->result() as $item) {
            $fecha_serie='<br>'.$item->fecha_obtencion;
        }
        return $fecha_serie;
    }
}