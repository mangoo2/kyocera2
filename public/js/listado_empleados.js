// Obtener la base_url 
var base_url = $('#base_url').val();
var idper = $('#idper').val();
$(document).ready(function(){
    // Inicializar la tabla
	table = $('#tabla_empleados').DataTable();
	load();

    // Listener para editar
	$('#tabla_empleados tbody').on('click', 'a.edit', function () {
        var tr = $(this).closest('tr');
        var row = table.row(tr);
        var data = row.data();
        window.location = 'empleados/edicion/'+data.personalId;
    });

    // Listener para Visualizar 
    $('#tabla_empleados tbody').on('click', 'a.visualiza', function () 
    {
        var tr = $(this).closest('tr');
        var row = table.row(tr);
        var data = row.data();
        window.location = 'empleados/visualizar/'+data.personalId;
    });

    // Listener para eliminar 
    $('#tabla_empleados tbody').on('click', 'a.elimina', function () 
    {
        var tr = $(this).closest('tr');
        var row = table.row(tr);
        var data = row.data();
        confirmaEliminarEmpleado(data.personalId);
    });
    /*
    $('#descargarExcel').on('click', function () 
    {
        var datos = document.getElementById('tabla_empleados');
        // Eliminar la última columna
        Exporter.export(datos, 'reportes.xls', 'reportes');        
        
    });
    */

});

// Eliminar empleado, usando JQUERY CONFIRM
function confirmaEliminarEmpleado(idEmpleado)
{
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de Eliminar este empleado? El cambio será irreversible',
        type: 'red',
        typeAnimated: true,
        buttons: 
        {
            // Si le da click al botón de "confirmar"
            confirmar: function () 
            {
                $.ajax({
                    url: base_url+"index.php/empleados/eliminarEmpleado/"+idEmpleado,
                    success: function (response) 
                    {
                            $.alert({
                                boxWidth: '30%',
                                useBootstrap: false,
                                title: 'Éxito!',
                                content: 'Usuario Eliminado!'});
                            setTimeout(function(){ 
                                window.location.href = base_url+"index.php/empleados"; 
                            }, 2000);
                    },
                    error: function(response)
                    {
                        $.alert({
                                boxWidth: '30%',
                                useBootstrap: false,
                                title: 'Error!',
                                content: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema'});   
                        setTimeout(function(){ 
                                window.location.href = base_url+"index.php/empleados";
                            }, 2000);  
                    }
                });
            },
            // Si le da click a "cancelar"
            cancelar: function () 
            {
                
            }
        }
    });
}

// Cargar la tabla de los empleados
function load() {
    // Destruye la tabla y la crea de nuevo con los datos cargados 
    table.destroy();
    table = $('#tabla_empleados').DataTable({
        search: {
                return: true
            }, 
        "ajax": {
            "url": base_url+"index.php/Empleados/getListadoEmpleados"
        },
        "columns": [
            {"data": "personalId"},
            {"data": "nombre"},
            {"data": "apellido_paterno"},
            {"data": "sexo",
                render:function(data,type,row){
                    if(data==1){
                        var html="Hombre";    
                    }else{
                        var html="Mujer";
                    }
                    return html;
                }
            },
            {"data": "email"},
            
            {"data": "perfil"},
            {"data": "UsuarioID",
                render:function(data,type,row){
                    var html
                    if(row.UsuarioID>0){
                        
                        html='Empleado';//es el empleado que tiene session
                    }else{
                        html='Usuario';// no tiene session 
                    }
                    return html;
                }
            },
            {"data": "personalId",
                render:function(data,type,row){
                    // Se muestran todos los botones correspondientes
                    $('.tooltipped').tooltip();
                    var html='<a class="b-btn b-btn-success tooltipped edit" data-position="top" data-delay="50" data-tooltip="Editar">\
                                    <i class="fas fa-pencil-alt"></i>\
                                  </a>\
                                  <a class="b-btn b-btn-primary tooltipped visualiza" data-position="top" data-delay="50" data-tooltip="Visualizar">\
                                    <i class="fas fa-eye"></i>\
                                  </a>\
                                  <a class="b-btn b-btn-danger tooltipped elimina" data-position="top" data-delay="50" data-tooltip="Eliminar">\
                                    <i class="fas fa-trash"></i>\
                                  </a>';
                    if(idper==1 || idper==17 || idper==18){
                        if(row.p_efectivo==1){
                                var pagoefe='checked';
                            }else{
                                var pagoefe='';
                            }
                        html+='<div class="switch"><br>\
                                  <label class="soloadministradores">Pago Efectivo\
                                    <input type="checkbox" name="bloqueo" id="pfectivo_'+row.personalId+'"  onclick="pagoefe('+row.personalId+')" '+pagoefe+'>\
                                    <span class="lever"></span>\
                                  </label>\
                            </div>'; 
                    }
                    return html;
                }
            }

        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[25, 50, 100], [25, 50, 100]],
        "dom": 'Bfrtpl',
        // Muestra el botón de Excel para importar la tabla
        "buttons": [
            /*{   
                extend: 'excelHtml5',
                text: ' Descargar Excel <i class="fa fa-download"></i>',
                className: 'btn classBotonFratsa'
            }*/
        ]
    }).on('draw',function(){
        $('.dataTables_filter').append('<i class="fa fa-search buttonsearch" onclick="searchtable_v()"></i>')

    });
}
function searchtable_v(){
    var contenedor = document.getElementById('tabla_empleados_filter');

    // Selecciona el input dentro del contenedor
    var input = contenedor.querySelector('input.form-control-bmz');

    // Obtén el valor del input
    var searchv = input.value;
    console.log('valorInput '+searchv);
    
    //var searchv =$("input[type=search]").val();
    table.search(searchv).draw();
}
function pagoefe(id){
    setTimeout(function(){ 
        var pfectivo = $('#pfectivo_'+id).is(':checked')==true?1:0;
        $.ajax({
            type:'POST',
            url: base_url+"index.php/Empleados/pagoefe",
            data: {
                    per:id,
                    pfectivo:pfectivo
                },
            success: function (response){
                
            },
            error: function(response){
                toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema", "Advertencia");
            }
        });
    }, 1000);
}
