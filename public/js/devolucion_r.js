var base_url = $('#base_url').val();
var a_confirm;
function devolucion(tipo,id,cantidad){
    var bodegastext=$('#bodegaselect_none').html();
	/*
		tipo
			1 equipos
			2 ventas_has_detallesEquipos_dev(2)
			3 ventas_has_detallesEquipos_accesorios_dev(2)
			4 ventas_has_detallesequipos_consumible_dev(2)
			5 ventas_has_detallesRefacciones_dev
	*/
	if(cantidad==1){
		var disabledradio='disabled';
	}else{
		var disabledradio='';
	}
    if(tipo==1){
        var inforpro='Equipo (junto con sus consumibles y/o accesorios)';
    }else if(tipo==2){
        var inforpro='Consumible';
    }else if(tipo==3){
        var inforpro='Accesorio';
    }else{
        var inforpro='';
    }
	a_confirm=$.confirm({
        boxWidth: '55%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: 'Confirma la devolución del '+inforpro+'<br>'+
                 'Para continuar se necesita permisos de administrador<br>'+
                 '<input type="text" placeholder="Contraseña" id="contrasena" class="name form-control" data-val="" oninput="passinput()" maxlength="10"/>'+
                 '<label><input type="radio" name="tipodevolucion" onclick="tipodevolucion('+cantidad+','+tipo+')" value="1" checked>Total</label> '+
                 '<label><input type="radio" name="tipodevolucion" onclick="tipodevolucion('+cantidad+','+tipo+')" value="0" '+disabledradio+'>Parcial</label> '+
                 '<div class="cantidaddev" style="display:none">'+
                 	'<label>Cantidad a devolver:</label><input type="number" name="cantidaddev" id="cantidaddev"  class="form-control" value="'+cantidad+'" max="'+cantidad+'" min="1">'+
                 '</div>'+
                 '<br><label>Motivo</label><textarea class="form-control" id="motivodev"></textarea>'
                 ,
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
            	    var var_tipodevolucion=$('input:radio[name=tipodevolucion]:checked').val();
                    var var_cantidad=$('#cantidaddev').val();
                    var var_motivo=$('#motivodev').val();
                    var pass=$('#contrasena').data('val');

                 $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Generales/devoluciones_r",
                    data: {
                        tipotable:tipo,
                        idrow:id,
                        tipodevolucion:var_tipodevolucion,
                        cantidad:var_cantidad,
                        motivo:var_motivo,
                        pass:pass
                    },
                    success: function (data){
                        if (data==1) {
                            swal("Hecho!", "Realizado", "success");
                            setTimeout(function(){ location.reload();  }, 3000);
                        }else{
                            swal("Error", "No tiene permiso", "error"); 
                        }
                    },
                    error: function(response){
                        $.alert({
                                boxWidth: '30%',
                                useBootstrap: false,
                                title: 'Error!',
                                content: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema'});   
                         
                    }
                });
               
                
            },
            cancelar: function () 
            {
                
            }
        }
    });
    setTimeout(function(){
        $("#contrasena").passwordify({
            maxLength: 10,
            onlyNumbers: true
          }).focus();
    },1000);

}
function passinput(){
    $("#contrasena").change();
    var pass=$('#contrasena').val();
    if(pass==''){
        $('#contrasena').data('val','');
    }
}
function devolucion2(tipo,equipo,serie){
    var bodegastext=$('#bodegaselect_none').html();
	/*
		tipo
			1 ventas_has_detallesConsumibles_dev
			2 ventas_has_detallesEquipos_dev(2)
			3 ventas_has_detallesEquipos_accesorios_dev(2)
			4 ventas_has_detallesequipos_consumible_dev(2)
			5 ventas_has_detallesRefacciones_dev
	*/
	a_confirm=$.confirm({
        boxWidth: '55%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: 'Confirma la devolución del producto<br>'+
                 'Para continuar se necesita permisos de administrador<br>'+
                 '<input type="text" placeholder="Contraseña" id="contrasena" class="name form-control" data-val="" oninput="passinput()" maxlength="10"/>'+
                 '<br><label>Motivo</label><textarea class="form-control" id="motivodev"></textarea>'
                 ,
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
            	var pass=$('#contrasena').data('val');
                 $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Generales/devoluciones_r",
                    data: {
                        tipotable:tipo,
                        idrow:equipo,
                        tipodevolucion:0,
                        cantidad:serie,
                        motivo:$('#motivodev').val(),
                        pass:pass
                    },
                    success: function (data){
                        if (data==1) {
                            swal("Hecho!", "Realizado", "success");
                            setTimeout(function(){ location.reload();  }, 3000);
                        }else{
                            swal("Error", "No tiene permiso", "error"); 
                        }
                    },
                    error: function(response){
                        $.alert({
                                boxWidth: '30%',
                                useBootstrap: false,
                                title: 'Error!',
                                content: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema'});   
                         
                    }
                });
               
                
            },
            cancelar: function () 
            {
                
            }
        }
    });
    setTimeout(function(){
        $("#contrasena").passwordify({
            maxLength: 10,
            onlyNumbers: true
          }).focus();
    },1000);

}
function tipodevolucion(cantidad,tipo){
	var tipodevolucion = $('input:radio[name=tipodevolucion]:checked').val()
	if(tipodevolucion==1){
		$('.cantidaddev').hide('show');
	}else{
		if(tipo==1 || tipo==5){
			$('.cantidaddev').show('show');	
			$('#cantidaddev').on('input', function () {
			    var value = $(this).val();
			    if ((value !== '') && (value.indexOf('.') === -1)) {
			        $(this).val(Math.max(Math.min(value, cantidad), 1));
			    }
			});
		}
		if(tipo==2){
			a_confirm.close();
			$('.btndevolucion2').show('show');
		}
        if(tipo==3){
            a_confirm.close();
            $('.btndevolucion3').show('show');
        }
		
	}

	
}