<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloSolRefacciones extends CI_Model {
    public function __construct() {
        parent::__construct();
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d');
    }

    function getsolicitudes($params){
        $tiposer = $params['tiposer'];
        $tecnico = $params['tecnico'];
        $status = $params['status'];
        $zona = $params['zona'];
        $pcliente = $params['pcliente'];
        $fechainicio = $params['fechainicio'];
        $fechafin = $params['fechafin'];

        $columns = array( 
            0=>'asignacionId',
            1=>'servicio',
            2=>'tecnico',
            3=>'empresa',
            4=>'fecha',
            5=>'modelo',
            6=>'serie',
            7=>'cot_refaccion',
            8=>'prioridad',
            9=>'cot_status',
            10=>'id',
            11=>'idEquipo',
            12=>'prioridadr',
            13=>'cot_refaccion3',
            14=>'cot_refaccion2',
            15=>'cot_tipo',
            16=>'cot_referencia_tipo',
            17=>'cot_referencia',
            18=>'personalId',
            19=>'zonaId',
            20=>'nruta',
            21=>'status',
            22=>'tiposervicio',
            23=>'hora_comida',
            24=>'horafin_comida',
            25=>'cpev',
            26=>'horafin',
            37=>'asignacionIde',
            38=>'priorizar_cot',
            39=>'cot_status_ob',
            40=>'serieId',
            41=>'idrow',
            42=>'tecnico2',
            43=>'cot_contador',
            44=>'per_fin',
            45=>'tecnicof',
            46=>'prioridad2',
            47=>'prioridad3',
            48=>'cot_referencia2',
            49=>'cot_referencia2'

        );
        
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        //==============================================
            $tiposer_w1='';$tiposer_w2='';$tiposer_w3='';$tiposer_w4='';
            $tecnico_w='';
            $status_w1='';$status_w2='';$status_w3='';$status_w4='';
            $zona_w1='';$zona_w2='';$zona_w3='';$zona_w4='';
            $cliente_w=''; 
            $fechainicio_w1='';$fechafin_w1='';
            $fechainicio_w2='';$fechafin_w2='';
            $fechainicio_w3='';$fechafin_w3='';
            $fechainicio_w4='';$fechafin_w4='';
            if($fechainicio=='' and $fechafin==''){
                /*
                $fechainicio_w1=" and ace.fecha='$this->fechahoy' ";
                $fechainicio_w2=" and ape.fecha='$this->fechahoy' ";
                $fechainicio_w3=" and acl.fecha='$this->fechahoy' ";
                $fechainicio_w4=" and acl.fecha='$this->fechahoy' ";
                */
            }
            if ($fechainicio!='') {
                $fechainicio_w1=" and ace.fecha >='$fechainicio' ";
                $fechainicio_w2=" and ape.fecha >='$fechainicio' ";
                $fechainicio_w3=" and acl.fecha >='$fechainicio' ";
                $fechainicio_w4=" and acl.fecha >='$fechainicio' ";

            }
            if ($fechafin!='') {
                $fechafin_w1=" and ace.fecha <='$fechafin' ";
                $fechafin_w2=" and ape.fecha <='$fechafin' ";
                $fechafin_w3=" and acl.fecha <='$fechafin' ";
                $fechafin_w4=" and acl.fecha <='$fechafin' ";
            }

            if($tiposer==1){
                $tiposer_w1=' and ac.tiposervicio=0 ';$tiposer_w2=' and ap.tiposervicio=0 ';$tiposer_w3=' and acl.tiposervicio=0';$tiposer_w4=' and acl.tiposervicio=0 ';
            }
            if($tiposer==2){
                $tiposer_w1=' and ac.tiposervicio=1 ';$tiposer_w2=' and ap.tiposervicio=1 ';$tiposer_w3=' and acl.tiposervicio=1';$tiposer_w4=' and acl.tiposervicio=1 ';
            }
            if ($tecnico>0) {
                $tecnico_w=" and (per.personalId='$tecnico' or perf.personalId='$tecnico')";
            }
            if($status==1){
                $status_w1=' and ace.status>=0 and ace.status<=1 ';
                $status_w2=' and ape.status>=0 and ape.status<=1 ';
                $status_w3=' and acl.status>=0 and acl.status<=1 ';
                $status_w4=' and acl.status>=0 and acl.status<=1 ';
            }
            if($status==2){
                $status_w1=' and ace.status=2 ';
                $status_w2=' and ape.status=2 ';
                $status_w3=' and acl.status=2 ';
                $status_w4=' and acl.status=2 ';
            }
            if($zona>0){
                $zona_w1=" and ac.zonaId=$zona ";
                $zona_w2=" and ap.zonaId=$zona ";
                $zona_w3=" and acl.zonaId=$zona ";
                $zona_w4=" and acl.zonaId=$zona ";
            }
            if($pcliente>0){
                $cliente_w=" and cli.id=$pcliente ";
            }
        //==============================================
        $subconsulta="(
                        (SELECT 
    '1' as cpev,    ac.asignacionId,ace.asignacionIde, 
    per.personalId, CONCAT(per.nombre, ' ', per.apellido_paterno, ' ', per.apellido_materno) as tecnico,
                    CONCAT(per2.nombre, ' ', per2.apellido_paterno, ' ', per2.apellido_materno) as tecnico2, 
    cli.id,cli.empresa,     ace.fecha, ace.hora,ace.horafin,
    ac.zonaId,ru.nombre as nruta, 
    rde.modelo,serp.serie,     ace.status, 
    ac.tiposervicio,      sereve.nombre as servicio, 
    ace.cot_status,ace.cot_status_ob,    ace.cot_refaccion,
    ac.contratoId,    ace.cot_tipo, 
    ace.cot_referencia,ace.cot_referencia_tipo, 
    ace.hora_comida,ace.horafin_comida,
    rde.idEquipo,     ace.prioridad,0 as prioridadr,
    ace.cot_refaccion2,ace.cot_refaccion3,ace.priorizar_cot,
    ace.serieId,rde.id as idrow,'' as direccionservicio,ace.cot_contador, 
    ace.per_fin,
    CONCAT(perf.nombre, ' ', perf.apellido_paterno, ' ', perf.apellido_materno) as tecnicof,
    ace.prioridad2,ace.prioridad3,ace.cot_referencia2,ace.cot_referencia3

FROM asignacion_ser_contrato_a_e ace
JOIN asignacion_ser_contrato_a ac ON ac.asignacionId=ace.asignacionId
LEFT JOIN personal per ON per.personalId=ace.tecnico
LEFT JOIN personal per2 ON per2.personalId=ace.tecnico2
JOIN contrato con ON con.idcontrato=ac.contratoId
JOIN rentas ren ON ren.id=con.idRenta
JOIN clientes cli ON cli.id=ren.idCliente
JOIN rutas ru ON ru.id=ac.zonaId
JOIN rentas_has_detallesEquipos rde ON rde.id=ace.idequipo
JOIN series_productos serp ON serp.serieId=ace.serieId
LEFT JOIN personal perf ON perf.personalId=ace.per_fin
LEFT JOIN servicio_evento sereve ON sereve.id=ac.tservicio
WHERE ace.cot_status!=0 $tiposer_w1 $tecnico_w $status_w1 $zona_w1 $cliente_w $fechainicio_w1 $fechafin_w1 )

UNION
(
SELECT 
    '2' as cpev,
    ap.asignacionId,ape.asignacionIde,     per.personalId, CONCAT(per.nombre, ' ', per.apellido_paterno, ' ', per.apellido_materno) as tecnico, 
    CONCAT(per2.nombre, ' ', per2.apellido_paterno, ' ', per2.apellido_materno) as tecnico2, 
    cli.id,cli.empresa,     ape.fecha, ape.hora, ape.horafin,
    ap.zonaId,ru.nombre as nruta,     equ.modelo,ape.serie, 
    ape.status,     ap.tiposervicio, 
    sereve.nombre as servicio,     ape.cot_status, ape.cot_status_ob, 
    ape.cot_refaccion,     '' as contratoId,
    ape.cot_tipo,    ape.cot_referencia,ape.cot_referencia_tipo, 
    ape.hora_comida,ape.horafin_comida,    pocde.idEquipo, 
    ap.prioridad, 0 as prioridadr,    ape.cot_refaccion2,ape.cot_refaccion3,ape.priorizar_cot,
    '' as serieId,'' as idrow,pocde.direccionservicio,ape.cot_contador, 
    ape.per_fin,
    CONCAT(perf.nombre, ' ', perf.apellido_paterno, ' ', perf.apellido_materno) as tecnicof,
    ape.prioridad2,ape.prioridad3,ape.cot_referencia2,ape.cot_referencia3
FROM asignacion_ser_poliza_a_e ape
JOIN asignacion_ser_poliza_a ap ON ap.asignacionId=ape.asignacionId
LEFT JOIN personal per ON per.personalId=ape.tecnico
LEFT JOIN personal per2 ON per2.personalId=ape.tecnico2
JOIN polizasCreadas poc ON poc.id=ap.polizaId
JOIN clientes cli ON cli.id=poc.idCliente
JOIN rutas ru ON ru.id=ap.zonaId
JOIN polizasCreadas_has_detallesPoliza pocde ON pocde.id=ape.idequipo
JOIN equipos equ ON equ.id=pocde.idEquipo
LEFT JOIN personal perf ON perf.personalId=ape.per_fin
LEFT JOIN servicio_evento sereve ON sereve.id=ap.tservicio
WHERE ape.cot_status != 0 $tiposer_w2 $tecnico_w $status_w2 $zona_w2 $cliente_w $fechainicio_w2 $fechafin_w2
)
UNION
(
SELECT
    '3' as cpev, 
    acl.asignacionId,acld.asignacionIdd as asignacionIde, 
    per.personalId,     CONCAT(per.nombre, ' ', per.apellido_paterno, ' ', per.apellido_materno) as tecnico,
    CONCAT(per2.nombre, ' ', per2.apellido_paterno, ' ', per2.apellido_materno) as tecnico2, 
    cli.id,cli.empresa,     acl.fecha, acl.hora, acl.horafin, 
    acl.zonaId,zo.nombre as nruta,    equ.modelo,acld.serie,
    acl.status,    acl.tiposervicio,
    sereve.nombre as servicio,    acld.cot_status,acld.cot_status_ob,
    acld.cot_refaccion,    '' as contratoId,
    acld.cot_tipo,    acld.cot_referencia,acld.cot_referencia_tipo,
    acl.hora_comida,acl.horafin_comida,    equ.id as idEquipo,
    acl.prioridad,0 as prioridadr,    acld.cot_refaccion2,acld.cot_refaccion3,acld.priorizar_cot,
    '' as serieId,'' as idrow,acld.direccion as direccionservicio,acld.cot_contador,
    acl.per_fin,
    CONCAT(perf.nombre, ' ', perf.apellido_paterno, ' ', perf.apellido_materno) as tecnicof,
    acld.prioridad2,acld.prioridad3,acld.cot_referencia2,acld.cot_referencia3
FROM asignacion_ser_cliente_a acl
JOIN personal per ON per.personalId=acl.tecnico
LEFT JOIN personal per2 ON per2.personalId=acl.tecnico2
JOIN clientes cli ON cli.id=acl.clienteId
JOIN rutas zo ON zo.id=acl.zonaId
LEFT JOIN servicio_evento sereve ON sereve.id=acl.tservicio
LEFT JOIN asignacion_ser_cliente_a_d acld ON acld.asignacionId=acl.asignacionId
LEFT JOIN polizas_detalles polizad ON polizad.id=acld.tservicio_a
LEFT JOIN equipos equ ON equ.id=polizad.modelo
LEFT JOIN personal perf ON perf.personalId=acl.per_fin
WHERE acld.cot_status != 0 $tiposer_w3 $tecnico_w $status_w3 $zona_w3 $cliente_w $fechainicio_w3 $fechafin_w3
)
UNION
(
SELECT 
    '4' as cpev,
    acl.asignacionId,acld.asignacionIdd as asignacionIde,
    per.personalId, CONCAT(per.nombre, ' ', per.apellido_paterno, ' ', per.apellido_materno) as tecnico,
    CONCAT(per2.nombre, ' ', per2.apellido_paterno, ' ', per2.apellido_materno) as tecnico2, 
    cli.id,cli.empresa,     acl.fecha, acl.hora, acl.horafin, 
    acl.zonaId,zo.nombre as nruta,    equ.modelo,acld.serie,
    acl.status,    acl.tiposervicio,
    sereve.nombre as servicio,    acl.cot_status,acl.cot_status_ob,
    acl.cot_refaccion,    '' as contratoId,
    acl.cot_tipo,    acl.cot_referencia,acl.cot_referencia_tipo,
    acl.hora_comida,acl.horafin_comida,    equ.id as idEquipo,
    acl.prioridad,acl.prioridadr, acl.cot_refaccion2,acl.cot_refaccion3,acl.priorizar_cot,
    '' as serieId,'' as idrow,'' as direccionservicio,acl.cot_contador,
    acl.per_fin,
    CONCAT(perf.nombre, ' ', perf.apellido_paterno, ' ', perf.apellido_materno) as tecnicof,
    acl.prioridadr2 as prioridad2,acl.prioridadr3 as prioridad3,acl.cot_referencia2,acl.cot_referencia3
FROM asignacion_ser_venta_a acl
JOIN personal per ON per.personalId=acl.tecnico
LEFT JOIN personal per2 ON per2.personalId=acl.tecnico
JOIN clientes cli ON cli.id=acl.clienteId
JOIN rutas zo ON zo.id=acl.zonaId
LEFT JOIN servicio_evento sereve ON sereve.id=acl.tservicio
LEFT JOIN asignacion_ser_venta_a_d acld ON acld.asignacionId=acl.asignacionId
LEFT JOIN polizas_detalles pold ON pold.id=acld.tservicio_a
LEFT JOIN equipos equ ON equ.id=pold.modelo
LEFT JOIN personal perf ON perf.personalId=acl.per_fin
WHERE acl.cot_status != 0 $tiposer_w4 $tecnico_w $status_w4 $zona_w4 $cliente_w $fechainicio_w4 $fechafin_w4 )
                        ) as datos";
        $this->db->from($subconsulta);

        //$this->db->where(array('facturaabierta'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    public function getsolicitudes_total($params){
        $tiposer = $params['tiposer'];
        $tecnico = $params['tecnico'];
        $status = $params['status'];
        $zona = $params['zona'];
        $pcliente = $params['pcliente'];
        $fechainicio = $params['fechainicio'];
        $fechafin = $params['fechafin'];

        $columns = array( 
            0=>'asignacionId',
            1=>'servicio',
            2=>'tecnico',
            3=>'empresa',
            4=>'fecha',
            5=>'modelo',
            6=>'serie',
            7=>'cot_refaccion',
            8=>'prioridad',
            9=>'cot_status',
            10=>'id',
            11=>'idEquipo',
            12=>'prioridadr',
            13=>'cot_refaccion3',
            14=>'cot_refaccion2',
            15=>'cot_tipo',
            16=>'cot_referencia_tipo',
            17=>'cot_referencia',
            18=>'personalId',
            19=>'zonaId',
            20=>'nruta',
            21=>'status',
            22=>'tiposervicio',
            23=>'hora_comida',
            24=>'horafin_comida',
            25=>'cpev',
            26=>'horafin',
            27=>'asignacionIde',
            28=>'per_fin',
            29=>'tecnicof'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(*) as total');
        //==============================================
            $tiposer_w1='';$tiposer_w2='';$tiposer_w3='';$tiposer_w4='';
            $tecnico_w='';
            $status_w1='';$status_w2='';$status_w3='';$status_w4='';
            $zona_w1='';$zona_w2='';$zona_w3='';$zona_w4='';
            $cliente_w=''; 
            $fechainicio_w1='';$fechafin_w1='';
            $fechainicio_w2='';$fechafin_w2='';
            $fechainicio_w3='';$fechafin_w3='';
            $fechainicio_w4='';$fechafin_w4='';
            if($fechainicio=='' and $fechafin==''){
                /*
                $fechainicio_w1=" and ace.fecha='$this->fechahoy' ";
                $fechainicio_w2=" and ape.fecha='$this->fechahoy' ";
                $fechainicio_w3=" and acl.fecha='$this->fechahoy' ";
                $fechainicio_w4=" and acl.fecha='$this->fechahoy' ";
                */
            }
            if ($fechainicio!='') {
                $fechainicio_w1=" and ace.fecha >='$fechainicio' ";
                $fechainicio_w2=" and ape.fecha >='$fechainicio' ";
                $fechainicio_w3=" and acl.fecha >='$fechainicio' ";
                $fechainicio_w4=" and acl.fecha >='$fechainicio' ";

            }
            if ($fechafin!='') {
                $fechafin_w1=" and ace.fecha <='$fechafin' ";
                $fechafin_w2=" and ape.fecha <='$fechafin' ";
                $fechafin_w3=" and acl.fecha <='$fechafin' ";
                $fechafin_w4=" and acl.fecha <='$fechafin' ";
            }

            if($tiposer==1){
                $tiposer_w1=' and ac.tiposervicio=0 ';$tiposer_w2=' and ap.tiposervicio=0 ';$tiposer_w3=' and acl.tiposervicio=0';$tiposer_w4=' and acl.tiposervicio=0 ';
            }
            if($tiposer==2){
                $tiposer_w1=' and ac.tiposervicio=1 ';$tiposer_w2=' and ap.tiposervicio=1 ';$tiposer_w3=' and acl.tiposervicio=1';$tiposer_w4=' and acl.tiposervicio=1 ';
            }
            if ($tecnico>0) {
                $tecnico_w=" and (per.personalId='$tecnico' or perf.personalId='$tecnico')";
            }
            if($status==1){
                $status_w1=' and ace.status>=0 and ace.status<=1 ';
                $status_w2=' and ape.status>=0 and ape.status<=1 ';
                $status_w3=' and acl.status>=0 and acl.status<=1 ';
                $status_w4=' and acl.status>=0 and acl.status<=1 ';
            }
            if($status==2){
                $status_w1=' and ace.status=2 ';
                $status_w2=' and ape.status=2 ';
                $status_w3=' and acl.status=2 ';
                $status_w4=' and acl.status=2 ';
            }
            if($zona>0){
                $zona_w1=" and ac.zonaId=$zona ";
                $zona_w2=" and ap.zonaId=$zona ";
                $zona_w3=" and acl.zonaId=$zona ";
                $zona_w4=" and acl.zonaId=$zona ";
            }
            if($pcliente>0){
                $cliente_w=" and cli.id=$pcliente ";
            }
        //==============================================
                $subconsulta="(
                        (SELECT 
    1 as cpev,    ac.asignacionId,ace.asignacionIde, 
    per.personalId, CONCAT(per.nombre, ' ', per.apellido_paterno, ' ', per.apellido_materno) as tecnico, 
    cli.id,cli.empresa,     ace.fecha, ace.hora,ace.horafin,
    ac.zonaId,ru.nombre as nruta, 
    rde.modelo,serp.serie,     ace.status, 
    ac.tiposervicio,      sereve.nombre as servicio, 
    ace.cot_status,    ace.cot_refaccion,
    ac.contratoId,    ace.cot_tipo, 
    ace.cot_referencia,ace.cot_referencia_tipo, 
    ace.hora_comida,ace.horafin_comida,
    rde.idEquipo,     ace.prioridad,0 as prioridadr,
    ace.cot_refaccion2,ace.cot_refaccion3,
    ace.per_fin,
    CONCAT(perf.nombre, ' ', perf.apellido_paterno, ' ', perf.apellido_materno) as tecnicof
FROM asignacion_ser_contrato_a_e ace
JOIN asignacion_ser_contrato_a ac ON ac.asignacionId=ace.asignacionId
LEFT JOIN personal per ON per.personalId=ace.tecnico
JOIN contrato con ON con.idcontrato=ac.contratoId
JOIN rentas ren ON ren.id=con.idRenta
JOIN clientes cli ON cli.id=ren.idCliente
JOIN rutas ru ON ru.id=ac.zonaId
JOIN rentas_has_detallesEquipos rde ON rde.id=ace.idequipo
JOIN series_productos serp ON serp.serieId=ace.serieId
LEFT JOIN servicio_evento sereve ON sereve.id=ac.tservicio
LEFT JOIN personal perf ON perf.personalId=ace.per_fin
WHERE ace.cot_status!=0 $tiposer_w1 $tecnico_w $status_w1 $zona_w1 $cliente_w $fechainicio_w1 $fechafin_w1 )

UNION
(
SELECT 
    2 as cpev,
    ap.asignacionId,ape.asignacionIde,     per.personalId, CONCAT(per.nombre, ' ', per.apellido_paterno, ' ', per.apellido_materno) as tecnico, 
    cli.id,cli.empresa,     ape.fecha, ape.hora, ape.horafin,
    ap.zonaId,ru.nombre as nruta,     equ.modelo,ape.serie, 
    ape.status,     ap.tiposervicio, 
    sereve.nombre as servicio,     ape.cot_status, 
    ape.cot_refaccion,     '' as contratoId,
    ape.cot_tipo,    ape.cot_referencia,ape.cot_referencia_tipo, 
    ape.hora_comida,ape.horafin_comida,    pocde.idEquipo, 
    ap.prioridad, 0 as prioridadr,    ape.cot_refaccion2,ape.cot_refaccion3,
    ape.per_fin,
    CONCAT(perf.nombre, ' ', perf.apellido_paterno, ' ', perf.apellido_materno) as tecnicof
FROM asignacion_ser_poliza_a_e ape
JOIN asignacion_ser_poliza_a ap ON ap.asignacionId=ape.asignacionId
LEFT JOIN personal per ON per.personalId=ape.tecnico
JOIN polizasCreadas poc ON poc.id=ap.polizaId
JOIN clientes cli ON cli.id=poc.idCliente
JOIN rutas ru ON ru.id=ap.zonaId
JOIN polizasCreadas_has_detallesPoliza pocde ON pocde.id=ape.idequipo
JOIN equipos equ ON equ.id=pocde.idEquipo
LEFT JOIN servicio_evento sereve ON sereve.id=ap.tservicio
LEFT JOIN personal perf ON perf.personalId=ape.per_fin
WHERE ape.cot_status != 0 $tiposer_w2 $tecnico_w $status_w2 $zona_w2 $cliente_w $fechainicio_w2 $fechafin_w2
)
UNION
(
SELECT
    3 as cpev, 
    acl.asignacionId,acld.asignacionIdd as asignacionIde, 
    per.personalId,     CONCAT(per.nombre, ' ', per.apellido_paterno, ' ', per.apellido_materno) as tecnico, 
    cli.id,cli.empresa,     acl.fecha, acl.hora, acl.horafin, 
    acl.zonaId,zo.nombre as nruta,    equ.modelo,acld.serie,
    acl.status,    acl.tiposervicio,
    sereve.nombre as servicio,    acld.cot_status,
    acld.cot_refaccion,    '' as contratoId,
    acld.cot_tipo,    acld.cot_referencia,acld.cot_referencia_tipo,
    acl.hora_comida,acl.horafin_comida,    equ.id as idEquipo,
    acl.prioridad,0 as prioridadr,    acld.cot_refaccion2,acld.cot_refaccion3,
    acl.per_fin,
    CONCAT(perf.nombre, ' ', perf.apellido_paterno, ' ', perf.apellido_materno) as tecnicof 
FROM asignacion_ser_cliente_a acl
JOIN personal per ON per.personalId=acl.tecnico
JOIN clientes cli ON cli.id=acl.clienteId
JOIN rutas zo ON zo.id=acl.zonaId
LEFT JOIN servicio_evento sereve ON sereve.id=acl.tservicio
LEFT JOIN asignacion_ser_cliente_a_d acld ON acld.asignacionId=acl.asignacionId
LEFT JOIN polizas_detalles polizad ON polizad.id=acld.tservicio_a
LEFT JOIN equipos equ ON equ.id=polizad.modelo
LEFT JOIN personal perf ON perf.personalId=acl.per_fin
WHERE acld.cot_status != 0 $tiposer_w3 $tecnico_w $status_w3 $zona_w3 $cliente_w $fechainicio_w3 $fechafin_w3
)
UNION
(
SELECT 
    4 as cpev,
    acl.asignacionId,acld.asignacionIdd as asignacionIde,
    per.personalId, CONCAT(per.nombre, ' ', per.apellido_paterno, ' ', per.apellido_materno) as tecnico, 
    cli.id,cli.empresa,     acl.fecha, acl.hora, acl.horafin, 
    acl.zonaId,zo.nombre as nruta,    equ.modelo,acld.serie,
    acl.status,    acl.tiposervicio,
    sereve.nombre as servicio,    acl.cot_status,
    acl.cot_refaccion,    '' as contratoId,
    acl.cot_tipo,    acl.cot_referencia,acl.cot_referencia_tipo,
    acl.hora_comida,acl.horafin_comida,    equ.id as idEquipo,
    acl.prioridad,acl.prioridadr, acl.cot_refaccion2,acl.cot_refaccion3,
    acl.per_fin,
    CONCAT(perf.nombre, ' ', perf.apellido_paterno, ' ', perf.apellido_materno) as tecnicof 
FROM asignacion_ser_venta_a acl
JOIN personal per ON per.personalId=acl.tecnico
JOIN clientes cli ON cli.id=acl.clienteId
JOIN rutas zo ON zo.id=acl.zonaId
LEFT JOIN servicio_evento sereve ON sereve.id=acl.tservicio
LEFT JOIN asignacion_ser_venta_a_d acld ON acld.asignacionId=acl.asignacionId
LEFT JOIN polizas_detalles pold ON pold.id=acld.tservicio_a
LEFT JOIN equipos equ ON equ.id=pold.modelo
LEFT JOIN personal perf ON perf.personalId=acl.per_fin
WHERE acl.cot_status != 0 $tiposer_w4 $tecnico_w $status_w4 $zona_w4 $cliente_w $fechainicio_w4 $fechafin_w4 )
                        ) as datos";
        $this->db->from($subconsulta);
        //$where = ;
        //$this->db->where(array('facturaabierta'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        //$this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        //$this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query->row()->total;
    }
    function infoequipossolicitudrefacciones($idser,$tipo){
        if($tipo==1){ //contrato
            $strq="SELECT 
                        '1' as cpev,    ac.asignacionId,ace.asignacionIde, 
                        per.personalId, CONCAT(per.nombre, ' ', per.apellido_paterno, ' ', per.apellido_materno) as tecnico,
                                        CONCAT(per2.nombre, ' ', per2.apellido_paterno, ' ', per2.apellido_materno) as tecnico2, 
                        cli.id,cli.empresa,     ace.fecha, ace.hora,ace.horafin,
                        ac.zonaId,ru.nombre as nruta, 
                        rde.modelo,serp.serie,     ace.status, 
                        ac.tiposervicio,      sereve.nombre as servicio, 
                        ace.cot_status,ace.cot_status_ob,    ace.cot_refaccion,
                        ac.contratoId,    ace.cot_tipo, 
                        ace.cot_referencia,ace.cot_referencia_tipo, 
                        ace.hora_comida,ace.horafin_comida,
                        rde.idEquipo,
                        ace.prioridad,
                        ace.prioridad2,
                        ace.prioridad3,
                        0 as prioridadr,
                        ace.cot_refaccion2,ace.cot_refaccion3,ace.priorizar_cot,
                        ace.serieId,rde.id as idrow,'' as direccionservicio,
                        ace.cot_contador

                    FROM asignacion_ser_contrato_a_e ace
                    JOIN asignacion_ser_contrato_a ac ON ac.asignacionId=ace.asignacionId
                    LEFT JOIN personal per ON per.personalId=ace.tecnico
                    LEFT JOIN personal per2 ON per2.personalId=ace.tecnico2
                    JOIN contrato con ON con.idcontrato=ac.contratoId
                    JOIN rentas ren ON ren.id=con.idRenta
                    JOIN clientes cli ON cli.id=ren.idCliente
                    JOIN rutas ru ON ru.id=ac.zonaId
                    JOIN rentas_has_detallesEquipos rde ON rde.id=ace.idequipo
                    JOIN series_productos serp ON serp.serieId=ace.serieId
                    LEFT JOIN servicio_evento sereve ON sereve.id=ac.tservicio
                    WHERE /*ace.cot_status!=0 and*/ ac.asignacionId='$idser' ";
        }
        if($tipo==2){ //poliza
            $strq="SELECT 
                    '2' as cpev,
                    ap.asignacionId,ape.asignacionIde,     per.personalId, CONCAT(per.nombre, ' ', per.apellido_paterno, ' ', per.apellido_materno) as tecnico, 
                    CONCAT(per2.nombre, ' ', per2.apellido_paterno, ' ', per2.apellido_materno) as tecnico2, 
                    cli.id,cli.empresa,     ape.fecha, ape.hora, ape.horafin,
                    ap.zonaId,ru.nombre as nruta,     equ.modelo,ape.serie, 
                    ape.status,     ap.tiposervicio, 
                    sereve.nombre as servicio,     ape.cot_status, ape.cot_status_ob, 
                    ape.cot_refaccion,     '' as contratoId,
                    ape.cot_tipo,    ape.cot_referencia,ape.cot_referencia_tipo, 
                    ape.hora_comida,ape.horafin_comida,    pocde.idEquipo, 
                    ape.prioridad,
                    ape.prioridad2,
                    ape.prioridad3,
                    0 as prioridadr,    ape.cot_refaccion2,ape.cot_refaccion3,ape.priorizar_cot,
                    '' as serieId,'' as idrow,pocde.direccionservicio,
                    ape.cot_contador
                FROM asignacion_ser_poliza_a_e ape
                JOIN asignacion_ser_poliza_a ap ON ap.asignacionId=ape.asignacionId
                LEFT JOIN personal per ON per.personalId=ape.tecnico
                LEFT JOIN personal per2 ON per2.personalId=ape.tecnico2
                JOIN polizasCreadas poc ON poc.id=ap.polizaId
                JOIN clientes cli ON cli.id=poc.idCliente
                JOIN rutas ru ON ru.id=ap.zonaId
                JOIN polizasCreadas_has_detallesPoliza pocde ON pocde.id=ape.idequipo
                JOIN equipos equ ON equ.id=pocde.idEquipo
                LEFT JOIN servicio_evento sereve ON sereve.id=ap.tservicio
                WHERE /*ape.cot_status != 0 and */ ap.asignacionId='$idser' ";
        }
        if($tipo==3){ //cliente
            $strq="SELECT
                    '3' as cpev, 
                    acl.asignacionId,acld.asignacionIdd as asignacionIde, 
                    per.personalId,     CONCAT(per.nombre, ' ', per.apellido_paterno, ' ', per.apellido_materno) as tecnico,
                    CONCAT(per2.nombre, ' ', per2.apellido_paterno, ' ', per2.apellido_materno) as tecnico2, 
                    cli.id,cli.empresa,     acl.fecha, acl.hora, acl.horafin, 
                    acl.zonaId,zo.nombre as nruta,    equ.modelo,acld.serie,
                    acl.status,    acl.tiposervicio,
                    sereve.nombre as servicio,    acld.cot_status,acld.cot_status_ob,
                    acld.cot_refaccion,    '' as contratoId,
                    acld.cot_tipo,    acld.cot_referencia,acld.cot_referencia_tipo,
                    acl.hora_comida,acl.horafin_comida,    equ.id as idEquipo,
                    0 as prioridadr,    acld.cot_refaccion2,acld.cot_refaccion3,acld.priorizar_cot,
                    '' as serieId,'' as idrow,acld.direccion as direccionservicio,
                    acld.prioridad,acld.prioridad2,acld.prioridad3,
                    acld.cot_contador
                FROM asignacion_ser_cliente_a acl
                JOIN personal per ON per.personalId=acl.tecnico
                LEFT JOIN personal per2 ON per2.personalId=acl.tecnico2
                JOIN clientes cli ON cli.id=acl.clienteId
                JOIN rutas zo ON zo.id=acl.zonaId
                LEFT JOIN servicio_evento sereve ON sereve.id=acl.tservicio
                LEFT JOIN asignacion_ser_cliente_a_d acld ON acld.asignacionId=acl.asignacionId
                LEFT JOIN polizas_detalles polizad ON polizad.id=acld.tservicio_a
                LEFT JOIN equipos equ ON equ.id=polizad.modelo
                WHERE /*acld.cot_status != 0 and */ acl.asignacionId='$idser' ";
        }
        if($tipo==4){ //venta
            $strq="SELECT 
                    '4' as cpev,
                    acl.asignacionId,acld.asignacionIdd as asignacionIde,
                    per.personalId, CONCAT(per.nombre, ' ', per.apellido_paterno, ' ', per.apellido_materno) as tecnico,
                    CONCAT(per2.nombre, ' ', per2.apellido_paterno, ' ', per2.apellido_materno) as tecnico2, 
                    cli.id,cli.empresa,     acl.fecha, acl.hora, acl.horafin, 
                    acl.zonaId,zo.nombre as nruta,    equ.modelo,acld.serie,
                    acl.status,    acl.tiposervicio,
                    sereve.nombre as servicio,    acl.cot_status,acl.cot_status_ob,
                    acl.cot_refaccion,    '' as contratoId,
                    acl.cot_tipo,    acl.cot_referencia,acl.cot_referencia_tipo,
                    acl.hora_comida,acl.horafin_comida,    equ.id as idEquipo,
                    acl.prioridadr, acl.cot_refaccion2,acl.cot_refaccion3,acl.priorizar_cot,
                    '' as serieId,'' as idrow,'' as direccionservicio,
                    acl.prioridadr as prioridad,acl.prioridadr2 as prioridad2,acl.prioridadr3 as prioridad3,
                    acl.cot_contador
                FROM asignacion_ser_venta_a acl
                JOIN personal per ON per.personalId=acl.tecnico
                LEFT JOIN personal per2 ON per2.personalId=acl.tecnico
                JOIN clientes cli ON cli.id=acl.clienteId
                JOIN rutas zo ON zo.id=acl.zonaId
                LEFT JOIN servicio_evento sereve ON sereve.id=acl.tservicio
                LEFT JOIN asignacion_ser_venta_a_d acld ON acld.asignacionId=acl.asignacionId
                LEFT JOIN polizas_detalles pold ON pold.id=acld.tservicio_a
                LEFT JOIN equipos equ ON equ.id=pold.modelo
                WHERE acl.cot_status != 0 and acl.asignacionId='$idser' ";
        }
        $query = $this->db->query($strq);
        return $query;
    }

}