var base_url = $('#base_url').val();
$(document).ready(function($) {
	
});
function parametrosfactura(idcon){
    
        setTimeout(function(){ 
            ejecutafunciones();
            $('#FormaPago').change();
        }, 1000);
	$.ajax({
        type:'POST',
        url: base_url+"index.php/Configuracionrentas_ext/parametrosfactura",
        data: {
            idcon:idcon
        },
        success: function (data){
        	$('#test11').html(data);
        }
    });
}
function ejecutafunciones(){
    $('#FormaPago').change(function(event) {
        if($('#FormaPago').val()=='PPD'){
            $('#MetodoPago option').prop('disabled',true);
            $('#MetodoPago').val('99').trigger('change');
            $('#MetodoPago option[value=99]').prop('disabled',false);
            $('#MetodoPago').select2();
        }
        if($('#FormaPago').val()=='PUE'){
            $('#MetodoPago option').prop('disabled',false);
            $('#MetodoPago option[value=99]').prop('disabled',true);
            $('#MetodoPago').select2();
        }
    });
    verficartiporfc();
}
function verficartiporfc(){
    var rfc = $('#rfc').val();
    if(rfc=='XAXX010101000'){
        var html='<div class="col s4">\
                    <label>Periodicidad</label>\
                    <select class="browser-default form-control-bmz" name="pg_periodicidad" id="pg_periodicidad" onchange="v_periocidad()">\
                      <option value="01">01 Diario</option>\
                      <option value="02">02 Semanal</option>\
                      <option value="03">03 Quincenal</option>\
                      <option value="04">04 Mensual</option>\
                      <option value="05">05 Bimestral</option>\
                    </select>\
                  </div>\
                  <div class="col s4">\
                    <label>Mes</label>\
                    <select class="browser-default form-control-bmz" name="pg_meses" id="pg_meses">\
                      <option value="01" class="select_no_bimestral">01 Enero</option>\
                      <option value="02" class="select_no_bimestral">02 Febrero</option>\
                      <option value="03" class="select_no_bimestral">03 Marzo</option>\
                      <option value="04" class="select_no_bimestral">04 Abril</option>\
                      <option value="05" class="select_no_bimestral">05 Mayo</option>\
                      <option value="06" class="select_no_bimestral">06 Junio</option>\
                      <option value="07" class="select_no_bimestral">07 Julio</option>\
                      <option value="08" class="select_no_bimestral">08 Agosto</option>\
                      <option value="09" class="select_no_bimestral">09 Septiembre</option>\
                      <option value="10" class="select_no_bimestral">10 Octubre</option>\
                      <option value="11" class="select_no_bimestral">11 Noviembre</option>\
                      <option value="12" class="select_no_bimestral">12 Diciembre</option>\
                      <option value="13" class="select_bimestral">13 Enero-Febrero</option>\
                      <option value="14" class="select_bimestral">14 Marzo-Abril</option>\
                      <option value="15" class="select_bimestral">15 Mayo-Junio</option>\
                      <option value="16" class="select_bimestral">16 Julio-Agosto</option>\
                      <option value="17" class="select_bimestral">17 Septiembre-Octubre</option>\
                      <option value="18" class="select_bimestral">18 Noviembre-Diciembre</option>\
                    </select>\
                  </div>\
                  <div class="col s4">\
                    <label>Año</label>\
                    <input type="number" name="pg_anio" id="pg_anio" class="form-control-bmz" readonly>\
                  </div>';
        $('.agregardatospublicogeneral').html(html);
        setTimeout(function(){ 
            var mesactual = $('#mesactual').val();
            var anioactual = $('#anioactual').val();
            console.log(mesactual);
            console.log(anioactual);
            $('#pg_meses').val(mesactual);
            $('#pg_anio').val(anioactual);
            $('.select_bimestral').hide('show');
        }, 1000);
    }else{
        $('.agregardatospublicogeneral').html('');
    }
    obtenerinfofiscal();
}
function obtenerinfofiscal(){
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Facturaslis/viewdatosfiscalesinfo",
        data: {
            cliente:$('#idCliente').val(),
            rfc:$('#rfc').val(),
        },
        success: function (response){
            console.log(response);
            var array = $.parseJSON(response);
            //$('.v_nomfical').html(array.nombrefiscal);
            if(array.cp!=''){
                var cp=array.cp;
            }else{
                var cp='<span style="color:red"><b>Sin Direccion Fiscal</b></span>';
            }
            //$('.v_direccionfiscal').html(cp);
            if(array.regimen!=''){
                var regimen=array.regimen;
            }else{
                var regimen='<span style="color:red"><b>Sin regimen fiscal</b></span>';
            }
            //$('.v_regimenfiscal').html(regimen);
            var infodf='<div class="col s4">\
                          <b>Nombre Fiscal: </b><br><span >'+array.nombrefiscal+'</span>\
                        </div>\
                        <div class="col s4">\
                          <b>Direccion Fiscal(C.P.): </b><br><span >'+cp+'</span>\
                        </div>\
                        <div class="col s4">\
                          <b>Regimen Fiscal Receptor: </b><br><span >'+regimen+'</span>\
                        </div>';
            $('.infodatosfiscales').html(infodf);
            $( "#uso_cfdi ."+array.regimennumber ).prop( "disabled", false ).trigger('change');
            $( "#uso_cfdi" ).trigger("chosen:updated");
            selectusocfdi(array.regimenclave);
        },
        error: function(response){
            $.alert({
                    boxWidth: '30%',
                    useBootstrap: false,
                    title: 'Error!',
                    content: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema'});   
             
        }
    });
}
function selectusocfdi(clave){
    clave=parseInt(clave);
    switch (clave) {
      case 601:
            $('#uso_cfdi').val('G03');
        break;
      case 603:
        $('#uso_cfdi').val('G03');
        break;
      case 606:
        $('#uso_cfdi').val('G03');
        break;
      case 612:
        $('#uso_cfdi').val('G03');
        break;
      case 620:
        $('#uso_cfdi').val('G03');
        break;
      case 621:
        $('#uso_cfdi').val('G03');
        break;
      case 622:
        $('#uso_cfdi').val('G03');
        break;
      case 623:
        $('#uso_cfdi').val('G03');
        break;
      case 624:
        $('#uso_cfdi').val('G03');
        break;
      case 625:
        $('#uso_cfdi').val('G03');
        break;
      case 626:
        $('#uso_cfdi').val('G03');
        break;
      default:
        console.log(clave);
    }
    $( "#uso_cfdi" ).trigger("chosen:updated");
}
function savedf(){
    var form =$('#validateSubmitForm_df');
    if (form.valid()) {
        var datos=form.serialize();
        $.ajax({
            type:'POST',
            url: base_url+"index.php/Configuracionrentas_ext/savedf",
            data: datos,
            statusCode:{
                404: function(data){
                    toastr["error"]("No se pudo pocesar la solicitud","Error"); 
                },
                500: function(data){
                    toastr["error"]("No se pudo pocesar la solicitud","Error"); 
                    console.log(data);
                }
            },
            success: function (data){
                $('#tab_li_11').click();
            }
        });
    }else{
        toastr.error('Faltan campos obligatorios');
    }
}