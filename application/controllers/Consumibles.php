<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/******** Esta parte es para poder leer archivo Excel ***********/
require_once APPPATH.'/third_party/Spout/Autoloader/autoload.php';
use Box\Spout\Reader\ReaderFactory;  
use Box\Spout\Common\Type;
/****************************************************************/
class Consumibles extends CI_Controller{
    public function __construct(){
        parent::__construct();
        $this->load->model('Consumibles_model', 'model');
        $this->load->model('login_model');
        $this->load->model('ModeloCatalogos');
        $this->load->helper('url');
        $this->idpersonal = $this->session->userdata('idpersonal');
        $this->perfilid =$this->session->userdata('perfilid');
    }

    // Listado de Consumibles
    function index(){
        $data['MenusubId']=6;
        $data['perfilid']=$this->perfilid;
        $this->load->view('header');
        $this->load->view('main');
        $this->load->view('consumibles/listado',$data);
        $this->load->view('consumibles/listado_js');
        $this->load->view('footer');    
    }

    // Alta de consumible
    function alta(){
        $data['MenusubId']=6;
        $data['perfilid']=$this->perfilid;
        $equipo = $this->model->getDataEquipos();
        $data['equipo'] = $equipo;
        $data['tipoVista'] = 1;

        $this->load->view('header');
        $this->load->view('main',$data);
        $this->load->view('consumibles/form_consumibles');
        $this->load->view('consumibles/form_consumibles_js');
        $this->load->view('footer');  
    }

    // Edicion de Consumible
    function edicion($idConsumible){
        $data['MenusubId']=6;
        $data['perfilid']=$this->perfilid;
        $equipo = $this->model->getDataEquipos();
        $data['equipo'] = $equipo;
        $data['consumible'] = $this->model->getConsumiblePorId($idConsumible);
        $data['tipoVista'] = 2;

        $this->load->view('header');
        $this->load->view('main',$data);
        $this->load->view('consumibles/form_consumibles');
        $this->load->view('consumibles/form_consumibles_js');
        $this->load->view('footer');   
    }

    // Visualización de Consumible
    function visualizar($idConsumible){
        $data['MenusubId']=6;
        $data['perfilid']=$this->perfilid;
        $equipo = $this->model->getDataEquipos();
        $data['equipo'] = $equipo;
        $data['consumible'] = $this->model->getConsumiblePorId($idConsumible);
        $data['tipoVista'] = 3;

        $this->load->view('header');
        $this->load->view('main',$data);
        $this->load->view('consumibles/form_consumibles');
        $this->load->view('consumibles/form_consumibles_js');
        $this->load->view('footer');     
    }

    // Eliminar empleados
    function eliminarConsumible($idConsumible){
        $datosConsumible = array('status' => 0);
        $eliminado = $this->model->update_consumibles($datosConsumible, $idConsumible);
        echo $eliminado;
        //$this->Modelobitacoras->Insert(array('contenido'=>'Se Elimino Consumible','nombretabla'=>'consumibles','idtable'=>$idConsumible,'tipo'=>'Delete','personalId'=>$this->idpersonal));
    }

    function getMenus($idPerfil){
        // Obtenemos los menús del perfil
        $menus = $this->login_model->getMenus($idPerfil);
        $submenusArray = array();
        // Obtenemos y asignamos los submenus 
        foreach ($menus as $item)
        {
            array_push($submenusArray, $this->login_model->submenus($idPerfil,$item->MenuId));
        }
        // Los asignamos al arary que se envía a la vista
        $data['menus'] = $menus;
        $data['submenusArray'] = $submenusArray;
        return $data;
    }

    public function insertaActualizaConsumibles(){
        $result = 0; 
        $data = $this->input->post();
        $equipos = $this->input->post('equipos2');
        
        $rendimiento_toner=$data["rendimiento_toner"];
        $tipo=$data["tipo"];
        if(isset($data['idConsumible'])){
            $idConsumible=$data['idConsumible'];
        }else{
            $idConsumible=0;
        }
           // Ordenamos los datos principales del cliente
           $datosConsumibles = array(
                        'modelo' => $data["modelo"],
                        'parte' => $data["parte"],
                        'stock' => $data["stock"],
                        'observaciones' => $data["observaciones"],
                        'rendimiento_toner' => $rendimiento_toner,
                        'rendimiento_unidad_imagen' => $data["rendimiento_unidad_imagen"],
                        'tipo' => $data["tipo"]
                        );
         
            if(!isset($data['idConsumible'])){
                $id = $this->model->insertar_consumibles($datosConsumibles);
                $result = $id;
                if($result > 0){
                    /*$datosConsumibles_costos = array(
                                'general' => $data["general"],
                                'iva' => $data["iva"],
                                'neto' => $data["neto"],
                                'frecuente' => $data["frecuente"],
                                'iva2' => $data["iva2"],
                                'neto2' => $data["neto2"],
                                'especial' => $data["especial"],
                                'iva3' => $data["iva3"],
                                'neto3' => $data["neto3"],
                                'poliza' => $data["poliza"],
                                'iva4' => $data["iva4"],
                                'neto4' => $data["neto4"],                            
                                'consumibles_id' => $result
                                );
                    $this->model->insertar_consumibles_costos($datosConsumibles_costos); 
                    */
                    $datosConsumibles_costos = array(
                        'general' => 0,
                        'frecuente' => 0,
                        'especial' => 0,
                        'poliza' => 0,
                        'consumibles_id' => $id,
                    );

                    $this->model->insertar_consumibles_costos($datosConsumibles_costos); 
                    //$this->Modelobitacoras->Insert(array('contenido'=>'Se Inserto Consumible','nombretabla'=>'consumibles','idtable'=>$id,'tipo'=>'Insert','personalId'=>$this->idpersonal));
               } 
               $this->ModeloCatalogos->updateuuid($id,3);
            }else{   

                $DAT = json_decode($equipos);
                for ($i = 0; $i < count($DAT); $i++) {
                    $idEquipo_1 = $DAT[$i]->idEquipo;
                    if ($idEquipo_1!=0) {
                           $e = $idEquipo_1;
                        }else{
                           $e = 0; 
                        }      
                }              
                $datos = $this->model->verificarequipo($e,$data['idConsumible']);
                $equi=0;
                foreach ($datos as $item) {
                   $equi = $item->id;
                }
                if($equi>=1){
                   $result = 'existe';           
                }else{
                    // update

                    $id=$data['idConsumible'];
                    $result = $id;
                    unset($data['idConsumible']);  
                    $this->model->update_consumibles($datosConsumibles,$id); 
                    //$this->Modelobitacoras->Insert(array('contenido'=>'Se Actualizo Consumible','nombretabla'=>'consumibles','idtable'=>$id,'tipo'=>'Update','personalId'=>$this->idpersonal));
                    /*
                    $datosConsumibles_costos = array(
                                    'general' => $data["general"],
                                    'iva' => $data["iva"],
                                    'neto' => $data["neto"],
                                    'frecuente' => $data["frecuente"],
                                    'iva2' => $data["iva2"],
                                    'neto2' => $data["neto2"],
                                    'especial' => $data["especial"],
                                    'iva3' => $data["iva3"],
                                    'neto3' => $data["neto3"],
                                    'poliza' => $data["poliza"],
                                    'iva4' => $data["iva4"],
                                    'neto4' => $data["neto4"]
                                    );
                    $this->model->update_consumibles_costos($datosConsumibles_costos,$data['idConsumiblesCostos']);
                    */
                 }
                
            }


            $DATA = json_decode($equipos);
                for ($i = 0; $i < count($DATA); $i++) {
                    $idEquipo = $DATA[$i]->idEquipo;
                        $temp=array(
                            "idEquipo"=>$idEquipo,
                            "idConsumibles"=>$id
                        );
                        if ($idEquipo>=1) {
                            $this->model->insertar_equipos_has_consumibles($temp,"equipos_has_consumibles");
                        }      
                }
        
            if($idConsumible>0){

                $resultados=$this->ModeloCatalogos->getselectwheren('consumibles_costos',array('consumibles_id'=>$idConsumible));
                $poliza=0;
                foreach ($resultados->result() as $item) {
                    $poliza=$item->poliza;
                }
                log_message('error','$poliza='.$poliza.' $rendimiento_toner='.$rendimiento_toner);
                if($poliza>0){
                    if($rendimiento_toner>0){
                        if ($tipo==5 || $tipo==2 || $tipo==3 || $tipo==4 || $tipo==7) {
                            
                            if (floatval($rendimiento_toner)>0) {
                                
                                $costopaginamono=0;
                                $costopaginacolor=floatval($poliza)/floatval($rendimiento_toner);
                            }
                        }
                        if ($tipo==1 || $tipo==6) {
            
                            if (floatval($rendimiento_toner)>0) {
                                $costopaginamono=floatval($poliza)/floatval($rendimiento_toner);
                                $costopaginacolor=0;
                            }
                        }

                        $this->ModeloCatalogos->updateCatalogo('equipos_consumibles',array('costo_pagina_monocromatica'=>$costopaginamono,'costo_pagina_color'=>$costopaginacolor),array('idconsumible'=>$idConsumible,'activo'=>1));




                    }
                }
            }
        
             
        echo $result;  
    }

    function cargafiles(){

        $consumibles = $this->input->post('consumibles');
        $upload_folder ='uploads/consumibles';
        $nombre_archivo = $_FILES['foto']['name'];
        $tipo_archivo = $_FILES['foto']['type'];
        $tamano_archivo = $_FILES['foto']['size'];
        $tmp_archivo = $_FILES['foto']['tmp_name'];
        //$archivador = $upload_folder . '/' . $nombre_archivo;
        $fecha=date('ymd-His');
        $newfile='r_-'.$fecha.'-'.$nombre_archivo;        
        $archivador = $upload_folder . '/'.$newfile;
        if (!move_uploaded_file($tmp_archivo, $archivador)) {
            $return = Array('ok' => FALSE, 'msg' => 'Ocurrió un error al subir el archivo. No pudo guardarse.', 'status' => 'error');
        }else{
            $arrayfoto = array('foto' => $newfile, );
            $this->model->update_foto($arrayfoto,$consumibles);
            $return = Array('ok'=>TRUE);
        }
        echo json_encode($return);
    }

    public function getListadoConsumibles(){
        $consumibles = $this->model->getListadoConsumibles();
        $json_data = array("data" => $consumibles);
        echo json_encode($json_data);
    }

    function getListadoHas_consumibles($id){   
        $consumible = $this->model->getListadoHas_consumibles($id);
        $json_data = array("data" => $consumible);
        echo json_encode($json_data);
    }

    function eliminar_has_consumibles($id){
        $eliminado = $this->model->eliminar_has_consumibles($id);
        echo $eliminado;
    }

    // Función para guardar el archivo de Excel en la BD
    function cargaArchivo(){

        // Borramos la tabla donde se encuentran las asignaciones hoy
        // no se pueden borrar o truncar estas tablas por que hay tablas relacionadas
        //$this->model->truncateTable('consumibles');
        //$this->model->truncateTable('consumibles_costos');            
        /***** Acá empieza la parte para poder leer el archivo e insertarlo en la BD *****/
        
        // Nombre Temporal del Archivo
        $inputFileName = $_FILES['inputFile']['tmp_name']; 
        
        //Lee el Archivo usando ReaderFactory
        $reader = ReaderFactory::create(Type::XLSX);
        
        //Esta linea mantiene el formato de nuestras horas y fechas
        //Sin esta linea Spout convierte la hora y fecha a su propio formato predefinido como DataTime
        $reader->setShouldFormatDates(true);

        // Abrimos el archivo
        $reader->open($inputFileName);
        $count = 1;

        // Numero de hojas en el documento EXCEL 
        foreach ($reader->getSheetIterator() as $sheet) 
        {
            // Numero de filas en el documento EXCEL
            foreach ($sheet->getRowIterator() as $row) 
            {
                // Lee los Datos despues del encabezado
                // El encabezado se encuentra en la primera fila y debe omitirse, 
                // por eso la variable $count se inicializa en 1 y no en 0
                if($count > 1) 
                {
                    $data1 = array(
                        'foto' => '',
                        'modelo' => $row[0],
                        'parte' => $row[1],
                        //'stock' => $row[2],
                        'observaciones' => $row[2],
                        'status' => 1,
                        //'rendimiento' =>$row[4],
                        'rendimiento_unidad_imagen' =>$row[3],
                        'rendimiento_toner' =>$row[4],
                        'tipo' =>$row[5],
                    ); 
                    $insertado = $this->model->insertar_consumibles($data1);

                    if($insertado > 0){
                        $cambio = $this->model->getTipoCambioConsumibles();
                        $tipoCambio = $cambio[0]->cambio;
                        $precio =$row[6];
                        $descuentos = $this->consumibles_model->getDescuentosConsumibles();
                        $descuentoConsumibles = $descuentos[0]->descuento;
                        //======================================================================
                            $precioPesos1=$precio*$tipoCambio;
                            $precioPesos2=100-$descuentoConsumibles;
                        //=================================================================
                        $precioPesos = (($precioPesos1)*($precioPesos2))/100;

                        $porcentajesGanancia = $this->consumibles_model->getPorcentajeGananciaConsumibles();

            // Guardamos cada uno de los porcentajes correspondientes
            // VENTA = GENERAL; RENTA = ESPECIAL; POLIZA = FRECUENTE; REVENDEDOR = POLIZA
            $porcentajeGeneral = $porcentajesGanancia[0]->gananciaVenta;
            $porcentajeEspecial = $porcentajesGanancia[0]->gananciaRenta;
            $porcentajeFrecuente = $porcentajesGanancia[0]->gananciaPoliza;
            $porcentajePoliza = $porcentajesGanancia[0]->gananciaRevendedor;


                        


                        // Obtenemos el total de ganancia para cada tarea
                        //$gananciaGeneral = ($porcentajeGeneral * $precioPesos) / 100;
                        //$gananciaEspecial = ($porcentajeEspecial * $precioPesos) / 100;
                        //$gananciaFrecuente = ($porcentajeFrecuente * $precioPesos) / 100;
                        //$gananciaPoliza = ($porcentajePoliza * $precioPesos) / 100;

                        // Sumamos las ganancias al precio
                        //$precioConGananciaGeneral = $precioPesos + $gananciaGeneral;
                        //$precioConGananciaEspecial = $precioPesos + $gananciaEspecial;
                        //$precioConGananciaFrecuente = $precioPesos + $gananciaFrecuente;
                        //$precioConGananciaPoliza = $precioPesos + $gananciaPoliza;

                        $totalDescuento = ($descuentoConsumibles * $precioPesos) / 100;

            $precioConDescuentoGeneral = $precioPesos/((100-$porcentajeGeneral)/100);
            //log_message('error', $precioPesos.'/((100-'.$porcentajeGeneral.')/100)');
            $precioConDescuentoFrecuente = $precioPesos/(1-($porcentajeFrecuente/100));
            //log_message('error', $precioPesos.'/(1-('.$porcentajeFrecuente.'/100))');
            $precioConDescuentoEspecial = $precioPesos/(1-($porcentajeEspecial/100));
            
            $precioConDescuentoPoliza = $precioPesos/(1-($porcentajePoliza/100));

                        $data = array(
                            'precioDolares'=>$precio,
                            'general' => $precioConDescuentoGeneral,
                            'frecuente' => $precioConDescuentoFrecuente,
                            'especial' => $precioConDescuentoEspecial,
                            'poliza' => $precioConDescuentoPoliza,
                            'consumibles_id' => $insertado,
                        ); 
                        /*
                        $data = array(
                            'general' => $row[4],
                            'iva' => $row[5],
                            'neto' => $row[6],
                            'frecuente' => $row[7],
                            'iva2' => $row[8],
                            'neto2' => $row[9],
                            'especial' => $row[10],
                            'iva3' => $row[11],
                            'neto3' => $row[12],
                            'poliza' => $row[13],
                            'iva4' => $row[13],
                            'neto4' => $row[15],
                            'consumibles_id' => $insertado,
                        ); 
                        */
                        $this->model->insertar_consumibles_costos($data);

                        $equipos=explode(",",$row[7]);
                        for ($i=0; $i < count($equipos); $i++) { 
                            $info= array('idEquipo' => $equipos[$i], 'idConsumibles'=> $insertado);
                            $this->model->insertar_consumibles_equipos($info);
                        }
                    }
                } 
                $count++;
            }
        }
        // cerramos el archivo EXCEL
        $reader->close();
        /***** Acá termina la parte para poder leer el archivo e insertarlo en la BD *****/        
    }

    public function verificarequipo(){
        $data = $this->input->post('datos');
        $id = $this->input->post('idconsu');
        $result='';$equipo=0;
        if($id>=1){
            $datos = $this->model->verificarequipo($data,$id);
            foreach ($datos as $item) {
               $equipo = $item->id;
            }
            if($equipo>=1){
               $result = 1;           
            }
        }
        
        echo $result;
    } 
    //========================================================================
    function imagenes_multiple(){
        $idequipo=$_POST['idequipo'];
      $data = [];
   
      $count = count($_FILES['files']['name']);
      $output = [];
      for($i=0;$i<$count;$i++){
    
        if(!empty($_FILES['files']['name'][$i])){
    
          $_FILES['file']['name'] = $_FILES['files']['name'][$i];
          $_FILES['file']['type'] = $_FILES['files']['type'][$i];
          $_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
          $_FILES['file']['error'] = $_FILES['files']['error'][$i];
          $_FILES['file']['size'] = $_FILES['files']['size'][$i];
          $DIR_SUC=FCPATH.'uploads/equipos';

          $config['upload_path'] = $DIR_SUC; 
          //$config['allowed_types'] = 'docx|xlsx|pdf|doc|xls|pptx';
          $config['allowed_types'] = '*';

          //$config['allowed_types'] = 'docx|xlsx|pdf|jpg|jpeg|png|gif';
          $config['max_size'] = 5000;
          $file_names=date('Ymd_His').'_'.rand(0, 500);
          //$config['file_name'] = $file_names.'_'.str_replace(' ', '_', $_FILES['files']['name'][$i]);
          $config['file_name'] = $file_names;
          //$config['file_name'] = $file_names;
   
          $this->load->library('upload',$config); 
    
          if($this->upload->do_upload('file')){
            $uploadData = $this->upload->data();
            $filename = $uploadData['file_name'];
            $filenametype = $uploadData['file_ext'];
   
            $data['totalFiles'][] = $filename;
            //$this->ModeloGeneral->tabla_inserta('equipo_imgs',array('idequipo'=>$idequipo,'imagen'=>$filename,'filetype'=>$filenametype,'usuario'=>$this->idpersonal));
            $this->ModeloCatalogos->Insert('consumibles_imgs',array('idconsumible'=>$idequipo,'imagen'=>$filename));
          }else{
            $data = array('error' => $this->upload->display_errors());
            log_message('error', json_encode($data)); 
          }
        }
   
      }
      echo json_encode($output);
    }
    function favorito(){
        $params = $this->input->post();
        $id=$params['equipo'];
        $mostrar = $params['status'];
        $this->ModeloCatalogos->updateCatalogo('consumibles',array('destacado'=>$mostrar),array('id'=>$id));
        echo $mostrar;
    }
    function mostrarweb(){
        $params = $this->input->post();
        $id=$params['idequipo'];
        $mostrar = $params['mostrar'];
        $this->ModeloCatalogos->updateCatalogo('consumibles',array('paginaweb'=>$mostrar),array('id'=>$id));
    }
    function viewimages(){
        $params = $this->input->post();
        $id=$params['idequipo'];
        $resultados=$this->ModeloCatalogos->getselectwheren('consumibles_imgs',array('idconsumible'=>$id,'activo'=>1));
        $html='';
                foreach ($resultados->result() as $item) {
                    $html.='<div class="card_img"><div class="card_img_c"><img class="materialboxed" src="'.base_url().'uploads/equipos/'.$item->imagen.'"></div><div class="card_img_a">
                                <a class="btn-floating red tooltipped" data-position="top" data-delay="50" data-tooltip="Eliminar" onclick="deleteimg('.$item->id.')"><i class="material-icons">delete_forever</i></a></div></div>';
                }
        echo $html;
    }
    function viewcaracte(){
        $params = $this->input->post();
        $id=$params['idequipo'];
        //$resultados=$this->ModeloCatalogos->getselectwheren('equipos_caracteristicas',array('idequipo'=>$id,'activo'=>1));
        
        $strq = "SELECT * FROM consumibles_caracteristicas where idconsumible=$id and activo=1 ORDER BY orden ASC";
        $query = $this->db->query($strq);

        $html='<table class="table striped"><thead><tr><th>Tipo</th><th>Descripcion</th><th>General</th><th>Orden</th><th></th></tr></thead><tbody>';
        foreach ($query->result() as $item) {
            if($item->general==1){
                $general='SI';
            }else{
                $general='';
            }
            $html.='<tr><td>'.$item->name.'</td><td>'.$item->descripcion.'</td><td>'.$general.'</td><td>'.$item->orden.'</td>
                        <td>
                            <a class="btn-floating green tooltipped edit soloadministradores eq_cact_'.$item->id.'" 
                                data-name="'.$item->name.'"
                                data-descripcion="'.$item->descripcion.'"
                                data-general="'.$item->general.'"
                                data-orden="'.$item->orden.'"
                                onclick="editar_ec('.$item->id.')" data-position="top" data-delay="50" data-tooltip="Editar" data-tooltip-id="f5aed0ec-5db2-aa68-1319-23959ebc0465"><i class="material-icons">mode_edit</i></a>
                            <a class="btn-floating red tooltipped elimina soloadministradores" data-position="top" data-delay="50" data-tooltip="Eliminar" data-tooltip-id="7817ec6e-15c1-ab9a-76f3-cff8bc1c9ab4" onclick="delete_ec('.$item->id.')"><i class="material-icons">delete_forever</i></a>
                        </td>
                    </tr>';
        }
        $html.='</tbody></table>';
        echo $html;
    }
    function deleteimg(){
        $params = $this->input->post();
        $id=$params['idequipo'];
        $this->ModeloCatalogos->updateCatalogo('consumibles_imgs',array('activo'=>0),array('id'=>$id));
    }
    function insertaActualizacaracteristicas(){
        $params=$this->input->post();
        $id = $params['id'];
        unset($params['id']);
        if($id>0){
            $this->ModeloCatalogos->updateCatalogo('consumibles_caracteristicas',$params,array('id'=>$id));
        }else{
            $this->ModeloCatalogos->Insert('consumibles_caracteristicas',$params);
        }
    }
    function deleteec(){
        $params = $this->input->post();
        $id=$params['idequipo'];
        $this->ModeloCatalogos->updateCatalogo('consumibles_caracteristicas',array('activo'=>0),array('id'=>$id));
    }
    public function getlistconsumibles() {
        $params = $this->input->post();
        $getdata = $this->model->getlistconsu($params);
        $totaldata= $this->model->getlistconsut($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
   
}

?>