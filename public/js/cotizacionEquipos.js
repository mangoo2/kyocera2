var base_url = $('#base_url').val();
var editor; 
var idCotizacion = $('#idCotizacion').val();

$(document).ready(function(){  
    /*
        editor = new $.fn.dataTable.Editor( {
                ajax: base_url+"index.php/Cotizaciones/actualizaDetalleCotizacionEquipo",
                table: "#tablaHorizontal",
                fields: [ {
                        label: "precio:",
                        name: "precio"
                    }
                ]
            } );

        table = $('#tablaHorizontal').DataTable({
          "dom": 't',
          "ordering": false
        });


        load(idCotizacion);

        // Activa la edición por casilla en cada ROW de la tabla
        $('#tablaHorizontal').on( 'click', 'tbody td:not(:first-child)', function (e){
            editor.inline(this);
            

        });
        
         $('#tablaHorizontal').keypress(function(e) {
            if(e.which == 13) {
                setTimeout(function(){ calcular(); }, 4000);
            }
            setTimeout(function(){ calcular(); }, 4000);
        });
    */
    $('.editarcosto').click(function(event) {
        var precio = $('#newprecio').val()==''?0:$('#newprecio').val();
        var equipo=$('#idEquipon').val();
        var pass=$('#password').val();
        if (pass!='') {
            //if (precio>0) {
                $.ajax({
                    type:'POST',
                    url: base_url+'Cotizaciones/editarprecio',
                    data: {
                        precio: precio,
                        equipo: equipo,
                        pass: pass
                        },
                        async: false,
                        statusCode:{
                            404: function(data){
                                swal("Error", "404", "error");
                            },
                            500: function(){
                                swal("Error", "500", "error"); 
                            }
                        },
                        success:function(data){
                          if (data==1) {
                            swal("Editado!", "Precio Editado!", "success");
                            setTimeout(function(){ window.location.href=''; }, 3000);
                          }else{
                            swal("Error", "No tiene permiso", "error"); 
                          }

                        }
                    });
            //}else{
            //    swal("Error", "El precio no debe de ser menor o igual a cero", "error"); 
            //}
        }else{
            swal("Error", "Debe de ingresar una contraseña", "error"); 
        }   
    }); 
    if ($('#estatuscot').val()==1) {
        $( '#contenido1' ).ckeditor();
        $( '#contenido2' ).ckeditor();
        $( '#contenido3' ).ckeditor();
        $( '#contenido4' ).ckeditor();
        $( '#textoHeader2' ).ckeditor();
    }
    /*
    $('#contenido3').ckeditor(function() {

         toolbar : [
            ['Cut','Copy','Paste','PasteText','PasteFromWord','-','Print', 'SpellChecker', 'Scayt'],
            ['Undo','Redo'],
            ['Bold','Italic','Underline','Strike','-','Subscript','Superscript'],
            ['NumberedList','BulletedList','-','Outdent','Indent','Blockquote'],
            ['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
            ['Link','Unlink','Anchor',  'Smiley'],
            ['Table','HorizontalRule','SpecialChar'],
            ['Styles','BGColor']
        ] } );
        */

    
    
});


function btn_guardar(){
    //window.open(base_url+'Cotizaciones/pdf/'+idCotizacion, '_blank');
    /*
    $('.print_css').append('.cot{color: red}');
    $('.pantalla').remove();
    $('.usuario').remove();
    $('.menu').remove();
    $('.kyoc').remove();
    $('.log').append("<img class='kyoc' src='"+base_url+"app-assets/images/altakyo.png' alt='materialize logo' width='200px;' height='100px;'>");
    $('.venta_text').append("<h6 align='center' style='font-size:15px; color:black;'>Venta-Renta-Mantenimiento<br>Impresoras y Multifuncionales monocromáticos y color<br> Blvd. Norte No. 18831, San Alejandro, CP 72090, Puebla, Pue.<br> Call Center: 273 3400, 249 5393<br> Kyoceraap.com</h6>");
   // $('.venta_img').append("<img class='kyoc' src='"+base_url+"app-assets/images/1024_kyocera_logo.png' alt='materialize logo' width='200px;' height='80px;'>");
    //$('.btn_imp').remove();
    */
    saveinfo_o(1);
    saveinfo_o(2);
    saveinfo_o(3);
    saveinfo_o(4);
    //setTimeout(function() {window.print();}, 500);
    //setTimeout(function() {location.reload();}, 5000);
    window.open(base_url+'index.php/Cotizaciones/pdf/'+idCotizacion, '_blank');
}

 /* 
function load(idCotizacion) {
    console.log('entrando');
    // Destruye la tabla y la crea de nuevo con los datos cargados 
    table.destroy();
    table = $('#tablaHorizontal').DataTable({
        "ajax": {
            "url": base_url+"index.php/Cotizaciones/getTablaDetallesEquipos/"+idCotizacion
        },
        "columns": [
            {"data": "DT_RowId", visible: false},
            {"data": "foto", class: "tablaBordesPrincipal",
                // Dependiendod el número de estatus que traiga el registro, se muestra el texto correspondiente
                render:function(data,type,row)
                {
                    if(row.foto==null)
                    {
                        var html = '<img src="'+base_url+'app-assets/images/1024_kyocera_logo.png" class="onMouseOverDisabled">';
                    }
                    else
                    {
                        var html = '<img src="'+base_url+'uploads/equipos/'+row.foto+'" class="onMouseOverDisabled">';
                    }
                    
                    return html;
                }
            },  
            {"data": "modelo", class: "tablaBordesPrincipal"},
            {"data": "precio", class: "tablaBordesPrincipal contardato"},
            {"data": "toner_black", class: "tablaBordesPrincipal"},
            {"data": "toner_cmy", class: "tablaBordesPrincipal"},
            {"data": "paginas", class: "tablaBordesPrincipal"},
            {"data": "garantia_servicio", class: "tablaBordesPrincipal"},
            {"data": "costo_pagina_monocromatica", class: "tablaBordesPrincipal"},
            {"data": "costo_pagina_color", class: "tablaBordesPrincipal"}
        ],
        "ordering": false,
        "dom": 't'
    });
    setTimeout(function(){ calcular(); }, 1000);
    
}*/

function calcular(){
    var addtp = 0;
    $(".contardato").each(function() {
        var contar = $(this).html()=='Precio'?0:$(this).html();
        var vstotal = parseFloat(contar);
        addtp += Number(vstotal);
    });
    console.log(addtp);
    $('.totalgeneral').html(addtp);
}
function editar(id,costo){
    if ($('#estatuscot').val()==1) {
        $('#password').val('');
        $('#modaleditor').modal();
        $('#modaleditor').modal('open');
        $('#newprecio').val(costo);
        $('#idEquipon').val(id);
    }
    
}
function saveinfo(idcoti,tipo){
    var info=$('.info'+tipo).html();
    console.log(info);
    $.ajax({
        type:'POST',
        url: base_url+'Generales/editarinfocotizacion',
        data: {
            cotizacion: idcoti,
            tipo: tipo,
            info: info
            },
            async: false,
            statusCode:{
                404: function(data){
                    //swal("Error", "404", "error");
                },
                500: function(){
                    //swal("Error", "500", "error"); 
                }
            },
            success:function(data){
              
            }
        });
}
function editarp(id,piezas){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: 'nueva cantidad a modificar<br>'+
                 '<input type="number" id="newcantidad" class="name form-control" value="'+piezas+'" required/>',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                
                     $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Cotizaciones/editarpiezasequipos",
                        data: {
                            id:id,
                            cantidad:$('#newcantidad').val()
                        },
                        success: function (response){
                            location.reload();
                        },
                        error: function(response){
                            $.alert({
                                    boxWidth: '30%',
                                    useBootstrap: false,
                                    title: 'Error!',
                                    content: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema'});   
                        }
                    });
            },
            cancelar: function (){
            }
        }
    });
}
function editarconficionesg(idCotizacion,eg_rm,eg_rvm,eg_rpem,eg_rc,eg_rvc,eg_rpec){
        var html="<div class='row'>\
                    <div class='col s2'>\
                        <label>Renta Monocromatico</label>\
                        <input type='number' id='eg_rm' value='"+eg_rm+"' min='0' class='form-control-bmz'>\
                    </div>\
                    <div class='col s2'>\
                        <label>Volumen incluido monocromatico</label>\
                        <input type='number' id='eg_rvm' value='"+eg_rvm+"' min='0' class='form-control-bmz'>\
                    </div>\
                    <div class='col s2'>\
                        <label>Excedente Monocromatico</label>\
                        <input type='number' id='eg_rpem' value='"+eg_rpem+"' min='0' class='form-control-bmz'>\
                    </div>\
                    <div class='col s2'>\
                        <label>Renta Color</label>\
                        <input type='number' id='eg_rc' value='"+eg_rc+"' min='0' class='form-control-bmz'>\
                    </div>\
                    <div class='col s2'>\
                        <label>Volumen incluido monocromatico</label>\
                        <input type='number' id='eg_rvc' value='"+eg_rvc+"' min='0' class='form-control-bmz'>\
                    </div>\
                    <div class='col s2'>\
                        <label>Excedente Monocromatico</label>\
                        <input type='number' id='eg_rpec' value='"+eg_rpec+"' min='0' class='form-control-bmz'>\
                    </div>\
                 </div>";
     $.confirm({
                boxWidth: '93%',
                useBootstrap: false,
                icon: 'fa fa-warning',
                title: 'Editar condiciones!',
                content: html,
                type: 'red',
                typeAnimated: true,
                buttons:{
                    confirmar: function (){
                            $.ajax({
                                type:'POST',
                                url: base_url+"index.php/Cotizaciones/editarcondicionesg",
                                data: {
                                    eg_rm:$('#eg_rm').val(),
                                    eg_rvm:$('#eg_rvm').val(),
                                    eg_rpem:$('#eg_rpem').val(),
                                    eg_rc:$('#eg_rc').val(),
                                    eg_rvc:$('#eg_rvc').val(),
                                    eg_rpec:$('#eg_rpec').val(),
                                    idCotizacion:idCotizacion

                                },
                                success: function (response){
                                        location.reload();
                                },
                                error: function(response){
                                    $.alert({
                                            boxWidth: '30%',
                                            useBootstrap: false,
                                            title: 'Error!',
                                            content: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema'});   
                                     
                                }
                            });
                                        
                        
                },
                    cancelar: function () {
                    }
                }
            });
}
function editarconficionesi(idCotizacion,eg_rm,eg_rvm,eg_rpem,eg_rc,eg_rvc,eg_rpec){
        var html="<div class='row'>\
                    <div class='col s2'>\
                        <label>Renta Monocromatico</label>\
                        <input type='number' id='eg_rm' value='"+eg_rm+"' min='0' class='form-control-bmz'>\
                    </div>\
                    <div class='col s2'>\
                        <label>Volumen incluido monocromatico</label>\
                        <input type='number' id='eg_rvm' value='"+eg_rvm+"' min='0' class='form-control-bmz'>\
                    </div>\
                    <div class='col s2'>\
                        <label>Excedente Monocromatico</label>\
                        <input type='number' id='eg_rpem' value='"+eg_rpem+"' min='0' class='form-control-bmz' readonly>\
                    </div>\
                    <div class='col s2'>\
                        <label>Renta Color</label>\
                        <input type='number' id='eg_rc' value='"+eg_rc+"' min='0' class='form-control-bmz'>\
                    </div>\
                    <div class='col s2'>\
                        <label>Volumen incluido monocromatico</label>\
                        <input type='number' id='eg_rvc' value='"+eg_rvc+"' min='0' class='form-control-bmz'>\
                    </div>\
                    <div class='col s2'>\
                        <label>Excedente Monocromatico</label>\
                        <input type='number' id='eg_rpec' value='"+eg_rpec+"' min='0' class='form-control-bmz' readonly>\
                    </div>\
                 </div>";
     $.confirm({
                boxWidth: '93%',
                useBootstrap: false,
                icon: 'fa fa-warning',
                title: 'Editar condiciones!',
                content: html,
                type: 'red',
                typeAnimated: true,
                buttons:{
                    confirmar: function (){
                            $.ajax({
                                type:'POST',
                                url: base_url+"index.php/Cotizaciones/editarcondicionesi",
                                data: {
                                    r_costo_m:$('#eg_rm').val(),
                                    r_volumen_m:$('#eg_rvm').val(),
                                    //excedente:$('#eg_rpem').val(),
                                    r_costo_c:$('#eg_rc').val(),
                                    r_volumen_c:$('#eg_rvc').val(),
                                    //excedentecolor:$('#eg_rpec').val(),
                                    idCotizacion:idCotizacion

                                },
                                success: function (response){
                                        location.reload();
                                },
                                error: function(response){
                                    $.alert({
                                            boxWidth: '30%',
                                            useBootstrap: false,
                                            title: 'Error!',
                                            content: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema'});   
                                     
                                }
                            });
                                        
                        
                },
                    cancelar: function () {
                    }
                }
            });
}
function deleteequipov(id){
        var html="¿Desea eliminar el equipo?";
     $.confirm({
            boxWidth: '30%',
            useBootstrap: false,
            icon: 'fa fa-warning',
            title: 'Editar condiciones!',
            content: html,
            type: 'red',
            typeAnimated: true,
            buttons:{
                confirmar: function (){
                    $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Cotizaciones/deleteequipov",
                        data: {
                            id:id
                        },
                        success: function (response){
                                location.reload();
                        },
                        error: function(response){
                            $.alert({
                                    boxWidth: '30%',
                                    useBootstrap: false,
                                    title: 'Error!',
                                    content: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema'});   
                        }
                    });   
            },
                cancelar: function () {
                }
            }
        });
}