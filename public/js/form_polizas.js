// Obtener la BASE URL
var base_url = $('#base_url').val();

$(document).ready(function(){
    setTimeout(function(){ 
            tipovista3();
    }, 1000);
    
    
	// Asignamos el formulario de envío
	var formulario_polizas = $('#polizas_form');

	// Validamos el formulario en base a las reglas mencionadas debajo
	formulario_polizas.validate({
            ignore: "",
            rules:{
				nombre: {
                    required: true
                },
                cubre: {
                    required: true
                },
                vigencia_meses: {
                    required: false
                },
                vigencia_clicks: {
                    required: false
                }
            },
            // Para mensajes personalizados
            messages:{
                nombre:{
                    required: "Ingrese un nombre para la póliza"
                },
                cubre:{
                    required: "Ingrese que cubre la póliza"
                },
                vigencia_meses:{
                    required: "Ingrese vigencia de las visitas"
                },
                vigencia_clicks:{
                    required: "Ingrese la vigencia de los clicks"
                }
            },
            errorElement : 'div',
            // Si están correctos los campos, procesamos el formulario mediante Ajax
            submitHandler: function (form) 
            {
                
                
            }
        });
    $('.savepoliza').click(function(event) {
        var valid =$('#polizas_form').valid();
        if (valid) {
                //================================
                    var arrayPolizasdetalles;
                    var DATApd  = [];
                    var DATApdv  = [];
                    var TABLA   = $("#poliza_detalle tbody > tr");
                        TABLA.each(function(){         
                        item = {};
                        item ["id"] = $(this).find("input[id*='registro']").val();
                        item ["modelo"]   = $(this).find("select[id*='modelo'] option:selected").val();
                        item ["sinmodelo"]   = $(this).find("input[name*='sinmodelo']").is(':checked')==true?1:0;
                        item ["newmodelo"]   = $(this).find("input[id*='newmodelo']").val();
                        item ["precio_local"]   = $(this).find("input[id*='precio_local']").val();
                        item ["precio_semi"]  = $(this).find("input[id*='precio_semi']").val();
                        item ["precio_foraneo"]  = $(this).find("input[id*='precio_foraneo']").val();
                        item ["precio_especial"]  = $(this).find("input[id*='precio_especial']").val();
                        DATApd.push(item);
                        //=========================para obtener duplicados
                        if($(this).find("select[id*='modelo'] option:selected").val()!=undefined){
                            itemd = {};
                            itemd = $(this).find("select[id*='modelo'] option:selected").val();
                            DATApdv.push(itemd);
                        }
                        //========================
                    });
                    INFO  = new FormData();
                    arrayPolizasdetalles   = JSON.stringify(DATApd);
                //================================
                    //============ para obtener duplicados===================
                        let duplicados = [];
                        const tempArray = [...DATApdv].sort();
                        for (let i = 0; i < tempArray.length; i++) {
                          if (tempArray[i + 1] === tempArray[i]) {
                            duplicados.push(tempArray[i]);
                          }
                        }
                    //=======================================================
                var datos = $('#polizas_form').serialize()+'&arraypolizasdetalles='+arrayPolizasdetalles;
                console.log(duplicados);
                if(duplicados.length==0){
                    $.ajax({
                        type:'POST',
                        url: base_url+'index.php/polizas/insertaActualizaPoliza',
                        data: datos,
                        success:function(response) {
                            // Si existe el usuario, mostramos el mensaje de entrada y redirigimos
                            if(response!=0){
                                swal({ title: "Éxito",
                                    text: "La póliza se ha registrado de manera correcta",
                                    type: 'success',
                                    showCancelButton: false,
                                    allowOutsideClick: false,
                                });
                                
                                setTimeout(function(){ 
                                    window.location.href = base_url+'index.php/polizas'; 
                                }, 2000);
                                
                            }
                            // En caso contrario, se notifica
                            else{
                                swal({ title: "Error",
                                    text: "La póliza no pudo guardarse. Contacte al administrador del sistema",
                                    type: 'error',
                                    showCancelButton: false,
                                    allowOutsideClick: false,
                                });
                            }
                        }
                    });
                }else{
                    swal({ title: "Error",
                        text: "En la poliza hay modelos duplicados",
                        type: 'error',
                        showCancelButton: false,
                        allowOutsideClick: false,
                    });
                    duplicados.forEach(function(element) {
                        console.log(element);
                        $('.modelo_'+element).addClass('duplicado');
                    });
                }
        }
    });
});
function tipovista3(){
    var tipoVista = $("#tipoVista").val();
    if(tipoVista == 3){
        var tipoVista = $("#tipoVista").val()
        $("input").prop('disabled', true);
        $("textarea").prop('disabled', true);
        $("select").prop('disabled', true);
        $("checkbox").prop('disabled', true);
        $("button").prop('disabled', true);
    }
} 
function polizasdetalles(id){
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Polizas/detallespolizas",
        data: {
            idPoliza:id
        },
        success: function (response){
            console.log(response);
            var array = $.parseJSON(response);
           console.log(array);
           if (array.length>0) {
                array.forEach(function(element) {
                    addpolizasdetalles(element.id,element.modelo,element.sinmodelo,element.newmodelo,element.precio_local,element.precio_semi,element.precio_foraneo,element.precio_especial,1);
               });
           }else{
                addpolizasdetalles(0,'',0,'','','','','',0);
           }
           

            
        },
        error: function(response){
            toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema','Error!'); 
        }
    });
}
var tr_rows=0;
function addpolizasdetalles(id,modelo,sinm,newmodelo,local,semi,foreano,especial,tipo){
    if (tipo==1) {
        var tr_row='row_p_r_'+id;
    }else{
        var tr_row='row_p_t_'+tr_rows;        
    }
    if(sinm==1){
        var sinmchecked='checked';
        //var sinmcheckednone='';
        var sinmcheckednone='none';
        //var sinmcheckednonenn='none';
        var sinmcheckednonenn='';
        var select='<option value="113" selected>Sin modelo</option>'
    }else{
        var sinmchecked='';
        var sinmcheckednone='none';
        var sinmcheckednonenn='';
        var select =$('#equiposbases').html();
    }
    if(newmodelo==null){
        newmodelo='';
    }
    
    var dpoliza='<tr class="'+tr_row+' modelo_'+modelo+'">\
                      <td class="input-field" style="width: 200px;">\
                        <input type="hidden" id="registro" value="'+id+'">\
                        <input type="text" id="newmodelo" class="newmodelo_'+tr_rows+' form-control-bmz" value="'+newmodelo+'" style="display:'+sinmcheckednone+'">\
                        <div class="select_newmodelo_'+tr_rows+'" style="display:'+sinmcheckednonenn+'">\
                        <select id="modelo" class="browser-default chosen-select modelo_'+tr_rows+'">\
                            '+select+'\
                        </select>\
                        </div>\
                        <label for="modelo" class="active">Modelo</label>\
                      </td>\
                      <td>\
                          <input type="checkbox" id="sinmodelo_'+tr_rows+'" name="sinmodelo" class="sinmodelo" value="1" '+sinmchecked+' onclick="sinmodelo('+tr_rows+')">\
                          <label for="sinmodelo_'+tr_rows+'">Sin modelo</label>\
                      </td>\
                      <td class="input-field">\
                        <input id="precio_local" type="number" value="'+local+'" class="form-control-bmz" placeholder="0.00">\
                        <label for="precio_local" class="active">Precio Cliente Local</label>\
                      </td>\
                      <td class="input-field">\
                        <input id="precio_semi" type="number" value="'+semi+'" class="form-control-bmz" placeholder="0.00">\
                        <label for="precio_semi" class="active">Precio Cliente Semi</label>\
                      </td>\
                      <td class="input-field">\
                        <input id="precio_foraneo" type="number" value="'+foreano+'" class="form-control-bmz" placeholder="0.00">\
                        <label for="precio_foraneo" class="active">Precio Cliente Foraneo</label>\
                      </td>\
                      <td class="input-field">\
                        <input id="precio_especial" type="number" value="'+especial+'" class="form-control-bmz" placeholder="0.00">\
                        <label for="precio_especial" class="active">Precio Cliente Especial</label>\
                      </td>\
                      <td class="soloadministradores">\
                            <a class="btn-floating red tooltipped" data-position="top" data-delay="50" data-tooltip="Eliminar" onclick="deletepd('+id+','+tr_rows+','+tipo+')">\
                                <i class="material-icons">delete_forever</i>\
                            </a>\
                      </td>\
                    </tr>';
    $('.poliza_detalle_tbody').append(dpoliza);
    if(sinm==1){
        $('.modelo_'+tr_rows).val(113);
    }else{
        $('.modelo_'+tr_rows).val(modelo);    
    }
    
    //$('.chosen-select').select2({width: 'resolve'});
    $('.chosen-select').chosen({width: 'resolve'});
    tr_rows++;
    validarsinmodelo();
}
function deletepd(id,tr,tipo){
    var idp=id;
    if (tipo==0) {
        $('.row_p_t_'+tr).remove();
    }else{
        $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de Eliminar este detalle de poliza?',
        type: 'red',
        typeAnimated: true,
        buttons: {
            // Si le da click al botón de "confirmar"
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"index.php/polizas/eliminarpolizad",
                    data: {
                        idPolizad:idp
                    },
                    success: function (response) {
                            $.alert({
                                boxWidth: '30%',
                                useBootstrap: false,
                                title: 'Éxito!',
                                content: 'Detalle eliminado!'});
                            $('.row_p_r_'+idp).remove();
                      
                    },
                    error: function(response){
                        $.alert({
                                boxWidth: '30%',
                                useBootstrap: false,
                                title: 'Error!',
                                content: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema'});   
                        setTimeout(function(){ 
                                window.location.href = '';
                        }, 2000);  
                        
                    }
                });
            },
            // Si le da click a "cancelar"
            cancelar: function (){
                
            }
        }
    });
    }
}
function adddetalle(){
     addpolizasdetalles(0,'','','','','','','',0);
}
function sinmodelo(row) {
    setTimeout(function(){
        var sinmodelov = $('#sinmodelo_'+row).is(':checked')==true?1:0; 
        if(sinmodelov==1){
            $('.modelo_'+row).html('<option value="113" selected>Sin modelo</option>');
            //$('.chosen-select').select2({width: 'resolve'});

            $('.chosen-select').chosen({width: '100%'});
            setTimeout(function(){
                $('.modelo_'+row).val(113);
                $('.modelo_'+row).trigger("chosen:updated");
            }, 500);
        }else{
            //$('.select_newmodelo_'+row).show('show');
            //$('.newmodelo_'+row).hide('show');

            var select =$('#equiposbases').html();
            $('.modelo_'+row).html(select);
            //$('.chosen-select').select2({width: 'resolve'});
            $('.chosen-select').chosen({width: '100%'});
            $('.modelo_'+row).trigger("chosen:updated");
        }
        validarsinmodelo();
    }, 500);
}
function validarsinmodelo(){
    var totalsinmodelos=0;
    var TABLA   = $("#poliza_detalle tbody > tr");
            TABLA.each(function(){         
            if($(this).find("input[class*='sinmodelo']").is(':checked')==true){
                totalsinmodelos++;
            }
            
        });

    if(totalsinmodelos>0){
        TABLA.each(function(){         
            if($(this).find("input[class*='sinmodelo']").is(':checked')==true){
                
            }else{
                $(this).find("input[class*='sinmodelo']").prop("disabled", true);
            }
            
        });
    }else{
        TABLA.each(function(){         
            
                $(this).find("input[class*='sinmodelo']").prop("disabled", false);
            
            
        });
    }
}