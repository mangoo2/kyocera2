<?php
if(isset($cliente))
{
$idCliente = $cliente->id;
$empresa = $cliente->empresa;
}
if($datosCotizacion[0]->propuesta==1){
  $titlecp='Propuesta';
}else{
  $titlecp='Cotización';
}
?>
<style type="text/css" class="print_css">
  .datosClienteCotizacion{
    background: <?php echo $configuracionCotizacion->color ?> !important;
    -webkit-print-color-adjust:exact;
  }
   @media print {
      .btn_imp{
        display: none;
      }
      .pdisminuirpading td{
        padding: 5px;
      }
      .letraTablaFooterPoliza tbody tr td{
        padding: 4px;
      }
      .datosClienteCotizacion tbody tr td{
        padding-top: 5px;
        padding-bottom: 5px;
      } 
   }
</style>
<input type="hidden" id="estatuscot" value="<?php echo $datosCotizacion[0]->estatus;?>">
<!-- START CONTENT -->
<section id="content" width="100%">
  <!--start container-->
  <div class="container" width="100%">
    <div class="section" style="margin-top: -53px;">
     <br>
      <div class="row">
        <input id="base_url" type="hidden" name="base_url" value="<?php echo base_url(); ?>">
        <input id="idPoliza" type="hidden" name="idPoliza" value="<?php echo $datosCotizacion[0]->id; ?>">
        <!-- FORMULARIO  card-panel-->
        <div class="col s12">
          <div class="">

            <!-- ENCABEZADO PRINCIPAL PDF -->
            <div class="row"> 
              <div class="col s3">
                <img src="<?php echo base_url(); ?>app-assets/images/alta.png">
              </div>
              <div class="col s6">
                <p class="headerPDF"><b>Impresoras y Multifuncionales, Venta, Renta y Mantenimiento.<br>Blvd. Norte 1831 V. San Alejandro. Puebla, Pue. Call Center 273 3400, 249 5393<br>www.kyoceraapp.com</b></p>
              </div> 
              <div class="col s3">
                <p align="center"><img class="" src="<?php echo base_url(); ?>app-assets/images/1024_kyocera_logo_mds.png" alt="materialize logo" width="200px;" height="50px;"></p>
              </div>
            </div>
            

            <!-- APARTADO DE INFORMACIÓN DE RECEPTOR DE COTIZACIÓN-->
            
            
            <div class="row">
              <table class="centered datosClienteCotizacion tablaBordesHeader">
                  <thead>
                    
                  </thead>
                  <tbody>
                    <tr >
                      <td class="tablaBordesHeader" >
                        <?php echo $titlecp;?> Especial Para: <br>
                        <?php echo $datosCliente->empresa ?>
                      </td>
                      <td class="tablaBordesHeader">
                        Atención para: <br>
                        <?php echo $datosCotizacion[0]->atencion ?>
                      </td>
                      <td class="tablaBordesHeader">
                        Correo: <?php echo $datosCotizacion[0]->correo ?> <br>
                        Teléfono: <?php echo $datosCotizacion[0]->telefono ?>
                      </td>
                      <td class="tablaBordesHeader">
                        <?php echo $titlecp;?> No. <?php echo $datosCotizacion[0]->id?> 
                      </td>
                    </tr>
                    <tr>
                      <td colspan="4" style="font-size: 14px;">
                        <b><?php echo $titlecp;?> de Servicio de Mantenimiento</b>
                      </td>
                    </tr>
                  </tbody>
                </table>
            </div>
            <br>
            <!------------ TABLA DE INFORMACIÓN DE EQUIPOS ------------>
            <div class="row">
              <table class="centered bordered letraTablaImpresionPoliza tablaBordesPrincipal" style="width:100%">
                <?php 

                    $totalg=0;
                  foreach ($datosCotizacionequipos->result() as $iteme) { 
                    $detallesCotizacion = $this->Cotizaciones_model->getDetallesCotizacionPolizaTablae($iteme->idCotizacion,$iteme->idEquipo);
                  ?>
                    <tr>
                      <th><?php echo $iteme->modelo.' '.$iteme->serie; ?></th>
                    </tr>
                    <tr>
                      <td>
                        <table class="centered bordered letraTablaImpresionPoliza"style="width:100%">
                          <thead >
                            <tr class="tablaBordesPrincipal">
                                <th class="tablaBordesPrincipal" >Tipo de Póliza</th>
                                <th class="tablaBordesPrincipal" >Cobertura</th>
                                <th class="tablaBordesPrincipal" >Vigencia</th>
                                <th class="tablaBordesPrincipal" >Visitas</th>
                                <th class="tablaBordesPrincipal" >Cantidad</th>
                                <th class="tablaBordesPrincipal" >Precio</th>
                                <?php if ($datosCotizacion[0]->propuesta==1) { ?>
                                  <!--<th class="tablaBordesPrincipal" >I.V.A</th>-->
                                  <th class="tablaBordesPrincipal" >Total</th>
                                <?php }  ?>
                            </tr>
                          </thead>
                          <tbody >
                            <?php 
                              
                            foreach ($detallesCotizacion->result() as $itemed) { 
                              if($itemed->precio_local!=null){
                                $preciovalor=$itemed->precio_local;
                                $columna='precio_local';
                              }
                              if($itemed->precio_semi!=null){
                                $preciovalor=$itemed->precio_semi;
                                $columna='precio_semi';
                              }
                              if($itemed->precio_foraneo!=null){
                                $preciovalor=$itemed->precio_foraneo;
                                $columna='precio_foraneo';
                              }
                              if($itemed->precio_especial!=null){
                                $preciovalor=$itemed->precio_especial;
                                $columna='precio_especial';
                              }

                              ?>
                              <tr class="tablaBordesPrincipal">
                                  <td class="tablaBordesPrincipal"><?php echo $itemed->nombre;?></td>
                                  <td class="tablaBordesPrincipal" 
                                      onclick="editar(<?php echo $itemed->id;?>,'cubre','<?php echo $itemed->cubre;?>')"
                                  ><?php echo $itemed->cubre;?></td>
                                  <td class="tablaBordesPrincipal" 
                                      onclick="editar(<?php echo $itemed->id;?>,'vigencia_meses','<?php echo $itemed->vigencia_meses;?>')"
                                  ><?php echo $itemed->vigencia_meses;?></td>
                                  <td class="tablaBordesPrincipal"><?php echo $itemed->vigencia_clicks;?></td>
                                  <!--<td class="tablaBordesPrincipal"><?php echo $itemed->modelo;?></td>-->
                                  <td class="tablaBordesPrincipal"><?php echo $itemed->cantidad;?></td>
                                  <td class="tablaBordesPrincipal" onclick="editar(<?php echo $itemed->id;?>,'<?php echo $columna; ?>','<?php echo $preciovalor;?>')">
                                    <?php 
                                      if($itemed->precio_local!=null){
                                        echo '$ '.number_format($itemed->precio_local,2,'.',',').'';
                                        $total=$itemed->precio_local*$itemed->cantidad;
                                      }
                                      if($itemed->precio_semi!=null){
                                        echo '$ '.number_format($itemed->precio_semi,2,'.',',').'';
                                        $total=$itemed->precio_semi*$itemed->cantidad;
                                      }
                                      if($itemed->precio_foraneo!=null){
                                        echo '$ '.number_format($itemed->precio_foraneo,2,'.',',').'';
                                        $total=$itemed->precio_foraneo*$itemed->cantidad;
                                      }
                                      if($itemed->precio_especial!=null){
                                        echo '$ '.number_format($itemed->precio_especial,2,'.',',').'';
                                        $total=$itemed->precio_especial*$itemed->cantidad;
                                      }
                                      $totalg=$totalg+$total;
                                    ?></td>
                                    <?php if ($datosCotizacion[0]->propuesta==1) { ?>
                                    <!--<td class="tablaBordesPrincipal">
                                      $ <?php echo number_format($total*0.16,2,'.',',');?>
                                    </td>-->
                                    <td class="tablaBordesPrincipal">
                                      $ <?php echo number_format($itemed->cantidad*$total,2,'.',',');?> 
                                    </td>
                                    <?php }  ?>

                              </tr>
                            <?php }  ?>
                            
                          </tbody>
                        </table>
                        
                      </td>
                    </tr>
                <?php }  ?>
                    
              </table> 
              <?php if ($datosCotizacion[0]->propuesta==0) { ?>
                        <table class="centered bordered letraTablaImpresionPoliza tablaBordesPrincipal" style="width:100%"> 
                          <tr class="tablaBordesPrincipal pdisminuirpading"> 
                              <td class="tablaBordesPrincipal" width="90%" align="right" style="text-align: right;"><b>SubTotal:</b></td> 
                              <td class="tablaBordesPrincipal "width="10%"><b>$ <?php echo number_format($totalg,2,'.',',');?></b></td>
                          </tr> 
                          <tr class="tablaBordesPrincipal pdisminuirpading"> 
                              <td class="tablaBordesPrincipal"  align="right" style="text-align: right;"><b>I.V.A:</b></td> 
                              <td class="tablaBordesPrincipal "><b>$ <?php echo number_format($totalg*0.16,2,'.',',');?></b></td>  
                          </tr> 
                          <tr class="tablaBordesPrincipal pdisminuirpading"> 
                              <td class="tablaBordesPrincipal"  align="right" style="text-align: right;"><b>TOTAL:</b></td> 
                              <td class="tablaBordesPrincipal "><b>$ <?php echo number_format($totalg+($totalg*0.16),2,'.',',');?></b></td>  
                          </tr> 
                        </table> 
                        <?php  } ?> 
                
            </div>

            <br><br>
            <!-- Condiciones de Equipos -->
            <div class="row" style="margin-bottom: -10px">
              <table class="centered letraTablaFooterPoliza">
                  <thead>
                      <tr>
                        <th>Garantía y Beneficios</th>
                        <th></th>
                        <th>Responsable</th>
                      </tr>
                  </thead>
                  <tbody >
                    <tr>
                        <td colspan="2">
                          <div contenteditable="true" class="info4" onchange="saveinfo_o(4)">
                            <?php 
                              if($datosCotizacion[0]->info4!=null){
                                echo $datosCotizacion[0]->info4;
                              }else{
                            ?>
                            <ul>
                              <li>
                                <b>Servicio Garantizado.</b> Atención con personal Certificado por "KYOCERA".
                              </li>
                              <li>
                                <b>Consumibles y Refacciones.</b> Para su mayor confianza, solo manejamos originales de "KYOCER.
                              </li>
                              <li>
                                <b>Tiempo de Solución (en otros casos tiempo de respuesta).</b> Dentro de las 24 hrs.
                              </li>
                              <li>
                                <b>Operación Óptima.</b> Calidad de Impresión permanente.
                              </li>
                              <li>
                                <b>Valor de la inversión.</b> Mayor vida útil y mayor valor de reventa.
                              </li>
                              <li>
                                <b>Precios.</b> Descuento especial del 30% en Refacciones, Toner y Kit de mantemiento para clientes con Póliza vigente.
                              </li>
                              <li>
                                <b>Respaldo.</b> En caso de robo o perdida total, apoyo con equipo similar (sujeto a disponibilidad).
                              </li>
                              <li>
                                <b>Soporte.</b> Instalación de KM-Net Viewer para control y monitoreo.
                              </li>
                              <li>
                                <b>Servicio Extraordinario.</b> 30% de Descueto sobre tarifa por evento extraordinario requerido durante la vigencia del contrato.
                              </li>
                              <li>
                                <b>Servicio Urgente en Taller.</b> $0.00 de mano de obra por evento urgente, en nuestro taller con previa cita, durante la vigencia del contrato.
                              </li>
                            </ul>
                          <?php } ?>
                          </div>
                        </td>
                        <!--<td width="33%"><b>Precios: </b>De contado, sujetos a I.V.A. y/o cambios sin previo aviso.  </td>-->
                        <td width="33%"><b><?php echo $fechareg ?></b><br><b><?php echo $nombreUsuario;?></b><br><?php echo $emailUsuario;?></td>
                    </tr>
                    <tr>
                        <td colspan="3">Depositar a nombre de <b>"Alta Productividad, S.A. de C.V."</b> Banco Banamex, Suc 0826, Cuenta 7347132, CLABE 002668082673471327 (para transferencia).<br><b>Mandar comprobante a este correo por favor: kyorent@kyoceraapp.com</b></td>
                    </tr>
                  </tbody>
                </table>
            </div>

           

<!--columna 1---------------------------------------------------------------->

              <form class="form" id="" method="post">
                <?php
                if(isset($cliente))
                {
                ?>
                <input id="idCliente" name="idCliente" type="hidden" value="<?php echo $idCliente; ?>">
                <?php
                }
                ?>
                <div class="row">
                  <div class="input-field col s12">
                    <button class="btn-bmz btn cyan waves-effect waves-light right btn_imp" type="button" onclick="btn_guardar()">Imprimir
                    <i class="material-icons right">send</i>
                    </button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<div id="modaleditor" class="modal">
  <div class="modal-content ">
    <h4>Editar</h4>
    <div class="border_datos" id="fiscales_dato1">
      <div class="col s24 m24 l12">
        <div class="row ">
            <div class="row">
              <input id="idpoliza" type="hidden" value="">
              <input id="polizacol" type="hidden" value="">
              <div class="input-field col s12">
                <input id="password" type="password" class="validate" autocomplete="new-password" required>
                <label for="password">Password</label>
              </div>
            </div>
            <div class="row">
              <div class="input-field col s12">
                <textarea class="materialize-textarea" id="valor"></textarea>
              </div>
            </div>
                                        

        </div>                          
      </div>
    </div>                    
  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
    <a href="#!" class="modal-action modal-close waves-effect waves-green green btn-flat editarvalor">Aceptar</a>
  </div>
</div>
<script type="text/javascript">
  function saveinfo_o(tipo){
    saveinfo(<?php echo $datosCotizacion[0]->id;?>,tipo);
  }
</script>