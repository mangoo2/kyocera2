<?php 
$label = 'Formulario de Agenda de llamadas';
?>
 
<!-- START CONTENT --> 
        <section id="content" width="100%">
          

          <!--start container-->
          <div class="container" width="100%">
            <div class="section">
                <h4 class="caption"><?php echo $label; ?></h4>
              <div class="row">
                <input id="base_url" type="hidden" name="base_url" value="<?php echo base_url(); ?>">
                <!-- FORMULARIO -->
                
                  <div class="col s12 m12 24">

                    <div class="card-panel">

                      <div class="row">
                         <form class="form" id="llamadas_from" method="post">

                          <input id="idProspecto" type="hidden" name="idProspecto" value="<?php echo $idProspecto; ?>">
                          <div class="row">
                            <div class="input-field col s2">
                              <i class="material-icons prefix">date_range</i>
                              <label>Próxima llamada</label>
                            </div>
                            <div class="input-field col s4">
                              <input type="date" name="proxima" id="proxima">
                            </div>

                            <div class="input-field col s2">
                              <i class="material-icons prefix">alarm</i>
                              <label>Hora de próx. llamada</label>
                            </div>
                            <div class="input-field col s4">
                              <input type="time" name="horaProxima" id="horaProxima">
                            </div>
                          </div>

                          <div class="row">
                            
                            <div class="input-field col s2">
                              <i class="material-icons prefix">textsms</i>
                              <label>Observaciones</label>
                            </div>
                            <div class="input-field col s4">
                              <textarea row="6" placeholder="Motivo de llamada" id="comentario" name="comentario"></textarea>
                            </div>

                            <div class="input-field col s2">
                              <i class="material-icons prefix">announcement</i>
                              <label>Motivo de próxima llamada</label>
                            </div>
                            <div class="input-field col s4">
                              <input type="text" name="motivo" id="motivo">
                            </div>

                          </div>

                          
                          <div class="row">
                            <div class="input-field col s12">
                              <button class="btn cyan waves-effect waves-light right" type="submit">Guardar
                                <i class="material-icons right">send</i>
                              </button>
                            </div>
                          </div>
                        </form>
                        <br>
                        <br>

                        <!----------------- TABLA DE LLAMADAS YA AGENDADAS -------------->
                        <div class="row col s12 18">
                          <table id="tabla_llamadas_agendadas" class="responsive-table display" cellspacing="0">
                            <thead>
                              <tr>
                                <th>#</th>
                                <th>Motivo de Próxima Llamada</th>
                                <th>Comentario</th>
                                <th>Próxima Llamada</th>
                                <th>Hora de la Próx. Llamada</th>
                                <th></th>
                              </tr>
                            </thead>
                            <tbody>
                            </tbody>
                          </table>

                          <br>
                          <br>
                          <br>
                          <br>
                        </div>
                        

                      </div>



                    </div>

                  </div>
                

              </div>
            </div>
          </div>
        </section>