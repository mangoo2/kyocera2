<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Crm extends CI_Controller 
{
    public function __construct() 
    {
        parent::__construct();
        $this->load->model('Prospectos_model', 'model');
        $this->load->model('Estados_model', 'estados');
        $this->load->model('Crm_model', 'crm');
        $this->load->model('login_model');
        $this->load->helper('url');
    }

    // Agendar llamadas a un prospecto
    function agendaLlamadas($idProspecto) 
    {
        $menus = $this->getMenus($this->session->userdata('perfilid'));
        $data['menus'] = $menus['menus'];
        $data['submenusArray'] = $menus['submenusArray'];
        $data['idProspecto'] = $idProspecto;

        $this->load->view('header');
        $this->load->view('main',$data);
        $this->load->view('crm/agendaLlamadas');
        $this->load->view('crm/agendaLlamada_js');
        $this->load->view('footer');
    }

    // Agendar correos a un prospecto
    function agendaCorreos($idProspecto) 
    {
        $menus = $this->getMenus($this->session->userdata('perfilid'));
        $data['menus'] = $menus['menus'];
        $data['submenusArray'] = $menus['submenusArray'];
        $data['idProspecto'] = $idProspecto;

        $this->load->view('header');
        $this->load->view('main',$data);
        $this->load->view('crm/agendaCorreos');
        $this->load->view('crm/agendaCorreos_js');
        $this->load->view('footer');
    }

    // Agendar visitas a un prospecto
    function agendaVisitas($idProspecto) 
    {
        $menus = $this->getMenus($this->session->userdata('perfilid'));
        $data['menus'] = $menus['menus'];
        $data['submenusArray'] = $menus['submenusArray'];
        $data['idProspecto'] = $idProspecto;

        $this->load->view('header');
        $this->load->view('main',$data);
        $this->load->view('crm/agendaVisitas');
        $this->load->view('crm/agendaVisitas_js');
        $this->load->view('footer');
    }


    // Agendar correos a un prospecto
    function revisarCalendario($idProspecto) 
    {
        $menus = $this->getMenus($this->session->userdata('perfilid'));
        $data['menus'] = $menus['menus'];
        $data['submenusArray'] = $menus['submenusArray'];
        $data['idProspecto'] = $idProspecto;

        $this->load->view('header');
        $this->load->view('main',$data);
        $this->load->view('crm/revisaCalendario');
        $this->load->view('footer');
        $this->load->view('crm/revisaCalendario_js');
    }

    function getMenus($idPerfil)
    {
        // Obtenemos los menús del perfil
        $menus = $this->login_model->getMenus($idPerfil);
        $submenusArray = array();
        // Obtenemos y asignamos los submenus 
        foreach ($menus as $item)
        {
            array_push($submenusArray, $this->login_model->submenus($idPerfil,$item->MenuId));
        }
        // Los asignamos al arary que se envía a la vista
        $data['menus'] = $menus;
        $data['submenusArray'] = $submenusArray;
        return $data;
    }

    function getListadoLlamadasPorProspecto($idProspecto)
    {
        $prospectos = $this->model->getListadoLlamadasPorProspecto($idProspecto);
        $json_data = array("data" => $prospectos);
        echo json_encode($json_data);
    }

    function getListadoCorreosPorProspecto($idProspecto)
    {
        $correos = $this->model->getListadoCorreosPorProspecto($idProspecto);
        $json_data = array("data" => $correos);
        echo json_encode($json_data);
    }

    function getListadoVisitasPorProspecto($idProspecto)
    {
        $visitas = $this->model->getListadoVisitasPorProspecto($idProspecto);
        $json_data = array("data" => $visitas);
        echo json_encode($json_data);
    }

    function eventos($idProspecto)
    {
        $eventos1 = $this->crm->getListadoLlamadasPorProspectoAgenda($idProspecto);
        $eventos2 = $this->crm->getListadoCorreosPorProspectoAgenda($idProspecto);
        $eventos3 = $this->crm->getListadoVisitasPorProspectoAgenda($idProspecto);
        $eventos= array_merge($eventos1,$eventos2,$eventos3);
        echo json_encode($eventos);
    }

    function insertaLlamada()
    {
        $data = $this->input->post();
        $data['estatus'] = 1;
        $data['fechaRegistro'] = date('Y-m-d');
        
        $llamadaInsertada = $this->model->insertar_llamada($data);
        echo $llamadaInsertada;
    }

    function insertaCorreo()
    {
        $data = $this->input->post();
        $data['estatus'] = 1;
        $data['fechaRegistro'] = date('Y-m-d');
        
        $correoInsertado = $this->model->insertar_correo($data);
        echo $correoInsertado;
    }

    function insertaVisita()
    {
        $data = $this->input->post();
        $data['estatus'] = 1;
        $data['fechaRegistro'] = date('Y-m-d');
        
        $visitaInsertado = $this->model->insertar_visita($data);
        echo $visitaInsertado;
    }

    function cambiarEstatusRegistroAgenda()
    {
        $data = $this->input->post();
        $id = $data['idRegistro'];
        $datos = array('estatus' => '2');
        // De acuerdo al tipo que venga del POST,
        // Sabremos si es llamada (1) , correo (2) o visita (3)
        if($data["tipo"]==1)
        {
            $actualizado = $this->crm->actualizaRegistroLlamada($id,$datos);
        }
        else if($data["tipo"]==2)
        {
            $actualizado = $this->crm->actualizaRegistroCorreo($id,$datos);   
        }
        else if($data["tipo"]==3)
        {
            $actualizado = $this->crm->actualizaRegistroVisita($id,$datos);
        }


        if($actualizado==true)
        {
            echo 1;
        }
         
    }
}

?>