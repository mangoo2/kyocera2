<?php
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloServicioh extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
//==============contratos==========
    function getlistaservicioc($params){
        $tiposer = $params['tiposer'];
        $tecnico = $params['tecnico'];
        $status = $params['status'];
        $zona = $params['zona'];
        $pcliente = $params['pcliente'];
        $fechainicio = $params['fechainicio'];
        $fechafin = $params['fechafin'];
        $columns = array( 
            0=>'ace.asignacionIde',
            1=>'per.personalId',
            2=>"CONCAT(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno) as tecnico",
            3=>'cli.id',
            4=>'cli.empresa',
            5=>'ace.fecha',
            6=>'ace.hora',
            7=>'ac.zonaId',
            8=>'ru.nombre',
            9=>'rde.modelo',
            10=>'serp.serie',
            11=>'ace.status',
            12=>'ac.tiposervicio',
            13=>'ace.horafin',
            14=>'ac.asignacionId',
            15=>'sereve.nombre as servicio',
            16=>'ace.t_fecha',
            17=>'ace.t_horainicio',
            18=>'ace.t_horafin',
            19=>'ace.horaserinicio',
            20=>'ace.horaserfin',
            21=>'ace.horaserinicioext',
            22=>'ace.horaserfinext',
            23=>'ace.comentario'
        );
        $columnss = array( 
            0=>'ace.asignacionIde',
            1=>'per.personalId',
            2=>"CONCAT(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno)",
            3=>'cli.id',
            4=>'cli.empresa',
            5=>'ac.fecha',
            6=>'ac.hora',
            7=>'ac.zonaId',
            8=>'ru.nombre',
            9=>'rde.modelo',
            10=>'serp.serie',
            11=>'ace.status',
            12=>'ac.tiposervicio',
            13=>'ace.horafin',
            14=>'sereve.nombre',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('asignacion_ser_contrato_a_e ace');
        $this->db->join('asignacion_ser_contrato_a ac','ac.asignacionId=ace.asignacionId');
        $this->db->join('personal per','per.personalId=ace.tecnico','left');
        $this->db->join('contrato con','con.idcontrato=ac.contratoId');
        $this->db->join('rentas ren','ren.id=con.idRenta');
        $this->db->join('clientes cli','cli.id=ren.idCliente');
        $this->db->join('rutas ru','ru.id=ac.zonaId');
        $this->db->join('rentas_has_detallesEquipos rde','rde.id=ace.idequipo');
        $this->db->join('series_productos serp','serp.serieId=ace.serieId');
        $this->db->join('servicio_evento sereve','sereve.id=ac.tservicio','left');
        /*
        $where = array('ca.status'=>1);
        $this->db->where($where);
        */
        if ($tiposer==1) {
            $this->db->where(array('ac.tiposervicio'=>0));
        }
        if ($tiposer==2) {
            $this->db->where(array('ac.tiposervicio'=>1));
        }
        if ($tecnico>0) {
            $this->db->where(array('per.personalId'=>$tecnico));
        }
        if ($status==1) {
            $this->db->where('ace.status >=',0);
            $this->db->where('ace.status <=',1);
        }
        if ($status==2) {
            $this->db->where(array('ace.status'=>2));
        }
        if ($zona>0) {
            $this->db->where(array('ac.zonaId'=>$zona));
        }
        if ($pcliente>0) {
            $this->db->where(array('cli.id'=>$pcliente));
        }
        if ($fechainicio!='') {
            $this->db->where('ace.fecha >=',$fechainicio);
        }
        if ($fechafin!='') {
            $this->db->where('ace.fecha <=',$fechafin);
        }
        
        $this->db->where(array('ac.activo'=>1));

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columnss as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    function getlistaservicioct($params){
        $tiposer = $params['tiposer'];
        $tecnico = $params['tecnico'];
        $status = $params['status'];
        $zona = $params['zona'];
        $pcliente = $params['pcliente'];
        $fechainicio = $params['fechainicio'];
        $fechafin = $params['fechafin'];
        $columns = array( 
            0=>'ace.asignacionIde',
            1=>'per.personalId',
            2=>"CONCAT(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno) as tecnico",
            3=>'cli.id',
            4=>'cli.empresa',
            5=>'ace.fecha',
            6=>'ace.hora',
            7=>'ace.zonaId',
            8=>'ru.nombre',
            9=>'rde.modelo',
            10=>'serp.serie',
            11=>'ace.status',
            12=>'ac.tiposervicio',
            13=>'ace.horafin',
            14=>'sereve.nombre as servicio',
        );
        $columnss = array( 
            0=>'ace.asignacionIde',
            1=>'per.personalId',
            2=>"CONCAT(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno)",
            3=>'cli.id',
            4=>'cli.empresa',
            5=>'ace.fecha',
            6=>'ace.hora',
            7=>'ac.zonaId',
            8=>'ru.nombre',
            9=>'rde.modelo',
            10=>'serp.serie',
            11=>'ace.status',
            12=>'ac.tiposervicio',
            13=>'ace.horafin',
            14=>'sereve.nombre',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(*) as total');
        $this->db->from('asignacion_ser_contrato_a_e ace');
        $this->db->join('asignacion_ser_contrato_a ac','ac.asignacionId=ace.asignacionId');
        $this->db->join('personal per','per.personalId=ace.tecnico','left');
        $this->db->join('contrato con','con.idcontrato=ac.contratoId');
        $this->db->join('rentas ren','ren.id=con.idRenta');
        $this->db->join('clientes cli','cli.id=ren.idCliente');
        $this->db->join('rutas ru','ru.id=ac.zonaId');
        $this->db->join('rentas_has_detallesEquipos rde','rde.id=ace.idequipo');
        $this->db->join('series_productos serp','serp.serieId=ace.serieId');
        $this->db->join('servicio_evento sereve','sereve.id=ac.tservicio','left');
        
        if ($tiposer==1) {
            $this->db->where(array('ac.tiposervicio'=>0));
        }
        if ($tiposer==2) {
            $this->db->where(array('ac.tiposervicio'=>1));
        }
        if ($tecnico>0) {
            $this->db->where(array('per.personalId'=>$tecnico));
        }
        if ($status==1) {
            $this->db->where('ace.status >=',0);
            $this->db->where('ace.status <=',1);
        }
        if ($status==2) {
            $this->db->where(array('ace.status'=>2));
        }
        if ($zona>0) {
            $this->db->where(array('ac.zonaId'=>$zona));
        }
        if ($pcliente>0) {
            $this->db->where(array('cli.id'=>$pcliente));
        }
        if ($fechainicio!='') {
            $this->db->where('ace.fecha >=',$fechainicio);
        }
        if ($fechafin!='') {
            $this->db->where('ace.fecha <=',$fechafin);
        }
        $this->db->where(array('ac.activo'=>1));
        /*
        $where = array('ca.status'=>1);
        $this->db->where($where);
        */
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columnss as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        //$this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        //$this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query->row()->total;
    }
//==============contratos solicitud refacciones==========
    function getlistasolicitud_Refacciones($params){
        $tiposer = $params['tiposer'];
        $tecnico = $params['tecnico'];
        $status = $params['status'];
        $zona = $params['zona'];
        $pcliente = $params['pcliente'];
        //$fechainicio = $params['fechainicio'];
        //$fechafin = $params['fechafin'];
        $columns = array( 
            0=>'ace.asignacionIde',
            1=>'per.personalId',
            2=>"CONCAT(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno) as tecnico",
            3=>'cli.id',
            4=>'cli.empresa',
            5=>'ace.fecha',
            6=>'ace.hora',
            7=>'ac.zonaId',
            8=>'ru.nombre',
            9=>'rde.modelo',
            10=>'serp.serie',
            11=>'ace.status',
            12=>'ac.tiposervicio',
            13=>'ace.horafin',
            14=>'ac.asignacionId',
            15=>'sereve.nombre as servicio',
            16=>'ac.contratoId',
            17=>'ace.cot_status',
            18=>'ace.cot_refaccion',
            19=>'ace.cot_tipo',
            20=>'ace.cot_referencia',
            21=>'ace.cot_referencia_tipo'
        );
        $columnss = array( 
            0=>'ace.asignacionIde',
            1=>'per.personalId',
            2=>"CONCAT(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno)",
            3=>'cli.id',
            4=>'cli.empresa',
            5=>'ac.fecha',
            6=>'ac.hora',
            7=>'ac.zonaId',
            8=>'ru.nombre',
            9=>'rde.modelo',
            10=>'serp.serie',
            11=>'ace.status',
            12=>'ac.tiposervicio',
            13=>'ace.horafin',
            14=>'sereve.nombre',
            15=>'ac.contratoId',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('asignacion_ser_contrato_a_e ace');
        $this->db->join('asignacion_ser_contrato_a ac','ac.asignacionId=ace.asignacionId');
        $this->db->join('personal per','per.personalId=ace.tecnico','left');
        $this->db->join('contrato con','con.idcontrato=ac.contratoId');
        $this->db->join('rentas ren','ren.id=con.idRenta');
        $this->db->join('clientes cli','cli.id=ren.idCliente');
        $this->db->join('rutas ru','ru.id=ac.zonaId');
        $this->db->join('rentas_has_detallesEquipos rde','rde.id=ace.idequipo');
        $this->db->join('series_productos serp','serp.serieId=ace.serieId');
        $this->db->join('servicio_evento sereve','sereve.id=ac.tservicio','left');
        /*
        $where = array('ca.status'=>1);
        $this->db->where($where);
        */
        if ($tiposer==1) {
            $this->db->where(array('ac.tiposervicio'=>0));
        }
        if ($tiposer==2) {
            $this->db->where(array('ac.tiposervicio'=>1));
        }
        if ($tecnico>0) {
            $this->db->where(array('per.personalId'=>$tecnico));
        }
        if ($status==1) {
            $this->db->where('ace.status >=',0);
            $this->db->where('ace.status <=',1);
        }
        if ($status==2) {
            $this->db->where(array('ace.status'=>2));
        }
        if ($zona>0) {
            $this->db->where(array('ac.zonaId'=>$zona));
        }
        if ($pcliente>0) {
            $this->db->where(array('cli.id'=>$pcliente));
        }
        $this->db->where(array('ace.cot_status !='=>0));
        /*
        if ($fechainicio!='') {
            $this->db->where('ace.fecha >=',$fechainicio);
        }
        if ($fechafin!='') {
            $this->db->where('ace.fecha <=',$fechafin);
        }
        */


        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columnss as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    function getlistasolicitud_Refaccionest($params){
        $tiposer = $params['tiposer'];
        $tecnico = $params['tecnico'];
        $status = $params['status'];
        $zona = $params['zona'];
        $pcliente = $params['pcliente'];
        //$fechainicio = $params['fechainicio'];
        //$fechafin = $params['fechafin'];
        $columns = array( 
            0=>'ace.asignacionIde',
            1=>'per.personalId',
            2=>"CONCAT(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno) as tecnico",
            3=>'cli.id',
            4=>'cli.empresa',
            5=>'ace.fecha',
            6=>'ace.hora',
            7=>'ace.zonaId',
            8=>'ru.nombre',
            9=>'rde.modelo',
            10=>'serp.serie',
            11=>'ace.status',
            12=>'ac.tiposervicio',
            13=>'ace.horafin',
            14=>'sereve.nombre as servicio',
            15=>'ac.contratoId',
        );
        $columnss = array( 
            0=>'ace.asignacionIde',
            1=>'per.personalId',
            2=>"CONCAT(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno)",
            3=>'cli.id',
            4=>'cli.empresa',
            5=>'ace.fecha',
            6=>'ace.hora',
            7=>'ac.zonaId',
            8=>'ru.nombre',
            9=>'rde.modelo',
            10=>'serp.serie',
            11=>'ace.status',
            12=>'ac.tiposervicio',
            13=>'ace.horafin',
            14=>'sereve.nombre',
            15=>'ac.contratoId',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(*) as total');
        $this->db->from('asignacion_ser_contrato_a_e ace');
        $this->db->join('asignacion_ser_contrato_a ac','ac.asignacionId=ace.asignacionId');
        $this->db->join('personal per','per.personalId=ace.tecnico','left');
        $this->db->join('contrato con','con.idcontrato=ac.contratoId');
        $this->db->join('rentas ren','ren.id=con.idRenta');
        $this->db->join('clientes cli','cli.id=ren.idCliente');
        $this->db->join('rutas ru','ru.id=ac.zonaId');
        $this->db->join('rentas_has_detallesEquipos rde','rde.id=ace.idequipo');
        $this->db->join('series_productos serp','serp.serieId=ace.serieId');
        $this->db->join('servicio_evento sereve','sereve.id=ac.tservicio','left');
        
        if ($tiposer==1) {
            $this->db->where(array('ac.tiposervicio'=>0));
        }
        if ($tiposer==2) {
            $this->db->where(array('ac.tiposervicio'=>1));
        }
        if ($tecnico>0) {
            $this->db->where(array('per.personalId'=>$tecnico));
        }
        if ($status==1) {
            $this->db->where('ace.status >=',0);
            $this->db->where('ace.status <=',1);
        }
        if ($status==2) {
            $this->db->where(array('ace.status'=>2));
        }
        if ($zona>0) {
            $this->db->where(array('ac.zonaId'=>$zona));
        }
        if ($pcliente>0) {
            $this->db->where(array('cli.id'=>$pcliente));
        }
        $this->db->where(array('ace.cot_status !='=>0));
        /*
        if ($fechainicio!='') {
            $this->db->where('ace.fecha >=',$fechainicio);
        }
        if ($fechafin!='') {
            $this->db->where('ace.fecha <=',$fechafin);
        }
        */
        /*
        $where = array('ca.status'=>1);
        $this->db->where($where);
        */
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columnss as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        //$this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        //$this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query->row()->total;
    }    
//==============poliza==========
    function getlistaserviciop($params){
        $tiposer = $params['tiposer'];
        $tecnico = $params['tecnico'];
        $status = $params['status'];
        $zona = $params['zona'];
        $pcliente = $params['pcliente'];
        $fechainicio = $params['fechainicio'];
        $fechafin = $params['fechafin'];
        $columns = array( 
            0=>'ape.asignacionIde',
            1=>'per.personalId',
            2=>"CONCAT(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno) as tecnico",
            3=>'cli.id',
            4=>'cli.empresa',
            5=>'ape.fecha',
            6=>'ape.hora',
            7=>'ap.zonaId',
            8=>'ru.nombre',
            9=>'equ.modelo',
            10=>'ape.serie',
            11=>'ape.status',
            12=>'ap.tiposervicio',
            13=>'ape.horafin',
            14=>'ap.asignacionId',
            15=>'sereve.nombre as servicio',
            16=>'ape.t_fecha',
            17=>'ape.horaserinicio',
            18=>'ape.horaserfin',
            19=>'ape.horaserinicio',
            20=>'ape.horaserfin',
            21=>'ape.cot_referencia',
            22=>'ape.horaserinicioext',
            23=>'ape.horaserfinext',
            24=>'ape.comentario'
        );
        $columnsss = array( 
            0=>'ape.asignacionIde',
            1=>'per.personalId',
            2=>"CONCAT(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno) as tecnico",
            3=>'cli.id',
            4=>'cli.empresa',
            5=>'ape.fecha',
            6=>'ape.hora',
            7=>'ape.zonaId',
            8=>'ru.nombre',
            9=>'equ.modelo',
            10=>'ape.serie',
            11=>'ape.status',
            12=>'ap.tiposervicio',
            13=>'ape.horafin',
            14=>'sereve.nombre',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('asignacion_ser_poliza_a_e ape');
        $this->db->join('asignacion_ser_poliza_a ap','ap.asignacionId=ape.asignacionId');
        $this->db->join('personal per','per.personalId=ape.tecnico','left');
        $this->db->join('polizasCreadas poc','poc.id=ap.polizaId');
        $this->db->join('clientes cli','cli.id=poc.idCliente');
        $this->db->join('rutas ru','ru.id=ap.zonaId');
        $this->db->join('polizasCreadas_has_detallesPoliza pocde','pocde.id=ape.idequipo');
        $this->db->join('equipos equ','equ.id=pocde.idEquipo');
        $this->db->join('servicio_evento sereve','sereve.id=ap.tservicio','left');
        if ($tiposer==1) {
            $this->db->where(array('ap.tiposervicio'=>0));
        }
        if ($tiposer==2) {
            $this->db->where(array('ap.tiposervicio'=>1));
        }
        if ($tecnico>0) {
            $this->db->where(array('per.personalId'=>$tecnico));
        }
        if ($status==1) {
            $this->db->where('ape.status >=',0);
            $this->db->where('ape.status <=',1);
        }
        if ($status==2) {
            $this->db->where(array('ape.status'=>2));
        }
        if ($zona>0) {
            $this->db->where(array('ap.zonaId'=>$zona));
        }
        if ($pcliente>0) {
            $this->db->where(array('cli.id'=>$pcliente));
        }
        if ($fechainicio!='') {
            $this->db->where('ape.fecha >=',$fechainicio);
        }
        if ($fechafin!='') {
            $this->db->where('ape.fecha <=',$fechafin);
        }
        $this->db->where(array('ap.activo'=>1));
        /*
        $where = array('ca.status'=>1);
        $this->db->where($where);
        */
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columnsss as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    function getlistaserviciopt($params){
        $tiposer = $params['tiposer'];
        $tecnico = $params['tecnico'];
        $status = $params['status'];
        $zona = $params['zona'];
        $pcliente = $params['pcliente'];
        $fechainicio = $params['fechainicio'];
        $fechafin = $params['fechafin'];
        

        $columns = array( 
            0=>'ape.asignacionIde',
            1=>'per.personalId',
            2=>"CONCAT(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno) as tecnico",
            3=>'cli.id',
            4=>'cli.empresa',
            5=>'ape.fecha',
            6=>'ape.hora',
            7=>'ape.zonaId',
            8=>'ru.nombre',
            9=>'equ.modelo',
            10=>'ape.serie',
            11=>'ape.status',
            12=>'ap.tiposervicio',
            13=>'ape.horafin',
            14=>'sereve.nombre as servicio',
        );
        $columnsss = array( 
            0=>'ape.asignacionIde',
            1=>'per.personalId',
            2=>"CONCAT(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno) as tecnico",
            3=>'cli.id',
            4=>'cli.empresa',
            5=>'ap.fecha',
            6=>'ap.hora',
            7=>'ap.zonaId',
            8=>'ru.nombre',
            9=>'equ.modelo',
            10=>'ape.serie',
            11=>'ape.status',
            12=>'ap.tiposervicio',
            13=>'ape.horafin',
            14=>'sereve.nombre',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(*) as total');
        $this->db->from('asignacion_ser_poliza_a_e ape');
        $this->db->join('asignacion_ser_poliza_a ap','ap.asignacionId=ape.asignacionId');
        $this->db->join('personal per','per.personalId=ape.tecnico','left');
        $this->db->join('polizasCreadas poc','poc.id=ap.polizaId');
        $this->db->join('clientes cli','cli.id=poc.idCliente');
        $this->db->join('rutas ru','ru.id=ap.zonaId');
        $this->db->join('polizasCreadas_has_detallesPoliza pocde','pocde.id=ape.idequipo');
        $this->db->join('equipos equ','equ.id=pocde.idEquipo');
        $this->db->join('servicio_evento sereve','sereve.id=ap.tservicio','left');
        if ($tiposer==1) {
            $this->db->where(array('ap.tiposervicio'=>0));
        }
        if ($tiposer==2) {
            $this->db->where(array('ap.tiposervicio'=>1));
        }
        if ($tecnico>0) {
            $this->db->where(array('per.personalId'=>$tecnico));
        }
        if ($status==1) {
            $this->db->where('ape.status >=',0);
            $this->db->where('ape.status <=',1);
        }
        if ($status==2) {
            $this->db->where(array('ape.status'=>2));
        }
        if ($zona>0) {
            $this->db->where(array('ap.zonaId'=>$zona));
        }
        if ($pcliente>0) {
            $this->db->where(array('cli.id'=>$pcliente));
        }
        if ($fechainicio!='') {
            $this->db->where('ape.fecha >=',$fechainicio);
        }
        if ($fechafin!='') {
            $this->db->where('ape.fecha <=',$fechafin);
        }
        $this->db->where(array('ap.activo'=>1));
        /*
        $where = array('ca.status'=>1);
        $this->db->where($where);
        */
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columnsss as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        //$this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        //$this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query->row()->total;
    }
    //=========poliza Solicud de refacciones==========
    function getlistaserviciop_solicitud($params){
        $tiposer = $params['tiposer'];
        $tecnico = $params['tecnico'];
        $status = $params['status'];
        $zona = $params['zona'];
        $pcliente = $params['pcliente'];
        //$fechainicio = $params['fechainicio'];
        //$fechafin = $params['fechafin'];
        $columns = array( 
            0=>'ape.asignacionIde',
            1=>'per.personalId',
            2=>"CONCAT(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno) as tecnico",
            3=>'cli.id',
            4=>'cli.empresa',
            5=>'ape.fecha',
            6=>'ape.hora',
            7=>'ap.zonaId',
            8=>'ru.nombre',
            9=>'equ.modelo',
            10=>'ape.serie',
            11=>'ape.status',
            12=>'ap.tiposervicio',
            13=>'ape.horafin',
            14=>'ap.asignacionId',
            15=>'sereve.nombre as servicio',
            16=>'ape.cot_status',
            17=>'ape.cot_refaccion',
            18=>'ape.cot_tipo',
            19=>'ape.cot_referencia',
            20=>'ape.cot_referencia_tipo'
        );
        $columnsss = array( 
            0=>'ape.asignacionIde',
            1=>'per.personalId',
            2=>"CONCAT(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno) as tecnico",
            3=>'cli.id',
            4=>'cli.empresa',
            5=>'ape.fecha',
            6=>'ape.hora',
            7=>'ape.zonaId',
            8=>'ru.nombre',
            9=>'equ.modelo',
            10=>'ape.serie',
            11=>'ape.status',
            12=>'ap.tiposervicio',
            13=>'ape.horafin',
            14=>'sereve.nombre',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('asignacion_ser_poliza_a_e ape');
        $this->db->join('asignacion_ser_poliza_a ap','ap.asignacionId=ape.asignacionId');
        $this->db->join('personal per','per.personalId=ape.tecnico','left');
        $this->db->join('polizasCreadas poc','poc.id=ap.polizaId');
        $this->db->join('clientes cli','cli.id=poc.idCliente');
        $this->db->join('rutas ru','ru.id=ap.zonaId');
        $this->db->join('polizasCreadas_has_detallesPoliza pocde','pocde.id=ape.idequipo');
        $this->db->join('equipos equ','equ.id=pocde.idEquipo');
        $this->db->join('servicio_evento sereve','sereve.id=ap.tservicio','left');
        if ($tiposer==1) {
            $this->db->where(array('ap.tiposervicio'=>0));
        }
        if ($tiposer==2) {
            $this->db->where(array('ap.tiposervicio'=>1));
        }
        if ($tecnico>0) {
            $this->db->where(array('per.personalId'=>$tecnico));
        }
        if ($status==1) {
            $this->db->where('ape.status >=',0);
            $this->db->where('ape.status <=',1);
        }
        if ($status==2) {
            $this->db->where(array('ape.status'=>2));
        }
        if ($zona>0) {
            $this->db->where(array('ap.zonaId'=>$zona));
        }
        if ($pcliente>0) {
            $this->db->where(array('cli.id'=>$pcliente));
        }
        $this->db->where(array('ape.cot_status !='=>0));
        /*
        if ($fechainicio!='') {
            $this->db->where('ape.fecha >=',$fechainicio);
        }
        if ($fechafin!='') {
            $this->db->where('ape.fecha <=',$fechafin);
        }
        */
        /*
        $where = array('ca.status'=>1);
        $this->db->where($where);
        */
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columnsss as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    function getlistaserviciopt_solicitud($params){
        $tiposer = $params['tiposer'];
        $tecnico = $params['tecnico'];
        $status = $params['status'];
        $zona = $params['zona'];
        $pcliente = $params['pcliente'];
        //$fechainicio = $params['fechainicio'];
        //$fechafin = $params['fechafin'];
        

        $columns = array( 
            0=>'ape.asignacionIde',
            1=>'per.personalId',
            2=>"CONCAT(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno) as tecnico",
            3=>'cli.id',
            4=>'cli.empresa',
            5=>'ape.fecha',
            6=>'ape.hora',
            7=>'ape.zonaId',
            8=>'ru.nombre',
            9=>'equ.modelo',
            10=>'ape.serie',
            11=>'ape.status',
            12=>'ap.tiposervicio',
            13=>'ape.horafin',
            14=>'sereve.nombre as servicio',
        );
        $columnsss = array( 
            0=>'ape.asignacionIde',
            1=>'per.personalId',
            2=>"CONCAT(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno) as tecnico",
            3=>'cli.id',
            4=>'cli.empresa',
            5=>'ap.fecha',
            6=>'ap.hora',
            7=>'ap.zonaId',
            8=>'ru.nombre',
            9=>'equ.modelo',
            10=>'ape.serie',
            11=>'ape.status',
            12=>'ap.tiposervicio',
            13=>'ape.horafin',
            14=>'sereve.nombre',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(*) as total');
        $this->db->from('asignacion_ser_poliza_a_e ape');
        $this->db->join('asignacion_ser_poliza_a ap','ap.asignacionId=ape.asignacionId');
        $this->db->join('personal per','per.personalId=ape.tecnico','left');
        $this->db->join('polizasCreadas poc','poc.id=ap.polizaId');
        $this->db->join('clientes cli','cli.id=poc.idCliente');
        $this->db->join('rutas ru','ru.id=ap.zonaId');
        $this->db->join('polizasCreadas_has_detallesPoliza pocde','pocde.id=ape.idequipo');
        $this->db->join('equipos equ','equ.id=pocde.idEquipo');
        $this->db->join('servicio_evento sereve','sereve.id=ap.tservicio','left');
        if ($tiposer==1) {
            $this->db->where(array('ap.tiposervicio'=>0));
        }
        if ($tiposer==2) {
            $this->db->where(array('ap.tiposervicio'=>1));
        }
        if ($tecnico>0) {
            $this->db->where(array('per.personalId'=>$tecnico));
        }
        if ($status==1) {
            $this->db->where('ape.status >=',0);
            $this->db->where('ape.status <=',1);
        }
        if ($status==2) {
            $this->db->where(array('ape.status'=>2));
        }
        if ($zona>0) {
            $this->db->where(array('ap.zonaId'=>$zona));
        }
        if ($pcliente>0) {
            $this->db->where(array('cli.id'=>$pcliente));
        }
        $this->db->where(array('ape.cot_status !='=>0));
        /*
        if ($fechainicio!='') {
            $this->db->where('ape.fecha >=',$fechainicio);
        }
        if ($fechafin!='') {
            $this->db->where('ape.fecha <=',$fechafin);
        }
        */
        /*
        $where = array('ca.status'=>1);
        $this->db->where($where);
        */
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columnsss as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        //$this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        //$this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query->row()->total;
    }
//==============clientes==========
    function getlistaserviciocl($params){
        $tiposer = $params['tiposer'];
        $tecnico = $params['tecnico'];
        $status = $params['status'];
        $zona = $params['zona'];
        $pcliente = $params['pcliente'];
        $fechainicio = $params['fechainicio'];
        $fechafin = $params['fechafin'];

        $columns = array( 
            0=>'acl.asignacionId',
            1=>'per.personalId',
            2=>"CONCAT(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno) as tecnico",
            3=>'cli.id',
            4=>'cli.empresa',
            5=>'acl.fecha',
            6=>'acl.hora',
            7=>'acl.horafin',
            8=>'acl.zonaId',
            9=>'acl.prioridad',
            10=>'zo.nombre',
            11=>'acld.status',
            12=>'acl.tiposervicio',
            13=>'sereve.nombre as servicio',
            14=>'acl.fecha',
            15=>'acld.horaserinicio',
            16=>'acld.horaserfin',
            17=>'acld.serie',
            18=>'acld.asignacionIdd',
            19=>'acld.horaserinicioext',
            20=>'acld.horaserfinext',
            21=>'acl.comentario'
        );
        $columnss = array( 
            0=>'acl.asignacionId',
            1=>'per.personalId',
            2=>"CONCAT(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno)",
            3=>'cli.id',
            4=>'cli.empresa',
            5=>'acl.fecha',
            6=>'acl.hora',
            7=>'acl.horafin',
            8=>'acl.zonaId',
            9=>'acl.prioridad',
            10=>'zo.nombre',
            11=>'acld.status',
            12=>'acl.tiposervicio',
            13=>'sereve.nombre',
            14=>'acl.fecha',
            15=>'acld.horaserinicio',
            16=>'acld.horaserfin',
            17=>'acld.serie',
            18=>'acld.asignacionIdd',
            19=>'acld.horaserinicioext',
            20=>'acld.horaserfinext'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('asignacion_ser_cliente_a acl');
        $this->db->join('personal per','per.personalId=acl.tecnico');
        $this->db->join('clientes cli','cli.id=acl.clienteId');
        $this->db->join('rutas zo','zo.id=acl.zonaId');
        $this->db->join('asignacion_ser_cliente_a_d acld','acld.asignacionId=acl.asignacionId');
        $this->db->join('servicio_evento sereve','sereve.id=acld.tservicio');

        if ($tiposer==1) {
            $this->db->where(array('acl.tiposervicio'=>0));
        }
        if ($tiposer==2) {
            $this->db->where(array('acl.tiposervicio'=>1));
        }
        if ($tecnico>0) {
            $this->db->where(array('per.personalId'=>$tecnico));
        }
        if ($status==1) {
            $this->db->where('acl.status >=',0);
            $this->db->where('acl.status <=',1);
        }
        if ($status==2) {
            $this->db->where(array('acl.status'=>2));
        }
        if ($zona>0) {
            $this->db->where(array('acl.zonaId'=>$zona));
        }
        if ($pcliente>0) {
            $this->db->where(array('cli.id'=>$pcliente));
        }
        if ($fechainicio!='') {
            $this->db->where('acl.fecha >=',$fechainicio);
        }
        if ($fechafin!='') {
            $this->db->where('acl.fecha <=',$fechafin);
        }

        $this->db->where(array('acl.activo'=>1));


        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columnss as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    function getlistaservicioclt($params){
        $tiposer = $params['tiposer'];
        $tecnico = $params['tecnico'];
        $status = $params['status'];
        $zona = $params['zona'];
        $pcliente = $params['pcliente'];
        $fechainicio = $params['fechainicio'];
        $fechafin = $params['fechafin'];
        $columns = array( 
            0=>'acl.asignacionId',
            1=>'per.personalId',
            2=>"CONCAT(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno) as tecnico",
            3=>'cli.id',
            4=>'cli.empresa',
            5=>'acl.fecha',
            6=>'acl.hora',
            7=>'acl.horafin',
            8=>'acl.zonaId',
            9=>'acl.prioridad',
            10=>'zo.nombre',
            11=>'acld.status',
            12=>'acl.tiposervicio',
            13=>'sereve.nombre as servicio',
            14=>'acl.fecha',
            15=>'acld.horaserinicio',
            16=>'acld.horaserfin',
            17=>'acld.serie',
            18=>'acld.asignacionIdd',
            19=>'acld.horaserinicioext',
            20=>'acld.horaserfinext'
        );
        $columnss = array( 
            0=>'acl.asignacionId',
            1=>'per.personalId',
            2=>"CONCAT(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno)",
            3=>'cli.id',
            4=>'cli.empresa',
            5=>'acl.fecha',
            6=>'acl.hora',
            7=>'acl.horafin',
            8=>'acl.zonaId',
            9=>'acl.prioridad',
            10=>'zo.nombre',
            11=>'acld.status',
            12=>'acl.tiposervicio',
            13=>'sereve.nombre',
            14=>'acl.fecha',
            15=>'acld.horaserinicio',
            16=>'acld.horaserfin',
            17=>'acld.serie',
            18=>'acld.asignacionIdd',
            19=>'acld.horaserinicioext',
            20=>'acld.horaserfinext'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(*) as total');
        $this->db->from('asignacion_ser_cliente_a acl');
        $this->db->join('personal per','per.personalId=acl.tecnico');
        $this->db->join('clientes cli','cli.id=acl.clienteId');
        $this->db->join('rutas zo','zo.id=acl.zonaId');
        
        $this->db->join('asignacion_ser_cliente_a_d acld','acld.asignacionId=acl.asignacionId');
        $this->db->join('servicio_evento sereve','sereve.id=acld.tservicio');
        if ($tiposer==1) {
            $this->db->where(array('acl.tiposervicio'=>0));
        }
        if ($tiposer==2) {
            $this->db->where(array('acl.tiposervicio'=>1));
        }
        if ($tecnico>0) {
            $this->db->where(array('per.personalId'=>$tecnico));
        }
        if ($status==1) {
            $this->db->where('acl.status >=',0);
            $this->db->where('acl.status <=',1);
        }
        if ($status==2) {
            $this->db->where(array('acl.status'=>2));
        }
        if ($zona>0) {
            $this->db->where(array('acl.zonaId'=>$zona));
        }
        if ($pcliente>0) {
            $this->db->where(array('cli.id'=>$pcliente));
        }
        if ($fechainicio!='') {
            $this->db->where('acl.fecha >=',$fechainicio);
        }
        if ($fechafin!='') {
            $this->db->where('acl.fecha <=',$fechafin);
        }
        $this->db->where(array('acl.activo'=>1));
        /*
        $where = array('ca.status'=>1);
        $this->db->where($where);
        */
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columnss as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        //$this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        //$this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query->row()->total;
    }
//==============clientes solicitudes==========
    function getlistaserviciocl_solicitudes($params){
        $tiposer = $params['tiposer'];
        $tecnico = $params['tecnico'];
        $status = $params['status'];
        $zona = $params['zona'];
        $pcliente = $params['pcliente'];
        //$fechainicio = $params['fechainicio'];
        //$fechafin = $params['fechafin'];
        $columns = array( 
            0=>'acl.asignacionId',
            1=>'per.personalId',
            2=>"CONCAT(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno) as tecnico",
            3=>'cli.id',
            4=>'cli.empresa',
            5=>'acl.fecha',
            6=>'acl.hora',
            7=>'acl.horafin',
            8=>'acl.zonaId',
            9=>'acl.prioridad',
            10=>'zo.nombre',
            11=>'acl.status',
            12=>'acl.tiposervicio',
            13=>'sereve.nombre as servicio',
            14=>'acl.cot_status',
            15=>'acl.cot_refaccion',
            16=>'acl.cot_tipo',
            17=>'acl.cot_referencia',
            18=>'acl.cot_referencia_tipo'
        );
        $columnss = array( 
            0=>'acl.asignacionId',
            1=>'per.personalId',
            2=>"CONCAT(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno)",
            3=>'cli.id',
            4=>'cli.empresa',
            5=>'acl.fecha',
            6=>'acl.hora',
            7=>'acl.horafin',
            8=>'acl.zonaId',
            9=>'acl.prioridad',
            10=>'zo.nombre',
            11=>'acl.status',
            12=>'acl.tiposervicio',
            13=>'sereve.nombre',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('asignacion_ser_cliente_a acl');
        $this->db->join('personal per','per.personalId=acl.tecnico');
        $this->db->join('clientes cli','cli.id=acl.clienteId');
        $this->db->join('rutas zo','zo.id=acl.zonaId');
        $this->db->join('servicio_evento sereve','sereve.id=acl.tservicio','left');

        if ($tiposer==1) {
            $this->db->where(array('acl.tiposervicio'=>0));
        }
        if ($tiposer==2) {
            $this->db->where(array('acl.tiposervicio'=>1));
        }
        if ($tecnico>0) {
            $this->db->where(array('per.personalId'=>$tecnico));
        }
        if ($status==1) {
            $this->db->where('acl.status >=',0);
            $this->db->where('acl.status <=',1);
        }
        if ($status==2) {
            $this->db->where(array('acl.status'=>2));
        }
        if ($zona>0) {
            $this->db->where(array('acl.zonaId'=>$zona));
        }
        if ($pcliente>0) {
            $this->db->where(array('cli.id'=>$pcliente));
        }
        $this->db->where(array('acl.cot_status !='=>0));
        /*
        if ($fechainicio!='') {
            $this->db->where('acl.fecha >=',$fechainicio);
        }
        if ($fechafin!='') {
            $this->db->where('acl.fecha <=',$fechafin);
        }
        */

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columnss as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    function getlistaservicioclt_solicitudes($params){
        $tiposer = $params['tiposer'];
        $tecnico = $params['tecnico'];
        $status = $params['status'];
        $zona = $params['zona'];
        $pcliente = $params['pcliente'];
        //$fechainicio = $params['fechainicio'];
        //$fechafin = $params['fechafin'];
        $columns = array( 
            0=>'acl.asignacionId',
            1=>'per.personalId',
            2=>"CONCAT(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno) as tecnico",
            3=>'cli.id',
            4=>'cli.empresa',
            5=>'acl.fecha',
            6=>'acl.hora',
            7=>'acl.horafin',
            8=>'acl.zonaId',
            9=>'acl.prioridad',
            10=>'zo.nombre',
            11=>'acl.status',
            12=>'acl.tiposervicio',
            13=>'sereve.nombre as servicio',
        );
        $columnss = array( 
            0=>'acl.asignacionId',
            1=>'per.personalId',
            2=>"CONCAT(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno)",
            3=>'cli.id',
            4=>'cli.empresa',
            5=>'acl.fecha',
            6=>'acl.hora',
            7=>'acl.horafin',
            8=>'acl.zonaId',
            9=>'acl.prioridad',
            10=>'zo.nombre',
            11=>'acl.status',
            12=>'acl.tiposervicio',
            13=>'sereve.nombre',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(*) as total');
        $this->db->from('asignacion_ser_cliente_a acl');
        $this->db->join('personal per','per.personalId=acl.tecnico');
        $this->db->join('clientes cli','cli.id=acl.clienteId');
        $this->db->join('rutas zo','zo.id=acl.zonaId');
        $this->db->join('servicio_evento sereve','sereve.id=acl.tservicio','left');

        if ($tiposer==1) {
            $this->db->where(array('acl.tiposervicio'=>0));
        }
        if ($tiposer==2) {
            $this->db->where(array('acl.tiposervicio'=>1));
        }
        if ($tecnico>0) {
            $this->db->where(array('per.personalId'=>$tecnico));
        }
        if ($status==1) {
            $this->db->where('acl.status >=',0);
            $this->db->where('acl.status <=',1);
        }
        if ($status==2) {
            $this->db->where(array('acl.status'=>2));
        }
        if ($zona>0) {
            $this->db->where(array('acl.zonaId'=>$zona));
        }
        if ($pcliente>0) {
            $this->db->where(array('cli.id'=>$pcliente));
        }
        $this->db->where(array('acl.cot_status !='=>0));
        /*
        if ($fechainicio!='') {
            $this->db->where('acl.fecha >=',$fechainicio);
        }
        if ($fechafin!='') {
            $this->db->where('acl.fecha <=',$fechafin);
        }
        */
        /*
        $where = array('ca.status'=>1);
        $this->db->where($where);
        */
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columnss as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        //$this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        //$this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query->row()->total;
    }
//=========================================
//==============ventas==========
    function getlistaserviciovent($params){
        $tiposer = $params['tiposer'];
        $tecnico = $params['tecnico'];
        $status = $params['status'];
        $zona = $params['zona'];
        $pcliente = $params['pcliente'];
        $fechainicio = $params['fechainicio'];
        $fechafin = $params['fechafin'];

        $columns = array( 
            0=>'acl.asignacionId',
            1=>'per.personalId',
            2=>"CONCAT(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno) as tecnico",
            3=>'cli.id',
            4=>'cli.empresa',
            5=>'acl.fecha',
            6=>'acl.hora',
            7=>'acl.horafin',
            8=>'acl.zonaId',
            9=>'acl.prioridad',
            10=>'zo.nombre',
            11=>'acl.status',
            12=>'acl.tiposervicio',
            13=>'sereve.nombre as servicio',
            14=>'acl.t_fecha',
            15=>'acl.horaserinicio',
            16=>'acl.horaserfin',
            17=>'acl.horaserinicioext',
            18=>'acl.horaserfinext',
            19=>'acl.comentario',
        );
        $columnss = array( 
            0=>'acl.asignacionId',
            1=>'per.personalId',
            2=>"CONCAT(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno)",
            3=>'cli.id',
            4=>'cli.empresa',
            5=>'acl.fecha',
            6=>'acl.hora',
            7=>'acl.horafin',
            8=>'acl.zonaId',
            9=>'acl.prioridad',
            10=>'zo.nombre',
            11=>'acl.status',
            12=>'acl.tiposervicio',
            13=>'sereve.nombre',
            14=>'acl.horaserinicio',
            15=>'acl.horaserfin',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('asignacion_ser_venta_a acl');
        $this->db->join('personal per','per.personalId=acl.tecnico');
        $this->db->join('clientes cli','cli.id=acl.clienteId');
        $this->db->join('rutas zo','zo.id=acl.zonaId');
        $this->db->join('servicio_evento sereve','sereve.id=acl.tservicio','left');

        if ($tiposer==1) {
            $this->db->where(array('acl.tiposervicio'=>0));
        }
        if ($tiposer==2) {
            $this->db->where(array('acl.tiposervicio'=>1));
        }
        if ($tecnico>0) {
            $this->db->where(array('per.personalId'=>$tecnico));
        }
        if ($status==1) {
            $this->db->where('acl.status >=',0);
            $this->db->where('acl.status <=',1);
        }
        if ($status==2) {
            $this->db->where(array('acl.status'=>2));
        }
        if ($zona>0) {
            $this->db->where(array('acl.zonaId'=>$zona));
        }
        if ($pcliente>0) {
            $this->db->where(array('cli.id'=>$pcliente));
        }
        if ($fechainicio!='') {
            $this->db->where('acl.fecha >=',$fechainicio);
        }
        if ($fechafin!='') {
            $this->db->where('acl.fecha <=',$fechafin);
        }

        $this->db->where(array('acl.activo'=>1));


        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columnss as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    function getlistaservicioventt($params){
        $tiposer = $params['tiposer'];
        $tecnico = $params['tecnico'];
        $status = $params['status'];
        $zona = $params['zona'];
        $pcliente = $params['pcliente'];
        $fechainicio = $params['fechainicio'];
        $fechafin = $params['fechafin'];
        $columns = array( 
            0=>'acl.asignacionId',
            1=>'per.personalId',
            2=>"CONCAT(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno) as tecnico",
            3=>'cli.id',
            4=>'cli.empresa',
            5=>'acl.fecha',
            6=>'acl.hora',
            7=>'acl.horafin',
            8=>'acl.zonaId',
            9=>'acl.prioridad',
            10=>'zo.nombre',
            11=>'acl.status',
            12=>'acl.tiposervicio',
            13=>'sereve.nombre as servicio',
        );
        $columnss = array( 
            0=>'acl.asignacionId',
            1=>'per.personalId',
            2=>"CONCAT(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno)",
            3=>'cli.id',
            4=>'cli.empresa',
            5=>'acl.fecha',
            6=>'acl.hora',
            7=>'acl.horafin',
            8=>'acl.zonaId',
            9=>'acl.prioridad',
            10=>'zo.nombre',
            11=>'acl.status',
            12=>'acl.tiposervicio',
            13=>'sereve.nombre',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(*) as total');
        $this->db->from('asignacion_ser_venta_a acl');
        $this->db->join('personal per','per.personalId=acl.tecnico');
        $this->db->join('clientes cli','cli.id=acl.clienteId');
        $this->db->join('rutas zo','zo.id=acl.zonaId');
        $this->db->join('servicio_evento sereve','sereve.id=acl.tservicio','left');

        if ($tiposer==1) {
            $this->db->where(array('acl.tiposervicio'=>0));
        }
        if ($tiposer==2) {
            $this->db->where(array('acl.tiposervicio'=>1));
        }
        if ($tecnico>0) {
            $this->db->where(array('per.personalId'=>$tecnico));
        }
        if ($status==1) {
            $this->db->where('acl.status >=',0);
            $this->db->where('acl.status <=',1);
        }
        if ($status==2) {
            $this->db->where(array('acl.status'=>2));
        }
        if ($zona>0) {
            $this->db->where(array('acl.zonaId'=>$zona));
        }
        if ($pcliente>0) {
            $this->db->where(array('cli.id'=>$pcliente));
        }
        if ($fechainicio!='') {
            $this->db->where('acl.fecha >=',$fechainicio);
        }
        if ($fechafin!='') {
            $this->db->where('acl.fecha <=',$fechafin);
        }
        $this->db->where(array('acl.activo'=>1));
        /*
        $where = array('ca.status'=>1);
        $this->db->where($where);
        */
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columnss as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        //$this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        //$this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query->row()->total;
    }
//==============clientes solicitudes==========
    function getlistaserviciovent_solicitudes($params){
        $tiposer = $params['tiposer'];
        $tecnico = $params['tecnico'];
        $status = $params['status'];
        $zona = $params['zona'];
        $pcliente = $params['pcliente'];
        //$fechainicio = $params['fechainicio'];
        //$fechafin = $params['fechafin'];
        $columns = array( 
            0=>'acl.asignacionId',
            1=>'per.personalId',
            2=>"CONCAT(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno) as tecnico",
            3=>'cli.id',
            4=>'cli.empresa',
            5=>'acl.fecha',
            6=>'acl.hora',
            7=>'acl.horafin',
            8=>'acl.zonaId',
            9=>'acl.prioridad',
            10=>'zo.nombre',
            11=>'acl.status',
            12=>'acl.tiposervicio',
            13=>'sereve.nombre as servicio',
            14=>'acl.cot_status',
            15=>'acl.cot_refaccion',
            16=>'acl.cot_tipo',
            17=>'acl.cot_referencia',
            18=>'acl.cot_referencia_tipo'
        );
        $columnss = array( 
            0=>'acl.asignacionId',
            1=>'per.personalId',
            2=>"CONCAT(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno)",
            3=>'cli.id',
            4=>'cli.empresa',
            5=>'acl.fecha',
            6=>'acl.hora',
            7=>'acl.horafin',
            8=>'acl.zonaId',
            9=>'acl.prioridad',
            10=>'zo.nombre',
            11=>'acl.status',
            12=>'acl.tiposervicio',
            13=>'sereve.nombre',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('asignacion_ser_venta_a acl');
        $this->db->join('personal per','per.personalId=acl.tecnico');
        $this->db->join('clientes cli','cli.id=acl.clienteId');
        $this->db->join('rutas zo','zo.id=acl.zonaId');
        $this->db->join('servicio_evento sereve','sereve.id=acl.tservicio','left');

        if ($tiposer==1) {
            $this->db->where(array('acl.tiposervicio'=>0));
        }
        if ($tiposer==2) {
            $this->db->where(array('acl.tiposervicio'=>1));
        }
        if ($tecnico>0) {
            $this->db->where(array('per.personalId'=>$tecnico));
        }
        if ($status==1) {
            $this->db->where('acl.status >=',0);
            $this->db->where('acl.status <=',1);
        }
        if ($status==2) {
            $this->db->where(array('acl.status'=>2));
        }
        if ($zona>0) {
            $this->db->where(array('acl.zonaId'=>$zona));
        }
        if ($pcliente>0) {
            $this->db->where(array('cli.id'=>$pcliente));
        }
        $this->db->where(array('acl.cot_status !='=>0));
        /*
        if ($fechainicio!='') {
            $this->db->where('acl.fecha >=',$fechainicio);
        }
        if ($fechafin!='') {
            $this->db->where('acl.fecha <=',$fechafin);
        }
        */

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columnss as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    function getlistaservicioventt_solicitudes($params){
        $tiposer = $params['tiposer'];
        $tecnico = $params['tecnico'];
        $status = $params['status'];
        $zona = $params['zona'];
        $pcliente = $params['pcliente'];
        //$fechainicio = $params['fechainicio'];
        //$fechafin = $params['fechafin'];
        $columns = array( 
            0=>'acl.asignacionId',
            1=>'per.personalId',
            2=>"CONCAT(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno) as tecnico",
            3=>'cli.id',
            4=>'cli.empresa',
            5=>'acl.fecha',
            6=>'acl.hora',
            7=>'acl.horafin',
            8=>'acl.zonaId',
            9=>'acl.prioridad',
            10=>'zo.nombre',
            11=>'acl.status',
            12=>'acl.tiposervicio',
            13=>'sereve.nombre as servicio',
        );
        $columnss = array( 
            0=>'acl.asignacionId',
            1=>'per.personalId',
            2=>"CONCAT(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno)",
            3=>'cli.id',
            4=>'cli.empresa',
            5=>'acl.fecha',
            6=>'acl.hora',
            7=>'acl.horafin',
            8=>'acl.zonaId',
            9=>'acl.prioridad',
            10=>'zo.nombre',
            11=>'acl.status',
            12=>'acl.tiposervicio',
            13=>'sereve.nombre',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(*) as total');
        $this->db->from('asignacion_ser_venta_a acl');
        $this->db->join('personal per','per.personalId=acl.tecnico');
        $this->db->join('clientes cli','cli.id=acl.clienteId');
        $this->db->join('rutas zo','zo.id=acl.zonaId');
        $this->db->join('servicio_evento sereve','sereve.id=acl.tservicio','left');

        if ($tiposer==1) {
            $this->db->where(array('acl.tiposervicio'=>0));
        }
        if ($tiposer==2) {
            $this->db->where(array('acl.tiposervicio'=>1));
        }
        if ($tecnico>0) {
            $this->db->where(array('per.personalId'=>$tecnico));
        }
        if ($status==1) {
            $this->db->where('acl.status >=',0);
            $this->db->where('acl.status <=',1);
        }
        if ($status==2) {
            $this->db->where(array('acl.status'=>2));
        }
        if ($zona>0) {
            $this->db->where(array('acl.zonaId'=>$zona));
        }
        if ($pcliente>0) {
            $this->db->where(array('cli.id'=>$pcliente));
        }
        $this->db->where(array('acl.cot_status !='=>0));
        /*
        if ($fechainicio!='') {
            $this->db->where('acl.fecha >=',$fechainicio);
        }
        if ($fechafin!='') {
            $this->db->where('acl.fecha <=',$fechafin);
        }
        */
        /*
        $where = array('ca.status'=>1);
        $this->db->where($where);
        */
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columnss as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        //$this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        //$this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query->row()->total;
    }
//=========================================
    //==================================================================
  
}