<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class Modeloproprioritarios extends CI_Model {
    public function __construct() {
        parent::__construct();
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d');
        $this->fechahoyl = date('Y-m-d H:i:s');
    }

    function getsolicitudes($params){
        $tipo=$params['tipo'];
        $prioridad=$params['prioridad'];
        $columns = array( 
            0=>'tipo',
            1=>'sum(cantidad) as cantidad',
            2=>'modelo',
            3=>'prioridad',
            4=>'idpro',
            5=>'GROUP_CONCAT(idCotizacion) as idCotizaciones',
        );
        $columns_c = array( 
            0=>'tipo',
            1=>'cantidad',
            2=>'modelo',
            3=>'prioridad',
            4=>'idpro',
        );
        
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $subconsulta=$this->sql_query($params);

        $this->db->from($subconsulta);
        if($tipo>0){
            $this->db->where(array('tipo'=>$tipo));
        }
        if($prioridad>0){
            $this->db->where(array('prioridad'=>$prioridad));
        }
        $this->db->group_by(array("tipo", "idpro", "prioridad"));

        //$this->db->where(array('facturaabierta'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns_c as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns_c[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    public function getsolicitudes_total($params){
        $tipo=$params['tipo'];
        $prioridad=$params['prioridad'];
        $columns = array( 
            0=>'tipo',
            1=>'cantidad',
            2=>'modelo',
            3=>'prioridad',
            4=>'idpro',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('*');
        $subconsulta=$this->sql_query($params);
        if($tipo>0){
            $this->db->where(array('tipo'=>$tipo));
        }
        if($prioridad>0){
            $this->db->where(array('prioridad'=>$prioridad));
        }
        $this->db->group_by(array("tipo", "idpro", "prioridad"));

        $this->db->from($subconsulta);
        //$where = ;
        //$this->db->where(array('facturaabierta'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        //$this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        //$this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query->num_rows();
    }
    function sql_query($params){
        $tipo=$params['tipo'];
        $reg_fecha = strtotime ( '- 1 month', strtotime ($this->fechahoyl) ) ;
        $reg_fecha = date ( 'Y-m-d' , $reg_fecha );
        $where_reg=" and cot.reg >= '$reg_fecha'";
        //$w_status=' AND cot.estatus=1 ';
        $w_status=' AND cot.pre_envio=0 ';
        $strq='(';
        $strq.="(SELECT 1 as tipo,cotd.piezas as cantidad,cotd.idConsumible as idpro,cotd.modelo,cotd.idCotizacion,ace.prioridad
                FROM cotizaciones as cot 
                INNER JOIN cotizaciones_has_detallesConsumibles as cotd on cotd.idCotizacion=cot.id 
                INNER JOIN asignacion_ser_contrato_a_e as ace on (ace.cot_referencia=cotd.idCotizacion or ace.cot_referencia2=cotd.idCotizacion or ace.cot_referencia3=cotd.idCotizacion)
                WHERE cot.priorizar_cot=1 $w_status and cot.denegada=0 $where_reg)";      
    $strq.=" UNION ";
    $strq.="(SELECT 1 as tipo,cotd.piezas as cantidad,cotd.idConsumible as idpro,cotd.modelo,cotd.idCotizacion,ape.prioridad 
    FROM cotizaciones as cot 
    INNER JOIN cotizaciones_has_detallesConsumibles as cotd on cotd.idCotizacion=cot.id 
    INNER JOIN asignacion_ser_poliza_a_e as ape on (ape.cot_referencia=cotd.idCotizacion or ape.cot_referencia2=cotd.idCotizacion or ape.cot_referencia3=cotd.idCotizacion)
    WHERE cot.priorizar_cot=1 $w_status and cot.denegada=0 $where_reg)";
    $strq.=" UNION ";
    $strq.="(SELECT 1 as tipo,cotd.piezas as cantidad,cotd.idConsumible as idpro,cotd.modelo,cotd.idCotizacion,acld.prioridad
    FROM cotizaciones as cot 
    INNER JOIN cotizaciones_has_detallesConsumibles as cotd on cotd.idCotizacion=cot.id 
    INNER JOIN asignacion_ser_cliente_a_d as acld on (acld.cot_referencia=cotd.idCotizacion or acld.cot_referencia2=cotd.idCotizacion or acld.cot_referencia3=cotd.idCotizacion)
    WHERE cot.priorizar_cot=1 $w_status and cot.denegada=0 $where_reg)";
    $strq.=" UNION ";
    $strq.="(SELECT 1 as tipo,cotd.piezas as cantidad,cotd.idConsumible as idpro,cotd.modelo,cotd.idCotizacion,acl.prioridadr AS prioridad
    FROM cotizaciones as cot 
    INNER JOIN cotizaciones_has_detallesConsumibles as cotd on cotd.idCotizacion=cot.id 
    INNER JOIN asignacion_ser_venta_a as acl on acl.cot_referencia=cotd.idCotizacion
    WHERE cot.priorizar_cot=1 $w_status and cot.denegada=0 $where_reg)";
    
    $strq.=" UNION ";

    $strq.="(SELECT 1 as tipo,cotd.cantidad,cotd.id_consumibles as idpro,con.modelo,cotd.id_cotizacion as idCotizacion,ace.prioridad
    FROM cotizaciones as cot 
    INNER JOIN cotizaciones_has_detallesEquipos_consumibles as cotd on cotd.id_cotizacion=cot.id 
    inner join consumibles as con on con.id=cotd.id_consumibles 
    INNER JOIN asignacion_ser_contrato_a_e as ace on (ace.cot_referencia=cotd.id_cotizacion or ace.cot_referencia2=cotd.id_cotizacion or ace.cot_referencia3=cotd.id_cotizacion)
    WHERE cot.priorizar_cot=1 $w_status and cot.denegada=0 $where_reg)";

    $strq.=" UNION ";
    $strq.="(SELECT 1 as tipo,cotd.cantidad,cotd.id_consumibles as idpro,con.modelo,cotd.id_cotizacion as idCotizacion,ape.prioridad
    FROM cotizaciones as cot 
    INNER JOIN cotizaciones_has_detallesEquipos_consumibles as cotd on cotd.id_cotizacion=cot.id 
    inner join consumibles as con on con.id=cotd.id_consumibles 
    INNER JOIN asignacion_ser_poliza_a_e as ape on (ape.cot_referencia=cotd.id_cotizacion or ape.cot_referencia2=cotd.id_cotizacion or ape.cot_referencia3=cotd.id_cotizacion)
    WHERE cot.priorizar_cot=1 $w_status and cot.denegada=0 $where_reg)";

    $strq.=" UNION ";
    $strq.="(SELECT 1 as tipo,cotd.cantidad,cotd.id_consumibles as idpro,con.modelo,cotd.id_cotizacion as idCotizacion,acld.prioridad
    FROM cotizaciones as cot 
    INNER JOIN cotizaciones_has_detallesEquipos_consumibles as cotd on cotd.id_cotizacion=cot.id 
    inner join consumibles as con on con.id=cotd.id_consumibles 
    INNER JOIN asignacion_ser_cliente_a_d as acld on acld.cot_referencia=cotd.id_cotizacion
    WHERE cot.priorizar_cot=1 $w_status and cot.denegada=0 $where_reg)";

    $strq.=" UNION";
    $strq.=" (SELECT 1 as tipo,cotd.cantidad,cotd.id_consumibles as idpro,con.modelo,cotd.id_cotizacion as idCotizacion,acl.prioridadr AS prioridad
    FROM cotizaciones as cot 
    INNER JOIN cotizaciones_has_detallesEquipos_consumibles as cotd on cotd.id_cotizacion=cot.id 
    inner join consumibles as con on con.id=cotd.id_consumibles 
    INNER JOIN asignacion_ser_venta_a as acl on (acl.cot_referencia=cotd.id_cotizacion or acl.cot_referencia2=cotd.id_cotizacion or acl.cot_referencia3=cotd.id_cotizacion) 
    WHERE cot.priorizar_cot=1 $w_status and cot.denegada=0 $where_reg)";

    $strq.=" UNION ";

    $strq.="(SELECT 2 as tipo,cotd.cantidad,cotd.id_accesorio as idpro,cata.nombre as modelo,cotd.id_cotizacion as idCotizacion,ace.prioridad
    FROM cotizaciones as cot
    INNER JOIN cotizaciones_has_detallesEquipos_accesorios as cotd on cotd.id_cotizacion=cot.id
    inner join catalogo_accesorios as cata on cata.id=cotd.id_accesorio
    INNER JOIN asignacion_ser_contrato_a_e as ace on (ace.cot_referencia=cotd.id_cotizacion or ace.cot_referencia2=cotd.id_cotizacion or ace.cot_referencia3=cotd.id_cotizacion)
    WHERE cot.priorizar_cot=1 $w_status and cot.denegada=0 $where_reg)";

    $strq.=" UNION ";
    $strq.="(SELECT 2 as tipo,cotd.cantidad,cotd.id_accesorio as idpro,cata.nombre as modelo,cotd.id_cotizacion as idCotizacion,ape.prioridad
    FROM cotizaciones as cot
    INNER JOIN cotizaciones_has_detallesEquipos_accesorios as cotd on cotd.id_cotizacion=cot.id
    inner join catalogo_accesorios as cata on cata.id=cotd.id_accesorio
    INNER JOIN asignacion_ser_poliza_a_e as ape on (ape.cot_referencia=cotd.id_cotizacion or ape.cot_referencia2=cotd.id_cotizacion or ape.cot_referencia3=cotd.id_cotizacion)
    WHERE cot.priorizar_cot=1 $w_status and cot.denegada=0 $where_reg)";

    $strq.=" UNION ";
    $strq.="(SELECT 2 as tipo,cotd.cantidad,cotd.id_accesorio as idpro,cata.nombre as modelo,cotd.id_cotizacion as idCotizacion,acld.prioridad
    FROM cotizaciones as cot
    INNER JOIN cotizaciones_has_detallesEquipos_accesorios as cotd on cotd.id_cotizacion=cot.id
    inner join catalogo_accesorios as cata on cata.id=cotd.id_accesorio
    INNER JOIN asignacion_ser_cliente_a_d as acld on (acld.cot_referencia=cotd.id_cotizacion or acld.cot_referencia2=cotd.id_cotizacion or acld.cot_referencia3=cotd.id_cotizacion)
    WHERE cot.priorizar_cot=1 $w_status and cot.denegada=0 $where_reg)";

    $strq.=" UNION ";
    $strq.="(SELECT 2 as tipo,cotd.cantidad,cotd.id_accesorio as idpro,cata.nombre as modelo,cotd.id_cotizacion as idCotizacion,acl.prioridadr AS prioridad
    FROM cotizaciones as cot
    INNER JOIN cotizaciones_has_detallesEquipos_accesorios as cotd on cotd.id_cotizacion=cot.id
    inner join catalogo_accesorios as cata on cata.id=cotd.id_accesorio
    INNER JOIN asignacion_ser_venta_a as acl on acl.cot_referencia=cotd.id_cotizacion
    WHERE cot.priorizar_cot=1 $w_status and cot.denegada=0 $where_reg)";

    $strq.=" UNION ";

    $strq.=" (SELECT 3 as tipo,cotd.cantidad,cotd.idEquipo as idpro,cotd.modelo,cotd.idCotizacion,ace.prioridad
    FROM cotizaciones as cot 
    INNER JOIN cotizaciones_has_detallesEquipos as cotd on cotd.idCotizacion=cot.id 
    INNER JOIN asignacion_ser_contrato_a_e as ace on (ace.cot_referencia=cotd.idCotizacion or ace.cot_referencia2=cotd.idCotizacion or ace.cot_referencia3=cotd.idCotizacion)
    WHERE cot.priorizar_cot=1 $w_status and cot.denegada=0 $where_reg)";

    $strq.=" UNION ";
    $strq.="(SELECT 3 as tipo,cotd.cantidad,cotd.idEquipo as idpro,cotd.modelo,cotd.idCotizacion,ape.prioridad
    FROM cotizaciones as cot 
    INNER JOIN cotizaciones_has_detallesEquipos as cotd on cotd.idCotizacion=cot.id 
    INNER JOIN asignacion_ser_poliza_a_e as ape on (ape.cot_referencia=cotd.idCotizacion or ape.cot_referencia2=cotd.idCotizacion or ape.cot_referencia3=cotd.idCotizacion)
    WHERE cot.priorizar_cot=1 $w_status and cot.denegada=0 $where_reg)";

    $strq.=" UNION ";
    $strq.="(SELECT 3 as tipo,cotd.cantidad,cotd.idEquipo as idpro,cotd.modelo,cotd.idCotizacion,acld.prioridad
    FROM cotizaciones as cot 
    INNER JOIN cotizaciones_has_detallesEquipos as cotd on cotd.idCotizacion=cot.id 
    INNER JOIN asignacion_ser_cliente_a_d as acld on (acld.cot_referencia=cotd.idCotizacion or acld.cot_referencia2=cotd.idCotizacion or acld.cot_referencia3=cotd.idCotizacion)
    WHERE cot.priorizar_cot=1 $w_status and cot.denegada=0 $where_reg)";
    
    $strq.=" UNION ";
    $strq.=" (SELECT 3 as tipo,cotd.cantidad,cotd.idEquipo as idpro,cotd.modelo,cotd.idCotizacion,acl.prioridadr AS prioridad
    FROM cotizaciones as cot 
    INNER JOIN cotizaciones_has_detallesEquipos as cotd on cotd.idCotizacion=cot.id 
    INNER JOIN asignacion_ser_venta_a as acl on (acl.cot_referencia=cotd.idCotizacion or acl.cot_referencia2=cotd.idCotizacion or acl.cot_referencia3=cotd.idCotizacion)
    WHERE cot.priorizar_cot=1 $w_status and cot.denegada=0 $where_reg)";
    $strq.=" UNION ";
    $strq.="(SELECT 4 as tipo,cotd.piezas as cantidad, cotd.idRefaccion as idpro,cotd.modelo, cotd.idCotizacion,ace.prioridad
FROM cotizaciones as cot 
INNER JOIN cotizaciones_has_detallesRefacciones as cotd on cotd.idCotizacion=cot.id 
INNER JOIN asignacion_ser_contrato_a_e as ace on (ace.cot_referencia=cotd.idCotizacion or ace.cot_referencia2=cotd.idCotizacion or ace.cot_referencia3=cotd.idCotizacion)
WHERE cot.priorizar_cot=1 $w_status and cot.denegada=0 $where_reg)";
    $strq.=" UNION ";
    $strq.="(SELECT 4 as tipo,cotd.piezas as cantidad, cotd.idRefaccion as idpro,cotd.modelo, cotd.idCotizacion,ape.prioridad
FROM cotizaciones as cot 
INNER JOIN cotizaciones_has_detallesRefacciones as cotd on cotd.idCotizacion=cot.id 
INNER JOIN asignacion_ser_poliza_a_e as ape on (ape.cot_referencia=cotd.idCotizacion or ape.cot_referencia2=cotd.idCotizacion or ape.cot_referencia3=cotd.idCotizacion)
WHERE cot.priorizar_cot=1 $w_status and cot.denegada=0 $where_reg)";
    $strq.=" UNION ";
    $strq.="(SELECT 4 as tipo,cotd.piezas as cantidad, cotd.idRefaccion as idpro,cotd.modelo, cotd.idCotizacion,acld.prioridad
FROM cotizaciones as cot 
INNER JOIN cotizaciones_has_detallesRefacciones as cotd on cotd.idCotizacion=cot.id 
INNER JOIN asignacion_ser_cliente_a_d as acld on (acld.cot_referencia=cotd.idCotizacion or acld.cot_referencia2=cotd.idCotizacion or acld.cot_referencia3=cotd.idCotizacion)
WHERE cot.priorizar_cot=1 $w_status and cot.denegada=0 $where_reg)";
    $strq.=" UNION ";
    $strq.="(SELECT 4 as tipo,cotd.piezas as cantidad, cotd.idRefaccion as idpro,cotd.modelo, cotd.idCotizacion,acl.prioridadr AS prioridad
FROM cotizaciones as cot 
INNER JOIN cotizaciones_has_detallesRefacciones as cotd on cotd.idCotizacion=cot.id 
INNER JOIN asignacion_ser_venta_a as acl on (acl.cot_referencia=cotd.idCotizacion or acl.cot_referencia2=cotd.idCotizacion or acl.cot_referencia3=cotd.idCotizacion)
WHERE cot.priorizar_cot=1 $w_status and cot.denegada=0 $where_reg)";
    $strq.=" ";
    $strq.=" ";
    $strq.=" ";

    
        $strq.=') as datos';
        return $strq;
    }

}