
// Obtener la base_url 
var base_url = $('#base_url').val();

$(document).ready(function(){

    var nameImage =  base_url+'app-assets/images/alta.png';
    $('#foto').dropify({
        defaultFile: nameImage ,
    });

    CKEDITOR.replace('textoHeader');
    CKEDITOR.replace('textoHeader2');
    CKEDITOR.replace('leyendaPrecios');
    CKEDITOR.replace('titulo1Footer');
    CKEDITOR.replace('contenido1Footer');
    CKEDITOR.replace('titulo2Footer');
    CKEDITOR.replace('contenido2Footer');
    CKEDITOR.replace('tituloObservaciones');
    CKEDITOR.replace('contenidoObservaciones');
    CKEDITOR.replace('firma');
	// Asignamos el formulario de envío
	var configuracion_cotizacion_form = $('#configuracion_cotizacion_form');
	// Obtenemos la URL base para poder usarla
	// Validamos el formulario en base a las reglas mencionadas debajo
	configuracion_cotizacion_form.validate({
            rules: {
            },
            // Si están correctos los campos, procesamos el formulario mediante Ajax
            submitHandler: function (form) 
            {
                var validado = validaTextos();

                if(validado==0)
                {   
                    alert('Error, no todos los campos tienen contenido');
                }
                else
                {
                    $.ajax({
                        type:'POST',
                        url: base_url+"index.php/ConfiguracionDocumentos/actualizaDatosConfiguracionCotizacion",
                        data: {'data': validado},

                        success:function(data)
                        {
                            // Si existe el usuario, mostramos el mensaje de entrada y redirigimos
                            if(data==true)
                            {
                                cargaImagen();

                                swal({ title: "Éxito",
                                    text: "La configuración se ha registrado de manera correcta",
                                    type: 'success',
                                    showCancelButton: false,
                                    allowOutsideClick: false,
                                });
                            
                                /*
                                setTimeout(function(){ 
                                    window.location.href = base_url+"index.php/equipos"; 
                                }, 2000);
                                */             
                            }

                            // En caso contrario, se notifica
                            else
                            {
                                swal({ title: "Error",
                                    text: "La configuración no pudo guardarse. Contacte al administrador del sistema",
                                    type: 'error',
                                    showCancelButton: false,
                                    allowOutsideClick: false,
                                });
                            }
                        }
                    });
                }
            }
        });

        // Basic
    $('.dropify').dropify({
        messages: {
            default: 'Arrastra y suelta un archivo aquí o haz clic',
            replace: 'Arrastre y suelte un archivo o haga clic para reemplazar',
            remove:  'Eliminar',
            error:  'Lo sentimos, el archivo es demasiado grande.'
        }
    });
    

    // Used events
    var drEvent = $('.dropify-event').dropify();

    drEvent.on('dropify.beforeClear', function(event, element){
        return confirm("Do you really want to delete \"" + element.filename + "\" ?");
    });

    drEvent.on('dropify.afterClear', function(event, element){
        alert('File deleted');
    });

});

function cargaImagen()
{
    var inputFileImage = document.getElementById('logo1');
    var file = inputFileImage.files[0];

    var inputFileImage2 = document.getElementById('logo2');
    var file2 = inputFileImage2.files[0];

    var data = new FormData();

    if (inputFileImage.files[0] === undefined)
    {
        console.log('No está definida la primer imagen');
    }
    else
    {
        cargaimagen1();
        
    }   

    if (inputFileImage2.files[0] === undefined)
    {
        console.log('No está definida la segunda imagen');
    }
    else
    {
        cargaimagen2();
        
    }
    /*
    $.ajax({
        url:base_url+'index.php/ConfiguracionDocumentos/cargaArchivos',
        type:'POST',
        contentType:false,
        data:data,
        processData:false,
        cache:false,
        success: function(data) 
        {
            var array = $.parseJSON(data);
            console.log(array.ok);
            if (array.ok=true) 
            {
               
            }
            else
            {
               
            }
        },
        error: function(jqXHR, textStatus, errorThrown) 
        {
            var data = JSON.parse(jqXHR.responseText);
        }
    });
    */
}

function validaTextos()
{
    var retorno = 1;
    var data = [];
    /*
    if(CKEDITOR.instances.textoHeader.getData()==''){
        retorno = 0;
    }
    else if(CKEDITOR.instances.textoHeader2.getData()==''){
        retorno = 0;
    }
    else if(CKEDITOR.instances.leyendaPrecios.getData()==''){
        retorno = 0;
    }
    else if(CKEDITOR.instances.tituloObservaciones.getData()==''){
        retorno = 0;
    }
    else if(CKEDITOR.instances.contenidoObservaciones.getData()==''){
        retorno = 0;
    }
    else if(CKEDITOR.instances.titulo1Footer.getData()==''){
        retorno = 0;
    }
    else if(CKEDITOR.instances.contenido1Footer.getData()==''){
        retorno = 0;
    }
    else if(CKEDITOR.instances.titulo2Footer.getData()==''){
        retorno = 0;
    }
    else if(CKEDITOR.instances.contenido2Footer.getData()==''){
        retorno = 0;
    }
    else if(CKEDITOR.instances.firma.getData()==''){
        retorno = 0;
    }
    else
    {
        */
        var json = JSON.stringify({ 'textoHeader': CKEDITOR.instances.textoHeader.getData(), 'textoHeader2': CKEDITOR.instances.textoHeader2.getData(), 'leyendaPrecios': CKEDITOR.instances.leyendaPrecios.getData(), 'tituloObservaciones': CKEDITOR.instances.tituloObservaciones.getData(), 'contenidoObservaciones': CKEDITOR.instances.contenidoObservaciones.getData(), 'titulo1Footer': CKEDITOR.instances.titulo1Footer.getData(), 'contenido1Footer': CKEDITOR.instances.contenido1Footer.getData(), 'titulo2Footer': CKEDITOR.instances.titulo2Footer.getData(), 'contenido2Footer': CKEDITOR.instances.contenido2Footer.getData(), 'firma': CKEDITOR.instances.firma.getData(),'color':$('#colorencabezado').val(),'tipov':$('#tipov option:selected').val()} );
    //}

    if(retorno == 0){
        return retorno;    
    }
    else return json;
    
}

function view_tipov(){
    var tipov=$('#tipov option:selected').val();
    window.location.href = base_url+"index.php/ConfiguracionDocumentos?tipov="+tipov;
}
function cargaimagen1(){
    var inputFileImage = document.getElementById('logo1');
    var file = inputFileImage.files[0];
    var data = new FormData();
    data.append('foto',file);
    data.append('configuracion',$('#tipov option:selected').val());
    $.ajax({
        url:base_url+'index.php/ConfiguracionDocumentos/cargaArchivos1',
        type:'POST',
        contentType:false,
        data:data,
        processData:false,
        cache:false,
        success: function(data) {
            var array = $.parseJSON(data);
            console.log(array.ok);
            if (array.ok=true){
               
            }
            else{
               
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            var data = JSON.parse(jqXHR.responseText);
        }
    });
}
function cargaimagen2(){
    var inputFileImage = document.getElementById('logo2');
    var file = inputFileImage.files[0];
    var data = new FormData();
    data.append('foto',file);
    data.append('configuracion',$('#tipov option:selected').val());
    $.ajax({
        url:base_url+'index.php/ConfiguracionDocumentos/cargaArchivos2',
        type:'POST',
        contentType:false,
        data:data,
        processData:false,
        cache:false,
        success: function(data) {
            var array = $.parseJSON(data);
            console.log(array.ok);
            if (array.ok=true){
               
            }
            else{
               
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            var data = JSON.parse(jqXHR.responseText);
        }
    });
}