var base_url = $('#base_url').val();
var perfilid = $('#perfilid').val();
var idpersonal = $('#idpersonal').val();
//var perfilid=5;//5 12
var prospectoCliente = $('#prospectoCliente').val();
var idCliente = $('#idCliente').val();
var cli_aux=0;
$(document).ready(function(){ 
    $('#idclientes').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una opción',
        allowClear: true,
        ajax: {
            url: base_url+'Ventasi/searchclientes',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    var html='';
                    if(element.empresa==element.razon_social){
                        html=element.empresa;
                    }else{
                        html=element.empresa+' ('+element.razon_social+')';
                    }
                    itemscli.push({
                        id: element.id,
                        text: html

                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    });
    $('.chosen-select').chosen({width: "100%"});
	table = $('#tabla_cotizaciones').DataTable();
	load();
	
    // Acción al escoger un equipo, dentro del modal de autorizar cotización de tipo equipos
    /*
    $('#botonEscoger').on('click', function () { 
        $("#botonEscoger").prop("disabled", true);
        funtioncatgando();
        setTimeout(function(){ 
            $("#botonEscoger").prop("disabled", false);
        }, 4000);
        if ($('.bloqueo').length==0) {
            //autorizaCotizacion($('#idCotizacion').val(),equipo,numserie,accesorio,nomserieacc,consumible,toner_black,toner_c,toner_m,toner_y,cantidade,cantidadc,cantidada);
            autorizarcotizacionequipos();
        }else{
            toastr["error"]("No esta permitido", "Error");
        }
             
    });
    */
    // Acción al escoger un equipo, dentro del modal de autorizar cotización de tipo equipos
    $('#botonEscogerPoliza').on('click', function () { 
        $("#botonEscogerPoliza").prop("disabled", true);
        funtioncatgando();
        setTimeout(function(){ 
            $("#botonEscogerPoliza").prop("disabled", false);
        }, 4000);
        var polizalength = 0;
        $(".polizaselected:checked").each(function() {
            $(this).val();
            polizalength++; 
        });
        
        if (polizalength>0) {
            //autorizaCotizacion($('#idCotizacionPoliza').val(),poliza,'',0,'',0,0,0,0,0,0,0,0);
            autorizaCotizacionpoliza();
        }else{
            $.alert({
                boxWidth: '30%',
                useBootstrap: false,
                title: 'Atención!',
                content: 'Debe de seleccionar una póliza por lo menos'});
            $('body').loadingModal('destroy');
        }
        
    });
    /*
    
    $('#idSelecionDetalleEquipo').change(function(event) {
        var equipo=$('#idSelecionDetalleEquipo option:selected').val();
        var coti = $('#idCotizacion').val();
        $('.nomserieinput').attr('style','display: block');
        $.ajax({
            type:'POST',
            url: base_url+"index.php/Cotizaciones/obteneraccesorios",
            data: {
                idequipo:equipo,
                idcoti:coti
            },
            success: function (response) {
                var array = $.parseJSON(response);
                $('#idseleccionaccesorio').html(array.accesorios);
                $('#idSelecionconsumible').html(array.consumible);
                if(array.costo_toner_black>0){
                    
                    $('.toner_black_class').show();
                }else{
                    $('.toner_black_class').hide();
                }
                if(array.costo_toner_c>0){
                    $('.toner_c_class').show();
                }else{
                    $('.toner_c_class').hide();
                }
                if(array.costo_toner_m>0){
                    $('.toner_m_class').show();
                }else{
                    $('.toner_m_class').hide();
                }
                if(array.costo_toner_y>0){
                    $('.toner_y_class').show();
                }else{
                    $('.toner_y_class').hide();
                }
                $('.chosen-select').trigger("chosen:updated");
            },
            error: function(response){
                $.alert({
                    boxWidth: '30%',
                    useBootstrap: false,
                    title: 'Atención!',
                    type: 'red',
                    theme: 'bootstrap',
                    content: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema'});  
            }
        });
    });
    $('#idseleccionaccesorio').change(function(event) {
        $('.nomserieaccinput').attr('style','display: block');
    });*/
    $('#btndenegar').click(function(event) {
        var motivo=$('#motivodenegacion').val();
        if (motivo!='') {
            $.ajax({
                type:'POST',
                url: base_url+"index.php/Cotizaciones/cancelarcotizacion",
                data: {
                    id:$('#denegacionId').val(),
                    motivos:motivo,
                },
                success: function (response){
                    $('#modaldenegar').modal('close');
                    load();    
                },
                error: function(response){
                    notificacion(1);
                }
            });
        }else{
            $.alert({boxWidth: '30%',useBootstrap: false,
                title: 'Atención!',
                type: 'red',theme: 'bootstrap',
                content: 'Tiene que ingresar un motivo de la eliminación'}); 
        }
    });
    $('#aceptarcserviciorf').change(function(event) {
        if($('#aceptarcserviciorf').is(':checked')){
            $('.modalAutorizacionrefacciones_ventaservicio').show('show');
            obtenerservicios3();
            direccionesclientes();
        }else{
            $('.modalAutorizacionrefacciones_ventaservicio').hide('show');
        }
    });
    $('#aceptarcservicio').change(function(event) {
        if($('#aceptarcservicio').is(':checked')){
            $('.modalAutorizacion_ventaservicio').show('show');
            obtenerservicios3();
            direccionesclientes();
        }else{
            $('.modalAutorizacion_ventaservicio').hide('show');
        }
    });
    $('#aceptarcserviciorcc').change(function(event) {
        if($('#aceptarcserviciorcc').is(':checked')){
            $('.modalAutorizacionconsumible_ventaservicio').show('show');
            obtenerservicios3();
            direccionesclientes();
        }else{
            $('.modalAutorizacionconsumible_ventaservicio').hide('show');
        }
    });
    obtenerventaspendientes();
}); 
// Dependiendo del tipo de Cotización que se elija para editar, indicaremos a ruta que debe seguir el link
function editar(idCotizacion,tipo,view){
    var tipoDocumento = '';
    if(tipo==1){
        tipoDocumento = 'documentoHorizontalConBordes';
    }
    else if(tipo==2){
        tipoDocumento = 'documentoConsumiblesBordes';
    }
    else if(tipo==3){
        tipoDocumento = 'documentoRefaccionesBordes';
    }
    else if(tipo==4){
        tipoDocumento = 'documentoPolizas';
    }   
    viewtag='?inf=0';
    if(view==1){
        viewtag+='&view=0';
    }else{
        viewtag+='';
    }
    if(perfilid==5 || perfilid==12){
        viewtag+='&vte=1';
    }
    
    // Redigirimos
    setTimeout(function(){ 
        //window.open(base_url+"index.php/Cotizaciones/"+tipoDocumento+"/"+idCotizacion); 
        window.open(base_url+"index.php/Cotizaciones/documento/"+idCotizacion+viewtag); 
    }, 100);
}
function autorizar(idCotizacion,tipo){  
 
    if(prospectoCliente=='Prospecto'){
         $.confirm({
            boxWidth: '30%',
            useBootstrap: false,
            icon: 'fa fa-warning',
            title: 'Atención!',
            content: 'Al aceptar esta cotización, el Prospecto automáticamente se convertirá en Cliente por lo cual deberá capturar sus datos fiscales en el apartado correspondiente. ¿Desea Continuar?',
            type: 'red',
            typeAnimated: true,
            buttons:{
                confirmar: function (){
                    // Si es tipo 1 (Equipos), mostraremos el modal para elegir el equipo definitivo
                    if(tipo==1){
                        $('.modal').modal({
                            dismissible: true
                        });
                        // Cargamos los datos de los equipos que vengan en esa cotización
                        cargaDatosCotizacion(idCotizacion);

                        // Mostrar el modal
                        $('#modalAutorizacion').modal('open');
                        $('#idCotizacion').val(idCotizacion);

                    }
                    else if(tipo==2){
                        $('.modal').modal({
                            dismissible: true
                        });
                        // Mostrar el modal
                        $('#modalAutorizacionconsumible').modal('open');
                        $('#idCotizacionConsumible').val(idCotizacion);
                    }else if(tipo==3){
                        $('.modal').modal({
                            dismissible: true
                        });
                        // Mostrar el modal
                        $('#modalAutorizacionconsumible').modal('open');
                        $('#idCotizacionConsumible').val(idCotizacion);
                    }

                    // Si es tipo 4 (Póliza), mostraremos el modal para elegir la póliza elegida
                    else if(tipo==4){
                        
                        $('.modal').modal({
                            dismissible: true
                        });
                        // Cargamos los datos de los equipos que vengan en esa cotización
                        cargaDatosCotizacionPoliza(idCotizacion);

                        // Mostrar el modal
                        $('#modalAutorizacionPoliza').modal('open');
                        $('#idCotizacionPoliza').val(idCotizacion);
                            
                    }
                    
                },
                cancelar: function (){   
                }
            }
        }); 
    }else{
        // Si es tipo 1 (Equipos), mostraremos el modal para elegir el equipo definitivo
        if(tipo==1){
            
            $('.modal').modal({
                dismissible: true
            });
            // Cargamos los datos de los equipos que vengan en esa cotización
            cargaDatosCotizacion(idCotizacion);

            // Mostrar el modal
            $('#modalAutorizacion').modal('open');
            $('#idCotizacion').val(idCotizacion);
            agregarserviciomodal('modalAutorizacion_ventaservicio');
            agregarserviciomodalfe('modalAutorizacion_ventaservicio_fe');
                
        }
        else if(tipo==2){
            //consumibles
            $('.modal').modal({
                dismissible: true
            });
            // Cargamos los datos de los equipos que vengan en esa cotización
            // Mostrar el modal
            $('#modalAutorizacionconsumible').modal('open');
            $('#idCotizacionConsumible').val(idCotizacion);
            $.ajax({
                type:'POST',
                url: base_url+"index.php/Cotizaciones/allconsumiblescotizacion",
                data: {id:idCotizacion
                },
                success: function (response){
                    $('.tabla_cotizarmostrar').html(response); 
                    $('.collapsible').collapsible();   
                },
                error: function(response){
                    notificacion(1); 
                }
            });
            agregarserviciomodal('modalAutorizacionconsumible_ventaservicio');
            agregarserviciomodalfe('modalAutorizacionconsumible_ventaservicio_fe');
        }
        else if(tipo==3){
            //consumibles
            $('.modal').modal({
                dismissible: true
            });
            // Cargamos los datos de los equipos que vengan en esa cotización
            // Mostrar el modal
            $('#modalAutorizacionrefacciones').modal('open');
            $('#idCotizacionRefaccion').val(idCotizacion);
            $.ajax({
                type:'POST',
                url: base_url+"index.php/Cotizaciones/allrefaccionescotizacion",
                data: {id:idCotizacion
                },
                success: function (response){
                    $('.tabla_cotizarmostrarre').html(response);    
                },
                error: function(response){
                    notificacion(1); 
                }
            });
            agregarserviciomodal('modalAutorizacionrefacciones_ventaservicio');
            agregarserviciomodalfe('modalAutorizacionrefacciones_ventaservicio_fe');
                
        }
        // Si es tipo 4 (Póliza), mostraremos el modal para elegir la póliza elegida
        else if(tipo==4){
            
            $('.modal').modal({
                dismissible: true
            });
            // Cargamos los datos de los equipos que vengan en esa cotización
            cargaDatosCotizacionPoliza(idCotizacion);

            // Mostrar el modal
            $('#modalAutorizacionPoliza').modal('open');
            $('#idCotizacionPoliza').val(idCotizacion);
                
        }
        // Si no, será para Pólizas, Consumibles o Refacciones
        //else{
            // Llamamos a la función para autorizar la cotización, 
            // Se envía un 0 porque no lleva un ID de detalle de Equipo o Póliza
            //autorizaCotizacion(idCotizacion,0,'',0,'',0,0,0,0,0,0,0,0);
       // }
    }    
}
function autorizar2(idCotizacion,tipo,tipovrp,cli,solor,emp,block_auto){
    var blq_perm_ven_ser = block_auto;
    $.ajax({
        type:'POST',
        url: base_url+'index.php/Reportes/rventasnum/'+cli,
        success:function(data){
            console.log(data);
            var numventas=parseInt(data);
            //=======================
                if(blq_perm_ven_ser==1){
                    var html_contect='El cliente tiene '+numventas+' pendientes de pago!';
                }else{
                    var html_contect='El cliente tiene '+numventas+' pendientes de pago!<br> Para continuar se necesita permisos de administrador<br>'+
                                         '<input type="text" placeholder="Contraseña" id="contrasena" name="contrasena" class="name form-control"  onpaste="return false;" required/>';
                }
            //=======================
            if(numventas>0){
                $.confirm({
                    boxWidth: '30%',
                    useBootstrap: false,
                    icon: 'fa fa-warning',
                    title: 'Atención!',
                    content: html_contect,
                    type: 'red',
                    typeAnimated: true,
                    buttons:{
                        confirmar: function (){
                            if(blq_perm_ven_ser==1){
                                autorizar2_confirm(idCotizacion,tipo,tipovrp,cli,solor,emp);
                            }else{
                                //var pass=$('#contrasena').val();
                                var pass=$("input[name=contrasena]").val();
                                if (pass!='') {
                                     $.ajax({
                                        type:'POST',
                                        url: base_url+"index.php/Sistema/solicitarpermiso/"+cli,
                                        data: {
                                            pass:pass
                                        },
                                        success: function (response){
                                                var respuesta = parseInt(response);
                                                if (respuesta==1) {
                                                    autorizar2_confirm(idCotizacion,tipo,tipovrp,cli,solor,emp);
                                                }else{
                                                    notificacion(2);
                                                }
                                        },
                                        error: function(response){
                                            notificacion(1);
                                        }
                                    });
                                }else{
                                    notificacion(0);
                                }
                            }
                            
                        },
                        'Ver Pendientes': function (){
                            obtenerventaspendientesinfo2(cli);
                        },
                        cancelar: function (){
                            
                        }
                    }
                });
                setTimeout(function(){
                    new MaskedPassword(document.getElementById("contrasena"), '\u25CF');
                    
                },1000);
            }else{
                autorizar2_confirm(idCotizacion,tipo,tipovrp,cli,solor,emp);
            }
        }
    });
}
function autorizar2_confirm(idCotizacion,tipo,tipovrp,cli,solor,emp){
    cli_aux=cli;
    var bloqueo=$('#bloqueo').val();
    $('#form-cot').html('');
    if(tipovrp!=2){
        if(solor==1){
            var sol_html='';
                sol_html+='<div class="col s6">';
                    sol_html+='<label>Num. Orden de compra</label>';
                    sol_html+='<div class="b-input-group mb-3">';
                      sol_html+='<input type="text" class="form-control-bmz" id="orden_comp" name="orden_comp" required>';
                      sol_html+='<div class="b-input-group-append">';
                        sol_html+='<button class="b-btn b-btn-primary"  type="button" onclick="retirar_soleq"><i class="fas fa-lock"></i></button>';
                      sol_html+='</div>';
                    sol_html+='</div>';
                sol_html+='</div>';
            $('#form-cot').html(sol_html);
        }
    }
    if(bloqueo==1){
        bloqueov();
    }else{
        $('#idCotizacion').val(idCotizacion);
        agregarserviciomodalfe('modalAutorizacionrefacciones_ventaservicio_fe');
        agregarserviciomodal('modalAutorizacionrefacciones_ventaservicio');
        $('.modal').modal({
                    dismissible: true
                });
                // Cargamos los datos de los equipos que vengan en esa cotización
                // Mostrar el modal
                $('#modalAutorizacionGeneral').modal('open');
                $('#idcotizaciongeneral').val(idCotizacion);
                $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Cotizaciones/allcotizaciong",
                    data: {id:idCotizacion
                    },
                    success: function (response){
                        console.log(response);
                        $('.tabla_cotizarmostrarre').html(response);  
                        $('.collapsible').collapsible();  
                        if(tipovrp==1){
                            $('.soloventas').show();
                        }else{
                            $('.soloventas').hide();
                        }
                        setTimeout(function(){ 
                            if(emp==1){
                                $('.tipo_v_selected_2').prop('disabled',true);
                            }
                            if(emp==2){
                                $('.tipo_v_selected_1').prop('disabled',true);
                            }
                        }, 1000);
                    },
                    error: function(response){
                        notificacion(1);  
                    }
                });
    }
}
// Cargar por AJAX los datos de una cotización seleccionada de tipo Equipos
/*
function cargaDatosCotizacion(idCotizacion){
    $.ajax({
        url: base_url+"index.php/Cotizaciones/getDetalleEquiposPorIdCotizacion/"+idCotizacion,
        success: function (response) 
        {
            $('#idSelecionDetalleEquipo').html('');
            $('#idSelecionDetalleEquipo').html(response);
            $('#idSelecionDetalleEquipo').trigger("chosen:updated");
        },
        error: function(response)
        {
            $.alert({
                boxWidth: '30%',
                useBootstrap: false,
                title: 'Atención!',
                content: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema'});  
        }
    });
} */
function cargaDatosCotizacion(idCotizacion){
    $.ajax({
        url: base_url+"index.php/Cotizaciones/getdatosdecotizacionequipos/"+idCotizacion,
        success: function (response){
            var array = $.parseJSON(response);
            $('.divcotizaciondataequipos').html(array.equipos);
            $('.divcotizaciondataconsumibles').html(array.consumibles);
            $('.divcotizaciondataaccessorios').html(array.accesorios);

            $('.input_cantidad_touspin').TouchSpin({
                min: 1,
                max:99999999999999999999999999
            });
            $('.chosen-select').chosen({width: "100%"});
            setTimeout(function(){ 
                $('.consumibles_check_coti').change();
                $('.accesorio_check_coti').change();
            }, 1000); 
        },
        error: function(response){
            notificacion(1); 
        }
    });
}  
// Cargar por AJAX los datos de una cotización seleccionada de tipo Póliza
function cargaDatosCotizacionPoliza(idCotizacion){
    $.ajax({
        url: base_url+"index.php/Cotizaciones/getDetallePolizaPorIdCotizacion/"+idCotizacion,
        success: function (response){
            $('.tableselectedpoliza').html('');
            $('.tableselectedpoliza').html(response);
            //$('.detallespoliza_e').change();
        },
        error: function(response){
            notificacion(1); 
        }
    });
} 
// Autorizamos la cotización en el controlador. Se actualiza el estatus de la cotización y se crea el nuevo registro según lo que sea
/*
function autorizaCotizacion(idCotizacion, idDetalle,numserie,accesorio,numserieacc,consumible,toner_black,toner_c,toner_m,toner_y,cantidade,cantidadc,cantidada){
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Cotizaciones/autorizarCotizacion",
        data: {
            idCotizacion:idCotizacion,
            idDetalle:idDetalle,
            serie:numserie,
            acce:accesorio,
            serieacc:numserieacc,
            consumible:consumible,
            toner_black:toner_black,
            toner_c:toner_c,
            toner_m:toner_m,
            toner_y:toner_y,
            cantidade:cantidade,
            cantidadc:cantidadc,
            cantidada:cantidada
        },
        success: function (response){
            var respuesta = $.parseJSON(response);
            
            $.alert({
                boxWidth: '30%',
                useBootstrap: false,
                title: 'Éxito!',
                type: 'green',
                theme: 'bootstrap',
                content: 'Se ha autorizado la cotización '+idCotizacion+' y se ha asignado el número: '+respuesta.idVenta+' que corresponde a '+respuesta.tipo+'. Podrá visualizar esta información en el listado de '+respuesta.rentaVentaPoliza
            }); 
            
            // Recargamos la página
            
            setTimeout(function(){ 
                location.reload();
            }, 3500);
        },
        error: function(response){
            notificacion(1); 
        }
    });
}*/
function autorizarcotizacionequipos(){
    var servicio = $('#aceptarcservicio').is(':checked')==true?1:0;
    //================================
        var equiposselect=0;
        var DATAe  = [];
        var TABLAe   = $("#divcotizaciondataequipos tbody > tr");
            TABLAe.each(function(){
                if ($(this).find("input[class*='equipo_check_coti']").is(':checked')) {         
                    item = {};
                    item ["equipo"] = $(this).find("input[class*='equipo_check_coti']").val();
                    item ["cantidad"]   = $(this).find("input[id*='cantidad_equipo']").val();
                    item ["bodega"]  = $(this).find("select[id*='serie_bodegae'] option:selected").val();
                    DATAe.push(item);
                    equiposselect++;
                }
        });
        arrayequipos   = JSON.stringify(DATAe);
    //================================
    //================================
        var DATAe  = [];
        var TABLAe   = $("#divcotizaciondataconsumibles tbody > tr");
            TABLAe.each(function(){
                if ($(this).find("input[class*='consumibles_check_coti']").is(':checked')) {         
                    item = {};
                    item ["consumible"] = $(this).find("input[class*='consumibles_check_coti']").val();
                    item ["id_consumibles"] = $(this).find("input[id*='id_consumibles']").val();
                    item ["cantidad"]   = $(this).find("input[id*='cantidad_consumibles']").val();
                    item ["bodega"]  = $(this).find("select[id*='serie_bodegae'] option:selected").val();
                    DATAe.push(item);
                }
        });
        arrayconsumibles   = JSON.stringify(DATAe);
    //================================
    //================================
        var DATAe  = [];
        var TABLAe   = $("#divcotizaciondataaccessorios tbody > tr");
            TABLAe.each(function(){
                if ($(this).find("input[class*='accesorio_check_coti']").is(':checked')) {         
                    item = {};
                    item ["accesorio"] = $(this).find("input[class*='accesorio_check_coti']").val();
                    item ["cantidad"]   = $(this).find("input[id*='cantidad_accesorio']").val();
                    item ["bodega"]  = $(this).find("select[id*='serie_bodegae'] option:selected").val();
                    DATAe.push(item);
                    equiposselect++;
                }
        });
        arrayaccessorios   = JSON.stringify(DATAe);
    //================================
    if (equiposselect>0) {
        var idCotizacion=$('#idCotizacion').val();
        $.ajax({
            type:'POST',
            url: base_url+"index.php/Cotizaciones/autorizarcotizacionequipos",
            data: {
                idCotizacion:$('#idCotizacion').val(),
                equipos:arrayequipos,
                consumibles:arrayconsumibles,
                accesorios:arrayaccessorios,
                servicio:servicio,
                poliza: $('#tpoliza3 option:selected').val(),
                servicio2: $('#tservicio3 option:selected').val(),
                direccion: $('#tdireccion3 option:selected').val(),
                tipo: $('#ttipo33 option:selected').val(),
                motivo: $('#tserviciomotivo3').val(),
                fechae:$('#ventaeservicio_fe').val()
            },
            success: function (response){
                var respuesta = $.parseJSON(response);
                
                $.alert({
                    boxWidth: '30%',
                    useBootstrap: false,
                    title: 'Éxito!',
                    type: 'green',
                    theme: 'bootstrap',
                    content: 'Se ha autorizado la cotización '+idCotizacion+' y se ha asignado el número: '+respuesta.idVenta+' que corresponde a '+respuesta.tipo+'. Podrá visualizar esta información en el listado de '+respuesta.rentaVentaPoliza
                }); 
                
                // Recargamos la página
                
                setTimeout(function(){ 
                    location.reload();
                }, 3500);
            },
            error: function(response){
                notificacion(1); 
            }
        });
    }else{
        $.alert({boxWidth: '30%',useBootstrap: false,
                    title: 'Atención!',
                    type: 'red',theme: 'bootstrap',
                    content: 'Debe de seleccionar un equipo mínimo'});  
        $('body').loadingModal('destroy');
    }
    
}
// Carga los datos de la tabla del listado de cotizaciones
function load() {
    var fechades = $('#fechades').val();
    var idCli = $('#idCliente').val();
    var idClis = $('#idclientes').val();
    var finicial = $('#finicial').val();
    var ffinal = $('#ffinal').val();
    var tipog = $('#tipog option:selected').val();
    var tipocot = $('#tipocot option:selected').val();
    if(idCli>0){
        var cli=idCli;
    }else{
        var cli=idClis;
    }
    var tipocd=$('#tipocotizacion option:selected').val();
    // Destruye la tabla y la crea de nuevo con los datos cargados 
    table.destroy();
    table = $('#tabla_cotizaciones').DataTable({
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        search: {
                return: true
            }, 
        "ajax": {
            "url": base_url+"index.php/Cotizaciones/getlistcotizaciones",
            type: "post",
            "data": {
                'idCliente':cli,
                'denegada':tipocd,
                'fi':finicial,
                'ff':ffinal,
                'tipog':tipog,
                'tipocot':tipocot
            },
        },
        "columns": [
            {"data": "id"},
            {"data": "empresa"},
            {"data": "tipo",
                 render:function(data,type,row){
                    if(data==1){
                        var html='Equipos';    
                    }else if(data==2){
                        var html='Consumibles';    
                    }else if(data==3){
                        var html='Refacciones';    
                    }else if(data==4){
                        var html='Pólizas';    
                    }else if(data==5){
                        var html='Accesorios';    
                    }else{
                        var html='';
                    } 
                    if(row.tipog!=null){
                        if(row.tipog!='0'){
                            var html='';
                            html+=tipocotizacion(row.tipog);
                        }
                    }
                    
                    return html;
                }
            },
            {"data": "rentaVentaPoliza",
                 render:function(data,type,row){
                    if(data==1){
                        var html='Venta';    
                    }else if(data==2){
                        var html='Renta';    
                    }else if(data==3){
                        var html='Póliza';    
                    }else{
                        var html='N/A';
                    }
                    
                    return html;
                }
            }, 
            {"data": "prioridad",
                 render:function(data,type,row){
                    return 'Prioridad '+row.prioridad;
                }
            }, 
            {"data": 'estatus',
                 render:function(data,type,row){
                    var html='';
                    if (row.denegada==0) {
                        if(row.estatus==1){
                            html+='Pendiente';    
                        }else if(data==2){
                            html+='Autorizada';    
                        } 
                    }else{
                        html+='Eliminada';
                    }
                    
                    return html;
                }
            }, 
            {"data": 'reg'},
            {"data": 'info2'},
            {"data": 'id',
                render:function(data,type,row){
                    var html='';
                        html+=row.seriep+' ';
                        html+=row.serier;
                    
                    return html;
                }
            },
            {"data": null,
                 render:function(data,type,row){
                    var html='';
                    var idvpr=row.resultado;
                        html=idvpr;
                    if (row.rentaVentaPoliza==1) {
                        var onc='detallev('+idvpr+',0)';
                    }
                    if (row.rentaVentaPoliza==2) {
                        var onc='detallev('+idvpr+',0)';
                    }
                    if (row.rentaVentaPoliza==3) {
                        var onc='detallep('+idvpr+')';
                    }
                    if(idvpr>0){
                        html='<a class="btn-floating green tooltipped" data-position="top" data-delay="50" data-tooltip="Prefactura" onclick="'+onc+'"><i class="material-icons">assignment</i></a>';
                    }
                    
                    return html;
                }
            }, 
            {"data": "id",
                 render:function(data,type,row) {
                    var blq_perm_ven_ser = row.bloqueo_perm_ven;
                    var btn_disabled='';
                    if(fechades>row.reg){
                        btn_disabled='disabled';
                    }else{

                    }
                    $('.tooltipped').tooltip();

                    var btn_disabled_p='';
                    row.visitas
                    /*
                    var tipopoliza=tipocotizacion_p(row.tipog);
                    if(tipopoliza==1){
                        var btn_disabled_p='disabled xp'+tipopoliza;
                    }else{
                        var btn_disabled_p='x'+tipopoliza;
                    }
                    */
                    //==========================
                        //var visitas ='12,1,12,1,1,12';
                        var visitas =row.visitas;
                        var numequipos = row.numequipos;
                        visitas = visitas.split(',');
                        //console.log(visitas);
                        if(numequipos>1){
                            visitas.forEach(function(item) {
                                //console.log(item);
                                if(item>1){
                                    //console.log('si es mayor');
                                    btn_disabled_p='disabled xp';
                                }
                            });
                        }
                    //==========================
                    var html=''; 
                    if(row.estatus==1){
                        if (row.denegada==0) {
                            if(btn_disabled=='disabled'){
                                html+='<button class="b-btn b-btn-primary tooltipped " data-position="top" data-delay="50" data-tooltip="Vizualizar" onclick="editar('+row.id+','+row.tipo+',1)"><i class="fas fa-eye"></i></button> ';
                            }
                            html+='<button class="b-btn b-btn-warning tooltipped ocultar_tec" data-position="top" data-delay="50" data-tooltip="Autorizar" onclick="autorizar2('+row.id+','+row.tipo+','+row.rentaVentaPoliza+','+row.idCliente+','+row.sol_orden_com+','+row.tipov+','+blq_perm_ven_ser+')" '+btn_disabled+' '+btn_disabled_p+'><i class="fas fa-check"></i></button> ';
                            html+='<button class="b-btn b-btn-success tooltipped" data-position="top" data-delay="50" data-tooltip="Editar" onclick="editar('+row.id+','+row.tipo+',0)" '+btn_disabled+'><i class="fas fa-pencil-alt"></i></button> ';
                            html+='<button class="b-btn b-btn-success tooltipped ocultar_tec editarcontacto_'+row.id+'" \
                                    data-position="top" data-delay="50" data-tooltip="Editar contacto" \
                                    data-atencion="'+row.atencion+'" data-correo="'+row.correo+'" data-telefono="'+row.telefono+'" \
                                    onclick="editarcontacto('+row.id+','+row.idCliente+')" '+btn_disabled+'>\
                                    <i class="fas fa-user"></i>\
                                  </button> ';
                            
                            html+='<button class="b-btn b-btn-danger tooltipped ocultar_tec" data-position="top" data-delay="50" data-tooltip="Eliminar" onclick="denegarc('+row.id+')"><i class="fas fa-times"></i></button> ';
                            
                          }else{
                            html+='<button class="btn-floating blue tooltipped" data-position="top" data-delay="50" data-tooltip="Visualizar" onclick="editar('+row.id+','+row.tipo+',1)">\
                                    <i class="material-icons">remove_red_eye</i>\
                                  </button>\
                                  <button class="cotizacion_c_'+row.id+' btn-floating waves-effect waves-light red accent-2 tooltipped ocultar_tec"\
                                    data-position="top" data-delay="50" data-tooltip="Motivo"\
                                    data-motivo="'+row.motivodenegada+'" onclick="motivod('+row.id+')">\
                                    <i class="material-icons">assignment</i>\
                                  </button>';
                          }
                          html+='<button class="b-btn b-btn-primary tooltipped ocultar_tec" data-position="top" data-delay="50" data-tooltip="Clonar" onclick="clonar('+row.id+')"><i class="fas fa-copy fa-fw"></i></button> ';
                        
                    }else{
                        html+='<button class="b-btn b-btn-primary tooltipped" data-position="top" data-delay="50" data-tooltip="Visualizar" onclick="editar('+row.id+','+row.tipo+',1)"><i class="fas fa-eye"></i></button> ';
                        html+='<button class="b-btn b-btn-primary tooltipped ocultar_tec" data-position="top" data-delay="50" data-tooltip="Clonar" onclick="clonar('+row.id+')"><i class="fas fa-copy fa-fw"></i></button> ';
                    }
                        html+='<button type="button" class="b-btn b-btn-primary email ocultar_tec" onclick="envio_s_m('+row.id+','+row.idCliente+')"><i class="fas fa-mail-bulk"></i></button>';
                        if(idpersonal==1 || idpersonal==17 || idpersonal==18 || idpersonal==64 || idpersonal==14){
                           html+='<a class="b-btn b-btn-primary tooltipped" data-position="top" data-delay="50" data-tooltip="Cambiar personal" onclick="cambiar_per('+row.id+')"><i class="fas fa-user-edit"></i></a> '; 
                        }
                        
                    return html;
                }
            }
        ],
        "order": [[ 0, "desc" ]],
    }).on('draw',function(){
        $("input[type=search]").addClass('input_search');
        $('.dataTables_filter').append('<i class="fa fa-search buttonsearch" onclick="searchtable_v()"></i>')
        if(perfilid==5 || perfilid==12){
            //ocultar columnas
            console.log('remueve cosas');
            table.columns([2, 3]).visible(false);
            $('.ocultar_tec').remove();
        }
    });
}
function searchtable_v(){
    var contenedor = document.getElementById('tabla_cotizaciones_filter');

    // Selecciona el input dentro del contenedor
    var input = contenedor.querySelector('input.form-control-bmz');

    // Obtén el valor del input
    var valorInput = input.value;
    console.log('valorInput '+valorInput);
    table.search(valorInput).draw();
}
/*
function autorizaCotizacionpoliza(){
    var cotizacion = $('#idCotizacionPoliza').val();
    var DATA = [];
    var TABLA = $("#tablepolizas tbody > tr");
    TABLA.each(function() {
        if($(this).find("input[name*='polizaselected']").is(':checked')){
            var poliza=$(this).find("input[name*='polizaselected']").val();
            var DATAse = [];
            var TABLAse = $(".table_series_e_"+poliza+" tbody > tr");
            TABLAse.each(function() {
                items = {};
                items["series"] = $(this).find("input[id*='polizaserie']").val();
                DATAse.push(items);
            });
            //aInfose = JSON.stringify(DATAse);

            item = {};
            item["cotizacion"] = cotizacion;
            item["poliza"] = poliza;
            item["cantidad"] = $(this).find("input[id*='cantidad']").val();
            item["serie"] = DATAse;
            DATA.push(item); 
        }
        
    });
    INFO = new FormData();
    aInfo = JSON.stringify(DATA);
    INFO.append('data', aInfo);

    $.ajax({
        type:'POST',
        url: base_url+"index.php/Cotizaciones/autorizarCotizacionpoliza",
        data: {
            cotizacion:cotizacion,
            polizas:aInfo
        },
        success: function (response){
            console.log(response);
            
            var respuesta = $.parseJSON(response);
            
            $.alert({
                boxWidth: '30%',
                useBootstrap: false,
                title: 'Éxito!',
                type: 'green',
                theme: 'bootstrap',
                content: 'Se ha autorizado la cotización '+cotizacion+' y se ha asignado el número: '+respuesta.idVenta+' que corresponde a Poliza. Podrá visualizar esta información en el listado de Pólizas'
            }); 
            
            // Recargamos la página
            
            setTimeout(function(){ 
                location.reload();
            }, 3500);
            
        },
        error: function(response){
            notificacion(1);
            $('body').loadingModal('destroy');
        }
    });
}
*/
// funcion del modal Consumibles de la cotizacion
/*
function checkconsumibles(){
    $(".checkconsumibles").prop("disabled", true);
    funtioncatgando();
    setTimeout(function(){ 
        $(".checkconsumibles").prop("disabled", false);
    }, 4000);
    // listo 
    var servicio = $('#aceptarcserviciorcc').is(':checked')==true?1:0;
    var etx = 0;
    var addtp = 0;
    $(".numchek").each(function(){
        if ($(this).is(':checked')) {
            addtp++;
        }  
    }); 
    var tabletr = $('.tableconsumible tbody > tr').length;       
    var suma = tabletr+addtp;
    if(suma==0){
        $.alert({
                boxWidth: '30%',
                useBootstrap: false,
                title: 'Atención!',
                content: 'Favor de selecionar por lo menos un consumible'});
        $('body').loadingModal('destroy');
        retorno = 0;
    }else{
        var nombre = [];
        $("#tableconsumiblep tbody > tr").each(function() {
            if($(this).find("input[name*='nombre']").is(':checked')){
                //var aux = $(this).attr("id").split('_');
                itemp={};
                itemp['id']=$(this).find("input[name*='nombre']").val();
                itemp['cantidad']=$(this).find("input[id*='cantidadnew']").val();                    
                nombre.push(itemp);
            }
        });
        INFOp  = new FormData();
        nombrep   = JSON.stringify(nombre);
        var DATAp  = [];
            var TABLA   = $(".tableconsumible tbody > tr");
                TABLA.each(function(){         
                item = {};
                item ["equipo"] = $(this).find("input[id*='equipotx']").val();
                item ["toner"]   = $(this).find("input[id*='tonertx']").val();
                item ["tonerp"]  = $(this).find("input[id*='tonerptx']").val();
                item ["piezas"]  = $(this).find("input[id*='piezastx']").val();
                DATAp.push(item);
            });
            INFO  = new FormData();
            arrayConsumible   = JSON.stringify(DATAp);
            INFO.append('data', arrayConsumible);

        $.ajax({
            type:'POST',
            url: base_url+"index.php/Cotizaciones/guardarconsumiblescheck",
            data: {datos:nombrep,
                   id:$('#idCotizacionConsumible').val(),
                   consumible:arrayConsumible,
                   servicio:servicio,
                   poliza: $('#tpoliza3 option:selected').val(),
                    servicio2: $('#tservicio3 option:selected').val(),
                    direccion: $('#tdireccion3 option:selected').val(),
                    tipo: $('#ttipo33 option:selected').val(),
                    motivo: $('#tserviciomotivo3').val(),
                    fechae:$('#ventaeservicio_fe').val()
            },
            success: function (response){
                $.alert({
                    boxWidth: '30%',
                    useBootstrap: false,
                    title: 'Éxito!',
                    type: 'green',
                    theme: 'bootstrap',
                    content: 'Aceptada Correctamente'
                }); 
                
                // Recargamos la página
                
                setTimeout(function(){ 
                    location.reload();
                }, 3500); 
            },
            error: function(response){
                notificacion(1);
                $('body').loadingModal('destroy'); 
            }
        });
    }        
}
*/
/*
function checkrefacciones(){
    $(".checkrefacciones").prop("disabled", true);
    funtioncatgando();
    setTimeout(function(){ 
        $(".checkrefacciones").prop("disabled", false);
    }, 4000);
    var servicio = $('#aceptarcserviciorf').is(':checked')==true?1:0;
    // listo 
    var etx = 0;
    var addtp = 0;
    $(".numchek").each(function(){
        if ($(this).is(':checked')) {
            addtp++;
        }  
    }); 
    var tabletr = $('.tablerefacciones tbody > tr').length;       
    var suma = tabletr+addtp;
    if(suma==0){
        $.alert({
                boxWidth: '30%',
                useBootstrap: false,
                title: 'Atención!',
                content: 'Favor de selecionar por lo menos un consumible'});
        $('body').loadingModal('destroy');
        retorno = 0;
    }else{
        var nombre = [];
        $("#tablerefaccionp tbody > tr").each(function() {
            if($(this).find("input[name*='nombre']").is(':checked')){
                //var aux = $(this).attr("id").split('_');
                itemp={};
                itemp['id']=$(this).find("input[name*='nombre']").val();
                itemp['cantidad']=$(this).find("input[id*='cantidadnew']").val(); 
                itemp['serie']=$(this).find("input[id*='serieselect']").val(); 
                itemp['bodega']=$(this).find("select[id*='serie_bodegarr']").val();                    
                nombre.push(itemp);
            }
        });
        INFOp  = new FormData();
        nombrep   = JSON.stringify(nombre);
        var DATAp  = [];
            var TABLA   = $(".tablerefacciones tbody > tr");
                TABLA.each(function(){         
                item = {};
                item ["equipo"] = $(this).find("input[id*='equipotx']").val();
                item ["refaccion"]   = $(this).find("input[id*='tonertx']").val();
                item ["refaccionp"]  = $(this).find("input[id*='tonerptx']").val();
                item ["piezas"]  = $(this).find("input[id*='piezastx']").val();
                item ["bodega"]  = $(this).find("input[id*='bodegatx']").val();
                DATAp.push(item);
            });
            INFO  = new FormData();
            arrayConsumible   = JSON.stringify(DATAp);
            INFO.append('data', arrayConsumible);

        $.ajax({
            type:'POST',
            url: base_url+"index.php/Cotizaciones/guardarrefaccionescheck",
            data: { datos:nombrep,
                    id:$('#idCotizacionRefaccion').val(),
                    consumible:arrayConsumible,
                    servicio:servicio,
                    poliza: $('#tpoliza3 option:selected').val(),
                    servicio2: $('#tservicio3 option:selected').val(),
                    direccion: $('#tdireccion3 option:selected').val(),
                    tipo: $('#ttipo33 option:selected').val(),
                    motivo: $('#tserviciomotivo3').val(),
                    fechae:$('#ventaeservicio_fe').val()
            },
            success: function (response){
                $.alert({
                    boxWidth: '30%',
                    useBootstrap: false,
                    title: 'Éxito!',
                    type: 'green',
                    theme: 'bootstrap',
                    content: 'Aceptada Correctamente'
                }); 
                
                // Recargamos la página
                
                setTimeout(function(){ 
                    location.reload();
                }, 3500); 
            },
            error: function(response){
                notificacion(1); 
            }
        });
    }        
}
*/
// funcion del modal consumibles agregar
function cambiar_per(id){
    var personal=$('#ejecutivoselect').html();
    $.confirm({
        boxWidth: '40%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Cambio de personal',
        content: '<div style="width: 99%;">¿Desea cambiar al personal?<br><label>Ejecutivo</label><select id="ejecutivoselect_new" class="browser-default form-control-bmz">'+personal+'</select></div>',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var eje=$('#ejecutivoselect_new option:selected').val();
                //var pass=$('#pass_cambio').val();
                $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Cotizaciones/cambiopersonal",
                    data: {
                        id:id,
                        eje:eje,
                    },
                    success: function (response){
                            toastr["success"]("Cambio Realizado");  
                            
                        
                    },
                    error: function(response){
                        toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema"); 
                         
                    }
                }); 
            },
            cancelar: function (){
                
            }
        }
    });
    
    
}
function selectequipo(){
    var id = $('#equipoListaConsumibles option:selected').val();
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Cotizaciones/mostrarselectorner",
        data: {id:id
        },
        success: function (data){
            $('.toner_mostrar').html('');
            $('.toner_mostrar').html(data);   
            $('.chosen-select').chosen({width: "100%"}); 
        },
        error: function(data){
            notificacion(1);  
        }
    });
}
function selectequipor(){
    var id = $('#equipoListarefacciones option:selected').val();
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Cotizaciones/mostrarselecrefaccionesr",
        data: {id:id
        },
        success: function (data){
            $('.refacciones_mostrar').html('');
            $('.refacciones_mostrar').html(data);   
            $('.chosen-select').chosen({width: "100%"}); 
        },
        error: function(data){
            notificacion(1);
        }
    });
}
// funcion del modal consumibles agregar
function selectprecio(){
    var id = $('#toner option:selected').val();
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Cotizaciones/mostrarselectprecios",
        data: {id:id
        },
        success: function (data){
            $('.tonerp_mostrar').html('');
            $('.tonerp_mostrar').html(data);   
            $('.chosen-select').chosen({width: "100%"}); 
        },
        error: function(data){
            notificacion(1);
        }
    });
}
function selectprecior(){
    var id = $('#srefacciones option:selected').val();
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Cotizaciones/mostrarselectpreciosr",
        data: {id:id
        },
        success: function (data){
            var array = $.parseJSON(data); 
            $('.refaccionesp_mostrar').html('');
            $('.refaccionesp_mostrar').html(array.select_option);   
            $('.chosen-select').chosen({width: "100%"}); 
        },
        error: function(data){
            notificacion(1);   
        }
    });
}
// Funcion de replica de modal de consumibles 
var sconsuadd=1;
function addconsutable(){
    var equipoval = $('#equipoListaConsumibles option:selected').val();
    var equipotext = $('#equipoListaConsumibles option:selected').text();
    var tornerval = $('#toner option:selected').val();
    var tornertext = $('#toner option:selected').text();
    var tornerpval = $('#tonerp option:selected').val();
    var tornerptext = $('#tonerp option:selected').text();
    var piezastext = $('#piezas').val();
    var tablerow='<tr class="consuelecteddd_row_'+sconsuadd+'">\
                    <td>\
                        <input type="hidden" id="equipotx" class="equipotx" value="'+equipoval+'">\
                        '+equipotext+'\
                    </td>\
                    <td>\
                        <input type="hidden" id="tonertx" class="tonertx" value="'+tornerval+'">\
                        '+tornertext+'\
                    </td>\
                    <td>\
                        <input type="hidden" id="tonerptx" class="tonerptx" value="'+tornerpval+'">\
                        '+tornerptext+'\
                    </td>\
                    <td>\
                        <input type="hidden" id="piezastx" class="piezastx" value="'+piezastext+'">\
                        '+piezastext+'\
                    </td>\
                    <td>\
                        <a onclick="deleteconsuadd('+sconsuadd+')"style="cursor: pointer;"><i class="material-icons" style="color: red;">delete_forever</i></a>\
                    </td>\
                  </tr>';
    $('.equipotableconsumicle .tableconsumible tbody').append(tablerow);
    sconsuadd++;
}
var srefaadd=1;
function addrefaccion(){
    var equipoval = $('#equipoListarefacciones option:selected').val();
    var equipotext = $('#equipoListarefacciones option:selected').text();
    var tornerval = $('#srefacciones option:selected').val();
    var tornertext = $('#srefacciones option:selected').text();
    var tornerpval = $('#refaccionesp option:selected').val();
    var tornerptext = $('#refaccionesp option:selected').text();
    var bodegaval = $('#serie_bodegarrs option:selected').val();
    var bodegatext = $('#serie_bodegarrs option:selected').text();
    var piezastext = $('#piezasr').val();
    var tablerow='<tr class="consuelecteddd_row_'+srefaadd+'">\
                    <td>\
                        <input type="hidden" id="equipotx" class="equipotx" value="'+equipoval+'">\
                        '+equipotext+'\
                    </td>\
                    <td>\
                        <input type="hidden" id="tonertx" class="tonertx" value="'+tornerval+'">\
                        '+tornertext+'\
                    </td>\
                    <td>\
                        <input type="hidden" id="tonerptx" class="tonerptx" value="'+tornerpval+'">\
                        '+tornerptext+'\
                    </td>\
                    <td>\
                        <input type="hidden" id="piezastx" class="piezastx" value="'+piezastext+'">\
                        '+piezastext+'\
                    </td>\
                    <td>\
                        <input type="hidden" id="bodegatx" class="piezastx" value="'+bodegaval+'">\
                        '+bodegatext+'\
                    </td>\
                    <td>\
                        <a onclick="deleteconsuadd('+srefaadd+')"style="cursor: pointer;"><i class="material-icons" style="color: red;">delete_forever</i></a>\
                    </td>\
                  </tr>';
    $('.equipotableconsumicle .tablerefacciones tbody').append(tablerow);
    srefaadd++;
}
function deleteconsuadd(id){
    $('.consuelecteddd_row_'+id).remove();
}
function appendseriespolizas(id){
    $('.table_series_e_'+id+' tbody').html('');
    var cantidad=$('.detallespoliza_e_'+id).val();
    console.log(cantidad);
    var addseries='';
    for (i = 0; i < cantidad; i++) { 
            addseries+='<tr>\
                            <td style="padding-top: 2px;padding-bottom: 2px;">\
                                <input type="text" id="polizaserie" class="form-control-b">\
                            <td>\
                        <tr>';
    }
    $('.table_series_e_'+id+' tbody').html(addseries);
}
function denegarc(idcotizacion){
    $('#motivodenegacion').val('');
    $('#modaldenegar').modal();
    $('#modaldenegar').modal('open');
    $('#denegacionId').val(idcotizacion);
}
function motivod(idcotizacion){
    $('#modalinfoeliminada').modal();
    $('#modalinfoeliminada').modal('open');
    var motivo=$('.cotizacion_c_'+idcotizacion).data('motivo');
    $('.infoeliminada').html(motivo);
}
function fuctionalert(titles,contents){
    $.alert({
        boxWidth: '30%',
        useBootstrap: false,
        title: titles,
        content: contents});
}
function fuctionalert2(titles,contents){
    $.alert({boxWidth: '30%',useBootstrap: false,
        title: titles,
        type: 'red',theme: 'bootstrap',
        content: contents});  
}
function alertastockequipo(id,stock){
    var cantidad = parseInt($('.equ_cant_'+id).val());
    setTimeout(function(){ 
        if ($('.equipo_check_'+id).is(':checked')) {
            if (stock==0) {
                toastr["error"]("No hay suficiente stock", "Alerta!");
                $('.equipo_check_'+id).addClass('bloqueo');
            }else if(stock<cantidad){
                toastr["error"]("No hay suficiente stock", "Alerta!");
                $('.equipo_check_'+id).addClass('bloqueo');
            }else{
                $('.equipo_check_'+id).removeClass('bloqueo');
            }
            
        }else{
            $('.equipo_check_'+id).removeClass('bloqueo');
        }
    }, 1000);  
}
function alertastocktoner(toner){
    //var toner = toner.value();
    var cantidad=$(".consu_cant_"+toner).val();
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Aconsumibles/stockconsumiblesid",
        data: {id:toner},
        success: function (data){
            $('.consu_stock_'+toner).html('('+data+')');
            setTimeout(function(){ 
                if ($('.consum_check_'+toner).is(':checked')) {
                    if (parseInt(data)==0) {
                        toastr["error"]("No hay suficiente stock", "Alerta!");
                        $('.consum_check_'+toner).addClass('bloqueo');
                    }else if(parseInt(data)<parseInt(cantidad)){
                        toastr["error"]("No hay suficiente stock", "Alerta!");
                        $('.consum_check_'+toner).addClass('bloqueo');
                    }else{
                        $('.consum_check_'+toner).removeClass('bloqueo');
                    }
                    
                }else{
                    $('.consum_check_'+toner).removeClass('bloqueo');
                }
            }, 1000);             
        },
        error: function(data){ 
            notificacion(1);
        }
    });
}
function alertastockacces(toner){
    //var toner = toner.value();
    var cantidad=$(".access_cant_"+toner).val();
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Aconsumibles/stockaccesorioid",
        data: {id:toner},
        success: function (data){
            $('.access_stock_'+toner).html('('+data+')');
            setTimeout(function(){ 
                if ($('.acce_check_'+toner).is(':checked')) {
                    if (parseInt(data)==0) {
                        toastr["error"]("No hay suficiente stock", "Alerta!");
                        $('.acce_check_'+toner).addClass('bloqueo');
                    }else if(parseInt(data)<parseInt(cantidad)){
                        toastr["error"]("No hay suficiente stock", "Alerta!");
                        $('.acce_check_'+toner).addClass('bloqueo');
                    }else{
                        $('.acce_check_'+toner).removeClass('bloqueo');
                    }
                    
                }else{
                    $('.acce_check_'+toner).removeClass('bloqueo');
                }
            }, 1000);             
        },
        error: function(data){ 
            notificacion(1);
        }
    });
}
toastr.options = {
  "closeButton": false,
  "debug": false,
  "newestOnTop": false,
  "progressBar": true,
  "positionClass": "toast-top-right",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "1000",
  "timeOut": "5000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}
function funtioncatgando(){
    $('body').loadingModal({text: 'Procesando...'});
    $('body').loadingModal('animation', 'circle');
        /*
     $('body').loadingModal('animation', 'rotatingPlane').loadingModal('backgroundColor', 'red'); 
                 $('body').loadingModal('animation', 'wave'); 
                 $('body').loadingModal('animation', 'wanderingCubes').loadingModal('backgroundColor', 'green'); 
                 $('body').loadingModal('animation', 'spinner'); 
                 $('body').loadingModal('animation', 'chasingDots').loadingModal('backgroundColor', 'blue'); 
                 $('body').loadingModal('animation', 'threeBounce'); 
                 $('body').loadingModal('animation', 'circle').loadingModal('backgroundColor', 'black'); 
                 $('body').loadingModal('animation', 'cubeGrid'); 
                 $('body').loadingModal('animation', 'fadingCircle').loadingModal('backgroundColor', 'gray'); 
                 $('body').loadingModal('animation', 'foldingCube'); return delay
                 $('body').loadingModal('color', 'black').loadingModal('text', 'Done :-)').loadingModal('backgroundColor', 'yellow');  
                 $('body').loadingModal('hide'); 
                 $('body').loadingModal('destroy');
    */
}
function agregarserviciomodal(classe){
    $('.limpiarservicio').html('');
    var selectpoliza=$('#selectedviewpoliza').html();
    var htmlclass='<div class="row ventaservicio">\
                        <div class="col s3 m3 l3">\
                            <label for="tpoliza3">Póliza</label>\
                            <select id="tpoliza3" class="browser-default chosen-select form-control-bmz" onchange="obtenerservicios3();">\
                                '+selectpoliza+'\
                            </select>\
                        </div>\
                        <div class="col s3 m3 l3">\
                          <label for="tservicio3">Equipo / Servicio</label>\
                            <select id="tservicio3" class="browser-default chosen-select form-control-bmz"></select>\
                        </div>\
                        <div class="col s4 m4 l4">\
                          <label for="tdireccion3">Dirección</label>\
                          <select id="tdireccion3" class="browser-default  form-control-bmz">\
                          </select>\
                        </div>\
                        <div class="col s2 m2 l2">\
                          <label class="ttipo2">Tipo Servicio</label>\
                          <select class="browser-default  form-control-bmz" name="ttipo3" id="ttipo33">\
                            <option value="1">Local</option>\
                            <option value="2">Semi</option>\
                            <option value="3">Foraneo</option>\
                            <option value="4">Especial</option>\
                          </select>\
                        </div>\
                    </div>\
                    <div class="row ventaservicio">\
                          <label for="tserviciomotivo3">Motivo del Servicio</label>\
                          <textarea id="tserviciomotivo3"></textarea>\
                  </div>';
    $('.'+classe).html(htmlclass);

}
function agregarserviciomodalfe(classe){
    $('.limpiarservicio_fe').html('');
    var html='<div class="col s3 m3 l3" style="display:none">\
                <label>Fecha Entrega</label>\
                <input type="date" id="ventaeservicio_fe" class="form-control-bmz">\
             </div>';
    $('.'+classe).html(html);
}
function obtenerservicios3(){
  var tpoliza = $('#tpoliza3').val();
  $.ajax({
        type:'POST',
        url: base_url+'index.php/Asignaciones/obtenerservicios',
        data: {
          tpoliza:tpoliza
          },
        success:function(data){
            $('#tservicio3').html(data);
            $('.chosen-select').trigger("chosen:updated");

        }
    });
}
function direccionesclientes(){
    var id = $('#idCliente').val();
  $.ajax({
        type:'POST',
        url: base_url+"Asignaciones/datoscliente",
        data: {
          id:id
        },
        success: function (data){
          console.log(data);
          var array = $.parseJSON(data);
          array.direccion.forEach(function(element) {
                $('#tdireccion3').append('<option>'+element.direccion+'</option>');
          });
        }
    }); 
}
function editarcontacto(id,cliente){
    $.ajax({
        type:'POST',
        url: base_url+"Cotizaciones/datoscontacto",
        data: {
          cli:cliente
        },
        success: function (data){
          $('#atencionselect').html(data);
        }
    }); 

    $('#modalEditarcontacto').modal();
    $('#modalEditarcontacto').modal('open');

    var atencion = $('.editarcontacto_'+id).data('atencion');
    var correo = $('.editarcontacto_'+id).data('correo');
    var telefono = $('.editarcontacto_'+id).data('telefono');
    $('#editarcontactocotizacion').val(id);
    $('#mec_atencion').val(atencion);
    $('#mec_correo').val(correo);
    $('#mec_telefono').val(telefono);
}
function obtenercontactosss(){
    var atencion = $('#atencionselect option:selected').data('atencionpara');
    var correo = $('#atencionselect option:selected').data('email');
    var telefono = $('#atencionselect option:selected').data('telefono');

    $('#mec_atencion').val(atencion);
    $('#mec_correo').val(correo);
    $('#mec_telefono').val(telefono);
}
function okbotoneditarcontacto(){
    $.ajax({
        type:'POST',
        url: base_url+"Cotizaciones/editarcontacto",
        data: {
            id:$('#editarcontactocotizacion').val(),
            atencion: $('#mec_atencion').val(),
            correo: $('#mec_correo').val(),
            telefono: $('#mec_telefono').val()
        },
        success: function (data){
            $('#modalEditarcontacto').modal('close');
            $.alert({boxWidth: '30%',useBootstrap: false,
                    title: 'Éxito!',
                    type: 'green',theme: 'bootstrap',
                    content: 'Se ha actualizado la cotización  '
                }); 
            load();
        }
    }); 
}
function autorizaciongeneral(){
    $( ".autorizaciongeneral" ).prop( "disabled", true );
    setTimeout(function(){ 
         $(".autorizaciongeneral" ).prop( "disabled", false );
    }, 10000);
    var verificarstock=1;
    var servicio = $('#aceptarcserviciorf').is(':checked')==true?1:0;
    var seleccionebodega=1;
    //================================
        var equiposselect=0;
        var DATAe  = [];
        var TABLAe   = $("#tableg_equipos tbody > tr");
            TABLAe.each(function(){
                if ($(this).find("input[class*='equipo_check_coti']").is(':checked')) {         
                    item = {};
                    item ["equipo"] = $(this).find("input[class*='equipo_check_coti']").val();
                    item ["cantidad"]   = $(this).find("input[id*='cantidad_equipo']").val();
                    item ["bodega"]  = $(this).find("select[id*='serie_bodegae'] option:selected").val();
                    if($(this).find("input[id*='cantidad_equipo']").val()>$(this).find("select[id*='serie_bodegae'] option:selected").data('stock')){
                        verificarstock=0;
                    }
                    if($(this).find("select[id*='serie_bodegae'] option:selected").val()==0){
                        seleccionebodega=0;
                    }
                    DATAe.push(item);
                    equiposselect++;
                }
        });
        arrayequipos   = JSON.stringify(DATAe);
    //================================
    //================================
        var DATAe  = [];
        var TABLAe   = $("#tableg_consumiblese tbody > tr");
            TABLAe.each(function(){
                if ($(this).find("input[class*='consumibles_check_coti']").is(':checked')) {         
                    item = {};
                    item ["consumible"] = $(this).find("input[class*='consumibles_check_coti']").val();
                    item ["id_consumibles"] = $(this).find("input[id*='id_consumibles']").val();
                    item ["cantidad"]   = $(this).find("input[id*='cantidad_consumibles']").val();
                    item ["bodega"]  = $(this).find("select[id*='serie_bodegae'] option:selected").val();
                    if($(this).find("input[id*='cantidad_consumibles']").val()>$(this).find("select[id*='serie_bodegae'] option:selected").data('stock')){
                        verificarstock=0;
                    }
                    if($(this).find("select[id*='serie_bodegae'] option:selected").val()==0){
                        seleccionebodega=0;
                    }
                    DATAe.push(item);
                    equiposselect++;
                }
        });
        arrayconsumibles   = JSON.stringify(DATAe);
    //================================
    //================================
        var DATAe  = [];
        var TABLAe   = $("#tableg_accesorios tbody > tr");
            TABLAe.each(function(){
                if ($(this).find("input[class*='accesorio_check_coti']").is(':checked')) {         
                    item = {};
                    item ["accesorio"] = $(this).find("input[class*='accesorio_check_coti']").val();
                    item ["cantidad"]   = $(this).find("input[id*='cantidad_accesorio']").val();
                    item ["bodega"]  = $(this).find("select[id*='serie_bodegae'] option:selected").val();
                    if($(this).find("input[id*='cantidad_accesorio']").val()>$(this).find("select[id*='serie_bodegae'] option:selected").data('stock')){
                        verificarstock=0;
                    }
                    if($(this).find("select[id*='serie_bodegae'] option:selected").val()==0){
                        seleccionebodega=0;
                    }
                    DATAe.push(item);
                    equiposselect++;
                }
        });
        arrayaccessorios   = JSON.stringify(DATAe);
    //================================
        var refa = [];
        $("#tableg_refacciones tbody > tr").each(function() {
            if($(this).find("input[name*='nombre']").is(':checked')){
                //var aux = $(this).attr("id").split('_');
                itemp={};
                itemp['id']=$(this).find("input[name*='nombre']").val();
                itemp['cantidad']=$(this).find("input[id*='cantidadnew']").val(); 
                itemp['serie']=$(this).find("input[id*='serieselect']").val(); 
                itemp['bodega']=$(this).find("select[id*='serie_bodegae']").val();  
                if($(this).find("input[id*='cantidadnew']").val()>$(this).find("select[id*='serie_bodegae'] option:selected").data('stock')){
                        verificarstock=0;
                }
                if($(this).find("select[id*='serie_bodegae'] option:selected").val()==0){
                        seleccionebodega=0;
                }                  
                refa.push(itemp);
                equiposselect++;
            }
        });
        INFOp  = new FormData();
        refacc   = JSON.stringify(refa);
    //================================
        var nombrec = [];
        $("#tableg_consumibles tbody > tr").each(function() {
            if($(this).find("input[name*='nombre']").is(':checked')){
                //var aux = $(this).attr("id").split('_');
                itemp={};
                itemp['id']=$(this).find("input[name*='nombre']").val();
                itemp['cantidad']=$(this).find("input[id*='cantidadnew']").val();
                itemp["bodega"]  = $(this).find("select[id*='serie_bodegae'] option:selected").val();   
                if($(this).find("input[id*='cantidadnew']").val()>$(this).find("select[id*='serie_bodegae'] option:selected").data('stock')){
                    verificarstock=0;
                }    
                if($(this).find("select[id*='serie_bodegae'] option:selected").val()==0){
                        seleccionebodega=0;
                }             
                nombrec.push(itemp);
                equiposselect++;
            }
        });
        INFOp  = new FormData();
        arrayconsu   = JSON.stringify(nombrec);
    //================================
        var DATAs = [];
        var TABLA = $("#tableg_servicio tbody > tr");
        TABLA.each(function() {
            if($(this).find("input[name*='polizaselected']").is(':checked')){
                var poliza=$(this).find("input[name*='polizaselected']").val();
                var DATAse = [];
                var TABLAse = $(".table_series_e_"+poliza+" tbody > tr");
                TABLAse.each(function() {
                    items = {};
                    items["series"] = $(this).find("input[id*='polizaserie']").val();
                    DATAse.push(items);
                });
            //aInfose = JSON.stringify(DATAse);

                item = {};
                item["cotizacion"] = $('#idCotizacion').val();
                item["poliza"] = poliza;
                item["cantidad"] = $(this).find("input[id*='cantidad']").val();
                item["serie"] = DATAse;
                DATAs.push(item); 
                equiposselect++;
        }
        
    });
    INFO = new FormData();
    arrserv = JSON.stringify(DATAs);
    //===================================

    if (equiposselect>0) {
        if(seleccionebodega==1){
            if(verificarstock==1){
                if($('#form-cot').valid()){
                    var ord_com = $('#orden_comp').val()==undefined?'':$('#orden_comp').val();
                    var idCotizacion=$('#idCotizacion').val();
                    $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Cotizaciones/autorizarcotizaciongeneral",
                        data: {
                            idCotizacion:$('#idCotizacion').val(),
                            equipos:arrayequipos,
                            consumibles:arrayconsumibles,
                            accesorios:arrayaccessorios,
                            servicio:servicio,
                            refacc:refacc,
                            arrayconsu:arrayconsu,
                            arrserv:arrserv,
                            poliza: $('#tpoliza3 option:selected').val(),
                            servicio2: $('#tservicio3 option:selected').val(),
                            direccion: $('#tdireccion3 option:selected').val(),
                            tipo: $('#ttipo33 option:selected').val(),
                            motivo: $('#tserviciomotivo3').val(),
                            fechae:$('#ventaeservicio_fe').val(),
                            v_ser_tipo:$('#v_ser_tipo').val(),
                            v_ser_id:$('#v_ser_id').val(),
                            ordenc:ord_com
                        },
                        success: function (response){
                            console.log(response);
                            var respuesta = $.parseJSON(response);
                            
                            $.alert({
                                boxWidth: '30%',
                                useBootstrap: false,
                                title: 'Éxito!',
                                type: 'green',
                                theme: 'bootstrap',
                                content: 'Se ha autorizado la cotización '+idCotizacion+' y se ha asignado el número: '+respuesta.idVenta+'. Podrá visualizar esta información en el listado'}); 
                            
                            // Recargamos la página
                            
                            setTimeout(function(){ 
                                location.reload();
                            }, 3500);
                        },
                        error: function(response){
                            notificacion(1); 
                        }
                    });
                }
            }else{
                $.alert({boxWidth: '30%',useBootstrap: false,
                        title: 'Atención!',
                        type: 'red',theme: 'bootstrap',
                        content: 'Verifique el stock de uno de sus productos'});  
            }
        }else{
            $.alert({boxWidth: '30%',useBootstrap: false,
                        title: 'Atención!',
                        type: 'red',theme: 'bootstrap',
                        content: 'Uno de los productos no tiene seleccionada una bodega'}); 
        }
    }else{
        $.alert({boxWidth: '30%',useBootstrap: false,
                    title: 'Atención!',
                    type: 'red',theme: 'bootstrap',
                    content: 'Debe de seleccionar un equipo mínimo'});  
        $('body').loadingModal('destroy');
    }
}
function bloqueov(){
    swal({
      title: "Bloqueado",
      text: "Cliente bloqueado, contacte a administración.",
      type: "warning",
      showCancelButton: false,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "OK",
      closeOnConfirm: true
    },
    function(){

    });
}
function clonar(idc){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Desea realizar clonado de la cotización?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                
                 $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Cotizaciones/clonar",
                    data: {
                        idc:idc
                    },
                    success: function (response){
                        $.alert({boxWidth: '30%',useBootstrap: false,
                            title: 'Éxito!',
                            type: 'green',theme: 'bootstrap',
                            content: 'Se realizo el clonado con éxito'}); 
                        load();
                    },
                    error: function(response){
                        notificacion(1);
                    }
                });
                    
                
            },
            cancelar: function (){
                
            }
        }
    });
}
function obtenerventaspendientes(){
    var idcliente = $('#idCliente').val();
    //restriccion para cuando tenga mas de 3 prefacturas pendientes de agregar pago
    if(idcliente>0){
        $.ajax({
            type:'POST',
            url: base_url+'index.php/Reportes/rventasnum/'+idcliente,
            success:function(data){
                console.log(data);
                var numventas=parseInt(data);
                if(numventas>0){
                    $.confirm({
                        boxWidth: '30%',
                        useBootstrap: false,
                        icon: 'fa fa-warning',
                        title: 'Atención!',
                        content: 'El cliente tiene '+numventas+' pendientes de pago!'+
                                 '<!--<input type="text" placeholder="Contraseña" id="contrasena" name="contrasena" class="name form-control" data-val="" onpaste="return false;" maxlength="10"/>-->',
                        type: 'red',
                        typeAnimated: true,
                        buttons:{

                            confirmar: function (){
                                /*
                                //var pass=$('#contrasena').val();
                                var pass=$("input[name=contrasena]").val();
                                if (pass!='') {
                                     $.ajax({
                                        type:'POST',
                                        url: base_url+"index.php/Sistema/solicitarpermiso/"+idcliente,
                                        data: {
                                            pass:pass
                                        },
                                        success: function (response){
                                                var respuesta = parseInt(response);
                                                if (respuesta==1) {
                                                }else{
                                                    notificacion(2);
                                                    setTimeout(function(){
                                                        location.reload();
                                                    },1000);
                                                }
                                        },
                                        error: function(response){
                                            notificacion(1);
                                            setTimeout(function(){
                                                        location.reload();
                                            },2000);
                                        }
                                    });
                                    
                                }else{
                                    notificacion(0);
                                    setTimeout(function(){
                                        window.location.href = base_url+"index.php/Clientes";
                                    },2000);
                                }
                                */
                            },
                            'Ver Pendientes': function (){
                                obtenerventaspendientesinfo(idcliente);
                            },
                            cancelar: function (){
                                setTimeout(function(){
                                    //window.location.href = base_url+"index.php/Clientes";
                                },1000);
                            }
                        }
                    });
                    setTimeout(function(){
                        new MaskedPassword(document.getElementById("contrasena"), '\u25CF');
                    },1000);
                }
            }
        });
        
        setTimeout(function(){
            /*
            $("#contrasena").passwordify({
                maxLength: 10,
                onlyNumbers: true
              }).focus();
              */
        },2000);
    }
    
}
function passinput(){
    $("#contrasena").change();
    var pass=$('#contrasena').val();
    if(pass==''){
        $('#contrasena').data('val','');
    }
}
//==========================envio
function envio_s_m(asi,cliente){
    //$('#firma_m_s').val(54);
    m_asi=asi;
    //m_tipo=tipo;
    m_cli=cliente;
    obtenercontacto(cliente);
    //obtenerbodymail(cliente);
    $('.input_envio_c').html('');
    $('.table_mcr_files_c').html('');
    //$('.input_envio_s').html('<input type="file" id="files" name="files[]" multiple accept="*">');
    $('.input_envio_s').html('<iframe id="ifracotiza" title="Cotizacion" src="'+base_url+'index.php/Cotizaciones/pdf/'+asi+'"></iframe>');
    $('.table_mcr_files_s').html('<table class="table bordered striped" id="table_mcr_files"><tbody class="tabbodycr_files"></tbody></table>');
    $('.addbtnservicio').html('<button type="button" class="waves-effect btn-bmz  red btn-envioser" onclick="envio_s('+asi+')">Enviar</button>');
    //inputfileadd();
    $('#ms_asucto2').val('Cotizacion '+asi);
    $('#modal_servicio').modal();
    $('#modal_servicio').modal('open');
}
function obtenercontacto(cliente){
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Calendario/obtenercontactos",
        data: {
            cliente:cliente
        },
        success: function (response){
            $('.m_s_contac').html('<select class="browser-default form-control-bmz" id="ms_contacto">'+response+'</select>');
        },
        error: function(response){
            notificacion(1);   
        }
    });
    //=====================================================================
}
function envio_s(asi){
    var email =$('#ms_contacto option:selected').data('email');
    $.ajax({
        type:'POST',
        url: base_url+'index.php/Mailenvio/envio_cotizacion',
        data: {
            asi:asi,
            email:email,
            per:$('#firma_m_s option:selected').val(),
            perbbc:$('#bbc_m_s option:selected').val(),
            asunto:$('#ms_asucto2').val(),
            comen:$('#ms_coment2').val()
        },
        success:function(data){
            console.log(data);
            var array = $.parseJSON(data);
            if(array.envio==1){
                toastr["success"]("Correo enviado");
                $('#modal_servicio').modal('close');
                $('#ms_coment2').val('');
            }else{
                toastr["error"]("No se pudo enviar el correo");
            }
        },
        error: function(response){
            notificacion(1);
        }
    });
}
function detallep(id){
    window.open(base_url+"PolizasCreadas/view/"+id, "Prefactura", "width=780, height=612");
}
function detallev(id,tipo){
    if (tipo==0) {
        window.open(base_url+"Prefactura/view/"+id, "Prefactura", "width=780, height=612");
    }else{
        window.open(base_url+"Prefactura/viewc/"+id, "Prefactura", "width=780, height=612");
    }
}
function servicioaddcliente(){
    $('#modalservicioadd').modal('open');
    var idCliente = cli_aux;
    $.ajax({
        type:'POST',
        url: base_url+'Generales/servicioaddcliente',
        data: {
            cli: idCliente
        },
        async: false,
        statusCode:{
            404: function(data){
                    swal("Error", "404", "error");
            },
            500: function(){
                swal("Error", "500", "error"); 
            }
        },
        success:function(data){
          $('.servicioaddcliente').html(data);

            if (typeof serieselected   !== 'undefined') {
                //console.log('La variable está definida.');
                if(serieselected!=''){
                    $('.series_hide_show').hide();
                    $('.'+serieselected).show();
                }
                
            } else {
                //console.log('La variable no está definida.');
            }

        }
    });
}
function selectservent(ser,tipo){
    var ser_fecha=$('.seri_'+tipo+'_'+ser).data('fecha');
    $('#v_ser_tipo').val(tipo);
    $('#v_ser_id').val(ser);
    var htmlser='Servicio: '+ser+' Fecha:'+ser_fecha+' ';
        htmlser+='<button class="b-btn b-btn-danger" onclick="deleteser()"><i class="fas fa-trash"></i></button>';
    $('.addinfoser').html(htmlser);
    $('#modalservicioadd').modal('close');
}
function deleteser(){
    $('#v_ser_tipo').val(0);
    $('#v_ser_id').val(0);
    $('.addinfoser').html('');
}

function tipocotizacion(tipog){
    var html='';
    if(tipog!=null){
        if(tipog!='0'){
            var tipos = tipog;
            // Convertir la cadena en un array utilizando el método split(',')
            var tiposArray = tipos.split(',');

            // Iterar sobre el array usando un bucle for
            for (var i = 0; i < tiposArray.length; i++) {
                // Acceder al elemento actual usando tiposArray[i]
                //console.log(tiposArray[i]);
                if(tiposArray[i]==1){
                    html+='<br>Equipos';    
                }else if(tiposArray[i]==2){
                    html+='<br>Consumibles';    
                }else if(tiposArray[i]==3){
                    html+='<br>Refacciones';    
                }else if(tiposArray[i]==4){
                    html+='<br>Pólizas';    
                }else if(tiposArray[i]==5){
                    html+='<br>Accesorios';    
                }
            }
        }
    }
    return html;
}
function tipocotizacion_p(tipog){
    var html=0;
    if(tipog!=null){
        if(tipog!='0'){
            var tipos = tipog;
            // Convertir la cadena en un array utilizando el método split(',')
            var tiposArray = tipos.split(',');

            // Iterar sobre el array usando un bucle for
            for (var i = 0; i < tiposArray.length; i++) {
                // Acceder al elemento actual usando tiposArray[i]
                //console.log(tiposArray[i]);
                if(tiposArray[i]==1){
                    //html+='<br>Equipos';    
                }else if(tiposArray[i]==2){
                    //html+='<br>Consumibles';    
                }else if(tiposArray[i]==3){
                    //html+='<br>Refacciones';    
                }else if(tiposArray[i]==4){
                    html=1;    
                }else if(tiposArray[i]==5){
                    //html+='<br>Accesorios';    
                }
            }
        }
    }
    return html;
}