<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Consumibles_sol extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->model('ModeloCatalogos');
        $this->load->model('Rentas_model');
        $this->load->model('login_model');
        $this->load->model('Consumibles_model');
        $this->enviosproductivo=0;//0 demo 1 productivo
    }

    function index(){
        $this->load->library('encryption');
        $codigo=$_GET['id'];
        //$codigocond=$this->encryption->decrypt($codigo);
        $idenvio=0;
        $strq = "SELECT * FROM envio_mail where folio='$codigo' ";
                
        $query_a = $this->db->query($strq);
        foreach ($query_a->result() as $item) {
            $idenvio=$item->id;
            $tipo=$item->tipo;
            $reg=$item->reg;
        }
        $strqcont = "SELECT emc.contratoid,con.idRenta,cli.empresa,
                            con.rentacolor,con.precio_c_e_color,con.tiporenta

                            FROM envio_mail_contrato as emc
                            INNER JOIN contrato as con on con.idcontrato=emc.contratoid
                            INNER JOIN rentas as ren on ren.id=con.idRenta
                            INNER JOIN clientes as cli on cli.id=ren.idCliente
                            WHERE emc.idenvio='$idenvio'";
        $query_cont = $this->db->query($strqcont);
        $empresa='';
        foreach ($query_cont->result() as $item) {
            $data['empresa']=$item->empresa;
            $data['idcontrato']=$item->contratoid;
            $data['idRenta']=$item->idRenta;
        }

        
        if($idenvio>0){
            $data['idenvio']=$idenvio;
            $mes=date('m');
            $data['periodo']=$this->messpanol($mes);
            //$data['periodo']=$reg;
            $data['query_cont']=$query_cont;
            $data['tipo']=$tipo;
            //$data['equiposerie'] = $this->Rentas_model->equiposrentas2($idRenta);

            $this->load->view('consumibles_sol',$data); 
        }else{
            echo '<!--'.$codigocond.'-->';
        }
         
    }
    function messpanol($mes){
      switch ($mes) {
        case '01':
          $mes ="ENERO";
          break;
        case '02':
          $mes ="FEBRERO";
          break;
        case '03':
          $mes ="MARZO";
          break;
        case '04':
          $mes ="ABRIL";
          break;
        case '05':
          $mes ="MAYO";
          break;
        case '06':
          $mes ="JUNIO";
          break;
        case '07':
          $mes ="JULIO";
          break;
        case '08':
          $mes ="AGOSTO";
          break;
        case '09':
          $mes ="SEPTIEMBRE";
          break;
        case '10':
          $mes ="OCTUBRE";
          break;
        case '11':
          $mes ="NOVIEMBRE";
          break;
        case '12':
          $mes ="DICIEMBRE";
          break;
        
        default:
          $mes ="";
          break;
      }
      return $mes;
    }
    function enviosol(){
        $datas = $this->input->post();
        $productos=$datas['productos'];
        $consumiblescot =$datas['cot'];
        $cotdes=$datas['cotdes'];
        $dataequiposrarray=array();
        
        $contratoid=0;

        $DATAc = json_decode($productos);
        for ($i=0;$i<count($DATAc);$i++) {
                $contratoid=$DATAc[$i]->contratoid;

                $datae['contratoid']=$contratoid;
                $datae['equipoid']=$DATAc[$i]->equipoid;
                $datae['equiporow']=$DATAc[$i]->equiporow;
                $datae['serieid']=$DATAc[$i]->serieid;
                $datae['consumibleid']=$DATAc[$i]->consumibleid;
                $datae['cantidad']=$DATAc[$i]->cantidad;
                $datae['tipo']=$DATAc[$i]->tipo;
                $datae['comen']=$DATAc[$i]->comen;
                $datae['idenvio']=$DATAc[$i]->idenvio;
                


                $dataequiposrarray[]=$datae;
        }
        if(count($dataequiposrarray)>0){
            $this->ModeloCatalogos->insert_batch('consumibles_solicitud',$dataequiposrarray);
        }


        $DATAco = json_decode($consumiblescot);
        if(count($DATAco)>0){
          
          $sql="SELECT ren.atencion,ren.correo,ren.telefono,ren.idCliente
                FROM contrato as con
                INNER JOIN rentas as ren on ren.id=con.idRenta
                WHERE con.idcontrato=$contratoid ";
          $query_c = $this->db->query($sql);
          $atencion='';
          $correo='';
          $telefono='';
          $idCliente='';
          foreach ($query_c->result() as $itemc) {
            $atencion=$itemc->atencion;
            $correo=$itemc->correo;
            $telefono=$itemc->telefono;
            $idCliente=$itemc->idCliente;
          }


          $dataconsumiblesarray=array();

          $datosCotizacion = array('atencion' => $atencion,
                                        'correo' => $correo,
                                        'telefono' => $telefono,
                                        'idCliente' => $idCliente,
                                        'tipo' => 2,
                                        'tiporenta' =>0,
                                        'eg_rm' =>0,
                                        'eg_rvm' =>0,
                                        'eg_rpem' =>0,
                                        'eg_rc' =>0,
                                        'eg_rvc' =>0,
                                        'eg_rpec' =>0,
                                        'nccc'=>0,
                                        'rentaVentaPoliza' => 1,
                                        'id_personal'=>49,
                                        'propuesta'=>0,
                                        'tipov'=>1,
                                        'estatus' => 1,
                                        'info2'=>$cotdes
                                    );
          $idCotizacion = $this->ModeloCatalogos->Insert('cotizaciones',$datosCotizacion);

          for ($l=0;$l<count($DATAco);$l++) {
              $idEquipo = $DATAco[$l]->idEquipo;
              $piezas = $DATAco[$l]->piezas;
              $idConsumible = $DATAco[$l]->idConsumible;

                $datosConsumible = $this->Consumibles_model->getConsumibleConPrecios($idConsumible);

                $total=$datosConsumible[0]->general*$piezas;

                $detallesCotizacion = array('idCotizacion' => $idCotizacion,
                                        'idEquipo' => $idEquipo,
                                        'idConsumible' => $idConsumible,
                                        'piezas' => $piezas, 
                                        'modelo' => $datosConsumible[0]->modelo,
                                        'rendimiento' =>'',
                                        'descripcion' => $datosConsumible[0]->observaciones,
                                        'precioGeneral' =>$datosConsumible[0]->general,
                                        'totalGeneral' => $total
                                    );
                
                $dataconsumiblesarray[]=$detallesCotizacion;
          }
          if(count($dataconsumiblesarray)>0){
            $this->ModeloCatalogos->insert_batch('cotizaciones_has_detallesConsumibles',$dataconsumiblesarray);
          }

          $this->enviamail($idCotizacion);


        }








        echo 'exito';
    }
    function getDatosConsumiblesPorEquipo($idEquipo){
      $detallesConsumibles = $this->Consumibles_model->getConsumiblesPorIdEquipo($idEquipo,0);
        $html = '<option selected="selected" disabled="disabled">Seleccione</option>';
        foreach($detallesConsumibles as $detalleConsumible){
            if($detalleConsumible->tipo==1){}else{
               $html .= '<option value="'.$detalleConsumible->id.'">'.$detalleConsumible->modelo.'</option>'; 
            }
            //$html .= $this->creaOptions($detalleConsumible);
            
        }
        echo $html;
    }
    function enviamail($idCotizacion){
            $asunto='Cotizacion '.$idCotizacion;
        //================================
            //cargamos la libreria email
            $this->load->library('email');

            /*
            * Configuramos los parámetros para enviar el email,
            * las siguientes configuraciones es recomendable
            * hacerlas en el fichero email.php dentro del directorio config,
            * en este caso para hacer un ejemplo rápido lo hacemos 
            * en el propio controlador
            */
            
            //Indicamos el protocolo a utilizar
            $config['protocol'] = 'smtp';
             
            //El servidor de correo que utilizaremos
            $config["smtp_host"] ='mail.altaproductividadapr.com'; 
             
            //Nuestro usuario
            $config["smtp_user"] = 'info@altaproductividadapr.com';

            //Nuestra contraseña
            $config["smtp_pass"] = 'yUs&J=T4*V$J';

            //Puerto
            $config["smtp_port"] = '465';

            $config["smtp_crypto"] = 'ssl';
                    
            //El juego de caracteres a utilizar
            $config['charset'] = 'utf-8'; 
     
            //Permitimos que se puedan cortar palabras
            $config['wordwrap'] = TRUE;
             
            //El email debe ser valido  
            $config['validate'] = true;

            $config['mailtype'] = 'html';

            //Establecemos esta configuración
            $this->email->initialize($config);
            //$asunto='Servicio';
            //Ponemos la dirección de correo que enviará el email y un nombre
            $this->email->from('info@altaproductividadapr.com',$asunto);
             
              /*
               * Ponemos el o los destinatarios para los que va el email
               * en este caso al ser un formulario de contacto te lo enviarás a ti
               * mismo
               */
            //======================
            //$this->email->bcc('andres@mangoo.mx');
            $this->email->bcc('info@altaproductividadapr.com');
            
            $correo='ag-gerardo@hotmail.com';
            if($this->enviosproductivo==1){
                $correo='kyorent2@kyoceraap.com';
            }
            
            

            //Definimos el asunto del mensaje

                $this->email->subject($asunto);
            
            $this->email->to($correo);

            $urlcotizacion=base_url().'Cotizaciones/documento/'.$idCotizacion;
        $message  = '<div style="background: url(https://altaproductividadapr.com/public/img/fondo.jpg) #eaeaea;width: 100%;height: 94%;min-height: 752px;padding-top: 25px;margin: 0px;">
                    <div style="width: 10%; height: 10px; float: left;">
                        
                    </div>
                    <div style="background: white; border-top: 3px solid #ff0101; padding: 25px 25px 0px 25px; width: 80%; max-width: 825px;margin-left: auto; margin-right: auto; float: left; -webkit-box-shadow: 10px 10px 42px -6px rgba(0,0,0,0.75);-moz-box-shadow: 10px 10px 42px -6px rgba(0,0,0,0.75);box-shadow: 10px 10px 42px -6px rgba(0,0,0,0.75);">
                        <div style="width: 100%; float: left;">
                            <div style="width: 30%;float: left;">
                                <img src="https://altaproductividadapr.com/public/img/cotheader2.png" style="max-height: 100px;">
                            </div>
                            <div style="width: 40%;float: left; min-height:10px; font-style: italic; font-size: 14px; color:#2e4064;">
                                Impresoras y Multifunciones, Venta, Renta y Mantenimiento. Blvd. Norte 1831 Col. Villas San Alejandro. Puebla, Pue. <br>Call Center 273 3400, 249 5393. www.kyoceraap.com
                            </div>
                            <div style="width: 30%;float: left;">
                                <img src="https://altaproductividadapr.com/public/img/alta.jpg" style="max-height: 100px;">
                            </div>
                        </div>
                        <div style="width: 100%; float: left;">
                            <div style="width: 50%;float: left;height: 10px;"></div>
                            <div style="width: 50%;float: left; border-bottom: 3px solid #ff0101; font-size:17px"><b>Cotizacion</b></div>
                        </div>
                        
                        <div style="width: 100%; float: left;font-size: 14px; margin-bottom: 10px;color:#2e4064; margin-bottom: 10px; text-align:justify">
                            Se realizo una cotizacion automatica por parte del cliente <a href="'.$urlcotizacion.'" style="font-weight: bold;color: red;">'.$idCotizacion.'</a>
                        </div>
                        <div style="width: 100%; float: left;font-size: 14px; margin-bottom: 10px;color:#2e4064; margin-bottom: 10px;">
                            
                        </div>
                        <div style="width: 100%; float: left;font-size: 14px; margin-bottom: 10px;color:#2e4064; margin-bottom: 10px;">
                            <img src="https://altaproductividadapr.com/public/img/cot_footer.jpg" width="100%">
                        </div>

                        
                    </div>
                </div>';

        $this->email->message($message);
        
        //Enviamos el email y si se produce bien o mal que avise con una flasdata
        if($this->email->send()){
            //$this->session->set_flashdata('envio', 'Email enviado correctamente');
        }else{
            //$this->session->set_flashdata('envio', 'No se a enviado el email');
        }
        $array=array(
                    'newcodigo'=>date('YmdGis')
                );
        echo json_encode($array);
        //==================
    }
}
