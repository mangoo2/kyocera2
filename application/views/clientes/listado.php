<?php
  if($perfilid==8){
    $disabledopciones=' style="display:none;" ';
    $disabledopciones='';
  }else{
    $disabledopciones='';
  }
?>


<style type="text/css">
  table td{
    font-size: 12px;
    padding: 6px 7px;
  }
  .clientebloqueado{
    color: red;
    font-size: 15px;
    margin-left: 30px;
  }
  <?php if($perfilid==11){ ?>
      #lista_llamadas,#lista_correos,#lista_visitas,#lista_agenda,#lista_timeline,#lista_cotiza,#lista_cotiza_historico{display: none;}
  <?php } ?>
  #tabla_clientes{
    width: 100% !important;
  }
  .ont_datosfiscales{
    color: #7c7373;
    font-size: 11px;
  }
</style>
<!-- START CONTENT -->
    <section id="content">
      <!--start container-->
        <div class="container">
          <div class="section">

          <input id="base_url" type="hidden" name="base_url" value="<?php echo base_url(); ?>">
          <input id="idUsuario" type="hidden" name="perfilid" value="<?php echo $_SESSION["usuarioid"]; ?>">
          <input id="perfilid" type="hidden"  value="<?php echo $perfilid; ?>">
          
          <div >
 
                <h4 class="header">Listado de Clientes</h4>
                <div class="row">
                  <div class="col s12" style="text-align:end">
                    <a class="waves-effect cyan btn-bmz" onclick="search_cotizacion()" style="margin-bottom: 10px;"><i class="fa fa-search"></i> Buscar Cotización</a>
                  </div>
                </div>
                <div class="row">
                  
                  <div class="col s3">
                      <a class="btn-bmz waves-effect waves-light gradient-45deg-green-teal gradient-shadow" href="<?php echo base_url(); ?>index.php/Clientes/alta">Nuevo</a>
                  </div>
                  <div class="col s4">
                  </div>
                  <div class="col s5" align="right">
                        <button type="submit" class="btn-bmz waves-effect waves-light purple lightrn-1" id="descargarExcel" name="descargarExcel">Descargar Excel <i class="material-icons">file_download</i></button>
                  </div>
                

                  <div class="col s12">

                    <table id="tabla_clientes" class="table" cellspacing="0">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Empresa</th>
                         <!-- <th>Persona contacto</th> -->
                          <th>Email</th>
                          <th>Estado</th>
                          <th>Municipio</th>
                          <th>Giro empresa</th>
                          <!--<th>Dirección</th>-->
                          <th>Observaciones</th>
                          <th>Vendedor</th>
                          <th>Acciones</th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
          </div>  
        </div>

        <div id="modalAcciones" class="modal"  >
          <div class="modal-content ">

            <h4> <i class="material-icons">mode_edit</i> Acciones </h4>

            <div class="col s12 m12 l6">
                  <div class="row" align="center">
                      <ul>
                        <li>
                          <a class="btn-floating green tooltipped edit" data-position="top" data-delay="50" data-tooltip="Editar" id="lista_editar"><i class="material-icons">mode_edit</i></a>
                          <a class="btn-floating blue tooltipped visualiza" data-position="top" data-delay="50" data-tooltip="Visualizar" id="lista_visualizar"  ><i class="material-icons">remove_red_eye</i></a>
                          <a class="btn-floating red tooltipped elimina" data-position="top" data-delay="50" data-tooltip="Eliminar" id="lista_eliminar" <?php echo $disabledopciones;?> ><i class="material-icons">delete_forever</i></a>
                          <a class="btn-floating light-blue darken-4 tooltipped" data-position="top" data-delay="50" data-tooltip="Venta" id="ventas" <?php echo $disabledopciones;?> ><i class="material-icons">add_shopping_cart</i></a>
                          <a class="btn-floating light-blue darken-4 tooltipped" data-position="top" data-delay="50" data-tooltip="Cotizar" id="lista_cotiza" <?php echo $disabledopciones;?> ><i class="material-icons">monetization_on</i></a>
                          <a class="btn-floating deep-orange darken-4 tooltipped" data-position="top" data-delay="50" data-tooltip="Lista de Cotizaciones" id="lista_cotiza_historico" <?php echo $disabledopciones;?>><i class="material-icons">format_list_numbered</i></a> 
                          <a class="btn-floating orange tooltipped llamadas disabled" data-position="top" data-delay="50" data-tooltip="Agenda Llamada" id="lista_llamadas"><i class="material-icons">phone_in_talk</i></a>
                          <a class="btn-floating pink tooltipped correos disabled" data-position="top" data-delay="50" data-tooltip="Agendar Correo" id="lista_correos"><i class="material-icons">email</i></a>
                          <a class="btn-floating amber tooltipped visitas disabled" data-position="top" data-delay="50" data-tooltip="Agendar Visita" id="lista_visitas"><i class="material-icons">add_alert</i></a>
                          <a class="btn-floating cyan tooltipped agenda disabled" data-position="top" data-delay="50" data-tooltip="Revisar Agenda" id="lista_agenda"><i class="material-icons">date_range</i></a>
                          <a class="btn-floating light tooltipped timeline disabled" data-position="top" data-delay="50" data-tooltip="Revisar Timeline" id="lista_timeline"><i class="material-icons">timeline</i></a>
                        </li>
                      </ul>
                      <input type="hidden" name="idClienteModal" id="idClienteModal">
                      <input type="hidden" name="empresa" id="empresa">
                  </div>
              </div>
              
          <div class="modal-footer">
            <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
          </div>
        </div>
     </div>
     
    </section>

<div id="modalsearchcoti" class="modal"  >
  <div class="modal-content ">
    <h5>  Buscar Cotización </h5>
    <div class="col s12 m12 l6">
          <div class="row">
            <div class="col s10">
                <input type="number" id="search_cont" class="form-control-bmz" placeholder="N° Cotizacion">
            </div>
            <div class="col s2">
              <a class="waves-effect cyan btn-bmz" onclick="search_cot()" style="margin-bottom: 10px;"><i class="fa fa-search"></i> Buscar</a>
            </div>
            <div class="col s12 table_searcho_cot"></div>
          </div>
      </div>
  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
  </div>
</div>