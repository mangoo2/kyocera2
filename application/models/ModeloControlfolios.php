<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloControlfolios extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    /*
    function getlistcontratofolio($tecnico){
        if ($tecnico!='') {
            $wheretecnico="where tecnico='$tecnico'";
        }else{
            $wheretecnico="";
        }
        $sql = "SELECT * from ( 
                    SELECT cf.*,c.modelo,c.parte,p.nombre,r.reg,cli.empresa,
                    (SELECT con.idcontrato FROM contrato as con WHERE con.idRenta=cf.idrenta ORDER BY con.idcontrato DESC LIMIT 1) as idcontrato,
                    (
                        select concat(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno)
                        from asignacion_ser_contrato_a_e as asig 
                        inner join personal as per on per.personalId=asig.tecnico
                        where asig.asignacionId= cf.serviciocId limit 1
                    ) as tecnico,r.estatus
                    FROM contrato_folio AS cf
                    INNER JOIN consumibles AS c ON c.id = cf.idconsumible
                    INNER JOIN rentas AS r ON r.id = cf.idrenta
                    INNER JOIN personal AS p ON p.personalId = r.id_personal
                    INNER JOIN clientes AS cli ON cli.id = r.idCliente
                    where cf.status=0 
                ) as datos 
                $wheretecnico
                ";
        $query = $this->db->query($sql);
        return $query->result();
    }
    /*
    function getlistcontratofolio($tecnico){
        if ($tecnico!='') {
            $wheretecnico="where tecnico='$tecnico'";
        }else{
            $wheretecnico="";
        }
        $sql = "SELECT * from ( 
                    SELECT cf.*,c.modelo,c.parte,p.nombre,r.reg,cli.empresa,
                    (
                        select concat(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno)
                        from asignacion_ser_contrato_a_e as asig 
                        inner join personal as per on per.personalId=asig.tecnico
                        where asig.asignacionId= cf.serviciocId limit 1
                    ) as tecnico,r.estatus
                    FROM contrato_folio AS cf
                    INNER JOIN consumibles AS c ON c.id = cf.idconsumible
                    INNER JOIN rentas AS r ON r.id = cf.idrenta
                    INNER JOIN personal AS p ON p.personalId = r.id_personal
                    INNER JOIN clientes AS cli ON cli.id = r.idCliente
                    where cf.status=0 
                ) as datos 
                $wheretecnico
                ";
        $query = $this->db->query($sql);
        return $query->result();
    }
    */
    function getlistcontratofolio($params){
        $tecnico = $params['tecnico'];
        $fi=$params['fi'];
        $ff=$params['ff'];
        $columns = array(
            0=>'cf.idcontratofolio',
            1=>'c.modelo',
            2=>'c.parte',
            3=>'p.nombre',
            4=>'cf.fechagenera',
            5=>'cli.empresa',
            6=>"(
                    select concat(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno)
                    from asignacion_ser_contrato_a_e as asig 
                    inner join personal as per on per.personalId=asig.tecnico
                    where asig.asignacionId= cf.serviciocId limit 1
                ) as tecnico",
            7=>'cf.foliotext',
            8=>'r.estatus',
            9=>'vdc.idVentas',
            10=>'cf.idrenta',
            11=>'cf.preren',
            12=>'cf.proinren'

        );
        $columnsss = array(
            0=>'cf.idcontratofolio',
            1=>'c.modelo',
            2=>'c.parte',
            3=>'p.nombre',
            4=>'cf.fechagenera',
            5=>'cli.empresa',
            6=>'cli.empresa',
            7=>'cf.foliotext',
            8=>'r.estatus',
            9=>'vdc.idVentas',
            10=>'cf.preren',
            11=>'cf.proinren'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('contrato_folio as cf');
        $this->db->join('consumibles c', 'c.id=cf.idconsumible');
        $this->db->join('rentas r', 'r.id=cf.idrenta');
        $this->db->join('personal p', 'p.personalId=cf.creado_personal','left');
        $this->db->join('clientes cli', 'cli.id=r.idCliente');
        $this->db->join('ventas_has_detallesConsumibles vdc', 'vdc.foliotext=cf.foliotext','left');

        $this->db->where('cf.status',0);
        if($tecnico!=''){
            $this->db->where('tecnico',$tecnico);
        }
        if($fi!=''){
            $this->db->where("cf.fechagenera >='$fi 00:00:00'");
        }
        if($ff!=''){
            $this->db->where("cf.fechagenera <='$ff 23:59:59'");
        }
        
        
        //$this->db->where(array('facturaabierta'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columnsss as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columnsss[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    public function getlistcontratofoliot($params){
        $tecnico = $params['tecnico'];
        $fi=$params['fi'];
        $ff=$params['ff'];
        $columns = array(
            0=>'cf.idcontratofolio',
            1=>'c.modelo',
            2=>'c.parte',
            3=>'p.nombre',
            4=>'cf.fechagenera',
            5=>'cli.empresa',
            6=>"(
                    select concat(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno)
                    from asignacion_ser_contrato_a_e as asig 
                    inner join personal as per on per.personalId=asig.tecnico
                    where asig.asignacionId= cf.serviciocId limit 1
                ) as tecnico",
            7=>'cf.foliotext',
            8=>'r.estatus',
            9=>'cf.idrenta',
            10=>'cf.preren',
            11=>'cf.proinren'
        );
        $columnsss = array(
            0=>'cf.idcontratofolio',
            1=>'c.modelo',
            2=>'c.parte',
            3=>'p.nombre',
            4=>'r.reg',
            5=>'cli.empresa',
            6=>'cf.foliotext',
            7=>'r.estatus',
            8=>'cf.idrenta',
            9=>'vdc.idVentas',
            10=>'cf.preren',
            11=>'cf.proinren'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(*) as total');
        $this->db->from('contrato_folio as cf');
        $this->db->join('consumibles c', 'c.id=cf.idconsumible');
        $this->db->join('rentas r', 'r.id=cf.idrenta');
        $this->db->join('personal p', 'p.personalId=cf.creado_personal','left');
        $this->db->join('clientes cli', 'cli.id=r.idCliente');
        $this->db->join('ventas_has_detallesConsumibles vdc', 'vdc.foliotext=cf.foliotext','left');

        $this->db->where('cf.status',0);
        if($tecnico!=''){
            $this->db->where('tecnico',$tecnico);
        }
        if($fi!=''){
            $this->db->where("cf.fechagenera >='$fi 00:00:00'");
        }
        if($ff!=''){
            $this->db->where("cf.fechagenera <='$ff 23:59:59'");
        }
        
        //$where = ;
        //$this->db->where(array('facturaabierta'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columnsss as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        //$this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        //$this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query->row()->total;
    }
}

?>