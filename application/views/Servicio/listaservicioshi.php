<style type="text/css">
  .chosen-container{margin-left: 0px;}
  td{font-size: 12px;}
  #tabla_asignar{margin: 0px;}
  #tabla_asignar td{padding-top: 5px;padding-right: 5px;padding-left: 5px;padding-bottom: 5px;  }
  .card-panel{padding-left: 10px;padding-right: 10px;}
  @media (max-width: 768px) {
    .container {
      margin-right: 1px;
      margin-left: 1px;
    }
    .section {padding-top: 0px;padding-bottom: 0px;}
    .card-panel {padding-top: 3px;padding-bottom: 3px;}
  }
</style>
<!-- START CONTENT -->
<section id="content">
  <!--start container-->
  <div class="container">
    <div class="section">
      <div class="card-panel">
      		<div id="table-datatables">
             <div class="row">
              <div class="col s8">
                <h4 class="header">Listado de Servicios</h4>
              </div>            
              <div class="col s4" align="right"><br>
                <button class="btn cyan waves-effect waves-light right savepoliza" type="button" onclick="load()"><i class="material-icons dp48">autorenew</i>
                </button>
              </div>   
            </div>
            <div class="row">
              <div class="col s6 m2 l2">
                <label>Servicio</label>
                <select  id="serv" class="browser-default  form-control-bmz" >
                  <option value="0">Seleccionar</option><option value="1">Contrato</option><option value="2">Poliza</option><option value="3">Evento</option><option value="4">Ventas</option>
                </select>
              </div>
              <div class="col s2 m2 l2" style="display: none;">
                <label>Tipo de Servicio</label>
                <select  id="tiposer" class="browser-default" ><option value="0">Todos</option><option value="1">Regular</option><option value="2">Asignación diaria</option></select>
              </div>
              <div class="col s6 m2 l2" <?php echo $idpersonaldisplay;?> >
                <label>Técnico/Especialista</label>
                <select id="tecnico" class="browser-default  form-control-bmz" >
                  <option value="0">Todos</option>
                  <?php 
                    foreach ($resultstec->result() as $item) { ?>
                      <option value="<?php echo $item->personalId;?>" <?php if($idpersonal==$item->personalId){ echo 'selected';} ?> ><?php echo $item->tecnico;?></option>
                  <?php } ?>
                </select>
              </div>
              <div class="col s2 m2 l2" style="display: none;">
                <label>Finalizados/activos</label>
                <select id="status" class="browser-default  chosen-select" >
                  <?php if($perfilid==10){ ?>
                    <option value="0">Todos</option>
                  <?php }else{ ?>
                    <option value="3">Finalizados</option>
                  <?php } ?>
                  <!--<option value="0">Todos</option>
                  <option value="1">Activos</option>
                  <option value="2">Finalizados</option>-->
                </select>
              </div>
              <div class="col s2 m2 l2" style="display: none;">
                <label>Zona</label>
                <select id="zona" class="browser-default  chosen-select" >
                  <option value="0">Todos</option>
                    
                </select>
              </div>
              <div class="col s2 m2 l2" style="display: none;">
                <label>Cliente</label>
                <select id="pcliente" class="browser-default" >
                  <option value="0">Todos</option>
                </select>
              </div>
            

              <div class="col s6 m2 l2">
                <label>Fecha inicio</label>
                <input type="date" id="fechainicio" class="form-control-bmz" value="<?php echo $fechainicial_reg;?>">
              </div>
              <div class="col s6 m2 l2">
                <label>Fecha Fin</label>
                <input type="date" id="fechafin" class="form-control-bmz" >
              </div>
              <div class="col s4 m1  l1">
                <button class="b-btn b-btn-primary" onclick="load()" style="margin-top: 20px;" title="Realizar consulta"><i class="fas fa-filter"></i> Filtrar</button>
              </div>
            </div>
            <div class="row">
              <br>
            </div>
            <div class="row">
              <div class="col s12">
                <table id="tabla_asignar" class="table display" cellspacing="0">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th></th>
                      <th>Cliente</th>
                      <th>Fecha</th>
                      <th>Hora</th>
                      <th>Comentario</th>
                      <th class="cambiartitulo_8">Equipo</th>
                      <th>Serie</th>
                      <th>Status</th>
                      <th>Acciones</th>
                    </tr>
                  </thead>
                  <tbody>
                    <!------------------------------------->
                  
                    <!------------------------------------->
                  </tbody>
                </table>
              </div>
            </div>
          </div>
      </div>
    </div>  
  </div>
</section>
<input type="hidden" id="idrow">
<input type="hidden" id="idtable"><!--1 contrato 2 pencion 3 asignacion dia-->
<input type="hidden" id="asignacionId">






<div id="modalventa" class="modal">
    <div class="modal-content ">
      <h4> <i class="material-icons">settings_applications</i>Venta</h4>
      <hr>
      <div class="col s12 m12 l6">
        
        <div class="row ">
          <table class="table striped" id="tableaddconsulmibles">
            <thead>
              <tr>
                <th>Cantidad</th>
                <th>consumible</th>
                <th>Precio</th>
                <th></th>
              </tr>
            </thead>
            <tbody class="tabaddconsul">
              
            </tbody>
            
          </table>
        </div>
        
        <div class="row ">
          <table class="table striped" id="tableaddrefacciones">
            <thead>
              <tr>
                <th>Cantidad</th>
                <th>Refaccion</th>
                <th>Precio</th>
                <th></th>
              </tr>
            </thead>
            <tbody class="taaddrefaccion">
              
            </tbody>
            
          </table>
        </div>
        <div class="row">
          <div class="col s12 m8 l8">
          </div>
          <div class="col s12 m3 l3">
            <p>Total: <b>$<span class="totalg">0.00</span></b></p>
          </div>
        </div>
      </div> 
      <div class="modal-footer">
          <a class="modal-action modal-close waves-effect waves-red gray btn-flat">Cerrar</a>
      </div>
    </div>
</div>
<div id="modal_lis_ser" class="modal">
    <div class="modal-content ">
      <div class="row">
        <div class="col s12">
          <table class="table">
            <thead>
              <tr>
                <th></th>
                <th>Hora inicio</th>
                <th>Hora Fin</th>
                <th>Tiempo</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td></td>
                <td class="text_h_i"></td>
                <td class="text_h_f"></td>
                <td class="text_h_t"></td>
              </tr>
              <tr class="tiempoextras">
                <td>Extraordinario</td>
                <td class="text_h_iex"></td>
                <td class="text_h_fex"></td>
                <td class="text_h_tex"></td>
              </tr>
            </tbody>
          </table>
        </div>
        
      </div>  
    <div class="modal-footer">
      <a class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
    </div>
  </div>
</div>