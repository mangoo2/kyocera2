var base_url = $('#base_url').val();
var idper = $('head').data('idper');

var latitud = undefined;
var longitud = undefined;
/*
if (navigator.geolocation) {
    var success = function(position) {
        latitud = position.coords.latitude;
        longitud = position.coords.longitude;
    }
    var error = function(msg) {
        console.error(msg);
        latitud = undefined;
        longitud = undefined;
    }
    navigator.geolocation.getCurrentPosition(success, error);
}
*/
if (navigator.geolocation) {
    // Función de éxito
    var success = function(position) {
        latitud = position.coords.latitude;
        longitud = position.coords.longitude;
        console.log("Latitud: " + latitud + ", Longitud: " + longitud);
    };

    // Función de error
    var error = function(error) {
        console.error("Error al obtener la ubicación:", error.message);
        latitud = undefined;
        longitud = undefined;
    };

    // Usar watchPosition para obtener actualizaciones continuas
    navigator.geolocation.watchPosition(success, error, {
        enableHighAccuracy: true, // Mejor precisión posible
        maximumAge: 0,           // No usar caché
        timeout: 10000           // Tiempo de espera (opcional)
    });
} else {
    console.error("La geolocalización no está soportada en este navegador.");
}
Notification.requestPermission().then(permission => {
  if (permission === 'granted') {
    console.log('Permiso para notificaciones concedido.');
  } else {
    console.log('Permiso para notificaciones denegado.');
  }
});
function volver_solicitar_permisos_ubicacion(){
    if (navigator.geolocation) {
        // Función de éxito
        var success = function(position) {
            latitud = position.coords.latitude;
            longitud = position.coords.longitude;
            console.log("Latitud: " + latitud + ", Longitud: " + longitud);
        };

        // Función de error
        var error = function(error) {
            console.error("Error al obtener la ubicación:", error.message);
            //latitud = undefined;
            //longitud = undefined;
        };

        // Usar watchPosition para obtener actualizaciones continuas
        navigator.geolocation.watchPosition(success, error, {
            enableHighAccuracy: true, // Mejor precisión posible
            maximumAge: 0,           // No usar caché
            timeout: 10000           // Tiempo de espera (opcional)
        });
    } else {
        console.error("La geolocalización no está soportada en este navegador.");
    }
}
$(document).ready(function($) {
	$('.chat-out2').click(function(event) {
		$('.iframeitinerario').html('<iframe src="'+base_url+'index.php/Generales/lisitinerarioview">');
	});	
	$('.chat-out-ticket').click(function(event) {
		$('.iframeticket').html('<iframe src="'+base_url+'Tickes/Tickesiframe" style="border: 0px;height: 452px;" title="solicitar asistencia"></iframe>');
	});	
	$('.chat-out-registro').click(function(event) {
		console.log(longitud+' -- '+latitud);
		if(longitud!=undefined){
			$.ajax({
		        type:'POST',
		        url: base_url+"Asistencias/registro",
		        data: {
		        	longitud: longitud,
		        	latitud: latitud
		        },
		        success: function (data){
		        	var status = parseInt(data);
		        	if(status==1){
		        		$('.chat-out-registro').addClass('activo');
		        		setTimeout(function(){ 
	                 		window.location.reload();
		                }, 1000);
		        	}else{
		        		$('.chat-out-registro').removeClass('activo');
		        	}
		        }
		    });
		}else{
			$.confirm({
		        boxWidth: '50%',
		        useBootstrap: false,
		        icon: 'fa fa-warning',
		        title: 'Atención!',
		        content: 'Favor de Activar tu ubicacion para continuar..',
		        type: 'red',
		        typeAnimated: true,
		        buttons:{
		            Solicitar: function (){
		                volver_solicitar_permisos_ubicacion();
		            },
		            cancelar: function (){

		            }
		        }
		    });
		}
	});
	verificarestatuspersonal();
	setInterval(function () {
		generarsolicitudsess();
	}, 15000);//15 seg
	$('#tablerefaccionestecnico').DataTable({
		"lengthChange" : false,
		"bInfo":false,
	});
	verificarvacaciones();
});
function verificarestatuspersonal(){
	$.ajax({
        type:'POST',
        url: base_url+"Generales/verificarestatuspersonal",
        data: {
        	per: 0
        },
        success: function (data){
        }
    });
}
function permisotemp(porcetaje){
	$.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: 'Agregar nueva contraseña temporal para permisos<br><input type="text" placeholder="Contraseña" id="contrasena" class="name form-control" required/>',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var pass=$('#contrasena').val();
                if (pass!='') {
                     $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Sistema/permisotempnew",
                        data: {
                            pass:pass
                        },
                        success: function (response){
                            toastr["success"]("Permiso agregado");
                        },
                        error: function(response){
                        	toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema");
                        }
                    });
                    
                }else{
                	toastr["error"]("Ingrese una contraseña");
                }
            },
            cancelar: function (){ }
        }
    });
}
function generarsolicitudsess(){
	$.ajax({
        type:'POST',
        url: base_url+"Generales/generarsolicitudsess",
        data: {
        	per: 0
        },
        success: function (data){
        }
    });
}
function savecoor(idrow){
	$.confirm({
        boxWidth: '50%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Confirma guardar la ubicacion actual para la direccion?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
			        type:'POST',
			        url: base_url+"Asistencias/savecoor",
			        data: {
			        	longitud: longitud,
			        	latitud: latitud,
			        	idrow:idrow
			        },
			        success: function (data){
			        	toastr.success('Coordenadas registradas','Hecho!');
			        }
			    });
            },
            cancelar: function (){ }
        }
    });
}
function view_coor(longitud,latitud){
	var url0=base_url+"index.php/Asistencias/ubicacion/"+longitud+"/"+latitud;
	var url1="https://www.google.com/maps?q="+latitud+","+longitud;
	console.log(url0);
	console.log(url1);
	//window.open(url0, "Prefactura", "width=880, height=612"); 
	window.open(url1, "Prefactura", "width=880, height=612");
}
function obtenercoor(){
	//var url0=base_url+"index.php/Asistencias/ubicacion/"+longitud+"/"+latitud;
	var url0=base_url+"index.php/Asistencias/obtenerubicacion";
	//window.open(url0, "Prefactura", "width=880, height=612"); 
	window.open(url0, "Prefactura", "width=880, height=612"); 
}
function verificarvacaciones(){
	$.ajax({
        type:'POST',
        url: base_url+"index.php/Generales/verificarvacaciones",
        data: {
            pass:0
        },
        success: function (response){
            var res = parseInt(response);
            if(res>0){
            	$('body').loading({theme: 'dark',message: 'Bloqueado por vacaciones y/o incapacidad...'});
            }
        },
        error: function(response){
        	toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema");
        }
    });
}
function obtenerventaspendientesinfo(cliente){
	$.ajax({
        type:'POST',
        url: base_url+"index.php/Generales/obtenerventaspendientes",
        data: {
            cli:cliente
        },
        success: function (response){
        	var html='<div>';
        	    html+=response;
        	    html+='</div>';
            $.confirm({
                boxWidth: '30%',
                useBootstrap: false,
                icon: 'fa fa-warning',
                title: 'Pendientes de pago',
                content: html,
                type: 'red',
                typeAnimated: true,
                buttons:{
                    Aceptar: function (){
                        setTimeout(function(){
                            location.reload();
                        },1000);
                    }
                }
            });
        },
        error: function(response){
        	toastr.error("Algo salió mal, intente de nuevo o contacte al administrador del sistema");
        }
    });
}
function obtenerventaspendientesinfo2(cliente){
	$.ajax({
        type:'POST',
        url: base_url+"index.php/Generales/obtenerventaspendientes",
        data: {
            cli:cliente
        },
        success: function (response){
        	var html='<div>';
        	    html+=response;
        	    html+='</div>';
            $.confirm({
                boxWidth: '30%',
                useBootstrap: false,
                icon: 'fa fa-warning',
                title: 'Pendientes de pago',
                content: html,
                type: 'red',
                typeAnimated: true,
                buttons:{
                    Aceptar: function (){

                    }
                }
            });
        },
        error: function(response){
        	toastr.error("Algo salió mal, intente de nuevo o contacte al administrador del sistema");
        }
    });
}
function detalle_vp(id,tipo){
    if (tipo==0) {
        window.open(base_url+"Prefactura/view/"+id, "Prefactura", "width=780, height=612");
    }
    if (tipo==1) {
        window.open(base_url+"Prefactura/viewc/"+id, "Prefactura", "width=780, height=612");
    }
    if(tipo==2 || tipo==3){
    	window.open(base_url+"PolizasCreadas/view/"+id, "Prefactura", "width=780, height=612");
    }
}
function cambiar_cliente(id,tipo){
    var html='<div style="width: 99%;">\
                ¿Desea cambiar al cliente?<br>Se necesita permisos de administrador<br>\
                <input type="text" id="pass_cambio" name="pass_cambio" name="pass_cambio" class="form-control-bmz">\
                <label>Cliente</label>\
                <select class="browser-default" name="idcliente" id="idcliente" required></select></div>';
    $.confirm({
        boxWidth: '40%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Cambiar Cliente',
        content: html,
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var cli=$('#idcliente option:selected').val();
                //var pass=$('input[name="pass_cambio"]').val();
                //var pass=$('#pass_cambio').val();
                var pass=$("input[name=pass_cambio]").val();
                if(cli>0){
	                $.ajax({
	                    type:'POST',
	                    url: base_url+"index.php/Generales/cambiocliente",
	                    data: {
	                        id:id,
	                        tipo:tipo,
	                        cli:cli,
	                        pass:pass
	                    },
	                    success: function (response){
	                        if(parseInt(response)==1){
	                            toastr["success"]("Cambio Realizado");  
	                            if (tipo<2) {
	                            	loadtable();
	                            }
	                            if(tipo==2){
	                            	load();
	                            }
	                            if(tipo==3){
	                            	load();
	                            }
	                        }else{
	                            toastr["error"]("No se tiene permiso"); 
	                        }
	                        
	                    },
	                    error: function(response){
	                        toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema"); 
	                         
	                    }
	                }); 
        		}else{
        			toastr["error"]("Seleccione un cliente"); 
        		}
            },
            cancelar: function (){

            }
        }
    });
    setTimeout(function(){
        select2idcliente();
        new MaskedPassword(document.getElementById("pass_cambio"), '\u25CF');
    },1000);
}
function select2idcliente(){
    $('#idcliente').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        dropdownParent: $('.jconfirm'),
        placeholder: 'Buscar una cliente',
        allowClear: true,
        ajax: {
            url: base_url+'Ventasi/searchclientes',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.id,
                        text: element.empresa
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function (e) {
        var data = e.params.data;
        //obtenerrfc(data.id,0)
    });
}
function viewstatustec(){
	var html='';
	$.ajax({
        type:'POST',
        url: base_url+"index.php/AsignacionesTecnico/estatus_tecnicos",
        data: {
            id:0,
        },
        success: function (response){
        	console.log(response);
            html=response;
            $.confirm({
		        boxWidth: '40%',
		        useBootstrap: false,
		        icon: 'fa fa-warning',
		        title: 'Estatus Tecnicos',
		        content: html,
		        type: 'red',
		        typeAnimated: true,
		        buttons:{
		            confirmar: function (){

		            }
		        }
		    });
        },
        error: function(response){
            toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema");
        }
    });
}
//=========================================================
$(document).ready(function($) {
	$('.ct_close').click(function(event) {
        $('.chat_info').removeClass('active');
        $('.chat-text').show('show');
    });	
});
function consultarstatuschat(dep,per){
	//console.log('consultarstatuschat');
	$.ajax({
        type:'POST',
        url: base_url+"index.php/Rchat/coun_solicitu_chat",
        data: {
            dep:dep,
            per:per
        },
        success: function (response){
            //console.log(response);
            var array = $.parseJSON(response);
        	var num_s_chat = parseInt(array.num);
        	if(num_s_chat>0){
        		$('.indicador_chat').html('<small class="notification-badge orange accent-3">'+num_s_chat+'</small>');
        	}else{
        		$('.indicador_chat').html('');
        	}
            if(array.asig_per_actual==1){
                $('#main').loading({theme: 'dark',message: 'Tiene una solicitud de chat'})
            }else{
                $('#main').loading('stop');
            }
        },
        error: function(response){
            toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema");
        }
    });
}
function view_chat(dep,per){
	$('.chat_info').addClass('active');
    $('.chat-text').hide();
    var url =base_url+'index.php/Rchat/view/'+dep+'/'+per;
    $('.chat_body').html('<iframe src="'+url+'" class="chat_body_if">');
}
async function saveubi(){
    console.log(longitud+' -- '+latitud);
    const  permi= await verificarPermisos()
        if(permi==1){
            $.ajax({
                type:'POST',
                url: base_url+"Asistencias/rutas",
                data: {
                    longitud: longitud,
                    latitud: latitud
                },
                success: function (data){

                }
            });
        }else{
            
            $.confirm({
                boxWidth: '50%',
                useBootstrap: false,
                icon: 'fa fa-warning',
                title: 'Atención!',
                content: 'Favor de Activar tu ubicacion para continuar..',
                type: 'red',
                typeAnimated: true,
                buttons:{
                    Solicitar: function (){
                        volver_solicitar_permisos_ubicacion();
                    },
                    cancelar: function (){

                    }
                }
            });
            
        }
}
async function saveubi_ser(ser,tipo,status){
    console.log(longitud+' -- '+latitud);
    const  permi= await verificarPermisos()
        if(permi==1){
            $.ajax({
                type:'POST',
                url: base_url+"Asistencias/rutas",
                data: {
                    longitud: longitud,
                    latitud: latitud,
                    idser:ser,
                    tiposer:tipo,
                    tipo:status
                },
                success: function (data){

                }
            });
        }else{
            
            $.confirm({
                boxWidth: '50%',
                useBootstrap: false,
                icon: 'fa fa-warning',
                title: 'Atención!',
                content: 'Favor de Activar tu ubicacion para continuar..',
                type: 'red',
                typeAnimated: true,
                buttons:{
                    Solicitar: function (){
                        volver_solicitar_permisos_ubicacion();
                    },
                    cancelar: function (){

                    }
                }
            });
            
        }
}
function irfactura(cli,tipo,idvcp,tipov){
    $(location).attr('href',base_url+'Facturaslis/add?g_cli='+cli+'&g_tipo='+tipo+'&g_idvcp='+idvcp+'&g_tipov='+tipov);
}
function agregarpago_p(idvp,tipo,monto){
    $('#modal_pago_s').modal('open');
    $('.monto_prefactura_s').html(new Intl.NumberFormat('es-MX',{style: "currency", currency: "MXN"}).format(monto));
    $('#idservicio').val(idvp);
    $('#tipos').val(tipo);
    table_tipo_compra_tec(idvp,tipo);
}
function guardar_pagos_tec(){
    var form_pago = $('#form_pago_servicio');
    form_pago.validate({
            rules: 
            { 
                fecha: {
                    required: true
                },
                idmetodo: {
                    required: true
                },
                pago: {
                    required: true
                }
            },
            // Para mensajes personalizados
            messages: 
            {   
                fecha:{
                    required: "Campo obligatirio"
                },
                idmetodo:{
                    required: "Campo obligatirio"
                },
                pago:{
                    required: "Campo obligatirio"
                }
            },
        errorElement : 'div'
    });
    var valid = form_pago.valid();
    if(valid){
        //var restante =parseFloat($('.restantepoliza').html());
        var pago_p =parseFloat($('#pago_p').val());
        if (pago_p>0) {
            var datos = form_pago.serialize();
                datos=datos+'&pagtec=1';
            $.ajax({
              type: "POST",
              url: base_url+"index.php/Ventas/guardar_pago_tipo",
              data:datos,
              success: function(data){
                 alertfunction('Atención!','Guardado correctamente');
                $('#fecha_p').val('');
                $('#pago_p').val('');
                $('#observacion_p').val('');
                table_tipo_compra_tec($('#idservicio').val(),$('#tipos').val());
              }
            });
        }else{
            alertfunction('Atención!','Monto mayor a 0 y menor o igual al restante');
        }
    } 
}
function alertfunction(titlea,contenta){
    $.alert({boxWidth: '30%',useBootstrap: false,title: titlea,theme: 'bootstrap',content: contenta});  
}
function table_tipo_compra_tec(id,tipo){
    $('.text_tabla_pago').html('');
    $.ajax({
      type: "POST",
      url: base_url+"index.php/Ventas/table_get_tipo_compra",
      data:{id:id,tip:tipo},
      success: function(data){
        $('.text_tabla_pago').html(data);
        var tec = $('#tecnico').val();
        $('.view_person_'+tec).show();
      }
    });
}
function deletepagov(id,tipo){
    if(tipo<2){
        var url_delete=base_url+"index.php/Ventas/vericar_pass";
    }else{
        var url_delete=base_url+"index.php/PolizasCreadas/vericar_pass";
    }
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Desea eliminar el pago?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type: "POST",
                    url: url_delete,
                    data:{
                        pago:id,
                        tipo:tipo,
                        pass:'x1a2b3'
                    },
                    success: function(data){
                        $('#ver_pass').val('');
                        $('#modal_eliminarpago').modal('close');
                        $('#modal_pago').modal('close');
                        if(parseFloat(data)==1){
                            alertfunction('Atención!','Pago Eliminado correctamente');
                            table_tipo_compra_tec(id,tipo);
                        }else{
                            swal({ title: "Error",
                                text: "Verificar la contraseña",
                                type: 'error',
                                showCancelButton: false,
                                allowOutsideClick: false,
                            });
                        }
                    }
                });
            },
            cancelar: function (){

            }
        }
    });
}
function vacas(){
    $('#modalvaca').modal({dismissible: false});
    $('#modalvaca').modal('open');
    view_solicitudes_vaca();
}
function okvaca(){
    var valid=$('#form_vaca').valid();
    var vc_fecha_i = $('#vc_fecha_i').val();
    var vc_fecha_f = $('#vc_fecha_f').val();
    if(valid){
        $.ajax({
            type: "POST",
            url: base_url+"index.php/Generales/addvacas",
            data:{
                fini:vc_fecha_i,
                ffin:vc_fecha_f
            },
            success: function(data){
                console.log(data);
                view_solicitudes_vaca();
            }
        });
    }   
}
function view_solicitudes_vaca(){
    $.ajax({
            type: "POST",
            url: base_url+"index.php/Generales/view_sol_vacas",
            data:{
                v:0
            },
            success: function(data){
                $('.view_sol_vacas').html(data);
            }
        });
}
function deletesolvaca(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Desea confirmar la eliminación de la solicitud?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Generales/deletesolvaca",
                    data: {
                        id:id
                    },
                    success: function (data){
                        toastr.success('Solicitud Eliminada');
                        view_solicitudes_vaca();
                    }
                });
            },
            cancelar: function (){

            }
        }
    });
}
function save_token_per(idper,token){
    $.ajax({
        type:'POST',
        url: base_url+"Generales/save_token_per",
        data: {
            id:idper,token:token
        },
        success: function (data){
        }
    });
}
function verificar_nuevos_servicios_ext(){
    $.ajax({
        type:'POST',
        url: base_url+"Notificacion/verificar_nuevos_servicios_ext",
        data: {
            id:0
        },
        success: function (data){
        }
    });
}
function retirar_soleq(){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Desea retirar la solicitud de orden de compra?<br>Se necesita permisos de administrador<br>'+
                 '<input type="text" placeholder="Contraseña" id="contrasena" name="contrasena" class="name form-control-bmz" style="width:99%" required/>',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var mot=$('#motivoeliminacion').val();
                //var pass=$('#contrasena').val();
                var pass=$("input[name=contrasena]").val();
                if (pass!='') {
                    $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Sistema/solicitarpermiso",
                        data: {
                            pass:pass
                        },
                        success: function (response){
                                var respuesta = parseInt(response);
                                if (respuesta==1) {
                    //=============================================================================
                                $('.form-cot').html('');
                    //=============================================================================
                                }else{
                                    notificacion(2);
                                }
                        },
                        error: function(response){  
                             notificacion(1);
                        }
                    });
                }else{
                    notificacion(0);
                }
            },
            cancelar: function (){

            }
        }
    });
    setTimeout(function(){
        new MaskedPassword(document.getElementById("contrasena"), '\u25CF');
    },800);
}
function obtenerventaspendientes_sinnot(idcliente){
    //restriccion para cuando tenga mas de 3 prefacturas pendientes de agregar pago
    var blq_perm_ven_ser = 1;
    $.ajax({
        type:'POST',
        url: base_url+'index.php/Reportes/rventasnum/'+idcliente,
        success:function(data){
            console.log(data);
            var numventas=parseInt(data);
            //=======================
            if(blq_perm_ven_ser==1){
                var html_contect='El cliente('+idcliente+') tiene '+numventas+' pendientes de pago!';
            }else{
                var html_contect='El cliente('+idcliente+') tiene '+numventas+' pendientes de pago!<br> Para continuar se necesita permisos de administrador<br>'+
                                     '<input type="text" placeholder="Contraseña" id="contrasena" name="contrasena" class="name form-control"  onpaste="return false;" required/>';
            }
            console.log(html_contect);
        }
    });
}

async function verificarPermisos() {
    if (!navigator.permissions) {
        console.warn("La API de permisos no está disponible en este navegador.");
        return 1; // Continuar intentando acceder a la ubicación
    }

    try {
        const permiso = await navigator.permissions.query({ name: "geolocation" });
        if (permiso.state === "granted") {
            console.log("Permiso concedido para acceder a la ubicación.");
            return 1;
        } else if (permiso.state === "prompt") {
            console.log("El navegador pedirá permiso al usuario.");
            return 0; // Permitimos que se solicite el permiso
        } else {
            console.warn("Permiso denegado para acceder a la ubicación.");
            return 0; // No continuar si está denegado
        }
    } catch (error) {
        console.error("Error al verificar los permisos:", error);
        return 0;
    }
}

function view_actividad(id){
    window.open(base_url+"Control_tareas/info_ac/"+id, "Prefactura", "width=600, height=660");
}