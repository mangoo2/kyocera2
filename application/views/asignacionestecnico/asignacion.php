<?php 
  if ($perfilid==1) {
    $ocultotecnico='';
    $idpersonals=0;
  }else{
    $ocultotecnico='style="display: none;"';
    $idpersonals=$idpersonal;
  }
?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url().'public/css/asignaciontecnicos.css?v=1'.date('dhis');?>">
<style type="text/css">
  .emoji_triste{background: url(<?php echo base_url()?>public/img/triste.jpg);background-size: 100%;background-repeat: no-repeat;    background-position: center;}
  .emoji_feliz{background: url(<?php echo base_url()?>public/img/feliz.jpg);background-size: 100%;background-repeat: no-repeat;    background-position: center;}
  table .emoji_triste{background: url(<?php echo base_url()?>public/img/triste.jpg);background-size: 85%;background-repeat: no-repeat;    background-position: center;}
  table .emoji_feliz{background: url(<?php echo base_url()?>public/img/feliz.jpg);background-size: 85%;background-repeat: no-repeat;    background-position: center;}
</style>
<div class="search_series">
  <div class="inputsearch">
    <div class="b-input-group mb-3">
      <input type="text" class="form-control-bmz" id="searchserie" placeholder="Buscar una serie">
      <div class="b-input-group-append">
        <button class="b-btn b-btn-primary" type="button" onclick="searchserie()">Buscar</button>
      </div>
    </div>
  </div>
  <div class="iconsearch">
    <i class="fas fa-search"></i>
  </div>
</div>
<section id="content">
  <!--start container-->
  <div class="container">
    <div class="section">   
      <!--<div>-->
          <!--<h4 class="header">Agregar Series</h4>-->
          <div class="array_ser_vig">
            <ol class="asv_or"></ol>
          </div>
          <div  class="row">
            <div class="col s12" style="text-align: end;">
              <button data-activates="chat-out5" class="btn-bmz chat-collapse  chat-out5  divmenugroup-btn"  title="Productos" onclick="div_view_pre(1)" style="margin-left: 8px;"><i class="fas fa-tasks"></i></button>
              <button data-activates="chat-out4" class="btn-bmz chat-collapse  chat-out4  divmenugroup-btn"  title="Prefacturas" onclick="div_view_pre(0)" style="margin-left: 8px;"><i class="fas fa-file-invoice"></i></button>
              <button data-activates="chat-out3" class="btn-bmz chat-collapse  chat-out3  divmenugroup-btn"  title="Refacciones" style="margin-left: 8px;"><i class="fas fa-wrench"></i></button>
              <button class="btn-floating  botonayuda" type="button" onclick="modal_ayuda()">?</button>
              <span class="btn-notificacionstado"></span>
            </div>
          </div>
          <div class="row" ><div class="col s12 addseviciosclientes"></div></div>
          <!--<div class="row">
            <div class="col s12 m12 ">-->
              <div class="card-panel">
              <!--  <h4 class="header2">Basic Form</h4> -->
                <div class="row" style="display:none;"><input type="hidden" id="tecnico" value="<?php echo $tecnico;?>"><input type="hidden" id="tipocpe" value="<?php echo $tipocpe;?>"><input type="hidden" id="calendarinput" value="<?php echo $fechahoy;?>"></div>
                <div class="row" style="display:none;"><div class="col s12"><label for="planta_sucursal">Seleccionar cliente</label><select id="planta_sucursal" name="planta_sucursal" class="browser-default form-control-bmz" onchange="cargaequipos()"></select></div></div>
                <div class="row">
                  <div class="col s12" style="padding: 0px;">
                    <table id="tableservicioequipos" class="table">
                      <thead><tr><th></th><th>Equipo</th><th>Serie</th><th>Falla</th><th>Persona de contacto:</th><th>Horario</th><th>Dirección/ubicación</th><th></th><th></th></tr></thead>
                      <tbody class="tableservicioequipostbody"></tbody>
                    </table>
                  </div>
                  <div class="row"><div class="col s12 estatusservicio" style="margin-bottom: 15px; margin-top: 15px;min-height:36px"></div></div>
                  <div class="row"><div class="col s12 infocontadores" style="margin-bottom: 5px;min-height:36px"></div></div>
                  <div class="row"><div class="col s12 infoser" style="margin-bottom: 5px;min-height:36px"></div></div>
                  <div class="row"><div class="col s12 solref" style="margin-bottom: 5px;min-height:36px"></div></div>
                  <div class="row"><div class="col s12 solventas" style="min-height:77px"></div></div>
                </div>
              </div>
            <!--</div>     
          </div>
      <!--</div>-->
    </div> 
  </div>
</section>

<div id="modalinfoser" class="modal bottom-sheet" style="z-index: 1007; display: none; opacity: 0; bottom: -100%; max-height: 88%;">
  <div class="modal-content">
    <h4>Información de servicio</h4>
    <div class="row">
      <div class="col s12">
          <div class="row rowmarginbotom">
              <div class="col s6 textcolorred">Cliente:</div>
              <div class="col s6 m6 6 datoscliente"></div>
          </div>
          <div class="row rowmarginbotom">
              <div class="col s6 textcolorred">Tipo de Venta:</div>
              <div class="col s6 m6 6 datosclientetipo"></div>
          </div>
          <div class="row rowmarginbotom">
              <div class="col s6 textcolorred">Tipo de Servicio:</div>
              <div class="col s6 m6 6 datosclienteservicio"></div>
          </div>
          <div class="row rowmarginbotom">
              <div class="col s6 textcolorred">Zona:</div>
              <div class="col s6 m6 6 datosclientezona"></div>
              <div class="col s6 m6 6 "></div>
          </div>
          <div class="row rowmarginbotom">
              <div class="col s6 textcolorred">Horario de servicio:</div>
              <div class="col s6 m6 6 datosclienteahorario" style="font-weight: bold;font-size: 16px;"></div>
          </div>
          <div class="row">
            <div class="col s12 m12 12">
              <table class="table striped" id="tablerentapoliza">
                  <thead><tr><td>Modelo</td><td>Serie</td><td>Dirección</td></tr></thead>
                  <tbody class="tablerentapoliza"></tbody>
                </table>
            </div>
          </div>
          <div class="row rowmarginbotom">
              <div class="col s6 textcolorred">Ciudad:</div>
              <div class="col s6 m6 6"></div>
          </div>
          <div class="row rowmarginbotom">
              <div class="col s6 textcolorred">Técnico:</div>
              <div class="col s6 m6 6 datosclientetecnico"></div>
          </div>
          <div class="row rowmarginbotom">
              <div class="col s6 textcolorred">Prioridad:</div>
              <div class="col s6 m6 6 datosclienteprioridad"></div>
          </div>
          <div class="row t_dconsumibles"></div>
          <div class="row rowmarginbotom equipoacceso"></div>
          <div class="row comentariocal"></div>
          <!--
          <div class="row rowmarginbotom">
              <div class="col s6 textcolorred">Documentos y accesorios :</div>
              <div class="col s6 m6 6 "></div>
          </div>
          <div class="row rowmarginbotom">
              <div class="col s6 textcolorred">Ubicación de equipo:</div>
              <div class="col s6 m6 6 "></div>
          </div>-->
      </div>
      <div class="row motivo_texto" style="display: block;">
        <div class="col s12 m6 62">
          <h5>Motivo del servicio</h5>
          <p>Motivo del Servicio: <span class="t_motivo_serv">IMPRESORA NO IMPRIME</span></p>
          <p>Documentos y accesorios: <span class="t_doc_acces">N/A</span></p>
          <p>Equipo y/o Serie: <span class="t_doc_equipo">N/A</span></p>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col s12">
        <a class="btn waves-effect waves-light btn-bmz" onclick="cl_ser_solrefregresar()">Regresar</a>
      </div>
    </div>
  </div>
</div>
<!--
<div id="modalconsumibleservicio" class="modal">
  <div class="modal-content ">
    <h4>Escribir refaccion</h4>
    <div class="row">
      <div class="col s12 m12 12">
        <input type="hidden" id="srid">
        <input type="hidden" id="sried">
        <input type="hidden" id="srtipo">
        <input type="hidden" id="srtipod">
        <textarea id="solicitudrefaccioncpe"></textarea>
        
        <input name="prioridadr" type="radio" id="prioridadr_1" value="1" />
        <label for="prioridadr_1">Prioridad 1</label><br>
        <input name="prioridadr" type="radio" id="prioridadr_2" value="2" />
        <label for="prioridadr_2">Prioridad 2</label><br>
        <input name="prioridadr" type="radio" id="prioridadr_3" value="3" />
        <label for="prioridadr_3">Prioridad 3</label>
      </div>                
    </div>                   
  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat">Cerrar</a>
    <a href="#!" class="modal-action  waves-effect waves-green green btn-flat mconserok">Aceptar</a>
  </div>
</div>-->
<div id="modalfinservicio" class="modal" >
  <div class="modal-content " style="padding: 5px;">
    <!--<h5>Firma del cliente</h5>-->
    <h5>Finalización</h5>
    <div class="row" style="margin-bottom: 0px;">
      <div class="col s12 m12 12">
        <input type="hidden" id="fsrid">
        <input type="hidden" id="fsried">
        <input type="hidden" id="fsrtipo">
        <div class="row" style="margin-bottom: 5px;">
          <div class="col s12 m12 12">
            <h6>Resumen de servicio</h6>
          </div>
        </div>
        <div class="row" style="margin-bottom: 5px;">
          <div class="col s6 m6 6">Tipo de servicio</div>
          <div class="col s6 m6 6 fmtiposervicio"></div>
        </div>
        <div class="row" style="margin-bottom: 5px;">
          <div class="col s6 m6 6">Descripción de la falla</div>
          <div class="col s6 m6 6 fmdescripfalla"></div>
        </div>
        <div class="row" style="margin-bottom: 5px;">
          <div class="col s6 m6 6">Costo</div>
          <div class="col s6 m6 6 fmcosto"></div>
        </div>
        <div class="row adicionales" style="margin-bottom: 5px; display: none;">
          <div class="col s12 m12 12">Adicionales</div>
        </div>
        <div class="row adicionales adicionalesrow" style="margin-bottom: 5px;">
          <div class="col s1 m1 1"></div>
          <div class="col s6 m6 6"></div>
          <div class="col s5 m5 5"></div>
        </div>
        <div class="row" style="margin-bottom: 5px;">
          <div class="col s6 m6 6"></div>
          <div class="col s2 m2 2">IVA:</div>
          <div class="col s2 m2 2 fmiva"></div>
        </div>
        <div class="row" style="margin-bottom: 5px;">
          <div class="col s6 m6 6"></div>
          <div class="col s2 m2 2">Total</div>
          <div class="col s2 m2 2 fmtotal"></div>
        </div>
        <div class="row" style="margin-bottom: 5px;">
          <div class="col s6 m6 6"></div>
          <div class="col s6 m6 6 infopromedio"></div>
        </div>
        <div id="aceptance" class="row" style="margin-bottom: 0px; display: none;">
          <div id="signature" class="signature col-lg-12 col-md-12  col-sm-12 col-xs-12" style="text-align:center;margin-bottom:10px;border-radius:4px;">
            <label for="patientSignature" class="sighiddeable hidden-xs hidden-sm marginTop text-md arial_text">Dibuje su firma con su mouse o su dedo</label><br>
            <canvas id="patientSignature" class="sighiddeable hidden-xs hidden-sm" width="300" height="180" style="width: 300px; height: 180px; border: 2px dashed rgb(29, 175, 147); cursor: crosshair;"></canvas>
            <!--<img src="<?php echo base_url(); ?>/public/img/icon_borrar.png" class="sighiddeable hidden-xs hidden-sm clearSignature" >-->
            <a class="waves-effect cyan btn-bmz sighiddeable hidden-xs hidden-sm clearSignature" 
              data-signature="patientSignature" style="width: 100%">Limpiar</a>
          </div>        
        </div>

        <div class="row stock_toner_comen" style="margin-bottom: 5px;">
          <label>Informacion del stock de consumibles</label>
          <textarea id="stock_toner_comen" class="form-control-bmz" style="min-height:100px"></textarea>
        </div>     
      </div>
      </div>                
    </div>                   
  <div class="modal-footer">
    <!--<a  class="modal-action modal-close waves-effect waves-red gray btn-flat" style="float: left;">Cerrar</a>-->
    <a  class="modal-action  waves-effect waves-green green btn-flat firmacliente">Aceptar</a>
  </div>
</div>
<div id="modalfinserviciotime" class="modal" >
  <div class="modal-content " style="padding: 5px;">
    <h5>Tiempo alternativo</h5>
    <div class="row" style="margin-bottom: 0px;">
      <div class="col s12" style="padding: 0;">
        <table class="table striped table_tedit_equipos">
          <thead>
            <tr>
              <th></th>
              <th>Modelo</th>
              <th>Serie</th>
              <!--<th>Direccion</th>-->
              <th></th>
            </tr>
          </thead>
          <tbody class="tipe_equipos_info"></tbody>
        </table>
        
      </div>
      
    </div>                
  </div>                   
  <div class="modal-footer">
    <a  class="modal-action modal-close waves-effect waves-red gray btn-flat">Cerrar</a>
    <a  class="modal-action  waves-effect waves-green green btn-flat okcambiartiempos">Aceptar</a>
  </div>
</div>
<div id="modalventa" class="modal" style="z-index: 1017; display: none; opacity: 0; transform: scaleX(0.7); top: 4%; margin-left: 10px;margin-right: 10px; width: 95%;">
    <div class="modal-content " style=" padding-left: 0px; padding-right: 0px;">
      <h4> <i class="material-icons">settings_applications</i>Partes a entregar</h4>
      <hr>
      <dir class="row" style="padding: 0px">
        <div class="col s12 m12 l12">
          <ul class="collapsible collapsible-accordion" data-collapsible="accordion">
            <li class="">
              <div class="collapsible-header">
                <i class="material-icons">subtitles</i>Consumibles</div>
              <div class="collapsible-body collpadding" style="padding: 1rem;">
                <!---------------------------------------->
                  <div class="row ">
                    <table class="table striped" id="tableaddconsulmibles">
                      <thead><tr><th>Cantidad</th><th>consumible</th><th>Precio</th><th></th></tr></thead>
                      <tbody class="tabaddconsul"></tbody>
                    </table>
                  </div>
                <!---------------------------------------->
              </div>
            </li>
            <li class="">
              <div class="collapsible-header">
                <i class="material-icons">subtitles</i>Refacciones</div>
              <div class="collapsible-body collpadding" style="padding: 1rem;">
                <!----------------------------------------->
                  <div class="row ">
                    <table class="table striped" id="tableaddrefacciones">
                      <thead><tr><th>Cantidad</th><th>Refaccion</th><th>Precio</th><th></th></tr></thead>
                      <tbody class="taaddrefaccion"></tbody>
                    </table>
                  </div>
                <!----------------------------------------->
              </div>
            </li>
            <li>
              <div class="collapsible-header">
                <i class="material-icons">subtitles</i>Accesorios</div>
              <div class="collapsible-body collpadding" style="padding: 1rem;">
                <!---------------------------------------------->
                  <div class="row ">
                    <table class="table striped" id="tableaddaccesorios">
                      <thead><tr><th>Cantidad</th><th>Accesorio</th><th>Precio</th><th></th></tr></thead>
                      <tbody class="taaddaccesorios"></tbody>
                    </table>
                  </div>
                <!---------------------------------------------->
              </div>
            </li>
          </ul>
        </div>  
      </dir>
      <div class="col s12 m12 l6">
        <div class="row">
          <div class="col s12 m8 l8"></div>
          <div class="col s12 m3 l3">
            <p>Total: <b>$<span class="totalg">0.00</span></b></p>
          </div>
        </div>
      </div> 
      <div class="modal-footer">
         <!--
          <a class=" waves-effect waves-red gray btn-flat">Edicion</a>
          <a class=" waves-effect waves-red gray btn-flat">Cancelar pedido</a>-->
          <a class="modal-action modal-close waves-effect waves-red gray btn-flat">Cerrar</a>
      </div>
    </div>
</div>
<div id="modal_ayuda_color" class="modal">
    <div class="modal-content">
      <div class="row">
        <div class="col m1" style="background: #ff0000; color: #ff0000;"><b>__</b></div>  
        <div class="col m11"><label style="font-size: 20px; color: black;">Servicios Correctivos</label></div>
      </div>
      <div class="row">
        <div class="col m1" style="background: #ff9100; color: #ff9100;"><b>__</b></div>  
        <div class="col m11"><label style="font-size: 20px; color: black;">Asesoría - Conexión remota</label></div>
      </div>
      <div class="row">
        <div class="col m1" style="background: #dbca05; color: #dbca05;"><b>__</b></div>  
        <div class="col m11"><label style="font-size: 20px; color: black;">Pólizas</label></div>
      </div>
      <div class="row">
        <div class="col m1" style="background: #00a1ff; color: #00a1ff;"><b>__</b></div>  
        <div class="col m11"><label style="font-size: 20px; color: black;">Servicio Regular</label></div>
      </div>
      <div class="row">
        <div class="col m1" style="background: #038f18; color: #038f18;"><b>__</b></div>  
        <div class="col m11"><label style="font-size: 20px; color: black;">Mensajería</label></div>
      </div>        
    </div>
    <div class="row">
      <div class="col m12" align="center">
        <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
      </div>
    </div>        
</div> 
<div id="modal_seach_serie" class="modal">
  <div class="modal-content ">
    <div class="row">
      <div class="col s12">
        <h4>Buscar Serie</h4>
      </div>
    </div>
      <div class="row">
        <div class="col s12 seach_serie_tbody"></div>
      </div> 
  </div>
  <div class="modal-footer">
    <a class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
  </div>
</div>