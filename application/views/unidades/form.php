<style type="text/css">
  .has-preview, .dropify-preview{
    height: 210px !important;
  }
</style>
<!-- START CONTENT --> 
<section id="content" width="100%">
    <!--start container-->
    <div class="container" width="100%">
      <div class="section">
        <h4 class="caption"><?php echo $label; ?></h4>
          <div class="row">
            <input id="base_url" type="hidden" name="base_url" value="<?php echo base_url(); ?>">
            <!-- FORMULARIO -->
                  <div class="col s12 m12 24">

                    <div class="card-panel">

                      <div class="row">
                         <form class="form" id="form_data" method="post">
                            <input id="idreg" name="id" type="hidden" value="<?php echo $id; ?>">
                          <h4>Datos generales</h4>
                          <div class="row">
                            <div class="col s12 m3" style="height: 243px;">
                               <div class="fileinput fileinput-new" data-provides="fileinput">
                                 <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 230px; height: 210px; border-radius: 12px;">
                                <?php if ($file!=''){ ?>
                                  <input type="file" id="file" name="file" class="dropify disabledperfil" data-default-file="<?php echo base_url(); ?>uploads/unidades/<?php echo $file; ?>">
                                  <a class="btn btn-info" href="<?php echo base_url(); ?>uploads/unidades/<?php echo $file; ?>" target="_blank">visualizar</a>
                                <?php }else{?>  
                                  <input type="file" id="file" name="file" class="dropify">
                                <?php }?>
                                </div>
                               </div>
                            </div>

                             <div class="input-field col s12 m5">
                              <i class="material-icons prefix">local_library</i>
                              <input id="modelo" name="modelo" type="text" value="<?php echo $modelo; ?>" class="disabledperfil">
                              <label class="active">Modelo</label>
                             </div>
                             <div class="input-field col s12 m4">
                              <i class="material-icons prefix">local_library</i>
                              <input id="anio" name="anio" type="number" value="<?php echo $anio; ?>" class="disabledperfil">
                              <label>Año</label>
                             </div>
                             <br>
                             <div class="input-field col s12 m5">
                              <i class="material-icons prefix">local_library</i>
                              <input id="marca" name="marca" type="text" value="<?php echo $marca; ?>" class="disabledperfil">
                              <label>Marca</label>
                             </div>
                             <div class="input-field col s12 m4">
                              <i class="material-icons prefix">local_library</i>
                              <input id="num_serie" name="num_serie" type="text" value="<?php echo $num_serie; ?>" class="disabledperfil">
                              <label>número de serie</label>
                             </div>
                             <br>
                             <div class="input-field col s12 m5">
                              <i class="material-icons prefix">local_library</i>
                              <input id="poliza_seguro" name="poliza_seguro" type="text" value="<?php echo $poliza_seguro; ?>" class="disabledperfil">
                              <label>Póliza de seguro</label>
                             </div>
                             <div class="input-field col s12 m4">
                              <i class="material-icons prefix">local_library</i>
                              <input id="kilometraje" name="kilometraje" type="number" value="<?php echo $kilometraje; ?>">
                              <label>kilometraje</label>
                             </div>
                             <br>
                             <div class="input-field col s12 m4">
                                <i class="material-icons prefix">event</i>
                                <input id="proximo_servicio" name="proximo_servicio" type="date"  value="<?php echo $proximo_servicio; ?>">
                                <label for="proximo_servicio" class="active">Fecha de próximo servicio</label>
                             </div>
                             <div class="input-field col s12 m4">
                                <i class="material-icons prefix">event</i>
                                <input id="vigencia_poliza_seguro" name="vigencia_poliza_seguro" type="date"  value="<?php echo $vigencia_poliza_seguro; ?>" class="disabledperfil">
                                <label for="vigencia_poliza_seguro" class="active">Vigencia de póliza de seguro</label>
                             </div>
                             <div class="input-field col s12 m4">
                              <i class="material-icons prefix">local_library</i>
                              <input id="placas" name="placas" type="text" value="<?php echo $placas; ?>" class="disabledperfil">
                              <label>Placas</label>
                             </div>
                             <br>
                             <div class="col s12 m3">
                              <i class="material-icons prefix">local_library</i>
                              <label for="tipo_unidad" class="active" style="top: 0px !important; font-size: 15px !important;">Tipo de unidad</label>
                              <select id="tipo_unidad" name="tipo_unidad" class="form-control-bmz browser-default disabledperfil" onchange="tipo_unidad_slet()" >
                                  <option value="" disabled selected>Selecciona una opción</option>
                                  <option value="1" <?php if($tipo_unidad==1) echo "selected"; ?>>Unidad propia</option>
                                  <option value="2" <?php if($tipo_unidad==2) echo "selected"; ?>>Leasing</option>
                              </select>
                              
                             </div>
                             <div class="col s12 m3">
                              
                              <label for="tecnico_asignado" class="active" style="top: 0px !important; font-size: 15px !important;">Tecnico asignado</label>
                              <select id="tecnico_asignado" name="tecnico_asignado" class="form-control-bmz browser-default"  >
                                  <option value="0"></option>
                                  <?php foreach ($resultstec as $item) { ?>
                                    <option value="<?php echo $item->personalId;?>" <?php if($tecnico_asignado==$item->personalId){ echo 'selected';} ?>  ><?php echo $item->tecnico;?></option>
                                  <?php  } ?>
                              </select>
                              <select class="form-control-bmz browser-default tecnico_asignado2" style="display:none;" >
                                  <?php foreach ($resultstec as $item) { 
                                      if($tecnico_asignado==$item->personalId){ 
                                    ?>
                                    <option value="<?php echo $item->personalId;?>"  ><?php echo $item->tecnico;?></option>
                                  <?php  }
                                    } ?>
                              </select>
                              
                             </div>
                             <div class="input-field col s5" style="display: none">
                              <i class="material-icons prefix">local_library</i>
                              <input id="tipo_unidad_texto" name="tipo_unidad_texto" type="text" value="<?php echo $tipo_unidad_texto; ?>">
                              <label>Leasing</label>
                             </div>
                             <div class="input-field col s4 leasing_txt" style="display: none">
                                <i class="material-icons prefix">event</i>
                                <input id="vigencia_contrato" name="vigencia_contrato" type="date"  value="<?php echo $vigencia_contrato; ?>">
                                <label for="vigencia_contrato" class="active">Vigencia de contrato</label>
                             </div>
                             <br>
                             
                          </div>
                          <br>
                          <h4>Archivos de unidad</h4>
                          <div class="row">
                            <div class="input-field col s3">
                               <label for="seguro" class="active">Póliza de seguro</label><br>
                               <div class="fileinput fileinput-new" data-provides="fileinput">
                                 <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 230px; height: 210px; border-radius: 12px;">
                                <?php if ($file_poliza!=''){ ?>
                                  <input type="file" id="file_poliza" name="file_poliza" class="dropifyunidad disabledperfil" data-default-file="<?php echo base_url(); ?>uploads/unidades_poliza/<?php echo $file_poliza; ?>">
                                  <a class="btn btn-info" href="<?php echo base_url(); ?>uploads/unidades_poliza/<?php echo $file_poliza; ?>" target="_blank">visualizar</a>
                                <?php }else{?>  
                                  <input type="file" id="file_poliza" name="file_poliza" class="dropifyunidad">
                                <?php }?>
                                </div>
                               </div>
                            </div>
                          </div>
                         </form>

                        <br><br><br>
                          <div class="row">
                            <div class="input-field col s12">
                              <button class="btn cyan waves-effect waves-light right add_registro" type="button" onclick="add_registro()">Guardar
                                <i class="material-icons right">send</i>
                              </button>
                            </div>
                          </div>
                        
                        
                      </div>

                    </div>

                  </div>
                
              </div>
            </div>
          </div>
        </section>