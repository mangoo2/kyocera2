<?php
require_once dirname(__FILE__) . '/TCPDF3/examples/tcpdf_include.php'; 
require_once dirname(__FILE__) . '/TCPDF3/tcpdf.php';

$GLOBALS["nombreUsuario"]=$nombreUsuario;
$GLOBALS["emailUsuario"]=$emailUsuario;
$idCotizacion='';
$GLOBALS["fechareg"]='';

$GLOBALS["empresa"]=$datosCliente->empresa;

$GLOBALS["tipov"]=1;
$GLOBALS["tipovlogo"]=$configuracionCotizacion->logo1;
$GLOBALS["tipovtextoHeader"]=$configuracionCotizacion->textoHeader;
$infog='';
if(isset($_GET['info'])){
	$conseptos=json_decode($_GET['info']);
	if(count($conseptos)==1){
		foreach ($conseptos as $item) {
			$idCotizacion=$item->preid;
			$GLOBALS["fechareg"]=$item->periodo;
			$infog='CLICKS EXCEDENTES PERIODO DE PRODUCCION DEL<BR>'.$item->periodo;
		}
	}
	
}
if($idCotizacion>0){
	$idCotizacionl='# '.$idCotizacion;	
}else{
	$idCotizacionl='';
}
$GLOBALS["idCotizacion"]=$idCotizacionl;
//
class MYPDF extends TCPDF {
  //Page header
  public function Header() {

      $img_file1=base_url().'public/img/cotheader1.png';
      $img_file2=base_url().'public/img/cotheader2.png';
      $img_file3=base_url().'public/img/cotheader3.png';
      if($GLOBALS["tipov"]==2){
        $img_file3=base_url().'app-assets/images/'.$GLOBALS["tipovlogo"];
      }                      
      $this->Image($img_file1, 0, 0, 0, 0, '', '', 10, false, 310, '', false, false, 0);
      $this->Image($img_file2, 1, 3, 70, 0, '', '', 10, false, 310, '', false, false, 0);
      $this->Image($img_file3, 160, 3, 40, 0, '', '', 10, false, 310, '', false, false, 0);

      $html = '<style type="text/css" class="print_css">
                  .titulocoti{
                    font-style: italic;
                    font-size: 13px;
                    color:#2e4064;
                  }
                  .numbercoti{
                    font-size: 14px;
                    color:#2e4064;
                    font-weight:bold;

                  }
                  .colorblue{
                    color:#2e4064;
                  }
              </style>';
      $html.= '<table border="0">
                  <tr>
                    <td width="34%"></td>
                    <td width="39%" class="titulocoti" align="center">'.$GLOBALS["tipovtextoHeader"].'<p></p></td>
                    <td width="29%"></td>
                  </tr>
                  <tr>
                    <td></td>
                    <td></td>
                    <td class="numbercoti">
                      <table>
                        <tr>
                          <td></td>
                          <td height="20px;"></td>
                        </tr>
                        <tr>
                          <td colspan="2" align="center">'.$GLOBALS["idCotizacion"].'</td>
                        </tr>
                        <tr>
                          <td colspan="2" align="center">'.$GLOBALS["fechareg"].'</td>
                        </tr>
                      </table>
                      </td>
                  </tr>
                </table>
                <table border="0">
                  <tr>
                    <td></td>
                  </tr>
                  <tr>
                    <td></td>
                  </tr>
                </table>
                
                ';      
      $this->writeHTML($html, true, false, true, false, '');
  }

    // Page footer
  public function Footer() {
    $html = '';
      $img_file=base_url().'public/img/cot_footer.jpg';
                            //x,y    w,h
      $this->Image($img_file, 0, 255, 0, 0, '', '', 10, false, 310, '', false, false, 0);
      $html .= '<table border="0">
                  <tr><td></td><td></td><td ></td></tr>
                  <tr><td></td><td></td><td ></td></tr>
                  <tr><td></td><td></td><td ></td></tr>
                  <tr><td></td><td></td><td ></td></tr>
                  <tr>
                    <td></td>
                    <td></td>
                    <td style="font-weight:bold;color:white;text-align:center;">Le atiende:<br>'.$GLOBALS["nombreUsuario"].'<br>'.$GLOBALS["emailUsuario"].'</td>
                  </tr>
                  <tr>
                    <td></td>
                    <td></td>
                    <td style="font-weight:bold;color:white; text-align:center" align="center">Pagina  '.$this->getAliasNumPage().'/'.$this->getAliasNbPages().'</td>
                  </tr>
                </table>';
      $this->writeHTML($html, true, false, true, false, '');
  }
} 
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('kyocera');
$pdf->SetTitle('Cotización '.$idCotizacion);
$pdf->SetSubject('Cotización');
$pdf->SetKeywords('Cotización');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins('8', '45', '8');
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin("45");

// set auto page breaks
$pdf->SetAutoPageBreak(true, '45');

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//$pdf->SetFont('dejavusans', '', 9);
// add a page
$pdf->AddPage('P', 'A4');
$col1='20';
$col2='10';
$col3='11';
$col4='47';
$col5='12';

$html='<style type="text/css" class="print_css">
          .div{
            border: 1px solid black;
            width: 100px;
          }
          .tamanotext{
            font-size:11px !important;
          }
          .titulositems{
            color:red;
            font-size:14px;
          }
          .titulositems2{font-size:12px;}
          .titulositems4{font-size:14px;}
          .trborderbuttom{
            border-bottom:2px sold red;
          }
          .trbordertop{
            border-top:1px sold #eaa2a2;
          }
          .tableitems{
            color:#2e4064;
          }
          .colorblue{color:#2e4064;}

          .t_c{text-align:center;}
          .httable{
              font-size: 10px;
            }
       </style>';
                $html.='<table border="0">';
                  $html.='<tr>';
                    $html.='<td width="25%" class="colorblue titulositems2" style="font-weight:bold">Empresa:</td>';
                    $html.='<td width="75%" class="colorblue titulositems2">'.$GLOBALS["empresa"].'</td>';
                  $html.='</tr>';
                  $html.='<tr>';
                    $html.='<td colspan="2"></td>';
                  $html.='</tr>';
                  $html.='<tr>';
                    $html.='<td colspan="2" class="colorblue titulositems4 t_c" style="font-weight:bold">Cotización de excedentes</td>';
                  $html.='</tr>';
                  $html.='<tr><td colspan="2"></td></tr>';
                $html.='</table>';

                $html.='<table border="1" cellpadding="2">';
                  $html.='<thead>';
                    $html.='<tr>';
                      $html.='<th style="background-color: #d8d4d4; color: black" align="center" class="httable" width="15%">CANTIDAD</th>';
                      $html.='<th style="background-color: #d8d4d4; color: black" align="center" class="httable" width="45%">DESCRIPCIÓN</th>';
                      $html.='<th style="background-color: #d8d4d4; color: black" align="center" class="httable" width="20%">PRECIO UNITARIO</th>';
                      $html.='<th style="background-color: #d8d4d4; color: black" align="center" class="httable" width="20%">TOTAL</th>';
                    $html.='</tr>';
                  $html.='</thead>';
                  $html.='<tbody>';
                  	
                  	$subtotalgeneral=0;
                  	if(isset($_GET['info'])){
						$conseptos=json_decode($_GET['info']);
						$idcontrato=0;
						foreach ($conseptos as $item) {
							
							if($item->tipo==1){
								$des ='CLICKS EXCEDENTES DEL '.$item->periodo;
							}else{
								$des ='CLICKS EXCEDENTES COLOR DEL '.$item->periodo;
							}

							if($item->exc>0 && $item->pexc>0){
								$total_per=$item->exc*$item->pexc;
								$subtotalgeneral+=$total_per;
								if($idcontrato!=$item->con){
									$idcontrato=$item->con;
									$html.='<tr><td colspan="4" align="center" class="httable"><b>'.$item->conf.'</b></td></tr>';
								}
								$html.='<tr>';
			                      $html.='<td  align="center" class="httable" width="15%">'.$item->exc.'</td>';
			                      $html.='<td  align="center" class="httable" width="45%">'.$des.'</td>';
			                      $html.='<td  align="center" class="httable" width="20%">$ '.number_format(($item->pexc),2,'.',',').'</td>';
			                      $html.='<td  align="center" class="httable" width="20%">$ '.number_format(($total_per),2,'.',',').'</td>';
			                    $html.='</tr>';
							}
						}

						$iva=round($subtotalgeneral*0.16,2);
                        $totalgeneral=$subtotalgeneral+$iva;
                        $html.='<tr>';
                          $html.='<td class="httable" align="center" rowspan="3" colspan="2">'.$infog.'</td>';
                          $html.='<td class="httable" align="center">Subtotal</td>';
                          $html.='<td class="httable" align="center">$ '.number_format(($subtotalgeneral),2,'.',',').'</td>';
                        $html.='</tr>';
                        $html.='<tr>';
                          $html.='<td class="httable" align="center">IVA</td>';
                          $html.='<td class="httable" align="center">$ '.number_format(($iva),2,'.',',').'</td>';
                        $html.='</tr>';
                        $html.='<tr>';
                          $html.='<td class="httable" align="center">TOTAL</td>';
                          $html.='<td class="httable" align="center">$ '.number_format(($totalgeneral),2,'.',',').'</td>';
                        $html.='</tr>';
					}
					
                    
                  $html.='</tbody>';
                  
                $html.='</table>';

          //log_message('error',$html);     
//================================================
//echo $html;
//var_dump($html);
          //log_message('error',$html);
$pdf->writeHTML($html, true, false, true, false, '');
//$pdf->Output('Captura.pdf', 'I');
$url=FCPATH.'/doccotizacion/';
//$url=$_SERVER['DOCUMENT_ROOT'].'/hulesyg/facturaspdf/';
$pdf->Output($url.'cotizacion_'.$idCotizacion.'.pdf', 'FI');

?>


?>