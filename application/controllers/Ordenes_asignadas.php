<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ordenes_asignadas extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->library('menus');
        $this->load->helper('url');
        $this->load->model('ModeloCatalogos');
        $this->load->model('Configuraciones_model');
        $this->load->model('ConfiguracionDocumentos_Model');
        $this->load->model('Clientes_model');
        $this->submenu=35;
        if($this->session->userdata('logeado')==true){
        }else{
            redirect('login');
        }
    }

    // Listado de Ordenes_asignadas
    function index(){ 
        $data['MenusubId']=$this->submenu;
        $menus = $this->menus->getMenus($this->session->userdata('perfilid'));
        $data['menus'] = $menus['menus'];
        $data['submenusArray'] = $menus['submenusArray'];
        $dd['serv'] = $this->Configuraciones_model->getDataServ();
        $dd['bodegasrow']=$this->ModeloCatalogos->getselectwherenall('bodegas',array('activo'=>1));
        $dd['equiposrow']=$this->ModeloCatalogos->getselectwherenall('equipos',array('estatus'=>1));

        $this->load->view('header');
        $this->load->view('main',$data);
        $this->load->view('Servicio/Ordenes_asignadas',$dd);
        $this->load->view('footer');
        $this->load->view('Servicio/Ordenes_asignadas_js');
    }

    public function nuevaOrden(){
        $data['configuracionCotizacion'] = $this->ConfiguracionDocumentos_Model->getConfiguracion(1);

        $where_personal=array('personalId'=>$_SESSION['idpersonal']);
        $resultadop = $this->ModeloCatalogos->getselectwheren('personal',$where_personal);
        $personal_nombre='';
        $personal_apellidop='';
        $personal_apellidom='';
        foreach ($resultadop->result() as $item) {
            $personal_nombre= $item->nombre;
            $personal_apellidop= $item->apellido_paterno;
            $personal_apellidom= $item->apellido_materno;
        }
        $data['ini_personal']=substr($personal_nombre, 0, 1).substr($personal_apellidop, 0, 1).substr($personal_apellidom, 0, 1);
        $data['rfc_datos']=$this->Clientes_model->getListadoDatosFiscales(0); 

        $data['metodopagorow']=$this->ModeloCatalogos->getselectwheren('f_metodopago',array('activo'=>1));
        $data['formapagorow']=$this->ModeloCatalogos->getselectwheren('f_formapago',array('activo'=>1));
        $data['cfdirow']=$this->ModeloCatalogos->getselectwheren('f_uso_cfdi',array('activo'=>1));

        $data['resultadoclipcontacto'] = $this->ModeloCatalogos->getselectwheren('cliente_has_persona_contacto','idCliente!=0 and status=1');
        //$data['serviciodetalles'] = $this->ModeloCatalogos->detalles_servicios_(1);
        $data['block_button']=1;
        $this->load->view('Servicio/orden_servicio',$data);
    }

    public function iniciar($id){
        echo json_encode("ok");
    }

    public function save(){
        $data = $this->input->post();
        $id=$data['ventaId'];
        $data['tipo']=1;
        $this->ModeloCatalogos->Insert('orden_servicio',$data);
        $dataup['prefactura']=1;
        $this->ModeloCatalogos->updateCatalogo('orden_servicio',$dataup,array('id' => $id));
    }
    
}

?>