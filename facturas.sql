-- phpMyAdmin SQL Dump
-- version 3.4.11.1
-- http://www.phpmyadmin.net
--
-- Servidor: mysql1002.mochahost.com
-- Tiempo de generación: 22-11-2019 a las 18:15:19
-- Versión del servidor: 5.6.29
-- Versión de PHP: 7.2.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `mangoo_facturacion33`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `facturas`
--

CREATE TABLE IF NOT EXISTS `facturas` (
  `FacturasId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Referencia` varchar(30) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `Rfc` varchar(45) DEFAULT NULL,
  `Nombre` varchar(45) DEFAULT NULL,
  `Direccion` varchar(500) DEFAULT NULL,
  `Cp` varchar(45) DEFAULT NULL,
  `Folio` int(10) DEFAULT NULL,
  `Estado` int(11) DEFAULT '1',
  `LeyendaAsignacion` varchar(550) DEFAULT NULL,
  `Lote` varchar(45) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `Caducidad` varchar(45) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `Paciente` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `LeyendaPagare` varchar(1000) DEFAULT NULL,
  `PaisReceptor` varchar(45) DEFAULT NULL,
  `TipoComprobante` varchar(45) DEFAULT NULL,
  `FormaPago` varchar(45) DEFAULT NULL,
  `MetodoPago` varchar(45) DEFAULT NULL,
  `rutaXml` varchar(100) DEFAULT NULL,
  `Clientes_ClientesId` int(10) unsigned DEFAULT NULL,
  `usuario_id` int(10) unsigned DEFAULT NULL,
  `creada_sesionId` int(10) NOT NULL,
  `cadenaoriginal` longtext NOT NULL,
  `sellocadena` longtext NOT NULL,
  `sellosat` longtext NOT NULL,
  `uuid` varchar(200) NOT NULL,
  `fechatimbre` datetime NOT NULL,
  `subtotal` decimal(12,2) NOT NULL,
  `iva` decimal(10,4) NOT NULL,
  `total` decimal(12,4) NOT NULL,
  `certificado` longtext NOT NULL,
  `nocertificado` longtext NOT NULL,
  `nocertificadosat` longtext NOT NULL,
  `tarjeta` varchar(4) NOT NULL,
  `fechacancelacion` datetime NOT NULL,
  `sellocancelacion` longtext NOT NULL,
  `ordenCompra` varchar(50) NOT NULL,
  `moneda` varchar(50) NOT NULL,
  `observaciones` text NOT NULL,
  `comprobado` tinyint(4) NOT NULL DEFAULT '0',
  `rutaAcuseCancelacion` varchar(200) DEFAULT NULL,
  `uso_cfdi` varchar(4) NOT NULL,
  `numproveedor` varchar(15) NOT NULL,
  `numordencompra` varchar(15) NOT NULL,
  `honorario` decimal(10,4) NOT NULL,
  `isr` decimal(10,4) NOT NULL,
  `ivaretenido` decimal(10,4) NOT NULL,
  `cedular` decimal(10,4) NOT NULL,
  PRIMARY KEY (`FacturasId`),
  KEY `fk_clientes_facturas_idx` (`Clientes_ClientesId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=115985 ;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `facturas`
--
ALTER TABLE `facturas`
  ADD CONSTRAINT `fk_clientes_facturas` FOREIGN KEY (`Clientes_ClientesId`) REFERENCES `clientes` (`ClientesId`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
