var base_url = $('#base_url').val();
var table_equipo;
$(document).ready(function() {
    table_equipo=$("#tabla_bitacora").DataTable();
    datos_consumibles();
    $('#bodegaId').change(function() {
        datos_consumibles()    
    });
});

function datos_consumibles(){
    table_equipo.destroy();
    table_equipo = $('#tabla_bitacora').DataTable({
        fixedHeader: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            url: base_url + "index.php/Bitacora/getdata",
            type: "post",
            error: function() {
                $("#tabla_bitacora").css("display", "none");
            }
        },
        "columns": [
            {"data": "bitacoraId"},
            {"data": "contenido"},
            {"data": "idtable"},
            {"data": "tipo"},
            {"data": "nombre"},
            {"data": "reg"},
           
          ],
        order: [
            [0, "desc"]
          ]
        
    });

}
