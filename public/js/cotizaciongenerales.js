var base_url=$('#base_url').val();
function editarseriepol(id,tipo){
	var html='<label>Se necesita permisos de administrador</label><br>\
				<input id="password_ns" type="password" class="validate form-control" autocomplete="new-password" placeholder="Contraseña"><br>\
				<label>Nueva serie</label><br>\
				<input id="new_serie_ns" type="text" class="form-control">';
	$.confirm({
        boxWidth: '50%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Editar serie',
        content: html,
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var pass=$('#password_ns').val();
                var serie=$('#new_serie_ns').val();
                if (pass!='') {
                    //if (precio>0) {
                        $.ajax({
                            type:'POST',
                            url: base_url+'Generales/editarserie_cot',
                            data: {
		                        id: id,
		                        pass: pass,
		                        tipo: tipo,
		                        serie:serie
                                },
                                async: false,
                                statusCode:{
                                    404: function(data){
                                        swal("Error", "404", "error");
                                    },
                                    500: function(){
                                        swal("Error", "500", "error"); 
                                    }
                                },
                                success:function(data){
                                  if (data==1) {
                                    swal("Hecho!", "Realizado", "success");
                                    setTimeout(function(){ window.location.href=''; }, 3000);
                                  }else{
                                    swal("Error", "No tiene permiso", "error"); 
                                  }

                                }
                            });
                    //}else{
                    //    swal("Error", "El precio no debe de ser menor o igual a cero", "error"); 
                    //}
                }else{
                    swal("Error", "Debe de ingresar una contraseña", "error"); 
                }  
            },
            cancelar: function (){
            }
        }
    });
}
function edit_cont(idc){
    var atencionselect_xx = $('#atencionselect_xx').html();
    var ate=$('.info_contacto').data('atencionpara');
    var mail= $('.info_contacto').data('correo');
    var tel=$('.info_contacto').data('tel');

    var html='<label>Desea editar los datos?</label>';
                html+='<div class="row">';
                    html+='<div class="col s5">';
                        html+='<label>Seleccione</label>';
                        html+='<select id="atencionselect" class="browser-default form-control-bmz"  onchange="obtenercontactosss()">';
                        html+=atencionselect_xx;
                        html+='</select>';
                    html+='</div>';
                html+='</div>';
                html+='<div class="row">';
                    html+='<div class="col s4">';
                        html+='<label>Atencion Para</label>';
                        html+='<input class="form-control-bmz" id="atencion" name="atencion" value="'+ate+'">';
                    html+='</div>';
                    html+='<div class="col s4">';
                        html+='<label>Correo</label>';
                        html+='<input class="form-control-bmz" id="correo" name="correo" value="'+mail+'">';
                    html+='</div>';
                    html+='<div class="col s4">';
                        html+='<label>Telefono</label>';
                        html+='<input class="form-control-bmz" id="telefono" name="telefono" value="'+tel+'">';
                    html+='</div>';
                html+='</div>';
    $.confirm({
        boxWidth: '50%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Editar',
        content: html,
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var atencion=$('#atencion').val();
                var correo=$('#correo').val();
                var telefono=$('#telefono').val();
                    //if (precio>0) {
                        $.ajax({
                            type:'POST',
                            url: base_url+'Cotizaciones/edit_cont',
                            data: {
                                idc: idc,
                                atencion: atencion,
                                correo: correo,
                                telefono:telefono
                                },
                                async: false,
                                statusCode:{
                                    404: function(data){
                                        swal("Error", "404", "error");
                                    },
                                    500: function(){
                                        swal("Error", "500", "error"); 
                                    }
                                },
                                success:function(data){
                                    swal("Hecho!", "Realizado", "success");
                                    setTimeout(function(){ window.location.href=''; }, 3000);
                                  

                                }
                            });
                    //}else{
                    //    swal("Error", "El precio no debe de ser menor o igual a cero", "error"); 
                    //}
                 
            },
            cancelar: function (){
            }
        }
    });
}
function obtenercontactosss(){
    var atencionpara = $('#atencionselect option:selected').data('atencionpara');
    var email = $('#atencionselect option:selected').data('email');
    var telefono = $('#atencionselect option:selected').data('telefono');
    var celular = $('#atencionselect option:selected').data('celular');
    if(telefono.length==0){
        telefono=celular;
    }
    //$('#atencion').show("slow");
    $('#atencion').val(atencionpara);
    $('#correo').val(email);
    $('#telefono').val(telefono);
}