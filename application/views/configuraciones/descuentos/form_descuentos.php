<section id="content" width="100%">
          

          <!--start container-->
          <div class="container" width="100%">
            <div class="section">
              <div class="row">
                <p class="caption col s6">Configuración de Descuentos.</p>
                
              </div>

              <div class="row" >
                <input id="base_url" type="hidden" name="base_url" value="<?php echo base_url(); ?>">
                
                <!-- FORMULARIO -->
                <form class="col s12 " id="descuentos-form" method="post">

                  <div class="col s12 m12 24" >

                    <div class="card-panel" align="center">
                      <!-- PORCENTAJES DE UTILIDAD PARA VENTA -->
                      <div class="row">
                        <div class="row col s12" >
                          
                          <div class="input-field col s2">
                            <label>Porcentajes de Descuentos Equipos</label>
                          </div>
                          <div class="input-field col s4">
                            <i class="material-icons prefix"> % </i>
                            <input id="porcentaje_descuentos_equipos" name="porcentaje_descuentos_equipos" type="text" placeholder="Equipos" value="<?php echo $porcentajes[0]->descuento; ?>">
                          </div>                          

                          <div class="input-field col s2">
                            <label>Porcentajes de Ganancia Refacciones</label>
                          </div>
                          <div class="input-field col s4">
                            <i class="material-icons prefix">% </i>
                            <input id="porcentaje_descuentos_refacciones" name="porcentaje_descuentos_refacciones" type="text" placeholder="Refacciones" value="<?php echo $porcentajes[1]->descuento; ?>">
                          </div>  

                        </div>

                        <div class="row col s12" >

                          <div class="input-field col s2">
                            <label>Porcentajes de Ganancia Consumibles</label>
                          </div>
                          <div class="input-field col s4">
                            <i class="material-icons prefix">% </i>
                            <input id="porcentaje_descuentos_consumibles" name="porcentaje_descuentos_consumibles" type="text" placeholder="Consumibles" value="<?php echo $porcentajes[2]->descuento; ?>">
                          </div>                          

                          <div class="input-field col s2">
                            <label>Porcentajes de Ganancia Accesorios</label>
                          </div>
                          <div class="input-field col s4">
                            <i class="material-icons prefix">% </i>
                            <input id="porcentaje_descuentos_accesorios" name="porcentaje_descuentos_accesorios" type="text" placeholder="Accesorios" value="<?php echo $porcentajes[3]->descuento; ?>">
                          </div>

                        </div>

                      </div>
                      <!-- PORCENTAJES DE UTILIDAD PARA VENTA -->

                      

                      <div class="row">
                        <div class="row" >
                          <div class="input-field col s2">
                            <button id="button" name="actualiza_porcentajes_ganancia" class="btn waves-effect waves-light green darken-1"> Actualizar</button> 
                          </div>
                        </div>
                      </div>

                    </div>

                  </div>

                </form>
              </div>

          </div>
        </div>
      </section>