<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="icon" href="<?php echo base_url(); ?>app-assets/images/favicon/favicon_kyocera.png" sizes="32x32">
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<title>Prefactura</title>
		<!-- jQuery Library -->
    	<script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/jquery-3.3.1.min.js"></script>
    	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <link href="<?php echo base_url(); ?>app-assets/vendors/sweetalert/dist/sweetalert.css" type="text/css" rel="stylesheet">
        <!--
        <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/js/ckeditor/ckeditor.js"></script>
    	<script type="text/javascript" src="<?php echo base_url(); ?>app-assets/js/ckeditor/adapters/jquery.js"></script>-->
		<style type="text/css">
			.tdstitle{
				background: #c1bcbc;
				font-weight: bold;
				color: white;
			}
			html{
			        -webkit-print-color-adjust: exact;
			}
			input{
				
				padding: 0px !important;
				height: 28px !important;
			}
			select{
				
				padding: 0px !important;
				height: 28px !important;
				
				 font-size: 11px !important;
			}
			@media print{
				input{
					background: transparent;
					border: 0 !important;
					
				}
				.buttonenvio{
					display: none;
				}
				html{
			        -webkit-print-color-adjust: exact;
				}
				.tdstitle{
					background: #c1bcbc;
					font-weight: bold;
					color: white;
				}
				.buttoimprimir{
					display: none;
				}
				select{
					background: transparent;
					border: 0 !important;
					-moz-appearance: none;
					 -webkit-appearance: none;
					 appearance: none;
				}
				.form-control:disabled, .form-control[readonly] {
				    background-color: transparent;
				    opacity: 1;
				}
			}
		</style>
	</head>
	<body>
		<input type="hidden" id="base_url" name="base_url" value="<?php echo base_url(); ?>">
		<form method="post" id="formprefactura">
			<input type="hidden" id="ventaId" name="ventaId" value="<?php echo $ventaId;?>">
			<table border="1" width="100%">
				<tr>
					<td>
						<img src="<?php echo base_url(); ?>app-assets/images/<?php echo $configuracionCotizacion->logo1; ?>">
					</td>
					<td>
						<?php echo $configuracionCotizacion->textoHeader; ?>
					</td>
					<td>
						<img class="" src="<?php echo base_url(); ?>app-assets/images/1024_kyocera_logo_mds.png" alt="materialize logo"  style="max-width:200px; width: 100%;">
					</td>
				</tr>
			</table>
			<table border="1" width="100%">
				<tr>
					<td class="tdstitle" style="width: 10%;">FECHA</td>
					<td style="width: 5%;"><?php echo $dia_reg;?></td>
					<td style="width: 5%;"><?php echo $mes_reg;?></td>
					<td style="width: 5%;"><?php echo $ano_reg;?></td>
					<td class="tdstitle" style="width: 5%">AC</td>
					<td style="width: 10%;"><?php echo $ini_personal;?></td>
					<td class="tdstitle" style="width: 20%">PROINVEN No.</td>
					<td style="width: 10%;"><?php echo $proinven;?></td>
					<td class="tdstitle" style="width: 10%">CDF</td>
					<td style="width: 20%"><?php echo $cfd;?></td>
				</tr>
			</table>
			<table border="1" width="100%">
				<tr>
					<td style="width: 5%;">Cia.</td>
					<td style="width: 80%;" colspan="5"><output id="razon_social"></output></td>
					<td style="width: 5%;">RFC</td>
					<td style="width: 10%;">
						<select class="form-control" id="rfc_id" name="rfc_id" <?php echo $rfc_id_button;?> onchange="selecrfcp()">>
						    <option value="">Seleccione</option>
						<?php foreach ($rfc_datos as $item) { ?>
								<option value="<?php echo $item->id; ?>" <?php if($item->id==$rfc_id){ echo 'selected';}?> ><?php echo $item->rfc; ?></option>
						<?php } ?>
					    </select>	
					</td>
				</tr>
				<tr>
					<td style="width: 5%; font-size: 9px">VENCE</td>
					<td style="width: 12%;">
						<input type="date" id="vencimiento" name="vencimiento" class="form-control" value="<?php echo $vence;?>" <?php echo $vence_block;?> style="max-width: 138px;"></td>
					<td style="width: 10%; font-size: 11px; text-align: center;">METODO DE PAGO</td>
					<td style="width: 20%; font-size: 11px; text-align: center;">
					<select class="form-control" id="metodopagoId" name="metodopagoId" <?php echo $metodopagoId_block;?> >
						<?php foreach ($metodopagorow->result() as $item) { ?>
								<option value="<?php echo $item->id; ?>" <?php if($item->id==$metodopagoId){ echo 'selected';} ?>    ><?php echo $item->metodopago_text; ?></option>
						<?php } ?>
					</select>
					</td>
					<td style="width: 12%; font-size: 11px; text-align: center;">FORMA DE PAGO</td>
					<td style="width: 14%; font-size: 11px; text-align: center;">
						<select class="form-control" id="formapagoId" name="formapagoId" <?php echo $formapago_block;?> >
						<?php foreach ($formapagorow->result() as $item) { ?>
								<option value="<?php echo $item->id; ?>" <?php if($item->id==$formapago){ echo 'selected';} ?>    ><?php echo $item->formapago_text; ?></option>
						<?php } ?>
					</select>
					</td>
					<td style="width: 10%; font-size: 11px;">USO DE CFDI</td>
					<td style="width: 17%; font-size: 11px;">
						<select class="form-control" id="usocfdiId" name="usocfdiId" <?php echo $cfdi_block;?>>
							<?php foreach ($cfdirow->result() as $item) { ?>
									<option value="<?php echo $item->id; ?>" <?php if($item->id==$cfdi){ echo 'selected';} ?>    ><?php echo $item->uso_cfdi_text; ?></option>
							<?php } ?>
						</select>
					</td>	
					
				</tr>
			</table>
			<table border="1" width="100%">
				<tr>
					<td style="font-size: 11px; width: 5%;">Calle</td>
					<td style="font-size: 11px; width: 26%;" colspan="3"><output id="calle"></output></td>
					<td style="font-size: 11px; width: 5%;">No.</td>
					<td style="font-size: 11px; width: 5%;"><output id="num_ext"></output></td>
					<td style="font-size: 11px; width: 5%;">Col.</td>
					<td style="font-size: 11px; width: 25%; " colspan="2"><output id="colonia"></output></td>
					<td style="font-size: 11px; width: 9%; text-align: center;">FORMA DE COBRO</td>
					<td style="font-size: 11px; width: 20%;">
						<input type="tex" id="formadecobro" name="formadecobro" class="form-control" value="<?php echo $formacobro;?>" <?php echo $formacobro_block;?> >
					</td>
				</tr>
				<tr>
					<td style="font-size: 11px; width: 5%;">Cd</td>
					<td style="font-size: 11px;" colspan="2"><?php echo $municipio;?></td>
					<td style="font-size: 11px; width: 5%;">Edo.</td>
					<td style="font-size: 11px; width: 5%;" colspan="4">
							<?php foreach ($estadorow->result() as $item) { 
									if($item->EstadoId==$estado){ 
									 	echo $item->Nombre;
									}
								} ?>
						C.P. <?php echo $cp;?>
					</td>
					<td style="font-size: 11px; width: 5%;">tel.</td>
					<td style="font-size: 11px;" colspan="2">
						<select class="form-control" id="telId" name="telId" <?php echo $telId_block; ?>>
						<?php foreach ($resultadoclitel->result() as $item) { ?>
							<option value="<?php echo $item->id;?>"   <?php if($telId==$item->id){echo 'selected'; }?> ><?php echo $item->tel_local;?></option>		
					    <?php } ?>
						</select>
					</td>
					
				</tr>
				<tr>
					<td style="font-size: 11px; width: 10%;" colspan="2">Contacto</td>
					<td style="font-size: 11px;" colspan="2">
						<select class="form-control" id="contactoId" name="contactoId" <?php echo $contactoId_block; ?> >
						<?php foreach ($resultadoclipcontacto->result() as $item) { ?>
							<option value="<?php echo $item->id;?>" <?php if($contactoId==$item->id){echo 'selected'; }?>><?php echo $item->persona_contacto;?></option>		
					    <?php } ?>
						</select>
					</td>
					<td style="font-size: 11px; width: 5%;">Cargo</td>
					<td style="font-size: 11px; width: 5%;" colspan="3">
						<input type="tex" id="cargo" name="cargo" class="form-control" value="<?php echo $cargo;?>" <?php echo $cargo_block;?> >
					</td>
					<td style="font-size: 11px; width: 5%;">email:</td>
					<td style="font-size: 11px;" colspan="2"><?php echo $email;?></td>
				</tr>
				
			</table>
			<table border="1" width="100%">
				<thead>
				<tr>
					<th style="font-size: 11px; ">Cantidad</th>
					<th style="font-size: 11px; ">Surtir</th>
					<th style="font-size: 11px; ">No. de Parte</th>
					<th style="font-size: 11px; " colspan="3">Descripcion</th>
					<th style="font-size: 11px; ">Precio Unitario</th>
					<th style="font-size: 11px; " >Total</th>
				</tr>
				</thead>
				<tbody>
					<?php 
						$totalgeneral=0;
                        // Tabla de polizas creadas detalles  		
						foreach ($polizaventadetalles->result() as $item) { 
									$totalc = 0;
									if($item->precio_local !==NULL) {
										$totalc=$item->precio_local;
									}
									if($item->precio_semi !==NULL) {
										$totalc=$item->precio_semi;
									}
									if($item->precio_foraneo !==NULL) {
										$totalc=$item->precio_foraneo;
									}
									$totalgeneral=$totalgeneral+$totalc;
									?>
									<tr>
										<td style="font-size: 11px; text-align: center;"><?php echo $item->cantidad;?></td>
										<td style="font-size: 11px; text-align: center;">OF</td>
										<td style="font-size: 11px; ">N/A</td>
										<td style="font-size: 11px; " colspan="3">
											<table border="1" width="100%">
												<tr>
													<td width="30%"></td>
													<td width="70%">Poliza</td>		
												</tr>
												<tr>
													<td>TIPO</td>
													<td>IMPRESORA</td>		
												</tr>
												<tr>
													<td>MODELO</td>
													<td><?php echo $item->modeloe;?></td>		
												</tr>
												<tr>
													<td>SERIE</td>
													<td><?php echo $item->serie;?></td>		
												</tr>
												<tr>
													<td></td>
													<td><?php echo $item->modelo;?></td>		
												</tr>
												<tr>
													<td>Cubre</td>
													<td><?php echo $item->cubre;?></td>		
												</tr>
												<tr>
													<td>Vigencia de meses</td>
													<td><?php echo $item->vigencia_meses;?></td>		
												</tr>
												<tr>
													<td>Vigencia de clicks</td>
													<td><?php echo $item->vigencia_clicks;?></td>		
												</tr>
												<?php if ($item->precio_local!=NULL) { ?>
													<tr>
														<td>Precio local</td>
														<td><?php echo number_format($item->precio_local,2,'.',',');?></td>		
													</tr>
												<?php	}?>
												<?php if ($item->precio_semi!=NULL) { ?>
													<tr>
														<td>Precio semi</td>
														<td><?php echo number_format($item->precio_semi,2,'.',',');?></td>		
													</tr>
												<?php	}?>
												<?php if ($item->precio_foraneo!=NULL) { ?>
													<tr>
														<td>Precio foraneo</td>
														<td><?php echo number_format($item->precio_foraneo,2,'.',',');?></td>		
													</tr>
												<?php	}?>
											</table>
										</td>
										<td style="font-size: 11px; text-align: center;"  >$<?php echo number_format($totalc,2,'.',',');?></td>
										<td style="font-size: 11px; text-align: center;" >$<?php echo number_format($totalc,2,'.',',');?></td>
									</tr>
								<?php	}
					?>	
					
				</tbody>
				<tfoot>
					<tr>
						<td style="font-size: 11px; " colspan="2">Subtotal</td>
						<td style="font-size: 11px; " colspan="2">$<?php echo number_format($totalgeneral,2,'.',',');?></td>
						<td style="font-size: 11px; ">Iva</td>
						<td style="font-size: 11px; " >$<?php echo number_format($totalgeneral*0.16,2,'.',',');?></td>
						<td style="font-size: 11px; ">Total</td>
						<td style="font-size: 11px; " >$<?php echo number_format(($totalgeneral*1.16),2,'.',',');?></td>
					</tr>
				</tfoot>
			</table>
			<label for="observaciones">Observaciones:</label>
			<textarea name="observaciones" id="observaciones" class="form-control" <?php echo $observaciones_block;?> ><?php echo $observaciones;?></textarea>
		</form>
		<div class="row">
		    <div class="col-md-12">
		        <div class="col-md-12 text-right">
		        	<?php 
		        		if($block_button==0){ ?>
		        			<button type="button" class="btn btn-success buttonenvio">Enviar</button>
		        	<?php  }else{  	?>	
		        		<button type="button" class="btn btn-success buttoimprimir">Imprimir</button>
		        	<?php  }  	?>	
		        </div>
		    </div>
		</div>
		<div class="modal fade" id="modalconfirmacionenvio" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h5 class="modal-title" id="exampleModalLabel">Enviar</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      <div class="modal-body">
		        <div class="row">
		        	<div class="col-md-12">
		        		<h4>¿Desea enviar la prefactura?</h4>
		        	</div>
		        </div>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
		        <button type="button" class="btn btn-primary buttonenvioconfirm">Enviar</button>
		      </div>
		    </div>
		  </div>
		</div>
		
	</body>
	<script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/sweetalert/dist/sweetalert.min.js"></script>
	<script type="text/javascript">
		var base_url = $('#base_url').val();
		$(document).ready(function(){ 
			$('.buttonenvio').click(function(event) {
				$('#modalconfirmacionenvio').modal();
			});
			$('.buttonenvioconfirm').click(function(event) {
				var datos = $('#formprefactura').serialize();
				$.ajax({
                    type:'POST',
                    url: base_url+'PolizasCreadas/save',
                    data: datos,
                    async: false,
                    statusCode:{
                            404: function(data){
                                swal("Error", "404", "error");
                            },
                            500: function(){
                                swal("Error", "500", "error"); 
                            }
                        },
                        success:function(data){
                            swal("Éxito", "Pre factura enviada. Por favor recargue la vista de listado de pólizas", "success");
                            setTimeout(function(){ window.location.href=''; }, 3000);
                         

                        }
                    });
			});
			$('.buttoimprimir').click(function(event) {
				window.print();
			});
			<?php if ($rfc_id>0) { ?>
				selectrcfv(<?php echo $rfc_id;?>);
			<?php } ?>

		});
		function selecrfcp() {
			var rfc_id = $('#rfc_id option:selected').val();
			selectrcfv(rfc_id);
			
		}
		function selectrcfv(rfc_id){
			$.ajax({
                url: base_url+'Prefactura/selecciontrcf',
                dataType: 'json',
                data:{id:rfc_id},
                statusCode:{
                        404: function(data){
                            swal("Error", "404", "error");
                        },
                        500: function(){
                            swal("Error", "500", "error"); 
                        }
                    },
                    success:function(data){
                    	var datos=data;
                        datos.forEach(function(r) {
                        $('#razon_social').val(r.razon_social);
                        $('#num_ext').val(r.num_ext);
                        $('#colonia').val(r.colonia);
                        $('#calle').val(r.calle);
                        });
                    }
            });
		}
	</script>
</html>
