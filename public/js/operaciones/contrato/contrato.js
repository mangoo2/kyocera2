var base_url = $('#base_url').val();
var perfilid = $('#perfilid').val();
var table;
var cl_idrenta;
var cl_idcontrato;
var new_folio;
var idrenta_selected;
$(document).ready(function() {	
    $('.modal').modal();
   table = $('#tabla_contrato').DataTable();
   $('.chosen-select').chosen({width: "100%"});
   load();
   $('#agregarejecutivocontrato').click(function(event) {
       $.ajax({
            type:'POST',
            url: base_url+"index.php/Contratos/addejecutivo",
            data: {
                contratoId:$('#idcontratoejecu').val(),
                ejecutivoId:$('#ejecontrato option:selected').val(),
            },
            success: function (response){
                    $.alert({
                        boxWidth: '30%',
                        useBootstrap: false,
                        title: 'Éxito!',
                        content: 'ejecutivo agregado!'});
                    $('#modalcontratoejecutivo').modal('close');
                    load();
            },
            error: function(response){
                notificacion(1);
            }
        });
   });
   $('#okclonarcontrato').click(function(event) {
       $( "#okclonarcontrato" ).prop( "disabled", true );
        setTimeout(function(){ 
             $("#okclonarcontrato" ).prop( "disabled", false );
        }, 5000);
        okclonarcontrato();
   });

   $('#okseleccionequipos').click(function(event) {
        $( "#okseleccionequipos" ).prop( "disabled", true );
        setTimeout(function(){ 
             $("#okseleccionequipos" ).prop( "disabled", false );
        }, 5000);
        okseleccionequipos();
    });
});
function load(){
    var tipo=$('#tipo option:selected').val();
    table.destroy();
   // Destruye la tabla y la crea de nuevo con los datos cargados 
    table = $('#tabla_contrato').DataTable({
        //pagingType: 'full_numbers',
           pagingType: 'input',
           //"sPaginationType": "listbox",
        stateSave: true,
        "bProcessing": true,
        "serverSide": true,
        search: {
                return: true
            },
        "ajax": {
            "url": base_url+"index.php/Contratos/getListadocontratosasi",
            type: "post",
           "data": {
                    "tipo": tipo
            },
        },
        "columns": [
            {"data": "idcontrato"},
            {"data": "empresa"},
            {"data": "folio"},
            {"data": "tipopersona",
                render:function(data,type,row){
                    // Se muestran todos los botones correspondientes
                    var html='';
                        if(row.tipopersona == 1){
                          html = 'Fisica';
                        }else if(row.tipopersona == 2){
                          html = 'Moral';
                        }
                    return html;
                }
            },
            {"data": "vigencia",
                render:function(data,type,row){
                    // Se muestran todos los botones correspondientes
                    var html='';
                        if(row.vigencia == 1){
                          html = '12 meses';
                        }else if(row.vigencia == 2){
                          html = '24 meses';
                        }else if(row.vigencia == 3){
                          html = '36 meses';
                        }
                    return html;
                }
            },
            {"data": "vendedor"},
            {"data": "fechasolicitud"},
            {"data": "fechainicio"},
            {"data": "arrendataria"},
            {"data": "rfc_df"},
            {"data": "file",
                render:function(data,type,row){
                    var comillas = "'";
                    var html = "";
                    if(row.file !=""){
                        html += '<a class="btn-bmz darken-4" style="background-color: #1d7d74" onclick="detalle('+comillas+row.file+comillas+')"><i class="material-icons">photo_camera</i></a>';
                    }
                    if(row.estatusr == 2 || row.estatusr == 0){
                         if (row.prefactura==1) {
                            var colorbtton='green';
                        }else{
                            var colorbtton='blue';
                        }
                        html='<a class="btn-floating '+colorbtton+' tooltipped" data-position="top" data-delay="50" data-tooltip="Prefactura" onclick="detalle('+row.idrenta+')">\
                                    <i class="material-icons">assignment</i>\
                                  </a> ';
                        html+='<a class="btn-floating amber tooltipped" data-position="top" data-delay="50" data-tooltip="Prefactura" onclick="detalleh('+row.idrenta+')">\
                                    <i class="material-icons">assignment</i>\
                                  </a> ';
                    }else{    
                        html='<a class="btn-floating blue tooltipped" disabled>\
                                    <i class="material-icons">assignment</i>\
                                  </a> ';
                    }
                    return html;
                }   
            },
            {"data": null,
                render:function(data,type,row){
                    var html='';
                    if (row.ejecutivo>0) {
                        if(perfilid==1){
                            var detelejejcutivo='<i class="fas fa-user-slash" style="color:red;cursor: pointer;" onclick="deleteejecutivo('+row.idcontrato+')"></i> ';
                        }else{
                            var detelejejcutivo='';
                        }
                        html+=''+row.ej+' '+row.ejap+' '+row.ejam+' '+detelejejcutivo+'<br>';                        
                    }
                    if(perfilid==1){
                        html+='<a class="waves-effect cyan btn-bmz" onclick="addejecutivo('+row.idcontrato+')"><i class="fas fa-user-plus"></i></a> '; 
                    }
                    return html;
                }
            },
            {"data": "idcontrato",
                render:function(data,type,row){ 
                    var html ='<div class="espaciobtn">';  
                        if(row.estatus == 2){  
                            html+='<a class="waves-effect green btn-bmz" href="'+base_url+'index.php/Contratos/contrato/'+row.idrenta+'">Concluido</a> ';
                            html+='<a class="waves-effect green btn-bmz" onclick="renovacion('+row.idrenta+')">Renovación</a>';         
                        }else{
                            html+='<a class="b-btn b-btn-cyan" href="'+base_url+'index.php/Contratos/contrato/'+row.idrenta+'">Mostrar contrato</a> ';
                        }
                        if(row.estatus==1){
                            html+='<a class="b-btn b-btn-success " href="'+base_url+'index.php/Contratos/contrato/'+row.idrenta+'?edit=1"><i class="fas fa-pencil-alt"></i></a> ';     
                        }
                        
                        if (row.estatus!=0) {
                            html+='<a class="b-btn b-btn-danger" onclick="cancelarc('+row.idcontrato+')"><i class="fas fa-times"></i></a> ';
                        } 
                        if(row.estatus==0){
                            html+='<a class="waves-effect cyan btn-bmz" href="'+base_url+'index.php/Configuracionrentas/ContratosPagos/'+row.idcontrato+'" title="ver rentas"><i class="fa fa-link fa-fw"></i></a> ';
                        }  
                        html+='<a class="b-btn b-btn-primary tooltipped" data-position="top" data-delay="50" data-tooltip="Cambiar personal" onclick="cambiar_per('+row.idcontrato+',3)"><i class="fas fa-user fa-fw"></i></a> ';
                        html+='<a class="b-btn b-btn-primary tooltipped" data-position="top" data-delay="50" data-tooltip="Cambiar de cliente" onclick="cambiar_cliente('+row.idrenta+',3)"><i class="fas fa-people-arrows"></i></a> ';
                        html+='<button class="b-btn b-btn-primary tooltipped" data-position="top" data-delay="50" data-tooltip="Clonar" onclick="clonarcontrato('+row.idrenta+','+row.idcontrato+')"><i class="fas fa-copy fa-fw"></i></button> ';
                    html +='</div>';
                        
                    return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[20, 50, 100], [20, 50, 100]],
        "dom": 'Bfrtpl',
        // Muestra el botón de Excel para importar la tabla
        "buttons": [
            
        ],
        
        rowCallback: function (row, data) {
            //if it is not the summation row the row should be selectable
            if (data.estatus == 2 ) {
                $(row).addClass('redcolor');
                //$('.red_color').css("background","red");
            }
        }
    }).on("draw", function(){
        var perfil=parseInt($('#perfilid').val());
        if (perfil==1 || perfil==6) {
            
        }else{
            console.log(perfil);
            table.columns( [11] ).visible( false );
        }
        $('.dataTables_filter').append('<i class="fa fa-search buttonsearch" onclick="searchtable_v()"></i>');
        
    }); 
}
function searchtable_v(){
    var contenedor = document.getElementById('tabla_contrato_filter');

    // Selecciona el input dentro del contenedor
    var input = contenedor.querySelector('input.form-control-bmz');

    // Obtén el valor del input
    var searchv = input.value;

    // Muestra el valor en la consola
    console.log(searchv);
    //var searchv =$("input[type=search]").val();
    table.search(searchv).draw();
}
function detalle(nombre){
    window.open(base_url+"Contratos/imgcontrato/"+nombre, "Foto", "width=780, height=612");
}
function cancelarc(id){
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Contratos/verificar_periodo",
        data: {
            contratoId:id,        },
        success: function (response){
            console.log(response);
        ////
            var v = parseFloat(response);
            if(v==0){
                
                $.confirm({
                    boxWidth: '30%',
                    useBootstrap: false,
                    icon: 'fa fa-warning',
                    title: 'Atención!',
                    content: '¿Está seguro de Cancelar este Contrato?<br>Se necesita permisos de administrador<br>'+
                            '<textarea id="motivoeliminacion" class="form-control" placeholder="Motivo"></textarea>'+
                             '<input type="password" placeholder="Contraseña" id="contrasena" class="name form-control" required/>',
                    type: 'red',
                    typeAnimated: true,
                    buttons:{
                        confirmar: function (){
                            var pass=$('#contrasena').val();
                            var motivoe = $('#motivoeliminacion').val();
                            if (pass!='') {
                                 $.ajax({
                                    type:'POST',
                                    url: base_url+"index.php/Sistema/solicitarpermiso",
                                    data: {
                                        pass:pass
                                    },
                                    success: function (response){
                                            var respuesta = parseInt(response);
                                            if (respuesta==1) {
                                                $.ajax({
                                                    type:'POST',
                                                    url: base_url+"index.php/Contratos/cancelar",
                                                    data: {
                                                        contratoId:id,
                                                        motivo:motivoe
                                                    },
                                                    success: function (response){
                                                            $.alert({
                                                                boxWidth: '30%',
                                                                useBootstrap: false,
                                                                title: 'Éxito!',
                                                                content: 'Contrato Cancelado!'});
                                                            load();
                                                    },
                                                    error: function(response){
                                                        notificacion(1);  
                                                         
                                                    }
                                                });
                                            }else{
                                                notificacion(2);
                                            }
                                    },
                                    error: function(response){
                                        notificacion(1);  
                                    }
                                });
                                
                            }else{
                                notificacion(0);
                            }
                            
                        },
                        cancelar: function () 
                        {
                            
                        }
                    }
                });
            }else{
                $.alert({
                boxWidth: '30%',
                useBootstrap: false,
                title: 'Advertencia!',
                content: 'Se tiene periodos pendientes'}); 
            }
        ////
        },
        error: function(response){
            notificacion(1);    
             
        }
    });
   
}
function addejecutivo(contrato){
    $('.modal').modal({
                            dismissible: true
                        });
    $('#modalcontratoejecutivo').modal('open');
    $('#idcontratoejecu').val(contrato);
}
function deleteejecutivo(contrato){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de quitar al ejecutivo?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Contratos/addejecutivo",
                    data: {
                        contratoId:contrato,
                        ejecutivoId:0,
                    },
                    success: function (response){
                        $.alert({
                            boxWidth: '30%',
                            useBootstrap: false,
                            title: 'Éxito!',
                            content: 'ejecutivo eliminado!'});
                            load();
                    },
                    error: function(response){
                        notificacion(1);    
                         
                    }
                });
            },
            cancelar: function (){
            }
        }
    });
}
function detalle(id){
    window.open(base_url+"RentasCompletadas/view/"+id, "Prefactura", "width=780, height=612");
}
function detalleh(idr){
    idrenta_selected=idr;
    $('#modalprefacturash').modal('open');
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Rentas/historialprefacturas",
        data: {
            idrenta:idr
        },
        success: function (response){
            $('.historialprefacturas').html(response);
            $('#historialprefacturas').DataTable();
        },
        error: function(response){
            notificacion(1);
        }
    });
}
function obtenerequipos_de_contratos(){
    $.ajax({
        type:'POST',
        url: base_url+'index.php/Rentas/obtenerequipos_de_contratos',
        data: {idRenta:idrenta_selected},
        success:function(data){
            $('.div_table_equipos').html(data);
            $('#modalseleccionequipos').modal({dismissible: false}); 
            $('#modalseleccionequipos').modal('open');
        }
    });
}
function okseleccionequipos(){
    var lis_equipos = $("#tableequipos tbody > tr");
    var num_equipos_selected=0;
    var DATAa  = [];
    lis_equipos.each(function(){  
        if ($(this).find("input[class*='serie_check']").is(':checked')) {
            num_equipos_selected++;
            item = {};                    
            item ["equipoid"]  = $(this).find("input[id*='equipoid']").val();
            item ["serieid"]  = $(this).find("input[id*='serieid']").val();
            //item ["equiporow"]  = $(this).find("input[id*='equiporow']").val();
            
            DATAa.push(item);
        }       
        
    });
    INFOa  = new FormData();
    aInfoa   = JSON.stringify(DATAa);
    console.log(aInfoa);
    if(num_equipos_selected>0){
        $.ajax({
            type:'POST',
            url: base_url+"index.php/Rentas/generacionpreequipos",
            data: {
                equipos:aInfoa,
                idRenta:idrenta_selected
            },
            async: false,
            statusCode:{
                404: function(data){
                    swal("Error", "404", "error");
                },
                500: function(){
                    swal("Error", "500", "error"); 
                }
            },
            success:function(response){  
                console.log(response);
                var array = $.parseJSON(response);
                if(array.historialid>0){
                    toastr.success('Equipos seleccionados');
                    
                    detalleh(idrenta_selected);
                    $('#modalseleccionequipos').modal('close');
                }
                

            }
        });
    }else{
        toastr.error('Favor de seleccionar las de un equipo');
    }
}
function prefacturah(idrenta,idhistorial){
    window.open(base_url+"RentasCompletadas/viewh/"+idrenta+'/'+idhistorial, "Prefactura", "width=780, height=612");
}
function eliminarpreh(idpreh){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de eliminar la prefactura?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Contratos/eliminarprehistorial",
                    data: {
                        id:idpreh
                    },
                    success: function (response){
                        $.alert({
                            boxWidth: '30%',
                            useBootstrap: false,
                            title: 'Éxito!',
                            content: 'prefactura eliminada!'});
                            load();
                    },
                    error: function(response){
                        notificacion(1);  
                    }
                });
            },
            cancelar: function (){
            }
        }
    });
}
function renovacion(idrenta){
     $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Confirma realizar una renovación del contrato?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                   window.location.href = base_url+"index.php/Contratos/renovacion/"+idrenta;                
            },
            cancelar: function () {
            }
        }
    });
}
function cambiar_per(id,tipo){
    var personal=$('#ejecontratoselect').html();
    $.confirm({
        boxWidth: '40%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Comentario',
        content: '<div style="width: 99%;">¿Desea cambiar al personal?<br>Se necesita permisos de administrador<br><input type="text" id="pass_cambio" name="pass_cambio" class="form-control-bmz"><label>Ejecutivo</label><select id="ejecutivoselect_new" class="browser-default form-control-bmz">'+personal+'</select></div>',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var eje=$('#ejecutivoselect_new').val();
                //var pass=$('#pass_cambio').val();
                var pass=$("input[name=pass_cambio]").val();
                $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Generales/cambiopersonal",
                    data: {
                        id:id,
                        tipo:tipo,
                        eje:eje,
                        pass:pass
                    },
                    success: function (response){
                        if(parseInt(response)==1){
                            toastr["success"]("Cambio Realizado");  
                            load();  
                        }else{
                            toastr["error"]("No se tiene permiso"); 
                        }
                    },
                    error: function(response){
                        toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema");  
                    }
                }); 
            },
            cancelar: function (){
            }
        }
    });
    setTimeout(function(){
        select2idcliente();
            new MaskedPassword(document.getElementById("pass_cambio"), '\u25CF');
    },1000);
    setTimeout(function(){ 
        $('#ejecutivoselect_new option[value=0]').remove();
    }, 1000);
}
function clonarcontrato(idrenta,idcontrato){
    var content_html='¿Esta seguro de clonar el contrato?<br>Se necesita permisos de administrador<br><input type="text" id="pass_cambio" name="pass_cambio" class="form-control-bmz" style="width: 99%;"><label>¿Desea agregar nuevo folio?</label><input type="text" id="new_folio" class="form-control-bmz" style="width: 99%;">';
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: content_html,
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var pass=$("input[name=pass_cambio]").val();
                new_folio = $('#new_folio').val();
                if(pass!=''){
                    $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Sistema/solicitarpermiso",
                        data: {
                            pass:pass
                        },
                        success: function (response){
                                var respuesta = parseInt(response);
                                if (respuesta==1) {
                                    cl_idrenta=idrenta;
                                    cl_idcontrato=idcontrato;
                                    clonarcontratopasa();
                                }else{
                                    notificacion(2);
                                }
                        },
                        error: function(response){
                            notificacion(1);  
                        }
                    });
                }else{
                    notificacion(0);
                }
            },
            cancelar: function (){
                
            }
        }
    });
    setTimeout(function(){
            new MaskedPassword(document.getElementById("pass_cambio"), '\u25CF');
    },1000);
}
function clonarcontratopasa(){
    $('#modalcontratoclonar').modal('open');
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Contratos/viewperiodoscontrato",
        data: {
            idrenta:cl_idrenta,
            idcontrato:cl_idcontrato
        },
        success: function (data){
            console.log(data);
            $('.tablefacturasall').html(data);
            $('#tablefacturasall').DataTable({"order": [[ 1, "desc" ]]});
        },
        error: function(response){
            notificacion(1);  
        }
    });
}
function comentariop(prefId){
    prefId_row=prefId;
    $('#modal_comentario').modal();
    $('#modal_comentario').modal('open');
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Generales/getcomentarioperiodo",
        data: {
            prfId:prefId
        },
        success: function (response){
            $('.comentariopre').html(response.replace(/(\r\n|\n|\r)/gm, " "));    
        },
        error: function(response){
        }
    });
}
function okclonarcontrato(){
    $('body').loading({theme: 'dark',message: 'Clonando...'});
    var num_pres_selected=0;
    var rentaslis = $("#tablefacturasall tbody > tr");
    var DATAr  = [];
        
    rentaslis.each(function(){  
        if ($(this).find("input[class*='periodos_checked']").is(':checked')) {
            num_pres_selected++;
            item = {};                    
            item ["per"]  = $(this).find("input[class*='periodos_checked']").val();
            DATAr.push(item);
        }
    });
    aInfor   = JSON.stringify(DATAr);

    datos='periodos='+aInfor+'&idrenta='+cl_idrenta+'&idcontrato='+cl_idcontrato+'&new_folio='+new_folio;
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Contratos/clonarcontrato",
        data: datos,
        success: function (response){
            console.log(response);
            swal({title: "Hecho",text: 'Contrato clonado',type: "success",showCancelButton: false});
            load();
            $('#modalcontratoclonar').modal('close');
            $('body').loading('stop');
        },
        error: function(response){
            notificacion(1); 
            $('body').loading('stop');
        }
    });
}