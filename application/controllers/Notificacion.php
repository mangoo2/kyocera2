<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require 'vendorgoo/autoload.php';
use Google\Auth\Credentials\ServiceAccountCredentials;
use Google\Auth\HttpHandler\HttpHandlerFactory;
class Notificacion extends CI_Controller {
    public function __construct()     {
        parent::__construct();

        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloGeneral');
        $this->load->model('Modelonotify');
        $this->load->helper('url');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoycorta = date('Y-m-d');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->hora_ac = date('G');//24 horas
    }
    function index(){
        
    }
    function datos($idper,$perfil) {
        $new_ser_add=0;
        if($this->hora_ac>8 && $this->hora_ac<18){
            if($perfil==5 || $perfil==10 || $perfil==12){
                /*
                $result=$this->ModeloGeneral->consultar_servicos_itinerario($idper,$this->fechahoycorta);  
                foreach ($result->result() as $item) {
                    $cadena =$item->tipag;
                    if (strpos($cadena, '1') !== false) {
                        $new_ser_add=1;
                    }
                    
                }
                */
                $strq="SELECT * from unidades_bitacora_itinerario where activo=1 and tipoagregado=1 and fecha='$this->fechahoycorta' and noti_push=0 and personal='$idper' ";
                $query = $this->db->query($strq);
                $v_personal=0;
                foreach ($query->result() as $item) {
                    $id=$item->id;
                    $personal=$item->personal;

                    if($v_personal!=$item->personal){
                        $v_personal=$item->personal;
                        $new_ser_add=1;
                    }
                    $this->ModeloCatalogos->updateCatalogo('unidades_bitacora_itinerario',array('noti_push'=>1),array('id'=>$id));
                    
                }
            }
        }
        



        $array=array(
            'idper'=>$idper,
            'perfil'=>$perfil,
            'new_ser_add'=>$new_ser_add
                    );
        echo json_encode($array);
        //echo json_decode($array);
    }
    function enviopush(){
        $credential = new ServiceAccountCredentials(
            "https://www.googleapis.com/auth/firebase.messaging",
            json_decode(file_get_contents(FCPATH . 'kyocera-58f51.json'), true)
        );

        $token = $credential->fetchAuthToken(HttpHandlerFactory::build());
        echo 'entro';
        /*
        https://fcm.googleapis.com//v1/projects/<YOUR-PROJECT-ID>/messages:send
        Content-Type: application/json
        Authorization: bearer <YOUR-ACCESS-TOKEN>

        {
          "message": {
            "token": "eEz-Q2sG8nQ:APA91bHJQRT0JJ...",
            "notification": {
              "title": "Background Message Title",
              "body": "Background message body"
            },
            "webpush": {
              "fcm_options": {
                "link": "https://dummypage.com"
              }
            }
          }
        }
        */
       $url = 'https://fcm.googleapis.com/v1/projects/kyocera-58f51/messages:send';

        $headers = array(
            'Authorization: Bearer ' . $token['access_token'],
            'Content-Type: application/json'
        );

        $data = '{
          "message": {
            "token": "ctaqJNe5YG0d09sxK_44gw:APA91bEJLzKrPGZt1umz6MmGUmutWd1aUptSacQUUQ39RTAEdRCA_Ykse2xUyNOM0b6NB6KVHqRIxqNRztJx6Oc88lsJadu6QaNEJl-VKpia3sPCFOJF6pMdqs30s_n5ZS7N1oBbS0K9",
            "notification": {
              "title": "Background Message Title",
              "body": "Background message body"
            },
            "webpush": {
              "fcm_options": {
                "link": "https://dummypage.com"
              }
            }
          }
        }';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);


        $response = curl_exec($ch);
        $info = curl_getinfo($ch);

        if (curl_errno($ch)) {
          echo curl_error($ch);
          echo '<br>Error';
        } else {
          $httpCode = $info['http_code'];
          if ($httpCode >= 200 && $httpCode < 300) {
            $decoded = json_decode($response, true);
            echo 'Success! Response:';
            print_r($decoded);
          } else {
            echo 'Error: HTTP code ' . $httpCode;
          }
        }

        curl_close($ch);
    }
    function enviopush2(){
        $credential = new ServiceAccountCredentials(
            "https://www.googleapis.com/auth/firebase.messaging",
            json_decode(file_get_contents('kyocera-58f51.json'), true)
        );

        $token = $credential->fetchAuthToken(HttpHandlerFactory::build());

        $ch = curl_init("https://fcm.googleapis.com/v1/projects/kyocera-58f51/messages:send");

        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
            'Authorization: Bearer '.$token['access_token']
        ]);

        curl_setopt($ch, CURLOPT_POSTFIELDS, '{
            "message": {
              "token": "fwnt-I609y-jqnP3leuOSX:APA91bFTZfal1uh0hfwiFM53RUBkXRg1rMR2ScBl1vKMreiFddgRKNGY34xi3YGYSbkj_h-OfRD2nngPt4meJ8YXm9yzWQPyQExKvtzA0aC-sIq5_OXzs25svz7LtqfGAnc1pqCTIVlQ",
              "notification": {
                "title": "Notificacion",
                "body": "Background message body",
                "image": "https://altaproductividadapr.com/app-assets/images/favicon/favicon_kyocera.png"
              },
              "webpush": {
                "fcm_options": {
                  "link": "https://altaproductividadapr.com"
                }
              }
            }
          }');

          curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "post");

          $response = curl_exec($ch);

          curl_close($ch);

          echo $response;
    }
    function enviopush3($idper,$titulo,$semsaje){
        $this->Modelonotify->enviopush($idper,$titulo,$semsaje);
    }
    function verificar_nuevos_servicios_ext(){
        $strq="SELECT * from unidades_bitacora_itinerario where activo=1 and tipoagregado=1 and fecha='$this->fechahoycorta' and noti_push=0 ORDER BY personal ASC";
        $query = $this->db->query($strq);
        $v_personal=0;
        foreach ($query->result() as $item) {
            $id=$item->id;
            $personal=$item->personal;
            $prioridad = $item->prioridad;
            $l_prio=$this->prioridadser($prioridad);
            if($v_personal!=$item->personal){
                $v_personal=$item->personal;
                //log_message('error','se envio '.$l_prio);
                $this->Modelonotify->enviopush($item->personal,'Servicios agregados','Se ha agregado un nuevo servicio, favor de actualizar su itinerario'.$l_prio.'..');
            }
            $this->ModeloCatalogos->updateCatalogo('unidades_bitacora_itinerario',array('noti_push'=>1),array('id'=>$id));
            
        }
    }
    function prioridadser($pri){
        $nameprioridad='';
        switch ($pri) {
            case 1:
                $nameprioridad=',Prioridad: Ninguno';
                break;
            case 2:
                $nameprioridad=',Prioridad: Muy pequeño';
                break;
            case 3:
                $nameprioridad=',Prioridad: Pequeño';
                break;
            case 4:
                $nameprioridad=',Prioridad: Muy bajo';
                break;
            case 5:
                $nameprioridad=',Prioridad: Bajo';
                break;
            case 6:
                $nameprioridad=',Prioridad: Moderado';
                break;
            case 7:
                $nameprioridad=',Prioridad: Alto';
                break;
            case 8:
                $nameprioridad=',Prioridad: Muy Alto';
                break;
            case 9:
                $nameprioridad=',Prioridad: Critico';
                break;
            
            default:
                $nameprioridad='';
                break;
        }
        return $nameprioridad;
    }
    


    
}
?>
