CREATE VIEW  ventas_servicios AS 
                SELECT ac.asignacionId,ac.tipo,cli.empresa, cli.email,cli.id
                FROM asignacion_ser_avc as ac 
                inner join contrato as con on con.idcontrato=ac.asignacionId
                inner join rentas as ren on ren.id=con.idRenta
                inner join clientes as cli on cli.id=ren.idCliente
                WHERE ac.activo=1 and ac.tipo=1
                UNION
                SELECT ac.asignacionId,ac.tipo,cli.empresa, cli.email,cli.id
                FROM asignacion_ser_avc as ac 
                inner join polizasCreadas as pol on pol.id=ac.asignacionId
                inner join clientes as cli on cli.id=pol.idCliente
                WHERE ac.activo=1 and ac.tipo=2
                UNION
                SELECT ac.asignacionId,ac.tipo,cli.empresa, cli.email,cli.id
                FROM asignacion_ser_avc as ac 
                inner join asignacion_ser_cliente_a as asig on asig.asignacionId=ac.asignacionId
                inner join clientes as cli on cli.id=asig.clienteId
                WHERE ac.activo=1 and ac.tipo=3
                UNION
                SELECT ac.asignacionId,ac.tipo,cli.empresa, cli.email,cli.id
                FROM asignacion_ser_avr as ac 
                inner join contrato as con on con.idcontrato=ac.asignacionId
                inner join rentas as ren on ren.id=con.idRenta
                inner join clientes as cli on cli.id=ren.idCliente
                WHERE ac.activo=1 and ac.tipo=1
                UNION
                SELECT ac.asignacionId,ac.tipo,cli.empresa, cli.email,cli.id
                FROM asignacion_ser_avr as ac 
                inner join polizasCreadas as pol on pol.id=ac.asignacionId
                inner join clientes as cli on cli.id=pol.idCliente
                WHERE ac.activo=1 and ac.tipo=2
                UNION
                SELECT ac.asignacionId,ac.tipo,cli.empresa, cli.email,cli.id
                FROM asignacion_ser_avr as ac 
                inner join clientes as cli on cli.id=ac.asignacionId
                WHERE ac.activo=1 and ac.tipo=3
-------------------------------------------------------------------------------   
-- DROP VIEW `ventas_servicios_e`             
CREATE VIEW  ventas_servicios_e AS 
SELECT asig.asignacionId, 3 as tipo, cli.empresa, cli.email,cli.id,asig.tiposervicio,asig.activo,asig.motivoeliminado,asig.reg
from asignacion_ser_cliente_a as asig
inner join clientes as cli on cli.id=asig.clienteId
where asig.status!=3
UNION
SELECT asic.asignacionId,1 as tipo, cli.empresa, cli.email,cli.id,asic.tiposervicio,asic.activo,asic.motivoeliminado,asic.reg
from asignacion_ser_contrato_a as asic
inner join contrato as con on con.idcontrato=asic.contratoId
inner join rentas as ren on ren.id=con.idRenta
inner join clientes as cli on cli.id=ren.idCliente
inner join asignacion_ser_contrato_a_e as asice on asice.asignacionId=asic.asignacionId
WHERE asice.status!=3
UNION
SELECT asip.asignacionId,2 as tipo,cli.empresa, cli.email,cli.id,asip.tiposervicio,asip.activo,asip.motivoeliminado,asip.reg
from asignacion_ser_poliza_a as asip
inner join polizasCreadas as pol on pol.id=asip.polizaId
inner join clientes as cli on cli.id=pol.idCliente
inner join asignacion_ser_poliza_a_e as asipe on asipe.asignacionId=asip.asignacionId
WHERE asipe.status!=3

------------------------------------------------------------------------------
-- DROP VIEW `vista_ventas`  
create view vista_ventas as
                SELECT 
                            vc.combinadaId as id,
                            vc.estatus,
                            vc.prefactura,
                            vc.telefono,
                            vc.correo,
                            cli.empresa,
                            per.nombre as vendedor,
                            '1' as combinada,
                            vc.idCliente,
                            vc.statuspago,
                            vc.motivo,
                            vc.activo,
                            vc.reg,
                            vc.equipo,
                            vc.consumibles,
                            vc.refacciones,
                            vc.id_personal
                        FROM ventacombinada as vc
                        inner JOIN clientes as cli on cli.id=vc.idCliente
                        inner join personal as per on per.personalId=vc.id_personal
                        UNION
                        SELECT
                            v.id,
                            v.estatus,
                            v.prefactura,
                            v.telefono,
                            v.correo,
                            cli.empresa,
                            per.nombre as vendedor,
                            '0' as combinada,
                            v.idCliente,
                            v.statuspago,
                            v.motivo,
                            v.activo,
                            v.reg,
                            0 as equipo,
                            0 as consumibles,
                            0 as refacciones,
                            v.id_personal
                            FROM ventas as v 
                            inner join clientes as cli on cli.id=v.idCliente
                            left join personal as per on per.personalId=v.id_personal
                        where v.combinada=0

-------------------------------------------------------------------------------------
------------------------------------------------------------------------------
-- DROP VIEW `vista_ventas3`  
create view vista_ventas3 as
                SELECT 
                            vc.combinadaId as id,
                            vc.estatus,
                            vc.prefactura,
                            vc.telefono,
                            vc.correo,
                            cli.empresa,
                            per.nombre as vendedor,
                            '1' as combinada,
                            vc.idCliente,
                            vc.statuspago,
                            vc.motivo,
                            vc.activo,
                            vc.reg,
                            vc.equipo,
                            vc.consumibles,
                            vc.refacciones,
                            vc.id_personal,
                            (
                                SELECT GROUP_CONCAT(fac.Folio) 
                                    FROM `factura_venta` as ven 
                                    INNER JOIN f_facturas as fac on fac.FacturasId=ven.facturaId 
                                    WHERE ven.combinada=1 and ven.ventaId=vc.combinadaId
                                ) as facturas,
                            pre.vencimiento,
                            vc.fechaentrega,

                            (
                                SELECT sum(vhdc.totalGeneral) as total 
                                    FROM ventas_has_detallesConsumibles as vhdc 
                                    WHERE vhdc.idVentas=vc.consumibles
                            ) as totalcon,
                            (
                                SELECT sum(vhdr.totalGeneral) as total 
                                    FROM ventas_has_detallesRefacciones as vhdr 
                                    WHERE vhdr.idVentas=vc.refacciones
                            ) as totalref,
                            (
                                SELECT sum(vhde.cantidad*vhde.precio) AS total 
                                    FROM ventas_has_detallesEquipos AS vhde 
                                    WHERE vhde.idVenta=vc.equipo
                            ) as totalequ
                        FROM ventacombinada as vc
                        inner JOIN clientes as cli on cli.id=vc.idCliente
                        inner join personal as per on per.personalId=vc.id_personal
                        left join prefactura as pre on pre.ventaId=vc.combinadaId and pre.tipo=3
                        UNION
                        SELECT
                            v.id,
                            v.estatus,
                            v.prefactura,
                            v.telefono,
                            v.correo,
                            cli.empresa,
                            per.nombre as vendedor,
                            '0' as combinada,
                            v.idCliente,
                            v.statuspago,
                            v.motivo,
                            v.activo,
                            v.reg,
                            0 as equipo,
                            0 as consumibles,
                            0 as refacciones,
                            v.id_personal,
                            (
                                SELECT GROUP_CONCAT(fac.Folio)
                                    FROM `factura_venta` as ven 
                                    INNER JOIN f_facturas as fac on fac.FacturasId=ven.facturaId 
                                    WHERE ven.combinada=0 and ven.ventaId=v.id
                                    ) as facturas,
                            pre.vencimiento,
                            v.fechaentrega,
                            (
                                SELECT sum(vhdc.totalGeneral) as total 
                                    FROM ventas_has_detallesConsumibles as vhdc 
                                    WHERE vhdc.idVentas=v.id
                            ) as totalcon,
                            (
                                SELECT sum(vhdr.totalGeneral) as total 
                                    FROM ventas_has_detallesRefacciones as vhdr 
                                    WHERE vhdr.idVentas=v.id
                            ) as totalref,
                            (
                                SELECT sum(vhde.cantidad*vhde.precio) AS total 
                                    FROM ventas_has_detallesEquipos AS vhde 
                                    WHERE vhde.idVenta=v.id 
                            ) as totalequ
                            
                            FROM ventas as v 
                            inner join clientes as cli on cli.id=v.idCliente
                            left join personal as per on per.personalId=v.id_personal
                            left join prefactura as pre on pre.ventaId=v.id and pre.tipo=0
                        where v.combinada=0 

-------------------------------------------------------------------------------------
-- DROP VIEW `vista_inventario_equipos`
create view vista_inventario_equipos as
    SELECT sp.productoid, bod.bodega, eq.modelo,sp.bodegaId,COUNT(*) as stock,GROUP_CONCAT(sp.serie SEPARATOR ', ') as series,sum(sp.resguardar_cant) as resguardar_cant
    FROM series_productos sp 
    JOIN bodegas bod ON bod.bodegaId=sp.bodegaId 
    JOIN equipos eq ON eq.id=sp.productoid 
    WHERE sp.status < 2 AND sp.activo = 1 
    GROUP BY eq.id,sp.bodegaId  

-------------------------------------------------------------------------------------
-- DROP VIEW vista_inventario_refacciones
create view vista_inventario_refacciones as
    SELECT cata.id,isr.serieId, bod.bodega, cata.nombre, sum(isr.cantidad) as cantidad, bod.bodegaId,isr.con_serie,sum(isr.resguardar_cant) as resguardar_cant
    FROM series_refacciones isr
    JOIN bodegas bod ON bod.bodegaId=isr.bodegaId
    JOIN refacciones cata ON cata.id=isr.refaccionid
    WHERE isr.activo = 1
    AND isr.status < 3
    GROUP BY isr.refaccionid, isr.bodegaId

-------------------------------------------------------------------------------------
-- DROP VIEW vista_inventario_accesorios
create view vista_inventario_accesorios as
    SELECT cata.id,ia.serieId, count(*) stock, bod.bodegaId, bod.bodega, cata.nombre,GROUP_CONCAT(ia.serie SEPARATOR ', ') as series
    FROM series_accesorios ia
    JOIN bodegas bod ON bod.bodegaId=ia.bodegaId
    JOIN catalogo_accesorios cata ON cata.id=ia.accesoriosid
    WHERE bod.activo = 1 and ia.status<2
    GROUP BY ia.accesoriosid, ia.bodegaId
-------------------------------------------------------------------------------------
    -- DROP VIEW vista_polizascreadas
    create view vista_polizascreadas as
        SELECT pc.id,
                c.empresa,
                pc.plazo,
                pc.area,
                pc.estatus,
                pc.idCotizacion,
                pc.reg,
                pc.idCliente,
                pc.prefactura,
                pc.atencion,
                pc.correo,
                pc.telefono,
                pc.tipo,
                pc.statuspago,
                pre.vencimiento,
                pc.combinada,
                pc.activo,
                pc.id_personal,
                (
                                SELECT GROUP_CONCAT(fac.Folio)
                                FROM `factura_poliza` as pol 
                                INNER JOIN f_facturas as fac on fac.FacturasId=pol.facturaId and fac.Estado=1
                                WHERE pol.polizaId=pc.id
                                    ) as facturas
                from polizasCreadas as pc
                inner join clientes as c on pc.idCliente=c.id
                left join prefactura as pre on pre.ventaId=pc.id and pre.tipo=1
--vista_entregas_dia-----------------------------------------------------------------------------------
    -- DROP VIEW vista_entregas_dia
    create view vista_entregas_dia as
    SELECT 
        v.estatus,
        v.id,
        v.prefactura,
        v.telefono,
        v.correo,
        cli.empresa,
        per.nombre as vendedor,
        'ventas' as tabla,
        1 as tipotabla,
        v.fechaentrega,
        v.e_entrega,
        v.e_entrega_p,
        v.idCliente,
        v.statuspago,
        v.activo,
        0 as id_rh,
        0 as renta_rh,
        pre.reg,
        v.id_personal,
        '' as equipo,
        '' as consumibles,
        '' as refacciones,
        '' as poliza
        FROM ventas as v 
    inner join clientes as cli on cli.id=v.idCliente 
    LEFT join personal as per on per.personalId=v.id_personal 
    left join prefactura as pre on pre.ventaId=v.id and pre.tipo=0
    WHERE v.activo=1 and v.combinada=0 and v.viewed=1

    union

    SELECT 
        v.estatus,
        v.id,
        v.prefactura,
        v.telefono,
        v.correo,
        cli.empresa,
        per.nombre vendedor,
        'rentas' tabla,
        2 as tipotabla,
        pre.vencimiento as fechaentrega,
        v.e_entrega,
        v.e_entrega_p,
        v.idCliente,
        v.statuspago,
        1 as activo,
        0 as id_rh,
        0 as renta_rh,
        pre.reg,
        v.id_personal,
        '' as equipo,
        '' as consumibles,
        '' as refacciones,
        '' as poliza
        from rentas as v
        inner join clientes as cli on cli.id=v.idCliente
        LEFT join personal as per on per.personalId=v.id_personal 
        left join prefactura as pre on pre.ventaId=v.id and pre.tipo=2
        WHERE v.estatus !=0 and v.viewed=1

    
    union

    SELECT

        vc.estatus,
        vc.combinadaId id,
        vc.prefactura,
        vc.telefono,
        vc.correo,
        cli.empresa,
        per.nombre vendedor,
        'combinada' tabla,
        4 as tipotabla,
        vc.fechaentrega,
        vc.e_entrega,
        vc.e_entrega_p,
        vc.idCliente,
        vc.statuspago,
        vc.activo,
        0 as id_rh,
        0 as renta_rh,
        pre.reg,
        vc.id_personal,
        vc.equipo,
        vc.consumibles,
        vc.refacciones,
        vc.poliza
        FROM ventacombinada vc
        inner join clientes as cli on cli.id=vc.idCliente
        LEFT join personal as per on per.personalId=vc.id_personal
        left join prefactura as pre on pre.ventaId=vc.combinadaId and pre.tipo=3
        WHERE vc.activo=1 and vc.viewed=1

    union

    SELECT 
        0 as estatus,
        pre.prefacturaId as id,
        1 prefactura,
        ren.telefono,
        ren.correo,
        cli.empresa,
        per.nombre vendedor,
        'rentas historial' as tabla,
        5 as tipotabla,
        rh.fechaentrega,
        rh.e_entrega,
        rh.e_entrega_p,
        ren.idCliente,
        ren.statuspago,
        rh.activo,
        rh.id as id_rh,
        rh.rentas as renta_rh,
        pre.reg,
        ren.id_personal,
        '' as equipo,
        '' as consumibles,
        '' as refacciones,
        '' as poliza
    FROM rentas_historial as rh
    INNER JOIN prefactura as pre on pre.ventaId=rh.id AND pre.tipo=4
    INNER JOIN rentas as ren on ren.id=rh.rentas
    INNER JOIN clientes as cli on cli.id=ren.idCliente
    LEFT join personal as per on per.personalId=ren.id_personal
    WHERE rh.activo=1 and rh.viewed=1
    /*
    casi misma estructura
    ventas_model->entregasventasnormal
    ventas_model->entregasventascombinada
    ventas_model->entregasrentas
    ventas_model->entregasrentashistorial
    */
--vista_entregas_dia_mes---------------------------------------------------------------------------------------------
    -- DROP VIEW vista_entregas_dia_mes
    create view vista_entregas_dia_mes as
    SELECT 
        v.estatus,
        v.id,
        v.prefactura,
        v.telefono,
        v.correo,
        cli.empresa,
        per.nombre as vendedor,
        'ventas' as tabla,
        1 as tipotabla,
        v.fechaentrega,
        v.fechaentregah,
        v.e_entrega,
        v.e_entrega_p,
        v.idCliente,
        v.statuspago,
        v.activo,
        0 as id_rh,
        0 as renta_rh,
        pre.reg,
        v.id_personal,
        '' as equipo,
        '' as consumibles,
        '' as refacciones,
        '' as poliza,
        v.tipov,
        v.entregado_fecha,
        v.not_dev,
        v.tec_ser,
        v.checada
        FROM ventas as v 
    inner join clientes as cli on cli.id=v.idCliente 
    LEFT join personal as per on per.personalId=v.id_personal 
    left join prefactura as pre on pre.ventaId=v.id and pre.tipo=0
    WHERE v.activo=1 and v.combinada=0 and v.viewed=1 and pre.reg>DATE_SUB(NOW(),INTERVAL '1' MONTH)

    union

    SELECT 
        v.estatus,
        v.id,
        v.prefactura,
        v.telefono,
        v.correo,
        cli.empresa,
        per.nombre vendedor,
        'rentas' tabla,
        2 as tipotabla,
        pre.vencimiento as fechaentrega,
        v.fechaentregah,
        v.e_entrega,
        v.e_entrega_p,
        v.idCliente,
        v.statuspago,
        1 as activo,
        0 as id_rh,
        0 as renta_rh,
        pre.reg,
        v.id_personal,
        pre.prefacturaId as equipo,
        '' as consumibles,
        '' as refacciones,
        '' as poliza,
        1 as tipov,
        v.entregado_fecha,
        0 as not_dev,
        v.tec_ser,
        v.checada
        from rentas as v
        inner join clientes as cli on cli.id=v.idCliente
        LEFT join personal as per on per.personalId=v.id_personal 
        left join prefactura as pre on pre.ventaId=v.id and pre.tipo=2
        WHERE v.cop_par=1 and v.estatus !=0 and v.viewed=1 and pre.reg>DATE_SUB(NOW(),INTERVAL '1' MONTH)

    
    union

    SELECT

        vc.estatus,
        vc.combinadaId id,
        vc.prefactura,
        vc.telefono,
        vc.correo,
        cli.empresa,
        per.nombre vendedor,
        'combinada' tabla,
        4 as tipotabla,
        vc.fechaentrega,
        vc.fechaentregah,
        vc.e_entrega,
        vc.e_entrega_p,
        vc.idCliente,
        vc.statuspago,
        vc.activo,
        0 as id_rh,
        0 as renta_rh,
        pre.reg,
        vc.id_personal,
        vc.equipo,
        vc.consumibles,
        vc.refacciones,
        vc.poliza,
        vc.tipov,
        vc.entregado_fecha,
        (SELECT pol.not_dev FROM polizasCreadas as pol WHERE pol.id=vc.poliza LIMIT 1) as not_dev,
        vc.tec_ser,
        vc.checada
        FROM ventacombinada vc
        inner join clientes as cli on cli.id=vc.idCliente
        LEFT join personal as per on per.personalId=vc.id_personal
        left join prefactura as pre on pre.ventaId=vc.combinadaId and pre.tipo=3
        WHERE vc.activo=1 and vc.viewed=1 and pre.reg>DATE_SUB(NOW(),INTERVAL '1' MONTH)

    union

    SELECT 
        0 as estatus,
        pre.prefacturaId as id,
        1 prefactura,
        ren.telefono,
        ren.correo,
        cli.empresa,
        per.nombre vendedor,
        'rentas historial' as tabla,
        5 as tipotabla,
        rh.fechaentrega,
        rh.fechaentregah,
        rh.e_entrega,
        rh.e_entrega_p,
        ren.idCliente,
        ren.statuspago,
        rh.activo,
        rh.id as id_rh,
        rh.rentas as renta_rh,
        pre.reg,
        ren.id_personal,
        '' as equipo,
        '' as consumibles,
        '' as refacciones,
        '' as poliza,
        1 as tipov,
        rh.entregado_fecha,
        0 as not_dev,
        rh.tec_ser,
        rh.checada
    FROM rentas_historial as rh
    INNER JOIN prefactura as pre on pre.ventaId=rh.id AND pre.tipo=4
    INNER JOIN rentas as ren on ren.id=rh.rentas
    INNER JOIN clientes as cli on cli.id=ren.idCliente
    LEFT join personal as per on per.personalId=ren.id_personal
    WHERE rh.activo=1 and rh.viewed=1 and pre.reg>DATE_SUB(NOW(),INTERVAL '1' MONTH)
    /*
    casi misma estructura sin la restriccion del todo lo de el ultimo mes
    ventas_model->entregasventasnormal
    ventas_model->entregasventascombinada
    ventas_model->entregasrentas
    ventas_model->entregasrentashistorial
    */
-------------------------------------------------------------------------------------

-- DROP VIEW vista_devoluciones
    create view vista_devoluciones as

    SELECT 
        v.idVentas,
        v.piezas,
        con.modelo,
        con.parte,
        '' as serie,
        per.nombre,
        per.apellido_paterno,
        per.apellido_materno,
        v.motivo_dev,
        v.reg_dev,
        1 as tipo,
        ven.idCliente,
        1 as vr,
        0 as row,
        0 as renta,
        0 as rentah
    FROM `ventas_has_detallesConsumibles_dev` as v
    INNER JOIN consumibles as con on con.id=v.idConsumible
    INNER JOIN personal as per on per.personalId=v.personal_dev
    inner join ventas as ven on ven.id=v.idVentas
    union

    SELECT 
        v.id_Venta as idVentas, 
        v.cantidad as piezas, 
        cac.nombre as modelo, 
        cac.no_parte as parte, 
        '' as serie, 
        per.nombre, 
        per.apellido_paterno, 
        per.apellido_materno, 
        v.motivo_dev, 
        v.reg_dev,
        2 as tipo,
        ven.idCliente,
        1 as vr,
        0 as row,
        0 as renta,
        0 as rentah
    FROM `ventas_has_detallesEquipos_accesorios_dev` as v 
    INNER JOIN catalogo_accesorios as cac on cac.id=v.id_accesorio 
    INNER JOIN personal as per on per.personalId=v.personal_dev
    inner join ventas as ven on ven.id=v.id_Venta
    union

    SELECT 
        v.idVenta as idVentas, 
        v.cantidad as piezas, 
        con.modelo, 
        con.parte, 
        '' as serie, 
        per.nombre, 
        per.apellido_paterno, 
        per.apellido_materno, 
        v.motivo_dev, 
        v.reg_dev,
        1 as tipo,
        ven.idCliente,
        1 as vr,
        0 as row,
        0 as renta,
        0 as rentah
    FROM `ventas_has_detallesequipos_consumible_dev` as v 
    INNER JOIN consumibles as con on con.id=v.id_consumibles 
    INNER JOIN personal as per on per.personalId=v.personal_dev
    inner join ventas as ven on ven.id=v.idVenta
    union

    SELECT 
        v.idVenta as idVentas, 
        v.cantidad as piezas, 
        eq.modelo, 
        eq.noparte as parte, 
        '' as serie, per.nombre, per.apellido_paterno, per.apellido_materno, v.motivo_dev, v.reg_dev,3 as tipo,ven.idCliente,1 as vr,0 as row,
        0 as renta,
        0 as rentah
        FROM `ventas_has_detallesEquipos_dev` as v 
        INNER JOIN equipos as eq on eq.id=v.idEquipo 
        INNER JOIN personal as per on per.personalId=v.personal_dev
        inner join ventas as ven on ven.id=v.idVenta

    union

    SELECT 
        v.idVentas, 
        v.piezas, 
        ref.nombre as modelo, 
        ref.codigo as parte, 
        '' as serie, per.nombre, per.apellido_paterno, per.apellido_materno, v.motivo_dev, v.reg_dev,4 as tipo,ven.idCliente,1 as vr,0 as row,
        0 as renta,
        0 as rentah
        FROM `ventas_has_detallesRefacciones_dev` as v 
        INNER JOIN refacciones as ref on ref.id=v.idRefaccion 
        INNER JOIN personal as per on per.personalId=v.personal_dev
        inner join ventas as ven on ven.id=v.idVentas
    union

    SELECT '' as idventas,readc.cantidad,ca.nombre as modelo, ca.no_parte as parte,'' as serie,per.nombre, 
        per.apellido_paterno, 
        per.apellido_materno, 
        readc.motivo_dev,
        readc.reg_dev,
        2 as tipo,
        ren.idCliente,
        0 as vr,
        readc.id_accesoriod as row,
        ren.id as renta,
        0 as rentah
        FROM rentas_has_detallesEquipos_accesorios_dev as readc
        INNER JOIN catalogo_accesorios as ca on ca.id=readc.id_accesorio
        INNER JOIN personal as per on per.personalId=readc.personal_dev
        INNER JOIN rentas as ren on ren.id=readc.idrentas
    union 
    SELECT 
        '' as idventas, 
        readc.cantidad, 
        co.modelo, 
        co.parte, 
        '' as serie, 
        per.nombre, 
        per.apellido_paterno, 
        per.apellido_materno, 
        readc.motivo_dev, 
        readc.reg_dev, 
        1 as tipo, 
        ren.idCliente,
        0 as vr,
        readc.id as row,
        ren.id as renta,
        0 as rentah
        FROM rentas_has_detallesequipos_consumible_dev as readc 
        INNER JOIN consumibles as co on co.id=readc.id_consumibles 
        INNER JOIN personal as per on per.personalId=readc.personal_dev 
        INNER JOIN rentas as ren on ren.id=readc.idrentas

-------------------------------------------------------------------------------------------------------------------------------------
-- DROP VIEW vista_de_entregas
    create view vista_de_entregas as
        SELECT 
        vdc.id,
        vdc.piezas as cantidad,
        con.modelo,
        con.parte as noparte,
        1 as tipo,
        vdc.entregado_status,
        vdc.entregado_fecha,
        vdc.entregado_personal 
    FROM ventas_has_detallesConsumibles as vdc 
    INNER JOIN consumibles as con on con.id=vdc.idConsumible 
    WHERE vdc.entregado_status>0
    UNION
    SELECT 
        vde.id,
        vde.cantidad,
        eq.modelo,
        eq.noparte,
        2 as tipo,
        vde.entregado_status, 
        vde.entregado_fecha, 
        vde.entregado_personal 
    FROM ventas_has_detallesEquipos as vde 
    INNER JOIN equipos as eq on eq.id=vde.idEquipo 
    WHERE vde.entregado_status>0
    UNION
    SELECT 
        vdea.id_accesoriod as id,
        vdea.cantidad,
        cacc.nombre as modelo,
        cacc.no_parte as noparte,
        3 as tipo, 
        vdea.entregado_status,
        vdea.entregado_fecha,
        vdea.entregado_personal 
    FROM ventas_has_detallesEquipos_accesorios as vdea 
    INNER JOIN catalogo_accesorios as cacc on cacc.id=vdea.id_accesorio 
    WHERE vdea.entregado_status>0
    UNION
    SELECT 
        vdr.id,
        vdr.piezas as cantidad,
        ref.nombre as modelo,
        ref.codigo as noparte,
        4 as tipo, 
        vdr.entregado_status, 
        vdr.entregado_fecha, 
        vdr.entregado_personal 
    FROM ventas_has_detallesRefacciones as vdr 
    INNER JOIN refacciones as ref on ref.id=vdr.idRefaccion 
    WHERE vdr.entregado_status>0
    UNION
    SELECT 
        rde.id,
        rde.cantidad,
        eq.modelo,
        eq.noparte,
        5 as tipo,
        rde.entregado_status,
        rde.entregado_fecha,
        rde.entregado_personal 
    FROM rentas_has_detallesEquipos as 
    rde INNER JOIN equipos as eq on eq.id=rde.idEquipo 
    WHERE rde.entregado_status>0
    UNION
    SELECT 
        rdea.id_accesoriod as id,
        rdea.cantidad, 
        cacc.nombre as modelo,
        cacc.no_parte as noparte,
        6 as tipo,
        rdea.entregado_status,
        rdea.entregado_fecha,
        rdea.entregado_personal 
    FROM rentas_has_detallesEquipos_accesorios as rdea 
    INNER JOIN catalogo_accesorios as cacc on cacc.id=rdea.id_accesorio 
    WHERE rdea.entregado_status>0
    UNION
    SELECT 
        rdec.id,
        rdec.cantidad,
        con.modelo,
        con.parte as noparte,
        7 as tipo,
        rdec.entregado_status,
        rdec.entregado_fecha,
        rdec.entregado_personal 
    FROM rentas_has_detallesequipos_consumible as rdec 
    INNER JOIN consumibles as con on con.id=rdec.id_consumibles 
    WHERE rdec.entregado_status>0

-----------------------------------------
    --DROP VIEW facturaspendientespago
    create view facturaspendientespago as
    SELECT * FROM(
    SELECT 
            fac.FacturasId,fac.serie,fac.Folio,fac.Clientes_ClientesId, 
            fac.Nombre as razonsocial,fac.Rfc,fac.total,fac.Estado,fac.fechatimbre,per.nombre as personal,fac.usuario_id, fac.pagada,cli.empresa,fac.rutaXml,fac.rutaAcuseCancelacion,fac.FormaPago,fac.uuid,
            fac.creada_sesionId,
            IFNULL((SELECT SUM(compd.ImpPagado)
                    FROM f_complementopago AS comp
                    inner join f_complementopago_documento as compd on compd.complementoId=comp.complementoId
                    where comp.Estado=1 and compd.facturasId=fac.FacturasId),0) as complementos,IFNULL((SELECT 
                        v.estatus 
                    FROM factura_venta as fv 
                    INNER JOIN ventas as v on v.id=fv.ventaId 
                    WHERE fv.facturaId=fac.FacturasId AND fv.combinada=0 LIMIT 1),0) ventas,
                    IFNULL((SELECT 
                        v.estatus 
                    FROM factura_venta as fv 
                    INNER JOIN ventacombinada as v on v.combinadaId=fv.ventaId 
                    WHERE fv.facturaId=fac.FacturasId AND fv.combinada=1 LIMIT 1),0) ventasc,
                    IFNULL((SELECT 
                        pol.estatus 
                    FROM factura_poliza as fpol 
                    INNER JOIN polizasCreadas as pol on pol.id=fpol.polizaId 
                    WHERE fpol.facturaId=fac.FacturasId LIMIT 1),0) poliza,
                    IFNULL((SELECT 
                        facr.statuspago 
                    FROM factura_prefactura as facr 
                    WHERE facr.facturaId=fac.FacturasId LIMIT 1),0) rentas
    FROM f_facturas as fac 
    INNER JOIN personal as per on per.personalId=fac.usuario_id
    inner join clientes as cli on cli.id=fac.Clientes_ClientesId
    WHERE fac.Estado=1 and fac.fechatimbre>DATE_SUB(NOW(),INTERVAL '1' YEAR)
    ) datosfac 
    WHERE total>complementos and ventas!=3 and ventasc!=3 and poliza!=3 and pagada!=1 and rentas!=1
    ORDER BY FacturasId  DESC
---------
create view vista_tecnicos as
SELECT per.personalId, per.nombre, per.apellido_paterno, per.apellido_materno, usu.perfilId 
                            FROM personal as per
                            INNER JOIN usuarios as usu on usu.personalId=per.personalId
                            WHERE per.estatus=1 AND (usu.perfilId=5 or usu.perfilId=12 or per.personalId=29 or per.personalId=19 or per.personalId=39) 
                            GROUP BY per.personalId;
-----------------------------------
ALTER TABLE `cotizaciones` ADD `prioridad` TINYINT(1) NULL DEFAULT '1' AFTER `cot_cliente`;
ALTER TABLE `cotizaciones` ADD `tipocot` INT NOT NULL DEFAULT '0' AFTER `prioridad`;
ALTER TABLE `cotizaciones` ADD `pre_envio` TINYINT NULL DEFAULT '0' AFTER `tipocot`;
