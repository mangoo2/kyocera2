<?php 
	if(isset($_GET['viewexcel'])){
		$start_time = microtime(true);
	}else{
		header("Pragma: public");
		header("Expires: 0");
		$filename = "ventas.xls";
		header("Content-type: application/x-msdownload");
		header("Content-Disposition: attachment; filename=$filename");
		header("Pragma: no-cache");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	}
	
?>
<table border="1">
	<thead>
		<tr>
			<th></th>
			<th>No</th>
			<th>Cliente</th>
			<th>Fecha creada</th>
			<th>Fecha Entrega</th>
			<th>Estatus</th>
			<th>Vendedor</th>
			<th>Subtotal</th>
			<th>Iva</th>
			<th>Total</th>
			<th>Saldo</th>
			<th></th>
			<th>
				<table>
					<tr>
						<td>Cantidad</td>
						<td>Modelo</td>
						<td>Serie</td>
					</tr>
				</table>
			</th>
			<th>Factura</th>
			<th>
				<table>
					<tr>
						<td>Fecha de pago</td>
						<td>Metodo de pago</td>
						<td>Monto pagado</td>
					</tr>
				</table>
			</th>
		</tr>
	</thead>
	<tbody>
		<?php 
			$html='';
			$vendedorid=$vendedor;
			if($tipovp==1 or $tipovp==0){
				$queryv = $this->ModeloCatalogos->reporteventasnormal($fechaini,$fechafin,$cliente,$vendedorid,$tipof);
				foreach ($queryv->result() as $itemv) {
					$vendedor=$itemv->nombre.' '.$itemv->apellido_paterno.' '.$itemv->apellido_materno;
					$FacturasId=$itemv->FacturasId;
					$montopagado=0;
					if($itemv->subtotal_general>0){//si ya se tienen los montos de subtotal, iva y total directamente en la prefactura
						$monto=$itemv->total_general;
						$subtotalg=$itemv->subtotal_general;
						$ivag=$itemv->iva_general;
					}else{//si no se tienen los montos
						$info_montos = $this->ModeloCatalogos->montoventas4($itemv->id);
						$monto=$info_montos['total'];
						$subtotalg=$info_montos['subtotal'];
						$ivag=$info_montos['iva'];
					}

					$resultadoproductos = $this->ModeloCatalogos->productosventa_union($itemv->id);
					if($FacturasId>0){
						$ventapagos = $this->ModeloCatalogos->pagos_v_vc_p_factura($FacturasId);//verificar para que este sustituya a ModeloCatalogos->table_get_tipo_compra para no hacer tantas consultas
						foreach ($ventapagos as $itemp) { 
							$montopagado=$montopagado+round($itemp->pago,2);
						}
						$resultvcp_cp = $this->ModeloCatalogos->obteciondecomplememtofactura($FacturasId);
						foreach ($resultvcp_cp->result() as $itemcp) {
                            $montopagado=$montopagado+round($itemcp->ImpPagado,2);
                        }
					}else{
						$ventapagos = $this->ModeloCatalogos->pagos_ventas($itemv->id);
						foreach ($ventapagos as $itemp) {
							$montopagado=$montopagado+round($itemp->pago,2);
						}
					}
					
					/*
					if($monto>0){
						$montopagado =$this->ModeloCatalogos->table_get_tipo_compra($itemv->id,0);	
					}else{
						$montopagado=0;
					}
					*/
					//if($FacturasId>0){
						//$resultvcp_cp = $this->ModeloCatalogos->obteciondecomplememtofactura($FacturasId);
                        //foreach ($resultvcp_cp->result() as $itemcp) { // no aplica ya se esta consultando arriba
                            //$montopagado=$montopagado+$itemcp->ImpPagado; 
                        //}
					//}
					$saldo=$monto-$montopagado;
					if($saldo<0){
						$saldo=0;
					}
					if($saldo>0){
						$saldostyle='style="background-color:red; color:white;"';
					}else{
						$saldostyle='';
					}
					
					
					$html.='<tr>';
						$html.='<td>Venta</td>';
						$html.='<td>'.$itemv->id.'</td>';
						$html.='<td>'.$itemv->empresa.'</td>';
						$html.='<td>'.$itemv->reg.'</td>';
						$html.='<td>'.$itemv->fechaentrega.'</td>';
						$html.='<td>';
								if($itemv->activo==1){ 
									$html.='vigente';
								}else{ 
									$html.='Eliminado';
								} 
						$html.='</td>';
						$html.='<td>'.$vendedor.'</td>';
						$html.='<td>$ '.number_format($subtotalg,2).'</td>';
						$html.='<td>$ '.number_format($ivag,2).'</td>';
						$html.='<td '.$saldostyle.'>$ '.number_format($monto, 2, '.', ',').'</td>';
						$html.='<td '.$saldostyle.' class="monto:'.$montopagado.'">$ '.number_format($saldo, 2, '.', ',').'</td>';
						$html.='<td></td>';
						$html.='<td>';
							$html.='<table>';
									//foreach($resultadoequipos->result() as $item_eq){
										/*
										$resuleserie=$this->ModeloCatalogos->getequiposerie($item->id);
										$arrayseries=array();
										foreach ($resuleserie->result() as $itemse) { 
											$arrayseries[]=$itemse->serie;
										}
										$arrayseriesall =implode("  ", $arrayseries);
										*/
									//	$html.="<tr><td>$item_eq->cantidad</td><td>$item_eq->modelo</td><td>$item_eq->series</td></tr>";
									//}
									//foreach($resultadoaccesorios->result() as $item){
										/*
										$resuleserie=$this->ModeloCatalogos->getaccesorioserie($item->id_accesoriod);
														$arrayseries=array();
														foreach ($resuleserie->result() as $itemse) { 
															$arrayseries[]=$itemse->serie;
														}
														$arrayseriesall =implode("  ", $arrayseries);
													*/

									//	$html.="<tr><td>$item->cantidad</td><td>$item->nombre</td><td>$item->series</td></tr>";
									//}
									//foreach($resultadoconsumibles->result() as $item){
									//	$html.="<tr><td>$item->cantidad</td><td>$item->modelo</td><td></td></tr>";
									//}
									foreach($resultadoproductos->result() as $item){
										$html.="<tr><td>$item->cantidad</td><td>$item->modelo</td><td>$item->series</td></tr>";
									}
									/*
									foreach($consumiblesventadetalles->result() as $item){
										$html.="<tr><td>consu 2 $item->cantidad</td><td>$item->modelo</td><td></td></tr>";
									}*/
									//foreach($ventadetallesrefacion->result() as $item){
										/*
										$resuleserie=$this->ModeloCatalogos->getrefaccionserie($item->id);
										$arrayseries=array();
										foreach ($resuleserie->result() as $itemse) { 
											$arrayseries[]=$itemse->serie;
										}
										$arrayseriesall =implode("  ", $arrayseries);
										*/
									//	$html.="<tr><td>$item->cantidad</td><td>$item->modelo</td><td>$item->series</td></tr>";
									//}
							$html.='</table>';
						$html.='</td>';
						$html.='<td class="fac: '.$FacturasId.'">'.$itemv->serie.' '.$itemv->Folio.'</td>';
						$html.='<td>';
							$html.='<table>';
								foreach ($ventapagos as $itemp) { 
									$html.='<tr><td>'.$itemp->fecha.'</td><td>'.$itemp->formapago_text.'</td><td>'.$itemp->pago.'</td></tr>';
								} 
								if($FacturasId>0){
			                        foreach ($resultvcp_cp->result() as $itemcp) {
			                            $html.='<tr><td>'.$itemcp->Fecha.'</td><td>Complemento</td><td>'.$itemcp->ImpPagado.'</td></tr>';
			                        }
								}
								
							$html.='</table>';
						$html.='</td>';
						
					$html.='</tr>';
					
				}
			}
			if($tipovp==2 or $tipovp==0){
				$queryvc = $this->ModeloCatalogos->reporteventasconbinada($fechaini,$fechafin,$cliente,$vendedorid,$tipof);
				foreach ($queryvc->result() as $itemvv) {
					$FacturasId = $itemvv->FacturasId;
					$montopagado=0;
					if($FacturasId>0){
						$ventapagosc = $this->ModeloCatalogos->pagos_v_vc_p_factura($FacturasId);//verificar para que este sustituya a ModeloCatalogos->table_get_tipo_compra para no hacer tantas consultas
						foreach ($ventapagosc as $itemp) { 
							$montopagado=$montopagado+round($itemp->pago,2);
						}
						$resultvcp_cp = $this->ModeloCatalogos->obteciondecomplememtofactura($FacturasId);
						foreach ($resultvcp_cp->result() as $itemcp) {
                            $montopagado=$montopagado+round($itemcp->ImpPagado,2);
                        }
					}else{
						$ventapagosc = $this->ModeloCatalogos->pagos_combinada($itemvv->combinadaId); 
						foreach ($ventapagosc as $itemp) {
							$montopagado=$montopagado+round($itemp->pago,2);
						}
					}
					
					if($itemvv->subtotal_general>0){
						$monto=$itemvv->total_general;
						$subtotalg=$itemvv->subtotal_general;
						$ivag=$itemvv->iva_general;	
					}else{
						$info_montos = $this->ModeloCatalogos->montocombinada3($itemvv->combinadaId);
						$monto=$info_montos['total'];
						$subtotalg=$info_montos['subtotal'];
						$ivag=$info_montos['iva'];	
					}
					
					/*
					if($monto>0){
						$montopagado =$this->ModeloCatalogos->table_get_tipo_compra($itemvv->combinadaId,1);	
					}else{
						$montopagado=0;
					}
					*/
					$saldo=round($monto,2)-round($montopagado,2);
					if($saldo<0){
						$saldo=0;
					}
					if($saldo>0){
						$saldostyle='style="background-color:red; color:white;"';
					}else{
						$saldostyle='';
					}
					$idvc=$itemvv->combinadaId;
	 				if($itemvv->equipo>0){
	                    $idvc=$itemvv->equipo;
	                }elseif($itemvv->consumibles>0){
	                    $idvc=$itemvv->consumibles;
	                }elseif($itemvv->refacciones>0){
	                    $idvc=$itemvv->refacciones;
	                }elseif($itemvv->poliza>0){
	                    $idvc=$itemvv->poliza;
	                }
	                //$resultadoequipos = $this->ModeloCatalogos->db6_getselectwheren('ventas_has_detallesEquipos',array('idVenta'=>$itemvv->equipo));
	                //$resultadoequipos = $this->ModeloCatalogos->getequiposerie_venta($itemvv->equipo);
					//$resultadoaccesorios = $this->ModeloCatalogos->accesoriosventa($itemvv->equipo);
					//$resultadoconsumibles = $this->ModeloCatalogos->consumiblesventa($itemvv->equipo);
					//$consumiblesventadetalles = $this->ModeloCatalogos->consumiblesventadetalles($itemvv->consumibles);
					//$resultadoconsumibles = $this->ModeloCatalogos->consumiblesventa_union($itemvv->equipo);

					//$ventadetallesrefacion = $this->ModeloCatalogos->ventas_has_detallesRefacciones_mini($itemvv->refacciones);
					$polizaventadetalles = $this->ModeloCatalogos->polizaventadetalles3($itemvv->poliza);
					$resultadoproductos = $this->ModeloCatalogos->productosventa_union($itemvv->equipo);
					
					$html.='<tr>';
						$html.='<td>Venta combinada</td>';
						$html.='<td>'.$idvc.'</td>';
						$html.='<td>'.$itemvv->empresa.'</td>';
						$html.='<td>'.$itemvv->reg.'</td>';
						$html.='<td>'.$itemvv->fechaentrega.'</td>';
						$html.='<td>';
								if($itemvv->activo==1){ 
									$html.='vigente';
								}else{ 
									$html.='Eliminado';
									}
						$html.='</td>';
						$html.='<td>'.$itemvv->nombre.' '.$itemvv->apellido_paterno.' '.$itemvv->apellido_materno.'</td>';
						$html.='<td>$ '.number_format($subtotalg,2).'</td>';
						$html.='<td>$ '.number_format($ivag,2).'</td>';
						$html.='<td '.$saldostyle.'>$ '.number_format($monto, 2, '.', ',').'</td>';
						$html.='<td '.$saldostyle.'><!--'.$itemvv->combinadaId.'----'.$monto.'-- $montopagado : '.$montopagado.'saldo:'.$saldo.' -->$ '.number_format($saldo, 2, '.', ',').'</td>';
						$html.='<td></td>';
						$html.='<td>';
							$html.='<table>';
								
								
									//foreach($resultadoequipos->result() as $item_eq){
										/*
										$resuleserie=$this->ModeloCatalogos->getequiposerie($item->id);
										$arrayseries=array();
										foreach ($resuleserie->result() as $itemse) { 
											$arrayseries[]=$itemse->serie;
										}
										$arrayseriesall =implode("  ", $arrayseries);
										*/
										//$html.="<tr><td>$item_eq->cantidad</td><td>$item_eq->modelo</td><td>$item_eq->series</td></tr>";
									//}
									//foreach($resultadoaccesorios->result() as $item){
										/*
										$resuleserie=$this->ModeloCatalogos->getaccesorioserie($item->id_accesoriod);
														$arrayseries=array();
														foreach ($resuleserie->result() as $itemse) { 
															$arrayseries[]=$itemse->serie;
														}
														$arrayseriesall =implode("  ", $arrayseries);
										*/
													

										//$html.="<tr><td>$item->cantidad</td><td>$item->nombre</td><td>$item->series</td></tr>";
									//}
									//foreach($resultadoconsumibles->result() as $item){
										//$html.="<tr><td>$item->cantidad</td><td>$item->modelo</td><td></td></tr>";
									//}
									/*
									foreach($consumiblesventadetalles->result() as $item){
										$html.="<tr><td>consu 2 $item->cantidad</td><td>$item->modelo</td><td></td></tr>";
									}
									*/
									//foreach($ventadetallesrefacion->result() as $item){
										/*
										$resuleserie=$this->ModeloCatalogos->getrefaccionserie($item->id);
										$arrayseries=array();
										foreach ($resuleserie->result() as $itemse) { 
											$arrayseries[]=$itemse->serie;
										}
										$arrayseriesall =implode("  ", $arrayseries);
										*/
										//$html.="<tr><td>$item->cantidad</td><td>$item->modelo</td><td>$item->series</td></tr>";
									//}
									foreach($resultadoproductos->result() as $item){
										$html.="<tr><td>$item->cantidad</td><td>$item->modelo</td><td>$item->series</td></tr>";
									}
									foreach($polizaventadetalles->result() as $item){
										$html.="<tr><td>$item->cantidad</td><td>".utf8_decode($item->nombre)."</td><td></td></tr>";
									}
									
								
							$html.='</table>';
						$html.='</td>';
						$html.='<td class="fac: '.$FacturasId.'">'.$itemvv->serie.' '.$itemvv->Folio.'</td>';
						$html.='<td>';
							$html.='<table>';
								foreach ($ventapagosc as $itemp) {
									$html.='<tr><td>'.$itemp->fecha.'</td><td>'.$itemp->formapago_text.'</td><td>'.$itemp->pago.'</td></tr>';
								}
								if($FacturasId>0){
			                        foreach ($resultvcp_cp->result() as $itemcp) {
			                            $html.='<tr><td>'.$itemcp->Fecha.'</td><td>Complemento</td><td>'.$itemcp->ImpPagado.'</td></tr>';
			                        }
								}								
							$html.='</table>';
						$html.='</td>';
					$html.='</tr>';
					
				}
			}
			if($tipovp==3 or $tipovp==0){
				$queryv = $this->ModeloCatalogos->reporteventaspoliza($fechaini,$fechafin,$cliente,$vendedorid,$tipof);
				foreach ($queryv->result() as $itemv) {
					$FacturasId = $itemv->FacturasId;
					$montopagado=0;
					if($FacturasId>0){
						$polizapagos = $this->ModeloCatalogos->pagos_v_vc_p_factura($FacturasId);//verificar para que este sustituya a ModeloCatalogos->table_get_tipo_compra para no hacer tantas consultas
						foreach ($polizapagos as $itemp) { 
							$montopagado=$montopagado+$itemp->pago;
						} 
						$resultvcp_cp = $this->ModeloCatalogos->obteciondecomplememtofactura($FacturasId);
                        foreach ($resultvcp_cp->result() as $itemcp) {
                            $montopagado=$montopagado+$itemcp->ImpPagado;
                        }
					}else{
						$polizapagos = $this->ModeloCatalogos->pagos_poliza($itemv->id); 
						foreach ($polizapagos as $itemp) { 
							$montopagado=$montopagado+$itemp->pago;
						} 
					}
					
					$polizaventadetalles = $this->ModeloCatalogos->polizaventadetalles3($itemv->id);
					if($itemv->subtotal>0){
						$monto=$itemv->total;
						$subtotalg=$itemv->subtotal;
						$ivag=$itemv->iva;
					}else{
						$info_montos = $this->ModeloCatalogos->montopoliza2($itemv->id);
						$monto=$info_montos['total'];
						$subtotalg=$info_montos['subtotal'];
						$ivag=$info_montos['iva'];
					}
					
					/*
					if($monto>0){
						$montopagado =$this->ModeloCatalogos->table_get_tipo_compra_poliza($itemv->id);	
					}else{
						$montopagado=0;
					}
					*/
					
					$saldo=$monto-$montopagado;
					if($saldo<0){
						$saldo=0;
					}
					if($saldo>0){
						$saldostyle='style="background-color:red; color:white;"';
					}else{
						$saldostyle='';
					}
					
					$html.='<tr>';
						$html.='<td>Poliza</td>';
						$html.='<td>'.$itemv->id.'</td>';
						$html.='<td>'.$itemv->empresa.'</td>';
						$html.='<td>'.$itemv->reg.'</td>';
						$html.='<td>'.$itemv->fechaentrega.'</td>';
						$html.='<td>';
								if($itemv->activo==1){ 
									$html.='vigente';
								}else{ 
									$html.='Eliminado';
									}
						$html.='</td>';
						$html.='<td>'.$itemv->nombre.' '.$itemv->apellido_paterno.' '.$itemv->apellido_materno.'</td>';
						$html.='<td>$ '.number_format($subtotalg,2).'</td>';
						$html.='<td>$ '.number_format($ivag,2).'</td>';
						$html.='<td '.$saldostyle.'>$ '.number_format($monto, 2, '.', ',').'</td>';
						$html.='<td '.$saldostyle.'>$ '.number_format($saldo, 2, '.', ',').'</td>';
						$html.='<td></td>';
						$html.='<td>';
							$html.='<table>';
								
								foreach($polizaventadetalles->result() as $item){
										$html.="<tr><td>$item->cantidad</td><td>".utf8_decode($item->nombre)."</td><td></td></tr>";
									}
								
							$html.='</table>';
						$html.='</td>';
						$html.='<td class="fac: '.$FacturasId.'">'.$itemv->serie.' '.$itemv->Folio.'</td>';
						$html.='<td>';
							$html.='<table>';
								foreach ($polizapagos as $itemp) { 
									$html.='<tr><td>'.$itemp->fecha.'</td><td>'.$itemp->formapago_text.'</td><td>'.$itemp->pago.'</td></tr>';
								} 
								if($FacturasId>0){
			                        foreach ($resultvcp_cp->result() as $itemcp) {
			                            $html.='<tr><td>'.$itemcp->Fecha.'</td><td>Complemento</td><td>'.$itemcp->ImpPagado.'</td></tr>';
			                        }
								}	
								
							$html.='</table>';
						$html.='</td>';
					$html.='</tr>';
					
				}
			}
			echo $html;
		?>
	</tbody>
</table>
<?php 
	if(isset($_GET['viewexcel'])){
		// Al final del script
		$end_time = microtime(true);

		// Calcula el tiempo de carga
		$load_time = $end_time - $start_time;

		echo "Tiempo de carga de la página: " . $load_time . " segundos.";
	}
	/*
		// Al final del script
	$end_time = microtime(true);

	// Calcula el tiempo de carga
	$load_time = $end_time - $start_time;

	echo "Tiempo de carga de la página: " . $load_time . " segundos.";
	*/
?>