<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Consumiblessol extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloCatalogos');
        $this->load->model('Modelosolicitudcons');
        $this->load->model('Consumibles_model');
        $this->submenu=74;
        if($this->session->userdata('logeado')==true){
            $this->perfilid =$this->session->userdata('perfilid');
            $this->idpersonal = $this->session->userdata('idpersonal');
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,$this->submenu);// 74 es el id del submenu
            if ($permiso==0) {
                redirect('Sistema');
            }
        }else{
            redirect('Sistema'); 
        }
    }

    public function index(){
        $data['MenusubId']=$this->submenu;
        $data['get_bodegas']=$this->ModeloCatalogos->view_session_bodegas();
    	$this->load->view('header');
        $this->load->view('main');
        $this->load->view('consumibles/sol_consumibles',$data);
        $this->load->view('footer');
        $this->load->view('consumibles/sol_consumiblesjs');
    }
    public function getListado() {
        $params = $this->input->post();

        $getdata = $this->Modelosolicitudcons->getlist($params);
        $totaldata= $this->Modelosolicitudcons->getlisttotal($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    function delete_sol(){
        $params = $this->input->post();
        $id = $params['id'];
        $motivo = $params['motivo'];
        $this->ModeloCatalogos->updateCatalogo('consumibles_solicitud',array('activo'=>0,'comen_delete'=>$motivo,'personal_delete'=>$this->idpersonal),array('id'=>$id));
    }
    function edit_sol(){
        $params = $this->input->post();
        $id = $params['id'];
        $can = $params['can'];
        $this->ModeloCatalogos->updateCatalogo('consumibles_solicitud',array('cantidad'=>$can),array('id'=>$id));
    }
    function view_sol(){
        $params = $this->input->post();
        $idsol = $params['idsol'];
        $idenvio = $params['idenvio'];
        $html='';
        $strqcont = "SELECT emc.contratoid,con.idRenta,cli.empresa,con.folio FROM envio_mail_contrato as emc 
                            INNER JOIN contrato as con on con.idcontrato=emc.contratoid
                            INNER JOIN rentas as ren on ren.id=con.idRenta
                            INNER JOIN clientes as cli on cli.id=ren.idCliente
                            WHERE emc.idenvio='$idenvio'";
        $query_cont = $this->db->query($strqcont);
        foreach ($query_cont->result() as $item) {
            $html.='<div class="row"><div class="col s2 conred">Contrato</div><div class="col s10">'.$item->empresa.' ('.$item->folio.')</div></div>';
        }
        $result=$this->ModeloCatalogos->getselectwheren('consumibles_solicitud',array('id'=>$idsol));
        foreach ($result->result() as $item) {
            $html.='<div class="row"><div class="col s2 conred">Solicitud</div><div class="col s10">'.$item->comen.'</div></div>';
        }
        echo $html;
    }
    function verificarstock(){
        $params = $this->input->post();
        $conid = $params['conid'];
        $bod = $params['bod'];
        $result=$this->Consumibles_model->getconsumiblesstock($conid,$bod);
        $stock=0;
        foreach ($result->result() as $item) {
            if ($item->stock>0) {
                $stock=$item->stock;
            }else{
                $stock=0;
            }
            if ($item->stock_resg>0) {
                $stock_resg=$item->stock_resg;
            }else{
                $stock_resg=0;
            }
            $stock=$stock-$stock_resg;
        }
        echo $stock;
    }
}
?>
