<script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/loading/jquery.loading.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/plugins/loading/demo.css">

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/select2.min.css">
<script src="<?php echo base_url(); ?>/public/js/select2.full.min.js" type="text/javascript"></script>

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/plugins/toastr/toastr.css">
<script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/toastr/toastr.min.js" ></script>

<script type="text/javascript" src="<?php echo base_url(); ?>public/js/asignacionesservicio.js?v=<?php echo date('YmdGis');?>"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>public/js/asignacionesservicio_cliente.js?v=<?php echo date('YmdGis');?>"></script>
<?php if (isset($_GET['tasign'])) { ?>
	<script type="text/javascript">
		$(document).ready(function($) {
			setTimeout(function(){ 
				$('input[name=tasign][value=<?php echo $_GET["tasign"];?>]').prop("checked", true).change();
				$('#pcliente').html('<option value="<?php echo $_GET["cliente"];?>"></option>');
				setTimeout(function(){ 
					$('input[name=prasignaciontipo][value=<?php echo $_GET["prasignaciontipo"];?>]').prop("checked", true).change();
					<?php if($_GET["prasignaciontipo"]==4){ ?>
					
						filtrarprefacturasventas();
					<?php } ?>
					<?php if($_GET["prasignaciontipo"]==2){ ?>
					setTimeout(function(){ 
						datospoliza(<?php echo $_GET["servicio"];?>);
					}, 2400);
					<?php } ?>
				}, 900);
			}, 1500);
			filtrarprefacturas();
		});
		function filtrarprefacturasventas(){
			setTimeout(function(){ 
				$('.deplegaracciones_<?php echo $_GET["servicio"];?>_<?php echo $_GET["tipo"];?>_<?php echo $_GET["cliente"];?>').click();
				$('.ventaservicio_<?php echo $_GET["servicio"];?>_<?php echo $_GET["tipo"];?>_<?php echo $_GET["cliente"];?>').removeClass('ventaserviciocheck');
				$('.ventaserviciocheck').remove();

			}, 2000);
		}
	</script>	
<?php } ?>