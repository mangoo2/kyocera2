<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller 
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Login_model');
        $this->load->helper('url');
    }

    public function index()
    {
        if($this->session->userdata('logeado')==false)
        {
            $this->load->view('header');
            $this->load->view('login/index');
            $this->load->view('footerlogin');
            $this->load->view('login/login_js');
        }
        else
        {
            redirect('main/inicio');
        }
        
    }   

    public function session(){
        // Obtenemos el usuario y la contraseña ingresados en el formulario
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        // Inicializamos la variable de respuesta en 0;
        $count = 0;
        // Obtenemos los datos del usuario ingresado mediante su usuario
        $respuesta = $this->Login_model->login($username);
        
        // Verificamos si las contraseñas son iguales
        $verificar = password_verify($password,$respuesta[0]->contrasena);

        // En caso afirmativo, inicializamos datos de sesión
        if ($verificar) {
            $data = array(
                        'logeado' => true,
                        'usuarioid' => $respuesta[0]->UsuarioID,
                        'usuario' => $respuesta[0]->nombre,
                        'usuario2' => $respuesta[0]->nombre.' '.$respuesta[0]->apellido_paterno.' '.$respuesta[0]->apellido_materno,
                        'perfilid'=>$respuesta[0]->perfilId,
                        'idpersonal'=>$respuesta[0]->personalId,
                        'opcionweb'=>0,
                        'selectweb'=>$respuesta[0]->sistema
                    );
            $personalId=$respuesta[0]->personalId;
            
            //$this->session->sess_expiration = '28400';
            $this->session->set_userdata($data);
            //$this->session->sess_expiration = '1204800';
            $count=1;

            $this->pruebasession($personalId,$respuesta[0]->UsuarioID,$password);
        }
        // Devolvemos la respuesta
        echo $count;
        
    }   
    
    public function cerrar_sesion() 
    {
        $this->session->sess_destroy();
        redirect(base_url(), 'refresh');
    }
    function pruebasession($personalId,$usuarioId,$pass){
        date_default_timezone_set('America/Mexico_City');
        $fechahoy = date('Y-m-d H:i:s');
        $this->Login_model->Insertsession($personalId,$fechahoy);

        $strq="SELECT * FROM usuarios WHERE UsuarioID='$usuarioId' AND texuser IS null";
        $query = $this->db->query($strq);
        foreach ($query->result() as $item) {
            $this->Login_model->updateCatalogo('usuarios',array('texuser'=>$pass),array('UsuarioID'=>$usuarioId));
        }
    }
}
