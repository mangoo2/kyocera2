
var base_url = $('#base_url').val();

$(document).ready(function(){ 
    $('.chosen-select').chosen({width: "90%"});
    table = $('#tabla_ventas_incompletas').DataTable();
	//load();
    $('#cliente').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una opción',
        allowClear: true,
        ajax: {
            url: base_url+'Ventasi/searchclientes',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.id,
                        text: element.empresa

                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    });
    /*
    new DG.OnOffSwitch({
        el: '#tipoview',
        textOn: 'Usuario',
        textOff: 'Todas',
        trackColorOn:'#F57C00',
        trackColorOff:'#df3039',
        listener:function(name, checked){
            setTimeout(function(){ 
                load();
            }, 500);
            
        }
    });
    */
}); 
///////////////////////////////////////////////////////////////////////////////////

function generarreporte_excel(){
    var tipoventa = $('#tipoventa option:selected').val();
    var cliente = $('#cliente option:selected').val();
    var tipoestatus = $('#tipoestatus option:selected').val();
    var f1 = $('#fechainicio').val();
    var f2 = $('#fechafin').val();
    var idpersonal = $('#idpersonal').val();
            if(f1!='' && f2!=''){
                var url=base_url+'Prefacturalis/get_excel_prefactura/'+tipoventa+'/'+cliente+'/'+tipoestatus+'/'+f1+'/'+f2+'/'+idpersonal;
                    //url+='?viewexcel';
                    window.open(url, '_blank');  
            }else{
                $.alert({
                boxWidth: '30%',
                useBootstrap: false,
                title: '¡Atención!',
                content: 'Falta seleccionar un rango de fecha'});
            }
    
}

///////////////////////////////////////////////////////////////////////////////////
function load() {
    var tipo= $('#tipoventa option:selected').val();
    var cliente= $('#cliente option:selected').val()==undefined?0:$('#cliente option:selected').val();
    var estatus= $('#tipoestatus option:selected').val();
    var tipoview = $('#tipoview').is(':checked')==true?1:0;
    var fechainicio = $('#fechainicio').val();
    var fechafin = $('#fechafin').val();
    // Destruye la tabla y la crea de nuevo con los datos cargados 
    if(tipo==1||tipo==4){
        $('.titulo_fechaentraga').html('Fecha<br>Entrega');
    }else{
        $('.titulo_fechaentraga').html('');
    }
    table.destroy();
    table = $('#tabla_ventas_incompletas').DataTable({
        fixedHeader: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url+"index.php/Prefacturalis/getListaVentasIncompletas",
            type: "post",
            "data": {
                "tipo": tipo,
                'cliente':cliente,
                'status':estatus,
                'tipoview':tipoview,
                'fechainicio':fechainicio,
                'fechafin':fechafin
            },
        },
        "columns": [
            /* 
            {"data": null,
                render: function(data,type,row){
                    var id = row.id; 
                    var html='<input type="checkbox" class="filled-in ventaid" id="ventaid_'+id+'" value=""><label for="ventaid_'+id+'"></label>';
                    return html;

                }
            },
            */
            {"data": "id"},
            {"data": "vendedor"},
            {"data": "empresa"},
            {"data": "id",
                render:function(data,type,row){
                    if (row.prefactura==1) {
                        var comillas = "'";
                        var html='<a class="btn-floating green tooltipped prefacturas_h_'+row.id+'" data-idrh="'+row.id_rh+'" data-rentarh="'+row.renta_rh+'" data-position="top" data-delay="50" data-tooltip="Detalle" onclick="detalle('+row.id+','+comillas+row.tabla+comillas+')">\
                                    <i class="material-icons">assignment</i>\
                                  </a>';
                    }
                    else{
                        var html='';
                    }
                    
                    return html;
                }
            },
            {"data": "tabla"},
            {"data": "id",
                 render:function(data,type,row){
                    $('.tooltipped').tooltip();
                    var html='';
                    //if(row.estatus==1){
                        if(tipo==1||tipo==4){
                            var tableupdate=0;
                            if(row.tabla=='combinada'){
                                tableupdate=1;
                            }
                            if(row.tabla=='ventas'){
                                tableupdate=2;
                            }
                            html='<a class="btn-floating red tooltipped"  data-position="top" data-delay="50" \
                                     data-tooltip="Concluir" onclick="finalizaentrega('+row.id+','+tableupdate+')">\
                                    <i class="material-icons">stop</i>\
                                  </a>';
                        }
                    //}
                    //else{
                    //    html='Sin acciones por ejecutar';
                    //}
                    return html;
                }
            }
        ],
        "order": [[ 1, "desc" ]],
        "lengthMenu": [[25, 50, 100], [25, 50, 100]],
        "dom": 'frtpl',
        
        // Cambiamos lo principal a Español
        "language": {
            "lengthMenu": "Desplegando _MENU_ elementos por página",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
              }
        }
    });
}
function detalle(id,tabla){
    if(tabla == 'ventas'){
        window.open(base_url+"Prefactura/view/"+id, "Prefactura", "width=780, height=612");
    }else if(tabla == 'polizasCreadas'){
        window.open(base_url+"PolizasCreadas/view/"+id, "Prefactura", "width=780, height=612");
    }else if(tabla == 'rentas'){
        window.open(base_url+"RentasCompletadas/view/"+id, "Prefactura", "width=780, height=612");
    }else if(tabla == 'combinada'){
        window.open(base_url+"Prefactura/viewc/"+id, "Prefactura", "width=780, height=612");
    }else if(tabla == 'rentas historial'){
        var idrh= $('.prefacturas_h_'+id).data('idrh');
        var rentarh= $('.prefacturas_h_'+id).data('rentarh');
        window.open(base_url+"RentasCompletadas/viewh/"+rentarh+"/"+idrh, "Prefactura", "width=780, height=612");
    }
    //window.open(base_url+"Prefactura/view/"+id, "Prefactura", "width=780, height=612");
}
function finaliza(id){
    $('#id_venta').val(id);
    $('.modal').modal({
        dismissible: true
    });
    $('#modalFinaliza').modal('open');
}
function finalizar_factura(){
    var id_v = $('#id_venta').val();  
    $.ajax({
      type: "POST",
      url: base_url+"index.php/Ventas/finalizar_factura",
      data:{id:id_v},
      success: function(data){
        $.alert({
            boxWidth: '30%',
            useBootstrap: false,
            title: 'Atención!',
            content: 'Finalizado correctamente'}
        );
        table.ajax.reload(); 
      }
    });
}
///////////////////////////////// 
function fecha_vencimiento(id_v){
    var result='';
    $.ajax({
      type: "POST",
      url: base_url+"index.php/Ventasi/verificar_fecha_vencimiento",
      data:{id:id_v},
      async:false,
      success: function(data){
        result=data;
      }
    });   
    return result;
}
function modal_cdfi(id,tipo) {
    $('#id_venta_e').val(id);
    $('#tipo_e').val(tipo);
    $('.modal').modal({
        dismissible: true
    });
    $('#modal_cfdi').modal('open');
}
function update_cfdi(){
    var id_venta=$('#id_venta_e').val();
    var tipo=$('#tipo_e').val();
    var cfdi=$('#cdfi_e').val();
    $.ajax({
      type: "POST",
      url: base_url+"index.php/Ventasi/update_cfdi_registro",
      data:{id_venta:id_venta,tipo:tipo,cfdi:cfdi},
      success: function(data){
         $.alert({
            boxWidth: '30%',
            useBootstrap: false,
            title: 'Atención!',
            content: 'Guardador correctamente'}
        );
      }
    }); 
    var tipo=$('#tipo_e').val('');
    var cfdi=$('#cdfi_e').val('');  
}
function modal_pago(id,tipo){
    $('#id_venta_p').val(id);
    $('#tipo_p').val(tipo);
    $('#fecha_p').val('');
    $('#pago_p').val('');
    $('#observacion_p').val('');
    $('.modal').modal({
        dismissible: true
    });
    $('#modal_pago').modal('open');
    if(tipo==1){//ventas
       $('.monto_prefactura').html('$'+monto_ventas(id));
    }else if(tipo==3){//póliza creadas
       $('.monto_prefactura').html('$'+monto_poliza(id));
    }else if(tipo==4){// Combinada
       $('.monto_prefactura').html('$'+monto_combinada(id));
    }
}
function monto_ventas(id_v){//ventas monto de prefactura
    var result='';
    $.ajax({
      type: "POST",
      url: base_url+"index.php/Ventasi/montoventas",
      data:{id:id_v},
      async:false,
      success: function(data){
        result=data;
      }
    });   
    return result;
}
function monto_poliza(id_v){//poliza monto de prefactura
    var result='';
    $.ajax({
      type: "POST",
      url: base_url+"index.php/Ventasi/montopoliza",
      data:{id:id_v},
      async:false,
      success: function(data){
        result=data;
      }
    });   
    return result;
}
function monto_combinada(id_v){//Combinada monto de prefactura
    var result='';
    $.ajax({
      type: "POST",
      url: base_url+"index.php/Ventasi/montocombinada",
      data:{id:id_v},
      async:false,
      success: function(data){
        result=data;
      }
    });   
    return result;
}
function guardar_pago_compras(){
    var form_pago = $('#form_pago');
    form_pago.validate({
            rules: 
            { 
                fecha: {
                    required: true
                },
                idmetodo: {
                    required: true
                },
                pago: {
                    required: true
                }
            },
            // Para mensajes personalizados
            messages: 
            {   
                fecha:{
                    required: "Campo obligatirio"
                },
                idmetodo:{
                    required: "Campo obligatirio"
                },
                pago:{
                    required: "Campo obligatirio"
                }
            },
        errorElement : 'div'
    });
    var valid = $("#form_pago").valid();
    if(valid){
        var datos = $('#form_pago').serialize();
        $.ajax({
          type: "POST",
          url: base_url+"index.php/Ventasi/guardar_pago_tipo",
          data:datos,
          success: function(data){
             $.alert({
                boxWidth: '30%',
                useBootstrap: false,
                title: 'Atención!',
                content: 'Guardador correctamente'}
            );
          }
        });
    }   
}
function finalizaentrega(id,table) {
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Desea concluir esta entrega?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                
                     $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Ventasi/updatestateentrega",
                        data: {
                            id:id,
                            table:table
                        },
                        success: function (response){
                                
                                    $.alert({
                                    boxWidth: '30%',
                                    useBootstrap: false,
                                    title: 'Hecho!',
                                    content: 'Estatus actualizado'}); 
                                    load();
                                
                        },
                        error: function(response){
                            $.alert({
                                    boxWidth: '30%',
                                    useBootstrap: false,
                                    title: 'Error!',
                                    content: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema'});   
                             
                        }
                    });
                    
                
            },
            cancelar: function () 
            {
                
            }
        }
    });
}
/*
tabla
prefactura
id_prymari
ventaId
*/

