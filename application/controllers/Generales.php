<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Generales extends CI_Controller {
    public function __construct()     {
        parent::__construct();

        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloGeneral');
        $this->load->model('Login_model');
        $this->load->helper('url');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d H:i:s');
        $this->fechacod = date('YmdGis');
        $this->fechah = date('Y-m-d');
        if($this->session->userdata('logeado')==true){
            $this->perfilid =$this->session->userdata('perfilid');
            $this->idpersonal = $this->session->userdata('idpersonal');
            $this->usuario = $this->session->userdata('usuario');
            
        }else{
            redirect('login'); 
        }
    }
    function index(){

    }
    function addfacturaperiodo(){
        $datas = $this->input->post();
        $facturas=$datas['facturas'];
        $contrato=$datas['contrato'];
        $periodo=$datas['periodo'];
        $facttipo=$datas['facttipo'];
        unset($datas['facturas']);

        //log_message('error','$facttipo: '.$facttipo);
        $this->ModeloCatalogos->updateCatalogo('alta_rentas_prefactura_d',array('statusfacturar'=>1,'statusfacturae'=>1),array('prefId'=>$periodo));


        $DATAf = json_decode($facturas);
      for ($i=0;$i<count($DATAf);$i++) {
        if($facttipo==0){
            $params_i=array('contratoId'=>$contrato,'prefacturaId'=>$periodo,'facturaId'=>$DATAf[$i]->FacturasIds,'idpersonal'=>$this->idpersonal);
            
            $resultfc=$this->ModeloCatalogos->getselectwheren('f_facturas',array('FacturasId'=>$DATAf[$i]->FacturasIds));
            foreach ($resultfc->result() as $item) {
                if($item->pagada>0){
                    $params_i['statuspago']=$item->pagada;
                }
            }
            $resultadoc=$this->ModeloCatalogos->complementofacturas2($DATAf[$i]->FacturasIds);
            $ImpPagado=0;
            $total=0;
            foreach ($resultadoc->result() as $itemcomp) {
                $ImpPagado=$ImpPagado+$itemcomp->ImpPagado;
                $total=$itemcomp->total;
            }
            if($ImpPagado>0){
                if(floatval($total)<=floatval($ImpPagado)){
                    $params_i['statuspago']=1;
                }
            }

            $this->ModeloCatalogos->Insert('factura_prefactura',$params_i);
        }
        if($facttipo==1){
            $this->ModeloCatalogos->updateCatalogo('alta_rentas',array('factura_depo'=>$DATAf[$i]->FacturasIds),array('idcontrato'=>$contrato));
        }
      }
    }
    function editarprecios(){
        $datas = $this->input->post();
        $tipot      = $datas['tipot'];
        $idrow      = $datas['idrow'];
        $newprecio  = $datas['newprecio'];

        if ($tipot==1) {
            $this->ModeloCatalogos->updateCatalogo('ventas_has_detallesEquipos',array('precio'=>$newprecio),array('id'=>$idrow));
            $this->ModeloCatalogos->Insert('bitacora',array('contenido'=>'Se realizo una edicion de precios de equipo','nombretabla'=>'ventas_has_detallesEquipos','idtable'=>$idrow,'tipo'=>'edicion','personalId'=>$this->idpersonal));
        }
        if ($tipot==2) {
            $this->ModeloCatalogos->updateCatalogo('ventas_has_detallesConsumibles',array('precioGeneral'=>$newprecio),array('id'=>$idrow));
            $this->ModeloCatalogos->Insert('bitacora',array('contenido'=>'Se realizo una edicion de precios de consumibles','nombretabla'=>'ventas_has_detallesConsumibles','idtable'=>$idrow,'tipo'=>'edicion','personalId'=>$this->idpersonal));
        }
        if ($tipot==3) {
            $solresult=$this->ModeloCatalogos->getselectwheren('ventas_has_detallesRefacciones',array('id'=>$idrow));
            foreach ($solresult->result() as $item) {
                $piezas=$item->piezas;
            }



            $editarefprecio['precioGeneral']=$newprecio;
            $editarefprecio['totalGeneral']=round($newprecio*$piezas,2);
            
            $this->ModeloCatalogos->updateCatalogo('ventas_has_detallesRefacciones',$editarefprecio,array('id'=>$idrow));
            $this->ModeloCatalogos->Insert('bitacora',array('contenido'=>'Se realizo una edicion de precios de Refacciones','nombretabla'=>'ventas_has_detallesRefacciones','idtable'=>$idrow,'tipo'=>'edicion','personalId'=>$this->idpersonal));
        }
        if ($tipot==4) {
            $this->ModeloCatalogos->updateCatalogo('ventas_has_detallesequipos_consumible',array('costo_toner'=>$newprecio),array('id'=>$idrow));
            $this->ModeloCatalogos->Insert('bitacora',array('contenido'=>'Se realizo una edicion de precios de consumibles','nombretabla'=>'ventas_has_detallesequipos_consumible','idtable'=>$idrow,'tipo'=>'edicion','personalId'=>$this->idpersonal));
        }
        if ($tipot==5) {
            $this->ModeloCatalogos->updateCatalogo('ventas_has_detallesEquipos_accesorios',array('costo'=>$newprecio),array('id_accesoriod'=>$idrow));
            $this->ModeloCatalogos->Insert('bitacora',array('contenido'=>'Se realizo una edicion de precios de accesorios','nombretabla'=>'ventas_has_detallesEquipos_accesorios','idtable'=>$idrow,'tipo'=>'edicion','personalId'=>$this->idpersonal));
        }
    }
    function editarcantidad(){
        $datas = $this->input->post();
        $tipot      = $datas['tipot'];
        $idrow      = $datas['idrow'];
        $newcantidad  = $datas['newcantidad'];

        if ($tipot==1) {
            $this->ModeloCatalogos->updateCatalogo('ventas_has_detallesEquipos',array('cantidad'=>$newcantidad),array('id'=>$idrow));
            $this->ModeloCatalogos->Insert('bitacora',array('contenido'=>'Se realizo una edicion de cantidad de equipo','nombretabla'=>'ventas_has_detallesEquipos','idtable'=>$idrow,'tipo'=>'edicion','personalId'=>$this->idpersonal));
        }
        if ($tipot==2) {
            $this->ModeloCatalogos->updateCatalogo('ventas_has_detallesConsumibles',array('piezas'=>$newcantidad),array('id'=>$idrow));
            $this->ModeloCatalogos->Insert('bitacora',array('contenido'=>'Se realizo una edicion de cantidad de consumibles','nombretabla'=>'ventas_has_detallesConsumibles','idtable'=>$idrow,'tipo'=>'edicion','personalId'=>$this->idpersonal));
        }
        if ($tipot==3) {
            $this->ModeloCatalogos->updateCatalogo('ventas_has_detallesRefacciones',array('piezas'=>$newcantidad),array('id'=>$idrow));
            $this->ModeloCatalogos->Insert('bitacora',array('contenido'=>'Se realizo una edicion de cantidad de Refacciones','nombretabla'=>'ventas_has_detallesRefacciones','idtable'=>$idrow,'tipo'=>'edicion','personalId'=>$this->idpersonal));
        }
        if ($tipot==4) {
            $this->ModeloCatalogos->updateCatalogo('ventas_has_detallesequipos_consumible',array('cantidad'=>$newcantidad),array('id'=>$idrow));
            $this->ModeloCatalogos->Insert('bitacora',array('contenido'=>'Se realizo una edicion de cantidad de consumibles','nombretabla'=>'ventas_has_detallesequipos_consumible','idtable'=>$idrow,'tipo'=>'edicion','personalId'=>$this->idpersonal));
        }
        if ($tipot==5) {
            $this->ModeloCatalogos->updateCatalogo('ventas_has_detallesEquipos_accesorios',array('cantidad'=>$newcantidad),array('id_accesoriod'=>$idrow));
            $this->ModeloCatalogos->Insert('bitacora',array('contenido'=>'Se realizo una edicion de cantidad de accesorios','nombretabla'=>'ventas_has_detallesEquipos_accesorios','idtable'=>$idrow,'tipo'=>'edicion','personalId'=>$this->idpersonal));
        }
    }
    function editarpreciosp(){
        $datas = $this->input->post();
        $idrow      = $datas['idrow'];
        $newprecio  = $datas['newprecio'];
        $tipo  = $datas['tipo'];
        if($tipo==1){
            $col='precio_local';
        }
        if($tipo==2){
            $col='precio_semi';
        }
        if($tipo==3){
            $col='precio_foraneo';
        }
        if($tipo==4){
            $col='precio_especial';
        }
        $this->ModeloCatalogos->updateCatalogo('polizasCreadas_has_detallesPoliza',array($col=>$newprecio),array('id'=>$idrow));
    }
    function editarinfocotizacion(){
        $datas = $this->input->post();
        $cotizacion = $datas['cotizacion'];
        $tipo       = 'info'.$datas['tipo'];
        $info       = $datas['info'];
        $this->ModeloCatalogos->db5_updateCatalogo('cotizaciones',array($tipo=>$info),array('id'=>$cotizacion));
    }
    function ticket(){
        $params = $this->input->post();
        $contenido = $params['contenido'];
        $title = $params['title'];
        if($contenido!='' or $title!=''){
            $idticket=$this->ModeloCatalogos->Insert('ticket',array('contenido'=>$contenido,'tcontenido'=>$title,'personalId'=>$this->idpersonal,'reg'=>$this->fechahoy));
            $this->envioemail($idticket);
        }
    }
    function ticketview(){
        $params = $this->input->post();
        $ticketid = $params['ticketid'];
        $solresult=$this->ModeloCatalogos->db13_getselectwheren('ticket',array('id'=>$ticketid));
        $html='';
        foreach ($solresult->result() as $item) {
            $html.='<div class="row" style="border-bottom: 1px solid #ced4da;">
                        <div class="col s9 col-md-9">'.$item->contenido.'</div>
                        <div class="col s3 col-md-3" style="font-size: 12px;color: #a29999;">'.$item->reg.'</div>
                    </div>';
        }
        $solresult=$this->ModeloCatalogos->db13_getselectwheren('ticket_detalle',array('ticketid'=>$ticketid));
        foreach ($solresult->result() as $item) {
            $html.='<div class="row" style="border-bottom: 1px solid #ced4da;">
                        <div class="col s9 col-md-9" >'.$item->contenido.'</div>
                        <div class="col s3 col-md-3" style="font-size: 12px;color: #a29999;">'.$item->usuario.' '.$item->reg.'</div>
                    </div>';
        }
        echo $html;
    }
    function ticketresponder(){
        $params = $this->input->post();
        $contenido = $params['contenido'];
        $ticketid = $params['ticketid'];
        if($this->idpersonal==1){
            $estatus_resp=1;
        }else{
            $estatus_resp=2;
        }
        $this->ModeloCatalogos->updateCatalogo('ticket',array('estatus_resp'=>$estatus_resp),array('id'=>$ticketid));
        $this->ModeloCatalogos->Insert('ticket_detalle',array('ticketid'=>$ticketid,'contenido'=>$contenido,'usuario'=>$this->usuario,'reg'=>$this->fechahoy));
        if($this->idpersonal!=1){
            $this->envioemail($ticketid);
        }
    }
    function ticketupdate(){
        $params = $this->input->post();
        $ticket = $params['ticket'];
        $status = $params['status'];

        $this->ModeloCatalogos->updateCatalogo('ticket',array('status'=>$status),array('id'=>$ticket));
    }
    function ticketupdate2(){
        $params = $this->input->post();
        $ticket = $params['ticket'];
        $status = $params['status'];

        $this->ModeloCatalogos->updateCatalogo('ticket',array('tipo'=>$status),array('id'=>$ticket));
    }
    function envioemail($idticket=0){
        $atencionpara='';
        //================================
            //cargamos la libreria email
            $this->load->library('email');

            /*
            * Configuramos los parámetros para enviar el email,
            * las siguientes configuraciones es recomendable
            * hacerlas en el fichero email.php dentro del directorio config,
            * en este caso para hacer un ejemplo rápido lo hacemos 
            * en el propio controlador
            */
            
            //Indicamos el protocolo a utilizar
            $config['protocol'] = 'smtp';
             
            //El servidor de correo que utilizaremos
            $config["smtp_host"] ='mail.altaproductividadapr.com'; 
             
            //Nuestro usuario
            $config["smtp_user"] = 'info@altaproductividadapr.com';

            //Nuestra contraseña
            $config["smtp_pass"] = 'yUs&J=T4*V$J';

            //Puerto
            $config["smtp_port"] = '465';

            $config["smtp_crypto"] = 'ssl';
                    
            //El juego de caracteres a utilizar
            $config['charset'] = 'utf-8'; 
     
            //Permitimos que se puedan cortar palabras
            $config['wordwrap'] = TRUE;
             
            //El email debe ser valido  
            $config['validate'] = true;

            $config['mailtype'] = 'html';

            //Establecemos esta configuración
            $this->email->initialize($config);
     
            //Ponemos la dirección de correo que enviará el email y un nombre
            $this->email->from('info@altaproductividadapr.com','Asistencia');
             
              /*
               * Ponemos el o los destinatarios para los que va el email
               * en este caso al ser un formulario de contacto te lo enviarás a ti
               * mismo
               */
            //======================
            //$this->email->bcc('andres@mangoo.mx');
            $this->email->bcc('info@altaproductividadapr.com');
            $this->email->to('gerardo@mangoo.com.mx', 'Desarrollo');
        
        
            $asunto='Asistencia';
        

            //Definimos el asunto del mensaje
                $this->email->subject($asunto);
         
            //Definimos el mensaje a enviar


        $message  = '<div style="background: url(http://anahuac.sicoi.net/public/img/anahuac-fondo-footer.jpg) #eaeaea;width: 100%;height: 94%;padding-top: 25px;margin: 0px;">
                    <div style="width: 10%; height: 10px; float: left;">
                        
                    </div>
                    <div style="background: white; border: 1px solid #F8F8F8; border-radius: 15px; padding: 25px; width: 80%; margin-left: auto; margin-right: auto; float: left; -webkit-box-shadow: 10px 10px 42px -6px rgba(0,0,0,0.75);-moz-box-shadow: 10px 10px 42px -6px rgba(0,0,0,0.75);box-shadow: 10px 10px 42px -6px rgba(0,0,0,0.75);">
                        <div style="width: 100%; float: left;">
                            <div style="width: 30%;float: left;">
                                <img src="https://altaproductividad.mx/public/img/alta.png" width="100%">
                            </div>
                            <div style="width: 40%;float: left; min-height:10px ">
                                
                            </div>
                            <div style="width: 30%;float: left;">
                                <img src="https://altaproductividad.mx/app-assets/images/1024_kyocera_logo_mds.png" width="100%">
                            </div>
                        </div>
                        <div style="width: 100%; float: left;">
                            <hr style="border: 0;   height: 1px;   background-image: -webkit-linear-gradient(left, #f0f0f0, red, #f0f0f0);  background-image: -moz-linear-gradient(left, #f0f0f0, red, #f0f0f0);  background-image: -ms-linear-gradient(left, #f0f0f0, red, #f0f0f0);  background-image: -o-linear-gradient(left, #f0f0f0, red, #f0f0f0);">
                            <hr style="border: 0;   height: 1px;   background-image: -webkit-linear-gradient(left, #f0f0f0, red, #f0f0f0);  background-image: -moz-linear-gradient(left, #f0f0f0, red, #f0f0f0);  background-image: -ms-linear-gradient(left, #f0f0f0, red, #f0f0f0);  background-image: -o-linear-gradient(left, #f0f0f0, red, #f0f0f0);">
                        </div>
                        <div style="width: 100%; float: left;">
                            <h2>Se ha levantado una nueva solicitud de asistencia <a href="'.base_url().'Tickes/Tickesiframeresponse/'.$idticket.'" target="_blank" style="color: inherit;text-decoration: inherit;">'.$idticket.'</a></h2>
                        </div>
                        
                        
                        
                        <div style="width: 100%; float: left;">
                            <hr style="border: 0;   height: 1px;   background-image: -webkit-linear-gradient(left, #f0f0f0, red, #f0f0f0);  background-image: -moz-linear-gradient(left, #f0f0f0, red, #f0f0f0);  background-image: -ms-linear-gradient(left, #f0f0f0, red, #f0f0f0);  background-image: -o-linear-gradient(left, #f0f0f0, red, #f0f0f0);">
                            <hr style="border: 0;   height: 1px;   background-image: -webkit-linear-gradient(left, #f0f0f0, red, #f0f0f0);  background-image: -moz-linear-gradient(left, #f0f0f0, red, #f0f0f0);  background-image: -ms-linear-gradient(left, #f0f0f0, red, #f0f0f0);  background-image: -o-linear-gradient(left, #f0f0f0, red, #f0f0f0);">
                        </div>
                    </div>
                </div>';

        $this->email->message($message);
       
        //Enviamos el email y si se produce bien o mal que avise con una flasdata
        if($this->email->send()){
            //$this->session->set_flashdata('envio', 'Email enviado correctamente');
        }else{
            //$this->session->set_flashdata('envio', 'No se a enviado el email');
        }

        //==================
    }
    function devoluciones(){
        $params = $this->input->post();
        $pass=$params['pass'];
        $cli=$params['cli'];
        log_message('error','devoluciones pass:'.$pass);
        $permiso=0;
        $permisos = $this->Login_model->permisoadminarray(array('1','9','17','18'));//1admin 9 conta 17 julio 18 diana
        foreach ($permisos as $item) {
            $permisotemp=0;

            $usuario=$item->Usuario;

            $verificar = password_verify($pass,$item->contrasena);

            if ($verificar==false) {
                $verificar = password_verify($pass,$item->clavestemp);
                if ($verificar) {
                    $permisotemp=1;
                }else{
                    //$result_permiso_cli = $this->ModeloGeneral->permisoscliente($cli,$pass);
                }
            }
            //if ($verificar) {
                //$verificar = $this->permisoscliente($cli,$pass);
            //}else{
                //$verificar = $this->ModeloGeneral->permisoscliente($cli,$pass);
            //}
            
            if ($verificar) {
                $permiso=1;
                    //=================================================
                        /*
                            tipotable
                                1 ventas_has_detallesConsumibles_dev
                                2 ventas_has_detallesEquipos_dev
                                3 ventas_has_detallesEquipos_accesorios_dev
                                4 ventas_has_detallesequipos_consumible_dev
                                5 ventas_has_detallesRefacciones_dev
                        */
                        $tipotable=$params['tipotable'];
                        $idrow=$params['idrow'];
                        $tipodevolucion=$params['tipodevolucion'];//1 total 0 parcial
                        $cantidad=$params['cantidad'];
                        $motivo=$params['motivo'];
                        $bodega=$params['bodega'];

                        if ($tipotable==1) {
                            $result=$this->ModeloCatalogos->getselectwheren('ventas_has_detallesConsumibles',array('id'=>$idrow));
                            $dataparams=$result->row();
                            //log_message('error',json_encode($dataparams));
                            unset($dataparams->id);
                            //=======================================
                                $idVentas=$dataparams->idVentas;
                                $idCliente=0;
                                $resultvent=$this->ModeloCatalogos->getselectwheren('ventas',array('id'=>$idVentas));
                                foreach ($resultvent->result() as $itemvn) {
                                    $idCliente=$itemvn->idCliente;
                                }
                            //=======================================
                            $dataparams->personal_dev=$this->idpersonal;
                            $dataparams->motivo_dev=$motivo;
                            if($tipodevolucion==1){
                                //log_message('error','devolucion total');
                                $this->ModeloCatalogos->Insert('ventas_has_detallesConsumibles_dev',$dataparams);
                                //==================================================================================
                                    $whereconsu = array(
                                        'bodegaId' => $bodega,
                                        'consumiblesId' => $dataparams->idConsumible,
                                        'status'=>1
                                    ); 
                                    $resultconsu = $this->ModeloCatalogos->getselectwheren('consumibles_bodegas',$whereconsu);
                                    if($resultconsu->num_rows()>0){
                                        $this->ModeloCatalogos->updatestock2('consumibles_bodegas','total','+',$dataparams->piezas,'consumiblesId',$dataparams->idConsumible,'bodegaId',$bodega);
                                    }else{
                                        $this->ModeloCatalogos->Insert('consumibles_bodegas',array('bodegaId'=>$bodega,'consumiblesId'=>$dataparams->idConsumible,'total'=>$dataparams->piezas));
                                    }
                                //==================================================================================
                                
                                $this->ModeloCatalogos->getdeletewheren('ventas_has_detallesConsumibles',array('id'=>$idrow));
                            }else{
                                //log_message('error','devolucion parcial');
                                unset($dataparams->piezas);
                                $dataparams->piezas=$cantidad;
                                $this->ModeloCatalogos->Insert('ventas_has_detallesConsumibles_dev',$dataparams);
                                //==================================================================================
                                    $whereconsu = array(
                                        'bodegaId' => $bodega,
                                        'consumiblesId' => $dataparams->idConsumible,
                                        'status'=>1
                                    ); 
                                    $resultconsu = $this->ModeloCatalogos->getselectwheren('consumibles_bodegas',$whereconsu);
                                    if($resultconsu->num_rows()>0){
                                        $this->ModeloCatalogos->updatestock2('consumibles_bodegas','total','+',$cantidad,'consumiblesId',$dataparams->idConsumible,'bodegaId',$bodega);
                                    }else{
                                        $this->ModeloCatalogos->Insert('consumibles_bodegas',array('bodegaId'=>$bodega,'consumiblesId'=>$dataparams->idConsumible,'total'=>$cantidad));
                                    }
                                //==================================================================================
                                
                                $this->ModeloCatalogos->updatestock('ventas_has_detallesConsumibles','piezas','-',$cantidad,'id',$idrow);
                            }
                            $this->ModeloCatalogos->Insert('bitacora_movimientos',array('id_item'=>0,'modeloId'=>$dataparams->idConsumible,'modelo'=>$dataparams->modelo,'tipo'=>4,'cantidad'=>$dataparams->piezas,'serieId'=>$dataparams->idConsumible,'entrada_salida'=>1,'bodega_d'=>$bodega,'personal'=>$this->idpersonal,'descripcion_evento'=>'devolucion venta: '.$idVentas.' de '.$dataparams->piezas.' '.$dataparams->modelo.' '.$motivo,'clienteId'=>$idCliente));
                        }
                        if ($tipotable==2) {
                            $result=$this->ModeloCatalogos->getselectwheren('ventas_has_detallesEquipos',array('id'=>$idrow));
                            $dataparams=$result->row();
                            //$dataparams['serie_o']=$dataparams->id;
                            //unset($dataparams->id);
                            $dataparams->personal_dev=$this->idpersonal;
                            $dataparams->motivo_dev=$motivo;
                            //=======================================
                                $idVentas=$dataparams->idVenta;
                                $idCliente=0;
                                $resultvent=$this->ModeloCatalogos->getselectwheren('ventas',array('id'=>$idVentas));
                                foreach ($resultvent->result() as $itemvn) {
                                    $idCliente=$itemvn->idCliente;
                                }
                            //=======================================
                            if($tipodevolucion==1){

                                $this->ModeloCatalogos->Insert('ventas_has_detallesEquipos_dev',$dataparams);
                                
                                $result_se=$this->ModeloCatalogos->getselectwheren('asignacion_series_equipos',array('id_equipo'=>$idrow));
                                foreach ($result_se->result() as $itemse) {
                                    //$this->ModeloCatalogos->getdeletewheren('asignacion_series_equipos',array('id_equipo'=>$idrow,'serieId'=>$itemse->serieId));
                                    $this->ModeloCatalogos->updateCatalogo('series_productos',array('status'=>0,'bodegaId'=>$bodega),array('serieId'=>$itemse->serieId));
                                    //$serieId=$itemse->serieId;
                                    $this->Configuraciones_model->registrarmovimientosseries($itemse->serieId,1);
                                    $this->ModeloCatalogos->Insert('bitacora_movimientos',array('id_item'=>0,'modeloId'=>$dataparams->idEquipo,'modelo'=>$dataparams->modelo,'tipo'=>1,'serieId'=>$itemse->serieId,'entrada_salida'=>1,'bodega_d'=>$bodega,'personal'=>$this->idpersonal,'descripcion_evento'=>'devolucion venta: '.$idVentas.' de '.$dataparams->modelo.' '.$motivo,'clienteId'=>$idCliente));
                                }

                                $this->ModeloCatalogos->getdeletewheren('ventas_has_detallesEquipos',array('id'=>$idrow));
                            }else{
                                $serie=$cantidad;// en este caso la cabriable cantidad se agregara el id de la serie
                                $dataparams->cantidad=1;
                                $dataparams->serie=$serie;
                                $this->ModeloCatalogos->Insert('ventas_has_detallesEquipos_dev',$dataparams);
                                //$this->ModeloCatalogos->getdeletewheren('asignacion_series_equipos',array('id_equipo'=>$idrow,'serieId'=>$serie));
                                $this->ModeloCatalogos->updateCatalogo('series_productos',array('status'=>0,'bodegaId'=>$bodega),array('serieId'=>$serie));$this->Configuraciones_model->registrarmovimientosseries($serie,1);
                                $this->ModeloCatalogos->updatestock('ventas_has_detallesEquipos','cantidad','-',1,'id',$idrow);
                                $this->ModeloCatalogos->Insert('bitacora_movimientos',array('id_item'=>0,'modeloId'=>$dataparams->idEquipo,'modelo'=>$dataparams->modelo,'tipo'=>1,'serieId'=>$serie,'entrada_salida'=>1,'bodega_d'=>$bodega,'personal'=>$this->idpersonal,'descripcion_evento'=>'devolucion venta: '.$idVentas.' de '.$dataparams->modelo.' '.$motivo,'clienteId'=>$idCliente));
                            }
                            
                        }
                        if ($tipotable==3) {
                            $result=$this->ModeloCatalogos->getselectwheren('ventas_has_detallesEquipos_accesorios',array('id_accesoriod'=>$idrow));
                            $dataparams=$result->row();
                            //unset($dataparams->id_accesoriod);
                            $dataparams->personal_dev=$this->idpersonal;
                            $dataparams->motivo_dev=$motivo;
                            //=======================================
                                $idVentas=$dataparams->id_venta;
                                $idCliente=0;
                                $resultvent=$this->ModeloCatalogos->getselectwheren('ventas',array('id'=>$idVentas));
                                foreach ($resultvent->result() as $itemvn) {
                                    $idCliente=$itemvn->idCliente;
                                }
                            //=======================================
                            //=======================================================
                                $result_m=$this->ModeloCatalogos->getselectwheren('catalogo_accesorios',array('id'=>$dataparams->id_accesorio));
                                $modelox='';
                                foreach ($result_m->result() as $itemm) {
                                    $modelox=$itemm->nombre;
                                }
                            //=======================================================
                            if($tipodevolucion==1){
                                $this->ModeloCatalogos->Insert('ventas_has_detallesEquipos_accesorios_dev',$dataparams);
                                
                                $result_se=$this->ModeloCatalogos->getselectwheren('asignacion_series_accesorios',array('id_accesorio'=>$idrow));
                                foreach ($result_se->result() as $itemse) {
                                    //$this->ModeloCatalogos->getdeletewheren('asignacion_series_accesorios',array('id_accesorio'=>$idrow,'serieId'=>$itemse->serieId));
                                    
                                    $info_serie_acc=$this->ModeloCatalogos->getselectwheren('series_accesorios',array('serieId'=>$itemse->serieId));
                                    if($info_serie_acc->num_rows()>0){
                                        $info_serie_acc=$info_serie_acc->row();
                                        if($info_serie_acc->con_serie==1){
                                            $this->ModeloCatalogos->updateCatalogo('series_accesorios',array('status'=>0,'bodegaId'=>$bodega),array('serieId'=>$itemse->serieId));$this->Configuraciones_model->registrarmovimientosseries($itemse->serieId,2);
                                        }else{
                                            $this->ModeloCatalogos->updatestock('series_accesorios','cantidad','+',$dataparams->cantidad,'serieId',$itemse->serieId);$this->Configuraciones_model->registrarmovimientosseries($itemse->serieId,2);
                                        }
                                    }

                                    
                                    //$serieId=$itemse->serieId;
                                    $this->ModeloCatalogos->Insert('bitacora_movimientos',array('id_item'=>0,'modeloId'=>$dataparams->id_accesorio,'modelo'=>$modelox,'tipo'=>2,'serieId'=>$itemse->serieId,'entrada_salida'=>1,'bodega_d'=>$bodega,'personal'=>$this->idpersonal,'descripcion_evento'=>'devolucion venta: '.$idVentas.' de '.$modelox.' '.$motivo,'clienteId'=>$idCliente));
                                }

                                $this->ModeloCatalogos->getdeletewheren('ventas_has_detallesEquipos_accesorios',array('id_accesoriod'=>$idrow));


                                
                            }else{
                                $serie=$cantidad;// en este caso la cabriable cantidad se agregara el id de la serie
                                $dataparams->cantidad=1;
                                $dataparams->serie=$serie;
                                $this->ModeloCatalogos->Insert('ventas_has_detallesEquipos_accesorios_dev',$dataparams);
                                //$this->ModeloCatalogos->getdeletewheren('asignacion_series_accesorios',array('id_accesorio'=>$idrow,'serieId'=>$serie));
                                    $info_serie_acc=$this->ModeloCatalogos->getselectwheren('series_accesorios',array('serieId'=>$itemse->serieId));
                                    if($info_serie_acc->num_rows()>0){
                                        $info_serie_acc=$info_serie_acc->row();
                                        if($info_serie_acc->con_serie==1){
                                            $this->ModeloCatalogos->updateCatalogo('series_accesorios',array('status'=>0,'bodegaId'=>$bodega),array('serieId'=>$itemse->serieId));$this->Configuraciones_model->registrarmovimientosseries($itemse->serieId,2);
                                        }else{
                                            $this->ModeloCatalogos->updatestock('series_accesorios','cantidad','+',$cantidad,'serieId',$itemse->serieId);$this->Configuraciones_model->registrarmovimientosseries($itemse->serieId,2);
                                        }
                                    }
                                //$this->ModeloCatalogos->updateCatalogo('series_accesorios',array('status'=>0,'bodegaId'=>$bodega),array('serieId'=>$serie));
                                $this->ModeloCatalogos->updatestock('ventas_has_detallesEquipos_accesorios','cantidad','-',1,'id_accesoriod',$idrow);

                                $this->ModeloCatalogos->Insert('bitacora_movimientos',array('id_item'=>0,'modeloId'=>$dataparams->id_accesorio,'modelo'=>$modelox,'tipo'=>2,'serieId'=>$serie,'entrada_salida'=>1,'bodega_d'=>$bodega,'personal'=>$this->idpersonal,'descripcion_evento'=>'devolucion venta: '.$idVentas.' de '.$modelox.' '.$motivo,'clienteId'=>$idCliente));
                            }
                        }
                        if ($tipotable==4) {
                            $result=$this->ModeloCatalogos->getselectwheren('ventas_has_detallesequipos_consumible',array('id'=>$idrow));
                            $dataparams=$result->row();
                            unset($dataparams->id);
                            $dataparams->personal_dev=$this->idpersonal;
                            $dataparams->motivo_dev=$motivo;
                            //=======================================
                                $idVentas=$dataparams->idventa;
                                $idCliente=0;
                                $resultvent=$this->ModeloCatalogos->getselectwheren('ventas',array('id'=>$idVentas));
                                foreach ($resultvent->result() as $itemvn) {
                                    $idCliente=$itemvn->idCliente;
                                }
                            //=======================================
                            //===============================================
                                $result_c=$this->ModeloCatalogos->getselectwheren('consumibles',array('id'=>$dataparams->id_consumibles));
                                $modelox='';
                                foreach ($result_c->result() as $itemcs) {
                                    $modelox=$itemcs->modelo;
                                }

                            //===============================================
                            if($tipodevolucion==1){
                                //$dataparams->bodegas=$dataparams->bodega;
                                unset($dataparams->bodega);
                                $this->ModeloCatalogos->Insert('ventas_has_detallesequipos_consumible_dev',$dataparams);
                                //==================================================================================
                                    $whereconsu = array(
                                        'bodegaId' => $bodega,
                                        'consumiblesId' => $dataparams->id_consumibles,
                                        'status'=>1
                                    ); 
                                    $resultconsu = $this->ModeloCatalogos->getselectwheren('consumibles_bodegas',$whereconsu);
                                    if($resultconsu->num_rows()>0){
                                        $this->ModeloCatalogos->updatestock2('consumibles_bodegas','total','+',$dataparams->cantidad,'consumiblesId',$dataparams->id_consumibles,'bodegaId',$bodega);
                                    }else{
                                        $this->ModeloCatalogos->Insert('consumibles_bodegas',array('bodegaId'=>$bodega,'consumiblesId'=>$dataparams->id_consumibles,'total'=>$dataparams->cantidad));
                                    }
                                //==================================================================================
                                
                                $this->ModeloCatalogos->getdeletewheren('ventas_has_detallesequipos_consumible',array('id'=>$idrow));

                                $this->ModeloCatalogos->Insert('bitacora_movimientos',array('id_item'=>0,'modelo'=>$modelox,'tipo'=>2,'modeloId'=>$dataparams->id_consumibles,'cantidad'=>$dataparams->cantidad,'entrada_salida'=>1,'bodega_d'=>$bodega,'personal'=>$this->idpersonal,'descripcion_evento'=>'devolucion venta: '.$idVentas.' de '.$modelox.' '.$motivo,'clienteId'=>$idCliente));
                            }else{
                                //$dataparams->bodegas=$dataparams->bodega;
                                unset($dataparams->bodega);
                                unset($dataparams->cantidad);
                                $dataparams->cantidad=$cantidad;
                                $this->ModeloCatalogos->Insert('ventas_has_detallesequipos_consumible_dev',$dataparams);
                                //==================================================================================
                                    $whereconsu = array(
                                        'bodegaId' => $bodega,
                                        'consumiblesId' => $dataparams->id_consumibles,
                                        'status'=>1
                                    ); 
                                    $resultconsu = $this->ModeloCatalogos->getselectwheren('consumibles_bodegas',$whereconsu);
                                    if($resultconsu->num_rows()>0){
                                        $this->ModeloCatalogos->updatestock2('consumibles_bodegas','total','+',$cantidad,'consumiblesId',$dataparams->id_consumibles,'bodegaId',$bodega);
                                    }else{
                                        $this->ModeloCatalogos->Insert('consumibles_bodegas',array('bodegaId'=>$bodega,'consumiblesId'=>$dataparams->id_consumibles,'total'=>$cantidad));
                                    }
                                //==================================================================================
                                
                                $this->ModeloCatalogos->updatestock('ventas_has_detallesequipos_consumible','cantidad','-',$cantidad,'id',$idrow);

                                $this->ModeloCatalogos->Insert('bitacora_movimientos',array('id_item'=>0,'modelo'=>$modelox,'tipo'=>2,'modeloId'=>$dataparams->id_consumibles,'cantidad'=>$dataparams->cantidad,'entrada_salida'=>1,'bodega_d'=>$bodega,'personal'=>$this->idpersonal,'descripcion_evento'=>'devolucion venta: '.$idVentas.' de '.$modelox.' '.$motivo,'clienteId'=>$idCliente));
                            }
                        }
                        if ($tipotable==5) {
                            $result=$this->ModeloCatalogos->getselectwheren('ventas_has_detallesRefacciones',array('id'=>$idrow));
                            $dataparams=$result->row();
                            $dataparams->personal_dev=$this->idpersonal;
                            $dataparams->motivo_dev=$motivo;
                            //=======================================
                                $idVentas=$dataparams->idVentas;
                                $idCliente=0;
                                $resultvent=$this->ModeloCatalogos->getselectwheren('ventas',array('id'=>$idVentas));
                                foreach ($resultvent->result() as $itemvn) {
                                    $idCliente=$itemvn->idCliente;
                                }
                            //=======================================
                            if($tipodevolucion==1){
                                $this->ModeloCatalogos->Insert('ventas_has_detallesRefacciones_dev',$dataparams);
                                //==================================================================================
                                    $whereconsu = array(
                                        'bodegaId' => $bodega,
                                        'refaccionid' => $dataparams->idRefaccion,
                                        'activo'=>1
                                    ); 
                                    $resultconsu = $this->ModeloCatalogos->getselectwheren('series_refacciones',$whereconsu);
                                    if($resultconsu->num_rows()>0){
                                        $this->ModeloCatalogos->updatestock2('series_refacciones','cantidad','+',$dataparams->piezas,'refaccionid',$dataparams->idRefaccion,'bodegaId',$bodega);
                                        //$this->Configuraciones_model->registrarmovimientosseries($dataparams->idRefaccion,3);
                                    }else{
                                        $this->ModeloCatalogos->Insert('series_refacciones',array('bodegaId'=>$bodega,'refaccionid'=>$dataparams->idRefaccion,'serie'=>'sin serie','con_serie'=>0,'personalId'=>$this->idpersonal,'cantidad'=>$dataparams->piezas));
                                    }
                                //==================================================================================
                                
                                //$this->ModeloCatalogos->getdeletewheren('asignacion_series_refacciones',array('id_refaccion'=>$idrow));
                                $this->ModeloCatalogos->getdeletewheren('ventas_has_detallesRefacciones',array('id'=>$idrow));
                            }else{
                                unset($dataparams->piezas);
                                $dataparams->piezas=$cantidad;
                                $this->ModeloCatalogos->Insert('ventas_has_detallesRefacciones_dev',$dataparams);
                                //==================================================================================
                                    $whereconsu = array(
                                        'bodegaId' => $bodega,
                                        'refaccionid' => $dataparams->idRefaccion,
                                        'activo'=>1
                                    ); 
                                    $resultconsu = $this->ModeloCatalogos->getselectwheren('series_refacciones',$whereconsu);
                                    if($resultconsu->num_rows()>0){
                                        $this->ModeloCatalogos->updatestock2('series_refacciones','cantidad','+',$dataparams->piezas,'refaccionid',$dataparams->idRefaccion,'bodegaId',$bodega);
                                        //$this->Configuraciones_model->registrarmovimientosseries($dataparams->idRefaccion,3);
                                    }else{
                                        $this->ModeloCatalogos->Insert('series_refacciones',array('bodegaId'=>$bodega,'refaccionid'=>$dataparams->idRefaccion,'serie'=>'sin serie','con_serie'=>0,'personalId'=>$this->idpersonal,'cantidad'=>$dataparams->piezas));
                                    }
                                //==================================================================================
                                
                                $this->ModeloCatalogos->updatestock('ventas_has_detallesRefacciones','piezas','-',$cantidad,'id',$idrow);
                            }
                            $this->ModeloCatalogos->Insert('bitacora_movimientos',array('id_item'=>0,'modelo'=>$dataparams->modelo,'tipo'=>3,'modeloId'=>$dataparams->idRefaccion,'cantidad'=>$dataparams->piezas,'entrada_salida'=>1,'bodega_d'=>$bodega,'personal'=>$this->idpersonal,'descripcion_evento'=>'devolucion venta: '.$idVentas.' de '.$dataparams->modelo.' '.$motivo,'clienteId'=>$idCliente));
                        }
                    //==========================================
                if($permisotemp==1){
                    $this->ModeloCatalogos->updateCatalogo('usuarios',array('clavestemp'=>null),array('UsuarioID'=>$item->UsuarioID));
                }

                
            }
        }
        echo $permiso;
    }
    
    function devoluciones_r(){
        $params = $this->input->post();
        $pass   = $params['pass'];
        log_message('error','devoluciones_r pass:'.$pass);
        $permiso=0;
        $permisos = $this->Login_model->permisoadminarray(array('1','9','17','18'));//1admin 9 conta 17 julio 18 diana

        foreach ($permisos as $item) {
            $permisotemp=0;

            $usuario=$item->Usuario;

            $verificar = password_verify($pass,$item->contrasena);

            if ($verificar==false) {
                $verificar = password_verify($pass,$item->clavestemp);
                if ($verificar) {
                    $permisotemp=1;
                }
            }
            
            if ($verificar) {
                $permiso=1;
                //=====================================================
                    /*
                        tipotable
                            1 equipos (equipo,consumible,accesorios)
                            2 consumible
                            3 accesorios
                            4 
                            5 
                    */
                    $tipotable=$params['tipotable'];
                    $idrow=$params['idrow'];
                    $tipodevolucion=$params['tipodevolucion'];//1 total 0 parcial
                    $cantidad=$params['cantidad'];
                    $motivo=$params['motivo'];

                    if($tipotable==1){
                        if($tipodevolucion==1){
                            $this->eliminarRentaE($idrow,$motivo);
                        }
                    }
                    if($tipotable==2){
                        if($tipodevolucion==1){
                            $this->consumibledevolucion($idrow,$motivo);
                        }
                    }
                    if($tipotable==3){
                        if($tipodevolucion==1){
                            $this->accesoriosdevolucion($idrow,$motivo,0);
                        }
                    }
                //=====================================================
                if($permisotemp==1){
                    $this->ModeloCatalogos->updateCatalogo('usuarios',array('clavestemp'=>null),array('UsuarioID'=>$item->UsuarioID));
                }
            }
        }
        echo $permiso;
    }
    function addfacturavcp(){
        $datas = $this->input->post();
        $facturas=$datas['facturas'];
        $idvcp=$datas['idvcp'];
        $tipo=$datas['tipo'];
        unset($datas['facturas']);
        $html='';
        $agregada=2;



        $DATAf = json_decode($facturas);
      for ($i=0;$i<count($DATAf);$i++) {
        //$facturas=$this->ModeloCatalogos->foliosviculados($DATAf[$i]->FacturasIds);
        //$html=$facturas->num_rows();
        if($tipo==0){// venta normal
            $result = $this->ModeloCatalogos->getselectwheren('factura_venta',array('ventaId'=>$idvcp,'combinada'=>0,'facturaId'=>$DATAf[$i]->FacturasIds));
            if($result->num_rows()==0){
                $this->ModeloCatalogos->Insert('factura_venta',array('ventaId'=>$idvcp,'combinada'=>0,'facturaId'=>$DATAf[$i]->FacturasIds,'idpersonal'=>$this->idpersonal));
                $agregada=1;
            }else{
                $agregada=0;
            }
        }
        if($tipo==1){//centa conbinada
            $result = $this->ModeloCatalogos->getselectwheren('factura_venta',array('ventaId'=>$idvcp,'combinada'=>0,'facturaId'=>$DATAf[$i]->FacturasIds));
            if($result->num_rows()==0){
                $this->ModeloCatalogos->Insert('factura_venta',array('ventaId'=>$idvcp,'combinada'=>1,'facturaId'=>$DATAf[$i]->FacturasIds,'idpersonal'=>$this->idpersonal));
                $agregada=1;
            }else{
                $agregada=0;
            }
        }
        if($tipo==2){//poliza
            $result = $this->ModeloCatalogos->getselectwheren('factura_poliza',array('polizaId'=>$idvcp,'facturaId'=>$DATAf[$i]->FacturasIds));
            if($result->num_rows()==0){
                $this->ModeloCatalogos->Insert('factura_poliza',array('polizaId'=>$idvcp,'facturaId'=>$DATAf[$i]->FacturasIds,'idpersonal'=>$this->idpersonal));
                $agregada=1;
            }else{
                $agregada=0;
            }
        }
        
      }
      echo $agregada;
    }
    function cambiarestatuspago(){
        $params = $this->input->post();
        $cvcp = $params['cvcp'];
        $monto = $params['montos'];
        unset($params['cvcp']);
        //log_message('error', 'monto '.$monto);
        if($monto>0){
            $DATAc = json_decode($cvcp);       
            for ($i=0;$i<count($DATAc);$i++) {
                if ($DATAc[$i]->tipo==0) {
                    $this->ModeloCatalogos->updateCatalogo('ventas',array('estatus'=>3),array('id'=>$DATAc[$i]->idventapoliza));
                }
                if ($DATAc[$i]->tipo==1) {
                    $this->ModeloCatalogos->updateCatalogo('ventacombinada',array('estatus'=>3),array('combinadaId'=>$DATAc[$i]->idventapoliza));
                }
                if ($DATAc[$i]->tipo==2) {
                    $this->ModeloCatalogos->updateCatalogo('polizasCreadas',array('estatus'=>3),array('id'=>$DATAc[$i]->idventapoliza));
                }
                if ($DATAc[$i]->tipo==3) {
                    $this->ModeloCatalogos->updateCatalogo('ventasd',array('estatus'=>3),array('id'=>$DATAc[$i]->idventapoliza));
                }
            }
        }
        
    }
    function cambiarestatuspagoperiodo(){
        $params = $this->input->post();
        $facId=$params['facId'];
        $this->ModeloCatalogos->updateCatalogo('factura_prefactura',array('statuspago'=>1),array('facId'=>$facId));
    }
    function viewcomplementos(){
        $datos = $this->input->post('datos');
        //log_message('error', $datos);
        $DATA = json_decode($datos);
        $facturas=array();
        for ($i=0;$i<count($DATA);$i++) {
            $complemento=0;
            $idfactura=$DATA[$i]->idfactura;
            $resultado=$this->ModeloCatalogos->complementofacturas2($idfactura);
            $ImpPagado=0;
            $total=0;
            foreach ($resultado->result() as $itemcomp) {
                $ImpPagado=$ImpPagado+$itemcomp->ImpPagado;
                $total=$itemcomp->total;
            }
            
            $facturas[]=array(
                    'factura'=>$idfactura,
                    'complemento'=>$resultado->num_rows(),
                    'imppagado'=>$ImpPagado,
                    'totalfac'=>floatval($total)
                );
        }
        echo json_encode($facturas);
    }
    function vericarpagosventas(){
        $datos = $this->input->post('datos');
        //log_message('error', $datos);
        $DATA = json_decode($datos);
        $ventas=array();
        for ($i=0;$i<count($DATA);$i++) {
            $idventa=$DATA[$i]->idventa;
            $tipoventa=$DATA[$i]->tipoventa;
            if($tipoventa==0){
                $resultado=$this->ModeloCatalogos->getselectwheren('pagos_ventas',array('idventa'=>$idventa,'confirmado'=>0));
                if($resultado->num_rows()>0){
                    $ventas[]=array('idventa'=>$idventa,'tipo'=>0);
                }
            }
            if($tipoventa==1){
                $resultado=$this->ModeloCatalogos->getselectwheren('pagos_combinada',array('idventa'=>$idventa,'confirmado'=>0));
                if($resultado->num_rows()>0){
                    $ventas[]=array('idventa'=>$idventa,'tipo'=>1);
                }    
            }
            
        }
        echo json_encode($ventas);
    }
    function vericarpagospoliza(){
        $datos = $this->input->post('datos');
        //log_message('error', $datos);
        $DATA = json_decode($datos);
        $polizas=array();
        for ($i=0;$i<count($DATA);$i++) {
            $idpoliza=$DATA[$i]->poliza;

            $resultado=$this->ModeloCatalogos->getselectwheren('pagos_poliza',array('idpoliza'=>$idpoliza,'confirmado'=>0));
            if($resultado->num_rows()>0){
                $polizas[]=array('idpoliza'=>$idpoliza);
            }
            
         
            
        }
        echo json_encode($polizas);
    }
    function vericarpagosrentas(){
        $datos = $this->input->post('datos');
        //log_message('error', $datos);
        $DATA = json_decode($datos);
        $prefactura=array();
        for ($i=0;$i<count($DATA);$i++) {
            $idpre=$DATA[$i]->idpre;

            $resultado=$this->ModeloCatalogos->getselectwheren('factura_prefactura_pagos',array('facId'=>$idpre,'confirmado'=>0));
            if($resultado->num_rows()>0){
                $prefactura[]=array('idpre'=>$idpre);
            }
            
         
            
        }
        echo json_encode($prefactura);
    }
    function regresarfacturaavigente(){
        $idf = $this->input->post('idf');
        $pass = $this->input->post('pass');
        log_message('error','regresarfacturaavigente pass:'.$pass);

        //=================================================================
            $permiso=0;
            $permisos = $this->Login_model->permisoadminarray(array('1','17','18'));//1admin 17 julio 18 diana

            foreach ($permisos as $item) {
                $permisotemp=0;

                $usuario=$item->Usuario;

                $verificar = password_verify($pass,$item->contrasena);

                if ($verificar==false) {
                    $verificar = password_verify($pass,$item->clavestemp);
                    if ($verificar) {
                        $permisotemp=1;
                    }
                }
                if ($verificar) {
                    $permiso=1;
                    
                    $this->ModeloCatalogos->updateCatalogo('f_facturas',array('Estado'=>1),array('FacturasId'=>$idf));

                    $this->ModeloCatalogos->Insert('bitacora',array('contenido'=>'Se reactivo la factura a vigente, persona que reactivo:'.$this->usuario,'nombretabla'=>'f_facturas','idtable'=>$idf,'tipo'=>'Reactivacion factura','personalId'=>$this->idpersonal));

                    if($permisotemp==1){
                        $this->ModeloCatalogos->updateCatalogo('usuarios',array('clavestemp'=>null),array('UsuarioID'=>$item->UsuarioID));
                    }

                    
                }
            }
            echo $permiso;
        
    }
    function cambiostatusfactura(){
        $idf = $this->input->post('idf');
        $pass = $this->input->post('pass');
        $pass = trim($pass);//para eliminar los espacios
        log_message('error','cambio estatus pass:'.$pass);
        $estatus = $this->input->post('estatus');

        $tex='';
        if($estatus==1){
            $tex='Se cambio a Timbrada';
        }
        if($estatus==0){
            $tex='Se cambio a Cancelada';
        }

        //=================================================================
            $permiso=0;
            $permisos = $this->Login_model->permisoadminarray(array('1','17','18'));//1admin 17 julio 18 diana

            foreach ($permisos as $item) {
                $permisotemp=0;

                $usuario=$item->Usuario;

                $verificar = password_verify($pass,$item->contrasena);

                if ($verificar==false) {
                    $verificar = password_verify($pass,$item->clavestemp);
                    if ($verificar) {
                        $permisotemp=1;
                    }
                }
                if ($verificar) {
                    $permiso=1;
                    
                    $this->ModeloCatalogos->updateCatalogo('f_facturas',array('Estado'=>$estatus),array('FacturasId'=>$idf));

                    $this->ModeloCatalogos->Insert('bitacora',array('contenido'=>$tex.' la factura , persona que reactivo:'.$this->usuario,'nombretabla'=>'f_facturas','idtable'=>$idf,'tipo'=>'cambio estatus factura','personalId'=>$this->idpersonal));

                    if($permisotemp==1){
                        $this->ModeloCatalogos->updateCatalogo('usuarios',array('clavestemp'=>null),array('UsuarioID'=>$item->UsuarioID));
                    }

                    
                }
            }
            echo $permiso;
        
    }
    function cambiostatuscomplemento(){
        $idf = $this->input->post('idf');
        $pass = $this->input->post('pass');
        $pass = trim($pass);//para eliminar los espacios
        log_message('error','cambio estatus pass:'.$pass);
        $estatus = $this->input->post('estatus');

        $tex='';
        if($estatus==1){
            $tex='Se cambio a Timbrada';
        }
        if($estatus==0){
            $tex='Se cambio a Cancelada';
        }

        //=================================================================
            $permiso=0;
            $permisos = $this->Login_model->permisoadminarray(array('1','17','18'));//1admin 17 julio 18 diana

            foreach ($permisos as $item) {
                $permisotemp=0;

                $usuario=$item->Usuario;

                $verificar = password_verify($pass,$item->contrasena);

                if ($verificar==false) {
                    $verificar = password_verify($pass,$item->clavestemp);
                    if ($verificar) {
                        $permisotemp=1;
                    }
                }
                if ($verificar) {
                    $permiso=1;
                    
                    $this->ModeloCatalogos->updateCatalogo('f_complementopago',array('Estado'=>$estatus),array('complementoId'=>$idf));

                    $this->ModeloCatalogos->Insert('bitacora',array('contenido'=>$tex.' el complemento , persona que reactivo:'.$this->usuario,'nombretabla'=>'f_complementopago','idtable'=>$idf,'tipo'=>'cambio estatus complemento','personalId'=>$this->idpersonal));
                    $this->verificarpagosfomplemento($idf,$estatus);

                    if($permisotemp==1){
                        $this->ModeloCatalogos->updateCatalogo('usuarios',array('clavestemp'=>null),array('UsuarioID'=>$item->UsuarioID));
                    }

                    
                }
            }
            echo $permiso;
        
    }
    function regresarcomplementoavigente(){
        $idf = $this->input->post('idf');
        $pass = $this->input->post('pass');
        log_message('error','regresarcomplementoavigente pass:'.$pass);
        //=================================================================
            $permiso=0;
            $permisos = $this->Login_model->permisoadminarray(array('1','17','18'));//1admin 17 julio 18 diana

            foreach ($permisos as $item) {
                $permisotemp=0;

                $usuario=$item->Usuario;

                $verificar = password_verify($pass,$item->contrasena);

                if ($verificar==false) {
                    $verificar = password_verify($pass,$item->clavestemp);
                    if ($verificar) {
                        $permisotemp=1;
                    }
                }
                if ($verificar) {
                    $permiso=1;
                    
                    $this->ModeloCatalogos->updateCatalogo('f_complementopago',array('Estado'=>1),array('complementoId'=>$idf));

                    $this->ModeloCatalogos->Insert('bitacora',array('contenido'=>'Se reactivo el complemento a vigente, persona que reactivo:'.$this->usuario,'nombretabla'=>'f_complementopago','idtable'=>$idf,'tipo'=>'Reactivacion complemento','personalId'=>$this->idpersonal));

                    if($permisotemp==1){
                        $this->ModeloCatalogos->updateCatalogo('usuarios',array('clavestemp'=>null),array('UsuarioID'=>$item->UsuarioID));
                    }

                    
                }
            }
            echo $permiso;
    }
    function inserupdatecomentario(){
        $params = $this->input->post();
        $this->ModeloCatalogos->updateCatalogo('alta_rentas_prefactura',array('comentario'=>$params['comen']),array('prefId'=>$params['prfId']));
    }
    function getcomentarioperiodo(){
        $params = $this->input->post();
        $result=$this->ModeloCatalogos->getselectwheren('alta_rentas_prefactura',array('prefId'=>$params['prfId']));
        $comen='';
        foreach ($result->result() as $item) {
            $comen.=strval($item->comentario);
        }
        echo preg_replace("/[\r\n|\n|\r]+/", " ", $comen);
    }
    function ocultarsolicitud(){
        $params = $this->input->post();
        $rowid=$params['rowid'];
        $tipo=$params['tipo'];

        if($tipo==1){
            $this->ModeloCatalogos->updateCatalogo('ventas_has_detallesEquipos',array('ocultarsolicitudserie'=>1),array('id'=>$rowid));
        }
        if($tipo==2){
            $this->ModeloCatalogos->updateCatalogo('ventas_has_detallesEquipos_accesorios',array('ocultarsolicitudserie'=>1),array('id_accesoriod'=>$rowid));
        }
        if($tipo==3){
            $this->ModeloCatalogos->updateCatalogo('ventas_has_detallesRefacciones',array('ocultarsolicitudserie'=>1),array('id'=>$rowid));
        }
        if($tipo==4){
            $this->ModeloCatalogos->updateCatalogo('rentas_has_detallesEquipos',array('ocultarsolicitudserie'=>1),array('id'=>$rowid));
        }
        if($tipo==5){
            $this->ModeloCatalogos->updateCatalogo('rentas_has_detallesEquipos_accesorios',array('ocultarsolicitudserie'=>1),array('id_accesoriod'=>$rowid));
        }
    }
    function verificarpagosafactura(){
        $datos = $this->input->post('datos');
        $DATA = json_decode($datos);
        $facturas=array();
        for ($i=0;$i<count($DATA);$i++) {
            $idfactura=$DATA[$i]->idfactura;
            $prefactura=0;
            //================================================venta
                $resultv=$this->ModeloCatalogos->facturaventas($idfactura);
                if($resultv->num_rows()>0){
                    $datos=$resultv->row();
                    $facturas[]=array(
                        'factura'=>$idfactura,
                        'pagado'=>$datos->estatus,
                        'prefactura'=>1
                    );    
                    $prefactura=1;
                }
            //===================================================
            //================================================venta
                $resultv=$this->ModeloCatalogos->facturaventascombinada($idfactura);
                if($resultv->num_rows()>0){
                    $datos=$resultv->row();
                    $facturas[]=array(
                        'factura'=>$idfactura,
                        'pagado'=>$datos->estatus,
                        'prefactura'=>1
                    );    
                    $prefactura=1;
                }
            //===================================================
            //================================================poliza
                $resultp=$this->ModeloCatalogos->facturapolizas($idfactura);
                if($resultp->num_rows()>0){
                    $datos=$resultp->row();
                    $facturas[]=array(
                        'factura'=>$idfactura,
                        'pagado'=>$datos->estatus,
                        'prefactura'=>1
                    );    
                    $prefactura=1;
                }
            //===================================================
            //================================================renta
                $resultr=$this->ModeloCatalogos->facturarenta($idfactura);
                if($resultr->num_rows()>0){
                    // agregar alguna funcion si en dado caso el estatus es 0 de no pagado, que verifique si la factura tiene complementos y si cubren la totalidad de la factura que el estatus se cambien a pagado
                    $datos=$resultr->row();
                    if($datos->statuspago==1){
                        $statuspago=3;
                    }else{
                        $statuspago=0;
                    }
                    $facturas[]=array(
                        'factura'=>$idfactura,
                        'pagado'=>$statuspago,
                        'prefactura'=>1
                    );    
                    $prefactura=1;
                }
            //===================================================
            if($prefactura===0){
                $resultfc=$this->ModeloCatalogos->getselectwheren('f_facturas',array('FacturasId'=>$idfactura));
                $datosf=$resultfc->row();
                if($datosf->pagada==1){
                    $statuspago=3;
                }else{
                    $statuspago=0;
                }
                $facturas[]=array(
                    'factura'=>$idfactura,
                    'pagado'=>$statuspago,
                    'prefactura'=>0
                );
            }
            
        }
        echo json_encode($facturas);
    }
    function editarfechaentrega(){
        $params = $this->input->post();
        $tipos=$params['tipo'];
        $fentregah='';
        if($tipos==1){
            $ventaid=$params['ventaid'];
            $fentrega=$params['fentrega'];
            $resultv=$this->ModeloCatalogos->getselectwheren('ventas',array('id'=>$ventaid));
            foreach ($resultv->result() as $item) {
                $fentregah=$item->fechaentregah.'<br> '.$item->fechaentrega;
            }


            $this->ModeloCatalogos->updateCatalogo('ventas',array('fechaentrega'=>$fentrega,'fechaentregah'=>$fentregah),array('id'=>$ventaid));    
        }
        if($tipos==2){
            $ventaid=$params['ventaid'];
            $fentrega=$params['fentrega'];
            $resultv=$this->ModeloCatalogos->getselectwheren('ventacombinada',array('combinadaId'=>$ventaid));
            $poliza=0;
            foreach ($resultv->result() as $item) {
                $fentregah=$item->fechaentregah.'<br> '.$item->fechaentrega;
                $poliza=$item->poliza;
            }
            $this->ModeloCatalogos->updateCatalogo('ventacombinada',array('fechaentrega'=>$fentrega,'fechaentregah'=>$fentregah),array('combinadaId'=>$ventaid));    
            if($poliza>0){
                $this->ModeloCatalogos->updateCatalogo('polizasCreadas',array('fechaentrega'=>$fentrega,'fechaentregah'=>$fentregah),array('id'=>$poliza)); 
            }
        }
        if($tipos==3){
            $ventaid=$params['ventaid'];
            $fentrega=$params['fentrega'];

            $resultv=$this->ModeloCatalogos->getselectwheren('rentas_historial',array('id'=>$ventaid));
            foreach ($resultv->result() as $item) {
                $fentregah=$item->fechaentregah.'<br> '.$item->fechaentrega;
            }
            $this->ModeloCatalogos->updateCatalogo('rentas_historial',array('fechaentrega'=>$fentrega,'fechaentregah'=>$fentregah),array('id'=>$ventaid));    
        }
        if($tipos==4){
            $ventaid=$params['ventaid'];
            $fentrega=$params['fentrega'];
            $resultv=$this->ModeloCatalogos->getselectwheren('ventasd',array('id'=>$ventaid));
            foreach ($resultv->result() as $item) {
                $fentregah=$item->fechaentregah.'<br> '.$item->fechaentrega;
            }


            $this->ModeloCatalogos->updateCatalogo('ventasd',array('fechaentrega'=>$fentrega,'fechaentregah'=>$fentregah),array('id'=>$ventaid));    
        }
        if($tipos==5){
            $ventaid=$params['ventaid'];
            $fentrega=$params['fentrega'];
            $resultv=$this->ModeloCatalogos->getselectwheren('polizasCreadas',array('id'=>$ventaid));
            foreach ($resultv->result() as $item) {
                $fentregah=$item->fechaentregah.'<br> '.$item->fechaentrega;
            }


            $this->ModeloCatalogos->updateCatalogo('polizasCreadas',array('fechaentrega'=>$fentrega,'fechaentregah'=>$fentregah),array('id'=>$ventaid));    
        }
    }
    function permisocontinuar(){
        $contrato = $this->input->post('contrato');
        $c_fecha=date("Y-m-d",strtotime($this->fechah."+ 4 month")); 
        $this->ModeloCatalogos->updateCatalogo('alta_rentas',array('continuarcontadores'=>1,'conti_cont_fecha'=>$c_fecha),array('idcontrato'=>$contrato));
    }
    function buscarseriefolio(){
        $params = $this->input->post();
        $id=$params['id'];
        $buscar=$params['buscar'];
        $result=$this->ModeloGeneral->buscarseriefolio($id,$buscar);
        echo json_encode($result->result());
    }
    function entregas(){
        $params=$this->input->post();
        $id=$params['id'];
        $table=$params['table'];
        $personal=$params['personal'];

        switch ($table) {
            case 1:
                $tableselect='ventas_has_detallesConsumibles';
                $idname='id';
                break;
            case 2:
                $tableselect='ventas_has_detallesEquipos';
                $idname='id';
                break;
            case 3:
                $tableselect='ventas_has_detallesEquipos_accesorios';
                $idname='id_accesoriod';
                break;
            case 4:
                $tableselect='ventas_has_detallesequipos_consumible';
                $idname='id';
                break;
            case 5:
                $tableselect='ventas_has_detallesRefacciones';
                $idname='id';
                break;
            case 6:
                $tableselect='rentas_historial_accesorios';
                $idname='';
                break;
            case 7:
                $tableselect='rentas_historial_consumibles';
                $idname='';
                break;
            case 8:
                $tableselect='rentas_historial_equipos';
                $idname='';
                break;
            case 9:
                $tableselect='rentas_has_detallesEquipos';
                $idname='id';
                break;
            case 10:
                $tableselect='rentas_has_detallesEquipos_accesorios';
                $idname='id_accesoriod';
                break;
            case 11:
                $tableselect='rentas_has_detallesequipos_consumible';
                $idname='id';
                break;
            
            default:
                // code...
                break;
        }
        $arraydatos=array(
                    'entregado_status'=>1,
                    'entregado_personal'=>$personal,
                    'entregado_fecha'=>$this->fechahoy
                    );
        if($table==3 or $table==2 or $table==9 or $table==10){
            $arraydatos['serie_bodega']=$params['bode'];
        }
        $this->ModeloCatalogos->updateCatalogo($tableselect,$arraydatos,array($idname=>$id));
        if($table==5){//ventas_has_detallesRefacciones
            $piezas=0;
            $idRefaccion=0;
            $result =$this->ModeloCatalogos->getselectwheren('ventas_has_detallesRefacciones',array('id'=>$id));
            foreach ($result->result() as $itemr) {
                $bodegas=$itemr->serie_bodega;
                $idRefaccion=$itemr->idRefaccion;
                $piezas=$itemr->piezas;

            }
            $strq="SELECT asr.id_asig,sr.* FROM asignacion_series_refacciones as asr INNER JOIN series_refacciones as sr on sr.serieId=asr.serieId WHERE asr.id_refaccion='$id'";
            $query = $this->db->query($strq);
            foreach ($query->result() as $item_sr) {
                $serieId_o = $item_sr->serieId;
                $id_asig =$item_sr->id_asig;
                if($item_sr->bodegaId!=$params['bode']){
                    if($item_sr->con_serie==0){
                        $this->ModeloCatalogos->updatestock('series_refacciones','cantidad','+',$piezas,'serieId',$serieId_o);$this->Configuraciones_model->registrarmovimientosseries($serieId_o,3);

                        $result_sr =$this->ModeloCatalogos->getselectwheren('series_refacciones',array('bodegaId'=>$params['bode'],'refaccionid'=>$idRefaccion,'activo'=>1));
                        if($result_sr->num_rows()>0){
                            $result_sr=$result_sr->row();
                            $serieId_d=$result_sr->serieId;
                        }else{
                            $serieId_d=0;
                        }
                        $this->ModeloCatalogos->updatestock('series_refacciones','cantidad','-',$piezas,'serieId',$serieId_d);$this->Configuraciones_model->registrarmovimientosseries($serieId_d,3);
                        if($serieId_d>0){
                            $this->ModeloCatalogos->updateCatalogo('asignacion_series_refacciones',array('serieId'=>$serieId_d),array('id_asig'=>$id_asig));
                        }
                    }else{
                        $this->ModeloCatalogos->updateCatalogo('series_refacciones',array('status'=>0),array('serieId'=>$serieId_o));$this->Configuraciones_model->registrarmovimientosseries($serieId_o,3);
                        $result_sr =$this->ModeloCatalogos->getselectwheren('series_refacciones',array('bodegaId'=>$params['bode'],'refaccionid'=>$idRefaccion,'activo'=>1));
                        if($result_sr->num_rows()>0){
                            $result_sr=$result_sr->row();
                            $serieId_d=$result_sr->serieId;
                            $this->ModeloCatalogos->updateCatalogo('series_refacciones',array('status'=>3),array('serieId'=>$serieId_d));
                        }
                    }
                    //1 equipo, 2 acesorio, 3 refaccion,4 consumible    
                    $this->ModeloCatalogos->Insert('bitacora_movimientos',array('id_item'=>0,'cantidad'=>$piezas,'modeloId'=>$idRefaccion,'tipo'=>3,'entrada_salida'=>1,'personal'=>$this->idpersonal,'descripcion_evento'=>'Entrada por cambio de bodega en venta','serieId'=>$serieId_o,'clienteId'=>0,'bodega_d'=>$bodegas));
                        //1 equipo, 2 acesorio, 3 refaccion,4 consumible    
                    $this->ModeloCatalogos->Insert('bitacora_movimientos',array('id_item'=>0,'cantidad'=>$piezas,'modeloId'=>$idRefaccion,'tipo'=>3,'entrada_salida'=>0,'personal'=>$this->idpersonal,'descripcion_evento'=>'Salida por cambio de bodega en venta','serieId'=>$serieId_d,'clienteId'=>0,'bodega_o'=>$params['bode']));
                    $this->ModeloCatalogos->updateCatalogo('ventas_has_detallesRefacciones',array('serie_bodega'=>$params['bode']),array('id'=>$id));
                }
            }

        }
        if($table==1){//ventas_has_detallesConsumibles
            $bode=$params['bode'];
            $result =$this->ModeloCatalogos->getselectwheren('ventas_has_detallesConsumibles',array('id'=>$id));
            foreach ($result->result() as $item) {
                $bodegas=$item->bodegas;
                $idConsumible=$item->idConsumible;
                $piezas=$item->piezas;
                if($bodegas>0){
                    if($bodegas!=$bode){
                        $this->ModeloCatalogos->updatestock3('consumibles_bodegas','total','+',$piezas,'bodegaId',$bodegas,'consumiblesId',$idConsumible,'status',1);
                        $this->ModeloCatalogos->updatestock3('consumibles_bodegas','total','-',$piezas,'bodegaId',$bode,'consumiblesId',$idConsumible,'status',1);
                        $this->ModeloCatalogos->updateCatalogo('ventas_has_detallesConsumibles',array('bodegas'=>$bode),array('id'=>$id));
                        //1 equipo, 2 acesorio, 3 refaccion,4 consumible    
                        $this->ModeloCatalogos->Insert('bitacora_movimientos',array('id_item'=>0,'cantidad'=>$piezas,'modeloId'=>$idConsumible,'tipo'=>4,'entrada_salida'=>1,'personal'=>$this->idpersonal,'descripcion_evento'=>'Entrada por cambio de bodega en venta','serieId'=>0,'clienteId'=>0,'bodega_d'=>$bodegas));
                        //1 equipo, 2 acesorio, 3 refaccion,4 consumible    
                        $this->ModeloCatalogos->Insert('bitacora_movimientos',array('id_item'=>0,'cantidad'=>$piezas,'modeloId'=>$idConsumible,'tipo'=>4,'entrada_salida'=>0,'personal'=>$this->idpersonal,'descripcion_evento'=>'Salida por cambio de bodega en venta','serieId'=>0,'clienteId'=>0,'bodega_o'=>$bode));
                    }
                }else{
                    $this->ModeloCatalogos->updateCatalogo('ventas_has_detallesConsumibles',array('bodegas'=>$bode),array('id'=>$id));
                }
            }

        }
        if($table==11){//rentas_has_detallesequipos_consumible
            $bode=$params['bode'];
            $result =$this->ModeloCatalogos->getselectwheren('rentas_has_detallesequipos_consumible',array('id'=>$id));
            foreach ($result->result() as $item) {
                $bodegas=$item->bodega;
                $idConsumible=$item->id_consumibles;
                $piezas=$item->cantidad;
                if($bodegas>0){
                    if($bodegas!=$bode){
                        $this->ModeloCatalogos->updatestock3('consumibles_bodegas','total','+',$piezas,'bodegaId',$bodegas,'consumiblesId',$idConsumible,'status',1);
                        $this->ModeloCatalogos->updatestock3('consumibles_bodegas','total','-',$piezas,'bodegaId',$bode,'consumiblesId',$idConsumible,'status',1);
                        $this->ModeloCatalogos->updateCatalogo('rentas_has_detallesequipos_consumible',array('bodega'=>$bode),array('id'=>$id));
                        //1 equipo, 2 acesorio, 3 refaccion,4 consumible    
                        $this->ModeloCatalogos->Insert('bitacora_movimientos',array('id_item'=>0,'cantidad'=>$piezas,'modeloId'=>$idConsumible,'tipo'=>4,'entrada_salida'=>1,'personal'=>$this->idpersonal,'descripcion_evento'=>'Entrada por cambio de bodega en renta','serieId'=>0,'clienteId'=>0,'bodega_d'=>$bodegas));
                        //1 equipo, 2 acesorio, 3 refaccion,4 consumible    
                        $this->ModeloCatalogos->Insert('bitacora_movimientos',array('id_item'=>0,'cantidad'=>$piezas,'modeloId'=>$idConsumible,'tipo'=>4,'entrada_salida'=>0,'personal'=>$this->idpersonal,'descripcion_evento'=>'Salida por cambio de bodega en renta','serieId'=>0,'clienteId'=>0,'bodega_o'=>$bode));
                    }
                }else{
                    $this->ModeloCatalogos->updateCatalogo('rentas_has_detallesequipos_consumible',array('bodega'=>$bode),array('id'=>$id));
                }
            }
        }
        if($table==4){//ventas_has_detallesequipos_consumible
            $bode=$params['bode'];
            $result =$this->ModeloCatalogos->getselectwheren('ventas_has_detallesequipos_consumible',array('id'=>$id));
            foreach ($result->result() as $item) {
                $bodegas=$item->bodega;
                $idConsumible=$item->id_consumibles;
                $piezas=$item->cantidad;
                if($bodegas>0){
                    if($bodegas!=$bode){
                        $this->ModeloCatalogos->updatestock3('consumibles_bodegas','total','+',$piezas,'bodegaId',$bodegas,'consumiblesId',$idConsumible,'status',1);
                        $this->ModeloCatalogos->updatestock3('consumibles_bodegas','total','-',$piezas,'bodegaId',$bode,'consumiblesId',$idConsumible,'status',1);
                        $this->ModeloCatalogos->updateCatalogo('ventas_has_detallesequipos_consumible',array('bodega'=>$bode),array('id'=>$id));
                        //1 equipo, 2 acesorio, 3 refaccion,4 consumible    
                        $this->ModeloCatalogos->Insert('bitacora_movimientos',array('id_item'=>0,'cantidad'=>$piezas,'modeloId'=>$idConsumible,'tipo'=>4,'entrada_salida'=>1,'personal'=>$this->idpersonal,'descripcion_evento'=>'Entrada por cambio de bodega en venta','serieId'=>0,'clienteId'=>0,'bodega_d'=>$bodegas));
                        //1 equipo, 2 acesorio, 3 refaccion,4 consumible    
                        $this->ModeloCatalogos->Insert('bitacora_movimientos',array('id_item'=>0,'cantidad'=>$piezas,'modeloId'=>$idConsumible,'tipo'=>4,'entrada_salida'=>0,'personal'=>$this->idpersonal,'descripcion_evento'=>'Salida por cambio de bodega en venta','serieId'=>0,'clienteId'=>0,'bodega_o'=>$bode));
                    }
                }else{
                    $this->ModeloCatalogos->updateCatalogo('ventas_has_detallesequipos_consumible',array('bodega'=>$bode),array('id'=>$id));
                }
            }
        }
    }
    // funcion parcialmente igual al controlador Contratos
    public function eliminarRentaE($idrow_equipo,$motivo){   
        //==================================
            $resulter = $this->ModeloCatalogos->db13_getselectwheren('rentas_has_detallesEquipos',array('id'=>$idrow_equipo));
            $idEquipo=0;
            $modeloe='';
            $idRenta=0;
            $clienteid=0;
            foreach ($resulter->result() as $itemer) {
                $idRenta=$itemer->idRenta;
                $idEquipo=$itemer->idEquipo;
                $modeloe=$itemer->modelo;
            }
            $resulteren = $this->ModeloCatalogos->db13_getselectwheren('rentas',array('id'=>$idRenta));
            foreach ($resulteren->result() as $itemeren) {
                $clienteid=$itemeren->idCliente;
            }
        //==================================
        $resultseries=$this->ModeloCatalogos->db13_getselectwheren('asignacion_series_r_equipos',array('id_equipo'=>$idrow_equipo));
        foreach ($resultseries->result() as $item) {
            $this->ModeloCatalogos->updateCatalogo('series_productos',array('status'=>0,'bodegaId'=>2,'usado'=>1),array('serieId'=>$item->serieId));
            $this->ModeloCatalogos->updateCatalogo('asignacion_series_r_equipos',array('activo'=>0),array('id_equipo'=>$idrow_equipo,'serieId'=>$item->serieId));
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_contrato_a_e',array('status'=>3,'motivosuspencion'=>'Eliminacion y/o Cambio'),array('serieId'=>$item->serieId,'idequipo'=>$idrow_equipo,'fecha >='=>$this->fechahoy,'status'=>0));
            $this->Configuraciones_model->registrarmovimientosseries($item->serieId,1);
            //1 equipo, 2 acesorio, 3 refaccion,4 consumible    
            $this->ModeloCatalogos->Insert('bitacora_movimientos',array('entrada_salida'=>1,'descripcion_evento'=>'devolucion de renta: '.$idRenta.','.$motivo,'personal'=>$this->idpersonal,'tipo'=>1,'id_item'=>$idrow_equipo,'modeloId'=>$idEquipo,'modelo'=>$modeloe,'serieId'=>$item->serieId,'cantidad'=>1,'bodega_d'=>2,'clienteId'=>$clienteid));
        }

        //=======================================
        //==============================================================
        $resultseriesa=$this->ModeloCatalogos->getselectwheren('rentas_has_detallesEquipos_accesorios',array('rowequipo'=>$idrow_equipo));
        foreach ($resultseriesa->result() as $itema) {
            $itema->personal_dev=$this->idpersonal;
            $itema->motivo_dev=$motivo;


            $this->ModeloCatalogos->Insert('rentas_has_detallesEquipos_accesorios_dev',$itema);

            $id_accesoriod=$itema->id_accesoriod;
            $resultseriesa0=$this->ModeloCatalogos->getselectwheren('asignacion_series_r_accesorios',array('id_accesorio'=>$id_accesoriod));
            foreach ($resultseriesa0->result() as $item0) {
                $serieaccesorio=$item0->serieId;
                $this->ModeloCatalogos->updateCatalogo('series_accesorios',array('status'=>0,'bodegaId'=>2),array('serieId'=>$serieaccesorio));$this->Configuraciones_model->registrarmovimientosseries($serieaccesorio,2);
            }
            
            $this->ModeloCatalogos->getdeletewheren('asignacion_series_r_accesorios',array('id_accesorio'=>$id_accesoriod));

            //1 equipo, 2 acesorio, 3 refaccion,4 consumible    
            $this->ModeloCatalogos->Insert('bitacora_movimientos',array('entrada_salida'=>1,'descripcion_evento'=>'devolucion de renta: '.$idRenta.','.$motivo,'personal'=>$this->idpersonal,'tipo'=>2,'id_item'=>$itema->id_accesoriod,'modeloId'=>$itema->id_accesorio,'modelo'=>'','serieId'=>$serieaccesorio,'cantidad'=>1,'bodega_d'=>2,'clienteId'=>$clienteid));
        }
        //==============================================================
        $resultconsum=$this->ModeloCatalogos->getselectwheren('rentas_has_detallesequipos_consumible',array('rowequipo'=>$idrow_equipo));
        foreach ($resultconsum->result() as $itemconsu) {
            $itemconsu->personal_dev=$this->idpersonal;
            $itemconsu->motivo_dev=$motivo;
            $this->ModeloCatalogos->Insert('rentas_has_detallesequipos_consumible_dev',$itemconsu);
        }

        $this->ModeloCatalogos->getdeletewheren('rentas_has_detallesequipos_consumible',array('rowequipo'=>$idrow_equipo));
        $this->ModeloCatalogos->getdeletewheren('rentas_has_detallesEquipos_accesorios',array('rowequipo'=>$idrow_equipo));
        //$this->ModeloCatalogos->getdeletewheren('asignacion_series_r_equipos',array('id_equipo'=>$idrow_equipo));


        $this->ModeloCatalogos->getdeletewheren('contrato_equipos',array('equiposrow'=>$idrow_equipo));
        //$this->ModeloCatalogos->getdeletewheren('rentas_has_detallesEquipos',array('id'=>$idrow_equipo));
        $this->ModeloCatalogos->updateCatalogo('rentas_has_detallesEquipos',array('estatus'=>0,'motivo_dev'=>$motivo,'personal_dev'=>$this->idpersonal),array('id'=>$idrow_equipo));
    }
    function consumibledevolucion($idrow,$motivo){
        $resultconsum=$this->ModeloCatalogos->getselectwheren('rentas_has_detallesequipos_consumible',array('id'=>$idrow));
        foreach ($resultconsum->result() as $itemconsu) {
            $itemconsu->personal_dev=$this->idpersonal;
            $itemconsu->motivo_dev=$motivo;
            $this->ModeloCatalogos->Insert('rentas_has_detallesequipos_consumible_dev',$itemconsu);
        }
        $this->ModeloCatalogos->getdeletewheren('rentas_has_detallesequipos_consumible',array('id'=>$idrow));
    }
    function accesoriosdevolucion($idrow,$motivo,$tipo){
        $resultseriesa=$this->ModeloCatalogos->getselectwheren('rentas_has_detallesEquipos_accesorios',array('id_accesoriod'=>$idrow));
        foreach ($resultseriesa->result() as $itema) {
            $itema->personal_dev=$this->idpersonal;
            $itema->motivo_dev=$motivo;
            $idrentas=$itema->idrentas;
            $id_accesorio=$itema->id_accesorio;

            $resulteren = $this->ModeloCatalogos->db13_getselectwheren('rentas',array('id'=>$idrentas));
            $clienteid=0;
            foreach ($resulteren->result() as $itemeren) {
                $clienteid=$itemeren->idCliente;
            }
            if($tipo==0){
                $this->ModeloCatalogos->Insert('rentas_has_detallesEquipos_accesorios_dev',$itema);
            }
            

            $id_accesoriod=$itema->id_accesoriod;
            $resultseriesa0=$this->ModeloCatalogos->getselectwheren('asignacion_series_r_accesorios',array('id_accesorio'=>$id_accesoriod));
            foreach ($resultseriesa0->result() as $item0) {
                $serieaccesorio=$item0->serieId;
                //$this->ModeloCatalogos->updateCatalogo('series_accesorios',array('status'=>0,'bodegaId'=>2),array('serieId'=>$serieaccesorio));
                $info_serie_acc=$this->ModeloCatalogos->getselectwheren('series_accesorios',array('serieId'=>$serieaccesorio));
                if($info_serie_acc->num_rows()>0){
                    $info_serie_acc=$info_serie_acc->row();
                    if($info_serie_acc->con_serie==1){
                        $this->ModeloCatalogos->updateCatalogo('series_accesorios',array('status'=>0,'bodegaId'=>2),array('serieId'=>$serieaccesorio));$this->Configuraciones_model->registrarmovimientosseries($serieaccesorio,2);
                    }else{
                        $this->ModeloCatalogos->updatestock('series_accesorios','cantidad','+',1,'serieId',$serieaccesorio);$this->Configuraciones_model->registrarmovimientosseries($serieaccesorio,2);
                    }
                }

                //1 equipo, 2 acesorio, 3 refaccion,4 consumible    
                $this->ModeloCatalogos->Insert('bitacora_movimientos',array('entrada_salida'=>1,'descripcion_evento'=>'devolucion de renta: '.$idrentas.','.$motivo,'personal'=>$this->idpersonal,'tipo'=>2,'id_item'=>$idrow,'modeloId'=>$id_accesorio,'modelo'=>'','serieId'=>$serieaccesorio,'cantidad'=>1,'bodega_d'=>2,'clienteId'=>$clienteid));
            }
            
            $this->ModeloCatalogos->getdeletewheren('asignacion_series_r_accesorios',array('id_accesorio'=>$id_accesoriod));
        }
        $this->ModeloCatalogos->getdeletewheren('rentas_has_detallesEquipos_accesorios',array('id_accesoriod'=>$idrow));
    }
    function agregarserviciointalacion(){
        $idhistorial = $this->input->post('idhistorial');
        $tipo = $this->input->post('tipo');

        $resultrh=$this->ModeloCatalogos->getselectwheren('rentas_historial',array('id'=>$idhistorial));
        $resultrh=$resultrh->row();
        if($resultrh->rentas>0){
            $resultcn=$this->ModeloCatalogos->getselectwheren('contrato',array('idRenta'=>$resultrh->rentas));
            $resultcn=$resultcn->row();
            if($resultcn->idcontrato>0){
                if($tipo==0){
                    $tsermot=' de accesorio';
                }else{
                    $tsermot='';
                }
                $serviciodata= array(
                                        'contratoId'=>$resultcn->idcontrato,
                                        'prioridad'=>1,
                                        'zonaId'=>1,//sin zona
                                        'tpoliza'=>17,//Servicios de instalación
                                        'tservicio_a'=>876,//sin modelo
                                        'ttipo'=>1,
                                        'tserviciomotivo'=>'Servicio de instalación'.$tsermot,
                                        'servicio_auto_inst'=>1,
                                        'creado_personal'=>$this->idpersonal
                                    );
                $asignacionId=$this->ModeloCatalogos->Insert('asignacion_ser_contrato_a',$serviciodata);
                if($resultrh->fechaentrega!='' and $resultrh->fechaentrega!=null and $resultrh->fechaentrega!='null'){
                    $fechaentrega=$resultrh->fechaentrega;
                }else{
                    $fechaentrega = strtotime ( '+ 1 day' , strtotime ($this->fechahoy));
                    $fechaentrega = date ( 'Y-m-d' , $fechaentrega);
                }
                if($tipo==0){
                    $resulteq=$this->ModeloGeneral->infoaccesorioshistorial($idhistorial,1);
                    $dataeqarray=array();
                    foreach ($resulteq->result() as $iteme) {
                        $datainfoeq=array(
                                    'asignacionId'=>$asignacionId,
                                    'idequipo'=>$iteme->rowequipo,
                                    'serieId'=>$iteme->serieId,
                                    'tecnico'=>49,
                                    'fecha'=>$fechaentrega,
                                    'hora'=>'09:00:00',
                                    'horafin'=>'18:00:00',
                                    'hora_comida'=>null,
                                    'horafin_comida'=>null
                                );
                            $dataeqarray[]=$datainfoeq;
                    }
                    if(count($dataeqarray)>0){
                        $this->ModeloCatalogos->insert_batch('asignacion_ser_contrato_a_e',$dataeqarray);
                    }
                    $resulteq=$this->ModeloGeneral->infoaccesorioshistorial($idhistorial,0);
                        $comentariocal='';
                    foreach ($resulteq->result() as $iteme) {
                        $comentariocal.=$iteme->cantidad.' '.$iteme->nombre;
                    }
                    $this->ModeloCatalogos->updateCatalogo('asignacion_ser_contrato_a',array('comentariocal'=>$comentariocal),array('asignacionId'=>$asignacionId));
                }
                if($tipo==1){
                    $resulteq=$this->ModeloCatalogos->getselectwheren('rentas_historial_equipos',array('historialid'=>$idhistorial));
                    $dataeqarray=array();
                    foreach ($resulteq->result() as $iteme) {
                        $datainfoeq=array(
                                    'asignacionId'=>$asignacionId,
                                    'idequipo'=>$iteme->equipo,
                                    'serieId'=>0,
                                    'tecnico'=>49,
                                    'fecha'=>$fechaentrega,
                                    'hora'=>'09:00:00',
                                    'horafin'=>'18:00:00',
                                    'hora_comida'=>null,
                                    'horafin_comida'=>null
                                );
                            $dataeqarray[]=$datainfoeq;
                    }
                    if(count($dataeqarray)>0){
                        $this->ModeloCatalogos->insert_batch('asignacion_ser_contrato_a_e',$dataeqarray);
                    }
                }
            }
        }
    }
    function verificarultimascoti(){
        $params = $this->input->post();
        $idcli  = $params['idcli'];
        $idr    = $params['idr'];

        $fecha_actual = $this->fechah;
        $fecha_actual = date("d-m-Y",strtotime($fecha_actual."- 1 month")); 

        $fecha = $fecha_actual;
        $result = $this->ModeloGeneral->vrefcoti($idcli,$idr,$fecha_actual);
        echo $result->num_rows();
    }
    function confirmarpagos(){
        $params = $this->input->post();
        $id     = $params['id'];
        $tipo   = $params['tipo'];
        if($tipo==0){
            $this->ModeloCatalogos->updateCatalogo('pagos_ventas',array('confirmado'=>1),array('idpago'=>$id));
        }
        if($tipo==1){
            $this->ModeloCatalogos->updateCatalogo('pagos_combinada',array('confirmado'=>1),array('idpago'=>$id));
        }
        if($tipo==3){
            $this->ModeloCatalogos->updateCatalogo('pagos_poliza',array('confirmado'=>1),array('idpago'=>$id));
        }
        if($tipo==4){
            $this->ModeloCatalogos->updateCatalogo('factura_prefactura_pagos',array('confirmado'=>1),array('fp_pagosId'=>$id));
        }
        if($tipo==5){
            $this->ModeloCatalogos->updateCatalogo('ventasd_pagos',array('confirmado'=>1),array('idpago'=>$id));
        }
    }
    function desvfactura(){
        $params = $this->input->post();
        $id     = $params['id'];
        $tipo   = $params['tipo'];
        if($tipo<=1){
            $this->ModeloCatalogos->getdeletewheren('factura_venta',array('ventaId'=>$id,'combinada'=>$tipo));
        }
        if($tipo==3){
            $this->ModeloCatalogos->getdeletewheren('factura_poliza',array('polizaId'=>$id));
        }
    }
    function verificardev(){
        $datos = $this->input->post('datos');
        //log_message('error', $datos);
        $DATA = json_decode($datos);
        $prefacturas=array();
        for ($i=0;$i<count($DATA);$i++) {
            $id         = $DATA[$i]->id;
            $tipotabla  = $DATA[$i]->tipotabla;
            $id_rh      = $DATA[$i]->id_rh;
            $renta_rh   = $DATA[$i]->renta_rh;
            /*
                $tipotabla
                1 ventas
                2 rentas
                4 combinada
                5 renta historial
            */
            if($tipotabla==1){
                $viewbodegas=array();
                $mensaje='';
                $totalitems=0;$totalitems_dev=0;
                $resultadoequipos = $this->ModeloCatalogos->db6_getselectwheren('ventas_has_detallesEquipos',array('idVenta'=>$id));
                $totalitems=$totalitems+$resultadoequipos->num_rows();
                $resultadoaccesorios = $this->ModeloCatalogos->accesoriosventa($id);
                $totalitems=$totalitems+$resultadoaccesorios->num_rows();
                $resultadoconsumibles = $this->ModeloCatalogos->consumiblesventa($id);
                $totalitems=$totalitems+$resultadoconsumibles->num_rows();
                $consumiblesventadetalles = $this->ModeloCatalogos->consumiblesventadetalles($id);
                $totalitems=$totalitems+$consumiblesventadetalles->num_rows();
                $ventadetallesrefacion = $this->ModeloCatalogos->ventas_has_detallesRefacciones($id);
                $totalitems=$totalitems+$ventadetallesrefacion->num_rows();
                //=======================================================
                    foreach ($resultadoequipos->result() as $item) {
                        $viewbodegas[]=$this->ModeloCatalogos->obtenerbodega($item->serie_bodega);
                    }
                    foreach($resultadoaccesorios->result() as $item){
                        $viewbodegas[]=$item->bodega;
                    }
                    foreach($resultadoconsumibles->result() as $item){
                        $viewbodegas[]=$item->bodega;
                    }
                    foreach($consumiblesventadetalles->result() as $item){
                        $viewbodegas[]=$item->bodega;
                    }
                    foreach($ventadetallesrefacion->result() as $item){
                        $viewbodegas[]=$item->bodega;
                    }
                //=======================================================


                $resultadoequipos_dev = $this->ModeloCatalogos->db6_getselectwheren('ventas_has_detallesEquipos_dev',array('idVenta'=>$id));
                $totalitems_dev=$totalitems_dev+$resultadoequipos_dev->num_rows();
                $resultadoaccesorios_dev = $this->ModeloCatalogos->accesoriosventa_dev($id);
                $totalitems_dev=$totalitems_dev+$resultadoaccesorios_dev->num_rows();
                $resultadoconsumibles_dev = $this->ModeloCatalogos->consumiblesventa_dev($id);
                $totalitems_dev=$totalitems_dev+$resultadoconsumibles_dev->num_rows();
                $consumiblesventadetalles_dev = $this->ModeloCatalogos->consumiblesventadetalles_dev($id);
                $totalitems_dev=$totalitems_dev+$consumiblesventadetalles_dev->num_rows();
                $ventadetallesrefacion_dev = $this->ModeloCatalogos->ventas_has_detallesRefacciones_dev($id);
                $totalitems_dev=$totalitems_dev+$ventadetallesrefacion_dev->num_rows();
                if($totalitems_dev>0){
                    if($totalitems>0){
                        $mensaje='Devolución Parcial';
                    }else{
                        $mensaje='Devolución Total';
                        $this->ModeloCatalogos->updateCatalogo('ventas',array('e_entrega'=>0,'activo'=>0,'motivo'=>'eliminacion por devolucion total'),array('id'=>$id));
                    }
                }
                $prefacturas[]=array('id'=>$id,'tipotabla'=>$tipotabla,'mensaje'=>$mensaje,'bodegas'=>implode(', ', array_unique($viewbodegas)));
            }
            if($tipotabla==5){
                $viewbodegas=array();
                $mensaje='';
                $totalitems=0;$totalitems_dev=0;
                $rentaventash = $this->ModeloCatalogos->rentaventasdh($renta_rh,$id_rh);
                foreach ($rentaventash->result() as $item) { 
                    if($item->personal_dev>0){
                        $totalitems_dev=$totalitems_dev+$rentaventash->num_rows();
                    }else{
                        $totalitems=$totalitems+$rentaventash->num_rows();
                    }
                     $viewbodegas[]=$item->bodega;
                }
                
                $rentavacessoriosh = $this->ModeloCatalogos->rentavacessoriosh($renta_rh,$id_rh);
                $totalitems=$totalitems+$rentavacessoriosh->num_rows();
                $rentavconsumiblesh = $this->ModeloCatalogos->rentavconsumiblesh($renta_rh,$id_rh);
                $totalitems=$totalitems+$rentavconsumiblesh->num_rows();
                //================================================================
                    foreach($rentavacessoriosh->result() as $item){
                        $viewbodegas[]=$item->bodega;
                    }
                    foreach($rentavconsumiblesh->result() as $item){
                        $viewbodegas[]=$item->bodegav;
                    }
                //================================================================

                $rentavacessoriosh_dev = $this->ModeloCatalogos->rentavacessoriosh_dev($renta_rh,$id_rh);
                $totalitems_dev=$totalitems_dev+$rentavacessoriosh_dev->num_rows();
                $rentavconsumiblesh_dev = $this->ModeloCatalogos->rentavconsumiblesh_dev($renta_rh,$id_rh);
                $totalitems_dev=$totalitems_dev+$rentavconsumiblesh_dev->num_rows();

                if($totalitems_dev>0){
                    if($totalitems>0){
                        $mensaje='Devolución Parcial';
                    }else{
                        $mensaje='Devolución Total';
                    }
                }
                $prefacturas[]=array('id'=>$id,'tipotabla'=>$tipotabla,'mensaje'=>$mensaje,'bodegas'=>implode(', ', array_unique($viewbodegas)));
            }
            if($tipotabla==4){
                $viewbodegas=array();
                $mensaje='';
                $totalitems=0;$totalitems_dev=0;

                $where_ventas=array('combinadaId'=>$id);
                $resultadov = $this->ModeloCatalogos->db6_getselectwheren('ventacombinada',$where_ventas);
                $id_personal=0;
                foreach ($resultadov->result() as $item) {
                    $id_personal=$item->id_personal;
                    $idCliente=$item->idCliente;
                    $equipo=$item->equipo;
                    $consumibles=$item->consumibles;
                    $refacciones=$item->refacciones;
                    $poliza=$item->poliza;
                    
                }
                $resultadoequipos = $this->ModeloCatalogos->db6_getselectwheren('ventas_has_detallesEquipos',array('idVenta'=>$equipo));
                $totalitems=$totalitems+$resultadoequipos->num_rows();
                $resultadoaccesorios = $this->ModeloCatalogos->accesoriosventa($equipo);
                $totalitems=$totalitems+$resultadoaccesorios->num_rows();
                $resultadoconsumibles = $this->ModeloCatalogos->consumiblesventa($equipo);
                $totalitems=$totalitems+$resultadoconsumibles->num_rows();
                $consumiblesventadetalles = $this->ModeloCatalogos->consumiblesventadetalles($consumibles);
                $totalitems=$totalitems+$consumiblesventadetalles->num_rows();
                $ventadetallesrefacion = $this->ModeloCatalogos->ventas_has_detallesRefacciones($refacciones);
                $totalitems=$totalitems+$ventadetallesrefacion->num_rows();
                $resultadopoliza = $this->ModeloCatalogos->db6_getselectwheren('polizasCreadas_has_detallesPoliza',array('idPoliza'=>$poliza));
                foreach ($resultadopoliza->result() as $itemser) {
                    $precio=0;
                    if($itemser->precio_local>0){
                        $precio=$itemser->precio_local;
                    }
                    if($itemser->precio_semi>0){
                        $precio=$itemser->precio_semi;
                    }
                    if($itemser->precio_foraneo>0){
                        $precio=$itemser->precio_foraneo;
                    }
                    if($itemser->precio_especial>0){
                        $precio=$itemser->precio_especial;
                    }
                    if($precio>0){
                        //$totalitems_dev=$totalitems_dev+1;
                        $totalitems=$totalitems+1;
                    }
                    // code...
                }
                //=======================================================
                    foreach ($resultadoequipos->result() as $item) {
                        $viewbodegas[]=$this->ModeloCatalogos->obtenerbodega($item->serie_bodega);
                    }
                    foreach($resultadoaccesorios->result() as $item){
                        $viewbodegas[]=$item->bodega;
                    }
                    foreach($resultadoconsumibles->result() as $item){
                        $viewbodegas[]=$item->bodega;
                    }
                    foreach($consumiblesventadetalles->result() as $item){
                        $viewbodegas[]=$item->bodega;
                    }
                    foreach($ventadetallesrefacion->result() as $item){
                        $viewbodegas[]=$item->bodega;
                    }
                //=======================================================
                $resultadoequipos_dev = $this->ModeloCatalogos->db6_getselectwheren('ventas_has_detallesEquipos_dev',array('idVenta'=>$equipo));
                $totalitems_dev=$totalitems_dev+$resultadoequipos_dev->num_rows();
                $resultadoconsumibles_dev = $this->ModeloCatalogos->consumiblesventa_dev($equipo);
                $totalitems_dev=$totalitems_dev+$resultadoconsumibles_dev->num_rows();
                $consumiblesventadetalles_dev = $this->ModeloCatalogos->consumiblesventadetalles_dev($consumibles);
                $totalitems_dev=$totalitems_dev+$consumiblesventadetalles_dev->num_rows();
                $ventadetallesrefacion_dev = $this->ModeloCatalogos->ventas_has_detallesRefacciones_dev($refacciones);
                $totalitems_dev=$totalitems_dev+$ventadetallesrefacion_dev->num_rows();

                if($totalitems_dev>0){
                    if($totalitems>0){
                        $mensaje='Devolución Parcial';
                    }else{
                        $mensaje='Devolución Total';
                        $this->ModeloCatalogos->updateCatalogo('ventacombinada',array('e_entrega'=>0,'activo'=>0,'motivo'=>'eliminacion por devolucion total'),array('combinadaId'=>$id));
                    }
                }
                if($mensaje!=''){
                    $mensaje='<span class="new badge orage badge_dev">'.$mensaje.'</span>';
                    $mensaje.='<a class="b-btn b-btn-primary" onclick="info_dev('.$id.','.$tipotabla.','.$id_rh.','.$renta_rh.')" title="detalle de la devolución" style="padding: 2px 6px;" ><i class="fas fa-file-alt"></i></a>';
                }
                $prefacturas[]=array('id'=>$id,'tipotabla'=>$tipotabla,'mensaje'=>$mensaje,'bodegas'=>implode(', ', array_unique($viewbodegas)));

            }
            /*
            $resultado=$this->ModeloCatalogos->getselectwheren('pagos_poliza',array('idpoliza'=>$idpoliza,'confirmado'=>0));
            if($resultado->num_rows()>0){
                $prefacturas[]=array('idpoliza'=>$idpoliza);
            }
            */
            
         
            
        }
        echo json_encode($prefacturas);
    }
    function verificardev_info(){
        $params = $this->input->post();
        $id = $params['id'];
        $tipotabla = $params['tipotabla'];
        $id_rh = $params['id_rh'];
        $renta_rh = $params['renta_rh'];
        $html='';
        $html.='<table class="table striped">';
        $html.='<thead>';
            $html.='<tr>';
                    $html.='<th>Cantidad</th>';
                    $html.='<th>Modelo</th>';
                    $html.='<th>Motivo de devolución</th>';
                    $html.='<th>Fecha de devolución</th>';
                $html.='</tr>';
        $html.='</thead>';
        $html.='<tbody>';

        if($tipotabla==1){
            $resultadoequipos_dev = $this->ModeloCatalogos->info_equipo_dev($id);
            $resultadoaccesorios_dev = $this->ModeloCatalogos->accesoriosventa_dev($id);
            $resultadoconsumibles_dev = $this->ModeloCatalogos->consumiblesventa_dev($id);
            $consumiblesventadetalles_dev = $this->ModeloCatalogos->consumiblesventadetalles_dev($id);
            $ventadetallesrefacion_dev = $this->ModeloCatalogos->ventas_has_detallesRefacciones_dev($id);
        }
        if($tipotabla==5){
            $rentavacessoriosh_dev = $this->ModeloCatalogos->rentavacessoriosh_dev($renta_rh,$id_rh);
            $rentavconsumiblesh_dev = $this->ModeloCatalogos->rentavconsumiblesh_dev($renta_rh,$id_rh);
            foreach ($rentavacessoriosh_dev->result() as $item){
                $html.='<tr>';
                    $html.='<td>'.$item->cantidad.'</td>';
                    $html.='<td>'.$item->nombre.'</td>';
                    $html.='<td>'.$item->motivo_dev.'</td>';
                    $html.='<td>'.$item->reg_dev.'</td>';
                $html.='</tr>';
            }
            foreach ($rentavconsumiblesh_dev->result() as $item){
                $html.='<tr>';
                    $html.='<td>'.$item->cantidad.'</td>';
                    $html.='<td>'.$item->modelo.'</td>';
                    $html.='<td>'.$item->motivo_dev.'</td>';
                    $html.='<td>'.$item->reg_dev.'</td>';
                $html.='</tr>';
            }
        }
        if($tipotabla==4){
            $viewbodegas=array();
            $mensaje='';
            $totalitems=0;$totalitems_dev=0;

            $where_ventas=array('combinadaId'=>$id);
            $resultadov = $this->ModeloCatalogos->db6_getselectwheren('ventacombinada',$where_ventas);
            $id_personal=0;
            foreach ($resultadov->result() as $item) {
                $id_personal=$item->id_personal;
                $idCliente=$item->idCliente;
                $equipo=$item->equipo;
                $consumibles=$item->consumibles;
                $refacciones=$item->refacciones;
                $poliza=$item->poliza;
                
            }
            
     
            $resultadoequipos_dev = $this->ModeloCatalogos->info_equipo_dev($equipo);
            $resultadoconsumibles_dev = $this->ModeloCatalogos->consumiblesventa_dev($equipo);
            $consumiblesventadetalles_dev = $this->ModeloCatalogos->consumiblesventadetalles_dev($consumibles);
            $ventadetallesrefacion_dev = $this->ModeloCatalogos->ventas_has_detallesRefacciones_dev($refacciones);
        }
        if($tipotabla==1 || $tipotabla==4){
            foreach ($resultadoequipos_dev->result() as $item){
                $html.='<tr>';
                    $html.='<td>'.$item->cantidad.'</td>';
                    $html.='<td>'.$item->modelo.'</td>';
                    $html.='<td>'.$item->motivo_dev.'</td>';
                    $html.='<td>'.$item->reg_dev.'</td>';
                $html.='</tr>';
            }
            foreach ($resultadoconsumibles_dev->result() as $item){
                $html.='<tr>';
                    $html.='<td>'.$item->cantidad.'</td>';
                    $html.='<td>'.$item->modelo.'</td>';
                    $html.='<td>'.$item->motivo_dev.'</td>';
                    $html.='<td>'.$item->reg_dev.'</td>';
                $html.='</tr>';
            }
            foreach ($consumiblesventadetalles_dev->result() as $item){
                $html.='<tr>';
                    $html.='<td>'.$item->cantidad.'</td>';
                    $html.='<td>'.$item->modelo.'</td>';
                    $html.='<td>'.$item->motivo_dev.'</td>';
                    $html.='<td>'.$item->reg_dev.'</td>';
                $html.='</tr>';
            }
            foreach ($ventadetallesrefacion_dev->result() as $item){
                $html.='<tr>';
                    $html.='<td>'.$item->cantidad.'</td>';
                    $html.='<td>'.$item->modelo.'</td>';
                    $html.='<td>'.$item->motivo_dev.'</td>';
                    $html.='<td>'.$item->reg_dev.'</td>';
                $html.='</tr>';
            }
        }
        $html.='</tbody>';
        $html.='</table>';
        echo $html;
    }
    function vericarprerentas(){
        $datos = $this->input->post('datos');
        //log_message('error', $datos);
        $DATA = json_decode($datos);
        $prefacturas=array();
        for ($i=0;$i<count($DATA);$i++) {
            $tipo         = $DATA[$i]->tipo;
            $row  = $DATA[$i]->row;
            $historialid=0;
            $rentas=0;
            $prefacturaId=0;
            if($tipo==1){//rentas_has_detallesequipos_consumible_dev
                $resulteq=$this->ModeloCatalogos->getselectwheren('rentas_historial_consumibles',array('consumible'=>$row));
                foreach ($resulteq->result() as $item) {
                    $historialid=$item->historialid;
                }
                $resulteqr=$this->ModeloCatalogos->getselectwheren('rentas_historial',array('id'=>$historialid));
                foreach ($resulteqr->result() as $item) {
                    $rentas=$item->rentas;
                }
                if($rentas==0){
                    $resulteqr=$this->ModeloCatalogos->getselectwheren('rentas_has_detallesequipos_consumible_dev',array('id'=>$row));
                    foreach ($resulteqr->result() as $item) {
                        $rentas=$item->idrentas;
                    }
                }
                
                
                if($historialid==0){
                    $resulteqrp=$this->ModeloCatalogos->getselectwheren('prefactura',array('tipo'=>4,'ventaId'=>$rentas));
                    foreach ($resulteqrp->result() as $item) {
                        $prefacturaId=$item->prefacturaId;
                    }
                }else{
                    $resulteqrp=$this->ModeloCatalogos->getselectwheren('prefactura',array('tipo'=>4,'ventaId'=>$historialid));
                    foreach ($resulteqrp->result() as $item) {
                        $prefacturaId=$item->prefacturaId;
                    }
                }
            }
            if($tipo==2){//rentas_has_detallesEquipos_accesorios_dev
                $resulteq=$this->ModeloCatalogos->getselectwheren('rentas_historial_accesorios',array('id_accesoriod'=>$row));
                foreach ($resulteq->result() as $item) {
                    $historialid=$item->historialid;
                }
                $resulteqr=$this->ModeloCatalogos->getselectwheren('rentas_historial',array('id'=>$historialid));
                foreach ($resulteqr->result() as $item) {
                    $rentas=$item->rentas;
                }
                if($rentas==0){
                    $resulteqr=$this->ModeloCatalogos->getselectwheren('rentas_has_detallesequipos_accesorios_dev',array('id_accesoriod'=>$row));
                    foreach ($resulteqr->result() as $item) {
                        $rentas=$item->idrentas;
                    }
                }
                if($historialid==0){
                    $resulteqrp=$this->ModeloCatalogos->getselectwheren('prefactura',array('tipo'=>2,'ventaId'=>$rentas));
                    foreach ($resulteqrp->result() as $item) {
                        $prefacturaId=$item->prefacturaId;
                    }
                }else{
                    $resulteqrp=$this->ModeloCatalogos->getselectwheren('prefactura',array('tipo'=>4,'ventaId'=>$historialid));
                    foreach ($resulteqrp->result() as $item) {
                        $prefacturaId=$item->prefacturaId;
                    }
                }


            }  
            $prefacturas[]=array('tipo'=>$tipo,'row'=>$row,'rentas'=>$rentas,'historialid'=>$historialid,'prefacturaId'=>$prefacturaId);        
            
        }
        echo json_encode($prefacturas);
    }
    function viewcondiciones(){
        $id = $this->input->post('id');

        $result=$this->ModeloCatalogos->getselectwheren('rentas_condiciones',array('id'=>$id));
        $resulteq=$this->ModeloCatalogos->getselectwheren('rentas_condiciones_detalle',array('idc'=>$id));



        $datos = array(
                    'datos'=>$result->row(),
                    'datosd'=>$resulteq->result()
                );
        echo json_encode($datos);
    }
    function agregarcomentario(){
        $params = $this->input->post();
        $id = $params['id'];
        $tipo = $params['tipo'];
        $com = $params['com'];
        if($tipo==0){
             $this->ModeloCatalogos->updateCatalogo('ventas',array('comentario'=>$com),array('id'=>$id));
        }
        if($tipo==1){
             $this->ModeloCatalogos->updateCatalogo('ventacombinada',array('comentario'=>$com),array('combinadaId'=>$id));
        }
        if($tipo==5){
             $this->ModeloCatalogos->updateCatalogo('ventasd',array('comentario'=>$com),array('id'=>$id));
        }
    }
    function deleteprefactura(){
        $pass = $this->input->post('pass');
        $pre = $this->input->post('pre');
        log_message('error','deleteprefactura pass:'.$pass);
        $permiso=0;
        $permisos=$this->Login_model->permisoadmin();
        $usuario='';
        foreach ($permisos as $item) {
            $permisotemp=0;

            $verificar = password_verify($pass,$item->contrasena);

            if ($verificar==false) {
                $verificar = password_verify($pass,$item->clavestemp);
                if ($verificar) {
                    $permisotemp=1;
                    $this->ModeloCatalogos->updateCatalogo('usuarios',array('clavestemp'=>null),array('UsuarioID'=>$item->UsuarioID));
                }
            }

            if ($verificar) {
                $permiso=1;
                $usuario.=$item->Usuario.' ';
            }
        }
        if ($permiso==1) {
            $result=$this->ModeloCatalogos->getselectwheren('alta_rentas_prefactura',array('prefId'=>$pre));
            $info='';
            foreach ($result->result() as $item) {
                $info='id_renta:'.$item->id_renta.' periodo:'.$item->periodo_inicial.'/'.$item->periodo_final;
            }

            $this->ModeloCatalogos->getdeletewheren('alta_rentas_prefactura_d',array('prefId'=>$pre));
            $this->ModeloCatalogos->getdeletewheren('alta_rentas_prefactura',array('prefId'=>$pre));
            $this->ModeloCatalogos->Insert('bitacora',array('contenido'=>'Se realizo una autorizacion de eliminacion de periodo usuario:'.$usuario.' '.$info,'nombretabla'=>'Permisos','idtable'=>0,'tipo'=>'Solicitud permiso','personalId'=>$this->idpersonal));
            
        }

        echo $permiso;
    }
    function editarprecios_vd(){
        $datas = $this->input->post();
        $tipot      = $datas['tipot'];
        $idrow      = $datas['idrow'];
        $newprecio  = $datas['newprecio'];

        if ($tipot==1) {
            $this->ModeloCatalogos->updateCatalogo('ventasd_has_detallesequipos',array('precio'=>$newprecio),array('id'=>$idrow));
            $this->ModeloCatalogos->Insert('bitacora',array('contenido'=>'Se realizo una edicion de precios de equipo','nombretabla'=>'ventasd_has_detallesequipos','idtable'=>$idrow,'tipo'=>'edicion','personalId'=>$this->idpersonal));
        }
        if ($tipot==2) {
            $this->ModeloCatalogos->updateCatalogo('ventasd_has_detallesconsumibles',array('precioGeneral'=>$newprecio),array('id'=>$idrow));
            $this->ModeloCatalogos->Insert('bitacora',array('contenido'=>'Se realizo una edicion de precios de consumibles','nombretabla'=>'ventasd_has_detallesconsumibles','idtable'=>$idrow,'tipo'=>'edicion','personalId'=>$this->idpersonal));
        }
        if ($tipot==3) {
            $this->ModeloCatalogos->updateCatalogo('ventasd_has_detallesrefacciones',array('precioGeneral'=>$newprecio),array('id'=>$idrow));
            $this->ModeloCatalogos->Insert('bitacora',array('contenido'=>'Se realizo una edicion de precios de Refacciones','nombretabla'=>'ventasd_has_detallesrefacciones','idtable'=>$idrow,'tipo'=>'edicion','personalId'=>$this->idpersonal));
        }
        if ($tipot==4) {
            $this->ModeloCatalogos->updateCatalogo('ventasd_has_detallesequipos_consumible',array('costo_toner'=>$newprecio),array('id'=>$idrow));
            $this->ModeloCatalogos->Insert('bitacora',array('contenido'=>'Se realizo una edicion de precios de consumibles','nombretabla'=>'ventasd_has_detallesequipos_consumible','idtable'=>$idrow,'tipo'=>'edicion','personalId'=>$this->idpersonal));
        }
        if ($tipot==5) {
            $this->ModeloCatalogos->updateCatalogo('ventasd_has_detallesequipos_accesorios',array('costo'=>$newprecio),array('id_accesoriod'=>$idrow));
            $this->ModeloCatalogos->Insert('bitacora',array('contenido'=>'Se realizo una edicion de precios de accesorios','nombretabla'=>'ventasd_has_detallesequipos_accesorios','idtable'=>$idrow,'tipo'=>'edicion','personalId'=>$this->idpersonal));
        }
    }
    function editardatosfiscales(){
        $datas = $this->input->post();
        $prefacturaId=$datas['prefacturaId'];
        $rfc=$datas['rfc'];
        $metodopagoId=$datas['metodopagoId'];
        $formapagoId=$datas['formapagoId'];
        $usocfdiId=$datas['usocfdiId'];
        $this->ModeloCatalogos->updateCatalogo('prefactura',array('rfc_id'=>$rfc,'metodopagoId'=>$metodopagoId,'formapagoId'=>$formapagoId,'usocfdiId'=>$usocfdiId),array('prefacturaId'=>$prefacturaId));
    }
    function cambiopersonal(){
        $params  = $this->input->post();
        $id     = $params['id'];
        $tipo   = $params['tipo'];
        $eje    = $params['eje'];
        $pass   = $params['pass'];
        log_message('error','cambiopersonal pass:'.$pass);
        $permiso=0;
        $permisos = $this->Login_model->permisoadminarray(array('1','17','18'));//1admin 17 julio 18 diana

        foreach ($permisos as $item) {
            $permisotemp=0;

            $usuario=$item->Usuario;

            $verificar = password_verify($pass,$item->contrasena);

            if ($verificar==false) {
                $verificar = password_verify($pass,$item->clavestemp);
                if ($verificar) {
                    $permisotemp=1;
                }
            }
            if($tipo==5){
                $verificar=true;
                $usuario=$this->usuario;
            }
            if ($verificar) {
                $permiso=1;
                if($tipo==0){
                    $this->ModeloCatalogos->updateCatalogo('ventas',array('id_personal'=>$eje),array('id'=>$id));  
                    $this->ModeloCatalogos->Insert('bitacora',array('contenido'=>'Se realizo una autorizacion de cambio de ejecutivo de creación de la venta, usuario:'.$usuario,'nombretabla'=>'Venta','idtable'=>$id,'tipo'=>'Solicitud permiso','personalId'=>$this->idpersonal)); 
                }
                if($tipo==1){
                    $this->ModeloCatalogos->updateCatalogo('ventacombinada',array('id_personal'=>$eje),array('combinadaId'=>$id));  
                    $this->ModeloCatalogos->Insert('bitacora',array('contenido'=>'Se realizo una autorizacion de cambio de ejecutivo de creación de la venta combinada, usuario:'.$usuario,'nombretabla'=>'ventacombinada','idtable'=>$id,'tipo'=>'Solicitud permiso','personalId'=>$this->idpersonal)); 
                }
                if($tipo==2){
                    $this->ModeloCatalogos->updateCatalogo('polizasCreadas',array('id_personal'=>$eje),array('id'=>$id));  
                    $this->ModeloCatalogos->Insert('bitacora',array('contenido'=>'Se realizo una autorizacion de cambio de ejecutivo de creación de la poliza, usuario:'.$usuario,'nombretabla'=>'polizasCreadas','idtable'=>$id,'tipo'=>'Solicitud permiso','personalId'=>$this->idpersonal)); 
                }
                if($tipo==3){
                    $this->ModeloCatalogos->updateCatalogo('contrato',array('personalId'=>$eje),array('idcontrato'=>$id));  
                    $this->ModeloCatalogos->Insert('bitacora',array('contenido'=>'Se realizo una autorizacion de cambio de ejecutivo de creación del Contrato, usuario:'.$usuario,'nombretabla'=>'polizasCreadas','idtable'=>$id,'tipo'=>'Solicitud permiso','personalId'=>$this->idpersonal)); 
                }
                if($tipo==4){
                    $this->ModeloCatalogos->updateCatalogo('f_facturas',array('usuario_id'=>$eje,'creada_sesionId'=>$eje),array('FacturasId'=>$id));  
                    $this->ModeloCatalogos->Insert('bitacora',array('contenido'=>'Se realizo una autorizacion de cambio de ejecutivo de creación de la Factura, usuario:'.$usuario,'nombretabla'=>'f_facturas','idtable'=>$id,'tipo'=>'Solicitud permiso','personalId'=>$this->idpersonal)); 
                }
                if($tipo==5){
                    $this->ModeloCatalogos->updateCatalogo('pagos_noidentificados',array('per_asignado'=>$eje),array('id'=>$id));  
                    $this->ModeloCatalogos->Insert('bitacora',array('contenido'=>'Se realizo una autorizacion de cambio de ejecutivo de asignacion de pago no identificado, usuario:'.$usuario,'nombretabla'=>'pagos_noidentificados','idtable'=>$id,'tipo'=>'Solicitud permiso','personalId'=>$this->idpersonal)); 
                }
                if($permisotemp==1){
                    $this->ModeloCatalogos->updateCatalogo('usuarios',array('clavestemp'=>null),array('UsuarioID'=>$item->UsuarioID));
                }

                
            }
        }
        echo $permiso;
    }
    public function lisitinerarioview(){
        $this->load->view('lisitinerarioview');
    }
    public function lisitinerario(){
       $data = $this->input->post();
       $inicio = date('Y-m-d',$data['start']);
       $fin = date('Y-m-d',$data['end']);
       $tecnico=$data['tecnicoid'];

       //$result = $this->ModeloAsignacion->getListcitas($inicio,$fin,$area);
       $result = $this->ModeloCatalogos->lisitinerario($inicio,$fin,$tecnico);
       echo json_encode($result->result());
    }
    function descartargarantia(){
        $params = $this->input->post();
        $tipo=$params['tipo'];
        $id=$params['id'];
        if($tipo==1){
            $this->ModeloCatalogos->updateCatalogo('ventas_has_detallesEquipos',array('garantia'=>0),array('id'=>$id));  
        }
        if($tipo==2){
            $this->ModeloCatalogos->updateCatalogo('ventas_has_detallesRefacciones',array('garantia'=>0),array('id'=>$id));  
        }
        if($tipo==3){
            $this->ModeloCatalogos->updateCatalogo('ventas_has_detallesEquipos_accesorios',array('garantia'=>0),array('id_accesoriod'=>$id));  
        }
    }
    function verificarestatuspersonal(){
        
        $personalv=$this->ModeloCatalogos->getselectwheren('personal',array('personalId'=>$this->idpersonal));
        foreach ($personalv->result() as $item){
          if(intval($item->estatus)===0){
            log_message('error','Se cerro la seccion por que el personal muestra como eliminado: '.$this->idpersonal);
            redirect('/Login/cerrar_sesion');
          }
        }
        if($this->idpersonal==77){
            log_message('error','Se cerro la seccion por que el personal muestra como eliminado: '.$this->idpersonal);
            redirect('/Login/cerrar_sesion');
        }
    }
    function generarsolicitudsess(){
        echo 'hola';
    }
    function registrarpni(){
        $params = $this->input->post();
        if(isset($params['pi_cliente'])){
            $pi_cliente = $params['pi_cliente'];
        }else{
            $pi_cliente = 0;
        }
        $dataspni['fecha']=$params['pi_fecha'];
        $dataspni['pago']=$params['pi_monto'];
        $dataspni['idmetodo']=$params['pi_idmetodo'];
        $dataspni['obser']=$params['pi_obser'];
        $dataspni['idpersonal_create']=$this->idpersonal;
        $dataspni['reg_create']=$this->fechahoy;
        $dataspni['clienteId']=$pi_cliente;
        $dataspni['per_asignado']=$params['pi_personal'];
        $this->ModeloCatalogos->Insert('pagos_noidentificados',$dataspni);
    }
    function load_pni(){
        $params = $this->input->post();
        $per=$params['per'];
        $result = $this->ModeloCatalogos->load_pni($per);
        $html='';
        foreach ($result->result() as $item) {
            if($item->comen!=''){
                $btn_comen='<a class="b-btn b-btn-primary comen_pni_info_'.$item->id.'"  onclick="comen_pni_info('.$item->id.')"><i class="fas fa-align-justify"></i></a>';
            }else{
                $btn_comen='';
            }
            if($this->idpersonal==1 || $this->idpersonal==58 || $this->idpersonal==18){
                $btn_cambio='<a class="b-btn b-btn-primary tooltipped" data-position="top" data-delay="50" data-tooltip="Cambiar personal" onclick="cambiar_per_pn('.$item->id.',5)"><i class="fas fa-user fa-fw"></i></a>';
            }else{
                $btn_cambio='';
            }

            $html.='<tr><td>'.$item->fecha.'</td><td>'.$item->empresa.'</td><td>'.$item->formapago_text.'</td><td>'.$item->pago.'</td><td>'.$item->personala.'</td><td>'.$item->obser.'</td><td>'.$btn_comen.'</td>
              <td><!--'.$item->nombre.' '.$item->apellido_paterno.'--> <a class="b-btn b-btn-danger delete_pni" onclick="delete_pni('.$item->id.')"><i class="fas fa-trash"></i></a>
              <a class="b-btn b-btn-primary info_pni" onclick="comen_pni('.$item->id.')"><i class="fas fa-comment"></i></a> '.$btn_cambio.'</td>
            </tr>';
        }
        echo $html;
    }
    function delete_pni(){
        $params = $this->input->post();
        $id = $params['id'];
        $pass = $params['pass'];
        log_message('error','delete_pni pass:'.$pass);
        //=========================================================================
          $permisos = $this->Login_model->permisoadminarray(array('1','17','18','58'));//1admin 17 julio 18 diana
          $verificap=0;
          foreach ($permisos as $item) {
            $permisotemp=0;

            $verificar = password_verify($pass,$item->contrasena);

            if ($verificar==false) {
                $verificar = password_verify($pass,$item->clavestemp);
                if ($verificar) {
                    $permisotemp=1;
                    $this->ModeloCatalogos->updateCatalogo('usuarios',array('clavestemp'=>null),array('UsuarioID'=>$item->UsuarioID));
                }
            }
            if ($verificar) {
                $verificap=1;
            }
          }
        //======================================================================
        $aux = 0;
          if($verificap == 1){
             $aux=1;
            $datad['activo']=0;
            $datad['idpersonal_delete']=$this->idpersonal;
            $datad['reg_delete']=$this->fechahoy;
            $this->ModeloCatalogos->updateCatalogo('pagos_noidentificados',$datad,array('id'=>$id));
          }
          echo $aux;
    }
    function comen_pni(){
        $params = $this->input->post();
        $id = $params['id'];
        $comen = $params['comen'];

        $datad['comen']=$comen;
        $this->ModeloCatalogos->updateCatalogo('pagos_noidentificados',$datad,array('id'=>$id));
    }
    function comentariopagos($id){
        $data['id']=$id;
        $this->load->view('tickes/comentariopagos',$data);
    }
    function comen_pni_info(){
        $params = $this->input->post();
        $id=$params['id'];
        $strq="SELECT * FROM pagos_noidentificados WHERE id='$id'";
        $query = $this->db->query($strq);
        $query = $query->row();
        echo $query->comen;
    }
    function cambioempresa(){
        $params  = $this->input->post();
        $id     = $params['id'];
        $tipo   = $params['tipo'];
        $emp    = $params['emp'];
        $pass   = $params['pass'];
        log_message('error','cambioempresa pass:'.$pass);
        $permiso=0;
        $permisos = $this->Login_model->permisoadminarray(array('1','9','17','18'));//1admin 9 conta 17 julio 18 diana

        foreach ($permisos as $item) {
            $permisotemp=0;

            $usuario=$item->Usuario;

            $verificar = password_verify($pass,$item->contrasena);

            if ($verificar==false) {
                $verificar = password_verify($pass,$item->clavestemp);
                if ($verificar) {
                    $permisotemp=1;
                }
            }
            if($tipo==5){
                $verificar=true;
                $usuario=$this->usuario;
            }
            if ($verificar) {
                $permiso=1;
                if($tipo==0){
                    $this->ModeloCatalogos->updateCatalogo('ventas',array('tipov'=>$emp),array('id'=>$id));  
                    $this->ModeloCatalogos->Insert('bitacora',array('contenido'=>'Se realizo una autorizacion de cambio de empresa de creación de la venta, usuario:'.$usuario,'nombretabla'=>'Venta','idtable'=>$id,'tipo'=>'Solicitud permiso','personalId'=>$this->idpersonal)); 
                }
                if($tipo==1){
                    $this->ModeloCatalogos->updateCatalogo('ventacombinada',array('tipov'=>$emp),array('combinadaId'=>$id));  
                    $this->ModeloCatalogos->Insert('bitacora',array('contenido'=>'Se realizo una autorizacion de cambio de empresa de creación de la venta combinada, usuario:'.$usuario,'nombretabla'=>'ventacombinada','idtable'=>$id,'tipo'=>'Solicitud permiso','personalId'=>$this->idpersonal)); 
                }
                if($tipo==2){
                    $this->ModeloCatalogos->updateCatalogo('polizasCreadas',array('tipov'=>$emp),array('id'=>$id));  
                    $this->ModeloCatalogos->Insert('bitacora',array('contenido'=>'Se realizo una autorizacion de cambio de empresa de creación de la poliza, usuario:'.$usuario,'nombretabla'=>'polizasCreadas','idtable'=>$id,'tipo'=>'Solicitud permiso','personalId'=>$this->idpersonal)); 
                }
                
                if($permisotemp==1){
                    $this->ModeloCatalogos->updateCatalogo('usuarios',array('clavestemp'=>null),array('UsuarioID'=>$item->UsuarioID));
                }

                
            }
        }
        echo $permiso;
    }
    function editarserie(){
        $params  = $this->input->post();
        $id     = $params['id'];
        $tipo   = $params['tipo'];
        $serie    = $params['serie'];
        $pass   = $params['pass'];
        log_message('error','editarserie pass:'.$pass);
        $permiso=0;
        $permisos = $this->Login_model->permisoadminarray(array('1','9','17','18'));//1admin 9 conta 17 julio 18 diana

        foreach ($permisos as $item) {
            $permisotemp=0;

            $usuario=$item->Usuario;

            $verificar = password_verify($pass,$item->contrasena);

            if ($verificar==false) {
                $verificar = password_verify($pass,$item->clavestemp);
                if ($verificar) {
                    $permisotemp=1;
                }
            }
            if($tipo==5){
                $verificar=true;
                $usuario=$this->usuario;
            }
            if ($verificar) {
                $permiso=1;
                //=============================================================  
                    if($tipo==0){
                        $this->ModeloCatalogos->getdeletewheren('polizasCreadas_has_detallesPoliza_series',array('iddetalle'=>$id));
                        $this->ModeloCatalogos->Insert('polizasCreadas_has_detallesPoliza_series',array('serie'=>$serie,'iddetalle'=>$id));
                        $this->ModeloCatalogos->updateCatalogo('polizasCreadas_has_detallesPoliza',array('serie'=>$serie),array('id'=>$id));

                        $get_result=$this->ModeloCatalogos->getselectwheren('polizasCreadas_has_detallesPoliza',array('id'=>$id));
                        foreach ($get_result->result() as $itemp) {
                            if($itemp->idvinculo>0){
                                $this->ModeloCatalogos->updateCatalogo('cotizaciones_has_detallesPoliza',array('serie'=>$serie),array('id'=>$itemp->idvinculo));
                            }
                            // code...
                        }
                        $this->ModeloCatalogos->updateCatalogo('asignacion_ser_poliza_a_e',array('serie'=>$serie),array('idequipo'=>$id));
                    }
                    if($tipo==1){
                        $this->ModeloCatalogos->updateCatalogo('ventas_has_detallesRefacciones',array('serie'=>$serie),array('id'=>$id)); 
                        $get_result=$this->ModeloCatalogos->getselectwheren('ventas_has_detallesRefacciones',array('id'=>$id));
                        foreach ($get_result->result() as $itemp) {
                            if($itemp->idvinculo>0){
                                $this->ModeloCatalogos->updateCatalogo('cotizaciones_has_detallesRefacciones',array('serieequipo'=>$serie),array('id'=>$itemp->idvinculo));
                            }
                            // code...
                        }
                    }
                //=============================================================                
                if($permisotemp==1){
                    $this->ModeloCatalogos->updateCatalogo('usuarios',array('clavestemp'=>null),array('UsuarioID'=>$item->UsuarioID));
                }

                
            }
        }
        echo $permiso;
    }
    function editarserie_cot(){
        $params  = $this->input->post();
        $id     = $params['id'];
        $tipo   = $params['tipo'];
        $serie    = $params['serie'];
        $pass   = $params['pass'];
        log_message('error','editarserie_cot pass:'.$pass);
        $permiso=0;
        $permisos = $this->Login_model->permisoadminarray(array('1','9','17','18'));//1admin 9 conta 17 julio 18 diana

        foreach ($permisos as $item) {
            $permisotemp=0;

            $usuario=$item->Usuario;

            $verificar = password_verify($pass,$item->contrasena);

            if ($verificar==false) {
                $verificar = password_verify($pass,$item->clavestemp);
                if ($verificar) {
                    $permisotemp=1;
                }
            }
            if($tipo==5){
                $verificar=true;
                $usuario=$this->usuario;
            }
            if ($verificar) {
                $permiso=1;
                //=============================================================  
                    if($tipo==0){
                        //$this->ModeloCatalogos->getdeletewheren('polizasCreadas_has_detallesPoliza_series',array('iddetalle'=>$id));
                        //$this->ModeloCatalogos->Insert('polizasCreadas_has_detallesPoliza_series',array('serie'=>$serie,'iddetalle'=>$id));
                        $this->ModeloCatalogos->updateCatalogo('cotizaciones_has_detallesPoliza',array('serie'=>$serie),array('id'=>$id));
                        $get_result=$this->ModeloCatalogos->getselectwheren('polizasCreadas_has_detallesPoliza',array('idvinculo'=>$id));
                        foreach ($get_result->result() as $item) {
                            $id_pold=$item->id;
                            $this->ModeloCatalogos->updateCatalogo('polizasCreadas_has_detallesPoliza',array('serie'=>$serie),array('id'=>$id_pold)); 
                            $this->ModeloCatalogos->getdeletewheren('polizasCreadas_has_detallesPoliza_series',array('iddetalle'=>$id_pold));
                            $this->ModeloCatalogos->Insert('polizasCreadas_has_detallesPoliza_series',array('iddetalle'=>$id_pold,'serie'=>$serie));
                            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_poliza_a_e',array('serie'=>$serie),array('idequipo'=>$id_pold));

                        }
                    }
                    if($tipo==1){
                        $this->ModeloCatalogos->updateCatalogo('cotizaciones_has_detallesRefacciones',array('serieequipo'=>$serie),array('id'=>$id)); 
                        $get_result=$this->ModeloCatalogos->getselectwheren('ventas_has_detallesRefacciones',array('idvinculo'=>$id));
                        foreach ($get_result->result() as $item) {
                            $id_refacc=$item->id;
                            $this->ModeloCatalogos->updateCatalogo('ventas_has_detallesRefacciones',array('serie'=>$serie),array('id'=>$id_refacc)); 
                        }
                    }
                //=============================================================                
                if($permisotemp==1){
                    $this->ModeloCatalogos->updateCatalogo('usuarios',array('clavestemp'=>null),array('UsuarioID'=>$item->UsuarioID));
                }

                
            }
        }
        echo $permiso;
    }
    function consultartimbres($fechainicio,$fechafin){
        //consultar timbres consumidos
        $strq1="SELECT COUNT(*) as total FROM `f_facturas` WHERE (Estado=1 or Estado=0) AND activo=1 AND fechatimbre BETWEEN '$fechainicio 00:00:00' AND '$fechafin 23:59:59'";
        $query1 = $this->db->query($strq1);
        $query1=$query1->row();
        echo 'Facturas timbradas: '.$query1->total.'<br>';

        $strq2="SELECT COUNT(*) as total FROM `f_facturas` WHERE Estado=0 AND activo=1 AND fechatimbre BETWEEN '$fechainicio 00:00:00' AND '$fechafin 23:59:59'";
        $query2 = $this->db->query($strq2);
        $query2=$query2->row();
        echo 'Facturas canceladas'.$query2->total.'<br>';

        

        $strq4="SELECT COUNT(*) as total FROM f_complementopago WHERE (Estado=1 or Estado=0) AND activo=1 AND fechatimbre BETWEEN '$fechainicio 00:00:00' AND '$fechafin 23:59:59'";
        $query4 = $this->db->query($strq4);
        $query4=$query4->row();
        echo 'Complementos Timbrados: '.$query4->total.'<br>';

        $strq3="SELECT COUNT(*) as total FROM f_complementopago WHERE Estado=0 AND activo=1 AND fechatimbre BETWEEN '$fechainicio 00:00:00' AND '$fechafin 23:59:59'";
        $query3 = $this->db->query($strq3);
        $query3=$query3->row();
        echo 'Complementos cancelados'.$query3->total.'<br><br>';
        $total=$query1->total+$query2->total+$query4->total+$query3->total;
        echo 'Total de timbres usados: '.$total;
    }
    function verificarvacaciones(){

        $result = $this->ModeloCatalogos->consultarvacainca($this->idpersonal,$this->fechah);
        $res=0;
        foreach ($result->result() as $item) {
            $res=1;
            if($item->fechapermitida==$this->fechah){
                $res=0;
            }
            // code...
        }
        echo $res;
    }
    function servicioaddcliente(){
        $params = $this->input->post();
        $cli = $params['cli'];
        $result_con = $this->ModeloCatalogos->get_obtener_ser($cli,$this->fechah,1);
        $result_pol = $this->ModeloCatalogos->get_obtener_ser($cli,$this->fechah,2);
        $result_cli = $this->ModeloCatalogos->get_obtener_ser($cli,$this->fechah,3);
        $result_ven = $this->ModeloCatalogos->get_obtener_ser($cli,$this->fechah,4);
        $html='<table class="table striped">';
        foreach ($result_con->result() as $item_c) {
            $btn='<a class="btn-bmz blue " onclick="obternerdatosservicio('.$item_c->asignacionId.',1)" title="Siguiente"><i class="fas fa-info-circle"></i></a>';
            $html.='<tr class="series_hide_show '.$item_c->seriesall.'"><td>'.$item_c->asignacionId.'</td><td>'.$item_c->fecha.'</td><td>'.$btn.'</td>
                    <td><a class="btn-bmz blue seri_1_'.$item_c->asignacionId.'"  data-fecha="'.$item_c->fecha.'" onclick="selectservent('.$item_c->asignacionId.',1)" >Seleccionar</a></td></tr>';

        }
        foreach ($result_pol->result() as $item_p) {
            $btn='<a class="btn-bmz blue " onclick="obternerdatosservicio('.$item_p->asignacionId.',2)" title="Siguiente"><i class="fas fa-info-circle"></i></a>';
            $html.='<tr class="series_hide_show '.$item_p->seriesall.'"><td>'.$item_p->asignacionId.'</td><td>'.$item_p->fecha.'</td><td>'.$btn.'</td>
                    <td><a class="btn-bmz blue seri_2_'.$item_p->asignacionId.'"  data-fecha="'.$item_p->fecha.'" onclick="selectservent('.$item_p->asignacionId.',2)" >Seleccionar</a></td></tr>';

        }
        foreach ($result_cli->result() as $item_cl) {
            $btn='<a class="btn-bmz blue " onclick="obternerdatosservicio('.$item_cl->asignacionId.',3)" title="Siguiente"><i class="fas fa-info-circle"></i></a>';
            $html.='<tr class="series_hide_show '.$item_cl->seriesall.'"><td>'.$item_cl->asignacionId.'</td><td>'.$item_cl->fecha.'</td><td>'.$btn.'</td>
                    <td><a class="btn-bmz blue seri_3_'.$item_cl->asignacionId.'" data-fecha="'.$item_cl->fecha.'"  onclick="selectservent('.$item_cl->asignacionId.',3)" >Seleccionar</a></td></tr>';

        }
        foreach ($result_ven->result() as $item_ven) {
            $btn='<a class="btn-bmz blue " onclick="obternerdatosservicio('.$item_ven->asignacionId.',4)" title="Siguiente"><i class="fas fa-info-circle"></i></a>';
            $html.='<tr class="series_hide_showxx '.$item_ven->seriesall.'"><td>'.$item_ven->asignacionId.'</td><td>'.$item_ven->fecha.'</td><td>'.$btn.'</td>
                    <td><a class="btn-bmz blue seri_4_'.$item_ven->asignacionId.'" data-fecha="'.$item_ven->fecha.'"  onclick="selectservent('.$item_ven->asignacionId.',4)" >Seleccionar</a></td></tr>';

        }
        $html.='</table>';
        echo $html;
    }
    function verifircar_dir_con(){
        $datos = $this->input->post('datos');
        $DATA = json_decode($datos);
        $direccion_array=array();
        for ($i=0;$i<count($DATA);$i++) {
            $equipo= $DATA[$i]->equipo;
            $serieid= $DATA[$i]->serieid;
            $direccion=$this->ModeloCatalogos->getdireccionequipos($equipo,$serieid);
            $direccion_array[]=array(
                        'equipo'=>$equipo,
                        'serieid'=>$serieid,
                        'dir'=>$direccion
                    );   
        }
        echo json_encode($direccion_array);
    }
    function obtenerventaspendientes(){
        $params = $this->input->post();
        $cli = $params['cli'];
        $result=$this->ModeloCatalogos->getselectwheren('clientes',array('id'=>$cli));
        $pendientes='';
        foreach ($result->result() as $item) {
            $pendientes=$item->bloqueo_fp_info;
        }
        echo $pendientes;
    }
    function cerrarperiodo(){
        $params = $this->input->post();
        $periodoid = $params['periodoid'];

        $this->ModeloCatalogos->updateCatalogo('alta_rentas_prefactura',array('descartar_not'=>1),array('prefId'=>$periodoid));
    }
    function cambiocliente(){
        $params = $this->input->post();
        $pass=$params['pass'];
        $id = $params['id'];
        $tipo = $params['tipo'];
        $cli = $params['cli'];
        log_message('error',"cambiocliente pass: '$pass'");
        $permisos = $this->Login_model->permisogeneral($pass);
        if($permisos==1){
            //===========================
                if($tipo==0){
                    $tipot_pre=0;
                    $this->ModeloCatalogos->updateCatalogo('ventas',array('idCliente'=>$cli),array('id'=>$id));
                    $this->ModeloCatalogos->Insert('bitacora',array('contenido'=>'Se realizo un cambio de cliente','nombretabla'=>'ventas','idtable'=>$id,'tipo'=>'edicion','personalId'=>$this->idpersonal));
                }
                if($tipo==1){
                    $tipot_pre=3;
                    $this->ModeloCatalogos->updateCatalogo('ventacombinada',array('idCliente'=>$cli),array('combinadaId'=>$id));

                    $result_rows=$this->ModeloCatalogos->getselectwheren('ventacombinada',array('combinadaId'=>$id));
                    foreach ($result_rows->result() as $item) {
                        if($item->equipo>0){
                            $this->ModeloCatalogos->updateCatalogo('ventas',array('idCliente'=>$cli),array('id'=>$item->equipo,'combinada'=>1));
                        }
                        if($item->poliza>0){
                            $this->ModeloCatalogos->updateCatalogo('polizasCreadas',array('idCliente'=>$cli),array('id'=>$item->poliza));
                        }
                    }

                    $this->ModeloCatalogos->Insert('bitacora',array('contenido'=>'Se realizo un cambio de cliente','nombretabla'=>'ventacombinada','idtable'=>$id,'tipo'=>'edicion','personalId'=>$this->idpersonal));
                }
                if($tipo==2){
                    $tipot_pre=1;
                    $this->ModeloCatalogos->updateCatalogo('polizasCreadas',array('idCliente'=>$cli),array('id'=>$id));
                    $this->ModeloCatalogos->Insert('bitacora',array('contenido'=>'Se realizo un cambio de cliente','nombretabla'=>'polizasCreadas','idtable'=>$id,'tipo'=>'edicion','personalId'=>$this->idpersonal));
                }
                if($tipo<3){
                    //==========================================
                        $result_rowcli=$this->ModeloCatalogos->getselectwheren('cliente_has_datos_fiscales',array('idCliente'=>$cli,'activo'=>1));
                        $id_dfis=0;$d_calle='';$d_numero='';$d_colonia='';$d_ciudad='';$d_estado='';
                        foreach ($result_rowcli->result() as $itemcli) {
                            $id_dfis=$itemcli->id;
                            $d_calle=$itemcli->calle;
                            $d_numero=$itemcli->num_ext;
                            $d_colonia=$itemcli->colonia;
                            $d_ciudad=$itemcli->municipio;
                            $d_estado=$itemcli->estado;
                        }
                    //==========================================
                    //==========================================
                        $result_rowclicont=$this->ModeloCatalogos->getselectwheren('cliente_datoscontacto',array('clienteId'=>$cli,'activo'=>1));
                        $id_clicont=0;
                        $puesto='';
                        foreach ($result_rowclicont->result() as $itemcont) {
                            $id_clicont=$itemcont->datosId;
                            $puesto=$itemcont->puesto;
                        }
                    //==========================================
                        $data_arrar_pre=array(
                                            'telId'=>$id_clicont,
                                            'contactoId'=>$id_clicont,
                                            'cargo'=>$puesto,
                                            'rfc_id'=>$id_dfis,
                                            'd_calle'=>$d_calle,
                                            'd_numero'=>$d_numero,
                                            'd_colonia'=>$d_colonia,
                                            'd_ciudad'=>$d_ciudad,
                                            'd_estado'=>$d_estado
                                            );
                    $this->ModeloCatalogos->updateCatalogo('prefactura',$data_arrar_pre,array('ventaId'=>$id,'tipo'=>$tipot_pre));    
                }

                if($tipo==3){
                    $this->ModeloCatalogos->updateCatalogo('rentas',array('idCliente'=>$cli),array('id'=>$id));

                    //==========================================
                        $result_rowcli=$this->ModeloCatalogos->getselectwheren('cliente_has_datos_fiscales',array('idCliente'=>$cli,'activo'=>1));
                        $id_dfis=0;
                        foreach ($result_rowcli->result() as $itemcli) {
                            $id_dfis=$itemcli->id;
                        }
                    //==========================================
                    $result_rows=$this->ModeloCatalogos->getselectwheren('contrato',array('idRenta'=>$id));

                    foreach ($result_rows->result() as $itemc) {
                        $idcontrato = $itemc->idcontrato;
                        
                        if($id_dfis>0){
                            $this->ModeloCatalogos->updateCatalogo('contrato',array('rfc'=>$id_dfis),array('idcontrato'=>$idcontrato));
                        }
                    }


                    $this->ModeloCatalogos->Insert('bitacora',array('contenido'=>'Se realizo un cambio de cliente en rentas','nombretabla'=>'rentas','idtable'=>$id,'tipo'=>'edicion','personalId'=>$this->idpersonal));
                }

            //===========================
        }

        echo $permisos;
    }
    function pintardireccionesclientes(){
        $idCliente = $this->input->post('clienteid');
        $resultadosdir=$this->ModeloCatalogos->getselectwheren('clientes_direccion',array('status'=>1,'idcliente'=>$idCliente));
        echo json_encode($resultadosdir->result());
    }
    function cambiarstatus(){
        $params  = $this->input->post();
        $pass   = $params['pass'];

        $permiso=0;
        $permisos = $this->Login_model->permisoadminarray(array('1','9','17','18'));//1admin 9 conta 17 julio 18 diana

        foreach ($permisos as $item) {
            $permisotemp=0;

            $usuario=$item->Usuario;

            $verificar = password_verify($pass,$item->contrasena);

            if ($verificar==false) {
                $verificar = password_verify($pass,$item->clavestemp);
                if ($verificar) {
                    $permisotemp=1;
                }
            }
            
            if ($verificar) {
                $permiso=1;
                //=======================================================
                        $this->ModeloCatalogos->updateCatalogo('ventas',array('prefactura'=>1),array('prefactura'=>0,'activo'=>1,'id_personal'=>$this->idpersonal,'combinada'=>0)); 
                    $this->ModeloCatalogos->updateCatalogo('ventacombinada',array('prefactura'=>1),array('prefactura'=>0,'activo'=>1,'id_personal'=>$this->idpersonal)); 
                $this->ModeloCatalogos->updateCatalogo('polizasCreadas',array('prefactura'=>1),array('prefactura'=>0,'activo'=>1,'id_personal'=>$this->idpersonal,'combinada'=>0));  
                    $this->ModeloCatalogos->Insert('bitacora',array('contenido'=>'Se realizo un cambio en el estatus de las prefacturas, usuario:'.$usuario,'nombretabla'=>'','idtable'=>0,'tipo'=>'Solicitud permiso','personalId'=>$this->idpersonal)); 
                //==================================================
                
                
                if($permisotemp==1){
                    $this->ModeloCatalogos->updateCatalogo('usuarios',array('clavestemp'=>null),array('UsuarioID'=>$item->UsuarioID));
                }

                
            }
        }
        echo $permiso;
    }
    function verificarpagosfomplemento($idc,$estado){
        //$resul_fac=$this->ModeloCatalogos->getselectwheren('f_complementopago_documento',array('complementoId'=>$idc));
        $resul_fac = $this->ModeloGeneral->complemento_fac($idc);
        foreach ($resul_fac->result() as $item_comd) {
            $facturasId=$item_comd->facturasId;
            $total_factura=$item_comd->total;
            //=========================================================================
                $resul_pre=$this->ModeloGeneral->fac_pre_facid($facturasId);
                foreach ($resul_pre->result() as $item_pre) {
                    $totaliva = $item_pre->totaliva;
                    $facId = $item_pre->facId;
                    $total_pagos=0;
                    //=======================================
                        $result = $this->ModeloCatalogos->pagos_factura($facId); 
                        foreach ($result as $item_pf) {
                            $total_pagos=$total_pagos+$item_pf->pago; 
                        }
                    //=======================================
                        $resultf = $this->ModeloCatalogos->facturasdelperiodo($facId); 
                        foreach ($resultf->result() as $itemf) {
                            $resultvcp_cp = $this->ModeloCatalogos->obteciondecomplememtofactura($itemf->facturaId);
                            foreach ($resultvcp_cp->result() as $itemcp) {
                                $total_pagos=$total_pagos+$itemcp->ImpPagado;
                            }
                        }
                    //=======================================
                    if($totaliva<=$total_pagos){
                        $this->ModeloCatalogos->updateCatalogo('factura_prefactura',array('statuspago'=>1),array('facId'=>$facId));
                    }else{
                        $this->ModeloCatalogos->updateCatalogo('factura_prefactura',array('statuspago'=>0),array('facId'=>$facId));
                    }
                    $this->ModeloCatalogos->Insert('bitacora',array('contenido'=>' Se actualizo el estatus de pago , persona que modifico:'.$this->usuario,'nombretabla'=>'factura_prefactura','idtable'=>$facId,'tipo'=>'cambio estatus','personalId'=>$this->idpersonal));
                }
            //=============================================================================
                $resul_ven=$this->ModeloCatalogos->getselectwheren('factura_venta',array('facturaId'=>$facturasId));
                foreach ($resul_ven->result() as $item_v) {
                    $combinada = $item_v->combinada;
                    $ventaId = $item_v->ventaId;
                    $total_pagos=0;

                    $resultfactura=$this->ModeloCatalogos->ontenerdatosfacturaventa($ventaId,$combinada);
                    //==============================================================
                        foreach($resultfactura->result() as $item){
                        $FacturasIdselected=$item->FacturasId;
                        $resultvcp = $this->ModeloCatalogos->obtencionventapolizafactura($item->FacturasId);

                        foreach ($resultvcp->result() as $itemvcp) {
                            if($itemvcp->combinada==0){

                                //=======================================
                                     $result = $this->ModeloCatalogos->pagos_ventas($itemvcp->ventaId); 
                                       foreach ($result as $item) { 
                                            $total_pagos=$total_pagos+$item->pago;
                                        } 
                                //=======================================
                            }elseif ($itemvcp->combinada==1) {
                                    
                                //========================================
                                    $result = $this->ModeloCatalogos->pagos_combinada($itemvcp->ventaId); 
                                    foreach ($result as $item) { 
                                        $total_pagos=$total_pagos+$item->pago; 
                                    } 
                                //========================================
                            }elseif ($itemvcp->combinada==2) {
                                    
                                //========================================
                                    $result = $this->ModeloCatalogos->pagos_poliza($itemvcp->ventaId);
                                    foreach ($result as $item) {
                                        $total_pagos=$total_pagos+$item->pago;
                                    }
                                //========================================
                            }
                        }
                        $resultvcp_cp = $this->ModeloCatalogos->obteciondecomplememtofactura($FacturasIdselected);
                        foreach ($resultvcp_cp->result() as $itemcp) {
                            $total_pagos=$total_pagos+$itemcp->ImpPagado;
                        }
                    }
                    //==============================================================
                    if($combinada==0){
                        if($total_factura<=$total_pagos){
                            $this->ModeloCatalogos->updateCatalogo('ventas',array('estatus'=>3),array('id'=>$ventaId));
                        }else{
                            $this->ModeloCatalogos->updateCatalogo('ventas',array('estatus'=>0),array('id'=>$ventaId));
                        }
                    }else{
                        if($total_factura<=$total_pagos){
                            $this->ModeloCatalogos->updateCatalogo('ventacombinada',array('estatus'=>3),array('combinadaId'=>$ventaId));
                        }else{
                            $this->ModeloCatalogos->updateCatalogo('ventacombinada',array('estatus'=>0),array('combinadaId'=>$ventaId));
                        }
                    }
                    $this->ModeloCatalogos->Insert('bitacora',array('contenido'=>' Se actualizo el estatus de pago id: '.$ventaId.' tipo:'.$combinada.', persona que modifico:'.$this->usuario,'nombretabla'=>'venta','idtable'=>$ventaId,'tipo'=>'cambio estatus','personalId'=>$this->idpersonal));
                    
                }
            //===================================================================
                $resul_pol=$this->ModeloCatalogos->getselectwheren('factura_poliza',array('facturaId'=>$facturasId));
                foreach ($resul_pol->result() as $item_p) {
                    $polizaId = $item_p->polizaId;
                    $total_pagos=0;
                    $resultfactura=$this->ModeloCatalogos->ontenerdatosfacturapoliza($polizaId);
                    //==============================================================
                        foreach($resultfactura->result() as $item){
                            $FacturasIdselected=$item->FacturasId;
                            $resultvcp = $this->ModeloCatalogos->obtencionventapolizafactura($item->FacturasId);

                            foreach ($resultvcp->result() as $itemvcp) {
                                if($itemvcp->combinada==0){

                                    //=======================================
                                         $result = $this->ModeloCatalogos->pagos_ventas($itemvcp->ventaId); 
                                           foreach ($result as $item) { 
                                                $total_pagos=$total_pagos+$item->pago;
                                            } 
                                    //=======================================
                                }elseif ($itemvcp->combinada==1) {
                                        
                                    //========================================
                                        $result = $this->ModeloCatalogos->pagos_combinada($itemvcp->ventaId); 
                                        foreach ($result as $item) { 
                                            $total_pagos=$total_pagos+$item->pago; 
                                        } 
                                    //========================================
                                }elseif ($itemvcp->combinada==2) {
                                        
                                    //========================================
                                        $result = $this->ModeloCatalogos->pagos_poliza($itemvcp->ventaId);
                                        foreach ($result as $item) {
                                            $total_pagos=$total_pagos+$item->pago;
                                        }
                                    //========================================
                                }
                            }
                            $resultvcp_cp = $this->ModeloCatalogos->obteciondecomplememtofactura($FacturasIdselected);
                            foreach ($resultvcp_cp->result() as $itemcp) {
                                $total_pagos=$total_pagos+$itemcp->ImpPagado;
                            }
                        }
                    //==============================================================
                    if($total_factura<=$total_pagos){
                        $this->ModeloCatalogos->updateCatalogo('polizasCreadas',array('estatus'=>3),array('id'=>$polizaId));
                    }else{
                        $this->ModeloCatalogos->updateCatalogo('polizasCreadas',array('estatus'=>0),array('id'=>$polizaId));
                    }
                    $this->ModeloCatalogos->Insert('bitacora',array('contenido'=>' Se actualizo el estatus de pago de poliza, persona que modifico:'.$this->usuario,'nombretabla'=>'polizasCreadas','idtable'=>$polizaId,'tipo'=>'cambio estatus','personalId'=>$this->idpersonal));
                }
            //============================================================================

            
        }
    }
    function deletepro(){
        $params = $this->input->post();
        $id = $params['id'];
        $tipo = $params['tipo'];
        if($tipo==1){
            $this->ModeloCatalogos->getdeletewheren('ventas_has_detallesEquipos',array('id'=>$id,'serie_estatus'=>0));
        }
        if($tipo==3){
            $this->ModeloCatalogos->getdeletewheren('ventas_has_detallesRefacciones',array('id'=>$id,'serie_estatus'=>0));
        }
        if($tipo==5){
            $this->ModeloCatalogos->getdeletewheren('ventas_has_detallesEquipos_accesorios',array('id_accesoriod'=>$id,'serie_estatus'=>0));
        }
    }
    function bodegarefacciones(){
        $params = $this->input->post();
        $id=$params['id'];
        $bodegai=$params['bodegai']; 
        $strq="SELECT bodegaId,bodega,tipov,stock_cs,stock_ss,stock_cs+stock_ss as totalstock FROM(
                SELECT 
                    bod.bodegaId,bod.bodega,bod.tipov,
                    (SELECT COUNT(*) FROM series_refacciones as sref WHERE sref.refaccionid='$id' AND sref.activo=1 AND sref.con_serie=1 AND sref.status<2 AND sref.bodegaId=bod.bodegaId AND sref.resguardar_cant=0) as stock_cs,
                    IFNULL((SELECT SUM(sref.cantidad-sref.resguardar_cant) FROM series_refacciones as sref WHERE sref.refaccionid='$id' AND sref.activo=1 AND sref.con_serie=0 AND sref.bodegaId=bod.bodegaId),0) as stock_ss
                FROM bodegas as bod
                WHERE bod.activo=1
                    ) as datos";
        $query = $this->db->query($strq);
        $options='';
        foreach ($query->result() as $item) {
            if($bodegai==$item->bodegaId){
                $b_selected='selected';
                $b_info=$item->bodega;
                $stock=1;
            }else{
                $b_selected='';
                $b_info=$item->bodega.' ('.$item->totalstock.')';
            }
            $options.='<option value="'.$item->bodegaId.'" data-stock="'.$item->totalstock.'" '.$b_selected.'>'.$b_info.'</option>';
        }
        echo $options;
    }
    function bodegaaccesorios(){
        $params = $this->input->post();
        $id=$params['id'];
        $bodegai=$params['bodegai']; 
        $strq="SELECT bodegaId,bodega,tipov,stock_cs,stock_ss,stock_cs+stock_ss as totalstock FROM(
                SELECT 
                    bod.bodegaId,bod.bodega,bod.tipov,
                    (SELECT COUNT(*) FROM series_accesorios as sref WHERE sref.accesoriosid='$id' AND sref.activo=1 AND sref.con_serie=1 AND sref.status<2 AND sref.bodegaId=bod.bodegaId AND sref.resguardar_cant=0) as stock_cs,
                    IFNULL((SELECT SUM(sref.cantidad-sref.resguardar_cant) FROM series_accesorios as sref WHERE sref.accesoriosid='$id' AND sref.activo=1 AND sref.con_serie=0 AND sref.bodegaId=bod.bodegaId),0) as stock_ss
                FROM bodegas as bod
                WHERE bod.activo=1
                    ) as datos";
        $query = $this->db->query($strq);
        $options='';
        foreach ($query->result() as $item) {
            if($bodegai==$item->bodegaId){
                $b_selected='selected';
                $b_info=$item->bodega;
                $stock=1;
            }else{
                $b_selected='';
                $b_info=$item->bodega.' ('.$item->totalstock.')';
            }
            $options.='<option value="'.$item->bodegaId.'" data-stock="'.$item->totalstock.'" '.$b_selected.'>'.$b_info.'</option>';
        }
        echo $options;
    }
    function bodegaequipos(){
        $params = $this->input->post();
        $id=$params['id'];
        $bodegai=$params['bodegai']; 
        $strq="SELECT 
                bod.bodegaId,bod.bodega,bod.tipov,
                (SELECT COUNT(*) FROM series_productos as sref WHERE sref.productoid='1' AND sref.activo=1 AND sref.status<2 AND sref.bodegaId=bod.bodegaId AND sref.resguardar_cant=0) as totalstock
            FROM bodegas as bod
            WHERE bod.activo=1";
        $query = $this->db->query($strq);
        $options='';
        foreach ($query->result() as $item) {
            if($bodegai==$item->bodegaId){
                $b_selected='selected';
                $b_info=$item->bodega;
                $stock=1;
            }else{
                $b_selected='';
                $b_info=$item->bodega.' ('.$item->totalstock.')';
            }
            $options.='<option value="'.$item->bodegaId.'" data-stock="'.$item->totalstock.'" '.$b_selected.'>'.$b_info.'</option>';
        }
        echo $options;
    }
    function addvacas(){
        $params = $this->input->post();
        $fechaini = $params['fini']; 
        $fechafin = $params['ffin']; 

        // Establece las fechas de inicio y fin
        $fechaInicio = new DateTime($fechaini);
        $fechaFin = new DateTime($fechafin);

        // Añadir 1 día al final para incluir el último día
        $fechaFin->modify('+1 day');

        // Crear un intervalo de 1 día
        $intervalo = new DateInterval('P1D');

        // Crear el periodo entre las dos fechas
        $periodo = new DatePeriod($fechaInicio, $intervalo, $fechaFin);

        // Iterar sobre cada día en el periodo
        foreach ($periodo as $fecha) {
            $fechaadd=$fecha->format('Y-m-d');
            $this->ModeloCatalogos->Insert('asistencias_vacaciones',array('idpersonal'=>$this->idpersonal,'fechainicio'=>$fechaadd,'fechafin'=>$fechaadd,'tipo'=>3,'codigog'=>$this->fechacod));
        }
    }
    function view_sol_vacas(){
        $strq="SELECT * FROM `asistencias_vacaciones` WHERE activo=1 AND idpersonal=$this->idpersonal AND codigog>0 GROUP BY codigog  ORDER BY `reg` DESC LIMIT 2";
        $query = $this->db->query($strq);
        $html='';
        foreach ($query->result() as $item) {
            $html.='<table class="table bordered striped">';
                $html.='<thead>';
                    $html.='<tr>';
                        $html.='<th>Fecha de solicitud</th>';
                        $html.='<th>Días de vacaciones</th>';
                        $html.='<th>Autorización</th>';
                        $html.='<th></th>';
                    $html.='</tr>';
                $html.='</thead>';
                $html.='<tbody>';
                $result = $this->ModeloCatalogos->getselectwheren('asistencias_vacaciones',array('activo'=>1,'idpersonal'=>$this->idpersonal,'codigog'=>$item->codigog));
                foreach ($result->result() as $item1) {
                    $btn_status='';
                    $btn_delete='<a class="b-btn b-btn-danger" onclick="deletesolvaca('.$item1->id.')" style="margin-top: 20px;"><i class="fas fa-trash"></i></a>';
                    if($item1->aceptada_rechzada=='0'){
                        $btn_status='<div class="des con">Rechazada</div>';
                        $btn_delete='';
                    }
                    if($item1->aceptada_rechzada=='1'){
                        $btn_status='<div class="des sin">Autorizada</div>';
                        $btn_delete='';
                    }
                    $html.='<tr>';
                        $html.='<td>'.$item1->reg.'</td>';
                        $html.='<td>'.$item1->fechainicio.'</td>';
                        $html.='<td>'.$btn_status.'</td>';
                        $html.='<td>'.$btn_delete.'</td>';
                    $html.='</tr>';
                }
                $html.='</tbody>';
                    $html.='<tr>';
                        $html.='<td></td>';
                        $html.='<td>Total de dias: <b>'.$result->num_rows().'</b></td>';
                        $html.='<td></td>';
                        $html.='<td></td>';
                    $html.='</tr>';
                $html.='<tfoot>';

                $html.='</tfoot>';
            $html.='</table>';

        }
        echo $html;
    }
    function deletesolvaca(){
        $params = $this->input->post();
        $id= $params['id'];
        $this->ModeloCatalogos->updateCatalogo('asistencias_vacaciones',array('activo'=>0),array('id'=>$id));
    }
    function refusado(){
        $params = $this->input->post();
        $id = $params['id'];
        $tipo = $params['tipo']; 
        $usa = $params['usa']; 
        if($tipo==1){
            $this->ModeloCatalogos->updateCatalogo('asignacion_series_refacciones',array('usado'=>$usa),array('id_asig'=>$id));    
        }
        
    }
    function cambiarstatus_req_fac(){
        $params  = $this->input->post();
        $pass   = $params['pass'];
        $id =$params['id'];
        $tipo =$params['tipo'];
        $vtipo =$params['vtipo'];
        //log_message('error',json_encode($params));
        if($vtipo==1){
            $permiso = $this->Login_model->permisogeneral($pass);
        }else{
            $permiso = 1;
        }
        if($vtipo==0){
            $vtipo_name=' no Requiere factura';
        }else{
            $vtipo_name=' Requiere Factura';
        }

        if($permiso){
            if($tipo==0){
                $nombretabla='ventas';
                $this->ModeloCatalogos->updateCatalogo($nombretabla,array('requ_fac'=>$vtipo),array('id'=>$id)); 
            }
            if($tipo==1){
                $nombretabla='ventacombinada';
                $this->ModeloCatalogos->updateCatalogo($nombretabla,array('requ_fac'=>$vtipo),array('combinadaId'=>$id)); 
            }
            if($tipo==3){
                $nombretabla='polizasCreadas';
                $this->ModeloCatalogos->updateCatalogo($nombretabla,array('requ_fac'=>$vtipo),array('id'=>$id)); 
            }
            $this->ModeloCatalogos->Insert('bitacora',array('contenido'=>'Se cambio a'.$vtipo_name.' la prefactura','nombretabla'=>$nombretabla,'idtable'=>$id,'tipo'=>'edicion','personalId'=>$this->perfilid));
        }
        echo $permiso;
    }
    function save_token_per(){
        $params = $this->input->post();
        $id=$params['id'];
        $token=$params['token'];
        $this->ModeloCatalogos->updateCatalogo('personal',array('token_user'=>$token),array('personalId'=>$id));
    }
    function verificarseriederentas(){
        $params = $this->input->post();
        $ser = $params['ser'];
        $ser = rtrim($ser); // Elimina espacios al final
        $ser = trim($ser); // Elimina espacios al inicio
        $result = $this->ModeloGeneral->get_verificarseriederentas($ser);

        echo json_encode($result->result());
    }
}
?>
