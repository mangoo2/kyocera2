<style type="text/css">
  @media only screen and (max-width: 450px){
    #modalinfocalendario.modal{width: 95%;margin: 0px;}
  }
  .datosclienteservicio .gar{font-size: 15px;position: absolute;margin-left: 15px;}
</style>
<div id="modalinfocalendario" class="modal"  >
    <div class="modal-content" style="padding-top: 0px; padding-left: 10px; padding-right: 10px;">
      <h5 >Información</h5>
      <br>
      <div class="row">
        <div class="col s12 m6 6" style="padding: 0 5px;">
          <div class="row rowmarginbotom">
              <div class="col s4 m5 6 textcolorred">Cliente:</div>
              <div class="col s8 m7 6 datoscliente"></div>
          </div>
          <div class="row rowmarginbotom">
              <div class="col s4 m5 6 textcolorred">Contacto:</div>
              <div class="col s8 m7 6 datosclientecontacto"></div>
          </div>
          <div class="row rowmarginbotom">
              <div class="col s4 m5 6 textcolorred">Tipo de Venta:</div>
              <div class="col s8 m7 6 datosclientetipo"></div>
          </div>
          <div class="row rowmarginbotom">
              <div class="col s4 m5 6 textcolorred">Tipo de Servicio:</div>
              <div class="col s8 m7 6 datosclienteservicio"></div>
          </div>
          <div class="row rowmarginbotom">
              <div class="col s4 m5 6 textcolorred">Zona:</div>
              <div class="col s8 m7 6 datosclientezona"></div>
          </div>
          <div class="row rowmarginbotom">
              <div class="col s4 m5 6 textcolorred">Ciudad:</div>
              <div class="col s8 m7 6">PUEBLA</div>
          </div>
          <div class="row rowmarginbotom">
              <div class="col s4 m5 6 textcolorred">Técnico:</div>
              <div class="col s8 m7 6 datosclientetecnico"></div>
          </div>
          <div class="row rowmarginbotom">
              <div class="col s4 m5 6 textcolorred">Prioridad:</div>
              <div class="col s8 m7 6 datosclienteprioridad"></div>
          </div>
          <div class="row rowmarginbotom equipoacceso"></div>
        </div>
        <div class="col s12 m6 6" style="padding: 0 5px;">
          <div class="row rowmarginbotom textcolorred datosclienteasignaciones">
            Asignaciones Mensuales / Asignaciones diarias
          </div>
          <div class="row rowmarginbotom datosclienteahorario"></div>
          <div class="row rowmarginbotom">
            <div class="col s12">
              <h4 class="datosclienteservicio"></h4>
            </div>
            <table class="table striped" id="tablerentapoliza">
              <thead>
                <tr>
                  <td>Modelo</td>
                  <td>Serie</td>
                  <td>Dirección</td>
                  <td class="infopro"></td>
                </tr>
              </thead>
              <tbody class="tablerentapoliza"></tbody>
            </table>
            <div class="col s12 t_dconsumibles"></div>
          </div>
        </div>
      </div>
      <!--- Motivo --->
      <div class="row motivo_texto">
        <div class="col s12 m6 62">
          <h5>Motivo del servicio</h5>
          <p>Motivo del Servicio: <span class="t_motivo_serv"></span></p>
          <p>Documentos y accesorios: <span class="t_doc_acces"></span></p>
        </div>
      </div> 
      <div class="row">
        <div class="col s12 m12 12">
          <div class="btn_confirmacion_servicio" style="text-align: end;"></div>
          <div class="btn_solicitud_consumibles" style="text-align: end;"></div>
        </div>
      </div>
      <span hidden="" class="id_asigna"></span>
      <span hidden="" class="id_tipo"></span>
      <div class="row">
        <div class="col s12 m12">
          <label>Comentario</label>
          <textarea class="form-control-bmz" id="comentariocal" style="height: 100px !important;" readonly></textarea>
        </div>
      </div>
      <div class="row">
        <div class="col m6">
          <div class="row">
            <div class="col m3">
              <!--<button type="button" class="waves-effect cyan btn-bmz modalconfimar" onclick="modalconfimar()">Confirmar</button>-->
            </div>
            <div class="col m3">
              <!--<button type="button" class="waves-effect cyan btn-bmz btn_reagendar" onclick="re_agendar()">Re Agendar</button>
              <button type="button" class="waves-effect btn-bmz btn_reagendar btn_reagendar_c" style="display: none" onclick="re_agendar_c()">Cancelar</button>-->
            </div>
            <div class="col m3 btn-clonar"></div>
            <div class="col m3 botondefinalizar"></div>  
          </div>
        </div>
        <div class="col m6">
          <span class="text_reagendar"></span>
        </div>
      </div>
      <div class="modal-footer">
        <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
      </div>
    </div>
</div>
<?php 
  if(isset($oserier)){

  }else{
    $oserier=0;
  }
?>
<script type="text/javascript">
function detallep(id){
  window.open(base_url+"PolizasCreadas/view/"+id, "Prefactura", "width=780, height=612");
}
function detallev(id,tipo){
  if (tipo==0) {
    window.open(base_url+"Prefactura/view/"+id, "Prefactura", "width=780, height=612");
  }else{
    window.open(base_url+"Prefactura/viewc/"+id, "Prefactura", "width=780, height=612");
  }
}
function obternerdatosservicio(asignacion,tipo){
    var fecha=$('#fechaactual').val();
    console.log(asignacion+'_'+tipo+'_'+fecha);
  $('#comentariocal').val('');
  $('.equipoacceso').html('');
  $('.infopro').html('');
  var idpersonal = $('#idpersonal').val();
    $.ajax({
        type:'POST',
        url: base_url+'index.php/calendario/obternerdatosservicio',
        data: {
          asignacion:asignacion,
          tipo:tipo,
          fecha:'',
          oserier:<?php echo $oserier;?>
        },
        success:function(data){
            console.log(data);
            var array = $.parseJSON(data);
            $('.datoscliente').html(array.cliente);
            $('.datosclientecontacto').html(array.contacto);
            $('#comentariocal').val(array.comentariocal);
            $('.equipoacceso').html(array.equipoacceso);
            //$('.botondefinalizar').html(array.botondefinalizar);
            if (tipo==1) {
                $('.datosclientetipo').html('Renta');
                $('.infopro').html('Producción<br>Promedio');
            }else if (tipo==2) {
                $('.datosclientetipo').html('Poliza');
            }else if (tipo==3) {
                $('.datosclientetipo').html('Evento');
            }else if (tipo==4) {
                $('.datosclientetipo').html('Venta');
            }
            if (array.tiposervicio==0) {
                $('.datosclienteasignaciones').html('Asignaciones Mensual');
            }else{
                $('.datosclienteasignaciones').html('Asignaciones diarias');
            }
            $('.datosclienteservicio').html(array.servicio);
            $('.datosclientezona').html(array.zona);
            $('.datosclientetecnico').html(array.tecnico);
            $('.datosclienteprioridad').html(array.prioridad);
            $('.datosclienteahorario').html(array.fecha+' '+array.hora+' '+array.horafin);
            $('.tablerentapoliza').html(array.equipos);
            $('.t_dconsumibles').html(array.dconsumibles);
            ///motivo
            if(array.m_tipo==3){
                $('.motivo_texto').css('display','block');
            }else{
                $('.motivo_texto').css('display','none');
            }
            $('.t_motivo_serv').html(array.m_servicio);
            $('.t_doc_acces').html(array.m_doc_acce);
            $('.id_asigna').html(asignacion);
            $('.id_tipo').html(tipo);
            if(array.confirmado==1){
                //if(idpersonal==13 || idpersonal==1){
                if(idpersonal==13){
                    $('.modalconfimar').removeClass('cyan').addClass('red');
                }else{
                    $('.modalconfimar').removeClass('cyan').addClass('red').prop( "disabled", true )
                }
            }else{
                $('.modalconfimar').removeClass('red').addClass('cyan').prop( "disabled", false );
            }
            if(tipo==1){//contratos
                const btn_c='<button type="button" class="waves-effect btn-bmz  red" onclick="envio_c('+asignacion+','+tipo+','+array.idcliente+')"><i class="fas fa-print"></i></button>';
                //$('.btn_solicitud_consumibles').html(btn_c);
                const btn_cls='<button type="button" class="btn-bmz  blue" onclick="clonarservicio('+asignacion+','+tipo+')" title="Clonar servicio"><i class="fas fa-clone"></i></button>';
                //$('.btn-clonar').html(btn_cls);
            }else{
                $('.btn_solicitud_consumibles').html('');
                $('.btn-clonar').html('');
            }
                const btn_s='<button type="button" class="waves-effect btn-bmz  red" onclick="envio_s_m('+asignacion+','+tipo+','+array.idcliente+')"><i class="fas fa-envelope-open-text"></i></button>';
               // $('.btn_confirmacion_servicio').html(btn_s);
        }
    });
    $('#modalinfocalendario').modal();
    $('#modalinfocalendario').modal('open');
}
</script>