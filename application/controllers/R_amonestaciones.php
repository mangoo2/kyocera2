<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class R_amonestaciones extends CI_Controller {
    public function __construct()     {
        parent::__construct();

        $this->load->model('ModeloCatalogos');
        $this->load->model('Modeloamonestacion');
        $this->load->helper('url');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->submenu=82;
        if($this->session->userdata('logeado')==true){
            $this->perfilid =$this->session->userdata('perfilid');
            $this->idpersonal = $this->session->userdata('idpersonal');
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,$this->submenu);// 52 es el id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
        }else{
            redirect('login'); 
        }
    }

    function index(){
        $data['MenusubId']=$this->submenu;
        $data['perfilid']=$this->perfilid;

        $data['personalrows']=$this->Configuraciones_model->view_session_searchTecnico(); 
            $this->load->view('header');
            $this->load->view('main');
            $this->load->view('amonestacion/list',$data);
            $this->load->view('footer');
            $this->load->view('amonestacion/listjs');
            $this->load->view('info_m_servisios'); //se comenta por conflicto con la funcion detalle()
    }
    public function getlistamo() {
        $params = $this->input->post();
        $getdata = $this->Modeloamonestacion->getlist_amonestacion($params);
        $totaldata= $this->Modeloamonestacion->getlist_amonestacion_t($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    function exportar($eje,$fecha){
        if($fecha==0){
            $fecha='';
        }
        //$params = $this->input->post();
        //$eje = $params['eje'];
        //$fecha = $params['fecha'];
        //http://localhost/kyocera2/index.php/Facturaslis/exportar/2022-02-01/2022-02-28
        $params['eje']=$eje;
        $params['fecha']=$fecha;
        $params['order'][0]['column']=1;
        $params['order'][0]['dir']='desc';
        $params['length']=500;
        $params['start']=0;
        $getdata = $this->Modeloamonestacion->getlist_amonestacion($params);
        //echo json_encode($getdata->result());

        

    
            header("Pragma: public");
            header("Expires: 0");
            $filename = "Reporte_amonestasion_".$this->fechahoy.".xls";
            header("Content-type: application/x-msdownload");
            header("Content-Disposition: attachment; filename=$filename");
            header("Pragma: no-cache");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");

        $html='';
        $html.='<table id="tabla_amo" class="responsive-table display" cellspacing="0">';
          $html.='<thead>';
            $html.='<tr>';
              $html.='<th>Fecha</th>';
              $html.='<th>Tecnico</th>';
              $html.='<th>Cliente</th>';
              $html.='<th>Servicio</th>';
              $html.='<th>Fecha y hora agendado</th>';
              $html.='<th>Fecha y hora atendido</th>';
              $html.='<th>Saltos</th>';
            $html.='</tr>';
          $html.='</thead>';
          $html.='<tbody>';
            foreach ($getdata->result() as $item) {
                $html.='<tr>';
                  $html.='<td>'.$item->fecha_reg.'</td>';
                  $html.='<td>'.$item->nombre.' '.$item->apellido_paterno.' '.$item->apellido_materno.'</td>';
                  $html.='<td>'.$item->sercli.'</td>';
                  $html.='<td>'.$item->sername.'</td>';
                  $html.='<td>'.$item->fecha_reg.' '.$item->hora.'</td>';
                  $html.='<td>'.$item->fecha_reg.' '.$item->hora_reg.'</td>';
                  $html.='<td>'.$item->serinfo.'</td>';
                $html.='</tr>';
            }
          $html.='</tbody>';
        $html.='</table>';
        echo $html;
    }
    function amones_servicios(){
        $params = $this->input->post();
        $eje=$params['eje'];
        $fecha=$params['fecha'];
        $w_eje='';$w_fecha='';
        if($eje>0){
            $w_eje=" and bit.personal='$eje' ";
        }
        if($fecha!=''){
            $w_fecha=" and bit.reg>='$fecha 00:00:00' and bit.reg<='$fecha 23:59:59' ";
        }

        $strq="SELECT bit.serv,bit.serve,bit.tipo,bit.comen,bit.comen_tipo,bit.personal,bit.reg,per.nombre,per.apellido_paterno,
        ren_eq.modelo,ser.serie,cli.empresa
            FROM bitacora_rechazo as bit
            INNER JOIN personal as per on per.personalId=bit.personal
            INNER JOIN asignacion_ser_contrato_a as asig on asig.asignacionId=bit.serv
            INNER JOIN contrato as con on con.idcontrato=asig.contratoId
            INNER JOIN rentas as ren on ren.id=con.idRenta
            INNER JOIN clientes as cli on cli.id=ren.idCliente
            LEFT JOIN asignacion_ser_contrato_a_e as asigd on asigd.asignacionIde=bit.serve
            LEFT JOIN rentas_has_detallesEquipos as ren_eq ON ren_eq.id=asigd.idequipo
            LEFT JOIN series_productos as ser on ser.serieId=asigd.serieId
            WHERE bit.nr_amonestacion>0 AND bit.tipo=1 $w_eje $w_fecha
            
            union
            
            SELECT bit.serv,bit.serve,bit.tipo,bit.comen,bit.comen_tipo,bit.personal,bit.reg,per.nombre,per.apellido_paterno,
                eq.modelo,asigd.serie,cli.empresa
            FROM bitacora_rechazo as bit
            INNER JOIN personal as per on per.personalId=bit.personal
            INNER JOIN asignacion_ser_poliza_a as asig on asig.asignacionId=bit.serv
            INNER JOIN polizasCreadas as pol on pol.id=asig.polizaId
            INNER JOIN clientes as cli on cli.id=pol.idCliente
            LEFT JOIN asignacion_ser_poliza_a_e as asigd on asigd.asignacionIde=bit.serve
            LEFT JOIN polizasCreadas_has_detallesPoliza as pold on pold.id=asigd.idequipo
            LEFT JOIN equipos as eq on eq.id=pold.idEquipo
            WHERE bit.nr_amonestacion>0 AND bit.tipo=2 $w_eje $w_fecha
            
            union
            
            SELECT bit.serv,bit.serve,bit.tipo,bit.comen,bit.comen_tipo,bit.personal,bit.reg,per.nombre,per.apellido_paterno,eq.modelo,asigd.serie,cli.empresa
            FROM bitacora_rechazo as bit
            INNER JOIN personal as per on per.personalId=bit.personal
            INNER JOIN asignacion_ser_cliente_a as asig on asig.asignacionId=bit.serv
            INNER JOIN clientes as cli ON cli.id=asig.clienteId
            LEFT JOIN asignacion_ser_cliente_a_d as asigd on asigd.asignacionIdd=bit.serve
            LEFT JOIN polizas_detalles as pold on pold.id=asigd.tservicio_a
            LEFT JOIN equipos as eq on eq.id=pold.modelo
            WHERE bit.nr_amonestacion>0 AND bit.tipo=3 $w_eje $w_fecha

            union

            SELECT bit.serv,bit.serve,bit.tipo,bit.comen,bit.comen_tipo,bit.personal,bit.reg,per.nombre,per.apellido_paterno,eq.modelo,asigd.serie,cli.empresa
            FROM bitacora_rechazo as bit
            INNER JOIN personal as per on per.personalId=bit.personal
            INNER JOIN asignacion_ser_venta_a as asig on asig.asignacionId=bit.serv
            LEFT JOIN asignacion_ser_venta_a_d as asigd on asigd.asignacionIdd=bit.serve
            LEFT JOIN polizas_detalles as pold on pold.id=asigd.tservicio_a
            LEFT JOIN equipos as eq on eq.id=pold.modelo
            INNER JOIN ventas as ven on ven.id=asig.ventaId
            INNER JOIN clientes as cli on cli.id=ven.idCliente
            WHERE  bit.nr_amonestacion>0 and bit.tipo=4 $w_eje $w_fecha ";
            $query = $this->db->query($strq);
            $html='';
            $html.='<table id="tabla_amo_rechazo" class="responsive-table display" cellspacing="0">';
                $html.='<thead>';
                  $html.='<tr>';
                    $html.='<th>Cliente</th>';
                    $html.='<th>Motivo</th>';
                    $html.='<th>Tipo</th>';
                    $html.='<th>Tecnico rechazo</th>';
                    $html.='<th>Equipo</th>';
                    $html.='<th>Serie</th>';
                    $html.='<th>Fecha</th>';
                    $html.='<th></th>';
                  $html.='</tr>';
                $html.='</thead>';
                $html.='<tbody>';
                    foreach ($query->result() as $item) {
                        $comentipo='';
                        if($item->comen_tipo==0){
                            $comentipo='Otro';
                        }
                        if($item->comen_tipo==1){
                            $comentipo='backup';
                        }
                        if($item->comen_tipo==2){
                            $comentipo='no se acudio al sitio';
                        }
                        if($item->comen_tipo==3){
                            $comentipo='sin acceso';
                        }
                        if($item->comen_tipo==4){
                            $comentipo='oficina cerrada';
                        }
                        if($item->comen_tipo==5){
                            $comentipo='intercambio de servicio';
                        }
                        $btn='<a class="vinc_c b-btn b-btn-primary btn-sm" onclick="obternerdatosservicio('.$item->serv.','.$item->tipo.')"><i class="fas fa-info-circle infoicon"></i></a>';
                        $html.='<tr>';
                            $html.='<td>'.$item->empresa.'</td>';
                            $html.='<td>'.$item->comen.'</td>';
                            $html.='<td>'.$comentipo.'</td>';
                            $html.='<td>'.$item->nombre.' '.$item->apellido_paterno.'</td>';
                            $html.='<td>'.$item->modelo.'</td>';
                            $html.='<td>'.$item->serie.'</td>';
                            $html.='<td>'.$item->reg.'</td>';
                            $html.='<td>'.$btn.'</td>';
                          $html.='</tr>';
                    }
                $html.='</tbody>';
            $html.='</table>';


            echo $html;
    }


    
}
?>
