<?php 
// Validamos si es creación o edición, para así cargar los datos respectivamente
if(isset($cliente)){
    $idCliente = $cliente->id;
    $empresa = $cliente->empresa;
  //  $persona_contacto = $cliente->persona_contacto;
    //$puesto_contacto = $cliente->puesto_contacto;
    $email = $cliente->email;
    $estado = $cliente->estado;
    $municipio = $cliente->municipio;
    $giro = $cliente->giro;
    $direccion = $cliente->direccion;
    $observaciones = $cliente->observaciones;
    $condicion = $cliente->condicion;
    $razon_social = $cliente->razon_social;
    $rfc = $cliente->rfc;
    $calle = $cliente->calle;
    $num_ext = $cliente->num_ext;
    $num_int = $cliente->num_int;
    $colonia = $cliente->colonia;
    $cp = $cliente->cp;
    //$atencionpara = $cliente->atencionpara;
    //$planta_sucursal = $cliente->planta_sucursal;
    $planta_sucursal = 0;
    //$planta_sucursal_dir = $cliente->planta_sucursal_dir;
    $fecha_registro= date("Y-m-d", strtotime($cliente->reg));
    $antiguedad = $cliente->antiguedad;
    $aaa = $cliente->aaa;
    $horario_disponible = $cliente->horario_disponible;
    $equipo_acceso = $cliente->equipo_acceso;
    $documentacion_acceso = $cliente->documentacion_acceso;
    $bloqueo = $cliente->bloqueo;
    $credito = $cliente->credito;
    $fm = $cliente->fisicamoral;
    $correo=$cliente->correo;
    $sol_orden_com =$cliente->sol_orden_com;
    $pass='xxxxx';
}else{
    $idCliente = 0;
    $empresa = '';
  //  $persona_contacto = '';
    //$puesto_contacto = '';
    $email = '';
    $estado = 0;
    $municipio = '';
    $giro = '';
    $direccion = '';
    $observaciones = '';
    $razon_social = '';
    $rfc = '';
    $calle = '';
    $num_ext = '';
    $num_int = '';
    $colonia = '';
    $cp = '';
    $planta_sucursal = 0;
    //$atencionpara = '';
    //$planta_sucursal_dir = 0; 
    $condicion ='';
    $fecha_registro=date('Y-m-d');
    $antiguedad = '';
    $aaa = '';
    $horario_disponible = '';
    $equipo_acceso = '';
    $documentacion_acceso = '';
    $bloqueo=0;
    $credito = 1;
    $fm=0;
    $correo='';
    $sol_orden_com=0;
    $pass='xxxxx';
} 
if($tipoVista==1){
  $label = 'Formulario de Alta de Cliente';
}else if($tipoVista==2){
  $label = 'Formulario de Edición de Cliente';
}else if($tipoVista==3){
  $label = 'Formulario de Visualización de Cliente';
}
?>
<style type="text/css">
  #modelocliente_chosen{
    margin: 0px;
  }
  .datosrequeridos{
    color: red;
  }
  .spannota{
    font-size: 11px;
    color: #9d9292;
  }
  .bloqueo{
    display: none;
  }
  #m_correo,#m_pass{
    width: 99%;
  }
  #modalinfopre{
    z-index: 1021 !important;
  }
  .infos_press td{
    padding: 5px 5px;
  }
</style>
<input id="fecha_vencimiento_info" type="hidden" value="<?php echo $fechavencimiento; ?>">
<input id="idCli" name="idCli" type="hidden" value="<?php echo $idCliente; ?>">
<input id="idpersonal" name="idpersonal" type="hidden" value="<?php echo $idpersonal; ?>">
<input type="password" placeholder="Contraseña"  style="position: absolute;background: transparent;z-index: -1;width: 10px;height: 10px;color: transparent !important;font-size: 1px;"/>
<!-- START CONTENT --> 
        <section id="content" width="100%">
          <!--start container-->
          <div class="container" width="100%">
            <div class="section">
              <div class="row">
                <div class="col s9"><h4 class="caption"><?php echo $label; ?></h4></div>
                <div class="col s3">
                  <?php 
                    if($idCliente>0){
                      if($perfilid==1){
                        if($idpersonal==1 || $idpersonal==17 || $idpersonal==9 || $idpersonal==18){
                          ?><!--<a class="btn-bmz red waves-effect waves-light right" id="establecerpass">Establecer Contraseña<i class="material-icons right">vpn_key</i></a>--><?php
                        }

                      }
                      ?>
                        <a class="b-btn b-btn-primary right" title="Agrega Acceso" onclick="addaccesoweb(<?php echo $idCliente;?>)"><i class="fas fa-unlock-alt"></i></a>
                      <?php
                    }
                  ?>
                  
                </div>
              </div>
               
              <div class="row">
                <input id="base_url" type="hidden" name="base_url" value="<?php echo base_url(); ?>">
                <input id="planta_sucursal_v" type="hidden" name="planta_sucursal_v" value="<?php echo $planta_sucursal; ?>">
                <input id="tipoVista" type="hidden" name="tipoVista" value="<?php echo $tipoVista; ?>">
                <!-- FORMULARIO -->
                  <div class="col s12 m12 24">
                    <div class="card-panel">
                      <div class="row">
                        <div class="input-field col s3">
                          <i class="material-icons prefix">date_range</i>
                          <!-- class="datepicker" -->
                          <input id="fecha_registro" name="fecha_registro" type="date" value="<?php echo $fecha_registro; ?>" disabled>
                          <label for="fecha_registro" class="active">Fecha de Registro</label>
                        </div>
                        <div class="col s1">
                          <div class="switch"><br>
                            <label>AAA
                              <input type="checkbox" name="aaa" id="aaa" <?php if($aaa==1) echo 'checked'?>>
                              <span class="lever"></span>
                            </label>
                          </div>
                        </div>
                        <div class="col s1 <?php if($idpersonal==1 or $idpersonal==17 or $idpersonal==18){ echo 'desbloqueo'; }else{ echo 'bloqueo'; } ?>" >
                            <div class="switch"><br>
                            <label>Bloqueo
                              <input type="checkbox" name="bloqueo" id="bloqueo" <?php if($bloqueo==1) echo 'checked'?>>
                              <span class="lever"></span>
                            </label>
                          </div>
                        </div>
                        <div class="col s2">
                          <div class="switch"><br>
                            <label>Solicitar Orden de compra
                              <input type="checkbox" name="sol_orden_com" id="sol_orden_com" <?php if($sol_orden_com==1) echo 'checked'?>>
                              <span class="lever"></span>
                            </label>
                          </div>
                        </div>
                      <div class="col s3">
                         <input type="radio" name="fisicamoral" id="fisicamoral_1" value="1" <?php if($fm==1){ echo 'checked';}?> onchange="fismo()">
                         <label for="fisicamoral_1">Física</label>
                         <input type="radio" name="fisicamoral" id="fisicamoral_2" value="2" <?php if($fm==2){ echo 'checked';}?> onchange="fismo()">
                         <label for="fisicamoral_2">Moral</label>
                      </div>
                      
                      <div class="row">
                         <form class="form" id="clientes_from" method="post">
                          <?php if(isset($cliente)){ ?>
                            <input id="idCliente" name="idCliente" type="hidden" value="<?php echo $idCliente; ?>">
                          <?php } ?>
                          <div class="row">
                            <div class="input-field col s9">
                              

                              <div class="b-input-group mb-3">
                                <input id="empresa" name="empresa" type="text" class="form-control-bmz" placeholder="Empresa" value="<?php echo $empresa; ?>" required onchange="verificarexis()">
                                <div class="b-input-group-append">
                                  <button class="b-btn b-btn-primary" type="button" onclick="verificarexis()">Verificar</button>
                                </div>
                              </div>
                            </div>
                            <div class="col s3">
                              <label>Credito</label>
                              <select class="browser-default form-control-bmz" name="credito" id="credito" onchange="creditofun()">
                                <option value="1" <?php if($credito==1){ echo 'selected';}?> class="credito_1">Sin crédito</option>
                                <option value="15" <?php if($credito==15){ echo 'selected';}?> class="credito_15">15 dias</option>
                                <option value="30" <?php if($credito==30){ echo 'selected';}?> class="credito_30">30 dias</option>
                                <option value="45" <?php if($credito==45){ echo 'selected';}?> class="credito_45">45 dias</option>
                                <option value="60" <?php if($credito==60){ echo 'selected';}?> class="credito_60">60 dias</option>
                              </select>
                            </div>
                            <div class="input-field col s6"></div>
                          </div>

                          <div class="row">
                            <div class="input-field col s3">
                              <div class="col s12 margenTopDivChosen" >
                                <i class="material-icons prefix">local_library</i>
                                <label for="estado" class="active">Estado</label>
                                <select id="estado" name="estado" class="browser-default chosen-select">
                                    <option></option>
                                    <?php foreach($estados as $estado_elegido){ ?>
                                      <option value="<?php echo $estado_elegido->EstadoId?>" <?php if($estado_elegido->EstadoId==$estado) echo "selected";   ?> ><?php echo $estado_elegido->Nombre; ?></option>
                                    <?php }?>
                                </select>
                              </div>   
                              <!---->
                            </div>

                            <div class="input-field col s3">
                              <i class="material-icons prefix">local_library</i>
                              <input id="municipio" name="municipio"  class="form-control-bmz" type="text" value="<?php echo $municipio; ?>">
                              <label for="municipio">Municipio</label>
                            </div>

                            <div class="input-field col s3">
                              <i class="material-icons prefix">local_offer</i>
                              <input id="giro" name="giro"  class="form-control-bmz" type="text" value="<?php echo $giro; ?>">
                              <label for="giro">Giro de la Empresa</label>
                            </div>
                            <div class="input-field col s3">
                              <i class="material-icons prefix">local_offer</i>
                              <input id="antiguedad" name="antiguedad" class="form-control-bmz" type="date" value="<?php echo $antiguedad; ?>">
                              <label for="antiguedad" class="active">Antiguedad</label>
                            </div>
                          </div>

                          <div class="row">
                            <div class="input-field col s7">
                              <i class="material-icons prefix">directions</i>
                              <input type="text" id="direcc" class="form-control-bmz">
                              <label for="direcc">Dirección de entrega/instalaciones</label>
                            </div>
                            <div class="input-field col s4">
                              <input type="text" id="spro_acc" class="form-control-bmz">
                              <label for="direcc">Protocolo de acceso</label>
                            </div>
                            <div class="col s1"><br>
                              <a class="btn-floating waves-effect waves-light cyan" onclick="add_address()"><i class="material-icons prefix">add</i></a>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col s12 tabla_direcc">
                                <table class="responsive-table display tabla_dir" cellspacing="0">
                                  <thead>
                                    <tr>
                                      <th>#</th>
                                      <th>Dirección</th>
                                      <th>Protocolo de acceso</th>
                                      <th style="width:14%">latitud</th>
                                      <th style="width:14%">longitud</th>
                                      <th style="width:10%">
                                        <a class="b-btn b-btn-primary" onclick="obtenercoor()" title="obtener coordenadas"><i class="fas fa-map-marker"></i></a>
                                      </th>
                                    </tr>
                                  </thead>
                                  <tbody class="tbody_direccion">
                                      <?php foreach ($cliente_dire as $item) {
                                            $btn_coordenadas='';
                                            if($item->longitud>0 || $item->latitud>0){
                                                $btn_coordenadas='<a class="b-btn b-btn-primary" onclick="view_coor('.$item->longitud.','.$item->latitud.')"><i class="fas fa-map-marked-alt"></i></a>';
                                            }
                                        ?>
                                        <tr>
                                          <td><input id="idclientedirecc" type="hidden"  value="<?php echo $item->idclientedirecc ?>" readonly></td>
                                          <td><input type="text" class="form-control-bmz" id="direccion" value="<?php echo $item->direccion ?>"></td>
                                          <td><input type="text" class="form-control-bmz" id="pro_acc" value="<?php echo $item->pro_acc ?>"></td>
                                          <td><input type="number" class="form-control-bmz" id="latitud" value="<?php echo $item->latitud ?>"></td>
                                          <td><input type="number" class="form-control-bmz" id="longitud" value="<?php echo $item->longitud ?>"></td>
                                          
                                          <td><?php echo $btn_coordenadas;?><a class="btn-floating red tooltipped" onclick="delete_direcc(<?php echo $item->idclientedirecc ?>)" data-position="top" data-delay="50" data-tooltip="Eliminar">
                                                        <i class="material-icons">delete_forever</i>
                                                      </a></td>
                                        </tr>
                                      <?php } ?>
                                  </tbody>
                                </table>
                            </div>
                          </div>
                          <div class="row">
                            <div class="input-field col s12">
                              <i class="material-icons prefix">question_answer</i>
                              <textarea id="observaciones" name="observaciones" class="materialize-textarea form-control-bmz"><?php echo $observaciones; ?></textarea>
                              <label for="message">Observaciones</label>
                            </div>
                          </div>
                          </form> 
                          <div class="row">
                            
                            <div class="input-field col s2">
                              <input id="atencionpara" name="atencionpara"  type="text" class="inputdatoscontacto form-control-bmz">
                              <label for="atencionpara">Atencion para:</label>
                            </div>
                            <div class="input-field col s2">
                              <input id="puesto_contacto" name="puesto_contacto" type="text" class="inputdatoscontacto form-control-bmz">
                              <label for="puesto_contacto">Puesto de contacto</label>
                            </div>
                            <div class="input-field col s2">
                              <input id="email" name="email"  type="email" class="inputdatoscontacto form-control-bmz" autocomplete="off">
                              <label for="email">Correo electrónico</label>
                            </div>
                            <div class="input-field col s2">
                              <input id="tel_local" name="tel_local"  type="text" class="inputdatoscontacto form-control-bmz">
                              <label for="tel_local">Telefono</label>
                            </div>
                            <div class="input-field col s2">
                              <input id="celular" name="celular"  type="text" class="inputdatoscontacto form-control-bmz">
                              <label for="celular">celular</label>
                            </div>
                            <div class="col s1">
                              <a class="waves-effect waves-light btn-bmz modal-trigger" onclick="agregar_pc()">Agregar</a>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col s12">
                              <table class="table table-striped table-condensed table-hover table-responsive" id="tabledatoscontacto">
                                <thead>
                                    <tr>
                                        <th>Atencion para</th>
                                        <th>Puesto</th>
                                        <th>Email</th>
                                        <th>Telefono</th>
                                        <th>Celular</th>
                                        <th>Direccion</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody class="tabledatoscontactotb"> 
                                  <?php foreach ($datoscontacto->result() as $item) { ?>
                                      <tr class="rowpc_<?php echo $item->datosId;?>_<?php echo $item->datosId;?>">
                                        <td>
                                          <input type="hidden" id="datosId" value="<?php echo $item->datosId;?>" readonly>
                                          <input type="hidden" id="atencionpara" value="<?php echo $item->atencionpara;?>" readonly>
                                          <?php echo $item->atencionpara;?>
                                        </td>
                                        <td>
                                          <input type="hidden" id="puesto" value="<?php echo $item->puesto;?>" readonly>
                                          <?php echo $item->puesto;?>
                                        </td>
                                        <td>
                                          <input type="hidden" id="email" value="<?php echo $item->email;?>" readonly>
                                          <?php echo $item->email;?>
                                        </td>
                                        <td><input type="hidden" id="telefono" value="<?php echo $item->telefono;?>" readonly>
                                          <?php echo $item->telefono;?>
                                        </td>
                                        <td><input type="hidden" id="celular" value="<?php echo $item->celular;?>" readonly>
                                          <?php echo $item->celular;?>
                                        </td>
                                        <td>
                                          <select class="browser-default form-control-bmz" id="dir_contac">
                                            <option value="0"></option>
                                            <?php foreach ($cliente_dire as $itemdir) {?>
                                              <option value="<?php echo $itemdir->idclientedirecc ?>" <?php if($itemdir->idclientedirecc==$item->dir){ echo 'selected';}?>><?php echo $itemdir->direccion ?></option>
                                            <?php } ?>
                                          </select>
                                        </td>
                                        <td>
                                          <a class="btn-floating red tooltipped" onclick="Eliminar_pc(<?php echo $item->datosId;?>,<?php echo $item->datosId;?>)" 
                                            data-position="top" data-delay="50" data-tooltip="Eliminar">
                                            <i class="material-icons">delete_forever</i>
                                          </a>
                                        </td>
                                    </tr>
                                  <?php } ?>
                                </tbody>
                              </table>
                            </div>
                            
                            

                          </div> 

                          
                          <!-- datosfiscales -->
                          
                          
                          <!-- fin datosfiscales -->
                          <div class="row">
                            <div class="col s12">
                                <h4 class="caption">Tabla de Datos Fiscales <a class="waves-effect waves-light btn-bmz modal-trigger"  onclick="agregar_d_f()">Agregar Datos Fiscales</a></h4>
                                <div class="row">   
                                   <div class="col s12">
                                        <table class="table table-striped table-condensed table-hover table-responsive border_datos" id="border_datos">
                                          <thead>
                                              <tr>
                                                  <th></th>
                                                  
                                              </tr>
                                          </thead>
                                          <tbody >
                                            <?php $datosfiscales = $this->model->getListadoDatosFiscales($idCliente); 
                                              foreach ($datosfiscales as $item) { ?>
                                                <tr>
                                                  <td><input readonly id="id_df" type="text" value="<?php echo $item->id ?>" style="display: none;" >
                                                    <div class="row">
                                                      <div class="col s6">
                                                        <label>Razón Social<span class="datosrequeridos">*</span></label>
                                                        <input id="razon_social" style="min-height: 50px;" onpaste="return false;" value="<?php echo str_replace(array('"'),array('&quot;'), $item->razon_social) ?>" onchange="validarrazonsocial()" class="class_readonly form-control-bmz" readonly>
                                                      </div>
                                                      <div class="col s2">
                                                        <label>R.F.C<span class="datosrequeridos">*</span></label>
                                                        <input id="rfc" type="text" value="<?php echo $item->rfc ?>" onchange="validarrfc(this)" <?php if($idpersonal==63 or $idpersonal==1){}else{ ?> onpaste="return false;" <?php } ?> class="class_readonly form-control-bmz" readonly>
                                                      </div>
                                                      <div class="col s2">
                                                        <label for="df_estado" class="active">Estado<span class="datosrequeridos">*</span></label>
                                                        
                                                        <select id="df_estado" class="browser-default form-control-bmz">
                                                            <option></option>
                                                            <option value="AGUASCALIENTES" <?php if('AGUASCALIENTES'==$item->estado) echo "selected";?> >AGUASCALIENTES</option>
                                                            <option value="BAJA CALIFORNIA" <?php if('BAJA CALIFORNIA'==$item->estado) echo "selected";?> >BAJA CALIFORNIA</option>
                                                            <option value="BAJA CALIFORNIA SUR" <?php if('BAJA CALIFORNIA SUR'==$item->estado) echo "selected";?> >BAJA CALIFORNIA SUR</option>
                                                            <option value="CAMPECHE" <?php if('CAMPECHE'==$item->estado) echo "selected";?> >CAMPECHE</option>
                                                            <option value="COAHUILA" <?php if('COAHUILA'==$item->estado) echo "selected";?> >COAHUILA</option>
                                                            <option value="COLIMA" <?php if('COLIMA'==$item->estado) echo "selected";?> >COLIMA</option>
                                                            <option value="CHIAPAS" <?php if('CHIAPAS'==$item->estado) echo "selected";?> >CHIAPAS</option>
                                                            <option value="CHIHUAHUA" <?php if('CHIHUAHUA'==$item->estado) echo "selected";?> >CHIHUAHUA</option>
                                                            <option value="CDMX" <?php if('CDMX'==$item->estado) echo "selected";?> >CDMX</option>
                                                            <option value="DURANGO" <?php if('DURANGO'==$item->estado) echo "selected";?> >DURANGO</option>
                                                            <option value="GUANAJUATO" <?php if('GUANAJUATO'==$item->estado) echo "selected";?> >GUANAJUATO</option>
                                                            <option value="GUERRERO" <?php if('GUERRERO'==$item->estado) echo "selected";?> >GUERRERO</option>
                                                            <option value="HIDALGO" <?php if('HIDALGO'==$item->estado) echo "selected";?> >HIDALGO</option>
                                                            <option value="JALISCO" <?php if('JALISCO'==$item->estado) echo "selected";?> >JALISCO</option>
                                                            <option value="ESTADO DE MEXICO" <?php if('ESTADO DE MEXICO'==$item->estado) echo "selected";?> >ESTADO DE MEXICO</option>
                                                            <option value="MICHOACAN" <?php if('MICHOACAN'==$item->estado) echo "selected";?> >MICHOACAN</option>
                                                            <option value="MORELOS" <?php if('MORELOS'==$item->estado) echo "selected";?> >MORELOS</option>
                                                            <option value="NAYARIT" <?php if('NAYARIT'==$item->estado) echo "selected";?> >NAYARIT</option>
                                                            <option value="NUEVO LEON" <?php if('NUEVO LEON'==$item->estado) echo "selected";?> >NUEVO LEON</option>
                                                            <option value="OAXACA" <?php if('OAXACA'==$item->estado) echo "selected";?> >OAXACA</option>
                                                            <option value="PUEBLA" <?php if('PUEBLA'==$item->estado) echo "selected";?> >PUEBLA</option>
                                                            <option value="QUERETARO" <?php if('QUERETARO'==$item->estado) echo "selected";?> >QUERETARO</option>
                                                            <option value="QUINTANA ROO" <?php if('QUINTANA ROO'==$item->estado) echo "selected";?> >QUINTANA ROO</option>
                                                            <option value="SAN LUIS POTOSI" <?php if('SAN LUIS POTOSI'==$item->estado) echo "selected";?> >SAN LUIS POTOSI</option>
                                                            <option value="SINALOA" <?php if('SINALOA'==$item->estado) echo "selected";?> >SINALOA</option>
                                                            <option value="SONORA" <?php if('SONORA'==$item->estado) echo "selected";?> >SONORA</option>
                                                            <option value="TABASCO" <?php if('TABASCO'==$item->estado) echo "selected";?> >TABASCO</option>
                                                            <option value="TAMAULIPAS" <?php if('TAMAULIPAS'==$item->estado) echo "selected";?> >TAMAULIPAS</option>
                                                            <option value="TLAXCALA" <?php if('TLAXCALA'==$item->estado) echo "selected";?> >TLAXCALA</option>
                                                            <option value="VERACRUZ" <?php if('VERACRUZ'==$item->estado) echo "selected";?> >VERACRUZ</option>
                                                            <option value="YUCATAN" <?php if('YUCATAN'==$item->estado) echo "selected";?> >YUCATAN</option>
                                                            <option value="ZACATECAS" <?php if('ZACATECAS'==$item->estado) echo "selected";?> >ZACATECAS</option>
                                                      </select>
                                                      </div>
                                                      <div class="col s2">
                                                        <label>Municipio<span class="datosrequeridos">*</span></label>
                                                        <input id="df_minicipio" type="text" value="<?php echo $item->municipio ?>" class="form-control-bmz">
                                                      </div>
                                                      
                                                    </div>
                                                    <div class="row">
                                                      <div class="col s2">
                                                        <label>Localidad</label>
                                                        <input id="df_localidad" type="text" value="<?php echo $item->localidad ?>" class="form-control-bmz">
                                                      </div>
                                                      <div class="col s1">
                                                        <label>C.P.<span class="datosrequeridos">*</span></label>
                                                        <input id="cp" type="text" value="<?php echo $item->cp ?>" class="form-control-bmz">
                                                      </div>
                                                      <div class="col s2">
                                                        <label>Calle<span class="datosrequeridos">*</span></label>
                                                        <textarea id="calle" style="min-height: 50px;" class="form-control-bmz"><?php echo $item->calle ?></textarea>
                                                      </div>
                                                      <div class="col s1">
                                                        <label># Ext</label>
                                                        <input id="num_ext" type="text" value="<?php echo $item->num_ext ?>" class="form-control-bmz">
                                                      </div>
                                                      <div class="col s1">
                                                        <label># Int</label>
                                                        <input id="num_int" type="text" value="<?php echo $item->num_int ?>" class="form-control-bmz">
                                                      </div>
                                                      <div class="col s2">
                                                        <label>Colonia</label>
                                                        <textarea id="colonia" style="min-height: 50px;" class="form-control-bmz"><?php echo $item->colonia ?></textarea>
                                                      </div>
                                                      <div class="col s2">
                                                        <label>RegimenFiscalReceptor <span class="datosrequeridos">*</span></label>
                                                        <select id="RegimenFiscalReceptor" name="RegimenFiscalReceptor" class="browser-default form-control-bmz" required>
                                                          <option value="0" <?php if($item->RegimenFiscalReceptor==0){ echo 'selected';}?> ></option>
                                                          <option value="601" <?php if($item->RegimenFiscalReceptor==601){ echo 'selected';}?> >601 General de Ley Personas Morales</option>
                                                          <option value="603" <?php if($item->RegimenFiscalReceptor==603){ echo 'selected';}?> >603 Personas Morales con Fines no Lucrativos</option>
                                                          <option value="605" <?php if($item->RegimenFiscalReceptor==605){ echo 'selected';}?> >605 Sueldos y Salarios e Ingresos Asimilados a Salarios</option>
                                                          <option value="606" <?php if($item->RegimenFiscalReceptor==606){ echo 'selected';}?> >606 Arrendamiento</option>
                                                          <option value="607" <?php if($item->RegimenFiscalReceptor==607){ echo 'selected';}?> >607 Régimen de Enajenación o Adquisición de Bienes</option>
                                                          <option value="608" <?php if($item->RegimenFiscalReceptor==608){ echo 'selected';}?> >608 Demás ingresos</option>
                                                          <option value="609" <?php if($item->RegimenFiscalReceptor==609){ echo 'selected';}?> >609 Consolidación</option>
                                                          <option value="610" <?php if($item->RegimenFiscalReceptor==610){ echo 'selected';}?> >610 Residentes en el Extranjero sin Establecimiento Permanente en México</option>
                                                          <option value="611" <?php if($item->RegimenFiscalReceptor==611){ echo 'selected';}?> >611 Ingresos por Dividendos (socios y accionistas)</option>
                                                          <option value="612" <?php if($item->RegimenFiscalReceptor==612){ echo 'selected';}?> >612 Personas Físicas con Actividades Empresariales y Profesionales</option>
                                                          <option value="614" <?php if($item->RegimenFiscalReceptor==614){ echo 'selected';}?> >614 Ingresos por intereses</option>
                                                          <option value="615" <?php if($item->RegimenFiscalReceptor==615){ echo 'selected';}?> >615 Régimen de los ingresos por obtención de premios</option>
                                                          <option value="616" <?php if($item->RegimenFiscalReceptor==616){ echo 'selected';}?> >616 Sin obligaciones fiscales</option>
                                                          <option value="620" <?php if($item->RegimenFiscalReceptor==620){ echo 'selected';}?> >620 Sociedades Cooperativas de Producción que optan por diferir sus ingresos</option>
                                                          <option value="621" <?php if($item->RegimenFiscalReceptor==621){ echo 'selected';}?> >621 Incorporación Fiscal</option>
                                                          <option value="622" <?php if($item->RegimenFiscalReceptor==622){ echo 'selected';}?> >622 Actividades Agrícolas, Ganaderas, Silvícolas y Pesqueras</option>
                                                          <option value="623" <?php if($item->RegimenFiscalReceptor==623){ echo 'selected';}?> >623 Opcional para Grupos de Sociedades</option>
                                                          <option value="624" <?php if($item->RegimenFiscalReceptor==624){ echo 'selected';}?> >624 Coordinados</option>
                                                          <option value="625" <?php if($item->RegimenFiscalReceptor==625){ echo 'selected';}?> >625 Régimen de las Actividades Empresariales con ingresos a través de Plataformas Tecnológicas</option>
                                                          <option value="626" <?php if($item->RegimenFiscalReceptor==626){ echo 'selected';}?> >626 Régimen Simplificado de Confianza</option>
                                                          <option value="628" <?php if($item->RegimenFiscalReceptor==628){ echo 'selected';}?> >628 Hidrocarburos</option>
                                                          <option value="629" <?php if($item->RegimenFiscalReceptor==629){ echo 'selected';}?> >629 De los Regímenes Fiscales Preferentes y de las Empresas Multinacionales</option>
                                                          <option value="630" <?php if($item->RegimenFiscalReceptor==630){ echo 'selected';}?> >630 Enajenación de acciones en bolsa de valores</option>      
                                                        </select>
                                                      </div>
                                                      <div class="col s1">
                                                        <?php if ($tipoVista ==3) { ?>
                                                            <a class="btn-floating red" data-delay="50"><i class="material-icons">delete_forever</i></a>  
                                                        <?php }else{?>  
                                                            <a class="btn-floating red tooltipped" onclick="Eliminar_Tabla_DatosF(<?php echo $item->id;?>)" data-position="top" data-delay="50" data-tooltip="Eliminar"><i class="material-icons">delete_forever</i></a>
                                                        <?php } ?>
                                                      </div>
                                                    </div>
                                                  </td>
                                                </tr>
                                                 
                                            <?php } ?>
                                          </tbody>
                                      </table> 
                                    </div>
                                </div>
                                <br>
                            </div>
                          </div>
                          
                          <div class="row">
                            <div class="col s12"><h5 class="header">Documentación y acceso</h5></div>
                            <div class="col s1 m1 1">
                              <h6>Servicio</h6>
                            </div>
                            <div class=" col s2 m2 2">
                              <label for="horario_disponible">Horario disponible</label>
                              <input id="horario_disponible" name="horario_disponible" type="text" class="for form-control-bmz" value="<?php echo $horario_disponible;?>">
                            </div>
                            <div class=" col s3 m3 3">
                              <label for="equipo_acceso">Equipo para acceso</label>
                              <input id="equipo_acceso" name="equipo_acceso" type="text" class="for form-control-bmz" value="<?php echo $equipo_acceso;?>">
                            </div>
                            <div class=" col s3 m3 3">
                              <label for="documentacion_acceso">Documentación de acceso</label>
                              <input id="documentacion_acceso" name="documentacion_acceso" type="text" class="for form-control-bmz" value="<?php echo $documentacion_acceso;?>">
                            </div>
                            
                            
                          </div>
                          <div class="row">
                              <div class="col s12 m12 12">
                                <h5 class="header">Equipos</h5>
                              </div>
                              <div class="col s2 m2 2">
                                <label>Modelo</label>
                                <select class="form-control-bmz browser-default" id="modelocliente">
                                  <?php foreach ($datosequipos->result() as $itemeq) { ?>
                                      <option value="<?php echo $itemeq->id;?>"><?php echo $itemeq->modelo;?></option>                                  
                                  <?php } ?>
                                </select>
                                <select class="form-control-bmz browser-default" id="modeloclientexxx" style="display:none;">
                                  <?php foreach ($datosequipos->result() as $itemeq) { ?>
                                      <option value="<?php echo $itemeq->id;?>"><?php echo $itemeq->modelo;?></option>                                  
                                  <?php } ?>
                                </select>
                              </div>
                              <div class="col s2 m2 2">
                                <p><input type="checkbox" id="sinmodelo"  value="1" onclick="sinmodelo()">
                                        <label for="sinmodelo">Sin modelo</label></p>
                              </div>
                              <div class="col s2 m2 2">
                                <label>Serie</label>
                                <input type="text" id="seriecliente" class="form-control-bmz">
                              </div>
                              <div class="col s4 m4 4">
                                <a class="btn-floating waves-effect waves-light purple lightrn-1 btn tooltipped" data-position="top"    
                                    data-tooltip="Agregar"><i class="material-icons" onclick="addequipo()">system_update_alt</i></a>
                              </div>
                          </div>
                          <div class="row">
                            <div class="col s7 m7 7">
                              <table class="table" id="table_modelos_cliente">
                                <thead>
                                  <tr>
                                    <th>Modelo</th>
                                    <th>Serie</th>
                                    <th></th>
                                  </tr>
                                </thead>
                                <tbody class="tbody_modelos_cliente">
                                  <?php foreach ($equiposclienterow->result() as $iteme) { ?>
                                    <tr class="modeloclirowe_<?php echo $iteme->cliequId;?>">
                                      <td>
                                          <input type="hidden" id="cliequId" value="<?php echo $iteme->cliequId;?>">
                                          <input type="hidden" id="modeloclienteid" value="<?php echo $iteme->modelo;?>">
                                          <?php echo $iteme->modelot;?>
                                      </td>
                                      <td>
                                          <input type="hidden" id="seriecliente" value="<?php echo $iteme->serie;?>">
                                          <?php echo $iteme->serie;?>
                                      </td>
                                      <td><a class="btn-floating waves-effect waves-light red accent-2" onclick="deleteequipo(<?php echo $iteme->cliequId;?>,1)"><i class="material-icons">clear</i></a></td>
                                    </tr>
                                  <?php } ?>
                                </tbody>
                              </table>
                            </div>
                          </div>
                         <div class="row">
                            <div class="input-field col s5"></div>
                            <div class="input-field col s5"></div>
                          </div>
                          <div class="row">
                            <div class="input-field col s5"></div>
                            <div class="input-field col s5"></div>
                          </div>
                          <div class="row">
                            <div class="col s12">
                              <label>Condiciones</label>
                              <textarea name="condicion" id="condicion"><?php echo $condicion;?></textarea>
                            </div>
                          </div>
                          <?php if($perfilid==1){
                        if($idpersonal==1 || $idpersonal==17 || $idpersonal==9 || $idpersonal==18){ ?>
                          <input type="hidden" id="m_row" value="1">
                          <div class="row">
                            <div class="col s12 m12 12">
                                <h5 class="header">Accesos</h5>
                            </div>
                            <div class="col s3">
                              <label>Correo</label><input type="mail" id="m_correo" class="form-control-bmz" value="<?php echo $correo;?>">
                            </div>
                            <div class="col s3">
                              <label>Contraseña</label><input type="password" id="m_pass" name="m_pass" class="form-control-bmz" value="<?php echo $pass;?>">
                            </div>
                            
                          </div>
                        <?php } }?>
                          <div class="row">
                            <div class="input-field col s12">
                              <button class="btn cyan waves-effect waves-light right submitform" >Guardar
                                <i class="material-icons right">send</i>
                              </button>
                            </div>
                          </div>
                          
                             
                      </div>
                    </div>
                  </div> 
              </div>
            </div>
          </div>
        </section>

<div id="modalclientesexit" class="modal" >
  <div class="modal-content" style="padding: 5px">
    <div class="row">
      <div class="col s12 ">
        <table>
          <thead><tr><th>Empresa</th><th></th></tr></thead>
          <tbody class="listadoclientes"></tbody>
        </table>
      </div>
    </div>
  </div>  
  <div class="modal-footer">
    <!--<a class="modal-action modal-close waves-effect waves-red gray btn-flat " href="javascript:location.reload()">Cerrar</a>-->
    <a class="modal-action modal-close waves-effect waves-red gray btn-flat " href="<?php echo base_url().'index.php/Clientes'?>">Salir</a>
    <a class="modal-action modal-close waves-effect waves-red gray btn-flat " >Cerrar</a>
  </div>
</div>
<div id="modalinfopre" class="modal" >
  <div class="modal-content" style="padding: 5px">
    <div class="row">
      <div class="col s12 infos_press">
        
      </div>
    </div>
  </div>  
  <div class="modal-footer">
    <!--<a class="modal-action modal-close waves-effect waves-red gray btn-flat " href="javascript:location.reload()">Cerrar</a>-->
    <a class="modal-action modal-close waves-effect waves-red gray btn-flat " >Cerrar</a>
  </div>
</div>
<div class="optiondireccion" style="display:none;"><option value="0"></option><?php foreach ($cliente_dire as $item) {
    echo '<option value="'.$item->idclientedirecc.'">'.$item->direccion.'</option>';
    } ?>
</div>


