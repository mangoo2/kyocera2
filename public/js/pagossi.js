var base_url=$('#base_url').val();
var per_pni;
var table_todo_pni;
$(document).ready(function($) {
	//load_pni();
    $('#idclientep').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una opción',
        allowClear: true,
        ajax: {
            url: base_url+'Ventasi/searchclientes',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    var html='';
                    
                        html=element.empresa+' ('+element.razon_social+')';
                    
                    itemscli.push({
                        id: element.id,
                        text: html

                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function (e) {
        //var data = e.params.data;
        //obtenerrfc(data.id)
        //loadtable();
    });
});
function r_pago_n_reconocidos(){
	$('#modalpagosi').modal();
	$('#modalpagosi').modal('open');
	load_pni(0);
}
function r_pago_n_ok(){
	var form = $('#form_psi');
	if(form.valid()){
		var datos=form.serialize();
		$.ajax({
            type:'POST',
            url: base_url+"index.php/Generales/registrarpni",
            data: datos,
            success:function(response){  
                toastr["success"]("Pago Registrado"); 
                load_pni(0);
                $("#form_psi")[0].reset();
            }
        });
	}
}
function load_pni(per){
    per_pni=per;
    //console.log('per:'+per);
	$.ajax({
        type:'POST',
        url: base_url+"index.php/Generales/load_pni",
        data: {'per':per},
        success:function(response){  
            $('.table_pni').html(response);
            table_todo_pni = $('#table_todo_pni').DataTable();
        }
    });
}
function delete_pni(id){
    var html='¿Confirma la eliminación del pago?';
        html+='<br><label>Se necesita permisos</label>';
        html+='<br><input type="text" placeholder="Contraseña" id="contrasena_pni" name="contrasena_pni" class="form-control-bmz" required/>';
	$.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: html,
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                //var pass=$('#contrasena_pni').val();
                var pass=$("input[name=contrasena_pni]").val();
                $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Generales/delete_pni",
                    data: {
                        id:id,
                        pass:pass
                    },
                    success: function (response){
                        var aux=parseInt(response);
                        if(aux==1){
                            toastr["success"]("Pago eliminado"); 
                        }else{
                            toastr["error"]("No se tiene permisos"); 
                        }
                        
                        load_pni(0);
                    },
                    error: function(response){  
                    	toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema");
                    }
                });                
                
            },
            cancelar: function (){
                
            }
        }
    });
    setTimeout(function(){
            new MaskedPassword(document.getElementById("contrasena_pni"), '\u25CF');
    },500);
}
/*
function comen_pni(id){
    var html='¿Desea agregar un comentario?';
        html+='<br><textarea placeholder="Comentario" id="comen_pni" class="form-control-"></textarea>';
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: html,
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var comen=$('#comen_pni').val();
                $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Generales/comen_pni",
                    data: {
                        id:id,
                        comen:comen
                    },
                    success: function (response){
                        
                            toastr["success"]("Comentario agregado"); 
                        
                        
                        load_pni(per_pni);
                    },
                    error: function(response){  
                        toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema");
                    }
                });                
                
            },
            cancelar: function (){
                
            }
        }
    });
}
*/
function comen_pni(id){
    $('#modal_icomen').modal();
    $('#modal_icomen').modal('open');
    $('.addiframecomep').html('<iframe src="'+base_url+'index.php/Generales/comentariopagos/'+id+'" id="myiframecomen"></iframe>' );
}
/*
var iframe = document.getElementById("myiframecomen");
   iframe.width = iframe.contentWindow.document.body.scrollWidth;
   iframe.height = iframe.contentWindow.document.body.scrollHeight;
*/
function comen_pni_info(id){
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Generales/comen_pni_info",
        data: {
            id:id
        },
        success: function (response){
            console.log(response);
            $('#modal_icomen_info').modal();
            $('#modal_icomen_info').modal('open');
            $('.addcomep_info').html(response);
            $('.addcomep_info img').addClass('materialboxed');
              $('.materialboxed').materialbox();
        },
        error: function(response){  
            toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema");
        }
    }); 
}
function cambiar_per_pn(id,tipo){
    var personal=$('#pi_personal').html();
    $.confirm({
        boxWidth: '40%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Comentario',
        content: '<div style="width: 99%;">¿Desea cambiar al personal?<label>Ejecutivo</label><select id="ejecutivoselect_new" class="browser-default form-control-bmz">'+personal+'</select></div>',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var eje=$('#ejecutivoselect_new').val();
                //var pass=$('#pass_cambio').val();
                $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Generales/cambiopersonal",
                    data: {
                        id:id,
                        tipo:tipo,
                        eje:eje,
                        pass:'12345'
                    },
                    success: function (response){
                        if(parseInt(response)==1){
                            toastr["success"]("Cambio Realizado");  
                            load_pni(0);
                        }else{
                            toastr["error"]("No se tiene permiso"); 
                        }
                        
                    },
                    error: function(response){
                        toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema"); 
                         
                    }
                }); 
            },
            cancelar: function (){
                
            }
        }
    });
    setTimeout(function(){ 
        $('#ejecutivoselect_new option[value=0]').remove();
    }, 1000);
    
}
function filtro_pn(){
    var per = $('#pn_personal option:selected').html();
    console.log(per);
    if(per=='Todos'){
        table_todo_pni.search('').draw()
    }else{
        table_todo_pni.search(per).draw()
    }
}