var base_url = $('#base_url').val();
var calendar;
var codigocarga = $('#codigocarga').val();
var m_asi;
var m_tipo;
var m_cli;
$(document).ready(function($) {
	$('.chosen-select').chosen({width: "100%"});
	/*
    $('#calendario').fullCalendar({
	    defaultView: 'agendaDay',
	    header: {
	      left: 'prev,next today',
	      center: 'title',
	      right: 'month,agendaWeek,agendaDay,resourceTimelineDay',
	    },
        
	    events: function(start, end, timezone, callback) {
           console.log(start);
	        $.ajax({
	            url: base_url+'Calendario/lisservicios',
	            dataType: 'json',
	            type:'POST',
	            data: {
	                start: start.unix(),
	                end: end.unix(),
	                servicioid:$('#servicioid option:selected').val(),
					tecnicoid:$('#tecnicoid option:selected').val(),
					clienteid:$('#clienteid option:selected').val(),
                    zona:$('#zona option:selected').val(),
	            },
	            success: function(doc) {
	            	
	                var datos=doc;
	                var events = [];
	                datos.forEach(function(r){
                        console.log(r);
	                	if(r.prioridad==1){
                            events.push({
		                        asignacionId: r.asignacionId,
		                        title: r.empresa,
		                        start: r.fecha+'T'+r.hora,
		                        end: r.fecha+'T'+r.horafin,
		                        asignacion: r.asignacionId,
		                        tipo: r.tipo,
		                        status: r.status,
		                        color:'#ff0000',
                                fecha: r.fecha,
                                horas: r.hora+' - '+r.horafin,
                                tecnico: r.nombre+' '+r.apellido_paterno+' '+r.apellido_materno,
                                poliza: r.poliza,
                                tserviciomotivo: r.tserviciomotivo,
                            confimardo:r.confirmado,
                            calfecha:r.fecha,
                            nr:r.nr
		                    });
	                	}else if(r.prioridad==2){
                            events.push({
		                        asignacionId: r.asignacionId,
		                        title: r.empresa,
		                        start: r.fecha+'T'+r.hora,
		                        end: r.fecha+'T'+r.horafin,
		                        asignacion: r.asignacionId,
		                        tipo: r.tipo,
		                        status: r.status,
		                        color:'#ff9100',
                                fecha: r.fecha,
                                horas: r.hora+' - '+r.horafin,
                                tecnico: r.nombre+' '+r.apellido_paterno+' '+r.apellido_materno,
                                poliza: r.poliza,
                                tserviciomotivo: r.tserviciomotivo,
                            confimardo:r.confirmado,
                            calfecha:r.fecha,
                            nr:r.nr
		                    });
	                	}else if(r.prioridad==3){
                            events.push({
		                        asignacionId: r.asignacionId,
		                        title: r.empresa,
		                        start: r.fecha+'T'+r.hora,
		                        end: r.fecha+'T'+r.horafin,
		                        asignacion: r.asignacionId,
		                        tipo: r.tipo,
		                        status: r.status,
		                        color:'#dbca05',
                                fecha: r.fecha,
                                horas: r.hora+' - '+r.horafin,
                                tecnico: r.nombre+' '+r.apellido_paterno+' '+r.apellido_materno,
                                poliza: r.poliza,
                                tserviciomotivo: r.tserviciomotivo,
                            confimardo:r.confirmado,
                            calfecha:r.fecha,
                            nr:r.nr
		                    });
	                	}else if(r.prioridad==4){
                            events.push({
		                        asignacionId: r.asignacionId,
		                        title: r.empresa,
		                        start: r.fecha+'T'+r.hora,
		                        end: r.fecha+'T'+r.horafin,
		                        asignacion: r.asignacionId,
		                        tipo: r.tipo,
		                        status: r.status,
		                        color:'#00a1ff',
                                fecha: r.fecha,
                                horas: r.hora+' - '+r.horafin,
                                tecnico: r.nombre+' '+r.apellido_paterno+' '+r.apellido_materno,
                                poliza: r.poliza,
                                tserviciomotivo: r.tserviciomotivo,
                            confimardo:r.confirmado,
                            calfecha:r.fecha,
                            nr:r.nr
		                    });
	                	}else if(r.prioridad==5){
                            events.push({
                                asignacionId: r.asignacionId,
                                title: r.empresa,
                                start: r.fecha+'T'+r.hora,
                                end: r.fecha+'T'+r.horafin,
                                asignacion: r.asignacionId,
                                tipo: r.tipo,
                                status: r.status,
                                color:'#038f18',
                                fecha: r.fecha,
                                horas: r.hora+' - '+r.horafin,
                                tecnico: r.nombre+' '+r.apellido_paterno+' '+r.apellido_materno,
                                poliza: r.poliza,
                                tserviciomotivo: r.tserviciomotivo,
                            confimardo:r.confirmado,
                            calfecha:r.fecha,
                            nr:r.nr
                            });
                        }else{
	                		events.push({
		                        asignacionId: r.asignacionId,
		                        title: r.empresa,
		                        start: r.fecha+'T'+r.hora,
		                        end: r.fecha+'T'+r.horafin,
		                        asignacion: r.asignacionId,
		                        tipo: r.tipo,
		                        status: r.status,
                                fecha: r.fecha,
                                horas: r.hora+' - '+r.horafin,
                                tecnico: r.nombre+' '+r.apellido_paterno+' '+r.apellido_materno,
                                poliza: r.poliza,
                                tserviciomotivo: r.tserviciomotivo,
		                        confimardo:r.confirmado,
                            calfecha:r.fecha,
                            nr:r.nr
		                    });
	                	}
	                });
	                callback(events);
	            }
	        });
	    },
	    eventRender: function(event, element){ 
	    	element.dblclick(function() {
                console.log(event.asignacion+'_'+event.tipo+'_'+event.calfecha);
	    		obternerdatosservicio(event.asignacion,event.tipo,event.calfecha);

	    		$('#modalinfocalendario').modal();
    			$('#modalinfocalendario').modal('open');
    			re_agendar_c();
                notificacionsercot();
	    	});
	    	if(event.confimardo==1) {
		        element.find(".fc-title").prepend('<i class="far fa-check-circle"></i> ');
	       }
            console.log(event.status);
            if(event.status==2) {
                element.find(".fc-title").prepend('<i class="fas fa-clipboard-check fa-2x colordeiconofinalizado"></i> ');
            }
            if(event.status==1) {
                element.find(".fc-title").prepend('<i class="fas fa-sync fa-spin"></i> ');
            }
            if(event.nr==1) {
                element.find(".fc-title").prepend('<i class="fas fa-times-circle fa-fw"></i> ');
            }
            var elementtop='Fecha: '+event.fecha+'<br>Horario: '+event.horas+'<br>Tecnico: '+event.tecnico+'<br>'+event.poliza+'<br>'+event.tserviciomotivo;
            Tipped.create(element, elementtop, {
                          title: event.title,
                          size: "large",
                          skin: "light",
                        });
	    }
	});
    */
    newcalendario();
	$('.cardinfo').click(function(event) {
		$('#modalinfocalendario').modal();
    	$('#modalinfocalendario').modal('open');
	});
	//=================================================
	$('#clienteid').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una opción',
        allowClear: true,
        ajax: {
            url: base_url+'Ventasi/searchclientes',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.id,
                        text: element.empresa
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function (e) {
          updatecalendar();
    }).on('select2:clear', function (e) {
          updatecalendar();
    });
    $('#servicioid').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una opción',
        allowClear: true,
        ajax: {
            url: base_url+'Calendario/searchservicio',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.id,
                        text: element.nombre
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function (e) {
          updatecalendar();
    }).on('select2:clear', function (e) {
          updatecalendar();
    });
    $('.fc-agendaWeek-button').click(function(event) {
    	var title= $('.fc-center h2').html();
    	$('.fc-center h2').html('Semana '+title);
    });
    setTimeout(function(){ 
    	var title= $('.fc-center h2').html();
    	$('.fc-center h2').html('Semana '+title);
    }, 1000);
    setInterval(function(){
    	updatecalendar();
    },1800000);
});
function inputfileadd(){
    $("#files").fileinput({
        showCaption: true,
        dropZoneEnabled: false,
        showUpload: true,// quita el boton de upload
        //rtl: true,
        //allowedFileExtensions: ["jpg","png","webp","jpeg"],
        browseLabel: 'Seleccionar documentos',
        uploadUrl: base_url+'Calendario/file_multiple',
        maxFilePreviewSize: 5000,
        previewFileIconSettings: {
            'docx': '<i class="fa fa-file-word-o text-primary"></i>',
            'xlsx': '<i class="fa fa-file-excel-o text-success"></i>',
            'pptx': '<i class="fa fa-file-powerpoint-o text-danger"></i>',
            'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
            'pdf': '<i class="fa fa-file-pdf-o text-danger"></i>',
            'zip': '<i class="fa fa-file-archive-o text-muted"></i>',
            'cer': '<i class="fas fa-file-invoice"></i>',
        },
        uploadExtraData: function (previewId, index) {
        var info = {
                    code:codigocarga
                };
        return info;
      }
    }).on('filebatchuploadcomplete', function(event, files, extra) {
      //location.reload();
        upload_data_view(codigocarga);
        $('#files').fileinput('clear');
    }).on('filebatchuploadsuccess', function(event, files, extra) {
      //location.reload();
      //toastr.success('Se cargo el Certificado Correctamente','Hecho!');
        upload_data_view(codigocarga);
        $('#files').fileinput('clear');
    });
}
function actualizar_cal(){
	//$('#calendario').fullCalendar( 'refetchEvents' );
    calendar.refetchEvents();
}
function updatecalendar(){
	//$('#calendario').fullCalendar( 'refetchEvents' );
    calendar.refetchEvents();
}
function cambiarfecha(){
	var nuevomesview = $('#nuevomesview option:selected').val();
	//$("#calendario").fullCalendar( 'gotoDate', nuevomesview);
    calendar.gotoDate(nuevomesview);
}
function obternerdatosservicio(asignacion,tipo,fecha){
  $('#comentariocal').val('');
  $('.equipoacceso').html('');
  $('.infopro').html('');
  var idpersonal = $('#idpersonal').val();
	$.ajax({
        type:'POST',
        url: base_url+'index.php/calendario/obternerdatosservicio',
        data: {
          asignacion:asignacion,
          tipo:tipo,
          fecha:fecha
        },
        success:function(data){
            console.log(data);
            var array = $.parseJSON(data);
            $('.datoscliente').html(array.cliente);
            $('.datosclientecontacto').html(array.contacto);
            $('#comentariocal').val(array.comentariocal);
            $('.equipoacceso').html(array.equipoacceso);
            $('.botondefinalizar').html(array.botondefinalizar);

            if (tipo==1) {
            	$('.datosclientetipo').html('Renta');
                $('.infopro').html('Producción<br>Promedio');
            }else if (tipo==2) {
				$('.datosclientetipo').html('Poliza');
            }else if (tipo==3) {
            	$('.datosclientetipo').html('Evento');
            }else if (tipo==4) {
                $('.datosclientetipo').html('Venta');
            }
            if (array.tiposervicio==0) {
            	$('.datosclienteasignaciones').html('Asignaciones Mensual');
            }else{
            	$('.datosclienteasignaciones').html('Asignaciones diarias');
            }
            $('.datosclienteservicio').html(array.servicio);
            $('.datosclientezona').html(array.zona);
            $('.datosclientetecnico').html(array.tecnico);
            $('.datosclienteprioridad').html(array.prioridad);
            $('.datosclienteahorario').html(array.fecha+' '+array.hora+' '+array.horafin);
            $('.tablerentapoliza').html(array.equipos);
            $('.t_dconsumibles').html(array.dconsumibles);
            ///motivo
            if(array.m_tipo==3){
                $('.motivo_texto').css('display','block');
            }else{
                $('.motivo_texto').css('display','none');
            }
            $('.t_motivo_serv').html(array.m_servicio);
            $('.t_doc_acces').html(array.m_doc_acce);
            $('.id_asigna').html(asignacion);
            $('.id_tipo').html(tipo);
            if(array.confirmado==1){
                //if(idpersonal==13 || idpersonal==1){
                if(idpersonal==13){
                    $('.modalconfimar').removeClass('cyan').addClass('red');
                }else{
                    $('.modalconfimar').removeClass('cyan').addClass('red').prop( "disabled", true )
                }
            }else{
                $('.modalconfimar').removeClass('red').addClass('cyan').prop( "disabled", false );
            }
            /*
            if (array.length>0) {
              //
              $('#id_servicio_f').val(array[0].asignacionId);
              $('#fecha').val(array[0].fecha);
              $("#hora").val(array[0].hora);
              $("#horafin").val(array[0].horafin);
              $("#tecnico").val(array[0].tecnico).change();
              $("#test"+array[0].prioridad).click();
              $("#tservicio").val(array[0].tservicio).change();
              $("#tserviciomotivo").val(array[0].tserviciomotivo);

              $("#tpoliza").val(array[0].tpoliza);
              $(".ttipo"+array[0].ttipo).attr('checked', true);
              if (array[0].generado==1) {
                $('.databtndisabled').attr('disabled',true);
              }
            }else{
              $('#id_servicio_f').val(0);
              $('#fecha').val('');
              $("#hora").val('');
              $("#horafin").val('');
              $("#tecnico").val(0).change();
              //$("#test"+array[0].prioridad).click();
              $("#tservicio").val(0).change();
              $("#tserviciomotivo").val('');
            }*/
            var btn_cls='';
            if(tipo==1){//contratos
                const btn_c='<button type="button" class="waves-effect btn-bmz  red" onclick="envio_c('+asignacion+','+tipo+','+array.idcliente+')"><i class="fas fa-print"></i></button>';
                $('.btn_solicitud_consumibles').html(btn_c);
                    btn_cls+='<button type="button" class="btn-bmz  blue" onclick="clonarservicio('+asignacion+','+tipo+')" title="Clonar servicio"><i class="fas fa-clone"></i></button>';
            }else{
                $('.btn_solicitud_consumibles').html('');
                btn_cls='';
            }
                const btn_s='<button type="button" class="waves-effect btn-bmz  red" onclick="envio_s_m('+asignacion+','+tipo+','+array.idcliente+')"><i class="fas fa-envelope-open-text"></i></button>';
                $('.btn_confirmacion_servicio').html(btn_s);
                if(array.pin!=''){
                    var passbtn='';
                }else{
                    var passbtn=' blue ';
                }
                if(idpersonal==1 || idpersonal==54 || idpersonal==42 || idpersonal==35){
                    btn_cls+='<button type="button" class="btn-bmz '+passbtn+' passservicio" onclick="passservicio('+asignacion+','+tipo+','+array.idcliente+')" data-pin="'+array.pin+'" title="Agregar pin pra finalizar"><i class="fas fa-key"></i></button>';
                }
                $('.btn-clonar').html(btn_cls);
        }
    });
    
}
function re_agendar(){
	$('.btn_reagendar').css('display','none');
	$('.btn_reagendar_c').css('display','block');
	$('.text_reagendar').html('<form method="post" id="form_agenda"><div class="row" align="center">\
                 <div class="col s12 m12 l12">\
                   <label class="col s12 m2 l2">Fecha</label>\
                   <div class="col s12 m8 l8">\
                     <input type="date" name="fecha" id="fecha" class="form-control-bmz">\
                   </div>\
                 </div>\
                 <div class="col s6 m6 l6">\
                   <label class="col s12 m12 l12">Hora Inicio</label>\
                   <div class="col s12 m12 l12">\
                     <input type="time" name="hora" id="hora" class="form-control-bmz">\
                   </div>\
                 </div> \
                 <div class="col s6 m6 l6">\
                   <label class="col s12 m12 l12">Hora Fin</label>\
                   <div class="col s12 m12 l12">\
                     <input type="time" name="horafin" id="horafin" class="form-control-bmz">\
                   </div>\
                 </div> \
              </div></form>\
              <div class="row" align="right">\
                <div class="col m12">\
                  <button type="button" class="waves-effect cyan btn-bmz" onclick="guardar_re_agendacion()">Guardar</button>\
                </div>\
              <div');
}
function re_agendar_c(){
	$('.btn_reagendar').css('display','block');
	$('.btn_reagendar_c').css('display','none');
	$('.text_reagendar').html('');
}
function guardar_re_agendacion(){
    var form_register = $('#form_agenda');
	// Validamos el formulario en base a las reglas mencionadas debajo
	form_register.validate({
	    ignore: "",
	    rules: 
	    {
			fecha: {
	            required: true
	        },
	        hora: {
	            required: true
	        },
	        horafin: {
	            required: true
	        },
	    },
	    // Para mensajes personalizados
	    messages: 
	    {
	        fecha:{
	            required: "Campo requerido"
	        },
	        hora:{
	            required: "Campo requerido"
	        },
	        horafin:{
	            required: "Campo requerido"
	        }
	    },
	    errorElement : 'div',
	});
	var $valid = $("#form_agenda").valid();
        if($valid) {
        	var datos= $('#form_agenda').serialize()+'&id_asignacion='+$('.id_asigna').text()+'&tipo='+$('.id_tipo').text()+'&comentariocal='+$('#comentariocal').val();
            $.ajax({
                type:'POST',
                url: base_url+'Calendario/registro_re_agenda',
                data: datos,
                statusCode:{
                    404: function(data){
                        swal("Error!", "No Se encuentra el archivo", "error");
                    },
                    500: function(){
                        swal("Error!", "500", "error");
                    }
                },
                success:function(data){
                        swal("Éxito", "Guardado Correctamente", "success");
                        $('#modalinfocalendario').modal('close');
                        $('#calendario').fullCalendar( 'refetchEvents' );
                }
            });
        }
}
function modalconfimar(){
	$('#modal_confimar').modal();
	$('#modal_confimar').modal('open');
}
function confimar_asignacion(){
	
	$.ajax({
        type:'POST',
        url: base_url+'Calendario/confirmar_asignar',
        data:{
        	id_asigna:$('.id_asigna').text(),
        	tipo:$('.id_tipo').text(),
          comentario:$('#comentariocal').val()
        },
        statusCode:{
            404: function(data){
                swal("Error!", "No Se encuentra el archivo", "error");
            },
            500: function(){
                swal("Error!", "500", "error");
            }
        },
        success:function(data){
                swal("Éxito", "Guardado Correctamente", "success");
                $('#modal_confimar').modal('close');
	           $('#modalinfocalendario').modal('close');
	           //$('#calendario').fullCalendar( 'refetchEvents' );
               calendar.refetchEvents();
                setTimeout(function(){ 
                    verificar_nuevos_servicios_ext();
                }, 1000);
               //verificar_nuevos_servicios_ext();
        }
    });
}
function detallep(id){
    window.open(base_url+"PolizasCreadas/view/"+id, "Prefactura", "width=780, height=612");
}
function detallev(id,tipo){
    if (tipo==0) {
        window.open(base_url+"Prefactura/view/"+id, "Prefactura", "width=780, height=612");
    }else{
        window.open(base_url+"Prefactura/viewc/"+id, "Prefactura", "width=780, height=612");
    }
}
function edit_contacto_evento(evento,cliente,tipo){
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Calendario/obtenercontactos",
        data: {
            cliente:cliente
        },
        success: function (response){
               $.confirm({
                    boxWidth: '30%',
                    useBootstrap: false,
                    icon: 'fa fa-warning',
                    title: 'Atención!',
                    content: 'Seleccione contacto<br>'+
                             '<select class="browser-default form-control-bmz" id="even_contacto">'+response+'</select>',
                    type: 'red',
                    typeAnimated: true,
                    buttons:{
                        confirmar: function (){
                            var cont_new=$('#even_contacto option:selected').val();
                            var idcont=$('#even_contacto option:selected').data('idcont');
                            console.log(cont_new);
                                 $.ajax({
                                    type:'POST',
                                    url: base_url+"index.php/Calendario/editar_e_contacto",
                                    data: {
                                        contacto:cont_new,
                                        ser:evento,
                                        tipo:tipo,
                                        idcont:idcont
                                    },
                                    success: function (response){
                                            swal("Éxito", "Guardado Correctamente", "success");
                                            $('#modalinfocalendario').modal('close');
                                    },
                                    error: function(response){
                                        notificacion(1);  
                                         
                                    }
                                });
                        },
                        cancelar: function () {
                            
                        }
                    }
                }); 
        },
        error: function(response){
            notificacion(1);
        }
    });
    //=====================================================================
    
}
function edit_equipo_doc(evento,col,tipo){
    if(col==1){
        var coltext='Equipo de acceso';
    }else{
        var coltext='Documentos de acceso';
    }
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: 'Modificar '+coltext+'<br>'+
                 '<textarea class="browser-default form-control-bmz" id="textinfo"></textarea>',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var textinfo=$('#textinfo').val();
                     $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Calendario/editar_e_equipo_documento",
                        data: {
                            textinfo:textinfo,
                            ser:evento,
                            col:col,
                            tipo:tipo
                        },
                        success: function (response){
                                swal("Éxito", "Guardado Correctamente", "success");
                                $('#modalinfocalendario').modal('close');
                        },
                        error: function(response){
                            notificacion(1);
                        }
                    });
            },
            cancelar: function () {
                
            }
        }
    }); 
    //=====================================================================
}
function modal_ayuda(){
  $('#modal_ayuda_color').modal();
  $('#modal_ayuda_color').modal('open');
}
//mismo en calendario
function cl_ser_statusser(serv,serve,tipo,iniciarfinalizar){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Confirma la finalización del servicio?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+'index.php/AsignacionesTecnico/cambiarstatusservicio2',
                    data: {
                        serv:serv,
                        serve:serve,
                        tipo:tipo,
                        iniciarfinalizar:iniciarfinalizar
                    },
                    success:function(data){
                        $('#modalinfocalendario').modal('close');
                        actualizar_cal();
                        if(iniciarfinalizar==0){
                            toastr["success"]("Servicio iniciado con éxito. Te recordamos que el servicio finalizará cuando el cliente firme de conformidad");
                        }else{
                            toastr["success"]("Equipo finalizado");
                        }
                    }
                });
            },
            cancelar: function () {
                
            }
        }
    });
}
function notificacionsercot(){
    setTimeout(function(){
        $('.notificacionsercot').click(function(event) {
            var asignacionide = $(this).data('asignacionide');
            var asignacionid = $(this).data('asignacionid');
            var cotrefaccion = $(this).data('cotrefaccion');
            var cotrefaccion2 = $(this).data('cotrefaccion2');
            var cotrefaccion3 = $(this).data('cotrefaccion3');
            var tiposer = $(this).data('tiposer');
            console.log(asignacionide+'_'+asignacionid+'_'+cotrefaccion+'_'+cotrefaccion2+'_'+cotrefaccion3+'_'+tiposer);
            var html='';
                if(cotrefaccion!=''){
                    html+='<div class="row"><div class="col s12"><b>POR DESGASTE</b></div><div class="col s12">'+cotrefaccion+'</div><div>';
                }
                if(cotrefaccion2!=''){
                    html+='<div class="row"><div class="col s12"><b>POR DAÑO</b></div><div class="col s12">'+cotrefaccion2+'</div><div>';
                }
                if(cotrefaccion3!=''){
                    html+='<div class="row"><div class="col s12"><b>POR GARANTIA</b></div><div class="col s12">'+cotrefaccion3+'</div><div>';
                }
            $.confirm({
                boxWidth: '30%',
                useBootstrap: false,
                icon: 'fa fa-warning',
                title: 'Cotizacion pendiente',
                content: html,
                type: 'red',
                typeAnimated: true,
                buttons:{
                    'Ir a la solicitud': function (){
                        var urldir=base_url+'index.php/Solicitud_Refacciones?tiposer='+tiposer+'&asignacionide='+asignacionide+'&asignacionid='+asignacionid
                        window.open(urldir, '_blank');
                    },
                    cancelar: function () {
                        
                    }
                }
            });
        });
    }, 1000);
}
function newcalendario(){
    var calendarEl = document.getElementById('calendario');

    calendar = new FullCalendar.Calendar(calendarEl, {
      //selectable: true,
      //editable: true, // enable draggable events
      //aspectRatio: 1.8,
      //scrollTime: '02:00', // undo default 6am scrollTime
        height: 'auto',//se define para que los header se queden fijos cuando se suba la bentana junto con el css
      headerToolbar: {
        left: 'today prev,next',
        center: 'title',
        right: 'timelineDay,timeGridWeek,dayGridMonth'
      },
      locale:'es',
      initialView: 'timelineDay',
      views: {
        timelineThreeDays: {
          type: 'timeline',
          duration: { days: 3 },
          buttonText: '3 days'
        }
      },
      events: function(info, successCallback, failureCallback) {

        console.log(info);
        var fechainicio=info.startStr.split('T');
        
        console.log(fechainicio[0]);
        var fechaend=info.endStr.split('T');
        console.log(fechaend[0]);
            $.ajax({
                url: base_url+'Calendario/lisservicios',
                dataType: 'json',
                type:'POST',
                data: {
                    start: fechainicio[0],
                    end: fechaend[0],
                    servicioid:$('#servicioid option:selected').val(),
                    tecnicoid:$('#tecnicoid option:selected').val(),
                    clienteid:$('#clienteid option:selected').val(),
                    zona:$('#zona option:selected').val(),
                    soli:$('#soli option:selected').val()
                },
                success: function(doc) {
                    
                    var datos=doc;
                    var events = [];
                    datos.forEach(function(r){
                        console.log(r);
                        if(r.prioridad==1){
                            events.push({
                                asignacionId: r.asignacionId,
                                title: r.empresa,
                                start: r.fecha+'T'+r.hora,
                                end: r.fecha+'T'+r.horafin,
                                asignacion: r.asignacionId,
                                tipo: r.tipo,
                                status: r.status,
                                color:'#ff0000',
                                eventBackgroundColor:'#ff0000',
                                fecha: r.fecha,
                                horas: r.hora+' - '+r.horafin,
                                tecnico: r.nombre+' '+r.apellido_paterno+' '+r.apellido_materno,
                                poliza: r.poliza,
                                tserviciomotivo: r.tserviciomotivo,
                            confimardo:r.confirmado,
                            calfecha:r.fecha,
                            nr:r.nr
                            });
                        }else if(r.prioridad==2){
                            events.push({
                                asignacionId: r.asignacionId,
                                title: r.empresa,
                                start: r.fecha+'T'+r.hora,
                                end: r.fecha+'T'+r.horafin,
                                asignacion: r.asignacionId,
                                tipo: r.tipo,
                                status: r.status,
                                color:'#ff9100',
                                eventBackgroundColor:'#ff9100',
                                fecha: r.fecha,
                                horas: r.hora+' - '+r.horafin,
                                tecnico: r.nombre+' '+r.apellido_paterno+' '+r.apellido_materno,
                                poliza: r.poliza,
                                tserviciomotivo: r.tserviciomotivo,
                            confimardo:r.confirmado,
                            calfecha:r.fecha,
                            nr:r.nr
                            });
                        }else if(r.prioridad==3){
                            events.push({
                                asignacionId: r.asignacionId,
                                title: r.empresa,
                                start: r.fecha+'T'+r.hora,
                                end: r.fecha+'T'+r.horafin,
                                asignacion: r.asignacionId,
                                tipo: r.tipo,
                                status: r.status,
                                color:'#dbca05',
                                eventBackgroundColor:'#dbca05',
                                fecha: r.fecha,
                                horas: r.hora+' - '+r.horafin,
                                tecnico: r.nombre+' '+r.apellido_paterno+' '+r.apellido_materno,
                                poliza: r.poliza,
                                tserviciomotivo: r.tserviciomotivo,
                            confimardo:r.confirmado,
                            calfecha:r.fecha,
                            nr:r.nr
                            });
                        }else if(r.prioridad==4){
                            events.push({
                                asignacionId: r.asignacionId,
                                title: r.empresa,
                                start: r.fecha+'T'+r.hora,
                                end: r.fecha+'T'+r.horafin,
                                asignacion: r.asignacionId,
                                tipo: r.tipo,
                                status: r.status,
                                color:'#00a1ff',
                                eventBackgroundColor:'#00a1ff',
                                fecha: r.fecha,
                                horas: r.hora+' - '+r.horafin,
                                tecnico: r.nombre+' '+r.apellido_paterno+' '+r.apellido_materno,
                                poliza: r.poliza,
                                tserviciomotivo: r.tserviciomotivo,
                            confimardo:r.confirmado,
                            calfecha:r.fecha,
                            nr:r.nr
                            });
                        }else if(r.prioridad==5){
                            events.push({
                                asignacionId: r.asignacionId,
                                title: r.empresa,
                                start: r.fecha+'T'+r.hora,
                                end: r.fecha+'T'+r.horafin,
                                asignacion: r.asignacionId,
                                tipo: r.tipo,
                                status: r.status,
                                color:'#038f18',
                                eventBackgroundColor:'#038f18',
                                fecha: r.fecha,
                                horas: r.hora+' - '+r.horafin,
                                tecnico: r.nombre+' '+r.apellido_paterno+' '+r.apellido_materno,
                                poliza: r.poliza,
                                tserviciomotivo: r.tserviciomotivo,
                            confimardo:r.confirmado,
                            calfecha:r.fecha,
                            nr:r.nr
                            });
                        }else{
                            events.push({
                                asignacionId: r.asignacionId,
                                title: r.empresa,
                                start: r.fecha+'T'+r.hora,
                                end: r.fecha+'T'+r.horafin,
                                asignacion: r.asignacionId,
                                tipo: r.tipo,
                                status: r.status,
                                fecha: r.fecha,
                                horas: r.hora+' - '+r.horafin,
                                tecnico: r.nombre+' '+r.apellido_paterno+' '+r.apellido_materno,
                                poliza: r.poliza,
                                tserviciomotivo: r.tserviciomotivo,
                                confimardo:r.confirmado,
                            calfecha:r.fecha,
                            nr:r.nr
                            });
                        }
                    });
                    successCallback(events);
                }
            });
        },
        eventContent: function(info) {
            var eventEl = document.createElement('div');
            eventEl.innerText = info.event.title;
            
            // Agregar un tooltip utilizando Tippy.js
            console.log('info');
            console.log(info);
            console.log(info.event.extendedProps);
            console.log('fin info');
            var elementtop='Fecha: '+info.event.extendedProps.fecha+'<br>Horario: '+info.event.extendedProps.horas+'<br>Tecnico: '+info.event.extendedProps.tecnico+'<br>'+info.event.extendedProps.poliza+'<br>'+info.event.extendedProps.tserviciomotivo;
            Tipped.create(eventEl, elementtop, {
                          title: info.event.title,
                          size: "large",
                          skin: "light",
                        });
            //info.el.find(".fc-title").prepend('<i class="fas fa-times-circle fa-fw"></i> ');
            //if (info.event.extendedProps.iconClass) {
                
                if(info.event.extendedProps.confimardo==1) {
                    //element.find(".fc-title").prepend('<i class="far fa-check-circle"></i> ');
                    var icon = document.createElement('i');
                    icon.className = 'far fa-check-circle';
                    eventEl.prepend(icon);
               }
                console.log(event.status);
                if(info.event.extendedProps.status==2) {
                    //element.find(".fc-title").prepend('<i class="fas fa-clipboard-check fa-2x colordeiconofinalizado"></i> ');
                    var icon = document.createElement('i');
                    icon.className = 'fas fa-clipboard-check fa-2x colordeiconofinalizado';
                    eventEl.prepend(icon);
                }
                if(info.event.extendedProps.status==1) {
                    //element.find(".fc-title").prepend('<i class="fas fa-sync fa-spin"></i> ');
                    var icon = document.createElement('i');
                    icon.className = 'fas fa-sync fa-spin';
                    eventEl.prepend(icon);
                }
                if(info.event.extendedProps.nr==1) {
                    //element.find(".fc-title").prepend('<i class="fas fa-times-circle fa-fw"></i> ');
                    var icon = document.createElement('i');
                    icon.className = 'fas fa-times-circle fa-fw';
                    eventEl.prepend(icon);
                }
            
            
            return { domNodes: [eventEl] };
        },
        eventClick: function(info) {
            console.log(info.event.extendedProps.asignacion+'_'+info.event.extendedProps.tipo+'_'+info.event.extendedProps.calfecha);
            obternerdatosservicio(info.event.extendedProps.asignacion,info.event.extendedProps.tipo,info.event.extendedProps.calfecha);

            $('#modalinfocalendario').modal();
            $('#modalinfocalendario').modal('open');
            re_agendar_c();
            notificacionsercot();
        },
    });

    calendar.render();
}
/*
function envio_c(asi){
    $.ajax({
        type:'POST',
        url: base_url+'index.php/Mailenvio/envio_c',
        data: {
            asi:asi
        },
        success:function(data){
            toastr["success"]("Correo enviado");
        }
    });
}
*/
function envio_s_m(asi,tipo,cliente){
    $('#firma_m_s').val(54);
    m_asi=asi;
    m_tipo=tipo;
    m_cli=cliente;
    obtenercontacto(cliente);
    obtenerbodymail(cliente);
    $('.input_envio_c').html('');
    $('.table_mcr_files_c').html('');
    $('.input_envio_s').html('<input type="file" id="files" name="files[]" multiple accept="*">');
    $('.table_mcr_files_s').html('<table class="table bordered striped" id="table_mcr_files"><tbody class="tabbodycr_files"></tbody></table>');
    $('.addbtnservicio').html('<button type="button" class="waves-effect btn-bmz  red btn-envioser" onclick="envio_s('+asi+','+tipo+')">Enviar</button>');
    inputfileadd();

    $('#modal_servicio').modal();
    $('#modal_servicio').modal('open');
}
function envio_s(asi,tipo){
    var email =$('#ms_contacto option:selected').data('email');
    $.ajax({
        type:'POST',
        url: base_url+'index.php/Mailenvio/envio_s',
        data: {
            asi:asi,
            tipo:tipo,
            code:codigocarga,
            email:email,
            per:$('#firma_m_s option:selected').val(),
            perbbc:$('#bbc_m_s option:selected').val(),
            asunto:$('#ms_asucto2').val(),
            comen:$('#ms_coment2').val()
        },
        success:function(data){
            console.log(data);
            var array = $.parseJSON(data);
            codigocarga=array.newcodigo;
            $('#codigocarga').val(codigocarga);
            toastr["success"]("Correo enviado");
        }
    });
}
function envio_c(asi,tipo,cliente){
    $('#firma_m').val(54);
    m_asi=asi;
    m_tipo=tipo;
    m_cli=cliente;
    $('.tabbodycr_contac').html('');
    $('.tbody_mcr_cont').html('');
    $('#mcr_coment').val('');
    $('#mcr_asucto').val('Consumible');
    $('.input_envio_c').html('<input type="file" id="files" name="files[]" multiple accept="*">');
    $('.table_mcr_files_c').html('<table class="table bordered striped" id="table_mcr_files"><tbody class="tabbodycr_files"></tbody></table>');
    $('.input_envio_s').html('');
    $('.table_mcr_files_s').html('');
    inputfileadd();
    codigocarga = $('#codigocarga').val();
    $.ajax({
        type:'POST',
        url: base_url+'index.php/Calendario/infoclientecontrato',
        data: {
            asi:asi
        },
        success:function(data){
            //console.log(data);
            var array = $.parseJSON(data);
            console.log(array);
            $('#modal_consumibles_renta').modal();
            $('#modal_consumibles_renta').modal('open');
            $('.mcr_cliente').html(array.general.empresa);
            $('.mcr_contrato').html(array.general.folio);
            $('#mcr_select_cont').html('');
            $('#mcr_select_contac').html('');
            array.contratos.forEach(function(r){
                $('#mcr_select_cont').append('<option value="'+r.idcontrato+'">'+r.folio+'</option>');
            });
            array.contactos.forEach(function(r){
                $('#mcr_select_contac').append('<option value="'+r.datosId+'" data-mail="'+r.email+'" data-tel="'+r.telefono+'" data-cel="'+r.celular+'">'+r.atencionpara+'</option>');
            });
            $('#mcr_select_cont').select2();
            $('#mcr_select_contac').select2();
        }
    });
}
var idrowcc=0;
function agregarcontratomail(){
    var con = $('#mcr_select_cont option:selected').val();
    var cont = $('#mcr_select_cont option:selected').text();
    var html ='<tr class="idrowcc_'+idrowcc+'">';
        html+='<td><input type="hidden" id="ec_con" value="'+con+'">'+con+'</td>';
        html+='<td>'+cont+'</td>';
        html+='<td><a class="b-btn b-btn-danger" onclick="delete_mcr('+idrowcc+')"><i class="fas fa-trash-alt"></i></a></td>';
        html+='</tr>';
    $('.tbody_mcr_cont').append(html);
    idrowcc++;
}
function agregarcontactomail(){
    var con = $('#mcr_select_contac option:selected').val();
    var cont = $('#mcr_select_contac option:selected').text();
    var mail = $('#mcr_select_contac option:selected').data('mail');
    var tel = $('#mcr_select_contac option:selected').data('tel');
        tel = tel.toString().replace('-','');
    var cel = $('#mcr_select_contac option:selected').data('cel');
        cel = cel.toString().replace('-','');

    var html ='<tr class="idrowcc_'+idrowcc+'">';
        html+='<td><input type="hidden" id="ec_con" value="'+con+'"><input type="hidden" id="ec_tel" value="'+tel+'"><input type="hidden" id="ec_cel" value="'+cel+'">'+con+'</td>';
        html+='<td>'+cont+'</td>';
        html+='<td>'+mail+'</td>';
        html+='<td>'+tel+'</td>';
        html+='<td>'+cel+'</td>';
        html+='<td><a class="b-btn b-btn-danger" onclick="delete_mcr('+idrowcc+')"><i class="fas fa-trash-alt"></i></a></td>';
        html+='</tr>';
    $('.tabbodycr_contac').append(html);
    idrowcc++;
}
function delete_mcr(id){
    $('.idrowcc_'+id).remove();
}
function envio(op,tipo){
    $('#firma_m').val(54);
    var contratos = $("#table_mcr_cont tbody > tr");
    //==============================================
        var DATAc  = [];
        contratos.each(function(){         
            item = {};                    
            item ["con"]   = $(this).find("input[id*='ec_con']").val();
            DATAc.push(item);
        });
        INFOa  = new FormData();
        aInfoc   = JSON.stringify(DATAc);
    //========================================
    var contactos = $("#table_mcr_contac tbody > tr");
    //==============================================
        var DATAco  = [];
        contactos.each(function(){         
            item = {};                    
            item ["cont"]   = $(this).find("input[id*='ec_con']").val();
            DATAco.push(item);
        });
        INFOa  = new FormData();
        aInfoco   = JSON.stringify(DATAco);
    //========================================
     var filesadd = $("#table_mcr_files tbody > tr");
    //==============================================
        var DATAfi  = [];
        filesadd.each(function(){         
            item = {};                    
            item ["fil"]   = $(this).find("input[id*='ec_file']").val();
            DATAfi.push(item);
        });
        INFOa  = new FormData();
        aInfofi   = JSON.stringify(DATAfi);
    //========================================
    var envio=1;
    if(tipo==1){
        if(contactos.length>1){
            envio=0;
            toastr["error"]("No se permite mas de un contacto");
        }
    }
    if(contratos.length==0){
        envio=0;
        toastr["error"]("Seleccione uno o mas contratos");
    }
    if(contactos.length==0){
        envio=0;
        toastr["error"]("Seleccione uno o mas contactos");
    }

    if(envio==1){
        var comen=$('#mcr_coment').val();
        var asunto=$('#mcr_asucto').val();
        var datos='op='+op+'&tipo='+tipo+'&comen='+comen+'&contratos='+aInfoc+'&contactos='+aInfoco;
        $.ajax({
            type:'POST',
            url: base_url+"index.php/Mailenvio/generalenvio",
            data: {
                op : op,
                tipo : tipo,
                asunto:asunto,
                comen : comen,
                contratos : aInfoc,
                contactos : aInfoco,
                files:aInfofi,
                asi:m_asi,
                tipos:m_tipo,
                per:$('#firma_m option:selected').val(),
                perbbc:$('#bbc_m option:selected').val(),
            },
            success: function (response){
                console.log(response);
                var array = $.parseJSON(response);
                    codigocarga=array.newcodigo;
                    $('#codigocarga').val(codigocarga);
                if(array.tipo==1){
                    var ec_cel = $('#ec_cel').val();
                    var ec_tel = $('#ec_tel').val();
                    var numerotel='';
                    if(ec_cel!=''){
                        var numerotel=ec_cel;
                    }else if(ec_tel!=''){
                        var numerotel=ec_tel;
                    }
                    if(numerotel!=''){
                        numerotel = numerotel.replace(" ", "");
                        numerotel = numerotel.replace(" ", "");
                        numerotel = numerotel.replace(" ", "");
                        numerotel = numerotel.replace(" ", "");
                        numerotel = numerotel.replace(" ", "");
                        numerotel = numerotel.replace(" ", "");
                        numerotel = numerotel.replace(" ", "");
                        numerotel = numerotel.replace(" ", "");
                        numerotel = numerotel.replace(" ", "");
                        var urlwhat='https://wa.me/'+numerotel+'?text='+base_url+'consumibles_sol?id='+array.codigo+'';
                        console.log(urlwhat);
                        window.open(urlwhat, '_blank');
                    }else{
                        toastr["error"]("No se tiene identificado un numero");
                    }
                    
                }else{
                    //setTimeout(function(){ 
                    //    location.reload();
                    //}, 1000);
                }
                toastr["success"]("Enviado");
            },
            error: function(response){
                toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema");
                
            }
        });
    }
}
function upload_data_view(codigo){
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Calendario/upload_data_view",
        data: {
            code : codigo
        },
        success: function (response){
            console.log(response);
            var array = $.parseJSON(response);
            array.forEach(function(r){
                var html ='<tr class="idrowcc_'+idrowcc+'">';
                    html+='<td><input type="hidden" id="ec_file" value="'+r.id+'">'+r.file+'</td>';
                    html+='<td><a class="b-btn b-btn-danger" onclick="delete_mcr('+idrowcc+')"><i class="fas fa-trash-alt"></i></a></td>';
                    html+='</tr>';
                    $('.tabbodycr_files').append(html);
                    idrowcc++;
            });
        },
        error: function(response){
            toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema");
        }
    });
}
function obtenercontacto(cliente){
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Calendario/obtenercontactos",
        data: {
            cliente:cliente
        },
        success: function (response){
            $('.m_s_contac').html('<select class="browser-default form-control-bmz" id="ms_contacto">'+response+'</select>');
        },
        error: function(response){
            notificacion(1);   
        }
    });
    //=====================================================================
}
function obtenerbodymail(cliente){
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Calendario/obtenerbodymail",
        data: {
            cliente:cliente
        },
        success: function (response){
            console.log(response);
            var com=response.toString();
            console.log(com);
            $('#ms_coment2').val(com);
            $('#mcr_coment').val(com);
        },
        error: function(response){
            notificacion(1);   
        }
    });
    //=====================================================================
}
function add_contacto_cli(cli){
    $('#dc_cliente').val(cli);
    $('#modal_add_contacto').modal();
    $('#modal_add_contacto').modal('open');
}
function save_contacto(){
    $( ".save_contacto" ).prop( "disabled", true );
    setTimeout(function(){ 
         $(".save_contacto" ).prop( "disabled", false );
    }, 2000);
    var formdc=$('#form_add_cont');
    if(formdc.valid()){
        var datos= formdc.serialize();
        $.ajax({
            type:'POST',
            url: base_url+'Clientes/save_contacto',
            data: datos,
            statusCode:{
                404: function(data){
                    toastr["error"]("Error 404");
                },
                500: function(){
                    toastr["error"]("Error 500");
                }
            },
            success:function(data){
                toastr["success"]("Contacto agregado");
                formdc[0].reset();
                $('#modal_add_contacto').modal('close');
            }
        });
    }else{
        toastr["error"]("Verifique los campos requeridos");
    }
}
function clonarservicio(asignacion,tipo){
     $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Confirma que desea clonar el servicio al mes siguiente?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var cont_new=$('#even_contacto option:Selected').val();
                console.log(cont_new);
                     $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Calendario/clonarservicio",
                        data: {
                            asignacion:asignacion,
                            tipo:tipo
                        },
                        success: function (response){
                                toastr.success('Se a creado el servicio','Hecho!');
                                $('#modalinfocalendario').modal('close');
                        },
                        error: function(response){
                            notificacion(1);  
                        }
                    });
            },
            cancelar: function (){
                
            }
        }
    });
}
function passservicio(asig,tipo,cli){
    obtenercontacto_pass(cli);
    var html='';
        html+='¿Desea agregar PIN y enviarlo por email?<br>';
        html+='<div class="row">';
            html+='<div class="col s12">';
                html+='<label>Correo envio</label> <div class="pass_contac"></div>';
            html+='</div>';
            html+='<div class="col s12">';
                html+='<label>PIN</label> <input type="number" class="form-control-bmz" id="add_pin">';
            html+='</div>';
        html+='</div>';
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: html,
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var mail=$('#pass_contac option:selected').data('email');

                var pin=$('#add_pin').val();
                
                     $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Mailenvio/passservicio",
                        data: {
                            mail:mail,
                            pin:pin,
                            asig:asig,
                            tipo:tipo
                        },
                        success: function (response){
                                toastr.success('Se le a asignado PIN','Hecho!');
                        },
                        error: function(response){
                            notificacion(1);  
                             
                        }
                    });
            },
            cancelar: function () {
            }
        }
    });
}
function obtenercontacto_pass(cliente){
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Calendario/obtenercontactos",
        data: {
            cliente:cliente
        },
        success: function (response){
            $('.pass_contac').html('<select class="browser-default form-control-bmz" id="pass_contac">'+response+'</select>');
        },
        error: function(response){
            notificacion(1);
        }
    }); 
}
function desv_venta_ser(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿confirma desvincular la venta?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var mail=$('#pass_contac option:selected').data('email');

                var pin=$('#add_pin').val();
                
                     $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Calendario/desv_venta_ser",
                        data: {
                            id:id
                        },
                        success: function (response){
                                toastr.success('Venta desvinculada','Hecho!');
                                $('#modalinfocalendario').modal('close');
                        },
                        error: function(response){
                            notificacion(1);  
                             
                        }
                    });
            },
            cancelar: function () {
            }
        }
    });
}