<?php
require_once dirname(__FILE__) . '/TCPDF3/examples/tcpdf_include.php'; 
require_once dirname(__FILE__) . '/TCPDF3/tcpdf.php';

  //$logos = base_url().'public/img/alta.jpg';
  $logos = base_url().'app-assets/images/'.$configuracionCotizacion->logo1;
  $logos2 = base_url().'public/img/kyocera.jpg';
  //=========================================================================
    $rfc_selected='';
    $razon_social_selected='';
    $metodopago_text='';
    $formapago_text='';
    $uso_cfdi_text='';
    $telefono='';
    $estadoval='';
    $puesto_t='';
    $email_t='';
    $atencionpara_t='';

    foreach ($rfc_datos as $item) { 
      if($item->id==$rfc_id){
        $razon_social_selected=$item->razon_social;
        $rfc_selected=$item->rfc; 
        $cp =$item->cp;
        $estadoval=$item->estado;
        if($item->rfc=='XAXX010101000'){
          $razon_social_selected.='<br>'.$empresars;
        }
      }      
    }
    foreach ($metodopagorow as $item) {
      if($item->id==$metodopagoId){ 
        $metodopago_text=$item->metodopago_text;
      }
    }
    foreach ($formapagorow->result() as $item) {
      if($item->id==$formapago){ 
        $formapago_text=$item->formapago_text;
      }
    }
    foreach ($cfdirow->result() as $item) {
      if($item->id==$cfdi){ 
        $uso_cfdi_text=$item->uso_cfdi_text;
      }
    } 
    foreach ($resultadoclitel->result() as $item) {
      if($telId==$item->id){
        $telefono=$item->tel_local;
      }
    } 
    foreach ($cliente_datoscontacto->result() as $item) { 
      if($telId==$item->datosId){
        $telefono=$item->telefono;
      }
    }
    
    foreach ($estadorow->result() as $item) { 
        if($item->EstadoId==$estado){ 
          $estadoval = $item->Nombre;
        }
    }
    foreach ($cliente_datoscontacto->result() as $item) { 
      if($contactoId==$item->datosId){
        $puesto_t=$item->puesto;
        $email_t=$item->email;
        $atencionpara_t=$item->atencionpara;
      }
    }
    if($confif->ConfiguracionesId==1){
      $datosbancarios='Banco BANAMEX, Cuenta 7347132, Sucursal 826, CLABE 2668082673471320<br>
                Call Center 273 3400, 249 5393. www.kyoceraap.com';
    }else{
      $datosbancarios='Banco BANAMEX, Cuenta 5537012058, CLABE 002668055370120584<br>
                Call Center 246 466 3943. www.bodegademayoreo.com';
    }
  //=========================================================================
//====================================================
class MYPDF extends TCPDF {
 
  public function Header() {

      
      //$this->writeHTML($html, true, false, true, false, '');
  }
    // Page footer
  public function Footer() {
    $html2='';
    
      //<img src="http://facturacion33.adminfactura.com.mx/view/image/viamex_logob100.png" width="100px">
      //$this->writeHTML($html2, true, false, true, false, '');
      
  }
} 
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Kyocera');
$pdf->SetTitle('PROINPOL');
$pdf->SetSubject('factura');
$pdf->SetKeywords('factura');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins('7', '10', '7');
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin('10');

// set auto page breaks
$pdf->SetAutoPageBreak(true, 10);//PDF_MARGIN_BOTTOM

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('dejavusans', '', 7);
// add a page
$pdf->AddPage('P', 'A4');
$html='<style type="text/css">
          .t_c{
            text-align:center;
          }
          .t_b{
            font-weight: bold;
          }
          .bor_b{
            border-bottom: 1px solid black;
          }
          .bor_l{
            border-left: 1px solid black;
          }
          .bor_r{
            border-right: 1px solid black;
          }
          .bor_t{
            border-top: 1px solid black;
          }
          .tableb th{
              background-color:rgb(217,217,217);          
          }
        </style>';

  $html.='<table border="0">
            <tr>
              <td width="20%" class="t_c"><img src="'.$logos.'" width="80px"></td>
              <td width="60%" class="t_c">
                "'.$confif->Nombre.'"<br>
                RFC: '.$confif->Rfc.'<br>
                Impresoras y Multifuncionales, Venta, Renta y Mantenimiento.<br>
                Domicilio Fiscal: '.$confif->Calle.' '.$confif->Noexterior.' Col. '.$confif->Colonia.'. '.$confif->Municipio.', '.$confif->Estado.'. Mexico C.P.'.$confif->CodigoPostal.'<br>
                '.$datosbancarios.'

              </td>
              <td width="20%" class="t_c"><img src="'.$logos2.'" width="100px"></td>
            </tr>
          </table>
          <p></p>
          <table border="0" class="tableb" cellpadding="2">
            <tr>
              <th class="bor_b bor_l  bor_t" >Fecha: </th>
              <td class="bor_b  bor_t" >'.$dia_reg.'/'.$mes_reg.'/'.$ano_reg.'</td>
              <th class="bor_b  bor_t" >AC</th>
              <td class="bor_b  bor_t" >'.$ini_personal.'</td>
              <th class="bor_b  bor_t" >PROINPOL No.</th>
              <td class="bor_b  bor_t" >'.$proinven.'</td>
              <th class="bor_b  bor_t" ></th>
              <td class="bor_b  bor_r bor_t" ></td>
            </tr>
            <tr>
              <th class="bor_b bor_l">Cia.</th>
              <td class="bor_b  " colspan="5">'.$razon_social_selected.'</td>
              <th class="bor_b  ">RFC</th>
              <td class="bor_b  bor_r">'.$rfc_selected.'</td>
            </tr>
          </table>
          <table border="0" class="tableb" cellpadding="2">
            <tr>
              <td class="bor_b bor_l bor_r " >Vence:</td>
              <td class="bor_b  bor_r " >'.$vence.'</td>
              <td class="bor_b  bor_r" >Método de pago:</td>
              <td class="bor_b  bor_r " >'.$metodopago_text.'</td>
              <td class="bor_b  bor_r" >Forma de pago:</td>
              <td class="bor_b  bor_r " >'.$formapago_text.'</td>
              <td class="bor_b  bor_r" >Uso de CFDI:</td>
              <td class="bor_b  bor_r " >'.$uso_cfdi_text.'</td>
            </tr>
            <tr>
              <td class="bor_b bor_l  " >Tel:</td>
              <td class="bor_b   " colspan="4">'.$telefono.'</td>
              <td class="bor_b  " >Forma de cobro:</td>
              <td class="bor_b  bor_r " colspan="2">'.$formacobro.'</td>
            </tr>
            <tr>
              <td class="bor_b bor_l bor_r " colspan="4">
                <span class="t_b">Dirección de Entrega/Instalación:</span><br>
                ';
                  foreach ($polizaventadetalles->result() as $item) {
                    $html.=$item->direccionservicio;
                  }
                $html.='
              </td>
              <td class="bor_b  bor_r " colspan="4">
                <table >
                  <tr>
                    <td colspan="2" class="t_b">Dirección Fiscal:</td>
                  </tr>
                  <tr>
                    <td>Calle:</td><td>'.$calle.'</td>
                  </tr>
                  <tr>
                    <td>No. </td><td>'.$num_ext.'</td>
                  </tr>
                  <tr>
                    <td>Col.</td><td>'.$colonia.'</td>
                  </tr>
                  <tr>
                    <td>CD.</td><td>'.$municipio.'</td>
                  </tr>
                  <tr>
                    <td>Estado.</td><td>'.$estadoval.'</td>
                  </tr>
                  <tr>
                    <td>C.P.</td><td>'.$cp.'</td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
              <td class="bor_b bor_l  bor_r" colspan="3"><span class="t_b">Contacto:</span>'.$atencionpara_t.'</td>
              <td class="bor_b  bor_r" colspan="2"><span class="t_b">Cargo:</span>'.$puesto_t.'</td>
              <td class="bor_b  bor_r "colspan="3" ><span class="t_b">Email:</span>'.$email_t.'</td>
            </tr>
          </table>
          <p></p>';
          $totalgeneral=0;
            $html.='<table class="tableb" cellpadding="2" align="center">
                      <tr>
                        <th class="bor_b bor_l  bor_r bor_t" width="19%">DESCRIPCION</th>
                        <th class="bor_b bor_l  bor_r bor_t" width="15%">TIPO</th>
                        <th class="bor_b bor_l  bor_r bor_t" width="11%">MODELO</th>
                        <th class="bor_b bor_l  bor_r bor_t" width="11%">SERIE</th>
                        <th class="bor_b bor_l  bor_r bor_t" width="9%">INCLUYE</th>
                        <th class="bor_b bor_l  bor_r bor_t" width="9%">CANTIDAD</th>
                        <th class="bor_b bor_l  bor_r bor_t" width="14%">PRECIO UNITARIO</th>
                        <th class="bor_b bor_l  bor_r bor_t" width="12%">IMPORTE</th>
                      </tr>';
                    foreach ($polizaventadetalles->result() as $item) { 
                      $totalc = 0;
                      if($item->precio_local !==NULL) {
                        $totalc=$item->precio_local;
                      }elseif($item->precio_semi !==NULL) {
                        $totalc=$item->precio_semi;
                      }elseif($item->precio_foraneo !==NULL) {
                        $totalc=$item->precio_foraneo;
                      }elseif($item->precio_especial !==NULL) {
                        $totalc=$item->precio_especial;
                      }else{
                        $tipoprecio=1;
                      }
                      $totalgeneral=$totalgeneral+($totalc*$item->cantidad);
                      $html.='
                      <tr>
                        <td class="bor_b bor_l  bor_r bor_t">Servicio</td>
                        <td class="bor_b bor_l  bor_r bor_t">'.$item->nombre.'</td>
                        <td class="bor_b bor_l  bor_r bor_t">'.$item->modeloe.'</td>
                        <td class="bor_b bor_l  bor_r bor_t">'.$item->serie.'</td>
                        <td class="bor_b bor_l  bor_r bor_t">'.$item->cubre.'</td>
                        <td class="bor_b bor_l  bor_r bor_t">'.$item->cantidad.'</td>
                        <td class="bor_b bor_l  bor_r bor_t">$ '.number_format($totalc,2,'.',',').'</td>
                        <td class="bor_b bor_l  bor_r bor_t">$ '.number_format($totalc*$item->cantidad,2,'.',',').'</td>
                      </tr>';
                      $html.='<tr><td colspan="8" class="bor_b bor_l  bor_r bor_t">'.$item->comentario.'</td></tr>';
                    }
            $html.='<tr>
                        
                        <td colspan="6"></td>
                        <td class="bor_b bor_l  bor_r bor_t">Subtotal</td>
                        <td class="bor_b bor_l  bor_r bor_t">$ '.number_format($totalgeneral,2,'.',',').'</td>
                      </tr>
                      <tr>
                        
                        <td colspan="6"></td>
                        <td class="bor_b bor_l  bor_r bor_t">IVA</td>
                        <td class="bor_b bor_l  bor_r bor_t">$ '.number_format($totalgeneral*0.16,2,'.',',').'</td>
                      </tr>
                      <tr>
                        
                        <td colspan="6"></td>
                        <td class="bor_b bor_l  bor_r bor_t">Total</td>
                        <td class="bor_b bor_l  bor_r bor_t">$ '.number_format(round($totalgeneral*0.16, 2)+round($totalgeneral, 2),2,'.',',').'</td>
                      </tr>
                    </table> <p></p>';   
          
          
          $html.='<p></p><p></p>';

              $html.='<table class="tableb" cellpadding="2" align="center">
                        <tr>
                          <th class="bor_b bor_l  bor_r bor_t">Vigencia</th>
                          <th class="bor_b bor_l  bor_r bor_t">Visitas</th>
                        </tr>';
                foreach ($polizaventadetalles->result() as $item) { 
                $html.='<tr>
                          <td class="bor_b bor_l  bor_r bor_t">'.$item->vigencia_meses.'</td>
                          <td class="bor_b bor_l  bor_r bor_t">'.$item->cubre.'</td>
                        </tr>';
                }
                   $html.='</table>';
            

          $html.='<table class="bor_b bor_l  bor_r bor_t" cellpadding="2">
                  <tr><td>Observaciones</td></tr>
                  <tr><td>'.$observaciones.'</td></tr>
                  </table>';
$pdf->writeHTML($html, true, false, true, false, '');



$pdf->IncludeJS('print(true);');
if($viewcont==1){
  $file='p_'.$ventaId.'_2_'.$ano_reg.$mes_reg.$dia_reg;
      $strq="UPDATE polizasCreadas SET filuuid = '$file' WHERE id='$ventaId' and filuuid IS NULL";
      $this->db->query($strq);
    
    $pdf->Output(FCPATH.'uploads/ventas/'.$file.'.pdf', 'FI');
}else{
  $pdf->Output('Folios_.pdf', 'I');
}


?>