<?php

class Compras_model extends CI_Model 
{
    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
        $this->DB9 = $this->load->database('other9_db', TRUE);
    }
    
    function Getseries_compras($params){
        $fecha=$params['fecha'];
        $tipoc=$params['tipoc'];
        $columns = array( 
            0=>'sp.compraId',
            1=>'sp.folio',
            2=>'p.nombre',
            3=>'sp.reg',
            4=>'(SELECT sum(total) as total from ( 
    SELECT compraId, "1" as tabla, sum(precio) as total from series_compra_productos  WHERE activo=1 GROUP by compraId
    UNION 
    SELECT compraId, "2" as tabla, sum(precio) as total from series_compra_accesorios  WHERE activo=1  GROUP by compraId
    UNION 
    SELECT compraId, "3" as tabla, sum(cantidad*precio) as total from series_compra_refacciones  WHERE activo=1 GROUP by compraId
    UNION 
    SELECT compraId, "4" as tabla, sum(cantidad*precio) as total from compra_consumibles a WHERE activo=1 GROUP by compraId) as datos WHERE compraId=sp.compraId) as total'
        );
        $columnss = array( 
            0=>'sp.compraId',
            1=>'sp.folio',
            2=>'p.nombre',
            3=>'sp.reg',
            4=>'(SELECT sum(total) as total from ( 
    SELECT compraId, "1" as tabla, sum(precio) as total from series_compra_productos  WHERE activo=1 GROUP by compraId
    UNION 
    SELECT compraId, "2" as tabla, sum(precio) as total from series_compra_accesorios  WHERE activo=1  GROUP by compraId
    UNION 
    SELECT compraId, "3" as tabla, sum(cantidad*precio) as total from series_compra_refacciones  WHERE activo=1 GROUP by compraId
    UNION 
    SELECT compraId, "4" as tabla, sum(cantidad*precio) as total from compra_consumibles a WHERE activo=1 GROUP by compraId) as datos WHERE compraId=sp.compraId)',
            5=>'pros.serie',
            6=>'prose.modelo',
            7=>'sca.serie',
            8=>'ca.nombre',
            9=>'scr.serie',
            10=>'ref.nombre',
            11=>'con.modelo',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->DB9->select($select);
        $this->DB9->from('series_compra sp');
        $this->DB9->join('personal p', 'p.personalId=sp.personalId');

        $this->DB9->join('series_compra_productos pros', 'pros.compraId=sp.compraId', 'left');
        $this->DB9->join('equipos prose', 'prose.id=pros.productoid', 'left');

        $this->DB9->join('series_compra_accesorios sca', 'sca.compraId=sp.compraId', 'left');
        $this->DB9->join('catalogo_accesorios ca', 'ca.id=sca.accesoriosid', 'left');

        $this->DB9->join('series_compra_refacciones scr', 'scr.compraId=sp.compraId', 'left');
        $this->DB9->join('refacciones ref', 'ref.id=scr.refaccionid', 'left');

        $this->DB9->join('compra_consumibles cc', 'cc.compraId=sp.compraId', 'left');
        $this->DB9->join('consumibles con', 'con.id=cc.consumibleId', 'left');


        $this->DB9->where(array('sp.activo'=>1));
        if ($fecha!='') {
            $this->DB9->where('sp.reg >=',$fecha.' 00:00:00');
            $this->DB9->where('sp.reg <=',$fecha.' 23:59:59');
        }
        if($tipoc==1){
            $this->DB9->where(array('sp.completada'=>1));
        }
        if($tipoc==2){
            $this->DB9->where(array('sp.completada'=>0));
        }
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->DB9->group_start();
            foreach($columnss as $c){
                $this->DB9->or_like($c,$search);
            }
            $this->DB9->group_end();
            
        }
        $this->DB9->group_by("sp.compraId");            
        $this->DB9->order_by($columnss[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->DB9->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->DB9->get();
        // print_r($query); die;
        return $query;
    }
    public function TotalSeriecompras($params){
        $fecha=$params['fecha'];
        $tipoc=$params['tipoc'];
        $columns = array( 
            0=>'sp.compraId',
            1=>'sp.folio',
            2=>'p.nombre',
            3=>'sp.reg',
            4=>'(SELECT sum(total) as total from ( 
    SELECT compraId,"1" as tabla, sum(precio) as total from series_compra_productos  WHERE activo=1 GROUP by compraId
    UNION 
    SELECT compraId,"2" as tabla, sum(precio) as total from series_compra_accesorios  WHERE activo=1  GROUP by compraId
    UNION 
    SELECT compraId,"3" as tabla, sum(cantidad*precio) as total from series_compra_refacciones  WHERE activo=1 GROUP by compraId
    UNION 
    SELECT compraId,"4" as tabla, sum(cantidad*precio) as total from compra_consumibles a WHERE activo=1 GROUP by compraId) as datos WHERE compraId=sp.compraId) as total',
        );
        $columnss = array( 
            0=>'sp.compraId',
            1=>'sp.folio',
            2=>'p.nombre',
            3=>'sp.reg',
            4=>'(SELECT sum(total) as total from ( 
    SELECT compraId, "1" as tabla, sum(precio) as total from series_compra_productos  WHERE activo=1 GROUP by compraId
    UNION 
    SELECT compraId, "2" as tabla, sum(precio) as total from series_compra_accesorios  WHERE activo=1  GROUP by compraId
    UNION 
    SELECT compraId, "3" as tabla, sum(cantidad*precio) as total from series_compra_refacciones  WHERE activo=1 GROUP by compraId
    UNION 
    SELECT compraId, "4" as tabla, sum(cantidad*precio) as total from compra_consumibles a WHERE activo=1 GROUP by compraId) as datos WHERE compraId=sp.compraId)',
            5=>'pros.serie',
            6=>'prose.modelo',
            7=>'sca.serie',
            8=>'ca.nombre',
            9=>'scr.serie',
            10=>'ref.nombre',
            11=>'con.modelo',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->DB9->select('*');
        $this->DB9->from('series_compra sp');
        $this->DB9->join('personal p', 'p.personalId=sp.personalId');

        $this->DB9->join('series_compra_productos pros', 'pros.compraId=sp.compraId', 'left');
        $this->DB9->join('equipos prose', 'prose.id=pros.productoid', 'left');

        $this->DB9->join('series_compra_accesorios sca', 'sca.compraId=sp.compraId', 'left');
        $this->DB9->join('catalogo_accesorios ca', 'ca.id=sca.accesoriosid', 'left');

        $this->DB9->join('series_compra_refacciones scr', 'scr.compraId=sp.compraId', 'left');
        $this->DB9->join('refacciones ref', 'ref.id=scr.refaccionid', 'left');

        $this->DB9->join('compra_consumibles cc', 'cc.compraId=sp.compraId', 'left');
        $this->DB9->join('consumibles con', 'con.id=cc.consumibleId', 'left');

        $this->DB9->where(array('sp.activo'=>1));
        if ($fecha!='') {
            $this->DB9->where('sp.reg >=',$fecha.' 00:00:00');
            $this->DB9->where('sp.reg <=',$fecha.' 23:59:59');
        }
        if($tipoc==1){
            $this->DB9->where(array('sp.completada'=>1));
        }
        if($tipoc==2){
            $this->DB9->where(array('sp.completada'=>0));
        }
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->DB9->group_start();
            foreach($columnss as $c){
                $this->DB9->or_like($c,$search);
            }
            $this->DB9->group_end();
            
        }
        $this->DB9->group_by("sp.compraId");         
        //$this->db->select('COUNT(1) as total');
        //$this->db->from('series_compra sa');
        $query=$this->DB9->get();
        return $query->num_rows();
    }
    function getselectwherenall($table,$where){
        $this->DB9->select('*');
        $this->DB9->from($table);
        $this->DB9->where($where);
        $query=$this->DB9->get(); 
        return $query;
    }
    function getcomprasconsumibles($idcompra){
        $strq = 'SELECT compc.cconsumibleId,compc.consumibleId,con.modelo,con.parte,compc.cantidad,compc.recibidos ,compc.precio 
                FROM compra_consumibles as compc
                inner join consumibles as con on con.id=compc.consumibleId
                WHERE compc.compraId='.$idcompra; 
        $query = $this->DB9->query($strq);
        return $query;
    }
    function getcomprasrefaccionesss($idcompra){
        $strq = 'SELECT scr.serieId,scr.refaccionid,ref.codigo,ref.nombre,scr.cantidad,scr.entraron ,scr.precio,scr.factura
                from series_compra_refacciones as scr
                inner JOIN refacciones as ref on ref.id=scr.refaccionid
                WHERE scr.con_serie=0 AND scr.compraId='.$idcompra; 
        $query = $this->DB9->query($strq);
        return $query;
    }
    function getseries_compra_productos($idcompra){
        $strq = 'SELECT scp.*, equ.modelo
                from series_compra_productos as scp
                inner JOIN equipos as equ on equ.id=scp.productoid
                WHERE scp.activo=1 AND scp.compraId='.$idcompra; 
        $query = $this->DB9->query($strq);
        return $query;
    }
    function getseries_compra_accesorios($idcompra){
        $strq = 'SELECT sca.*, cata.nombre
                from series_compra_accesorios as sca
                inner JOIN catalogo_accesorios as cata on cata.id=sca.accesoriosid
                WHERE sca.activo=1  and sca.con_serie=1 AND sca.compraId='.$idcompra; 
        $query = $this->db->query($strq);
        return $query;
    }
    function getseries_compra_accesoriosss($idcompra){
        $strq = 'SELECT sca.*, cata.nombre
                from series_compra_accesorios as sca
                inner JOIN catalogo_accesorios as cata on cata.id=sca.accesoriosid
                WHERE sca.activo=1 and sca.con_serie=0 AND sca.compraId='.$idcompra; 
        $query = $this->DB9->query($strq);
        return $query;
    }
    function getseries_compra_refacciones($idcompra){
        $strq = 'SELECT scr.*, ref.nombre
                from series_compra_refacciones as scr
                inner JOIN refacciones as ref on ref.id=scr.refaccionid
                WHERE scr.activo=1 AND scr.con_serie=1 and  scr.compraId='.$idcompra; 
        $query = $this->DB9->query($strq);
        return $query;
    }
    function Getscompras_solicitud($params){
        
        $columns = array( 
            0=>'sof.id',
            1=>'sof.cantidad',
            2=>'ref.codigo',
            3=>'sof.idrefaccion',
            4=>'per.personalId',
            5=>'sof.estatus',
            6=>'sof.reg',
            7=>'ref.nombre refaccion',
            8=>'per.nombre personal',
            9=>'per.apellido_paterno',
            10=>'per.apellido_materno'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->DB9->select($select);
        $this->DB9->from('solicitud_refaccion sof');
        $this->DB9->join('refacciones ref', 'ref.id=sof.idrefaccion');
        $this->DB9->join('personal per', 'per.personalId=sof.personal');
        $this->DB9->where(array('sof.activo'=>1));
        
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->DB9->group_start();
            foreach($columns as $c){
                $this->DB9->or_like($c,$search);
            }
            $this->DB9->group_end();
            
        }            
        $this->DB9->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->DB9->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->DB9->get();
        // print_r($query); die;
        return $query;
    }
    public function Getscompras_solicitudt($params){
        $columns = array( 
            0=>'sof.id',
            1=>'sof.cantidad',
            2=>'ref.codigo',
            3=>'sof.idrefaccion',
            4=>'per.personalId',
            5=>'sof.estatus',
            6=>'sof.reg',
            7=>'ref.nombre refaccion',
            8=>'per.nombre personal',
            9=>'per.apellido_paterno',
            10=>'per.apellido_materno'
        );
    
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->DB9->select('COUNT(*) as total');
        $this->DB9->from('solicitud_refaccion sof');
        $this->DB9->join('refacciones ref', 'ref.id=sof.idrefaccion');
        $this->DB9->join('personal per', 'per.personalId=sof.personal');
        $this->DB9->where(array('sof.activo'=>1));

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->DB9->group_start();
            foreach($columns as $c){
                $this->DB9->or_like($c,$search);
            }
            $this->DB9->group_end();
            
        }        
        //$this->db->select('COUNT(1) as total');
        //$this->db->from('series_compra sa');
        $query=$this->DB9->get();
        return $query->row()->total;
    }
}

?>