var base_url = $('#base_url').val();
var table;
var id;
$(document).ready(function(){

    /*****  DATOS Y FUNCIONES PARA ASIGNAR DATOS A SERVICIOS ******** */ 
	$('.modal').modal();
	table = $('#tabla_asignar').DataTable();
	$('.asig_tec').click(function(event) {
		$('#modalAsigna').modal('open');
        var tr = $(this).closest('tr');
        var row = table.row(tr);
        var data = row.data();
        $("#id_asig").val(data.id);
	});

	$('.reagenda').click(function(event) {
		$('#modalReagenda').modal('open');
		var tr = $(this).closest('tr');
        var row = table.row(tr);
        var data = row.data();
		$("#id_asig").val(data.id);
	});

    $('#acepta_serv').click(function(event) {
        id_serv = $("#serv_selec option:selected").val();
        nueva_asignaForm(id_serv);
    });

    $('.iniciar').click(function(event) {
        var tr = $(this).closest('tr');
        var row = table.row(tr);
        var data = row.data();
        id = data.id;
    });

    $('.solicita').click(function(event) {
        $('#modal_addref').modal('open');   
    });

    $('.pdf').click(function(event) {
        var tr = $(this).closest('tr');
        var row = table.row(tr);
        var data = row.data();
        pdf_cotiza(data.id);
    });

    $('#tecnico').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una opción',
        ajax: {
            url: base_url+'Asignaciones/searchTecnicos',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.UsuarioID,
                        text: element.tecnico

                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    });


});

function reagenda(){
    var fecha = $("#fecha").val();
    var hora = $("#hora").val();
    var motivo = $("#motivo").val();
    var id = $("#id_asig").val();
    /*$.ajax({
      type: "POST",
      url: base_url+"index.php/Asignaciones/editar_fecha",
      data:{id:id,editar_fecha:editar_fecha,hora:hora,motivo:motivo},
      success: function(data){
        swal({ title: "Éxito",
            text: "Se reagendó el servicio",
            type: 'success',
            showCancelButton: false,
            allowOutsideClick: false,
        });
        setTimeout(function(){
        location.reload();
        },1000);
      }
    });*/
    swal({ title: "Éxito",
        text: "Se reagendó el servicio",
        type: 'success',
        showCancelButton: false,
        allowOutsideClick: false,
    });
}

function tecnico(){
    var tecnico = $("#tecnico").val();
    var id = $("#id_asig").val();
    /*$.ajax({
      type: "POST",
      url: base_url+"index.php/Asignaciones/editar_tecnico",
      data:{id:id,tecnico:tecnico},
      success: function(data){
        swal({ title: "Éxito",
            text: "Se asigno al nuevo técnico",
            type: 'success',
            showCancelButton: false,
            allowOutsideClick: false,
        });
        setTimeout(function(){
        location.reload();
        },1000);
      }
    });*/
    swal({ title: "Éxito",
        text: "Se asigno al nuevo técnico",
        type: 'success',
        showCancelButton: false,
        allowOutsideClick: false,
    });
}

function detalle(id){
    window.open(base_url+"Ordenes_asignadas/nuevaOrden/"+id, "Orden de Servicio", "width=780, height=612");
}

function nueva_asigna(){
    $('#modalNvo').modal('open');   
}
function nueva_asignaForm(id_serv){
    window.open(base_url+"Ordenes_asignadas/nuevaOrden/", "Nueva", "width=780, height=612");
}

function iniciar(status){
    if(status==1){
        $.confirm({
            boxWidth: '30%',
            useBootstrap: false,
            icon: 'fa fa-warning',
            title: 'Atención!',
            content: '¿Está seguro que desea iniciar este servicio?'+
             '<form action="" id="form_activa" class="formName">' +
                '<div class="form-group">' +
                '<label>Observaciones</label>' +
                '<input type="text" placeholder="Observaciones" class="name form-control" />' +
                '</div>' +
                '</form>',
            type: 'red',
            typeAnimated: true,
            buttons: 
            {
                confirmar: function () 
                {
                    $.ajax({
                        url: base_url+"index.php/Ordenes_asignadas/iniciar/"+id,
                        success: function (response) 
                        {
                            console.log(response);
                            if($.trim(response)=='"ok"')
                            {
                                $.alert({
                                    boxWidth: '30%',
                                    useBootstrap: false,
                                    title: 'Éxito!',
                                    content: 'Servicio iniciado!'});
                                //location.reload();
                            }
                            else
                            {
                                $.alert({
                                    boxWidth: '30%',
                                    useBootstrap: false,
                                    title: 'Error!',
                                    content: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema'}); 
                                 
                            }
                        },
                        error: function(response)
                        {
                            $.alert({
                                    boxWidth: '30%',
                                    useBootstrap: false,
                                    title: 'Error!',
                                    content: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema'});   
                            
                        }
                    });
                },
                cancelar: function () 
                {
                    
                }
            }
        });
    }else{
        $.alert({
            boxWidth: '30%',
            useBootstrap: false,
            title: 'Error!',
            content: 'Este servicio ya está iniciado'
        }); 
    }
}

function selected_newequipo(){
    var equipo= $('#selected_newequipo').val();
    $.ajax({
        type:'POST',
        url: base_url+'index.php/Rentas/obtenerconsumiblesrefacciones',
        data: {equipo:equipo},
        success:function(data){
            $('.nttbodya').html('');
            $('.nttbodyc').html('');
            console.log(data);
            var array = $.parseJSON(data);
            //accesorios
            //consumibles
            $('#selected_newconsumible').html(array.consumibles);
            $('#selected_newaccesorio').html(array.accesorios);
            $(".chosen-select").trigger("chosen:updated");
        }
    });
}
function temp(){
    if ($('#temp').is(':checked')) {
        $(".divtemp_reg").show( "slow" );
    }else{
        $(".divtemp_reg").hide( "slow" );
    }
}


var btn_acceso=0;
function addaccesorio(){
    var cantidad= $('#selected_newaccesorio_cantidad').val();
    var bodega = $('#selected_newaccesorio_b option:selected').val();
    var bodegatext = $('#selected_newaccesorio_b option:selected').text();
    var acceso = $('#selected_newaccesorio option:selected').val();
    var accesotext = $('#selected_newaccesorio option:selected').text();
    var html ='<tr class="addaccesoriorow_'+btn_acceso+'">\
                  <td><input id="cantidad" value="'+cantidad+'" readonly style="border-bottom: 0px;width: 50px;"></td>\
                  <td>\
                  <input id="accesorio" value="'+acceso+'" readonly style="display:none">\
                  '+accesotext+'\
                  </td>\
                  <td>\
                  <input id="bodega" value="'+bodega+'" readonly style="display:none">\
                  '+bodegatext+'\
                  </td>\
                  <td>\
                    <a class="btn-floating waves-effect waves-light red darken-1" onclick="deleteaccesorio('+btn_acceso+')">\
                      <i class="material-icons">delete_forever</i>\
                    </a>\
                  </td>\
                </tr>';
    $('.nttbodya').append(html);
    btn_acceso++;
}
function deleteaccesorio(row){
    $('.addaccesoriorow_'+row).remove();
}

function pdf_cotiza(){
    
}