<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class ControlFolios extends CI_Controller {
    public function __construct()     {
        parent::__construct();
        date_default_timezone_set('America/Mexico_City');
        $this->load->model('Clientes_model', 'model');
        $this->load->model('Estados_model', 'estados');
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloControlfolios');
        $this->load->model('Configuraciones_model');
        $this->load->helper('url');
        $this->idpersonal = $this->session->userdata('idpersonal');
        $this->fechaactual=date('Y-m-d H:i:s');
        $this->submenu=48;
    }

    // Listado de Empleados
    function index()     {
        $data['MenusubId']=$this->submenu;
        if($this->session->userdata('logeado')==true){   
            //$data['resultstec']=$this->Configuraciones_model->searchTecnico(); 
            $data['resultstec']=$this->Configuraciones_model->view_session_searchTecnico(); 
            $data['fechainicial_reg'] = date("Y-m-d",strtotime($this->fechaactual."- 6 month")); 
            $this->load->view('header');
            $this->load->view('main');
            $this->load->view('almacen/contratofolio/listado',$data);
            $this->load->view('almacen/contratofolio/jslistado');
            $this->load->view('footer');
        }
        else{
            redirect('login');
        }
    }
    function getListadocontratofolio(){
        $params = $this->input->post('tecnico'); 
        $registro = $this->ModeloControlfolios->getlistcontratofolio($params);
        $json_data = array("data" => $registro);
        echo json_encode($json_data);
    }
    public function getListadocontratofolio2() {
        $params     = $this->input->post();
        $getdata    = $this->ModeloControlfolios->getlistcontratofolio($params);
        $totaldata  = $this->ModeloControlfolios->getlistcontratofoliot($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    function viewetiqueta($id){
        $data['etiqueta'] = $this->ModeloCatalogos->getlistcontratofolioid($id);
        $this->load->view('Reportes/etiqueta',$data);
    }
    function viewetiquetas(){
        //$data['etiqueta'] = $this->ModeloCatalogos->getlistcontratofolioid($id);
        $this->load->view('Reportes/etiquetas');
    }
    function updatstatusfolio(){
        $id = $this->input->post('id');
        $this->ModeloCatalogos->updateCatalogo('contrato_folio',array('status' => 1 ),array('idcontratofolio' => $id));
    }
    function updatstatusfolios(){
        $ids = $this->input->post('idsfolio');
        $DATAf = json_decode($ids);
        for ($i=0;$i<count($DATAf);$i++) {
            $this->ModeloCatalogos->updateCatalogo('contrato_folio',array('status' => 1 ),array('idcontratofolio' => $DATAf[$i]->folio));
        }
    }
}