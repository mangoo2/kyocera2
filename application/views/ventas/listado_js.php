<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/plugins/toastr/toastr.css">
<script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/toastr/toastr.min.js" ></script>
<!--<script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/swich/on-off-switch.js?v=<?php echo date('Ymd');?>" ></script>
<script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/swich/on-off-switch-onload.js?v=<?php echo date('Ymd');?>" ></script>
<link href="<?php echo base_url(); ?>public/plugins/swich/on-off-switch.css" type="text/css" rel="stylesheet">-->
<link href="<?php echo base_url(); ?>public/plugins/switcher/switcher.css" rel="stylesheet">
<script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/switcher/jquery.switcher.js?v=<?php echo date('Ymd');?>" ></script>


<link href="<?php echo base_url(); ?>public/plugins/select2/select2.min.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/select2/select2.full.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/plugins/touchspin/jquery.bootstrap-touchspin.min.css">
<script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/touchspin/jquery.bootstrap-touchspin.min.js" ></script>

<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/responsive.dataTables/responsive.dataTables.min.css">
<script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/responsive.dataTables/dataTables.responsive.min.js"></script>-->

<script type="text/javascript" src="<?php echo base_url(); ?>public/js/listado_ventas_incompletas.js?v=<?php echo date('YmdGi');?>" ></script>
<script type="text/javascript" src="<?php echo base_url(); ?>public/js/listado_polizasCreadas2.js?v=<?php echo date('YmdGi');?>" ></script>
<script type="text/javascript" src="<?php echo base_url(); ?>public/js/addfacturaexterna.js?v=<?php echo date('YmdHGis');?>" ></script>
<?php if(isset($_GET['cli'])){ ?>
<script type="text/javascript">
	$(document).ready(function($) {
		setTimeout(function(){ 
			$('#tabla_ventas_incompletas_filter input').val("<?php echo $_GET['cli']?>").keyup();
		}, 2000);
	});
</script>
<?php } ?>
<script type="text/javascript">
	$(document).ready(function($) {
		setTimeout(function(){ 
			<?php foreach ($lispagosvp->result() as $item) { ?>
				$('#id_venta_p').val(<?php echo $item->id;?>);
    			$('#tipo_p').val(0);
    			$('.monto_prefactura').html('$ <span class="totalpoliza">'+monto_ventas(<?php echo $item->id;?>)+'</span>');
				table_tipo_compra(<?php echo $item->id;?>,0);
				setTimeout(function(){ 
        			calcularrestante();
    			}, 1000);
			<?php } ?>

		}, 1000);
	});
</script>
<?php if(isset($_GET['viewidpre'])){ 
?>
<script type="text/javascript">
	$(document).ready(function($) {
		setTimeout(function(){ 
			table.search(<?php echo $_GET['viewidpre'];?>).draw();
		}, 1000);
		setTimeout(function(){ 
			//load();
		}, 2000);
	});
</script>
<?php } ?>

<?php 
	foreach ($lispagosvcp->result() as $item) {
		if($item->ImpPagado >= $item->total_general){
			$combinadaId = $item->combinadaId;
			$this->ModeloCatalogos->updateCatalogo('ventacombinada',array('estatus'=>3),array('combinadaId'=>$combinadaId));
		}
	}
?>
