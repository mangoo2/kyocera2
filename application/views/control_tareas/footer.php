            
        <!---- --->
        </div>
      <!-- END WRAPPER -->
    </div>
    <!-- END MAIN -->
    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- START FOOTER -->
<!-- ================================================
    Scripts
    ================================================ -->
    
    <!--materialize js-->
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/js/materialize.min.js"></script>
    <!--scrollbar-->
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <!--plugins.js - Some Specific JS codes for Plugin Settings-->
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/js/plugins.js"></script>
    <!--custom-script.js - Add your own theme custom JS--
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/js/custom-script.js"></script>-->
    <!-- chartist -->
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/jquery-validation/jquery.validate.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/jquery-validation/additional-methods.min.js"></script>
    <!--plugins.js - Some Specific JS codes for Plugin Settings-->
    <!--<script type="text/javascript" src="<?php echo base_url(); ?>app-assets/js/plugins.js"></script>-->
    <!--form-validation.js - Page Specific JS codes-->
    <!--<script type="text/javascript" src="<?php echo base_url(); ?>app-assets/js/scripts/form-validation.js"></script>-->
    <!--sweetalert -->
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/sweetalert/dist/sweetalert.min.js"></script>
    <!--extra-components-sweetalert.js - Some Specific JS-->
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/js/scripts/extra-components-sweetalert.js"></script>
    <!--<script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/data-tables/js/jquery.dataTables.min.js"></script>-->
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/data-tables/js/jquery.dataTables.min.1.11.4.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/data-tables/js/dataTables.responsive.min.2.2.9.js"></script>
    <!-- dropify -->
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/dropify/js/dropify.min.js"></script>
    <!-- jquery confirm -->
    <script src="<?php echo base_url(); ?>app-assets/vendors/confirm/jquery-confirm.min.js"></script>
    <!-- JS Export -->
    <script src="<?php echo base_url(); ?>app-assets/vendors/exportJS/export.js"></script>
    <script src="<?php echo base_url(); ?>app-assets/vendors/exportJS/export.min.js"></script>
    <!-- Full Calendar -->
    <!--<script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/fullcalendar/lib/jquery-ui.custom.min.js"></script>-->
    <!--<script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/fullcalendar/lib/moment.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/fullcalendar/js/fullcalendar.min.js"></script>
    <!--<script type="text/javascript" src="https://fullcalendar.io/releases/list/4.1.0/main.min.js"></script>-->
    
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/fullcalendar/lib/moment.min.js?v=1"></script>

    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/fullcalendar_6.1.9/index.global.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/fullcalendar_6.1.9/locales-all.global.min.js"></script>
    

   
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/datatable-Editable/js/dataTables.editor.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/datatable-Editable/js/dataTables.editor.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/datatable-Editable/js/editor.bootstrap.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/datatable-Editable/js/editor.bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/datatable-Editable/js/editor.bootstrap4.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/datatable-Editable/js/editor.bootstrap4.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/datatable-Editable/js/editor.foundation.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/datatable-Editable/js/editor.foundation.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/datatable-Editable/js/editor.jqueryui.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/datatable-Editable/js/editor.jqueryui.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/datatable-Editable/js/editor.semanticui.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/datatable-Editable/js/editor.semanticui.min.js"></script>

    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/data-tables/js/dataTables.select.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/data-tables/js/dataTables.buttons.min.js"></script>

    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/js/jquery.jeditable.js"></script>

    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/js/chosen.jquery.js" ></script>
    <!--ckeditor js-->
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/js/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/js/ckeditor/adapters/jquery.js"></script>
    <!-- datatable buttons 2.2.2 -->
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/data-tables/Buttons.2.2.2/js/dataTables.buttons.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/data-tables/Buttons.2.2.2/js/buttons.html5.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/data-tables/Buttons.2.2.2/js/buttons.print.min.js"></script>

    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/data-tables/Buttons.2.2.2/libs/jszip.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/data-tables/Buttons.2.2.2/libs/pdfmake.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/data-tables/Buttons.2.2.2/libs/vfs_fonts.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/data-tables/js/input.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/data-tables/js/select.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/toastr/toastr.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/plugins/toastr/toastr.css">
    <script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/loading/jquery.loading.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/plugins/loading/demo.css">
    <script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/MaskFocus/MaskedPassword.js"></script>
    
    <script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/mask/jquery.mask.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/mask/jquery.passwordify.js"></script>
    
    <script type="text/javascript" src="<?php echo base_url(); ?>public/js/servicioscal.js?v=<?php echo date('Ymd');?>"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>public/js/notificacion.js?v=<?php echo date('Ymd');?>"></script>
    <style type="text/css">
        #content{
            margin-top: 0px !important;
        }
        .section{
            padding-top: 0px;
        }
    </style>
     
  </body>
</html>
