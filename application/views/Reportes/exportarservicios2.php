<?php 
	if(isset($_GET['viewexcel'])){
		$viewexcel=1;
		$tiempo_inicio = microtime(true);
	}else{
		header("Pragma: public");
		header("Expires: 0");
		$filename = "Servicios.xls";
		header("Content-type: application/x-msdownload");
		header("Content-Disposition: attachment; filename=$filename");
		header("Pragma: no-cache");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		$viewexcel=0;
	}
	date_default_timezone_set('America/Mexico_City');
    $this->fechahoy = date('Y-m-d G:i:s');
    $this->fechaactual = date('Y-m-d');
    $this->horaactual = date('G:i:s');
    
?>
<table border="1">
	<thead>
		<tr>
			<!--<th>Tipo Servicio</th>
			<th>No. Servicio</th>-->
			<th>Fecha</th>
			<th>Cliente</th>
			<th>Modelo</th>
			<th>Serie</th>
			<th>Hora inicio</th>
			<th>Hora fin</th>
			<th>Diferencia</th>
			<th></th>
			<th></th>
			<th>Servicio</th>
			<th><?php echo utf8_decode('Producción promedio');?></th>
			<th></th>
			<th>Tecnico</th>
			<th>Comentarios</th>
			<th>Garantia</th>
			<th>Horario</th>
			
		</tr>
	</thead>
	<tbody>
		<?php 
			$params['tiposer']=$tiposer;
			$params['tecnico']=$tecnico;
			$params['fechainicio']=$fechainicio;
			$params['fechafin']=$fechafin;
			$params['pcliente']=$pcliente;
			$params['status']=$status;
			$params['zona']=$zona;
			$row=1;
			if($tipo==1 or $tipo==0){//contrato
				$datos=$this->ModeloServicio->getlistaservicioc_reporte($params);
				$asignacionId_consu=0;
				$html='';
				foreach ($datos->result() as $item) { 
					$asignacionId = $item->asignacionId;
					
					//$direccionequipo= $this->ModeloCatalogos->getdireccionequipos($item->idequipo,$item->serieId);
					/*
					$namestatus='';
					if($item->status==2){
						$namestatus='finalizado';
					}elseif($item->status==1){
						$namestatus='proceso';
					}elseif($item->status==0){
						if(strtotime($item->fecha)<strtotime($this->fechaactual)){
							$namestatus='Sin atencion';
						}elseif(strtotime($item->fecha)>strtotime($this->fechaactual)){
							$namestatus='En espera';
						}elseif(strtotime($this->fechaactual)==strtotime($item->fecha)){
							$namestatus='En espera';
							if($this->horaactual>='17:00:00'){
								$namestatus='Sin atencion';
							}
						}
					}
					*/
                    
					
					$html.='<tr class="t_1 '.$item->asignacionId.' '.$row.'">';
						//$html.='<td>Contrato</td>';
						//$html.='<td>'.$item->asignacionId.'</td>';
						$html.='<td>'.$item->fecha.'</td>';
						$html.='<td>'.$item->empresa.' ('.$item->folio.')'.'</td>';
						$html.='<td>'.$item->modelo.'</td>';
						$html.='<td>'.$item->serie.'</td>';
						$html.='<td>'.$item->horaserinicio.'</td>';
						$html.='<td>'.$item->horaserfin.'</td>';
						$html.='<td>';
							if($item->horaserinicio!='' && $item->horaserfin!=''){
								$hora1 = new DateTime($item->horaserinicio);
								$hora2 = new DateTime($item->horaserfin);
								// Calcular la diferencia
								$diferencia = $hora1->diff($hora2);

								// Mostrar la diferencia en formato horas y minutos
								$html.=$diferencia->format('%H:%I');
							}	
						$html.='</td>';
						$html.='<td>'.utf8_decode($item->comentariocal).'</td>';
						$html.='<td>'.utf8_decode($item->tserviciomotivo).'</td>';
						
						$html.='<td>';
							$html.=$item->poliza;
							if($item->idserpexs>0){
								$html.=' Garantia';
							}
						$html.='</td>';
						$html.='<td>';
									$promedio_prod=0;
									if($item->fecha==date('Y-m-d')){
										//$promedio_prod=$this->Rentas_model->produccionpromedio($item->idequipo,$item->serieId,1,'','');
										$promedio_prod=round($item->promedio_prod);
									
									}
									$promedio_prod=$item->promedio_prod;
									$html.=round($promedio_prod);
						$html.='</td>';
						$html.='<td>';
							if($asignacionId_consu!=$asignacionId){
								$asignacionId_consu=$asignacionId;
											                    //=============================================
									/*
									if($item->fecha==date('Y-m-d')){ // se coloco asi para aligerar los rangos largos
				                        $resultado=$this->ModeloAsignacion->getdatosrenta($asignacionId,'',0,0);
				                        foreach ($resultado->result() as $item0) {
				                            $datosconsumiblesfolio = $this->ModeloServicio->getfoliosservicios($asignacionId,$item0->serieId);
				                            
				                            //log_message('error','$serid '.$serid.''.$item0->serieId.' datos:'.$datosconsumiblesfolio->num_rows());
				                            foreach ($datosconsumiblesfolio->result() as $item1) {
				                                    $html.=''.$item1->modelo.' '.$item1->foliotext.' / ';
				                            }
				                        }
			                    	}
			                    	*/
			                        //============================================ esto aplica para las optciones 1 2 3
			                            /*
			                            $datosres_equi = $this->ModeloCatalogos->obtenerproducventavinculada($asignacionId,1);
			                            foreach ($datosres_equi->result() as $itemre) {
			                                $productos.='Cant: '.$itemre->cantidad.' '.$itemre->modelo.' '.$itemre->serie.' / ';
			                            }
			                            */
			                            
			                            //$html.=''.$item->productosadds.$item->productosadds2.$item->productosadds3.$item->productosadds4;//se comenta por que sobrecarga la consulta
			                        //============================================
			                    //=============================================
							}
						$html.='</td>';
						$html.='<td>';  
							if($item->per_fin>0){
								$html.= utf8_decode($item->tecnicofin);
							}else{
								$html.= utf8_decode($item->tecnico).'<br>'.utf8_decode($item->tecnico2);
							}
						$html.='</td>';
						$html.='<td>'.utf8_decode($item->comentario).' '.utf8_decode($item->comentario_tec).'</td>';
						$html.='<td>'; 
							if($item->idserpexs>0){
								$html.=$this->ModeloAsignacion->obtenertecnicodelservicio($item->idserpexs,$item->tipopexs);
							}
						$html.='</td>';
						
							if($item->horaserinicioext!='' && $item->horaserinicioext!='null' && $item->horaserinicioext!=null){
								$html.='<td>Manual</td>';
							}else{
								$html.='<td>Automatico</td>';
							}
					$html.='</tr>';
						$row++;
				}
				echo $html;
				
			}
			if($tipo==2 or $tipo==0){//poliza
				$datos=$this->ModeloServicio->getlistaserviciop_reporte($params);
				$asignacionId_consu=0;
				foreach ($datos->result() as $item) { 
					$polizaId=$item->polizaId;
					$asignacionId = $item->asignacionId;
					
					?>
					<tr class="t_2 <?php echo $item->asignacionId;?> <?php echo $row;?> ">
						<!--<td>Poliza</td>
						<td><?php echo $item->asignacionId;?></td>-->
						<td><?php echo $item->fecha;?></td>
						<td><?php echo $item->empresa;?></td>
						<td><?php echo $item->modelo;?></td>
						<td><?php echo $item->serie;?></td>
						<td><?php echo $item->hora;?></td>
						<td><?php echo $item->horafin;?></td>
						<td><?php
							if($item->hora!='' && $item->horafin!=''){
								$hora1 = new DateTime($item->hora);
								$hora2 = new DateTime($item->horafin);
								// Calcular la diferencia
								$diferencia = $hora1->diff($hora2);

								// Mostrar la diferencia en formato horas y minutos
								echo $diferencia->format('%H:%I');
							}	
						?></td>
						<td><?php echo utf8_decode($item->comentariocal);?></td>
						<td><?php echo utf8_decode($item->tserviciomotivo);?></td>
						<td><?php 
							echo $item->servicio;
							if($item->idserpexs>0){
								echo ' Garantia';
							}
							?></td>
						<td style="background-color: grey;"></td>
						<td><?php 
							
							$productos='';
							if($asignacionId_consu!=$item->asignacionId){
								$asignacionId_consu=$item->asignacionId;
								/*
								$productos.='<br>**'.$polizaId.'**<br>';
								$dat_pol_eq=$this->ModeloServicio->obtenerproductospoliza($polizaId);
			                    foreach ($dat_pol_eq->result() as $item_pe) {
			                        $productos.=$item_pe->modelo.' '.$item_pe->serie.' / ';
			                    }
			                    $productos.='<br>****<br>';
			                    */
			                    //$productos.=''.$item->op_pro.$item->op_pro2.$item->op_pro3.$item->op_pro4.$item->op_pro5.'';//se comenta por que sobrecarga la consulta
			                    //============================================ esto aplica para las optciones 1 2 3
			                        /*
			                        $datosres_equi = $this->ModeloCatalogos->obtenerproducventavinculada($asignacionId,2);
			                        foreach ($datosres_equi->result() as $itemre) {
			                            $productos.='Cant: '.$itemre->cantidad.' '.$itemre->modelo.' '.$itemre->serie.' / ';
			                        }
			                        */
			                        //$productos.=''.$item->productosadds.$item->productosadds2.$item->productosadds3.$item->productosadds4;//se comenta por que sobrecarga la consulta
			                    //============================================
							}
							echo $productos;
							?></td>
						<td><?php 
								if($item->per_fin>0){
									echo utf8_decode($item->tecnicofin);
								}else{
									echo utf8_decode($item->tecnico).'<br>'.utf8_decode($item->tecnico2);
								}
							?></td>
						<td><?php echo utf8_decode($item->comentario);?> <?php echo utf8_decode($item->comentario_tec);?></td>
						<td><?php 
							if($item->idserpexs>0){
								echo $this->ModeloAsignacion->obtenertecnicodelservicio($item->idserpexs,$item->tipopexs);
							}
						?></td>
						<?php 
							if($item->horaserinicioext!='' && $item->horaserinicioext!='null' && $item->horaserinicioext!=null){
								echo '<td>Manual</td>';
							}else{
								echo '<td>Automatico</td>';
							}
						?>
						
					</tr>
					<?php $row++;?>
				<?php }

			}
			if($tipo==3 or $tipo==0){//evento
				$datos=$this->ModeloServicio->getlistaserviciocl_reporte($params);
				$asignacionId_consu=0;
				foreach ($datos->result() as $item) { 
					$asignacionId = $item->asignacionId;
					/*
					$namestatus='';
					if($item->status==2){
						$namestatus='finalizado';
					}elseif($item->status==1){
						$namestatus='proceso';
					}elseif($item->status==0){
						if(strtotime($item->fecha)<strtotime($this->fechaactual)){
							$namestatus='Sin atencion';
						}elseif(strtotime($item->fecha)>strtotime($this->fechaactual)){
							$namestatus='En espera';
						}elseif(strtotime($this->fechaactual)==strtotime($item->fecha)){
							$namestatus='En espera';
							if($this->horaactual>='17:00:00'){
								$namestatus='Sin atencion';
							}
						}
					}
					*/
					?>
					<tr class="t_3 <?php echo $item->asignacionId;?> <?php echo $row;?>">
						<!--<td>Cliente</td>
						<td><?php echo $item->asignacionId;?></td>-->
						<td><?php echo $item->fecha;?></td>
						<td><?php echo $item->empresa;?></td>
						<td><?php echo $item->modelo;?></td>
						<td><?php echo $item->serie;?></td>
						<td><?php echo $item->horaserinicio;?></td>
						<td><?php echo $item->horafin;?></td>
						<td><?php
							if($item->horaserinicio!='' && $item->horaserfin!=''){
								$hora1 = new DateTime($item->horaserinicio);
								$hora2 = new DateTime($item->horaserfin);
								// Calcular la diferencia
								$diferencia = $hora1->diff($hora2);

								// Mostrar la diferencia en formato horas y minutos
								echo $diferencia->format('%H:%I');
							}	
						?></td>
						<td><?php echo utf8_decode($item->comentariocal);?></td>
						<td><?php echo utf8_decode($item->tserviciomotivo);?></td>
						<td><?php 
							echo $item->servicio;
							echo $item->poliza;
							if($item->idserpexs>0){
								echo ' Garantia';
							}
							?></td>
						<td style="background-color: grey;"></td>
						<td><?php
							
							$productos='';
							if($asignacionId_consu!=$item->asignacionId){
								$asignacionId_consu=$item->asignacionId;
								//============================================ esto aplica para las optciones 1 2 3
								/* //se comenta por que sobrecarga la consulta
		                        $datosres_equi = $this->ModeloCatalogos->obtenerproducventavinculada($asignacionId,3);
		                        $productos='';
		                        foreach ($datosres_equi->result() as $itemre) {
		                            $productos.='Cant: '.$itemre->cantidad.' '.$itemre->modelo.' '.$itemre->serie.' / ';
		                        }
		                        */
		                    //====================================================
							}
							echo $productos;
						?></td>
						<td><?php
							if($item->idtecnico_ex>0){
								echo utf8_decode($item->tecnico_ex);
							}else{
								if($item->per_fin>0){
									echo utf8_decode($item->tecnicofin);
								}else{
									echo utf8_decode($item->tecnico).'<br>'.utf8_decode($item->tecnico2);
								} 
							}
							
						?></td>
						<td><?php echo utf8_decode($item->comentario);?> <?php echo utf8_decode($item->comentario_tec);?></td>
						<td><?php 
							if($item->idserpexs>0){
								echo  $this->ModeloAsignacion->obtenertecnicodelservicio($item->idserpexs,$item->tipopexs);
							}
						?></td>
						<?php 
							if($item->horaserinicioext!='' && $item->horaserinicioext!='null' && $item->horaserinicioext!=null){
								echo '<td>Manual</td>';
							}else{
								echo '<td>Automatico</td>';
							}
						?>
					</tr>
					<?php $row++;?>
				<?php }
			}
			if($tipo==4 or $tipo==0){//ventas
				$datos=$this->ModeloServicio->getlistaserviciovent_reporte($params);
				foreach ($datos->result() as $item) { 
					/*
					$namestatus='';
					if($item->status==2){
						$namestatus='finalizado';
					}elseif($item->status==1){
						$namestatus='proceso';
					}elseif($item->status==0){
						if(strtotime($item->fecha)<strtotime($this->fechaactual)){
							$namestatus='Sin atencion';
						}elseif(strtotime($item->fecha)>strtotime($this->fechaactual)){
							$namestatus='En espera';
						}elseif(strtotime($this->fechaactual)==strtotime($item->fecha)){
							$namestatus='En espera';
							if($this->horaactual>='17:00:00'){
								$namestatus='Sin atencion';
							}
						}
					}
					*/	

					?>
					<tr class="t_4 <?php echo $item->asignacionId;?> <?php echo $row;?>">
						<!--<td>Ventas</td>
						<td><?php echo $item->asignacionId;?></td>-->
						<td><?php echo $item->fecha;?></td>
						<td><?php echo $item->empresa;?></td>
						<td><?php echo $item->modelo;?></td>
						<td><?php echo $item->serie;?></td>
						<td><?php echo $item->hora;?></td>
						<td><?php echo $item->horafin;?></td>
						<td><?php
							if($item->hora!='' && $item->horafin!=''){
								$hora1 = new DateTime($item->hora);
								$hora2 = new DateTime($item->horafin);
								// Calcular la diferencia
								$diferencia = $hora1->diff($hora2);

								// Mostrar la diferencia en formato horas y minutos
								echo $diferencia->format('%H:%I');
							}	
						?></td>
							
						<td><?php echo utf8_decode($item->comentariocal);?></td>
						<td><?php echo utf8_decode($item->tserviciomotivo);?></td>
						<td><?php 
							echo $item->servicio;
							if($item->idserpexs>0){
								echo ' Garantia';
							}
							?></td>
						<td style="background-color: grey;"></td>
						<td></td>
						<td><?php 
							if($item->per_fin>0){
								echo utf8_decode($item->tecnicofin);
							}else{
								echo utf8_decode($item->tecnico).'<br>'.utf8_decode($item->tecnico2);
							} 
						?></td>
						<td><?php echo utf8_decode($item->comentario);?> <?php echo utf8_decode($item->comentario_tec);?></td>
						<td><?php 
							if($item->idserpexs>0){
								echo  $this->ModeloAsignacion->obtenertecnicodelservicio($item->idserpexs,$item->tipopexs);
							}
						?></td>
						<?php 
							if($item->horaserinicioext!='' && $item->horaserinicioext!='null' && $item->horaserinicioext!=null){
								echo '<td>Manual</td>';
							}else{
								echo '<td>Automatico</td>';
							}
						?>
					</tr>
					<?php $row++;?>
				<?php }
			}
			if($viewexcel==1){
				// Fin del cronómetro
				$tiempo_fin = microtime(true);
				$tiempo_total = $tiempo_fin - $tiempo_inicio;

				echo "El script tardó " . $tiempo_total . " segundos en ejecutarse.";
			}
			
		?>
	</tbody>
</table>