<style type="text/css">
  .serie_instalada ,
  .serie_retirada,
  .fecha_tramite,
  .fecha_envio{
    width: 120px !important;
  }
  .folio_rma_rechazo{
    display: none;
  }
  .folio_rma_text[readonly] {
    background-color: #fa4545 !important;
  }
  .folio_rma_text[readonly]::placeholder {
    color: white;
  }
  .folio_rma_text::placeholder{
    color: transparent;
    text-align: center;
  }
  #tableg td{
    font-size: 12px;
  }
  .bloqueado .folio_rma_text[readonly] {
    background-color: #e9ecef !important;
  }
</style>
<!-- START CONTENT -->
    <section id="content">
      <!--start container-->
        <div class="container">
          <div class="section">

          <input id="base_url" type="hidden" name="base_url" value="<?php echo base_url(); ?>">

          <div id="table-datatables">
            <br>
                <h4 class="header">Lista de garantias</h4>
                <div class="row">
                  <div class="col s12">
                    <div class="card-panel">
                      <div class="row">
                        <div class="col s2">
                          <label>Tipo</label>
                          <select class="form-control-bmz browser-default" id="tipog" onchange="load()">
                            <option value="1">Equipo</option>
                            <option value="2">Refaccion</option>
                            <option value="3">Accesorios</option>
                          </select>
                        </div>
                        <div class="col s2">
                          <label>Estatus</label>
                          <select class="form-control-bmz browser-default" id="estatus" onchange="load()">
                            <option value="0">Todos</option>
                            <option value="1">Sin proceso</option>
                            <option value="2">En proceso</option>
                            <option value="3">Finalizado</option>
                          </select>
                        </div>
                        <div class="col s12">
                          <table class="table striped" id="tableg">
                            <thead>
                              <tr>
                                <th>Id Prefactura</th>
                                <th>Cliente</th>
                                <th>Serie del equipo</th>
                                <th>Modelo</th>
                                <th>Serie Instalada</th>
                                <th>Serie retirada</th>
                                <th>Folio RMA</th>
                                <th>Folio RMA</th>
                                <th>Fecha de tramite</th>
                                <th>Fecha de envio</th>
                                <th>Folio nota de credito</th>
                              </tr>
                            </thead>
                            
                          </table>
                        </div>
                      </div>
                    </div>
                  </div> 
                      
                </div>
                   
              </div>
            </div>  
               </div>
        </section>
