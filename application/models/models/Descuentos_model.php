<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class Descuentos_model extends CI_Model 
{
    public function __construct() 
    {
        parent::__construct();
    }

    public function actualizaPorcentajes($id, $data)
    {
    	$this->db->set($data);
	    $this->db->where('id', $id);
	    return $this->db->update('descuentosCategorias');
    }

   	public function getDescuentos()
   	{
   		$sql = "SELECT * FROM descuentosCategorias";
        $query = $this->db->query($sql);
        return $query->result();
   	}
}

?>