<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Listapreciosupdate extends CI_Controller 
{
    public function __construct() {
        parent::__construct();
        $this->load->model('equipos_model');
        $this->load->model('ModeloCatalogos');
        $this->load->model('consumibles_model');
        $this->load->model('refacciones_model');
        $this->load->model('login_model');
        $this->load->helper('url');
        if($this->session->userdata('logeado')==true){
            $this->perfilid =$this->session->userdata('perfilid');
            $this->idpersonal = $this->session->userdata('idpersonal');
            //=========================================================================
            $this->getTipoCambioEquipos = $this->equipos_model->getTipoCambioEquipos();
            $this->getDescuentosEquipos = $this->equipos_model->getDescuentosEquipos();
            $this->getPorcentajeGananciaEquipos = $this->equipos_model->getPorcentajeGananciaEquipos();
            //===========================================================================
            $this->getTipoCambioConsumibles = $this->consumibles_model->getTipoCambioConsumibles();
            $this->getDescuentosConsumibles = $this->consumibles_model->getDescuentosConsumibles();
            $this->getPorcentajeGananciaConsumibles = $this->consumibles_model->getPorcentajeGananciaConsumibles();
            //=========================================================================
            $this->getTipoCambioRefacciones=$this->refacciones_model->getTipoCambioRefacciones();
            $this->getPorcentajeGananciaRefacciones=$this->refacciones_model->getPorcentajeGananciaRefacciones();
            $this->getDescuentosRefacciones=$this->refacciones_model->getDescuentosRefacciones();
            $this->tableresult = $this->ModeloCatalogos->getselectwheren('tabla_descuentos_refacciones',array('activo'=>1));

            
        }else{
            redirect('login');
        }
    }

    // Listado de Precios de Equipos
    function index(){
        $listadoequipos = $this->ModeloCatalogos->getselectwheren('equipos',array('estatus'=>1,'precioDolares >'=>0));
        foreach ($listadoequipos->result() as $item) {
            $this->actualizarprecioequipos($item->id,$item->precioDolares);

        }
        $listadoconsumibles = $this->ModeloCatalogos->getselectwheren('consumibles_costos',array('precioDolares >'=>0));
        foreach ($listadoconsumibles->result() as $item) {
            $this->actualizarpreciosconsumibles($item->id,$item->precioDolares);
        }
        $listadorefacciones = $this->ModeloCatalogos->getselectwheren('refacciones_costos',array('precioDolares >'=>0));
        foreach ($listadorefacciones->result() as $item) {
            $this->actualizarpreciosrefacciones($item->id,$item->precioDolares);
        }
    }
    function actualizarprecioequipos($id,$dolares){
         // Obtenemos el Tipo de Cambio
            $cambio = $this->getTipoCambioEquipos;
            
            // Obtenemos los descuentos
            $descuentos = $this->getDescuentosEquipos;
            $descuentoEquipos = $descuentos[0]->descuento;

            // Convertimos a Pesos 
            $tipoCambio = $cambio[0]->cambio;
            $precio = $dolares;
            $precioPesos =  (($precio*$tipoCambio)*(100-$descuentoEquipos))/100;


            // Obtenemos los porcentajes de Ganancia para equipos
            $porcentajesGanancia = $this->getPorcentajeGananciaEquipos;
            // Guardamos cada uno de los porcentajes correspondientes
            $porcentajeVenta = $porcentajesGanancia[0]->gananciaVenta;
            $porcentajeRenta = $porcentajesGanancia[0]->gananciaRenta;
            $porcentajePoliza = $porcentajesGanancia[0]->gananciaPoliza;
            $porcentajeRevendedor = $porcentajesGanancia[0]->gananciaRevendedor;

            // Hacemos los descuentos sobre los precios con Ganancia
            $totalDescuento = ($descuentoEquipos * $precioPesos) / 100;
            $precioConDescuentoVenta = $precioPesos/((100-$porcentajeVenta)/100);
            $precioConDescuentoRenta = $precioPesos/(1-($porcentajeRenta/100));
            $precioConDescuentoPoliza = $precioPesos/(1-($porcentajePoliza/100));
            $precioConDescuentoRevendedor = $precioPesos/(1-($porcentajeRevendedor/100));;
            

            $data['costo_venta'] = $precioConDescuentoVenta;
            $data['costo_renta'] = $precioConDescuentoRenta;
            $data['costo_poliza'] = $precioConDescuentoPoliza;
            $data['costo_revendedor'] = $precioConDescuentoRevendedor;
            $data['costo_pesos'] = $precioPesos;
            $data['porcentaje_ganancia'] = $porcentajeVenta;
            $data['descuento'] = $totalDescuento;

            $this->ModeloCatalogos->updateCatalogo('equipos',$data,array('id'=>$id));
    }
    function actualizarpreciosconsumibles($id,$dolares){
        // Obtenemos el Tipo de Cambio
            $cambio = $this->getTipoCambioConsumibles;
            // Convertimos a Pesos 
            $tipoCambio = $cambio[0]->cambio;
            $precio = $dolares;
            $descuentos = $this->getDescuentosConsumibles;
            $descuentoConsumibles = $descuentos[0]->descuento;
            //======================================================================
                $precioPesos1=$precio*$tipoCambio;
                $precioPesos2=100-$descuentoConsumibles;
            //======================================================================
            $precioPesos = (($precioPesos1)*($precioPesos2))/100;
            // Obtenemos los porcentajes de Ganancia para Refacciones
            $porcentajesGanancia = $this->getPorcentajeGananciaConsumibles;

            // Guardamos cada uno de los porcentajes correspondientes
            // VENTA = GENERAL; RENTA = ESPECIAL; POLIZA = FRECUENTE; REVENDEDOR = POLIZA
            $porcentajeGeneral = $porcentajesGanancia[0]->gananciaVenta;
            $porcentajeEspecial = $porcentajesGanancia[0]->gananciaPoliza;
            $porcentajeFrecuente = $porcentajesGanancia[0]->gananciaRenta;
            $porcentajePoliza = $porcentajesGanancia[0]->gananciaRevendedor;

            // Hacemos los descuentos sobre los precios con Ganancia
            $totalDescuento = ($descuentoConsumibles * $precioPesos) / 100;

            $precioConDescuentoGeneral = $precioPesos/((100-$porcentajeGeneral)/100);
            //log_message('error', $precioPesos.'/((100-'.$porcentajeGeneral.')/100)');
            $precioConDescuentoFrecuente = $precioPesos/(1-($porcentajeFrecuente/100));
            //log_message('error', $precioPesos.'/(1-('.$porcentajeFrecuente.'/100))');
            $precioConDescuentoEspecial = $precioPesos/(1-($porcentajeEspecial/100));
            
            $precioConDescuentoPoliza = $precioPesos/(1-($porcentajePoliza/100));

            $data['general'] = $precioConDescuentoGeneral;
            $data['especial'] = $precioConDescuentoEspecial;
            $data['frecuente'] = $precioConDescuentoFrecuente;
            $data['poliza'] = $precioConDescuentoPoliza;
            $data['costo_pesos'] = $precioPesos;
            $data['porcentaje_ganancia'] = $porcentajeGeneral;
            $data['descuento'] = $totalDescuento;

            $this->ModeloCatalogos->updateCatalogo('consumibles_costos',$data,array('id'=>$id));
    }
    function actualizarpreciosrefacciones($id,$dolares){
        $cambio = $this->getTipoCambioRefacciones;

            // Convertimos a Pesos 
            $tipoCambio = $cambio[0]->cambio;
            $precio = $dolares;
            $precioPesos = $tipoCambio * $precio;

            // Obtenemos los porcentajes de Ganancia para Refacciones
            $porcentajesGanancia = $this->getPorcentajeGananciaRefacciones;

            // Guardamos cada uno de los porcentajes correspondientes
            $porcentajeGeneral = $porcentajesGanancia[0]->gananciaVenta;
            // Obtenemos los descuentos
            $descuentos = $this->getDescuentosRefacciones;
            $descuentoRefacciones = $descuentos[0]->descuento;

            // Hacemos los descuentos sobre los precios con Ganancia
            $totalDescuento = ($descuentoRefacciones * $precioPesos) / 100;

            //============================================================================
            $precioConDescuentoGeneral=0;
            $precioConDescuentoEspecial=0;
            $precioConDescuentoFrecuente=0;
            $precioConDescuentoPoliza=0;
            $tableresult = $this->tableresult;
            foreach ($tableresult->result() as $item) {
                if ($item->id<6) {
                    if (floatval($precio)>=floatval($item->pd_inicio) && floatval($precio)<=floatval($item->pd_fin)) {
                        $precioConDescuentoGeneral=$precio*$item->for_gen;
                        $precioConDescuentoEspecial=floatval($item->aaa)*floatval($precio);
                        $precioConDescuentoFrecuente=floatval($item->sem_frec)*floatval($precio);
                        //log_message('error', $item->id.' '.$item->sem_frec.' * '.$precio);
                        $precioConDescuentoPoliza=floatval($item->local_poli)*floatval($precio);
                        
                    }
                }else{
                    if (floatval($precio)>=floatval($item->pd_inicio)) {
                        $precioConDescuentoGeneral=$precio*$item->for_gen;
                        $precioConDescuentoEspecial=floatval($item->aaa)*floatval($precio);
                        $precioConDescuentoFrecuente=floatval($item->sem_frec)*floatval($precio);
                        $precioConDescuentoPoliza=floatval($item->local_poli)*floatval($precio);
                        //log_message('error', $item->id.' '.$item->pd_inicio.'>='.$precio);
                    }
                }
            }

            $data['general'] = $precioConDescuentoGeneral;
            $data['especial'] = $precioConDescuentoEspecial;
            $data['frecuente'] = $precioConDescuentoFrecuente;
            $data['poliza'] = $precioConDescuentoPoliza;
            $data['costo_pesos'] = $precioPesos;
            $data['porcentaje_ganancia'] = $porcentajeGeneral;
            $data['descuento'] = $totalDescuento;
            $this->ModeloCatalogos->updateCatalogo('refacciones_costos',$data,array('id'=>$id));
    }

   


}