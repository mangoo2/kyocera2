<?php
require_once dirname(__FILE__) . '/TCPDF3/examples/tcpdf_include.php'; 
require_once dirname(__FILE__) . '/TCPDF3/tcpdf.php';

  $logos = base_url().'public/img/alta.jpg';
  $logos2 = base_url().'public/img/kyocera.jpg';
  //=========================================================================
    $rfc_selected='';
    $razon_social_selected='';
    $metodopago_text='';
    $formapago_text='';
    $uso_cfdi_text='';
    $telefono='';
    $estadoval='';
    $puesto_t='';
    $email_t='';
    $atencionpara_t='';

    foreach ($rfc_datos as $item) { 
      if($item->id==$rfc_id){
        $razon_social_selected=$item->razon_social;
        $rfc_selected=$item->rfc; 
        $cp =$item->cp;
        $estadoval=$item->estado;
        if($item->rfc=='XAXX010101000'){
          $razon_social_selected.='<br>'.$empresars;
        }
      }      
    }
    foreach ($metodopagorow->result() as $item) {
      if($item->id==$metodopagoId){ 
        $metodopago_text=$item->metodopago_text;
      }
    }
    foreach ($formapagorow->result() as $item) {
      if($item->id==$formapago){ 
        $formapago_text=$item->formapago_text;
      }
    }
    foreach ($cfdirow->result() as $item) {
      if($item->id==$cfdi){ 
        $uso_cfdi_text=$item->uso_cfdi_text;
      }
    } 
    foreach ($resultadoclitel->result() as $item) {
      if($telId==$item->id){
        $telefono=$item->tel_local;
      }
    } 
    foreach ($datoscontacto->result() as $item) { 
      if($telId==$item->datosId){
        $telefono=$item->telefono;
      }
    }
    
    foreach ($estadorow->result() as $item) { 
        if($item->EstadoId==$estado){ 
          $estadoval = $item->Nombre;
        }
    }
    

    foreach ($resultadoclipcontacto->result() as $item) { 
      if($contactoId==$item->id){
        $atencionpara_t=$item->persona_contacto;
      } 
    }
    foreach ($datoscontacto->result() as $item) { 
      if($contactoId==$item->datosId){
        $email_t=$item->email;
        $puesto_t=$item->puesto;
        $telefono=$item->telefono;
        $atencionpara_t=$item->atencionpara;




      }
    }

  //=========================================================================
//====================================================
class MYPDF extends TCPDF {
 
  public function Header() {

      
      //$this->writeHTML($html, true, false, true, false, '');
  }
    // Page footer
  public function Footer() {
    $html2='';
    
      //<img src="http://facturacion33.adminfactura.com.mx/view/image/viamex_logob100.png" width="100px">
      //$this->writeHTML($html2, true, false, true, false, '');
      
  }
} 
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('PROINREN');
$pdf->SetTitle('PROINREN');
$pdf->SetSubject('factura');
$pdf->SetKeywords('factura');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins('7', '10', '7');
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin('10');

// set auto page breaks
$pdf->SetAutoPageBreak(true, 10);//PDF_MARGIN_BOTTOM

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('dejavusans', '', 7);
// add a page
$pdf->AddPage('P', 'A4');
$html='<style type="text/css">
          .t_c{
            text-align:center;
          }
          .t_b{
            font-weight: bold;
          }
          .bor_b{
            border-bottom: 1px solid black;
          }
          .bor_l{
            border-left: 1px solid black;
          }
          .bor_r{
            border-right: 1px solid black;
          }
          .bor_t{
            border-top: 1px solid black;
          }
          .tableb th{
              background-color:rgb(217,217,217);          
          }
        </style>';

  $html.='<table border="0">
            <tr>
              <td width="20%" class="t_c"><img src="'.$logos.'" width="80px"></td>
              <td width="60%" class="t_c">
                "ALTA PRODUCTIVIDAD, S.A DE C.V."<br>
                RFC: APR980122KZ6<br>
                Impresoras y Multifuncionales, Venta, Renta y Mantenimiento.<br>
                Domicilio Fiscal: Blvd. Norte 1831 Col. Villas San Alejandro. Puebla, Pue. Mexico C.P.72090<br>
                Banco BANAMEX, Cuenta 7347132, Sucursal 826, CLABE 2668082673471320<br>
                Call Center 273 3400, 249 5393. www.kyoceraap.com

              </td>
              <td width="20%" class="t_c"><img src="'.$logos2.'" width="100px"></td>
            </tr>
          </table>
          <p></p>
          <table border="0" class="tableb" cellpadding="2">
            <tr>
              <th class="bor_b bor_l  bor_t" >Fecha: </th>
              <td class="bor_b  bor_t" >'.$dia_reg.'/'.$mes_reg.'/'.$ano_reg.'</td>
              <th class="bor_b  bor_t" >AC</th>
              <td class="bor_b  bor_t" >'.$ini_personal.'</td>
              <th class="bor_b  bor_t" >PROINREN  No.</th>
              <td class="bor_b  bor_t" >'.$proinven.'</td>
              <th class="bor_b  bor_t" >ENTREGA</th>
              <td class="bor_b  bor_r bor_t" >'.$rentaventashg->fechaentrega.'</td>
            </tr>
            <tr>
              <th class="bor_b bor_l">Cia.</th>
              <td class="bor_b  " colspan="5">'.$razon_social_selected.'</td>
              <th class="bor_b  ">RFC</th>
              <td class="bor_b  bor_r">'.$rfc_selected.'</td>
            </tr>
          </table>
          <table border="0" class="tableb" cellpadding="2">
            <tr>
              <td class="bor_b bor_l bor_r " >Vence:</td>
              <td class="bor_b  bor_r " >'.$vence.'</td>
              <td class="bor_b  bor_r" >Método de pago:</td>
              <td class="bor_b  bor_r " >'.$metodopago_text.'</td>
              <td class="bor_b  bor_r" >Forma de pago:</td>
              <td class="bor_b  bor_r " >'.$formapago_text.'</td>
              <td class="bor_b  bor_r" >Uso de CFDI:</td>
              <td class="bor_b  bor_r " >'.$uso_cfdi_text.'</td>
            </tr>
            <tr>
              <td class="bor_b bor_l  " >Tel:</td>
              <td class="bor_b   " colspan="4">'.$telefono.'</td>
              <td class="bor_b  " >Forma de cobro:</td>
              <td class="bor_b  bor_r " colspan="2">'.$formacobro.'</td>
            </tr>
            <tr>
              <td class="bor_b bor_l bor_r " colspan="4">
                <span class="t_b">Dirección de Entrega/Instalación:</span><br>
                '.$domicilio_entrega.'
              </td>
              <td class="bor_b  bor_r " colspan="4">
                <table >
                  <tr>
                    <td colspan="2" class="t_b">Dirección Fiscal:</td>
                  </tr>
                  <tr>
                    <td>Calle:</td><td>'.$calle.'</td>
                  </tr>
                  <tr>
                    <td>No. </td><td>'.$num_ext.'</td>
                  </tr>
                  <tr>
                    <td>Col.</td><td>'.$colonia.'</td>
                  </tr>
                  <tr>
                    <td>CD.</td><td>'.$municipio.'</td>
                  </tr>
                  <tr>
                    <td>Estado.</td><td>'.$estadoval.'</td>
                  </tr>
                  <tr>
                    <td>C.P.</td><td>'.$cp.'</td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
              <td class="bor_b bor_l  bor_r" colspan="3"><span class="t_b">Contacto:</span>'.$atencionpara_t.'</td>
              <td class="bor_b  bor_r" colspan="2"><span class="t_b">Cargo:</span>'.$cargo.'</td>
              <td class="bor_b  bor_r "colspan="3" ><span class="t_b">Email:</span>'.$email.'</td>
            </tr>
            <tr>
              <td class="bor_b bor_l  bor_r" ><span class="t_b">Contrato:</span> </td>
              <th class="bor_b  bor_r" colspan="7" align="center">'.$folio.'</th>
            </tr>
          </table>
          <p></p>';
          
          $html.='<table class="tableb" cellpadding="2" align="center">
            <tr>
              <th class="bor_b bor_l  bor_r bor_t">Cant.</th>
              <th class="bor_b bor_l  bor_r bor_t">Marca</th>
              <th class="bor_b bor_l  bor_r bor_t">Tipo</th>
              <th class="bor_b bor_l  bor_r bor_t">Modelo</th>
              <th class="bor_b bor_l  bor_r bor_t">Serie</th>
              <th class="bor_b bor_l  bor_r bor_t">No. De Parte</th>
              <th class="bor_b bor_l  bor_r bor_t">Folio</th>
              <th class="bor_b bor_l  bor_r bor_t">Ubicacion</th>
              <th class="bor_b bor_l  bor_r bor_t">Referencia (Equipo)</th>
              <th class="bor_b bor_l  bor_r bor_t">Serie de Referencia (Serie equipo)</th>
              <th class="bor_b bor_l  bor_r bor_t">Bodega</th>
            </tr>';
            

            $arrays_tr=array();
            $arrays_tr[]=array('equipo'=>0,'html'=>'');
            $totalgeneral=0;
            foreach ($rentaventash->result() as $item) { 
                //$totalgeneral=$totalgeneral+($item->precio*$item->cantidad);
                $totalgeneral=$totalgeneral+0;
                $datosseries = $this->ModeloCatalogos->rentaventasdseries($item->id);
                $usado=0;
                foreach($datosseries->result() as $items) {
                  $usado=$items->usado;
                }
                if($usado==1){
                  $vusado='-mc';
                }else{
                  $vusado='';
                }
                $var_series='';
                foreach($datosseries->result() as $items) {
                  $var_series.=$items->serie.' ';    
                }
                ${'equiposerie_'.$item->id}=$var_series;
                $bodega=$this->ModeloCatalogos->obtenerbodega($item->serie_bodega);
              $html0=''; 
              $html0.='<tr>
                      <td class="bor_b bor_l  bor_r bor_t">'.$item->cantidad.'</td>
                      <td class="bor_b bor_l  bor_r bor_t">KYOCERA</td>
                      <td class="bor_b bor_l  bor_r bor_t">Equipo</td>
                      <td class="bor_b bor_l  bor_r bor_t">'.$item->modelo.$vusado.'</td>
                      <td class="bor_b bor_l  bor_r bor_t">'.$var_series.'</td>
                      <td class="bor_b bor_l  bor_r bor_t">'.$item->noparte.'</td>
                      <td class="bor_b bor_l  bor_r bor_t"></td>
                      <td class="bor_b bor_l  bor_r bor_t"></td>
                      <td class="bor_b bor_l  bor_r bor_t"></td>
                      <td class="bor_b bor_l  bor_r bor_t"></td>
                      <td class="bor_b bor_l  bor_r bor_t">'.$bodega.'</td>
                </tr>';
              $arrays_tr[]=array('equipo'=>$item->id,'html'=>$html0);
            }
          
            foreach ($rentavacessoriosh->result() as $item) { 
                $equiporef=$this->ModeloCatalogos->obtenerequipo($item->id_equipo);
                //$totalgeneral=$totalgeneral+($item->precio*$item->cantidad);
                $html0='';
                $var_series='';
                $datosseries = $this->ModeloCatalogos->rentaventasaccessoriosseries($item->id_accesoriod); 
                foreach($datosseries->result() as $items) { 
                    $var_series.=$items->serie.' ';
                    
                }
                $modeloref='';
                $serieref='';
                $datosseriesequipos = $this->ModeloCatalogos->acc_equipo_serie($item->id_accesoriod); 
                  foreach ($datosseriesequipos->result() as $itemae) {
                    $modeloref=$itemae->modelo;
                    
                    $serieref=$itemae->serie;
                    
                  }
                  if($modeloref==''){
                      $modeloref=$equiporef;
                  }
                  if($serieref==''){
                    if($item->rowequipo>0){
                      if(isset(${'equiposerie_'.$item->rowequipo})){
                        $serieref=${'equiposerie_'.$item->rowequipo};
                      }
                    }
                  }
                $html0.='<tr>
                      <td class="bor_b bor_l  bor_r bor_t">'.$item->cantidad.'</td>
                    <td class="bor_b bor_l  bor_r bor_t">KYOCERA</td>
                    <td class="bor_b bor_l  bor_r bor_t">Accesorio</td>
                    <td class="bor_b bor_l  bor_r bor_t">'.$item->nombre.'</td>
                    <td class="bor_b bor_l  bor_r bor_t">'.$var_series.'</td>
                    <td class="bor_b bor_l  bor_r bor_t">'.$item->no_parte.'</td>
                    <td class="bor_b bor_l  bor_r bor_t"></td>
                    <td class="bor_b bor_l  bor_r bor_t"></td>
                    <td class="bor_b bor_l  bor_r bor_t">'.$modeloref.'</td>
                    <td class="bor_b bor_l  bor_r bor_t">'.$serieref.'</td>
                    <td class="bor_b bor_l  bor_r bor_t"></td>
                  </tr>';
                $arrays_tr[]=array('equipo'=>$item->rowequipo,'html'=>$html0);
            }


           
            foreach ($rentavconsumiblesh->result() as $item) { 
                  $serieref='';
                  $bodega=$this->ModeloCatalogos->obtenerbodega($item->bodega);
                  $equipo=$this->ModeloCatalogos->obtenerequipo($item->idequipo);

                  if($serieref==''){
                    if($item->rowequipo>0){
                      if(isset(${'equiposerie_'.$item->rowequipo})){
                        $serieref=${'equiposerie_'.$item->rowequipo};
                      }
                    
                    }
                  }
                  $html0='<tr>
                        <td class="bor_b bor_l  bor_r bor_t">'.$item->cantidad.'</td>
                        <td class="bor_b bor_l  bor_r bor_t">KYOCERA</td>
                        <td class="bor_b bor_l  bor_r bor_t">Consumible</td>
                        <td class="bor_b bor_l  bor_r bor_t">'.$item->modelo.'</td>
                        <td class="bor_b bor_l  bor_r bor_t"></td>
                        <td class="bor_b bor_l  bor_r bor_t">'.$item->parte.'</td>
                        <td class="bor_b bor_l  bor_r bor_t">'.$item->folios.'</td>
                        <td class="bor_b bor_l  bor_r bor_t"></td>
                        <td class="bor_b bor_l  bor_r bor_t">'.$equipo.'</td>
                        <td class="bor_b bor_l  bor_r bor_t">'.$serieref.'</td>
                        <td class="bor_b bor_l  bor_r bor_t">'.$bodega.'</td>
                      </tr>';
            
              $arrays_tr[]=array('equipo'=>$item->rowequipo,'html'=>$html0);
            }
            

            foreach ($arrays_tr as $key => $row) {
                $aux[$key] = $row['equipo'];
            }
            array_multisort($aux, SORT_ASC, $arrays_tr);
            foreach ($arrays_tr as $key => $row) {
                log_message('error',$row['html']);
                  $html.=$row['html'];
            }

          $html.='</table>
          <p></p>';

          $html.='<table class="bor_b bor_l  bor_r bor_t" cellpadding="2">
                  <tr><td>Observaciones</td></tr>
                  <tr><td>'.$observaciones.'</td></tr>
                  </table>';
$pdf->writeHTML($html, true, false, true, false, '');



$pdf->IncludeJS('print(true);');
$pdf->Output('Folios_.pdf', 'I');

?>