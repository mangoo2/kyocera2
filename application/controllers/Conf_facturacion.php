<?php

defined('BASEPATH') OR exit('No direct script access allowed');
class Conf_facturacion extends CI_Controller {
    public function __construct()     {
        parent::__construct();

        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloUnidadesservicios');
        $this->load->helper('url');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->load->helper(array('form'));
        $this->submenu=60;
        if($this->session->userdata('logeado')==true){
            $this->perfilid =$this->session->userdata('perfilid');
            $this->idpersonal = $this->session->userdata('idpersonal');
            /*
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,60);// 52 es el id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
            */
        }else{
            redirect('login'); 
        }
    }

    function index(){
        $data['MenusubId']=$this->submenu;
        $tipov=1;
        if(isset($_GET['tipov'])){
            if($_GET['tipov']>0){
                $tipov=$_GET['tipov'];
            }
        }
        if($tipov==2){
            $rutafile='2';
        }else{
            $rutafile='';
        }
        $data['tipov']=$tipov;
        $datosconfiguracion=$this->ModeloCatalogos->getselectwheren('f_configuraciones',array('ConfiguracionesId'=>$tipov));
        $datosconfiguracion=$datosconfiguracion->result();
        $datosconfiguracion=$datosconfiguracion[0];
        $data['ConfiguracionesId']=$datosconfiguracion->ConfiguracionesId;
        $data['Rfc']=$datosconfiguracion->Rfc;
        $data['Curp']=$datosconfiguracion->Curp;
        $data['Nombre']=$datosconfiguracion->Nombre;
        $data['Direccion']=$datosconfiguracion->Direccion;
        $data['PaisExpedicion']=$datosconfiguracion->PaisExpedicion;
        $data['Regimen']=$datosconfiguracion->Regimen;
        $data['Calle']=$datosconfiguracion->Calle;
        $data['Estado']=$datosconfiguracion->Estado;
        $data['Municipio']=$datosconfiguracion->Municipio;
        $data['CodigoPostal']=$datosconfiguracion->CodigoPostal;
        $data['Noexterior']=$datosconfiguracion->Noexterior;
        $data['Colonia']=$datosconfiguracion->Colonia;
        $data['localidad']=$datosconfiguracion->localidad;
        $data['Nointerior']=$datosconfiguracion->Nointerior;

        $timbresvigentes=$datosconfiguracion->timbresvigentes;
        $data['timbresvigentes']=$timbresvigentes;
        $tv_inicio=$datosconfiguracion->tv_inicio;
        $tv_fin=$datosconfiguracion->tv_fin;
        $facturastimbradas=$this->ModeloCatalogos->total_facturas(1,$tv_inicio,$tv_fin);
        
        $facturascanceladas_f=$this->ModeloCatalogos->total_facturas(0,$tv_inicio,$tv_fin);
        $facturascanceladas_c=$this->ModeloCatalogos->totalcomplementos(0,$tv_inicio,$tv_fin);

        $facturascanceladas=$facturascanceladas_f+$facturascanceladas_c;

        $complementotimbradas=$this->ModeloCatalogos->totalcomplementos(1,$tv_inicio,$tv_fin);
        $data['facturastimbradas'] = $facturastimbradas;
        $data['facturascanceladas'] = $facturascanceladas;
        $data['complementotimbradas'] = $complementotimbradas;
        $data['facturasdisponibles'] = $timbresvigentes-$facturastimbradas-$facturascanceladas-$complementotimbradas;
        //==================================================================================
        $nombre_fichero = 'kyocera/temporalsat'.$rutafile.'/FinVigencia.txt';
        if (file_exists($nombre_fichero)) {
            $fh = fopen(base_url().$nombre_fichero, 'r') or die("Se produjo un error al abrir el archivo");
                $linea = fgets($fh);
            fclose($fh); 

            $remplazar = array('notAfter=','  ','GMT'); 
            $remplazarpor = array('',' ','');
            $fechavigente = str_replace($remplazar,$remplazarpor,$linea);
            $fechavigenteex = explode(" ", $fechavigente);
            $fechafin=$fechavigenteex[1].' '.$fechavigenteex[0].' '.$fechavigenteex[3];
        }else{
            $fechafin='';
        }
        $data['fechafin']=$fechafin;
        //==================================================================================
        $this->load->view('header');
        $this->load->view('main');
        $this->load->view('configuraciones/facturacion',$data);
        $this->load->view('footer');
        $this->load->view('configuraciones/facturacionjs');
    }
    function cargacertificado(){
        $ConfiguracionesId=$_POST['ConfiguracionesId'];
        if($ConfiguracionesId==2){
            $rut_file='2';
            $ConfiguracionesId=2;
        }else{
            $rut_file='';
            $ConfiguracionesId=1;
        }
        $configUpload['upload_path'] = FCPATH.'kyocera/elementos'.$rut_file.'/';
        $configUpload['allowed_types'] = '*';
        $configUpload['max_size'] = '5000';
        $configUpload['file_name'] = 'archivos';
        $this->load->library('upload', $configUpload);
        $this->upload->do_upload('cerdigital');  
        $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
        $file_name = $upload_data['file_name']; //uploded file name
        $extension=$upload_data['file_ext'];    // uploded file extension

        $this->ModeloCatalogos->updateCatalogo('f_configuraciones',array('archivocer'=>$file_name),array('ConfiguracionesId'=>$ConfiguracionesId));

        $output = [];
        echo json_encode($output);
    }
    function cargakey(){
        $ConfiguracionesId=$_POST['ConfiguracionesId'];
        if($ConfiguracionesId==2){
            $rut_file='2';
            $ConfiguracionesId=2;
        }else{
            $rut_file='';
            $ConfiguracionesId=1;
        }

        $rutainterna= $_SERVER['DOCUMENT_ROOT'].'/kyocera2/';
        //$rutainterna= $_SERVER['DOCUMENT_ROOT'];
        $rutaarchivos=$rutainterna.'/kyocera/elementos'.$rut_file.'/';
        
        $pass = $_POST['pass'];
        $configUpload['upload_path'] = FCPATH.'kyocera/elementos'.$rut_file.'/';
        //$configUpload['upload_path'] = base_url().'kyocera/elementos/';
        $configUpload['allowed_types'] = '*';
        $configUpload['max_size'] = '5000';
        $configUpload['file_name'] = 'archivos';
        $this->load->library('upload', $configUpload);
        $this->upload->do_upload('claveprivada');  
        $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
        $file_name = $upload_data['file_name']; //uploded file name
        $extension=$upload_data['file_ext'];    // uploded file extension

        $this->ModeloCatalogos->updateCatalogo('f_configuraciones',array('archivokey'=>$file_name,'paswordkey'=>$pass),array('ConfiguracionesId'=>$ConfiguracionesId));
        file_put_contents($rutaarchivos.'passs.txt', $pass);
        file_put_contents($rutaarchivos.'pass.txt', $pass);
        $output = [];
        echo json_encode($output);
    }
    function procesar(){
        $ConfiguracionesId=$_POST['ConfiguracionesId'];
        if($ConfiguracionesId==2){
            $rut_file='2';
        }else{
            $rut_file='';
        }

        $paswordkey=$_POST['passprivada'];
        $configUpload['upload_path'] = FCPATH.'kyocera/elementos'.$rut_file.'/';
        $configUpload['allowed_types'] = '*';
        $configUpload['max_size'] = '50000';
        $configUpload['file_name'] = 'archivos';
        $archivokey='';
        $archivocer='';
        $mensajes=array();
        $this->load->library('upload', $configUpload);
        if (!$this->upload->do_upload('cerdigital')) {
            $mensajes[]=array('error'=>$this->upload->display_errors());
        }else{
            $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
            $archivocer = $upload_data['file_name']; //uploded file name
            if (!$this->upload->do_upload('claveprivada')) {
                $mensajes[]=array('error'=>$this->upload->display_errors());
            }else{
                $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
                $archivokey = $upload_data['file_name']; //uploded file name
                
               // echo 'correcto';
            }

            
            ///echo 'correcto';
        }
        $this->ModeloCatalogos->updateCatalogo('f_configuraciones',array('archivokey'=>$archivokey,'archivocer'=>$archivocer,'paswordkey'=>$paswordkey),array('ConfiguracionesId'=>1));
        //===============================================================================================================
        $this->combertir();
    }
    function combertir(){
        /*
        $datosconfiguracion=$this->ModeloCatalogos->getselectwheren('f_configuraciones',array('ConfiguracionesId'=>1));
        $datosconfiguracion=$datosconfiguracion->result();
        $datosconfiguracion=$datosconfiguracion[0];
        */
        $rutainterna= $_SERVER['DOCUMENT_ROOT'].'/kyocera2/';// da la ruta raiz ejemplo C:\xampp\htdocs\kyocera2\
        //$rutainterna= $_SERVER['DOCUMENT_ROOT'];// da la ruta raiz ejemplo C:\xampp\htdocs\kyocera2\
        //$rutainterna= base_url();// da la ruta raiz ejemplo C:\xampp\htdocs\kyocera2\
        //echo $rutainterna;

        $rutaarchivos=$rutainterna.'kyocera/elementos/';
        $ruta=$rutainterna.'kyocera/temporalsat/';
        /*
        $archivocer=$datosconfiguracion->archivocer;
        $archivokey=$datosconfiguracion->archivokey;
        $paswordkey=$datosconfiguracion->paswordkey;
        $rutacer=$rutaarchivos.$archivocer;
        */
        exec('openssl x509 -inform DER -in '.$rutaarchivos.'archivos.cer -noout -text > '.$ruta.'datos.txt');
    }
    function datosgenerales(){
        $data = $this->input->post();
        $id = $data['ConfiguracionesId']; 
        unset($data['ConfiguracionesId']);

        $this->ModeloCatalogos->updateCatalogo('f_configuraciones',$data,array('ConfiguracionesId'=>$id));
    }
    function unidades_servicios(){
        $this->load->view('header');
        $this->load->view('main');
        $this->load->view('configuraciones/unidadesservicios');
        $this->load->view('footer');
        $this->load->view('configuraciones/unidadesserviciosjs');
    }
    function unidades_servicios_default(){
        $usdefault=$this->ModeloCatalogos->getselectwheren('unidades_servicios_default',array('activo'=>1));
        
        $data['usdefault']=$usdefault;

        $this->load->view('header');
        $this->load->view('main');
        $this->load->view('configuraciones/usdefault',$data);
        $this->load->view('footer');
        $this->load->view('configuraciones/usdefaultjs');
    }
    
    public function getlistservicios() {
        $params = $this->input->post();
        $getdata = $this->ModeloUnidadesservicios->getservicios($params);
        $totaldata= $this->ModeloUnidadesservicios->total_servicios($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    public function getlistunidades() {
        $params = $this->input->post();
        $getdata = $this->ModeloUnidadesservicios->getunidades($params);
        $totaldata= $this->ModeloUnidadesservicios->total_unidades($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    function adservicio(){
        $params = $this->input->post();
        $this->ModeloCatalogos->updateCatalogo('f_servicios',array('activo'=>$params['activo']),array('ServiciosId'=>$params['ServiciosId']));
    }
    function adunidad(){
        $params = $this->input->post();
        $this->ModeloCatalogos->updateCatalogo('f_unidades',array('activo'=>$params['activo']),array('UnidadId'=>$params['UnidadId']));
    }
    function updateuni_ser_def(){
        $datas = $this->input->post();
        $unidades=$datas['unidades'];
        $DATAc = json_decode($unidades);
        $vowels = array("  ", "\n");
          for ($i=0;$i<count($DATAc);$i++) {

                $dataco['unidadsat'] =$DATAc[$i]->unidadsat;
                $dataco['unidadsat_text'] =str_replace($vowels,'',$DATAc[$i]->unidadsat_text);
                $dataco['conceptosat'] =$DATAc[$i]->conceptosat;
                $dataco['conceptosat_text'] =str_replace($vowels,'',$DATAc[$i]->conceptosat_text);
                
                $this->ModeloCatalogos->updateCatalogo('unidades_servicios_default',$dataco,array('id'=>$DATAc[$i]->id));
          }
    }


    
}
?>
