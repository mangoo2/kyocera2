var base_url =$('#base_url').val();
var tsubmenu;
$(document).ready(function($) {
	loadperfiles();
    $('#table-menu').DataTable({
        "pageLength": 60
    });
    tsubmenu=$('#table-submenu').DataTable();
});
function loadperfiles(){
	$.ajax({
        type:'POST',
        url: base_url+"Perfiles/viewperfiles",
        data: {
            datos:0
        },
        success: function (data){
            //console.log(data);
            var array = $.parseJSON(data);
            console.log(array);
            var options='<option value="0"></option>';
            $('.tbody-perfil').html('');
            $.each(array, function(index, item) {
                if(item.perfilId==1){
                    var btnsp='';
                }else{
                    var btnsp='<a class="waves-effect btn-bmz blue editp_'+item.perfilId+'" data-nombre="'+item.nombre+'" onclick="editp('+item.perfilId+')"><i class="fas fa-pencil-alt"></i></a>';
                        btnsp+='<a class="waves-effect btn-bmz red" onclick="deletep('+item.perfilId+')"><i class="fas fa-trash-alt"></i></a>';
                }
                var htmltablep='<tr><td>'+item.perfilId+'</td><td>'+item.nombre+'</td><td>'+btnsp+'</td></tr>';
                $('.tbody-perfil').append(htmltablep);
                options+='<option value="'+item.perfilId+'">'+item.nombre+'</option>';
            });
            $('#selectedperfiles').html(options);
            $('#table-perfil').DataTable();
        }
    });
}
function editp(rowid){
    var nomp=$('.editp_'+rowid).data('nombre');
    addperfil(rowid,nomp);
}
function addperfil(id,nom){
    $('#modaladdupdateperfil').modal();
    $('#modaladdupdateperfil').modal('open');
    $('#perfilId').val(id);
    $('#nombre').val(nom);
}
function deletep(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Confirma Eliminación del perfil?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Perfiles/deletep",
                    data: {
                        idf:id
                    },
                    success: function (data){
                        loadperfiles();
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}
function inserupdateperfil(){
    $.ajax({
        type:'POST',
        url: base_url+"Perfiles/inserupdateperfil",
        data: {
            id:$('#perfilId').val(),
            nom:$('#nombre').val()
        },
        success: function (data){
            toastr["success"]("Datos Registrados", "Hecho");
            $('#modaladdupdateperfil').modal('close');
            loadperfiles();
        }
    });
}
function viepermisos(){
    tsubmenu.destroy();
    $.ajax({
        type:'POST',
        url: base_url+"Perfiles/viewperfilespermisos",
        data: {
            perfilid:$('#selectedperfiles option:selected').val(),
        },
        success: function (data){
            //console.log(data);
            var array = $.parseJSON(data);
            var htmltablep='';
            $.each(array, function(index, item) {
                
                htmltablep+='<tr><td>'+item.menu+'</td><td>'+item.submenu+'</td><td><a class="waves-effect btn-bmz red" onclick="deletesubm('+item.Perfil_detalleId+')"><i class="fas fa-trash-alt"></i></td></tr>';
            });
            $('.tbody-submenu').html(htmltablep);
            tsubmenu=$('#table-submenu').DataTable({
                            "lengthMenu": [ 50, 70, 100 ]
                        });
        }
    });
}
function agregarpermiso(idsubmenu){
    $.ajax({
        type:'POST',
        url: base_url+"Perfiles/insertpermiso",
        data: {
            perfilid:$('#selectedperfiles option:selected').val(),
            submenu:idsubmenu
        },
        success: function (data){
            var numrow=parseInt(data);
            if(numrow==0){
                toastr["success"]("Permiso agregado", "Hecho");
            }else{
                toastr["error"]("El permiso ya se encuentra agregado");
            }
            
            viepermisos();
        }
    });
}
function deletesubm(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Confirma Eliminación del permiso?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Perfiles/deletesubm",
                    data: {
                        idps:id
                    },
                    success: function (data){
                        toastr["success"]("Permiso Eliminado", "Hecho");
                        viepermisos();
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}