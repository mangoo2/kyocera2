<link href="<?php echo base_url(); ?>public/css/control_tareas.css?v=3" type="text/css" rel="stylesheet">
<style type="text/css">
<?php 
    //$visor=1;//0 todos,1 solo el personal de session
    if($idpersonal==1 || $idpersonal==18 || $idpersonal==17 || $idpersonal==9){
        $visor=0;
        $select_personal=0;
    }else{
        $visor=1;
        $select_personal=$idpersonal;
    }
    if($visor==1){
        ?>  /*
            .btn-delete,.con_1,.btn-agregar,.select_admin{
                display: none !important;
            }*/
            .btn-delete,.select_admin{display: none !important;}
        <?php 
    }
?>
.act_fin{background-color:#43a047 !important;}
.act_fin .per_titulo,.act_fin .NdQKKfeqJDDdX3{
  color: white;
}
.advertencia {
  background-color:#df2f39;
  animation: infinite resplandorAnimationback 2s;
}
@keyframes resplandorAnimationback {
  0%,100%{
    background-color:#df2f39;
  }
  50%{
    background-color:#ffffff;
  }
}
<?php
  if($viewtipo==0){
    echo ' .con_event_group_4 { display:none;} ';
    echo ' .con_event_group_5 { display:none;} ';
    $title_mod='Control de tareas';
  }
  if($viewtipo==1){
    echo ' .con_event_group_1 { display:none;} ';
    echo ' .con_event_group_2 { display:none;} ';
    echo ' .con_event_group_3 { display:none;} ';
    $title_mod='Acuerdos / incidencias';
  }
?>
</style>
<input type="hidden" id="visor" value="<?php echo $visor;?>">
<input type="hidden" id="viewtipo" value="<?php echo $viewtipo;?>">
<!-- START CONTENT -->
<section id="content">
  <!--start container-->
  <div class="container">
    <div class="section">
      <div >
        <h4 class="header"><?php echo $title_mod;?></h4>
        <!---------------------------------------------->
        <div class="row">
          <div class="col s4 m2 l2">
            <a class="btn-bmz waves-effect waves-light green darken-1 btn-agregar" onclick="addtarea()" title="Agregar">Agregar</a>
          </div>
          <div class="col s4 m2 l2">
            <label class="select_admin">Personal</label>
            <select class="form-control-bmz browser-default select_admin" id="personal" onchange="recargardatos()">
                  <option value="0">Todos</option>
                 <?php 
                  foreach ($res_personal->result() as $item) {
                    if($select_personal==$item->personalId){
                      $option_selected=' selected ';
                    }else{
                      $option_selected='';
                    }
                    echo '<option value="'.$item->personalId.'" '.$option_selected.'>'.$item->nombre.' '.$item->apellido_paterno.' '.$item->apellido_materno.'</option>';
                  }
                 ?>
               </select>
          </div>
          <div class="col s1 m2 l2"></div>
          <div class="col s3 m6 l6" style="text-align: end;">
            <a class="btn-bmz waves-effect waves-light green darken-1" onclick="recargardatos()" title="Agregar Equipo"><i class="material-icons">autorenew</i></a>
          </div>
        </div>
        <div class="row" style="margin-top: 10px;">
          <div class="col s12">
            <!-------------------------------->
            <div class="contenedor con_1 con_event_group_0" ondrop="soltar(event)" ondragover="permitirSoltar(event,0)">
              <div class="contenedor2">
                <div class="bPNGI_VbtbXQ8v" data-testid="list-header" draggable="true">
                  <div class="mKJWg6W_CLHoiO"><h2 dir="auto" class="KLvU2mDGTQrsWG" role="textbox" data-testid="list-name">ACTIVIDADES</h2></div>
                  <div class="S10qr1dFzMP0OB"></div>
                </div>
                <ol class="RD2CmKQFZKidd6 class_card_0" data-testid="list-cards" data-auto-scrollable="true">
                    
                </ol>
              </div>
            </div>
            <div class="contenedor con_event_group_1" ondrop="soltar(event)" ondragover="permitirSoltar(event,1)">
              <div class="contenedor2">
                <div class="bPNGI_VbtbXQ8v" data-testid="list-header" draggable="true">
                  <div class="mKJWg6W_CLHoiO"><h2 dir="auto" class="KLvU2mDGTQrsWG" role="textbox" data-testid="list-name">POR HACER</h2></div>
                  <div class="S10qr1dFzMP0OB"></div>
                </div>
                <ol class="RD2CmKQFZKidd6 class_card_1" data-testid="list-cards" data-auto-scrollable="true">
                  
                </ol>
              </div>
            </div>
            <div class="contenedor con_event_group_2" ondrop="soltar(event)" ondragover="permitirSoltar(event,2)">
              <div class="contenedor2">
                <div class="bPNGI_VbtbXQ8v" data-testid="list-header" draggable="true">
                  <div class="mKJWg6W_CLHoiO"><h2 dir="auto" class="KLvU2mDGTQrsWG" role="textbox" data-testid="list-name">EN PROCESO</h2></div>
                  <div class="S10qr1dFzMP0OB"></div>
                </div>
                <ol class="RD2CmKQFZKidd6 class_card_2" data-testid="list-cards" data-auto-scrollable="true">
                  
                </ol>
              </div>
            </div>
            <div class="contenedor con_event_group_3" ondrop="soltar(event)" ondragover="permitirSoltar(event,3)">
              <div class="contenedor2">
                <div class="bPNGI_VbtbXQ8v" data-testid="list-header" draggable="true">
                  <div class="mKJWg6W_CLHoiO"><h2 dir="auto" class="KLvU2mDGTQrsWG" role="textbox" data-testid="list-name">REALIZADO</h2></div>
                  <div class="S10qr1dFzMP0OB"></div>
                </div>
                <ol class="RD2CmKQFZKidd6 class_card_3" data-testid="list-cards" data-auto-scrollable="true">
                  
                </ol>
              </div>
            </div>
            <div class="contenedor con_event_group_4" ondrop="soltar(event)" ondragover="permitirSoltar(event,4)">
              <div class="contenedor2">
                <div class="bPNGI_VbtbXQ8v" data-testid="list-header" draggable="true">
                  <div class="mKJWg6W_CLHoiO"><h2 dir="auto" class="KLvU2mDGTQrsWG" role="textbox" data-testid="list-name">Acuerdos</h2></div>
                  <div class="S10qr1dFzMP0OB"></div>
                </div>
                <ol class="RD2CmKQFZKidd6 class_card_4" data-testid="list-cards" data-auto-scrollable="true">
                  
                </ol>
              </div>
            </div>
            <div class="contenedor con_event_group_5" ondrop="soltar(event)" ondragover="permitirSoltar(event,5)">
              <div class="contenedor2">
                <div class="bPNGI_VbtbXQ8v" data-testid="list-header" draggable="true">
                  <div class="mKJWg6W_CLHoiO"><h2 dir="auto" class="KLvU2mDGTQrsWG" role="textbox" data-testid="list-name">Incidencias</h2></div>
                  <div class="S10qr1dFzMP0OB"></div>
                </div>
                <ol class="RD2CmKQFZKidd6 class_card_5" data-testid="list-cards" data-auto-scrollable="true">
                  
                </ol>
              </div>
            </div>
            <!-------------------------------->
          </div>
        </div>
        <!---------------------------------------------->
        
        
      </div>
    </div>
  </div>
  
  
</section>



