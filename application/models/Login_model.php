<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class Login_model extends CI_Model 
{
    public function __construct() 
    {
        parent::__construct();
        $this->DB3 = $this->load->database('other3_db', TRUE);
        $this->DB11 = $this->load->database('other11_db', TRUE);
        $this->DB12 = $this->load->database('other12_db', TRUE);

    }
    function updateCatalogo($Tabla,$data,$where){
        $this->db->set($data);
        $this->db->where($where);
        $this->db->update($Tabla);
        //return $id;
    }
    function Insert($Tabla,$data){
        $this->db->insert($Tabla, $data);
        $id=$this->db->insert_id();
        return $id;
    }
    function login($usuario){
        //$strq = "CALL SP_GET_SESSION('".$usuario."');";
        $strq = "SELECT usu.UsuarioID,per.personalId,per.nombre, usu.perfilId, usu.contrasena,usu.sistema,per.apellido_paterno,per.apellido_materno
                FROM usuarios as usu 
                INNER JOIN personal as per on per.personalId=usu.personalId
                where per.estatus=1 and usu.Usuario = '$usuario'";
        $query = $this->DB11->query($strq);
        return $query->result();
    }

    function getMenus($perfil){
        $strq ="SELECT distinct men.MenuId,men.Nombre,men.Icon,men.orden from menu as men, menu_sub as mens, perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.PerfilId='$perfil' ORDER BY men.orden  ASC";
        $query = $this->DB3->query($strq);
        return $query->result();
    } 

    function submenus($perfil,$menu){
        $strq ="SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon from menu_sub as menus, perfiles_detalles as perfd WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='$perfil' and menus.MenuId='$menu' ORDER BY menus.MenusubId ASC";
        $query = $this->DB3->query($strq);
        return $query->result();
    }
    function permisoadmin() {
        $strq = "SELECT 
                    usu.* 
                FROM usuarios as usu 
                inner join personal as per on per.personalId=usu.personalId
                WHERE per.estatus=1 and usu.perfilId=1";
        $query = $this->db->query($strq);
        return $query->result();
    }
    function Insertsession($id,$fecha){
        $strq = "INSERT INTO bitacorasession (personalId,fecha) VALUES ('$id','$fecha')";
        $query = $this->DB12->query($strq);
    }
    function permisoadminarray($arrayperson) {
        $this->db->select('usu.*');
        $this->db->from('usuarios usu');
        $this->db->join('personal per','per.personalId=usu.personalId');
        //$this->db->where(array('per.estatus'=>1,'usu.perfilId'=>1));
        $this->db->where(array('per.estatus'=>1));
        if(count($arrayperson)>0){
            $this->db->group_start();
            foreach($arrayperson as $p){
                //$this->db->or_like('per.personalId',$p);
                $this->db->or_where('per.personalId',$p);
            }
            $this->db->group_end();  
        }
        $query=$this->db->get();
        return $query->result();
    }
    function permisogeneral($pass){
        //log_message('error', "pass: '$pass'");
        $this->idpersonal = $this->session->userdata('idpersonal');
        //$this->perfilid =$this->session->userdata('perfilid');
        $permiso=0;
        $permisos = $this->permisoadminarray(array('1','9','17','18'));//1admin 9 conta 17 julio 18 diana
        foreach ($permisos as $item) {
            $permisotemp=0;

            $usuario=$item->Usuario;

            $verificar = password_verify($pass,$item->contrasena);

            if ($verificar==false) {
                $verificar = password_verify($pass,$item->clavestemp);
                if ($verificar) {
                    $permisotemp=1;
                }
            }
            
            if ($verificar) {
                $permiso=1;
                if($permisotemp==1){
                    $this->updateCatalogo('usuarios',array('clavestemp'=>null),array('UsuarioID'=>$item->UsuarioID));
                }

                $this->Insert('bitacora',array('contenido'=>'Se realizo una autorizacion con el usuario:'.$item->Usuario,'nombretabla'=>'Permisos','idtable'=>0,'tipo'=>'Solicitud permiso','personalId'=>$this->idpersonal));
            }
        }

        return $permiso;

    }
    function permisogeneral_l($pass,$per,$per2){
        //log_message('error', "pass: '$pass'");
        $this->idpersonal = $this->session->userdata('idpersonal');
        //$this->perfilid =$this->session->userdata('perfilid');
        $permiso=0;
        $permisos = $this->permisoadminarray(array('1','9','17','18',$per,$per2));//1admin 9 conta 17 julio 18 diana 12 guadalupe
        foreach ($permisos as $item) {
            $permisotemp=0;

            $usuario=$item->Usuario;

            $verificar = password_verify($pass,$item->contrasena);

            if ($verificar==false) {
                $verificar = password_verify($pass,$item->clavestemp);
                if ($verificar) {
                    $permisotemp=1;
                }
            }
            
            if ($verificar) {
                $permiso=1;
                if($permisotemp==1){
                    $this->updateCatalogo('usuarios',array('clavestemp'=>null,'clavestemp_text'=>null),array('UsuarioID'=>$item->UsuarioID));
                }

                $this->Insert('bitacora',array('contenido'=>'Se realizo una autorizacion con el usuario:'.$item->Usuario,'nombretabla'=>'Permisos','idtable'=>0,'tipo'=>'Solicitud permiso','personalId'=>$this->idpersonal));
            }
        }

        return $permiso;

    }

}