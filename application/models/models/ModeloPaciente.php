<?php
$a=session_id();
if(empty($a)) session_start();
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloPaciente extends CI_Model {
    public function __construct() {
        parent::__construct();
        if (isset($_SESSION['idpersonal'])) {
            $this->idpersonal=$_SESSION['idpersonal'];
        }else{
            $this->idpersonal=0;
        }
    }
    function filaspacientes($suc) {
    	if ($suc==0) {
    		$sucu='';
    	}else{
    		$sucu='and sucursalId='.$suc;
    	}



        $strq = "SELECT COUNT(*) as total FROM pacientes where activo=1 $sucu";
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $total =$row->total;
        } 
        return $total;
    }
    function total_pacientes($por_pagina,$segmento,$suc) {
        if ($suc==0) {
    		$sucu='';
    	}else{
    		$sucu='and sucursalId='.$suc;
    	}

        if ($segmento!='') {
            $segmento=','.$segmento;
        }else{
            $segmento='';
        }
        $strq = "SELECT * FROM pacientes where activo=1 $sucu LIMIT $por_pagina $segmento";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function addpaciente($id,$sucursal,$nombre,$fechanacimiento,$edad,$sexo,$ecivil,$ocupacion,$calle,$num,$int,$col,$muni,$estado,$cp,$lada,$tel,$cel,$whats,$email,$facebook,$motivo,$nutri,$esteti,$ozonot){
        $usu=$this->idpersonal;
        $strq = "CALL SP_ADD_PACIENTE($id,$sucursal,'$nombre','$fechanacimiento',$edad,$sexo,$ecivil,$ocupacion,'$calle','$num','$int','$col','$muni',$estado,'$cp','$lada','$tel','$cel',$whats,'$email','$facebook','$motivo',$nutri,$esteti,$ozonot,$usu)";
        $query = $this->db->query($strq);
        $this->db->close();
        if ($id==0) {
            foreach ($query->result() as $row) {
                $IDs =$row->IDs;
            }
        }else{
            $IDs=$id;
        } 
        
        
        return $IDs;

    }
    function pacientesearch($search,$sucu){
        if ($sucu==0) {
            $sucuselect='';
        }else{
            $sucuselect='and sucursalId='.$sucu;
        }
        $strq=" SELECT * FROM pacientes where activo=1 and nombre like '%".$search."%' ";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function pacienteview($id){
        $strq=" SELECT * FROM pacientes where pacienteId=$id ";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function insertToCatalogo($data,$catalogo){
        return $this->db->insert(''.$catalogo, $data);
    }
    function updateCatalogo($data,$idname,$id,$catalogo){
        $this->db->set($data);
        $this->db->where($idname, $id);
        return $this->db->update(''.$catalogo);
    }
    function getviewtable($table,$id){
        $strq=" SELECT * FROM $table where clienteId=$id LIMIT 1";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
        //return $strq;
    }
    function getviewtableall($table,$id){
        $strq=" SELECT * FROM $table where clienteId=$id";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
        //return $strq;
    }
    function tableultimoregistro($table,$id){
        $strq=" SELECT * FROM $table where clienteId=$id ORDER BY consultanId DESC LIMIT 1";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function tableultimoregistro2($table,$id){
        $strq=" SELECT * FROM $table where clienteId=$id ORDER BY reg DESC LIMIT 1";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function colsultaaleatoria($tipo){
        $strq="SELECT * FROM plan_alimenticios WHERE tipo=$tipo ORDER BY RAND() LIMIT 7";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
}