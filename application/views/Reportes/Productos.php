<style type="text/css">
  .selecte, .selectr, .selecta, .selectc{
    display: none;
  }
</style>
<!-- START CONTENT -->
    <section id="content">
      <!--start container-->
        <div class="container">
          <div class="section">

          <input id="base_url" type="hidden" name="base_url" value="<?php echo base_url(); ?>">

          <div id="table-datatables">
            <br>
                <h4 class="header">Entradas / Salidas</h4>
                <div class="row">
                  <div class="col s12">
                    <div class="card-panel">
                      <div class="row">
                        <div class="col s2">
                          <label>Fecha inicial</label>
                          <input type="date" id="fechainicio" class="form-control-bmz">
                        </div>
                        <div class="col s2">
                          <label>Fecha final</label>
                          <input type="date" id="fechafinal" class="form-control-bmz">
                        </div>
                        <div class="col s2" style="display:none;">
                          <label>Tipo</label>
                          <select class="form-control-bmz browser-default" id="tiporeporte">
                            <!--<option value="0">Todas</option>
                            <option value="1">Venta</option>
                            <option value="2">Venta Combinada</option>
                            <option value="3">Rentas</option>-->
                            <option value="4">Entrada/salidas</option>
                          </select>
                        </div>
                        <div class="col s2" style="display:none;">
                          <label>Cliente</label>
                          <select class="form-control-bmz browser-default" id="idcliente"></select>
                        </div>
                        <div class="col s2">
                          <label>Tipo</label>
                          <select class="browser-default form-control-bmz" id="tipopgg">
                            <option value="0">Detallada</option>
                            <option value="1">Global</option>
                          </select>
                        </div>
                        <div class="col s2 selecttipop">
                          <label>Producto</label>
                          <select class="browser-default form-control-bmz" id="tipop" onchange="tipopselec()">
                            <option value="0">Todos</option>
                            <option value="1">Equipos</option>
                            <option value="2">Refacciones</option>
                            <option value="3">Accesorios</option>
                            <option value="4">Consumibles</option>
                          </select>
                        </div>
                        <div class="col s2 selecte">
                          <label>Equipos</label>
                          <select class="browser-default form-control-bmz" id="selecte"></select>
                        </div>
                        <div class="col s2 selectr">
                          <label>Refacciones</label>
                          <select class="browser-default form-control-bmz" id="selectr"></select>
                        </div>
                        <div class="col s2 selecta">
                          <label>Accesorios</label>
                          <select class="browser-default form-control-bmz" id="selecta"></select>
                        </div>
                        <div class="col s2 selectc">
                          <label>Consumibles</label>
                          <select class="browser-default form-control-bmz" id="selectc"></select>
                        </div>
                        <div class="col s2">
                          <button class="btn-bmz cyan waves-effect waves-light" type="button" onclick="generarreporte()">Generar</button>
                        </div>
                      </div>
                    </div>
                  </div> 
                      
                </div>
                   
              </div>
            </div>  
               </div>
        </section>

