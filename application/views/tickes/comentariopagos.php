<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
    <link href="<?php echo base_url(); ?>public/plugins/note/bootstrap.min.css" rel="stylesheet">
    <script src="<?php echo base_url(); ?>public/plugins/note/jquery-3.5.1.min.js"></script>
    <script src="<?php echo base_url(); ?>public/plugins/note/bootstrap.min.js"></script>
    <!-- include summernote css/js -->
    <link href="<?php echo base_url(); ?>public/plugins/note/summernote.min.css" rel="stylesheet">
    <script src="<?php echo base_url(); ?>public/plugins/note/summernote.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>public/js/ticket.js?v=<?php echo date('YmdGis');?>" ></script>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/plugins/toastr/toastr.css">
    <script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/toastr/toastr.min.js" ></script>
  </head>
  <body>
    <input type="hidden" id="base_url" value="<?php echo base_url();?>">
    <input type="hidden" id="idp" value="<?php echo $id;?>">
    <div class="container">
      <div class="row">
        <div class="col-md-12 summernotetext">
          <label>Descripción</label>
          <textarea class="form-control summernote" style="min-height:200px !important"></textarea>
        </div>
      </div>
      <div class="row">
        <div class="input-field col-md-12">
          <button class="btn btn-success right enviocomentario">Enviar</button>
        </div>
      </div>
    </div>
    
  </body>
</html>