<style type="text/css">
  #table_reporte_2 th{font-size: 12px;padding-left: 10px;}
  #table_reporte_1{font-size: 12px;}
  #table_reporte_4{font-size: 12px;}
</style>
<section id="content">
  <div id="breadcrumbs-wrapper">
    <div class="header-search-wrapper grey lighten-2 hide-on-large-only">
      <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
    </div>
    <div class="container">
      <div class="row">
        <div class="col s10 m6 l6">
          <h5 class="breadcrumbs-title" style="margin-bottom: 18px">Reportes Facturas</h5>
        </div>
      </div>
      <div class="row">
        <div class="col s3">
          <label for="">Empresa</label>
          <select name="tipov" id="tipov" class="browser-default form-control-bmz"><option value="0">Seleccione</option><option value="1" >Alta Productividad</option><option value="2" >D-Impresión</option></select>
        </div>
        
      </div>
    </div>
  </div>
  <!--start container-->
  <div class="container">
    <div class="section">
      <div class="card-panel">
            <div class="row">
              <form id="form_repor1">
                <div class="col s2">
                  <label for="tiporeporte">Fecha inicio</label>
                  <input type="date" id="fechainicio" name="fechainicio" value="<?php echo date('Y-m-d');?>" min="<?php echo date("Y-m-d",strtotime(date('Y-m-d')."- 4 month")); ?>" max="<?php echo date('Y-m-d');?>" class="form-control-bmz" required onchange="calcu_mes(1)">
                </div>
                <div class="col s2">
                  <label for="tiporeporte">Fecha final</label>
                  <input type="date" id="fechafinal" name="fechafinal" value="<?php echo date('Y-m-d');?>" max="<?php echo date('Y-m-d');?>" class="form-control-bmz" required onchange="calcu_mes(1)">
                </div>
                <div class="col s2">
                    <label>Seleccionar cliente:</label>
                    <select class="browser-default form-control-bmz" id="pcliente" onchange="calcu_mes(1)"></select>
                </div>
                <div class="col s2">
                  <label for="tiporeporte">Tipo reporte</label>
                  <select class="browser-default form-control-bmz" name="tiporeporte" id="tiporeporte" >
                       <option value="0">Todas</option> 
                       <option value="1">Ventas</option>
                       <option value="2">Ventas conbinadas</option> 
                       <option value="3">Polizas</option> 
                       <option value="4">Servicio</option> 
                       <option value="5">Rentas</option> 
                  </select>
                </div>
                <div class="col s2">
                  <label for="tiporeporte">Pago</label>
                  <select class="browser-default form-control-bmz" name="tpago" id="tpago" >
                       <option value="0">Todas</option> 
                       <option value="1">Pagadas</option>
                       <option value="2">Pendientes</option>
                  </select>
                </div>
              </form>
              <div class="col s2">
                <button class="btn-bmz cyan waves-effect waves-light" type="button" onclick="generarreporte()">Generar
                </button>
              </div>
            </div>  
      </div>
      <div class="card-panel">
            <div class="row">
              <div class="col s12">
                <h5 class="breadcrumbs-title" style="margin-bottom: 18px;font-size: 16px;">Reporte Comparativo</h5>
              </div>
            </div>
            <div class="row">
              <div class="col s2">
                <label for="tiporeporte">Fecha inicio</label>
                <input type="date" id="rfechainicio" value="<?php echo date('Y-m-d');?>" class="form-control-bmz">
              </div>
              <div class="col s2">
                <label for="tiporeporte">Fecha final</label>
                <input type="date" id="rfechafinal" value="<?php echo date('Y-m-d');?>" class="form-control-bmz">
              </div>
              <div class="col s2" style="display:none;">
                <label for="tiporeporte">Tipo reporte</label>
                <select class="browser-default form-control-bmz" name="rtiporeporte" id="rtiporeporte" ><option value="0">Reporte comparativo</option></select>
              </div>
              <div class="col s2">
                <button class="btn-bmz cyan waves-effect waves-light" type="button" onclick="generarreporter()">Generar
                </button>
              </div>
            </div>  
      </div>
      <div class="card-panel">
            <div class="row">
              <div class="col s12">
                <h5 class="breadcrumbs-title" style="margin-bottom: 18px;font-size: 16px;">Reporte Facturas canceladas</h5>
              </div>
            </div>
            <div class="row">
              <div class="col s2">
                <label for="tiporeporte">Fecha inicio</label>
                <input type="date" id="fcfechainicio" value="<?php echo date('Y-m-d');?>" class="form-control-bmz">
              </div>
              <div class="col s2">
                <label for="tiporeporte">Fecha final</label>
                <input type="date" id="fcfechafinal" value="<?php echo date('Y-m-d');?>" class="form-control-bmz">
              </div>
              
              <div class="col s2">
                <button class="btn-bmz cyan waves-effect waves-light" type="button" onclick="generarreportefc()">Generar
                </button>
              </div>
            </div>  
      </div>
    </div>  
  </div>
</section>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/select2.min.css">
<script src="<?php echo base_url(); ?>/public/js/select2.full.min.js" type="text/javascript"></script>
<script type="text/javascript">
  $(document).ready(function($) {
    $('#pcliente').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una opción',
        allowClear: true,
        ajax: {
            url: '<?php echo base_url();?>Ventasi/searchclientes',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.id,
                        text: element.empresa

                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function (e) {

    });
    
  });
  function generarreporte(){
    var form = $('#form_repor1');
    var emp = $('#tipov option:selected').val();
    var fechainicio = $('#fechainicio').val();
    var fechafinal = $('#fechafinal').val();
    var tiporeporte = $('#tiporeporte').val();
    var tpago = $('#tpago option:selected').val();
    var cli=$('#pcliente option:selected').val()==undefined?0:$('#pcliente option:selected').val();
    if(form.valid()){
      window.open('<?php echo base_url();?>Facturas/exportar/'+fechainicio+'/'+fechafinal+'/'+tiporeporte+'/'+cli+'/'+emp+'/'+tpago, '_blank');
    }
  }
  function generarreporter(){
    var emp = $('#tipov option:selected').val();
    var fechainicio = $('#rfechainicio').val();
    var fechafinal = $('#rfechafinal').val();
    var tiporeporte = $('#rtiporeporte').val();
    window.open('<?php echo base_url();?>Facturas/exportarrc/'+fechainicio+'/'+fechafinal+'/'+tiporeporte+'/'+emp, '_blank');
  }
  function generarreportefc(){
    var emp = $('#tipov option:selected').val();
    var fechainicio = $('#fcfechainicio').val();
    var fechafinal = $('#fcfechafinal').val();
    var url='<?php echo base_url();?>Facturas/exportarfc/'+fechainicio+'/'+fechafinal+'/'+emp;
        //url+='?viewexcel=1'
    window.open(url, '_blank');
  }
  function calcu_mes(report){
    if(report==1){
      var cli=$('#pcliente option:selected').val();
      if(cli>0){
        var menosmeses=12;
      }else{
        var menosmeses=4;
      }
      var fini = $('#fechafinal').val();
      var idfechaini='#fechainicio';
    }




      var fechaFinal = moment(fini, "YYYY-MM-DD").subtract(menosmeses, 'months');

      // Formatear la fecha en formato YYYY-MM-DD
      var fechaInicialFormateada = fechaFinal.format("YYYY-MM-DD");

      // Actualizar el elemento con la fecha inicial
      console.log(fechaInicialFormateada);
      $(idfechaini).prop('min',fechaInicialFormateada);
      $(idfechaini).prop('max',fini);
  }
</script>