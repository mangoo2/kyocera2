<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
<!--
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/formv/formValidation.min3f0d.css?v2.2.0">
-->
		<link rel="icon" href="<?php echo base_url(); ?>app-assets/images/favicon/favicon_kyocera.png" sizes="32x32">
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<title>Prefactura</title>
		<!-- jQuery Library -->
    	<script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/jquery-3.3.1.min.js"></script>
    	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <link href="<?php echo base_url(); ?>app-assets/vendors/sweetalert/dist/sweetalert.css" type="text/css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>public/css/fontawesome/css/all.css" type="text/css" rel="stylesheet">
		<style type="text/css">
			.tdstitle{background: #c1bcbc;font-weight: bold;color: white;}
			html{-webkit-print-color-adjust: exact;}
			input{padding: 0px !important;height: 28px !important;}
			select{padding: 0px !important;height: 28px !important;font-size: 11px !important;}
			@media print{
				input{background: transparent;border: 0 !important;}
				.buttonenvio{display: none;}
				html{-webkit-print-color-adjust: exact;}
				.tdstitle{background: #c1bcbc;font-weight: bold;color: white;}
				.buttoimprimir{display: none;}
				select{background: transparent; border: 0 !important; -moz-appearance: none; -webkit-appearance: none;appearance: none;}
				.form-control:disabled, .form-control[readonly] {background-color: transparent; opacity: 1;}
			}
			table td{text-align: center; font-size: 11px;}
			option:disabled{
				background-color: #dadada;
			}
		</style>
				<?php $finalizadogeneral=1;?>

	</head>
	<body>
		<?php 
			$bloquearsaveequipo=0;
			$bloquearsaveserie=0;
		?>
		<input type="hidden" id="base_url" name="base_url" value="<?php echo base_url(); ?>">
		<form method="post" id="formprefactura">
			<input type="hidden" id="ventaId" name="ventaId" value="<?php echo $ventaId;?>">
			
			
			<table border="1" width="100%" class="table table-bordered table-striped">
				<thead>
				<tr>
					<th style="font-size: 11px; ">Cantidad</th>
					<th style="font-size: 11px; ">No. de Parte</th>
					<th style="font-size: 11px; ">Modelo</th>
					<th style="font-size: 11px; ">Serie/folio</th>
					<th style="font-size: 11px; ">Precio Unitario</th>
					<th style="font-size: 11px; " >Total</th>
					<th></th>
				</tr>
				</thead>
				<tbody>
					<?php 
						$totalgeneral=0;
                        			// Tabla de polizas creadas detalles  		
						foreach ($rentaventas->result() as $item) { 
							//=================================
								if($item->entregado_status>0){
									$btn_status='btn-secondary';
									if($item->entregado_status>1){
										$btn_status='btn-success';	
									}
									$info_btn='<br>'.$item->personal.' '.$item->apellido_paterno.'<br>'.$item->entregado_fecha;
								}else{
									$finalizadogeneral=0;
									$btn_status='btn-primary';
									$info_btn='';
								}
							//=================================
							$rowequipos = $item->id;
							$equipo_id = $item->idEquipo;
							$renta_id = $item->idRenta;
							$bodega=$item->bodega;	
							//$totalgeneral=$totalgeneral+($item->precio*$item->cantidad);
							$totalgeneral=$totalgeneral+0;
							$datosseries = $this->ModeloCatalogos->rentaventasdseries($item->id);
							$usado=0;
							$vusado='';
							foreach($datosseries->result() as $items) { 
								$usado=$items->usado;
							}
							if($usado==1){
								$vusado='-mc';
							}
							if($item->serie_bodega==2){
								$vusado='-mc';
							}
							?>
							<tr>
								<td style="font-size: 11px; text-align: center;"><?php echo $item->cantidad;?></td>
								<td style="font-size: 11px; "><?php echo $item->noparte;?></td>
								<td style="font-size: 11px; "><b><?php echo $item->modelo.$vusado;?></b></td>
								<td style="font-size: 11px; ">
									<?php 
                                                			 
									foreach($datosseries->result() as $items) { 
										echo $items->serie.'<br>';		
												    
									} ?>
								</td>
										
								<td style="font-size: 11px; text-align: center;"  >$<?php echo number_format(0,2,'.',',');?></td>
								<td style="font-size: 11px; text-align: center;" >$<?php echo number_format(0,2,'.',',');?></td>
								<td><button type="button" class="btn <?php echo $btn_status;?>" onclick="entregas(<?php echo $item->id; ?>,9,<?php echo $item->bodegaId; ?>)"><i class="fas fa-handshake fa-fw"></i></button><?php echo $info_btn;?></td>
							</tr>

					<?php	$rentaequipos_consumible = $this->ModeloCatalogos->rentaequipos_consumible($renta_id,$equipo_id,$item->id);
						foreach ($rentaequipos_consumible->result() as $item) {
							//=================================
								if($item->entregado_status>0){
									$btn_status='btn-secondary';
									if($item->entregado_status>1){
										$btn_status='btn-success';	
									}
									$info_btn='<br>'.$item->personal.' '.$item->apellido_paterno.'<br>'.$item->entregado_fecha;
								}else{
									$finalizadogeneral=0;
									$btn_status='btn-primary';
									$info_btn='';
								}
							//=================================
							?>
								<tr>
									<td style="font-size: 11px; text-align: center;"><?php echo $item->cantidad;?></td>
									<td style="font-size: 11px; "><?php echo $item->parte;?></td>
									<td style="font-size: 11px; "><?php echo $item->modelo;?></td>
									<td style="font-size: 11px; "><?php 
													//============================ cuando ya todos tengan relacion se quitara
													$where = array('idrenta'=>$renta_id,'idconsumible'=>$item->id_consumibles);
                                    									$contrato_folio = $this->ModeloCatalogos->db2_getselectwheren('contrato_folio',$where);
                                    									foreach ($contrato_folio->result() as $itemx) {
                                    										if($itemx->idrelacion>0){
                                       
										                                }else{
										                                     echo $itemx->foliotext.',';
										                                }
									                                }
									                                //============================ cuando ya todos tengan relacion remplazara a la anterior
									                                $contrato_folio2 = $this->ModeloCatalogos->obtenerfoliosconsumiblesrenta($renta_id,$item->id_consumibles,$item->id); 
									                                foreach ($contrato_folio2->result() as $itemx) {
                                    										if($itemx->idrelacion>0){

									                                      		echo $itemx->foliotext;
									                              		}
									                                }
												?></td>
									
									<td style="font-size: 11px; text-align: center;"  >$<?php echo number_format(0,2,'.',',');?></td>
									<td style="font-size: 11px; text-align: center;" >$<?php echo number_format(0,2,'.',',');?></td>
									<td><button type="button" class="btn <?php echo $btn_status;?>" onclick="entregas(<?php echo $item->id;?>,11,<?php echo $item->bodegaId; ?>)"><i class="fas fa-handshake fa-fw"></i></button><?php echo $info_btn;?></td>
								</tr>
							<?php 
						}
						$rentaequipos_accesorios = $this->ModeloCatalogos->rentaequipos_accesorios($renta_id,$equipo_id,$rowequipos);
						foreach ($rentaequipos_accesorios->result() as $item) { 
							//=================================
								if($item->entregado_status>0){
									$btn_status='btn-secondary';
									if($item->entregado_status>1){
										$btn_status='btn-success';	
									}
									$info_btn='<br>'.$item->personal.' '.$item->apellido_paterno.'<br>'.$item->entregado_fecha;
								}else{
									$finalizadogeneral=0;
									$btn_status='btn-primary';
									$info_btn='';
								}
							//=================================
						?>
		                         	<tr>
		                                   <td style="font-size: 11px; text-align: center;"><?php echo $item->cantidad;?></td>        
		                                   <td style="font-size: 11px; "><?php echo $item->no_parte;?></td>    
		                                   <td style="font-size: 11px; "><?php echo $item->nombre;?></td>    
		                                   <td style="font-size: 11px; "><?php echo $item->series;?></td>    
		                                   <td style="font-size: 11px; ">$ 0.0</td>
		                                   <td style="font-size: 11px; ">$ 0.0</td> 
		                                   <td><button type="button" class="btn <?php echo $btn_status;?>" onclick="entregas(<?php echo $item->id_accesoriod; ?>,10,<?php echo $item->bodegaId; ?>)"><i class="fas fa-handshake fa-fw"></i></button><?php echo $info_btn; ?></td>   
		                                </tr>
                                 		<?php
                          			}
					}
					?>
						
					
				</tbody>
				<tfoot>
					<tr>
						<td style="font-size: 11px; " >Subtotal</td>
						<td style="font-size: 11px; " >$<?php echo number_format($totalgeneral,2,'.',',');?></td>
						<td style="font-size: 11px; ">Iva</td>
						<td style="font-size: 11px; " >$<?php echo number_format($totalgeneral*0.16,2,'.',',');?></td>
						<td style="font-size: 11px; ">Total</td>
						<td style="font-size: 11px; " >$<?php echo number_format(($totalgeneral*1.16),2,'.',',');?></td>
						<td></td>
					</tr>
				</tfoot>

			</table>
		
		</form>
		 
		
	</body>
	<script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/sweetalert/dist/sweetalert.min.js"></script>
	<!--
<script src="<?php echo base_url(); ?>app-assets/vendors/js/formvalidation/formValidation.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>app-assets/vendors/js/formvalidation/framework/bootstrap.min.js"></script>
-->
	<link href="<?php echo base_url(); ?>app-assets/vendors/confirm/jquery-confirm.min.css" type="text/css" rel="stylesheet">
	<script src="<?php echo base_url(); ?>app-assets/vendors/confirm/jquery-confirm.min.js"></script>
	<script type="text/javascript">
		function entregas(idrow,tipo,bodegai){
			var htmlselect=$('#personalresult').html();
			var htmlselectb=$('#bodegasresult').html();
			setTimeout(function () {
  				$("#bodegaselect option[value='"+bodegai+"']").attr("selected", true);
			},1000);
			$.confirm({
		        boxWidth: '35%',
		        useBootstrap: false,
		        icon: 'fa fa-warning',
		        title: 'Atención!',
		        content: '¿Desea registrar la entrega?<br>seleccione el tecnico<br><select id="tecnicoselect" class="form-control">'+htmlselect+'</select><br>seleccione bodega<br><select id="bodegaselect" class="form-control">'+htmlselectb+'</select>',
		        type: 'red',
		        typeAnimated: true,
		        buttons:{
		            confirmar: function (){
		                var tec=$('#tecnicoselect option:selected').val();
		                var bode=$('#bodegaselect option:selected').val();
	                     $.ajax({
	                        type:'POST',
	                        url: "<?php echo base_url()?>index.php/Generales/entregas",
	                        data: {
	                            id:idrow,
	                            table:tipo,
	                            personal:tec,
	                            bode:bode
	                        },
	                        success: function (response){
	                            location.reload();
	                        },
	                        error: function(response){
	                            $.alert({
	                                    boxWidth: '30%',
	                                    useBootstrap: false,
	                                    title: 'Error!',
	                                    content: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema'});   
	                             
	                        }
	                    });
		                    
		            },
		            cancelar: function (){
		                
		            }
		        }
		    });
			bloqueoseries();
		}
		function bloqueoseries(){
			setTimeout(function(){ 
				$.ajax({
		        type:'POST',
		        url: '<?php echo base_url()?>AsignacionesSeries/bodegasview',
		        data: {
		            accesorio: 0
		            },
		            async: false,
		            statusCode:{
		                404: function(data){
		                    swal("Error", "404", "error");
		                },
		                500: function(){
		                    swal("Error", "500", "error"); 
		                }
		            },
		            success:function(data){
		                var array = $.parseJSON(data);
		                $.each(array, function(index, item) {
		                    if(parseInt(item.tipov)==parseInt(1)){
		                    	$('.bodega_'+item.bodegaId).attr('disabled',false);
		                    }else{
		                        $('.bodega_'+item.bodegaId).attr('disabled',true);
		                    }
		                
		            });
		            }
		        });
		        console.log('sssss');
			}, 1000);
			
		    
		}
	</script>
</html>
<select id="personalresult" style="display: none;">
	<?php 
	$resultstec=$this->Configuraciones_model->view_session_searchTecnico();
	foreach ($resultstec as $item) { ?>
     	<option value="<?php echo $item->personalId;?>"><?php echo $item->tecnico;?></option>
    <?php } ?>
</select>
<?php 
	if($finalizadogeneral==1){
		$this->ModeloCatalogos->updateCatalogo('rentas',array('e_entrega'=>1,'entregado_fecha'=>date('Y-m-d H:i:s')),array('id'=>$ventaId));
	}
?>
<select id="bodegasresult" style="display: none;">
	<?php foreach ($resultbodegas->result() as $item) { ?>
		<option value="<?php echo $item->bodegaId;?>" class="bodega_<?php echo $item->bodegaId;?>" data-stock="1"><?php echo $item->bodega;?></option>
	<?php } ?>
</select>