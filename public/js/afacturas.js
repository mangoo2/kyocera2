var base_url = $('#base_url').val();
var total=0;
var blockearfact=1;
var trunquearredondear=0;//0 redondeo 1 trunquear (dos dijitos)
var bloqueoabierto=0;
$(document).ready(function(){
	$('.chosen-select').chosen({width: "91%"});
    //$('#FormaPago').select2({width: 'resolve'});
    $('#rfc').select2({width: 'resolve'});
    $('#MetodoPago').select2({width: 'resolve'});
    $('#uso_cfdi').select2({width: 'resolve'});
    $('#moneda').select2({width: 'resolve'});
    $('#sconseptosat').select2({width: 'resolve'});
    functionsunidadsat();

	$('#sunidadsat').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un Unidad',
          ajax: {
            url: base_url+'Configuracionrentas/searchunidadsat',
            dataType: "json",
            data: function (params) {
              var query = {
                search: params.term,
                type: 'public'
              }
              return query;
          },
          processResults: function(data){
            var clientes=data;
            var itemscli = [];
            data.forEach(function(element) {
                    itemscli.push({
                        id: element.Clave,
                        text: element.Clave+' / '+element.nombre
                    });
                });
                return {
                    results: itemscli
                };        
          },  
        }
    });
    /*
	$('#sconseptosat').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un Consepto',
          ajax: {
            url: base_url+'Configuracionrentas/searchconceptosat',
            dataType: "json",
            data: function (params) {
              var query = {
                search: params.term,
                type: 'public'
              }
              return query;
          },
          processResults: function(data){
            var clientes=data;
            var itemscli = [];
            data.forEach(function(element) {
                    itemscli.push({
                        id: element.Clave,
                        text: element.nombre
                    });
                });
                return {
                    results: itemscli
                };        
          },  
        }
    });
    */
    select2idcliente();
    
    $('input:radio[name=tiporelacion]').click(function(event) {
    	var tiporelacion = $('input:radio[name=tiporelacion]:checked').val();
        var tipov = $('#tipov option:selected').val();
        if(tipov>0){
        	if (tiporelacion>0) {
        		$.ajax({
    		        type:'POST',
    		        url: base_url+"Facturaslis/obtenerventas",
    		        data: {
    		        	cliente:$('#idcliente').val(),
    		        	tipo: tiporelacion,
                        tipov:tipov
    		        },
    		        success: function (data){
    		        	$('.infoventas').html(data); 
    		        }
    		    });
        	}else{
        		$('.infoventas').html('');
        		$('#ventaviculada').val(0);
        	}
        }else{
            toastr["warning"]("Seleccione una empresa");
        }
    });
    $('.registrofac').click(function(event) {
        $( ".registrofac" ).prop( "disabled", true );
        setTimeout(function(){ 
             $(".registrofac" ).prop( "disabled", false );
        }, 10000);
        registrofac(1,0);
	});
    $('.registrofac_preview').click(function(event) {
        var tipov = $('#tipov option:selected').val();
        if(tipov>0){
        registrofac_preview();
        }else{
            alertfunction('Atención!','Seleccione la Empresa'); 
        }
    });
    $('#facturarelacionada').click(function(event) { //agregado version 4.0
        if($('#facturarelacionada').is(':checked')){
            $('.divfacturarelacionada').show('show');
        }else{
            $('.divfacturarelacionada').hide('show');
            $('#uuid_r').val('');
        }
        /* Act on the event */
    });
    $('#FormaPago').change(function(event) {
        if($('#FormaPago').val()=='PPD'){
            $('#MetodoPago option').prop('disabled',true);
            $('#MetodoPago').val('PorDefinir').trigger('change');
            $('#MetodoPago option[value=PorDefinir]').prop('disabled',false);
            $('#MetodoPago').select2();
        }
        if($('#FormaPago').val()=='PUE'){
            $('#MetodoPago option').prop('disabled',false);
            $('#MetodoPago option[value=PorDefinir]').prop('disabled',true);
            $('#MetodoPago').select2();
        }
    });
    $('#infoanticipo').click(function(event) {
        if($('#infoanticipo').is(':checked')){
            $('#FormaPago').val('PUE');
            $('#TipoRelacion').val('07');
            $('#sconseptosat').val('84111506').select2();
            $('#scantidad').val(1).prop('readonly',true);
            $('#sunidadsat').html('<option value="ACT" selected>ACT / Actividad</option>');
            functionsunidadsat();
            $('#sdescripcion').val('Anticipo del bien o servicio').prop('readonly',true);
        }else{
            $('#TipoRelacion').val('01');
            $('#scantidad').val(1).prop('readonly',false);
            $('#sunidadsat').html('');
            functionsunidadsat();
            $('#sdescripcion').val('').prop('readonly',false);
        }
    });
    $('#FormaPago').change();
});
function select2idcliente(){
    $('#idcliente').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una opción',
        allowClear: true,
        ajax: {
            url: base_url+'Ventasi/searchclientes',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.id,
                        text: element.empresa

                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function (e) {
        var data = e.params.data;
        obtenerrfc(data.id,0)
    });
}
function functionsunidadsat(){
    $('#sunidadsat').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un Unidad',
          ajax: {
            url: base_url+'Configuracionrentas/searchunidadsat',
            dataType: "json",
            data: function (params) {
              var query = {
                search: params.term,
                type: 'public'
              }
              return query;
          },
          processResults: function(data){
            var clientes=data;
            var itemscli = [];
            data.forEach(function(element) {
                    itemscli.push({
                        id: element.Clave,
                        text: element.Clave+' / '+element.nombre
                    });
                });
                return {
                    results: itemscli
                };        
          },  
        }
    });
}
function registrofac(save,saveante){
    var tipov = $('#tipov option:selected').val();
    $( ".registrofac" ).prop( "disabled", true );
    setTimeout(function(){ 
         $(".registrofac" ).prop( "disabled", false );
    }, 10000);
        blockearfact=1;
        var form =$('#validateSubmitForm');
        var valid =form.valid();
        if (valid) {
            
            var datos = form.serialize();
            var productos = $("#table_conceptos tbody > tr");
            //==============================================
                var DATAa  = [];
                productos.each(function(){         
                    item = {};                    
                    item ["Cantidad"]   = $(this).find("input[id*='cantidad']").val();
                    item ["Unidad"]  = $(this).find("select[id*='unidadsat']").val();
                    console.log($(this).find("select[id*='unidadsat'] option:selected").val());
                    if ($(this).find("select[id*='unidadsat'] option:selected").val()==null || $(this).find("select[id*='unidadsat'] option:selected").val()=='') {
                        blockearfact=0;
                    }
                    item ["servicioId"]  = $(this).find("select[id*='conseptosat'] option:selected").val();
                    console.log($(this).find("select[id*='conseptosat'] option:selected").val());
                    if ($(this).find("select[id*='conseptosat'] option:selected").val()==null || $(this).find("select[id*='conseptosat'] option:selected").val()=='') {
                        blockearfact=0;
                    }
                    item ["Descripcion"]  = $(this).find("select[id*='conseptosat'] option:selected").text();
                    item ["Descripcion2"]  = $(this).find("input[id*='descripcion']").val();
                    item ["Cu"]  = $(this).find("input[id*='precio']").val();
                    if ($(this).find("input[id*='precio']").val()>0) { }else{
                        blockearfact=0;
                    }
                    item ["descuento"]  = $(this).find("input[id*='descuento']").val();
                    item ["Importe"]  = $(this).find("input[id*='subtotal']").val();
                    item ["iva"]  = $(this).find("input[id*='tiva']").val();
                    DATAa.push(item);
                });
                INFOa  = new FormData();
                aInfoa   = JSON.stringify(DATAa);
            //========================================
            var productoslength=productos.length;
            if (blockearfact==0) {
                productoslength=0;
            }
            if (productoslength>0) {
                $('body').loading({theme: 'dark',message: 'Timbrando factura...'});

                var tiporelacion = $('input:radio[name=tiporelacion]:checked').val();
                //====================================================================
                //var ventaviculada = $('#ventaviculada').val();
                //var ventaviculadatipo = $('#ventaviculadatipo').val();
                var ventaviculadatipo = '';
                //==============================================
                    var DATAvv  = [];
                    var tablevv = $("#vrtable tbody > tr");
                    tablevv.each(function(){         
                        item = {};                    
                        item ["tiporelacion"]   = $(this).find("input[id*='tiporelacion']").val();
                        item ["ventaviculadatipo"]   = $(this).find("input[id*='ventaviculadatipo']").val();
                        item ["ventaviculada"]   = $(this).find("input[id*='ventaviculada_']").val();
                        DATAvv.push(item);
                    });
                    //INFOa  = new FormData();
                    //aInfoa   
                    var ventaviculada = JSON.stringify(DATAvv);
                //========================================
                //====================================================================
                var Subtotal=$('#Subtotal').val();
                var iva=$('#iva').val();
                var visr= $('#isr').val();
                var vriva= $('#ivaretenido').val();
                var v5millar= $('#5almillarval').val();
                var outsourcing= $('#outsourcing').val();

                var total=$('#total').val();

                var f_r = $('#facturarelacionada').is(':checked')==true?1:0; //agregados version 4.0
                var f_r_t = $('#TipoRelacion option:selected').val();
                var f_r_uuid = $('#uuid_r').val();
                var des=$('#descuentof').val();
                               
                datos=datos+'&subtotal='+Subtotal+'&iva='+iva+'&visr='+visr+'&vriva='+vriva+'&v5millar='+v5millar+'&outsourcing='+outsourcing+'&total='+total+'&conceptos='+aInfoa+'&tiporelacion='+tiporelacion+'&ventaviculada='+ventaviculada+'&ventaviculadatipo='+ventaviculadatipo+'&save='+save+'&saveante='+saveante+'&f_r='+f_r+'&f_r_t='+f_r_t+'&f_r_uuid='+f_r_uuid+'&des='+des;
                $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Configuracionrentas/generafacturarabierta",
                    data: datos,
                    success: function (response){
                        console.log(response);
                        var array = $.parseJSON(response);
                        if (array.resultado=='error') {
                            /*
                            swal({    title: "Error "+array.CodigoRespuesta+"!",
                                          text: array.MensajeError,
                                          type: "warning",
                                          showCancelButton: false
                             });
                            */
                            retimbrar(array.facturaId,0);
                        }else{
                            if(save==0){
                                var textofactura='Se ha Guardado la factura';
                            }else{
                                var textofactura='Se ha creado la factura';
                            }
                            swal({
                                title:"Éxito!", 
                                text:textofactura, 
                                type:"success"
                                },
                                function(){
                                    $(location).attr('href',base_url+'Facturaslis?tipov='+tipov);
                                });
                        }
                        //setTimeout(function(){ 
                            //window.location.href = base_url+"index.php/Facturaslis/"; 
                        //}, 5000);
                        
                    },
                    error: function(response){
                        notificacion(1);
                        
                    }
                });
            }else{
                alertfunction('Atención!','Agregar por lo menos un concepto');
                $('body').loading('stop');
            }
        }else{
            alertfunction('Atención!','Faltan campos obligatorios');
        }
}
function calculartotal(){
	var cantidad=$('#scantidad').val()==''?0:$('#scantidad').val();
	var precio=$('#sprecio').val()==''?0:$('#sprecio').val();
	total=parseFloat(cantidad)*parseFloat(precio);
	//total=total.toFixed(2);
    total=redondear(total, 2);

	//$('.montototal').html(new Intl.NumberFormat('es-MX').format(total))
}
function agregarconcepto(){
    var rfc = $('#rfc option:selected').text();
    
    if(bloqueoabierto==1){
        var precio = $('#sprecio').val();
        var unidadt = $('#sunidadsat option:selected').text();
    	if(precio>0 && unidadt!=''){
    		var cantidad = $('#scantidad').val();
    		var unidad = $('#sunidadsat option:selected').val();
            var unidadt = $('#sunidadsat option:selected').text();
            var concepto = $('#sconseptosat option:selected').val();
            var conceptot = $('#sconseptosat option:selected').text();
    		var descripcion = $('#sdescripcion').val();
    		var aiva = $('#aplicariva').is(':checked')==true?1:0;
            var rfc = $('#rfc option:selected').text();
            if(rfc=='XAXX010101000'){
                if($('#pg_global').is(':checked')){
                    if(unidad!='ACT'){
                        alertfunction('Atención!','la <b>Unidad SAT</b> para PUBLICO EN GENERAL debera de ser <b>ACT</b> "Actividad" ');
                    }
                    if(concepto!='01010101'){
                        alertfunction('Atención!','la <b>Concepto SAT</b> para PUBLICO EN GENERAL debera de ser <b>01010101</b> "No existe en el catálogo" ');
                    }
                    if(descripcion!='Venta'){
                        alertfunction('Atención!','la <b>Descripción</b> para PUBLICO EN GENERAL debera de ser <b>Venta</b>');
                    }
                }
            }

    		agregarconceptovalor(cantidad,unidad,unidadt,concepto,conceptot,descripcion,precio,aiva,0,0);
    		$('#sdescripcion').val('');
    		$('#scantidad').val(1);
    		$('#sprecio').val(0);
    	}else{
            alertfunction('Atención!','Precio en cero y/o unidad sin seleccionar');
        }
    }else{
        permisoparafacturaabierta();
    }
    validarparaanticipos();
}
var rowcobrar=0;
function agregarconceptovalor(cantidad,unidad,unidadt,concepto,conceptot,descripcion,precio,aiva,idv,descuento){

	var subtotal=parseFloat(cantidad)*parseFloat(precio);
	if (aiva==1) {
		var siva =subtotal*0.16;
        if(trunquearredondear==0){
            //siva = siva.toFixed(4);
            if(idv>0){
                siva=redondear(siva, 2);
            }else{
                siva=redondear(siva, 4);
            }
            
        }else{
            siva=Math.floor(siva * 100) / 100;
        }
		
	}else{
		var siva =0.00;
	}
	
	var subtotal=parseFloat(subtotal)+parseFloat(siva);
		//subtotal=subtotal.toFixed(2);
        subtotal=redondear(subtotal, 4);
	var datoscobrar ='	<tr class="rowcobrar_'+rowcobrar+'">\
                          <td>\
                            <input type="number" name="cantidad" id="cantidad" class="cantidad_row_'+rowcobrar+' form-control-bmz" value="'+cantidad+'" style="background: transparent !important; border: 0px !important;" readonly\
                                data-venta="'+idv+'" >\
                          </th>\
                          <th >\
                            <select name="sunidadsat" id="unidadsat" class="browser-default form-control-bmz unidadsat">\
                            	<option value="'+unidad+'">'+unidadt+'</option>\
                            </select>\
                          </th>\
                          <th >\
                            <select name="conseptosat" id="conseptosat" class="browser-default form-control-bmz conseptosat">\
                            	<option value="'+concepto+'">'+conceptot+'</option>\
                            </select>\
                          </th>\
                          <th >\
                            <input type="text" name="descripcion" id="descripcion" class="form-control-bmz" value="'+descripcion+'" style="background: transparent !important; border: 0px !important;" onclick="habilitardescripcion('+rowcobrar+')" readonly>\
                          </th>\
                          <th >\
                            <input type="number" name="precio" id="precio" class="preciorow form-control-bmz precio_'+rowcobrar+'" value="'+precio+'" style="background: transparent !important; border: 0px !important;" readonly>\
                          </th>\
                          <th>\
                            <input type="number"\
                            name="descuento"\
                            id="descuento"\
                            data-diva="'+aiva+'"\
                            class="form-control-bmz cdescuento cdescuento_'+rowcobrar+'"\
                            onclick="activardescuento('+rowcobrar+')"\
                            value="'+descuento+'" style="width:100px" readonly>\
                          </th>\
                          <th>\
                          	<input type="number" name="subtotal" id="subtotal" class="form-control-bmz csubtotal csubtotal_'+rowcobrar+'" value="'+subtotal+'" style="background: transparent !important; border: 0px !important;" readonly>\
                          </th>\
                          <th >\
                            <input type="number"  id="tiva" value="'+siva+'" class="form-control-bmz siva tiva_'+rowcobrar+'" style="background: transparent !important; border: 0px !important;" readonly>\
                          </th>\
                          <th >\
                            <a class="waves-effect red btn-bmz" onclick="deleteconcepto('+rowcobrar+')"><i class="fas fa-trash-alt"></i></a>\
                          </th>\
                        </tr>';

	$('.addcobrar').append(datoscobrar);
    if(descuento>0){
        var desrow=rowcobrar;
        setTimeout(function(){ 
            calculardescuento(desrow);
            console.log('ajuste con el descuento'+desrow);
        }, 2000);
        setTimeout(function(){ 
            calculardescuento(desrow);
            console.log('ajuste con el descuento'+desrow);
        }, 3000);
    }
	rowcobrar++;
	calculartotales();
}
function deleteconcepto(row){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Confirma la eliminación de la partida?,Se necesita permisos de administrador<br>'+
                '<input type="text" placeholder="Contraseña" id="contrasena3" name="contrasena3" class="name form-control" required/>',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var pass=$("input[name=contrasena3]").val();
                if (pass!='') {
                     $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Sistema/solicitarpermiso",
                        data: {
                            pass:pass
                        },
                        success: function (response){
                                var respuesta = parseInt(response);
                                if (respuesta==1) {
                                    //==================================
                                        $('.rowcobrar_'+row).remove();
                                        calculartotales();
                                        removeventaidtable();
                                        validarparaanticipos();
                                    //=================================
                                }else{
                                    notificacion(2);
                                }
                        },
                        error: function(response){
                            notificacion(1);
                        }
                    });
                    
                }else{
                    notificacion(0);
                }
                
            },
            cancelar: function () 
            {
                
            }
        }
    });
    setTimeout(function(){
            new MaskedPassword(document.getElementById("contrasena3"), '\u25CF');
    },500);
	
}
function calculartotales(){
	var totales = 0;
    /*
    $(".preciorow").each(function() {
        var vtotales = parseFloat($(this).val());
        totales += Number(vtotales);
    });
    */
    var TABLApr   = $("#table_conceptos tbody > tr");
        TABLApr.each(function(){ 
            var vtotales = parseFloat($(this).find("input[id*='cantidad']").val())*parseFloat($(this).find("input[id*='precio']").val());
                vtotales = redondear(vtotales,2);
                //console.log(redondear(vtotales,2));
                //vtotales = vtotales.toFixed(2);
                //console.log(vtotales);
                //console.log(Math.ceil(vtotales));
                totales += Number(vtotales);
        });
    //=================================================
    var ivas = 0;
    $(".siva").each(function() {
        var vivas = parseFloat($(this).val());
        ivas += Number(vivas);
    });
    var dest = 0;
    $(".cdescuento").each(function() {
        var destc = parseFloat($(this).val());
        dest += Number(destc);
    });


    var subtotalc = parseFloat(totales);
    	subtotalc = subtotalc.toFixed(2);
    	//ivas=ivas.toFixed(2);
        ivas=redondear(ivas, 2);


    	//totales=totales.toFixed(2);
        totales=redondear(totales, 2);
        ivas = parseFloat(ivas);
        //ivas =ivas.toFixed(2);
        ivas=redondear(ivas, 2);

    var Subtotalinfo=parseFloat(subtotalc);
    var subtotalc=parseFloat(subtotalc);
    
    if ($('#risr').is(':checked')) {
        var v_isr=Subtotalinfo*0.1;
            //v_isr =v_isr.toFixed(2);
            v_isr=redondear(v_isr, 2);
            $('#isr').val(v_isr);
            totales=totales-v_isr;
            totales=totales.toFixed(2);
    }else{
        var v_isr=0;
        $('#isr').val(0.00);
    }
    if ($('#riva').is(':checked')) {
        var v_riva=Subtotalinfo*0.106666;
            v_riva =v_riva.toFixed(2);
            $('#ivaretenido').val(v_riva);
            totales=totales-v_riva;
            totales=totales.toFixed(2);
    }else{
        $('#ivaretenido').val(0.00);
    }
    if ($('#5almillar').is(':checked')) {
        var v_5millar=(Subtotalinfo/1000)*5;
            v_5millar =v_5millar.toFixed(2);
            $('#5almillarval').val(v_5millar);
            totales=totales-v_5millar;
            totales=totales.toFixed(2);
    }else{
        $('#5almillarval').val(0.00);
    }
    if ($('#aplicaout').is(':checked')) {
        var aplicaout = Subtotalinfo*0.06;
            aplicaout = aplicaout.toFixed(2);
        
            $('#outsourcing').val(aplicaout);
            totales=totales-aplicaout;
            totales=totales.toFixed(2);
    }else{
        $('#outsourcing').val(0.00);
    }

    totales=totales-parseFloat(dest)+parseFloat(ivas);
    $('#Subtotalinfo').val(Subtotalinfo);
    $('#Subtotal').val(subtotalc);
    $('#descuentof').val(dest);
    $('#iva').val(ivas);//productivo
    //=========================== temporal para que le cuadre
    /*
    var iva_temp=parseFloat(subtotalc)*0.16;
    console.log('iva:'+iva_temp);
    totales=parseFloat(subtotalc)+iva_temp;
    $('#iva').val(iva_temp.toFixed(2));
    */
    //==========================================
    //$('#total').val(totales.toFixed(2));
    $('#total').val(redondear(totales, 2));

}
function obtenerrfc(cliente,rfc){
	$.ajax({
        type:'POST',
        url: base_url+"Facturaslis/obtenerrfc",
        data: {
        	id:cliente,
            rfc:rfc
        },
        success: function (data){
        	$('#rfc').html(data).change(); 
            $('#rfc').select2({width: 'resolve'});
        }
    });
}
function alertfunction(titlea,contenta){
    $.alert({boxWidth: '30%',useBootstrap: false,title: titlea,theme: 'bootstrap',content: contenta});  
}
function detalle(id,tipo){
    if (tipo==0) {
        window.open(base_url+"Prefactura/view/"+id, "Prefactura", "width=780, height=612");
    }
    if (tipo==1){
        window.open(base_url+"Prefactura/viewc/"+id, "Prefactura", "width=780, height=612");
    }
    if (tipo==5) {
        window.open(base_url+"Prefactura/viewd/"+id, "Prefactura", "width=780, height=612");
    }
}
function detallep(id){
    window.open(base_url+"PolizasCreadas/view/"+id, "Prefactura", "width=780, height=612");
}
function importarproductos(id,tipo,tipovent){
    
    $('.import_'+id).prop( "disabled", true );
    //$('.addcobrar').html('');
	$.ajax({
        type:'POST',
        url: base_url+"Facturaslis/obtenerventasproductos",
        data: {
        	venta:id,
        	tipo: tipo
        },
        success: function (data){
        	
        	var array = $.parseJSON(data);
            console.log(array); 
            //$('#rfc').val(array.prefacturadatos.rfc_id).change();
            if(array.tipov>0){
               $('#tipov').val(array.tipov).change(); 
            }
            if(array.prefacturadatos!=null){
                if(array.prefacturadatos.metodopagoId==1){
                    var metodopagoId ='PUE';
                }else{
                    var metodopagoId ='PPD';
                }
            }else{
                var metodopagoId ='PUE';
            }
            
            console.log('folio_venta: '+array.folio_venta);
            if(array.folio_venta!=''){
                var ob=$('#observaciones').val();
                    if(ob!=''){
                        ob=ob+'  ';
                    }
                $('#observaciones').val(ob+array.folio_venta);
                if($('.lipedimento.active').length==0){
                    $('.lipedimento').click();    
                }
                
            }
            $('#FormaPago').val(metodopagoId).change();
            if(array.prefacturadatos!=null){
                //===============================
                    console.log('formapagoId :'+array.prefacturadatos.formapagoId);
                    switch (array.prefacturadatos.formapagoId){
                        case '1':
                            $('#MetodoPago').val('Efectivo').change();
                        break;
                        case '2':
                            $('#MetodoPago').val('ChequeNominativo').change();
                        break;
                        case '3':
                            $('#MetodoPago').val('TransferenciaElectronicaFondos').change();
                        break;
                        case '4':
                           $('#MetodoPago').val('TarjetasDeCredito').change();
                        break;
                        case '5':
                           $('#MetodoPago').val('MonederoElectronico').change();
                        break;
                        case '6':
                            $('#MetodoPago').val('DineroElectronico').change();
                        break;
                        case '7':
                            $('#MetodoPago').val('ValesDeDespensa').change();
                        break;
                        case '8':
                             $('#MetodoPago').val('DacionPago').change();
                        break;
                        case '9':
                             $('#MetodoPago').val('PagoSubrogacion').change();
                        break;
                        case '10':
                             $('#MetodoPago').val('PagoConsignacion').change();
                        break;
                        case '11':
                             $('#MetodoPago').val('Condonacion').change();
                        break;
                        case '12':
                             $('#MetodoPago').val('Compensacion').change();
                        break;
                        case '13':
                            $('#MetodoPago').val('Novacion').change();                  
                        break;
                        case '14':
                             $('#MetodoPago').val('Confusion').change();                   
                        break;
                        case '15':
                            $('#MetodoPago').val('RemisionDeuda').change();
                        break;
                        case '16':
                            $('#MetodoPago').val('PrescripcionoCaducidad').change();
                        break;
                        case '17':
                            $('#MetodoPago').val('SatisfaccionAcreedor').change();
                        break;
                        case '18':
                            $('#MetodoPago').val('TarjetaDebito').change();
                        break;
                        case '19':
                            $('#MetodoPago').val('TarjetaServicio').change();
                        break;
                        case '20':
                            $('#MetodoPago').val('AplicacionAnticipos').change();
                        break;
                        case '21':
                             $('#MetodoPago').val('PorDefinir').change();
                        break;
                    case '22':
                             $('#MetodoPago').val('intermediariopagos').change();
                        break;
                                    
                                                      
                    }
                //===============================
                //===============================
                    console.log('formapagoId :'+array.prefacturadatos.formapagoId);
                    console.log('uso: '+array.prefacturadatos.usocfdiId);
                    switch (array.prefacturadatos.usocfdiId){
                        case '1':
                            $('#uso_cfdi').val('G01').change();
                        break;
                        case '2':
                            $('#uso_cfdi').val('G02').change();
                        break;
                        case '3':
                            $('#uso_cfdi').val('G03').change();
                        break;
                        case '4':
                           $('#uso_cfdi').val('I01').change();
                        break;
                        case '5':
                           $('#uso_cfdi').val('I02').change();
                        break;
                        case '6':
                            $('#uso_cfdi').val('I03').change();
                        break;
                        case '7':
                            $('#uso_cfdi').val('I04').change();
                        break;
                        case '8':
                             $('#uso_cfdi').val('I05').change();
                        break;
                        case '9':
                             $('#uso_cfdi').val('I06').change();
                        break;
                        case '10':
                             $('#uso_cfdi').val('I07').change();
                        break;
                        case '11':
                             $('#uso_cfdi').val('I08').change();
                        break;
                        case '12':
                             $('#uso_cfdi').val('D01').change();
                        break;
                        case '13':
                            $('#uso_cfdi').val('D02').change();                  
                        break;
                        case '14':
                             $('#uso_cfdi').val('D03').change();                   
                        break;
                        case '15':
                            $('#uso_cfdi').val('D04').change();
                        break;
                        case '16':
                            $('#uso_cfdi').val('D05').change();
                        break;
                        case '17':
                            $('#uso_cfdi').val('D06').change();
                        break;
                        case '18':
                            $('#uso_cfdi').val('D07').change();
                        break;
                        case '19':
                            $('#uso_cfdi').val('D08').change();
                        break;
                        case '20':
                            $('#uso_cfdi').val('D09').change();
                        break;
                        case '21':
                             $('#uso_cfdi').val('D10').change();
                        break;
                        case '22':
                             $('#uso_cfdi').val('P01').change();
                        break;
                                                      
                    }                                               
                //===============================
            }else{
                $('#MetodoPago').val('Efectivo').change();
                $('#uso_cfdi').val('G03').change();
            }
            //$('#CondicionesDePago').val(array.prefacturadatos.formadecobro);
        	$.each(array.arrayproductos, function(index, item) {
        		if (parseFloat(item.iva)>0) {
        			var aiva=1;
        		}else{
        			var aiva=0;
        		}
        		agregarconceptovalor(item.cantidad,item.unidadsat,item.unidadsat_text,item.conceptosat,item.conceptosat_text,item.producto,item.preciou,aiva,id,0);
        	});
        	funcionconceptos();
            tableventarelacion(id,tipo,array.tipov,tipovent);
            if(array.fac_pend.length>0){
                var s_icli=$('#idcliente option:selected').val();
                var s_clin=$('#idcliente option:selected').text();
                var url_lisf=base_url+'Facturaslis?clienteid='+s_icli+'&cliente='+s_clin;
                var html='La Pre ya tiene una factura pendiente de timbrar, favor de retimbrar si ya se corrigieron los Datos fiscales<br><a class="b-btn b-btn-primary" href="'+url_lisf+'">Ir al listado</a>';
                $('body').loading({theme: 'dark',message: html}); 
            }
            if(array.orden_comp!=''){
                var ob=$('#observaciones').val();
                if(ob!=''){
                    ob=ob+'  ';
                }
                $('#observaciones').val(ob+array.orden_comp);
                if($('.lipedimento.active').length==0){
                    $('.lipedimento').click();    
                }
                //$('.lipedimento').click();
            }
        }
    });
    
    $('#ventaviculada').val(id);
}
function importarproductos_pass(id,tipo,tipovent){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: 'Pre fuera de tiempo,Se necesita permisos de administrador<br>'+
                 '<input type="text" placeholder="Contraseña" id="contrasena3" name="contrasena3" class="name form-control" required/>',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                //var pass=$('#contrasena').val();
                var pass=$("input[name=contrasena3]").val();
                if (pass!='') {
                     $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Sistema/solicitarpermiso",
                        data: {
                            pass:pass
                        },
                        success: function (response){
                                var respuesta = parseInt(response);
                                if (respuesta==1) {
                                    importarproductos(id,tipo,tipovent);
                                }else{
                                    notificacion(2);
                                }
                        },
                        error: function(response){
                            notificacion(1);
                        }
                    });
                    
                }else{
                    notificacion(0);
                }
            },
            cancelar: function (){
                
            }
        }
    });
    setTimeout(function(){
            new MaskedPassword(document.getElementById("contrasena3"), '\u25CF');
    },1000);
}
function funcionconceptos(){
	$('.unidadsat').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un Unidad',
          ajax: {
            url: base_url+'Configuracionrentas/searchunidadsat',
            dataType: "json",
            data: function (params) {
              var query = {
                search: params.term,
                type: 'public'
              }
              return query;
          },
          processResults: function(data){
            var clientes=data;
            var itemscli = [];
            data.forEach(function(element) {
                    itemscli.push({
                        id: element.Clave,
                        text: element.Clave+' / '+element.nombre
                    });
                });
                return {
                    results: itemscli
                };        
          },  
        }
    });
	$('.conseptosat').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un Concepto',
          ajax: {
            url: base_url+'Configuracionrentas/searchconceptosat',
            dataType: "json",
            data: function (params) {
              var query = {
                search: params.term,
                type: 'public'
              }
              return query;
          },
          processResults: function(data){
            var clientes=data;
            var itemscli = [];
            data.forEach(function(element) {
                    itemscli.push({
                        id: element.Clave,
                        text: element.nombre
                    });
                });
                return {
                    results: itemscli
                };        
          },  
        }
    });
}
function retimbrar(idfactura,contrato){
    var tipov = $('#tipov option:selected').val();
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Configuracionrentas/retimbrar",
        data: {
            factura:idfactura
        },
        success:function(response){  
            console.log(response);
            var array = $.parseJSON(response);
            if (array.resultado=='error') {
                swal({    title: "Error "+array.CodigoRespuesta+"!",
                              text: array.MensajeError,
                              type: "warning",
                              showCancelButton: false
                 },
                    function(){
                        //retimbrar(idfactura,0);
                        $(location).attr('href',base_url+'Facturaslis?tipov='+tipov);
                    });
            }else{
                swal({
                    title:"Éxito!", 
                    text:"Se ha creado la factura", 
                    type:"success"
                    },
                    function(){
                        $(location).attr('href',base_url+'Facturaslis?tipov='+tipov);
                    });
            }

        }
    });       
}
function importarproductoss(id,tipo,tipovent){
    //$('.addcobrar').html('');
    $.ajax({
        type:'POST',
        url: base_url+"Facturaslis/obtenerventasproductoss",
        data: {
            venta:id,
            tipo: tipo
        },
        success: function (data){
            console.log(data); 
            var array = $.parseJSON(data);
            $.each(array, function(index, item) {
                if (parseFloat(item.iva)>0) {
                    var aiva=1;
                }else{
                    var aiva=0;
                }
                agregarconceptovalor(item.cantidad,item.unidadsat,item.unidadsat_text,item.conceptosat,item.conceptosat_text,item.producto,item.preciou,aiva,id,0);
            });
            funcionconceptos();
        }
    });
    //$('#ventaviculada').val(id);
    //$('#ventaviculadatipo').val(tipo);
    tableventarelacion(id,tipo,1,tipovent);
}
function activardescuento(idrow){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: 'Se necesita permisos de administrador<br>'+
                 '<input type="password" placeholder="Contraseña" id="contrasena" class="name form-control" required/>',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var pass=$('#contrasena').val();
                if (pass!='') {
                     $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Sistema/solicitarpermiso",
                        data: {
                            pass:pass
                        },
                        success: function (response){
                                var respuesta = parseInt(response);
                                if (respuesta==1) {
                                    $('.cdescuento_'+idrow).attr({'readonly':false});
                                    $('.cdescuento_'+idrow).removeAttr("onclick");
                                    //$('.cdescuento_'+idrow).removeAttr("onclick");
                                    $('.cdescuento_'+idrow).attr('onchange', 'calculardescuento('+idrow+');');
                                }else{
                                    notificacion(2);
                                }
                        },
                        error: function(response){
                            notificacion(1);
                        }
                    });
                    
                }else{
                    notificacion(0);
                }
            },
            cancelar: function (){
                
            }
        }
    });
}
function calculardescuento(idrow){
    var costo =  parseFloat($('.cantidad_row_'+idrow).val())*parseFloat($('.precio_'+idrow).val());
                //costo.toFixed(2);
                costo=redondear(costo,2);
    var descuento = $('.cdescuento_'+idrow).val();
    var rowtotal =parseFloat(costo)-parseFloat(descuento);
    var ivaif = $('.cdescuento_'+idrow).data('diva');
    if (ivaif==1) {
        var siva =rowtotal*0.16;
        //siva = siva.toFixed(2);
        if(trunquearredondear==0){
            //siva = siva.toFixed(4);
            siva=redondear(siva,4);
        }else{
            siva=Math.floor(siva * 100) / 100;
        }
    }else{
        var siva =0.00;
    }
    $('.tiva_'+idrow).val(siva);
    var totalg = parseFloat(rowtotal)+parseFloat(siva);
    $('.csubtotal_'+idrow).val(totalg);
    calculartotales();
}
function calculartotales_set(tiempo){
    setTimeout(function(){ 
        calculartotales();
    }, tiempo);
}
function registrofac_preview(){
    blockearfact=1;
        var form =$('#validateSubmitForm');
        var valid =form.valid();
        if (valid) {
            //$( ".registrofac" ).prop( "disabled", true );
            var datos = form.serialize();
            var productos = $("#table_conceptos tbody > tr");
            //==============================================
                var DATAa  = [];
                productos.each(function(){         
                    item = {};                    
                    item ["Cantidad"]   = $(this).find("input[id*='cantidad']").val();
                    item ["Unidad"]  = $(this).find("select[id*='unidadsat'] option:selected").val();
                    console.log($(this).find("select[id*='unidadsat']").val());
                    if ($(this).find("select[id*='unidadsat']").val()==null || $(this).find("select[id*='unidadsat']").val()=='') {
                        blockearfact=0;
                    }
                    item ["servicioId"]  = $(this).find("select[id*='conseptosat'] option:selected").val();
                    console.log($(this).find("select[id*='conseptosat']").val());
                    if ($(this).find("select[id*='conseptosat']").val()==null || $(this).find("select[id*='conseptosat']").val()=='') {
                        blockearfact=0;
                    }
                    //item ["Descripcion"]  = $(this).find("select[id*='conseptosat'] option:selected").text();
                    item ["Descripcion2"]  = $(this).find("input[id*='descripcion']").val();
                    item ["Cu"]  = $(this).find("input[id*='precio']").val();
                    item ["descuento"]  = $(this).find("input[id*='descuento']").val();
                    item ["Importe"]  = $(this).find("input[id*='subtotal']").val();
                    item ["iva"]  = $(this).find("input[id*='tiva']").val();
                    DATAa.push(item);
                });
                INFOa  = new FormData();
                aInfoa   = JSON.stringify(DATAa);
            //========================================
            var productoslength=productos.length;
            if (blockearfact==0) {
                productoslength=0;
            }
            if (productoslength>0) {
                //$('body').loading({theme: 'dark',message: 'Timbrando factura...'});

                var tiporelacion = $('input:radio[name=tiporelacion]:checked').val();
                var ventaviculada = $('#ventaviculada').val();
                var ventaviculadatipo = $('#ventaviculadatipo').val();
                var Subtotal=$('#Subtotal').val();
                var iva=$('#iva').val();
                var visr= $('#isr').val();
                var vriva= $('#ivaretenido').val();
                var v5millar= $('#5almillarval').val();
                var outsourcing= $('#outsourcing').val();
                var f_r = $('#facturarelacionada').is(':checked')==true?1:0; //agregados version 4.0
                var f_r_t = $('#TipoRelacion option:selected').val();
                var f_r_uuid = $('#uuid_r').val();

                var total=$('#total').val();               
                datos=datos+'&subtotal='+Subtotal+'&iva='+iva+'&visr='+visr+'&vriva='+vriva+'&v5millar='+v5millar+'&outsourcing='+outsourcing+'&total='+total+'&conceptos='+aInfoa+'&tiporelacion='+tiporelacion+'&ventaviculada='+ventaviculada+'&ventaviculadatipo='+ventaviculadatipo+'&f_relacion='+f_r+'&f_r_tipo='+f_r_t+'&f_r_uuid='+f_r_uuid;
                //===
                $('#modal_previefactura').modal({
                    dismissible: false
                });
                $('#modal_previefactura').modal('open');
                //obtenerinfofiscal();
                setTimeout(function(){ 
                    var urlfac=base_url+"index.php/Preview/factura?"+datos;
                    var htmliframe="<iframe src='"+urlfac+"' title='description' class='ifrafac'>";
                    $('.preview_iframe').html(htmliframe);
                    //window.location.href = base_url+"index.php/Preview/factura?"+datos; 
                }, 1000);
            }else{
                alertfunction('Atención!','Agregar por lo menos un concepto');
            }
        }else{
            alertfunction('Atención!','Faltan campos obligatorios');
        }
}
function tableventarelacion(id,tipo,tipov,tiporelacion){
    //var tiporelacion = $('input:radio[name=tiporelacion]:checked').val();
    var tiporelacion_text='';
    if(tiporelacion==1){
        tiporelacion_text='Venta';
    }
    if(tiporelacion==2){
        tiporelacion_text='Venta combinada';
    }
    if(tiporelacion==3){
        tiporelacion_text='Póliza';
    }
    if(tiporelacion==4){
        tiporelacion_text='Servicio';
    }
    if(tiporelacion==5){
        tiporelacion_text='Venta';
    }
    var view_tipo='';
    if(tipov==1){
        view_tipo='Alta Productividad';
    }
    if(tipov==2){
        view_tipo='D-Impresion';
    }
    var html='<tr>\
                  <td>\
                    <input type="hidden"  id="tiporelacion" value="'+tiporelacion+'">\
                    <input type="hidden"  id="ventaviculadatipo" value="'+tipo+'">\
                    <input type="hidden"  id="ventaviculada_" value="'+id+'">\
                    '+tiporelacion_text+'\
                  </td>\
                  <td>'+view_tipo+'</td>\
                  <td>\
                  '+id+'\
                  </td>\
                </tr>';
    $('.vrtabletbody').append(html);
}
function obtenerdatosfactura(factura){
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Facturaslis/obtenerdatosfactura",
        data: {
            factura:factura
        },
        success: function (response){
            console.log(response);
            var array = $.parseJSON(response);
            $('#idcliente').html('<option value="'+array.facturas[0].Clientes_ClientesId+'">'+array.facturas[0].Nombre+'<option>');
            
            $('#idcliente').select2();
            obtenerrfc(array.facturas[0].Clientes_ClientesId,array.facturas[0].Rfc);
            console.log(array.facturas[0].Rfc);
            //$('#rfc').html('<option>'+array.facturas[0].Rfc+'<option>');
            //$('#rfc').select2();
            $('#FormaPago').val(array.facturas[0].FormaPago).change();

            $('#moneda').val(array.facturas[0].moneda);
            
            $('#uso_cfdi').val(array.facturas[0].uso_cfdi);
            $('#uso_cfdi').select2();

            $('#CondicionesDePago').val(array.facturas[0].CondicionesDePago);

            formapagoselect(array.facturas[0].MetodoPago);
            
            $('#numproveedor').val(array.facturas[0].numproveedor);
            $('#numordencompra').val(array.facturas[0].numordencompra);
            $('#observaciones').val(array.facturas[0].observaciones);
            if(array.facturas[0].isr>0){
                $('#risr').attr('checked',true);
            }
            if(array.facturas[0].ivaretenido>0){
                $('#riva').attr('checked',true);
            }
            if(array.facturas[0].cincoalmillarval>0){
                $('#5almillar').attr('checked',true);
            }
            if(array.facturas[0].outsourcing>0){
                $('#aplicaout').attr('checked',true);
            }
            array.facturasd.forEach(function(element) {
                if(element.iva>0){
                    var aplicaiva=1;
                }else{
                    var aplicaiva=0;
                }
                if($('#facturarelacionada').is(':checked')){
                    var tr =$('#TipoRelacion option:selected').val();
                    if(tr=='01'){
                        element.Unidad='ACT';

                        element.servicioId='84111506';
                        element.Descripcion='Servicios de facturacion';
                    }
                }
                agregarconceptovalor(element.Cantidad,element.Unidad,element.Unidad,element.servicioId,element.Descripcion,element.Descripcion2,element.Cu,aplicaiva,0,element.descuento);
            });
        }
    });
}
function formapagoselect(forma){
    switch (forma) {
      case '01':
        $('#MetodoPago').val('Efectivo');
        break;
      case '02':
        $('#MetodoPago').val('ChequeNominativo');
        break;
      case '03':
        $('#MetodoPago').val('TransferenciaElectronicaFondos');
        break;
      case '04':
        $('#MetodoPago').val('TarjetasDeCredito');
        break;
      case '05':
        $('#MetodoPago').val('MonederoElectronico');
        break;
      case '06':
        $('#MetodoPago').val('DineroElectronico');
        break;
      case '08':
        $('#MetodoPago').val('ValesDeDespensa');
        break;
      case '12':
        $('#MetodoPago').val('DacionPago');
        break;
      case '13':
        $('#MetodoPago').val('PagoSubrogacion');
        break;
      case '14':
        $('#MetodoPago').val('PagoConsignacion');
        break;
      case '15':
        $('#MetodoPago').val('Condonacion');
        break;
      case '17':
        $('#MetodoPago').val('Compensacion');
        break;
      case '23':
        $('#MetodoPago').val('Novacion');
        break;
      case '24':
        $('#MetodoPago').val('Confusion');
        break;
      case '25':
        $('#MetodoPago').val('RemisionDeuda');
        break;
      case '26':
        $('#MetodoPago').val('PrescripcionoCaducidad');
        break;
      case '27':
        $('#MetodoPago').val('SatisfaccionAcreedor');
        break;
      case '28':
        $('#MetodoPago').val('TarjetaDebito');
        break;
      case '29':
        $('#MetodoPago').val('TarjetaServicio');
        break;
      case '30':
        $('#MetodoPago').val('AplicacionAnticipos');
        break;
      case '99':
        $('#MetodoPago').val('PorDefinir');
        break;
    case '31':
        $('#MetodoPago').val('intermediariopagos');
        break;
      
      default:
        //Declaraciones ejecutadas cuando ninguno de los valores coincide con el valor de la expresión
        break;
        
    }
    $('#MetodoPago').select2();
}
function removeventaidtable(){
    var productos = $("#table_conceptos tbody > tr");
    var DATAa  = [];
        productos.each(function(){         
            item = {};            
            var ventaremove=$(this).find("input[id*='cantidad']").data('venta');        
            item = ventaremove;
            
            DATAa.push(item);
        });
    console.log(DATAa);
    var DATAauni=DATAa.filter(unique);
    console.log(DATAauni);
    /*==================================================================*/
    $("#vrtable tbody > tr").each(function(){         
            var ventaremoves=$(this).find("input[id*='ventaviculada_']").val();        
            if(DATAauni.find(element => element == ventaremoves)>0){

            }else{
                $(this).remove();
                $('.import_'+ventaremoves).prop('disabled',false);
            }
        });
}
const unique = (value, index, self) => {
  return self.indexOf(value) === index
}
function permisoparafacturaabierta(){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: 'Se necesita permisos de administrador<br>'+
                 '<input type="password" placeholder="Contraseña" id="contrasena" class="name form-control" required/>',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var pass=$('#contrasena').val();
                if (pass!='') {
                     $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Sistema/solicitarpermiso",
                        data: {
                            pass:pass
                        },
                        success: function (response){
                                var respuesta = parseInt(response);
                                if (respuesta==1) {
                                    bloqueoabierto=1;
                                    agregarconcepto();
                                }else{
                                    notificacion(2);
                                }
                        },
                        error: function(response){
                            notificacion(1);
                        }
                    });
                    
                }else{
                    notificacion(0);
                }
            },
            cancelar: function (){
                
            }
        }
    });
}
//====================================================
function pgglobal(){
    setTimeout(function(){ 
        var pgg=$('#pg_global').is(':checked');
        if(pgg==true){
            $('.infoglobal').show();
            $('#pg_global').val(1);
        }else{
            $('.infoglobal').hide();
            $('#pg_global').val(0);
        }
    }, 1000);
}
function verficartiporfc(){
    var rfc = $('#rfc option:selected').text();
    if(rfc=='XAXX010101000'){
        //05 Bimestral solo es para el regimen del cliente que emite 621
        var html='<div>\
                            <input type="checkbox" class="filled-in" id="pg_global" name="pg_global" wtx-context="4D1A9C7C-0B37-41A5-88EB-ECB7877AFCAD" onclick="pgglobal()">\
                            <label for="pg_global">GLOBAL</label>\
                          </div><div class="infoglobal" style="display:none"><div class="col s4">\
                    <label>Periodicidad</label>\
                    <select class="browser-default form-control-bmz" name="pg_periodicidad" id="pg_periodicidad" onchange="v_periocidad()">\
                      <option value="01">01 Diario</option>\
                      <option value="02">02 Semanal</option>\
                      <option value="03">03 Quincenal</option>\
                      <option value="04">04 Mensual</option>\
                      <option value="05" disabled>05 Bimestral</option>\
                    </select>\
                  </div>\
                  <div class="col s4">\
                    <label>Mes</label>\
                    <select class="browser-default form-control-bmz" name="pg_meses" id="pg_meses">\
                      <option value="01" class="select_no_bimestral">01 Enero</option>\
                      <option value="02" class="select_no_bimestral">02 Febrero</option>\
                      <option value="03" class="select_no_bimestral">03 Marzo</option>\
                      <option value="04" class="select_no_bimestral">04 Abril</option>\
                      <option value="05" class="select_no_bimestral">05 Mayo</option>\
                      <option value="06" class="select_no_bimestral">06 Junio</option>\
                      <option value="07" class="select_no_bimestral">07 Julio</option>\
                      <option value="08" class="select_no_bimestral">08 Agosto</option>\
                      <option value="09" class="select_no_bimestral">09 Septiembre</option>\
                      <option value="10" class="select_no_bimestral">10 Octubre</option>\
                      <option value="11" class="select_no_bimestral">11 Noviembre</option>\
                      <option value="12" class="select_no_bimestral">12 Diciembre</option>\
                      <option value="13" class="select_bimestral">13 Enero-Febrero</option>\
                      <option value="14" class="select_bimestral">14 Marzo-Abril</option>\
                      <option value="15" class="select_bimestral">15 Mayo-Junio</option>\
                      <option value="16" class="select_bimestral">16 Julio-Agosto</option>\
                      <option value="17" class="select_bimestral">17 Septiembre-Octubre</option>\
                      <option value="18" class="select_bimestral">18 Noviembre-Diciembre</option>\
                    </select>\
                  </div>\
                  <div class="col s4">\
                    <label>Año</label>\
                    <input type="number" name="pg_anio" id="pg_anio" class="form-control-bmz" readonly>\
                  </div>\
                </div>';
        $('.agregardatospublicogeneral').html(html);
        setTimeout(function(){ 
            var mesactual = $('#mesactual').val();
            var anioactual = $('#anioactual').val();
            console.log(mesactual);
            console.log(anioactual);
            $('#pg_meses').val(mesactual);
            $('#pg_anio').val(anioactual);
            $('.select_bimestral').hide('show');
        }, 1000);
        $('#sdescripcion').val('Venta');
        $('#uso_cfdi').val('S01').select2();
    }else{
        $('.agregardatospublicogeneral').html('');
    }
    obtenerinfofiscal();
}
function v_periocidad(){
    var mesactual = $('#mesactual').val();
    var pg_periodicidad = $('#pg_periodicidad').val();
    if(pg_periodicidad=='05'){
        $('.select_no_bimestral').hide('show');
        $('.select_bimestral').show('show');
        if(mesactual=='01' || mesactual=='02'){
            var mesactual_n=13;
        }
        if(mesactual=='03' || mesactual=='04'){
            var mesactual_n=14;
        }
        if(mesactual=='05' || mesactual=='06'){
            var mesactual_n=15;
        }
        if(mesactual=='07' || mesactual=='08'){
            var mesactual_n=16;
        }
        if(mesactual=='09' || mesactual=='10'){
            var mesactual_n=17;
        }
        if(mesactual=='11' || mesactual=='12'){
            var mesactual_n=18;
        }
        $('#pg_meses').val(mesactual_n);
    }else{
        $('#pg_meses').val(mesactual);
        $('.select_no_bimestral').show('show');
        $('.select_bimestral').hide('show');
    }
    
}
function obtenerinfofiscal(){
    var idclientes=$('#idcliente option:selected').val();
    if(idclientes>0){
        $.ajax({
            type:'POST',
            url: base_url+"index.php/Facturaslis/viewdatosfiscalesinfo",
            data: {
                cliente:idclientes,
                rfc:$('#rfc option:selected').text(),
            },
            success: function (response){
                console.log(response);
                var array = $.parseJSON(response);
                //$('.v_nomfical').html(array.nombrefiscal);
                if(array.cp!=''){
                    var cp=array.cp;
                }else{
                    var cp='<span style="color:red"><b>Sin Direccion Fiscal</b></span>';
                }
                //$('.v_direccionfiscal').html(cp);
                if(array.regimen!=''){
                    var regimen=array.regimen;
                }else{
                    var regimen='<span style="color:red"><b>Sin regimen fiscal</b></span>';
                }
                //$('.v_regimenfiscal').html(regimen);
                var infodf='<div class="col s4">\
                              <b>Nombre Fiscal: </b><br><span >'+array.nombrefiscal+'</span>\
                            </div>\
                            <div class="col s4">\
                              <b>Direccion Fiscal(C.P.): </b><br><span >'+cp+'</span>\
                            </div>\
                            <div class="col s4">\
                              <b>Regimen Fiscal Receptor: </b><br><span >'+regimen+'</span>\
                            </div>';
                $('.infodatosfiscales').html(infodf);
                $( "#uso_cfdi ."+array.regimennumber ).prop( "disabled", false ).trigger('change');
                $( "#uso_cfdi" ).select2();
                selectusocfdi(array.regimenclave);

            },
            error: function(response){
                notificacion(1);
            }
        });  
    }
    
}
function selectusocfdi(clave){
    clave=parseInt(clave);
    switch (clave) {
      case 601:
            $('#uso_cfdi').val('G03');
        break;
      case 603:
        $('#uso_cfdi').val('G03');
        break;
      case 606:
        $('#uso_cfdi').val('G03');
        break;
      case 612:
        $('#uso_cfdi').val('G03');
        break;
      case 620:
        $('#uso_cfdi').val('G03');
        break;
      case 621:
        $('#uso_cfdi').val('G03');
        break;
      case 622:
        $('#uso_cfdi').val('G03');
        break;
      case 623:
        $('#uso_cfdi').val('G03');
        break;
      case 624:
        $('#uso_cfdi').val('G03');
        break;
      case 625:
        $('#uso_cfdi').val('G03');
        break;
      case 626:
        $('#uso_cfdi').val('G03');
        break;
      default:
        console.log(clave);
    }
    $('#uso_cfdi').select2();
}
function habilitardescripcion(row){
    var des=$('.rowcobrar_'+row+' #descripcion').is('[readonly]');
    if(des){
        $.confirm({
            boxWidth: '30%',
            useBootstrap: false,
            icon: 'fa fa-warning',
            title: 'Atención!',
            content: 'Se necesita permisos de administrador para habilitar el campo<br>'+
                     '<input type="password" placeholder="Contraseña" id="contrasena" class="name form-control" required/>',
            type: 'red',
            typeAnimated: true,
            buttons:{
                confirmar: function (){
                    var pass=$('#contrasena').val();
                    if (pass!='') {
                         $.ajax({
                            type:'POST',
                            url: base_url+"index.php/Sistema/solicitarpermiso",
                            data: {
                                pass:pass
                            },
                            success: function (response){
                                    var respuesta = parseInt(response);
                                    if (respuesta==1) {
                                        $('.rowcobrar_'+row+' #descripcion').prop('readonly', false);
                                    }else{
                                        notificacion(2);
                                    }
                            },
                            error: function(response){
                                notificacion(1);
                            }
                        });
                        
                    }else{
                        notificacion(0);
                    }
                },
                cancelar: function (){
                    
                }
            }
        });
    }
}
function redondearLejosDeCero(numero){
    return Math.sign(numero) * Math.floor(Math.abs(numero) + 0.5);
}
function redondear(numero, digitos){
    let base = Math.pow(10, digitos);
    let entero = redondearLejosDeCero(numero * base);
    return entero / base;
}
//=========================================================
function mpublicogeneral(){
    $('#mpublicogeneral').modal({
        dismissible: false
    });
    $('#mpublicogeneral').modal('open');
    $('#idcliente').html('<option value="2118">PUBLICO EN GENERAL</option>');
    select2idcliente();
    obtenerrfc(2118,0);
}
function obtenerpg(){
    var tipov = $('#tipov option:selected').val();
    var faci=$('#pg_fechai').val();
    var facf=$('#pg_fechaf').val();
    if(tipov>0){
        $.ajax({
            type:'POST',
            url: base_url+"index.php/Facturaslis/obtenerpg",
            data: {
                faci:faci,
                facf:facf,
                per:$('#pg_personal option:selected').val(),
                empresa:tipov
            },
            success: function (response){
                    $('.infoventaspg').html(response);
            },
            error: function(response){  
                notificacion(1);
            }
        });
    }else{ 
        not_alert('Seleccione la empresa');
    }
}
function import_pg(){
    
    $(".import_pg").each(function() {
        $(this).click();
            //var id_pg=$(this).data('id');
            //var tipo_pg=$(this).data('tipo');
            //import_pg2(id_pg,tipo_pg);
    });
}
var importt = 500;
function import_pg2(id_pg,tipo_pg){
    setTimeout(function(){ 
        console.log('.import_pg_'+id_pg+'_'+tipo_pg);
        $('.import_pg_'+id_pg+'_'+tipo_pg).click();
        importt=importt+500;
        console.log(importt);
    }, importt);
    $( "#pg_global" ).prop( "checked", true ); 
    pgglobal();
}
function not_alert(text){
    $.alert({boxWidth: '30%',useBootstrap: false,
                        title: 'Error!',
                        content: text});
}
function importarproductospg(id,tipo,tipovent){

    $('.import_'+id).prop( "disabled", true );
    //$('.addcobrar').html('');
    $.ajax({
        type:'POST',
        url: base_url+"Facturaslis/obtenerventasproductos/1",
        data: {
            venta:id,
            tipo: tipo
        },
        success: function (data){
            
            var array = $.parseJSON(data);
            console.log(array); 


            //$('#CondicionesDePago').val(array.prefacturadatos.formadecobro);
            $.each(array.arrayproductos, function(index, item) {
                if (parseFloat(item.iva)>0) {
                    var aiva=1;
                }else{
                    var aiva=0;
                }
                agregarconceptovalor(item.cantidad,item.unidadsat,item.unidadsat_text,item.conceptosat,item.conceptosat_text,item.producto,item.preciou,aiva,id,0);
            });
            funcionconceptos();
            tableventarelacion(id,tipo,array.tipov,tipovent);
        }
    });
    
    $('#ventaviculada').val(id);
}
function selecttiporelacion(){
    var tr =$('#TipoRelacion option:selected').val();
    var tc =$('#TipoComprobante option:selected').val();
    if(tr=='01'){
        $('#sunidadsat').html('<option value="ACT">ACT / Actividad</option>');
        $('#sconseptosat').val('84111506').select2();
        functionsunidadsat();
        $('#uso_cfdi').val('G02').select2();
        
        setTimeout(function(){ 
            $('.option_G02').prop('disabled',false); 
            $('#uso_cfdi').val('G02').select2();
            $('#FormaPago').val('PUE');
            $('#MetodoPago').val('TransferenciaElectronicaFondos').select2();
            
        }, 2000);
    }
    if(tr=='07'){
        $('#sunidadsat').html('<option value="ACT">ACT / Actividad</option>');
        functionsunidadsat();
        $('#sconseptosat').val('84111506').select2();
        if(tc=='I'){
          $('#sdescripcion').val('Anticipo del bien o servicio');  
        }
        if(tc=='E'){
        $('#sdescripcion').val('Aplicacion de Anticipo');
        }
      $('.rowcobrar_0').remove()
        
    }
}
function validarparaanticipos(){
    if($('#infoanticipo').is(':checked')){
        var productos = $("#table_conceptos tbody > tr");
        if(productos.length>0){
            $('.agregarconcepto').hide('show');
        }else{
            $('.agregarconcepto').show('show');
        }
    }else{
        $('.agregarconcepto').show('show');
    }
}
function addpermisoseditcon(){
    var html='<a class="b-btn b-btn-primary tooltipped" data-position="top" data-delay="50" data-tooltip="Editar conceptos importados" onclick="addpermisoseditcon2()">Editar conceptos</a>';
    $('.addpermisoseditcon').html(html);
    $('.tooltipped').tooltip()
}
function addpermisoseditcon2(){
    $.confirm({
            boxWidth: '30%',
            useBootstrap: false,
            icon: 'fa fa-warning',
            title: 'Atención!',
            content: 'Se necesita permisos de administrador para habilitar el campo<br>'+
                     '<input type="password" placeholder="Contraseña" id="contrasena" class="name form-control" required/>',
            type: 'red',
            typeAnimated: true,
            buttons:{
                confirmar: function (){
                    var pass=$('#contrasena').val();
                    if (pass!='') {
                         $.ajax({
                            type:'POST',
                            url: base_url+"index.php/Sistema/solicitarpermiso",
                            data: {
                                pass:pass
                            },
                            success: function (response){
                                    var respuesta = parseInt(response);
                                    if (respuesta==1) {
                                        //=====================================
                                            funcionconceptos();
                                        //=====================================
                                    }else{
                                        notificacion(2);
                                    }
                            },
                            error: function(response){
                                notificacion(1);
                            }
                        });
                        
                    }else{
                        notificacion(0);
                    }
                },
                cancelar: function (){
                    
                }
            }
        });
}
function fac_pen_pago_pue(num){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: 'Tiene facturas PUE del mes anterior sin completar Pagos<br>Se necesita permisos de administrador para continuar<br>'+
                '<input type="text" placeholder="Contraseña" id="contrasena4" name="contrasena4" class="name form-control-bmz" required/>',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var pass=$("input[name=contrasena4]").val();
                if (pass!='') {
                     $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Sistema/solicitarpermiso",
                        data: {
                            pass:pass
                        },
                        success: function (response){
                                var respuesta = parseInt(response);
                                if (respuesta==1) {
                                    //==================================
                                        
                                    //=================================
                                }else{
                                    fac_pen_pago_pue(num);
                                    notificacion(2);
                                    
                                }
                        },
                        error: function(response){
                            notificacion(1);
                        }
                    });
                    
                }else{
                    notificacion(0);
                }
            },
            cancelar: function () {
                window.location.href = base_url+"index.php/Facturaslis";
            }
        }
    });
    setTimeout(function(){
            new MaskedPassword(document.getElementById("contrasena4"), '\u25CF');
    },500);
}