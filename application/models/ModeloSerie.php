<?php
$a=session_id();
if(empty($a)) session_start();
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloSerie extends CI_Model {
    public function __construct() {
        parent::__construct();
        $this->DB10 = $this->load->database('other10_db', TRUE);
    }

    function Getseries_equipos($params){
            $bodegaId=$params['bodegaId'];
            $columns = array( 
                0=>'sp.serieId',
                1=>'b.bodega',
                2=>'ca.modelo',
                3=>'sp.serie',
                4=>'sp.reg',
                5=>'sp.fechas_mov',
                6=>'p.nombre',
                7=>'sp.status',
                8=>'sp.bodegaId',
                9=>'b.bodega',
                10=>'sp.productoid',
                11=>'sp.activo',
                12=>'sp.resguardar_cant',
            );

            $select="";
            foreach ($columns as $c) {
                $select.="$c, ";
            }
            $this->DB10->select($select);
            $this->DB10->from('series_productos sp');
            $this->DB10->join('equipos ca', 'ca.id=sp.productoid');
            $this->DB10->join('bodegas b', 'b.bodegaId=sp.bodegaId');
            $this->DB10->join('personal p', 'p.personalId=sp.personalId');
            if($bodegaId==0){
            	$where = array(
	                'sp.activo'=>1
	            );
            }else{
            	$where = array(
	                'sp.activo'=>1,
	                'sp.bodegaId'=>$bodegaId
	            );
            }
            $this->DB10->where($where);
            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->DB10->group_start();
                foreach($columns as $c){
                    $this->DB10->or_like($c,$search);
                }
                $this->DB10->group_end();
                
            }
            if($params['bodegaId']!=0){
                $this->DB10->where("b.bodegaId=".$params['bodegaId']);
            }
            
            $this->DB10->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
            $this->DB10->limit($params['length'],$params['start']);
            //echo $this->DB10->get_compiled_select();
            $query=$this->DB10->get();
            // print_r($query); die;
            return $query;
    }
    public function TotalSerieequipo($params){
        $this->DB10->select('COUNT(*) as total');
        $this->DB10->from('series_productos sa');
        $this->DB10->where("sa.activo", 1);
        if($params['bodegaId']!=0){
                $this->DB10->where("bodegaId=".$params['bodegaId']);
            }
        $query=$this->DB10->get();
        return $query->row()->total;
    }
    function Getseries_accesorios($params){
            $bodegaId=$params['bodegaId'];
            //log_message('error', 'bodega: '.$bodegaId);
            $columns = array( 
                0=>'sa.serieId',
                1=>'sa.bodegaId',
                2=>'b.bodega',
                3=>'sa.accesoriosid',
                4=>'ca.nombre',
                5=>'sa.serie',
                6=>'sa.status',
                7=>'sa.reg',
                8=>'sa.fechas_mov',
                9=>'p.nombre AS persona',
                10=>'sa.activo',
                11=>'sa.con_serie',
                12=>'sa.cantidad',
                13=>'sa.resguardar_cant'
            );
            $columnssss = array( 
                0=>'sa.serieId',
                1=>'sa.bodegaId',
                2=>'b.bodega',
                3=>'sa.accesoriosid',
                4=>'ca.nombre',
                5=>'sa.serie',
                6=>'sa.status',
                7=>'sa.reg',
                8=>'p.nombre',
                9=>'sa.activo',
            );

            $select="";
            foreach ($columns as $c) {
                $select.="$c, ";
            }
            $this->db->select($select);
            $this->db->from('series_accesorios sa');
            $this->db->join('catalogo_accesorios ca', 'ca.id=sa.accesoriosid');
            $this->db->join('bodegas b', 'b.bodegaId=sa.bodegaId');
            $this->db->join('personal p', 'p.personalId=sa.personalId');
            if($bodegaId==0){
                $where = array(
                    'sa.activo'=>1
                );
            }else{
                $where = array(
                    'sa.activo'=>1,
                    'sa.bodegaId'=>$bodegaId
                );
            }
            $this->db->where($where);
            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->db->group_start();
                foreach($columnssss as $c){
                    $this->db->or_like($c,$search);
                }
                $this->db->group_end();
                
            }
            if($params['bodegaId']!=0){
                $this->db->where("b.bodegaId=".$params['bodegaId']);
            }
            $this->db->order_by($columnssss[$params['order'][0]['column']], $params['order'][0]['dir']);
            $this->db->limit($params['length'],$params['start']);
            //echo $this->db->get_compiled_select();
            $query=$this->db->get();
            // print_r($query); die;
            return $query;
    }
    public function TotalSerieaccesorios($params){
        $this->db->select('COUNT(*) as total');
        $this->db->from('series_accesorios sa');
        $this->db->where("sa.activo", 1);
        if($params['bodegaId']!=0){
                $this->db->where("bodegaId=".$params['bodegaId']);
            }
        $query=$this->db->get();
        return $query->row()->total;
    }
    public function stockaccesoriosbodega($accesorio,$bodegaId){
        $total=0;
        $sql="SELECT COUNT(*) as total 
              FROM series_accesorios as acc 
              WHERE acc.accesoriosid=$accesorio AND 
              acc.bodegaId=$bodegaId AND 
              acc.status<=1 and acc.con_serie=1";

        $query = $this->DB10->query($sql);
        foreach ($query->result() as $item) {
            $total=$total+$item->total;
        }
        $sql2="SELECT SUM(acc.cantidad) as total 
              FROM series_accesorios as acc 
              WHERE 
                acc.accesoriosid=$accesorio AND 
                acc.bodegaId=$bodegaId AND 
                acc.status<=1 and 
                acc.con_serie=0";

        $query2 = $this->DB10->query($sql2);
        foreach ($query2->result() as $item) {
            if($item->total>0){
                $total=$total+$item->total;    
            }
            
        }

        
        return $total;
    }
    function stocktonerbodegas($toner,$bodega){
        $total=0;
        $sql="SELECT SUM(cbod.total) as total FROM consumibles_bodegas as cbod WHERE cbod.consumiblesId=$toner AND cbod.bodegaId=$bodega";
        $query2 = $this->DB10->query($sql);
        foreach ($query2->result() as $item) {
            if($item->total>0){
                $total=$item->total;    
            }
            
        }
        return $total;
    }
    function Getseries_refacciones($params){
            $bodegaId=$params['bodegaId'];
            //log_message('error', 'bodega: '.$bodegaId);
            $columns = array( 
                0=>'sr.serieId',
                1=>'sr.bodegaId',
                2=>'b.bodega',
                3=>'sr.refaccionid',
                4=>'ca.nombre',
                5=>'sr.serie',
                6=>'sr.status',
                7=>'sr.reg',
                8=>'sr.fechas_mov',
                9=>'p.nombre AS persona',
                10=>'sr.activo',
                11=>'ca.codigo',
                12=>'sr.con_serie',
                13=>'sr.cantidad',
                14=>'sr.resguardar_cant'
            );
            $columnsss = array( 
                0=>'sr.serieId',
                1=>'sr.bodegaId',
                2=>'b.bodega',
                3=>'sr.refaccionid',
                4=>'ca.nombre',
                5=>'sr.serie',
                6=>'sr.status',
                7=>'sr.reg',
                8=>'p.nombre',
                9=>'sr.activo',
                10=>'ca.codigo',
                11=>'sr.con_serie',
                12=>'sr.cantidad',
            );
            

            $select="";
            foreach ($columns as $c) {
                $select.="$c, ";
            }
            $this->db->select($select);
            $this->db->from('series_refacciones sr');
            $this->db->join('refacciones ca', 'ca.id=sr.refaccionid');
            $this->db->join('bodegas b', 'b.bodegaId=sr.bodegaId');
            $this->db->join('personal p', 'p.personalId=sr.personalId');
            $this->db->where(array('ca.status'=>1));
            if($bodegaId==0){
                $where = array(
                    'sr.activo'=>1
                );
            }else{
                $where = array(
                    'sr.activo'=>1,
                    'sr.bodegaId'=>$bodegaId
                );
            }
            $this->db->where($where);
            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->db->group_start();
                foreach($columnsss as $c){
                    $this->db->or_like($c,$search);
                }
                $this->db->group_end();
                
            }
            if($params['bodegaId']!=0){
                $this->db->where("b.bodegaId=".$params['bodegaId']);
            }
            $this->db->order_by($columnsss[$params['order'][0]['column']], $params['order'][0]['dir']);
            $this->db->limit($params['length'],$params['start']);
            //echo $this->db->get_compiled_select();
            $query=$this->db->get();
            // print_r($query); die;
            return $query;
    }
    public function TotalSerierefacciones($params){
        $this->db->select('COUNT(*) as total');
        $this->db->from('series_refacciones sa');
        $this->db->where("sa.activo", 1);
        if($params['bodegaId']!=0){
                $this->db->where("bodegaId=".$params['bodegaId']);
            }
        $query=$this->db->get();
        return $query->row()->total;
    }
    function serieequipoubicacion($serie){
        $strq = "SELECT vene.idVenta,cli.empresa,asigs.reg, 0 as combinada
                    FROM asignacion_series_equipos as asigs 
                    inner join ventas_has_detallesEquipos as vene on vene.id=asigs.id_equipo
                    inner join ventas as ven on ven.id=vene.idVenta
                    INNER JOIN clientes as cli on cli.id=ven.idCliente
                    WHERE asigs.serieId=$serie and ven.combinada=0 and ven.activo=1
                    UNION
                    SELECT 
                    venc.combinadaId as idVenta,
                    cli.empresa,asigs.reg, 1 as combinada
                    FROM asignacion_series_equipos as asigs 
                    inner join ventas_has_detallesEquipos as vene on vene.id=asigs.id_equipo
                    inner join ventas as ven on ven.id=vene.idVenta
                    INNER JOIN clientes as cli on cli.id=ven.idCliente
                    inner JOIN ventacombinada as venc on venc.equipo=ven.id 
                    WHERE asigs.serieId=$serie and ven.combinada=1 and venc.activo=1"; 
        $query = $this->DB10->query($strq);

        return $query;
    }
    function serieequipoubicacionr($serie){
        $strq = "SELECT cont.idcontrato,rente.idRenta, cli.empresa, asige.reg,cont.estatus,asige.activo
                FROM asignacion_series_r_equipos as asige
                inner join rentas_has_detallesEquipos as rente on rente.id=asige.id_equipo
                left join contrato as cont on cont.idRenta=rente.idRenta AND cont.estatus=1
                INNER JOIN clientes as cli on cli.id=asige.idCliente
                WHERE asige.serieId=$serie AND asige.activo=1 
                group by cont.idcontrato
                "; 
        $query = $this->DB10->query($strq);
        
        return $query;
    }
    function serieaccesorioubicacion($serie){
        $strq = "SELECT 
                    vda.id_venta,
                    ven.combinada,
                    venc.combinadaId, 
                    cli.empresa,
                    aser.reg 
                FROM asignacion_series_accesorios as aser 
                INNER JOIN ventas_has_detallesEquipos_accesorios as vda on vda.id_accesoriod=aser.id_accesorio 
                INNER JOIN ventas as ven on ven.id=vda.id_venta 
                INNER JOIN clientes as cli on cli.id=ven.idCliente 
                LEFT JOIN ventacombinada as venc on venc.equipo=ven.id 
                WHERE aser.serieId=$serie"; 
        $query = $this->DB10->query($strq);

        return $query;
    }
    function serieaccesorioubicacionr($serie){
        $strq = "SELECT 
                    reaa.idrentas, 
                    cli.empresa, 
                    con.idcontrato, 
                    asserr.reg,
                    con.idRenta
                FROM asignacion_series_r_accesorios as asserr 
                INNER JOIN rentas_has_detallesEquipos_accesorios as reaa on reaa.id_accesoriod=asserr.id_accesorio 
                INNER JOIN rentas as ren on ren.id=reaa.idrentas 
                INNER JOIN clientes as cli on cli.id=ren.idCliente 
                LEFT JOIN contrato as con on con.idRenta=ren.id 
                WHERE asserr.serieId=$serie
                "; 
        $query = $this->DB10->query($strq);
        
        return $query;
    }
    

}