<?php 
	/*
	header("Pragma: public");
	header("Expires: 0");
	$filename = "salidas.xls";
	header("Content-type: application/x-msdownload");
	header("Content-Disposition: attachment; filename=$filename");
	header("Pragma: no-cache");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	*/
?>
<?php 
if($tipo==1 or $tipo==2 or $tipo==0){ ?>
<table border="1">
	<thead>
		<tr>
			<th>No Venta</th>
			<th>Cliente</th>
			<th>Cantidad</th>
			<th>Equipo</th>
			<th>Vendedor</th>
			<th>Estatus</th>
			<th>Fecha</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<?php 
			if($tipo==1 or $tipo==0){
				$queryv = $this->ModeloCatalogos->reporteventasnormal($fechaini,$fechafin,$cliente,0,0);
         		
         		foreach ($queryv->result() as $item) { ?>
        			<tr>
						<td><?php echo $item->id; ?></td>
						<td><?php echo $item->empresa; ?></td>
						<td></td>
						<td></td>
						<td><?php echo $item->nombre.' '.$item->apellido_paterno.' '.$item->apellido_materno; ?></td>
						<td><?php if($item->activo==1){ echo 'vigente';}else{ echo 'Eliminado';} ?></td>
						<td><?php echo $item->reg; ?></td>
						<td><?php echo $item->motivo; ?></td>
					</tr>
					<?php $resultadoequipos = $this->ModeloCatalogos->db6_getselectwheren('ventas_has_detallesEquipos',array('idVenta'=>$item->id));
						  foreach ($resultadoequipos->result() as $iteme) { ?>
						  	<tr>
								<td colspan="2"></td>
								<td><?php echo $iteme->cantidad;?></td>
								<td><?php echo $iteme->modelo;?></td>
								<td colspan="4"></td>
							</tr>
					<?php }
						  $resultadoaccesorios = $this->ModeloCatalogos->accesoriosventa($item->id);
						  foreach ($resultadoaccesorios->result() as $itema) { ?>
					
							<tr>
								<td colspan="2"></td>
								<td><?php echo $itema->cantidad;?></td>
								<td><?php echo $itema->nombre;?></td>
								<td colspan="4"></td>
							</tr>
					<?php } 
						  $resultadoconsumibles = $this->ModeloCatalogos->consumiblesventa($item->id);
						  foreach ($resultadoconsumibles->result() as $itemc) { ?>	
							<tr>
								<td colspan="2"></td>
								<td><?php echo $itemc->cantidad;?></td>
								<td><?php echo $itemc->modelo;?></td>
								<td colspan="4"></td>
							</tr>
					<?php } 
						  $consumiblesventadetalles = $this->ModeloCatalogos->consumiblesventadetalles($item->id);
						  foreach ($consumiblesventadetalles->result() as $itemcc) { ?>	
							<tr>
								<td colspan="2"></td>
								<td><?php echo $itemcc->cantidad;?></td>
								<td><?php echo $itemcc->modelo;?></td>
								<td colspan="4"></td>
							</tr>
					<?php }
						  $ventadetallesrefacion = $this->ModeloCatalogos->ventas_has_detallesRefacciones($item->id);
						  foreach ($ventadetallesrefacion->result() as $itemr) { ?>	
								<tr>
									<td colspan="2"></td>
									<td><?php echo $itemr->cantidad;?></td>
									<td><?php echo $itemr->modelo;?></td>
									<td colspan="4"></td>
								</tr>		
        <?php  			  }
    			}
			} ?>
		<?php 
			if($tipo==2 or $tipo==0){
				$queryv = $this->ModeloCatalogos->reporteventasconbinada($fechaini,$fechafin,$cliente,0);
         		
         		foreach ($queryv->result() as $item) { 
         				$idvc=$item->combinadaId;
         				if($item->equipo>0){
                            $idvc=$item->equipo;
                        }elseif($item->consumibles>0){
                            $idvc=$item->consumibles;
                        }elseif($item->refacciones>0){
                            $idvc=$item->refacciones;
                        }elseif($item->poliza>0){
                            $idvc=$item->poliza;
                        }
         			?>
        			<tr>
						<td><?php echo $idvc; ?></td>
						<td><?php echo $item->empresa; ?></td>
						<td></td>
						<td></td>
						<td><?php echo $item->nombre.' '.$item->apellido_paterno.' '.$item->apellido_materno; ?></td>
						<td><?php if($item->activo==1){ echo 'vigente';}else{ echo 'Eliminado';} ?></td>
						<td><?php echo $item->reg; ?></td>
						<td><?php echo $item->motivo; ?></td>
					</tr>
					<?php $resultadoequipos = $this->ModeloCatalogos->db6_getselectwheren('ventas_has_detallesEquipos',array('idVenta'=>$item->equipo));
						  foreach ($resultadoequipos->result() as $iteme) { ?>
						  	<tr>
								<td colspan="2"></td>
								<td><?php echo $iteme->cantidad;?></td>
								<td><?php echo $iteme->modelo;?></td>
								<td colspan="4"></td>
							</tr>
					<?php }
						  $resultadoaccesorios = $this->ModeloCatalogos->accesoriosventa($item->equipo);
						  foreach ($resultadoaccesorios->result() as $itema) { ?>
					
							<tr>
								<td colspan="2"></td>
								<td><?php echo $itema->cantidad;?></td>
								<td><?php echo $itema->nombre;?></td>
								<td colspan="4"></td>
							</tr>
					<?php } 
						  $resultadoconsumibles = $this->ModeloCatalogos->consumiblesventa($item->equipo);
						  foreach ($resultadoconsumibles->result() as $itemc) { ?>	
							<tr>
								<td colspan="2"></td>
								<td><?php echo $itemc->cantidad;?></td>
								<td><?php echo $itemc->modelo;?></td>
								<td colspan="4"></td>
							</tr>
					<?php } 
						  $consumiblesventadetalles = $this->ModeloCatalogos->consumiblesventadetalles($item->consumibles);
						  foreach ($consumiblesventadetalles->result() as $itemcc) { ?>	
							<tr>
								<td colspan="2"></td>
								<td><?php echo $itemcc->cantidad;?></td>
								<td><?php echo $itemcc->modelo;?></td>
								<td colspan="4"></td>
							</tr>
					<?php }
						  $ventadetallesrefacion = $this->ModeloCatalogos->ventas_has_detallesRefacciones($item->refacciones);
						  foreach ($ventadetallesrefacion->result() as $itemr) { ?>	
								<tr>
									<td colspan="2"></td>
									<td><?php echo $itemr->cantidad;?></td>
									<td><?php echo $itemr->modelo;?></td>
									<td colspan="4"></td>
								</tr>		
        <?php  			  }
    			}
			} ?>
		
	</tbody>
</table>
<?php 
} 
if($tipo==3 or $tipo==0){ ?>
<table border="1">
	<thead>
		<tr>
			<th>No Contrato</th>
			<th>Cliente</th>
			<th>Cantidad</th>
			<th>Equipo</th>
			<th>Vendedor</th>
			<th>Estatus</th>
			<th>Fecha</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<?php
			$contrastos = $this->ModeloCatalogos->reportecontratos($fechaini,$fechafin,$cliente);
			foreach ($contrastos->result() as $itemc) { ?>
				<tr>
					<td><?php echo $itemc->idcontrato;?></td>
					<td><?php echo $itemc->empresa;?></td>
					<td>Cantidad</td>
					<td>Equipo</td>
					<td><?php echo $itemc->nombre.' '.$itemc->apellido_paterno.' '.$itemc->apellido_materno; ?></td>
					<td><?php if($itemc->estatus==1){ echo 'vigente';}else{ echo 'Eliminado';} ?></td>
					<td><?php echo $itemc->fechasolicitud;?></td>
					<td></td>
				</tr>
		<?php
				$rentaventas = $this->ModeloCatalogos->rentaventasd($itemc->idRenta);  
				foreach ($rentaventas->result() as $iteme) { 
					$renta_id = $iteme->idRenta;
                	$equipo_id = $iteme->idEquipo;
                	$serieId = $iteme->serieId;
					?>
				 	<tr>
						<td colspan="2"></td>
						<td><?php echo $iteme->cantidad;?></td>
						<td><?php echo $iteme->modelo;?></td>
						<td colspan="4"></td>
					</tr>	
		  <?php 	$rentaequipos_consumible = $this->ModeloCatalogos->rentaequipos_consumible($renta_id,$equipo_id);
                          foreach ($rentaequipos_consumible->result() as $itemcc) { ?>
                          	<tr>
								<td colspan="2"></td>
								<td><?php echo $itemcc->cantidad;?></td>
								<td><?php echo $itemcc->modelo;?></td>
								<td colspan="4"></td>
							</tr>
                    <?php }
                    $rentaequipos_accesorios = $this->ModeloCatalogos->rentaequipos_accesorios($renta_id,$equipo_id);
                          foreach ($rentaequipos_accesorios->result() as $itema) { ?>
                          	<tr>
								<td colspan="2"></td>
								<td><?php echo $itema->cantidad;?></td>
								<td><?php echo $itema->nombre;?></td>
								<td colspan="4"></td>
							</tr>
                    <?php }
				} 
			} ?>
	</tbody>
</table>
<?php } ?>