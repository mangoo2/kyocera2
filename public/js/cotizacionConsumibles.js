var base_url = $('#base_url').val();
var editor; 
var idCotizacion = $('#idCotizacion').val();
/*
$(document).ready(function(){  

    editor = new $.fn.dataTable.Editor( {
            ajax: base_url+"index.php/Cotizaciones/actualizaDetalleCotizacionConsumible",
            table: "#tablaRefacciones",
            fields: [ {
                    label: "precioGeneral:",
                    name: "precioGeneral"
                },
                {
                    label: "totalGeneral:",
                    name: "totalGeneral"
                }
            ]
        } );

    table = $('#tablaRefacciones').DataTable({
      "dom": 't',
      "ordering": false
    });


    load(idCotizacion);

    // Activa la edición por casilla en cada ROW de la tabla
    $('#tablaRefacciones').on( 'click', 'tbody td:not(:first-child)', function (e){
        editor.inline(this);
    });

});


function btn_guardar(){
    $('.print_css').append('.cot{color: red}');
    $('.pantalla').remove();
    $('.usuario').remove();
    $('.menu').remove();
    $('.kyoc').remove();
    $('.log').append("<img class='kyoc' src='"+base_url+"app-assets/images/altakyo.png' alt='materialize logo' width='200px;' height='100px;'>");
    $('.venta_text').append("<h6 align='center' style='font-size:15px; color:black;'>Venta-Renta-Mantenimiento<br>Impresoras y Multifuncionales monocromáticos y color<br> Blvd. Norte No. 18831, San Alejandro, CP 72090, Puebla, Pue.<br> Call Center: 273 3400, 249 5393<br> Kyoceraap.com</h6>");
   // $('.venta_img').append("<img class='kyoc' src='"+base_url+"app-assets/images/1024_kyocera_logo.png' alt='materialize logo' width='200px;' height='80px;'>");
    $('.btn_imp').remove();
    setTimeout(function() {window.print();}, 500);
    setTimeout(function() {location.reload();}, 5000);
}

  
function load(idCotizacion) {
    console.log('entrando');
    // Destruye la tabla y la crea de nuevo con los datos cargados  
    table.destroy();
    table = $('#tablaRefacciones').DataTable({
        "ajax": {
            "url": base_url+"index.php/Cotizaciones/getTablaDetallesConsumibles/"+idCotizacion
        },
        "columns": [
            
            {"data": "piezas", class: "tablaBordesPrincipal"},
            {"data": "modelo", class: "tablaBordesPrincipal"},
            {"data": "parte", class: "tablaBordesPrincipal"},
            {"data": "equipo", class: "tablaBordesPrincipal"},
            {"data": "rendimiento", class: "tablaBordesPrincipal"},
            {"data": "precioGeneral", class: "tablaBordesPrincipal"},
            {"data": "totalGeneral", class: "tablaBordesPrincipal"},
            {"data": "idConsumible", class: "tablaBordesPrincipal", visible: false}
        ],
        "ordering": false,
        "dom": 't'
    });
*/
function btn_guardar(){
    //window.open(base_url+'Cotizaciones/pdf/'+idCotizacion, '_blank');
   //$('.btn_imp').remove();
   saveinfo_o(1);
    saveinfo_o(2);
    saveinfo_o(3);
    saveinfo_o(4);
    setTimeout(function() {window.print();}, 500);
}
function editar(id,costo,cantidad){
    if ($('#estatuscot').val()==1) {
        $('#password').val('');
        $('#modaleditor').modal();
        $('#modaleditor').modal('open');
        $('#newprecio').val(costo);
        $('#idEquipon').val(id);
        $('#cantidad').val(cantidad); 
    }
}
$('.editarcosto').click(function(event) {
        var precio = $('#newprecio').val()==''?0:$('#newprecio').val();
        var equipo=$('#idEquipon').val();
        var pass=$('#password').val();
        var cantidad = $('#cantidad').val();
        var sumatotal = precio;
        if (pass!='') {
            //if (precio>0) {
                $.ajax({
                    type:'POST',
                    url: base_url+'Cotizaciones/editarprecioConsumicles',
                    data: {
                        precio: precio,
                        equipo: equipo,
                        pass: pass,
                        sumtotal: sumatotal
                        },
                        async: false,
                        statusCode:{
                            404: function(data){
                                swal("Error", "404", "error");
                            },
                            500: function(){
                                swal("Error", "500", "error"); 
                            }
                        },
                        success:function(data){
                          if (data==1) {
                            swal("Editado!", "Precio Editado!", "success");
                            setTimeout(function(){ window.location.href=''; }, 3000);
                          }else{
                            swal("Error", "No tiene permiso", "error"); 
                          }

                        }
                    });
            //}else{
              //  swal("Error", "El precio no debe de ser menor o igual a cero", "error"); 
            //}
        }else{
            swal("Error", "Debe de ingresar una contraseña", "error"); 
        }   
    });
function saveinfo(idcoti,tipo){
    var info=$('.info'+tipo).html();
    console.log(info);
    $.ajax({
        type:'POST',
        url: base_url+'Generales/editarinfocotizacion',
        data: {
            cotizacion: idcoti,
            tipo: tipo,
            info: info
            },
            async: false,
            statusCode:{
                404: function(data){
                    //swal("Error", "404", "error");
                },
                500: function(){
                    //swal("Error", "500", "error"); 
                }
            },
            success:function(data){
              
            }
        });
}
function editarp(id,piezas){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: 'nueva cantidad a modificar<br>'+
                 '<input type="number" id="newcantidad" class="name form-control" value="'+piezas+'" required/>',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                
                     $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Cotizaciones/editarpiezasConsumicles",
                        data: {
                            id:id,
                            cantidad:$('#newcantidad').val()
                        },
                        success: function (response){
                            location.reload();
                        },
                        error: function(response){
                            $.alert({
                                    boxWidth: '30%',
                                    useBootstrap: false,
                                    title: 'Error!',
                                    content: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema'});   
                        }
                    });
            },
            cancelar: function (){
            }
        }
    });
}
function deleteconsumiblev(id){
        var html="¿Desea eliminar el consumible?";
     $.confirm({
            boxWidth: '30%',
            useBootstrap: false,
            icon: 'fa fa-warning',
            title: 'Editar condiciones!',
            content: html,
            type: 'red',
            typeAnimated: true,
            buttons:{
                confirmar: function (){
                    $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Cotizaciones/deleteconsumiblev",
                        data: {
                            id:id
                        },
                        success: function (response){
                                location.reload();
                        },
                        error: function(response){
                            $.alert({
                                    boxWidth: '30%',
                                    useBootstrap: false,
                                    title: 'Error!',
                                    content: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema'});   
                        }
                    });   
            },
                cancelar: function () {
                }
            }
        });
}