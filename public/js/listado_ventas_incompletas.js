var base_url = $('#base_url').val();
var table;
var idpago;
var tipopago;
var fechahoy = $('#fechahoy').val();
var add_ser_ex_idventa=0;
var add_ser_ex_tipovc=0;
$(document).ready(function(){ 
    $('.modal').modal({dismissible: true});
    $('.chosen-select').chosen({width: "90%"});
    $('.selecte_general').select2();
    table = $('#tabla_ventas_incompletas').DataTable();
	loadtable();
    searchcliente();
    
    $('#cli_rfc').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una opción',
        allowClear: true,
        ajax: {
            url: base_url+'Ventas/searchrfcpres',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    var html='';
                    itemscli.push({
                        id: element.rfc,
                        text: element.rfc

                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    });
    $("#pago_p").TouchSpin({
                min: 0,
                max: 9999999999999999999999999999999999,
                initval: 0.25,
                step: 0.01,
                decimals: 2,
            });
    $.switcher('#tipoview');
    $('.filtrosdatatable').change(function(event) {
        table.state.clear();
    });
     
}); 
function searchcliente(){
    $('#cliente').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una opción',
        allowClear: true,
        ajax: {
            url: base_url+'Ventasi/searchclientes',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    var html='';
                    if(element.empresa==element.razon_social){
                        html=element.empresa;
                    }else{
                        html=element.empresa+' ('+element.razon_social+')';
                    }
                    itemscli.push({
                        id: element.id,
                        text: html

                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    });
}
function loadtableswitch(){
    setTimeout(function(){ 
        load();
    }, 500);
}
function loadtable(){
    var tipoventa=$('#tipoventa option:selected').val();
    if(tipoventa==0){
        loadtodo();
    }
    if (tipoventa==1) {
        load();
        verificaruser();
    }
    if (tipoventa==2) {
        load();
        verificaruser();
    }
    if (tipoventa==3) {
        loadservicio();
        $('.div_servicio').css('display','block');
    }
    if (tipoventa==4) {
        //window.location.href = base_url+"index.php/Ventasp";
        loadp();
    }
    if (tipoventa==5) {
        load();
        verificaruser();
        $('.divsologarantias').hide();
    }else{
        $('.divsologarantias').hide();
    }
}
function garantiaexport(){
    var cliente= $('#cliente option:selected').val()==undefined?0:$('#cliente option:selected').val();
    var estatus= $('#tipoestatus option:selected').val();
    var tipoventa= $('#tipoventa option:selected').val();//1 venta 2 combinada 3 servicios
    var estatusventa= $('#tipoventadelete option:selected').val();
    var tipoview = $('#tipoview').is(':checked')==true?1:0;
    var idpersonal = $('#ejecutivoselect option:selected').val();
    var fechainicial_reg = $('#fechainicial_reg').val();
    var emp=$('#empresaselect option:selected').val();
    window.open(base_url+"Ventas/exportgarantias?cliente="+cliente+"&status="+estatus+"&tipoventa="+tipoventa+"&estatusventa="+estatusventa+"&tipoview="+tipoview+"&idpersonal="+idpersonal+"&fechainicial_reg="+fechainicial_reg+'&emp='+emp, '_blank');  

}
function load() {
    var cliente= $('#cliente option:selected').val()==undefined?0:$('#cliente option:selected').val();
    var estatus= $('#tipoestatus option:selected').val();
    var tipoventa= $('#tipoventa option:selected').val();//1 venta 2 combinada 3 servicios
    var estatusventa= $('#tipoventadelete option:selected').val();
    var tipoview = $('#tipoview').is(':checked')==true?1:0;
    var idpersonal = $('#ejecutivoselect option:selected').val();
    var fechainicial_reg = $('#fechainicial_reg').val();
    var fechafinal_reg = $('#fechafinal_reg').val();
    var emp=$('#empresaselect option:selected').val();
    var filf=$('#filtrarfolio').is(':checked')==true?1:0;
    var rfc =$('#cli_rfc option:selected').val()==undefined?'':$('#cli_rfc option:selected').val();
    var pre =$('#s_prefactura option:selected').val();
    
    // Destruye la tabla y la crea de nuevo con los datos cargados 
    table.destroy();
    table = $('#tabla_ventas_incompletas').DataTable({
           //pagingType: 'full_numbers',
           pagingType: 'input',
           //"sPaginationType": "listbox",
        //stateSave: true,
        responsive: !0,
        "bProcessing": true,
         search: {
                return: true
            }, 
        "serverSide": true,
        "ajax": {
            "url": base_url+"index.php/Ventas/getListaVentasIncompletasasi/",
            type: "post",
            "data": {
                'cliente':cliente,
                'status':estatus,
                'tipoventa':tipoventa,
                'estatusventa':estatusventa,
                'tipoview':tipoview,
                'idpersonal':idpersonal,
                'fechainicial_reg':fechainicial_reg,
                'fechafinal_reg':fechafinal_reg,
                'emp':emp,
                'filf':filf,
                'pagosview':0,
                'rfc':rfc,
                'pre':pre
            },
        },
        "columns": [
            {"data": "id",
                render:function(data,type,row){
                    var html='';
                    if (row.combinada==1) {
                        if(row.equipo>0){
                            html=row.equipo;
                        }else if(row.consumibles>0){
                            html=row.consumibles;
                        }else if(row.refacciones>0){
                            html=row.refacciones;
                        }
                    }else{
                        html=row.id;
                    }
                    return html;
                }
            },
            {"data": "empresa"},
            {"data": "id",
                render:function(data,type,row){
                    if (row.prefactura==1) {
                        var colorbtton='green';
                        var btn_vm='';
                    }else{
                        var colorbtton='blue';
                        var btn_vm='<button class="btn-floating blue btn-mas" onclick="additemventa('+row.id+','+row.combinada+','+row.idCliente+')"><i class="fas fa-plus fa-fw"></i></button>';
                    }
                    var html='<span class="btn_vm"><a class="btn-floating '+colorbtton+' tooltipped" data-position="top" data-delay="50" data-tooltip="Prefactura" onclick="detallev('+row.id+','+row.combinada+')"><i class="material-icons">assignment</i></a>'+btn_vm+'</span>';
                    var totalcon= 0;
                    var totalref= 0;
                    var totalequ= 0;
                    var totalacc= 0;
                    
                    if(row.total_general>0){
                        var totalventa=row.total_general;
                    }else{
                        if(row.totalcon>0){
                            var totalcon=row.totalcon;
                        }
                        if(row.totalref>0){
                            var totalref=row.totalref;
                        }
                        if(row.totalequ>0){
                            var totalequ=row.totalequ;
                        }
                        if(row.totalacc>0){
                            var totalacc=row.totalacc;
                        }

                        var totalventa=parseFloat(totalcon)+parseFloat(totalref)+parseFloat(totalequ)+parseFloat(totalacc);
                        html+='<!--'+parseFloat(totalcon)+'+'+parseFloat(totalref)+'+'+parseFloat(totalequ)+'='+totalventa+' '+row.totalref+'-->';
                    }
                    if(totalventa==0){
                        html+=' Renta';
                    }
                    if(row.combinada==3){
                        var colorbtton='blue';
                        if (row.prefactura==1) {
                            colorbtton='green';
                        }
                        var html='<a class="btn-floating '+colorbtton+' tooltipped" data-position="top" data-delay="50" data-tooltip="Prefactura" onclick="detallep('+row.id+')"><i class="material-icons">assignment</i></a>';
                        if(row.viewcont==1){
                            if (row.prefactura==1) {
                                html+='<a href="'+base_url+'PolizasCreadas/servicios/'+row.id+'" class="btn-floating tooltipped" data-position="top" data-delay="50" data-tooltip="Servicio" target="_black"><i class="fas fa-file-invoice"></i></a>';
                            }
                        }
                    }
                    return html;
                }
            },
            {"data": "vendedor"},
            {"data": "estatus",
                 render:function(data,type,row){
                    var html=''; 
                    if(data==1){
                        html='Pendiente';    
                    } 
                    if(data==3){
                        html='Pagado';    
                    }  
                    return html;
                }
            },
            {"data": "reg"}, 
            {"data": null,
                 render:function(data,type,row){
                    var html='';
                    if (row.prefactura==1) {
                        if(row.vencimiento!='0000-00-00'){
                            var html=row.vencimiento;    
                        }    
                    }  
                    return html;
                }
            }, 
            {"data": null,
                 render:function(data,type,row){
                    var html='';
                    if (row.prefactura==1) {
                        if(row.fechaentrega!='0000-00-00'){
                            html=row.fechaentrega;    
                        }
                    }  
                    return html;
                }
            }, 
            {"data": null,
                 render:function(data,type,row){
                    var html='';
                    if (row.tipov==1) {
                        html='Alta Productividad';    
                    }
                    if (row.tipov==2) {
                        html='D-Impresión';    
                    }  
                    return html;
                }
            }, 
            {"data": "combinada",
                 render:function(data,type,row){
                    var html='';
                    var totalcon= 0;
                    var totalref= 0;
                    var totalequ= 0;
                    var totalacc= 0;
                    var totalcon2= 0;
                    /*
                    if(data==1){
                        var html='Venta combinada';    
                    } else{
                        var html='Venta'; 
                    }
                    */
                    
                    if(row.total_general>0){
                        var totalventa=row.total_general;
                        if (row.combinada==3) {
                            if(totalventa>0){
                                if(row.siniva==1){
                                    totalventa=parseFloat(totalventa)+parseFloat(totalventa);
                                }else{
                                    totalventa=parseFloat(totalventa)+(parseFloat(totalventa)*0.16);
                                }
                                
                            }
                            
                        }
                    }else{
                        if(row.totalcon>0){
                            var totalcon=row.totalcon;
                        }
                        if(row.totalref>0){
                            var totalref=row.totalref;
                        }
                        if(row.totalequ>0){
                            var totalequ=row.totalequ;
                        }
                        if(row.totalacc>0){
                            var totalacc=row.totalacc;
                        }
                        if(row.totalcon2>0){
                            var totalcon2=row.totalcon2;
                        }
                        var totalventa=parseFloat(totalcon)+parseFloat(totalref)+parseFloat(totalequ)+parseFloat(totalacc)+parseFloat(totalcon2);
                        if (row.combinada==1) {
                            if(row.totalpoli>0){
                                var totalpoli=row.totalpoli;
                            }else{
                                var totalpoli= 0;
                            }
                            totalventa=parseFloat(totalventa)+parseFloat(totalpoli);
                        }
                        if(row.siniva==1){
                            totalventa=totalventa+parseFloat(totalventa);
                        }else{
                            totalventa=totalventa+(parseFloat(totalventa)*0.16);
                        }
                        html+='<!--consumibles:'+parseFloat(totalcon)+'-->\
                                <!--consumibles:'+parseFloat(totalcon2)+'-->\
                                <!--refacciones'+parseFloat(totalref)+'-->\
                                <!--equipos'+parseFloat(totalequ)+'-->\
                                <!--accesorios'+parseFloat(totalacc)+'-->\
                                <!--Total:'+totalventa+'-->';
                        }
                        
                    html+=new Intl.NumberFormat('es-MX',{style: "currency", currency: "MXN"}).format(totalventa);
                        return html;
                }
            }, 
            //{"data": "idCotizacion"}, 
            {"data": "facturas",
                render:function(data,type,row){
                    var html='';
                    if(row.facturas!='' && row.facturas!='null' && row.facturas!=null){
                        html+=row.facturas;
                        html+=' <a class="b-btn b-btn-primary tooltipped" data-position="top" data-delay="50" data-tooltip="Desvicular Factura" onclick="desvfactura('+row.id+','+row.combinada+')"><i class="fas fa-unlink"></i></a>';
                    }
                        
                    return html;
                }
            },
            {"data": "id",
                 render:function(data,type,row){
                    var totalcon= 0;
                    var totalref= 0;
                    var totalequ= 0;
                    var totalacc= 0;
                    var totalcon2= 0;


                    if(row.facturas!='' && row.facturas!='null' && row.facturas!=null){
                        var fac=1;
                    }else{
                        var fac=0;
                    }
                    var html='<div class="btn_list">';
                   
                    if(row.total_general>0){
                        var totalventa=row.total_general;
                    }else{
                        if(row.totalcon>0){
                            var totalcon=row.totalcon;
                        }
                        if(row.totalref>0){
                            var totalref=row.totalref;
                        }
                        if(row.totalequ>0){
                            var totalequ=row.totalequ;
                        }
                        if(row.totalacc>0){
                            var totalacc=row.totalacc;
                        }
                        if(row.totalcon2>0){
                            var totalcon2=row.totalcon2;
                        }
                        var totalventa=parseFloat(totalcon)+parseFloat(totalref)+parseFloat(totalequ)+parseFloat(totalacc)+parseFloat(totalcon2);
                            if(row.siniva==1){
                                totalventa=totalventa.toFixed(2);
                            }else{
                                totalventa=(totalventa+(parseFloat(totalventa)*0.16)).toFixed(2);
                            }
                            
                    }
                    $('.tooltipped').tooltip();
                    if(row.e_entrega==1){
                        var disabledbtn='disabled';
                    }else{
                        var disabledbtn='';
                    }
                    if(row.comentario!='' && row.comentario!='null' && row.comentario!=null){
                        var btn_com='green';
                    }else{
                        var btn_com='blue';
                    }
                    html+='<button class="btn-floating '+btn_com+' tooltipped coment_'+row.id+'_'+row.combinada+'" data-comentario="'+row.comentario+'" data-position="top" data-delay="50" data-tooltip="Comentario" onclick="comentarioventa('+row.id+','+row.combinada+')"><i class="fas fa-comment fa-fw"></i></button> ';
                    if(estatusventa==1){
                        html+='<button class="btn-floating red tooltipped" data-position="top" data-delay="50" data-tooltip="Eliminar" onclick="eliminarventa('+row.id+','+row.combinada+','+fac+')" '+disabledbtn+'><i class="fas fa-trash-alt"></i></button> ';
                    }

                    //if(parseInt(row.estatus)==1){
                        
                        if (row.prefactura==1 && totalventa>0) {
                            if(row.estatus==3){
                                var colorbuton='green';
                            }else{
                                var colorbuton='blue';
                                //if(row.vencimiento!='0000-00-00'){
                                    if(row.vencimiento<fechahoy){
                                        var colorbuton='red';
                                    }
                                //}
                            }
                            html+='<a class="btn-floating '+colorbuton+' tooltipped modal_pagos modal_pagos_'+row.id+' verificar_'+row.id+'_'+row.combinada+' modal_pago_'+row.combinada+'_'+row.id+'" data-cliente="'+row.idCliente+'" \
                                    data-position="top" data-delay="50" \
                                    data-tooltip="Pago" \
                                    data-idventa="'+row.id+'" \
                                    data-tipoventa="'+row.combinada+'" \
                                    onclick="modal_pago('+row.id+','+row.combinada+')">\
                                        <i class="material-icons">attach_money</i>\
                                      </a> ';
                        }
                        
                    //}
                    if(parseInt(row.activo)==1){
                        if (row.prefactura==1) {
                            html+='<a class="b-btn b-btn-primary tooltipped" \
                                    data-position="top" data-delay="50" data-tooltip="Agregar Factura" \
                                    onclick="addfactura('+row.id+','+row.idCliente+','+row.combinada+','+totalventa+','+row.tipov+')"><i class="fas fa-folder-plus"></i></a> ';
                        }
                    }   //if (row.prefactura==1) {
                        html+='<a class="b-btn b-btn-primary tooltipped solounosusuarios" \
                                    data-position="top" data-delay="50" data-tooltip="Clonar Venta" \
                                    onclick="v_ext_copy('+row.id+','+row.combinada+')"> <i class="fas fa-copy fa-fw"></i></a> ';
                        //}
                    if(parseInt(row.activo)==1){ 
                        html+='<a class="b-btn b-btn-primary tooltipped" data-position="top" data-delay="50" data-tooltip="Cambiar personal" onclick="cambiar_per('+row.id+','+row.combinada+')"><i class="fas fa-user fa-fw"></i></a> ';
                        html+='<a class="b-btn b-btn-primary tooltipped" data-position="top" data-delay="50" data-tooltip="Cambiar Empresa x" onclick="cambiar_emp('+row.id+','+row.combinada+')"><i class="fas fa-hotel"></i></a> ';
                    }
                        
                    if(parseInt(row.activo)==1){
                        if(row.facturas!='' && row.facturas!='null' && row.facturas!=null){}else{
                            html+=' <a class="b-btn b-btn-primary tooltipped" data-position="top" data-delay="50" data-tooltip="Cambiar de cliente" onclick="cambiar_cliente('+row.id+','+row.combinada+')"><i class="fas fa-people-arrows"></i></a> ';
                        }
                    }
                    
                    if(row.combinada==3){
                        var html='';
                        if(row.siniva==1){
                            var totalventa=parseFloat(row.total_general).toFixed(2);
                        }else{
                            var totalventa=(parseFloat(row.total_general)+(parseFloat(row.total_general)*0.16)).toFixed(2);
                        }
                        
                        $('.tooltipped').tooltip();
                        var btn_cfdi='';
                            var btn_pago='';
                            if (row.prefactura==1) {
                               btn_cfdi='<a class="btn-floating blue tooltipped" data-position="top" data-delay="50" data-tooltip="CFDI" onclick="modal_cdfi('+row.id+')">CFDI</a>';
                                if(totalventa>0){
                                    if(row.estatus==3){
                                        var colorbuton='green';
                                    }else{
                                        var colorbuton='blue';
                                        //if(row.vencimiento!='0000-00-00'){
                                            console.log('Poliza '+row.id+' '+row.vencimiento+'<'+fechahoy);
                                            if(row.vencimiento<fechahoy){
                                                var colorbuton='red';
                                            }
                                        //}
                                    }
                                    btn_pago='<a class="btn-floating '+colorbuton+' tooltipped modal_pagosp verificarp_'+row.id+' modal_pago_4_'+row.id+'" data-cliente="'+row.idCliente+'" data-position="top" data-delay="50" data-tooltip="Pago" data-poliza="'+row.id+'" onclick="modal_pago('+row.id+',4)">\
                                            <i class="material-icons">attach_money</i></a>';
                                }
                            }
                            if(estatusventa==1){
                                html+='<a class="btn-floating red tooltipped facturar" data-position="top" data-delay="50" data-tooltip="Eliminar" onclick="finalizapo('+row.id+')"><i class="fas fa-trash-alt"></i></a>';
                            }

                            html+=''+btn_pago+'';
                        if(parseInt(row.activo)==1){
                            if (row.prefactura==1 && totalventa>0) {
                                
                                html+='<a class="b-btn b-btn-primary tooltipped" data-position="top" data-delay="50" \
                                        data-tooltip="Agregar Factura"  onclick="addfactura('+row.id+','+row.idCliente+',2,'+totalventa+','+row.tipov+')"><i class="fas fa-folder-plus"></i></a>';
                            }
                        }
                        if(parseInt(row.activo)==1){
                            html+=' <a class="b-btn b-btn-primary tooltipped" data-position="top" data-delay="50" data-tooltip="Cambiar personal" onclick="cambiar_per('+row.id+',2)"><i class="fas fa-user fa-fw"></i></a>';
                            html+=' <a class="b-btn b-btn-primary tooltipped" data-position="top" data-delay="50" data-tooltip="Cambiar Empresa" onclick="cambiar_emp('+row.id+',2)"><i class="fas fa-hotel"></i></a> ';
                            if(row.facturas!='' && row.facturas!='null' && row.facturas!=null){}else{
                                html+=' <a class="b-btn b-btn-primary tooltipped" data-position="top" data-delay="50" data-tooltip="Cambiar de cliente" onclick="cambiar_cliente('+row.id+',2)"><i class="fas fa-people-arrows"></i></a> ';
                            }
                        }
                        html+='';
                    }
                    if(parseInt(row.activo)==1){
                        //if (row.prefactura==1) {
                            if(row.combinada<3){
                                
                                html+=' <a class="b-btn b-btn-primary tooltipped" data-position="top" data-delay="50" data-tooltip="Agregar Servicio Existente" onclick="agregarservicioex('+row.id+','+row.combinada+','+row.idCliente+')"><i class="fab fa-staylinked"></i></a> ';
                            }
                        //}
                        if(totalventa>0){
                            if(row.facturas!='' && row.facturas!='null' && row.facturas!=null){}else{
                                html+='<a class="b-btn b-btn-primary tooltipped" data-position="top" data-delay="50" data-tooltip="Ir a Generación de factura" onclick="irfactura('+row.idCliente+','+row.combinada+','+row.id+','+row.tipov+')"><i class="fas fa-file-invoice"></i></a>';
                            }
                        }
                    }
                    if(row.requ_fac==1){
                        var r_f_i='b-btn-primary';
                        var r_f_it='Requiere factura';
                    }else{
                        var r_f_i='b-btn-warning';
                        var r_f_it='No requiere factura';
                    }
                    html+=' <a class="b-btn '+r_f_i+' tooltipped" data-position="top" data-delay="50" data-tooltip="'+r_f_it+'" onclick="requiere_factura('+row.id+','+row.combinada+','+row.requ_fac+')"><i class="fas fa-file-invoice"></i></a> ';
                    
                    html+='</div">';
                    
                    return html;
                }
            }
        ],
        "order": [[ 5, "desc" ]],
        "language": {
            "processing": "<div class='row'><div class='col s12 procesandoclass'><img src='"+base_url+"public/img/tenor.gif' width='50px;' height='50px;'>Procesando</div></div>"
        },
        createdRow:function(row,data,dataIndex){
            $(row).find('td:eq(8)')
                .addClass('obtencionfacturas venta_tipo_'+data.combinada+'_id_'+data.id)
                .attr('data-tipo',data.combinada).attr('data-idv',data.id);
        }
    }).on('draw',function(){
        $("input[type=search]").addClass('input_search');
        $('.dataTables_filter').append('<i class="fa fa-search buttonsearch" onclick="searchtable_v()"></i>')
        vericarpagosventas();
        if(tipoventa==5){
            vericarpagospoliza();
        }
    });

}
function searchtable_v(){
    var contenedor = document.getElementById('tabla_ventas_incompletas_filter');

    // Selecciona el input dentro del contenedor
    var input = contenedor.querySelector('input.form-control-bmz');

    // Obtén el valor del input
    var valorInput = input.value;
    console.log('valorInput '+valorInput);
    table.search(valorInput).draw();
}
function loadservicio(){
    var cliente= $('#cliente option:selected').val()==undefined?0:$('#cliente option:selected').val();
    var tipo_servicio= $('#tipo_servicio option:selected').val();
    var estatusventa= $('#tipoventadelete option:selected').val();
    // Destruye la tabla y la crea de nuevo con los datos cargados 
    var idpersonal = $('#ejecutivoselect option:selected').val();
    var fechainicial_reg = $('#fechainicial_reg').val();
    table.destroy();
    table = $('#tabla_ventas_incompletas').DataTable({
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        search: {
                return: true
            },
        "ajax": {
            "url": base_url+"index.php/Ventas/getlistaservicios",
            type: "post",
            "data": {
                'cliente':cliente,
                'tipo_servicio':tipo_servicio,
                'activo':estatusventa,
                'idpersonal':idpersonal,
                'fechainicial_reg':fechainicial_reg
            },
        },
        "columns": [
            {"data": "asignacionId"},
            {"data": "empresa"},
            {"data": null,
                render:function(data,type,row){
                    var prefactura=0;
                    if (row.prefacturaId>0) {
                        var colorbtton='green';
                    }else{
                        var colorbtton='blue';
                    }
                    var html='<a class="btn-floating '+colorbtton+' tooltipped" data-position="top" data-delay="50" data-tooltip="Prefactura" onclick="detalleservicio('+row.asignacionId+','+row.tipo+')"><i class="material-icons">assignment</i> </a>';
                    return html;
                }
            },
            {"data": null,"visible": false},
            {"data": null,"visible": false},
             
            {"data": "reg"}, 
            {"data": null,"visible": false},
            {"data": null,
                render:function(data,type,row){
                    var html='';
                    if (row.tipo==1) {html='Contrato';}
                    if (row.tipo==2) {html='Poliza';}
                    if (row.tipo==3) {html='Evento';}
                    return html;
                }
            }, 
            {"data": null,"visible": false},
            {"data": null,"visible": false},
            {"data": "id",
                 render:function(data,type,row){
                    $('.tooltipped').tooltip();
                    var html='';
                    if (row.activo==1) {
                        if (row.prefacturaId>0) {
                            html+='<a class="btn-floating blue tooltipped" data-position="top" data-delay="50" data-tooltip="CFDI" onclick="modal_cdfimser('+row.asignacionId+','+row.tipo+')">CFDI</a>';
                            html+='<a class="btn-floating green tooltipped" data-position="top" data-delay="50" data-tooltip="Pago" onclick="modal_pagoser('+row.asignacionId+','+row.tipo+')"><i class="material-icons">attach_money</i></a>';
                        }else{
                            html+='<a class="btn-floating red tooltipped" data-position="top" data-delay="50" data-tooltip="Eliminar" onclick="eliminarser('+row.asignacionId+','+row.tipo+')"><i class="fas fa-trash-alt"></i></a>';
                        }
                    }else{
                        //motivo de la eliminacion
                        html+='<a class="btn-floating red  eliminadoservicio_'+row.asignacionId+'_'+row.tipo+'" data-motivo="'+row.motivoeliminado+'" onclick="mensajedelete('+row.asignacionId+','+row.tipo+')"><i class="material-icons">description</i></a>';
                    }
                    return html;
                }
            }
        ],
        "order": [[ 0, "desc" ]],
        "language": {
            "processing": "<div class='row'><div class='col s12 procesandoclass'><img src='"+base_url+"public/img/tenor.gif' width='50px;' height='50px;'>Procesando</div></div>"
        },
    }).on('draw',function(){
        $('.dataTables_filter').append('<i class="fa fa-search buttonsearch" onclick="searchtable_v()"></i>')

    });
}
function obtencionfacturas(tipo,id){
    $.ajax({
      type:'POST',
      url: base_url+'index.php/Ventas/obtencionfacturas',
      data: {
        tipo:tipo,
        id:id
        },
      success:function(data){
          $('.venta_tipo_'+tipo+'_id_'+id).html(data);
      }
  });
}
function detallev(id,tipo){
    if (tipo==0) {
        window.open(base_url+"Prefactura/view/"+id, "Prefactura", "width=780, height=612");
    }else{
        window.open(base_url+"Prefactura/viewc/"+id, "Prefactura", "width=780, height=612");
    }
}
function additemventa(id,tipo,cli){
    window.open(base_url+"Ventas/add/"+cli+"?idventaadd="+id+"&idventatipo="+tipo, '_blank');  
}
function detalleservicio(id,tipo){
    window.open(base_url+"Prefactura/viewser/"+id+"/"+tipo, "Prefactura", "width=880, height=612");    
}
function finaliza(id,tipo){
    $('#id_venta').val(id);
    $('#tipoventa').val(tipo);
    $('#modalFinaliza').modal('open');
}
function finalizar_factura(){
    var id_v = $('#id_venta').val(); 
    var tipo = $('#tipoventa').val(); 
    // tipo 0 es la venta combinada
    if(tipo==0) {
        $.ajax({
          type: "POST",
          url: base_url+"index.php/Ventas/finalizar_factura",
          data:{id:id_v},
          success: function(data){
            alertfunction('Atención!','Finalizado correctamente');
            table.ajax.reload(); 
          }
        });
    }else{
        $.ajax({
          type: "POST",
          url: base_url+"index.php/Ventas/finalizar_facturac",
          data:{id:id_v},
          success: function(data){
            alertfunction('Atención!','Finalizado correctamente');
            table.ajax.reload(); 
          }
        });
    }
}
//////////////////////
function fecha_vencimiento(id_v){
    var result='';
    $.ajax({
      type: "POST",
      url: base_url+"index.php/Ventas/verificar_fecha_vencimiento",
      data:{id:id_v},
      async:false,
      success: function(data){
        result=data;
      }
    });   
    return result;
}
function modal_cdfi(id,tipo) {
    $('#id_venta_e').val(id);
    $('#tipo_e').val(tipo);
    $('#modal_cfdi').modal('open');
}
function update_cfdi(){
    var id_venta=$('#id_venta_e').val();
    var tipo=$('#tipo_e').val();
    var cfdi=$('#cdfi_e').val();
    $.ajax({
      type: "POST",
      url: base_url+"index.php/Ventas/update_cfdi_registro",
      data:{id_venta:id_venta,tipo:tipo,cfdi:cfdi},
      success: function(data){
        alertfunction('Atención!','Guardado correctamente');
      }
    }); 
    var tipo=$('#tipo_e').val('');
    var cfdi=$('#cdfi_e').val('');  
}
function modal_pago(id,tipo){
    $('#id_venta_p').val(id);
    $('#tipo_p').val(tipo);
    $('#fecha_p').val('');
    $('#pago_p').val('');
    $('#observacion_p').val('');
    $('#modal_pago').modal('open');
    if(tipo==1){//Combinada 
       $('.monto_prefactura').html('$ <span class="totalpoliza">'+monto_combinada(id)+'</span>');
    }
    if(tipo==0){// ventas
       $('.monto_prefactura').html('$ <span class="totalpoliza">'+monto_ventas(id)+'</span>');
    }
    if (tipo==4) {
        $('.monto_prefactura').html('$ <span class="totalpoliza">'+monto_poliza(id)+'</span>');
    }
    table_tipo_compra(id,tipo);
    setTimeout(function(){ 
        calcularrestante();
    }, 1000);
}
function calcularrestante(){
    var totalpoliza=parseFloat($('.totalpoliza').html());
    var pagos = 0;
    $(".montoagregado").each(function() {
        var vstotal = parseFloat($(this).html());
        pagos += Number(vstotal);
    });
    pagos=pagos.toFixed(2);
    var restante = parseFloat(totalpoliza)-parseFloat(pagos);
    if(restante<=0){
        restante=0;
        cambiarestatuspago(totalpoliza);
    }
    $('.restante_prefactura').html('$ <span class="restantepoliza">'+restante.toFixed(2)+'</span>');
}
function monto_ventas(id_v){//ventas monto de prefactura
    var result='';
    $.ajax({
      type: "POST",
      url: base_url+"index.php/Ventas/montoventas",
      data:{id:id_v},
      async:false,
      success: function(data){
        result=data;
      }
    });   
    return result;
}
function monto_combinada(id_v){//Combinada monto de prefactura
    var result='';
    $.ajax({
      type: "POST",
      url: base_url+"index.php/Ventas/montocombinada",
      data:{id:id_v},
      async:false,
      success: function(data){
        result=data;
      }
    });   
    return result;
}
function guardar_pago_compras(){
    $( ".guardar_pago_compras" ).prop( "disabled", true );
    setTimeout(function(){ 
         $(".guardar_pago_compras" ).prop( "disabled", false );
    }, 2000);
    var form_pago = $('#form_pago');
    form_pago.validate({
            rules: 
            { 
                fecha: {
                    required: true
                },
                idmetodo: {
                    required: true
                },
                pago: {
                    required: true
                }
            },
            // Para mensajes personalizados
            messages: 
            {   
                fecha:{
                    required: "Campo obligatirio"
                },
                idmetodo:{
                    required: "Campo obligatirio"
                },
                pago:{
                    required: "Campo obligatirio"
                }
            },
        errorElement : 'div'
    });
    var valid = $("#form_pago").valid();
    if(valid){
        var restante =parseFloat($('.restantepoliza').html());
        var pago_p =parseFloat($('#pago_p').val());
        if (pago_p>0 && pago_p<=restante) {
            var datos = $('#form_pago').serialize();
            $.ajax({
              type: "POST",
              url: base_url+"index.php/Ventas/guardar_pago_tipo",
              data:datos,
              success: function(data){
                 alertfunction('Atención!','Guardado correctamente');
                $('#fecha_p').val('');
                $('#pago_p').val('');
                $('#observacion_p').val('');
                table_tipo_compra($('#id_venta_p').val(),$('#tipo_p').val());
                setTimeout(function(){ 
                    calcularrestante();
                }, 1000);
                setTimeout(function(){
                    //AQUI SE AGREGARA LA FUNCION PARA QUE VUEVA A CONSULTAR AL CLIENTE SI SIGUE COMO SUSPENDIDO O NO
                }, 2000); 
              }
            });
        }else{
            alertfunction('Atención!','Monto mayor a 0 y menor o igual al restante');
        }
    }   
}
function table_tipo_compra(id,tipo){
    $('.text_tabla_pago').html('');
    $.ajax({
      type: "POST",
      url: base_url+"index.php/Ventas/table_get_tipo_compra",
      data:{id:id,tip:tipo},
      success: function(data){
        $('.text_tabla_pago').html(data);
      }
    });
}
function deletepagov(id,tipo){
    $('#modal_eliminarpago').modal('open');
    idpago=id;
    tipopago=tipo;
}
function eliminarpago(){
    $.ajax({
        type: "POST",
        url: base_url+"index.php/Ventas/vericar_pass",
        data:{
            pago:idpago,
            tipo:tipopago,
            pass:$('#ver_pass').val()
        },
        success: function(data){
            $('#ver_pass').val('');
            $('#modal_eliminarpago').modal('close');
            $('#modal_pago').modal('close');
            if(parseFloat(data)==1){
                alertfunction('Atención!','Pago Eliminado correctamente');
            }else{
                swal({ title: "Error",
                    text: "Verificar la contraseña",
                    type: 'error',
                    showCancelButton: false,
                    allowOutsideClick: false,
                });
            }
        }
    });
}
function  eliminarventa(id,tipo,fac) {
    if(fac==1){
        alertfunction('Atención!','Cancele la factura antes de continuar');
    }else{
        $.confirm({
            boxWidth: '30%',
            useBootstrap: false,
            icon: 'fa fa-warning',
            title: 'Atención!',
            content: '¿Desea eliminar la pre?<br>Motivo de la eliminacion<br><textarea id="motivoeliminacion"></textarea><br>Se necesita permisos de administrador<br>'+
                     '<input type="text" placeholder="Contraseña" id="contrasena" name="contrasena" class="name form-control-bmz" style="width:99%" required/>',
            type: 'red',
            typeAnimated: true,
            buttons:{
                confirmar: function (){
                    var mot=$('#motivoeliminacion').val();

                    //var pass=$('#contrasena').val();
                    var pass=$("input[name=contrasena]").val();
                    if (pass!='') {
                        $.ajax({
                            type:'POST',
                            url: base_url+"index.php/Sistema/solicitarpermiso",
                            data: {
                                pass:pass
                            },
                            success: function (response){
                                    var respuesta = parseInt(response);
                                    if (respuesta==1) {
                        //=============================================================================
                                    ventadelete(id,tipo,mot);
                        //=============================================================================
                                    }else{
                                        notificacion(2);
                                    }
                            },
                            error: function(response){  
                                 notificacion(1);
                            }
                        });

                    }else{
                        notificacion(0);
                    }
                    
                },
                cancelar: function (){
                    
                }
            }
        });
        setTimeout(function(){
                new MaskedPassword(document.getElementById("contrasena"), '\u25CF');
            },800);
    }
}
function  ventadelete(id,tipo,mo) {
    var motivo=$('#motivoeliminacion').val();
    $.ajax({
        type: "POST",
        url: base_url+"index.php/Ventas/ventadelete",
        data:{
            ventaid:id,
            ventatipo:tipo,
            motivo:mo
        },
        success: function(data){
            var status= parseFloat(data);
            if (status==1) {
                alertfunction('Atención!','Eliminado correctamente');
                $('#modaleliminar').modal('close');
                loadtable(); 
            }else{
                alertfunction('Atención!','No es posible eliminar, Venta facturada');
            }
        }
    });
}
function  eliminarser(id,tipo) {
    $('#motivoeliminacionservicio').val('');
    $('#deleteservicio').val(id);
    $('#deleteserviciotipo').val(tipo);
    $('#modaleliminarservicio').modal('open');
}
function  serviciodelete() {
    var motivo=$('#motivoeliminacionservicio').val();
    $.ajax({
        type: "POST",
        url: base_url+"index.php/Ventas/serviciodelete",
        data:{
            servicioid:$('#deleteservicio').val(),
            serviciotipo:$('#deleteserviciotipo').val(),
            motivo:$('#motivoeliminacionservicio').val()
        },
        success: function(data){
            loadtable();
            $('#modaleliminarservicio').modal('close');
        }
    });
}
function mensajedelete(id,tipo){
    var motivo=$('.eliminadoservicio_'+id+'_'+tipo).data('motivo');
    swal(motivo);
}
function modal_cdfimser(id,tipo) {
    $('#cdfi_ser').val('');
    $('#id_cfdi_servicio').val(id);
    $('#tipo_cfdi_servicio').val(tipo);
    $('#modal_cfdi_ser').modal('open');
}
function update_cfdi_ser(){
    var id=$('#id_cfdi_servicio').val();
    var tipo=$('#tipo_cfdi_servicio').val();
    var cfdi=$('#cdfi_ser').val();
    $.ajax({
      type: "POST",
      url: base_url+"index.php/Ventas/update_cfdi_servicio",
      data:{id:id,tipo:tipo,cfdi:cfdi},
      success: function(data){
        alertfunction('Atención!','Guardado correctamente');
      }
    }); 
      
}
//===========================================
function modal_pagoser(id,tipo){
    $('#idservicio').val(id);
    $('#tipos').val(tipo);
    $('#fecha_p_s').val('');
    $('#pago_p_s').val('');
    $('#observacion_p').val('');
    $('#modal_pago_s').modal('open');
    if(tipo==1){//Combinada 
       $('.monto_prefactura_s').html('$ <span class="totalpolizas">'+monto_servicios(id,1)+'</span>');
    }else if(tipo==2){// ventas
       $('.monto_prefactura_s').html('$ <span class="totalpolizas">'+monto_servicios(id,2)+'</span>');
    }else if(tipo==3){// ventas
       $('.monto_prefactura_s').html('$ <span class="totalpolizas">'+monto_servicios(id,3)+'</span>');
    }
    table_tipo_servicio(id,tipo);
    setTimeout(function(){ 
        calcularrestantes();
    }, 1000);
}
function guardar_pago_servicios(){
    var form_pago = $('#form_pago_servicio');
    form_pago.validate({
            rules: 
            { 
                fecha: {
                    required: true
                },
                idmetodo: {
                    required: true
                },
                pago: {
                    required: true
                }
            },
            // Para mensajes personalizados
            messages: 
            {   
                fecha:{
                    required: "Campo obligatirio"
                },
                idmetodo:{
                    required: "Campo obligatirio"
                },
                pago:{
                    required: "Campo obligatirio"
                }
            },
        errorElement : 'div'
    });
    var valid = $("#form_pago_servicio").valid();
    if(valid){
        var restante =parseFloat($('.totalpolizas').html());
        var pago_p =parseFloat($('#pago_p_s').val());
        if (pago_p>0 && pago_p<=restante) {
            var datos = $('#form_pago_servicio').serialize();
            $.ajax({
              type: "POST",
              url: base_url+"index.php/Ventas/guardar_pago_tipo_servicio",
              data:datos,
              success: function(data){
                 $.alert({
                    boxWidth: '30%',
                    useBootstrap: false,
                    title: 'Atención!',
                    content: 'Guardado correctamente'}
                );
                $('#fecha_p').val('');
                $('#pago_p').val('');
                $('#observacion_p').val('');
                table_tipo_servicio($('#idservicio').val(),$('#tipos').val());
                setTimeout(function(){ 
                    calcularrestante();
                }, 1000);
              }
            });
        }else{
            alertfunction('Atención!','Monto mayor a 0 y menor o igual al restante');
        }
    }   
}
function table_tipo_servicio(id,tipo){
    $('.text_tabla_pago').html('');
    $.ajax({
      type: "POST",
      url: base_url+"index.php/Ventas/table_get_pagos_servicios",
      data:{id:id,tip:tipo},
      success: function(data){
        $('.text_tabla_pago_servicios').html(data);
      }
    });
}
function calcularrestantes(){
    var totalpoliza=parseFloat($('.totalpolizas').html());
    var pagos = 0;
    $(".montoagregados").each(function() {
        var vstotal = parseFloat($(this).html());
        pagos += Number(vstotal);
    });
    var restante = parseFloat(totalpoliza)-parseFloat(pagos);
    if(restante<0){
        restante=0;
    }
    $('.restante_prefactura_s').html('$ <span class="restantepoliza">'+restante.toFixed(2)+'</span>');
}
function monto_servicios(id_v,tipos){//Combinada monto de prefactura
    var result='';
    $.ajax({
      type: "POST",
      url: base_url+"index.php/Ventas/montoservicio",
      data:{
        id:id_v,
        tipo:tipos
        },
      async:false,
      success: function(data){
        result=data;
      }
    });   
    return result;
}
function deletepagoser(id){
    $.ajax({
      type: "POST",
      url: base_url+"index.php/Ventas/deletepagoser",
      data:{
        id:id
        },
      async:false,
      success: function(data){
        $('.remove_table_pagos_ser_'+id).remove();
        calcularrestantes();
      }
    }); 
}
function confirmarpagov(id,tipo){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: 'Se necesita permisos de administrador<br>'+
                 '<input type="password" placeholder="Contraseña" id="contrasena" class="name form-control" required/>',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var pass=$('#contrasena').val();
                if (pass!='') {
                    $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Sistema/solicitarpermiso",
                        data: {
                            pass:pass
                        },
                        success: function (response){
                                var respuesta = parseInt(response);
                                if (respuesta==1) {
                    //=============================================================================
                        confirmarpagov2(id,tipo);
                    //=============================================================================
                                }else{
                                    notificacion(2);
                                }
                        },
                        error: function(response){  
                             notificacion(1);
                        }
                    });

                }else{
                    notificacion(0);
                }
                
            },
            cancelar: function (){
                
            }
        }
    });
}
function confirmarpagov2(id,tipo){
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Generales/confirmarpagos",
        data: {
            id:id,
            tipo:tipo
        },
        success: function (response){
            $('#modal_pago').modal('close');
        },
        error: function(response){  
            notificacion(1);
        }
    });
}
function desvfactura(id,tipo){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: 'para desvicular las facturas Se necesita permisos de administrador<br>'+
                 '<input type="text" placeholder="Contraseña" id="contrasena" name="contrasena" class="name form-control" required/>',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var pass=$('#contrasena').val();
                var pass=$("input[name=contrasena]").val();
                if (pass!='') {
                    $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Sistema/solicitarpermiso",
                        data: {
                            pass:pass
                        },
                        success: function (response){
                                var respuesta = parseInt(response);
                                if (respuesta==1) {
                    //=============================================================================
                        desvfactura2(id,tipo);
                    //=============================================================================
                                }else{
                                    toastr["warning"]("No tiene permisos", "Advertencia");
                                }
                        },
                        error: function(response){
                            toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema"); 
                             
                        }
                    });

                }else{
                    toastr["warning"]("Ingrese una contraseña", "Advertencia");
                }
                
            },
            cancelar: function () 
            {
                
            }
        }
    });
    setTimeout(function(){
        new MaskedPassword(document.getElementById("contrasena"), '\u25CF');
    },800);
}
function desvfactura2(id,tipo){
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Generales/desvfactura",
        data: {
            id:id,
            tipo:tipo
        },
        success: function (response){
                toastr["success"]("Facturas desvinculadas");
                loadtable();
        },
        error: function(response){
            toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema"); 
             
        }
    });
}
function vericarpagosventas(){
    var tipoventa = $('#tipoventa option:selected').val();
    if(tipoventa!=4){
        console.log('vericarpagosventas');
        var DATAf  = [];
        $(".modal_pagos").each(function() {
            item = {};
            item ["idventa"] = $(this).data('idventa');
            item ["tipoventa"] = $(this).data('tipoventa');
            DATAf.push(item);
        });
        aInfo   = JSON.stringify(DATAf);
        //console.log(aInfo);
        var datos=aInfo;
        
        $.ajax({
            type:'POST',
            url: base_url+"Generales/vericarpagosventas",
            data: {
                datos:aInfo
            },
            success: function (data){
                //console.log(data);
                var array = $.parseJSON(data);
                console.log(array);
                $.each(array, function(index, item) {
                        $(".verificar_"+item.idventa+"_"+item.tipo).addClass( "yellowc" );
                    
                });
            }
        });
    }
    
}
function v_ext_copy(id,tipo){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Desea realizar un clonado de la venta seleccionada?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Ventas/v_ext",
                    data: {
                        id:id,
                        tipo:tipo
                    },
                    success: function (data){
                        console.log(data);
                        var array = $.parseJSON(data);
                        console.log(array.equipos.length);
                        if(array.equipos.length==0){
                            toastr["success"]("Venta Clonada");
                            loadtable();
                        }else{
                            var html='Verifique Existencia de los siguientes productos';
                                html+='<table class="table"><thead><tr><th>Modelo</th><th>Bodega</th></tr></thead><tbody>';
                            $.each(array.equipos, function(index, item) {
                                 html+='<tr><th>'+item.modelo+'</th><th>'+item.bodega+'</th></tr>';
                                
                            });
                                html+='</tbody></table>';
                            $.alert({
                                boxWidth: '30%',
                                useBootstrap: false,
                                title: 'Atención!',
                                content: html}
                            );
                        }
                        /*
                        var array = $.parseJSON(data);
                        console.log(array);
                        $.each(array, function(index, item) {
                                $(".verificar_"+item.idventa+"_"+item.tipo).addClass( "yellowc" );
                            
                        });
                        */
                    }
                });
        },
            cancelar: function () 
            {
                
            }
        }
    });
}
function comentarioventa(id,tipo){
    var co=$('.coment_'+id+'_'+tipo).data('comentario')==null?'':$('.coment_'+id+'_'+tipo).data('comentario');
    if(co=='null'){
        co='';
    }
    $.confirm({
        boxWidth: '40%',
        useBootstrap: false,
        icon: 'fa fa-success',
        title: 'Comentario',
        content: '<textarea id="comentarioventa" class="form-control-bmz">'+co+'</textarea>',
        type: 'green',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var comt=$('#comentarioventa').val();
                $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Generales/agregarcomentario",
                    data: {
                        id:id,
                        tipo:tipo,
                        com:comt
                    },
                    success: function (response){
                        toastr["success"]("Comentario agregado");    
                    },
                    error: function(response){
                        toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema"); 
                         
                    }
                }); 
            },
            cancelar: function (){
                
            }
        }
    });
}
function alertfunction(titlea,contenta){
    $.alert({boxWidth: '30%',useBootstrap: false,title: titlea,theme: 'bootstrap',content: contenta});  
}
function cambiar_per(id,tipo){
    var personal=$('#ejecutivoselect').html();
    $.confirm({
        boxWidth: '40%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Cambio de personal',
        content: '<div style="width: 99%;">¿Desea cambiar al personal?<br>Se necesita permisos de administrador<br><input type="text" id="pass_cambio" name="pass_cambio" class="form-control-bmz"><label>Ejecutivo</label><select id="ejecutivoselect_new" class="browser-default form-control-bmz">'+personal+'</select></div>',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var eje=$('#ejecutivoselect_new option:selected').val();
                //var pass=$('#pass_cambio').val();
                var pass=$("input[name=pass_cambio]").val();
                $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Generales/cambiopersonal",
                    data: {
                        id:id,
                        tipo:tipo,
                        eje:eje,
                        pass:pass
                    },
                    success: function (response){
                        if(parseInt(response)==1){
                            toastr["success"]("Cambio Realizado");  
                            loadtable();  
                        }else{
                            toastr["error"]("No se tiene permiso"); 
                        }
                        
                    },
                    error: function(response){
                        toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema"); 
                         
                    }
                }); 
            },
            cancelar: function (){
                
            }
        }
    });
    setTimeout(function(){
            new MaskedPassword(document.getElementById("pass_cambio"), '\u25CF');
        },1000);
    setTimeout(function(){ 
        $('#ejecutivoselect_new option[value=0]').remove();
    }, 1000);
    
}
function verificaruser(){
    var eje =$('#ejecutivoselect option:selected').val();
    if(eje == 63 || eje == 64 || eje == 1){
        $('.divfiltrarfolio').show();
    }else{
        $('#filtrarfolio').prop('checked',false);
        $('.divfiltrarfolio').hide();
    }
}
function garantia(id,combinada,cgarantia){

    if(cgarantia==1){
        var title="¿Desea especificar como garantia?";
    }else{
        var title="¿Desea quitar de garantia?";
    }
    $.confirm({
        boxWidth: '40%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Comentario',
        content: title,
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                
                $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Ventas/garantia",
                    data: {
                        id:id,
                        combinada:combinada,
                        garantia:cgarantia
                    },
                    success: function (response){
                            toastr["success"]("Cambio Realizado");  
                            loadtable();  
                        
                        
                    },
                    error: function(response){
                        toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema"); 
                         
                    }
                }); 
            },
            cancelar: function (){
                
            }
        }
    });
}
function cambiar_emp(id,tipo){
    $.confirm({
        boxWidth: '40%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Cambiar empresa',
        content: '<div style="width: 99%;">¿Desea cambiar la empresa?<br>Se necesita permisos de administrador<br><input type="text" id="pass_cambio" name="pass_cambio" name="pass_cambio" class="form-control-bmz"><label>Empresa</label><select id="empresaselect_new" class="browser-default form-control-bmz"><option value="1">Alta Productividad</option><option value="2">D-Impresión</option></select></div>',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var emp=$('#empresaselect_new').val();
                //var pass=$('input[name="pass_cambio"]').val();
                //var pass=$('#pass_cambio').val();
                var pass=$("input[name=pass_cambio]").val();
                $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Generales/cambioempresa",
                    data: {
                        id:id,
                        tipo:tipo,
                        emp:emp,
                        pass:pass
                    },
                    success: function (response){
                        if(parseInt(response)==1){
                            toastr["success"]("Cambio Realizado");  
                            loadtable();  
                        }else{
                            toastr["error"]("No se tiene permiso"); 
                        }
                        
                    },
                    error: function(response){
                        toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema"); 
                         
                    }
                }); 
            },
            cancelar: function (){
                
            }
        }
    });
    
    setTimeout(function(){
            new MaskedPassword(document.getElementById("pass_cambio"), '\u25CF');
    },1000);
}

function agregarservicioex(idv,tipovc,cli){
    add_ser_ex_idventa=idv;
    add_ser_ex_tipovc=tipovc;
    $('#modalservicioadd').modal('open');
    var idCliente = $('#idCliente').val();
    $.ajax({
        type:'POST',
        url: base_url+'Generales/servicioaddcliente',
        data: {
            cli: cli
        },
        async: false,
        statusCode:{
            404: function(data){
                    swal("Error", "404", "error");
            },
            500: function(){
                swal("Error", "500", "error"); 
            }
        },
        success:function(data){
          $('.servicioaddcliente').html(data);

        }
    });
}

function selectservent(ser,tipo){
    var ser_fecha=$('.seri_'+tipo+'_'+ser).data('fecha');
    var html = '¿Desea agregar el servicio de fecha: '+ser_fecha+' a la venta?';
    $.confirm({
        boxWidth: '40%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Agrear servicio',
        content: html,
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                
                $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Ventas/agregarservicioex",
                    data: {
                        ser:ser,
                        tipo:tipo,
                        idventa:add_ser_ex_idventa,
                        ventatipo:add_ser_ex_tipovc
                    },
                    success: function (response){
                            toastr["success"]("Servicio agregado");  
                        $('#modalservicioadd').modal('close');
                        
                    },
                    error: function(response){
                        toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema"); 
                         
                    }
                }); 
            },
            cancelar: function (){
                
            }
        }
    });
}
function cambiarstatus(){
    $.confirm({
        boxWidth: '40%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Cambiar Estatus',
        content: '<div style="width: 99%;">¿Desea cambiar el estatus de las prefacturas pendientes a completadas?<br>Se necesita permisos de administrador<br><input type="text" id="pass_cambio" name="pass_cambio" name="pass_cambio" class="form-control-bmz"></div>',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var emp=$('#empresaselect_new').val();
                //var pass=$('input[name="pass_cambio"]').val();
                //var pass=$('#pass_cambio').val();
                var pass=$("input[name=pass_cambio]").val();
                $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Generales/cambiarstatus",
                    data: {
                        pass:pass
                    },
                    success: function (response){
                        if(parseInt(response)==1){
                            toastr["success"]("Cambio Realizado");  
                            loadtable();  
                        }else{
                            toastr["error"]("No se tiene permiso"); 
                        }
                        
                    },
                    error: function(response){
                        toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema"); 
                         
                    }
                }); 
            },
            cancelar: function (){
                
            }
        }
    });
    
    setTimeout(function(){
            new MaskedPassword(document.getElementById("pass_cambio"), '\u25CF');
    },1000);
}
function msearchpre(){
    $('#modalsearchpre').modal('open');
}
function searchpre(){
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Ventas/buscarpre",
        data: {
            pre:$('#searchpre').val()
        },
        success: function (response){
            $('.table_result_search').html(response);
            $('#table_search_pre').DataTable();
        },
        error: function(response){
            toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema"); 
             
        }
    }); 
}
function searchprelimpiar(){
    $('#searchpre').val('');
    $('.table_result_search').html('');
}
function viewpreir(row){
    var fecha =$('.viewpreir_'+row).data('fecha');
    var tipopre =$('.viewpreir_'+row).data('tipopre');
    var idcliente =$('.viewpreir_'+row).data('idcliente');
    var empresa =$('.viewpreir_'+row).data('empresa');
    var idventa =$('.viewpreir_'+row).data('idventa');

    $('#fechainicial_reg').val(fecha);
    $('#tipoventa').val(tipopre).change();
    $('#fechafinal_reg').val(fecha);
    $('#cliente').html('<option value="'+idcliente+'">'+empresa+'</option>');
    $('#ejecutivoselect').val(0).change();
    searchcliente();
    loadtable();
    setTimeout(function(){ 
        table.search(idventa).draw();
    }, 1000);
}
function requiere_factura(id,tipo,reg){
    var html='<div style="width: 99%;">';
        if(reg==1){
            html+='¿Desea Marcar la prefactura, de que no requiere factura?<br>';
            var v_tipo=0;
        }else{
            html+='¿Desea Marcar la prefactura, de que si requiere factura?<br>';
            var v_tipo=1;
            html+='Se necesita permisos de administrador<br>';
            html+='<input type="text" id="pass_cambio" name="pass_cambio" name="pass_cambio" class="form-control-bmz">';
        }
        
        
        html+='</div>';
    $.confirm({
        boxWidth: '40%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Cambiar Estatus',
        content: html,
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                
                var pass=$("input[name=pass_cambio]").val()==undefined?'':$("input[name=pass_cambio]").val();
                $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Generales/cambiarstatus_req_fac",
                    data: {
                        pass:pass,
                        id:id,
                        tipo:tipo,
                        vtipo:v_tipo
                    },
                    success: function (response){
                        if(parseInt(response)==1){
                            toastr["success"]("Cambio Realizado");  
                            loadtable();  
                        }else{
                            toastr["error"]("No se tiene permiso"); 
                        }
                        
                    },
                    error: function(response){
                        toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema"); 
                         
                    }
                }); 
            },
            cancelar: function (){
                
            }
        }
    });
    if(reg==0){
        setTimeout(function(){
                new MaskedPassword(document.getElementById("pass_cambio"), '\u25CF');
        },800);//1 seg 1000 
    }
}
function fpago_s(){
    var met = $('#idmetodo_ps option:selected').val();
    var idper = $('#idper').val()
      var fecha = $('#fechaactual').val();
      if(met==1){
        if(idper==1 || idper==17 || idper==18){

        }else{
            $('#fecha_p_s').val(fecha).prop({'readonly':true});
        }
        
      }else{
        $('#fecha_p_s').prop({'readonly':false});
      }
}
function fpago_0(){
    var met = $('#idmetodo option:selected').val();
    var idper = $('#idper').val()
      var fecha = $('#fechaactual').val();
      if(met==1){
        if(idper==1 || idper==17 || idper==18){

        }else{
            $('#fecha_p').val(fecha).prop({'readonly':true});    
        }
        
      }else{
        $('#fecha_p').prop({'readonly':false});
      }
}