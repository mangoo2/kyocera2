var base_url=$('#base_url').val();
$(document).ready(function($) {
	setTimeout(function(){ 
		$('.collapsible').collapsible('open', 0);
	}, 1000);
});
'use strict'; 

const video = document.querySelector('video');
const canvas = window.canvas = document.querySelector('canvas');
//canvas.width = 480;
//canvas.height = 360;

//const button = document.querySelector('button');
const constraints = {
  audio: false,
  video: true
};

function handleSuccess(stream) {
  window.stream = stream; // make stream available to browser console
  video.srcObject = stream;
} 

function handleError(error) {
  console.log('navigator.MediaDevices.getUserMedia error: ', error.message, error.name);
}

navigator.mediaDevices.getUserMedia(constraints).then(handleSuccess).catch(handleError);

///////
function normal(){

	stream.getTracks().forEach(function(track) {
  track.stop();
});

	const constraints = {
	  audio: false,
	  video: true
	};
	navigator.mediaDevices.getUserMedia(constraints).then(handleSuccess).catch(handleError);
}
function frontal(){
	stream.getTracks().forEach(function(track) {
  track.stop();
});
	const constraints = {
	  audio: false,
	  video: { facingMode: { exact: "environment" } }
	};
	navigator.mediaDevices.getUserMedia(constraints).then(handleSuccess).catch(handleError);
}
function campurar(){
	$('.collapsible').collapsible('open', 1);
	canvas.width = video.videoWidth;
  	canvas.height = video.videoHeight;
  	canvas.getContext('2d').drawImage(video, 0, 0, canvas.width, canvas.height);
  $('.css_guardar').css('display','block');  
}
//==================================================================================================================
function guardar(){
	var dataURL = canvas.toDataURL();
	console.log(dataURL);
	$.ajax({
        type:'POST',
        url: base_url+"index.php/Configuracionrentas/guardarimg",
        data: {
        	image:dataURL,
        	equipo:$('#equipo').val(),
          idserie:$('#idserie').val(),
          comentario:$('#comentario').val(),
          tipo:$('#tipo').val()
        },
        success: function (response){
            console.log(response);
            swal({ title: "Éxito",
                            text: "Se ha registrado correctamente",
                            type: 'success',
                            showCancelButton: false,
                            allowOutsideClick: false,
                        });
            setTimeout(function() { window.location.href = base_url+"index.php/Configuracionrentas/servicios";}, 1000);
        },
        error: function(response){
            alertfunction('Atención!','Algo salió mal, intente de nuevo o contacte al administrador del sistema'); 
        }
    });
}
function alertfunction(titlea,contenta){
    $.alert({boxWidth: '30%',useBootstrap: false,title: titlea,theme: 'bootstrap',content: contenta});  
}