<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="icon" href="<?php echo base_url(); ?>app-assets/images/favicon/favicon_kyocera.png" sizes="32x32">
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<title>Prefactura</title>
		<!-- jQuery Library -->
    	<script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/jquery-3.3.1.min.js"></script>
    	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <link href="<?php echo base_url(); ?>app-assets/vendors/sweetalert/dist/sweetalert.css" type="text/css" rel="stylesheet">
        <script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/MaskFocus/MaskedPassword.js"></script>
        <!--
        <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/js/ckeditor/ckeditor.js"></script>
    	<script type="text/javascript" src="<?php echo base_url(); ?>app-assets/js/ckeditor/adapters/jquery.js"></script>-->
		<style type="text/css">
			.tdstitle{background: #c1bcbc;font-weight: bold;color: white;}
			html{ -webkit-print-color-adjust: exact;}
			input{padding: 0px !important;height: 28px !important;}
			select{padding: 0px !important;height: 28px !important;font-size: 11px !important;}
			@media print{
				input{background: transparent;border: 0 !important;}
				.buttonenvio{display: none;}
				html{-webkit-print-color-adjust: exact;}
				.tdstitle{background: #c1bcbc;font-weight: bold;color: white;}
				.buttoimprimir{display: none;}
				select{background: transparent;border: 0 !important;-moz-appearance: none;-webkit-appearance: none;appearance: none;}
				.form-control:disabled, .form-control[readonly] {background-color: transparent;opacity: 1;}
			}
			.error{color: red;font-size: 10px;}
			.form-control:disabled, .form-control[readonly] {
    			background-color: #e9ecef !important;
    			opacity: 1;
			}
			option:disabled {
				background-color: #e9ecef !important; /* Cambiar el color de fondo */
			}
			.devolucion{background-color: #ff02026b;}
		</style>
		<?php 
			if($prefacturaId>0){
				$ediciondf='onclick="editardatosfiscales()"';
			}else{
				$ediciondf='';
			}
		?>
	</head>
	<body>
		<input type="hidden" id="base_url" name="base_url" value="<?php echo base_url(); ?>">
		<form method="post" id="formprefactura">
			
			<input type="hidden" id="prefacturaId" name="prefacturaId" value="<?php echo $prefacturaId;?>">
			<input type="hidden" id="ventaId" name="ventaId" value="<?php echo $ventaId;?>">
			<table border="1" width="100%">
				<tr>
					<td>
						<img src="<?php echo base_url(); ?>app-assets/images/<?php echo $configuracionCotizacion->logo1; ?>" height="50px">
					</td>
					<td>
						<?php echo $configuracionCotizacion->textoHeader; ?>
					</td>
					<td>
						<img class="" src="<?php echo base_url(); ?>app-assets/images/1024_kyocera_logo_mds.png" alt="materialize logo"  style="max-width:200px; width: 100%;">
					</td>
				</tr>
			</table>
			<table border="1" width="100%">
				<tr>
					<td class="tdstitle" style="width: 10%;">FECHA</td>
					<td style="width: 5%;"><?php echo $dia_reg;?></td>
					<td style="width: 5%;"><?php echo $mes_reg;?></td>
					<td style="width: 5%;"><?php echo $ano_reg;?></td>
					<td class="tdstitle" style="width: 5%">AC</td>
					<td style="width: 10%;"><?php echo $ini_personal;?></td>
					<td class="tdstitle" style="width: 20%">PROINPOL No.</td>
					<td style="width: 10%;"><?php echo $proinven;?></td>
					<td class="tdstitle" style="width: 10%">ENTREGA</td>
					<td style="width: 20%"><input type="date" id="fechaentrega" value="<?php echo $fechaentrega;?>" class="form-control" onchange="actualizarfechae(<?php echo $ventaId;?>)" required data-entrega="0"data-iser="<?php echo $iser;?>" <?php if($iser==1){ echo 'onclick="notserentrega()" readonly ';} ?> ></td>
				</tr>
			</table>
			<table border="1" width="100%">
				<tr>
					<td style="width: 5%;">Cia.</td>
					<td style="width: 80%;" colspan="5"><output id="razon_social"></output></td>
					<td style="width: 5%;">RFC</td>
					<td style="width: 10%;" <?php echo $ediciondf;?> >
						<select class="form-control" id="rfc_id" name="rfc_id" <?php echo $rfc_id_button;?> onchange="selecrfcp()" required>
						    <option value="">Seleccione</option>
						<?php foreach ($rfc_datos as $item) { ?>
								<option value="<?php echo $item->id; ?>" <?php if($item->id==$rfc_id){ echo 'selected';}?> ><?php echo $item->rfc; ?></option>
						<?php } ?>
					    </select>	
					</td>
				</tr>
				<tr>
					<td style="width: 5%; font-size: 9px">VENCE</td>
					<td style="width: 12%;">
						<input type="date" id="vencimiento" name="vencimiento" class="form-control" value="<?php echo $vence;?>" <?php echo $vence_block;?> <?php echo $maxcredito; ?> style="max-width: 138px;" required></td>
					<td style="width: 10%; font-size: 11px; text-align: center;">METODO DE PAGO</td>
					<td style="width: 20%; font-size: 11px; text-align: center;" <?php echo $ediciondf;?>>
					<select class="form-control" id="metodopagoId" name="metodopagoId" <?php echo $metodopagoId_block;?> >
						<?php foreach ($metodopagorow as $item) { ?>
								<option value="<?php echo $item->id; ?>" <?php if($item->id==$metodopagoId){ echo 'selected';} ?>    ><?php echo $item->metodopago_text; ?></option>
						<?php } ?>
					</select>
					</td>
					<td style="width: 12%; font-size: 11px; text-align: center;">FORMA DE PAGO</td>
					<td style="width: 14%; font-size: 11px; text-align: center;" <?php echo $ediciondf;?>>
						<select class="form-control" id="formapagoId" name="formapagoId" <?php echo $formapago_block;?> required>
						<?php foreach ($formapagorow->result() as $item) { ?>
								<option value="<?php echo $item->id; ?>" <?php if($item->id==$formapago){ echo 'selected';} ?>    ><?php echo $item->formapago_text; ?></option>
						<?php } ?>
					</select>
					</td>
					<td style="width: 10%; font-size: 11px;">USO DE CFDI</td>
					<td style="width: 17%; font-size: 11px;" <?php echo $ediciondf;?>>
						<select class="form-control" id="usocfdiId" name="usocfdiId" <?php echo $cfdi_block;?>>
							<?php foreach ($cfdirow->result() as $item) { ?>
									<option value="<?php echo $item->id; ?>" <?php if($item->id==$cfdi){ echo 'selected';} ?>    ><?php echo $item->uso_cfdi_text; ?></option>
							<?php } ?>
						</select>
					</td>	
					
				</tr>
			</table>
			<table border="1" width="100%">
				<tr>
					<td style="font-size: 11px; width: 5%;">Calle</td>
					<td style="font-size: 11px; width: 26%;" colspan="3" >
						<input type="text" value="<?php echo $calle;?>" id="calle" name="d_calle" style="width: 100%; border: 0px;" onclick="editardireccion(<?php echo $idCliente;?>)" readonly>
					</td>
					<td style="font-size: 11px; width: 5%;">No.</td>
					<td style="font-size: 11px; width: 5%;">
						<input type="text" value="<?php echo $num_ext;?>" id="num_ext" name="d_numero" style="width: 100%; border: 0px;" onclick="editardireccion(<?php echo $idCliente;?>)" readonly>
					</td>
					<td style="font-size: 11px; width: 5%;">Col.</td>
					<td style="font-size: 11px; width: 25%; " colspan="2">
						<input type="text" value="<?php echo $colonia;?>" id="colonia" name="d_colonia" style="width: 100%; border: 0px;" onclick="editardireccion(<?php echo $idCliente;?>)"readonly>
					</td>
					<td style="font-size: 11px; width: 9%; text-align: center;">FORMA DE COBRO</td>
					<td style="font-size: 11px; width: 20%;">
						<?php if($formacobro!=''){
							echo $formacobro;
						}else{ ?> 
							<select class="form-control" name="formadecobro" id="formadecobro" required>
								<option></option>
								<option>Mostrador</option>
								<option>Transferencia</option>
								<option>Deposito directo</option>
								<option>Tecnico</option>
							</select>
						<?php } ?>
					</td>
				</tr>
				<tr>
					<td style="font-size: 11px; width: 5%;">Cd</td>
					<td style="font-size: 11px;" colspan="2">
						<input type="text" value="<?php echo $municipio;?>" id="d_ciudad" name="d_ciudad" onclick="editardireccion(<?php echo $idCliente;?>)" style="width: 100%; border: 0px;" readonly>
					</td>
					<td style="font-size: 11px; width: 5%;">Edo.</td>
					<td style="font-size: 11px; width: 5%;" colspan="4">
							<?php 
								$estadoval='';
								foreach ($estadorow->result() as $item) { 
									if($item->EstadoId==$estado){ 
									 	$estadoval = $item->Nombre;
									}
								} ?>
						
						<input type="text" value="<?php if($estadovals!=''){echo $estadovals;}else{echo $estadoval.' C.P.'.$cp;}?>" id="d_estado" name="d_estado" onclick="editardireccion(<?php echo $idCliente;?>)" style="width: 100%; border: 0px;" readonly>
					</td>
					<td style="font-size: 11px; width: 5%;">tel.</td>
					<td style="font-size: 11px;" colspan="2">
						<select class="form-control" id="telId" name="telId" <?php echo $telId_block; ?>>
						<?php foreach ($cliente_datoscontacto->result() as $item) { ?>
							<option value="<?php echo $item->datosId;?>"   <?php if($telId==$item->datosId){echo 'selected'; }?> ><?php echo $item->telefono;?></option>		
					    <?php } ?>
						</select>
					</td>
					
				</tr>
				<tr>
					<td style="font-size: 11px; width: 10%;" colspan="2">Contacto</td>
					<td style="font-size: 11px;" colspan="2">
						<select class="form-control" id="contactoId" name="contactoId" <?php echo $contactoId_block; ?> onchange="copiardatoscc()">
						<?php foreach ($cliente_datoscontacto->result() as $item) { ?>
							<option 
								value="<?php echo $item->datosId;?>" 
								<?php if($contactoId==$item->datosId){echo 'selected'; }?>
								data-dcemail="<?php echo $item->email;?>"
								data-dcpuesto="<?php echo $item->puesto;?>"

								>
								<?php echo $item->atencionpara;?>	
							</option>		
					    <?php } ?>
						</select>
					</td>
					<td style="font-size: 11px; width: 5%;">Cargo</td>
					<td style="font-size: 11px; width: 5%;" colspan="3">
						<input type="tex" id="cargo" name="cargo" class="form-control" value="<?php echo $cargo;?>" <?php echo $cargo_block;?> readonly>
					</td>
					<td style="font-size: 11px; width: 5%;">email:</td>
					<td style="font-size: 11px;" colspan="2" class="dcemail"><?php echo $email;?></td>
				</tr>
				
			</table>
			<table border="1" width="100%">
				<thead>
				<tr>
					<th style="font-size: 11px; ">Cantidad</th>
					<th style="font-size: 11px; ">Surtir</th>
					<th style="font-size: 11px; ">No. de Parte</th>
					<th style="font-size: 11px; " colspan="3">Descripcion</th>
					<th style="font-size: 11px; ">Precio Unitario</th>
					<th style="font-size: 11px; " >Total</th>
				</tr>
				</thead>
				<tbody>
					<?php 
						$totalgeneral=0;
                        // Tabla de polizas creadas detalles  		
						foreach ($polizaventadetalles->result() as $item) { 
									$totalc = 0;
									$tipoprecio=1;
									if($item->precio_local !==NULL) {
										$totalc=$item->precio_local;
									}elseif($item->precio_semi !==NULL) {
										$totalc=$item->precio_semi;
									}elseif($item->precio_foraneo !==NULL) {
										$totalc=$item->precio_foraneo;
									}elseif($item->precio_especial !==NULL) {
										$totalc=$item->precio_especial;
									}else{
										$tipoprecio=1;
									}
									$totalgeneral=$totalgeneral+($totalc*$item->cantidad);
									?>
									<tr>
										<td style="width: 7%; font-size: 11px; text-align: center;"><?php echo $item->cantidad;?></td>
										<td style="width: 9%; font-size: 11px; text-align: center;">OF</td>
										<td style="width: 9%; font-size: 11px; ">N/A</td>
										<td style="width: 56%; font-size: 11px; " colspan="3">
											<table border="1" width="100%">
												<tr>
													<td width="30%"></td>
													<td width="70%">Poliza</td>		
												</tr>
												<tr>
													<td>TIPO</td>
													<td><?php echo $item->nombre;?></td>		
												</tr>
												<tr>
													<td>MODELO</td>
													<td><?php echo $item->modeloe;?></td>		
												</tr>
												<tr>
													<td ondblclick="editarseriepol(<?php echo $item->id;?>,0)">SERIE </td>
													<td ondblclick="editarseriepol(<?php echo $item->id;?>,0)"><?php 
													$seriesall=$this->ModeloCatalogos->seriespolizas($item->id);
													if ($seriesall!='') {
														echo $seriesall;
													}else{
														echo $item->serie;	
													}

													?></td>		
												</tr>
												<!--	
												<tr>
													<td></td>
													<td><?php echo $item->modelo;?></td>		
												</tr>-->
												<tr>
													<td>Cubre</td>
													<td><?php echo $item->cubre;?></td>		
												</tr>
												<tr>
													<td>Vigencia de meses</td>
													<td><?php echo $item->vigencia_meses;?></td>		
												</tr>
												<tr>
													<td>Visitas</td>
													<td><?php echo $item->vigencia_clicks;?></td>		
												</tr>
												<?php if ($item->precio_local!=NULL) { 
													$tipoprecio=1;
												?>
													<tr>
														<td>Precio local</td>
														<td><?php echo number_format($item->precio_local,2,'.',',');?></td>		
													</tr>
												<?php	}?>
												<?php if ($item->precio_semi!=NULL) { 
													$tipoprecio=2;
												?>
													<tr>
														<td>Precio semi</td>
														<td><?php echo number_format($item->precio_semi,2,'.',',');?></td>		
													</tr>
												<?php	}?>
												<?php if ($item->precio_foraneo!=NULL) { 
													$tipoprecio=3;
												?>
													<tr>
														<td>Precio foraneo</td>
														<td><?php echo number_format($item->precio_foraneo,2,'.',',');?></td>		
													</tr>
												<?php	}?>
													<tr>
														<td>Domicilio</td>
														<td class="adddireccionespolizatd">
															<input type="text" id="direccionservicio" class="direccionservicio_<?php echo $item->id;?> direccionser form-control adddireccionespoliza" data-cliente="<?php echo $idCliente;?>" data-idrow="<?php echo $item->id;?>" value="<?php echo $item->direccionservicio;?>" style="background: transparent; font-size:12px;" <?php echo $cargo_block;?> <?php if($cargo_block=='readonly'){ echo 'ondblclick="desbloquearcomentarios()"';} ?> required>
														
														</td>		
													</tr>
													

											</table>
										</td>
										<td style="width: 10%; font-size: 11px; text-align: center;"  
											onclick="editarprecio(<?php echo $item->id;?>,<?php echo $tipoprecio;?>,<?php echo $totalc;?>)"
										>$<?php echo number_format($totalc,2,'.',',');?></td>
										<td style="width: 9%; font-size: 11px; text-align: center;" >$<?php echo number_format(($totalc*$item->cantidad),2,'.',',');?></td>
									</tr>
									<tr>
										<td>Comentario</td>
										<td class="addcomentariotd" colspan="7">
											<input type="text" id="comentario" class="form-control" data-cliente="<?php echo $idCliente;?>" data-idrow="<?php echo $item->id;?>" value="<?php echo $item->comentario;?>" style="background: transparent; font-size: 11px;" <?php echo $comentario_block; ?> <?php if($cargo_block=='readonly'){ echo 'ondblclick="desbloquearcomentarios()"';} ?>>
										</td>		
									</tr>
								<?php	
						}
						foreach ($polizaventadetalles_dev->result() as $item) { 
									$totalc = 0;
									$tipoprecio=1;
									if($item->precio_local !==NULL) {
										$totalc=$item->precio_local;
									}elseif($item->precio_semi !==NULL) {
										$totalc=$item->precio_semi;
									}elseif($item->precio_foraneo !==NULL) {
										$totalc=$item->precio_foraneo;
									}elseif($item->precio_especial !==NULL) {
										$totalc=$item->precio_especial;
									}else{
										$tipoprecio=1;
									}
									//$totalgeneral=$totalgeneral+($totalc*$item->cantidad);
									?>
									<tr class="devolucion">
										<td style="width: 7%; font-size: 11px; text-align: center;"><?php echo $item->cantidad;?></td>
										<td style="width: 9%; font-size: 11px; text-align: center;">OF</td>
										<td style="width: 9%; font-size: 11px; ">N/A</td>
										<td style="width: 56%; font-size: 11px; " colspan="3">
											<table border="1" width="100%">
												<tr>
													<td width="30%"></td>
													<td width="70%">Poliza</td>		
												</tr>
												<tr>
													<td>TIPO</td>
													<td><?php echo $item->nombre;?></td>		
												</tr>
												<tr>
													<td>MODELO</td>
													<td><?php echo $item->modeloe;?></td>		
												</tr>
												<tr>
													<td ondblclick="editarseriepol(<?php echo $item->id;?>,0)">SERIE </td>
													<td ondblclick="editarseriepol(<?php echo $item->id;?>,0)"><?php 
													$seriesall=$this->ModeloCatalogos->seriespolizas($item->id);
													if ($seriesall!='') {
														echo $seriesall;
													}else{
														echo $item->serie;	
													}

													?></td>		
												</tr>
												<!--	
												<tr>
													<td></td>
													<td><?php echo $item->modelo;?></td>		
												</tr>-->
												<tr>
													<td>Cubre</td>
													<td><?php echo $item->cubre;?></td>		
												</tr>
												<tr>
													<td>Vigencia de meses</td>
													<td><?php echo $item->vigencia_meses;?></td>		
												</tr>
												<tr>
													<td>Visitas</td>
													<td><?php echo $item->vigencia_clicks;?></td>		
												</tr>
												<?php if ($item->precio_local!=NULL) { 
													$tipoprecio=1;
												?>
													<tr>
														<td>Precio local</td>
														<td><?php echo number_format($item->precio_local,2,'.',',');?></td>		
													</tr>
												<?php	}?>
												<?php if ($item->precio_semi!=NULL) { 
													$tipoprecio=2;
												?>
													<tr>
														<td>Precio semi</td>
														<td><?php echo number_format($item->precio_semi,2,'.',',');?></td>		
													</tr>
												<?php	}?>
												<?php if ($item->precio_foraneo!=NULL) { 
													$tipoprecio=3;
												?>
													<tr>
														<td>Precio foraneo</td>
														<td><?php echo number_format($item->precio_foraneo,2,'.',',');?></td>		
													</tr>
												<?php	}?>
													<tr>
														<td>Domicilio</td>
														<td class="adddireccionespolizatd">
															<?php echo $item->direccionservicio;?>
														
														</td>		
													</tr>
													

											</table>
										</td>
										<td style="width: 10%; font-size: 11px; text-align: center;" >$<?php echo number_format($totalc,2,'.',',');?></td>
										<td style="width: 9%; font-size: 11px; text-align: center;" >$<?php echo number_format(($totalc*$item->cantidad),2,'.',',');?></td>
									</tr>
									<tr class="devolucion">
										<td>Comentario</td>
										<td class="addcomentariotd" colspan="7">
											<?php echo $item->comentario;?>
										</td>		
									</tr>
								<?php	}
					?>	
					
				</tbody>
				<tfoot>
					<tr><?php $tg_iva=$totalgeneral*0.16; ?>
						<td style="font-size: 11px; " colspan="2">Subtotal</td>
						<td style="font-size: 11px; " colspan="<?php if($siniva==1){ echo '4';}else{echo '2';}?>">$<?php echo number_format($totalgeneral,2,'.',',');?></td>
						<?php if($siniva!=1){ 
							$addiva='';
						?>
						<td style="font-size: 11px; " class="quitariva">Iva</td>
						<td style="font-size: 11px; "class="quitariva" >$<?php echo number_format($tg_iva,2,'.',',');?></td>
						<?php }else{$tg_iva=0;
							$addiva='onclick="quitariva('.$ventaId.',2,1)"';
						} ?>
						<td style="font-size: 11px; ">Total</td>
						<td style="font-size: 11px; " >$<?php echo number_format(round($tg_iva, 2)+round($totalgeneral, 2),2,'.',',');?></td>
						<?php 
							$this->ModeloCatalogos->updateCatalogo('polizasCreadas',array('subtotal'=>$totalgeneral,'iva'=>$tg_iva,'total'=>round($tg_iva, 2)+round($totalgeneral, 2)),array('id'=>$ventaId));
						?>
					</tr>
				</tfoot>
			</table>
			<label for="observaciones">Observaciones:</label>
			<textarea id="observaciones" class="form-control" <?php echo $observaciones_block;?> style="min-height: 170px;"><?php echo $observaciones;?></textarea>
		</form>
		<div class="row">
		    <div class="col-md-12">
		        <div class="col-md-12 text-right buttoresave">
		        	<?php 
		        		if($block_button==0){ ?>
		        			<button type="button" class="btn btn-success buttonenvio">Enviar</button>
		        	<?php  }else{  	?>	
		        		<button type="button" class="btn btn-success buttoimprimir">Imprimir</button>
		        	<?php  }  	?>	
		        </div>
		    </div>
		</div>
		<div class="modal fade" id="modalconfirmacionenvio" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h5 class="modal-title" id="exampleModalLabel">Enviar</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      <div class="modal-body">
		        <div class="row">
		        	<div class="col-md-12">
		        		<h4>¿Desea enviar la prefactura?</h4>
		        	</div>
		        </div>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
		        <button type="button" class="btn btn-primary buttonenvioconfirm">Enviar</button>
		      </div>
		    </div>
		  </div>
		</div>
		<div class="modal fade" id="modaldirecciones" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		  <div class="modal-dialog modal-xl" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h5 class="modal-title" id="exampleModalLabel">Direcciones</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      <div class="modal-body">
		        <div class="row">
		        	<div class="col-md-12 viewdirecciones" style="font-size: 12px;">
		        		
		        	</div>
		        </div>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
		      </div>
		    </div>
		  </div>
		</div>
		
	</body>
	<script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/sweetalert/dist/sweetalert.min.js"></script>
	<link href="<?php echo base_url(); ?>app-assets/vendors/confirm/jquery-confirm.min.css" type="text/css" rel="stylesheet">
	<script src="<?php echo base_url(); ?>app-assets/vendors/confirm/jquery-confirm.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/jquery-validation/jquery.validate.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/jquery-validation/additional-methods.min.js"></script>
    <link href="<?php echo base_url(); ?>app-assets/vendors/data-tables/css/jquery.dataTables.min.1.11.4.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/data-tables/js/jquery.dataTables.min.1.11.4.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/MaskFocus/MaskedPassword.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>public/js/prefacturasg.js?v=<?php echo date('YmdGi');?>" ></script>
	<script type="text/javascript">
		var base_url = $('#base_url').val();
		$(document).ready(function(){ 
			$('#metodopagoId').change();//solo se agrego ya que en esta vista el valor esta por defecto
			copiardatoscc();
			$('.buttonenvio').click(function(event) {
				//$('#modalconfirmacionenvio').modal();
				if($('#formprefactura').valid()){
					$('#modalconfirmacionenvio').modal();
				}else{
					swal("Advertencia", "Seleccione los campos requeridos", "error");

				}
			});
			$('.buttonenvioconfirm').click(function(event) {
				$( ".buttonenvioconfirm" ).prop( "disabled", true );
		        setTimeout(function(){ 
		             $(".buttonenvioconfirm" ).prop( "disabled", false );
		        }, 5000);
				var dirrow = [];
				var direccionesrow   = $(".adddireccionespolizatd");
				direccionesrow.each(function(){ 
					item = {};
					item['id'] = $(this).find("input[id*='direccionservicio']").data('idrow');
					item['dir'] = $(this).find("input[id*='direccionservicio']").val();
					dirrow.push(item);
				});
				aInfodir   = JSON.stringify(dirrow);
				//========================================
					var comenrow = [];
					var comentariosrow   = $(".addcomentariotd");
					comentariosrow.each(function(){ 
						item = {};
						item['id'] = $(this).find("input[id*='comentario']").data('idrow');
						var comen=$(this).find("input[id*='comentario']").val();
							comen=comen.replace(/[&'"]/g, ' ');
						item['comen'] = comen;
						comenrow.push(item);
					});
					aInfocome   = JSON.stringify(comenrow);
				//=========================================
					var obser=$('#observaciones').val();
						obser=obser.replace(/[&'"]/g, ' ');
				var datos = $('#formprefactura').serialize()+'&direcciones='+aInfodir+'&comentarios='+aInfocome+'&observaciones='+obser;
				$.ajax({
                    type:'POST',
                    url: base_url+'PolizasCreadas/save',
                    data: datos,
                    async: false,
                    statusCode:{
                            404: function(data){
                                swal("Error", "404", "error");
                            },
                            500: function(){
                                swal("Error", "500", "error"); 
                            }
                        },
                        success:function(data){
                            swal("Éxito", "Pre factura enviada. Por favor recargue la vista de listado de pólizas", "success");
                            setTimeout(function(){ window.location.href=''; }, 3000);
                         

                        }
                    });
			});
			$('.buttoimprimir').click(function(event) {
				//window.print();
				var ventaId = $('#ventaId').val();
				window.location.href = base_url+"index.php/PolizasCreadas/view/"+ventaId+"/1";
			});
			
			<?php if ($rfc_id>0) { ?>
				selectrcfv(<?php echo $rfc_id;?>);
			<?php } ?>
			$('.adddireccionespoliza').click(function(event) {
				var cliente=$(this).data('cliente');
				var idrow=$(this).data('idrow');
				if($('.direccionservicio_'+idrow).prop("readonly")==false){
					if($('.direccionservicio_'+idrow).val().length == 0){
				
						$('#modaldirecciones').modal();
						$.ajax({
							type:'POST',
			                url: base_url+'PolizasCreadas/pintardireccionesclientes',
			                data:{clienteid:cliente},
			                statusCode:{
			                        404: function(data){
			                            swal("Error", "404", "error");
			                        },
			                        500: function(){
			                            swal("Error", "500", "error"); 
			                        }
			                    },
			                    success:function(data){
			                    	$('.viewdirecciones').html(data);
			                    	$('.adddirecciones').click(function(event) {
			                    		var direccion = $(this).html();
			                    		//console.log(direccion);
			                    		$('.direccionservicio_'+idrow).val(direccion);
			                    	});
			                    	$('#table_pinta_dir').dataTable();

			                    }
			            });
		        	}
		    	}
			});

		});
		function selecrfcp() {
			var rfc_id = $('#rfc_id option:selected').val();
			selectrcfv(rfc_id);	
		}
		function selectrcfv(rfc_id){
			$.ajax({
                url: base_url+'Prefactura/selecciontrcf',
                dataType: 'json',
                data:{id:rfc_id},
                statusCode:{
                        404: function(data){
                            swal("Error", "404", "error");
                        },
                        500: function(){
                            swal("Error", "500", "error"); 
                        }
                    },
                    success:function(data){
                    	var datos=data;
                        datos.forEach(function(r) {
                        if(r.rfc=='XAXX010101000'){
                        		$('#razon_social').val(r.razon_social+' ('+r.empresa+')');
                        		$('.quitariva').attr('onclick', 'quitariva(<?php echo $ventaId ?>,2,0)');
                        }else{
                        		$('#razon_social').val(r.razon_social);
                        }
                        <?php if ($rfc_id==0) { ?>
                        	$('#num_ext').val(r.num_ext);
                        	$('#colonia').val(r.colonia);
                        	$('#calle').val(r.calle);
                    	<?php } ?>
                        });
                    }
            });
		}
		function editardireccion(cliente){
			 
		}
		function copiardatoscc(){
			var id = $('#contactoId option:selected').val();
			var email = $('#contactoId option:selected').data('dcemail');
			var puesto = $('#contactoId option:selected').data('dcpuesto');
			$('#cargo').val(puesto);
			$('.dcemail').html(email);
			$('#telId').val(id);

		}
		function editarprecio(id,tipo,precio){
			<?php if($row_vfac==0){ ?>
					editarpreciorow(id,tipo,precio);
			<?php }else{ ?>
				$.alert({boxWidth: '40%',useBootstrap: false,title: 'Advertencia!',content: 'Tiene una factura relacionada cancele antes de continuar'}); 
			<?php } ?>
		}
		function editarpreciorow(id,tipo,precio){
			//if (editar==0) {ddddd
				$.confirm({
					        boxWidth: '40%',
					        useBootstrap: false,
					        icon: 'fa fa-warning',
					        title: 'Atención!',
					        content: 'Se necesita permisos de administrador<br>'+
					                 '<input type="text" placeholder="Contraseña" id="contrasena" name="contrasena"  class="name form-control" required /><br>'+
					                 'Precio<br>'+
					                 '<input type="number" placeholder="Precio" id="newprecio" class="form-control" value="'+precio+'"  /><br>',
					        type: 'red',
					        typeAnimated: true,
					        buttons:{
					            confirmar: function (){
					                //var pass=$('#contrasena').val();
					                var pass=$("input[name=contrasena]").val();
					                precio=$('#newprecio').val();
					                if (pass!='') {
					                     $.ajax({
					                        type:'POST',
					                        url: base_url+"index.php/Sistema/solicitarpermiso/<?php echo $idCliente;?>",
					                        data: {
					                            pass:pass
					                        },
					                        success: function (response){
					                                var respuesta = parseInt(response);
					                                if (respuesta==1) {
					                //===================================================
					                    
					                    //console.log(aInfoa);
					                        $.ajax({
					                            type:'POST',
					                            url: base_url+"index.php/Generales/editarpreciosp",
					                            data: {
					                                idrow:id,
					                                newprecio:precio,
					                                tipo:tipo
					                            },
					                            success:function(response){  
					                                
					                                swal("Éxito!", "Se ha Modificado", "success");
					                                setTimeout(function(){ 
														location.reload();
													}, 1000);

					                            }
					                        });
					                    
					                //================================================
					                                }else{
					                                    $.alert({
					                                    boxWidth: '30%',
					                                    useBootstrap: false,
					                                    title: 'Advertencia!',
					                                    content: 'No tiene permisos'}); 
					                                }
					                        },
					                        error: function(response){
					                            $.alert({
					                                    boxWidth: '30%',
					                                    useBootstrap: false,
					                                    title: 'Error!',
					                                    content: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema'});   
					                             
					                        }
					                    });
					                    
					                }else{
					                    $.alert({
					                                    boxWidth: '30%',
					                                    useBootstrap: false,
					                                    title: 'Advertencia!',
					                                    content: 'Ingrese una contraseña'}); 
					                }
					            },
					            cancelar: function () 
					            {
					                
					            }
					        }
					    });
				setTimeout(function(){
                    new MaskedPassword(document.getElementById("contrasena"), '\u25CF');
                },1000);
				
			//}
		}
		function quitariva(id,tipo,sincon){
			//sincon 0 sin iva 1 con iva
			if(sincon==0){
				var label='Desea quitar el iva? ';
			}else{
				var label='Desea agregar el iva? ';
			}
			$.confirm({
		        boxWidth: '41%',
		        useBootstrap: false,
		        icon: 'fa fa-warning',
		        title: 'Quitar iva',
		        content: '<label>'+label+'<br>Se necesita permisos de administrador</label><br><input id="password" type="password" class="validate form-control" autocomplete="new-password" placeholder="Contraseña" required>',
		        type: 'red',
		        typeAnimated: true,
		        buttons:{
		            confirmar: function (){
		                var pass=$('#password').val();
		                if (pass!='') {
		                    //if (precio>0) {
		                        $.ajax({
		                            type:'POST',
		                            url: base_url+'Cotizaciones/quitariva',
		                            data: {
				                        id: id,
				                        pass: pass,
				                        tipo: tipo,
				                        sincon:sincon
		                                },
		                                async: false,
		                                statusCode:{
		                                    404: function(data){
		                                        swal("Error", "404", "error");
		                                    },
		                                    500: function(){
		                                        swal("Error", "500", "error"); 
		                                    }
		                                },
		                                success:function(data){
		                                  if (data==1) {
		                                    swal("Hecho!", "Realizado", "success");
		                                    setTimeout(function(){ window.location.href=''; }, 3000);
		                                  }else{
		                                    swal("Error", "No tiene permiso", "error"); 
		                                  }

		                                }
		                            });
		                    //}else{
		                    //    swal("Error", "El precio no debe de ser menor o igual a cero", "error"); 
		                    //}
		                }else{
		                    swal("Error", "Debe de ingresar una contraseña", "error"); 
		                }  
		            },
		            cancelar: function (){
		            }
		        }
		    });
			
		}
		function editardatosfiscales(){
			<?php if($row_vfac==0){ ?>
				var idCliente=$('#idCliente').val();
					var rfc_id = $('#rfc_id').html();
					var metodopagoId = $('#metodopagoId').html();
					var formapagoId = $('#formapagoId').html();
					var usocfdiId = $('#usocfdiId').html();
					$.confirm({
					        boxWidth: '60%',
					        useBootstrap: false,
					        icon: 'fa fa-warning',
					        title: 'Atención!',
					        content: 'Se necesita permisos de administrador<br>'+
					                 '<input type="password" placeholder="Contraseña" id="contrasena" class="name form-control" required />'+
					                 '<div class="row">\
					                 	<div class="col-md-6">\
					                 		RFC:<br>\
					                 		<select id="new_rfc_id" class="form-control" onchange="selectrcf2()">'+rfc_id+'</select>\
					                 	</div>\
					                 	<div class="col-md-6">\
					                 		Metodo de pago:<br>\
					                 		<select id="new_metodopagoId" class="form-control">'+metodopagoId+'</select>\
					                 	</div>\
					                 </div>\
					                 <div class="row">\
					                 	<div class="col-md-6">\
					                 		Forma de pago:<br>\
					                 		<select id="new_formapagoId" class="form-control">'+formapagoId+'</select>\
					                 	</div>\
					                 	<div class="col-md-6">\
					                 		Uso de CFDI:<br>\
					                 		<select id="new_usocfdiId" class="form-control">'+usocfdiId+'</select>\
					                 	</div>\
					                 </div>',
					        type: 'red',
					        typeAnimated: true,
					        buttons:{
					            confirmar: function (){
					            	var new_rfc_id=$('#new_rfc_id option:selected').val();
					            	var new_metodopagoId=$('#new_metodopagoId option:selected').val();
					            	var new_formapagoId=$('#new_formapagoId option:selected').val();
					            	var new_usocfdiId=$('#new_usocfdiId option:selected').val();
					                var pass=$('#contrasena').val();
					                precio=$('#newprecio').val();
					                if (pass!='') {
					                     $.ajax({
					                        type:'POST',
					                        url: base_url+"index.php/Sistema/solicitarpermiso/"+idCliente,
					                        data: {
					                            pass:pass
					                        },
					                        success: function (response){
					                                var respuesta = parseInt(response);
					                                if (respuesta==1) {
					                //===================================================
					                    
					                    //console.log(aInfoa);
					                        $.ajax({
					                            type:'POST',
					                            url: base_url+"index.php/Generales/editardatosfiscales",
					                            data: {
					                                prefacturaId:<?php echo $prefacturaId;?>,
					                                rfc:new_rfc_id,
					                                metodopagoId:new_metodopagoId,
													formapagoId:new_formapagoId,
													usocfdiId:new_usocfdiId,
					                            },
					                            success:function(response){  
					                                
					                                swal("Éxito!", "Se ha Modificado", "success");
					                                setTimeout(function(){ 
														location.reload();
													}, 1000);

					                            }
					                        });
					                    
					                //================================================
					                                }else{
					                                    $.alert({
					                                    boxWidth: '30%',
					                                    useBootstrap: false,
					                                    title: 'Advertencia!',
					                                    content: 'No tiene permisos'}); 
					                                }
					                        },
					                        error: function(response){
					                            $.alert({
					                                    boxWidth: '30%',
					                                    useBootstrap: false,
					                                    title: 'Error!',
					                                    content: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema'});   
					                             
					                        }
					                    });
					                    
					                }else{
					                    $.alert({
					                                    boxWidth: '30%',
					                                    useBootstrap: false,
					                                    title: 'Advertencia!',
					                                    content: 'Ingrese una contraseña'}); 
					                }
					            },
					            cancelar: function () 
					            {
					                
					            }
					        }
					    });
				setTimeout(function(){ 
					$('#contrasena').val('');
				}, 1000);
			<?php }else{ ?>
				$.alert({boxWidth: '40%',useBootstrap: false,title: 'Advertencia!',content: 'Tiene una factura relacionada cancele antes de continuar'}); 
			<?php } ?>
		}
		function selectrcf2(){
			var rfc_id = $('#new_rfc_id option:selected').val();
			selectrcfv(rfc_id);
		}
		function actualizarfechae(idventa){
			var fechaentrega = $('#fechaentrega').val();
			var fecha=fechaentrega;
			var fecha=fecha.split("-");
			//if(fecha[0]>2021){

				$.confirm({
			        boxWidth: '40%',
			        useBootstrap: false,
			        icon: 'fa fa-warning',
			        title: 'Atención!',
			        content: '¿Confirma la edición de la fecha de entrega a <b>'+fechaentrega+'</b>?',
			        type: 'red',
			        typeAnimated: true,
			        buttons:{
			            confirmar: function (){
			                //===================================================
			                    
			                    //console.log(aInfoa);
			                        $.ajax({
			                            type:'POST',
			                            url: base_url+"index.php/Generales/editarfechaentrega",
			                            data: {
			                                ventaid:idventa,
			                                fentrega:fechaentrega,
			                                tipo:5
			                            },
			                            success:function(response){  
			                                
			                                swal("Éxito!", "Se ha Modificado", "success");
			                                setTimeout(function(){ 
												location.reload();
											}, 1000);

			                            }
			                        });
			                    
			                //================================================
			                 
			            },
			            cancelar: function () 
			            {
			                
			            }
			        }
			    });
			//}
		}
		function desbloquearcomentarios(){
			$.confirm({
		        boxWidth: '50%',
		        useBootstrap: false,
		        icon: 'fa fa-warning',
		        title: 'Atención!',
		        content: '¿Desea editar las direcciones?',
		        type: 'red',
		        typeAnimated: true,
		        buttons:{
		            confirmar: function (){
		                //===================================================
		                    $('.adddireccionespoliza').prop('disabled', false).prop('readonly', false);
		                    $('#comentario').prop('disabled', false).prop('readonly', false);
		                    $('.buttoresave').html('<button type="button" class="btn btn-success " onclick="buttoresave()">Actualizar</button>');
		                    
		                //================================================
		                 
		            },
		            cancelar: function () 
		            {
		                
		            }
		        }
		    });
		}
		function buttoresave(){
		    $('#modalconfirmacionenvio').modal();
		}
	</script>
</html>
