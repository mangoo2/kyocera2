<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Ventas extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloSerie');
        $this->load->model('ModeloGeneral');//insert generales
        $this->load->model('Cotizaciones_model');
        $this->load->model('Equipos_model');
        $this->load->model('Polizas_model');
        $this->load->model('Clientes_model');
        $this->load->model('Prospectos_model');
        $this->load->model('Consumibles_model');
        $this->load->model('Refacciones_model');
        $this->load->model('ConfiguracionDocumentos_Model');
        $this->load->model('Ventas_model');
        $this->load->model('Rentas_model');
        $this->load->model('Login_model');
        $this->load->model('Personal/ModeloPersonal');
        date_default_timezone_set('America/Mexico_City');
        $this->fechaactual=date('Y-m-d H:i:s');
        $this->submenu=28;
        if($this->session->userdata('logeado')==true){
            $this->perfilid =$this->session->userdata('perfilid');
            $this->idpersonal = $this->session->userdata('idpersonal');
            /*
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,28);// 20 es el id del submenu
            if ($permiso==0) {
                redirect('Login');
            }*/
            
        }else{
            redirect('login');
        }
    }

    public function index(){
        $data['MenusubId']=$this->submenu;
        if($this->perfilid==11){
                redirect('Ventasd');
            }
        /*
        $where = array('personalId' =>$this->idpersonal);   
        $result=$this->ModeloCatalogos->db10_getselectwheren('personal',$where); 
        foreach ($result->result() as $item) { 
           $data['nombre']=$item->nombre; 
           $data['apellido_paterno']=$item->apellido_paterno; 
           $data['apellido_materno']=$item->apellido_materno; 
        } 
        */
        //$where_m = array('activo' =>1);   
        //$data['get_f_formapago']=$this->ModeloCatalogos->db10_getselectwheren('f_formapago',$where_m); 
        $data['fechainicial_reg'] = date("Y-m-d",strtotime($this->fechaactual."- 1 month")); 
        $data['fechafinal_reg']='';
        $data['perfilid']=$this->perfilid;
        //$data['personalrows']=$this->ModeloCatalogos->getselectwheren('personal',array('estatus'=>1));
        $data['personalrows']=$this->ModeloGeneral->listadopersonal();
        $data['idpersonal']=$this->idpersonal;
        $data['lispagosvp'] =$this->ModeloGeneral->verificarpagosventas();
        $data['lispagosvcp']=$this->ModeloGeneral->verificarpagosventascombinadas();
        $lispagospol=$this->ModeloGeneral->verificarpagosventascombinadascomp();//
        foreach ($lispagospol->result() as $itemp) {
            $this->ModeloCatalogos->updateCatalogo('polizasCreadas',array('estatus'=>3),array('id'=>$itemp->id));
        }
        
        $pfectivo=0;
        $r_p_e=$this->ModeloCatalogos->getselectwheren('personal',array('p_efectivo'=>1,'personalId'=>$this->idpersonal));
        if($r_p_e->num_rows()>0){
            $pfectivo=1;
        }
        $data['pfectivo']=$pfectivo;

        $this->load->view('header');
        $this->load->view('main');
        $this->load->view('ventas/listado',$data);
        
        $this->load->view('footer'); 
        $this->load->view('ventas/listado_js');
        $this->load->view('info_m_servisios'); //se comenta por conflicto con la funcion detalle()
    }
    //========================================
    /*
    function getListaVentasIncompletas(){
        $data = $this->input->post();
        $ventas = $this->Ventas_model->getListaVentasIncompletas($data);
        $json_data = array("data" => $ventas);
        echo json_encode($json_data);
    }
    */
    //===========================================
    public function getListaVentasIncompletasasi() {
        $params = $this->input->post();
        //$params['idpersonal']=$this->idpersonal;
        $params['perfilid']=$this->perfilid;
        $tipoventa=$params['tipoventa'];
        if($tipoventa==1){
            $productos = $this->Ventas_model->getlistadosoloventas($params);
            $totalRecords=$this->Ventas_model->getlistadosoloventast($params); 
        }else if($tipoventa==2){
            $productos = $this->Ventas_model->getlistadosoloventasc($params);
            $totalRecords=$this->Ventas_model->getlistadosoloventasct($params); 
        }else if($tipoventa==5){
            $productos = $this->Ventas_model->getlistadosoloventasgarantias($params);
            $totalRecords=$this->Ventas_model->getlistadosoloventasgarantiast($params); 
        }else{
            $productos = $this->Ventas_model->getListaVentasIncompletasasi($params);
            $totalRecords=$this->Ventas_model->getListaVentasIncompletasasit($params); 
        }   
        
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totalRecords),  
            "recordsFiltered" => intval($totalRecords),
            "data"            => $productos->result(),
            "query"           =>$this->db->last_query()   
        );
        
        echo json_encode($json_data);
    }
    function getlistaservicios(){
        $params = $this->input->post();
        $productos = $this->Ventas_model->getlistaservicios($params);
        $totalRecords=$this->Ventas_model->getlistaserviciost($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totalRecords),  
            "recordsFiltered" => intval($totalRecords),
            "data"            => $productos->result(),
            "query"           =>$this->db->last_query()   
        );
        
        echo json_encode($json_data);
    }
    // Vista para crear Cotización
    function add($idCliente)    {
        $data['MenusubId']=$this->submenu;
            $this->ModeloCatalogos->updateCatalogo('consumibles_bodegas',array('total'=>0),array('total <'=>0));
            $this->ModeloCatalogos->updateCatalogo('series_accesorios',array('cantidad'=>0),array('cantidad <'=>0));
            $data['datoscontacto']=$this->ModeloCatalogos->db10_getselectwheren('cliente_datoscontacto',array('clienteId'=>$idCliente,'activo'=>1));
            $data['idCliente']          = $idCliente;
            $data['equipos']            = $this->Equipos_model->getEquiposParaSelect();  
            $data['polizas']            = $this->Polizas_model->getListadoPolizas();      
            //$data['familia']            = $this->Clientes_model->getDataFamilia();
            $data['equiposimg']         = $this->Clientes_model->getDataEquipo2();
            $datosCliente               = $this->Clientes_model->getClientePorId($idCliente);
            $data['nombreCliente']      = $datosCliente->empresa;
            $data['horario_disponible'] = $datosCliente->horario_disponible;
            $data['equipo_acceso']      = $datosCliente->equipo_acceso;
            $data['documentacion_acceso'] = $datosCliente->documentacion_acceso;
            $data['condicion']          = $datosCliente->condicion;
            $data['emailCliente']       = '';
            $data['atencionCliente']    = $datosCliente->puesto_contacto;
            $data['atencionpara']       = '';
            $data['aaa']                = $datosCliente->aaa;
            $data['bloqueo']            = $datosCliente->bloqueo;
            $data['fm_confirmado']      = $datosCliente->fm_confirmado;
            $data['blq_perm_ven_ser']   = $datosCliente->bloqueo_perm_ven;
            $data['sol_orden_com']      = $datosCliente->sol_orden_com;
            $where  = array('idCliente' => $idCliente,'status'=>1);
            $datosClientet = $this->ModeloCatalogos->db10_getselectwheren('cliente_has_telefono',$where);
            $telefono='';
            foreach ($datosClientet->result() as $item) {
                $telefono=$item->tel_local;
            }
            $data['telefonoCliente'] = $telefono;
            //$data['restbodegas']=$this->ModeloCatalogos->db10_getselectwheren('bodegas',array('activo' => 1));
            $data['restbodegas']=$this->ModeloCatalogos->view_session_bodegas();
            $data['perfilid']=$this->perfilid;
            $data['idpersonal']=$this->idpersonal;
            $data['resultpolizas']=$this->ModeloCatalogos->db10_getselectwheren('polizas',array('estatus'=>1));
            $this->load->view('header');
            $this->load->view('main',$data);
            $this->load->view('ventas/form_ventas',$data);
            $this->load->view('ventas/form_ventas_js');
            $this->load->view('footerck');  
            $this->load->view('info_m_servisios');       
    }
    // Función para registrar una nueva ventas

    function nuevaventa(){
        $data = $this->input->post();
        
        // Saber que se va a cotizar
        $eleccionCotizar = $data['eleccionCotizar'];
        unset($data['eleccionCotizar']);
        
        if(isset($data['accesorios'])){
            $accesorios = $data['accesorios'];
            unset($data['accesorios']);
        }

        if(isset($data['consumibles'])){
            $consumibles = $data['consumibles'];
            unset($data['consumibles']);
        }

        // Según sea el tipo de Cotización que se va a elegir, se procesará de diferente manera
        switch ($eleccionCotizar){   
            // Equipos
            case 1:
                
            break;
            // Consumibles
            case 2:
                
            break;
            // Refacciones
            case 3:
                $equiposSeleccionados = $data['arrayEquipos'];
                unset($data['arrayEquipos']);

                $arrayRefaccion = $data['arrayRefaccion'];
                unset($data['arrayRefaccion']);

                $piezasSeleccionadas = $data['arrayPiezas'];
                unset($data['arrayPiezas']);


                $equiposArreglo = explode(',', $equiposSeleccionados);
                $refaccionesArreglo = explode(',', $arrayRefaccion);
                $piezasArreglo = explode(',', $piezasSeleccionadas);
                
                $datosCotizacion = array('atencion' => $data['atencion'],
                                            'correo' => $data['correo'],
                                            'telefono' => $data['telefono'],
                                            'idCliente' => $data['idCliente'],
                                            'tipo' => $eleccionCotizar,
                                            //'rentaVentaPoliza' => 1,
                                            'estatus' => 1,
                                            'reg'=>$this->fechaactual
                                            );
                
                $idCotizacion = $this->ModeloGeneral->getinsert($datosCotizacion,'ventas');
                //$idCotizacion = $this->Cotizaciones_model->insertarCotizacion($datosCotizacion);
                /*
                for ($i=0; $i < count($refaccionesArreglo); $i++){

                    $datosRefaccion = $this->Refacciones_model->getRefaccionConPrecios($refaccionesArreglo[$i]);
                    
                    $detallesCotizacion = array(
                                            'idCotizacion' => $idCotizacion,
                                            'idEquipo' => $equiposArreglo[$i],
                                            'piezas' => $piezasArreglo[$i], 
                                            'modelo' => $datosRefaccion[0]->nombre,
                                            'descripcion' => $datosRefaccion[0]->observaciones,
                                            'precioGeneral' => $datosRefaccion[0]->general,
                                            'totalGeneral' => ($piezasArreglo[$i]*$datosRefaccion[0]->general),
                                            'idRefaccion' => $refaccionesArreglo[$i]
                                        );
                    $this->ModeloGeneral->getinsert($detallesCotizacion,'cotizaciones_has_detallesRefacciones');                   
                    //$this->Cotizaciones_model->insertarDetallesCotizacionRefacciones($detallesCotizacion);
                }*/
                echo $idCotizacion;
            break;
            // Pólizas
            case 4:
                
            break;
            
            default:
                
            break;
        }
    }
    function detallesventas(){
        $id = $this->input->post('id');
        $datos = $this->Ventas_model->detallesventas($id);
        $datosconsu = $this->Ventas_model->detallesventasconsumibles($id);
        $datosaccesorio = $this->Ventas_model->detallesaccesoriosv($id);
        $html='
            <table class="responsive-table display tabledetallesventas" cellspacing="0">
              <thead>
                <tr>
                  <th>Producto</th>
                  <th>Num. Serie</th>
                </tr>
              </thead>
              <tbody>';
                   foreach ($datos as $item) {
                    $html.='<tr>';
                    $html.='<td>'.$item->modelo.'</td>';
                    $html.='<td>'.$item->serie.'</td>';
                    $html.='</tr>';
                   }
              $html.='</tbody>
                   </table>';
        
        if($datosconsu->num_rows()>=1){
            $html.='
            <table class="responsive-table display tabledetallesventas" cellspacing="0">
              <thead>
                <tr>
                  <th>Consumible</th>
                  <th>Toner</th>
                </tr>
              </thead>
              <tbody>';
                   foreach ($datosconsu->result() as $item) {
                    $htmlc='';
                    $html.='<tr>';
                    $html.='<td>'.$item->modelo.'</td>';
                        if ($item->tblack==1) {
                            $htmlc='Toner Black:'.$item->costo_toner_black;
                        }else{
                            $cmy=$item->costo_toner_cmy/3;
                            $cmy =round($cmy, 2);
                            if ($item->tc=1) {
                               $htmlc.='<p>Toner C:'.$cmy.'</p>';
                            }
                            if ($item->tm=1) {
                                $htmlc.='<p>Toner M:'.$cmy.'</p>';
                            }
                            if ($item->ty=1) {
                               $htmlc.='<p>Toner Y:'.$cmy.'</p>';
                            }
                        }



                    $html.='<td>'.$htmlc.'</td>';
                    $html.='</tr>';
                   }
              $html.='</tbody>
                   </table>';
        }
        if($datosaccesorio->num_rows()>=1){
            $html.='
            <table class="responsive-table display tabledetallesventas" cellspacing="0">
              <thead>
                <tr>
                  <th>Accesoio</th>
                  <th>Serie</th>
                </tr>
              </thead>
              <tbody>';
                   foreach ($datosaccesorio->result() as $item) {
                    $html.='<tr>';
                    $html.='<td>'.$item->nombre.'</td>';
                    $html.='<td>'.$item->serie.'</td>';
                    $html.='</tr>';
                   }
              $html.='</tbody>
                   </table>';
        }
        echo $html;
    }
    function finalizar_factura(){
        $id = $this->input->post('id');
        $data = array('estatus' => 3);
        $resul = $this->Ventas_model->updateCatalogo($data,'id',$id,'ventas');
        echo $resul;
    }
    function finalizar_facturac(){
        $id = $this->input->post('id');
        $data = array('estatus' => 3);
        $resul = $this->Ventas_model->updateCatalogo($data,'combinadaId',$id,'ventacombinada');
        echo $resul;
    }
    function nuevaCotizacionpoliza(){
        $data = $this->input->post();
        $datosCotizacion = array('atencion' => $data['atencion'],
                                            'correo' => $data['correo'],
                                            'telefono' => $data['telefono'],
                                            'idCliente' => $data['idCliente'],
                                            'tipo' => 4,
                                            //'rentaVentaPoliza' => 3,
                                            'id_personal' => $this->idpersonal,
                                            'estatus' => 1,
                                            'combinada' => $data['combinada'],
                                            'tipov' => $data['tipov']
                                            );

        $idCotizacion = $this->ModeloCatalogos->Insert('polizasCreadas',$datosCotizacion);
        $arrayPolizas = $data['arrayPolizas'];
        $dataeqarrayinsert=array();
        unset($data['arrayPolizas']);
        $DATAc = json_decode($arrayPolizas);
        for ($i=0;$i<count($DATAc);$i++) {
            $datosPoliza = $this->Polizas_model->getPolizaConUnDetalle($DATAc[$i]->poliza,$DATAc[$i]->polizad);
            $precio_local=null;
            $precio_semi=null;
            $precio_foraneo=null;
            $precio_especial=null;
            //log_message('error', 'costo poliza: '.$DATAc[$i]->costo);
            if ($DATAc[$i]->costo=='local') {
                        $precio_local=$datosPoliza[0]->precio_local;
                        $precio_semi=null;
                        $precio_foraneo=null;
                        $precio_especial=null;
            }elseif ($DATAc[$i]->costo=='foraneo') {
            //}elseif ($polizasArregloDetallescosto[$i]=='foraneo') {
                        $precio_local=null;
                        $precio_semi=null;
                        $precio_especial=null;
                        $precio_foraneo=$datosPoliza[0]->precio_foraneo;
            }elseif($DATAc[$i]->costo=='semi'){
                        $precio_local=null;
                        $precio_semi=$datosPoliza[0]->precio_semi;
                        $precio_foraneo=null;
                        $precio_especial=null;
            }elseif($DATAc[$i]->costo=='especial'){
                        $precio_local=null;
                        $precio_semi=null;
                        $precio_foraneo=null;
                        $precio_especial=$datosPoliza[0]->precio_especial;
            }

            $detallesCotizacion = array('idPoliza' => $idCotizacion,
                                            'idEquipo' => $DATAc[$i]->equipo,
                                            'serie'=> $DATAc[$i]->serie,
                                            'idCotizacion' => 0,
                                            'nombre' => $datosPoliza[0]->nombre, 
                                            'cubre' => $datosPoliza[0]->cubre,
                                            'vigencia_meses' => $datosPoliza[0]->vigencia_meses,
                                            'vigencia_clicks' => $datosPoliza[0]->vigencia_clicks,
                                            'modelo' => $datosPoliza[0]->modelo,
                                            'precio_local' => $precio_local,
                                            'precio_semi' => $precio_semi,
                                            'precio_foraneo' => $precio_foraneo,
                                            'precio_especial' => $precio_especial,
                                            'comentario' => $DATAc[$i]->motivo,

                                        );
            $dataeqarrayinsert[]=$detallesCotizacion;
            //$this->Cotizaciones_model->insertarDetallesCotizacionPoliza($detallesCotizacion);
            //$this->ModeloCatalogos->Insert('polizasCreadas_has_detallesPoliza',$detallesCotizacion);
        }
        if(count($DATAc)>0){
            $this->ModeloCatalogos->insert_batch('polizasCreadas_has_detallesPoliza',$dataeqarrayinsert);
        }
        echo $idCotizacion;
    }
    function editarpoliza(){
        $idpoliza = $this->input->post('idpoliza');
        $polizacol = $this->input->post('polizacol');
        $pass = $this->input->post('pass');
        $valor = $this->input->post('valor');
        log_message('error',"editarpoliza pass: '$pass' ");
        $permiso=0;
        $permisos=$this->Login_model->permisoadmin();
        foreach ($permisos as $item) {
            $permisotemp=0;

            
            $verificar = password_verify($pass,$item->contrasena);

            if ($verificar==false) {
                $verificar = password_verify($pass,$item->clavestemp);
                if ($verificar) {
                    $permisotemp=1;
                    $this->ModeloCatalogos->updateCatalogo('usuarios',array('clavestemp'=>null,'clavestemp_text'=>null),array('UsuarioID'=>$item->UsuarioID));
                }
            }

            if ($verificar) {
                $permiso=1;
            }
        }
        if ($permiso==1) {
            $data[$polizacol]=$valor;
            $where = array('id' => $idpoliza );
            $this->ModeloCatalogos->updateCatalogo('polizasCreadas_has_detallesPoliza',$data,$where);
        }

        echo $permiso;
    }
    function viewventa(){
        $idventa = $this->input->post('idventa');
        $where_vde=array('idVenta'=>$idventa);
        $result_vde=$this->ModeloCatalogos->db10_getselectwheren('ventas_has_detallesEquipos',$where_vde);
        $result_vdacc=$this->ModeloCatalogos->ventasaccesorios($idventa);
        $result_vdcon=$this->ModeloCatalogos->ventasconsumibles($idventa);
        //$resultbodegas=$this->ModeloCatalogos->db10_getselectwheren('bodegas',array('activo' => 1));
        $resultbodegas=$this->ModeloCatalogos->view_session_bodegas();
        $html='';
        if($result_vde->num_rows()>0){
        $html.='<table class="highlight" id="venta_equipos">
                      <thead>
                        <tr><th>Cantidad</th><th>Equipo</th><th>Bodega</th>
                        </tr>
                      </thead>
                      <tbody> ';
        }
        foreach ($result_vde->result() as $item) {
            $stockequipo=$this->ModeloGeneral->stockequipo($item->idEquipo);
            $html.='<tr>
                          <td>
                            <input type="hidden" name="id" id="id" value="'.$item->id.'">
                            <input type="number" name="cantidad" id="cantidad" value="'.$item->cantidad.'" min="1" class="validstockequedit_'.$item->id.'"oninput="validstockequedit($(this),'.$stockequipo.')">
                          </td>
                          <td>
                            '.$item->modelo.'
                          </td>
                          <td>
                            <select id="serie_bodega" name="serie_bodega" class="browser-default serie_bodega chosen-select form-control-bmz" >';
                              foreach ($resultbodegas as $itemb) {
                                  $html.='<option value="'.$itemb->bodegaId.'" class="tipo_v_selected_'.$itemb->tipov.'">'.$itemb->bodega.'</option>';
                               }  
                            $html.='</select>  
                            
                          </td>
                        </tr>';

        }
        if($result_vde->num_rows()>0){
            $html.='</tbody></table>';
        }
        if($result_vdacc->num_rows()>0){
        $html.='<table class="highlight" id="venta_accesorios">
                      <thead>
                        <tr><th>Cantidad</th><th>Accesorio</th><th>Bodega</th></tr>
                      </thead>
                      <tbody>';
        }
        foreach ($result_vdacc->result() as $item) {
                $html.='<tr>
                          <td>
                            <input type="hidden" name="id_accesorio" id="id_accesorio" value="'.$item->id_accesorio.'">
                            <input type="number" name="cantidad" id="cantidad" value="'.$item->cantidad.'" min="1">
                          </td>
                          <td>
                            '.$item->nombre.'
                          </td>
                          <td>
                            <select id="serie_bodega" name="serie_bodega" class="browser-default serie_bodega chosen-select form-control-bmz" >';
                              foreach ($resultbodegas as $itemb) {
                                  $html.='<option value="'.$itemb->bodegaId.'" class="tipo_v_selected_'.$itemb->tipov.'">'.$itemb->bodega.'</option>';
                               }  
                            $html.='</select> 
                          </td>
                        </tr>';
        }
        if($result_vdacc->num_rows()>0){
         $html.='</tbody></table>';
        }
        if($result_vdcon->num_rows()>0){
        $html.='<table class="highlight" id="venta_consumibles">
                      <thead>
                        <tr><th>Cantidad</th><th>Consumible</th><th>Bodega</th></tr>
                      </thead>
                      <tbody>';
        }
        foreach ($result_vdcon->result() as $item) {
            

            $html.='<tr>
                          <td>
                            <input type="hidden" name="id_accesorio" id="id_accesorio" value="'.$item->id.'">
                            <input type="hidden" name="id_consu" id="id_consu" value="'.$item->id_consumibles.'">
                            <input type="number" name="cantidad" id="cantidad" value="'.$item->cantidad.'" min="1">
                          </td>
                          <td>
                            '.$item->modelo.'
                          </td>
                          <td>
                            <select id="serie_bodega" name="serie_bodega" class="browser-default serie_bodega chosen-select form-control-bmz" >';
                              foreach ($resultbodegas as $itemb) {
                                  $html.='<option value="'.$itemb->bodegaId.'" class="tipo_v_selected_'.$itemb->tipov.'">'.$itemb->bodega.'</option>';
                               }  
                            $html.='</select> 
                          </td>
                          

                        </tr>';
        }
        if($result_vdcon->num_rows()>0){
        $html.='</tbody></table>';
        }
        /*
        $html.='<div class="row">
                    <div class="col s12 m12 12">
                        <input type="hidden" id="ventaservicioc">
                        <div class="switch">
                            <label style="font-size: 20px;">
                                ¿Desea incluir instalación y/o servicio o póliza de cortesia?
                                <input type="checkbox" id="aceptareservicio" value="'.$idventa.'" onchange="aceptareservicio()">
                                <span class="lever"></span>
                            </label>
                        </div>
                    </div>                
                </div>';
        */
        echo $html;
    }
    function editarventacs(){
        $data = $this->input->post();
        $arrayequipos = $data['arrayequipos'];
        $arrayaccesorios = $data['arrayaccesorios'];
        $arrayconsumibles = $data['arrayconsumibles'];
        $servicio = $data['servicio'];

        $polizae = $data['polizae'];
        $servicioe = $data['servicioe'];
        $direccione = $data['direccione'];
        $tipoe = $data['tipoe'];
        $motivoe = $data['motivoe'];
        $fechae = $data['fechae'];
        $sidventa = $data['sidventa'];

        $DATAe = json_decode($arrayequipos);
        for ($i=0;$i<count($DATAe);$i++) {
            $datae['cantidad']=$DATAe[$i]->cantidad;
            $datae['serie_bodega']=$DATAe[$i]->serie;
            $this->ModeloCatalogos->updateCatalogo('ventas_has_detallesEquipos',$datae,array('id'=>$DATAe[$i]->id));
        }
        $DATAa = json_decode($arrayaccesorios);
        for ($i=0;$i<count($DATAa);$i++) {
            $dataa['cantidad']=$DATAa[$i]->cantidad;
            $dataa['serie_bodega']=$DATAa[$i]->serie;
            $this->ModeloCatalogos->updateCatalogo('ventas_has_detallesEquipos_accesorios',$dataa,array('id_accesorio'=>$DATAa[$i]->id_accesorio));
        }
        $DATAc = json_decode($arrayconsumibles);
        for ($i=0;$i<count($DATAc);$i++) {
            $cantidad=$DATAc[$i]->cantidad;
            $datacc['cantidad']=$cantidad;
            $bodega=$DATAc[$i]->bodega;
            $datacc['bodega']=$bodega;
            $id_consu=$DATAc[$i]->id_consu;
            $this->ModeloCatalogos->updateCatalogo('ventas_has_detallesequipos_consumible',$datacc,array('id'=>$DATAc[$i]->id));
            $this->ModeloCatalogos->updatestock2('consumibles_bodegas','total','-',$cantidad,'consumiblesId',$id_consu,'bodegaId',$bodega);
        }
        if ($servicio>0) {
            $idventa=intval($servicio);
            $this->ModeloCatalogos->updateCatalogo('ventas',array('servicio'=>1,'servicio_poliza'=>$polizae,'servicio_servicio'=>$servicioe,'servicio_direccion'=>$direccione,'servicio_tipo'=>$tipoe,'servicio_motivo'=>$motivoe),array('id'=>$idventa));
            $this->ModeloCatalogos->updateCatalogo('ventacombinada',array('servicio'=>1),array('equipo'=>$idventa));
            $this->ModeloCatalogos->updateCatalogo('ventacombinada',array('servicio'=>1),array('consumibles'=>$idventa));
            $this->ModeloCatalogos->updateCatalogo('ventacombinada',array('servicio'=>1),array('refacciones'=>$idventa));
            $this->ModeloCatalogos->updateCatalogo('ventacombinada',array('servicio'=>1),array('poliza'=>$idventa));
        }else{
            $idventa=intval($sidventa);
            $this->ModeloCatalogos->updateCatalogo('ventas',array('fechaentrega'=>$fechae),array('id'=>$idventa));
            $this->ModeloCatalogos->updateCatalogo('ventacombinada',array('fechaentrega'=>$fechae),array('equipo'=>$idventa,'fechaentrega'=>NULL));
            $this->ModeloCatalogos->updateCatalogo('ventacombinada',array('fechaentrega'=>$fechae),array('consumibles'=>$idventa,'fechaentrega'=>NULL));
            $this->ModeloCatalogos->updateCatalogo('ventacombinada',array('fechaentrega'=>$fechae),array('refacciones'=>$idventa,'fechaentrega'=>NULL));
            $this->ModeloCatalogos->updateCatalogo('ventacombinada',array('fechaentrega'=>$fechae),array('poliza'=>$idventa,'fechaentrega'=>NULL));
        }
    }
    function editarventacsrenta(){
        $data = $this->input->post();
        $arrayequipos = $data['arrayequipos'];
        $arrayaccesorios = $data['arrayaccesorios'];
        $arrayconsumibles = $data['arrayconsumibles'];
        $DATAe = json_decode($arrayequipos);
        for ($i=0;$i<count($DATAe);$i++) {
            $datae['cantidad']=$DATAe[$i]->cantidad;
            $datae['serie_bodega']=$DATAe[$i]->serie;
            $this->ModeloCatalogos->updateCatalogo('rentas_has_detallesEquipos',$datae,array('id'=>$DATAe[$i]->id));
           
            $data_rentas = $this->ModeloCatalogos->getselectwherenall('rentas_has_detallesEquipos',array('id'=>$DATAe[$i]->id));
            foreach ($data_rentas as $item) {
                $this->ModeloCatalogos->updateCatalogo('rentas_has_detallesEquipos_accesorios',$datae,array('idrentas'=> $item->idRenta,'id_equipo'=>$item->idEquipo));
            }
        }
        $DATAa = json_decode($arrayaccesorios);
        for ($i=0;$i<count($DATAa);$i++) {
            $dataa['cantidad']=$DATAa[$i]->cantidad;
            $dataa['serie_bodega']=$DATAa[$i]->serie;
            $this->ModeloCatalogos->updateCatalogo('rentas_has_detallesEquipos_accesorios',$dataa,array('id_accesorio'=>$DATAa[$i]->id_accesorio));
        }
        $DATAc = json_decode($arrayconsumibles);
        for ($i=0;$i<count($DATAc);$i++) {
            $datacc['cantidad']=$DATAc[$i]->cantidad;
            $bodega=$DATAc[$i]->bodega;
            $datacc['bodega']=$bodega;
            $this->ModeloCatalogos->updateCatalogo('rentas_has_detallesequipos_consumible',$datacc,array('id'=>$DATAc[$i]->id));
        }

    }
    /*
    function nuevaventaequipos(){
        $data = $this->input->post();
        $eleccionCotizar = 1;
        $tpeq=$data['tipoprecioequipo'];
        unset($data['tipoprecioequipo']);
        unset($data['eleccionCotizar']);
        if(isset($data['accesorios'])){
            $accesorios = $data['accesorios'];
            unset($data['accesorios']);
        }
        
        if(isset($data['consumibles'])){
            $consumibles = $data['consumibles'];
            unset($data['consumibles']);
        }
        //===============================================
                $tipoCotizacion = $data["tipo"];
                unset($data['tipo']);
                //$data['rentaVentaPoliza'] = $tipoCotizacion;
                // Saber si es cotización MANUAL o por FAMILIA
                $tipoCotizacionEquipos = $data["cotizar"];
                unset($data['cotizar']);
                // Agisnamos lo que se va a cotizar
                $data['tipo'] = $eleccionCotizar;
                // Se inserta el registro principal
                if ($tipoCotizacion==1) {
                    $datosCotizacion = array('atencion' => $data['atencion'],
                                        'correo' => $data['correo'],
                                        'telefono' => $data['telefono'],
                                        'idCliente' => $data['idCliente'],
                                        'tipo' => $data['tipo'],
                                        //'rentaVentaPoliza' => $tipoCotizacion,
                                        'id_personal'=>$this->idpersonal,
                                        'estatus' => 1,
                                        'reg'=>$this->fechaactual,
                                        'combinada' => $data['combinada'],
                                        'tipov' => $data['tipov']

                                    );
                    // Se inserta la cotización principal
                    $idCotizacion = $this->ModeloCatalogos->Insert('ventas',$datosCotizacion);
                }elseif ($tipoCotizacion==2) {
                    $datosCotizacion = array('atencion' => $data['atencion'],
                                        'correo' => $data['correo'],
                                        'telefono' => $data['telefono'],
                                        'idCliente' => $data['idCliente'],
                                        //'rentaVentaPoliza' => $tipoCotizacion,
                                        'id_personal'=>$this->idpersonal,
                                        'estatus' => 1,
                                        'reg'=>$this->fechaactual,
                                        'combinada' => $data['combinada']);
                    // Se inserta la cotización principal
                    $idCotizacion = $this->ModeloCatalogos->Insert('rentas',$datosCotizacion);
                    //log_message('error', 'idrentas a: '.$idCotizacion);
                }
                
                    // Si trae el tipo de cotización de equipos con valor 1, es MANUAL
                    //if($tipoCotizacionEquipos==1){
                    //$arregloEquipos = explode(',',$data["equiposIndividuales"]);
                    $equiposIndividuales=$data["equiposIndividuales"];
                    $DATAeq = json_decode($equiposIndividuales);
                    //foreach ($arregloEquipos as $equipoInd) {
                    $dataarrayinsert_eq_r=array();
                    for ($j=0;$j<count($DATAeq);$j++){
                        $equipoInd=$DATAeq[$j]->equipos;
                        $cantidadeq=$DATAeq[$j]->cantidad;
                        $equipo = $this->Equipos_model->getEquipoPorId($equipoInd);
                        // Dependiendo si es VENTA, RENTA o PÓLIZA, es el precio que se va a tomar
                        if($tipoCotizacion==1){
                            if ($tpeq==1) {
                                $precio = $equipo->costo_venta;
                            }elseif ($tpeq==2) {
                                $precio = $equipo->costo_poliza;
                            }else{
                                $precio = $equipo->costo_renta;
                            }
                            
                        }
                        else if ($tipoCotizacion==2){
                            $precio = $equipo->costo_renta;
                        }
                        else if ($tipoCotizacion==3){
                            $precio = $equipo->costo_poliza;
                        }
                        if ($tpeq==3) {
                                $precio = 0;
                        }
                        //========================================
                            //$costo_toner_black = $equipo->costo_toner_black;
                            //$costo_toner_cmy = $equipo->costo_toner_cmy;
                            //$rendimiento_toner_black = $equipo->rendimiento_toner_black;
                            //$rendimiento_toner_cmy = $equipo->rendimiento_toner_cmy;
                            //$costo_unidad_imagen = $equipo->costo_unidad_imagen;
                            //$rendimiento_unidad_imagen = $equipo->rendimiento_unidad_imagen;
                            //$costo_garantia_servicio = $equipo->costo_garantia_servicio;
                            //$rendimiento_garantia_servicio = $equipo->rendimiento_garantia_servicio;
                            //$costo_pagina_monocromatica = $equipo->costo_pagina_monocromatica;
                            //$costo_pagina_color = $equipo->costo_pagina_color;
                        //========================================
                            $tipo = 1;
                            $costo_toner = 0;
                            $rendimiento_toner = 0;
                            $costo_unidad_imagen = 0;
                            $rendimiento_unidad_imagen = 0;
                            $costo_garantia_servicio = 0;
                            $rendimiento_garantia_servicio = 0;
                            $costo_pagina_monocromatica = 0;
                            $costo_pagina_color = 0;
                        //========================================
                            $DATAc = json_decode($consumibles);
                            for ($i=0;$i<count($DATAc);$i++) {
                                if ($equipo->id==$DATAc[$i]->equiposelected) {
                                    $idconsumuble=$DATAc[$i]->consumibleselected;
                                    //log_message('error', 'idconsumible '.$idconsumuble);
                                    $resultadoaddcon=$this->ModeloCatalogos->consumibleagregadoselected($idconsumuble,$equipo->id);
                                    foreach ($resultadoaddcon->result() as $item2) {
                                        $tipo = $item2->tipo;
                                        $costo_toner = $item2->poliza;
                                        $rendimiento_toner = $item2->rendimiento_toner;
                                        $costo_unidad_imagen = $item2->costo_unidad_imagen;
                                        $rendimiento_unidad_imagen = $item2->rendimiento_unidad_imagen;
                                        $costo_garantia_servicio = $item2->costo_garantia_servicio;
                                        $rendimiento_garantia_servicio = $item2->rendimiento_garantia_servicio;
                                        $costo_pagina_monocromatica = $item2->costo_pagina_monocromatica;
                                        $costo_pagina_color = $item2->costo_pagina_color;


                                    }
                                    $datachfc["idventa"]=$idCotizacion;
                                        $datachfc["idequipo"]=$DATAc[$i]->equiposelected;
                                        $datachfc["cantidad"]=$cantidadeq;
                                        $datachfc["id_consumibles"]=$idconsumuble;
                                        $datachfc["costo_toner"]=$costo_toner;
                                        $datachfc["rendimiento_toner"]=$rendimiento_toner;
                                        $datachfc["rendimiento_unidad_imagen"]=$rendimiento_unidad_imagen;
                                        if ($tipoCotizacion==1) {
                                            $this->ModeloCatalogos->Insert('ventas_has_detallesequipos_consumible',$datachfc);
                                            
                                        }elseif ($tipoCotizacion==2) {
                                            unset($datachfc["idventa"]);
                                            $datachfc["idrentas"]=$idCotizacion;
                                            $this->ModeloCatalogos->Insert('rentas_has_detallesequipos_consumible',$datachfc);

                                        }
                                }
                            }
                        //=========================================================
                            $DATAa = json_decode($accesorios);
                            for ($l=0;$l<count($DATAa);$l++) {
                                if ($equipo->id==$DATAa[$l]->equiposelected) {
                                    if ($tipoCotizacion==1) {
                                        $dataas['id_venta']=$idCotizacion;
                                        $dataas['id_equipo']=$DATAa[$l]->equiposelected;
                                        $dataas['id_accesorio']=$DATAa[$l]->accesorioselected;
                                        $dataas['cantidad']=$cantidadeq;
                                        $dataas['costo']=$this->ModeloCatalogos->costoaccesorio($DATAa[$l]->accesorioselected);
                                        $this->ModeloCatalogos->Insert('ventas_has_detallesEquipos_accesorios',$dataas);
                                    }elseif ($tipoCotizacion==2) {
                                        //log_message('error', 'idrentas Accesorio: '.$idCotizacion);
                                        $dataas["idrentas"]=$idCotizacion;
                                        $dataas['id_equipo']=$DATAa[$l]->equiposelected;
                                        $dataas['id_accesorio']=$DATAa[$l]->accesorioselected;
                                        $dataas['cantidad']=$cantidadeq;
                                        $this->ModeloCatalogos->Insert('rentas_has_detallesEquipos_accesorios',$dataas);
                                    }
                                }
                            }   
                        // Se arma el array para insertar los datos
                        if ($tipoCotizacion==1) {
                            $detallesCotizacion = array('idVenta' => $idCotizacion,
                                                        'idEquipo' => $equipo->id,
                                                        'cantidad'=>$cantidadeq,
                                                        'modelo' => $equipo->modelo,
                                                        'especificaciones_tecnicas' => $equipo->especificaciones_tecnicas,
                                                        'precio' => $precio,
                                                        'costo_unidad_imagen' => $costo_unidad_imagen,
                                                        'rendimiento_unidad_imagen' => $rendimiento_unidad_imagen,
                                                        'costo_garantia_servicio' => $costo_garantia_servicio,
                                                        'rendimiento_garantia_servicio' => $rendimiento_garantia_servicio,
                                                        'costo_pagina_monocromatica' => $costo_pagina_monocromatica,
                                                        'costo_pagina_color' => $costo_pagina_color,
                                                        'excedente' => $equipo->excedente,
                                                        'tipotoner' => $tipo,
                                                        'costo_toner' => $costo_toner,
                                                        'rendimiento_toner' => $rendimiento_toner,           
                                            );
                            // Se insertan en la BD
                            $this->ModeloCatalogos->Insert('ventas_has_detallesEquipos',$detallesCotizacion);
                        }elseif ($tipoCotizacion==2) {
                            $detallesCotizacion = array('idRenta' => $idCotizacion,
                                                        'idEquipo' => $equipo->id,
                                                        'cantidad'=>$cantidadeq,
                                                        'modelo' => $equipo->modelo,
                                                        'especificaciones_tecnicas' => $equipo->especificaciones_tecnicas,
                                                        'precio' => $precio,
                                                        'costo_unidad_imagen' => $costo_unidad_imagen,
                                                        'rendimiento_unidad_imagen' => $rendimiento_unidad_imagen,
                                                        'costo_garantia_servicio' => $costo_garantia_servicio,
                                                        'rendimiento_garantia_servicio' => $rendimiento_garantia_servicio,
                                                        'costo_pagina_monocromatica' => $costo_pagina_monocromatica,
                                                        'costo_pagina_color' => $costo_pagina_color,
                                                        'excedente' => $equipo->excedente,
                                                        'tipotoner' => $tipo,
                                                        'costo_toner' => $costo_toner,
                                                        'rendimiento_toner' => $rendimiento_toner,           
                                            );
                            // Se insertan en la BD
                            $dataarrayinsert_eq_r[]=$detallesCotizacion;
                            //$this->ModeloCatalogos->Insert('rentas_has_detallesEquipos',$detallesCotizacion);
                        }
                        //$this->Cotizaciones_model->insertarDetallesCotizacionEquipos($detallesCotizacion);
                        
                    }
                    if(count($dataarrayinsert_eq_r)>0){
                        $this->ModeloCatalogos->insert_batch('rentas_has_detallesEquipos',$dataarrayinsert_eq_r);
                    }
                //}
                 
                // Se regresa el ID de la cotización
                echo $idCotizacion;
                //==================================================================
    }
    */
    
    function nuevaventaequiposr(){
        $data = $this->input->post();
        $eleccionCotizar = 1;
        $tpeq=$data['tipoprecioequipo'];
        unset($data['tipoprecioequipo']);
        unset($data['eleccionCotizar']);
        if(isset($data['accesorios'])){
            $accesorios = $data['accesorios'];
            unset($data['accesorios']);
        }
        
        if(isset($data['consumibles'])){
            $consumibles = $data['consumibles'];
            unset($data['consumibles']);
        }
        //===============================================
        $tipoCotizacion = $data["tipo"];
                unset($data['tipo']);
                //$data['rentaVentaPoliza'] = $tipoCotizacion;
                // Saber si es cotización MANUAL o por FAMILIA
                $tipoCotizacionEquipos = $data["cotizar"];
                unset($data['cotizar']);
                // Agisnamos lo que se va a cotizar
                $data['tipo'] = $eleccionCotizar;
                // Se inserta el registro principal
                if ($tipoCotizacion==1) {
                    /*
                    $datosCotizacion = array('atencion' => $data['atencion'],
                                        'correo' => $data['correo'],
                                        'telefono' => $data['telefono'],
                                        'idCliente' => $data['idCliente'],
                                        'tipo' => $data['tipo'],
                                        //'rentaVentaPoliza' => $tipoCotizacion,
                                        'id_personal'=>$this->idpersonal,
                                        'estatus' => 1,
                                        'reg'=>$this->fechaactual,
                                        'combinada' => $data['combinada']);
                    // Se inserta la cotización principal
                    $idCotizacion = $this->ModeloCatalogos->Insert('ventas',$datosCotizacion);
                    */
                }elseif ($tipoCotizacion==2) {
                    $datosCotizacion = array('atencion' => $data['atencion'],
                                        'correo' => $data['correo'],
                                        'telefono' => $data['telefono'],
                                        'idCliente' => $data['idCliente'],
                                        //'rentaVentaPoliza' => $tipoCotizacion,
                                        'id_personal'=>$this->idpersonal,
                                        'estatus' => 1,
                                        'reg'=>$this->fechaactual,
                                        'combinada' => $data['combinada']);
                    // Se inserta la cotización principal
                    $idCotizacion = $this->ModeloCatalogos->Insert('rentas',$datosCotizacion);
                    //log_message('error', 'idrentas a: '.$idCotizacion);
                }
                
                // Si trae el tipo de cotización de equipos con valor 1, es MANUAL
                //if($tipoCotizacionEquipos==1){
                    //$arregloEquipos = explode(',',$data["equiposIndividuales"]);
                    $equiposIndividuales=$data["equiposIndividuales"];
                    $DATAeq = json_decode($equiposIndividuales);
                    //foreach ($arregloEquipos as $equipoInd) {
                    $dataarrayinsert_eq_r=array();
                    for ($j=0;$j<count($DATAeq);$j++){
                        $equipoInd=$DATAeq[$j]->equipos;
                        $cantidadeq=$DATAeq[$j]->cantidad;
                        $equipo = $this->Equipos_model->getEquipoPorId($equipoInd);
                        // Dependiendo si es VENTA, RENTA o PÓLIZA, es el precio que se va a tomar
                        if($tipoCotizacion==1){
                            if ($tpeq==1) {
                                $precio = $equipo->costo_venta;
                            }elseif ($tpeq==2) {
                                $precio = $equipo->costo_poliza;
                            }else{
                                $precio = $equipo->costo_renta;
                            }
                            
                        }
                        else if ($tipoCotizacion==2){
                            $precio = $equipo->costo_renta;
                        }
                        else if ($tipoCotizacion==3){
                            $precio = $equipo->costo_poliza;
                        }
                        if ($tpeq==3) {
                                $precio = 0;
                        }
                        //========================================
                            //$costo_toner_black = $equipo->costo_toner_black;
                            //$costo_toner_cmy = $equipo->costo_toner_cmy;
                            //$rendimiento_toner_black = $equipo->rendimiento_toner_black;
                            //$rendimiento_toner_cmy = $equipo->rendimiento_toner_cmy;
                            //$costo_unidad_imagen = $equipo->costo_unidad_imagen;
                            //$rendimiento_unidad_imagen = $equipo->rendimiento_unidad_imagen;
                            //$costo_garantia_servicio = $equipo->costo_garantia_servicio;
                            //$rendimiento_garantia_servicio = $equipo->rendimiento_garantia_servicio;
                            //$costo_pagina_monocromatica = $equipo->costo_pagina_monocromatica;
                            //$costo_pagina_color = $equipo->costo_pagina_color;
                        //========================================
                            $tipo = 1;
                            $costo_toner = 0;
                            $rendimiento_toner = 0;
                            $costo_unidad_imagen = 0;
                            $rendimiento_unidad_imagen = 0;
                            $costo_garantia_servicio = 0;
                            $rendimiento_garantia_servicio = 0;
                            $costo_pagina_monocromatica = 0;
                            $costo_pagina_color = 0;
                        //========================================
                            $DATAc = json_decode($consumibles);
                            for ($i=0;$i<count($DATAc);$i++) {
                                if ($equipo->id==$DATAc[$i]->equiposelected) {
                                    $idconsumuble=$DATAc[$i]->consumibleselected;
                                    //log_message('error', 'idconsumible '.$idconsumuble);
                                    $resultadoaddcon=$this->ModeloCatalogos->consumibleagregadoselected($idconsumuble,$equipo->id);
                                    foreach ($resultadoaddcon->result() as $item2) {
                                        $tipo = $item2->tipo;
                                        $costo_toner = $item2->poliza;
                                        $rendimiento_toner = $item2->rendimiento_toner;
                                        $costo_unidad_imagen = $item2->costo_unidad_imagen;
                                        $rendimiento_unidad_imagen = $item2->rendimiento_unidad_imagen;
                                        $costo_garantia_servicio = $item2->costo_garantia_servicio;
                                        $rendimiento_garantia_servicio = $item2->rendimiento_garantia_servicio;
                                        $costo_pagina_monocromatica = $item2->costo_pagina_monocromatica;
                                        $costo_pagina_color = $item2->costo_pagina_color;


                                    }
                                        $datachfc["idventa"]=$idCotizacion;
                                        $datachfc["idequipo"]=$DATAc[$i]->equiposelected;
                                        $datachfc["cantidad"]=1;
                                        $datachfc["id_consumibles"]=$idconsumuble;
                                        $datachfc["costo_toner"]=$costo_toner;
                                        $datachfc["rendimiento_toner"]=$rendimiento_toner;
                                        $datachfc["rendimiento_unidad_imagen"]=$rendimiento_unidad_imagen;
                                        
                                        if ($tipoCotizacion==2) {
                                            unset($datachfc["idventa"]);
                                            $datachfc["idrentas"]=$idCotizacion;
                                            for ($a=0;$a<$DATAc[$i]->cantidad;$a++) {
                                                $this->ModeloCatalogos->Insert('rentas_has_detallesequipos_consumible',$datachfc);
                                            }

                                        }
                                }
                            }
                        //=========================================================
                            $DATAa = json_decode($accesorios);
                            for ($l=0;$l<count($DATAa);$l++) {
                                if ($equipo->id==$DATAa[$l]->equiposelected) {
                                    if ($tipoCotizacion==2) {
                                        //log_message('error', 'idrentas Accesorio: '.$idCotizacion);
                                        $dataas["idrentas"]=$idCotizacion;
                                        $dataas['id_equipo']=$DATAa[$l]->equiposelected;
                                        $dataas['id_accesorio']=$DATAa[$l]->accesorioselected;
                                        $dataas['cantidad']=1;
                                        for ($a=0;$a<$DATAa[$l]->cantidad;$a++) {
                                            $this->ModeloCatalogos->Insert('rentas_has_detallesEquipos_accesorios',$dataas);
                                        }
                                    }
                                }
                            }   
                        // Se arma el array para insertar los datos
                        if ($tipoCotizacion==2) {
                            $detallesCotizacion = array('idRenta' => $idCotizacion,
                                                        'idEquipo' => $equipo->id,
                                                        'cantidad'=>1,
                                                        'modelo' => $equipo->modelo,
                                                        'especificaciones_tecnicas' => $equipo->especificaciones_tecnicas,
                                                        'precio' => $precio,
                                                        'costo_unidad_imagen' => $costo_unidad_imagen,
                                                        'rendimiento_unidad_imagen' => $rendimiento_unidad_imagen,
                                                        'costo_garantia_servicio' => $costo_garantia_servicio,
                                                        'rendimiento_garantia_servicio' => $rendimiento_garantia_servicio,
                                                        'costo_pagina_monocromatica' => $costo_pagina_monocromatica,
                                                        'costo_pagina_color' => $costo_pagina_color,
                                                        'excedente' => $equipo->excedente,
                                                        'tipotoner' => $tipo,
                                                        'costo_toner' => $costo_toner,
                                                        'rendimiento_toner' => $rendimiento_toner,           
                                            );
                            // Se insertan en la BD
                            //$dataarrayinsert_eq_r[]=$detallesCotizacion;
                            for ($b=0;$b<$cantidadeq;$b++) {
                                $this->ModeloCatalogos->Insert('rentas_has_detallesEquipos',$detallesCotizacion);
                            }
                        }
                        //$this->Cotizaciones_model->insertarDetallesCotizacionEquipos($detallesCotizacion);
                        
                    }
                    //if(count($dataarrayinsert_eq_r)>0){
                    //    $this->ModeloCatalogos->insert_batch('rentas_has_detallesEquipos',$dataarrayinsert_eq_r);
                    //}
                //}
                 /*
                $DATAa = json_decode($accesorios);
               // log_message('error', 'accesorios:acc'.$accesorios);
                //log_message('error', 'accesorios: data '.$DATAa);
                for ($l=0;$l<count($DATAa);$l++) {
                    
                    
                    if ($tipoCotizacion==1) {
                        $dataas['id_venta']=$idCotizacion;
                        $dataas['id_equipo']=$DATAa[$l]->equiposelected;
                        $dataas['id_accesorio']=$DATAa[$l]->accesorioselected;
                        $this->ModeloCatalogos->Insert('ventas_has_detallesEquipos_accesorios',$dataas);
                    }elseif ($tipoCotizacion==2) {
                        log_message('error', 'idrentas Accesorio: '.$idCotizacion);
                        $dataas["idrentas"]=$idCotizacion;
                        $dataas['id_equipo']=$DATAa[$l]->equiposelected;
                        $dataas['id_accesorio']=$DATAa[$l]->accesorioselected;
                        $this->ModeloCatalogos->Insert('rentas_has_detallesEquipos_accesorios',$dataas);
                    }
                    //log_message('error', 'Equipo: '.$equiposelected.' accesorio: '.$accesorioselected);
                }   
                */
                // Se regresa el ID de la cotización
                $this->ordenarrentas($idCotizacion);
                echo $idCotizacion;
                //==================================================================
    }
    function ordenarrentas($idrenta){
        $pendiente=0;
        $resultequipos=$this->ModeloCatalogos->getselectwheren('rentas_has_detallesEquipos',array('idRenta'=>$idrenta,'estatus'=>1));
        foreach ($resultequipos->result() as $iteme) {
            $idrow_equipo=$iteme->id;
            $equipo=$iteme->idEquipo;
            $racce=$this->ModeloCatalogos->getselectwherenlimit('rentas_has_detallesEquipos_accesorios',array('idrentas'=>$idrenta,'id_equipo'=>$equipo,'rowequipo'=>null),1);
            foreach ($racce->result() as $itema) {
                $this->ModeloCatalogos->updateCatalogo('rentas_has_detallesEquipos_accesorios',array('rowequipo'=>$idrow_equipo),array('id_accesoriod'=>$itema->id_accesoriod));
            }
            $raccc=$this->ModeloCatalogos->getselectwherenlimit('rentas_has_detallesequipos_consumible',array('idrentas'=>$idrenta,'idequipo'=>$equipo,'rowequipo'=>null),1);
            foreach ($raccc->result() as $itemc) {
                $this->ModeloCatalogos->updateCatalogo('rentas_has_detallesequipos_consumible',array('rowequipo'=>$idrow_equipo),array('id'=>$itemc->id));
            }
        }

        $raccenum=$this->ModeloCatalogos->getselectwheren('rentas_has_detallesEquipos_accesorios',array('idrentas'=>$idrenta,'rowequipo'=>null));
        if($raccenum->num_rows()>0){
            $pendiente=1;
        }
        $racccnum=$this->ModeloCatalogos->getselectwheren('rentas_has_detallesequipos_consumible',array('idrentas'=>$idrenta,'rowequipo'=>null));
        if($racccnum->num_rows()>0){
            $pendiente=1;
        }
        if($pendiente==1){
            $this->ordenarrentas($idrenta);
        }

    }
    function nuevaventaconsumibles(){
        $data = $this->input->post();

        $equiposSeleccionados = $data['arrayEquipos'];
                unset($data['arrayEquipos']);

                $tonersSeleccionados = $data['arrayToner'];
                unset($data['arrayToner']);

                $piezasSeleccionadas = $data['arrayPiezas'];
                unset($data['arrayPiezas']);

                $bodegasc = $data['bodegasc'];
                unset($data['bodegasc']);

                $presiosSeleccionadas = $data['arrayPrecio'];
                unset($data['arrayPrecio']);

                unset($data['equipoListaConsumibles']);
                unset($data['toner']);

                $data['tipo'] = 2;

                $equiposArreglo = explode(',', $equiposSeleccionados);
                $tonersArreglo = explode(',', $tonersSeleccionados);
                $piezasArreglo = explode(',', $piezasSeleccionadas);
                $precioArreglo = explode(',', $presiosSeleccionadas);
                $bodegascArreglo = explode(',', $bodegasc);
                $clienteId=$data['idCliente'];
                $datosCotizacion = array('atencion' => $data['atencion'],
                                            'correo' => $data['correo'],
                                            'telefono' => $data['telefono'],
                                            'idCliente' => $data['idCliente'],
                                            'tipo' => $data['tipo'],
                                            //'rentaVentaPoliza' => 1,
                                            'id_personal'=>$this->idpersonal,
                                            'estatus' => 1,
                                            'reg'=>$this->fechaactual,
                                        'combinada' => $data['combinada'],
                                            'tipov' => $data['tipov']
                                            );
                $idCotizacion = $this->ModeloGeneral->getinsert($datosCotizacion,'ventas');
                //$idCotizacion = $this->Cotizaciones_model->insertarCotizacion($datosCotizacion);
                $dataconsumiblesarray=array();
                for ($i=0; $i < count($equiposArreglo); $i++){
                    $bodegaselect=$bodegascArreglo[$i];
                    if($tonersArreglo[$i]>0){
                        $datosConsumible = $this->Consumibles_model->getConsumibleConPrecios($tonersArreglo[$i]);
                        $total=$precioArreglo[$i]*$piezasArreglo[$i];
                        $detallesCotizacion = array('idVentas' => $idCotizacion,
                                                'idEquipo' => $equiposArreglo[$i],
                                                'idConsumible' => $datosConsumible[0]->id,
                                                'piezas' => $piezasArreglo[$i], 
                                                'modelo' => $datosConsumible[0]->modelo,
                                                'rendimiento' =>'',
                                                'descripcion' => $datosConsumible[0]->observaciones,
                                                'precioGeneral' => $precioArreglo[$i],
                                                'totalGeneral' => $total,
                                                'bodegas' => $bodegaselect,
                                            );
                        $this->ModeloCatalogos->updatestock2('consumibles_bodegas','total','-',$piezasArreglo[$i],'consumiblesId',$datosConsumible[0]->id,'bodegaId',$bodegaselect);
                        $this->ModeloCatalogos->Insert('bitacora_movimientos',array('id_item'=>$datosConsumible[0]->id,'modeloId'=>$datosConsumible[0]->id,'modelo'=>$datosConsumible[0]->modelo,'tipo'=>4,'entrada_salida'=>0,'personal'=>$this->idpersonal,'descripcion_evento'=>'Salida para venta '.$idCotizacion.'','clienteId'=>$clienteId));
                        $dataconsumiblesarray[]=$detallesCotizacion;
                        //$this->ModeloGeneral->getinsert($detallesCotizacion,'ventas_has_detallesConsumibles');
                        //$this->Cotizaciones_model->insertarDetallesCotizacionConsumibles($detallesCotizacion);
                    }
                }
                if(count($dataconsumiblesarray)>0){
                    $this->ModeloCatalogos->insert_batch('ventas_has_detallesConsumibles',$dataconsumiblesarray);
                }
                echo $idCotizacion;
    }
    /*
    function nuevaventarefacciones(){
        $data = $this->input->post();

        $arrayRefaccion = $data['arrayRefaccion'];
        unset($data['arrayRefaccion']);
        
        $datosCotizacion = array('atencion' => $data['atencion'],
                                    'correo' => $data['correo'],
                                    'telefono' => $data['telefono'],
                                    'idCliente' => $data['idCliente'],
                                    'tipo' => 3,
                                    //'rentaVentaPoliza' => 1,
                                    'estatus' => 1,
                                    'reg'=>$this->fechaactual,
                                'combinada' => $data['combinada'],
                                'id_personal'=> $this->idpersonal,
                                'tipov' => $data['tipov'],
                                    );
                
        $idVenta = $this->ModeloGeneral->getinsert($datosCotizacion,'ventas');
        $datarefaccionesarray=array();
        $DATAc = json_decode($arrayRefaccion);       
        for ($i=0;$i<count($DATAc);$i++) {
            // insertar 
            if ($DATAc[$i]->toner>0) {
                
                $datosConsumible = $this->Consumibles_model->getrefaccionesConPrecios($DATAc[$i]->toner);
                    $total=$DATAc[$i]->tonerp*$DATAc[$i]->piezas;
                    $detallesCotizacion = array('idVentas' => $idVenta,
                                            'idEquipo' => $DATAc[$i]->equipo,
                                            'serie' => $DATAc[$i]->serie,
                                            'idRefaccion' => $datosConsumible[0]->id,
                                            'piezas' => $DATAc[$i]->piezas, 
                                            'modelo' => $datosConsumible[0]->nombre,
                                            'rendimiento' =>'',
                                            'descripcion' => $datosConsumible[0]->observaciones,
                                            'precioGeneral' => $DATAc[$i]->tonerp,
                                            'totalGeneral' => $total,
                                            'serie_bodega' => $DATAc[$i]->bodega
                                        );
                    $datarefaccionesarray[] =$detallesCotizacion;
                    
            }
        }
        if(count($datarefaccionesarray)>0){
            $this->ModeloCatalogos->insert_batch('ventas_has_detallesRefacciones',$datarefaccionesarray);
        }
        echo $idVenta;
    }
    */
    function nuevaventacombinada(){
        $data = $this->input->post();
        $data['id_personal']=$this->idpersonal;

        $idcombinada=$this->ModeloCatalogos->Insert('ventacombinada',$data);
        echo $idcombinada;
    }
    // funcion para editar los datos de rentas cuando ya se a aceptado la venta de tipo renta
    function viewventarenta(){
        $idventa = $this->input->post('idventa');
        $where_vde=array('idRenta'=>$idventa);
        $result_vde=$this->ModeloCatalogos->db10_getselectwheren('rentas_has_detallesEquipos',$where_vde);
        //$resultbodegas=$this->ModeloCatalogos->db10_getselectwheren('bodegas',array('activo' => 1));

        $result_vdacc=$this->ModeloCatalogos->ventasaccesoriosrentas($idventa);
        $result_vdcon=$this->ModeloCatalogos->ventasconsumiblesrenta($idventa);
        //$resultbodegas=$this->ModeloCatalogos->db10_getselectwheren('bodegas',array('activo' => 1));
        $resultbodegas=$this->ModeloCatalogos->view_session_bodegas();

        $html='<table class="highlight" id="venta_equiposrenta">
                      <thead>
                        <tr>
                          <th>Cantidad</th>
                          <th>Equipo</th>
                          <th>Bodega</th>
                        </tr>
                      </thead>
                      <tbody>
                        
                      ';
        foreach ($result_vde->result() as $item) {
            $html.='<tr>
                          <td>
                            <input type="hidden" name="id" id="id" value="'.$item->id.'">
                            <input type="number" name="cantidad" id="cantidad" value="'.$item->cantidad.'" min="1">
                          </td>
                          <td>
                            '.$item->modelo.'
                          </td>
                          <td>
                            <select id="serie_bodega" name="serie_bodega" class="browser-default chosen-select form-control-bmz" >';
                              foreach ($resultbodegas as $itemb) {
                                    $stockbe=$this->ModeloGeneral->stockequipobodega($item->idEquipo,$itemb->bodegaId);
                                  $html.='<option value="'.$itemb->bodegaId.'" data-stock="'.$stockbe.'">'.$itemb->bodega.' ('.$stockbe.')</option>';
                               }  
                            $html.='</select> 
                          </td>
                        </tr>';

        }
        $html.='</tbody>
            </table>';
        $html.='<table class="highlight" id="venta_accesorios">
                      <thead>
                        <tr>
                          <th>Cantidad</th>
                          <th>Accesorio</th>
                          <th>Bodega</th>
                        </tr>
                      </thead>
                      <tbody>';
        foreach ($result_vdacc->result() as $item) {
                $html.='<tr>
                          <td>
                            <input type="hidden" name="id_accesorio" id="id_accesorio" value="'.$item->id_accesorio.'">
                            <input type="number" name="cantidad" id="cantidad" value="'.$item->cantidad.'" min="1">
                          </td>
                          <td>
                            '.$item->nombre.'
                          </td>
                          <td>
                            <select id="serie_bodega" name="serie_bodega" class="browser-default chosen-select form-control-bmz" >';
                              foreach ($resultbodegas as $itemb) {
                                    $stockacc=$this->ModeloSerie->stockaccesoriosbodega($item->id_accesorio,$itemb->bodegaId);
                                  $html.='<option value="'.$itemb->bodegaId.'" data-stock="'.$stockacc.'">'.$itemb->bodega.'('.$stockacc.')</option>';
                               }  
                            $html.='</select> 
                          </td>
                        </tr>';
        }
        $html.='</tbody>
            </table>';
        $html.='<table class="highlight" id="venta_consumibles">
                      <thead>
                        <tr>
                          <th>Cantidad</th>
                          <th>Consumible</th>
                          <th>Bodega</th>
                        </tr>
                      </thead>
                      <tbody>';
        foreach ($result_vdcon->result() as $item) {
            

            $html.='<tr>
                          <td>
                            <input type="hidden" name="id_accesorio" id="id_accesorio" value="'.$item->id.'">
                            <input type="number" name="cantidad" id="cantidad" value="'.$item->cantidad.'" min="1">
                          </td>
                          <td>
                            '.$item->modelo.'
                          </td>
                          <td>
                            <select id="serie_bodega" name="serie_bodega" class="browser-default chosen-select form-control-bmz" >';
                              foreach ($resultbodegas as $itemb) {
                                $stocktoner=$this->ModeloSerie->stocktonerbodegas($item->id_consumibles,$itemb->bodegaId);
                                  $html.='<option value="'.$itemb->bodegaId.'" data-stock="'.$stocktoner.'">'.$itemb->bodega.' ('.$stocktoner.')</option>';
                               }  
                            $html.='</select> 
                          </td>

                        </tr>';
        }
        $html.='</tbody>
            </table>';
        echo $html;
    }
        ////////////////7 
    public function verificar_fecha_vencimiento(){ 
        $id = $this->input->post('id'); 
        $ven=''; 
        $aux=''; 
        $arraywhere = array('ventaId'=>$id); 
        $result=$this->ModeloCatalogos->db3_getselectwheren_2('vencimiento','prefactura',$arraywhere); 
        foreach ($result as $item) { 
           $ven=$item->vencimiento; 
        } 
        if($ven=='0000-00-00'){ 
           $aux=''; 
        }else{ 
           $aux=$ven; 
        } 
        echo $aux; 
    } 
    public function update_cfdi_registro(){ 
        $id_venta = $this->input->post('id_venta'); 
 
        //$data[''] = $this->input->post('tipo'); 
        $cfdi = $this->input->post('cfdi'); 
        $arraydata = array('ventaId' =>$id_venta); 
        $data = array('cfdi'=>$cfdi); 
        $this->ModeloCatalogos->updateCatalogo('prefactura',$data,$arraydata); 
    } 
    function montoventas(){//ventas 
        $id=$this->input->post('id'); 
        $totalgeneral=0; 

        $resultfactura=$this->ModeloCatalogos->ontenerdatosfacturaventa($id,0);
        
        $rowventa=$this->ModeloCatalogos->db10_getselectwheren('ventas',array('id'=>$id)); 
        $siniva=0;
        foreach ($rowventa->result() as $itemv) {
            $siniva=$itemv->siniva;
        }
        if($resultfactura->num_rows()==0){

            $rowequipos=$this->ModeloCatalogos->db10_getselectwheren('ventas_has_detallesEquipos',array('idVenta'=>$id)); 
            foreach ($rowequipos->result() as $item) { 
                $totalgeneral=$totalgeneral+($item->cantidad*$item->precio); 
            } 
            $resultadoaccesorios = $this->ModeloCatalogos->accesoriosventa($id); 
            foreach ($resultadoaccesorios->result() as $item) { 
                $totalgeneral=$totalgeneral+($item->cantidad*$item->costo); 
            } 
            $resultadoconsumibles = $this->ModeloCatalogos->consumiblesventa($id); 
            foreach ($resultadoconsumibles->result() as $item) {  
                $totalgeneral=$totalgeneral+($item->cantidad*$item->costo_toner); 
            } 
            $consumiblesventadetalles = $this->ModeloCatalogos->consumiblesventadetalles($id); 
            foreach ($consumiblesventadetalles->result() as $item) {  
                $totalgeneral=$totalgeneral+($item->cantidad*$item->costo_toner); 
            } 
            $ventadetallesrefacion = $this->ModeloCatalogos->ventas_has_detallesRefacciones($id); 
            foreach ($ventadetallesrefacion->result() as $item) {  
                $totalgeneral=$totalgeneral+($item->cantidad*$item->precioGeneral); 
            } 
            if($siniva==1){

            }else{
                $totalgeneral=$totalgeneral+($totalgeneral*0.16); 
            }
            

        }else{
            foreach ($resultfactura->result() as $item) {
                $totalgeneral=$totalgeneral+$item->total;
            }
        }
        echo round($totalgeneral, 2);  
    } 
    function montocombinada(){// convinadas 
        $id=$this->input->post('id'); 

        $resultfactura=$this->ModeloCatalogos->ontenerdatosfacturaventa($id,1);
        $totalgeneral=0; 

        if($resultfactura->num_rows()==0){

            $where_ventas=array('combinadaId'=>$id); 
            $resultadov = $this->ModeloCatalogos->db10_getselectwheren('ventacombinada',$where_ventas); 
            $equipo=0; 
            $consumibles=0; 
            $refacciones=0; 
            $poliza=0; 
            foreach ($resultadov->result() as $item) { 
                    $equipo=$item->equipo; 
                    $consumibles=$item->consumibles; 
                    $refacciones=$item->refacciones; 
                    $poliza=$item->poliza; 
                    $siniva=$item->siniva;
            } 
            
            $rowequipos=$this->ModeloCatalogos->db10_getselectwheren('ventas_has_detallesEquipos',array('idVenta'=>$equipo)); 
            foreach ($rowequipos->result() as $item) { 
                $totalgeneral=$totalgeneral+($item->cantidad*$item->precio); 
            } 
            $resultadoaccesorios = $this->ModeloCatalogos->accesoriosventa($equipo); 
            foreach ($resultadoaccesorios->result() as $item) { 
                $totalgeneral=$totalgeneral+($item->cantidad*$item->costo); 
            } 
            $resultadoconsumibles = $this->ModeloCatalogos->consumiblesventa($equipo); 
            foreach ($resultadoconsumibles->result() as $item) {  
                $totalgeneral=$totalgeneral+($item->cantidad*$item->costo_toner); 
            } 
            $consumiblesventadetalles = $this->ModeloCatalogos->consumiblesventadetalles($consumibles); 
            foreach ($consumiblesventadetalles->result() as $item) {  
                $totalgeneral=$totalgeneral+($item->cantidad*$item->costo_toner); 
            } 
            $ventadetallesrefacion = $this->ModeloCatalogos->ventas_has_detallesRefacciones($refacciones); 
            foreach ($ventadetallesrefacion->result() as $item) {  
                $totalgeneral=$totalgeneral+($item->cantidad*$item->precioGeneral); 
            } 
            $polizaventadetalles = $this->ModeloCatalogos->polizaventadetalles3($poliza); 
            foreach ($polizaventadetalles->result() as $item) { 
                $totalc=0; 
                if($item->precio_local>0) { 
                    $totalc=$item->precio_local; 
                } 
                if($item->precio_semi>0) { 
                    $totalc=$item->precio_semi; 
                } 
                if($item->precio_foraneo>0) { 
                    $totalc=$item->precio_foraneo; 
                } 
                 if($item->precio_especial>0) { 
                    $totalc=$item->precio_especial; 
                } 
                $totalgeneral=$totalgeneral+($totalc); 
                //$totalgeneral=$totalgeneral+($totalc*item->cantidad); 
                     
            } 
            if($siniva==1){

            }else{
                $totalgeneral=$totalgeneral+($totalgeneral*0.16);
            }
            
        }else{
            foreach ($resultfactura->result() as $item) {
                $totalgeneral=$totalgeneral+$item->total;
            }
        }
        echo round($totalgeneral, 2);
    } 
    function montoventas2($id){//ventas 
        //misma estructura en ModeloCatalogos->montoventas2
        $totalgeneral=0; 
        $rowequipos=$this->ModeloCatalogos->db10_getselectwheren('ventas_has_detallesEquipos',array('idVenta'=>$id)); 
        foreach ($rowequipos->result() as $item) { 
            $totalgeneral=$totalgeneral+($item->cantidad*$item->precio); 
        } 
        $resultadoaccesorios = $this->ModeloCatalogos->accesoriosventa($id); 
        foreach ($resultadoaccesorios->result() as $item) { 
            $totalgeneral=$totalgeneral+($item->cantidad*$item->costo); 
        } 
        $resultadoconsumibles = $this->ModeloCatalogos->consumiblesventa($id); 
        foreach ($resultadoconsumibles->result() as $item) {  
            $totalgeneral=$totalgeneral+($item->cantidad*$item->costo_toner); 
        } 
        $consumiblesventadetalles = $this->ModeloCatalogos->consumiblesventadetalles($id); 
        foreach ($consumiblesventadetalles->result() as $item) {  
            $totalgeneral=$totalgeneral+($item->cantidad*$item->costo_toner); 
        } 
        $ventadetallesrefacion = $this->ModeloCatalogos->ventas_has_detallesRefacciones($id); 
        foreach ($ventadetallesrefacion->result() as $item) {  
            $totalgeneral=$totalgeneral+($item->cantidad*$item->precioGeneral); 
        } 
        $totalgeneral=$totalgeneral+($totalgeneral*0.16); 
 
        return round($totalgeneral, 2);  
    } 
    function montocombinada2($id){// convinadas 
        //misma estructura en ModeloCatalogos->montocombinada2
        $where_ventas=array('combinadaId'=>$id); 
        $resultadov = $this->ModeloCatalogos->db10_getselectwheren('ventacombinada',$where_ventas); 
        $equipo=0; 
        $consumibles=0; 
        $refacciones=0; 
        $poliza=0; 
        foreach ($resultadov->result() as $item) { 
                $equipo=$item->equipo; 
                $consumibles=$item->consumibles; 
                $refacciones=$item->refacciones; 
                $poliza=$item->poliza; 
        } 
        $totalgeneral=0; 
        $rowequipos=$this->ModeloCatalogos->db10_getselectwheren('ventas_has_detallesEquipos',array('idVenta'=>$equipo)); 
        foreach ($rowequipos->result() as $item) { 
            $totalgeneral=$totalgeneral+($item->cantidad*$item->precio); 
        } 
        $resultadoaccesorios = $this->ModeloCatalogos->accesoriosventa($equipo); 
        foreach ($resultadoaccesorios->result() as $item) { 
            $totalgeneral=$totalgeneral+($item->cantidad*$item->costo); 
        } 
        $resultadoconsumibles = $this->ModeloCatalogos->consumiblesventa($equipo); 
        foreach ($resultadoconsumibles->result() as $item) {  
            $totalgeneral=$totalgeneral+($item->cantidad*$item->costo_toner); 
        } 
        $consumiblesventadetalles = $this->ModeloCatalogos->consumiblesventadetalles($consumibles); 
        foreach ($consumiblesventadetalles->result() as $item) {  
            $totalgeneral=$totalgeneral+($item->cantidad*$item->costo_toner); 
        } 
        $ventadetallesrefacion = $this->ModeloCatalogos->ventas_has_detallesRefacciones($refacciones); 
        foreach ($ventadetallesrefacion->result() as $item) {  
            $totalgeneral=$totalgeneral+($item->cantidad*$item->precioGeneral); 
        } 
        $polizaventadetalles = $this->ModeloCatalogos->polizaventadetalles2($poliza); 
        foreach ($polizaventadetalles->result() as $item) {  
            if($item->precio_local !==NULL) { 
                $totalc=$item->precio_local; 
            } 
            if($item->precio_semi !==NULL) { 
                $totalc=$item->precio_semi; 
            } 
            if($item->precio_foraneo !==NULL) { 
                $totalc=$item->precio_foraneo; 
            } 
            $totalgeneral=$totalgeneral+($totalc); 
            //$totalgeneral=$totalgeneral+($totalc*item->cantidad); 
            
 
             
        } 
        $totalgeneral=$totalgeneral+($totalgeneral*0.16); 
        return round($totalgeneral, 2); 
    } 
    public function guardar_pago_tipo(){ 
        $data = $this->input->post(); 
        //log_message('error',json_encode($data));
        $tipo = $data['tipo']; 
        $idcompra = $data['idcompra']; 
        $data['idpersonal']=$this->idpersonal; 
        unset($data['idcompra']); 
        unset($data['tipo']); 
        if(isset($data['pagtec'])){
        }else{
            $data['pagtec']=0;
        }
        if($this->idpersonal==18){// si diana es la que deposito se confirma en automatico
            $data['confirmado']=1;
        }
        if($tipo==1){//combinada 
            $data['idventa']=$idcompra; 
            $this->ModeloCatalogos->Insert('pagos_combinada',$data); 
            //===================================================================
            $rowpagos=$this->ModeloCatalogos->db10_getselectwheren('pagos_combinada',array('idventa'=>$idcompra));
            $totalpagos=0;
            foreach ($rowpagos->result() as $item) {
                $totalpagos=$totalpagos+$item->pago;
            }
            $totalpre=$this->montocombinada2($idcompra);
            if ($totalpagos>=$totalpre) {
                $this->ModeloCatalogos->updateCatalogo('ventacombinada',array('statuspago'=>1),array('combinadaId'=>$idcompra));
            }

        }
        if($tipo==0){//venta  
            $data['idventa']=$idcompra; 
            $this->ModeloCatalogos->Insert('pagos_ventas',$data); 
            //===================================================================
            $rowpagos=$this->ModeloCatalogos->db10_getselectwheren('pagos_ventas',array('idventa'=>$idcompra));
            $totalpagos=0;
            foreach ($rowpagos->result() as $item) {
                $totalpagos=$totalpagos+$item->pago;
            }
            
            $totalpre=$this->montoventas2($idcompra);
            if ($totalpagos>=$totalpre) {
                $this->ModeloCatalogos->updateCatalogo('ventas',array('statuspago'=>1),array('id'=>$idcompra));
            }
            

        }
        if($tipo==4){//poliza  
            $data['idpoliza']=$idcompra;

            if($this->idpersonal==18){// si diana es la que deposito se confirma en automatico
                $data['confirmado']=1;
            }
            $this->ModeloCatalogos->Insert('pagos_poliza',$data);

            //=============================================
            $rowpagos=$this->ModeloCatalogos->getselectwheren('pagos_poliza',array('idpoliza'=>$idcompra));
            $totalpagos=0;
            foreach ($rowpagos->result() as $item) {
                $totalpagos=$totalpagos+$item->pago;
            }
            $totalpre=$this->montopoliza2($idcompra);
            if ($totalpagos>=$totalpre) {
                $this->ModeloCatalogos->updateCatalogo('polizasCreadas',array('statuspago'=>1),array('id'=>$idcompra));
            }
        }    
    } 
    function montopoliza2($id){//polizas creadas
        $totalgeneral=0;
        $polizaventadetalles = $this->ModeloCatalogos->polizaventadetalles3($id);
        foreach ($polizaventadetalles->result() as $item) { 
            if($item->precio_local !==NULL) {
                $totalc=$item->precio_local;
            }
            if($item->precio_semi !==NULL) {
                $totalc=$item->precio_semi;
            }
            if($item->precio_foraneo !==NULL) {
                $totalc=$item->precio_foraneo;
            }
            $totalgeneral=$totalgeneral+($totalc);
            //$totalgeneral=$totalgeneral+($totalc*item->cantidad);
        }
        $totalgeneral=$totalgeneral+($totalgeneral*0.16);

        return round($totalgeneral, 2); 
    }
    public function table_get_tipo_compra(){ 
        $id = $this->input->post('id'); 
        $tip = $this->input->post('tip'); 
        $resultfactura=$this->ModeloCatalogos->ontenerdatosfacturaventa($id,$tip,1);
        $resultfactura_0=$this->ModeloCatalogos->ontenerdatosfacturaventa($id,$tip,0);
        $html='<table class="responsive-table display striped" cellspacing="0" id="tablepagosg"> 
                    <thead> 
                      <tr> 
                        <th>Fecha</th> 
                        <th>Método de pago</th> 
                        <th>Monto</th>
                        <th>Comentario</th> 
                        <th>Ejecutivo</th> 
                        <!--<th>Comprobante</th>-->
                        <th></th> 
                      </tr> 
                    </thead> 
                    <tbody>
                        <tr style="display: none;">
                            <td><input type="hidden" id="idventapoliza" value="'.$id.'" data-tipovp="'.$tip.'" ></td>
                             <td></td>
                             <td></td>
                             <td></td> 
                             <!--<td></td>-->
                             <td></td> 
                        </tr>
                    '; 
        if($tip==1){ 
            if($resultfactura->num_rows()==0){
                $result = $this->ModeloCatalogos->pagos_combinada($id); 
                foreach ($result as $item) { 
                    if($item->confirmado==1){
                        $itemconfirmado='Confirmado <i class="material-icons" style="color:green;">done_all</i>';
                   }else{
                        $itemconfirmado='Sin Confirmar <a class="new badge yellow" style="padding: 9px 2px 0px;" onclick="confirmarpagov('.$item->idpago.',1)"><i class="material-icons">warning</i> </a>';
                   }
                $html.='<tr class="tr_1"> 
                         <td>
                            <input type="hidden" id="idventapoliza" value="'.$id.'" data-tipovp="1" >
                            '.$item->fecha.'</td> 
                         <td>'.$item->formapago_text.'</td> 
                         <td class="montoagregado">'.$item->pago.'</td> 
                         <td>'.$item->observacion.'</td> 
                         <td>'.$item->nombre.' '.$item->apellido_paterno.' </td> 
                         <!--<td>'.$item->comprobante.'</td>-->

                         <td>
                            <span 
                                class="new badge red view_person view_person_'.$item->personalId.' " 
                                style="cursor: pointer;" 
                                onclick="deletepagov('.$item->idpago.',1)">
                                <i class="material-icons">delete</i>
                            </span>
                            '.$itemconfirmado.'  '.$item->reg.'
                        </td> 
                        </tr>'; 
                } 
            }
        }
        if($tip==0){ 
            if($resultfactura->num_rows()==0){
               $result = $this->ModeloCatalogos->pagos_ventas($id); 
               foreach ($result as $item) {
               if($item->confirmado==1){
                    $itemconfirmado='Confirmado <i class="material-icons" style="color:green;">done_all</i>';
               }else{
                    $itemconfirmado='Sin Confirmar <a class="new badge yellow" style="padding: 9px 2px 0px;" onclick="confirmarpagov('.$item->idpago.',0)"><i class="material-icons">warning</i> </a>';
               } 
                $html.='<tr class="tr_2"> 
                         <td>
                            <input type="hidden" id="idventapoliza" value="'.$id.'" data-tipovp="0" >
                            '.$item->fecha.'</td> 
                         <td>'.$item->formapago_text.'</td> 
                         <td class="montoagregado">'.$item->pago.'</td> 
                         <td>'.$item->observacion.'</td> 
                         <td>'.$item->nombre.' '.$item->apellido_paterno.' </td> 
                         <!--<td>'.$item->comprobante.'</td>--> 
                         <td>
                            <span 
                                class="new badge red view_person view_person_'.$item->personalId.'" 
                                style="cursor: pointer;" 
                                onclick="deletepagov('.$item->idpago.',0)">
                                <i class="material-icons">delete</i>
                            </span>
                            '.$itemconfirmado.' '.$item->reg.'
                        </td>
                        </tr>'; 
                } 
            }
        }
        if($tip==4){
            $resultfactura=$this->ModeloCatalogos->ontenerdatosfacturapoliza($id);
            if($resultfactura->num_rows()==0){
                $result = $this->ModeloCatalogos->pagos_poliza($id);
                foreach ($result as $item) {
                    if($item->confirmado==1){
                            $itemconfirmado='Confirmado <i class="material-icons" style="color:green;">done_all</i>';
                       }else{
                            $itemconfirmado='Sin Confirmar <a class="new badge yellow" style="padding: 9px 2px 0px;" onclick="confirmarpagov('.$item->idpago.',3)"><i class="material-icons">warning</i> </a>';
                       }
                $html.='<tr>
                         <td>
                            <input type="hidden" id="idventapoliza" value="'.$id.'" data-tipovp="2" >
                            '.$item->fecha.'</td>
                         <td>'.$item->formapago_text.'</td>
                         <td class="montoagregado">'.$item->pago.'</td>
                         <td>'.$item->observacion.'</td> 
                         <td>'.$item->nombre.' '.$item->apellido_paterno.' </td>
                         <!--<td>'.$item->comprobante.'</td>-->
                         <td>
                            <span 
                                class="new badge red view_person view_person_'.$item->personalId.'" 
                                style="cursor: pointer;" onclick="deletepagop('.$item->idpago.')">
                                <i class="material-icons">delete</i>
                            </span>
                            '.$itemconfirmado.' '.$item->reg.'
                         </td> 
                        </tr>';
                }
            }
        }
                foreach($resultfactura->result() as $item){
                    $FacturasIdselected=$item->FacturasId;
                    $resultvcp = $this->ModeloCatalogos->obtencionventapolizafactura($item->FacturasId);

                    foreach ($resultvcp->result() as $itemvcp) {
                        if($itemvcp->combinada==0){

                                    $html.='<tr class="tr_3" style="display: none;">
                                            <td><input type="hidden" id="idventapoliza" value="'.$itemvcp->ventaId.'" data-tipovp="0" ></td>
                                            <td></td><td></td><td></td><!--<td></td>--><td></td></tr>';
                            //=======================================
                                 $result = $this->ModeloCatalogos->pagos_ventas($itemvcp->ventaId); 
                                   foreach ($result as $item) { 
                                    if($item->confirmado==1){
                                        $itemconfirmado='Confirmado <i class="material-icons" style="color:green;">done_all</i>';
                                   }else{
                                        $itemconfirmado='Sin Confirmar <a class="new badge yellow" style="padding: 9px 2px 0px;" onclick="confirmarpagov('.$item->idpago.',0)"><i class="material-icons">warning</i> </a>';
                                   } 
                                    $html.='<tr class="tr_4"> 
                                             <td>
                                                <input type="hidden" id="idventapoliza" value="'.$itemvcp->ventaId.'" data-tipovp="0">
                                                '.$item->fecha.'</td> 
                                             <td>'.$item->formapago_text.'</td> 
                                             <td class="montoagregado">'.$item->pago.'</td> 
                                             <td>'.$item->observacion.'</td> 
                                             <td>'.$item->nombre.' '.$item->apellido_paterno.' </td>
                                             <!--<td>'.$item->comprobante.'</td>--> 
                                             <td>
                                                <span 
                                                    class="new badge red view_person view_person_0" 
                                                    style="cursor: pointer;" 
                                                    onclick="deletepagov('.$item->idpago.',0)">
                                                    <i class="material-icons">delete</i>
                                                </span>
                                                '.$itemconfirmado.' '.$item->reg.'
                                            </td>
                                            </tr>'; 
                                    } 
                            //=======================================
                        }elseif ($itemvcp->combinada==1) {
                                $html.='<tr class="tr_4" style="display: none;">
                                            <td><input type="hidden" id="idventapoliza" value="'.$itemvcp->ventaId.'" data-tipovp="1" ></td>
                                            <td></td><td></td><td></td><!--<td></td>--><td></td></tr>';
                            //========================================
                                $result = $this->ModeloCatalogos->pagos_combinada($itemvcp->ventaId); 
                                foreach ($result as $item) { 
                                    if($item->confirmado==1){
                                        $itemconfirmado='Confirmado <i class="material-icons" style="color:green;">done_all</i>';
                                   }else{
                                        $itemconfirmado='Sin Confirmar <a class="new badge yellow" style="padding: 9px 2px 0px;" onclick="confirmarpagov('.$item->idpago.',1)"><i class="material-icons">warning</i> </a>';
                                   }
                                $html.='<tr class="tr_6"> 
                                         <td>
                                            <input type="hidden" id="idventapoliza" value="'.$itemvcp->ventaId.'" data-tipovp="1">
                                            '.$item->fecha.'</td> 
                                         <td>'.$item->formapago_text.'</td> 
                                         <td class="montoagregado">'.$item->pago.'</td> 
                                         <td>'.$item->observacion.'</td> 
                                         <td>'.$item->nombre.' '.$item->apellido_paterno.' </td>
                                         <!--<td>'.$item->comprobante.'</td>-->

                                         <td>
                                            <span 
                                                class="new badge red view_person view_person_'.$item->personalId.'" 
                                                style="cursor: pointer;" 
                                                onclick="deletepagov('.$item->idpago.',1)">
                                                <i class="material-icons">delete</i>
                                            </span>
                                            '.$itemconfirmado.' '.$item->reg.'
                                        </td> 
                                        </tr>'; 
                                } 
                            //========================================
                        }elseif ($itemvcp->combinada==2) {
                                $html.='<tr style="display: none;">
                                            <td><input type="hidden" id="idventapoliza" value="'.$itemvcp->ventaId.'" data-tipovp="2" ></td>
                                            <td></td><td></td><td></td><!--<td></td>--><td></td></tr>';
                            //========================================
                                $result = $this->ModeloCatalogos->pagos_poliza($itemvcp->ventaId);
                                foreach ($result as $item) {
                                    if($item->confirmado==1){
                                        $itemconfirmado='Confirmado <i class="material-icons" style="color:green;">done_all</i>';
                                   }else{
                                        $itemconfirmado='Sin Confirmar <a class="new badge yellow" style="padding: 9px 2px 0px;" onclick="confirmarpagov('.$item->idpago.',3)"><i class="material-icons">warning</i> </a>';
                                   }
                                $html.='<tr class="tr_7">
                                         <td>
                                            <input type="hidden" id="idventapoliza" value="'.$itemvcp->ventaId.'" data-tipovp="2">
                                            '.$item->fecha.'</td>
                                         <td>'.$item->formapago_text.'</td>
                                         <td class="montoagregado">'.$item->pago.'</td>
                                         <td>'.$item->observacion.'</td> 
                                         <td>'.$item->nombre.' '.$item->apellido_paterno.' </td>
                                         <!--<td>'.$item->comprobante.'</td>-->
                                         <td>
                                            <span 
                                                class="new badge red view_person view_person_'.$item->personalId.'" 
                                                style="cursor: pointer;" onclick="deletepagop('.$item->idpago.')">
                                                <i class="material-icons">delete</i>
                                            </span>
                                            '.$itemconfirmado.' '.$item->reg.'
                                         </td> 
                                        </tr>';
                                }
                            //========================================
                        }
                    }
                    
                }
                foreach($resultfactura_0->result() as $item){
                    $FacturasIdselected=$item->FacturasId;
                    $resultvcp_cp = $this->ModeloCatalogos->obteciondecomplememtofactura($FacturasIdselected);
                    foreach ($resultvcp_cp->result() as $itemcp) {
                        $html.='<tr class="tr_8"> 
                                     <td>
                                        <input type="hidden" id="idventapoliza" value="'.$id.'" data-tipovp="'.$tip.'" class="pagodecomplemento">
                                        '.$itemcp->Fecha.'
                                    </td> 
                                     <td></td> 
                                     <td class="montoagregado">'.$itemcp->ImpPagado.'</td> 
                                     <td>Complemento</td> 
                                     <td></td> 
                                     <!--<td></td>--> 
                                     <td>
                                        
                                    </td>
                                    </tr>'; 
                    }
                }
        $html.='</tbody> 
                  </table>'; 
        echo $html; 
    } 
    function vericar_pass(){
      $pass = $this->input->post('pass');
      $pago = $this->input->post('pago');
      $tipo = $this->input->post('tipo'); 
      log_message('error',"vericar_pass: '$pass' ");
      //$verifica = $this->Rentas_model->verificar_pass2($pass);
      //=========================================================================
      $permisos = $this->Login_model->permisoadminarray(array('1','17','18'));//1admin 17 julio 18 diana
      $verificap=0;
      foreach ($permisos as $item) {
        $permisotemp=0;

        $verificar = password_verify($pass,$item->contrasena);

        if ($verificar==false) {
            $verificar = password_verify($pass,$item->clavestemp);
            if ($verificar) {
                $permisotemp=1;
                $this->ModeloCatalogos->updateCatalogo('usuarios',array('clavestemp'=>null,'clavestemp_text'=>null),array('UsuarioID'=>$item->UsuarioID));
            }
        }
        if ($verificar) {
            $verificap=1;
        }
      }
      //======================================================================
      //log_message('error', '------ '.$verifica);
      $aux = 0;
      if($pass=='x1a2b3'){
        $verificap=1;
      }
      if($verificap == 1){
         $aux=1;
        if ($tipo==1) {
            //combinada
            //log_message('error', ' pagos combinada ');
            $this->ModeloCatalogos->getdeletewheren2('pagos_combinada',$pago);
            //$this->Modelobitacoras->Insert(array('contenido'=>'Se Elimino pago de venta combinada','nombretabla'=>'pagos_combinada','idtable'=>$pago,'tipo'=>'Delete','personalId'=>$this->idpersonal));
        }
        if ($tipo==0) {
            //venta normal
            //log_message('error', ' pagos ventas ');
            $this->ModeloCatalogos->getdeletewheren2('pagos_ventas',$pago);
            //$this->Modelobitacoras->Insert(array('contenido'=>'Se Elimino pago de venta','nombretabla'=>'pagos_ventas','idtable'=>$pago,'tipo'=>'Delete','personalId'=>$this->idpersonal));
        }
      }
      echo $aux;
    }
    function addseriviciovd(){
        $data = $this->input->post(); 
        $idventa=intval($data['idventa']);
        $poliza = $data['poliza'];
        $servicio = $data['servicio'];
        $direccion = $data['direccion'];
        $tipo = $data['tipo'];
        $motivo = $data['motivo'];
        $fechaentrega=$data['fecharecoleccion'];

        //$this->ModeloCatalogos->updateCatalogo('ventas',array('servicio'=>1,'servicio_poliza'=>$poliza,'servicio_servicio'=>$servicio,'servicio_direccion'=>$direccion,'servicio_tipo'=>$tipo,'servicio_motivo'=>$motivo,'fechaentrega'=>$fechaentrega),array('id'=>$idventa));
        $this->ModeloCatalogos->updateCatalogo('ventas',array('servicio'=>1,'servicio_poliza'=>$poliza,'servicio_servicio'=>$servicio,'servicio_direccion'=>$direccion,'servicio_tipo'=>$tipo,'servicio_motivo'=>$motivo),array('id'=>$idventa));
    }
    function addseriviciovd2(){
        $data = $this->input->post(); 
        $idventa=intval($data['idventa']);
        $fechaentrega=$data['fecharecoleccion'];

        $this->ModeloCatalogos->updateCatalogo('ventas',array('fechaentrega'=>$fechaentrega),array('id'=>$idventa));
    }
    ///////////////////////////
    public function por_evento(){
        $this->load->view('header');
        $this->load->view('ventas/porevento');
        $this->load->view('footer');
        $this->load->view('ventas/poreventojs');
    }
    public function tipo_formato($id){
        $data['configuracionCotizacion'] = $this->ConfiguracionDocumentos_Model->getConfiguracion(1);
        if($id==1){ 
            $this->load->view('Reportes/porevento',$data); 
        }else{
            $this->load->view('Reportes/contrato',$data);
        }

    }
    function ventadelete(){
        $data = $this->input->post();
        $ventaid = $data['ventaid'];
        $ventatipo = $data['ventatipo'];
        $motivo = $data['motivo'];
        if (isset($this->idpersonal)) {
            $idpersonal=$this->idpersonal;
        }else{
            $idpersonal=0;
        }
        if ($ventatipo==0) {//normal
            //$resultfarventa=$this->ModeloCatalogos->db10_getselectwheren('factura_venta',array('ventaId'=>$ventaid,'combinada'=>0));
            $resultfarventa=$this->Ventas_model->validarfactura($ventaid,0);
            if ($resultfarventa->num_rows()>0) {
                $eliminacion=0;
            }else{
                $eliminacion=1;
                
                $datosventa=$this->ModeloCatalogos->db10_getselectwheren('ventas',array('id'=>$ventaid));
                $clienteid=0;
                foreach ($datosventa->result() as $itemv) {
                    $clienteid=$itemv->idCliente;
                    $activo=$itemv->activo;
                }
                $this->ModeloCatalogos->updateCatalogo('ventas',array('motivo'=>$motivo,'activo'=>0,'personaldelete'=>$idpersonal),array('id'=>$ventaid));
                if($activo==1){
                    //====================== consumibles 
                        $datosconsumibles=$this->ModeloCatalogos->db10_getselectwheren('ventas_has_detallesConsumibles',array('idVentas'=>$ventaid));
                        foreach ($datosconsumibles->result() as $item) {
                            if($item->bodegas>0){
                                $itembodegas=$item->bodegas;
                            }else{
                                $itembodegas=1;//lo manda directo a boule
                            }
                            $this->ModeloCatalogos->updatestock2('consumibles_bodegas','total','+',$item->piezas,'consumiblesId',$item->idConsumible,'bodegaId',$itembodegas);
                            $this->ModeloCatalogos->getdeletewheren('contrato_folio',array('foliotext'=>$item->foliotext));
                            //1 equipo, 2 acesorio, 3 refaccion,4 consumible    
                            $this->ModeloCatalogos->Insert('bitacora_movimientos',array('entrada_salida'=>1,'descripcion_evento'=>'devolucion por eliminacion para venta '.$ventaid.'','id_item'=>$item->id,'cantidad'=>$item->piezas,'modeloId'=>$item->idConsumible,'tipo'=>4,'personal'=>$this->idpersonal,'bodega_d'=>$itembodegas,'clienteId'=>$clienteid));
                        }
                    //======================
                    //====================== equipos 
                        $datosequipos=$this->ModeloCatalogos->db10_getselectwheren('ventas_has_detallesEquipos',array('idVenta'=>$ventaid));
                        foreach ($datosequipos->result() as $item) {

                            $datosseriese=$this->ModeloCatalogos->db10_getselectwheren('asignacion_series_equipos',array('id_equipo'=>$item->id));
                            foreach ($datosseriese->result() as $items) {
                                
                                $this->ModeloCatalogos->updateCatalogo('series_productos',array('status'=>0),array('serieId'=>$items->serieId));$this->Configuraciones_model->registrarmovimientosseries($items->serieId,1);

                            //1 equipo, 2 acesorio, 3 refaccion,4 consumible    
                            $this->ModeloCatalogos->Insert('bitacora_movimientos',array('entrada_salida'=>1,'descripcion_evento'=>'devolucion serie por eliminacion para venta '.$ventaid,'personal'=>$this->idpersonal,'tipo'=>1,'id_item'=>$item->id,'modeloId'=>$item->idEquipo,'serieId'=>$items->serieId,'bodega_d'=>0,'clienteId'=>$clienteid,'cantidad'=>1));
                            }
                        }
                    //======================
                    //====================== accesorios 
                        $datosaccesorios=$this->ModeloCatalogos->db10_getselectwheren('ventas_has_detallesEquipos_accesorios',array('id_venta'=>$ventaid));
                        foreach ($datosaccesorios->result() as $item) {

                            $datosseriese=$this->ModeloCatalogos->db10_getselectwheren('asignacion_series_accesorios',array('id_accesorio'=>$item->id_accesorio));
                            foreach ($datosseriese->result() as $items) {
                                
                                $this->ModeloCatalogos->updateCatalogo('series_accesorios',array('status'=>0),array('serieId'=>$items->serieId));$this->Configuraciones_model->registrarmovimientosseries($items->serieId,2);
                                //1 equipo, 2 acesorio, 3 refaccion,4 consumible    
                                $this->ModeloCatalogos->Insert('bitacora_movimientos',array('id_item'=>$item->id_accesoriod,'cantidad'=>1,'modeloId'=>$item->id_accesorio,'tipo'=>2,'entrada_salida'=>1,'personal'=>$this->idpersonal,'descripcion_evento'=>'devolucion serie por eliminacion para venta '.$ventaid,'serieId'=>$items->serieId,'bodega_d'=>0,'clienteId'=>$clienteid));
                            }
                        }
                    //======================
                    //====================== refacciones 
                        $datosrefacciones=$this->ModeloCatalogos->db10_getselectwheren('ventas_has_detallesRefacciones',array('idVentas'=>$ventaid));
                        foreach ($datosrefacciones->result() as $item) {
                            $bodega=$item->serie_bodega;
                            $piezas=$item->piezas;
                            //log_message('error', 'idRefaccion '.$item->idRefaccion);
                            //log_message('error', 'id '.$item->id);
                            $datosseriesr=$this->ModeloCatalogos->db10_getselectwheren('asignacion_series_refacciones',array('id_refaccion'=>$item->id));
                            foreach ($datosseriesr->result() as $items) {
                                $serieId=$items->serieId;
                                $cantidad=$items->cantidad;
                                if($items->serieId>0){
                                    $datosseriesrss=$this->ModeloCatalogos->db10_getselectwheren('series_refacciones',array('serieId'=>$items->serieId));
                                    foreach ($datosseriesrss->result() as $itemm) {
                                        if ($itemm->con_serie==1) {
                                            $cantidad=1;
                                            $this->ModeloCatalogos->updateCatalogo('series_refacciones',array('status'=>0,'cantidad'=>$cantidad),array('serieId'=>$items->serieId));
                                            $this->Configuraciones_model->registrarmovimientosseries($items->serieId,3);
                                        }else{
                                            //log_message('error', 'serieId '.$items->serieId);
                                            $this->ModeloCatalogos->updatestock('series_refacciones','cantidad','+',$piezas,'serieId',$items->serieId);
                                            //$cantidad=$cantidad+$itemm->cantidad;
                                            $this->Configuraciones_model->registrarmovimientosseries($items->serieId,3);
                                        }
                                        //1 equipo, 2 acesorio, 3 refaccion,4 consumible    
                                        $this->ModeloCatalogos->Insert('bitacora_movimientos',array('id_item'=>$item->id,'cantidad'=>$piezas,'modeloId'=>$item->idRefaccion,'tipo'=>3,'entrada_salida'=>1,'personal'=>$this->idpersonal,'descripcion_evento'=>'devolucion serie por eliminacion para venta '.$ventaid,'serieId'=>$items->serieId,'bodega_d'=>0,'clienteId'=>$clienteid));
                                    }
                                }
                                ///=================================================
                                
                            }
                        }
                    //======================
                }
            }
        }else{//combinada
            //$resultfarventa=$this->ModeloCatalogos->db10_getselectwheren('factura_venta',array('ventaId'=>$ventaid,'combinada'=>1));
            $resultfarventa=$this->Ventas_model->validarfactura($ventaid,1);
            if ($resultfarventa->num_rows()>0) {
                $eliminacion=0;
            }else{
                $eliminacion=1;

                

                $datosventacon=$this->ModeloCatalogos->db10_getselectwheren('ventacombinada',array('combinadaId'=>$ventaid));
                $vequipo=0;
                $vconsumible=0;
                $vrefacciones=0;
                foreach ($datosventacon->result() as $itemc) {
                    $vequipo=$itemc->equipo;
                    $vconsumible=$itemc->consumibles;
                    $vrefacciones=$itemc->refacciones;
                    $idCliente=$itemc->idCliente;
                    $clienteid=$itemc->idCliente;
                    $activo=$itemc->activo;
                }

                $this->ModeloCatalogos->updateCatalogo('ventacombinada',array('motivo'=>$motivo,'activo'=>0,'personaldelete'=>$idpersonal),array('combinadaId'=>$ventaid));
                if($vequipo>0 && $activo==1){
                    //====================== equipos 
                        $this->ModeloCatalogos->updateCatalogo('ventas',array('activo'=>0),array('id'=>$vequipo));
                        $datosequipos=$this->ModeloCatalogos->db10_getselectwheren('ventas_has_detallesEquipos',array('idVenta'=>$vequipo));
                        foreach ($datosequipos->result() as $item) {

                            $datosseriese=$this->ModeloCatalogos->db10_getselectwheren('asignacion_series_equipos',array('id_equipo'=>$item->id));
                            foreach ($datosseriese->result() as $items) {
                                
                                $this->ModeloCatalogos->updateCatalogo('series_productos',array('status'=>0),array('serieId'=>$items->serieId));
                                $this->Configuraciones_model->registrarmovimientosseries($items->serieId,1);
                                //1 equipo, 2 acesorio, 3 refaccion,4 consumible    
                                $this->ModeloCatalogos->Insert('bitacora_movimientos',array('id_item'=>$item->id,'cantidad'=>1,'modeloId'=>$item->idEquipo,'tipo'=>1,'entrada_salida'=>1,'personal'=>$this->idpersonal,'descripcion_evento'=>'devolucion serie por eliminacion para venta '.$ventaid,'serieId'=>$items->serieId,'clienteId'=>$clienteid));
                            }
                        }
                    //======================
                    //====================== accesorios 
                        $this->ModeloCatalogos->updateCatalogo('ventas',array('activo'=>0),array('id'=>$vequipo));
                        $datosaccesorios=$this->ModeloCatalogos->db10_getselectwheren('ventas_has_detallesEquipos_accesorios',array('id_venta'=>$vequipo));
                        foreach ($datosaccesorios->result() as $item) {

                            $datosseriese=$this->ModeloCatalogos->db10_getselectwheren('asignacion_series_accesorios',array('id_accesorio'=>$item->id_accesorio));
                            foreach ($datosseriese->result() as $items) {
                                
                                $this->ModeloCatalogos->updateCatalogo('series_accesorios',array('status'=>0),array('serieId'=>$items->serieId));
                                $this->Configuraciones_model->registrarmovimientosseries($items->serieId,2);
                                //1 equipo, 2 acesorio, 3 refaccion,4 consumible    
                                $this->ModeloCatalogos->Insert('bitacora_movimientos',array('id_item'=>$item->id_accesoriod,'cantidad'=>1,'modeloId'=>$item->id_accesorio,'tipo'=>2,'entrada_salida'=>1,'personal'=>$this->idpersonal,'descripcion_evento'=>'devolucion serie por eliminacion para venta combinada '.$ventaid,'serieId'=>$items->serieId,'clienteId'=>$clienteid));
                            }
                        }
                    //======================
                }
                if($vconsumible>0 && $activo==1){
                    //====================== consumibles 
                        $this->ModeloCatalogos->updateCatalogo('ventas',array('activo'=>0),array('id'=>$vconsumible));
                        $datosconsumibles=$this->ModeloCatalogos->db10_getselectwheren('ventas_has_detallesConsumibles',array('idVentas'=>$vconsumible));
                        foreach ($datosconsumibles->result() as $item) {
                            //$this->ModeloCatalogos->updatestock('consumibles','stock','+',$item->piezas,'id',$item->idEquipo);//xxx validaryo
                            if($item->bodegas>0){
                                $itembodegas=$item->bodegas;
                            }else{
                                $itembodegas=1;// lo manda directo a BOULEVARD
                            }
                            $this->ModeloCatalogos->updatestock2('consumibles_bodegas','total','+',$item->piezas,'consumiblesId',$item->idConsumible,'bodegaId',$itembodegas);//

                            //1 equipo, 2 acesorio, 3 refaccion,4 consumible    
                            $this->ModeloCatalogos->Insert('bitacora_movimientos',array('id_item'=>$item->id,'cantidad'=>$item->piezas,'modeloId'=>$item->idConsumible,'tipo'=>4,'entrada_salida'=>1,'personal'=>$this->idpersonal,'descripcion_evento'=>'devolucion por eliminacion para venta combinada'.$ventaid.'','clienteId'=>$clienteid));
                        }
                    //======================
                }
                if($vrefacciones>0 && $activo==1){
                    //====================== refacciones 
                    $this->ModeloCatalogos->updateCatalogo('ventas',array('activo'=>0),array('id'=>$vrefacciones));
                        $datosrefacciones=$this->ModeloCatalogos->db10_getselectwheren('ventas_has_detallesRefacciones',array('idVentas'=>$vrefacciones));
                        foreach ($datosrefacciones->result() as $item) {
                            $bodega=$item->serie_bodega;
                            $piezas=$item->piezas;
                            $datosseriesr=$this->ModeloCatalogos->db10_getselectwheren('asignacion_series_refacciones',array('id_refaccion'=>$item->id));
                            foreach ($datosseriesr->result() as $items) {
                                $serieId=$items->serieId;
                                $cantidad=$items->cantidad;
                                if($items->serieId>0){
                                    $datosseriesrss=$this->ModeloCatalogos->db10_getselectwheren('series_refacciones',array('serieId'=>$items->serieId));
                                    foreach ($datosseriesrss->result() as $itemm) {
                                        if ($itemm->con_serie==1) {
                                            $cantidad=1;

                                            $this->ModeloCatalogos->updateCatalogo('series_refacciones',array('status'=>0,'cantidad'=>$cantidad),array('serieId'=>$items->serieId));
                                            $this->Configuraciones_model->registrarmovimientosseries($items->serieId,3);
                                        }else{
                                            $this->ModeloCatalogos->updatestock('series_refacciones','cantidad','+',$piezas,'serieId',$items->serieId);
                                            $this->Configuraciones_model->registrarmovimientosseries($items->serieId,3);
                                           

                                        }
                                        //1 equipo, 2 acesorio, 3 refaccion,4 consumible    
                                        $this->ModeloCatalogos->Insert('bitacora_movimientos',array('id_item'=>$item->id,'cantidad'=>$piezas,'modeloId'=>$item->idRefaccion,'tipo'=>3,'entrada_salida'=>1,'personal'=>$this->idpersonal,'descripcion_evento'=>'devolucion serie por eliminacion para venta combinada '.$ventaid,'serieId'=>$items->serieId,'clienteId'=>$clienteid));
                                    }
                                    ///=================================================
                                    
                                }
                            }
                        }
                    //======================
                }
            }
        }
        echo $eliminacion;
    }
    function serviciodelete(){
        $data = $this->input->post();
        $servicioid = $data['servicioid'];
        $serviciotipo = $data['serviciotipo'];
        $motivo = $data['motivo'];
        
        if ($serviciotipo==1) {
            $table='asignacion_ser_contrato_a';
        }elseif ($serviciotipo==2) {
            $table='asignacion_ser_poliza_a';
        }elseif ($serviciotipo==3) {
            $table='asignacion_ser_cliente_a';
        }  
        $this->ModeloCatalogos->updateCatalogo($table,array('activo'=>0,'motivoeliminado'=>$motivo),array('asignacionId'=>$servicioid));
    }
    function update_cfdi_servicio(){
        $data = $this->input->post();
        $servicioid = $data['id'];
        $serviciotipo = $data['tipo'];
        $cfdi = $data['cfdi'];
        
        if ($serviciotipo==1) {
            $table='asignacion_ser_contrato_a';
        }elseif ($serviciotipo==2) {
            $table='asignacion_ser_poliza_a';
        }elseif ($serviciotipo==3) {
            $table='asignacion_ser_cliente_a';
        }  
        $this->ModeloCatalogos->updateCatalogo($table,array('cdfi'=>$cfdi),array('asignacionId'=>$servicioid));
    }
    function guardar_pago_tipo_servicio(){
        $data = $this->input->post();
        $data['idpersonal']=$this->idpersonal;
        $this->ModeloCatalogos->Insert('pagos_servicios',$data);
    }
    function montoservicio(){
        $data = $this->input->post();
        $idcpe=$data['id'];
        $tipo=$data['tipo'];

        $tprecio=0;
        $totalgeneral=0;
        if ($tipo==1) {
            $ventacon=$this->ModeloCatalogos->db10_getselectwheren('asignacion_ser_contrato_a',array('asignacionId'=>$idcpe));
            foreach ($ventacon->result() as $item) {
                $tservicio=$item->tservicio;
            }
            $ventats=$this->ModeloCatalogos->db10_getselectwheren('servicio_evento',array('id'=>$tservicio));
            foreach ($ventats->result() as $item) {
                $tprecio=$item->local;
            }
        }elseif ($tipo==2) {
            $ventacon=$this->ModeloCatalogos->db10_getselectwheren('asignacion_ser_poliza_a',array('asignacionId'=>$idcpe));
            foreach ($ventacon->result() as $item) {
                $tservicio=$item->tservicio;
            }
            $ventats=$this->ModeloCatalogos->db10_getselectwheren('servicio_evento',array('id'=>$tservicio));
            foreach ($ventats->result() as $item) {
                $t_servicio=$item->nombre;
                $tprecio=$item->semi;
            }
        }elseif ($tipo==3) {
            $ventacon=$this->ModeloCatalogos->db10_getselectwheren('asignacion_ser_cliente_a',array('asignacionId'=>$idcpe));
            foreach ($ventacon->result() as $item) {
                $idCliente=$item->clienteId;
                $t_modelo=$item->equipostext;
                $firma=$item->firma;
                $t_servicio_motivo=$item->tserviciomotivo;
                $data['cdfif']=$item->cdfi;
            }
            $ventacon2=$this->ModeloCatalogos->db10_getselectwheren('asignacion_ser_cliente_a_d',array('asignacionId'=>$idcpe));
            $t_modelo='';
            foreach ($ventacon2->result() as $item) {
                $tservicio=$item->tservicio;
                $t_modelo.=$item->serie.' ';
            }
            $ventats=$this->ModeloCatalogos->db10_getselectwheren('servicio_evento',array('id'=>$tservicio));
            foreach ($ventats->result() as $item) {
                $t_servicio=$item->nombre;
                $tprecio=$item->foraneo;
            }
        }

        $resultadoconsumibles=$this->Ventas_model->datosserviciosconsumibles($idcpe,$tipo);
        $resultadorefacciones=$this->Ventas_model->datosserviciosrefacciones($idcpe,$tipo);
        $resultadoaccesorios=$this->Ventas_model->datosserviciosaccesorios($idcpe,$tipo);
        foreach ($resultadoconsumibles->result() as $item) { 
            $costo=$item->costo;
            $totalc=$item->cantidad*$costo;
            $totalgeneral=$totalgeneral+$totalc;
        }
        foreach ($resultadorefacciones->result() as $item) { 
            $totala=$item->cantidad*$item->costo;
            $totalgeneral=$totalgeneral+$totala;
        }
        foreach ($resultadoaccesorios->result() as $item) { 
            $totala=$item->cantidad*$item->costo;
            $totalgeneral=$totalgeneral+$totala;
        }

        $tprecio=($tprecio+$totalgeneral)*1.16;
        echo round($tprecio, 2);
    }
    public function table_get_pagos_servicios(){ 
        $id = $this->input->post('id'); 
        $tip = $this->input->post('tip'); 
        $html='<table class="responsive-table display striped" cellspacing="0"> 
                    <thead> 
                      <tr> 
                        <th>Fecha</th> 
                        <th>Método de pago</th> 
                        <th>Monto</th>
                        <th>Comentario</th> 
                        <!--<th>Comprobante</th>-->
                        <th></th> 
                      </tr> 
                    </thead> 
                    <tbody>'; 
        
           
            $strq = "SELECT p.idpago,p.fecha,f.formapago_text,p.pago,p.comprobante,p.observacion
                FROM pagos_servicios as p
                inner JOIN f_formapago as f on f.id=p.idmetodo
                WHERE p.idservicio=".$id." and p.tipos=".$tip; 

            $query = $this->db->query($strq);
            foreach ($query->result() as $item) { 
            $html.='<tr class="remove_table_pagos_ser_'.$item->idpago.'"> 
                     <td>'.$item->fecha.'</td> 
                     <td>'.$item->formapago_text.'</td> 
                     <td class="montoagregados">'.$item->pago.'</td> 
                     <td>'.$item->observacion.'</td> 
                     <!--<td>'.$item->comprobante.'</td>-->

                     <td>
                        <span 
                            class="new badge red" 
                            style="cursor: pointer;" 
                            onclick="deletepagoser('.$item->idpago.')">
                            <i class="material-icons">delete</i>
                        </span>
                    </td> 
                    </tr>'; 
            } 
        
        $html.='</tbody> 
                  </table>'; 
        echo $html; 
    } 
    function deletepagoser(){
        $id = $this->input->post('id'); 
        $this->ModeloCatalogos->getdeletewheren3('pagos_servicios',array('idpago'=>$id));
    }
    function prefacturaspedientes(){
        $datos = $this->input->post();
        $idcliente=$datos['idcliente'];
        $datosarray= array(
                            'prefactura'=>0,
                            'activo'=>1,
                            'idCliente'=>$idcliente
                            );
        $result= $this->ModeloCatalogos->db10_getselectwheren('vista_ventas',$datosarray);
        echo json_encode($result->result());
    }
    function cliente_equipo_historial_group(){
        $datos = $this->input->post();
        $idcliente=$datos['idcliente'];
        
        $result= $this->ModeloCatalogos->cliente_has_equipo_historial_group($idcliente);
        echo json_encode($result->result());
    }
    function obtencionfacturas(){
        $tipo = $this->input->post('tipo');
        $id = $this->input->post('id');
        $result = $this->ModeloCatalogos->obtencionfacturasventas($tipo,$id);
        $folio='';
        foreach ($result->result() as $item) {
            $folio=$item->Folio;
        }
        echo $folio;
    }
    function cliente_refacciones_cotizacion(){
        $datos = $this->input->post();
        $idcliente=$datos['idcliente'];
        $cotizaciones=$datos['cotizaciones'];
        
        $result= $this->ModeloCatalogos->cliente_refacciones_cotizacion($idcliente,$cotizaciones);
        echo json_encode($result->result());
    }
    /*
    function nuevaCotizacionaccesorios(){
        $data = $this->input->post();

        $arrayac = $data['arrayac'];
        $idventatablee = $data['idventatablee'];
        unset($data['arrayac']);
        if($idventatablee>0){
            $idVenta=intval($idventatablee);
        }else{
            $datosCotizacion = array('atencion' => $data['atencion'],
                                    'correo' => $data['correo'],
                                    'telefono' => $data['telefono'],
                                    'idCliente' => $data['idCliente'],
                                    'tipo' => 3,
                                    //'rentaVentaPoliza' => 1,
                                    'estatus' => 1,
                                    'reg'=>$this->fechaactual,
                                'combinada' => $data['combinada'],
                                'id_personal'=> $this->idpersonal,
                                'tipov' => $data['tipov'],
                                    );
                
            $idVenta = $this->ModeloGeneral->getinsert($datosCotizacion,'ventas');
        }
        $datareaccesorioarray=array();
        $DATAc = json_decode($arrayac);       
        for ($i=0;$i<count($DATAc);$i++) {
            
                
                
                    $detallesCotizacion = array(
                                            'id_venta'=>$idVenta,
                                            'id_equipo'=>$DATAc[$i]->equipo,
                                            'id_accesorio'=>$DATAc[$i]->accesorio,
                                            'serie_bodega'=>$DATAc[$i]->bodega,
                                            'costo'=>$DATAc[$i]->costo,
                                            'cantidad'=>$DATAc[$i]->cantidad
                                        );
                    $datareaccesorioarray[] =$detallesCotizacion;
                    
            
        }
        if(count($datareaccesorioarray)>0){
            $this->ModeloCatalogos->insert_batch('ventas_has_detallesEquipos_accesorios',$datareaccesorioarray);
        }
        echo $idVenta;
    }
    */
    function stockporbodegas(){
        $params = $this->input->post();
        $equipo = $params['equipo'];
        //log_message('error', 'equipo: '.$equipo);

        if($equipo>0){
            $result=$this->Clientes_model->consultarporbodega($equipo);
            $html='';
            foreach ($result->result() as $item) {
                $html.=$item->bodega.': '.$item->total.'<br>';
            }
            
        }else{
            $html='Consultando';
        }
        echo $html;
    }
    function v_ext(){
        $params=$this->input->post();
        $id=$params['id'];
        $tipo=$params['tipo'];
        

        $verifequipos=array();
        $insert_eq=array();
        $insert_acc=array();
        $insert_cone=array();
        $insert_con=array();
        $insert_ref=array();
        $insert_pol=array();

        $equipo=$id;
        $consumibles=$id;
        $refacciones=$id;
        $poliza=0;
        if($tipo==1){
            $resultadov = $this->ModeloCatalogos->db6_getselectwheren('ventacombinada',array('combinadaId'=>$id));
            foreach ($resultadov->result() as $item) {
                $id_personal=$item->id_personal;
                $idCliente=$item->idCliente;
                $equipo=$item->equipo;
                $consumibles=$item->consumibles;
                $refacciones=$item->refacciones;
                $poliza=$item->poliza;
                
            }
        }else{
            $resultadov = $this->ModeloCatalogos->db6_getselectwheren('ventas',array('id'=>$id));
            foreach ($resultadov->result() as $item) {
                $idCliente=$item->idCliente;
            }
        }
            
        $resultadoequipos = $this->ModeloCatalogos->db6_getselectwheren('ventas_has_detallesEquipos',array('idVenta'=>$equipo));
        foreach ($resultadoequipos->result() as $item) {
            $cantidad=$item->cantidad;
            $idEquipo=$item->idEquipo;
            $bodega=$this->ModeloCatalogos->obtenerbodega($item->serie_bodega);
            $idbodega=$item->serie_bodega;
            $modelo=$item->modelo;

            //=====================================================
            $stock=$this->Clientes_model->getexistenciaequipos($idEquipo,$idbodega);
            if($stock<$cantidad){
                $verifequipos[]=array('modelo'=>$modelo,'bodega'=>$bodega);
            }

        }
        $resultadoaccesorios = $this->ModeloCatalogos->accesoriosventa($equipo);
        foreach ($resultadoaccesorios->result() as $item) {
            $cantidad=$item->cantidad;
            $id_accesorio=$item->id_accesorio;
            $bodega=$item->bodega;
            $idbodega=$item->serie_bodega;
            $modelo=$item->nombre;
            //==========================================
            $stock=$this->ModeloGeneral->stockaccesoriobodega($id_accesorio,$idbodega);
            if($stock<$cantidad){
                $verifequipos[]=array('modelo'=>$modelo,'bodega'=>$bodega);
            }

        }
        $resultadoconsumibles = $this->ModeloCatalogos->consumiblesventa($equipo);
        foreach ($resultadoconsumibles->result() as $item) {
            $cantidad=$item->cantidad;
            $id_consumibles=$item->id_consumibles;
            $idbodega=$item->serie_bodega;
            $modelo=$item->modelo;
            
            //==========================================
            $stock=$this->ModeloGeneral->stockconsumiblesbodega($id_consumibles,$idbodega);
            if($stock<$cantidad){
                $verifequipos[]=array('modelo'=>$modelo,'bodega'=>$bodega);
            }
        }
        $consumiblesventadetalles = $this->ModeloCatalogos->consumiblesventadetalles($consumibles);
        foreach ($consumiblesventadetalles->result() as $item) {
            $cantidad=$item->cantidad;
            $id_consumibles=$item->idConsumible;
            $bodega=$item->bodega;
            $idbodega=$item->bodegas;
            $modelo=$item->modelo;
            //==========================================
            $stock=$this->ModeloGeneral->stockconsumiblesbodega($id_consumibles,$idbodega);
            if($stock<$cantidad){
                $verifequipos[]=array('modelo'=>$modelo,'bodega'=>$bodega);
            }
        }
        $ventadetallesrefacion = $this->ModeloCatalogos->ventas_has_detallesRefacciones($refacciones);
        foreach ($ventadetallesrefacion->result() as $item) {
            $cantidad=$item->cantidad;
            $idRefaccion=$item->idRefaccion;
            $modelo=$item->modelo;
            $bodega=$item->bodega;
            $idbodega=$item->serie_bodega;
            
            //==========================================
            $stock=$this->ModeloGeneral->stockrefaccionesbodega($idRefaccion,$idbodega);
            if($stock<$cantidad){
                $verifequipos[]=array('modelo'=>$modelo,'bodega'=>$bodega);
            }
        }
            /*
            $resultadopoliza = $this->ModeloCatalogos->db6_getselectwheren('polizasCreadas',array('id'=>$poliza));
            $resultadopoliza=$resultadopoliza->row();
            $polizaventadetalles = $this->ModeloCatalogos->polizaventadetalles3($poliza);
            */

        
        //===========================================================================
        if(count($verifequipos)==0){
            $resultventa = $this->ModeloCatalogos->getselectwheren('ventas',array('id'=>$id));
            $resultventa = $resultventa->row();
            unset($resultventa->id);
            $resultventa->reg=$this->fechaactual;
            $resultventa->prefactura=0;
            $resultventa->id_personal=$this->idpersonal;
            $resultventa->e_entrega=0;
            $resultventa->e_entrega_p=null;
            $idCliente=$resultventa->idCliente;
            $idVenta_new=$this->ModeloCatalogos->Insert('ventas',$resultventa);

            $resultadoequipos = $this->ModeloCatalogos->getselectwheren('ventas_has_detallesEquipos',array('idVenta'=>$equipo));
            foreach ($resultadoequipos->result() as $item) {
                    unset($item->id);
                    unset($item->idVenta);
                    $item->idVenta=$idVenta_new;
                    $item->serie_estatus=0;
                    $insert_eq[]=$item;
            }
            $resultadoaccesorios = $this->ModeloCatalogos->getselectwheren('ventas_has_detallesEquipos_accesorios',array('id_venta'=>$equipo));
            foreach ($resultadoaccesorios->result() as $item) {
                    unset($item->id_accesoriod);
                    unset($item->id_venta);
                    $item->id_venta=$idVenta_new;
                    $item->serie_estatus=0;
                    $insert_acc[]=$item;

                    
            }
            $resultadoconsumibles = $this->ModeloCatalogos->getselectwheren('ventas_has_detallesequipos_consumible',array('idventa'=>$equipo));
            foreach ($resultadoconsumibles->result() as $item) {
                    unset($item->id);
                    unset($item->idventa);
                    $item->idventa=$idVenta_new;
                    $insert_cone[]=$item;

                    $this->ModeloCatalogos->updatestock2('consumibles_bodegas','total','-',$item->cantidad,'consumiblesId',$item->id_consumibles,'bodegaId',$item->bodega);

                    //1 equipo, 2 acesorio, 3 refaccion,4 consumible    
                    $this->ModeloCatalogos->Insert('bitacora_movimientos',array('id_item'=>0,'cantidad'=>$item->cantidad,'modeloId'=>$item->id_consumibles,'tipo'=>4,'entrada_salida'=>0,'personal'=>$this->idpersonal,'descripcion_evento'=>'salida para venta '.$idVenta_new,'serieId'=>0,'clienteId'=>$idCliente,'bodega_o'=>$item->bodega));
            }
            $consumiblesventadetalles = $this->ModeloCatalogos->getselectwheren('ventas_has_detallesConsumibles',array('idVentas'=>$consumibles));
            foreach ($consumiblesventadetalles->result() as $item) {
                    unset($item->id);
                    unset($item->idVentas);
                    $item->idVentas=$idVenta_new;
                    //============================
                        $foliotext=$item->foliotext;
                        //======================================================
                            $result_cf = $this->ModeloCatalogos->getselectwheren('contrato_folio',array('foliotext'=>$foliotext));
                            $idrelacion=0;
                            foreach ($result_cf->result() as $item_cf) {
                                $idrelacion=$item_cf->idrelacion;
                            }
                        //======================================================

                        $ft_count=strlen($foliotext);
                        if($ft_count>=7 and $ft_count<=9){
                            $folio = explode("T", $foliotext);
                            $idrenta = $folio[0];
                            if(intval($idrenta)>0){

                                $contr_folio = $this->ModeloCatalogos->contratofolio($idCliente);
                                foreach ($contr_folio as $itemc) {
                                    $aux = $itemc->folio;
                                }
                                $folio = $aux +1; 
                                /// $idretashdc = $item->id;
                                $foliotext = $idrenta.'T'.str_pad($folio, 3, "0", STR_PAD_LEFT);

                                $data_folio = array('idrenta'=>$idrenta,'idCliente'=>$idCliente,'idconsumible'=>$item->idConsumible,'folio'=>$folio,'foliotext'=>$foliotext,'idrelacion'=>$idrelacion,'creado_personal'=>$this->idpersonal);
                                $this->ModeloCatalogos->Insert('contrato_folio',$data_folio);
                                $item->comentario = $foliotext;
                                $item->foliotext = $foliotext;
                            }

                        }
                    //============================
                    $insert_con[]=$item;

                    $this->ModeloCatalogos->updatestock2('consumibles_bodegas','total','-',$item->piezas,'consumiblesId',$item->idConsumible,'bodegaId',$item->bodegas);

                    //1 equipo, 2 acesorio, 3 refaccion,4 consumible    
                    $this->ModeloCatalogos->Insert('bitacora_movimientos',array('id_item'=>0,'cantidad'=>$item->piezas,'modeloId'=>$item->idConsumible,'tipo'=>4,'entrada_salida'=>0,'personal'=>$this->idpersonal,'descripcion_evento'=>'salida para venta '.$idVenta_new,'serieId'=>0,'clienteId'=>$idCliente,'bodega_o'=>$item->bodegas));
                
            }
            $ventadetallesrefacion = $this->ModeloCatalogos->getselectwheren('ventas_has_detallesRefacciones',array('idVentas'=>$refacciones));
            foreach ($ventadetallesrefacion->result() as $item) {
                
                    unset($item->id);
                    unset($item->idVentas);
                    $item->idVentas=$idVenta_new;
                    $item->serie_estatus=0;
                    $insert_ref[]=$item;
            }
            if(count($insert_eq)>0){
                $this->ModeloCatalogos->insert_batch('ventas_has_detallesEquipos',$insert_eq);
            }
            if(count($insert_acc)>0){
                $this->ModeloCatalogos->insert_batch('ventas_has_detallesEquipos_accesorios',$insert_acc);
            }
            if(count($insert_cone)>0){
                $this->ModeloCatalogos->insert_batch('ventas_has_detallesequipos_consumible',$insert_cone);
            }
            if(count($insert_con)>0){
                $this->ModeloCatalogos->insert_batch('ventas_has_detallesConsumibles',$insert_con);
            }
            if(count($insert_ref)>0){
                $this->ModeloCatalogos->insert_batch('ventas_has_detallesRefacciones',$insert_ref);
            }
            $resultpoliza = $this->ModeloCatalogos->getselectwheren('polizasCreadas',array('id'=>$poliza));
            if($resultpoliza->num_rows()>0){
                $resultpoliza=$resultpoliza->row();
                unset($resultpoliza->id);
                $idpoliza_new=$this->ModeloCatalogos->Insert('polizasCreadas',$resultpoliza);

                $resultpolizadll = $this->ModeloCatalogos->getselectwheren('polizasCreadas_has_detallesPoliza',array('idPoliza'=>$poliza));
                foreach ($resultpolizadll->result() as $item) {
                
                    unset($item->id);
                    unset($item->idPoliza);
                    $item->idPoliza=$idpoliza_new;
                    $insert_pol[]=$item;
                }

                if(count($insert_pol)>0){
                    $this->ModeloCatalogos->insert_batch('polizasCreadas_has_detallesPoliza',$insert_pol);
                }


                $datavc=array('equipo'=>$idVenta_new,'consumibles'=>$idVenta_new,'refacciones'=>$idVenta_new,'poliza'=>$idpoliza_new,'telefono'=>$resultventa->telefono,'correo'=>$resultventa->correo,'idCliente'=>$resultventa->idCliente,'id_personal'=>$this->idpersonal);
                        $this->ModeloCatalogos->Insert('ventacombinada',$datavc);
                        $this->ModeloCatalogos->updateCatalogo('ventas',array('combinada'=>1),array('id'=>$idVenta_new));

            }




        }

        $datos = array('equipos'=>$verifequipos);
        echo json_encode($datos);
    }

    function nuevaventageneral(){
        $data = $this->input->post();
        $idCotizacion=0;
        $idcombinada=0;
        $eleccionCotizar = 1;
        $idventa=0;
        $idpoliza=0;
        $clienteId=0;
        $idventaaddnew =$data['idventaaddnew'];
        if(isset($data['tipoprecioequipo'])){
            $tpeq=$data['tipoprecioequipo'];
        }
        
        unset($data['tipoprecioequipo']);
        unset($data['eleccionCotizar']);
        if(isset($data['accesorios'])){
            $accesorios = $data['accesorios'];
            unset($data['accesorios']);
        }
        
        if(isset($data['consumibles'])){
            $consumibles = $data['consumibles'];
            unset($data['consumibles']);
        }
        if(isset($data['viewcont'])){
            $viewcont=$data['viewcont'];
        }else{
            $viewcont=0;
        }
        if(isset($data['orden_comp'])){
            $orden_comp=$data['orden_comp'];
            unset($data['orden_comp']);
        }else{
            $orden_comp='';
        }
        //===============================================
                $tipoCotizacion = 1;
                unset($data['tipo']);
                //$data['rentaVentaPoliza'] = $tipoCotizacion;
                // Saber si es cotización MANUAL o por FAMILIA
                //$tipoCotizacionEquipos = $data["cotizar"];
                unset($data['cotizar']);
                // Agisnamos lo que se va a cotizar
                $data['tipo'] = $eleccionCotizar;
                // Se inserta el registro principal
                //==================================================
                    $estatusventa=0;
                    $estatuspoliza=0;
                    if(isset($data["equiposIndividuales"])){
                        $estatusventa=1;
                    }
                    if(isset($data['arrayEquipos'])){
                        $estatusventa=1;
                    }
                    if(isset($data['arrayRefaccion'])){
                        $estatusventa=1;
                    }
                    if(isset($data['arrayac'])){
                        $estatusventa=1;
                    }
                    if(isset($data['arrayPolizas'])){
                        $estatuspoliza=1;
                        $arrayPolizas_count = $data['arrayPolizas'];
                        $DATA_poliza_count = json_decode($arrayPolizas_count);
                        if(count($DATA_poliza_count)==0){
                            $estatuspoliza=0;
                        }
                    }

                //==================================================
                if ($estatusventa==1) {
                    $clienteId=$data['idCliente'];
                    $datosCotizacion = array('atencion' => $data['atencion'],
                                        'correo' => $data['correo'],
                                        'telefono' => $data['telefono'],
                                        'idCliente' => $data['idCliente'],
                                        'tipo' => $data['tipo'],
                                        //'rentaVentaPoliza' => $tipoCotizacion,
                                        'id_personal'=>$this->idpersonal,
                                        'estatus' => 1,
                                        'reg'=>$this->fechaactual,
                                        'combinada' => 0,
                                        'tipov' => $data['tipov'],
                                        'v_ser_tipo' => $data['v_ser_tipo'],
                                        'v_ser_id' => $data['v_ser_id'],
                                        'orden_comp'=>$orden_comp

                                    );
                    // Se inserta la cotización principal
                    if($idventaaddnew>0){
                        $idventa = $idventaaddnew;
                    }else{
                        $idventa = $this->ModeloCatalogos->Insert('ventas',$datosCotizacion);
                    }
                    
                }
                if($estatuspoliza==1){
                    if($idventaaddnew>0){
                        $estatusventa=1;
                        $idventa = $idventaaddnew;
                    }
                    $datosCotizacionp = array('atencion' => $data['atencion'],
                                            'correo' => $data['correo'],
                                            'telefono' => $data['telefono'],
                                            'idCliente' => $data['idCliente'],
                                            'tipo' => 4,
                                            //'rentaVentaPoliza' => 3,
                                            'id_personal' => $this->idpersonal,
                                            'estatus' => 1,
                                            'combinada' => 0,
                                            'tipov' => $data['tipov'],
                                            'orden_comp'=>$orden_comp
                                            );

                    $idpoliza = $this->ModeloCatalogos->Insert('polizasCreadas',$datosCotizacionp);
                }
                if($estatuspoliza==1 and $estatusventa==1){
                    $clienteId=$data['idCliente'];
                    $datavc=array(
                        'equipo'=>$idventa,
                        'consumibles'=>$idventa,
                        'refacciones'=>$idventa,
                        'poliza'=>$idpoliza,
                        'telefono'=>$data['telefono'],
                        'correo'=>$data['correo'],
                        'idCliente'=>$data['idCliente'],
                        'tipov'=>$data['tipov'],
                        'id_personal'=>$this->idpersonal,
                        'v_ser_tipo' => $data['v_ser_tipo'],
                        'v_ser_id' => $data['v_ser_id'],
                        'orden_comp'=>$orden_comp
                    );
                    $idcombinada=$this->ModeloCatalogos->Insert('ventacombinada',$datavc);
                    $this->ModeloCatalogos->updateCatalogo('ventas',array('combinada'=>1),array('id'=>$idventa));
                    $this->ModeloCatalogos->updateCatalogo('polizasCreadas',array('combinada'=>1),array('id'=>$idpoliza));
                }
                
                if(isset($data["equiposIndividuales"])){
                    $equiposIndividuales=$data["equiposIndividuales"];
                    $DATAeq = json_decode($equiposIndividuales);
                    //foreach ($arregloEquipos as $equipoInd) {
                    $dataarrayinsert_eq_r=array();
                    for ($j=0;$j<count($DATAeq);$j++){
                        $equipoInd=$DATAeq[$j]->equipos;
                        $cantidadeq=$DATAeq[$j]->cantidad;
                        $equipo = $this->Equipos_model->getEquipoPorId($equipoInd);
                        // Dependiendo si es VENTA, RENTA o PÓLIZA, es el precio que se va a tomar
                        if($tipoCotizacion==1){
                            if ($tpeq==1) {
                                $precio = $equipo->costo_venta;
                            }elseif ($tpeq==2) {
                                $precio = $equipo->costo_poliza;
                            }else{
                                $precio = $equipo->costo_renta;
                            }
                            
                        }
                        else if ($tipoCotizacion==2){
                            $precio = $equipo->costo_renta;
                        }
                        else if ($tipoCotizacion==3){
                            $precio = $equipo->costo_poliza;
                        }
                        if ($tpeq==3) {
                                $precio = 0;
                        }
                        //========================================
                        //========================================
                            $tipo = 1;
                            $costo_toner = 0;
                            $rendimiento_toner = 0;
                            $costo_unidad_imagen = 0;
                            $rendimiento_unidad_imagen = 0;
                            $costo_garantia_servicio = 0;
                            $rendimiento_garantia_servicio = 0;
                            $costo_pagina_monocromatica = 0;
                            $costo_pagina_color = 0;
                        //========================================
                            $DATAc = json_decode($consumibles);
                            for ($i=0;$i<count($DATAc);$i++) {
                                if ($equipo->id==$DATAc[$i]->equiposelected) {
                                    $idconsumuble=$DATAc[$i]->consumibleselected;
                                    //log_message('error', 'idconsumible '.$idconsumuble);
                                    $resultadoaddcon=$this->ModeloCatalogos->consumibleagregadoselected($idconsumuble,$equipo->id);
                                    foreach ($resultadoaddcon->result() as $item2) {
                                        $tipo = $item2->tipo;
                                        $costo_toner = $item2->poliza;
                                        $rendimiento_toner = $item2->rendimiento_toner;
                                        $costo_unidad_imagen = $item2->costo_unidad_imagen;
                                        $rendimiento_unidad_imagen = $item2->rendimiento_unidad_imagen;
                                        $costo_garantia_servicio = $item2->costo_garantia_servicio;
                                        $rendimiento_garantia_servicio = $item2->rendimiento_garantia_servicio;
                                        $costo_pagina_monocromatica = $item2->costo_pagina_monocromatica;
                                        $costo_pagina_color = $item2->costo_pagina_color;


                                    }
                                    $datachfc["idventa"]=$idventa;
                                        $datachfc["idequipo"]=$DATAc[$i]->equiposelected;
                                        $datachfc["cantidad"]=$cantidadeq;
                                        $datachfc["id_consumibles"]=$idconsumuble;
                                        $datachfc["costo_toner"]=$costo_toner;
                                        $datachfc["rendimiento_toner"]=$rendimiento_toner;
                                        $datachfc["rendimiento_unidad_imagen"]=$rendimiento_unidad_imagen;
                                        if ($tipoCotizacion==1) {
                                            $this->ModeloCatalogos->Insert('ventas_has_detallesequipos_consumible',$datachfc);
                                            
                                        }elseif ($tipoCotizacion==2) {
                                            unset($datachfc["idventa"]);
                                            $datachfc["idrentas"]=$idCotizacion;
                                            $this->ModeloCatalogos->Insert('rentas_has_detallesequipos_consumible',$datachfc);

                                        }
                                }
                            }
                        //=========================================================
                            $DATAa = json_decode($accesorios);
                            for ($l=0;$l<count($DATAa);$l++) {
                                if ($equipo->id==$DATAa[$l]->equiposelected) {
                                    if ($tipoCotizacion==1) {
                                        $dataas['id_venta']=$idventa;
                                        $dataas['id_equipo']=$DATAa[$l]->equiposelected;
                                        $dataas['id_accesorio']=$DATAa[$l]->accesorioselected;
                                        $dataas['cantidad']=$cantidadeq;
                                        $dataas['costo']=$this->ModeloCatalogos->costoaccesorio($DATAa[$l]->accesorioselected);
                                        $this->ModeloCatalogos->Insert('ventas_has_detallesEquipos_accesorios',$dataas);
                                    }elseif ($tipoCotizacion==2) {
                                        //log_message('error', 'idrentas Accesorio: '.$idCotizacion);
                                        $dataas["idrentas"]=$idCotizacion;
                                        $dataas['id_equipo']=$DATAa[$l]->equiposelected;
                                        $dataas['id_accesorio']=$DATAa[$l]->accesorioselected;
                                        $dataas['cantidad']=$cantidadeq;
                                        $this->ModeloCatalogos->Insert('rentas_has_detallesEquipos_accesorios',$dataas);
                                    }
                                }
                            }   
                        // Se arma el array para insertar los datos
                        if ($tipoCotizacion==1) {
                            $detallesCotizacion = array('idVenta' => $idventa,
                                                        'idEquipo' => $equipo->id,
                                                        'cantidad'=>$cantidadeq,
                                                        'modelo' => $equipo->modelo,
                                                        'especificaciones_tecnicas' => $equipo->especificaciones_tecnicas,
                                                        'precio' => $precio,
                                                        'costo_unidad_imagen' => $costo_unidad_imagen,
                                                        'rendimiento_unidad_imagen' => $rendimiento_unidad_imagen,
                                                        'costo_garantia_servicio' => $costo_garantia_servicio,
                                                        'rendimiento_garantia_servicio' => $rendimiento_garantia_servicio,
                                                        'costo_pagina_monocromatica' => $costo_pagina_monocromatica,
                                                        'costo_pagina_color' => $costo_pagina_color,
                                                        'excedente' => $equipo->excedente,
                                                        'tipotoner' => $tipo,
                                                        'costo_toner' => $costo_toner,
                                                        'rendimiento_toner' => $rendimiento_toner,           
                                            );
                            // Se insertan en la BD
                            $this->ModeloCatalogos->Insert('ventas_has_detallesEquipos',$detallesCotizacion);
                        }elseif ($tipoCotizacion==2) {
                            $detallesCotizacion = array('idRenta' => $idventa,
                                                        'idEquipo' => $equipo->id,
                                                        'cantidad'=>$cantidadeq,
                                                        'modelo' => $equipo->modelo,
                                                        'especificaciones_tecnicas' => $equipo->especificaciones_tecnicas,
                                                        'precio' => $precio,
                                                        'costo_unidad_imagen' => $costo_unidad_imagen,
                                                        'rendimiento_unidad_imagen' => $rendimiento_unidad_imagen,
                                                        'costo_garantia_servicio' => $costo_garantia_servicio,
                                                        'rendimiento_garantia_servicio' => $rendimiento_garantia_servicio,
                                                        'costo_pagina_monocromatica' => $costo_pagina_monocromatica,
                                                        'costo_pagina_color' => $costo_pagina_color,
                                                        'excedente' => $equipo->excedente,
                                                        'tipotoner' => $tipo,
                                                        'costo_toner' => $costo_toner,
                                                        'rendimiento_toner' => $rendimiento_toner,           
                                            );
                            // Se insertan en la BD
                            $dataarrayinsert_eq_r[]=$detallesCotizacion;
                            //$this->ModeloCatalogos->Insert('rentas_has_detallesEquipos',$detallesCotizacion);
                        }
                        //$this->Cotizaciones_model->insertarDetallesCotizacionEquipos($detallesCotizacion);
                        
                    }
                    if(count($dataarrayinsert_eq_r)>0){
                        $this->ModeloCatalogos->insert_batch('rentas_has_detallesEquipos',$dataarrayinsert_eq_r);
                    }
                }
                if(isset($data['arrayEquipos'])){
                    $equiposSeleccionados = $data['arrayEquipos'];
                    unset($data['arrayEquipos']);

                    $tonersSeleccionados = $data['arrayToner'];
                    unset($data['arrayToner']);

                    $piezasSeleccionadas = $data['arrayPiezas'];
                    unset($data['arrayPiezas']);

                    $bodegasc = $data['bodegasc'];
                    unset($data['bodegasc']);

                    $presiosSeleccionadas = $data['arrayPrecio'];
                    unset($data['arrayPrecio']);

                    unset($data['equipoListaConsumibles']);
                    unset($data['toner']);

                    $data['tipo'] = 2;

                    $equiposArreglo = explode(',', $equiposSeleccionados);
                    $tonersArreglo = explode(',', $tonersSeleccionados);
                    $piezasArreglo = explode(',', $piezasSeleccionadas);
                    $precioArreglo = explode(',', $presiosSeleccionadas);
                    $bodegascArreglo = explode(',', $bodegasc);

                    $dataconsumiblesarray=array();
                    for ($i=0; $i < count($equiposArreglo); $i++){
                        $bodegaselect=$bodegascArreglo[$i];
                        if($tonersArreglo[$i]>0){
                            $datosConsumible = $this->Consumibles_model->getConsumibleConPrecios($tonersArreglo[$i]);
                            $total=$precioArreglo[$i]*$piezasArreglo[$i];
                            $detallesCotizacion = array('idVentas' => $idventa,
                                                    'idEquipo' => $equiposArreglo[$i],
                                                    'idConsumible' => $datosConsumible[0]->id,
                                                    'piezas' => $piezasArreglo[$i], 
                                                    'modelo' => $datosConsumible[0]->modelo,
                                                    'rendimiento' =>'',
                                                    'descripcion' => $datosConsumible[0]->observaciones,
                                                    'precioGeneral' => $precioArreglo[$i],
                                                    'totalGeneral' => $total,
                                                    'bodegas' => $bodegaselect,
                                                );
                            $this->ModeloCatalogos->updatestock2('consumibles_bodegas','total','-',$piezasArreglo[$i],'consumiblesId',$datosConsumible[0]->id,'bodegaId',$bodegaselect);
                            $this->ModeloCatalogos->Insert('bitacora_movimientos',array('id_item'=>$datosConsumible[0]->id,'modeloId'=>$datosConsumible[0]->id,'modelo'=>$datosConsumible[0]->modelo,'tipo'=>4,'entrada_salida'=>0,'personal'=>$this->idpersonal,'descripcion_evento'=>'Salida para venta '.$idventa.'','bodega_o'=>$bodegaselect,'clienteId'=>$clienteId,'cantidad'=>$piezasArreglo[$i]));
                            $dataconsumiblesarray[]=$detallesCotizacion;
                            //$this->ModeloGeneral->getinsert($detallesCotizacion,'ventas_has_detallesConsumibles');
                            //$this->Cotizaciones_model->insertarDetallesCotizacionConsumibles($detallesCotizacion);
                        }
                    }
                    if(count($dataconsumiblesarray)>0){
                        $this->ModeloCatalogos->insert_batch('ventas_has_detallesConsumibles',$dataconsumiblesarray);
                    }

                }
                if(isset($data['arrayRefaccion'])){
                    $arrayRefaccion=$data['arrayRefaccion'];
                    $datarefaccionesarray=array();
                    $DATAc = json_decode($arrayRefaccion);       
                    for ($i=0;$i<count($DATAc);$i++) {
                        // insertar 
                        if ($DATAc[$i]->toner>0) {
                            
                            $datosConsumible = $this->Consumibles_model->getrefaccionesConPrecios($DATAc[$i]->toner);
                                $total=floatval($DATAc[$i]->tonerp)*floatval($DATAc[$i]->piezas);
                                $detallesCotizacion = array('idVentas' => $idventa,
                                                        'idEquipo' => $DATAc[$i]->equipo,
                                                        'serie' => $DATAc[$i]->serie,
                                                        'idRefaccion' => $datosConsumible[0]->id,
                                                        'piezas' => $DATAc[$i]->piezas, 
                                                        'modelo' => $datosConsumible[0]->nombre,
                                                        'rendimiento' =>'',
                                                        'descripcion' => $datosConsumible[0]->observaciones,
                                                        'precioGeneral' => $DATAc[$i]->tonerp,
                                                        'totalGeneral' => $total,
                                                        'serie_bodega' => $DATAc[$i]->bodega
                                                    );
                                $datarefaccionesarray[] =$detallesCotizacion;
                                
                        }
                    }
                    if(count($datarefaccionesarray)>0){
                        $this->ModeloCatalogos->insert_batch('ventas_has_detallesRefacciones',$datarefaccionesarray);
                    }
                }
                if(isset($data['arrayac'])){
                    $arrayac=$data['arrayac'];
                    $datareaccesorioarray=array();
                    $DATAc = json_decode($arrayac);       
                    for ($i=0;$i<count($DATAc);$i++) {
                        
                            
                            
                                $detallesCotizacion = array(
                                                        'id_venta'=>$idventa,
                                                        'id_equipo'=>$DATAc[$i]->equipo,
                                                        'id_accesorio'=>$DATAc[$i]->accesorio,
                                                        'serie_bodega'=>$DATAc[$i]->bodega,
                                                        'costo'=>$DATAc[$i]->costo,
                                                        'cantidad'=>$DATAc[$i]->cantidad
                                                    );
                                $datareaccesorioarray[] =$detallesCotizacion;
                                
                        
                    }
                    if(count($datareaccesorioarray)>0){
                        $this->ModeloCatalogos->insert_batch('ventas_has_detallesEquipos_accesorios',$datareaccesorioarray);
                    }
                }
                if(isset($data['arrayPolizas'])){
                    $vigencia=0;
                    $arrayPolizas = $data['arrayPolizas'];
                    $dataeqarrayinsert=array();
                    unset($data['arrayPolizas']);
                    $DATAc = json_decode($arrayPolizas);
                    for ($i=0;$i<count($DATAc);$i++) {
                        $datosPoliza = $this->Polizas_model->getPolizaConUnDetalle($DATAc[$i]->poliza,$DATAc[$i]->polizad);
                        $precio_local=null;
                        $precio_semi=null;
                        $precio_foraneo=null;
                        $precio_especial=0;
                        
                        if($datosPoliza[0]->vigencia_clicks=='Ilimitadas'){
                            $vigencia=100;
                        }else{
                            if(intval($datosPoliza[0]->vigencia_clicks)>0){
                                $vigencia=intval($datosPoliza[0]->vigencia_clicks); 
                            }     
                        }
                        //log_message('error', 'costo poliza: '.$DATAc[$i]->costo);
                        if ($DATAc[$i]->costo=='local') {
                                    $precio_local=$datosPoliza[0]->precio_local;
                                    $precio_semi=null;
                                    $precio_foraneo=null;
                                    $precio_especial=null;
                        }elseif ($DATAc[$i]->costo=='foraneo') {
                        //}elseif ($polizasArregloDetallescosto[$i]=='foraneo') {
                                    $precio_local=null;
                                    $precio_semi=null;
                                    $precio_especial=null;
                                    $precio_foraneo=$datosPoliza[0]->precio_foraneo;
                        }elseif($DATAc[$i]->costo=='semi'){
                                    $precio_local=null;
                                    $precio_semi=$datosPoliza[0]->precio_semi;
                                    $precio_foraneo=null;
                                    $precio_especial=null;
                        }elseif($DATAc[$i]->costo=='especial'){
                                    $precio_local=null;
                                    $precio_semi=null;
                                    $precio_foraneo=null;
                                    $precio_especial=$datosPoliza[0]->precio_especial;
                        }elseif($DATAc[$i]->costo>0){
                            $precio_local=null;
                            $precio_semi=null;
                            $precio_foraneo=null;
                            $precio_especial=$DATAc[$i]->costo;
                        }

                        $detallesCotizacion = array('idPoliza' => $idpoliza,
                                                        'idEquipo' => $DATAc[$i]->equipo,
                                                        'serie'=> $DATAc[$i]->serie,
                                                        'idCotizacion' => 0,
                                                        'nombre' => $datosPoliza[0]->nombre, 
                                                        'cubre' => $datosPoliza[0]->cubre,
                                                        'vigencia_meses' => $datosPoliza[0]->vigencia_meses,
                                                        'vigencia_clicks' => $datosPoliza[0]->vigencia_clicks,
                                                        'modelo' => $datosPoliza[0]->modelo,
                                                        'precio_local' => $precio_local,
                                                        'precio_semi' => $precio_semi,
                                                        'precio_foraneo' => $precio_foraneo,
                                                        'precio_especial' => $precio_especial,
                                                        'comentario' => $DATAc[$i]->motivo,

                                                    );
                        $dataeqarrayinsert[]=$detallesCotizacion;
                        //$this->Cotizaciones_model->insertarDetallesCotizacionPoliza($detallesCotizacion);
                        //$this->ModeloCatalogos->Insert('polizasCreadas_has_detallesPoliza',$detallesCotizacion);
                    }
                    if(count($DATAc)>0){
                        $this->ModeloCatalogos->insert_batch('polizasCreadas_has_detallesPoliza',$dataeqarrayinsert);
                    }
                    if(intval($vigencia)>1){
                        $this->ModeloCatalogos->updateCatalogo('polizasCreadas',array('viewcont'=>1,'viewcont_cant'=>$vigencia),array('id'=>$idpoliza));
                    }
                }

                

                    
                
                $json_data = array("idventa" => $idventa,'idpoliza'=>$idpoliza,'combinada'=>$idcombinada);
                echo json_encode($json_data);
                //==================================================================
    }
    function additemventa($id,$tipo){
        $data['id']=$id;
        $data['tipo']=$tipo;
        $this->load->view('header');
        $this->load->view('ventas/additemventa',$data);
        
        $this->load->view('footer'); 
    }
    function garantia(){
        $params = $this->input->post();
        $id = $params['id'];
        $combinada = $params['combinada'];
        $garantia = $params['garantia'];
        if($combinada==1){
            $this->ModeloCatalogos->updateCatalogo('ventacombinada',array('garantia'=>$garantia),array('combinadaId'=>$id));
        }
        if($combinada==0){
            $this->ModeloCatalogos->updateCatalogo('ventas',array('garantia'=>$garantia),array('id'=>$id));
        }
    }
    function exportgarantias(){
        $params = $this->input->get();

        $params['perfilid']=$this->perfilid;
        $params['filf']=0;
        $subconsulta = $this->Ventas_model->sqlgarantias($params);

        $this->db->select('*');
        $this->db->from($subconsulta);
        $query=$this->db->get();

        header("Pragma: public");
        header("Expires: 0");
        $filename = "Garantias.xls";
        header("Content-type: application/x-msdownload");
        header("Content-Disposition: attachment; filename=$filename");
        header("Pragma: no-cache");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");

        $html='<table id="tabla_ventas_incompletas" class="table display" cellspacing="0" border="1">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>Cliente</th>
                            <th>Vendedor</th>
                            <th>Estatus</th>
                            <th>Fecha de<br>Creación</th>
                            <th>Fecha de<br>vencimiento</th>
                            <th>Fecha de<br>Entrega</th>
                            <th>Empresa</th>
                            <th>Factura</th>
                            <th>Cantidad</th>
                            <th>Producto</th>
                          </tr>
                        </thead>
                        <tbody>';
                        foreach ($query->result() as $item) {
                            $idrow='';
                            if ($item->combinada==1) {
                                if($item->equipo>0){
                                    $idrow=$item->equipo;
                                }else if($item->consumibles>0){
                                    $idrow=$item->consumibles;
                                }else if($item->refacciones>0){
                                    $idrow=$item->refacciones;
                                }
                            }else{
                                $idrow=$item->id;
                            }
                            $estatus=''; 
                            if($item->estatus==1){
                                $estatus='Pendiente';    
                            } 
                            if($item->estatus==3){
                                $estatus='Pagado';    
                            }
                            $empresa='';
                            if ($item->tipov==1) {
                                $empresa='Alta Productividad';    
                            }
                            if ($item->tipov==2) {
                                $empresa='D-Impresión';    
                            }  
                            $html.='<tr><td>'.$idrow.'</td><td>'.$item->empresa.'</td><td>'.$item->vendedor.'</td><td>'.$estatus.'</td><td>'.$item->reg.'</td><td>'.$item->vencimiento.'</td><td>'.$item->fechaentrega.'</td><td>'.$empresa.'</td><td>'.$item->facturas.'</td><td></td><td></td></tr>';

                            if($item->combinada==0){
                                $resultadoequipos = $this->ModeloCatalogos->db6_getselectwheren('ventas_has_detallesEquipos',array('idVenta'=>$item->id));
                                $resultadoaccesorios = $this->ModeloCatalogos->accesoriosventa($item->id);
                                $resultadoconsumibles = $this->ModeloCatalogos->consumiblesventa($item->id);
                                $consumiblesventadetalles = $this->ModeloCatalogos->consumiblesventadetalles($item->id);
                                $ventadetallesrefacion = $this->ModeloCatalogos->ventas_has_detallesRefacciones($item->id);

                                foreach ($resultadoequipos->result() as $iteme) {
                                    $html.='<tr><td colspan="9"></td><td>'.$iteme->cantidad.'</td><td>'.$iteme->modelo.'</td></tr>';
                                }
                                foreach ($resultadoaccesorios->result() as $itema) {
                                    $html.='<tr><td colspan="9"></td><td>'.$itema->cantidad.'</td><td>'.$itema->nombre.'</td></tr>';
                                }
                                foreach ($resultadoconsumibles->result() as $itemc) {
                                    $html.='<tr><td colspan="9"></td><td>'.$itemc->cantidad.'</td><td>'.$itemc->modelo.'</td></tr>';
                                }
                                foreach ($consumiblesventadetalles->result() as $itemcc) {
                                    $html.='<tr><td colspan="9"></td><td>'.$itemcc->cantidad.'</td><td>'.$itemcc->modelo.'</td></tr>';
                                }
                                foreach ($ventadetallesrefacion->result() as $itemr) {
                                    $html.='<tr><td colspan="9"></td><td>'.$itemr->cantidad.'</td><td>'.$itemr->modelo.'</td></tr>';
                                }
                            }
                            if($item->combinada==1){
                                $resultadoequipos = $this->ModeloCatalogos->db6_getselectwheren('ventas_has_detallesEquipos',array('idVenta'=>$item->equipo));
                                $resultadoaccesorios = $this->ModeloCatalogos->accesoriosventa($item->equipo);
                                $resultadoconsumibles = $this->ModeloCatalogos->consumiblesventa($item->equipo);
                                $consumiblesventadetalles = $this->ModeloCatalogos->consumiblesventadetalles($item->consumibles);
                                $ventadetallesrefacion = $this->ModeloCatalogos->ventas_has_detallesRefacciones($item->refacciones);

                                foreach ($resultadoequipos->result() as $iteme) {
                                    $html.='<tr><td colspan="9"></td><td>'.$iteme->cantidad.'</td><td>'.$iteme->modelo.'</td></tr>';
                                }
                                foreach ($resultadoaccesorios->result() as $itema) {
                                    $html.='<tr><td colspan="9"></td><td>'.$itema->cantidad.'</td><td>'.$itema->nombre.'</td></tr>';
                                }
                                foreach ($resultadoconsumibles->result() as $itemc) {
                                    $html.='<tr><td colspan="9"></td><td>'.$itemc->cantidad.'</td><td>'.$itemc->modelo.'</td></tr>';
                                }
                                foreach ($consumiblesventadetalles->result() as $itemcc) {
                                    $html.='<tr><td colspan="9"></td><td>'.$itemcc->cantidad.'</td><td>'.$itemcc->modelo.'</td></tr>';
                                }
                                foreach ($ventadetallesrefacion->result() as $itemr) {
                                    $html.='<tr><td colspan="9"></td><td>'.$itemr->cantidad.'</td><td>'.$itemr->modelo.'</td></tr>';
                                }
                            }



                            
                        }
                $html.='</tbody></table>';


        echo $html;

    }
    function searchrfcpres(){
        $pro = $this->input->get('search');
        $results=$this->ModeloCatalogos->getpresrfclike($pro);
        //echo $results;
        echo json_encode($results->result());
    }
    function pres_pendientes_por_completar(){
        $result_v=$this->Ventas_model->pres_pendientes_por_com(0,$this->idpersonal);
        $result_vc=$this->Ventas_model->pres_pendientes_por_com(1,$this->idpersonal);
        $result_p=$this->Ventas_model->pres_pendientes_por_com(2,$this->idpersonal);
        $total_v=$result_v->num_rows();
        $total_vc=$result_vc->num_rows();
        $total_p=$result_p->num_rows();
        $totalgeneral=$total_v+$total_vc+$total_p;
        $array=array(
                    'totalg'=>$totalgeneral
                        );
        echo json_encode($array);
    }
    function agregarservicioex(){
        $params = $this->input->post();
        $ser = $params['ser'];
        $tipo = $params['tipo'];
        $idventa = $params['idventa'];
        $ventatipo = $params['ventatipo'];
        if($ventatipo==1){
            $resultvc=$this->ModeloCatalogos->getselectwheren('ventacombinada',array('combinadaId'=>$idventa));
            foreach ($resultvc->result() as $item) {
                $equipo = $item->equipo;
                $this->ModeloCatalogos->updateCatalogo('ventas',array('v_ser_tipo'=>$tipo,'v_ser_id'=>$ser),array('id'=>$equipo));
            }
            $this->ModeloCatalogos->updateCatalogo('ventacombinada',array('v_ser_tipo'=>$tipo,'v_ser_id'=>$ser),array('equipo'=>$idventa));
        }else{
            $this->ModeloCatalogos->updateCatalogo('ventas',array('v_ser_tipo'=>$tipo,'v_ser_id'=>$ser),array('id'=>$idventa));
        }
    }
    function buscarpre(){
        $params = $this->input->post();
        $pre = $params['pre'];
        $result = $this->ModeloGeneral->buscarventapoliza($pre);
        $html='<table id="table_search_pre"><thead>
                    <tr>
                        <th># Prefactura</th>
                        <th>Cliente</th>
                        <th>Total</th>
                        <th>Fecha</th>
                        <th>Factura</th>
                        <th></th>
                    </tr>
                </thead><tbody>';
        $row=0;
        foreach ($result->result() as $item) {
            $html.='<tr>';
                $html.='<td>'.$item->idventa.'</td>';
                $html.='<td>'.$item->empresa.'</td>';
                $html.='<td>'.$item->total_general.'</td>';
                $html.='<td>'.$item->reg.'</td>';
                $html.='<td>'.$item->serie.$item->Folio.'</td>';
                $html.='<td>';
                    $html.='<a class="b-btn b-btn-primary viewpreir_'.$row.'" onclick="viewpreir('.$row.')" ';
                    $html.=' data-fecha="'.date("Y-m-d",strtotime($item->reg)).'" ';
                    $html.=' data-tipopre="'.$item->tipopre.'" ';
                    $html.=' data-idcliente="'.$item->idCliente.'" ';
                    $html.=' data-empresa="'.$item->empresa.'" ';
                    $html.=' data-idventa="'.$item->idventa.'" ';
                    $html.='><i class="fas fa-random"></i></a>';
                $html.='</td>';
            $html.='</tr>';
            $row++;
        }
        $html.='</tbody></table>';
        echo $html;
    }

} 
