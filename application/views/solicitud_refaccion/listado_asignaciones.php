<style type="text/css">
  .select2-container{
    width: 100% !important;
  }
  #tableaddrefacciones td,#tableaddconsulmibles td{
    padding: 0px;
  }
</style>
<!-- START CONTENT -->
<section id="content">
  <!--start container-->
  <div class="container">
    <div class="section">
      <div class="card-panel">
          <div id="table-datatables">
            <br>
            <h4 class="header">Asignaciones de Servicios</h4>
            <div class="row">
              <div class="col s6">
                <h4>Seleccionar tipo de asignacion:</h4>
              </div>
              <div class="col s6">
                <input name="tasign" type="radio" class="tasign" id="asign1" value="1">
                <label for="asign1">Pre- Asignación</label>
                <input name="tasign" type="radio" class="tasign" id="asign2" value="2">
                <label for="asign2">Asignacion del día</label>
              </div>
            </div>
            <div class="row valortipoasignacion">
              
            </div>
            <span class="datos_cliente"></span>
            <div class="row">
              <div class="col s12 mostrarsoloclientes" style="display: none;">
                <a class="btn-floating red tooltipped datos"
                  data-position="top" data-delay="50" data-tooltip="Venta"
                  data-tooltip-id="dca448ed-3a90-3ed2-3922-f974f10908a4" onclick="venta(0,3)">
                  <i class="fas fa-cart-plus"></i>
                </a>
                <a class="btn-floating red tooltipped" data-position="top" data-delay="50" data-tooltip="Datos de Entrega" data-tooltip-id="dca448ed-3a90-3ed2-3922-f974f10908a4" onclick="calendariop2()">
                 <i class="material-icons">date_range</i></a>
                <a class="btn-floating red tooltipped" data-position="top" data-delay="50" data-tooltip="Asignar Tecnico" data-tooltip-id="dca448ed-3a90-3ed2-3922-f974f10908a4" onclick="asig_tec2()">
                  <i class="material-icons">person_add</i></a>
                 <a class="btn-floating red tooltipped " data-position="top" data-delay="50" data-tooltip="Asignar Prioridad" data-tooltip-id="dca448ed-3a90-3ed2-3922-f974f10908a4" onclick="prioridad2()">
                  <i class="material-icons">report_problem</i></a>  
                <a class="btn-floating red tooltipped" data-position="top" data-delay="50" data-tooltip="Ruta" data-tooltip-id="dca448ed-3a90-3ed2-3922-f974f10908a4" onclick="ruta2()">
                  <i class="material-icons">navigation</i></a>
                  <a class="btn-floating red tooltipped prefactura" data-position="top" data-delay="50" data-tooltip="Prefactura" data-tooltip-id="dca448ed-3a90-3ed2-3922-f974f10908a4" onclick="serviciot2()">
                  <i class="fas fa-wrench"></i></a> 
                  <a class="waves-effect cyan btn-bmz" onclick="generarsolicitud()">Generar</a>
              </div>
            </div>
            
          </div>
      </div>
    </div>  
  </div>
</section>
<input type="hidden" name="id_cp_f" id="id_cp_f" value="0"><!--id de contrato o id de poliza-->
<input type="hidden" name="id_servicio_f" id="id_servicio_f" value="0"><!--id del registro si es que existe-->
<div id="modalAsigna" class="modal">
    <div class="modal-content ">
      <h4>Asignar Técnico </h4>
      <div class="col s12" style="text-align: center;">
        <h5>Seleccionar técnico / especialista</h5>
      </div>
      <div class="col s12 m12 l6">
          <div class="row" align="center">
             <div class="col s12 m12 l12">
               <label class="col s12 m2 l2">Técnico</label>
               <div class="col s12 m8 l8" >
                 <select name="tecnico" id="tecnico" class="browser-default chosen-select">
                  <?php 
                    foreach ($resultstec as $item) { ?>
                      <option value="<?php echo $item->personalId;?>"><?php echo $item->tecnico;?></option>
                    <?php  } ?>
                   <!--<option>Juan Perez</option>
                   <option>Casimiro Lejos</option>-->
                 </select>
               </div>
             </div> 
          </div>
      </div>  
    <div class="modal-footer">
      <a class="modal-action modal-close waves-effect waves-red gray btn-flat " onclick="savetecnico()">Aceptar</a>
      <a class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
    </div>
  </div>
</div>
<div id="modalRuta" class="modal">
    <div class="modal-content ">
      <h4>Zona</h4>
      <div class="col s12" align="center">
        <h5>Seleccionar zona</h5>
      </div>
      <div class="col s12 m12 l6">
          <div class="row" align="center">
             <div class="col s12 m12 l12">
               
               <div class="col s12 m12 l12">
                 <select name="ruta" id="ruta" class="browser-default ">
                  <?php 
                    foreach ($resultzonas->result() as $item) { ?>
                      <option value="<?php echo $item->id;?>"><?php echo $item->nombre;?></option>
                    <?php  } ?>
                 </select>
               </div>
             </div> 
          </div>
      </div> 
    <div class="modal-footer">
      <a class="modal-action modal-close waves-effect waves-red gray btn-flat " onclick="savezona()">Aceptar</a>
      <a class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
    </div>
  </div>
</div>
<div id="modalPrioridad" class="modal">
    <div class="modal-content ">
      <h4> <i class="material-icons">mode_edit</i> Asignar Prioridad </h4>
      <div class="col s12 m12 l6">
          <div class="row" align="center">
             <div class="col s12 m12 l12">
               <label class="col s12 m2 l2">Prioridad</label>
               <div class="col s12 m10 l10">
                <input name="priodidad" type="radio" id="test1" class="soloasgig" value="1" />
                <label for="test1">Mismo día</label>
                <input name="priodidad" type="radio" id="test2" class="soloasgig"  value="2"/>
                <label for="test2">Día siguiente</label>
                <input name="priodidad" type="radio" id="test3" class="soloasgig"  value="3"/>
                <label for="test3">15 días</label>
                <input name="priodidad" type="radio" id="test4" class="solopreasgig" value="4"/>
                <label for="test4">Servicio Regular</label>
               </div>
             </div>  
          </div>
      </div>  
    <div class="modal-footer">
      <a class="modal-action modal-close waves-effect waves-red gray btn-flat " onclick="saveprioridad()">Aceptar</a>
      <a class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
    </div>
  </div>
</div>
<div id="modalDatos" class="modal">
    <div class="modal-content ">
      <h4>Seleccionar fecha y hora</h4>
      <div class="col s12 m12 l6">
          <div class="row" align="center">
             <div class="col s12 m12 l12">
               <label class="col s12 m2 l2">Fecha</label>
               <div class="col s12 m8 l8">
                 <input type="date" name="fecha" id="fecha" class="form-control-bmz">
               </div>
             </div>
             <div class="col s6 m6 l6">
               <label class="col s12 m12 l12">Hora Inicio</label>
               <div class="col s12 m12 l12">
                 <input type="time" name="hora" id="hora" class="form-control-bmz">
               </div>
             </div> 
             <div class="col s6 m6 l6">
               <label class="col s12 m12 l12">Hora Fin</label>
               <div class="col s12 m12 l12">
                 <input type="time" name="horafin" id="horafin" class="form-control-bmz">
               </div>
             </div> 
          </div>
      </div>  
    <div class="modal-footer">
      <a  class="modal-action modal-close waves-effect waves-red gray btn-flat " onclick="savefecha()">Aceptar</a>
      <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
    </div>
  </div>
</div>

<div id="modalOrden" class="modal">
    <div class="modal-content ">
      <h4> <i class="material-icons">settings_applications</i> Crear Orden </h4>
      <hr>
      <div class="col s12 m12 l6">
          <div class="row" align="center">
             <h5>Desea crear la orden de servicio 146?</h5>
             <input type="hidden" name="num_orden" id="num_orden" value="146">
          </div>
      </div> 
    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat" id="conf_crea">Crear</a>
      <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat">Cancelar</a>
    </div>
  </div>
</div>
<div id="modalservicio" class="modal">
    <div class="modal-content ">
      <h4> <i class="material-icons">settings_applications</i> Seleccionar tipo de servicio</h4>
      <hr>
      <div class="col s12 m12 l6">
        <div class="row">
          <label for="tpoliza">Poliza</label>
          <select id="tpoliza" class="browser-default chosen-select form-control-bmz" onchange="obtenerservicios();">
            <option value="0">Sin poliza</option>
            <?php foreach ($resultpolizas->result() as $item) { ?>
                <option value="<?php echo $item->id;?>"><?php echo $item->nombre;?></option>
            <?php } ?>
          </select>
        </div>
        <div class="row">
          <label for="tservicio">Servicio</label>
          <select id="tservicio" class="browser-default chosen-select form-control-bmz">
            
          </select>
        </div>
        <div class="row">
          <input name="ttipo" type="radio" class="ttipo1" id="ttipo1"  value="1" cheched>
          <label for="ttipo1">Local</label>
          <input name="ttipo" type="radio" class="ttipo2" id="ttipo2"  value="2" cheched>
          <label for="ttipo2">Semi</label>
          <input name="ttipo" type="radio" class="ttipo3" id="ttipo3"  value="3" cheched>
          <label for="ttipo3">Foraneo</label>
        </div>
        <div class="row">
          <label for="tserviciomotivo">Motivo del Servicio</label>
          <textarea id="tserviciomotivo" ></textarea>
        </div>
        <div class="row ncequiposs" style="display: none;">
          <label for="ncequipos">Equipos</label>
          <textarea id="ncequipos" ></textarea>
        </div>
        <div class="row ncequiposs" style="display: none;">
          <label for="documentaccesorio">documentos y accesorios</label>
          <textarea id="documentaccesorio" ></textarea>
        </div>
          
      </div> 
    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat" onclick="savemotivos()">Aceptar</a>
      <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat">Cancelar</a>
    </div>
  </div>
</div>
<div id="modalequipos" class="modal">
    <div class="modal-content ">
      <h4> <i class="material-icons">settings_applications</i>Equipos</h4>
      <hr>
      <div class="col s12 m12 l6">
        <div class="row ">
          <table class="table bordered" id="tableequipos">
            <tbody class="equipospolizacontrato">
              
            </tbody>
            
          </table>
          
        </div>
        
          
      </div> 
    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat">Cerrar</a>
    </div>
  </div>
</div>
<div id="modalventa" class="modal">
    <div class="modal-content ">
      <h4> <i class="material-icons">settings_applications</i>Venta</h4>
      <hr>
      <div class="col s12 m12 l6">
        <div class="row">
          <div class="col s12 m3 l3">
            <label>Cantidad</label>
            <input type="number" name="cantidadc" id="cantidadc" class="form-control-bmz" value="1" min="1">
          </div>
          <div class="col s12 m3 l3">
            <label>Consumible</label>
            <select name="sconcumibles" id="sconcumibles" class="browser-default form-control-bmz sconcumibles" onchange="funcionConsumiblesprecios($(this))">
              <option></option>
              <?php foreach ($consumiblesresult->result() as $item) { ?>
                  <option value="<?php echo $item->id;?>"><?php echo $item->modelo;?> (<?php echo $item->parte;?>)</option>
              <?php } ?>
            </select>
          </div>
          <div class="col s12 m3 l3">
            <label>Precio</label>
            <select name="sconcumiblesp" id="sconcumiblesp" class="browser-default form-control-bmz">
              
            </select>
          </div>
          <div class="col s12 m3 l3">
            <a class="waves-effect cyan btn-bmz" onclick="addconsu()">Agregar</a>
          </div>
        </div>
        <div class="row ">
          <table class="table striped" id="tableaddconsulmibles">
            <thead>
              <tr>
                <th>Cantidad</th>
                <th>consumible</th>
                <th>Precio</th>
                <th></th>
              </tr>
            </thead>
            <tbody class="tabaddconsul">
              
            </tbody>
            
          </table>
        </div>
        <div class="row">
          <div class="col s12 m3 l3">
            <label>Cantidad</label>
            <input type="number" name="cantidadr" id="cantidadr" class="form-control-bmz" value="1" min="1">
          </div>
          <div class="col s12 m3 l3">
            <label>Refaccion</label>
            <select name="srefaccions" id="srefaccions" class="browser-default form-control-bmz" onchange="selectprecior()">
              <option></option>
              <?php foreach ($refaccionesresult->result() as $item) { ?>
                  <option value="<?php echo $item->id;?>"><?php echo $item->nombre;?></option>
              <?php } ?>
            </select>
          </div>
          <div class="col s12 m3 l3">
            <label>Precio</label>
            <select name="srefaccionp" id="srefaccionp" class="browser-default form-control-bmz">
              
            </select>
          </div>
          <div class="col s12 m3 l3">
            <a class="waves-effect cyan btn-bmz" onclick="addrefa()">Agregar</a>
          </div>
        </div>
        <div class="row ">
          <table class="table striped" id="tableaddrefacciones">
            <thead>
              <tr>
                <th>Cantidad</th>
                <th>Refaccion</th>
                <th>Precio</th>
                <th></th>
              </tr>
            </thead>
            <tbody class="taaddrefaccion">
              
            </tbody>
            
          </table>
        </div>
        <div class="row">
          <div class="col s12 m8 l8">
          </div>
          <div class="col s12 m3 l3">
            <p>Total: <b>$<span class="totalg">0.00</span></b></p>
          </div>
        </div>
      </div> 
      <div class="modal-footer">
         
          <a class=" waves-effect waves-red gray btn-flat">Edicion</a>
          <a class=" waves-effect waves-red gray btn-flat">Cancelar pedido</a>
          <a class="modal-action modal-close waves-effect waves-red gray btn-flat">Cerrar</a>
          
      </div>
    </div>
</div>