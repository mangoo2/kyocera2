<?php
  $ep_num=$pendientesentrega_reg->num_rows();

  if($ep_num>0){
    $class_ep_num='class_ep_num';
  }else{
    $class_ep_num='';
  }
?>
<style type="text/css">
  table td{font-size: 12px;padding: 6px 7px;  }
  .chosen-container{margin-left: 0px;  }
  td{font-size: 12px !important;  }
  .personaentregada{font-size: 10px;  }
  .detalleprefactura{padding: 5px;margin-left: 8px;    width: 34px;  }
  .detalleprefactura i{margin: 0px;  }
  #modaldetallep{top: 5% !important;max-height: 90%;  }
  .modaliframedetalles iframe{width: 100%;border: 0px;min-height: 380px;  }
  .fechaentregah{color:#c0b9b9;font-size: 10px;  }
  .fechadiferente{background-color: #f1bf8670 !important;  }
  .class_ep_num{    background: #f44336;    height: 50px;    border-radius: 5px;  }
  .camposanimados h6{    display: none;    font-weight: bold;    color: white;  }
  .animate__animated.animate__bounce {
  --animate-duration: 2s;
}
:root {
  --animate-duration: 5s;
  --animate-delay: 0.9s;
}
body{
  overflow-x: hidden;
}
.c_can_ser{
  font-size: 11px;
}
.badge_dev{
  min-width: 109px !important;
}
</style>
<input type="date" id="fechades" class="form-control-bmz fechades" value="<?php 
                  $fechameses=date('Y-m-d H:i:s');
                echo date("Y-m-d",strtotime($fechameses."- 3 days"));?>">
<link    rel="stylesheet"    href="<?php echo base_url();?>public/css/animate.min.css"  />
<!-- START CONTENT -->
    <section id="content">
      <!--start container-->
        <div class="container">
          <div class="row">
            <div class="col s10 m4 l3">
              <h5 class="breadcrumbs-title">Listado de entregas del día</h5>
            </div>
            <div class="col s12 m8 l9">
              <div class="col s4">
                <h6 class="breadcrumbs-title">Entregas pendientes(<?php echo $ep_num; ?>)</h6>
              </div>
              <div class="col s8 camposanimados <?php echo $class_ep_num ;?>">
              </div>
              
            </div>
          </div>
        </div>
        <div class="container">
          <div class="section">
            <div class="card-panel">
            <input id="base_url" type="hidden" name="base_url" value="<?php echo base_url(); ?>">
            <div id="table-datatables">
                  <div class="row">
                    <div class="col s6 m2 l2">
                      <label>Fecha inicio</label>
                      <input type="date" id="fechainicial_reg" class="form-control-bmz" value="<?php echo $fechainicial_reg;?>" >
                    </div>
                    
                    <div class="col s6 m10 l10" style="text-align:end">
                      <a class="btn-bmz btn-danger green btn-sm "  href="<?php echo base_url(); ?>Devoluciones">Devoluciones</a>
                    </div>
                  </div>
                  <div class="row">
                      <div class="col s4 m2 l2">
                        <label>Tipo de listado</label>
                        <select id="tipoventa" class="browser-default form-control-bmz" >
                          <option value="0">Todo ultimo mes</option>
                          <option value="1">Venta</option><option value="2">Rentas</option>
                          <option value="5">Rentas Historial</option><!--<option value="3">Pólizas Creadas</option>--><option value="4">Combinada</option>
                        </select>
                      </div>
                      <div class="col s4 m2 l2">
                        <label>Clientes</label>
                        <select id="cliente" class="browser-default" >
                          <option value="0">Selecione</option>
                        </select>
                      </div>
                      <div class="col s6 m2 l2">
                        <label>Técnico/Especialista</label>
                        <select id="tecnico" class="browser-default  form-control-bmz" >
                          <option value="0">Todos</option>
                          <?php foreach ($resultstec as $item) { ?>
                              <option value="<?php echo $item->personalId;?>"><?php echo $item->tecnico;?></option>
                          <?php } ?>
                          <option value="XXX" title="servicio generado automaticamente de contrato">Sin tecnico</option>
                        </select>
                      </div>
                      <div class="col s4 m2 l2">
                        <label>Tipo de estatus</label>
                        <select id="tipoestatus" class="browser-default form-control-bmz" >
                          <option value="0">Selecione</option><option value="1">Entregado</option><option value="2">Pendientes</option><option value="3">Pendientes con cambio de fecha</option>
                        </select>
                      </div>
                      <div class="col s4 m2 l2">
                        <label>Fecha entrega</label>
                        <input type="date" class="form-control-bmz" id="fecha" style="margin:0px">
                      </div>
                      <div class="col s4 m1 l1 soloventas">
                        <label>Empresa</label>
                        <select id="empresaselect" class="browser-default form-control-bmz" >
                          <option value="0">Todos</option><option value="1">Kyocera</option><option value="2">D-Impresión</option>
                        </select>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col s12 m12 l12" style="text-align:end">
                        <a class="b-btn b-btn-primary" style="margin-top: 20px;">Pendientes <span class="count_pre_pen">0</span></a>
                        <a class="b-btn b-btn-success btn_entregas_dia" data-fecha="<?php echo date('Y-m-d')?>" onclick="generarreporte()" style="margin-top: 20px;">Reporte Entregas</a>
                        <button class="b-btn b-btn-primary" onclick="load()" style="margin-top: 20px;" title="Realizar consulta"><i class="fas fa-filter"></i> <span class="btn-filtrar"></span></button>
                      </div>
                  </div>
                  <!--
                  <div class="col s2">  
                    <button class="btn waves-effect waves-light gradient-45deg-green-teal gradient-shadow" >Facturar</button>
                  </div>
                  -->
                <div class="row">
                  <div class="col s12">

                      <table id="tabla_ventas_incompletas" class="responsive-table display" cellspacing="0">
                        <thead>
                          <tr>
                            <th>Checada</th>
                            <th>#</th>
                            <th>Cliente</th>
                            <th>PRE FACTURA</th>

                            <th>Tipo</th>
                            <th>Vendedor</th>
                            <th>Estatus</th>
                            <th>Creado</th>
                            <th>Bodegas</th>
                            <th class="titulo_fechaentraga"></th>
                            <th>Tecnico</th>
                            <th></th>
                            <th>Acciones</th>
                          </tr>
                        </thead>
                        <tbody>
                        </tbody>
                      </table>
                    </div>
                  </div>
            </div>
          </div>
          </div>  
            <!-- Modal finaliza -->
            <div id="modalFinaliza" class="modal"  >
                <div class="modal-content ">
                  <h4>Confirmatión</h4>
                  <div class="col s12 m12 l6">
                        <div class="row" align="center">
                          <h5>Deseas finalizar factura</h5>
                        </div>
                  </div>  
                <input type="hidden" id="id_venta">  
                <div class="modal-footer">
                  <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
                  <a href="#!" class="modal-action modal-close waves-effect waves-red red btn-flat" style="color: white;" onclick="finalizar_factura()">Aceptar</a>
                </div>
              </div>
              <!-- Modal finaliza fin-->
        </div>
        <!-- Modal 1 -->
        <div id="modalDetalles" class="modal"  >
          <div class="modal-content ">
            <h4> Detalles </h4>
            <div class="col s12 m12 l6">
                  <div class="row" align="center">
                    <div class="detalles_ventas"></div>
                  </div>
            </div>  
          <div class="modal-footer">
            <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
          </div>
        </div>
        
        </div>

        <div id="modal_cfdi" class="modal"  >
          <div class="modal-content ">
            <h4 align="center">Captura CFDI</h4>
            <br>
            <div class="row">
              <div class="input-field col s2"></div>
              <div class="input-field col s8">
                <input type="text" id="cdfi_e">
              </div>
              <div class="input-field col s2"></div>
              <input type="hidden" id="id_venta_e">
              <input type="hidden" id="tipo_e">
            </div> 
          </div>    
          <div class="modal-footer">
            <a class="modal-action modal-close waves-effect waves-red gray btn-flat" onclick="update_cfdi()">Guardar</a>
          </div>
        </div>
        <!-- Modal 1 -->
        <div id="modal_pago" class="modal"  >
          <div class="modal-content ">
            <div class="row">
              <div class="col s6">
                <h5>Registro de nuevo pago</h5>
              </div>
              <div class="col s6" align="right">
                <p><h6>Monto de prefactura</h6></p>
                <p><span style="color: red" class="monto_prefactura"></span></p>
              </div>
            </div>  
            <br>
            <h5>Vendedor: <?php echo $nombre.' '.$apellido_paterno.' '.$apellido_materno ?></h5>
            <form class="form" id="form_pago" method="post"> 
              <input type="hidden" id="id_venta_p" name="idcompra"><!--id que se va aguardar dependiendo si es venta, renta, poliza y combinada -->
              <input type="hidden" id="tipo_p" name="tipo"><!-- tipo para verificar en que tabla se va insertar --> 
              <div class="row">
                <div class="input-field col s6">
                  <input type="date" name="fecha" id="fecha_p">
                  <label for="fecha" class="active">Fecha</label>
                </div>
                <div class="input-field col s6">
                  <label for="idmetodo" class="active">Método de pago</label><br>
                  <select name="idmetodo" id="idmetodo" class="browser-default chosen-select">
                     <?php foreach ($get_metodo_pago->result() as $item) { ?>
                      <option value="<?php echo $item->id ?>"><?php echo $item->metodopago_text ?></option>
                    <?php } ?>
                  </select>  
                </div>
              </div>
              <div class="row">
                <div class="input-field col s6">
                  <input type="number" name="pago" id="pago_p">
                  <label for="pago" class="active">Monto</label>
                </div>
              </div> 
              <div class="row">
                <div class="input-field col s12">
                  <textarea rows="2" name="observacion" id="observacion_p"></textarea>
                  <label class="active">Comentario</label>
                </div>
              </div>
            </form>
          </div>    
          <div class="modal-footer">
            <a class="modal-action waves-effect waves-red gray btn-flat" onclick="guardar_pago_compras()">Guardar</a>
          </div>
          <div class="modal-footer">
             <div class="col s12">
              <table id="tabla_pagos" class="responsive-table display" cellspacing="0">
                <thead>
                  <tr>
                    <th>Fecha</th>
                    <th>Método de pago</th>
                    <th>Monto</th>
                    <th>Comprobante</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        
    </section>

<div style="display:none;">
  <select id="empleados_pre" class="browser-default">
    <?php foreach ($empleados->result() as $item) { ?>
      <option value="<?php echo $item->personalId;?>"><?php echo $item->nombre.' '.$item->apellido_paterno.' '.$item->apellido_materno;?></option>
    <?php } ?>
    
  </select>
</div>

<div id="modaldetallep" class="modal">
  <div class="modal-content">
      <h5>Detalles</h5>
      <div class="row modaliframedetalles">
        
      </div>
    <div class="modal-footer">
      <a  class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
    </div>
  </div>
</div>


<div id="modaldetalledev" class="modal">
  <div class="modal-content">
      <h5>Detalles</h5>
      <div class="row ">
        <div class="col s12 detalles_dev">
          
        </div>
      </div>
    <div class="modal-footer">
      <a  class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
    </div>
  </div>
</div>