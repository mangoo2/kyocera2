<style type="text/css">
  td{
    font-size: 12px;
  }
  .preview_iframe{
    text-align: center;
  }
  .ifrafac{
    border: 0px;
    width: 100%;
    min-height: 350px;
  }
</style>
<!-- START CONTENT -->
<input type="hidden" id="codigoenvio" value="<?php echo date('YmdGis');?>">
<section id="content">
  <!--start container-->
    <div class="container">
      <div class="section">
        <div class="row">
          <div class="col s9"><h4 class="caption">Generación de estado de cuenta</h4></div>
          <div class="col s3"></div>
        </div>
        <div class="card-panel">
          <div class="row">
            <div class="col s3">
              <label>Seleccione Cliente</label>
              <select class="browser-default chosen-select" id="idcliente" onchange="generar()"></select>
            </div>
            <div class="col s3">
              <div class="switch"><br>
                <label >Mostrar todas las facturas
                  <input type="checkbox" name="mostrar_todo" id="mostrar_todo" onchange="generar()">
                  <span class="lever"></span>
                </label>
              </div>
            </div>
            <div class="col s3">
              <label>Ejecutivo</label>
              <select id="ejecutivoselect" class="browser-default form-control-bmz" onchange="generar();">
                <option value="0">Todos</option>
                <?php foreach ($personalrows->result() as $item) { 
                  echo '<option value="'.$item->personalId.'">'.$item->nombre.' '.$item->apellido_paterno.' '.$item->apellido_paterno.'</option>';
                   } ?>
              </select>
            </div>
            <div class="col s3">
              <!--<button class="b-btn b-btn-primary"  onclick="generar()">Generar</button>-->
            </div>
            
          </div>
          <div class="row">
            <div class="col s12">
                <div class="col s12">
                  <ul class="collapsible popout collapsible-accordion" data-collapsible="expandable">
                     <li class="">
                        <div class="collapsible-header">
                        <i class="material-icons">subtitles</i>Rentas</div>
                        <div class="collapsible-body" style="display: none;">
                           <!------------------------------------------------------------->
                              <table class="table display no-footer dataTable dtr-inline" id="date-rentas">
                                 <thead>
                                    <tr>
                                       <th><input type="checkbox" class="filled-in" id="factura_0" onchange="all_check_rentas()"><label for="factura_0"></label></th>
                                       <th>ID periodo</th>
                                       <th>Contrato</th>
                                       <th>Folio</th>
                                       <th>Periodo Inicial</th>
                                       <th>Periodo Final</th>
                                       <th>Monto</th>
                                       <th>Pagado</th>
                                       <th>Saldo</th>
                                       <th>Fecha Timbre</th>
                                       <th>Factura</th>
                                       <th>Partidas</th>
                                       
                                       <th></th>
                                    </tr>
                                 </thead>
                                 <tbody class="date-rentas">
                                    
                                    
                                    
                                 </tbody>
                              </table>
                           <!------------------------------------------------------------->
                        </div>
                     </li>
                     
                     <li>
                        <div class="collapsible-header">
                        <i class="material-icons">library_books</i>Ventas</div>
                        <div class="collapsible-body">
                           <!------------------------------------------------------------->
                              
                              <table class="table display no-footer dataTable dtr-inline" id="table_ventas">
                                 <thead>
                                    <tr>
                                       <th><input type="checkbox" class="filled-in" id="factura_v_0" onchange="all_check_ventas()"><label for="factura_v_0"></label></th>
                                       <th>Id Venta</th>
                                       <th>Fecha Creación</th>
                                       <th>Monto</th>
                                       <th>Pagado</th>
                                       <th>Saldo</th>
                                       <th>Folio</th>
                                       <th>Factura</th>
                                       <th>Partidas</th>
                                       <th></th>
                                    </tr>
                                 </thead>
                              <tbody class="table_ventas"></tbody>
                           </table>
                           <!------------------------------------------------------------->
                        </div>
                     </li>
                     <li>
                        <div class="collapsible-header">
                        <i class="material-icons">library_books</i>Servicios</div>
                        <div class="collapsible-body">
                           <!------------------------------------------------------------->
                              
                              <table class="table display no-footer dataTable dtr-inline" id="table_polizas">
                                 <thead>
                                    <tr>
                                       <th><input type="checkbox" class="filled-in" id="factura_v_2" onchange="all_check_poliza()"><label for="factura_v_2"></label></th>
                                       <th>Id Poliza</th>
                                       <th>Fecha Creación</th>
                                       <th>Monto</th>
                                       <th>Pagado</th>
                                       <th>Saldo</th>
                                       <th>Folio</th>
                                       <th>Factura</th>
                                       <th>Partidas</th>
                                       <th></th>
                                    </tr>
                                 </thead>
                              <tbody class="table_polizas"></tbody>
                           </table>
                           <!------------------------------------------------------------->
                        </div>
                     </li>
                     <li>
                        <div class="collapsible-header">
                        <i class="material-icons">library_books</i>Otra Facturas</div>
                        <div class="collapsible-body">
                           <!------------------------------------------------------------->
                              
                              <table class="table display no-footer dataTable dtr-inline" id="table_fac_sin">
                                 <thead>
                                    <tr>
                                       <th><input type="checkbox" class="filled-in" id="factura_v_4" onchange="all_check_facturas()"><label for="factura_v_2"></label></th>
                                       <th>Folio</th>
                                       <th>cliente</th>
                                       <th>RFC</th>
                                       <th>Monto</th>
                                       <th>Pagado</th>
                                       <th>Saldo</th>
                                       <th>Fecha timbre</th>
                                       <th>Factura</th>
                                       <th>Partidas</th>
                                       <th></th>
                                    </tr>
                                 </thead>
                              <tbody class="table_tb_fac_sin"></tbody>
                           </table>
                           <!------------------------------------------------------------->
                        </div>
                     </li>
                     
                  </ul>
               </div>
            </div>
          </div>
          <div class="row">
            <div class="col s8 preview_iframe">
              
            </div>
            <div class="col s4">
              <button class="b-btn b-btn-warning" onclick="generarestado()">Generar Estado de cuenta</button>
              <button class="b-btn b-btn-warning" onclick="enviarestado()">Enviar por correo electrónico</button>
            </div>
          </div>
        </div>


      
      </div>  
    </div>
</section>




<div id="modal_servicio" class="modal">
    <div class="modal-content">
      <div class="col s12">
        <label>Firma del ejecutivo</label>
        <select class="browser-default form-control-bmz" id="firma_m_s">
          <?php foreach ($perfirma->result() as $itemf) {
            echo '<option value="'.$itemf->personalId.'">'.$itemf->nombre.' '.$itemf->apellido_paterno.' '.$itemf->apellido_materno.' / '.$itemf->email.' / '.$itemf->celular.'</option>';
          }?>
        </select>
      </div>
      <div class="col s12">
        <label>Copia oculta (BBC)</label>
        <select class="browser-default form-control-bmz" id="bbc_m_s"><option value="0"></option>
          <?php foreach ($perfirma->result() as $itemf) {
            echo '<option value="'.$itemf->personalId.'">'.$itemf->nombre.' '.$itemf->apellido_paterno.' '.$itemf->apellido_materno.' / '.$itemf->email.' / '.$itemf->celular.'</option>';
          }?>
        </select>
      </div>
      <div class="col s12">
          <label>Correo envio</label>
          <div class="m_s_contac"></div>
        </div>
      <div class="row">
        <div class="col s12">
          <label>Asunto</label>
          <input class="form-control-bmz" id="ms_asucto2" value="Servicio">
        </div>
        <div class="col s12">
          <label>Comentario</label>
          <textarea class="form-control-bmz" id="ms_coment2" style="min-height: 150px;"></textarea>
        </div>
        <div class="col s5 input_envio_s">
        </div>
        
      </div>
      <div class="row"><div class="col s12 addbtnservicio"></div></div>
 
    </div>
    <div class="row">
      <div class="col m12" align="right">
        <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
      </div>
    </div>        
</div>