ELECT pv.*,cli.id,cli.empresa,cli.bloqueo_fp 
FROM pagos_ventas as pv 
inner join ventas as v on v.id=pv.idventa
inner join clientes as cli on cli.id=v.idCliente
where pv.reg>'2024-01-01 00:00:00' and pv.reg<'2024-11-01 00:00:00' and cli.bloqueo_fp=1 and v.tipov=1

SELECT pv.*,cli.id,cli.empresa,cli.bloqueo_fp 
FROM pagos_poliza as pv 
inner join polizasCreadas  as v on v.id =pv.idpoliza
inner join clientes as cli on cli.id=v.idCliente
where pv.reg>'2024-01-01 00:00:00' and pv.reg<'2025-11-01 00:00:00' and cli.bloqueo_fp=1 and v.tipov=1

SELECT pv.*,cli.id,cli.empresa,cli.bloqueo_fp 
FROM pagos_combinada as pv 
inner join ventacombinada as v on v.combinadaId =pv.idventa
inner join clientes as cli on cli.id=v.idCliente
where pv.reg>'2024-01-01 00:00:00' and pv.reg<'2025-11-01 00:00:00' and cli.bloqueo_fp=1 and v.tipov=1


SELECT * from(
SELECT cli.id,cli.empresa,cli.bloqueo_fp,pv.reg
FROM pagos_ventas as pv 
inner join ventas as v on v.id=pv.idventa
inner join clientes as cli on cli.id=v.idCliente
where pv.reg>'2024-01-01 00:00:00' and pv.reg<'2024-11-01 00:00:00' and cli.bloqueo_fp=1 and v.tipov=1
union
SELECT cli.id,cli.empresa,cli.bloqueo_fp ,pv.reg
FROM pagos_poliza as pv 
inner join polizasCreadas  as v on v.id =pv.idpoliza
inner join clientes as cli on cli.id=v.idCliente
where pv.reg>'2024-01-01 00:00:00' and pv.reg<'2025-11-01 00:00:00' and cli.bloqueo_fp=1 and v.tipov=1
union
SELECT cli.id,cli.empresa,cli.bloqueo_fp ,pv.reg
FROM pagos_combinada as pv 
inner join ventacombinada as v on v.combinadaId =pv.idventa
inner join clientes as cli on cli.id=v.idCliente
where pv.reg>'2024-01-01 00:00:00' and pv.reg<'2025-11-01 00:00:00' and cli.bloqueo_fp=1 and v.tipov=1
) as datos group by id



-------------------------------------------------------------------------------------------------------------


SELECT * from(
SELECT facpre.facId, facpre.contratoId,facpre.prefacturaId,facpre.statuspago,fac.FacturasId, fac.Folio,fac.fechatimbre ,fac.total,SUM(docd.ImpPagado) as ImpPagado
FROM factura_prefactura as facpre
INNER JOIN f_facturas as fac on fac.FacturasId=facpre.facturaId
INNER JOIN f_complementopago_documento as docd on docd.facturasId=fac.FacturasId
INNER JOIN f_complementopago as doc on doc.complementoId=docd.complementoId
WHERE facpre.statuspago=0 AND fac.Estado=1 AND doc.Estado=1
GROUP BY facpre.facId,fac.FacturasId 
) as datos where  ImpPagado>=total
ORDER BY FacturasId DESC;


//agregarle o verificas que exista una funcion como esta
SELECT p.id,p.idCliente,p.combinada,p.reg,p.estatus,p.total, fac.FacturasId,fac.serie,fac.Folio,docd.ImpPagado
FROM polizasCreadas as p
INNER JOIN factura_poliza as facp on facp.polizaId=p.id
INNER JOIN f_facturas as fac on fac.FacturasId=facp.facturaId
INNER JOIN f_complementopago_documento as docd on docd.facturasId=fac.FacturasId
WHERE p.activo=1 AND p.combinada=0 AND p.estatus!=3 AND p.total>0 AND docd.ImpPagado>=p.total;


SELECT v.id,v.idCliente,v.combinada,v.reg,v.estatus,v.total_general as total, fac.FacturasId,fac.serie,fac.Folio,docd.ImpPagado
FROM ventas as v
INNER JOIN factura_venta as facv on facv.ventaId=v.id AND facv.combinada=0
INNER JOIN f_facturas as fac on fac.FacturasId=facv.facturaId
INNER JOIN f_complementopago_documento as docd on docd.facturasId=fac.FacturasId
WHERE v.activo=1 AND v.combinada=0 AND v.estatus!=3 AND docd.ImpPagado>=v.total_general;


SELECT v.equipo ,v.reg,v.estatus,v.total_general as total, fac.FacturasId,fac.serie,fac.Folio,docd.ImpPagado
FROM ventacombinada as v
INNER JOIN factura_venta as facv on facv.ventaId=v.combinadaId AND facv.combinada=1
INNER JOIN f_facturas as fac on fac.FacturasId=facv.facturaId
INNER JOIN f_complementopago_documento as docd on docd.facturasId=fac.FacturasId
WHERE v.activo=1 AND  v.estatus!=3 AND docd.ImpPagado>=v.total_general;









=========================================consultas cuando estan como pagadas pero los complementos estan cancelados
select * from(
SELECT facpre.facId, facpre.contratoId,facpre.prefacturaId,facpre.statuspago,fac.FacturasId, fac.Folio,fac.fechatimbre ,fac.total,docd.ImpPagado,docd.complementoId,(select docd2.complementoId from f_complementopago_documento as docd2 where docd2.facturasId=fac.FacturasId and docd2.Estado=1 limit 1) as nuew_complemento
FROM factura_prefactura as facpre
INNER JOIN f_facturas as fac on fac.FacturasId=facpre.facturaId AND fac.Estado=1
INNER JOIN f_complementopago_documento as docd on docd.facturasId=fac.FacturasId 
INNER JOIN f_complementopago as doc on doc.complementoId=docd.complementoId 
WHERE facpre.statuspago=1  AND doc.Estado=0
ORDER BY `facpre`.`facId` DESC
) as datos where nuew_complemento is null;

select * from(
SELECT p.id,p.idCliente,p.combinada,p.reg,p.estatus,p.total, fac.FacturasId,fac.serie,fac.Folio,docd.ImpPagado,doc.fechatimbre,doc.FechaPago,doc.Estado,docd.Estado,
(select docd2.complementoId from f_complementopago_documento as docd2 where docd2.facturasId=fac.FacturasId and docd2.Estado=1 limit 1) as nuew_complemento
FROM polizasCreadas as p
INNER JOIN factura_poliza as facp on facp.polizaId=p.id
INNER JOIN f_facturas as fac on fac.FacturasId=facp.facturaId
INNER JOIN f_complementopago_documento as docd on docd.facturasId=fac.FacturasId
INNER JOIN f_complementopago as doc on doc.complementoId=docd.complementoId
WHERE p.activo=1 AND p.combinada=0 AND p.estatus=3 AND doc.Estado=0
) as datos where nuew_complemento is null;


select * from(
SELECT v.id,v.idCliente,v.combinada,v.reg,v.estatus,v.total_general as total, fac.FacturasId,fac.serie,fac.Folio,docd.ImpPagado,
(select docd2.complementoId from f_complementopago_documento as docd2 where docd2.facturasId=fac.FacturasId and docd2.Estado=1 limit 1) as nuew_complemento
FROM ventas as v
INNER JOIN factura_venta as facv on facv.ventaId=v.id AND facv.combinada=0
INNER JOIN f_facturas as fac on fac.FacturasId=facv.facturaId
INNER JOIN f_complementopago_documento as docd on docd.facturasId=fac.FacturasId
INNER JOIN f_complementopago as doc on doc.complementoId=docd.complementoId
WHERE v.activo=1 AND v.combinada=0 AND v.estatus=3 AND doc.Estado=0
) as datos where nuew_complemento is null;



select * from(
SELECT v.equipo ,v.reg,v.estatus,v.total_general as total, fac.FacturasId,fac.serie,fac.Folio,docd.ImpPagado,
(select docd2.complementoId from f_complementopago_documento as docd2 where docd2.facturasId=fac.FacturasId and docd2.Estado=1 limit 1) as nuew_complemento
FROM ventacombinada as v
INNER JOIN factura_venta as facv on facv.ventaId=v.combinadaId AND facv.combinada=1
INNER JOIN f_facturas as fac on fac.FacturasId=facv.facturaId
INNER JOIN f_complementopago_documento as docd on docd.facturasId=fac.FacturasId
INNER JOIN f_complementopago as doc on doc.complementoId=docd.complementoId
WHERE v.activo=1 AND  v.estatus=3 AND doc.Estado=0
) as datos where nuew_complemento is null;