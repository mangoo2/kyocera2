<style type="text/css">
.chosen-container{margin-left: 0px;}
table.dataTable tbody th, table.dataTable tbody td {padding: 5px 10px;}
.burbuja_agb{position: absolute;margin-top: -8px !important;border-radius: 14px !important;padding: 0px !important;}
.btn_agb_w{min-width: 134px;}
.error{  color: red !important;}
.costocero{display: none;}
.soloventas{display: none;}
  <?php
    //27 35 42 antonio 9 israel 56 adriana 62 crstina 50 marcela 17 julio 18 diana
    if($idpersonal==27 or $idpersonal==35 or $idpersonal==42 or $idpersonal==9 or $idpersonal==56 or $idpersonal==62 or $idpersonal==50 or $idpersonal==18 or $idpersonal==17){ ?>
       .costocero{
            display: block !important;
          }
  <?php } ?>
</style>
<div class="newstyles"></div>
<?php if(isset($_GET['eleccionCotizar'])){?>
  <script type="text/javascript">
    $(document).ready(function($) {
      setTimeout(function(){ 
          <?php 
          if($_GET['eleccionCotizar']==1){?>
            $('.equipos_select').attr('style','display: block');
          <?php }
          if($_GET['eleccionCotizar']==2){?>
            $('.selectEquiposConsumibles').attr('style','display: block');
          <?php }
          if($_GET['eleccionCotizar']==3){?>
            $('.selectEquiposRefacciones').attr('style','display: block');
                $('.addpolizass').attr('style','display: none');
                $('.addpolizas').attr('style','display: none');
          <?php }
          if($_GET['eleccionCotizar']==4){?>
              $('#selectDetallesPolizas').attr('style','display: block');
                $('#selectDetallesPolizas2').attr('style','display: block');
                $('.addpolizass').attr('style','display: block');
                seleccionpoliza();
          <?php }
          if($_GET['eleccionCotizar']==5){?>
              $('.addpolizass').attr('style','display: none');
                $('.addpolizas').attr('style','display: none');

                $('.accesorios_selected').attr('style','display: block');
          <?php }
          ?>

      }, 1000);
    });
  </script>
<?php } ?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/bulma-badge.min.css">
<!-- START CONTENT -->
<section id="content" width="100%">
  <!--start container-->
  <div class="container" width="100%">
    <input id="contadorPolizas" type="hidden" name="contadorPolizas" value="1">
    <input id="contadorConsumibles" type="hidden" name="contadorConsumibles" value="1">
    <input id="contadorRefacciones" type="hidden" name="contadorRefacciones" value="1">
    <input id="aaa" type="hidden" value="<?php echo $aaa ?>">
    <div class="section">
      <div class="content-header">Cotización</div>
      <div class="row">
                <div class="col s2 m2 l2 desbloqueocostosceros" style="float:right; text-align: right;" onclick="desbloqueocostosceros()">
                  <?php
    //27 35 42 antonio 9 israel 56 adriana 62 crstina 50 marcela 17 julio 18 diana
    if($idpersonal==27 or $idpersonal==35 or $idpersonal==42 or $idpersonal==9 or $idpersonal==56 or $idpersonal==62 or $idpersonal==50 or $idpersonal==18 or $idpersonal==17){ ?>
       Costos Cero <i class="fa fa-lock-open fa-fw" style="color: green;"></i>
  <?php }else{ ?>
        Costos Cero <i class="fa fa-lock fa-fw"></i>
  <?php } ?>
                  
                </div>
              </div>
      <?php if ($condicion!='') { ?>
      <div class="row">
        <div class="col s12 m12 l12">
          <div class="card gradient-45deg-deep-orange-orange darken-2">
            <div class="card-content " style="padding-top: 5px;padding-bottom: 5px;">
              <div class="row">
                <div class="card col s12 m12 l12">
                  <?php echo $condicion;?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <?php  } ?>
      <div class="row">
        <div class="col s12 m12 24">
          <div class="card-panel">
            <div class="row">
              <div class="col s6"></div>
              <div class="col s6">
                <select id="atencionselect" class="browser-default form-control-bmz" style="display: none;" onchange="obtenercontactosss()">
                  <option data-atencionpara="" data-email="" data-telefono="" >Seleccione</option>
                  <?php foreach ($datoscontacto->result() as $item) { ?>
                  <option
                    data-atencionpara="<?php echo $item->atencionpara;?>"
                    data-email="<?php echo $item->email;?>"
                    data-telefono="<?php echo $item->telefono;?>"
                  ><?php echo $item->atencionpara;?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
            <form id="formulario_cotizacion">
              <div class="row">
                <div class="col s6">
                  <select name="tipov" id="tipov" class="browser-default form-control-bmz selectweb">
                    <option value="1">Alta Productividad</option>
                    <option value="2">D-Impresión</option>
                  </select>
                </div>
              </div>
              <input type="hidden" name="idcotizacion" id="idcotizacion" value="0">
              <div class="row editcotizacion">
                
                <div class="input-field col s6">
                  <i class="material-icons prefix">account_box</i>
                  <input id="cliente" name="cliente"  type="text" value="<?php echo $nombreCliente;?>" disabled="">
                  <label for="cliente">Cliente / Prospecto</label>
                </div>
                <div class="input-field col s6">
                  <i class="material-icons prefix">account_box</i>
                  <input id="atencion" name="atencion" placeholder=" "  type="text" value="<?php echo $atencionpara;?>" required>
                  
                  <label for="atencion" class="active">Atención para:</label>
                  <i class="fas fa-list-alt material-icons prefix" onclick="obtenercontactos()"></i>
                </div>
              </div>
              <div class="row editcotizacion">
                <div class="input-field col s6">
                  <i class="material-icons prefix">mail_outline</i>
                  <input id="correo" name="correo" placeholder=" " value="<?php echo $emailCliente;?>" type="text" required>
                  <label for="cliente" class="active">Correo</label>
                </div>
                <div class="input-field col s6">
                  <i class="material-icons prefix">call</i>
                  <input id="telefono" name="telefono" placeholder=" " value="<?php echo $telefonoCliente;?>"  type="text" required>
                  <label for="telefono" class="active">Telefono:</label>
                </div>
              </div>
              
              <!-- ELECCION DE COTIZACION -->
              <div class="row">
                <div class="col s6">
                  <div class="col s5" style="margin-left: -12px;">
                    <i class="material-icons prefix">computer</i>
                    <label for="estado">¿Que desea cotizar?</label>
                  </div>
                  <div class="col s7 margenTopDivChosen" >
                    <p>
                        <input type="checkbox" class="filled-in" id="ventae" name="eleccionCotizar" value="1"  onchange="eleccionCotizar2(1)" />
                        <label for="ventae">Equipos</label>
                      </p>
                      <p class="nopararentas">
                        <input type="checkbox" class="filled-in" id="ventac" name="eleccionCotizar" value="2"  onchange="eleccionCotizar2(2)" />
                        <label for="ventac">Consumibles</label>
                      </p>
                      <p class="nopararentas">
                        <input type="checkbox" class="filled-in" id="ventar" name="eleccionCotizar" value="3"  onchange="eleccionCotizar2(3)" />
                        <label for="ventar">Refacciones</label>
                      </p>
                      <p class="nopararentas">
                        <input type="checkbox" class="filled-in" id="ventap" name="eleccionCotizar" value="4"  onchange="eleccionCotizar2(4)" />
                        <label for="ventap">Servicio</label>
                      </p>
                      <p class="nopararentas">
                        <input type="checkbox" class="filled-in" id="ventaa" name="eleccionCotizar" value="5"  onchange="eleccionCotizar2(5)" />
                        <label for="ventaa">Accesorios</label>
                      </p>
                    <!--<select id="eleccionCotizar" name="eleccionCotizar" class="browser-default form-control-bmz" >
                      <option value="" disabled selected>¿Que desea cotizar?</option>
                      <option value="1">Equipos</option>
                      <option value="2">Consumibles</option>
                      <option value="3">Refacciones</option>
                      <option value="4">Servicio</option>
                      <option value="5">Accesorios</option>
                    </select>-->
                  </div>
                </div>
                <div class="col s6" id="opcionesEquipos">
                  <div class="col s4" style="margin-left: -12px;text-align: end;">
                    <i class="material-icons prefix">monetization_on</i>
                    <label for="estado">Tipo de Cotización</label>
                  </div>
                  <div class="col s7 margenTopDivChosen" >
                    <select id="tipo" name="tipo2" class="browser-default form-control-bmz tipo2selected" onchange="tipo2selected()" required>
                      <option value="" disabled selected>Selecciona tipo de cotización</option>
                      <option value="1">Venta</option>
                      <option value="2">Renta</option>
                      <option value="3">Póliza</option>
                    </select>
                  </div>
                  <div class="col s4 editcotizacion" style="margin-left: -12px;text-align: end;">
                    <label for="prioridad">Prioridad</label>
                  </div>
                  <div class="col s7 editcotizacion margenTopDivChosen" >
                    <select id="prioridad" name="prioridad" class="browser-default form-control-bmz" >
                      <option value="1">Prioridad 1</option>
                      <option value="2">Prioridad 2</option>
                      <option value="3">Prioridad 3</option>
                    </select>
                  </div>
                </div>
                <div class="col s6 soloventas">
                  <div style="text-align: end;"><a class="btn-bmz blue" onclick="servicioaddcliente()" style="margin-bottom: 10px;">Agregar un servicio existente</a></div>
                  <div>
                    <input type="number" name="v_ser_tipo" id="v_ser_tipo" style="display:none">
                    <input type="number" name="v_ser_id" id="v_ser_id" style="display:none">
                  </div>
                  <div class="addinfoser"></div>
                </div>
                <div class="input-field col s6" style="display: none" id="opcionesConsumiblesRefacciones">
                  <div class="col s5" style="margin-left: -12px;">
                    <i class="material-icons prefix">monetization_on</i>
                    <label for="estado">Tipo de Cotización</label>
                  </div>
                  <div class="col s7 margenTopDivChosen" >
                    <select id="tipo" name="tipo" class="browser-default chosen-select tipoConsumibles">
                      <option value="1">Venta</option>
                    </select>
                  </div>
                </div>
                
                
              </div>
              <div class="row editcotizacion">
                <div class="col s6">
                  <div class="col s5" style="margin-left: -12px;">
                    <i class="material-icons prefix">computer</i>
                    <label for="estado">¿Que tipo?</label>
                  </div>
                  <div class="col s7 margenTopDivChosen" >
                    <select id="propuesta" name="propuesta" class="browser-default form-control-bmz" >
                      <option value="0">Cotizacion</option>
                      <option value="1">Propuesta</option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="input-field col s6" id="tipoCotizacion" style="display: none;">
                  <div class="col s5" style="margin-left: -12px;">
                    <i class="material-icons prefix">format_list_bulleted</i>
                    <label for="estado">Cotización</label>
                  </div>
                  <div class="col s7 margenTopDivChosen" >
                    <select id="cotizar" name="cotizar" onchange="selectcotiza()" class="browser-default chosen-select">
                      <option value="" disabled selected>Forma de cotización</option>
                      <option value="1">Manual</option>
                      <!--<option value="2">Familia</option>-->
                    </select>
                  </div>
                </div>
                <div class="input-field col s6 select_familia" style="display: none;" id="select_familia">
                  <div class="col s5" style="margin-left: -12px;">
                    <i class="material-icons prefix">person</i>
                    <label for="estado">Familiar</label>
                  </div>
                  <div class="col s7 margenTopDivChosen" >
                    <select id="idfamilia" name="idfamilia" class="browser-default chosen-select" onchange="verificaFamilia(this)">
                      <option value="" disabled selected>Selecciona una opción</option>
                      
                    </select>
                  </div>
                </div>
                <div class="col s6 tiporentaselect" style="display: none">
                  <div class="col s5" style="margin-left: -12px;">
                    <i class="material-icons prefix">format_list_bulleted</i>
                    <label for="estado">Tipo de Renta</label>
                  </div>
                  <div class="col s7 margenTopDivChosen" >
                    <select id="tiporenta" name="tiporenta" class="browser-default form-control-bmz tipoConsumibles">
                      <option value="" disabled selected>Selecciona tipo de cotización</option>
                      <option value="0"></option>
                      <option value="1">Individual</option>
                      <option value="2">Global</option>
                    </select>
                  </div>
                  
                </div>
                
                <div class="row col s12 costoglobalvaldiv" style="display: none;">
                  <div class="col s2">
                    <label class="active" for="eg_rm">Renta Monocromatica</label>
                    <input type="text" id="eg_rm" name="eg_rm" value="0">
                  </div>
                  <div class="col s2">
                    <label class="active" for="eg_rvm">Volumen incluido monocromatico</label>
                    <input type="text" id="eg_rvm" name="eg_rvm" value="0">
                  </div>
                  <div class="col s2">
                    <label class="active" for="eg_rpem">Precio Excedente monocromatico</label>
                    <input type="text" id="eg_rpem" name="eg_rpem" value="0">
                  </div>
                  <div class="col s2">
                    <label class="active" for="eg_rc">Renta Color</label>
                    <input type="text" id="eg_rc" name="eg_rc" value="0">
                  </div>
                  <div class="col s2">
                    <label class="active" for="eg_rvc">Volumen incluido color</label>
                    <input type="text" id="eg_rvc" name="eg_rvc" value="0">
                  </div>
                  <div class="col s2">
                    <label class="active" for="eg_rpec">Precio Excedent color</label>
                    <input type="text" id="eg_rpec" name="eg_rpec" value="0">
                  </div>
                  <div class="col s6"></div>
                  <div class="col s6">
                    <input type="checkbox" class="" id="eg_compra_consumible" name="eg_compra_consumible" value="1"/>
                    <label for="eg_compra_consumible">No Compra consumible color</label>
                  </div>
                </div>
                
                
              </div>
              <div class="row">
                <div class="input-field col s6" id="selectEquipos" style="display: none;">
                  <div class="col s5" style="margin-left: -12px;">
                    <i class="material-icons prefix">format_list_bulleted</i>
                    <label for="estado">Equipos</label>
                  </div>
                  <div class="col s7 margenTopDivChosen" >
                    <select id="equipoLista" name="equipoLista" class="browser-default chosen-select">
                      <option value="" disabled selected></option>
                      <?php foreach($equipos as $equipo){
                            echo '<option value="'.$equipo->id.'" >'.$equipo->modelo.'</option>';
                          }
                      ?>
                    </select>
                  </div>
                </div>
                
              </div>
              <br>
              <!-------------------------------------------------------------->
              <!------------------- CONSUMIBLES POR EQUIPO ------------------->
            
              <!-------------------------------------------------------------->
              <!------------------- REFACCIONES POR EQUIPO ------------------->
              
              <div class="row rowPolizas" id="rowPolizas1" name="rowPolizas1">
                <div class="input-field col s4" id="selectPolizas" style="display: none;">
                  <i class="material-icons prefix">format_list_bulleted</i>
                  <label for="estado">Pólizas Existentes</label>
                  <br>
                  <div class="col s12 margenTopDivChosen" >
                    <select id="polizasLista" name="polizasLista" class="browser-default chosen-select polizasLista" onchange="funcionDetalles($(this))">
                      <option value="" disabled selected></option>
                      <?php foreach($polizas as $poliza) {
                              echo '<option value="'.$poliza->id.'" data-visitas="'.$poliza->vigencia_clicks.'" >'.$poliza->nombre.'</option>';
                            }
                      ?>
                    </select>
                  </div>
                </div>
                <!--
                <div class="input-field col s4" style="display: none;" id="selectDetallesPolizas">
                  <i class="material-icons prefix">person</i>
                  <label for="estado">Modelos de Póliza</label>
                  <br>
                  <div class="col s12 margenTopDivChosen" >
                    <select id="detallesPoliza" name="detallesPoliza" class="browser-default chosen-select detallesPoliza" onchange="funcionDetalles2($(this))">
                      <option value="" disabled selected>Selecciona una opción</option>
                    </select>
                  </div>
                </div>
                <div class="input-field col s4" style="display: none;" id="selectDetallesPolizas2">
                  <i class="material-icons prefix">$</i>
                  <label for="estado">Costo</label>
                  <br>
                  <div class="col s10 margenTopDivChosen" >
                    <select id="detallesPolizacosto" name="detallesPolizacosto" class="browser-default chosen-select detallesPolizacosto">
                      <option value="" disabled selected>Selecciona una opción</option>
                    </select>
                  </div>
                  <div class="col s1" >
                    <a class="btn-floating waves-effect waves-light purple lightrn-1 btn tooltipped" data-position="top" data-tooltip="Nuevo" id="botonAgregarPoliza" name="botonAgregarPoliza">
                      <i class="material-icons">add</i>
                    </a>
                  </div>
                  <div class="col s1" >
                    <a class="btn-floating waves-effect waves-light red lightrn-1 btn tooltipped botonEliminarPoliza" data-position="top" data-tooltip="Eliminar" id="botonEliminarPoliza" name="botonEliminarPoliza">
                      <i class="material-icons">delete</i>
                    </a>
                  </div>
                </div>
                -->
              </div>
              
              <input type="hidden" name="idCliente" id="idCliente" value="<?php echo $idCliente; ?>">
            </form>
            <br><br>
              <div class="row">
                <ul class="collapsible" data-collapsible="accordion">
                  <li class="equipos_select" style="display: none;">
                    <div class="collapsible-header">Equipos</div>
                    <div class="collapsible-body">
                      <div class="col s6">
                        <div class="col s5" style="margin-left: -12px;">
                          <label>Seleccionar precio</label>
                        </div>
                        <div class="col s7">
                          <select  id="tipoprecioequipo" class="browser-default form-control-bmz">
                            <option value="0">General</option>
                            <option value="1">Reventa local</option>
                            <option value="2">Reventa foráneo</option>
                          </select>
                        </div>
                        
                        
                      </div>
                      <table id="tables_equipos_selected" class="display">
                        <thead>
                          <tr>
                            <th></th>
                            <th></th>
                            <th>Equipo</th>
                            <th>Consumibles</th>
                            <th>Accesorios</th>
                            <th></th>
                            <th></th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php foreach ($equiposimg as $item){
                          switch ($item->categoriaId) {
                          case 14:
                          $tipoimpresora=0;
                          break;
                          case 15:
                          $tipoimpresora=1;
                          break;
                          case 16:
                          $tipoimpresora=0;
                          break;
                          case 17:
                          $tipoimpresora=1;
                          break;
                          case 18:
                          $tipoimpresora=0;
                          break;
                          case 21:
                          $tipoimpresora=1;
                          break;
                          case 22:
                          $tipoimpresora=1;
                          break;
                          case 23:
                          $tipoimpresora=1;
                          break;
                          
                          default:
                          $tipoimpresora=0;
                          break;
                          }
                          ?>
                          <tr>
                            <td>
                              <input type="checkbox" class="equipo_check nombre" id="equipo_check_<?php echo $item->id ?>" value="<?php echo $tipoimpresora;?>"/>
                              <label for="equipo_check_<?php echo $item->id ?>"></label>
                              <input type="hidden" id="equipo_selected_all" value="<?php echo $item->id ?>"/>
                            </td>
                            <td style="width: 150px;">
                              <input type="text" id="equipo_cantidad" class="equipo_cantidad" value="1"  />
                            </td>
                            <td>
                              <label class="btn btn-info btn_agb_w has-badge-rounded has-badge-danger notificacion_tipped notificacion_tipped_<?php echo $item->id ?>" for="equipo_check_<?php echo $item->id ?>" data-badge="<?php echo $item->stockequipo;?>"
                                onmouseover="bodegasequipo(<?php echo $item->id ?>)" 
                                data-equipo="<?php echo $item->id ?>"
                                >
                                <?php echo $item->modelo ?>
                                
                              </label>
                            </td>
                            <td class="equipo_consumibleselected equipo_c_<?php echo $item->id ?>"></td>
                            <td class="equipo_accessoriosselected equipo_s_<?php echo $item->id ?>"></td>
                            <td class="equipo_rendimientoselected equipo_r_<?php echo $item->id ?>"></td>
                            <td><a class="btn btn-info"  style="padding: 0 5px;" onclick="addaccesorios(<?php echo $item->id ?>)"><i class="material-icons">list</i></a></td>
                          </tr>
                          <?php } ?>
                        </tbody>
                      </table>
                    </div>
                  </li>
                  <li class="selectEquiposConsumibles" style="display: none;">
                    <div class="collapsible-header">Consumibles</div>
                    <div class="collapsible-body">
                      <div class="row">
                        <div class="col s2">
                          <label>Equipos</label>
                          <select id="equipoListaConsumibles" name="equipoListaConsumibles" class="browser-default chosen-select equipoListaConsumibles" onchange="funcionConsumibles()">
                            <option value="" disabled selected></option>
                            <option value="0">General</option>
                            <?php foreach($equipos as $equipo){
                                echo '<option value="'.$equipo->id.'" >'.$equipo->modelo.'</option>';
                           } ?>
                          </select>
                        </div>
                        <div class="col s3">
                          <label>Disponibles</label>
                          <select id="toner" name="toner" class="browser-default chosen-select toner" onchange="funcionConsumiblesprecios()">
                            <option value="" disabled selected>Selecciona una opción</option>
                          </select>
                        </div>
                        <div class="col s3">
                          <label>Precios</label>
                          <select id="tonerp" name="tonerp" class="browser-default form-control-bmz tonerp">
                            <option value="" disabled selected>Selecciona una opción</option>
                          </select>
                        </div>
                        <div class="col s2">
                          <label>Cantidad</label>
                          <input type="number" name="" min="1" max="5" id="piezasc" class="piezas form-control-bmz">
                        </div>
                        <div class="col s2">
                          <a class="btn-floating waves-effect waves-light purple lightrn-1 btn tooltipped" data-position="top" data-tooltip="Agregar" id="botonAgregarConsumible" name="botonAgregarConsumible" onclick="addcosumibles()">
                              <i class="material-icons">system_update_alt</i>
                            </a>
                        </div>
                        <div class="col s12">
                          <table class="table bordered striped" id="table-addconsumibles">
                            <tbody class="tbody-addconsumibles">
                              
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </li>
                  <li class="selectEquiposRefacciones" style="display: none;">
                    <div class="collapsible-header">Refacciones</div>
                    <div class="collapsible-body">
                      <div class="row">
                        <div class="col s1">
                          <a class="btn-bmz material-icons prefix" onClick="addh_refacciones_cotizacion()" title="Equipos registrados">format_list_bulleted</a>
                        </div>
                        <div class="col s2">
                          <label>Equipos</label>
                          <select id="equipoListaRefacciones" name="equipoListaRefacciones" class="browser-default chosen-select equipoListaRefacciones" onchange="funcionRefacciones()">
                            <option value="" disabled selected></option>
                            <?php foreach($equipos as $equipo){ 

                                echo '<option value="'.$equipo->id.'" >'.$equipo->modelo.'</option>';
                             }  ?>
                          </select>
                        </div>
                        <div class="col s2">
                          <label>Serie</label>
                          <input type="text" name="seriee" id="seriee" class="form-control-bmz seriee" placeholder=" ">
                        </div>
                        <div class="col s2">
                          <label>Disponibles</label>
                          <select id="refaccion" name="refaccion" class="browser-default chosen-select refaccion" onchange="selectprecior()">
                            <option value="" disabled selected>Selecciona una opción</option>
                          </select>
                        </div>
                        <div class="col s2">
                          <label for="estado">Precios</label>
                          <div class="refaccionesp_mostrar"></div>
                        </div>
                        <div class="col s2">
                          <label>Cantidad</label>
                          <input type="number" name="piezasRefacion" min="1" max="5" class="piezasRefacion form-control-bmz" id="piezasRefacion">
                        </div>
                        <div class="col s1">
                          <a class="btn-floating waves-effect waves-light purple lightrn-1 btn tooltipped" data-position="top" data-tooltip="Agregar" onclick="addrefaccion()"><i class="material-icons">system_update_alt</i></a>
                        </div>
                        <div class="col s12">
                          <table class="table bordered striped" id="table-addrefacciones">
                            <tbody class="tbody-addrefacciones">
                              
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </li>
                  <li class="addpolizass" style="display: none;">
                    <div class="collapsible-header">Servicio</div>
                    <div class="collapsible-body">
                      <div class="row addpolizass" >
                        <a class="btn waves-effect waves-light cyan" id="btn_add_equipo_poliza">Agregar equipo</a>
                      </div>
                      <div class="row addpolizas">
                
                      </div>
                    </div>
                  </li>
                  <li class="accesorios_selected" style="display: none;">
                    <div class="collapsible-header">Accesorios</div>
                    <div class="collapsible-body">
                      <div class="row">
                        <div class="col s12">
                          <table class="table" id="table_venta_accesorios">
                            <thead>
                              <tr>
                                <th>Equipo</th>
                                <th>Accesorio</th>
                                <th>Precio</th>
                                <th>Cantidad</th>
                                <th></th>
                                <th></th>
                              </tr>
                              <tr>
                                <th>
                                  <select id="equipo_acce" class="browser-default chosen-select" onchange="obtenerabsesorios()">
                                      <option value="" disabled selected></option>
                                      <?php foreach($equipos as $equipo) { 
                                          echo '<option value="'.$equipo->id.'" >'.$equipo->modelo.'</option>';
                                       } ?>
                                  </select>
                                </th>
                                <th>
                                  <select id="accesorio_acce" class="browser-default chosen-select" onchange="obtenerabsesorioscosto()">
                                      
                                  </select>
                                </th>
                                <th>
                                  <input type="number" id="precio_acce" class="form-control-bmz" readonly>
                                </th>
                                <th>
                                  <input type="number" id="cantidad_acc" class="form-control-bmz">
                                </th>
                                <th></th>
                                
                                <th>
                                  <a class="btn-floating waves-effect waves-light purple lightrn-1 btn tooltipped" data-position="top" data-tooltip="Agregar" id="botonAgregarConsumible" name="botonAgregarConsumible" onclick="addaccesoriov()">
                                    <i class="material-icons">system_update_alt</i>
                                  </a>
                                </th>
                              </tr>
                            </thead>
                            <tbody class="tbody_acce"></tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </li>
                </ul>
              </div>
            <div class="row center fixed-action-btn" style="bottom: 50px; right: 19px;">
              <button class="btn waves-effect waves-light cyan" id="btn_cotizar">Aceptar</button>
            </div>
            <br><br>
          </div>
          
        </div>
      </div>
    </div>
    
  </div>
</section>
<div id="modalaccesorios" class="modal">
  <div class="modal-content ">
    
    <div class="row">
      <div class="col s12">
        <ul class="tabs">
          <li class="tab col s3"><a class="active" href="#test1">Accesorios</a></li>
          <li class="tab col s3"><a  href="#test2">Consumibles</a></li>
          <li class="tab col s3 solopararentas" style="display: none;"><a  href="#test3">Condiciones</a></li>
        </ul>
      </div>
      <div id="test1" class="col s12 modaltableaccesorios"></div>
      <div id="test2" class="col s12 modaltableconsu"></div>
      <div id="test3" class="col s12 modaltablerendimientos">
        <div class="row">
          <div class="col s3">
            <label>Volumen monocromatico</label>
            <input type="text" id="volumen_monocromatico" value="0">
          </div>
          <div class="col s1" style="margin-top: 25px;">
            X
          </div>
          <div class="col s3">
            <label>Costo</label>
            <input type="text" id="costo_monocromatico" class="form-control-b" value="0.25" readonly>
          </div>
          <div class="col s1" style="margin-top: 25px;">
            =
          </div>
          <div class="col s3">
            <label>Total</label>
            <div class="input-group-b">
              <span class="input-group-btn-b">
                <button class="btn-b btn-primary-b bootstrap-touchspin-down" type="button">$</button>
              </span>
              <input type="text" id="total_monocromatico" class="form-control-b" value="0.00" readonly>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col s3">
            <label>Volumen color</label>
            <input type="text" id="volumen_color" value="0">
          </div>
          <div class="col s1" style="margin-top: 25px;">
            X
          </div>
          <div class="col s3">
            <label>Costo</label>
            <input type="text" id="costo_color" class="form-control-b" value="2.5" readonly>
          </div>
          <div class="col s1" style="margin-top: 25px;">
            =
          </div>
          <div class="col s3">
            <label>Total</label>
            <div class="input-group-b">
              <span class="input-group-btn-b">
                <button class="btn-b btn-primary-b bootstrap-touchspin-down" type="button">$</button>
              </span>
              <input type="text" id="total_color" class="form-control-b" value="0.00" readonly>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col s4">
            <input type="checkbox" class="equipo_check nombre" id="compra_consumible" value="0"/>
            <label for="compra_consumible">No Compra consumible color</label>
          </div>
        </div>
        <div class="row">
          <input type="hidden" id="tiporendimientoselectede">
          <input type="hidden" id="tiporendimientoselectedm">
          <input type="hidden" id="tiporendimientoselectedc">
          <button  class="waves-effect waves-green green btn-flat addtabuladorrendimiento">Aceptar</button>
        </div>
      </div>
      
    </div>
  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
  </div>
</div>
<div id="modaleditor" class="modal" style="z-index: 1003; display: none; opacity: 0; transform: scaleX(0.7); top: 4%;">
  <div class="modal-content ">
    <h4>Editar Precio</h4>
    <div class="border_datos" id="fiscales_dato1">
      <div class="col s24 m24 l12">
        <div class="row ">
          <div class="row">
            <input id="idEquipon" type="hidden" value="185">
            <div class="input-field col s12">
              <input id="password" type="password" class="validate" autocomplete="new-password" required="">
              <label for="password">Password</label>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="modal-footer">
    <button class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</button>
    <button class="modal-action modal-close waves-effect waves-green green btn-flat editarcosto">Aceptar</button>
  </div>
</div>
<div id="modalserieshistorial" class="modal" style="min-height: 497px;">
  <div class="modal-content " style="padding-top: 10px;">
    <div class="row">
      <div class="col s12 m12 12 table-h-e-s">
      </div>
    </div>
  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat">Cerrar</a>
  </div>
</div>
<div id="modalrefaccionescotizacion" class="modal" style="min-height: 497px;">
  <div class="modal-content " style="padding-top: 10px;">
    <div class="row">
      <div class="col s12 m12 12 table-h-r-c">
      </div>
    </div>
  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat">Cerrar</a>
  </div>
</div>

<div id="modalservicioadd" class="modal" style="z-index: 1003; display: none; opacity: 0; transform: scaleX(0.7); top: 4%;">
  <div class="modal-content ">
    <h5>Seleccione servicio existente</h5>
    <div class="row">
      <div class="col s12 servicioaddcliente">
        
      </div>
    </div>
  </div>
  <div class="modal-footer">
    <button class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</button>
  </div>
</div>
