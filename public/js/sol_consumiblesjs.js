var base_url = $('#base_url').val();
var table;
$(document).ready(function($) {
	table = $('#tableg').DataTable();
	loadtable();
});

function loadtable(){
    var sol_con = $('#sol_con option:selected').val();
	table.destroy();
	table = $('#tableg').DataTable({
        //stateSave: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url+"index.php/Consumiblessol/getListado",
            type: "post",
            "data": {
                generado:sol_con,
                activo:1
            },
        },
        "columns": [
            {"data": "id"},
            {"data": "contratoid",
                render:function(data,type,row){
                    var html='';
                        html+=row.empresa;
                        if(row.tipo=='0'){
                            html+=' ('+row.folio+')';    
                        }
                        
                    return html;
                }
            },
            {"data": "modeloeq"},
            {"data": "serie"},
            {"data": "modelocon",
                render:function(data,type,row){
                    var html='';
                        if(row.tipo=='0'){
                            html+=' '+row.modelocon+'';    
                        }else{
                            html+='<a class="b-btn b-btn-primary" onclick="view_sol('+row.id+','+row.idenvio+')" ><i class="far fa-eye"></i></a> ';
                        }
                        
                    return html;
                }
            },
            {"data": "modelocon",
                render:function(data,type,row){
                    var html='';
                        if(row.tipo=='0'){
                            if(row.generado>0){
                                var checked_d=' disabled ';
                            }else{
                                var checked_d='';
                            }
                            //html+=' '+row.modelocon+''; 
                            html='<input type="checkbox" class="filled-in solicitud_checked" id="sol_'+row.id+'" value="'+row.id+'" ';
                            html+=' data-modcon="'+row.modelocon+'" ';
                            html+=' data-modconid="'+row.consumibleid+'" ';
                            html+=' data-idcliente="'+row.idCliente+'" ';
                            html+=' data-idcontrato="'+row.idcontrato+'" ';

                            html+=' data-equipoid="'+row.equipoid+'" ';
                            html+=' data-modeloeq="'+row.modeloeq+'" ';
                            html+=' data-equiporow="'+row.equiporow+'" ';

                            html+=' data-serieid="'+row.serieid+'" ';
                            html+=' data-serie="'+row.serie+'" ';
                            html+=' data-solid="'+row.id+'" ';

                            html+=checked_d;


                            html+='>';
                            html+='<label for="sol_'+row.id+'"></label>';   
                        }else{
                            
                        }
                        
                    return html;
                }
            },
            {"data": "cantidad",
                render:function(data,type,row){
                    var html='';
                        if(row.tipo=='0'){
                            html+=' '+row.cantidad+'';    
                        }
                        
                    return html;
                }
            },
            {"data": "reg"},
            {"data": null,
            	render:function(data,type,row){
            		var html='';
                        html+='<div class="btn_list"></div>';
                        if(row.tipo=='0'){
            		      html+='<a class="b-btn b-btn-primary tooltipped" data-position="top" data-delay="50" data-tooltip="Editar" onclick="edit_sol('+row.id+','+row.cantidad+')" ><i class="fas fa-pencil-alt"></i></a> ';
                        }
                        html+='<a class="b-btn b-btn-danger tooltipped" data-position="top" data-delay="50" data-tooltip="Eliminar" onclick="delete_sol('+row.id+')" ><i class="fas fa-trash-alt"></i></i></a> ';
                        html+='</div>';
                        if(row.generado>0){
                            html='';
                        }
                    return html;
            	}
        	},
            
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[25, 50, 100], [25, 50,100]],
        "searching": false
        
    }).on('draw',function(){
        //vericarprerentas();
    });
}
function delete_sol(id){
    var html='¿Confirma la eliminacion de la solicitud?';
        html+='<br><textarea class="form-control-bmz" id="motivodelete" placeholder="Motivo"></textarea>';
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: html,
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var motivo=$('#motivodelete').val();
                $.ajax({
                    type:'POST',
                    url: base_url+"Consumiblessol/delete_sol",
                    data: {
                        id:id,
                        motivo:motivo
                    },
                    success: function (data){
                        toastr["success"]("Solicitud eliminada");
                        loadtable();
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}
function edit_sol(id,can){
    var html='¿Confirma la Edicion de cantidad de la solicitud?';
        html+='<br><input type="number" class="form-control-bmz" id="cantidadedit" value="'+can+'" placeholder="Motivo">';
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: html,
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var can=$('#cantidadedit').val();
                $.ajax({
                    type:'POST',
                    url: base_url+"Consumiblessol/edit_sol",
                    data: {
                        id:id,
                        can:can
                    },
                    success: function (data){
                        toastr["success"]("Solicitud editada");
                        loadtable();
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}
function view_sol(idsol,idenvio){
    $.ajax({
        type:'POST',
        url: base_url+"Consumiblessol/view_sol",
        data: {
            idsol:idsol,
            idenvio:idenvio
        },
        success: function (data){
            $('.htmlinfosol').html(data);
            $('#modalinfosol').modal();
            $('#modalinfosol').modal('open');
        }
    });
}
var row=0;
function generarfolio(){
    $('.tbody_selec_con').html('');
    var consollis = $("#tableg tbody > tr");
    //var DATAa  = [];
    var num_con_selected=0;
    consollis.each(function(){  
        if ($(this).find("input[class*='solicitud_checked']").is(':checked')) {
            num_con_selected++;
            //item = {};                    
            //item ["soli"]  = $(this).find("input[class*='solicitud_checked']").val();
            
            //DATAa.push(item);
        }       
        
    });
    //INFOa  = new FormData();
    //aInfoa   = JSON.stringify(DATAa);
    if(num_con_selected>0){
        $('#modalgenerafolio').modal();
        $('#modalgenerafolio').modal('open');
        consollis.each(function(){  
            if ($(this).find("input[class*='solicitud_checked']").is(':checked')) {
                var modcon = $(this).find("input[class*='solicitud_checked']").data('modcon');
                var modconid = $(this).find("input[class*='solicitud_checked']").data('modconid');
                var idcliente = $(this).find("input[class*='solicitud_checked']").data('idcliente');
                var idcontrato = $(this).find("input[class*='solicitud_checked']").data('idcontrato');
                var equipoid = $(this).find("input[class*='solicitud_checked']").data('equipoid');
                var modeloeq = $(this).find("input[class*='solicitud_checked']").data('modeloeq');
                var equiporow = $(this).find("input[class*='solicitud_checked']").data('equiporow');
                var serieid = $(this).find("input[class*='solicitud_checked']").data('serieid');
                var serie = $(this).find("input[class*='solicitud_checked']").data('serie');
                var solid = $(this).find("input[class*='solicitud_checked']").data('solid');
                var html ='<tr class="row_selected_con_'+row+'">';
                        html+='<td>';
                        html+='<input type="hidden" value="'+modcon+'" id="modcon"> ';
                        html+='<input type="hidden" value="'+modconid+'" id="modconid"> ';
                        html+='<input type="hidden" value="'+idcliente+'" id="idcliente"> ';
                        html+='<input type="hidden" value="'+idcontrato+'" id="idcontrato"> ';
                        html+='<input type="hidden" value="'+equipoid+'" id="equipoid"> ';
                        html+='<input type="hidden" value="'+modeloeq+'" id="modeloeq"> ';
                        html+='<input type="hidden" value="'+equiporow+'" id="equiporow"> ';
                        html+='<input type="hidden" value="'+serieid+'" id="serieid"> ';
                        html+='<input type="hidden" value="'+serie+'" id="serie"> ';
                        html+='<input type="hidden" value="'+solid+'" id="solid"> ';
                        html+='<input type="hidden" value="" id="modconid_stock" class="modconid_stock_'+modconid+'"> ';
                        html+=modcon;
                        html+='</td>';
                        html+='<td class="info_modconid_stock info_modconid_stock_'+modconid+'" data-cons="'+modconid+'">';
                        html+='';
                        html+='</td>';
                        html+='<td>';
                        html+='<a class="b-btn b-btn-danger delete_pni" onclick="delete_coss('+row+')"><i class="fas fa-trash"></i></a>';
                        html+='</td>';
                    html+='</tr>';
                $('.tbody_selec_con').append(html);
            }       
            
        });
    }else{
        toastr["error"]("Seleccione por lo menos una solicitud");
    }
    
}
function delete_coss(row){
    $('.row_selected_con_'+row).remove()
}
function infostockconsumibles(){
    var bod = $('#nbodega option:selected').val();
    if(bod>0){
        $(".info_modconid_stock").each(function() {
            var conid = parseFloat($(this).data('cons'));
            verificarstock(conid,bod);
        });
    }
}
function verificarstock(conid,bod){
    $.ajax({
        type:'POST',
        url: base_url+"Consumiblessol/verificarstock",
        data: {
            conid:conid,
            bod:bod
        },
        success: function (data){
            var stock =parseInt(data);
            if(stock>0){
                $('.info_modconid_stock_'+conid).html('');
                $('.modconid_stock_'+conid).val(stock);
            }else{
                $('.info_modconid_stock_'+conid).html('No hay suficiente stock');
                $('.modconid_stock_'+conid).val(0);
            }
        }
    });
}
function generarf(){
    var bod = $('#nbodega option:selected').val();

    var consollis = $("#table_selec_con tbody > tr");
    var DATAc  = [];
    var num_con_selected=0;
    var vstock=1;
    consollis.each(function(){  
        item = {};
        item['cliente']=$(this).find("input[id*='idcliente']").val();
        item['serie']=$(this).find("input[id*='serieid']").val();
        item['contrato']=$(this).find("input[id*='idcontrato']").val();
        item['servicio']=0;
        item['consumible']=$(this).find("input[id*='modconid']").val();
        item['consumiblett']=$(this).find("input[id*='modcon']").val();
        //item['fecha']=''
        item['bodega']=bod;
        item['equipo']=$(this).find("input[id*='equiporow']").val();
        item['equipom']=$(this).find("input[id*='equipoid']").val();
        item['generado']=$(this).find("input[id*='solid']").val();
        var valorstock=$(this).find("input[id*='modconid_stock']").val();
        if(valorstock>0){

        }else{
            vstock=0;
        }
        DATAc.push(item);
    });
    INFOc  = new FormData();
    aInfoc   = JSON.stringify(DATAc);
    //===========================================
    if(vstock==1){
    
        var datos = 'consumibles='+aInfoc;
        $.ajax({
            type:'POST',
            url: base_url+"index.php/NewFolio/addconsumibles",
            data: datos,
            success: function (response){
                toastr["success"]("Se generaron los folios");
                loadtable();
                
                setTimeout(function(){ 
                    $('#modalgenerafolio').modal('close');
                    $('#nbodega').val(0);
                    $('.tbody_selec_con').html('');
                }, 1000);

            },
            error: function(response){
                toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema");
            }
        });
    }else{
        toastr["error"]("Uno de los consumibles no tiene suficiente stock");
    }
}