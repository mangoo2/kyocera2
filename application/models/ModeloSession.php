<?php
//ini_set("session.cookie_lifetime","86400");// 0 asta que el navegador se cierre o especificar los segundos, 86400 un dia
//ini_set("session.gc_maxlifetime","86400");// 0 asta que el navegador se cierre o especificar los segundos, 86400 un dia

$a=session_id();
if(empty($a)) session_start();
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloSession extends CI_Model {
    public function __construct() {
        parent::__construct();
        date_default_timezone_set('America/Mexico_City');
        $this->hora = date('G');//hora sin ceros iniciales
        $this->DB3 = $this->load->database('other3_db', TRUE); 
    }
    function getselectwheren($table,$where){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($where);
        $query=$this->db->get(); 
        return $query;
    }
    function login($usu,$pass) {
        $strq = "SELECT usu.UsuarioID,per.personalId,per.nombre, usu.perfilId, usu.contrasena 
                FROM usuarios as usu 
                INNER JOIN personal as per on per.personalId=usu.personalId where per.estatus=1 AND usu.Usuario ='".$usu."'";
        $count = 0;
        $passwo =0;
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $passwo =$row->contrasena;
            $id = $row->UsuarioID;
            $nom =$row->nombre;
            $perfil = $row->perfilId; 
            $idpersonal = $row->personalId; 
            $verificar = password_verify($pass,$passwo);
            if ($verificar) {
                $_SESSION['usuarioid']=$id;
                $_SESSION['usuario']=$nom;
                $_SESSION['perfilid']=$perfil;
                $_SESSION['idpersonal']=$idpersonal;
                
                
                $count=1;
                //$count=$passwo.'/'.$id.'/'.$nom.'/'.$perfil.'/'.$idpersonal;
            }
            /*
                 si es cero aqui se agregara los mismos parametros pero para el aseso de los residentes si es que lo piden
            */
        } 
        /*
        if ($count==0) {
            $residentes = $this->db->query("CALL SP_GET_SESSIONRESIDENTES('$usu','$pass')");
            $this->db->close();

            foreach ($residentes->result() as $row) {

                $id = $row->AspiranteId;
                $nom =$row->Nombre;
            
            
                $_SESSION['usuarioid']=$id;
                $_SESSION['usuario']=$nom;
                $_SESSION['perfilid']=3;
                $_SESSION['idpersonal']=0;
                $count=1;
            
           
            }
        }
        */
        
        echo $count;
        //echo $strq;
    }
    public function menus($perfil){
        $strq ="SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.PerfilId='$perfil' ORDER BY men.MenuId ASC";
        //$strq="CALL SP_GET_MENUS($perfil)";
        $query = $this->db->query($strq);
        return $query;

    }
    public function submenus($perfil,$menu){
        $strq ="SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon from menu_sub as menus, perfiles_detalles as perfd WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='$perfil' and menus.MenuId='$menu' ORDER BY menus.MenusubId ASC";
        $query = $this->db->query($strq);
        return $query;
    }
    /*
    public function noticacionequipos()
    {
        $sql = "SELECT c.empresa AS cliente,e.modelo AS equipo,p.nombre AS vendedor, v.reg FROM ventas AS v
                INNER JOIN ventas_has_detallesEquipos AS vde ON vde.idVenta = v.id
                INNER JOIN equipos AS e ON e.id = vde.idEquipo
                INNER JOIN clientes AS c ON c.id = v.idCliente
                INNER JOIN personal AS p ON p.personalId = v.id_personal
                WHERE vde.serie_estatus = 0";
        $query = $this->db->query($sql);
        return $query->result();        
    }
    public function noticacionaccesorios()
    {
        $sql = "SELECT c.empresa AS cliente,e.modelo AS equipo,p.nombre AS vendedor, v.reg FROM ventas AS v
                INNER JOIN ventas_has_detallesEquipos_accesorios AS vda ON vda.id_venta = v.id
                INNER JOIN equipos AS e ON e.id = vda.id_equipo
                INNER JOIN clientes AS c ON c.id = v.idCliente
                INNER JOIN personal AS p ON p.personalId = v.id_personal
                WHERE vda.serie_estatus = 0";
        $query = $this->db->query($sql);
        return $query->result();        
    }
    public function noticacionrefaccion()
    {
        $sql = "SELECT c.empresa AS cliente,e.modelo AS equipo,p.nombre AS vendedor, v.reg FROM ventas AS v
                INNER JOIN ventas_has_detallesRefacciones AS vdr ON vdr.idVentas = v.id
                INNER JOIN equipos AS e ON e.id = vdr.idEquipo
                INNER JOIN clientes AS c ON c.id = v.idCliente
                INNER JOIN personal AS p ON p.personalId = v.id_personal
                WHERE vdr.serie_estatus = 0";
        $query = $this->db->query($sql);
        return $query->result();        
    }
    // Rentas 
    public function noticacionrequipos()
    {
        $sql = "SELECT c.empresa AS cliente,e.modelo AS equipo,p.nombre AS vendedor, r.reg FROM rentas AS r
                INNER JOIN rentas_has_detallesEquipos AS vde ON vde.idRenta = r.id
                INNER JOIN equipos AS e ON e.id = vde.idEquipo
                INNER JOIN clientes AS c ON c.id = r.idCliente
                INNER JOIN personal AS p ON p.personalId = r.id_personal
                WHERE vde.serie_estatus = 0";
        $query = $this->db->query($sql);
        return $query->result();        
    }
    public function noticacionraccesorios()
    {
        $sql = "SELECT c.empresa AS cliente,e.modelo AS equipo,p.nombre AS vendedor, r.reg FROM rentas AS r
                INNER JOIN rentas_has_detallesEquipos_accesorios AS vda ON vda.idrentas = r.id
                INNER JOIN equipos AS e ON e.id = vda.id_equipo
                INNER JOIN clientes AS c ON c.id = r.idCliente
                INNER JOIN personal AS p ON p.personalId = r.id_personal
                WHERE vda.serie_estatus = 0";
        $query = $this->db->query($sql);
        return $query->result();        
    }
    */
    function notificaciones(){
        $sql = "SELECT c.empresa AS cliente,e.modelo AS equipo,p.nombre AS vendedor, v.reg,'1' as tipo 
                FROM ventas AS v
                INNER JOIN ventas_has_detallesEquipos AS vde ON vde.idVenta = v.id
                INNER JOIN equipos AS e ON e.id = vde.idEquipo
                INNER JOIN clientes AS c ON c.id = v.idCliente
                INNER JOIN personal AS p ON p.personalId = v.id_personal
                WHERE vde.serie_estatus = 0
                UNION
                SELECT c.empresa AS cliente,e.modelo AS equipo,p.nombre AS vendedor, v.reg,'2' as tipo
                FROM ventas AS v
                INNER JOIN ventas_has_detallesEquipos_accesorios AS vda ON vda.id_venta = v.id
                INNER JOIN equipos AS e ON e.id = vda.id_equipo
                INNER JOIN clientes AS c ON c.id = v.idCliente
                INNER JOIN personal AS p ON p.personalId = v.id_personal
                WHERE vda.serie_estatus = 0
                UNION
                SELECT c.empresa AS cliente,e.modelo AS equipo,p.nombre AS vendedor, v.reg,'3' as tipo 
                FROM ventas AS v
                INNER JOIN ventas_has_detallesRefacciones AS vdr ON vdr.idVentas = v.id
                INNER JOIN equipos AS e ON e.id = vdr.idEquipo
                INNER JOIN clientes AS c ON c.id = v.idCliente
                INNER JOIN personal AS p ON p.personalId = v.id_personal
                WHERE vdr.serie_estatus = 0
                UNION
                SELECT c.empresa AS cliente,e.modelo AS equipo,p.nombre AS vendedor, r.reg,'4' as tipo
                FROM rentas AS r
                INNER JOIN rentas_has_detallesEquipos AS vde ON vde.idRenta = r.id
                INNER JOIN equipos AS e ON e.id = vde.idEquipo
                INNER JOIN clientes AS c ON c.id = r.idCliente
                INNER JOIN personal AS p ON p.personalId = r.id_personal
                WHERE vde.serie_estatus = 0
                UNION
                SELECT c.empresa AS cliente,e.modelo AS equipo,p.nombre AS vendedor, r.reg,'5' as tipo
                FROM rentas AS r
                INNER JOIN rentas_has_detallesEquipos_accesorios AS vda ON vda.idrentas = r.id
                INNER JOIN equipos AS e ON e.id = vda.id_equipo
                INNER JOIN clientes AS c ON c.id = r.idCliente
                INNER JOIN personal AS p ON p.personalId = r.id_personal
                WHERE vda.serie_estatus = 0 
                ORDER BY `reg` DESC";
        $query = $this->db->query($sql);
        return $query;
    }


    /*
    public function notifi_ser_avc()
    {
        $sql = "SELECT avc.asId,c.modelo,avc.cantidad,avc.reg
                FROM asignacion_ser_avc AS avc
                INNER JOIN consumibles AS c ON c.id = avc.consumible
                WHERE avc.notificacion = 1";
        $query = $this->db->query($sql);
        return $query->result();  
    }
    public function notifi_ser_avr()
    {
        $sql = "SELECT avr.asId,r.nombre AS modelo,avr.cantidad,avr.reg
                FROM asignacion_ser_avr AS avr
                INNER JOIN refacciones AS r ON r.id = avr.refacciones
                WHERE avr.notificacion = 1";
        $query = $this->db->query($sql);
        return $query->result();  
    }
    */
    function notifi_ser_av_(){
        $sql = "SELECT avc.asId,c.modelo,avc.cantidad,avc.reg,'c' as tipo 
                FROM asignacion_ser_avc AS avc 
                INNER JOIN consumibles AS c ON c.id = avc.consumible 
                WHERE avc.notificacion = 1 
                UNION 
                SELECT avr.asId,r.nombre AS modelo,avr.cantidad,avr.reg,'r' as tipo 
                FROM asignacion_ser_avr AS avr 
                INNER JOIN refacciones AS r ON r.id = avr.refacciones 
                WHERE avr.notificacion = 1 
                ORDER BY `reg` DESC";
        $query = $this->db->query($sql);
        return $query;  
    }
    function verifi_asistencia($personal,$fecha){
        //log_message('error', 'hora: '.$this->hora);
        $horapermitida=22;//7pm 19 10pm 22
        //$horapermitida=9;
        if($this->hora>$horapermitida){
            $where_salida=" and hora_salida is null ";
        }else{
            $where_salida="";
        }
        //log_message('error',$this->hora);
        $sql = "SELECT * FROM `asistencias` WHERE personal='$personal' and fecha='$fecha' AND activo=1 $where_salida";
        $query = $this->db->query($sql);

        if($this->hora>$horapermitida){
            $query = $this->getselectwheren('asistencias_per_fhorario',array('personalid'=>$personal,'fecha'=>$fecha,'activo'=>1));
        }


        return $query; 
    }
    function personalview($id) {
        $strq = "SELECT *  FROM personal where personalId='$id'";
        $query = $this->db->query($strq);
        return $query;
    }
}
