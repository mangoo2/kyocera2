<?php 
	
	header("Pragma: public");
	header("Expires: 0");
	$filename = "$fecha Entregas.xls";
	header("Content-type: application/x-msdownload");
	header("Content-Disposition: attachment; filename=$filename");
	header("Pragma: no-cache");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	
	
	
?>
<table border="1">
	<thead>
		<tr>
			<th></th>
			<th>#</th>
			<th>Cliente</th>
			<th>Cantidad</th>
			<th>Producto</th>
			<th></th>
			<th>No. de Parte</th>
			<th></th>
			<td>Bodega</td>
			<td>Estatus</td>
			<td>Se le entrego</td>
			<td>Fecha de entrega</td>
			<td>Tecnico</td>
		</tr>
	</thead>
	<tbody>
		<?php //1
			foreach ($ventan->result() as $item) {
				$ventaid=$item->id;
				$cliente=$item->empresa;
				$tecnico=$item->tecnico;
				$this->ModeloCatalogos->db5_updateCatalogo('ventas',array('add_report'=>1),array('id'=>$ventaid));

				$resultadoequipos = $this->ModeloCatalogos->db6_getselectwheren('ventas_has_detallesEquipos',array('idVenta'=>$ventaid));
				foreach ($resultadoequipos->result() as $iteme) {
					$bodegaselect = $this->ModeloCatalogos->obtenerbodega($iteme->serie_bodega);
					
						$html='<tr>';
							$html.='<td>Venta</td>';
							$html.='<td>'.$ventaid.'</td>';
							$html.='<td>'.$cliente.'</td>';
							$html.='<td>'.$iteme->cantidad.'</td>';
							$html.='<td>'.$iteme->modelo.'</td>';
							$html.='<td>';
								
									$resuleserie=$this->ModeloCatalogos->getequiposerie($iteme->id);
									foreach ($resuleserie->result() as $itemse) { 
										$html.=$itemse->serie.'<br>';
									}
							$html.='</td>';
							$html.='<td></td>';
							$html.='<td>'.utf8_encode($iteme->comentario).'</td>';
							$html.='<td>'.$bodegaselect.'</td>';
							$html.='<td>';
									if($iteme->entregado_status==1){ $html.='Entregado';}
							$html.='</td>';
							$html.='<td>';
										if($iteme->entregado_status==1){ 
											$html.=utf8_encode($this->ModeloCatalogos->obtenerpersonal($iteme->entregado_personal));
										}
							$html.='</td>';
							$html.='<td>'.$fecha.'</td>';
							$html.='<td>'.$tecnico.'</td>';
						$html.='</tr>';
						echo $html;
					
				}
				$resultadoequipos_dev = $this->ModeloCatalogos->getselectwheren('ventas_has_detallesEquipos_dev',array('idVenta'=>$ventaid));//dev
				foreach ($resultadoequipos_dev->result() as $iteme) {
					$bodegaselect = $this->ModeloCatalogos->obtenerbodega($iteme->serie_bodega);
					?>
						<tr >
							<td>Venta</td>
							<td><?php echo $ventaid;?></td>
							<td><?php echo $cliente;?></td>
							<td><?php echo $iteme->cantidad; ?></td>
							<td><?php echo $iteme->modelo;?></td>
							<td><?php 
									$resuleserie=$this->ModeloCatalogos->getequiposerie($iteme->id);
									foreach ($resuleserie->result() as $itemse) { 
										echo $itemse->serie.'<br>';
									}
							?> <b style="color:red">(DN)</b></td>
							<td></td>
							<td><?php echo utf8_encode($iteme->comentario);?></td>
							<td><?php echo $bodegaselect;?></td>
							<td><?php if($iteme->entregado_status==1){ echo 'Entregado';}?></td>
							<td><?php if($iteme->entregado_status==1){ 
									echo utf8_encode($this->ModeloCatalogos->obtenerpersonal($iteme->entregado_personal));
							}?></td>
							<td><?php echo $fecha;?></td>
							<td><?php echo $tecnico;?></td>
						</tr>
					<?php
				}
				$resultadoaccesorios = $this->ModeloCatalogos->accesoriosventa($ventaid);
				foreach ($resultadoaccesorios->result() as $itema) {
					?>
						<tr>
							<td>Venta</td>
							<td><?php echo $ventaid;?></td>
							<td><?php echo $cliente;?></td>
							<td><?php echo $itema->cantidad; ?></td>
							<td><?php echo $itema->nombre;?></td>
							<td><?php 
									$resuleserie=$this->ModeloCatalogos->getaccesorioserie($itema->id_accesoriod);
									foreach ($resuleserie->result() as $itemse) { 
										echo $itemse->serie.'<br>';
									}
							?></td>
							<td><?php echo $itema->no_parte;?></td>
							<td><?php echo utf8_encode($itema->comentario);?></td>
							<td><?php echo $itema->bodega;?></td>
							<td><?php if($itema->entregado_status==1){echo 'Entregado';};?></td>
							<td><?php if($itema->entregado_status==1){echo utf8_encode($itema->personal.' '.$itema->apellido_paterno);}?></td>
							<td><?php echo $fecha;?></td>
							<td><?php echo $tecnico;?></td>
						</tr>
					<?php
				}
				$resultadoaccesorios_dev = $this->ModeloCatalogos->accesoriosventa_dev($ventaid);//dev
				foreach ($resultadoaccesorios_dev->result() as $itema) {

					?>
						<tr>
							<td>Venta</td>
							<td><?php echo $ventaid;?></td>
							<td><?php echo $cliente;?></td>
							<td><?php echo $itema->cantidad; ?></td>
							<td><?php echo $itema->nombre;?></td>
							<td><?php 
									$resuleserie=$this->ModeloCatalogos->getaccesorioserie($itema->id_accesoriod);
									foreach ($resuleserie->result() as $itemse) { 
										echo $itemse->serie.'<br>';
									}
							?> <b style="color:red">(DN)</b></td>
							<td><?php echo $itema->no_parte;?></td>
							<td><?php echo utf8_encode($itema->comentario);?></td>
							<td><?php echo $itema->bodega;?></td>
							<td><?php if($itema->entregado_status==1){echo 'Entregado';};?></td>
							<td><?php if($itema->entregado_status==1){echo utf8_encode($itema->personal.' '.$itema->apellido_paterno);}?></td>
							<td><?php echo $fecha;?></td>
							<td><?php echo $tecnico;?></td>
						</tr>
					<?php
				}
				$resultadoconsumibles = $this->ModeloCatalogos->consumiblesventa($ventaid);
				foreach ($resultadoconsumibles->result() as $itemc) {
					
						$html='<tr>';
							$html.='<td>Venta</td>';
							$html.='<td>'.$ventaid.'</td>';
							$html.='<td>'.$cliente.'</td>';
							$html.='<td>'.$itemc->cantidad.'</td>';
							$html.='<td>'.$itemc->modelo.'</td>';
							$html.='<td></td>';
							$html.='<td>'.$itemc->parte.'</td>';
							$html.='<td>'.utf8_encode($itemc->comentario).'</td>';
							$html.='<td>'.$itemc->bodega.'</td>';
							$html.='<td>';
									if($itemr->entregado_status==1){$html.='Entregado';}
							$html.='</td>';
							$html.='<td>';
									if($itemr->entregado_status==1){$html.=utf8_encode($itemr->personal.' '.$itemr->apellido_paterno);}
							$html.='</td>';
							$html.='<td>'.$fecha.'</td>';
							$html.='<td>'.$tecnico.'</td>';
						$html.='</tr>';
						echo $html;
					
				}
				$resultadoconsumibles_dev = $this->ModeloCatalogos->consumiblesventa_dev($ventaid);//dev
				foreach ($resultadoconsumibles_dev->result() as $itemc) {
					?>
						<tr>
							<td>Venta</td>
							<td><?php echo $ventaid;?></td>
							<td><?php echo $cliente;?></td>
							<td><?php echo $itemc->cantidad; ?></td>
							<td><?php echo $itemc->modelo;?></td>
							<td> <b style="color:red">(DN)</b></td>
							<td><?php echo $itemc->parte;?></td>
							<td><?php echo utf8_encode($itemc->comentario);?></td>
							<td><?php echo $itemc->bodega;?></td>
							<td></td>
							<td></td>
							<td><?php echo $fecha;?></td>
							<td><?php echo $tecnico;?></td>
						</tr>
					<?php
				}
				$consumiblesventadetalles = $this->ModeloCatalogos->consumiblesventadetalles($ventaid);
				foreach ($consumiblesventadetalles->result() as $itemc) {
					
						$html='<tr>';
							$html.='<td>Venta</td>';
							$html.='<td>'.$ventaid.'</td>';
							$html.='<td>'.$cliente.'</td>';
							$html.='<td>'.$itemc->cantidad.'</td>';
							$html.='<td>'.$itemc->modelo.'</td>';
							$html.='<td>'.$itemc->foliotext.'</td>';
							$html.='<td>'.$itemc->parte.'</td>';
							$html.='<td>'.utf8_encode($itemc->comentario).'</td>';
							$html.='<td>'.$itemc->bodega.'</td>';
							$html.='<td>';
									if($itemc->entregado_status==1){$html.='Entregado';}		
							$html.='</td>';
							$html.='<td>';
								if($itemc->entregado_status==1){$html.=utf8_encode($itemc->personal.' '.$itemc->apellido_paterno);}		
							$html.='</td>';
							$html.='<td>'.$fecha.'</td>';
							$html.='<td>'.$tecnico.'</td>';
						$html.='</tr>';
						echo $html;
					
				}
				$consumiblesventadetalles = $this->ModeloCatalogos->consumiblesventadetalles_dev($ventaid);
				foreach ($consumiblesventadetalles->result() as $itemc) {
					?>
						<tr>
							<td>Venta</td>
							<td><?php echo $ventaid;?></td>
							<td><?php echo $cliente;?></td>
							<td><?php echo $itemc->cantidad; ?></td>
							<td><?php echo $itemc->modelo;?></td>
							<td><?php echo $itemc->foliotext;?> <b style="color:red">(DN)</b></td>
							<td><?php echo $itemc->parte;?></td>
							<td><?php echo utf8_encode($itemc->comentario);?></td>
							<td><?php echo $itemc->bodega;?></td>
							<td></td>
							<td></td>
							<td><?php echo $fecha;?></td>
							<td><?php echo $tecnico;?></td>
						</tr>
					<?php
				}
				$ventadetallesrefacion = $this->ModeloCatalogos->ventas_has_detallesRefacciones($ventaid);
				foreach ($ventadetallesrefacion->result() as $itemr) {
					?>
						<tr>
							<td>Venta</td>
							<td><?php echo $ventaid;?></td>
							<td><?php echo $cliente;?></td>
							<td><?php echo $itemr->cantidad; ?></td>
							<td><?php echo $itemr->modelo;?></td>
							<td><?php echo str_replace("(VU)", ' <b style="color:red">(VU)</b>', $itemr->series);?></td>
							<td><?php echo $itemr->codigo;?></td>
							<td><?php echo $itemr->comentario;?></td>
							<td><?php echo $itemr->bodega;?></td>
							<td><?php if($itemr->entregado_status==1){echo 'Entregado';};?></td>
							<td><?php if($itemr->entregado_status==1){echo utf8_encode($itemr->personal.' '.$itemr->apellido_paterno);}?></td>
							<td><?php echo $fecha;?></td>
							<td><?php echo $tecnico;?></td>
						</tr>
					<?php
				}
				$ventadetallesrefacion = $this->ModeloCatalogos->ventas_has_detallesRefacciones_dev($ventaid);//dev
				foreach ($ventadetallesrefacion->result() as $itemr) {
					?>
						<tr>
							<td>Venta</td>
							<td><?php echo $ventaid;?></td>
							<td><?php echo $cliente;?></td>
							<td><?php echo $itemr->cantidad; ?></td>
							<td><?php echo $itemr->modelo;?></td>
							<td> <b style="color:red">(DN)</b></td>
							<td><?php echo $itemr->codigo;?></td>
							<td><?php echo $itemr->comentario;?></td>
							<td><?php echo $itemr->bodega;?></td>
							<td><?php if($itemr->entregado_status==1){echo 'Entregado';};?></td>
							<td><?php if($itemr->entregado_status==1){echo utf8_encode($itemr->personal.' '.$itemr->apellido_paterno);}?></td>
							<td><?php echo $fecha;?></td>
							<td><?php echo $tecnico;?></td>
						</tr>
					<?php
				}
			}
			 //4
			foreach ($ventac->result() as $item) {
				$ventaid=$item->id;
				$cliente=$item->empresa;

				$equipo=$item->equipo;
                $consumibles=$item->consumibles;
                $refacciones=$item->refacciones;
                $tecnico=$item->tecnico;
                
                $this->ModeloCatalogos->db5_updateCatalogo('ventacombinada',array('add_report'=>1),array('combinadaId'=>$ventaid));

				$resultadoequipos = $this->ModeloCatalogos->db6_getselectwheren('ventas_has_detallesEquipos',array('idVenta'=>$equipo));
				foreach ($resultadoequipos->result() as $iteme) {
					$bodegaselect = $this->ModeloCatalogos->obtenerbodega($iteme->serie_bodega);
					?>
						<tr>
							<td>Venta Combinada</td>
							<td><?php echo $equipo;?></td>
							<td><?php echo $cliente;?></td>
							<td><?php echo $iteme->cantidad; ?></td>
							<td><?php echo $iteme->modelo;?></td>
							<td><?php 
									$resuleserie=$this->ModeloCatalogos->getequiposerie($iteme->id);
									foreach ($resuleserie->result() as $itemse) { 
										echo $itemse->serie.'<br>';
									}
							?></td>
							<td></td>
							<td><?php echo utf8_encode($iteme->comentario);?></td>
							<td><?php echo $bodegaselect;?></td>
							<td><?php if($iteme->entregado_status==1){ echo 'Entregado';}?></td>
							<td><?php if($iteme->entregado_status==1){ 
									echo utf8_encode($this->ModeloCatalogos->obtenerpersonal($iteme->entregado_personal));
							}?></td>
							<td><?php echo $fecha;?></td>
							<td><?php echo $tecnico;?></td>
						</tr>
					<?php
				}
				$resultadoequipos_dev = $this->ModeloCatalogos->getselectwheren('ventas_has_detallesEquipos_dev',array('idVenta'=>$equipo));//dev
				foreach ($resultadoequipos_dev->result() as $iteme) {
					$bodegaselect = $this->ModeloCatalogos->obtenerbodega($iteme->serie_bodega);
					?>
						<tr>
							<td>Venta</td>
							<td><?php echo $ventaid;?></td>
							<td><?php echo $cliente;?></td>
							<td><?php echo $iteme->cantidad; ?></td>
							<td><?php echo $iteme->modelo;?></td>
							<td><?php 
									$resuleserie=$this->ModeloCatalogos->getequiposerie($iteme->id);
									foreach ($resuleserie->result() as $itemse) { 
										echo $itemse->serie.'<br>';
									}
							?>  <b style="color:red">(DN)</b></td>
							<td></td>
							<td><?php echo utf8_encode($iteme->comentario);?></td>
							<td><?php echo $bodegaselect;?></td>
							<td><?php if($iteme->entregado_status==1){ echo 'Entregado';}?></td>
							<td><?php if($iteme->entregado_status==1){ 
									echo utf8_encode($this->ModeloCatalogos->obtenerpersonal($iteme->entregado_personal));
							}?></td>
							<td><?php echo $fecha;?></td>
							<td><?php echo $tecnico;?></td>
						</tr>
					<?php
				}
				$resultadoaccesorios = $this->ModeloCatalogos->accesoriosventa($equipo);
				foreach ($resultadoaccesorios->result() as $itema) {
					?>
						<tr>
							<td>Venta Combinada</td>
							<td><?php echo $equipo;?></td>
							<td><?php echo $cliente;?></td>
							<td><?php echo $itema->cantidad; ?></td>
							<td><?php echo $itema->nombre;?></td>
							<td><?php 
									$resuleserie=$this->ModeloCatalogos->getaccesorioserie($itema->id_accesoriod);
									foreach ($resuleserie->result() as $itemse) { 
										echo $itemse->serie.'<br>';
									}
							?></td>
							<td><?php echo $itema->no_parte;?></td>
							<td><?php echo utf8_encode($itema->comentario);?></td>
							<td><?php echo $itema->bodega;?></td>
							<td><?php if($itema->entregado_status==1){echo 'Entregado';};?></td>
							<td><?php if($itema->entregado_status==1){echo utf8_encode($itema->personal.' '.$itema->apellido_paterno);}?></td>
							<td><?php echo $fecha;?></td>
							<td><?php echo $tecnico;?></td>
						</tr>
					<?php
				}
				$resultadoaccesorios_dev = $this->ModeloCatalogos->accesoriosventa_dev($equipo);//dev
				foreach ($resultadoaccesorios_dev->result() as $itema) {
					?>
						<tr>
							<td>Venta</td>
							<td><?php echo $equipo;?></td>
							<td><?php echo $cliente;?></td>
							<td><?php echo $itema->cantidad; ?></td>
							<td><?php echo $itema->nombre;?></td>
							<td><?php 
									$resuleserie=$this->ModeloCatalogos->getaccesorioserie($itema->id_accesoriod);
									foreach ($resuleserie->result() as $itemse) { 
										echo $itemse->serie.'<br>';
									}
							?> <b style="color:red">(DN)</b></td>
							<td><?php echo $itema->no_parte;?></td>
							<td><?php echo utf8_encode($itema->comentario);?></td>
							<td><?php echo $itema->bodega;?></td>
							<td><?php if($itema->entregado_status==1){echo 'Entregado';};?></td>
							<td><?php if($itema->entregado_status==1){echo utf8_encode($itema->personal.' '.$itema->apellido_paterno);}?></td>
							<td><?php echo $fecha;?></td>
							<td><?php echo $tecnico;?></td>
						</tr>
					<?php
				}
				$resultadoconsumibles = $this->ModeloCatalogos->consumiblesventa($equipo);
				foreach ($resultadoconsumibles->result() as $itemc) {
					?>
						<tr>
							<td>Venta Combinada</td>
							<td><?php echo $equipo;?></td>
							<td><?php echo $cliente;?></td>
							<td><?php echo $itemc->cantidad; ?></td>
							<td><?php echo $itemc->modelo;?></td>
							<td></td>
							<td><?php echo $itemc->parte;?></td>
							<td><?php echo utf8_encode($itemc->comentario);?></td>
							<td><?php echo $itemc->bodega;?></td>
							<td><?php if($itemc->entregado_status==1){echo 'Entregado';};?></td>
							<td><?php if($itemc->entregado_status==1){echo utf8_encode($itemc->personal.' '.$itemc->apellido_paterno);}?></td>
							<td><?php echo $fecha;?></td>
							<td><?php echo $tecnico;?></td>
						</tr>
					<?php
				}
				$resultadoconsumibles_dev = $this->ModeloCatalogos->consumiblesventa_dev($equipo);//dev
				foreach ($resultadoconsumibles_dev->result() as $itemc) {
					?>
						<tr>
							<td>Venta</td>
							<td><?php echo $ventaid;?></td>
							<td><?php echo $cliente;?></td>
							<td><?php echo $itemc->cantidad; ?></td>
							<td><?php echo $itemc->modelo;?></td>
							<td><b style="color:red">(DN)</b></td>
							<td><?php echo $itemc->parte;?></td>
							<td><?php echo utf8_encode($itemc->comentario);?></td>
							<td><?php echo $itemc->bodega;?></td>
							<td></td>
							<td></td>
							<td><?php echo $fecha;?></td>
							<td><?php echo $tecnico;?></td>
						</tr>
					<?php
				}
				$consumiblesventadetalles = $this->ModeloCatalogos->consumiblesventadetalles($consumibles);
				foreach ($consumiblesventadetalles->result() as $itemc) {
					
						$html='<tr>';
							$html.='<td>Venta Combinada</td>';
							$html.='<td>'.$equipo.'</td>';
							$html.='<td>'.$cliente.'</td>';
							$html.='<td>'.$itemc->cantidad.'</td>';
							$html.='<td>'.$itemc->modelo.'</td>';
							$html.='<td>'.$itemc->foliotext.'</td>';
							$html.='<td>'.$itemc->parte.'</td>';
							$html.='<td>'.utf8_encode($itemc->comentario).'</td>';
							$html.='<td>'.$itemc->bodega.'</td>';
							$html.='<td>';
								if($itemc->entregado_status==1){$html.='Entregado';}
							$html.='</td>';
							$html.='<td>';
								if($itemc->entregado_status==1){$html.=utf8_encode($itemc->personal.' '.$itemc->apellido_paterno);}
							$html.='</td>';
							$html.='<td>'.$fecha.'</td>';
							$html.='<td>'.$tecnico.'</td>';
						$html.='</tr>';
						echo $html;
					
				}
				$consumiblesventadetalles = $this->ModeloCatalogos->consumiblesventadetalles_dev($ventaid);//dev
				foreach ($consumiblesventadetalles->result() as $itemc) {
					?>
						<tr>
							<td>Venta</td>
							<td><?php echo $equipo;?></td>
							<td><?php echo $cliente;?></td>
							<td><?php echo $itemc->cantidad; ?></td>
							<td><?php echo $itemc->modelo;?></td>
							<td><?php echo $itemc->foliotext;?> <b style="color:red">(DN)</b></td>
							<td><?php echo $itemc->parte;?></td>
							<td><?php echo utf8_encode($itemc->comentario);?></td>
							<td><?php echo $itemc->bodega;?></td>
							<td><?php if($itemc->entregado_status==1){echo 'Entregado';};?></td>
							<td><?php if($itemc->entregado_status==1){echo utf8_encode($itemc->personal.' '.$itemc->apellido_paterno);}?></td>
							<td><?php echo $fecha;?></td>
							<td><?php echo $tecnico;?></td>
						</tr>
					<?php
				}
				$ventadetallesrefacion = $this->ModeloCatalogos->ventas_has_detallesRefacciones($refacciones);
				foreach ($ventadetallesrefacion->result() as $itemr) {
					
						$html='<tr>';
							$html.='<td>Venta Combinada</td>';
							$html.='<td>'.$equipo.'</td>';
							$html.='<td>'.$cliente.'</td>';
							$html.='<td>'.$itemr->cantidad.'</td>';
							$html.='<td>'.$itemr->modelo.'</td>';
							$html.='<td>'.str_replace("(VU)", ' <b style="color:red">(VU)</b>', $itemr->series).'</td>';
							$html.='<td>'.$itemr->codigo.'</td>';
							$html.='<td>'.utf8_encode($itemr->comentario).'</td>';
							$html.='<td>'.$itemr->bodega.'</td>';
							$html.='<td>';
								if($itemr->entregado_status==1){$html.='Entregado';}		
							$html.='</td>';
							$html.='<td>';
								if($itemr->entregado_status==1){$html.=utf8_encode($itemr->personal.' '.$itemr->apellido_paterno);}		
							$html.='</td>';
							$html.='<td>'.$fecha.'</td>';
							$html.='<td>'.$tecnico.'</td>';
						$html.='</tr>';
						echo $html;
					
				}
				$ventadetallesrefacion = $this->ModeloCatalogos->ventas_has_detallesRefacciones_dev($refacciones);//dev
				foreach ($ventadetallesrefacion->result() as $itemr) {
					?>
						<tr>
							<td>Venta</td>
							<td><?php echo $equipo;?></td>
							<td><?php echo $cliente;?></td>
							<td><?php echo $itemr->cantidad; ?></td>
							<td><?php echo $itemr->modelo;?></td>
							<td><b style="color:red">(DN)</b></td>
							<td><?php echo $itemr->codigo;?></td>
							<td><?php echo utf8_encode($itemr->comentario);?></td>
							<td><?php echo $itemr->bodega;?></td>
							<td><?php if($itemr->entregado_status==1){echo 'Entregado';};?></td>
							<td><?php if($itemr->entregado_status==1){echo utf8_encode($itemr->personal.' '.$itemr->apellido_paterno);}?></td>
							<td><?php echo $fecha;?></td>
							<td><?php echo $tecnico;?></td>
						</tr>
					<?php
				}
			}
			//5
			foreach ($ventarh->result() as $item) {
				$id =$item->renta_rh;
				$idhostorial=$item->id_rh;

				$empresa = $item->empresa;
				$rentaventash = $this->ModeloCatalogos->rentaventasdh($id,$idhostorial);
            	$rentavacessoriosh = $this->ModeloCatalogos->rentavacessoriosh($id,$idhostorial);
            	$rentavconsumiblesh = $this->ModeloCatalogos->rentavconsumiblesh($id,$idhostorial);
            	$this->ModeloCatalogos->db5_updateCatalogo('rentas_historial',array('add_report'=>1),array('id'=>$idhostorial));
            	$tecnico='';
            	if($item->idconsul_rh>0){
            		$rentaventashe = $this->ModeloCatalogos->vertecnicorh($item->idconsul_rh);
                    foreach ($rentaventashe->result() as $iteme) {
                        $tecnico=$iteme->nombre.' '.$iteme->apellido_paterno.' '.$iteme->apellido_materno;
                    }
            	}

            	foreach ($rentaventash->result() as $item) { 
            		$datosseries = $this->ModeloCatalogos->rentaventasdseries($item->id);
            		?>
            		<tr>
            			<td>Renta Historial</td>
            			<td><?php echo $idhostorial;?></td>
            			<td><?php echo $empresa;?></td>
            			<td><?php echo $item->cantidad;?></td>
            			<td><?php echo $item->modelo;?></td>
            			<td><?php 
            				foreach($datosseries->result() as $items) {
								echo $items->serie.'<br>';
							}
            			?></td>
            			<td></td>
            			<td></td>
            			<td></td>
            			<td><?php if($item->entregado_status==1){echo 'Entregado';};?></td>
						<td><?php if($item->entregado_status==1){echo utf8_encode($item->personal.' '.$item->apellido_paterno);}?></td>
						<td><?php echo $fecha;?></td>
						<td><?php echo $tecnico;?></td>
            		</tr>
            	<?php }
            	foreach ($rentavacessoriosh->result() as $item) {  
            		$datosseries = $this->ModeloCatalogos->rentaventasaccessoriosseries($item->id_accesoriod); 
            		?>
            		<tr>
            			<td>Renta Historial</td>
            			<td><?php echo $idhostorial;?></td>
            			<td><?php echo $empresa;?></td>
            			<td><?php echo $item->cantidad;?></td>
            			<td><?php echo $item->nombre;?></td>
            			<td><?php 
            				foreach($datosseries->result() as $items) {
								echo $items->serie.'<br>';
							}
            			?></td>
            			<td></td>
            			<td></td>
            			<td></td>
            			<td><?php if($item->entregado_status==1){echo 'Entregado';};?></td>
						<td><?php if($item->entregado_status==1){echo utf8_encode($item->personal.' '.$item->apellido_paterno);}?></td>
						<td><?php echo $fecha;?></td>
						<td><?php echo $tecnico;?></td>
            		</tr>
            	<?php }
            	foreach ($rentavconsumiblesh->result() as $itemch) { ?>
            		<tr>
            			<td>Renta Historial</td>
            			<td><?php echo $idhostorial;?></td>
            			<td><?php echo $empresa;?></td>
            			<td><?php echo $itemch->cantidad;?></td>
            			<td><?php echo $itemch->modelo;?></td>
            			<td><?php echo $itemch->folios;?></td>
            			<td></td>
            			<td></td>
            			<td></td>
            			<td><?php if($itemch->entregado_status==1){echo 'Entregado';};?></td>
						<td><?php if($itemch->entregado_status==1){echo utf8_encode($itemch->personal.' '.$itemch->apellido_paterno);}?></td>
						<td><?php echo $fecha;?></td>
						<td><?php echo $tecnico;?></td>
            		</tr>
            	<?php }

			}
			 //2
			foreach ($ventar->result() as $item) { 
				$idrenta=$item->id;
				$empresa = $item->empresa;
				$domicilio_entrega = $item->domicilio_entrega;
				$rentaventas = $this->ModeloCatalogos->rentaventasd($idrenta);
	            $rentaventasacc = $this->ModeloCatalogos->rentaventasaccesoriosd($idrenta);
	            $rentaventasacc_dev = $this->ModeloCatalogos->rentaventasaccesoriosd_dev($idrenta);
	            $this->ModeloCatalogos->db5_updateCatalogo('rentas',array('add_report'=>1),array('id'=>$idrenta));
	            $tecnico='';
	            foreach ($rentaventas->result() as $item) {  
	            	$equipo_id = $item->idEquipo;
	            	$renta_id = $item->idRenta;
	            	
	            	$tecnico = $item->tecnico;

	            	$datosseries = $this->ModeloCatalogos->rentaventasdseries($item->id); 
	            	?>

	            	<tr>
	        			<td>Renta </td>
	        			<td><?php echo $idrenta;?></td>
	        			<td><?php echo $empresa;?></td>
	        			<td><?php echo $item->cantidad;?></td>
	        			<td><?php echo $item->modelo;?></td>
	        			<td><?php 
	        				foreach($datosseries->result() as $items) { 
	        					echo $items->serie.'<br>';
							}
	        			?></td>
	        			<td><?php echo $item->noparte;?></td>
	        			<td><?php echo utf8_encode($domicilio_entrega);?></td>
	        			<td><?php echo $item->bodega;?></td>
	        			<td><?php if($item->entregado_status==1){echo 'Entregado';};?></td>
						<td><?php if($item->entregado_status==1){echo utf8_encode($item->personal.' '.$item->apellido_paterno);}?></td>
						<td><?php echo $fecha;?></td>
						<td><?php echo $tecnico;?></td>
	        		</tr>

	            	<?php
	            	$rentaequipos_consumible = $this->ModeloCatalogos->rentaequipos_consumible($renta_id,$equipo_id,$item->id);
	            	foreach ($rentaequipos_consumible->result() as $itemc) {
	            		?>
	            			<tr>
			        			<td>Renta </td>
			        			<td><?php echo $idrenta;?></td>
			        			<td><?php echo $empresa;?></td>
			        			<td><?php echo $itemc->cantidad;?></td>
			        			<td><?php echo $itemc->modelo;?></td>
			        			<td><?php 
			        				$where = array('idrenta'=>$renta_id,'idconsumible'=>$itemc->id_consumibles);
    									$contrato_folio = $this->ModeloCatalogos->db2_getselectwheren('contrato_folio',$where);
    									foreach ($contrato_folio->result() as $itemx) {
    										if($itemx->idrelacion>0){
       
		                                }else{
		                                     echo $itemx->foliotext.',';
		                                }
	                                }
	                                //============================ cuando ya todos tengan relacion remplazara a la anterior
	                                $contrato_folio2 = $this->ModeloCatalogos->obtenerfoliosconsumiblesrenta($renta_id,$itemc->id_consumibles,$itemc->id); 
	                                foreach ($contrato_folio2->result() as $itemx) {
    									if($itemx->idrelacion>0){

	                                      		echo $itemx->foliotext;
	                              		}
	                                }
			        			?></td>
			        			<td><?php echo $itemc->parte;?></td>
			        			<td><?php echo utf8_encode($domicilio_entrega);?></td>
			        			<td><?php echo $itemc->bodega;?></td>
			        			<td><?php if($itemc->entregado_status==1){echo 'Entregado';};?></td>
								<td><?php if($itemc->entregado_status==1){echo utf8_encode($itemc->personal.' '.$itemc->apellido_paterno);}?></td>
								<td><?php echo $fecha;?></td>
								<td><?php echo $tecnico;?></td>
			        		</tr>
	            		<?php
	            	}
	            	$rentaequipos_consumible_dev = $this->ModeloCatalogos->rentaequipos_consumible_dev($renta_id,$equipo_id,$item->id);
	            	foreach ($rentaequipos_consumible_dev->result() as $itemc) {
	            		?>
	            			<tr>
			        			<td>Renta </td>
			        			<td><?php echo $idrenta;?></td>
			        			<td><?php echo $empresa;?></td>
			        			<td><?php echo $itemc->cantidad;?></td>
			        			<td><?php echo $itemc->modelo;?></td>
			        			<td><?php 
			        				$where = array('idrenta'=>$renta_id,'idconsumible'=>$itemc->id_consumibles);
    									$contrato_folio = $this->ModeloCatalogos->db2_getselectwheren('contrato_folio',$where);
    									foreach ($contrato_folio->result() as $itemx) {
    										if($itemx->idrelacion>0){
       
		                                }else{
		                                     echo $itemx->foliotext.',';
		                                }
	                                }
	                                //============================ cuando ya todos tengan relacion remplazara a la anterior
	                                $contrato_folio2 = $this->ModeloCatalogos->obtenerfoliosconsumiblesrenta($renta_id,$itemc->id_consumibles,$itemc->id); 
	                                foreach ($contrato_folio2->result() as $itemx) {
    									if($itemx->idrelacion>0){

	                                      		echo $itemx->foliotext;
	                              		}
	                                }
			        			?><b style="color:red">(DN)</b></td>
			        			<td><?php echo $itemc->parte;?></td>
			        			<td><?php echo utf8_encode($domicilio_entrega);?></td>
			        			<td><?php echo $itemc->bodega;?></td>
			        			<td></td>
			        			<td></td>
			        			<td><?php echo $fecha;?></td>
			        			<td><?php echo $tecnico;?></td>
			        		</tr>
	            		<?php
	            	}

	        	}

		        foreach ($rentaventasacc->result() as $item) { 
		        	?>
		        		<tr>
		        			<td>Renta </td>
		        			<td><?php echo $idrenta;?></td>
		        			<td><?php echo $empresa;?></td>
		        			<td><?php echo $item->cantidad;?></td>
		        			<td><?php echo $item->nombre;?></td>
		        			<td><?php 
		        				$datosseries = $this->ModeloCatalogos->rentaventasaccessoriosseries($item->id_accesoriod); 
		        				foreach($datosseries->result() as $items) { 
									echo $items->serie.'<br>';
								}
		        			?></td>
		        			<td><?php echo $item->no_parte;?></td>
		        			<td><?php echo utf8_encode($domicilio_entrega);?></td>
		        			<td><?php echo $item->bodega;?></td>
		        			<td><?php if($item->entregado_status==1){echo 'Entregado';};?></td>
							<td><?php if($item->entregado_status==1){echo utf8_encode($item->personal.' '.$item->apellido_paterno);}?></td>
							<td><?php echo $fecha;?></td>
							<td><?php echo $tecnico;?></td>
		        		</tr>
		        	<?php 
		    	}
		    	foreach ($rentaventasacc_dev->result() as $item) { 
		        	?>
		        		<tr>
		        			<td>Renta </td>
		        			<td><?php echo $idrenta;?></td>
		        			<td><?php echo $empresa;?></td>
		        			<td><?php echo $item->cantidad;?></td>
		        			<td><?php echo $item->nombre;?></td>
		        			<td><?php 
		        				$datosseries = $this->ModeloCatalogos->rentaventasaccessoriosseries($item->id_accesoriod); 
		        				foreach($datosseries->result() as $items) { 
									echo $items->serie.'<br>';
								}
		        			?><b style="color:red">(DN)</b></td>
		        			<td><?php echo $item->no_parte;?></td>
		        			<td><?php echo utf8_encode($domicilio_entrega);?></td>
		        			<td><?php echo $item->bodega;?></td>
		        			<td></td>
		        			<td></td>
		        			<td><?php echo $fecha;?></td>
		        			<td><?php echo $tecnico;?></td>
		        		</tr>
		        	<?php 
		    	}
		 	}
		?>
	</tbody>
</table>