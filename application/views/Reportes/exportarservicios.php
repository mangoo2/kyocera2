<?php 
	/*
	header("Pragma: public");
	header("Expires: 0");
	$filename = "Servicios.xls";
	header("Content-type: application/x-msdownload");
	header("Content-Disposition: attachment; filename=$filename");
	header("Pragma: no-cache");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	*/
	//http://localhost/kyocera2/Listaservicios/exportar?tipo=0&tecnico=0&cliente=0&finicio=2024-01-10&ffin=2024-03-05&tiposer=0&status=0&zona=0
?>
<table border="1">
	<thead>
		<tr>
			<th>Tecnico</th>
			<th>Cliente</th>
			<th>Domicilio</th>
			<th>Fecha</th>
			<th>Hora inicio</th>
			<th>Hora fin</th>
			<th>No de Equipos</th>
		</tr>
	</thead>
	<tbody>
		<?php 
			$params['tiposer']=$tiposer;
			$params['tecnico']=$tecnico;
			$params['fechainicio']=$fechainicio;
			$params['fechafin']=$fechafin;
			$params['pcliente']=$pcliente;
			$params['status']=$status;
			$params['zona']=$zona;
			if($tipo==1 or $tipo==0){//contrato
				$datos=$this->ModeloServicio->getlistaservicioc_reporte($params);
				$array_info_ser=array();
				foreach ($datos->result() as $item) { 
					$direccionequipo= $this->ModeloCatalogos->getdireccionequipos($item->idequipo,$item->serieId);
					$array_info_ser[]=array(
						'tec'=>$item->tecnico,
						'emp'=>$item->empresa.' ('.$item->folio.')',
						'dir'=>'<!--'.$item->idCliente.'-->'.$direccionequipo,
						'fecha'=>$item->fecha,
						'hora'=>$item->hora,
						'horafin'=>$item->horafin
					);
					
					$html_tr='<tr><td>'.utf8_decode($item->tecnico).'</td><td>'.$item->empresa.' ('.$item->folio.')</td><td>'.$direccionequipo.'</td><td>'.$item->fecha.'</td><td>'.$item->hora.'</td><td>'.$item->horafin.'</td></tr>';
					//echo $html_tr;
				}
				$grouped_array = array();
				foreach ($array_info_ser as $element) {
					$grouped_array[$element['dir']][] = $element;
				}
				//var_dump($grouped_array);
				foreach ($grouped_array as $item) {
					$tec_array=array();
					foreach($item as $itemte) {
						$tec_array[]=$itemte['tec'];
					}
					$res_array_tec=array_unique($tec_array);
					$tecnicos=implode(',',$res_array_tec);
					//echo $tecnicos;
					//var_dump($res_array_tec);
					echo '<tr><td>'.utf8_decode($tecnicos).'</td><td>'.$item[0]['emp'].'</td><td>'.$item[0]['dir'].'</td><td>'.$item[0]['fecha'].'</td><td>'.$item[0]['hora'].'</td><td>'.$item[0]['horafin'].'</td><td>'.count($item).'</td></tr>';
				}
			}
			if($tipo==2 or $tipo==0){//poliza
				$params_p=$params;
				$params_p['tipoc']=1;
				$datos=$this->ModeloServicio->getlistaserviciop_reporte($params_p);
				$array_info_ser=array();
				foreach ($datos->result() as $item) { 
					$html_tr='<tr>
						<td>'.utf8_decode($item->tecnicogroup).'</td>
						<td>'.$item->empresa.'</td>
						<td>'.$item->direccionservicio.'</td>
						<td>'.$item->fecha.'</td>
						<td>'.$item->hora.'</td>
						<td>'.$item->horafin.'</td>
						<td>'.$item->numequipos.'</td>
					</tr>';
					echo $html_tr;
				}
			}
			if($tipo==3 or $tipo==0){//evento
				$params_c=$params;
				$params_c['tipoc']=1;
				$datos=$this->ModeloServicio->getlistaserviciocl_reporte($params_c);
				foreach ($datos->result() as $item) { ?>
					<tr>
						<td><?php echo utf8_decode($item->tecnicogroup);?></td>
						<td><?php echo $item->empresa;?></td>
						<td><?php echo $item->direccion;?></td>
						<td><?php echo $item->fecha;?></td>
						<td><?php echo $item->hora;?></td>
						<td><?php echo $item->horafin;?></td>
						<td><?php echo $item->numequipos;?></td>
					</tr>
				<?php }
			}
			if($tipo==4 or $tipo==0){//ventas
				$params_v=$params;
				$params_v['tipoc']=1;
				$datos=$this->ModeloServicio->getlistaserviciovent_reporte($params_v);
				foreach ($datos->result() as $item) { 
						

					?>
					<tr>
						<td><?php echo utf8_decode($item->tecnico);?></td>
						<td><?php echo $item->empresa;?></td>
						<td><?php echo $item->direccion;?></td>
						<td><?php echo $item->fecha;?></td>
						<td><?php echo $item->hora;?></td>
						<td><?php echo $item->horafin;?></td>
					</tr>
				<?php }
			}
		?>
	</tbody>
</table>