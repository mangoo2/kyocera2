<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Bodega extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->model('ModeloCatalogos');
        $this->load->model('login_model');
        $this->load->helper('url');
        $this->submenu=40;
        if($this->session->userdata('logeado')==true){
            $this->perfilid =$this->session->userdata('perfilid');
            $this->idpersonal = $this->session->userdata('idpersonal');
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,$this->submenu);// 40 es el id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
        }else{
            redirect('login');
        }
    }

    function index(){
        $data['MenusubId']=$this->submenu;
        $this->load->view('header');
        $this->load->view('main');
        $this->load->view('configuraciones/bodega/formbodega.php');
        $this->load->view('configuraciones/bodega/jsbodega.php');
        $this->load->view('footer');  
    }
    public function insertaActualiza(){
        $data = $this->input->post();
        $id = $data['bodegaId']; 
        if($id!=""){
            $idarray = array('bodegaId' => $id);
            unset($data['bodegaId']);
            $this->ModeloCatalogos->updateCatalogo('bodegas',$data,$idarray);
            //$this->Modelobitacoras->Insert(array('contenido'=>'Se realizo una actualizacion de bodegas','nombretabla'=>'bodegas','idtable'=>$id,'tipo'=>'Update','personalId'=>$this->idpersonal));
            $result = 2;   
        }else{
            $id=$this->ModeloCatalogos->Insert('bodegas',$data);
            //$this->Modelobitacoras->Insert(array('contenido'=>'Se creo una nueva bodega','nombretabla'=>'bodegas','idtable'=>$id,'tipo'=>'Insert','personalId'=>$this->idpersonal));
            $result = 1;
        }   
        echo $result;
    }
    public function getListadoBodega(){  
        $activo = array('activo' => 1);
        $listadata = $this->ModeloCatalogos->getselectwherenall('bodegas',$activo);
        $json_data = array("data" => $listadata);
        echo json_encode($json_data);
    }
    function deletebodega(){
        $params = $this->input->post();
        $id=$params['id'];
        $this->ModeloCatalogos->updateCatalogo('bodegas',array('activo'=>0),array('bodegaId' => $id));
    }
}