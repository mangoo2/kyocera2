<style type="text/css">
  .newinputstyle{display: block; width: 89% !important; padding: .375rem .75rem !important; font-size: 1rem !important; line-height: 1.5; color: #495057; background-color: #fff !important; background-clip: padding-box; border: 1px solid #ced4da !important; border-radius: .25rem !important; transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out !important; height: 1rem !important;}
  #profactura thead{font-size: 13px;}
  table td,table th{font-size: 12px; padding: 6px 7px;}
  .floatThead-container{background: white;}
  #profactura thead th,.floatThead-table thead th{height: 63px;vertical-align: bottom;}
  option:disabled{background: #8080808a;}
  @media only screen and (max-width: 600px) {
    .btn_rcp_fac,.btn_rcp_xml,.btn_rcp_facc,.btn_rcp_faccan,.btn_rcp_facd{display: none;}
  }
  .yellowc {background-color: #ffeb3b !important;}
  #test8 #content{margin-top: 0px !important;}
  #test8{padding-left: 0px !important; padding-right: 0px !important;}
  #test8 #content .container{margin-left: 0px !important; margin-right: 0px !important; width: 100%;}
  .equiposuspendido{background-color: #f55c4b !important;}
  .kv-file-upload{display: none !important;}
  .iframefac iframe{border: 0px; width: 100%;}
  .input_select_con.inc{max-width: 100px;}
  .sin_pro{background-color: #f4ff0024 !important;}
  .equiposuspendido{background-color: #ff000063 !important;}
  .search_serie_div_edit{right: 1px;}
  .infoequiporetiro{color: red;}
  .lis_folio{margin-right: 10px;}
  .colorf{background: #ff000026;}
  .lis_folio:hover .colorf{background: red;}
  .lis_folio .notification-badge{right: 0px;top: -14px;font-size: 9px;}
  .text-red{color: red;/* font-weight: bold; */font-style: italic;}
  .deleteocfile{display: none;}
  .tabs .tab a {font-size: 13px!important;}
  .statuscv {
    background: #e55043;
    color: white;
    text-align: center;
    font-weight: bold;
    margin-bottom: 11px;
  }
</style>
<?php 
  $result_pre=$this->ModeloCatalogos->actualizarstatusprefactura($idcontrato);
  foreach ($result_pre->result() as $item) {
    //log_message('error','prefId: '.$item->prefId.' subtutal:'.$item->subtotal.' total:'.$item->total);
    if($item->subtotal>=$item->total){
      //log_message('error','entra:'.$item->prefId);
      $this->ModeloCatalogos->updateCatalogo('alta_rentas_prefactura',array('factura_renta'=>1,'factura_excedente'=>1),array('prefId'=>$item->prefId));
      $this->ModeloCatalogos->updateCatalogo('alta_rentas_prefactura_d',array('statusfacturar'=>1,'statusfacturae'=>1),array('prefId'=>$item->prefId));
    }
  }
   
  $pfectivo=0;
        $r_p_e=$this->ModeloCatalogos->getselectwheren('personal',array('p_efectivo'=>1,'personalId'=>$idpersonal));
        if($r_p_e->num_rows()>0){
            $pfectivo=1;
        }

?>
<input type="hidden" id="idcondicionextra" value="0">
<input type="hidden" id="codigocarga" value="<?php echo date('YmdGis');?>" readonly>
<input type="hidden" id="idCliente" value="<?php echo $idcliente;?>" readonly>
<input type="hidden" id="permt_continuar" value="<?php echo $permt_continuar;?>" readonly>
<!-- START CONTENT -->
<section id="content" width="100%">
  <!--start container-->
  <div class="container" width="100%">
    <div class="section">
      <div class=""><p class="caption" style="margin: 0px;">Contrato pagos <span class="infoclientecontrato"></span></p></div>
      <div class="row">        
          <div class="col s12 m12 24">
            <div class="card-panel" style="padding-top: 0px; margin: 0px;">
              <input type="hidden" id="personalId" name="personalId" value="<?php echo $personalId ?>">
              <input type="hidden" id="idcontrato" name="idcontrato" value="<?php echo $idcontrato ?>">
              <input type="hidden" id="continuarcontadores" name="continuarcontadores" value="0">
              <div class="row">
                <div id="basic-tabs" class="section">
                  <div class="row">
                    <div class="col s12">
                      <div class="row">
                        <div class="col s12">
                          <ul class="tabs tab-demo z-depth-1">
                            <li class="tab col"><a class="active" href="#test1">Condiciones de pago</a></li>
                            <li class="tab col"><a href="#test2" onclick="dataprefactura0()">Periodos</a></li>
                            <!--<li class="tab col"><a href="#test3" onclick="mostrarhojaestado()">Hojas de estado</a></li>-->
                            <li class="tab col"><a href="#test4" >Refacciones</a></li>
                            <li class="tab col"><a href="#test5" id="test_tab_5" onclick="viewfacturas(<?php echo $idcontrato ?>)">Listas de Periodos</a></li>
                            <li class="tab col"><a href="#test6" onclick="listfacturas(<?php echo $idcontrato ?>)">Listas de factura</a></li>
                            <li class="tab col"><a href="#test7" id="listfolios" onclick="listfolios(<?php echo $idcontrato ?>)">Listas de Folios</a></li>
                            <li class="tab col"><a href="#test10" id="listfolios" onclick="upload_data_view_oc(<?php echo $idcontrato ?>)">Ordenes de compra</a></li>
                            <li class="tab col"><a href="#test8" onclick="listclientecontratos(<?php echo $idcliente; ?>)" >Contratos</a></li>
                            
                            <li class="tab col"><a href="#test9" style="display:none;" onclick="obtenercontratosoption(<?php echo $idcliente;?>)">Excedentes externos</a></li>
                            <li class="tab col tab_test11" style="display:none;"><a href="#test11" id="tab_li_11" onclick="parametrosfactura(<?php echo $idcontrato; ?>)" >Facturacion</a></li>

                            
                          </ul>
                        </div>
                        <div class="col s12">
                          <div id="test1" class="col s12"> 
                            <?php include('tab_contratos.php'); ?> 
                          </div>
                          <div id="test2" class="col s12">
                          <!--2 -->  
                            <?php include('tab_factura.php'); ?> 
                          <!-- fin2-->
                          </div>
                          <!--<div id="test3" class="col s12">
                            <br>
                            <?php include'tab_hojasestado.php'; ?>
                          </div>-->
                          <div id="test4" class="col s12">
                            <div class="row">
                              <div class="col s3">
                                <label>Fecha Inicial</label>
                                <input type="date" class="form-control-bmz" id="ref_fecha_inicial">
                              </div>
                              <div class="col s3">
                                <label>Fecha Final</label>
                                <input type="date" class="form-control-bmz" id="ref_fecha_final">
                              </div>
                              <div class="col s3">
                                <button class="btn-bmz green waves-effect waves-light right" type="button" onclick="mostrarrefacciones()">Consultar</button>
                              </div>
                            </div>
                          </div>
                          <div id="test5" class="col s12 ">
                            <div class="row ">
                              <div class="col s7"></div>
                              <div class="col s5" style="text-align:right;">
                                <a class="b-btn b-btn-primary tooltipped" data-position="top" data-delay="50" data-tooltip="Agregar Factura en desposito" onclick="addfactura2(<?php echo $idcontrato ?>,0)"><i class="fas fa-folder-plus"></i></a>
                                <a class="b-btn b-btn-primary tooltipped" data-position="top" data-delay="50" data-tooltip="Generacion de reporte de contadores"  onclick="reporte_contadores(<?php echo $idcontrato;?>)">Reporte contadores</a>
                              </div>
                              <div class="col s7"> </div>
                              <div class="col s5 addfacturadeposito" style="text-align:right;"></div>
                            </div>
                            <div class="row viewfacturas"></div>
                            <br>
                          </div>
                          <div id="test6" class="col s12 ">
                            <div class="row">
                              <div class="input-field col s4"> 
                                <label for="tipofacturasstatus" class="active">Tipo</label><br> 
                                <select name="tipofacturasstatus" id="tipofacturasstatus" class="browser-default" onchange="listfacturas(<?php echo $idcontrato ?>)"> 
                                  <option value="0" selected>Todas</option>
                                  <option value="1">Pendientes</option>
                                  <option value="2">Pagadas</option>
                                </select>   
                              </div>
                              <div class="col s4"></div>
                              <div class="col s4">
                                <a class="waves-effect cyan btn-bmz" href="<?php echo base_url();?>Configuracionrentas/Estadodecuenta/<?php echo $idcontrato ?>" target="_blank">Estado de cuenta contrato actual</a>
                                <a class="waves-effect cyan btn-bmz" href="<?php echo base_url();?>Configuracionrentas/Estadodecuenta/<?php echo $idcontrato.'/'.$idcliente; ?>" target="_blank">Estados de cuenta global del cliente</a>
                              </div>
                            </div>
                            <div class="row listfacturas"></div>
                            <br>
                          </div>
                          <div id="test7" class="col s12">
                            <div class="row">
                              <div class="col s12 m12 12">
                                <ul class="collapsible popout collapsible-accordion" data-collapsible="accordion">
                                  <li class="">
                                    <div class="collapsible-header">
                                      <i class="material-icons">add</i>Agregar</div>
                                    <div class="collapsible-body" style="display: none;">
                                      <!---------------------->
                                        <div class="row">
                                          <div class="col s3" style="display:none;">
                                             <label>Clientes</label> 
                                             <select class="browser-default form-control-bmz" id="nfclientes"></select>
                                          </div>
                                          <div class="col s1">
                                             <label>Contratos</label> 
                                             <select class="browser-default form-control-bmz" id="nfcontratos" onchange="cargaequiposs()"></select>
                                          </div>
                                          <div class="col s2">
                                              <label>Servicios</label>
                                              <select class="browser-default form-control-bmz" id="nfservicios"></select>
                                          </div>
                                          <div class="col s2">
                                             <label>Equipos</label> 
                                             <select class="browser-default form-control-bmz" id="nfequipos" onchange="cargaconsumibles()"></select>
                                          </div>
                                          <div class="col s2">
                                             <label>Consumibles</label> 
                                             <select class="browser-default form-control-bmz" id="nfconsumibles"></select>
                                          </div>
                                          <div class="col s2">
                                            <label>Bodega</label>
                                            <select id="nbodega" class="browser-default form-control-bmz">
                                              <?php foreach ($get_bodegas->result() as $item) { ?><option value="<?php echo $item->bodegaId;?>"><?php echo $item->bodega;?></option><?php } ?>
                                            </select> 
                                          </div>
                                          <div class="col s2">
                                           <label>Fecha</label> 
                                           <input type="date" class="browser-default form-control-bmz" id="nfecha" wtx-context="9562B610-3B7A-4901-AF7F-58EEFC1E8F79">
                                          </div>
                                          <div class="col s1">
                                                  <a class="btn-floating waves-effect waves-light green darken-1 " onclick="addconsumibles()"><i class="material-icons">play_for_work</i></a>
                                          </div>
                                        </div>
                                        <div class="row">
                                          <div class="col s12">
                                            <table id="tabla_cons" class="responsive-table display" cellspacing="0"><thead><tr><th>Cliente</th><th>Contrato</th><th>Servicio</th><th>Equipo</th><th>Consumible</th><th>Fecha</th><th>Bodega</th><th>Acciones</th></tr></thead><tbody class="addconsumiblesbody"></tbody></table>
                                          </div>
                                        </div>
                                        <div class="row">
                                          <div class="col s12">
                                            <a class="btn waves-effect waves-light green darken-1 " onclick="agregarconsumibles()">Agregar</a>
                                          </div>
                                        </div>
                                      <!---------------------->
                                    </div>
                                  </li>
                                  
                                </ul>
                              </div>
                            </div>
                            
                            <div class="row">
                              <div class="col s12 m12 12 listfolios"></div>
                            </div>
                            <br>
                          </div>
                          <div id="test8" class="col s12">
                            <?php include('configuracion.php'); ?> 
                          </div>
                          <div id="test9" class="col s12">
                            <?php include('tab_excedentes_ext.php'); ?> 
                          </div>
                          <div id="test10" class="col s12">
                            <div class="row">
                              <div class="col s12 files_orden"></div>
                            </div>
                          </div>
                          <div id="test11" class="col s12">
                            
                          </div>
                        </div>
                      </div>
                      <br>
                    </div>
                  </div>
                </div>
                <hr>
              </div>
            </div>
          </div>
      </div>
    </div>
  </section>
  <!-- Modal finaliza -->
<div id="modal_img" class="modal"  >
    <div class="modal-content ">
        <h4>Editar imagen</h4>
        <div class="col s12 m12 l6">
            <div class="row" align="center">
                <div class="input-field col s3">
                   <div class="fileinput fileinput-new" data-provides="fileinput">
                     <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 530px; height: 250px;"> 
                      <input type="file" id="foto" name="foto" class="dropify">
                    </div>
                   </div>
                </div>
            </div>
        </div>  
        <input type="hidden" id="idestado_r"><input type="hidden" id="ti">
        <div class="modal-footer">
            <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
            <a href="#!" class="modal-action modal-close waves-effect waves-red red btn-flat" style="color: white;" onclick="editarimg()">Aceptar</a>
        </div>
    </div>
    <!-- Modal finaliza fin-->
</div>
<div id="modal_pagos" class="modal">
  <div class="modal-content">
    <div class="row">
      <h4>Registro de nuevo pago</h4>
    </div>
    <div class="row">
      <div class="col s12 m12 l6">
        <label>Fecha</label>
        <input type="date" name="" class="form-control">
      </div>
      <div class="col s12 m12 l6">
        <label>Método de pago</label>
        <select class=""></select>
      </div>
    </div>
  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect waves-red btn-flat ">Disagree</a>
    <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat ">Agree</a>
  </div>
</div>
<div id="modalcontinuarcontadores" class="modal">
  <div class="modal-content ">
    <h4>Autorizar</h4>
    <div class="border_datos" id="fiscales_dato1">
      <div class="col s24 m24 l12">
        <div class="row">
          <div class="col s12">
            A partir de este momento, se autoriza la continuidad del contrato
          </div>
        </div>
        <div class="row ">
            <div class="row">
              <div class="input-field col s12">
                <input id="password" type="password" class="validate form-control-bmz" autocomplete="new-password" required="">
                <label for="password">Password</label>
              </div>
            </div>                                        
        </div>                          
      </div>
    </div>                    
  </div>
  <div class="modal-footer">
    <button class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</button>
    <button class="modal-action modal-close waves-effect waves-green green btn-flat" onclick="autorizaragregar()">Aceptar</button>
  </div>
</div>
<div id="modal_pago" class="modal"  > 
  <div class="modal-content "> 
    <div class="row"> 
      <div class="col s6"> 
        <h5>Registro de nuevo pago</h5> 
      </div> 
      <div class="col s6" align="right"> 
        <div class="row" style="margin-bottom: 0px;">
          <div class="col s7">
            Monto de prefactura
          </div>
          <div style="color: red" class="col s5 monto_prefactura"></div>
        </div>
        <div class="row" style="margin-bottom: 0px;">
          <div class="col s7">
            Restante
          </div>
          <div class="col s5 restante_prefactura" style="color: red"></div>
        </div>
      </div> 
    </div>   
    <h6>Vendedor: <?php echo $nombre.' '.$apellido_paterno.' '.$apellido_materno ?></h6> 
    <form class="form" id="form_pago" method="post">  
      <input type="hidden" id="id_factura" name="id_factura">
      <div class="row"> 
        <div class="input-field col s4"> 
          <input type="date" name="fecha" id="fecha_p" class="form-control-bmz"> 
          <label for="fecha" class="active">Fecha</label> 
        </div> 
        <div class="input-field col s4"> 
          <label for="idmetodo" class="active">Método de pago</label>
          <select name="idmetodo" id="idmetodo" class="browser-default chosen-select" onchange="fm_pago_se()"> 
            <option></option>
             <?php foreach ($get_f_formapago->result() as $item) { 
                      if($item->id==1){
                        if($pfectivo==1){
                          echo '<option value="'.$item->id.'">'.$item->formapago_text.'</option>';
                        }
                      }else{
                        echo '<option value="'.$item->id.'">'.$item->formapago_text.'</option>';
                      }
                  } ?> 
          </select>   
        </div>
        <div class="col s4"> 
          <label for="pago" class="active">Monto</label> 
          <input type="number" name="pago" id="pago_p" class="form-control-bmz"> 
          
        </div> 
      </div>   
      <div class="row"> 
        <div class="input-field col s9"> 
          <textarea name="observacion" id="observacion_p"></textarea> 
          <label for="observacion_p" class="active">Comentario</label> 
        </div>
        <div class="col s3">
          <a class="waves-effect waves-light btn modal-triggert" onclick="guardar_pago_compras()">Guardar</a> 
        </div> 
      </div> 
    </form>
    <div class="row">
      <div class="col s12">
        <span class="text_tabla_pago"></span> 
      </div>
    </div>
  </div>
</div>
  <!-- Modal finaliza -->
<style type="text/css">
  #modal_modi_contadores{width: 90% !important;top: 5% !important;max-height: 85% !important;}
  #modal_modi_contadores input{margin-bottom: 0px;}
</style>
<div id="modal_modi_contadores" class="modal"  style="width: 90%">
    <div class="modal-content ">
      <h4>Editar Contadores</h4>
      <div class="row search_serie_div_edit">
        <div class="col s9 m9 " align="right"><label>Serie</label></div>
        <div class="col s2 m2 "><input type="text" id="search_serie_edit" class="form-control-bmz"></div>
        <div class="col s1 m1 "><button class="btn-bmz cyan waves-effect" type="button" onclick="buscar_serie_edit()">Buscar</button></div>
      </div>
      <div class="row">
        <div class="col s12"></div>
        <div class="col s12 viewcontadorescargados"></div>
      </div>
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
        <!--<a href="#!" class="modal-action modal-close waves-effect waves-red red btn-flat " style="color: white;">Aceptar</a>-->
    </div>
    <!-- Modal finaliza fin-->
</div> 
  <!-- Modal finaliza -->
<div id="modal_exportarcontadores" class="modal"  style="width: 90%">
  <div class="modal-content ">
    <h4>Exportar Contadores disponibles</h4>
    <div class="row viewcontadoresesportar"></div>
    <div class="row">
      <div class="col s3">
        <label class="active row" for="pcliente">Seleccionar cliente:</label>
        <select class="browser-default" id="pcliente"></select>
      </div>
      <div class="col s3">
        <label class="active" for="idcontrato_f">Seleccionar contrato:</label>
        <select id="idcontrato_f" name="idcontrato_f" class="browser-default form-control-bmz" onchange="facturaselect()"></select>
      </div>
      <div class="col s3">
        <label class="active" for="idfactura_f">Seleccionar Periodo:</label>
        <select id="idfactura_f" name="idfactura_f" class="browser-default form-control-bmz"></select>
      </div>
    </div>
      
  </div>
  <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
      <a href="#!" class="modal-action modal-close waves-effect waves-red red btn-flat exportar" style="color: white;">Aceptar</a>
  </div>
  <!-- Modal finaliza fin-->
</div> 
<div id="modal_addfactura" class="modal"  style="width: 90%">
    <div class="modal-content ">
        <h4>Agregar una factura</h4>
        <input type="hidden" id="addfactcontrato" value="<?php echo $idcontrato;?>">
        <input type="hidden" id="addfactperiodo">
        <input type="hidden" id="addfacttipo">
        <input type="hidden" id="addfactpersonal" value="<?php echo $personalId;?>">
        <input type="hidden" id="addfactcliente" value="0">
        <input type="hidden" id="totalgeneralvalor" value="">
        <div class="row">
          <div class="col s12">
            <table id="tabla_facturacion" name="tabla_facturacion_cliente" class="responsive-table display" cellspacing="0">
              <thead>
                <tr>
                  <th>#</th><th>Folio</th><th>Cliente</th><th>RFC</th><th>Monto</th>
                  <th>Estatus</th><!--sin timbrar timbrada cancelada-->
                  <th>Fecha</th><th></th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
        <a href="#!" class="modal-action modal-close waves-effect waves-red red btn-flat" onclick="agregarfacturas()" style="color: white;">Aceptar</a>
    </div>
    <!-- Modal finaliza fin-->
</div> 

<div id="modal_comentario" class="modal"  >
    <div class="modal-content ">
        <h4>Comentario</h4>
        <div class="row" > 
          <div class="col s12">
            <textarea class="form-control-bmz" id="comentariopre" style="height:100px !important"></textarea>
          </div>   
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
            <a href="#!" class="modal-action modal-close waves-effect waves-red red btn-flat" style="color: white;" onclick="updatecomentario()">Aceptar</a>
        </div>
    </div>
    <!-- Modal finaliza fin-->
</div>

<div id="modal_enviomail" class="modal">
    <div class="modal-content">
      <div class="col s12">
        <label>Firma del ejecutivo</label>
        <select class="browser-default form-control-bmz" id="firma_m_f">
          <?php foreach ($perfirma->result() as $itemf) {
            echo '<option value="'.$itemf->personalId.'">'.$itemf->nombre.' '.$itemf->apellido_paterno.' '.$itemf->apellido_materno.' / '.$itemf->email.' / '.$itemf->celular.'</option>';
          }?>
        </select>
      </div>
      <div class="col s12">
        <label>Copia oculta (BBC)</label>
        <select class="browser-default form-control-bmz" id="bbc_m_f"><option value="0"></option>
          <?php foreach ($perfirma->result() as $itemf) {
            echo '<option value="'.$itemf->personalId.'">'.$itemf->nombre.' '.$itemf->apellido_paterno.' '.$itemf->apellido_materno.' / '.$itemf->email.' / '.$itemf->celular.'</option>';
          }?>
        </select>
      </div>
      <div class="col s12">
          <label>Correo</label>
          <div class="m_s_contac"></div>
        </div>
      <div class="row">
        <div class="col s12">
          <label>Asunto</label>
          <input class="form-control-bmz" id="ms_asucto2" value="Factura">
        </div>
        <div class="col s12">
          <label>Comentario</label>
          <textarea class="form-control-bmz" id="ms_coment2" style="min-height: 150px;"></textarea>
        </div>
        <div class="col s12 input_envio_s">
        </div>
        <div class="col s12 table_mcr_files_s"></div>
      </div>
      <div class="row"><div class="col s12 iframefac"></div></div>
      <div class="row"><div class="col s12 addbtnservicio"></div></div>
    </div>
    <div class="row">
      <div class="col m12" align="right">
        <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
      </div>
    </div>        
</div>



<div id="modal_condiciones" class="modal fade modal-fixed-footer" data-keyboard="false" data-backdrop="static" style="width: 95%;">
    <div class="modal-content " style="padding-left: 0px; padding-right: 0px;">
        <h4 style="padding-left: 24px">Condiciones</h4>
        
        
              <form id="form_condg" >
                <div class="row">
                  <div class="col s2">
                    <input type="hidden" class="input_select_con inc_v" id="con_preid" name="con_preid" value="0">
                    <input type="hidden" class="input_select_con inc_v" id="con_id" name="con_id" value="0">
                    <input type="hidden" class="input_select_con" id="con_renta" name="con_renta" value="<?php echo $idRenta ?>">
                    <label>Tipo</label>
                    <select class="browser-default form-control-bmz input_select_con" id="con_tipo" name="con_tipo" onchange="con_tipochange()">
                      <option value="2">Global</option>
                      <option value="1">Individual</option>
                    </select>
                  </div>
                  <div class="col s10">
                    <label>Comentario</label>
                    <input type="text" class="form-control-bmz input_select_con inc_v" id="comentario" name="comentario">
                  </div>
                </div>
                <div class="row">
                  <div class="col s2" style="display:none;">
                    <label>Fecha inicial</label>
                    <input type="date" class="form-control-bmz input_select_con inc_v" id="con_fechainicial" name="con_fechainicial">
                  </div>
                  <div class="col s2" style="display:none;">
                    <label>Fecha Final</label>
                    <input type="date" class="form-control-bmz input_select_con inc_v" id="con_fechafinal" name="con_fechafinal">
                  </div>
                  
                </div>
                <div class="row div_global">
                  <div class="col s2">
                    <label>Renta Monocromatica</label>
                    <input type="number" class="form-control-bmz input_select_con inc_v" id="con_renta_m" name="con_renta_m">
                  </div>
                  <div class="col s2">
                    <label>Click Monocromatica</label>
                    <input type="number" class="form-control-bmz input_select_con inc_v" id="con_click_m" name="con_click_m">
                  </div>
                  <div class="col s2">
                    <label>Excedentes Monocromatica</label>
                    <input type="number" class="form-control-bmz input_select_con inc_v" id="con_excedente_m" name="con_excedente_m">
                  </div>
                  <div class="col s2">
                    <label>Renta Color</label>
                    <input type="number" class="form-control-bmz input_select_con inc_v" id="con_renta_c" name="con_renta_c">
                  </div>
                  <div class="col s2">
                    <label>Click Color</label>
                    <input type="number" class="form-control-bmz input_select_con inc_v" id="con_click_c" name="con_click_c">
                  </div>
                  <div class="col s2">
                    <label>Excedentes Color</label>
                    <input type="number" class="form-control-bmz input_select_con inc_v" id="con_excedente_c" name="con_excedente_c">
                  </div>
                </div>
              </form>
              <div class="row div_individual" style="display:none;">
                <table class="striped" id="table_individual">
                  <thead><tr><th>MODELO</th><th>SERIE</th><th>Renta Monocromatica</th><th>Click Monocromatica</th><th>Excedentes Monocromatica</th><th>Renta Color</th><th>Click Color</th><th>Excedentes Color</th></tr></thead>
                  <tbody class="tbody_table_individual">
                    
                  </tbody>
                </table>
              </div>

    </div>
    <div class="modal-footer">
        <a class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
        <a class="waves-effect waves-red red btn-flat" style="color: white;" onclick="savecondiciones()">Save condiciones</a>
    </div>
    <!-- Modal finaliza fin-->
</div>


<div id="modal_contactos" class="modal fade modal-fixed-footer" data-keyboard="false" data-backdrop="static" style="width: 95%;">
    <div class="modal-content " style="padding-left: 0px; padding-right: 0px;">
        <h5 style="padding-left: 24px">Condiciones</h5>
        <div class="row" style="width: 99%;">
          <div class="col s12">
            <table id="tablecontactos">
              <thead>
                <tr>
                  <th>Tipo</th>
                  <th>Nombre</th>
                  <th>Telefono</th>
                  <th>Correo</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                <?php 
                  $result_con = $this->ModeloCatalogos->getselectwheren('cliente_datoscontacto',array('clienteId'=>$idcliente,'activo'=>1));
                  foreach ($result_con->result() as $itemc) {
                    echo '<tr>
                  <td>'.$itemc->puesto.'</td>
                  <td>'.$itemc->atencionpara.'</td>
                  <td>'.$itemc->telefono.'</td>
                  <td>'.$itemc->email.'</td>
                  <td><a class="b-btn b-btn-primary addmodalcontactos_'.$itemc->datosId.'" 
                    data-puesto="'.$itemc->puesto.'"
                    data-atencionpara="'.$itemc->atencionpara.'"
                    data-telefono="'.$itemc->telefono.'"
                    data-email="'.$itemc->email.'"
                  onclick="addmodalcontactos('.$itemc->datosId.')">Agregar</a></td>
                </tr>';
                  }
              
                ?>
              </tbody>
            </table>
          </div>
          
          
        </div>
    </div>
    <div class="modal-footer">
        <a class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
    </div>
    <!-- Modal finaliza fin-->
</div>