<!-- START CONTENT -->
<style type="text/css">
  .width_img{width: 36px;  }
  .tr_green{background: #c5dfb8;  }
  .tr_gris{background: #f2f2f2;  }
  .tr_orage{
    background: #fab45f66;
  }
  .tr_red{
    background: #e625256e;
  }
  .tr_blue{
    background: #2196f342;
  }
  .txt_c{text-align: center;  }
  .title_tec{font-size: 21px;    font-weight: bold;  }
  .coloborder{border: 3px solid red; border-radius: 19px;    min-height: 480px;  }
  #newhora{width: 99%;  }
  .div_table{height: 538px;    overflow: auto;  }
  .table-servicios td{font-size: 13px;  }
  .infoicon{
    font-size: 21px;
    color: #095b9c;
  }
  .datosclientecontacto a{
    display: none;
  }
  .colordeiconofinalizado{
    color: green;
  }
  .cansvg{
    background: url(<?php echo base_url().'public/img/control/cancel.svg';?>);
    background-size: 100%;
    background-repeat: no-repeat;
    background-position: center;
  }
  .pinsvg{
    background: url(<?php echo base_url().'public/img/control/pin.svg';?>);
    background-size: 100%;
    background-repeat: no-repeat;
    background-position: center;
  }
  .cansvg, .pinsvg{
    width: 37px;
  }

  .tr_porvencer{
    box-shadow: 0px 0px 20px rgba(250,10,10,1);
    background-color: #fbb4b4;
    animation: infinite resplandorAnimation_alert 2s;
  }
  @keyframes resplandorAnimation_alert {
    0%,100%{
      box-shadow: 0px 0px 20px rgba(250,10,10,1);
      background-color: #ff0000;
      color: white;
    }
    50%{
      background-color:#f9f9f9;
      box-shadow: 0px 0px 0px;
      color: black;
    }
  }
  .tr_porvencer .span_hora{
    animation: infinite resplandorAnimation_alert_span 2s;
  }
  @keyframes resplandorAnimation_alert_span {
    0%,100%{
      color: White;
    }
    50%{
      color:red;
    }
  }
  .tr_poriniciar{
    box-shadow: 0px 0px 20px rgba(250,10,10,1);
    background-color: #fec665;
    animation: infinite resplandorAnimation_alert2 2s;

  }

@keyframes resplandorAnimation_alert2 {
  0%,100%{
    box-shadow: 0px 0px 20px #fec665;
    background-color: #fec665;
  }
  50%{
    background-color:#f9f9f9;
    box-shadow: 0px 0px 0px;
  }
}
@media only screen and (max-width: 450px){
  .jconfirm.jconfirm-bootstrap .jconfirm-box{
    width: 90% !important;
  }
  .jconfirm.jconfirm-light .jconfirm-box{
      width: 90% !important;
  }
  .titulo_tablero h4{
    font-size: 1.90rem;
  }
}
  <?php if($perfilid==5 || $perfilid==12){ ?>
    .rechag{
      display: none;
    }
  <?php } ?>
  .dif_hrs{
    text-align: center;
    font-weight: bold;
  }
  .dif_hrs.mayor{
    color: red;
  }
</style>
<?php
  $fecha2 = strtotime ( '-3 day', strtotime (date('Y-m-d')) ) ;
  $fecha2 = date ( 'Y-m-d' , $fecha2 );

  $mov_seer_act=0;//1 solo fecha alctual 0 cualquier fecha 
  $mov_seer_activo=0;
  if($mov_seer_act==1){
    if($fecha==date('Y-m-d')){
      $mov_seer_activo=1;
    }else{
      $mov_seer_activo=0;  
    }
  }else{
    $mov_seer_activo=1;
  }
  if($perfilid==5){
    $mov_seer_activo=0;
  }
?>
<section id="content">
<!--start container-->
  <div class="container">
    <div class="section">
      <input type="hidden" id="ac_fecha_ac" value="<?php echo $ac_fecha_ac;?>">
    <input id="fechaactual" type="hidden" value="<?php echo $fecha;?>">
    <input id="fechaactualtablero" type="hidden" value="<?php echo $fecha;?>">
    <input type="hidden" id="mov_seer_activo" value="<?php echo $mov_seer_activo;?>">
    <div id="table-datatables">
      <div class="row">
        <div class="col 2 s2 l1 <?php echo $fecha2 ;?>">
          <?php if($fecha>$fecha2){  ?>
            <a class="btn-bmz blue gradient-shadow btn-accion click_load" href="<?php echo base_url();?>index.php/TableroControl/viewt/<?php echo date("Y-m-d", strtotime($fecha. "-1 day"))?>" title="Anterior"><i class="fas fa-arrow-circle-left"></i></a>
          <?php } ?>
        </div>
        <div class="col 8 s8 l10 txt_c titulo_tablero" >
          <h4 ><b> Día: <?php echo $fecha;?> - Tablero de control</b></h4>
        </div>
        <div class="col 2 s2 l1">
          <a class="btn-bmz blue gradient-shadow btn-accion click_load" href="<?php echo base_url();?>index.php/TableroControl/viewt/<?php echo date("Y-m-d", strtotime($fecha. "+1 day"))?>" title="Siguiente"><i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <br>
      <div class="row info_tablero">
          <?php //echo $this->Modelotablero->tablero($mov_seer_activo,$fecha) ?>
          <?php /* foreach ($resultstec as $item_tec) { 
            $opcion_mover_tec=' ondrop="soltar(event)" ondragover="permitirSoltar(event,'.$item_tec->personalId.')" ';
            $aux=array();

            $opcion_mover_todos=' ondragstart="arrastrar_todo(event,'.$item_tec->personalId.')" ';
            if($mov_seer_activo==0){
              $opcion_mover_todos='';
              $opcion_mover_tec='';
            }

          ?>

            <!--<option value="<?php echo $item_tec->personalId;?>"  ><?php echo $item_tec->tecnico;?></option>-->
            <div class="col s3 tecnico_info_<?php echo $item_tec->personalId;?>"  data-nametec="<?php echo $item_tec->tecnico;?>">
              <div class="coloborder">
                <div style="padding: 10px;">
                  <p class="title_tec txt_c" <?php echo $opcion_mover_todos;?> ><b><?php echo $item_tec->tecnico;?> </b><img style="width: 30px;" src="<?php echo base_url() ?>public/img/control/monitor.svg"></p>
                  <div class="div_table" <?php echo $opcion_mover_tec;?> >
                    
                  
                    <table class="table table-datatables table-servicios" >
                      <thead>
                        <tr>
                          <td></td>
                        </tr>
                      </thead>
                      <tbody>
                        <?php 
                          $url_img_pin=base_url().'public/img/control/pin.svg';
                          $url_img_info=base_url().'public/img/control/pin.svg';
                          $url_img_cancel=base_url().'public/img/control/cancel.svg';


                          $arrays_tr=array();
                          
                          $resultser_con=$this->ModeloAsignacion->getlistadocalentariotipogroup($fecha,$item_tec->personalId,1);
                          foreach ($resultser_con->result() as $item) {
                            $colortr='';
                            //=====================
                                $resultser_con=$this->ModeloAsignacion->get_info_status($item->g_status);
                                if($resultser_con==1){
                                  $colortr=' tr_orage ';
                                }
                                if($resultser_con==2){
                                  $colortr=' tr_green ';
                                }
                            //=====================
                            $hora=date("G", strtotime($item->hora));
                            $horal=date("h:i A", strtotime($item->hora));
                            $onclick_h="cambiarhora(1,$item->asignacionId,'$item->hora','$item->horafin','$item->hora_comida','$item->horafin_comida')";
                            $onclick_r="notificacionrechazogeneral(1,$item->asignacionId)";
                            //=======================
                              $rest_iti1=$this->ModeloAsignacion->get_info_itinerario_asignacion($item->asignacionId,1,$item_tec->personalId);
                              foreach ($rest_iti1->result() as $itemasig) {
                                $hora=date("G", strtotime($itemasig->hora_inicio));
                                $horal=date("h:i A", strtotime($itemasig->hora_inicio));
                              }
                            //=======================

                            //$opcion_mover=' ondrop="soltar(event)" ondragover="permitirSoltar(event,1,'.$item->asignacionId.')" ondragstart="arrastrar(event,1,'.$item->asignacionId.')" ';
                            $opcion_mover=' ondragstart="arrastrar(event,1,'.$item->asignacionId.','.$item_tec->personalId.')" ';
                            if($mov_seer_activo==0){
                              $opcion_mover='';
                            }
                            $html='<tr class=" '.$item->g_status.' -'.$resultser_con.'- contrato '.$colortr.' status_'.$item->status.'"  '.$opcion_mover.' draggable="true">
                                    <td><span>'.$item->empresa.'</span><br><span style="color: red" ondblclick="'.$onclick_h.'">'.$horal.'</span> </td>
                                    <td ondblclick="obternerdatosservicio('.$item->asignacionId.',1)"><i class="fas fa-info-circle infoicon"></i></td>
                                    <td ondblclick="obtenerdirecciones(1,'.$item->asignacionId.')"><img class="width_img" src="'.$url_img_pin.'"></td>
                                    <td ondblclick="'.$onclick_r.'"><img class="width_img" src="'.$url_img_cancel.'"></td>
                                  </tr>';

                            $arrays_tr[]=array('hora'=>$hora,'html'=>$html);
                          }
                          $resultser_pol=$this->ModeloAsignacion->getlistadocalentariotipogroup($fecha,$item_tec->personalId,2);
                          foreach ($resultser_pol->result() as $item) {
                            $colortr='';
                            if($item->status==2){
                              $colortr=' tr_green ';
                            }
                            //=====================
                                $resultser_con=$this->ModeloAsignacion->get_info_status($item->g_status);
                                if($resultser_con==1){
                                  $colortr=' tr_orage ';
                                }
                                if($resultser_con==2){
                                  $colortr=' tr_green ';
                                }
                            //=====================
                            $hora=date("G", strtotime($item->hora));
                            $horal=date("h:i A", strtotime($item->hora));

                            $onclick_h="cambiarhora(2,$item->asignacionId,'$item->hora','$item->horafin','$item->hora_comida','$item->horafin_comida')";
                            $onclick_r="notificacionrechazogeneral(2,$item->asignacionId)";
                            //$opcion_mover=' ondrop="soltar(event)" ondragover="permitirSoltar(event,1,'.$item->asignacionId.')" ondragstart="arrastrar(event,1,'.$item->asignacionId.')" ';
                            $opcion_mover=' ondragstart="arrastrar(event,2,'.$item->asignacionId.','.$item_tec->personalId.')" ';
                            //=======================
                              $rest_iti2=$this->ModeloAsignacion->get_info_itinerario_asignacion($item->asignacionId,2,$item_tec->personalId);
                              foreach ($rest_iti2->result() as $itemasig) {
                                $hora=date("G", strtotime($itemasig->hora_inicio));
                                $horal=date("h:i A", strtotime($itemasig->hora_inicio));
                              }
                            //=======================
                            if($mov_seer_activo==0){
                              $opcion_mover='';
                            }

                            $html='<tr class="poliza '.$colortr.'" '.$opcion_mover.' draggable="true">
                                    <td>'.$item->empresa.'<br><span style="color: red" ondblclick="'.$onclick_h.'">'.$horal.'</span> </td>
                                    <td ondblclick="obternerdatosservicio('.$item->asignacionId.',2)"><i class="fas fa-info-circle infoicon"></i></td>
                                    <td ondblclick="obtenerdirecciones(2,'.$item->asignacionId.')" ><img class="width_img" src="'.$url_img_pin.'"></td>
                                    <td ondblclick="'.$onclick_r.'"><img class="width_img" src="'.$url_img_cancel.'"></td>
                                  </tr>';

                            $arrays_tr[]=array('hora'=>$hora,'html'=>$html);
                          }
                          $resultser_cli=$this->ModeloAsignacion->getlistadocalentariotipogroup($fecha,$item_tec->personalId,3);
                          foreach ($resultser_cli->result() as $item) {
                            $colortr='';
                            if($item->estatus1==2){
                              $colortr=' tr_green ';
                            }
                            //=====================
                                $resultser_con=$this->ModeloAsignacion->get_info_status($item->g_status1);
                                if($resultser_con==1){
                                  $colortr=' tr_orage ';
                                }
                                if($resultser_con==2){
                                  $colortr=' tr_green ';
                                }
                            //=====================
                            $hora=date("G", strtotime($item->hora));
                            $horal=date("h:i A", strtotime($item->hora));
                            
                            $onclick_h="cambiarhora(3,$item->asignacionId,'$item->hora','$item->horafin','$item->hora_comida','$item->horafin_comida')";
                            $onclick_r="notificacionrechazogeneral(3,$item->asignacionId)";

                            //$opcion_mover=' ondrop="soltar(event)" ondragover="permitirSoltar(event,1,'.$item->asignacionId.')" ondragstart="arrastrar(event,1,'.$item->asignacionId.')" ';
                            $opcion_mover=' ondragstart="arrastrar(event,3,'.$item->asignacionId.','.$item_tec->personalId.')" ';
                            //=======================
                              $rest_iti3=$this->ModeloAsignacion->get_info_itinerario_asignacion($item->asignacionId,3,$item_tec->personalId);
                              foreach ($rest_iti3->result() as $itemasig) {
                                $hora=date("G", strtotime($itemasig->hora_inicio));
                                $horal=date("h:i A", strtotime($itemasig->hora_inicio));
                              }
                            //=======================
                            if($mov_seer_activo==0){
                              $opcion_mover='';
                            }

                            $html='<tr class=" eventocli '.$colortr.' '.$item->estatus1.' '.$item->estatus2.' " '.$opcion_mover.' draggable="true">
                                    <td>'.$item->empresa.'<br><span style="color: red" ondblclick="'.$onclick_h.'">'.$horal.'</span> </td>
                                    <td ondblclick="obternerdatosservicio('.$item->asignacionId.',3)"><i class="fas fa-info-circle infoicon"></i></td>
                                    <td ondblclick="obtenerdirecciones(3,'.$item->asignacionId.')"><img class="width_img" src="'.$url_img_pin.'"></td>
                                    <td ondblclick="'.$onclick_r.'"><img class="width_img" src="'.$url_img_cancel.'"></td>
                                  </tr>';

                            $arrays_tr[]=array('hora'=>$hora,'html'=>$html);
                          }
                          $resultser_ve=$this->ModeloAsignacion->getlistadocalentariotipogroup($fecha,$item_tec->personalId,4);
                          foreach ($resultser_ve->result() as $item) {
                            $colortr='';
                            if($item->status==2){
                              $colortr=' tr_green ';
                            }
                            //=====================
                                $resultser_con=$this->ModeloAsignacion->get_info_status($item->g_status);
                                if($resultser_con==1){
                                  $colortr=' tr_orage ';
                                }
                                if($resultser_con==2){
                                  $colortr=' tr_green ';
                                }
                            //=====================
                            $hora=date("G", strtotime($item->hora));
                            $horal=date("h:i A", strtotime($item->hora));
                            
                            $onclick_h="cambiarhora(4,$item->asignacionId,'$item->hora','$item->horafin','$item->hora_comida','$item->horafin_comida')";
                            $onclick_r="notificacionrechazogeneral(4,$item->asignacionId)";

                            //$opcion_mover=' ondrop="soltar(event)" ondragover="permitirSoltar(event,1,'.$item->asignacionId.')" ondragstart="arrastrar(event,1,'.$item->asignacionId.')" ';
                            $opcion_mover=' ondragstart="arrastrar(event,4,'.$item->asignacionId.','.$item_tec->personalId.')" ';
                            //=======================
                              $rest_iti3=$this->ModeloAsignacion->get_info_itinerario_asignacion($item->asignacionId,4,$item_tec->personalId);
                              foreach ($rest_iti3->result() as $itemasig) {
                                $hora=date("G", strtotime($itemasig->hora_inicio));
                                $horal=date("h:i A", strtotime($itemasig->hora_inicio));
                              }
                            //=======================
                            if($mov_seer_activo==0){
                              $opcion_mover='';
                            }

                            $html='<tr class=" eventocli '.$colortr.' '.$item->status.'" '.$opcion_mover.' draggable="true">
                                    <td>'.$item->empresa.'<br><span style="color: red" ondblclick="'.$onclick_h.'">'.$horal.'</span> </td>
                                    <td ondblclick="obternerdatosservicio('.$item->asignacionId.',4)"><i class="fas fa-info-circle infoicon"></i></td>
                                    <td ondblclick="obtenerdirecciones(4,'.$item->asignacionId.')"><img class="width_img" src="'.$url_img_pin.'"></td>
                                    <td ondblclick="'.$onclick_r.'"><img class="width_img" src="'.$url_img_cancel.'"></td>
                                  </tr>';

                            $arrays_tr[]=array('hora'=>$hora,'html'=>$html);
                          }
                          //var_dump($arrays_tr);
                          
                          


                          //==============================================================================
                          //==============================================================================
                          if(count($arrays_tr)>0){
                            foreach ($arrays_tr as $key => $row) {
                              if(isset($row['html'])){
                                $aux[$key] = $row['hora'];
                              }
                            }
                            //var_dump($aux);
                            array_multisort($aux, SORT_ASC, $arrays_tr);
                            foreach ($arrays_tr as $key => $row) {
                                  echo $row['html'];
                            }
                          }
                          

                         
                        ?>




                        
                      </tbody>
                    </table>
                  </div>
                </div>  
              </div>
            </div> 

          <?php } */?>
        
      </div>


      
    </div>
  </div>  
</section>


