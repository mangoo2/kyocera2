<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Tiposcambio extends CI_Controller 
{
    public function __construct() 
    {
        parent::__construct();
        $this->load->model('Cambios_model', 'model');
        $this->load->model('login_model');
        $this->load->helper('url');
        $this->load->library('menus');
        $this->load->model('ModeloCatalogos');
        $this->load->model('refacciones_model');
        $this->submenu=16;
        if($this->session->userdata('logeado')==true){
            $this->perfilid =$this->session->userdata('perfilid');
            $this->idpersonal = $this->session->userdata('idpersonal');
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,$this->submenu);// 16 es el id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
        }else{
            redirect('login');
        }
    }

    // Listado de configuraciones
    function index(){
        $data['MenusubId']=$this->submenu;
            // Obtiene los menús disponibles del sistema dependiendo el perfil, ayudado de la libreria "MENUS"
            $menus = $this->menus->getMenus($this->session->userdata('perfilid'));
            $data['menus'] = $menus['menus'];
            $data['submenusArray'] = $menus['submenusArray'];
            $data['tiposCambio'] = $this->model->getTiposCambio();
            $data['tablacondiciones']=$this->ModeloCatalogos->getselectwheren('tabla_descuentos_refacciones',array('activo'=>1));
            $porcentajesGanancia = $this->refacciones_model->getPorcentajeGananciaRefacciones();
            $data['porc_gana_refa']=$porcentajesGanancia[0]->gananciaVenta;
            $this->load->view('header');
            $this->load->view('main',$data);
            $this->load->view('configuraciones/tiposCambio/form_cambios');
            $this->load->view('configuraciones/tiposCambio/form_cambios_js');
            $this->load->view('footer');
    }

    function actualizarTiposCambio()    {
        // Obtenemos el POST que viene del formulario
        $data = $this->input->post();
        
        // Organizamos los datos, para dentro de un ciclo actualizar
        $datos[1] = array('cambio' => $data["cambio_equipos"]);
        $datos[2] = array('cambio' => $data["cambio_refacciones"]);
        $datos[3] = array('cambio' => $data["cambio_consumibles"]);
        $datos[4] = array('cambio' => $data["cambio_accesorios"]);

        // Como los ID's sono fijos, los vamos pasando conforme se ejecute el ciclo
        for ($i=1; $i <=4 ; $i++) 
        { 
            // Actualiza cada registro
            $this->model->actualizaTipoCambio($i,$datos[$i]);
        }
        //$this->Modelobitacoras->Insert(array('contenido'=>'Se realizo una actializacion en los tipos de cambios','nombretabla'=>'tiposCambio','idtable'=>0,'tipo'=>'Update','personalId'=>$this->idpersonal));
        
    }
    function tabladescuentosrefacciones(){
        $data = $this->input->post();
        $arrayRefaccion = $data['arrayRefaccion'];
        $DATAc = json_decode($arrayRefaccion);
        for ($i=0;$i<count($DATAc);$i++) {
            $datas = array(
                'pd_inicio'=>$DATAc[$i]->pd_inicio,
                'pd_fin'=>$DATAc[$i]->pd_fin,
                'for_gen'=>$DATAc[$i]->for_gen,
                'sem_frec'=>$DATAc[$i]->sem_frec,
                'local_poli'=>$DATAc[$i]->local_poli,
                'aaa'=>$DATAc[$i]->aaa
            );
            $this->ModeloCatalogos->updateCatalogo('tabla_descuentos_refacciones',$datas,array('id'=>$DATAc[$i]->id));
        }
    }


}

?>