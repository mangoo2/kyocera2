<html class="no-js" lang="en"><!--<![endif]--><head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title>Kyo chat</title>

  <meta content="width=device-width,initial-scale=1.0" name="viewport">


  <!--<script src="<?php echo base_url();?>client/lib/jquery-1.8.2.min.js"></script>-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>client/themes/default/jquery.phpfreechat.min.css">
  <link rel="stylesheet" type="text/css" href="https://chat.altaproductividadapr.com/public/css/style_chat.css?v=<?php echo date('Ymd');?>">
  <link href="<?php echo base_url()?>public/plugins/bootstrap/bootstrap.min.css" rel="stylesheet"  >
        <script src="<?php echo base_url()?>public/plugins/bootstrap/bootstrap.bundle.min.js"  ></script>
  <style type="text/css">
    .pfc-messages{
      height: auto !important;
    }
    .i_eje{
      font-size: 12px;
      font-style: italic;
      color: grey;
      margin-left: 10px;
      margin-right: 10px;
    }
  </style>
</head>
<body>
  <header>

  </header>
  <div role="main">
    <input type="hidden" id="base_url" value="<?php echo base_url();?>">
    <div class="row">
      <div class="col-md-12 i_eje">Asessor asignado:<b><?php echo $ejecutivo;?></b></div>
    </div>
    <?php
      
      
      if($status==1){
        $html='';
          //======================================
            $html.='<div class="pfc-messages" id="pfc-messages">';
                  //====================================
                    $html.='<div class="group_message">';
                      $html.='<div class="tit">Motivo del contacto</div>';
                      $html.='<div class="content">'.$motivo.'</div>';
                  $html.='</div>';
                  foreach ($resutld->result() as $item_c) {
                      $html.='<div class="group_message">';
                          $html.='<div class="tit">'.$item_c->name.'</div>';
                          $html.='<div class="content">'.$item_c->contenido.'</div>';
                          $html.='<div class="inf_ch">'.date("H:m:s", strtotime($item_c->reg)).'</div>';
                        $html.='</div>';
                  }
                  //====================================

            $html.='</div>';
            
            
              echo $html;
          //======================================
      }
      
    ?>
    
    
  </div>
  <footer>
   
  </footer>

</body>


</html>