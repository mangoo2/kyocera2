<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Clientes_suspendidos extends CI_Controller {
    public function __construct()     {
        parent::__construct();
        $this->load->model('ModeloCatalogos');
        $this->load->helper('url');
        $this->idpersonal = $this->session->userdata('idpersonal');
        $this->perfilid =$this->session->userdata('perfilid');
        $this->version = date('YmdHi');
        $this->fechahoyc = date('Y-m-d H:i:s');
        $this->submenu=75;
        if($this->session->userdata('logeado')==true){
            $this->perfilid =$this->session->userdata('perfilid');
            $this->idpersonal = $this->session->userdata('idpersonal');
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,$this->submenu);// 75 es el id del submenu
            if ($permiso==0) {
                redirect('Sistema');
            }
        }else{
            redirect('Sistema'); 
        }
    }

    // Listado de Empleados
    function index()     {
            $data['MenusubId']=$this->submenu;
            
            //$data['result']=$this->ModeloCatalogos->getselectwheren('clientes',array('estatus'=>1,'bloqueo'=>1));
            $data['result']=$this->ModeloCatalogos->get_listado_clientes_bloqueados_suspendidos(0);
            $data['idpersonal']=$this->idpersonal;
            $data['result_vf']=$this->ModeloCatalogos->get_listado_clientes_bloqueados_suspendidos(1);
            $this->load->view('header');
            $this->load->view('main',$data);
            $this->load->view('clientes/listadocs');
            
            $this->load->view('footer');
            $this->load->view('clientes/listadocsjs');
        
    }
    function permitir(){
        $params = $this->input->post();
        $cli = $params['cli'];
        $per = $params['per'];
        $perv = $params['perv'];
        $this->ModeloCatalogos->updateCatalogo('clientes',array('bloqueo_perm_ven_ser'=>$per,'bloqueo_perm_ven'=>$perv),array('id'=>$cli));
    }

    
}
?>
