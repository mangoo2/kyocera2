<!------------------------------->
            <form id="formulario_cotizacion">
              <input type="hidden" name="idcotizacion" id="idcotizacion" value="0">
              <div class="row editcotizacion">
                
                <div class="col s6">
                  <label for="cliente">Cliente / Prospecto</label>
                  <input id="cliente" name="cliente"  type="text" class="form-control-bmz" readonly>
                </div>
                <div class="col s6">
                  <label for="atencion" class="active">Atención para:</label>
                  <input id="atencion" name="atencion" class="form-control-bmz"  type="text" required>
                </div>
              </div>
              <div class="row editcotizacion">
                <div class="col s6">
                  <label for="cliente" class="active">Correo</label>
                  <input id="correo" name="correo"   type="text" class="form-control-bmz" required>
                  
                </div>
                <div class="col s6">
                  <label for="telefono" class="active">Telefono:</label>
                  <input id="telefono" name="telefono"  type="text" class="form-control-bmz" required>
                  
                </div>
              </div>
              
              <!-- ELECCION DE COTIZACION -->
              <div class="row editcotizacion">
                <div class="col s8">
                  <div class="col s2" style="margin-left: -12px;">
                    <label for="estado">¿Que desea cotizar?</label>
                  </div>
                  <div class="col s2 margenTopDivChosen">
                    <input type="checkbox" class="filled-in" id="ventae" name="eleccionCotizar" value="1"  onchange="eleccionCotizar2(1)" />
                    <label for="ventae">Equipos</label>
                  </div>
                  <div class="col s2 margenTopDivChosen">
                    <input type="checkbox" class="filled-in" id="ventac" name="eleccionCotizar" value="2"  onchange="eleccionCotizar2(2)" />
                    <label for="ventac">Consumibles</label>
                  </div>
                  <div class="col s2 margenTopDivChosen">
                    <input type="checkbox" class="filled-in" id="ventar" name="eleccionCotizar" value="3"  onchange="eleccionCotizar2(3)" />
                    <label for="ventar">Refacciones</label>
                  </div>
                  <div class="col s2 margenTopDivChosen">
                    <input type="checkbox" class="filled-in" id="ventap" name="eleccionCotizar" value="4"  onchange="eleccionCotizar2(4)" />
                    <label for="ventap">Servicio</label>
                  </div>
                  <div class="col s2 margenTopDivChosen">
                    <input type="checkbox" class="filled-in" id="ventaa" name="eleccionCotizar" value="5"  onchange="eleccionCotizar2(5)" />
                    <label for="ventaa">Accesorios</label>
                  </div>
                  
                </div>
                <div class="col s4" id="opcionesEquipos" style="display:none;">
                  <div class="col s5" style="margin-left: -12px;">
                    <i class="material-icons prefix">monetization_on</i>
                    <label for="estado">Tipo de Cotización</label>
                  </div>
                  <div class="col s7 margenTopDivChosen" >
                    <select id="tipo" name="tipo2" class="browser-default form-control-bmz" required>
                      <!--<option value="" disabled selected>Selecciona tipo de cotización</option>-->
                      <option value="1">Venta</option>
                      <!--<option value="2">Renta</option>
                      <option value="3">Póliza</option>-->
                    </select>
                  </div>
                </div>
                <div class="input-field col s2" style="display: none" id="opcionesConsumiblesRefacciones">
                  <div class="col s5" style="margin-left: -12px;">
                    <label for="estado">Tipo de Cotización</label>
                  </div>
                  <div class="col s7 margenTopDivChosen" >
                    <select id="tipo" name="tipo" class="browser-default chosen-select tipoConsumibles">
                      <option value="1">Venta</option>
                    </select>
                  </div>
                </div>
                <div class="col s4 editcotizacion">
                  <div class="col s5" >
                    <label for="estado">¿Que tipo?</label>
                  </div>
                  <div class="col s7 margenTopDivChosen" >
                    <select id="propuesta" name="propuesta" class="browser-default form-control-bmz" >
                      <option value="0">Cotizacion</option>
                      <option value="1">Propuesta</option>
                    </select>
                  </div>
                </div>
                
              </div>
              <div class="row">
                <div class="input-field col s6" id="tipoCotizacion" style="display: none;">
                  <div class="col s5" style="margin-left: -12px;">
                    <i class="material-icons prefix">format_list_bulleted</i>
                    <label for="estado">Cotización</label>
                  </div>
                  <div class="col s7 margenTopDivChosen" >
                    <select id="cotizar" name="cotizar" onchange="selectcotiza()" class="browser-default chosen-select">
                      <option value="" disabled selected>Forma de cotización</option>
                      <option value="1">Manual</option>
                      <!--<option value="2">Familia</option>-->
                    </select>
                  </div>
                </div>
                <div class="input-field col s6 select_familia" style="display: none;" id="select_familia">
                  <div class="col s5" style="margin-left: -12px;">
                    <i class="material-icons prefix">person</i>
                    <label for="estado">Familiar</label>
                  </div>
                  <div class="col s7 margenTopDivChosen" >
                    <select id="idfamilia" name="idfamilia" class="browser-default chosen-select" onchange="verificaFamilia(this)">
                      <option value="" disabled selected>Selecciona una opción</option>
                      
                    </select>
                  </div>
                </div>
                <div class="col s6 tiporentaselect" style="display: none">
                  <div class="col s5" style="margin-left: -12px;">
                    <i class="material-icons prefix">format_list_bulleted</i>
                    <label for="estado">Tipo de Renta</label>
                  </div>
                  <div class="col s7 margenTopDivChosen" >
                    <select id="tiporenta" name="tiporenta" class="browser-default form-control-bmz tipoConsumibles">
                      <option value="" disabled selected>Selecciona tipo de cotización</option>
                      <option value="0"></option>
                      <option value="1">Individual</option>
                      <option value="2">Global</option>
                    </select>
                  </div>
                  
                </div>
                
                <div class="row col s12 costoglobalvaldiv" style="display: none;">
                  <div class="col s2">
                    <label class="active" for="eg_rm">Renta Monocromatica</label>
                    <input type="text" id="eg_rm" name="eg_rm" value="0">
                  </div>
                  <div class="col s2">
                    <label class="active" for="eg_rvm">Volumen incluido monocromatico</label>
                    <input type="text" id="eg_rvm" name="eg_rvm" value="0">
                  </div>
                  <div class="col s2">
                    <label class="active" for="eg_rpem">Precio Excedente monocromatico</label>
                    <input type="text" id="eg_rpem" name="eg_rpem" value="0">
                  </div>
                  <div class="col s2">
                    <label class="active" for="eg_rc">Renta Color</label>
                    <input type="text" id="eg_rc" name="eg_rc" value="0">
                  </div>
                  <div class="col s2">
                    <label class="active" for="eg_rvc">Volumen incluido color</label>
                    <input type="text" id="eg_rvc" name="eg_rvc" value="0">
                  </div>
                  <div class="col s2">
                    <label class="active" for="eg_rpec">Precio Excedent color</label>
                    <input type="text" id="eg_rpec" name="eg_rpec" value="0">
                  </div>
                  <div class="col s6"></div>
                  <div class="col s6">
                    <input type="checkbox" class="" id="eg_compra_consumible" name="eg_compra_consumible" value="1"/>
                    <label for="eg_compra_consumible">No Compra consumible color</label>
                  </div>
                </div>
                
                
              </div>
              <div class="row" style="display: none;">
                <div class="input-field col s6" id="selectEquipos" style="display: none;">
                  <div class="col s5" style="margin-left: -12px;">
                    <i class="material-icons prefix">format_list_bulleted</i>
                    <label for="estado">Equipos</label>
                  </div>
                  <div class="col s7 margenTopDivChosen" >
                    <select id="equipoLista" name="equipoLista" class="browser-default chosen-select">
                      <option value="" disabled selected></option>
                      <?php foreach($equipos as $equipo)
                      {
                      ?>
                      <option value="<?php echo $equipo->id;?>" >
                        <?php echo $equipo->modelo;?>
                      </option>
                      <?php
                      }
                      ?>
                    </select>
                  </div>
                </div>
                
              </div>
              
              <!-------------------------------------------------------------->
              <!------------------- CONSUMIBLES POR EQUIPO ------------------->
            
              <!-------------------------------------------------------------->
              <!------------------- REFACCIONES POR EQUIPO ------------------->
              
              <div class="row rowPolizas" id="rowPolizas1" name="rowPolizas1" style="display:none;">
                <div class="input-field col s4" id="selectPolizas" style="display: none;">
                  <i class="material-icons prefix">format_list_bulleted</i>
                  <label for="estado">Pólizas Existentes</label>
                  <br>
                  <div class="col s12 margenTopDivChosen" >
                    <select id="polizasLista" name="polizasLista" class="browser-default chosen-select polizasLista" onchange="funcionDetalles($(this))">
                      <option value="" disabled selected></option>
                      <?php foreach($polizas as $poliza)
                      {
                      ?>
                      <option value="<?php echo $poliza->id;?>" >
                        <?php echo $poliza->nombre;?>
                      </option>
                      <?php
                      }
                      ?>
                    </select>
                  </div>
                </div>
                
              </div>
              
              <input type="hidden" name="idCliente" id="idCliente" value="0">
            </form>
            
              <div class="row">
                <ul class="collapsible" data-collapsible="accordion">
                  <li class="equipos_select" style="display: none;">
                    <div class="collapsible-header">Equipos</div>
                    <div class="collapsible-body">
                      <div class="col s6">
                        <div class="col s5" style="margin-left: -12px;">
                          <label>Seleccionar precio</label>
                        </div>
                        <div class="col s7">
                          <select  id="tipoprecioequipo" class="browser-default form-control-bmz">
                            <option value="0">General</option>
                            <option value="1">Reventa local</option>
                            <option value="2">Reventa foráneo</option>
                          </select>
                        </div>
                        
                        
                      </div>
                      <table id="tables_equipos_selected" class="display">
                        <thead>
                          <tr>
                            <th></th>
                            <th></th>
                            <th>Equipo</th>
                            <th>Consumibles</th>
                            <th>Accesorios</th>
                            <th></th>
                            <th></th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php foreach ($equiposimg as $item){
                          switch ($item->categoriaId) {
                          case 14:
                          $tipoimpresora=0;
                          break;
                          case 15:
                          $tipoimpresora=1;
                          break;
                          case 16:
                          $tipoimpresora=0;
                          break;
                          case 17:
                          $tipoimpresora=1;
                          break;
                          case 18:
                          $tipoimpresora=0;
                          break;
                          case 21:
                          $tipoimpresora=1;
                          break;
                          case 22:
                          $tipoimpresora=1;
                          break;
                          case 23:
                          $tipoimpresora=1;
                          break;
                          
                          default:
                          $tipoimpresora=0;
                          break;
                          }
                          ?>
                          <tr>
                            <td>
                              <input type="checkbox" class="equipo_check nombre" id="equipo_check_<?php echo $item->id ?>" value="<?php echo $tipoimpresora;?>"/>
                              <label for="equipo_check_<?php echo $item->id ?>"></label>
                              <input type="hidden" id="equipo_selected_all" value="<?php echo $item->id ?>"/>
                            </td>
                            <td style="width: 150px;">
                              <input type="text" id="equipo_cantidad" class="equipo_cantidad" value="1"  />
                            </td>
                            <td>
                              <label class="btn btn-info btn_agb_w has-badge-rounded has-badge-danger notificacion_tipped notificacion_tipped_<?php echo $item->id ?>" for="equipo_check_<?php echo $item->id ?>" data-badge="<?php echo $item->stockequipo;?>"
                                onmouseover="bodegasequipo(<?php echo $item->id ?>)" 
                                data-equipo="<?php echo $item->id ?>"
                                >
                                <?php echo $item->modelo ?>
                                
                              </label>
                            </td>
                            <td class="equipo_consumibleselected equipo_c_<?php echo $item->id ?>"></td>
                            <td class="equipo_accessoriosselected equipo_s_<?php echo $item->id ?>"></td>
                            <td class="equipo_rendimientoselected equipo_r_<?php echo $item->id ?>"></td>
                            <td><a class="btn btn-info"  style="padding: 0 5px;" onclick="addaccesorios(<?php echo $item->id ?>)"><i class="material-icons">list</i></a></td>
                          </tr>
                          <?php } ?>
                        </tbody>
                      </table>
                    </div>
                  </li>
                  <li class="selectEquiposConsumibles" style="display: none;">
                    <div class="collapsible-header">Consumibles</div>
                    <div class="collapsible-body">
                      <div class="row">
                        <div class="col s2">
                          <label>Equipos</label>
                          <select id="equipoListaConsumibles" name="equipoListaConsumibles" class="browser-default chosen-select equipoListaConsumibles" onchange="funcionConsumibles()">
                            <option value="" disabled selected></option>
                            <?php foreach($equipos as $equipo){?>
                            <option value="<?php echo $equipo->id;?>" ><?php echo $equipo->modelo;?></option>
                          <?php } ?>
                          </select>
                        </div>
                        <div class="col s3">
                          <label>Disponibles</label>
                          <select id="toner" name="toner" class="browser-default chosen-select toner" onchange="funcionConsumiblesprecios()">
                            <option value="" disabled selected>Selecciona una opción</option>
                          </select>
                        </div>
                        <div class="col s3">
                          <label>Precios</label>
                          <select id="tonerp" name="tonerp" class="browser-default form-control-bmz tonerp">
                            <option value="" disabled selected>Selecciona una opción</option>
                          </select>
                        </div>
                        <div class="col s2">
                          <label>Cantidad</label>
                          <input type="number" name="" min="1" max="5" id="piezasc" class="piezas form-control-bmz">
                        </div>
                        <div class="col s2">
                          <a class="btn-floating waves-effect waves-light purple lightrn-1 btn tooltipped" data-position="top" data-tooltip="Agregar" id="botonAgregarConsumible" name="botonAgregarConsumible" onclick="addcosumibles()">
                              <i class="material-icons">system_update_alt</i>
                            </a>
                        </div>
                        <div class="col s12">
                          <table class="table bordered striped" id="table-addconsumibles">
                            <tbody class="tbody-addconsumibles">
                              
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </li>
                  <li class="selectEquiposRefacciones" style="display: none;">
                    <div class="collapsible-header">Refacciones</div>
                    <div class="collapsible-body">
                      <div class="row">
                        <div class="col s1">
                          <a class="btn-bmz material-icons prefix" onClick="addh_refacciones_cotizacion()" title="Equipos registrados">format_list_bulleted</a>
                        </div>
                        <div class="col s2">
                          <label>Equipos</label>
                          <select id="equipoListaRefacciones" name="equipoListaRefacciones" class="browser-default chosen-select equipoListaRefacciones" onchange="funcionRefacciones()">
                            <option value="" disabled selected></option>
                            <?php foreach($equipos as $equipo){ ?>
                              <option value="<?php echo $equipo->id;?>" ><?php echo $equipo->modelo;?></option>
                            <?php }  ?>
                          </select>
                        </div>
                        <div class="col s2">
                          <label>Serie</label>
                          <input type="text" name="seriee" id="seriee" class="form-control-bmz seriee" placeholder=" ">
                        </div>
                        <div class="col s2">
                          <label>Disponibles</label>
                          <select id="refaccion" name="refaccion" class="browser-default chosen-select refaccion" onchange="selectprecior()">
                            <option value="" disabled selected>Selecciona una opción</option>
                          </select>
                        </div>
                        <div class="col s2">
                          <label for="estado">Precios</label>
                          <div class="refaccionesp_mostrar"></div>
                        </div>
                        <div class="col s2">
                          <label>Cantidad</label>
                          <input type="number" name="piezasRefacion" min="1" max="5" class="piezasRefacion form-control-bmz" id="piezasRefacion">
                        </div>
                        <div class="col s1">
                          <a class="btn-floating waves-effect waves-light purple lightrn-1 btn tooltipped" data-position="top" data-tooltip="Agregar" onclick="addrefaccion()"><i class="material-icons">system_update_alt</i></a>
                        </div>
                        <div class="col s12">
                          <table class="table bordered striped" id="table-addrefacciones">
                            <tbody class="tbody-addrefacciones">
                              
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </li>
                  <li class="addpolizass" style="display: none;">
                    <div class="collapsible-header">Pólizas</div>
                    <div class="collapsible-body">
                      <div class="row addpolizass" >
                        <a class="btn waves-effect waves-light cyan" id="btn_add_equipo_poliza">Agregar equipo</a>
                      </div>
                      <div class="row addpolizas">
                
                      </div>
                    </div>
                  </li>
                  <li class="accesorios_selected" style="display: none;">
                    <div class="collapsible-header">Accesorios</div>
                    <div class="collapsible-body">
                      <div class="row">
                        <div class="col s12">
                          <table class="table" id="table_venta_accesorios">
                            <thead>
                              <tr>
                                <th>Equipo</th>
                                <th>Accesorio</th>
                                <th>Precio</th>
                                <th>Cantidad</th>
                                <th></th>
                              </tr>
                              <tr>
                                <th>
                                  <select id="equipo_acce" class="browser-default chosen-select" onchange="obtenerabsesorios()">
                                      <option value="" disabled selected></option>
                                      <?php foreach($equipos as $equipo) { ?>
                                          <option value="<?php echo $equipo->id;?>" >
                                              <?php echo $equipo->modelo;?>
                                          </option>
                                      <?php } ?>
                                  </select>
                                </th>
                                <th>
                                  <select id="accesorio_acce" class="browser-default chosen-select" onchange="obtenerabsesorioscosto()">
                                      
                                  </select>
                                </th>
                                <th>
                                  <input type="number" id="precio_acce" class="form-control-bmz" readonly>
                                </th>
                                <th>
                                  <input type="number" id="cantidad_acc" class="form-control-bmz">
                                </th>
                                
                                <th>
                                  <a class="btn-floating waves-effect waves-light purple lightrn-1 btn tooltipped" data-position="top" data-tooltip="Agregar" id="botonAgregarConsumible" name="botonAgregarConsumible" onclick="addaccesoriov()">
                                    <i class="material-icons">system_update_alt</i>
                                  </a>
                                </th>
                              </tr>
                            </thead>
                            <tbody class="tbody_acce"></tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </li>
                </ul>
              </div>
        <!------------------------------------->