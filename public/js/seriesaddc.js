var base_url = $('#base_url').val();
var trserieequipo=0;
var trserieaccesorio=0;
var trserierefaccion=0;
var tablee;
var tablea;
var tabler;
var addtr=0;
$(document).ready(function(){
	$('.chosen-select').chosen({width: "100%"});
	$('.addbtnequipo').click(function(event) {
		
		var equipo=$('#equipos option:selected').val();
		var equipot=$('#equipos option:selected').text();
		var serie=$('#serie1').val();
		var precio=$('#Precio1').val();

		var equipo_pasa=1;
		var TABLA   = $("#tabla_sequipos tbody > tr");
	        TABLA.each(function(){ 
	            if(equipo==$(this).find("input[id*='equipo']").val()){
	            	equipo_pasa=0;
	            }
	        });
	    if(equipo_pasa==1){
			if (serie!='') {
					//==========================================
				    addbtnequipo(equipo,equipot,serie,precio);
				    //====================================
			}else{
				alertfunction('Atención!','El numero de cantidad no debe de estar vacio');
			}
		}else{
			$.confirm({
		        boxWidth: '30%',
		        useBootstrap: false,
		        icon: 'fa fa-warning',
		        title: 'Atención!',
		        content: 'El Modelo ya se encuentra agregado, ¿Desea Continuar?',
		        type: 'red',
		        typeAnimated: true,
		        buttons:{
		            confirmar: function (){
		                addbtnequipo(equipo,equipot,serie,precio);
		            },
		            cancelar: function (){
		                
		            }
		        }
		    });
		}
	});
	$('.addbtnaccesorio').click(function(event) {
		var accesorio=$('#accesorio option:selected').val();
		var accesoriot=$('#accesorio option:selected').text();
		var serie=$('#serie2').val();
		var precio=$('#Precio2').val();

		var accesorio_pasa=1;
		var TABLA   = $("#tabla_saccesorios tbody > tr");
        TABLA.each(function(){ 
        	if(accesorio==$(this).find("input[id*='accesorio']").val()){
        		accesorio_pasa=0;
        	}        
        });
        if(accesorio_pasa==1){
			if (serie!='') {
					//==========================================
				    addbtnaccesorio(accesorio,accesoriot,serie,precio);
				    //====================================
			}else{
				alertfunction('Atención!','El numero de cantidad no debe de estar vacio');
			}
		}else{
			$.confirm({
		        boxWidth: '30%',
		        useBootstrap: false,
		        icon: 'fa fa-warning',
		        title: 'Atención!',
		        content: 'El Modelo ya se encuentra agregado, ¿Desea Continuar?',
		        type: 'red',
		        typeAnimated: true,
		        buttons:{
		            confirmar: function (){
		                addbtnaccesorio(accesorio,accesoriot,serie,precio);
		            },
		            cancelar: function (){
		                
		            }
		        }
		    });
		}
	});
	$('.addbtnrefaccion').click(function(event) {
		
		var refaccion=$('#refaccion option:selected').val();
		var refacciont=$('#refaccion option:selected').text();
		var serie=$('#serie3').val();
		var precio=$('#Precio3').val();

		var refaccion_pasa=1;
		var TABLA   = $("#tabla_srefacciones tbody > tr");
            TABLA.each(function(){ 
	            if(refaccion==$(this).find("input[id*='refaccion']").val()){
	            	refaccion_pasa=0;
	            }
	        });
	    if(refaccion_pasa==1){
			if (serie!='') {
					//==========================================
					
				    addbtnrefaccion(refaccion,refacciont,serie,precio);
				    //====================================
			}else{
				alertfunction('Atención!','El numero de cantidad no debe de estar vacio');
			}
		}else{
			$.confirm({
		        boxWidth: '30%',
		        useBootstrap: false,
		        icon: 'fa fa-warning',
		        title: 'Atención!',
		        content: 'El Modelo ya se encuentra agregado, ¿Desea Continuar?',
		        type: 'red',
		        typeAnimated: true,
		        buttons:{
		            confirmar: function (){
		                addbtnrefaccion(refaccion,refacciont,serie,precio);
		            },
		            cancelar: function (){
		                
		            }
		        }
		    });
		}
	});
	$('.addbtnconsumible').click(function(event) {
		
		var consumible=$('#consumible option:selected').val();
		var consumiblet=$('#consumible option:selected').text();
		var cantidad=$('#cantidad4').val();
		var precio=$('#Precio4').val();

		var consumible_pasa=1;
		var TABLA   = $("#tabla_sconsumible tbody > tr");
	        TABLA.each(function(){ 
	        	if(consumible==$(this).find("input[id*='consumible']").val()){
	        		consumible_pasa=0;
	        	}
	        });

	    if(consumible_pasa==1){
			if (cantidad!='') {
					//==========================================
				    addbtnconsumible(consumible,consumiblet,cantidad,precio);
				    //====================================
			}else{
				alertfunction('Atención!','El numero de cantidad no debe de estar vacio');
			}
		}else{
			$.confirm({
		        boxWidth: '30%',
		        useBootstrap: false,
		        icon: 'fa fa-warning',
		        title: 'Atención!',
		        content: 'El Modelo ya se encuentra agregado, ¿Desea Continuar?',
		        type: 'red',
		        typeAnimated: true,
		        buttons:{
		            confirmar: function (){
		                addbtnconsumible(consumible,consumiblet,cantidad,precio);
		            },
		            cancelar: function (){
		                
		            }
		        }
		    });
		}
	});
	$('.saveallseries').click(function(event) {
			$(".aceptar_series").prop("disabled", true);
		  	setTimeout(function(){ 
		      	$(".saveallseries").prop("disabled", false);
		  	}, 5000);
		var folio=$('#folio').val();
		var idcompra=$('#idcompra').val();
		if(folio!=''){
			var DATAe  = [];
	        var TABLA   = $("#tabla_sequipos tbody > tr");
	            TABLA.each(function(){         
	            item = {};
	            item ["equipo"]   = $(this).find("input[id*='equipo']").val();
	            item ["serie"]  = $(this).find("input[id*='serie']").val();
	            item ["precio"]  = $(this).find("input[id*='precio']").val();
	            DATAe.push(item);
	        });
	        INFO  = new FormData();
	        arrayequipos   = JSON.stringify(DATAe);

	        var DATAa  = [];
	        var TABLA   = $("#tabla_saccesorios tbody > tr");
	            TABLA.each(function(){         
	            item = {};
	            item ["accesorio"]   = $(this).find("input[id*='accesorio']").val();
	            item ["serie"]  = $(this).find("input[id*='serie']").val();
	            item ["con_serie"]  = $(this).find("input[class*='sinseriea']").is(':checked')==true?0:1;
	            item ["precio"]  = $(this).find("input[id*='precio']").val();
	            DATAa.push(item);
	        });
	        INFO  = new FormData();
	        arrayacessorios   = JSON.stringify(DATAa);

	        var DATAr  = [];
	        var TABLA   = $("#tabla_srefacciones tbody > tr");
	            TABLA.each(function(){         
	            item = {};
	            item ["refaccion"]   = $(this).find("input[id*='refaccion']").val();
	            item ["serie"]  = $(this).find("input[id*='serie']").val();
	            item ["con_serie"]  = $(this).find("input[class*='sinserie']").is(':checked')==true?0:1;
	            item ["precio"]  = $(this).find("input[id*='precio']").val();
	            DATAr.push(item);
	        });
	        INFO  = new FormData();
	        arrayrefacciones  = JSON.stringify(DATAr);

	        var DATAc  = [];
	        var TABLA   = $("#tabla_sconsumible tbody > tr");
	            TABLA.each(function(){         
	            item = {};
	            item ["consumible"]   = $(this).find("input[id*='consumible']").val();
	            item ["cantidad"]  = $(this).find("input[id*='cantidad']").val();
	            item ["precio"]  = $(this).find("input[id*='precio']").val();
	            DATAc.push(item);
	        });
	        INFO  = new FormData();
	        arrayconsumibles  = JSON.stringify(DATAc);

		    var datos = 'arrayequipos='+arrayequipos+'&arrayacessorios='+arrayacessorios+'&arrayrefacciones='+arrayrefacciones+'&arrayconsumibles='+arrayconsumibles+'&folio='+folio+'&idcompra='+idcompra;
		    $.ajax({
		        type:'POST',
		        url: base_url+"index.php/Series/addseriesc",
		        data: datos,
		        success: function (response){
		            idventatabler=response;
	                alertfunction('Éxito!','Se ha creado la compra: '+response);
	                setTimeout(function(){ 
	                    window.location.href = base_url+"index.php/Compras";
	                }, 3000);
		        },
		        error: function(response){
		            alertfunction('Atención!','Algo salió mal, intente de nuevo o contacte al administrador del sistema'); 
		        }
		    });
		}else{
			alertfunction('Atención!','Folio no puede estar vacio'); 
		}	
	});
});
function alertfunction(titlea,contenta){
    $.alert({boxWidth: '30%',useBootstrap: false,title: titlea,theme: 'bootstrap',content: contenta});  
}
function deteletr(id){
	$('.addtr_'+id).remove();
}
function addbtnequipo(equipo,equipot,serie,precio){
	var addtre='<tr class="addtr_'+addtr+'">\
                	<td><input id="equipo" type="hidden"  value="'+equipo+'" readonly>'+equipot+'</td>\
                <td><input id="serie" type="text" class="datostableinput2 seriev" value="'+serie+'" readonly></td>\
                <td><input id="precio" type="text" class="datostableinput2 seriev" value="'+precio+'" readonly></td>\
                <td><a class="btn-floating waves-effect waves-light red accent-2" onclick="deteletr('+addtr+')"><i class="material-icons">clear</i></a></td>\
              </tr>';
    $('.tabla_addsequipos').append(addtre);
    addtr++;
}
function addbtnaccesorio(accesorio,accesoriot,serie,precio){
	var addtra='<tr class="addtr_'+addtr+'">\
	                	<td><input id="accesorio" type="hidden"  value="'+accesorio+'" readonly>'+accesoriot+'</td>\
	                <td><input id="serie" type="text" class="datostableinput2 seriev" value="'+serie+'" readonly></td>\
	                <td><input type="checkbox" id="sinseriea_'+addtr+'" class="sinseriea" /><label for="sinseriea_'+addtr+'">Sin series</label></td>\
	                <td><input id="precio" type="text" class="datostableinput2 seriev" value="'+precio+'" readonly></td>\
	                <td><a class="btn-floating waves-effect waves-light red accent-2" onclick="deteletr('+addtr+')"><i class="material-icons">clear</i></a></td>\
	              </tr>';
	    $('.tabla_addsaccesorios').append(addtra);
	    addtr++;
}
function addbtnrefaccion(refaccion,refacciont,serie,precio){
	var addtre='<tr class="addtr_'+addtr+'">\
                	<td><input id="refaccion" type="hidden"  value="'+refaccion+'" readonly>'+refacciont+'</td>\
                <td><input id="serie" type="text" class="datostableinput2 seriev" value="'+serie+'" readonly></td>\
                <td><input type="checkbox" id="sinserie_'+addtr+'" class="sinserie" /><label for="sinserie_'+addtr+'">Sin series</label></td>\
                <td><input id="precio" type="text" class="datostableinput2 seriev" value="'+precio+'" readonly></td>\
                <td><a class="btn-floating waves-effect waves-light red accent-2" onclick="deteletr('+addtr+')"><i class="material-icons">clear</i></a></td>\
              </tr>';
    $('.tabla_addsrefacciones').append(addtre);
    addtr++;
}
function addbtnconsumible(consumible,consumiblet,cantidad,precio){
	var addtre='<tr class="addtr_'+addtr+'">\
                	<td><input id="consumible" type="hidden"  value="'+consumible+'" readonly>'+consumiblet+'</td>\
                <td><input id="cantidad" type="text" class="datostableinput2 seriev" value="'+cantidad+'" readonly></td>\
                <td><input id="precio" type="text" class="datostableinput2 seriev" value="'+precio+'" readonly></td>\
                <td><a class="btn-floating waves-effect waves-light red accent-2" onclick="deteletr('+addtr+')"><i class="material-icons">clear</i></a></td>\
              </tr>';
    $('.tabla_addsconsumible').append(addtre);
    addtr++;
}