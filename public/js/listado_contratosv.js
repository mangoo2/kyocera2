var base_url = $('#base_url').val();
var tablef;
$(document).ready(function($) {
	tablef = $('#tabla_contratos').DataTable();
	loadtable();
});
function loadtable(){
	tablef.destroy();
	tablef = $('#tabla_contratos').DataTable({
        pagingType: 'input',
        search: {
                return: true
            }, 
        stateSave: true,//guarda el ultimo estado
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url+"index.php/R_contratosv/getlistconven",
            type: "post",
            "data": {
                'personal':0
            },
        },
        "columns": [
            {"data": "idcontrato"},
            {"data": "folio"},
            {"data": "empresa",},
            {"data": "fechainicio"},
            {"data": "meses"},
            {"data": "fecha_final"},
            {"data": "conti_cont_fecha"},
            {"data": null,
                render:function(data,type,row){
                    var html='';
                    	if(row.permt_continuar==1){
                    		var icon='<i class="fas fa-check"></i>';
                    		var title='';
                    		var btn_class='b-btn-success';
                    	}else{
                    		var icon='<i class="fas fa-calendar"></i>';
                    		var title='';
                    		var btn_class='b-btn-primary';
                    	}
                        html='<button type="button" class="b-btn '+btn_class+'" onclick="permitircontinuar('+row.idcontrato+','+row.permt_continuar+')" title="">'+icon+'</button>';
                    return html;
                }
            },
            
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[20,25, 50, 100,200,400,-1], [20,25, 50,100,200,400,'todo']],
        //"lengthMenu": [[25, 50, 100,200,400], [25, 50,100,200,400]],
        //"searching": false
        //columnDefs: [ { orderable: false, targets: [8,9] }],// deshabilita el ordenamiento de la columna
        
    }).on('draw',function(){
        $('.dataTables_filter').append('<i class="fa fa-search buttonsearch" onclick="searchtable_vf()"></i>');
    });
}
function searchtable_vf(){
        var searchv =$("#tabla_contratos_filter input[type=search]").val();
        tablef.search(searchv).draw();
}
function permitircontinuar(idcon,tipo){
		var content_label='';
		var title_label='';
		var type_color='';
		if(tipo==1){
			content_label='¿Desea retirar el permiso que el contrato se pueda continuar sin contraseña?';
			title_label='No Permitir continuar';
			type_color='red';
		}else{
			content_label='¿Desea permitir que el contrato se pueda continuar sin contraseña?';
			title_label='Permitir continuar';
			type_color='green';
		}
	$.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: title_label,
        content: content_label+'<br>Se necesita permisos de administrador<br>'+
                 '<input type="text" placeholder="Contraseña" id="contrasena_pc" name="contrasena_pc" class="name form-control-bmz"/>',
        type: type_color,
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                //var pass=$('#contrasena').val();
                var pass=$("input[name=contrasena_pc]").val();
                if (pass!='') {
                    $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Sistema/solicitarpermiso",
                        data: {
                            pass:pass
                        },
                        success: function (response){
                                var respuesta = parseInt(response);
                                if (respuesta==1) {
                    //=============================================================================
                        permitircontinuar2(idcon,tipo);
                    //=============================================================================
                                }else{
                                    notificacion(2); 
                                }
                        },
                        error: function(response){
                            notificacion(1);  
                             
                        }
                    });

                }else{
                    notificacion(0);
                }
                
            },
            cancelar: function (){
                
            }
        }
    });
    setTimeout(function(){
            new MaskedPassword(document.getElementById("contrasena_pc"), '\u25CF');
    },500);
}
function permitircontinuar2(idcon,tipo){
	$.ajax({
        type:'POST',
        url: base_url+"R_contratosv/permitircontinuar",
        data: {
            idcon:idcon,
            tipo:tipo
        },
        statusCode:{
	        404: function(data){
	            toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema", "Advertencia");
	        },
	        500: function(){
	            toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema", "Advertencia");
	        }
	    },
        success: function (data){
            toastr.success("Cambio Realizado");  
            loadtable();  
        }
    });
}