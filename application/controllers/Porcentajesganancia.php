<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Porcentajesganancia extends CI_Controller 
{
    public function __construct() 
    {
        parent::__construct();
        $this->load->model('Porcentajes_model', 'model');
        $this->load->model('login_model');
        $this->load->helper('url');
        $this->load->library('menus');
        $this->load->model('ModeloCatalogos');
        $this->submenu=21;
        if($this->session->userdata('logeado')==true){
            $this->perfilid =$this->session->userdata('perfilid');
            $this->idpersonal = $this->session->userdata('idpersonal');
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,$this->submenu);// 21 es el id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
        }else{
            redirect('login');
        }
    }

    // Listado de porcentajes
    function index(){
            $data['MenusubId']=$this->submenu;
            // Obtiene los menús disponibles del sistema dependiendo el perfil, ayudado de la libreria "MENUS"
            $menus = $this->menus->getMenus($this->session->userdata('perfilid'));
            $data['menus'] = $menus['menus'];
            $data['submenusArray'] = $menus['submenusArray'];
            $data['porcentajes'] = $this->model->getPorcentajesGanancia();
            
            $this->load->view('header');
            $this->load->view('main',$data);
            $this->load->view('configuraciones/porcentajes/form_porcentajes');
            $this->load->view('configuraciones/porcentajes/form_porcentajes_js');
            $this->load->view('footer');
        
        
    }

    function actualizarPorcentajesGanancia(){
        // Obtenemos el POST que viene del formulario
        $data = $this->input->post();
        
        // Organizamos los datos, para dentro de un ciclo actualizar
        $datos[1] = array('gananciaVenta' => $data["porcentaje_equipos_venta"],
                          'gananciaRenta' => $data["porcentaje_equipos_renta"],
                          'gananciaPoliza' => $data["porcentaje_equipos_poliza"],
                          'gananciaRevendedor' => $data["porcentaje_equipos_revendedor"]);

        $datos[2] = array('gananciaVenta' => $data["porcentaje_refacciones_venta"],
                          'gananciaRenta' => $data["porcentaje_refacciones_renta"],
                          'gananciaPoliza' => $data["porcentaje_refacciones_poliza"],
                          'gananciaRevendedor' => $data["porcentaje_refacciones_revendedor"]);

        $datos[3] = array('gananciaVenta' => $data["porcentaje_consumibles_venta"],
                          'gananciaRenta' => $data["porcentaje_consumibles_renta"],
                          'gananciaPoliza' => $data["porcentaje_consumibles_poliza"],
                          'gananciaRevendedor' => $data["porcentaje_consumibles_revendedor"]);
        
        $datos[4] = array('gananciaVenta' => $data["porcentaje_accesorios_venta"],
                          'gananciaRenta' => $data["porcentaje_accesorios_renta"],
                          'gananciaPoliza' => $data["porcentaje_accesorios_poliza"],
                          'gananciaRevendedor' => $data["porcentaje_accesorios_revendedor"]);

        // Como los ID's sono fijos, los vamos pasando conforme se ejecute el ciclo
        for ($i=1; $i <=4 ; $i++) 
        { 
            // Actualiza cada registro
            $this->model->actualizaPorcentajes($i,$datos[$i]);
        }
        //$this->Modelobitacoras->Insert(array('contenido'=>'Se realizo una actializacion del porcentale de ganancia','nombretabla'=>'porcentajeGanancia','idtable'=>0,'tipo'=>'Update','personalId'=>$this->idpersonal));
    }

   
}

?>