<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class AsignacionesTecnico extends CI_Controller {
    public function __construct()     {
        parent::__construct();
        //$this->load->model('Clientes_model', 'model');
        date_default_timezone_set('America/Mexico_City');
        $this->load->model('ModeloCatalogos');
        $this->load->model('Configuraciones_model');
        $this->load->model('ModeloAsignacion');
        $this->load->model('Ventas_model');
        $this->load->model('Rentas_model');
        $this->load->model('ModeloGeneral');
        $this->load->model('Login_model');
        $this->load->helper('url');
        $this->fechahoy = date('Y-m-d');
        $this->fechahoycorta = date('Y-m-d');
        $this->horaactual = date('H:i:s');
        $this->version = date('YmdHi');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->tolerancia='-10 minute';
        $this->tolerancia2='+10 minute';
        $this->submenu=56;
        if($this->session->userdata('logeado')==true){
            $this->perfilid =$this->session->userdata('perfilid');
            $this->idpersonal = $this->session->userdata('idpersonal');
        }else{
            redirect('login');
        }
    }

    function index($tecnico,$tipo){
        
    }
    function info($tecnico,$tipo){
        $data['MenusubId']=$this->submenu;
        $data['tecnico']=$tecnico;
        $data['tipocpe']=$tipo;
        $data['perfilid']=$this->perfilid;
        $data['idpersonal']=$this->idpersonal;
        $data['version']=$this->version;
        if (isset($_GET['fech'])) {
          $fechahoy=$_GET['fech'];
        }else{
          $fechahoy=$this->fechahoy;
        }
        $data['fechahoy']=$fechahoy;
        $this->load->view('header');
        $this->load->view('main');
        $this->load->view('asignacionestecnico/asignacion',$data);
        $this->load->view('footerl');
        $this->load->view('asignacionestecnico/asignacionjs');
        $this->load->view('rentas/servicios_modal');
        $this->load->view('asignacionestecnico/ser_modal_report');
    }
    public function servicio(){
        $this->load->view('header');
        $this->load->view('main');
        $this->load->view('asignacionestecnico/asignacion2');
        $this->load->view('footer');
        $this->load->view('asignacionestecnico/asignacionjs');
    }
    public function servicio3(){
        $this->load->view('header');
        $this->load->view('main');
        $this->load->view('asignacionestecnico/asignacion3');
        $this->load->view('footer');
        $this->load->view('asignacionestecnico/asignacionjs');
    }
    public function servicio4(){
        $this->load->view('header');
        $this->load->view('main');
        $this->load->view('asignacionestecnico/asignacion4');
        $this->load->view('footer');
        $this->load->view('asignacionestecnico/asignacionjs');
    }
    public function servicio5(){
        $this->load->view('header');
        $this->load->view('main');
        $this->load->view('asignacionestecnico/asignacion5');
        $this->load->view('footer');
        $this->load->view('asignacionestecnico/asignacionjs');
    }
    function clientes(){
        $tecnico = $this->input->post('tecnico');
        $tipocpe = $this->input->post('tipocpe');
        $fecha   = $this->input->post('fechaselect');
        //$fecha='2020-05-25';//borrar xxx
        $result = $this->ModeloAsignacion->getlistadocalentariotipo($fecha,$fecha,0,$tecnico,0,0,$this->perfilid,$this->idpersonal,$tipocpe,0,0);
        $html ='<option value="0" data-tipo="0">Seleccione</option>';
        
        foreach ($result->result() as $item) {
            $html .='<option value="'.$item->asignacionId.'" data-tipo="'.$item->tipo.'">'.$item->empresa.'</option>';
        }
        echo $html;
    }
    function clientes2(){
        $tecnico = $this->input->post('tecnico');
        $tipocpe = $this->input->post('tipocpe');
        $fecha   = $this->input->post('fechaselect');
        //$fecha='2020-05-25';//borrar xxx
        $result1 = $this->ModeloAsignacion->getlistadocalentariotipo($fecha,$fecha,0,$tecnico,0,0,$this->perfilid,$this->idpersonal,1,0,0);
        $result2 = $this->ModeloAsignacion->getlistadocalentariotipo($fecha,$fecha,0,$tecnico,0,0,$this->perfilid,$this->idpersonal,2,0,0);
        $result3 = $this->ModeloAsignacion->getlistadocalentariotipo($fecha,$fecha,0,$tecnico,0,0,$this->perfilid,$this->idpersonal,3,0,0);
        $result4 = $this->ModeloAsignacion->getlistadocalentariotipo($fecha,$fecha,0,$tecnico,0,0,$this->perfilid,$this->idpersonal,4,0,0);

        $html ='<div class="middle">';
        $arrays_tr=array();
        $arrays_tr[]=array('pri'=>0,'html'=>'');
        foreach ($result1->result() as $item) {
            //$html .='<option value="'.$item->asignacionId.'" data-tipo="'.$item->tipo.'">'.$item->empresa.'</option>';
            $html2='<label><input type="radio" name="radio" value="'.$item->asignacionId.'" data-tipo="'.$item->tipo.'" />';
                    $html2.='<div class="front-end box prioridad_'.$item->prioridad.' search_select '.$item->series.'" onclick="cargaequipos2('.$item->asignacionId.','.$item->tipo.','.$item->stock_toner.')">';
                        $html2.='<span class="titlet">'.$item->empresa.'</span>';
                        $html2.='<div class="triangulo pri2_'.$item->prioridad2.'"></div>';
                    $html2.='</div></label>';
            $arrays_tr[]=array('pri'=>$item->prioridad2,'html'=>$html2);
        }
        foreach ($result2->result() as $item) {
            //$html .='<option value="'.$item->asignacionId.'" data-tipo="'.$item->tipo.'">'.$item->empresa.'</option>';
            $html2='<label><input type="radio" name="radio" value="'.$item->asignacionId.'" data-tipo="'.$item->tipo.'" />';
                        $html2.='<div class="front-end box prioridad_'.$item->prioridad.' search_select '.$item->series.'" onclick="cargaequipos2('.$item->asignacionId.','.$item->tipo.',0)">';
                            $html2.='<span class="titlet">'.$item->empresa.'</span>';
                            $html2.='<div class="triangulo pri2_'.$item->prioridad2.'"></div>';
                        $html2.='</div></label>';
            $arrays_tr[]=array('pri'=>$item->prioridad2,'html'=>$html2);
        }
        foreach ($result3->result() as $item) {
            //$html .='<option value="'.$item->asignacionId.'" data-tipo="'.$item->tipo.'">'.$item->empresa.'</option>';
            $html2='<label><input type="radio" name="radio" value="'.$item->asignacionId.'" data-tipo="'.$item->tipo.'" />';
                    $html2.='<div class="front-end box prioridad_'.$item->prioridad.' search_select '.$item->series.'" onclick="cargaequipos2('.$item->asignacionId.','.$item->tipo.',0)">';
                        $html2.='<span class="titlet">'.$item->empresa.'</span>';
                        $html2.='<div class="triangulo pri2_'.$item->prioridad2.'"></div>';
                    $html2.='</div></label>';
            $arrays_tr[]=array('pri'=>$item->prioridad2,'html'=>$html2);
        }
        foreach ($result4->result() as $item) {
            //$html .='<option value="'.$item->asignacionId.'" data-tipo="'.$item->tipo.'">'.$item->empresa.'</option>';
            $html2='<label><input type="radio" name="radio" value="'.$item->asignacionId.'" data-tipo="'.$item->tipo.'" />';
                        $html2.='<div class="front-end box prioridad_'.$item->prioridad.' search_select '.$item->series.'" onclick="cargaequipos2('.$item->asignacionId.','.$item->tipo.',0)">';
                            $html2.='<span class="titlet">'.$item->empresa.'</span>';
                            $html2.='<div class="triangulo pri2_'.$item->prioridad2.'"></div>';
                        $html2.='</div></label>';
            $arrays_tr[]=array('pri'=>$item->prioridad2,'html'=>$html2);
        }
            //==============================================
                foreach ($arrays_tr as $key => $row) {
                    $aux[$key] = $row['pri'];
                }
                array_multisort($aux, SORT_DESC, $arrays_tr);
                foreach ($arrays_tr as $key => $row) {
                        $html.=$row['html'];
                }
            //=========================================
        $html.='</div>';
        echo $html;
    }
    function clientes3(){
        $tecnico = $this->input->post('tecnico');
        $tipocpe = $this->input->post('tipocpe');
        $fecha   = $this->input->post('fechaselect');
        $serviciospendientes=0;
        if($fecha===$this->fechahoycorta){
            $fecha_inicial = date("Y-m-d",strtotime($fecha."- 1 week")); 
            $fecha_final = date("Y-m-d",strtotime($fecha."- 1 days")); 
            $result1 = $this->ModeloAsignacion->getlistadocalentariotipo($fecha_inicial,$fecha_final,0,$tecnico,0,0,$this->perfilid,$this->idpersonal,1,0,0);
            $result2 = $this->ModeloAsignacion->getlistadocalentariotipo($fecha_inicial,$fecha_final,0,$tecnico,0,0,$this->perfilid,$this->idpersonal,2,0,0);
            $result3 = $this->ModeloAsignacion->getlistadocalentariotipo($fecha_inicial,$fecha_final,0,$tecnico,0,0,$this->perfilid,$this->idpersonal,3,0,0);
            $result4 = $this->ModeloAsignacion->getlistadocalentariotipo($fecha_inicial,$fecha_final,0,$tecnico,0,0,$this->perfilid,$this->idpersonal,4,0,0);
            $serviciospendientes=$result1->num_rows()+$result2->num_rows()+$result3->num_rows()+$result4->num_rows();

        }
        echo $serviciospendientes;
    }
    function clientes4(){
        $tecnico = $this->input->post('tecnico');
        $tipocpe = $this->input->post('tipocpe');
        $fecha   = $this->input->post('fechaselect');
        //$fecha='2020-05-25';//borrar xxx
        $result1 = $this->ModeloAsignacion->getlistadocalentariotipo($fecha,$fecha,0,$tecnico,0,0,$this->perfilid,$this->idpersonal,1,0,0);
        $result2 = $this->ModeloAsignacion->getlistadocalentariotipo($fecha,$fecha,0,$tecnico,0,0,$this->perfilid,$this->idpersonal,2,0,0);
        $result3 = $this->ModeloAsignacion->getlistadocalentariotipo($fecha,$fecha,0,$tecnico,0,0,$this->perfilid,$this->idpersonal,3,0,0);
        $result4 = $this->ModeloAsignacion->getlistadocalentariotipo($fecha,$fecha,0,$tecnico,0,0,$this->perfilid,$this->idpersonal,4,0,0);

        $arrays_tr=array();
        $asig_1='';
        $asig_2='';
        $asig_3='';
        $asig_4='';
        foreach ($result1->result() as $item) {
            $asignacionId = $item->asignacionId;
            $idCliente = $item->idCliente;
            $rest_dir = $this->ModeloAsignacion->otenerdireccion($asignacionId,1);
            $asig_1="|1,$asignacionId|";
            $asig_v="|$asignacionId,1°";
            if($item->hora==$item->horafin){
                $horafin=$item->horafin;
                $fijo=1;
            }else{
                $horafin = date('H:i:s', strtotime('-60 minutes', strtotime($item->horafin)));
                $fijo=0;
                //log_message('error','0 asignacion:'.$asignacionId.' horafin:'.$item->horafin.' '.$horafin);
            }
            
            if($horafin=='19:00:00'){
                $horafin = date('H:i:s', strtotime('-120 minutes', strtotime($item->horafin)));
                //log_message('error','1 asignacion:'.$asignacionId.' horafin:'.$item->horafin.' '.$horafin);
            }
            //===================================
                // Convertir a objetos DateTime
                $timeInicial = DateTime::createFromFormat('H:i:s', $item->hora);
                $timeFin = DateTime::createFromFormat('H:i:s', $horafin);

                // Comparar las horas
                if ($timeFin < $timeInicial) {
                    //echo "La hora de fin ($horafin) es menor que la hora de inicio ($item->hora).";
                    //$horafin = date('H:i:s', strtotime('+60 minutes', strtotime($item->horafin)));
                    $horafin =$item->horafin;
                } else {
                    //echo "La hora de fin ($horafin) no es menor que la hora de inicio ($item->hora).";
                }
            //===================================
            $arrays_tr[]=array('emp'=>$item->empresa,'asig'=>$asignacionId,'asigg'=>$asig_1,'tipo'=>1,'idrow'=>$rest_dir['idrow'],'direccion'=>$rest_dir['direccion'],'longitud'=>$rest_dir['longitud'],'latitud'=>$rest_dir['latitud'],'cli'=>$item->idCliente,'hora'=>$item->hora,'horafin'=>$horafin,'prioridad2'=>$item->prioridad2,'hora_cri_ser'=>$item->hora_cri_ser,'hora_comida'=>$item->hora_comida,'horafin_comida'=>$item->horafin_comida,'vinc'=>$asig_v,'fijo'=>$fijo);
        }
        foreach ($result2->result() as $item) {
            $asignacionId = $item->asignacionId;
            $idCliente = $item->idCliente;
            $asig_2="|2,$asignacionId|";
            $asig_v="|$asignacionId,2°";
            $rest_dir = $this->ModeloAsignacion->otenerdireccion($asignacionId,2);

            if($item->hora==$item->horafin){
                $horafin=$item->horafin;
                $fijo=1;
            }else{
                $horafin = date('H:i:s', strtotime('-60 minutes', strtotime($item->horafin)));
                $fijo=0;
            }
            if($horafin=='18:00:00'){
                $horafin = date('H:i:s', strtotime('-120 minutes', strtotime($item->horafin)));
            }
            //===================================
                // Convertir a objetos DateTime
                $timeInicial = DateTime::createFromFormat('H:i:s', $item->hora);
                $timeFin = DateTime::createFromFormat('H:i:s', $horafin);

                // Comparar las horas
                if ($timeFin < $timeInicial) {
                    //echo "La hora de fin ($horafin) es menor que la hora de inicio ($item->hora).";
                    //$horafin = date('H:i:s', strtotime('+60 minutes', strtotime($item->horafin)));
                    $horafin =$item->horafin;
                } else {
                    //echo "La hora de fin ($horafin) no es menor que la hora de inicio ($item->hora).";
                }
            //===================================
            $arrays_tr[]=array('emp'=>$item->empresa,'asig'=>$asignacionId,'asigg'=>$asig_2,'tipo'=>2,'idrow'=>$rest_dir['idrow'],'direccion'=>$rest_dir['direccion'],'longitud'=>$rest_dir['longitud'],'latitud'=>$rest_dir['latitud'],'cli'=>$item->idCliente,'hora'=>$item->hora,'horafin'=>$horafin,'prioridad2'=>$item->prioridad2,'hora_cri_ser'=>$item->hora_cri_ser,'hora_comida'=>$item->hora_comida,'horafin_comida'=>$item->horafin_comida,'vinc'=>$asig_v,'fijo'=>$fijo);
        }
        foreach ($result3->result() as $item) {
            $asignacionId = $item->asignacionId;
            $idCliente = $item->idCliente;
            $asig_3="|3,$asignacionId|";
            $asig_v="|$asignacionId,3°";
            $rest_dir = $this->ModeloAsignacion->otenerdireccion($asignacionId,3);

            if($item->hora==$item->horafin){
                $horafin=$item->horafin;
                $fijo=1;
            }else{
                $horafin = date('H:i:s', strtotime('-60 minutes', strtotime($item->horafin)));
                $fijo=0;
            }
            if($horafin=='18:00:00'){
                $horafin = date('H:i:s', strtotime('-120 minutes', strtotime($item->horafin)));
            }
            //===================================
                // Convertir a objetos DateTime
                $timeInicial = DateTime::createFromFormat('H:i:s', $item->hora);
                $timeFin = DateTime::createFromFormat('H:i:s', $horafin);

                // Comparar las horas
                if ($timeFin < $timeInicial) {
                    //echo "La hora de fin ($horafin) es menor que la hora de inicio ($item->hora).";
                    //$horafin = date('H:i:s', strtotime('+60 minutes', strtotime($item->horafin)));
                    $horafin =$item->horafin;
                } else {
                    //echo "La hora de fin ($horafin) no es menor que la hora de inicio ($item->hora).";
                }
            //===================================
            $arrays_tr[]=array('emp'=>$item->empresa,'asig'=>$asignacionId,'asigg'=>$asig_3,'tipo'=>3,'idrow'=>$rest_dir['idrow'],'direccion'=>$rest_dir['direccion'],'longitud'=>$rest_dir['longitud'],'latitud'=>$rest_dir['latitud'],'cli'=>$item->idCliente,'hora'=>$item->hora,'horafin'=>$horafin,'prioridad2'=>$item->prioridad2,'hora_cri_ser'=>$item->hora_cri_ser,'hora_comida'=>$item->hora_comida,'horafin_comida'=>$item->horafin_comida,'vinc'=>$asig_v,'fijo'=>$fijo);
        }
        foreach ($result4->result() as $item) {
            $asignacionId = $item->asignacionId;
            $idCliente = $item->idCliente;
            $asig_4="|4,$asignacionId|";
            $asig_v="|$asignacionId,4°";
            $rest_dir = $this->ModeloAsignacion->otenerdireccion($asignacionId,4);
            //log_message('error','tipo 4 asignacion: '.$asignacionId.' hora fin: '.$item->horafin);
            if($item->hora==$item->horafin){
                $horafin=$item->horafin;
                $fijo=1;
            }else{
                $horafin = date('H:i:s', strtotime('-60 minutes', strtotime($item->horafin)));
                $fijo=0;
            }
            if($horafin=='18:00:00'){
                $horafin = date('H:i:s', strtotime('-120 minutes', strtotime($item->horafin)));
            }
            //log_message('error','tipo 4 asignacion: '.$asignacionId.' hora fin: '.$item->horafin.' -60 min:'.$horafin);
            //===================================
                // Convertir a objetos DateTime
                $timeInicial = DateTime::createFromFormat('H:i:s', $item->hora);
                $timeFin = DateTime::createFromFormat('H:i:s', $horafin);

                // Comparar las horas
                if ($timeFin < $timeInicial) {
                    //echo "La hora de fin ($horafin) es menor que la hora de inicio ($item->hora).";
                    //$horafin = date('H:i:s', strtotime('+60 minutes', strtotime($item->horafin)));
                    $horafin =$item->horafin;
                } else {
                    //echo "La hora de fin ($horafin) no es menor que la hora de inicio ($item->hora).";
                }
            //===================================
            $arrays_tr[]=array('emp'=>$item->empresa,'asig'=>$asignacionId,'asigg'=>$asig_4,'tipo'=>4,'idrow'=>$rest_dir['idrow'],'direccion'=>$rest_dir['direccion'],'longitud'=>$rest_dir['longitud'],'latitud'=>$rest_dir['latitud'],'cli'=>$item->idCliente,'hora'=>$item->hora,'horafin'=>$horafin,'prioridad2'=>$item->prioridad2,'hora_cri_ser'=>$item->hora_cri_ser,'hora_comida'=>$item->hora_comida,'horafin_comida'=>$item->horafin_comida,'vinc'=>$asig_v,'fijo'=>$fijo);
        }
        /*
        foreach ($arrays_tr as $key => $row) {
            $aux[$key] = $row['idrow'];
        }
        array_multisort($aux, SORT_ASC, $arrays_tr);
        $arrays_tr2=array();
        $aux='';
        foreach ($arrays_tr as $key => $row) {
            $cli_dir=$row['cli'].$row['idrow'];
            if($row['idrow']=='0'){
                $arrays_tr2[]=$row;
            }else{
                if($aux!=$cli_dir){
                    $arrays_tr2[]=$row;
                    $aux=$cli_dir;
                }
            }
            
            
        }
        */
        $agrupados = [];
        //log_message('error',json_encode($arrays_tr));
        foreach ($arrays_tr as $elemento) {
            $clave = $elemento['cli'] . '-' . $elemento['idrow'];
            
            if (!isset($agrupados[$clave])) {
                $agrupados[$clave] = $elemento;
            } else {
                $agrupados[$clave]['asigg'] .= '|' . $elemento['tipo'].',' . $elemento['asig'].'|';
                $agrupados[$clave]['vinc'] .= '|' . $elemento['asig'].','.$elemento['tipo'].'°';
            }
        }



        echo json_encode($agrupados);
        //echo json_encode($arrays_tr);
    }
    
    function clientesequipos(){
        $idasignacion = $this->input->post('asignacion');
        $tipo = $this->input->post('tipo');
        $fecha = $this->input->post('fecha');
        $tecnico = $this->input->post('tecnico');
        $html='';
        $btncstserv2='';
        $productivo=1;//0 pruebas 1 productivo
        $rowtr=0;
        if ($tipo==1) {
            $resultado=$this->ModeloAsignacion->getdatosrenta($idasignacion,$fecha,0,$tecnico);
            foreach ($resultado->result() as $item) {
                if($item->cot_refaccion!=''){
                    $c_s_r_1=1;
                }else{
                    $c_s_r_1=0;
                }
                if($item->cot_refaccion2!=''){
                    $c_s_r_2=1;
                }else{
                    $c_s_r_2=0;
                }
                if($item->cot_refaccion3!=''){
                    $c_s_r_3=1;
                }else{
                    $c_s_r_3=0;
                }
                $contacto=$item->contacto;
                if($item->contacto_text!=null){
                    $contacto=$item->contacto_text;
                }
                if ($item->status==0) {
                    $btncst='green';
                    $btncst_finis='';
                    $btnstyle0='';
                    $btnstyle='style="display:none"';
                }else{
                    $btncst='grey';
                    $btncst_finis=' servicio_hecho ';
                    $btnstyle0='style="display:none"';
                    $btnstyle='';
                }
                if($productivo==0){
                    $btnstyle0='style="display:none"';
                    $btnstyle='';
                }
                if ($item->status==1) {
                    $btncstserv='servicioinicializado';
                    $btncstserv2='serini';
                }else{
                    $btncstserv='';
                    $btncstserv2='';
                }
                if($item->horaserinicioext==''){
                    $statusext=0;
                }else{
                    $statusext=1;
                }
                if($item->nr==1){
                    $class_red=' red ';
                }else{
                    $class_red='';
                }
                
                $serie=$item->serie;
                //log_message('error',"'$serie'");
                $serien = substr($serie, -4);
                $serienew=str_replace($serien,'****',$serie);
                $onck_serie="'$serie'";
                if($item->serie==''){
                    $productivo=0;   
                }
                if($productivo==0){
                    $serienew=$item->serie;
                }
                $direccion=$this->ModeloCatalogos->getdireccionequipos2($item->id_equipo,$item->serieId,$fecha);
                //====================================
                $rest_iti1=$this->ModeloAsignacion->get_info_itinerario_asignacion($item->asignacionId,1,$tecnico);
                $hora_inicio=$item->hora;
                foreach ($rest_iti1->result() as $itemasig) {
                    $hora_inicio=$itemasig->hora_inicio;
                    $hora_inicio = date('H:i:s',strtotime($this->tolerancia, strtotime($hora_inicio)));
                }
                //=====================================
                if($item->ubicacion!=''){
                    $ubicacion='<br>Ubicación: '.$item->ubicacion.'';
                }else{
                    $ubicacion='';
                }
                $html.='<tr class="'.$btncst_finis.$class_red.' ser1">';
                    $html.='<td class="'.$btncstserv2.' numberidasig" onclick="habilitarbtnsearch()">';
                        $html.='<input type="hidden" id="asignacion" value="'.$item->asignacionIde.'" readonly style="width:50px">';
                        $html.='<input type="hidden" id="tipo" value="1">';
                        $html.=$item->asignacionIde;
                    $html.='</td>';
                    $html.='<td >'.$item->modelo.'</td>';
                    $html.='<td class="rowtr_'.$rowtr.'" data-serie="'.$item->serie.'" onclick="buscarserie('.$rowtr.')">'.$serienew.'<br><span style="color:transparent;">'.$item->serie.'<span></td>';
                    $html.='<td>'.$item->servicio.'</td>';
                    $html.='<td>'.$contacto.'</td>';
                    if($item->hora_comida!='00:00:00'){
                        $horacomida="<br>Comida<!--1-->: $item->hora_comida a $item->horafin_comida";
                    }else{
                        $horacomida='';
                    }
                    $html.='<td>'.$item->fecha.'<br>'.$item->hora.' '.$item->horafin.$horacomida.'</td>';
                    $html.='<td>'.$direccion.$ubicacion.'</td>';
                    $html.='<td><a class="b-btn b-btn-primary tooltipped comment_info_'.$rowtr.'" data-comen="'.$item->comentariocal.'" onclick="comment_info('.$rowtr.')" ><i class="fas fa-comment-dots"></i></a></td>';
                    $html.='<td>';
                            $html.='<a class="'.$btncst.' '.$btncstserv.' btn_0_'.$serien.' waves-effect waves-light btn-bmz td_button_agregar" onclick="notificaciondisabled()" '.$btnstyle0.'>Agregar</a>';

                        $html.='<button class=" '.$btncst.' '.$btncstserv.' btn_1_'.$serien.' waves-effect waves-light btn-bmz equipo_'.$item->asignacionIde.' button_agregar click_ser_1_'.$item->asignacionId.' ser_1_'.$item->asignacionId.'_'.$item->asignacionIde.'" '.$btnstyle.' data-pin="'.$item->pass.'" onclick="click_ser('.$item->asignacionId.','.$item->asignacionIde.',1,'.$item->id_equipo.','.$item->serieId.','.$item->status.','.$statusext.','.$onck_serie.',0)" data-horaser="'.$hora_inicio.'" data-sername="'.$item->servicio.'" data-sercliid="'.$item->idCliente.'" data-sercli="'.$item->empresa.'" data-modelo="'.$item->modelo.'" data-serie="'.$item->serie.'" data-status="'.$item->status.'">Agregar</button>
                            <span style="color:transparent;">'.$serien.'</span></td>
                        </tr>';
                        $rowtr++;
            }
        }
        if ($tipo==2) {
            $resultado=$this->ModeloAsignacion->getdatospoliza($idasignacion,0,$tecnico);
            $servicio_pol=0;
            foreach ($resultado->result() as $item) {
                if($item->cot_refaccion!=''){
                    $c_s_r_1=1;
                }else{
                    $c_s_r_1=0;
                }
                if($item->cot_refaccion2!=''){
                    $c_s_r_2=1;
                }else{
                    $c_s_r_2=0;
                }
                if($item->cot_refaccion3!=''){
                    $c_s_r_3=1;
                }else{
                    $c_s_r_3=0;
                }
                if($item->combinada==1){
                    
                    $datosvc=$this->ModeloCatalogos->getselectwheren('ventacombinada',array('poliza'=>$item->polizaId));
                    foreach ($datosvc->result() as $itemvc) {
                        $resultadoequipos = $this->ModeloCatalogos->db6_getselectwheren('ventas_has_detallesEquipos',array('idVenta'=>$itemvc->equipo));
                        $resultadoaccesorios = $this->ModeloCatalogos->accesoriosventa($itemvc->equipo);
                        $resultadoconsumibles = $this->ModeloCatalogos->consumiblesventa($itemvc->equipo);
                        $consumiblesventadetalles = $this->ModeloCatalogos->consumiblesventadetalles($itemvc->consumibles);
                        $ventadetallesrefacion = $this->ModeloCatalogos->ventas_has_detallesRefacciones($itemvc->refacciones);
                        foreach($resultadoequipos->result() as $itempve){
                            if($itempve->garantia==1){
                                $servicio_pol=1;
                            }
                        }
                        foreach($resultadoaccesorios->result() as $itempva){
                            if($itempva->garantia==1){
                                $servicio_pol=1;
                            }
                        }
                        foreach($resultadoconsumibles->result() as $itempvc){
                            if($itempvc->garantia==1){
                                $servicio_pol=1;
                            }
                        }
                        foreach($consumiblesventadetalles->result() as $itempvc){
                            if($itempvc->garantia==1){
                                $servicio_pol=1;
                            }
                        }
                        foreach($ventadetallesrefacion->result() as $itempvr){
                            if($itempvr->garantia==1){
                                $servicio_pol=1;
                            }
                        }
                    }
                }
                if($item->formadecobro=='Tecnico'){
                    $infocobro=' <i class="material-icons">attach_money</i>';
                }else{
                    $infocobro='';
                }
                //====================================
                $rest_iti1=$this->ModeloAsignacion->get_info_itinerario_asignacion($item->asignacionId,2,$tecnico);
                $hora_inicio=$item->hora;
                foreach ($rest_iti1->result() as $itemasig) {
                    $hora_inicio=$itemasig->hora_inicio;
                    $hora_inicio = date('H:i:s',strtotime($this->tolerancia, strtotime($hora_inicio)));
                }
                //=====================================
                if ($item->status==0) {
                    $btncst='green';
                    $btncst_finis=' ';
                    $btnstyle0='';
                    $btnstyle='style="display:none"';

                }else{
                    $btncst='grey';
                    $btncst_finis=' servicio_hecho ';
                    $btnstyle0='style="display:none"';
                    $btnstyle='';
                }
                if($productivo==1){//se cambio a 1 por que ya no aplica
                    $btnstyle0='style="display:none"';
                    $btnstyle='';
                }
                if ($item->status==1) {
                    $btncstserv='servicioinicializado';
                    $btncstserv2='serini';
                }else{
                    $btncstserv='';
                    $btncstserv2='';
                }

                if($item->horaserinicioext==null){
                    $statusext=0;
                }else{
                    $statusext=1;
                }
                if($item->nr==1){
                    $class_red=' red ';
                }else{
                    $class_red='';
                }
                $serie=$item->serie;
                $serien = substr($serie, -4);
                $serienew=str_replace($serien,'****',$serie);
                $onck_serie="'$serie'";
                if($item->serie==''){
                    $productivo=0;  
                    $btnstyle0='style="display:none"';
                    $btnstyle=''; 
                }
                if($productivo==1){//se cambio a 1 por que ya no aplica
                    $serienew=$item->serie;
                }
                $contacto=$item->atencion;
                if($item->contacto_text!=null){
                    $contacto=$item->contacto_text;
                }
                $html.='<tr class="'.$btncst_finis.$class_red.' ser2">';
                        $html.='<td class="'.$btncstserv2.' numberidasig" onclick="habilitarbtnsearch()">';
                            $html.='<input type="hidden"  id="asignacion"value="'.$item->asignacionIde.'" readonly style="width:50px">';
                            $html.='<input type="hidden" id="tipo" value="2">';
                            $html.=$item->asignacionIde.$infocobro;
                          $html.='</td>';
                          $html.='<td >'.$item->modelo.'</td>';
                          $html.='<td class="rowtr_'.$rowtr.'" data-serie="'.$item->serie.'" onclick="buscarserie('.$rowtr.')">'.$serienew.'<br><span style="color:transparent;">'.$item->serie.'<span></td>';
                          $html.='<td>'.$item->servicio.'</td>';
                          $html.='<td>'.$contacto.'</td>';
                            if($item->hora_comida!='00:00:00'){
                                $horacomida="<br>Comida<!--2-->: $item->hora_comida a $item->horafin_comida";
                            }else{
                                $horacomida='';
                            }
                    $html.='<td>'.$item->fecha.'<br>'.$item->hora.' '.$item->horafin.$horacomida.'</td>';
                    $html.='<td>'.$item->direccionservicio.'</td>';
                          $html.='<td><a class="b-btn b-btn-primary tooltipped comment_info_'.$rowtr.'" data-comen="'.$item->comentariocal.'" onclick="comment_info('.$rowtr.')" ><i class="fas fa-comment-dots"></i></a></td>';
                          $html.='<td>';
                            $html.='<a class="'.$btncst.' '.$btncstserv.' btn_0_'.$serien.' waves-effect waves-light btn-bmz td_button_agregar" onclick="notificaciondisabled()"'.$btnstyle0.'>Agregar</a>
                            <button class=" '.$btncst.' '.$btncstserv.' btn_1_'.$serien.' waves-effect waves-light btn-bmz equipo_'.$item->asignacionIde.' button_agregar click_ser_2_'.$item->asignacionId.' ser_2_'.$item->asignacionId.'_'.$item->asignacionIde.'" '.$btnstyle.' data-pin="'.$item->pass.'" onclick="click_ser('.$item->asignacionId.','.$item->asignacionIde.',2,'.$item->id_equipo.',0,'.$item->status.','.$statusext.','.$onck_serie.','.$servicio_pol.')" data-horaser="'.$hora_inicio.'" data-sername="'.$item->servicio.'" data-sercliid="'.$item->idCliente.'" data-sercli="'.$item->empresa.'" data-modelo="'.$item->modelo.'" data-serie="'.$item->serie.'" data-cotref1="'.$c_s_r_1.'" data-cotref2="'.$c_s_r_2.'" data-cotref3="'.$c_s_r_3.'" data-status="'.$item->status.'">Agregar</button>
                                <span style="color:transparent;">'.$serien.'</span></td>
                        </tr>';
                $rowtr++;
            }
        }
        if ($tipo==3) {
            $resultado=$this->ModeloAsignacion->getdatoscliente($idasignacion,$tecnico);
            foreach ($resultado->result() as $item) {
                if($item->cot_refaccion!=''){
                    $c_s_r_1=1;
                }else{
                    $c_s_r_1=0;
                }
                if($item->cot_refaccion2!=''){
                    $c_s_r_2=1;
                }else{
                    $c_s_r_2=0;
                }
                if($item->cot_refaccion3!=''){
                    $c_s_r_3=1;
                }else{
                    $c_s_r_3=0;
                }
                if ($item->status==0) {
                    $btncst='green';
                    $btncst_finis=' ';
                    $btnstyle0='';
                    $btnstyle='style="display:none"';
                }else{
                    $btncst='grey';
                    $btncst_finis=' servicio_hecho ';
                    $btnstyle0='style="display:none"';
                    $btnstyle='';
                }
                if($productivo==1){// para este caso no aplica esta nueva funcion ya que asi lo requieren por lo tanto es 1
                    $btnstyle0='style="display:none"';
                    $btnstyle='';
                }
                if ($item->status==1) {
                    $btncstserv='servicioinicializado';
                    $btncstserv2='serini';
                }else{
                    $btncstserv='';
                    $btncstserv2='';
                }
                if($item->horaserinicioext==null){
                    $statusext=0;
                }else{
                    $statusext=1;
                }
                if($item->nr==1){
                    $class_red=' red ';
                }else{
                    $class_red='';
                }
                //====================================
                $rest_iti1=$this->ModeloAsignacion->get_info_itinerario_asignacion($item->asignacionId,3,$tecnico);
                $hora_inicio=$item->hora;
                foreach ($rest_iti1->result() as $itemasig) {
                    $hora_inicio=$itemasig->hora_inicio;
                    $hora_inicio = date('H:i:s',strtotime($this->tolerancia, strtotime($hora_inicio)));
                }
                //=====================================
                $serie=$item->serie;
                $serien = substr($serie, -4);
                $serienew=str_replace($serien,'****',$serie);
                $onck_serie="'$serie'";
                if($item->serie==''){
                    $productivo=0;   
                    $btnstyle0='style="display:none"';
                    $btnstyle=''; 
                }
                if($productivo==1){// para este caso no aplica esta nueva funcion ya que asi lo requieren por lo tanto es 1
                    $serienew=$item->serie;
                }
                if($item->retiro==1){
                    $modelo_eq=$item->equipostext.' '.$item->poliza.' '.$item->modeloretiro;
                }else{
                    $modelo_eq=$item->equipostext.' '.$item->poliza.' '.$item->modelo;
                }
                $html.='<tr class="'.$btncst_finis.$class_red.' ser3">';
                          $html.='<td class="'.$btncstserv2.' numberidasig" onclick="habilitarbtnsearch()">';
                            $html.='<input type="hidden"  id="asignacion"value="'.$item->asignacionIdd.'" readonly style="width:50px">';
                            $html.='<input type="hidden" id="tipo" value="3">'.$item->asignacionIdd.'</td>';
                          $html.='<td class="equipostext">'.$modelo_eq.'</td>';
                          $html.='<td class="rowtr_'.$rowtr.'" data-serie="'.$item->serie.'" onclick="buscarserie('.$rowtr.')">'.$serienew.'<br><span style="color:transparent;">'.$item->serie.'<span></td>';
                          $html.='<td class="servicio">'.$item->servicio.' '.$item->poliza.'</td>';
                          $html.='<td>'.$item->empresa.'</td>';
                            if($item->hora_comida!='00:00:00'){
                                $horacomida="<br>Comida<!--3-->: $item->hora_comida a $item->horafin_comida";
                            }else{
                                $horacomida='';
                            }
                          $html.='<td>'.$item->fecha.'<br>'.$item->hora.' '.$item->horafin.$horacomida.'</td>';
                          $html.='<td>'.$item->direccion.'</td>';
                          $html.='<td><a class="b-btn b-btn-primary tooltipped comment_info_'.$rowtr.'" data-comen="'.$item->comentariocal.'" onclick="comment_info('.$rowtr.')" ><i class="fas fa-comment-dots"></i></a></td>';
                          $html.='<td>';
                            $html.='<a class="'.$btncst.' '.$btncstserv.' btn_0_'.$serien.'waves-effect waves-light btn-bmz td_button_agregar" onclick="notificaciondisabled()" '.$btnstyle0.'>Agregar</a>';
                            $html.='<button class="'.$btncst.' '.$btncstserv.' btn_1_'.$serien.'waves-effect waves-light btn-bmz equipo_'.$item->asignacionIdd.' button_agregar click_ser_3_'.$item->asignacionId.' ser_3_'.$item->asignacionId.'_'.$item->asignacionIdd.'" '.$btnstyle.' data-pin="'.$item->pass.'" onclick="click_ser('.$item->asignacionId.','.$item->asignacionIdd.',3,0,0,'.$item->status.','.$statusext.','.$onck_serie.',0)" data-horaser="'.$hora_inicio.'" data-sername="'.$item->servicio.' '.$item->poliza.'" data-sercliid="'.$item->clienteId.'" data-sercli="'.$item->empresa.'" data-modelo="'.$modelo_eq.'" data-serie="'.$item->serie.'" data-cotref1="'.$c_s_r_1.'" data-cotref2="'.$c_s_r_2.'" data-cotref3="'.$c_s_r_3.'" data-status="'.$item->status.'">Agregar</button>';
                                $html.='<span style="color:transparent;">'.$serien.'</span></td>';
                        $html.='</tr>';
                $rowtr++;
            }
        }
        if ($tipo==4) {
            $resultado=$this->ModeloAsignacion->getdatosventas($idasignacion);
            foreach ($resultado->result() as $item) {
                if($item->cot_refaccion!=''){
                    $c_s_r_1=1;
                }else{
                    $c_s_r_1=0;
                }
                if($item->cot_refaccion2!=''){
                    $c_s_r_2=1;
                }else{
                    $c_s_r_2=0;
                }
                if($item->cot_refaccion3!=''){
                    $c_s_r_3=1;
                }else{
                    $c_s_r_3=0;
                }
                if($item->formadecobro=='Tecnico'){
                    $infocobro=' <i class="material-icons">attach_money</i>';
                }else{
                    $infocobro='';
                }
                if ($item->status==0) {
                    $btncst='green';
                    $btncst_finis=' ';
                    $btnstyle0='';
                    $btnstyle='style="display:none"';
                }else{
                    $btncst='grey';
                    $btncst_finis=' servicio_hecho ';
                    $btnstyle0='style="display:none"';
                    $btnstyle='';
                }
                if($productivo==1){ //se cambio a 1 para que no solisite la serie
                    $btnstyle0='style="display:none"';
                    $btnstyle='';
                }
                if ($item->status==1) {
                    $btncstserv='servicioinicializado';
                    $btncstserv2='serini';
                }else{
                    $btncstserv='';
                    $btncstserv2='';
                }
                if($item->horaserinicioext==null){
                    $statusext=0;
                }else{
                    $statusext=1;
                }
                if($item->nr==1){
                    $class_red=' red ';
                }else{
                    $class_red='';
                }
                //====================================
                $rest_iti1=$this->ModeloAsignacion->get_info_itinerario_asignacion($item->asignacionId,4,$tecnico);
                $hora_inicio=$item->hora;
                foreach ($rest_iti1->result() as $itemasig) {
                    $hora_inicio=$itemasig->hora_inicio;
                    $hora_inicio = date('H:i:s',strtotime($this->tolerancia, strtotime($hora_inicio)));
                    //log_message('error','$hora_inicio:'.$hora_inicio);
                }
                //=====================================
                $serie=$item->serie;
                $serien = substr($serie, -4);
                $serienew=str_replace($serien,'****',$serie);
                $onck_serie="'$serie'";
                if($item->serie==''){
                    $productivo=0; 
                    $btnstyle0='style="display:none"';
                    $btnstyle=''; 
                }
                if($productivo==1){ //se cambio a 1 para que no solisite la serie
                    $serienew=$item->serie;
                }
                if($item->hora_comida!='00:00:00'){
                    $horacomida="<br>Comida<!--4-->: $item->hora_comida a $item->horafin_comida";
                }else{
                    $horacomida='';
                }
                $contacto=$item->atencionpara;
                if($item->contacto_text!=null){
                    $contacto=$item->contacto_text;
                }
                $html.='<tr class="'.$btncst_finis.$class_red.' ser4">
                          <td class="'.$btncstserv2.' numberidasig" onclick="habilitarbtnsearch()">
                            <input type="hidden"  id="asignacion"value="'.$item->asignacionId.'" readonly style="width:50px">
                            <input type="hidden" id="tipo" value="4">
                            '.$item->asignacionId.$infocobro.'
                          </td>
                          <td >'.$item->equipostext.'</td>
                          <td class="rowtr_'.$rowtr.'" data-serie="'.$item->serie.'" onclick="buscarserie('.$rowtr.')">'.$serienew.'<br><span style="color:transparent;">'.$item->serie.'<span></td>
                          <td>'.$item->servicio.' '.$item->tserviciomotivo.'</td>
                          <td>'.$contacto.'</td>
                          <td>'.$item->fecha.' '.$item->hora.' '.$item->horafin.$horacomida.'</td>
                          <td>'.$item->direccion.'</td>
                          <td><a class="b-btn b-btn-primary tooltipped comment_info_'.$rowtr.'" data-comen="'.$item->comentariocal.'" onclick="comment_info('.$rowtr.')" ><i class="fas fa-comment-dots"></i></a></td>
                          <td>
                            <button class="'.$btncst.' '.$btncstserv.' btn_0_'.$serien.'  waves-effect waves-light btn-bmz td_button_agregar" '.$btnstyle0.' onclick="notificaciondisabled()">Agregar</button>
                            <button class="'.$btncst.' '.$btncstserv.' btn_1_'.$serien.'  waves-effect waves-light btn-bmz equipo_'.$item->asignacionId.' '.$item->horaserinicioext.' button_agregar click_ser_4_'.$item->asignacionId.' ser_4_'.$item->asignacionId.'_0" '.$btnstyle.' onclick="click_ser('.$item->asignacionId.',0,4,0,0,'.$item->status.','.$statusext.','.$onck_serie.',0)" data-horaser="'.$hora_inicio.'" data-sername="'.$item->servicio.' '.$item->tserviciomotivo.'" data-sercliid="'.$item->clienteId.'" data-sercli="'.$item->empresa.'" data-modelo="'.$item->equipostext.'" data-serie="'.$item->serie.'" data-cotref1="'.$c_s_r_1.'" data-cotref2="'.$c_s_r_2.'" data-cotref3="'.$c_s_r_3.'" data-status="'.$item->status.'">Agregar</button>
                                <span style="color:transparent;">'.$serien.'</span></td>
                        </tr>';
                $rowtr++;
            }
        }
        echo $html;
    }
    function solicitudrefaccion(){
        $ser = $this->input->post('ser');
        $serve = $this->input->post('serve');
        $tipo = $this->input->post('tipo');//1 contato 2 poliza 3 evento
        $tipod = $this->input->post('tipod');
        $prioridad = $this->input->post('prioridad'); 
        $refaccion = $this->input->post('refaccion');
        $cot_contador = $this->input->post('cot_contador');

        $array_cot=array('cot_status'=>1);
        $array_cot['cot_contador']=$cot_contador;
        if ($tipod==1) {
            $cot_tipo='POR DESGASTE';
            $array_cot['cot_refaccion']=$refaccion;
            if ($tipo==4) {
                $array_cot['prioridadr']=$prioridad;
            }else{
                $array_cot['prioridad']=$prioridad;
            }
            
            
        }elseif ($tipod==2) {
            $cot_tipo='POR DAÑO';
            $array_cot['cot_refaccion2']=$refaccion;
            if ($tipo==4) {
                $array_cot['prioridadr2']=$prioridad;
            }else{
                $array_cot['prioridad2']=$prioridad;
            }
            
            
        }elseif ($tipod==3) {
            $cot_tipo='POR GARANTIA';
            $array_cot['cot_refaccion3']=$refaccion;
            if ($tipo==4) {
                $array_cot['prioridadr3']=$prioridad;
            }else{
                $array_cot['prioridad3']=$prioridad;
            }
            
            
        }else{
            $cot_tipo='';
        }
        $affected_rows=0;
        $this->ModeloCatalogos->Insert('bitacora',array('contenido'=>'Solicitud de refaccion tipo:'.$tipo.' '.$cot_tipo.' descripcion: '.$refaccion.' serve:'.$serve.' ser:'.$ser,'nombretabla'=>'','idtable'=>0,'tipo'=>'Solicitud Refaccion','personalId'=>$this->idpersonal));

        if ($tipo==1) {
            $affected_rows=$this->ModeloCatalogos->updateCatalogo('asignacion_ser_contrato_a_e',$array_cot,array('asignacionIde'=>$serve,'asignacionId'=>$ser));
        }
        if ($tipo==2) {
            $affected_rows=$this->ModeloCatalogos->updateCatalogo('asignacion_ser_poliza_a_e',$array_cot,array('asignacionIde'=>$serve,'asignacionId'=>$ser));
        }
        if ($tipo==3) {
            $affected_rows=$this->ModeloCatalogos->updateCatalogo('asignacion_ser_cliente_a_d',$array_cot,array('asignacionIdd'=>$serve));
        }
        if ($tipo==4) {
            $affected_rows=$this->ModeloCatalogos->updateCatalogo('asignacion_ser_venta_a',$array_cot,array('asignacionId'=>$ser));
        }
        echo $affected_rows;
    }
    function obtenerinfosolicitudrefaccion(){
        $ser = $this->input->post('ser');
        $serve = $this->input->post('serve');
        $tipo = $this->input->post('tipo');//1 contato 2 poliza 3 evento
        $tipod = $this->input->post('tipod');

        
        
        $prioridad='';
        $cot_refaccion='';
        if ($tipo==1) {

            $result=$this->ModeloCatalogos->getselectwheren('asignacion_ser_contrato_a_e',array('asignacionIde'=>$serve,'asignacionId'=>$ser));
            foreach ($result->result() as $item) {
                
                $cot_contador=$item->cot_contador;
                if ($tipod==1) {
                    $cot_refaccion=$item->cot_refaccion;
                    $prioridad=$item->prioridad;
                }elseif ($tipod==2) {
                    $cot_refaccion=$item->cot_refaccion2;
                    $prioridad=$item->prioridad2;
                }elseif ($tipod==3) {
                    $cot_refaccion=$item->cot_refaccion3;
                    $prioridad=$item->prioridad3;
                }
            }

        }
        if ($tipo==2) {
            

            $result=$this->ModeloCatalogos->getselectwheren('asignacion_ser_poliza_a_e',array('asignacionIde'=>$serve,'asignacionId'=>$ser));
            foreach ($result->result() as $item) {
                $prioridad=$item->prioridad;
                $cot_contador=$item->cot_contador;
                if ($tipod==1) {
                    $cot_refaccion=$item->cot_refaccion;
                    $prioridad=$item->prioridad;
                }elseif ($tipod==2) {
                    $cot_refaccion=$item->cot_refaccion2;
                    $prioridad=$item->prioridad2;
                }elseif ($tipod==3) {
                    $cot_refaccion=$item->cot_refaccion3;
                    $prioridad=$item->prioridad3;
                }
            }
        }
        if ($tipo==3) {
            
            $result=$this->ModeloCatalogos->getselectwheren('asignacion_ser_cliente_a_d',array('asignacionIdd'=>$serve));
            foreach ($result->result() as $item) {
                $prioridad=$item->prioridad;
                $cot_contador=$item->cot_contador;
                if ($tipod==1) {
                    $cot_refaccion=$item->cot_refaccion;
                    $prioridad=$item->prioridad;
                }elseif ($tipod==2) {
                    $cot_refaccion=$item->cot_refaccion2;
                    $prioridad=$item->prioridad2;
                }elseif ($tipod==3) {
                    $cot_refaccion=$item->cot_refaccion3;
                    $prioridad=$item->prioridad3;
                }
            }
        }
        if ($tipo==4) {
            
            $result=$this->ModeloCatalogos->getselectwheren('asignacion_ser_venta_a',array('asignacionId'=>$ser));
            foreach ($result->result() as $item) {
                
                $cot_contador=$item->cot_contador;
                if ($tipod==1) {
                    $cot_refaccion=$item->cot_refaccion;
                    $prioridad=$item->prioridadr;
                }elseif ($tipod==2) {
                    $cot_refaccion=$item->cot_refaccion2;
                    $prioridad=$item->prioridadr2;
                }elseif ($tipod==3) {
                    $cot_refaccion=$item->cot_refaccion3;
                    $prioridad=$item->prioridadr3;
                }
            }
        }
        $array = array(
            'prioridad'=>$prioridad,
            'cot_refaccion'=>$cot_refaccion,
            'cot_contador'=>$cot_contador
        );
        echo json_encode($array);
    }
    function cambiarstatusservicio(){//ver como cambiar este proceso (173) al de cambiar a dos botones
        $serv = $this->input->post('serv');
        $serve = $this->input->post('serve');
        $tipo = $this->input->post('tipo');
        $tec = $this->input->post('tec');
        //log_message('error','$serv('.$serv.') $serve('.$serve.') $tipo('.$tipo.') $tec('.$tec.')');
        $iniciarfinalizar = $this->input->post('iniciarfinalizar');
        $localserver=0;//0 local 1 server
        $hi=$iniciarfinalizar;
        $estatusbloqueoservicio=0;
        $r_datos_per = $this->ModeloCatalogos->getselectwheren('personal',array('personalId'=>$this->idpersonal));
        $ts_servicio=0;
        foreach ($r_datos_per->result() as $itemrs) {
            $ts_servicio=$itemrs->servicio;
        }

        if ($tipo==1) {
            /*
            $resultc=$this->ModeloCatalogos->getselectwheren('asignacion_ser_contrato_a_e',array('asignacionIde'=>$serve));
            foreach ($resultc->result() as $item) {
                if ($item->horaserinicio!=null) {
                    $hi=1;
                }
            }
            */
            if ($hi==0) {
                if($ts_servicio==0){
                    $this->ModeloCatalogos->updateCatalogo('asignacion_ser_contrato_a_e',array('hora'=>$this->horaactual,'hora_i'=>1),array('asignacionId'=>$serv,'nr'=>0));
                    $this->ModeloCatalogos->updateCatalogo('asignacion_ser_contrato_a_e',array('horaserinicio'=>$this->horaactual,'status'=>1),array('asignacionIde'=>$serve,'nr'=>0));
                    $this->ModeloCatalogos->updateCatalogo('personal',array('servicio'=>1,'servicio_info'=>"|$tipo,$serv|"),array('personalId'=>$this->idpersonal));

                    $this->ModeloCatalogos->updateCatalogo('asignacion_ser_contrato_a_e',array('tecnico2'=>null,'tecnico3'=>null,'tecnico4'=>null),array('asignacionIde'=>$serve,'tecnico'=>$this->idpersonal));
                    $this->ModeloCatalogos->updateCatalogo('asignacion_ser_contrato_a_e',array('tecnico'=>$this->idpersonal,'tecnico2'=>null,'tecnico3'=>null,'tecnico4'=>null),array('asignacionIde'=>$serve,'tecnico2'=>$this->idpersonal));
                    $this->ModeloCatalogos->updateCatalogo('asignacion_ser_contrato_a_e',array('tecnico'=>$this->idpersonal,'tecnico2'=>null,'tecnico3'=>null,'tecnico4'=>null),array('asignacionIde'=>$serve,'tecnico3'=>$this->idpersonal));
                    $this->ModeloCatalogos->updateCatalogo('asignacion_ser_contrato_a_e',array('tecnico'=>$this->idpersonal,'tecnico2'=>null,'tecnico3'=>null,'tecnico4'=>null),array('asignacionIde'=>$serve,'tecnico4'=>$this->idpersonal));

                }else{
                    $estatusbloqueoservicio=1;
                }
            }else{
                $this->ModeloCatalogos->updateCatalogo('asignacion_ser_contrato_a_e',array('horaserfin'=>$this->horaactual,'status'=>2,'per_fin'=>$this->idpersonal),array('asignacionIde'=>$serve));
                $this->ModeloCatalogos->updateCatalogo('asignacion_ser_contrato_a_e',array('horafin'=>$this->horaactual),array('asignacionId'=>$serv));
                $this->ModeloCatalogos->updateCatalogo('personal',array('servicio'=>0,'servicio_info'=>''),array('personalId'=>$this->idpersonal));
                $this->ModeloCatalogos->updateCatalogo('personal',array('servicio'=>0,'servicio_info'=>''),array('servicio_info'=>"|$tipo,$serv|"));
                if($localserver==1){
                    //$this->envioemail(1,$serve);
                }
            }
            //========================================================================================
                $serviciosresult=$this->ModeloCatalogos->getselectwheren('asignacion_ser_contrato_a_e',array('asignacionId'=>$serv,'activo'=>1,'status !='=>2));
                $serviciofinales=1; 
                foreach ($serviciosresult->result() as $item) {
                    if($item->status!=3){
                        $serviciofinales=0;    
                    }
                    
                }
            //========================================================================================
        }
        if ($tipo==2) {
            /*
            $resultc=$this->ModeloCatalogos->getselectwheren('asignacion_ser_poliza_a_e',array('asignacionIde'=>$serve));
            foreach ($resultc->result() as $item) {
                if ($item->horaserinicio!=null) {
                    $hi=1;
                }
            }
            */
            if ($hi==0) {
                if($ts_servicio==0){
                    $this->ModeloCatalogos->updateCatalogo('asignacion_ser_poliza_a_e',array('hora'=>$this->horaactual,'hora_i'=>1),array('asignacionId'=>$serv,'nr'=>0));
                    $this->ModeloCatalogos->updateCatalogo('asignacion_ser_poliza_a_e',array('horaserinicio'=>$this->horaactual,'status'=>1),array('asignacionIde'=>$serve,'nr'=>0));
                    $this->ModeloCatalogos->updateCatalogo('personal',array('servicio'=>1,'servicio_info'=>"|$tipo,$serv|"),array('personalId'=>$this->idpersonal));

                    $this->ModeloCatalogos->updateCatalogo('asignacion_ser_poliza_a_e',array('tecnico2'=>null,'tecnico3'=>null,'tecnico4'=>null),array('asignacionIde'=>$serve,'tecnico'=>$this->idpersonal));
                    $this->ModeloCatalogos->updateCatalogo('asignacion_ser_poliza_a_e',array('tecnico'=>$this->idpersonal,'tecnico2'=>null,'tecnico3'=>null,'tecnico4'=>null),array('asignacionIde'=>$serve,'tecnico2'=>$this->idpersonal));
                    $this->ModeloCatalogos->updateCatalogo('asignacion_ser_poliza_a_e',array('tecnico'=>$this->idpersonal,'tecnico2'=>null,'tecnico3'=>null,'tecnico4'=>null),array('asignacionIde'=>$serve,'tecnico3'=>$this->idpersonal));
                    $this->ModeloCatalogos->updateCatalogo('asignacion_ser_poliza_a_e',array('tecnico'=>$this->idpersonal,'tecnico2'=>null,'tecnico3'=>null,'tecnico4'=>null),array('asignacionIde'=>$serve,'tecnico4'=>$this->idpersonal));

                }else{
                    $estatusbloqueoservicio=1;
                }
            }else{
                $this->ModeloCatalogos->updateCatalogo('asignacion_ser_poliza_a_e',array('horaserfin'=>$this->horaactual,'status'=>2),array('asignacionIde'=>$serve));
                $this->ModeloCatalogos->updateCatalogo('asignacion_ser_poliza_a_e',array('horafin'=>$this->horaactual,'per_fin'=>$this->idpersonal),array('asignacionId'=>$serv));
                $this->ModeloCatalogos->updateCatalogo('personal',array('servicio'=>0,'servicio_info'=>''),array('personalId'=>$this->idpersonal));
                $this->ModeloCatalogos->updateCatalogo('personal',array('servicio'=>0,'servicio_info'=>''),array('servicio_info'=>"|$tipo,$serv|"));
                if($localserver==1){
                    //$this->envioemail(2,$serve);
                }
            }
            //========================================================================================
                $serviciosresult=$this->ModeloCatalogos->getselectwheren('asignacion_ser_poliza_a_e',array('asignacionId'=>$serv,'status !='=>2));
                $serviciofinales=1; 
                foreach ($serviciosresult->result() as $item) {
                    $serviciofinales=0;
                }
            //========================================================================================
        }
        if ($tipo==3) {
            /*
            $resultc=$this->ModeloCatalogos->getselectwheren('asignacion_ser_cliente_a_d',array('asignacionIdd'=>$serve));
            foreach ($resultc->result() as $item) {
                if ($item->horaserinicio!=null) {
                    $hi=1;
                }
            }
            */
            if ($hi==0) {
                if($ts_servicio==0){
                    $this->ModeloCatalogos->updateCatalogo('asignacion_ser_cliente_a',array('hora'=>$this->horaactual,'hora_i'=>1),array('asignacionId'=>$serv));
                    $this->ModeloCatalogos->updateCatalogo('asignacion_ser_cliente_a_d',array('horaserinicio'=>$this->horaactual,'status'=>1),array('asignacionIdd'=>$serve,'nr'=>0));
                    $this->ModeloCatalogos->updateCatalogo('personal',array('servicio'=>1,'servicio_info'=>"|$tipo,$serv|"),array('personalId'=>$this->idpersonal));

                    $this->ModeloCatalogos->updateCatalogo('asignacion_ser_cliente_a_d',array('tecnico2'=>null,'tecnico3'=>null,'tecnico4'=>null),array('asignacionIdd'=>$serve,'tecnico'=>$this->idpersonal));
                    $this->ModeloCatalogos->updateCatalogo('asignacion_ser_cliente_a_d',array('tecnico'=>$this->idpersonal,'tecnico2'=>null,'tecnico3'=>null,'tecnico4'=>null),array('asignacionIdd'=>$serve,'tecnico2'=>$this->idpersonal));
                    $this->ModeloCatalogos->updateCatalogo('asignacion_ser_cliente_a_d',array('tecnico'=>$this->idpersonal,'tecnico2'=>null,'tecnico3'=>null,'tecnico4'=>null),array('asignacionIdd'=>$serve,'tecnico3'=>$this->idpersonal));
                    $this->ModeloCatalogos->updateCatalogo('asignacion_ser_cliente_a_d',array('tecnico'=>$this->idpersonal,'tecnico2'=>null,'tecnico3'=>null,'tecnico4'=>null),array('asignacionIdd'=>$serve,'tecnico4'=>$this->idpersonal));
                    
                }else{
                    $estatusbloqueoservicio=1;
                }
            }else{
                $this->ModeloCatalogos->updateCatalogo('asignacion_ser_cliente_a_d',array('horaserfin'=>$this->horaactual,'status'=>2),array('asignacionIdd'=>$serve));
                $this->ModeloCatalogos->updateCatalogo('asignacion_ser_cliente_a',array('horafin'=>$this->horaactual,'per_fin'=>$this->idpersonal),array('asignacionId'=>$serv));
                $this->ModeloCatalogos->updateCatalogo('personal',array('servicio'=>0,'servicio_info'=>''),array('personalId'=>$this->idpersonal));
                $this->ModeloCatalogos->updateCatalogo('personal',array('servicio'=>0,'servicio_info'=>''),array('servicio_info'=>"|$tipo,$serv|"));
                $this->verificarretiroequipos($serve,3);
                if($localserver==1){
                    //$this->envioemail(3,$serve);
                }
            }
            //========================================================================================
                $serviciosresult=$this->ModeloCatalogos->getselectwheren('asignacion_ser_cliente_a_d',array('asignacionId'=>$serv,'status !='=>2));
                $serviciofinales=1; 
                foreach ($serviciosresult->result() as $item) {
                    $serviciofinales=0;
                }
            //========================================================================================
        }
        if ($tipo==4) {
            /*
            $resultc=$this->ModeloCatalogos->getselectwheren('asignacion_ser_venta_a',array('asignacionId'=>$serv));
            foreach ($resultc->result() as $item) {
                if ($item->horaserinicio!=null) {
                    $hi=1;
                }
            }
            */
            $serviciofinales=0;
            if ($hi==0) {
                if($ts_servicio==0){
                    $this->ModeloCatalogos->updateCatalogo('asignacion_ser_venta_a',array('hora'=>$this->horaactual,'hora_i'=>1),array('asignacionId'=>$serv,'nr'=>0));
                    $this->ModeloCatalogos->updateCatalogo('asignacion_ser_venta_a',array('horaserinicio'=>$this->horaactual,'status'=>1),array('asignacionId'=>$serv,'nr'=>0));
                    $this->ModeloCatalogos->updateCatalogo('personal',array('servicio'=>1,'servicio_info'=>"|$tipo,$serv|"),array('personalId'=>$this->idpersonal));
                }else{
                    $estatusbloqueoservicio=1;
                }
            }else{
                $this->ModeloCatalogos->updateCatalogo('asignacion_ser_venta_a',array('horaserfin'=>$this->horaactual,'status'=>2),array('asignacionId'=>$serv));
                $this->ModeloCatalogos->updateCatalogo('asignacion_ser_venta_a',array('horafin'=>$this->horaactual,'per_fin'=>$this->idpersonal),array('asignacionId'=>$serv));
                $this->ModeloCatalogos->updateCatalogo('personal',array('servicio'=>0,'servicio_info'=>''),array('personalId'=>$this->idpersonal));
                $this->ModeloCatalogos->updateCatalogo('personal',array('servicio'=>0,'servicio_info'=>''),array('servicio_info'=>"|$tipo,$serv|"));
                if($localserver==1){
                    //$this->envioemail(3,$serve);
                }
                $serviciofinales=1;
            }
        }
        //echo $serviciofinales;
        //==========================================
            $result_per=$this->ModeloCatalogos->getselectwheren('personal',array('personalId'=>$tec));
            $not_new_ser=0;
            foreach ($result_per->result() as $itemper) {
                $not_new_ser=$itemper->not_new_ser;
            }
        //==========================================
        $rest=array(
                    'serviciofinales'=>$serviciofinales,
                    'not_new_ser'=>$not_new_ser,
                    'estatusbloqueoservicio'=>$estatusbloqueoservicio

                );
        echo json_encode($rest);
    }
    function cambiarstatusservicio_promedio(){
        $serv = $this->input->post('serv');
        $serve = $this->input->post('serve');
        $tipo = $this->input->post('tipo');
        $iniciarfinalizar = $this->input->post('iniciarfinalizar');
        $localserver=0;//0 local 1 server
        $hi=$iniciarfinalizar;
        $promedio=0;
        if ($tipo==1) {
            /*
            $resultc=$this->ModeloCatalogos->getselectwheren('asignacion_ser_contrato_a_e',array('asignacionIde'=>$serve));
            foreach ($resultc->result() as $item) {
                if ($item->horaserinicio!=null) {
                    $hi=1;
                }
            }
            */
            $result=$this->ModeloCatalogos->getselectwheren('asignacion_ser_contrato_a_e',array('asignacionIde'=>$serve));
            foreach ($result->result() as $item) {
                $promedio1=$this->Rentas_model->produccionpromedio($item->idequipo,$item->serieId,1,'','');
                $promedio2=$this->Rentas_model->produccionpromedio($item->idequipo,$item->serieId,2,'','');
                $promedio=$promedio1+$promedio2;
                // code...
            }
            
        }
        echo $promedio;
    }
    function cambiarstatusservicio3(){
        $serv = $this->input->post('serv');
        $serve = $this->input->post('serve');
        $tipo = $this->input->post('tipo');
        $iniciarfinalizar = $this->input->post('iniciarfinalizar');
        $localserver=0;//0 local 1 server
        $hi=$iniciarfinalizar;
        if ($tipo==1) {
            //========================================================================================
                $serviciosresult=$this->ModeloCatalogos->getselectwheren('asignacion_ser_contrato_a_e',array('asignacionId'=>$serv,'activo'=>1,'status !='=>2));
                $serviciofinales=1; 
                foreach ($serviciosresult->result() as $item) {
                    if($item->status!=3){
                        $serviciofinales=0;    
                    }
                    
                }
            //========================================================================================
        }
        if ($tipo==2) {
            //========================================================================================
                $serviciosresult=$this->ModeloCatalogos->getselectwheren('asignacion_ser_poliza_a_e',array('asignacionId'=>$serv,'status !='=>2));
                $serviciofinales=1; 
                foreach ($serviciosresult->result() as $item) {
                    $serviciofinales=0;
                }
            //========================================================================================
        }
        if ($tipo==3) {
            //========================================================================================
                $serviciosresult=$this->ModeloCatalogos->getselectwheren('asignacion_ser_cliente_a_d',array('asignacionId'=>$serv,'status !='=>2));
                $serviciofinales=1; 
                foreach ($serviciosresult->result() as $item) {
                    $serviciofinales=0;
                }
            //========================================================================================
        }
        if ($tipo==4) {
            $serviciofinales=0;
            if ($hi==0) {
                //$this->ModeloCatalogos->updateCatalogo('asignacion_ser_venta_a',array('horaserinicio'=>$this->horaactual,'status'=>1),array('asignacionId'=>$serv));
            }else{
                //$this->ModeloCatalogos->updateCatalogo('asignacion_ser_venta_a',array('horaserfin'=>$this->horaactual,'status'=>2),array('asignacionId'=>$serv));
                //if($localserver==1){
                    //$this->envioemail(3,$serve);
                //}
                $serviciofinales=1;
            }
        }
        echo $serviciofinales;
    }
    function cambiarstatusservicio2(){//ver como cambiar este proceso (173) al de cambiar a dos botones
        $serv = $this->input->post('serv');
        $serve = $this->input->post('serve');
        $tipo = $this->input->post('tipo');
        $iniciarfinalizar = $this->input->post('iniciarfinalizar');
        $localserver=0;//0 local 1 server
        $hi=$iniciarfinalizar;

        $tec=0;
        $tec2=0;

        if ($tipo==1) {
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_contrato_a_e',array('horaserfin'=>$this->horaactual,'status'=>2,'firma'=>'','per_fin'=>$this->idpersonal),array('asignacionId'=>$serv));
            //==================
                $result_ser = $this->ModeloCatalogos->getselectwheren('asignacion_ser_contrato_a_e',array('asignacionId'=>$serv));
                foreach ($result_ser->result() as $itemser) {
                    $tec=$itemser->tecnico;
                    $tec2=$itemser->tecnico2;
                }
            //==================
        }
        if ($tipo==2) {
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_poliza_a_e',array('horaserfin'=>$this->horaactual,'status'=>2,'firma'=>'','per_fin'=>$this->idpersonal),array('asignacionId'=>$serv));
            //==================
                $result_ser = $this->ModeloCatalogos->getselectwheren('asignacion_ser_poliza_a_e',array('asignacionId'=>$serv));
                foreach ($result_ser->result() as $itemser) {
                    $tec=$itemser->tecnico;
                    $tec2=$itemser->tecnico2;
                }
            //==================
        }
        if ($tipo==3) {
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_cliente_a_d',array('horaserfin'=>$this->horaactual,'status'=>2,'firma'=>''),array('asignacionId'=>$serv));
            //==================
                $result_ser = $this->ModeloCatalogos->getselectwheren('asignacion_ser_cliente_a',array('asignacionId'=>$serv));
                foreach ($result_ser->result() as $itemser) {
                    $tec=$itemser->tecnico;
                    $tec2=$itemser->tecnico2;
                }
            //==================
        }
        if ($tipo==4) {
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_venta_a',array('horaserfin'=>$this->horaactual,'status'=>2,'firma'=>'','per_fin'=>$this->idpersonal),array('asignacionId'=>$serv));
            //==================
                $result_ser = $this->ModeloCatalogos->getselectwheren('asignacion_ser_venta_a',array('asignacionId'=>$serv));
                foreach ($result_ser->result() as $itemser) {
                    $tec=$itemser->tecnico;
                    $tec2=$itemser->tecnico2;
                }
            //==================
        }
        if($tec>0){
            $this->ModeloCatalogos->updateCatalogo('personal',array('servicio'=>0),array('personalId'=>$tec));
        }
        if($tec2>0){
            $this->ModeloCatalogos->updateCatalogo('personal',array('servicio'=>0),array('personalId'=>$tec2));
        }
    }
    function firmacliente(){
        $datax = $this->input->post();
        $fsrid = $datax['fsrid'];
        $fsried = $datax['fsried'];
        $fsrtipo = $datax['fsrtipo'];
        $firma = $datax['firma'];
        //$qrecibio=$datax['qrecibio'];
        //$comentario = $datax['comentario'];
        $stock_toner_comen= $datax['stock_toner_comen'];

        //$namefile=date('Y_m_d-h_i_s').'.txt';
        //file_put_contents('uploads/firmasservicios/'.$namefile,$firma);
        $namefile='';$info_comen=1;
        if ($fsrtipo==1) {
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_contrato_a',array('stock_toner_comen'=>$stock_toner_comen),array('asignacionId'=>$fsrid));
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_contrato_a_e',array('firma'=>$namefile,'comentario'=>''),array('asignacionId'=>$fsrid));
            
            $result_i_eq=$this->ModeloCatalogos->getselectwheren('asignacion_ser_contrato_a_e',array('asignacionIde'=>$fsried));
        }
        if ($fsrtipo==2) {
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_poliza_a_e',array('firma'=>$namefile,'comentario'=>''),array('asignacionId'=>$fsrid));
            
            $result_i_eq=$this->ModeloCatalogos->getselectwheren('asignacion_ser_poliza_a_e',array('asignacionIde'=>$fsried));
        }
        if ($fsrtipo==3) {
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_cliente_a',array('firma'=>$namefile,'comentario'=>''),array('asignacionId'=>$fsrid));

            $result_i_eq=$this->ModeloCatalogos->getselectwheren('asignacion_ser_cliente_a_d',array('asignacionIdd'=>$fsried));
        }
        if ($fsrtipo==4) {
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_venta_a',array('firma'=>$namefile,'comentario'=>''),array('asignacionId'=>$fsrid));

            $result_i_eq=$this->ModeloCatalogos->getselectwheren('asignacion_ser_venta_a',array('asignacionId'=>$fsrid));
        }

        foreach ($result_i_eq->result() as $item) {
            if($item->comentario_tec==null || $item->comentario_tec=='null' || $item->comentario_tec==''){
                $info_comen=0;
            }
        }

        $this->ModeloCatalogos->updateCatalogo('personal',array('servicio'=>0,'servicio_info'=>''),array('personalId'=>$this->idpersonal));
        $array =array(
            'asig'=>$fsrid,
            'asige'=>$fsried,
            'comen'=>$info_comen);
        echo json_encode($array);
    }
    function editar_qrecibio(){
        $datax = $this->input->post();
        $fsrid = $datax['ser'];
        $sere = $datax['sere'];
        $fsrtipo = $datax['tipo'];
        $qrecibio=$datax['nqrecibio'];

        $namefile='';
        if ($fsrtipo==1) {
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_contrato_a_e',array('qrecibio'=>$qrecibio),array('asignacionId'=>$fsrid,'asignacionIde'=>$sere));
        }
        if ($fsrtipo==2) {
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_poliza_a_e',array('qrecibio'=>$qrecibio),array('asignacionId'=>$fsrid,'asignacionIde'=>$sere));
        }
        if ($fsrtipo==3) {
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_cliente_a_d',array('qrecibio'=>$qrecibio),array('asignacionId'=>$fsrid,'asignacionIdd'=>$sere));
        }
        if ($fsrtipo==4) {
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_venta_a',array('qrecibio'=>$qrecibio),array('asignacionId'=>$fsrid));
        }
    }
    function infoser(){
        $datax = $this->input->post();
        $serv = $datax['serv'];
        $serve = $datax['serve'];
        $tipo = $datax['tipo'];
        $stock_toner=0;$nombre='';
        if ($tipo==1) {
            $resultca=$this->ModeloCatalogos->getselectwheren('asignacion_ser_contrato_a',array('asignacionId'=>$serv));
            foreach ($resultca->result() as $item) {
                $ttipo=$item->ttipo;
                $tservicio=$item->tservicio;
                $tservicio_a=$item->tservicio_a;
                $tserviciomotivo=$item->tserviciomotivo;
                $stock_toner=$item->stock_toner;
            }

            $resultser=$this->ModeloCatalogos->getselectwheren('servicio_evento',array('id'=>$tservicio));
            foreach ($resultser->result() as $item) {
                $nombre=$item->nombre;
                $local=$item->local;
                $semi=$item->semi;
                $foraneo=$item->foraneo;
            }
            $resultser=$this->ModeloCatalogos->getselectwheren('polizas_detalles',array('id'=>$tservicio_a));
            foreach ($resultser->result() as $item) {
                $nombre='';
                $local=$item->precio_local;
                $semi=$item->precio_semi;
                $foraneo=$item->precio_foraneo;
                $especial=$item->precio_especial;
            }
                $costo=0;
            if ($ttipo==1) {
                $costo=$local;
            }
            if ($ttipo==2) {
                $costo=$semi;
            }
            if ($ttipo==3) {
                $costo=$foraneo;
            }
            if ($ttipo==4) {
                $costo=$especial;
            }

        }
        if ($tipo==2) {
            $resultpeq=$this->ModeloAsignacion->getserviciopolizaequipos($serv);
            $costo=0;
            foreach ($resultpeq->result() as $item) {
                $nombre=$item->nombre;
                $tserviciomotivo=$item->comentario;
                if ($item->precio_local!=null) {
                    $costo=$costo+$item->precio_local;
                }
                if ($item->precio_semi!=null) {
                    $costo=$costo+$item->precio_semi;
                }
                if ($item->precio_foraneo!=null) {
                    $costo=$costo+$item->precio_foraneo;
                }
            }


            /*
            $resultca=$this->ModeloCatalogos->getselectwheren('asignacion_ser_poliza_a',array('asignacionId'=>$serv));
            foreach ($resultca->result() as $item) {
                $ttipo=$item->ttipo;
                //$tservicio=$item->tservicio;
                //$tserviciomotivo=$item->tserviciomotivo;
            }
            */
            /*
            $resultser=$this->ModeloCatalogos->getselectwheren('servicio_evento',array('id'=>$tservicio));
            foreach ($resultser->result() as $item) {
                //$nombre=$item->nombre;
                $local=$item->local;
                $semi=$item->semi;
                $foraneo=$item->foraneo;
            }
            */
            /*
            if ($ttipo==1) {
                $costo=$local;
            }
            if ($ttipo==2) {
                $costo=$semi;
            }
            if ($ttipo==3) {
                $costo=$foraneo;
            }
            */
        }
        if ($tipo==3) {
            $resultca=$this->ModeloCatalogos->getselectwheren('asignacion_ser_cliente_a',array('asignacionId'=>$serv));
            foreach ($resultca->result() as $item) {
                
                //$tservicio=$item->tservicio;
                $tserviciomotivo=$item->tserviciomotivo;
            }
            $resultca2=$this->ModeloCatalogos->getselectwheren('asignacion_ser_cliente_a_d',array('asignacionId'=>$serv));
            foreach ($resultca2->result() as $item) {
                $tservicio=$item->tservicio;
                $tservicio_a=$item->tservicio_a;
                $ttipo=$item->ttipo;
            }

            $resultser=$this->ModeloCatalogos->getselectwheren('servicio_evento',array('id'=>$tservicio));
            foreach ($resultser->result() as $item) {
                $nombre=$item->nombre;
                $local=$item->local;
                $semi=$item->semi;
                $foraneo=$item->foraneo;
            }
            $resultser=$this->ModeloCatalogos->getselectwheren('polizas_detalles',array('id'=>$tservicio_a));
            foreach ($resultser->result() as $item) {
                $nombre='';
                $local=$item->precio_local;
                $semi=$item->precio_semi;
                $foraneo=$item->precio_foraneo;
                $especial=$item->precio_especial;
            }
                $costo=0;
            if ($ttipo==1) {
                $costo=$local;
            }
            if ($ttipo==2) {
                $costo=$semi;
            }
            if ($ttipo==3) {
                $costo=$foraneo;
            }
            if ($ttipo==4) {
                $costo=$especial;
            }
        }
        if ($tipo==4) {
            $idventa=0;
            $tipov=0;
            $totalgeneral=0;
            $resultca=$this->ModeloCatalogos->getselectwheren('asignacion_ser_venta_a',array('asignacionId'=>$serv));
            foreach ($resultca->result() as $item) {
                $idventa=$item->ventaId;
                $tipov=$item->tipov;
                //$tservicio=$item->tservicio;
                $tserviciomotivo=$item->tserviciomotivo;
            }
            $resultca2=$this->ModeloCatalogos->getselectwheren('asignacion_ser_venta_a_d',array('asignacionId'=>$serv));
            foreach ($resultca2->result() as $item) {
                $tservicio=$item->tservicio;
                $tservicio_a=$item->tservicio_a;
                $ttipo=$item->ttipo;
            }

            $resultser=$this->ModeloCatalogos->getselectwheren('servicio_evento',array('id'=>$tservicio));
            foreach ($resultser->result() as $item) {
                $nombre=$item->nombre;
                $local=$item->local;
                $semi=$item->semi;
                $foraneo=$item->foraneo;
            }
            $resultser=$this->ModeloCatalogos->getselectwheren('polizas_detalles',array('id'=>$tservicio_a));
            foreach ($resultser->result() as $item) {
                $nombre='';
                $local=$item->precio_local;
                $semi=$item->precio_semi;
                $foraneo=$item->precio_foraneo;
                $especial=$item->precio_especial;
            }
                $costo=0;
            if ($ttipo==1) {
                $costo=$local;
            }
            if ($ttipo==2) {
                $costo=$semi;
            }
            if ($ttipo==3) {
                $costo=$foraneo;
            }
            if ($ttipo==4) {
                $costo=$especial;
            }
            ////////////////////////////////////////////////////////////////////////////////////////////////
            if($tipov==0){
                $tipov_pre=0;
            }
            if($tipov==1){
                $tipov_pre=3;
            }
            $resultpre=$this->ModeloCatalogos->getselectwheren('prefactura',array('ventaId'=>$idventa,'tipo'=>$tipov_pre));
            $formadecobro='';
            foreach ($resultpre->result() as $itemp) {
                $formadecobro=$itemp->formadecobro;
            }
            if($formadecobro=='Tecnico'){
                if($tipov==0){
                    $rowequipos=$this->ModeloCatalogos->db10_getselectwheren('ventas_has_detallesEquipos',array('idVenta'=>$idventa)); 
                    foreach ($rowequipos->result() as $item) { 
                        $totalgeneral=$totalgeneral+($item->cantidad*$item->precio); 
                    } 
                    $resultadoaccesorios = $this->ModeloCatalogos->accesoriosventa($idventa); 
                    foreach ($resultadoaccesorios->result() as $item) { 
                        $totalgeneral=$totalgeneral+($item->cantidad*$item->costo); 
                    } 
                    $resultadoconsumibles = $this->ModeloCatalogos->consumiblesventa($idventa); 
                    foreach ($resultadoconsumibles->result() as $item) {  
                        $totalgeneral=$totalgeneral+($item->cantidad*$item->costo_toner); 
                    } 
                    $consumiblesventadetalles = $this->ModeloCatalogos->consumiblesventadetalles($idventa); 
                    foreach ($consumiblesventadetalles->result() as $item) {  
                        $totalgeneral=$totalgeneral+($item->cantidad*$item->costo_toner); 
                    } 
                    $ventadetallesrefacion = $this->ModeloCatalogos->ventas_has_detallesRefacciones($idventa); 
                    foreach ($ventadetallesrefacion->result() as $item) {  
                        $totalgeneral=$totalgeneral+($item->cantidad*$item->precioGeneral); 
                    }  
                }
                if($tipov==1){
                    $where_ventas=array('combinadaId'=>$idventa); 
                    $resultadov = $this->ModeloCatalogos->db10_getselectwheren('ventacombinada',$where_ventas); 
                    $equipo=0; 
                    $consumibles=0; 
                    $refacciones=0; 
                    $poliza=0; 
                    foreach ($resultadov->result() as $item) { 
                            $equipo=$item->equipo; 
                            $consumibles=$item->consumibles; 
                            $refacciones=$item->refacciones; 
                            $poliza=$item->poliza; 
                    } 
                    
                    $rowequipos=$this->ModeloCatalogos->db10_getselectwheren('ventas_has_detallesEquipos',array('idVenta'=>$equipo)); 
                    foreach ($rowequipos->result() as $item) { 
                        $totalgeneral=$totalgeneral+($item->cantidad*$item->precio); 
                    } 
                    $resultadoaccesorios = $this->ModeloCatalogos->accesoriosventa($equipo); 
                    foreach ($resultadoaccesorios->result() as $item) { 
                        $totalgeneral=$totalgeneral+($item->cantidad*$item->costo); 
                    } 
                    $resultadoconsumibles = $this->ModeloCatalogos->consumiblesventa($equipo); 
                    foreach ($resultadoconsumibles->result() as $item) {  
                        $totalgeneral=$totalgeneral+($item->cantidad*$item->costo_toner); 
                    } 
                    $consumiblesventadetalles = $this->ModeloCatalogos->consumiblesventadetalles($consumibles); 
                    foreach ($consumiblesventadetalles->result() as $item) {  
                        $totalgeneral=$totalgeneral+($item->cantidad*$item->costo_toner); 
                    } 
                    $ventadetallesrefacion = $this->ModeloCatalogos->ventas_has_detallesRefacciones($refacciones); 
                    foreach ($ventadetallesrefacion->result() as $item) {  
                        $totalgeneral=$totalgeneral+($item->cantidad*$item->precioGeneral); 
                    }
                }
                $costo=$costo+$totalgeneral;
            }
            ///////////////////////////////////////////////////////////////////////////////////////////
        }
        $resultadoconsu=$this->Ventas_model->datosserviciosconsumibles($serv,$tipo);
        $resultadorefa=$this->Ventas_model->datosserviciosrefacciones($serv,$tipo);



        $array = array(
            'fmtiposervicio'=>$nombre,
            'fmdescripfalla'=>$tserviciomotivo,
            'fmcosto'=>$costo,
            'consumi'=>$resultadoconsu->result(),
            'refaccion'=>$resultadorefa->result(),
            'stock_toner'=>$stock_toner
         );
        echo json_encode($array);
    }
    function envioemail($tipo,$asignacion){
        $atencionpara='';
        //================================
            //cargamos la libreria email
            $this->load->library('email');

            /*
            * Configuramos los parámetros para enviar el email,
            * las siguientes configuraciones es recomendable
            * hacerlas en el fichero email.php dentro del directorio config,
            * en este caso para hacer un ejemplo rápido lo hacemos 
            * en el propio controlador
            */
            
            //Indicamos el protocolo a utilizar
            $config['protocol'] = 'smtp';
             
            //El servidor de correo que utilizaremos
            $config["smtp_host"] ='mocha3033.mochahost.com'; 
             
            //Nuestro usuario
            $config["smtp_user"] = 'info@altaproductividad.mx';

            //Nuestra contraseña
            $config["smtp_pass"] = 'yUs&J=T4*V$J';

            //Puerto
            $config["smtp_port"] = '465';

            $config["smtp_crypto"] = 'ssl';
                    
            //El juego de caracteres a utilizar
            $config['charset'] = 'utf-8'; 
     
            //Permitimos que se puedan cortar palabras
            $config['wordwrap'] = TRUE;
             
            //El email debe ser valido  
            $config['validate'] = true;

            $config['mailtype'] = 'html';

            //Establecemos esta configuración
            $this->email->initialize($config);
     
            //Ponemos la dirección de correo que enviará el email y un nombre
            $this->email->from('info@altaproductividad.mx','informacion');
             
              /*
               * Ponemos el o los destinatarios para los que va el email
               * en este caso al ser un formulario de contacto te lo enviarás a ti
               * mismo
               */
        //======================
        $existecorreo=0;
        $this->email->bcc('info@altaproductividad.mx');
        if ($tipo==1) {
            $resultc=$this->ModeloCatalogos->getselectwheren('asignacion_ser_contrato_a_e',array('asignacionIde'=>$asignacion));
            foreach ($resultc->result() as $item) {
                $asignacionIde=$item->asignacionIde;
                $asignacionId=$item->asignacionId;
            }
        }
        if ($tipo==2) {
            $resultc=$this->ModeloCatalogos->getselectwheren('asignacion_ser_poliza_a_e',array('asignacionIde'=>$asignacion));
            foreach ($resultc->result() as $item) {
                $asignacionIde=$item->asignacionIde;
                $asignacionId=$item->asignacionId;
            }
        }
        if($tipo==3){
            $asignacionId=$asignacion;
        }
        $contactoId=0;
         $resultprefactura = $this->ModeloCatalogos->getselectwheren('prefactura_servicio',array('asignacionId'=>$asignacionId,'tipo'=>$tipo));     
         foreach ($resultprefactura->result() as $item) {
            $contactoId=$item->contactoId;
         }
        $resultcontacto=$this->ModeloCatalogos->getselectwheren('cliente_datoscontacto',array('datosId'=>$contactoId));
        foreach ($resultcontacto->result() as $item) {
            $atencionpara=$item->atencionpara;
            $atencionparaemail=$item->email;
            $existecorreo=1;
        }

     




        if ($existecorreo==1) {
           $this->email->to($atencionparaemail, $atencionpara);
        }else{
            $this->email->to('gerardo@mangoo.com.mx', $atencionpara);
        }
        
        
        $asunto='Programación';
        

      //Definimos el asunto del mensaje
        $this->email->subject($asunto);
         
      //Definimos el mensaje a enviar
      //$this->email->message($body)


        $message  = '<div style="background: url(http://anahuac.sicoi.net/public/img/anahuac-fondo-footer.jpg) #eaeaea;width: 100%;height: 94%;padding-top: 25px;margin: 0px;">
                    <div style="width: 10%; height: 10px; float: left;">
                        
                    </div>
                    <div style="background: white; border: 1px solid #F8F8F8; border-radius: 15px; padding: 25px; width: 80%; margin-left: auto; margin-right: auto; float: left; -webkit-box-shadow: 10px 10px 42px -6px rgba(0,0,0,0.75);-moz-box-shadow: 10px 10px 42px -6px rgba(0,0,0,0.75);box-shadow: 10px 10px 42px -6px rgba(0,0,0,0.75);">
                        <div style="width: 100%; float: left;">
                            <div style="width: 30%;float: left;">
                                <img src="https://altaproductividad.mx/public/img/alta.png" width="100%">
                            </div>
                            <div style="width: 40%;float: left; min-height:10px ">
                                
                            </div>
                            <div style="width: 30%;float: left;">
                                <img src="https://altaproductividad.mx/app-assets/images/1024_kyocera_logo_mds.png" width="100%">
                            </div>
                        </div>
                        <div style="width: 100%; float: left;">
                            <hr style="border: 0;   height: 1px;   background-image: -webkit-linear-gradient(left, #f0f0f0, red, #f0f0f0);  background-image: -moz-linear-gradient(left, #f0f0f0, red, #f0f0f0);  background-image: -ms-linear-gradient(left, #f0f0f0, red, #f0f0f0);  background-image: -o-linear-gradient(left, #f0f0f0, red, #f0f0f0);">
                            <hr style="border: 0;   height: 1px;   background-image: -webkit-linear-gradient(left, #f0f0f0, red, #f0f0f0);  background-image: -moz-linear-gradient(left, #f0f0f0, red, #f0f0f0);  background-image: -ms-linear-gradient(left, #f0f0f0, red, #f0f0f0);  background-image: -o-linear-gradient(left, #f0f0f0, red, #f0f0f0);">
                        </div>
                        <div style="width: 100%; float: left;">
                            <h2>Estimad@:'.$atencionpara.'</h2>
                        </div>
                        <div style="width: 100%; float: left;">
                            <h4>En el presente se adjunta la prefactura de su servicio, asi mismo la hora con el detalle del mismo.</h4>
                        </div>
                        <div style="width: 100%; float: left;">
                            <h4>Para cualquier duda o detalle favor de comunicarse con su ejecutiva de cuenta.</h4>
                        </div>
                        <div style="width: 100%; float: left; text-align: center; ">
                            <p>(Direccion de alta productividad)</p>
                            <p>(Tomar de prefactura)</p>
                        </div>
                        <div style="width: 100%; float: left;">
                            <hr style="border: 0;   height: 1px;   background-image: -webkit-linear-gradient(left, #f0f0f0, red, #f0f0f0);  background-image: -moz-linear-gradient(left, #f0f0f0, red, #f0f0f0);  background-image: -ms-linear-gradient(left, #f0f0f0, red, #f0f0f0);  background-image: -o-linear-gradient(left, #f0f0f0, red, #f0f0f0);">
                            <hr style="border: 0;   height: 1px;   background-image: -webkit-linear-gradient(left, #f0f0f0, red, #f0f0f0);  background-image: -moz-linear-gradient(left, #f0f0f0, red, #f0f0f0);  background-image: -ms-linear-gradient(left, #f0f0f0, red, #f0f0f0);  background-image: -o-linear-gradient(left, #f0f0f0, red, #f0f0f0);">
                        </div>
                    </div>
                </div>';

        $this->email->message($message);
        if ($tipo==1) { //contrato renta
            $resultc=$this->ModeloCatalogos->getselectwheren('asignacion_ser_contrato_a_e',array('asignacionIde'=>$asignacion));
            foreach ($resultc->result() as $item) {
                $asignacionIde=$item->asignacionIde;
                $asignacionId=$item->asignacionId;
            }
            $this->email->attach(base_url().'Listaservicios/contrato_formato/'.$asignacionId.'/'.$tipo.'/'.$asignacionIde, 'attachment', 'orden.pdf');
            //log_message('error', base_url().'Listaservicios/contrato_formato/'.$asignacionId.'/'.$tipo.'/'.$asignacionIde);
            //log_message('error', base_url().'Prefactura/viewser/'.$asignacionId.'/'.$tipo.'?getpdf=true');
            $this->email->attach(base_url().'Prefactura/viewser/'.$asignacionId.'/'.$tipo.'?getpdf=true','attachment', 'prefactura.pdf');
        }
        if ($tipo==2) { //Poliza
            $resultc=$this->ModeloCatalogos->getselectwheren('asignacion_ser_poliza_a_e',array('asignacionIde'=>$asignacion));
            foreach ($resultc->result() as $item) {
                $asignacionIde=$item->asignacionIde;
                $asignacionId=$item->asignacionId;
            }
            $this->email->attach(base_url().'Listaservicios/tipo_formato/'.$asignacionId.'/'.$tipo.'/'.$asignacionIde, 'attachment', 'orden.pdf');
            $this->email->attach(base_url().'Prefactura/viewser/'.$asignacionId.'/'.$tipo.'?getpdf=true','attachment', 'prefactura.pdf');
            //http://localhost/kyocera2/Prefactura/viewser/'.$asignacionId.'/'.$tipo.'
        }
        if ($tipo==3) { //evento
            
            $this->email->attach(base_url().'Listaservicios/tipo_formato/'.$asignacionId.'/'.$tipo.'/0', 'attachment', 'orden.pdf');
            $this->email->attach(base_url().'Prefactura/viewser/'.$asignacionId.'/'.$tipo.'?getpdf=true','attachment', 'prefactura.pdf');
            //http://localhost/kyocera2/Prefactura/viewser/'.$asignacionId.'/'.$tipo.'
        }
        //Enviamos el email y si se produce bien o mal que avise con una flasdata
        if($this->email->send()){
            //$this->session->set_flashdata('envio', 'Email enviado correctamente');
        }else{
            //$this->session->set_flashdata('envio', 'No se a enviado el email');
        }

        //==================
    }
    function pdfser($asignacion,$tipo){
        $data['asignacion']=$asignacion;
        $data['tipo']=$tipo;
        $this->load->view('Reportes/pdfser',$data);
    }
    function infoventa(){
        $datax = $this->input->post();
        $serv = $datax['serv'];
        $serve = $datax['serve'];
        $tipo = $datax['tipo'];
        $consu_html='';
        $refa_html='';
        $acce_html='';
        $strq_ava="SELECT ava.asId,ava.cantidad,ava.costo,ava.accessorio,acc.nombre
                    FROM asignacion_ser_ava as ava 
                    inner join catalogo_accesorios as acc on acc.id=ava.accessorio
                    where ava.asignacionId=$serv and ava.tipo=$tipo ";
        $query_ava = $this->db->query($strq_ava);

        $strq_avc="SELECT avc.asId,avc.cantidad,avc.costo, avc.consumible,con.modelo
                    FROM asignacion_ser_avc as avc 
                    inner join consumibles as con on con.id=avc.consumible
                    where avc.asignacionId=$serv and avc.tipo=$tipo ";
        $query_avc = $this->db->query($strq_avc);

        $strq_avr="SELECT avr.asId,avr.cantidad,avr.costo, avr.refacciones,ref.nombre
                    FROM asignacion_ser_avr as avr 
                    inner join refacciones as ref on ref.id=avr.refacciones
                    where avr.asignacionId=$serv and avr.tipo=$tipo ";
        $query_avr = $this->db->query($strq_avr);
        $totalgeneral=0;
        foreach ($query_ava->result() as $item) {
            $acce_html.='<tr>
                            <td>'.$item->cantidad.'</td>
                            <td>'.$item->nombre.'</td>
                            <td>'.$item->costo.'</td>
                            <td>
                                <a class="btn-floating red tooltipped elimina" data-position="top" data-delay="50" 
                                onclick="delete_ava('.$item->asId.','.$serv.','.$serve.','.$tipo.',1)"
                                data-tooltip="Eliminar">
                                    <i class="fas fa-minus"></i></a>
                            </td>
                        </tr>';
            $totalgeneral=$totalgeneral+($item->cantidad*$item->costo);
        }
        foreach ($query_avc->result() as $item) {
            $consu_html.='<tr>
                            <td>'.$item->cantidad.'</td>
                            <td>'.$item->modelo.'</td>
                            <td>'.$item->costo.'</td>
                            <td>
                                <a class="btn-floating red tooltipped elimina" data-position="top" data-delay="50" 
                                onclick="delete_avc('.$item->asId.','.$serv.','.$serve.','.$tipo.',2)"
                                data-tooltip="Eliminar">
                                    <i class="fas fa-minus"></i></a>
                            </td>
                        </tr>';
            $totalgeneral=$totalgeneral+($item->cantidad*$item->costo);
        }
        foreach ($query_avr->result() as $item) {
            $refa_html.='<tr>
                            <td>'.$item->cantidad.'</td>
                            <td>'.$item->nombre.'</td>
                            <td>'.$item->costo.'</td>
                            <td>
                                <a class="btn-floating red tooltipped elimina" data-position="top" data-delay="50" 
                                onclick="delete_avr('.$item->asId.','.$serv.','.$serve.','.$tipo.',3)"
                                data-tooltip="Eliminar">
                                    <i class="fas fa-minus"></i></a>
                            </td>
                        </tr>';
            $totalgeneral=$totalgeneral+($item->cantidad*$item->costo);
        }
        
        $array = array(
            'consu'=>$consu_html,
            'refa'=>$refa_html,
            'acce'=>$acce_html,
            'totalg'=>$totalgeneral
         );
        echo json_encode($array);
    }
    function disminuircantidad($id,$table){
        if ($table==1) {
            $tablet='asignacion_ser_ava';
        }
        if ($table==2) {
            $tablet='asignacion_ser_avc';
        }
        if ($table==3) {
            $tablet='asignacion_ser_avr';
        }
        $this->db->query("UPDATE $tablet SET cantidad=cantidad-1 WHERE asId=$id");
    }
    function cambiartiempos(){
        $equipos=$this->input->post('equipos');
        $DATAeq = json_decode($equipos);
        /*
            $fsrid = $datax['fsrid'];
            $fsried = $datax['fsried'];
            $fsrtipo = $datax['fsrtipo'];

            $horaserinicioext = $datax['hini'];
            $horaserfinext = $datax['hfin'];

            
            if ($fsrtipo==1) {
                $this->ModeloCatalogos->db4_updateCatalogo('asignacion_ser_contrato_a_e',array('hora'=>$horaserinicioext,'horafin'=>$horaserfinext,'horaserinicioext'=>$horaserinicioext,'horaserfinext'=>$horaserfinext),array('asignacionIde'=>$fsried));
            }
            if ($fsrtipo==2) {
                $this->ModeloCatalogos->updateCatalogo('asignacion_ser_poliza_a_e',array('hora'=>$horaserinicioext,'horafin'=>$horaserfinext,'horaserinicioext'=>$horaserinicioext,'horaserfinext'=>$horaserfinext),array('asignacionIde'=>$fsried));
            }
            if ($fsrtipo==3) {
                $this->ModeloCatalogos->updateCatalogo('asignacion_ser_cliente_a_d',array('horaserinicio'=>$horaserinicioext,'horaserfin'=>$horaserfinext,'horaserinicioext'=>$horaserinicioext,'horaserfinext'=>$horaserfinext),array('asignacionIdd'=>$fsried));
            }
            if ($fsrtipo==4) {
                $this->ModeloCatalogos->updateCatalogo('asignacion_ser_venta_a',array('hora'=>$horaserinicioext,'horafin'=>$horaserfinext,'horaserinicioext'=>$horaserinicioext,'horaserfinext'=>$horaserfinext),array('asignacionId'=>$fsrid));
            }
        */
        for ($j=0;$j<count($DATAeq);$j++){
            $hi = $DATAeq[$j]->hi;
            $hf = $DATAeq[$j]->hf;
            $tipo = $DATAeq[$j]->tipo;
            $serve = $DATAeq[$j]->serve;
            log_message('error',"cambiartiempos tipo:$tipo serve:$serve hi:$hi hf:$hf personal: $this->idpersonal ");
            
            if ($tipo==1) {
                $this->ModeloCatalogos->updateCatalogo('asignacion_ser_contrato_a_e',array('hora'=>$hi,'horafin'=>$hf,'horaserinicioext'=>$hi,'horaserfinext'=>$hf,'horaserinicio'=>$hi,'horaserfin'=>$hf,'status'=>2,'per_fin'=>$this->idpersonal),array('asignacionIde'=>$serve));
            }
            if ($tipo==2) {
                $this->ModeloCatalogos->updateCatalogo('asignacion_ser_poliza_a_e',array('hora'=>$hi,'horafin'=>$hf,'horaserinicioext'=>$hi,'horaserfinext'=>$hf,'horaserinicio'=>$hi,'horaserfin'=>$hf,'status'=>2,'per_fin'=>$this->idpersonal),array('asignacionIde'=>$serve));
            }
            if ($tipo==3) {
                $this->ModeloCatalogos->updateCatalogo('asignacion_ser_cliente_a_d',array('horaserinicio'=>$hi,'horaserfin'=>$hf,'horaserinicioext'=>$hi,'horaserfinext'=>$hf,'status'=>2),array('asignacionIdd'=>$serve));
            }
            if ($tipo==4) {
                $this->ModeloCatalogos->updateCatalogo('asignacion_ser_venta_a',array('hora'=>$hi,'horafin'=>$hf,'horaserinicio'=>$hi,'horaserfin'=>$hf,'horaserinicioext'=>$hi,'horaserfinext'=>$hf,'status'=>2,'per_fin'=>$this->idpersonal),array('asignacionId'=>$serve));
            }
            
            $this->ModeloCatalogos->Insert('bitacora',array('contenido'=>"cambiartiempos tipo:$tipo serve:$serve hi:$hi hf:$hf personal: $this->idpersonal ",'nombretabla'=>'','idtable'=>$serve,'tipo'=>'edicion de horarios','personalId'=>$this->idpersonal,'activo'=>0));

            $this->ModeloCatalogos->updateCatalogo('personal',array('servicio'=>0),array('personalId'=>$this->idpersonal));
        }
    }
    function addcomentarios(){
        $datax = $this->input->post();
        $serv = $datax['serv'];
        $serve = $datax['serve'];
        $tipo = $datax['tipo'];
        $comentario = $datax['comentario'];
        //$contadorini = $datax['contadorini'];
        $contadorfin = $datax['contadorfin'];
        $qrecibio = $datax['qrecibio'];
        $mismo = $datax['mismo'];

        if ($tipo==1) {
            if($mismo==1){
                $this->ModeloCatalogos->updateCatalogo('asignacion_ser_contrato_a_e',array('qrecibio'=>$qrecibio),array('asignacionId'=>$serv));
            }
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_contrato_a_e',array('comentario_tec'=>$comentario,'contadorfin'=>$contadorfin,'qrecibio'=>$qrecibio),array('asignacionIde'=>$serve));
        }
        if ($tipo==2) {
            if($mismo==1){
                $this->ModeloCatalogos->updateCatalogo('asignacion_ser_poliza_a_e',array('qrecibio'=>$qrecibio),array('asignacionId'=>$serv));
            }
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_poliza_a_e',array('comentario_tec'=>$comentario,'qrecibio'=>$qrecibio),array('asignacionIde'=>$serve));
        }
        if ($tipo==3) {
            if($mismo==1){
                $this->ModeloCatalogos->updateCatalogo('asignacion_ser_cliente_a_d',array('qrecibio'=>$qrecibio),array('asignacionId'=>$serv));
            }
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_cliente_a_d',array('comentario_tec'=>$comentario,'qrecibio'=>$qrecibio),array('asignacionIdd'=>$serve));
        }
        if ($tipo==4) {
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_venta_a',array('comentario_tec'=>$comentario,'qrecibio'=>$qrecibio),array('asignacionId'=>$serv));
        }
    }
    function viewcomentarios(){
        $datax = $this->input->post();
        $serv = $datax['serv'];
        $serve = $datax['serve'];
        $tipo = $datax['tipo'];

        if ($tipo==1) {
           $result=$this->ModeloCatalogos->getselectwheren('asignacion_ser_contrato_a_e',array('asignacionIde'=>$serve));
        }
        if ($tipo==2) {
            $result=$this->ModeloCatalogos->getselectwheren('asignacion_ser_poliza_a_e',array('asignacionIde'=>$serve));
        }
        if ($tipo==3) {
            $result=$this->ModeloCatalogos->getselectwheren('asignacion_ser_cliente_a_d',array('asignacionIdd'=>$serve));
        }
        if ($tipo==4) {
            $result=$this->ModeloCatalogos->getselectwheren('asignacion_ser_venta_a',array('asignacionId'=>$serv));
        }
        $comentario_tec='';
        foreach ($result->result() as $item) {
            if($item->comentario_tec!='null' && $item->comentario_tec!=null){
                $comentario_tec=$item->comentario_tec;    
            }else{
                $comentario_tec='';
            }
            
            if($tipo==1){
                $contadorini=$item->contadorini;
                $contadorfin = $item->contadorfin;
            }else{
                $contadorini='';
                $contadorfin = '';
            }
            if($item->qrecibio!='null' && $item->qrecibio!=null){
                $qrecibio=$item->qrecibio;
            }else{
                $qrecibio='';
            }
        }
        $array = array(
            'comentario'=>$comentario_tec,
            'contadorini'=>$contadorini,
            'contadorfin'=>$contadorfin,
            'qrecibio'=>$qrecibio
         );
        echo json_encode($array);
    }
    function viewcomentarios_error(){
        $datax = $this->input->post();
        $serv = $datax['serv'];
        $serve = $datax['serve'];
        $tipo = $datax['tipo'];
        $info = $datax['info'];

        if ($tipo==1) {
           $this->ModeloCatalogos->updateCatalogo('asignacion_ser_contrato_a_e',array('notificarerror'=>$info,'notificarerror_estatus'=>1),array('asignacionIde'=>$serve));
        }
        if ($tipo==2) {
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_poliza_a_e',array('notificarerror'=>$info,'notificarerror_estatus'=>1),array('asignacionIde'=>$serve));
        }
        if ($tipo==3) {
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_cliente_a_d',array('notificarerror'=>$info,'notificarerror_estatus'=>1),array('asignacionIdd'=>$serve));
        }
        if ($tipo==4) {
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_venta_a',array('notificarerror'=>$info,'notificarerror_estatus'=>1),array('asignacionId'=>$serv));
        }

    }
    function descartarcomentarios_error(){
        $datax = $this->input->post();
        $id = $datax['id'];
        $tipo = $datax['tipo'];
        

        if ($tipo==1) {
           $this->ModeloCatalogos->updateCatalogo('asignacion_ser_contrato_a_e',array('notificarerror_estatus'=>0),array('asignacionIde'=>$id));
        }
        if ($tipo==2) {
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_poliza_a_e',array('notificarerror_estatus'=>0),array('asignacionIde'=>$id));
        }
        if ($tipo==3) {
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_cliente_a_d',array('notificarerror_estatus'=>0),array('asignacionIdd'=>$id));
        }
        if ($tipo==4) {
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_venta_a',array('notificarerror_estatus'=>0),array('asignacionId'=>$id));
        }
    }
    function notificacionrechazo(){
        $datax = $this->input->post();
        $serv = $datax['serv'];
        $serve = $datax['serve'];
        $tipo = $datax['tipo'];
        $info = $datax['info'];
        $comtipo = $datax['comtipo'];
        $pass = $datax['pass'];
        log_message('error',"Pass: '$pass' ");
        $permiso=0;
        $pasa_process=0;
        $nr_amonestacion=0;
        if($pass!=''){
            $v_pass=1;
            //====================
                $permiso=0;
                $permisos = $this->Login_model->permisoadminarray(array('1','9','17','18','12','54'));//1admin 9 conta 17 julio 18 diana 12 lupita 54 mayra
                foreach ($permisos as $item) {
                    $permisotemp=0;

                    $usuario=$item->Usuario;

                    $verificar = password_verify($pass,$item->contrasena);

                    if ($verificar==false) {
                        $verificar = password_verify($pass,$item->clavestemp);
                        if ($verificar) {
                            $permisotemp=1;
                            $pasa_process=1;
                        }else{
                            //$result_permiso_cli = $this->ModeloGeneral->permisoscliente($cli,$pass);
                            log_message('error',"pass rechazo: '$pass' ");
                        }
                    }
                    //if ($verificar) {
                        //$verificar = $this->permisoscliente($cli,$pass);
                    //}else{
                        //$verificar = $this->ModeloGeneral->permisoscliente($cli,$pass);
                    //}
                    
                    if ($verificar) {
                        $permiso=1;
                        $pasa_process=1;
                        if($permisotemp==1){
                            $this->ModeloCatalogos->updateCatalogo('usuarios',array('clavestemp'=>null,'clavestemp_text'=>null),array('UsuarioID'=>$item->UsuarioID));
                        }

                        
                    }
                }
            //====================
        }else{
            $v_pass=0;
            $pasa_process=1;
            $nr_amonestacion=1;
        }

        if($pasa_process==1){
            if ($tipo==1) {
               $this->ModeloCatalogos->updateCatalogo('asignacion_ser_contrato_a_e',array('nr_coment'=>$info,'nr_coment_tipo'=>$comtipo,'nr'=>1,'status'=>0),array('asignacionIde'=>$serve,'status'=>0));
               $this->regresarequipoporrechazo(0,$serve,1);
            }
            if ($tipo==2) {
                $this->ModeloCatalogos->updateCatalogo('asignacion_ser_poliza_a_e',array('nr_coment'=>$info,'nr_coment_tipo'=>$comtipo,'nr'=>1,'status'=>0),array('asignacionIde'=>$serve,'status'=>0));
            }
            if ($tipo==3) {
                $this->ModeloCatalogos->updateCatalogo('asignacion_ser_cliente_a_d',array('nr_coment'=>$info,'nr_coment_tipo'=>$comtipo,'nr'=>1,'status'=>0),array('asignacionIdd'=>$serve,'status'=>0));
                $rest_info_ser=$this->ModeloCatalogos->getselectwheren('asignacion_ser_cliente_a_d',array('asignacionIdd'=>$serve,'status'=>0));
                foreach ($rest_info_ser->result() as $item_i_s) {
                    if($item_i_s->retiro==1){
                        if($item_i_s->rowequipo>0){
                            $this->ModeloCatalogos->updateCatalogo('asignacion_series_r_equipos',array('poreliminar'=>0),array('id_equipo'=>$item_i_s->rowequipo));
                        }
                    }
                }
            }
            if ($tipo==4) {
                $this->ModeloCatalogos->updateCatalogo('asignacion_ser_venta_a',array('nr_coment'=>$info,'nr_coment_tipo'=>$comtipo,'nr'=>1,'status'=>0),array('asignacionId'=>$serv));
            }
            $this->ModeloCatalogos->Insert('bitacora_rechazo',array('serv'=>$serv,'serve'=>$serve,'tipo'=>$tipo,'comen'=>$info,'comen_tipo'=>$comtipo,'personal'=>$this->idpersonal,'nr_amonestacion'=>$nr_amonestacion));
            $this->ModeloCatalogos->updateCatalogo('personal',array('servicio'=>0,'servicio_info'=>''),array('personalId'=>$this->idpersonal));
        }

        $array = array(
                        'v_pass'=>$v_pass,
                        'permiso'=>$permiso);
        echo json_encode($array);
    }
    function notificacionrechazogeneral(){
        $datax = $this->input->post();
        $asignacion = $datax['asignacion'];
        $tipo = $datax['tipo'];
        $info = $datax['info'];
        $comtipo = $datax['comtipo'];
        $pass = $datax['pass'];
        log_message('error',"Pass: '$pass' ");
        $permiso=0;
        $pasa_process=0;
        $nr_amonestacion=0;
        if($pass!=''){
            $v_pass=1;
            //====================
                $permiso=0;
                $permisos = $this->Login_model->permisoadminarray(array('1','9','17','18','12','54'));//1admin 9 conta 17 julio 18 diana 12 lupita 54 mayra
                foreach ($permisos as $item) {
                    $permisotemp=0;

                    $usuario=$item->Usuario;

                    $verificar = password_verify($pass,$item->contrasena);

                    if ($verificar==false) {
                        $verificar = password_verify($pass,$item->clavestemp);
                        if ($verificar) {
                            $permisotemp=1;
                            $pasa_process=1;
                        }else{
                            //$result_permiso_cli = $this->ModeloGeneral->permisoscliente($cli,$pass);
                            log_message('error',"pass rechazo: '$pass' ");
                        }
                    }
                    //if ($verificar) {
                        //$verificar = $this->permisoscliente($cli,$pass);
                    //}else{
                        //$verificar = $this->ModeloGeneral->permisoscliente($cli,$pass);
                    //}
                    
                    if ($verificar) {
                        $permiso=1;
                        $pasa_process=1;
                        if($permisotemp==1){
                            $this->ModeloCatalogos->updateCatalogo('usuarios',array('clavestemp'=>null,'clavestemp_text'=>null),array('UsuarioID'=>$item->UsuarioID));
                        }

                        
                    }
                }
            //====================
        }else{
            $v_pass=0;
            $pasa_process=1;
            $nr_amonestacion=2;
        }

        if($pasa_process==1){
            if ($tipo==1) {
               $this->ModeloCatalogos->updateCatalogo('asignacion_ser_contrato_a_e',array('nr_coment'=>$info,'nr_coment_tipo'=>$comtipo,'nr'=>1,'status'=>0),array('asignacionId'=>$asignacion,'status'=>0));
               $this->regresarequipoporrechazo($asignacion,0,1);
            }
            if ($tipo==2) {
                $this->ModeloCatalogos->updateCatalogo('asignacion_ser_poliza_a_e',array('nr_coment'=>$info,'nr_coment_tipo'=>$comtipo,'nr'=>1,'status'=>0),array('asignacionId'=>$asignacion,'status'=>0));
            }
            if ($tipo==3) {
                $this->ModeloCatalogos->updateCatalogo('asignacion_ser_cliente_a_d',array('nr_coment'=>$info,'nr_coment_tipo'=>$comtipo,'nr'=>1,'status'=>0),array('asignacionId'=>$asignacion,'status'=>0));
                $rest_info_ser=$this->ModeloCatalogos->getselectwheren('asignacion_ser_cliente_a_d',array('asignacionId'=>$asignacion,'status'=>0));
                foreach ($rest_info_ser->result() as $item_i_s) {
                    if($item_i_s->retiro==1){
                        if($item_i_s->rowequipo>0){
                            $this->ModeloCatalogos->updateCatalogo('asignacion_series_r_equipos',array('poreliminar'=>0),array('id_equipo'=>$item_i_s->rowequipo));
                        }
                    }
                }
            }
            if ($tipo==4) {
                $this->ModeloCatalogos->updateCatalogo('asignacion_ser_venta_a',array('nr_coment'=>$info,'nr_coment_tipo'=>$comtipo,'nr'=>1,'status'=>0),array('asignacionId'=>$asignacion));
            }
            $this->ModeloCatalogos->updateCatalogo('unidades_bitacora_itinerario',array('nr'=>1),array('asignacion'=>$asignacion,'tipo_asignacion'=>$tipo));
            $this->ModeloCatalogos->Insert('bitacora_rechazo',array('serv'=>$asignacion,'serve'=>0,'tipo'=>$tipo,'comen'=>'General: '.$info,'comen_tipo'=>$comtipo,'personal'=>$this->idpersonal,'nr_amonestacion'=>$nr_amonestacion));
            $this->ModeloCatalogos->updateCatalogo('personal',array('servicio'=>0,'servicio_info'=>''),array('personalId'=>$this->idpersonal));
        }

        $array = array(
                        'v_pass'=>$v_pass,
                        'permiso'=>$permiso);
        echo json_encode($array);

    }
    function reactivarser(){
        $datax = $this->input->post();
        $serv = $datax['serv'];
        $serve = $datax['serve'];
        $tipo = $datax['tipo'];
        $info = $datax['info'];
        $comtipo = $datax['comtipo'];
        $indiglob = $datax['indiglob'];
        $rechazo = $datax['rechazo'];

            //$dataarray=array()
            $dataarray['status']=0;
        if($rechazo==1){
            $dataarray['nr_coment']=$info;
            $dataarray['nr_coment_tipo']=$comtipo;
            $dataarray['nr']=1;
        }
        if($indiglob==1){//1 individual 0 global
            $wherearray1=array('asignacionIde'=>$serve);
            $wherearray2=array('asignacionIdd'=>$serve);
            $wherearray3=array('asignacionId'=>$serv);
        }else{
            $wherearray1=array('asignacionId'=>$serv);
            $wherearray2=array('asignacionId'=>$serv);
            $wherearray3=array('asignacionId'=>$serv);
        }


        if ($tipo==1) {
           $this->ModeloCatalogos->updateCatalogo('asignacion_ser_contrato_a_e',$dataarray,$wherearray1);
        }
        if ($tipo==2) {
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_poliza_a_e',$dataarray,$wherearray1);
        }
        if ($tipo==3) {
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_cliente_a_d',$dataarray,$wherearray2);
        }
        if ($tipo==4) {
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_venta_a',$dataarray,$wherearray3);
        }
        if($rechazo==1){
            $this->ModeloCatalogos->Insert('bitacora_rechazo',array('serv'=>$serv,'serve'=>$serve,'tipo'=>$tipo,'comen'=>$info,'comen_tipo'=>$comtipo,'personal'=>$this->idpersonal));
        }
    }
    /*
    function otenerdireccion($asig,$tipo){
        $direccion=array('idrow'=>'0','direccion'=>'','longitud'=>'','latitud'=>'');
        if($tipo==1){
            $strq="SELECT * FROM `asignacion_ser_contrato_a_e` WHERE asignacionId='$asig'";
            $rest_asig = $this->db->query($strq);
            foreach ($rest_asig->result() as $item) {

                $resultdir=$this->ModeloCatalogos->db2_getselectwheren('contrato_equipo_direccion',array('equiposrow'=>$item->idequipo,'serieId'=>$item->serieId));
                $iddireccionget='g_0';
                //$direccion='';
                foreach ($resultdir->result() as $itemdir) {
                    if($itemdir->direccion!=''){
                        $iddireccionget=$itemdir->direccion;
                    }
                }
                //log_message('error','iddireccionget '.$iddireccionget);
                if($iddireccionget!='g_0'){
                    $tipodireccion=explode('_',$iddireccionget);

                    $tipodir = $tipodireccion[0];
                    $iddir   = $tipodireccion[1];
                    if ($tipodir=='g') {
                        $resultdirc_where['idclientedirecc']=$iddir;
                        
                        
                        $resultdirc=$this->ModeloCatalogos->db2_getselectwheren('clientes_direccion',$resultdirc_where);
                        foreach ($resultdirc->result() as $item) {
                            $direccion=array('idrow'=>'g_'.$iddir,'direccion'=>$item->direccion,'longitud'=>$item->longitud,'latitud'=>$item->latitud);
                        }
                    }
                    if ($tipodir=='f') {
                        $resultdirc_where['id']=$iddir;
                        
                        
                        $resultdirc=$this->ModeloCatalogos->db2_getselectwheren('cliente_has_datos_fiscales',$resultdirc_where);
                        foreach ($resultdirc->result() as $item) {

                            $direccion=array(
                                            'idrow'=>'f_'.$iddir,
                                            'direccion'=>'Razon social: '.$item->razon_social.' Colonia:'.$item->colonia.' Calle:'.$item->calle.' Numero:'.$item->num_ext,
                                            'longitud'=>$item->longitud,
                                            'latitud'=>$item->latitud
                                        );
                        }
                    }
                }
            }
        }
        if($tipo==2){
            $strq="SELECT * FROM asignacion_ser_poliza_a WHERE asignacionId='$asig'";
            $rest_asig = $this->db->query($strq);
            foreach ($rest_asig->result() as $item) {
                $polizaId=$item->polizaId;
                $strq_p="SELECT * FROM polizasCreadas_has_detallesPoliza WHERE idPoliza='$polizaId'";
                $rest_p = $this->db->query($strq_p);
                foreach ($rest_p->result() as $itemp) {
                    $direccionservicio=$itemp->direccionservicio;
                    $idrow='0';
                    $longitud='';
                    $latitud='';
                    $strq_d="SELECT * FROM clientes_direccion WHERE direccion='$direccionservicio'";
                    $rest_d = $this->db->query($strq_d);
                    foreach ($rest_d->result() as $itemd) {
                        $idrow='g_'.$itemd->idclientedirecc;
                        $longitud=$itemd->longitud;
                        $latitud=$itemd->latitud;
                    }



                    $direccion=array('idrow'=>$idrow,'direccion'=>$direccionservicio,'longitud'=>$longitud,'latitud'=>$latitud);
                }

                
            }
        }
        if($tipo==3){
            $strq="SELECT * FROM asignacion_ser_cliente_a_d WHERE asignacionId='$asig'";
            $rest_asig = $this->db->query($strq);
            foreach ($rest_asig->result() as $item) {
                $direccionl=$item->direccion;
                $idrow='0';
                $longitud='';
                $latitud='';
                $strq_d="SELECT * FROM clientes_direccion WHERE direccion='$direccionl'";
                $rest_d = $this->db->query($strq_d);
                foreach ($rest_d->result() as $itemd) {
                    $idrow='g_'.$itemd->idclientedirecc;
                    $longitud=$itemd->longitud;
                    $latitud=$itemd->latitud;
                }


                $direccion=array('idrow'=>$idrow,'direccion'=>$direccionl,'longitud'=>$longitud,'latitud'=>$latitud);
            }
        }
        if($tipo==4){
            $strq="SELECT * FROM asignacion_ser_venta_a_d WHERE asignacionId='$asig'";
            $rest_asig = $this->db->query($strq);
            foreach ($rest_asig->result() as $item) {
                $direccionl=$item->direccion;
                $idrow='0';
                $longitud='';
                $latitud='';
                $strq_d="SELECT * FROM clientes_direccion WHERE direccion='$direccionl'";
                $rest_d = $this->db->query($strq_d);
                foreach ($rest_d->result() as $itemd) {
                    $idrow='g_'.$itemd->idclientedirecc;
                    $longitud=$itemd->longitud;
                    $latitud=$itemd->latitud;
                }


                $direccion=array('idrow'=>$idrow,'direccion'=>$direccionl,'longitud'=>$longitud,'latitud'=>$latitud);
            }
        }
        //echo json_encode($direccion);
        return $direccion;
    }
    */
    function not_new_ser(){
        $params = $this->input->post();
        $tec = $params['tec'];

        $this->ModeloCatalogos->updateCatalogo('personal',array('not_new_ser'=>0),array('personalId'=>$tec));
    }
    function estatus_tecnicos(){
        $html='';
        $res_tec = $this->ModeloServicio->view_estatus_tecnicos(date('Y-m-d'),1);
        $html.='<table class="table"><tr><th>personal</th><th>Retrasos</th><th>Estatus</th></tr>';
        foreach ($res_tec->result() as $item) {
            if($item->counr_ser>0){
                $emji=' emoji_triste ';
                $counr_ser=$item->counr_ser;
            }else{
                $emji=' emoji_feliz ';
                $counr_ser='';
            }
            $html.='<tr><td>'.$item->nombre.' '.$item->apellido_paterno.'</td><td >'.$counr_ser.'</td><td class="'.$emji.'"></td></tr>';
        }
        $html.='</table>';

        echo $html;
    }
    function obtenernotificacion(){
        $params = $this->input->post();
        $tec = $params['tec'];
        /*
        $status_tec=' b-btn-secondary ';
        
        $res_tec = $this->ModeloServicio->view_estatus_tecnicos(date('Y-m-d'),0);
        $viewcont=$res_tec->num_rows();
        if($viewcont>0){
          $status_tec=' b-btn-danger ';
          
        }
        */
        $emji=' emoji_feliz ';


        $res_tec = $this->ModeloServicio->view_estatus_tecnicos(date('Y-m-d'),1);
        $viewcont=0;
        foreach ($res_tec->result() as $item) {
            $viewcont = $item->counr_ser;
            if($item->personalId==$tec){
                if($item->counr_ser>0){
                    $emji=' emoji_triste ';
                    $counr_ser=$item->counr_ser;
                }else{
                    $emji=' emoji_feliz ';
                    $counr_ser='';
                }
            }
            
            
        }
        echo '<button class="btn-floating  '.$emji.' viewstatustec"  title="Estatus tecnicos" onclick="viewstatustec()" data-count="'.$viewcont.'" style="margin-left: 5px; margin-right: 5px;"></button>';
    }
    function prefacturas($tecnico,$fecha,$views){
        $data['result1'] = $this->ModeloAsignacion->getlistadocalentariotipo($fecha,$fecha,0,$tecnico,0,0,$this->perfilid,$this->idpersonal,1,0,1);
        $data['result2'] = $this->ModeloAsignacion->getlistadocalentariotipo($fecha,$fecha,0,$tecnico,0,0,$this->perfilid,$this->idpersonal,2,0,1);
        $data['result3'] = $this->ModeloAsignacion->getlistadocalentariotipo($fecha,$fecha,0,$tecnico,0,0,$this->perfilid,$this->idpersonal,3,0,1);
        $data['result4'] = $this->ModeloAsignacion->getlistadocalentariotipo($fecha,$fecha,0,$tecnico,0,0,$this->perfilid,$this->idpersonal,4,0,1);
        $data['views']=$views;
        
        $this->load->view('asignacionestecnico',$data);
    }
    function verificarretiroequipos($idsere,$tipo){
        if ($tipo==3) {
            $idequipo=0;
            $serieid=0;

            $serviciosresult=$this->ModeloCatalogos->getselectwheren('asignacion_ser_cliente_a_d',array('asignacionIdd'=>$idsere,'tpoliza'=>22));//tpoliza 22 es retiro
            foreach ($serviciosresult->result() as $item) {
                $idequipo=$item->rowequipo;
                $serieid=$item->serieId;
            }
            if($idequipo>0 && $serieid>0){
                $this->retirarequipo($idequipo,$serieid,0);
            }
        }
    }
    function retirarequipo($idequipo,$serieid,$tipo){
        //============================================
            $resultserie=$this->ModeloCatalogos->getselectwheren('asignacion_series_r_equipos',array('id_equipo'=>$idequipo,'serieId'=>$serieid,'activo'=>1));
              $poreliminar=0;
              foreach ($resultserie->result() as $items) {
                $poreliminar=$items->poreliminar_tec;
              }
              if($tipo==1){
                $poreliminar=1;
              }
              //============================
                    /*
                $strq="SELECT asca.equipostext,ascae.modelo,ascae.serie,asca.fecha,ascae.asignacionIdd,ascae.status,ascae.rowequipo,ascae.serieId,per.nombre,per.apellido_paterno,per.apellido_materno
                    FROM asignacion_ser_cliente_a as asca
                    INNER JOIN asignacion_ser_cliente_a_d as ascae ON ascae.asignacionId=asca.asignacionId
                    INNER JOIN personal as per on per.personalId=asca.tecnico
                    WHERE ascae.tpoliza=22 AND ascae.tservicio_a=1067 AND ascae.activo=1 AND ascae.nr=0 AND ascae.rowequipo='$idequipo' AND ascae.serieId='$serieid'
                    ORDER BY `ascae`.`asignacionIdd` DESC LIMIT 1";
                    $result_ser = $this->db->query($strq);
                    foreach ($result_ser->result() as $itemser) {
                      if($itemser->status==2){

                      }else{
                        $poreliminar=0;
                      }
                    }
                    */
              //============================
        if($poreliminar==1){
            $equipo=$idequipo;
            $serie=$serieid;
            $this->ModeloCatalogos->updateCatalogo('series_productos',array('status'=>0,'bodegaId'=>2,'usado'=>1),array('serieId'=>$serie));
            $this->Configuraciones_model->registrarmovimientosseries($serie,1);
            //$this->ModeloCatalogos->getdeletewheren('asignacion_series_r_equipos',array('id_equipo'=>$equipo,'serieId'=>$serie));
            $this->ModeloCatalogos->updateCatalogo('asignacion_series_r_equipos',array('activo'=>0),array('id_equipo'=>$equipo,'serieId'=>$serie));
            $this->ModeloCatalogos->updatestock('rentas_has_detallesEquipos','cantidad','-',1,'id',$equipo);
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_contrato_a_e',array('status'=>0,'motivosuspencion'=>'Eliminacion y/o Cambio'),array('serieId'=>$serie,'idequipo'=>$equipo,'fecha >='=>$this->fechahoy));
            //==========================================================
                $resultac=$this->ModeloCatalogos->db13_getselectwheren('rentas_has_detallesEquipos_accesorios',array('rowequipo'=>$equipo));
                foreach ($resultac->result() as $itemac) {
                    $id_accesoriod=$itemac->id_accesoriod;//idrow
                    $id_accesorio=$itemac->id_accesorio;//modelo
                    $resultac2=$this->ModeloCatalogos->db13_getselectwheren('asignacion_series_r_accesorios',array('id_accesorio'=>$id_accesoriod));
                    foreach ($resultac2->result() as $itemara) {
                        //$itemara->serieId;
                        $this->ModeloCatalogos->updateCatalogo('series_accesorios',array('status'=>0),array('serieId'=>$itemara->serieId));
                        $this->Configuraciones_model->registrarmovimientosseries($itemara->serieId,2);
                        $this->ModeloCatalogos->Insert('bitacora_movimientos',array('entrada_salida'=>1,'descripcion_evento'=>'Entrada del accesorio por retiro por ultimo contador','personal'=>$this->idpersonal,'tipo'=>2,'id_item'=>$id_accesoriod,'modeloId'=>$id_accesorio,'modelo'=>'','serieId'=>$itemara->serieId,'bodega_d'=>0,'clienteId'=>0));
                    }
                }
                //$this->ModeloCatalogos->getdeletewheren('rentas_has_detallesEquipos_accesorios',array('rowequipo'=>$equipo)); se deja comentado y se vera si se ocupa
                //$this->ModeloCatalogos->getdeletewheren('rentas_has_detallesequipos_consumible',array('rowequipo'=>$equipo)); se deja comentado y se vera si se ocupa

            //==========================================================
                $resultve=$this->ModeloCatalogos->db13_getselectwheren('rentas_has_detallesEquipos',array('id'=>$equipo));
                foreach ($resultve->result() as $itemv) {
                    $idmodelo=$itemv->idEquipo;
                    $modeloeq=$itemv->modelo;
                    if($itemv->cantidad<=0){
                        $this->ModeloCatalogos->updateCatalogo('rentas_has_detallesEquipos',array('estatus'=>0),array('id'=>$equipo));
                    }
                }
            //==========================================================
            $this->ModeloCatalogos->Insert('bitacora_movimientos',array('entrada_salida'=>1,'descripcion_evento'=>'Entrada del equipo por retiro equipo:'.$idequipo.' serie:'.$serieid.' por ultimo contador','personal'=>$this->idpersonal,'tipo'=>1,'id_item'=>$equipo,'modeloId'=>$idmodelo,'modelo'=>$modeloeq,'serieId'=>$serieid,'bodega_d'=>2,'clienteId'=>0));
        }
    }
    function regresarequipoporrechazo($asig,$asigdll,$tipo){
        $where_asig='';
        if($asig>0){
            $where_asig=" asige.asignacionId='$asig' and ";
        }
        if($asigdll>0){
            $where_asig=" asige.asignacionIde='$asigdll' and ";
        }
        $strq="SELECT asige.*,asig.tpoliza
            FROM asignacion_ser_contrato_a_e as asige
            INNER JOIN asignacion_ser_contrato_a as asig on asig.asignacionId=asige.asignacionId
            WHERE $where_asig asig.tpoliza=17";
        $query = $this->db->query($strq);
        foreach ($query->result() as $itemser) {
            $idequipo=$itemser->idequipo;
            $serieid=$itemser->serieId;
            if($idequipo>0 && $serieid>0){
                //$this->retirarequipo($idequipo,$serieid,1); se quito por que ya no se tiene que quitar por este medio
            }
        }
    }
    function amonestacion(){
        $params = $this->input->post();
        $hora_inicio=$params['hora'];
        $serinfo=$params['serinfo'];
        $hora_inicio = date('H:i:s',strtotime($this->tolerancia2, strtotime($hora_inicio)));
        $resul=$this->ModeloCatalogos->getselectwheren('asignacion_ser_amonestacion',array('idpersonal'=>$params['tec'],'asig'=>$params['serv'],'tipo'=>$params['tipo']));
        if($resul->num_rows()==0){
            if($serinfo!=''){
                $this->ModeloCatalogos->Insert('asignacion_ser_amonestacion',array('idpersonal'=>$params['tec'],'asig'=>$params['serv'],'tipo'=>$params['tipo'],'hora'=>$hora_inicio,'sername'=>$params['sername'],'sercli'=>$params['sercli'],'serinfo'=>$serinfo));  
            }
              
        }
        
    }
    function obtener_servicio_siguiente(){
        $params = $this->input->post();
        $tec=$params['tec'];
        $fecha=$params['fecha'];
        $result=$this->ModeloGeneral->consultar_servicos_itinerario($tec,$fecha);
        $ser='0';
        $hora='';
        $sertipo='';
        $array=array();
        $row=1;
        $tipag=0;
        foreach ($result->result() as $item) {
            $cadena =$item->tipag;
            if (strpos($cadena, '1') !== false) {
                $tipag=1;
            }
            if($row<=2){
                $ser=$item->sers;
                $hora=$item->hora_inicio;
                $array[] = array(
                    'ser'=>$ser,
                    'hora'=>$hora,
                    'tipag'=>$tipag,
                    'clihrs'=>$item->cliente_hora,
                    'cli'=>$item->cli
                );
            }

            $row++;
        }
        
        echo json_encode($array);
    }
    function verificar_si_desfase_y_ultimo_ser(){
        $params = $this->input->post();
        $fecha = $params['fecha'];
        $tec = $params['tec'];

        $hora_actual = new DateTime();
        $hora_actual->modify('-15 minutes');//15 minutos de tolerancio, verificar si la hora actual es la correcta si no colocarle 75

        $ser_desfaces=array();
        $noalcanza = array();

        $result=$this->ModeloGeneral->consultar_servicos_itinerario_2($tec,$fecha,1);
        $rowtr=0;
        $rowtr_result=$result->num_rows();
        foreach ($result->result() as $item) {
            $rowtr++;
            $html_ser='';$view_tr=1;
            $empresa='';$hr_disp=''; $hora_ini=''; $hora_fin='';$horaserinicio='';$horaserfin='';$btn_desfase=''; $btn_des=0;$btn_ubi='';$servicio_name='';
            if($item->tiposer==1){
                $resultado=$this->ModeloAsignacion->getdatosrenta($item->asignacionId,$fecha,0,0);
                $row_co=0;if($resultado->num_rows()==0){$view_tr=0;}
                $statusservicio=0;
                $idCliente=0;$amo_ultimo=0;$ser_desface=0;
                foreach ($resultado->result() as $item1) {
                    $idCliente=$item1->idCliente;
                    if($item1->status>0){
                        $statusservicio=1;
                    }

                    $empresa=$item1->empresa;
                    $hr_disp=$item1->horario_disponible;
                    $servicio_name = $item1->servicio;
                    if($item1->tservicio_a>0){
                        $servicio_name = $item1->poliza;
                    }

                    //=========================================
                        //log_message('error',json_encode($item1));
                        $hora_comparar = new DateTime($item->hora_inicio);
                        //log_message('error','horaserinicio:'.$item1->horaserinicio.' horaverificar:'.json_encode($hora_comparar).' < horaactual:'.json_encode($hora_actual));
                        if ($hora_comparar < $hora_actual) {
                            $ser_desface=1;
                            //if($row_co!=$item1->asignacionId){
                                //$ser_desfaces[]=array('cli'=>$empresa,'ser'=>$servicio_name,'hrs'=>$item->hora_inicio,'tiposer'=>1,'serid'=>$item1->asignacionId,'seride'=>$item1->asignacionIde);
                             //   $row_co=$item1->asignacionId;
                            //}
                            
                        }
                        //===================================================
                        if($rowtr==$rowtr_result){
                            if($item1->status==0){
                                $hora_finser = new DateTime($item1->horafin);
                                $hora_finser->modify('-60 minutes');
                                if ($hora_finser < $hora_actual) {
                                    //if($row_co!=$item1->asignacionId){
                                        //$noalcanza[]=array('cli'=>$empresa,'ser'=>$servicio_name,'hrs'=>$item->hora_inicio,'tiposer'=>1,'asignacionId'=>$item->asignacionId);
                                        $amo_ultimo=1;
                                        //$row_co=$item1->asignacionId;
                                    //}
                                }
                            }
                        }
                    //=========================================
                    
                }
                if($statusservicio==0 && $idCliente>0){
                    if($ser_desface==1){
                        $ser_desfaces[]=array('cli'=>$empresa,'ser'=>$servicio_name,'hrs'=>$item->hora_inicio,'tiposer'=>1,'serid'=>$item->asignacionId);
                    }

                    if($amo_ultimo==1){
                        $noalcanza[]=array('cli'=>$empresa,'ser'=>$servicio_name,'hrs'=>$item->hora_inicio,'tiposer'=>1,'asignacionId'=>$item->asignacionId);
                    }
                }
            }
            if($item->tiposer==2){
                $resultado=$this->ModeloAsignacion->getdatospoliza($item->asignacionId,0,0);
                $row_po=0;if($resultado->num_rows()==0){$view_tr=0;}
                foreach ($resultado->result() as $item1) {
                    $empresa=$item1->empresa;
                    $hr_disp=$item1->horario_disponible;
                    $servicio_name = $item1->servicio;
                    if($row_po==0){
                        $horaserinicio=$item1->horaserinicio;
                    }
                    //=========================================
                        $hora_comparar = new DateTime($item->hora_inicio);
                        if ($hora_comparar < $hora_actual) {
                            $ser_desfaces[]=array('cli'=>$empresa,'ser'=>$servicio_name,'hrs'=>$item->hora_inicio,'tiposer'=>2);
                        }
                        //===================================================
                        if($rowtr==$rowtr_result){
                            if($item1->status==0){
                                $hora_finser = new DateTime($item1->horafin);
                                $hora_finser->modify('-60 minutes');
                                if ($hora_finser < $hora_actual) {
                                    $noalcanza[]=array('cli'=>$empresa,'ser'=>$servicio_name,'hrs'=>$item->hora_inicio,'tiposer'=>2,'asignacionId'=>$item->asignacionId);
                                }
                            }
                        }
                    //=========================================
                }
            }
            if($item->tiposer==3){
                $resultado=$this->ModeloAsignacion->getdatoscliente($item->asignacionId);
                $row_cli=0;if($resultado->num_rows()==0){$view_tr=0;}
                foreach ($resultado->result() as $item1) {
                    if($item1->status==0){
                        $empresa=$item1->empresa;
                        $hr_disp=$item1->horario_disponible;
                        $servicio_name = $item1->servicio;
                        if($item1->tservicio_a>0){
                            $servicio_name = $item1->poliza;
                        }
                        if($row_cli==0){
                            $horaserinicio=$item1->horaserinicio;
                        }
                        //=========================================
                            $hora_comparar = new DateTime($item->hora_inicio);
                            if ($hora_comparar < $hora_actual) {
                                $ser_desfaces[]=array('cli'=>$empresa,'ser'=>$servicio_name,'hrs'=>$item->hora_inicio,'tiposer'=>3,'asignacionId'=>$item->asignacionId);
                            }
                            //===================================================
                            if($rowtr==$rowtr_result){
                                if($item1->status==0){
                                    $hora_finser = new DateTime($item1->horafin);
                                    $hora_finser->modify('-60 minutes');
                                    if ($hora_finser < $hora_actual) {
                                        $noalcanza[]=array('cli'=>$empresa,'ser'=>$servicio_name,'hrs'=>$item->hora_inicio,'tiposer'=>3,'asignacionId'=>$item->asignacionId);
                                    }
                                }
                            }
                        //=========================================
                    }
                }
            }
            if($item->tiposer==4){
                $resultado=$this->ModeloAsignacion->getdatosventas($item->asignacionId);
                if($resultado->num_rows()==0){$view_tr=0;}
                foreach ($resultado->result() as $item1) {
                    $empresa=$item1->empresa;
                    $hr_disp=$item1->horario_disponible;
                    $horaserinicio=$item1->horaserinicio;
                    $horaserfin=$item1->horaserfin;
                    $servicio_name = $item1->servicio;
                    //=========================================
                        $hora_comparar = new DateTime($item->hora_inicio);
                        if ($hora_comparar < $hora_actual) {
                            $ser_desfaces[]=array('cli'=>$empresa,'ser'=>$servicio_name,'hrs'=>$item->hora_inicio,'tiposer'=>4);
                        }
                        //===================================================
                        if($rowtr==$rowtr_result){
                            if($item1->status==0){
                                $hora_finser = new DateTime($item1->horafin);
                                $hora_finser->modify('-60 minutes');
                                if ($hora_finser < $hora_actual) {
                                    $noalcanza[]=array('cli'=>$empresa,'ser'=>$servicio_name,'hrs'=>$item->hora_inicio,'tiposer'=>4,'asignacionId'=>$item->asignacionId);
                                }
                            }
                        }
                    //=========================================
                }
            }
        }

        $arrays_result = array(
            'ser_desfaces'=>$ser_desfaces,
            'noalcanza'=>$noalcanza,
            'horaactual'=>$hora_actual
            );
        echo json_encode($arrays_result);
    }
    function optencioninfoservicios($fecha,$tecnico,$viewi=0){
        /*
        $params = $this->input->post();
        $fecha = $params['fecha'];
        $tecnico = $params['tecnico'];
        */
        //$tecf=1;
        $result1 = $this->ModeloAsignacion->getlistadocalentariotipo($fecha,$fecha,0,$tecnico,0,0,$this->perfilid,$tecnico,1,0,1);
        $result2 = $this->ModeloAsignacion->getlistadocalentariotipo($fecha,$fecha,0,$tecnico,0,0,$this->perfilid,$tecnico,2,0,1);
        $result3 = $this->ModeloAsignacion->getlistadocalentariotipo($fecha,$fecha,0,$tecnico,0,0,$this->perfilid,$tecnico,3,0,1);
        $result4 = $this->ModeloAsignacion->getlistadocalentariotipo($fecha,$fecha,0,$tecnico,0,0,$this->perfilid,$tecnico,4,0,1);

        $html='';
        foreach ($result1->result() as $item0) {//contrato
            $asignacionId = $item0->asignacionId;
            $equipos='';$htmlservicio=''; $detalleservicio='';$comentario_tec='';
            $qrecibio='';
            $html.='<table class="table bordered">';
                //=============================================================================
                    $resultado=$this->ModeloAsignacion->getdatosrenta($asignacionId,'',0,0);
                    $row_tr_r=0;$status_2=1;
                    foreach ($resultado->result() as $item) {
                        $qrecibio=$item->qrecibio;
                        $v_ufp=$this->Rentas_model->verificar_ultimifolios_periodo($item->id_equipo);
                        if($v_ufp['consu_alert']==1){
                            $equipos_no_consu.='<div class="ocultar_tec" style="color:red">El Modelo:'.$item->modelo.' Serie:'.$item->serie.' tiene '.$v_ufp['consu_cant'].' consumibles por resurtir</div>';
                        }
                        $comentario_tec=$item->comentarioasge;
                        $pin=$item->pass;
                        $contacto=$item->contacto.' '.$item->telefono;
                        if($item->contacto_text!=null){
                            $contacto=$item->contacto_text;
                        }
                        if($item->idpexs>0){
                            $gar='<span class="gar"> Garantia</span>';
                        }else{
                            $gar='';
                        }
                        if($item->status<2 && $item->nr==0){
                            $status_2=0;
                        }
                        $idCliente=$item->idCliente;
                        $empresa = $item->empresa;
                        $servicio = $item->servicio.$gar;
                        $zona = $item->zona;
                        $tecnico = $item->tecnico.', '.$item->tecnico2;
                        if($item->per_fin>0){
                            $tecnico = $item->tecnicofin;
                        }
                        $prioridad = $item->prioridad;
                        $tiposervicio = $item->tiposervicio;
                        $fecha = $item->fecha;//log_message('error','$item->id_equipo: '.$item->id_equipo.' $item->serieId: '.$item->serieId);
                        $direccionequipo= $this->ModeloCatalogos->getdireccionequipos($item->id_equipo,$item->serieId,$fecha);
                        
                        if(intval($row_tr_r)==0){
                            //$hora = $item->hora.'<!--a '.$row_tr_r.'-->';
                            $hora = $item->hora;
                            if($item->horaserinicio!=''){
                                $hora_data_i =date('G',strtotime($item->horaserinicio));    
                            }else{
                                $hora_data_i=0;
                            }
                            
                            //$hora = $item->hora.'<!--b '.$row_tr_r.' '.$hora_data_i.'-->';
                            $hora = $item->hora;
                            if($hora_data_i>0){
                                //$hora = $item->horaserinicio.'<!--c '.$row_tr_r.' '.$hora_data_i.' '.$item->horaserinicio.'-->';
                                $hora = $item->horaserinicio;
                            }else{

                            }
                        }
                        $horafin = $item->horafin;
                        $hora_comida = $item->hora_comida;
                        $horafin_comida = $item->horafin_comida;
                        $comentariocal=$item->comentariocal;
                        $confirmado=$item->confirmado;
                        if($item->tservicio_a>0){
                            $servicio = $item->poliza.$gar;
                        }
                        $equipo_acceso=$item->servicio_equipo;//info contrato
                        $documentacion_acceso=$item->servicio_doc;//info contrato
                        $equipo_acceso=$item->equipo_acceso;//info clientes
                        $documentacion_acceso=$item->documentacion_acceso;//info clientes
                        //$protocolo_acceso=$item->protocolo_acceso;
                        $protocolo_acceso= $this->getdireccionequipos_pac($item->id_equipo,$item->serieId);
                        $detalleservicio=$item->tserviciomotivo;
                        if($item->equipo_acceso_info!=null){
                            $equipo_acceso=$item->equipo_acceso_info;//info clientes
                        }
                        if($item->documentacion_acceso_info!=null){
                            $documentacion_acceso=$item->documentacion_acceso_info;//info clientes
                        }
                        if($item->ubicacion!=''){
                            $ubicacion='<br>Ubicación: '.$item->ubicacion.'';
                        }else{
                            $ubicacion='';
                        }
                        if($item->contadores!=''){
                            $inf_conta=' ('.$item->contadores.')';
                        }else{
                            $inf_conta='';
                        }
                        if($item->nr==1){
                            $nr=' <i class="fas fa-times-circle fa-fw" title="Equipo Rechazado"></i>';
                        }else{
                            $nr='';
                        }
                        $r_sol_cot_r_icon='';
                        //$r_sol_cot_r=$this->ModeloCatalogos->db2_getselectwheren('asignacion_ser_contrato_a_e',array('idequipo'=>$item->id_equipo,'serieId'=>$item->serieId,'cot_status'=>1));
                        $r_sol_cot_r=$this->ModeloCatalogos->get_info_pendiente_solicitud_refaccion($item->id_equipo,$item->serieId,$item->serie);
                        if($viewi==0 && $r_sol_cot_r->num_rows() >0){
                            foreach ($r_sol_cot_r->result() as $item_as) {
                                /*
                                    1 contrato
                                    2 poliza
                                    3 evento
                                    4 venta
                                */
                                $r_sol_cot_r_icon.=' <i class="far fa-clipboard fa-fw notificacionsercot" title="Solicitud de refacción pendiente de cotizar" 
                                                data-asignacionide="'.$item_as->asignacionIde.'" data-asignacionid="'.$item_as->asignacionId.'"
                                                data-cotrefaccion="'.$item_as->cot_refaccion.'" data-cotrefaccion2="'.$item_as->cot_refaccion2.'"
                                                data-cotrefaccion3="'.$item_as->cot_refaccion3.'" data-tiposer="'.$item_as->tiposer.'"
                                                ></i>';
                            }
                            
                            
                        }
                        $estatuseq='';
                        if($viewi==0 && $item->status==1){
                            $estatuseq='<i class="fas fa-sync fa-spin"></i>';
                        }
                        if($viewi==0 && $item->status==2){
                            $estatuseq='<i class="fas fa-clipboard-check fa-2x colordeiconofinalizado"></i>';
                        }
                        $btn_info_cot=$this->info_ultima_cot($item->serieId,$item->serie,$viewi);
                        if($viewi==1){
                            $col_1='width="15%" style="font-size:11px;"';
                            $col_2='width="15%" style="font-size:11px;"';
                            $col_3='width="60%" style="font-size:9px;"';
                            $col_4='width="10%" style="font-size:9px;"';
                            $btn_info_cot_t='';
                            if($btn_info_cot!=''){
                                $btn_info_cot_t='<tr><td colspan="4">'.$btn_info_cot.'</td></tr>';
                            }
                            $btn_info_cot='';
                        }else{
                            $col_1='style="font-size:12px; padding-right: 6px;"';
                            $col_2='style="font-size:12px; padding-right: 6px;"';
                            $col_3='style="font-size:10px;"';
                            $col_4='style="font-size:10px;"';
                            $btn_info_cot_t='';
                        }
                        
                        $equipos .='<tr>';
                                        $equipos .='<td '.$col_1.'>'.$estatuseq.''.$item->modelo.'</td>';
                                        $equipos .='<td '.$col_2.'> '.$this->ModeloGeneral->ocultar_ult_dig(4,$item->serie).''.$btn_info_cot.$inf_conta.''.$nr.''.$r_sol_cot_r_icon.'</td>';
                                        $equipos .='<td '.$col_3.'>'.$direccionequipo.' '.$ubicacion.'</td>';

                                        $promedio1=$this->Rentas_model->produccionpromedio($item->id_equipo,$item->serieId,1,'','');
                                        $promedio2=$this->Rentas_model->produccionpromedio($item->id_equipo,$item->serieId,2,'','');
                                        $promedio=$promedio1+$promedio2;
                                        $equipos .='<td '.$col_4.'>'.$promedio.'</td>';
                        $equipos .='</tr>';
                        $equipos .=$btn_info_cot_t;
                            

                        //===============================================================================================
                            $class_eq=' equipo_'.$item->asignacionId.'_'.$item->asignacionIde.'_1 ';
                            if($item->cot_refaccion!=''){
                                $c_s_r_1=' b-btn-success';
                            }else{
                                $c_s_r_1=' b-btn-primary';
                            }
                            if($item->cot_refaccion2!=''){
                                $c_s_r_2=' b-btn-success';
                            }else{
                                $c_s_r_2=' b-btn-primary';
                            }
                            if($item->cot_refaccion3!=''){
                                $c_s_r_3=' b-btn-success';
                            }else{
                                $c_s_r_3=' b-btn-primary';
                            }
                            $btn_sol_ref ='<a class="b-btn btn-sm'.$c_s_r_1.''.$class_eq.'sol_ref_1" data-modelo="'.$item->modelo.'" data-serie="'.$item->serie.'" onclick="solicitudr('.$item->asignacionId.','.$item->asignacionIde.',1,1)">Desgaste</a>';
                            $btn_sol_ref.='<a class="b-btn btn-sm'.$c_s_r_2.''.$class_eq.'sol_ref_2" onclick="solicitudr('.$item->asignacionId.','.$item->asignacionIde.',1,2)">Daño</a>';
                            $btn_sol_ref.='<a class="b-btn btn-sm'.$c_s_r_3.''.$class_eq.'sol_ref_3" onclick="solicitudr('.$item->asignacionId.','.$item->asignacionIde.',1,3)">Garantia</a>';
                            if($viewi==1){
                                $btn_sol_ref='';    
                            }
                            if($qrecibio!='' && $qrecibio!='null' && $qrecibio!=null){
                                $btn_edit_qr='<a class="b-btn btn-sm b-btn-primary data_qrecivio_1_'.$asignacionId.'_'.$item->asignacionIde.'" data-nom="'.$qrecibio.'" onclick="editar_qrecibio(1,'.$asignacionId.','.$item->asignacionIde.')"><i class="fas fa-pencil-alt"></i></a>';
                                if($viewi==1){$btn_edit_qr='';}
                                $valido='<span class="qvalid">Validó el servicio: '.$qrecibio.$btn_edit_qr.'</span>';
                            }else{
                                $valido='';
                            }
                            
                            $equipos .='<tr><td style="font-size:10px;" colspan="4">'.$btn_sol_ref.$valido.'</td></tr>';
                        //===============================================================================================
                            
                        if($item->tpoliza==22){
                            if($item->comentario!='' and $item->comentario!='null'){
                                $equipos .='<tr><td colspan="2">'.$item->comentario.'</td><td></td></tr>';
                            }
                            
                            

                            $rentaequipos_accesorios = $this->ModeloCatalogos->rentaequipos_accesorios1($item->idRenta,$item->idEquipo,$item->id_equipo);
                            foreach ($rentaequipos_accesorios->result() as $itemacc) {
                                $equipos .='<tr style="font-size:12px;"><td>'.$itemacc->cantidad.' '.$itemacc->nombre.'</td><td>'.$itemacc->series.'</td><td></td></tr>';
                            }
                            //$idcontratofolio=0;
                            $arrayconsumibles=array();
                            $rentaequipos_consumible = $this->ModeloCatalogos->rentaequipos_consumible1($item->idRenta,$item->idEquipo,$item->id_equipo);
                            foreach ($rentaequipos_consumible->result() as $item0) {
                                $contrato_folio2 = $this->ModeloCatalogos->obtenerfoliosconsumiblesrenta3($item->idRenta,$item0->id_consumibles,$item0->id,$item->serieId); 
                                foreach ($contrato_folio2->result() as $itemx) {
                                    //if($idcontratofolio!=$itemx->idcontratofolio){
                                        $arrayconsumibles[$itemx->idcontratofolio]=array('modelo'=>$item0->modelo,'parte'=>$item0->parte,'folio'=>$itemx->foliotext);
                                        //$equipos .='<tr><td>'.$item0->modelo.' No° parte '.$item0->parte.'</td><td>'.$itemx->foliotext.'</td><td></td></tr>';
                                    //}  
                                } 
                            }
                            foreach ($arrayconsumibles as $itemu) {
                                $equipos .='<tr style="font-size:12px;"><td>'.$itemu['modelo'].' No° parte '.$itemu['parte'].'</td><td>'.$itemu['folio'].'</td><td></td></tr>';
                            }
                            
                        }
                        if($item->comentarioasge!='' and $item->comentarioasge!=null){
                                //$equipos .='<tr><td colspan="3">'.$item->comentarioasge.'</td><td></td></tr>';
                            }
                        
                        $row_tr_r++;
                        
                    }
                    foreach ($resultado->result() as $item) {
                        $datosconsumiblesfolio = $this->ModeloServicio->getfoliosservicios($asignacionId,$item->serieId);
                        
                        if ($datosconsumiblesfolio->num_rows()>0) {
                            $htmlservicio.=' <p> <b>Consumibles</b></p>';
                        
                            $htmlservicio.='<ul>';
                        
                            foreach ($datosconsumiblesfolio->result() as $item) {
                                    $htmlservicio.='<li><b>Modelo:</b> '.$item->modelo.' <b>parte:</b>'.$item->parte.' <b>Folio:</b>'.$item->foliotext.' <b>Bodega:</b> '.$item->bodega.'</li>';
                            }
                            $htmlservicio.='</ul>';
                        }
                    }
                    $datosres_equi = $this->ModeloCatalogos->obtenerproducventavinculada($asignacionId,1);
                    if($datosres_equi->num_rows()>0){
                        $htmlservicio.='<ul>';
                        foreach ($datosres_equi->result() as $itemre) {
                            $htmlservicio.='<li><b>Cant:</b> '.$itemre->cantidad.' <b>Modelo:</b> '.$itemre->modelo.' <b>Serie:</b>'.$itemre->serie.' <b>Bodega:</b> '.$itemre->bodega.'</li>';
                        }
                        $htmlservicio.='</ul>';
                    }
                //=============================================================================
                    if($viewi==0 && $status_2==1){
                        $btn_edit_hrs=' <a class="b-btn btn-sm b-btn-primary editar_hrs_ser_1_'.$asignacionId.'" data-ini="'.$hora.'" data-fin="'.$horafin.'" onclick="editar_hrs_ser(1,'.$asignacionId.')"><i class="fas fa-user-clock"></i></a>';
                    }else{
                        $btn_edit_hrs='';
                    }
                    $html.='<tr><th colspan="2" class="t_title" title="1-'.$asignacionId.'">'.$empresa.'</th></tr>';
                    $html.='<tr><td width="17%" class="b_l_b">Tipo de Servicio</td><td width="83%" class="b_l_b">'.$servicio.'</td></tr>';
                    $html.='<tr><td class="b_l_b">Fecha/Hora servicio</td><td class="b_l_b">'.$fecha.' '.$hora.' '.$horafin.''.$btn_edit_hrs.'</td></tr>';
                    $html.='<tr><td class="b_l_b">Falla</td><td class="b_l_b" style="font-size:12px">'.$detalleservicio.'</td></tr>';
                    if($comentario_tec!=''){
                        $html.='<tr><td class="b_l_b">Comentario</td><td class="b_l_b">'.$comentario_tec.'</td></tr>';
                    }
                    $html.='<tr><td colspan="2"></td></tr>';
                    $html.='<tr><td colspan="2">';
                        $html.='<table border="0" cellpadding="2">';
                        $html.=$equipos;
                        $html.='</table>';
                    $html.='</td></tr>';
                    $html.='<tr><td colspan="2">';
                        $html.=$htmlservicio;
                    $html.='</td></tr>';
            $html.='</table>';
            $html.='<table><tr><td></td></tr></table>';
        }
        foreach ($result2->result() as $item0) {//poliza
            $asignacionId = $item0->asignacionId;
            $equipos='';$htmlservicio='';$observaciones='';$comentario_tec='';$status_2=1;
            $html.='<table class="table bordered">';
            $qrecibio='';
            //=====================================
                $resultado=$this->ModeloAsignacion->getdatospoliza($asignacionId,0,0);
                foreach ($resultado->result() as $item) {
                    $qrecibio=$item->qrecibio;
                    if($item->status<2){
                        $status_2=0;
                    }
                    if($item->idpexs>0){
                        $gar='<span class="gar"> Garantia</span>';
                    }else{
                        $gar='';
                    }
                    $pin=$item->pass;
                    $contacto=$item->atencion.' '.$item->telefono;
                    if($item->contacto_text!=null){
                        $contacto=$item->contacto_text;
                    }
                    $idCliente=$item->idCliente;
                    $empresa = $item->empresa;
                    $servicio = $item->servicio.$gar;
                    $zona = $item->zona;
                    $tecnico = $item->tecnico.', '.$item->tecnico2;
                    if($item->per_fin>0){
                        $tecnico = $item->tecnicofin;
                    }
                    $prioridad = $item->prioridad;
                    $tiposervicio = $item->tiposervicio;
                    $fecha = $item->fecha;
                    $hora = $item->hora;
                    $horafin = $item->horafin;
                    $hora_comida = $item->hora_comida;
                    $horafin_comida = $item->horafin_comida;
                    $comentariocal=$item->comentariocal;
                    $confirmado=$item->confirmado;
                    $equipo_acceso=$item->equipo_acceso;//info clientes
                    $documentacion_acceso=$item->documentacion_acceso;//info clientes
                    //$protocolo_acceso=$item->protocolo_acceso;
                    $protocolo_acceso='';
                    if($item->iddir>0){
                        $result_dir = $this->ModeloCatalogos->getselectwheren('clientes_direccion',array('idclientedirecc'=>$item->iddir));
                        foreach ($result_dir->result() as $itempa) {
                            $protocolo_acceso=$itempa->pro_acc;
                        }
                    }
                    if($item->equipo_acceso_info!=null){
                        $equipo_acceso=$item->equipo_acceso_info;//info clientes
                    }
                    if($item->documentacion_acceso_info!=null){
                        $documentacion_acceso=$item->documentacion_acceso_info;//info clientes
                    }
                    if($item->nr==1){
                        $nr='<i class="fas fa-times-circle fa-fw"></i>';
                    }else{
                        $nr='';
                    }
                    $estatuseq='';
                    if($item->status==1){
                        $estatuseq='<i class="fas fa-sync fa-spin"></i>';
                    }
                    if($item->status==2){
                        $estatuseq='<i class="fas fa-clipboard-check fa-2x colordeiconofinalizado"></i>';
                    }
                    $equipoacceso='';
                    $r_sol_cot_r_icon='';
                    //$r_sol_cot_r=$this->ModeloCatalogos->db2_getselectwheren('asignacion_ser_contrato_a_e',array('idequipo'=>$item->id_equipo,'serieId'=>$item->serieId,'cot_status'=>1));
                    $r_sol_cot_r=$this->ModeloCatalogos->get_info_pendiente_solicitud_refaccion(0,0,$item->serie);
                    if($viewi==0 && $r_sol_cot_r->num_rows() >0){
                        foreach ($r_sol_cot_r->result() as $item_as) {
                            /*
                                1 contrato
                                2 poliza
                                3 evento
                                4 venta
                            */
                            $r_sol_cot_r_icon.='<i class="far fa-clipboard fa-fw notificacionsercot" title="Solicitud de refacción pendiente de cotizar" 
                                            data-asignacionide="'.$item_as->asignacionIde.'" data-asignacionid="'.$item_as->asignacionId.'"
                                            data-cotrefaccion="'.$item_as->cot_refaccion.'" data-cotrefaccion2="'.$item_as->cot_refaccion2.'"
                                            data-cotrefaccion3="'.$item_as->cot_refaccion3.'" data-tiposer="'.$item_as->tiposer.'"
                                            ></i>';
                        }
                        
                        
                    }
                    $observaciones=$item->observaciones;



                        $btn_info_cot=$this->info_ultima_cot(0,$item->serie,$viewi);
                        if($viewi==1){
                            $col_1='width="15%" style="font-size:11px;"';
                            $col_2='width="15%" style="font-size:11px;"';
                            $col_3='width="70%" style="font-size:10px;"';
                            //$col_4='width="10%" ';
                            $btn_info_cot_t='';
                            if($btn_info_cot!=''){
                                $btn_info_cot_t='<tr><td colspan="3">'.$btn_info_cot.'</td></tr>';
                            }
                            $btn_info_cot='';
                        }else{
                            $col_1='';
                            $col_2='';
                            $col_3='style="font-size:10px;"';
                            //$col_4='';
                            $btn_info_cot_t='';
                        }
                    $equipos .='<tr>';
                        $equipos .='<td '.$col_1.'>'.$estatuseq.''.$item->modelo.'</td>';
                        $equipos .='<td '.$col_2.'>'.$item->serie.$btn_info_cot.$nr.$r_sol_cot_r_icon.'</td>';
                        $equipos .='<td '.$col_3.'>'.$item->direccionservicio.'</td>';

                                $class_eq=' equipo_'.$item->asignacionId.'_'.$item->asignacionIde.'_2 ';
                                if($item->cot_refaccion!=''){
                                    $c_s_r_1=' b-btn-success ';
                                }else{
                                    $c_s_r_1=' b-btn-primary ';
                                }
                                if($item->cot_refaccion2!=''){
                                    $c_s_r_2=' b-btn-success ';
                                }else{
                                    $c_s_r_2=' b-btn-primary ';
                                }
                                if($item->cot_refaccion3!=''){
                                    $c_s_r_3=' b-btn-success ';
                                }else{
                                    $c_s_r_3=' b-btn-primary ';
                                }
                                $btn_sol_ref ='<a class="b-btn btn-sm'.$c_s_r_1.''.$class_eq.'sol_ref_1" data-modelo="'.$item->modelo.'" data-serie="'.$item->serie.'" onclick="solicitudr('.$item->asignacionId.','.$item->asignacionIde.',2,1)">Desgaste</a>';
                                $btn_sol_ref.='<a class="b-btn btn-sm'.$c_s_r_2.''.$class_eq.'sol_ref_2" onclick="solicitudr('.$item->asignacionId.','.$item->asignacionIde.',2,2)">Daño</a>';
                                $btn_sol_ref.='<a class="b-btn btn-sm'.$c_s_r_3.''.$class_eq.'sol_ref_3" onclick="solicitudr('.$item->asignacionId.','.$item->asignacionIde.',2,3)">Garantia</a>';
                                if($viewi==1){
                                    $btn_sol_ref='';    
                                }
                                
                    $equipos .='</tr>';
                    $equipos .=$btn_info_cot_t;
                    
                    $equipos .='<tr><td colspan="3">'.$item->comentario.' '.$item->comeninfo.'</td></tr>';
                    if($qrecibio!='' && $qrecibio!='null' && $qrecibio!=null){
                        $btn_edit_qr='<a class="b-btn btn-sm b-btn-primary data_qrecivio_2_'.$asignacionId.'_'.$item->asignacionIde.'" data-nom="'.$qrecibio.'" onclick="editar_qrecibio(2,'.$item->asignacionId.','.$item->asignacionIde.')"><i class="fas fa-pencil-alt"></i></a>';
                        if($viewi==1){$btn_edit_qr='';}
                        $valido='<span class="qvalid">Validó el servicio: '.$qrecibio.$btn_edit_qr.'</span>';
                    }else{
                        $valido='';
                    }
                    $equipos .='<tr><td style="font-size:10px;" colspan="4">'.$btn_sol_ref.$valido.'</td></tr>';

                    if($item->comentarioasge!='' and $item->comentarioasge!=null){
                                //$equipos .='<tr><td colspan="3">'.$item->comentarioasge.'</td><td></td></tr>';
                            }
                    $comentario_tec=$item->comentarioasge;
                    

                }
                $datosconsumiblesfolio = $this->ModeloServicio->getfoliosservicios2($asignacionId);
                
                if ($datosconsumiblesfolio->num_rows()>0) {
                    $htmlservicio.=' <p> <b> Consumibles</b></p>';
                
                    $htmlservicio.='<ul>';
                    foreach ($datosconsumiblesfolio->result() as $item) {
                            $htmlservicio.='<li> Modelo: '.$item->modelo.' parte:'.$item->parte.' Folio:'.$item->foliotext.'</li>';
                    }
                    $htmlservicio.='</ul>';
                }
                $datosres_equi = $this->ModeloCatalogos->obtenerproducventavinculada($asignacionId,2);
                if ($datosres_equi->num_rows()>0) {
                    $htmlservicio.='<ul>';
                    foreach ($datosres_equi->result() as $itemre) {
                        $htmlservicio.='<li>Cant: '.$itemre->cantidad.' Modelo: '.$itemre->modelo.' Serie:'.$itemre->serie.' </li>';
                    }
                    $htmlservicio.='</ul>';
                }
            //=====================================
                if($viewi==0 && $status_2==1){
                    $btn_edit_hrs=' <a class="b-btn btn-sm b-btn-primary editar_hrs_ser_2_'.$asignacionId.'" data-ini="'.$hora.'" data-fin="'.$horafin.'" onclick="editar_hrs_ser(2,'.$asignacionId.')"><i class="fas fa-user-clock"></i></a>';
                }else{
                    $btn_edit_hrs='';
                }
                $html.='<tr><th colspan="2" class="t_title" title="2-'.$asignacionId.'">'.$empresa.'</th></tr>';
                    $html.='<tr><td class="b_l_b" width="17%">Tipo de Servicio</td><td class="b_l_b" width="83%">'.$servicio.'</td></tr>';
                    $html.='<tr><td class="b_l_b">Fecha/Hora servicio</td><td class="b_l_b">'.$fecha.' '.$hora.' '.$horafin.''.$btn_edit_hrs.'</td></tr>';
                    $html.='<tr><td class="b_l_b">Falla</td><td class="b_l_b" style="font-size:12px">'.$observaciones.'</td></tr>';
                    if($comentario_tec!=''){
                        $html.='<tr><td class="b_l_b">Comentario</td><td>'.$comentario_tec.'</td></tr>';
                    }
                    $html.='<tr><td colspan="2"></td></tr>';
                    $html.='<tr><td colspan="2">';
                        $html.='<table border="0">';
                        $html.=$equipos;
                        $html.='</table>';
                    $html.='</td></tr>';
                    $html.='<tr><td colspan="2">';
                        $html.=$htmlservicio;
                    $html.='</td></tr>';
            $html.='</table>';
            $html.='<table><tr><td></td></tr></table>';
        }
        foreach ($result3->result() as $item0) {//evento
            $asignacionId = $item0->asignacionId;
            $equipos='';$htmlservicio='';$comentario_tec='';$status_2=1;
            $html.='<table class="table bordered">';
            $qrecibio='';
            //==============================================================
                $resultado=$this->ModeloAsignacion->getdatoscliente($asignacionId);
                foreach ($resultado->result() as $item) {
                    $qrecibio=$item->qrecibio2;
                    if($item->status<2){
                        $status_2=0;
                    }
                    $comentario_tec=$item->comentarioasge;
                    if($item->idpexs>0){
                        $gar='<span class="gar"> Garantia</span>';
                    }else{
                        $gar='';
                    }
                    $pin=$item->pass;
                    $idCliente=$item->clienteId;
                    if($item->contacto_text!=''){
                        $contacto=$item->contacto_text;
                    }
                    $equipoacceso='';
                    $empresa = $item->empresa;
                    $servicio = $item->servicio.$gar;
                    $zona = $item->zona;
                    $tecnico = $item->tecnico.', '.$item->tecnico2;
                    $prioridad = $item->prioridad;
                    $tiposervicio = $item->tiposervicio;
                    $fecha = $item->fecha;
                    $hora = $item->hora;
                    $horafin = $item->horafin;
                    $hora_comida = $item->hora_comida;
                    $horafin_comida = $item->horafin_comida;
                    if($item->nr==1){
                        $nr='<i class="fas fa-times-circle fa-fw"></i>';
                    }else{
                        $nr='';
                    }
                    $r_sol_cot_r_icon='';
                    //$r_sol_cot_r=$this->ModeloCatalogos->db2_getselectwheren('asignacion_ser_contrato_a_e',array('idequipo'=>$item->id_equipo,'serieId'=>$item->serieId,'cot_status'=>1));
                    $r_sol_cot_r=$this->ModeloCatalogos->get_info_pendiente_solicitud_refaccion(0,0,$item->serie);
                    if($viewi==0 && $r_sol_cot_r->num_rows() >0){
                        foreach ($r_sol_cot_r->result() as $item_as) {
                            /*
                                1 contrato
                                2 poliza
                                3 evento
                                4 venta
                            */
                            $r_sol_cot_r_icon.='<i class="far fa-clipboard fa-fw notificacionsercot" title="Solicitud de refacción pendiente de cotizar" 
                                            data-asignacionide="'.$item_as->asignacionIde.'" data-asignacionid="'.$item_as->asignacionId.'"
                                            data-cotrefaccion="'.$item_as->cot_refaccion.'" data-cotrefaccion2="'.$item_as->cot_refaccion2.'"
                                            data-cotrefaccion3="'.$item_as->cot_refaccion3.'" data-tiposer="'.$item_as->tiposer.'"
                                            ></i>';
                        }
                        
                        
                    }
                    if($item->comentario!=''){
                        $td_comen='<td style="font-size:10px;">'.$item->comentario.'</td>';
                    }else{
                        $td_comen='';
                    }
                    $estatuseq='';
                    if($item->status==1){
                        $estatuseq='<i class="fas fa-sync fa-spin"></i>';
                    }
                    if($item->status==2){
                        $estatuseq='<i class="fas fa-clipboard-check fa-2x colordeiconofinalizado"></i>';
                    }
                    if($item->retiro==1){
                        $modelo_eq=$item->modeloretiro;
                    }else{
                        $modelo_eq=$item->modelo;
                    }
                    $class_eq=' equipo_'.$item->asignacionId.'_'.$item->asignacionIdd.'_3 ';
                    if($item->cot_refaccion!=''){
                        $c_s_r_1=' b-btn-success ';
                    }else{
                        $c_s_r_1=' b-btn-primary ';
                    }
                    if($item->cot_refaccion2!=''){
                        $c_s_r_2=' b-btn-success ';
                    }else{
                        $c_s_r_2=' b-btn-primary ';
                    }
                    if($item->cot_refaccion3!=''){
                        $c_s_r_3=' b-btn-success ';
                    }else{
                        $c_s_r_3=' b-btn-primary ';
                    }
                    $btn_sol_ref ='<a class="b-btn btn-sm'.$c_s_r_1.''.$class_eq.'sol_ref_1" data-modelo="'.$modelo_eq.'" data-serie="'.$item->serie.'" onclick="solicitudr('.$item->asignacionId.','.$item->asignacionIdd.',3,1)">Desgaste</a>';
                    $btn_sol_ref.='<a class="b-btn btn-sm'.$c_s_r_2.''.$class_eq.'sol_ref_2" onclick="solicitudr('.$item->asignacionId.','.$item->asignacionIdd.',3,2)">Daño</a>';
                    $btn_sol_ref.='<a class="b-btn btn-sm'.$c_s_r_3.''.$class_eq.'sol_ref_3" onclick="solicitudr('.$item->asignacionId.','.$item->asignacionIdd.',3,3)">Garantia</a>';
                    if($viewi==1){
                        $btn_sol_ref='';    
                    }
                    //$btns_sol ='<td style="font-size:10px;">'.$btn_sol_ref.'</td>';
                    
                        $btn_info_cot=$this->info_ultima_cot(0,$item->serie,$viewi);
                        if($viewi==1){
                            $col_1='width="15%" style="font-size:11px;"';
                            $col_2='width="15%" style="font-size:11px;"';
                            $col_3='width="60%" style="font-size:10px;"';
                            //$col_4='width="10%" ';
                            $btn_info_cot_t='';
                            if($btn_info_cot!=''){
                                $btn_info_cot_t='<tr><td colspan="3">'.$btn_info_cot.'</td></tr>';
                            }
                            $btn_info_cot='';
                        }else{
                            $col_1='';
                            $col_2='';
                            $col_3='style="font-size:10px;"';
                            //$col_4='';
                            $btn_info_cot_t='';
                        }
                    $equipos .='<tr><td '.$col_1.'>'.$estatuseq.$modelo_eq.'</td><td '.$col_2.'>'.$item->serie.$btn_info_cot.$nr.$r_sol_cot_r_icon.'</td><td '.$col_3.' >'.$item->direccion.'</td>'.$td_comen.'</tr>';
                    $equipos .=$btn_info_cot_t;
                    
                    if($qrecibio!='' && $qrecibio!='null' && $qrecibio!=null){
                        $btn_edit_qr='<a class="b-btn btn-sm b-btn-primary data_qrecivio_3_'.$asignacionId.'_'.$item->asignacionIdd.'" data-nom="'.$qrecibio.'" onclick="editar_qrecibio(3,'.$item->asignacionId.','.$item->asignacionIdd.')"><i class="fas fa-pencil-alt"></i></a>';
                        if($viewi==1){$btn_edit_qr='';}
                        $valido='<span class="qvalid">Validó el servicio: '.$qrecibio.$btn_edit_qr.'</span>';
                    }else{
                        $valido='';
                    }
                    $equipos .='<tr><td colspan="4">'.$btn_sol_ref.$valido.'</td></tr>';
                    $tserviciomotivo = $item->tserviciomotivo;
                    $documentaccesorio = $item->documentaccesorio;
                    $comentariocal=$item->comentariocal;
                    $confirmado=$item->confirmado;
                    if($item->tservicio_a>0){
                        $servicio = $item->poliza;
                    }
                    $equipo_acceso=$item->equipo_acceso;//info clientes
                    $documentacion_acceso=$item->documentacion_acceso;//info clientes
                    //$protocolo_acceso=$item->protocolo_acceso;
                    $protocolo_acceso='';
                    if($item->iddir>0){
                        $result_dir = $this->ModeloCatalogos->getselectwheren('clientes_direccion',array('idclientedirecc'=>$item->iddir));
                        foreach ($result_dir->result() as $itempa) {
                            $protocolo_acceso=$itempa->pro_acc;
                        }
                    }
                    if($item->equipo_acceso_info!=null){
                        $equipo_acceso=$item->equipo_acceso_info;//info clientes
                    }
                    if($item->documentacion_acceso_info!=null){
                        $documentacion_acceso=$item->documentacion_acceso_info;//info clientes
                    }
                    $comentario_tec=$item->comentarioasge;
                    
                }
                $datosres_equi = $this->ModeloCatalogos->obtenerproducventavinculada($asignacionId,3);
                if ($datosres_equi->num_rows()>0) {
                    $htmlservicio.='<ul>';
                    foreach ($datosres_equi->result() as $itemre) {
                        $htmlservicio.='<li>Cant: '.$itemre->cantidad.' Modelo: '.$itemre->modelo.' Serie:'.$itemre->serie.' </li>';
                    }
                    $htmlservicio.='</ul>';
                }
            //==============================================================
                if($viewi==0 && $status_2==1){
                    $btn_edit_hrs=' <a class="b-btn btn-sm b-btn-primary editar_hrs_ser_3_'.$asignacionId.'" data-ini="'.$hora.'" data-fin="'.$horafin.'" onclick="editar_hrs_ser(3,'.$asignacionId.')"><i class="fas fa-user-clock"></i></a>';
                }else{
                    $btn_edit_hrs='';
                }
                $html.='<tr><th colspan="2" class="t_title" title="3-'.$asignacionId.'">'.$empresa.'</th></tr>';
                    $html.='<tr><td class="b_l_b" width="17%">Tipo de Servicio</td><td class="b_l_b" width="83%">'.$servicio.'</td></tr>';
                    $html.='<tr><td class="b_l_b">Fecha/Hora servicio</td><td class="b_l_b">'.$fecha.' '.$hora.' '.$horafin.''.$btn_edit_hrs.'</td></tr>';
                    $html.='<tr><td class="b_l_b">Falla</td><td class="b_l_b" style="font-size:12px">'.$tserviciomotivo.'</td></tr>';
                    if($comentario_tec!=''){
                        $html.='<tr><td class="b_l_b">Comentario</td><td>'.$comentario_tec.'</td></tr>';
                    }
                    $html.='<tr><td colspan="2"></td></tr>';
                    $html.='<tr><td colspan="2">';
                        $html.='<table>';
                        $html.=$equipos;
                        $html.='</table>';
                    $html.='</td></tr>';
                    $html.='<tr><td colspan="2">';
                        $html.=$htmlservicio;
                    $html.='</td></tr>';
            $html.='</table>';
            $html.='<table><tr><td></td></tr></table>';
        }
        foreach ($result4->result() as $item0) {//venta
            $asignacionId = $item0->asignacionId;
            $equipos='';$htmlservicio='';$observaciones='';$comentario_tec='';$status_2=1;
            $html.='<table class="table bordered">';
            $qrecibio='';
            //===============================================
                $resultado=$this->ModeloAsignacion->getdatosventas($asignacionId);
                foreach ($resultado->result() as $item) {
                    $qrecibio=$item->qrecibio;
                    if($item->status<2){
                        $status_2=0;
                    }
                    $comentario_tec=$item->comentario;
                    if($item->idpexs>0){
                        $gar='<span class="gar"> Garantia</span>';
                    }else{
                        $gar='';
                    }
                    $idCliente=$item->clienteId;
                    $equipoacceso='';
                    $tipov = $item->tipov;
                    $empresa = $item->empresa;
                    $servicio = $item->servicio.$gar;
                    $zona = $item->zona;
                    $tecnico = $item->tecnico;
                    if($item->per_fin>0){
                        $tecnico = $item->tecnicofin;
                    }
                    $prioridad = $item->prioridad;
                    $tiposervicio = $item->tiposervicio;
                    $fecha = $item->fecha;
                    $hora = $item->hora;
                    $horafin = $item->horafin;
                    $hora_comida = $item->hora_comida;
                    $horafin_comida = $item->horafin_comida;
                    $direccion = $item->direccion;
                    $ventaId = $item->ventaId;
                    $ventaIdd = $item->ventaIdd;
                    if($item->nr==1){
                        $nr='<i class="fas fa-times-circle fa-fw"></i>';
                    }else{
                        $nr='';
                    }
                    //$equipos .='<tr><td colspan="2">'.$item->serie.'</td><td style="font-size:10px;">'.$item->direccion.'</td></tr>';
                    $tserviciomotivo = $item->tserviciomotivo;
                    $documentaccesorio = $item->documentaccesorio;
                    $comentariocal=$item->comentariocal;
                    $confirmado=$item->confirmado;
                    $contacto=$item->atencionpara;
                    if($item->contacto_text!=null){
                        $contacto=$item->contacto_text;
                    }
                    
                    if($item->tservicio_a>0){
                        $servicio = $item->poliza.$gar;
                    }
                    $equipo_acceso=$item->equipo_acceso;//info clientes
                    $documentacion_acceso=$item->documentacion_acceso;//info clientes
                    if($item->equipo_acceso_info!=null){
                        $equipo_acceso=$item->equipo_acceso_info;//info clientes
                    }
                    if($item->documentacion_acceso_info!=null){
                        $documentacion_acceso=$item->documentacion_acceso_info;//info clientes
                    }
                    $estatuseq='';
                    if($viewi==0 && $item->status==1){
                        $estatuseq='<i class="fas fa-sync fa-spin"></i>';
                    }
                    if($viewi==0 && $item->status==2){
                        $estatuseq='<i class="fas fa-clipboard-check fa-2x colordeiconofinalizado"></i>';
                    }
                    $equipoacceso.='<div class="row">
                                        <div class="col s4 m5 6 textcolorred ">
                                            Equipo acceso
                                        </div>
                                        <div class="col s8 m7 6 ">
                                            '.$equipo_acceso.'
                                        </div>
                                    </div>';
                    $equipoacceso.='<div class="row">
                                        <div class="col s4 m5 6 textcolorred ">
                                            Documentacion acceso
                                        </div>
                                        <div class="col s8 m7 6 ">
                                            '.$documentacion_acceso.'
                                        </div>
                                    </div>';
                    $observaciones=$item->observaciones;
                    $equipoacceso.=' <div class="row">
                                        <div class="col s4 m5 6 textcolorred v_'.$ventaIdd.'">
                                            Observaciones
                                        </div>
                                        <div class="col s8 m7 6 ">
                                            '.$item->observaciones.'
                                        </div>
                                    </div>';
                    if($hora_comida=='00:00:00'){

                    }else{
                    $equipoacceso.='<div class="row">
                                        <div class="col s4 m5 6 textcolorred ">
                                            Comida
                                        </div>
                                        <div class="col s8 m7 6 ">
                                            '.$hora_comida.' '.$horafin_comida.'
                                        </div>
                                    </div>';
                    }
                    $equipoacceso.='<div class="row">
                                    <div class="col s12 m5 6 textcolorred">
                                        
                                      </div>
                                      <div class="col s12 m7 6">
                                        <a class="btn-floating green tooltipped" data-position="top" data-delay="50" data-tooltip="Prefactura" onclick="detallev('.$item->ventaId.','.$tipov.')"><i class="material-icons">assignment</i></a>
                                      </div>
                                    </div>
                                    ';
                    

                }
                $class_eq=' equipo_'.$asignacionId.'_0_4 ';
                if($item->cot_refaccion!=''){
                    $c_s_r_1=' b-btn-success ';
                }else{
                    $c_s_r_1=' b-btn-primary ';
                }
                if($item->cot_refaccion2!=''){
                    $c_s_r_2=' b-btn-success ';
                }else{
                    $c_s_r_2=' b-btn-primary ';
                }
                if($item->cot_refaccion3!=''){
                    $c_s_r_3=' b-btn-success ';
                }else{
                    $c_s_r_3=' b-btn-primary ';
                }
                $btn_sol_ref ='<a class="b-btn btn-sm'.$c_s_r_1.''.$class_eq.'sol_ref_1" data-modelo="" data-serie="'.$item->serie.'" onclick="solicitudr('.$asignacionId.',0,4,1)">Desgaste</a>';
                $btn_sol_ref.='<a class="b-btn btn-sm'.$c_s_r_2.''.$class_eq.'sol_ref_2" onclick="solicitudr('.$asignacionId.',0,4,2)">Daño</a>';
                $btn_sol_ref.='<a class="b-btn btn-sm'.$c_s_r_3.''.$class_eq.'sol_ref_3" onclick="solicitudr('.$asignacionId.',0,4,3)">Garantia</a>';
                if($viewi==1){
                    $btn_sol_ref='';    
                }
                if($qrecibio!='' && $qrecibio!='null' && $qrecibio!=null){
                    $btn_edit_qr='<a class="b-btn btn-sm b-btn-primary data_qrecivio_4_'.$asignacionId.'_0" data-nom="'.$qrecibio.'" onclick="editar_qrecibio(4,'.$asignacionId.',0)"><i class="fas fa-pencil-alt"></i></a>';
                    if($viewi==1){$btn_edit_qr='';}
                    $valido='<span class="qvalid">Validó el servicio: '.$qrecibio.$btn_edit_qr.'</span>';
                }else{
                    $valido='';
                }
                $equipos_sr='<tr><td style="font-size:10px;" colspan="4">'.$btn_sol_ref.$valido.'</td></tr>';

                if ($tipov==1) {
                    $reesequi = $this->ModeloCatalogos->db13_getselectwheren('ventacombinada',array('combinadaId'=>$ventaIdd));
                    foreach ($reesequi->result() as $item) {
                        $ventaIdd=$item->equipo;
                    }
                }
                $resultadoequipos = $this->ModeloCatalogos->db13_getselectwheren('ventas_has_detallesEquipos',array('idVenta'=>$ventaIdd));
                $resultadoaccesorios = $this->ModeloCatalogos->accesoriosventa($ventaIdd);
                $resultadoconsumibles = $this->ModeloCatalogos->consumiblesventa($ventaIdd);
                $consumiblesventadetalles = $this->ModeloCatalogos->consumiblesventadetalles($ventaIdd);
                $polizaventadetalles = $this->ModeloCatalogos->polizaventadetalles($ventaIdd);
                $ventadetallesrefacion = $this->ModeloCatalogos->ventas_has_detallesRefacciones($ventaIdd);

                foreach ($resultadoequipos->result() as $item) { 
                    $model=$this->ModeloCatalogos->obtenernumeroserieequipo($item->idEquipo);
                    $resuleserie=$this->ModeloCatalogos->getequiposerie($item->id);

                    foreach ($resuleserie->result() as $itemse) {
                        $btn_info_cot=$this->info_ultima_cot(0,$itemse->serie,$viewi);
                        if($viewi==1){
                            $col_1='width="15%" style="font-size:11px;"';
                            $col_2='width="15%" style="font-size:11px;"';
                            $col_3='width="10%" style="font-size:11px;"';
                            $col_4='width="60%" style="font-size:10px;"';
                            $btn_info_cot_t='';
                            if($btn_info_cot!=''){
                                $btn_info_cot_t='<tr><td colspan="4">'.$btn_info_cot.'</td></tr>';
                            }
                            $btn_info_cot='';
                        }else{
                            $col_1='';$col_2='';$col_3='';
                            $col_4='style="font-size:10px;"';
                            $btn_info_cot_t='';
                        }
                        $equipos .='<tr><td '.$col_1.'>'.$estatuseq.''.$item->modelo.'</td><td '.$col_2.'>'.$itemse->serie.$btn_info_cot.$nr.'</td><td '.$col_3.'>'.$itemse->bodega.'</td><td '.$col_4.'>'.$direccion.'</td>';
                        $equipos .=$btn_info_cot_t;
                        
                        

                        $equipos .='</tr>';
                    }
                    
                }
                foreach ($resultadoaccesorios->result() as $item) { 
                    $resuleserie=$this->ModeloCatalogos->getaccesorioserie($item->id_accesoriod);
                    foreach ($resuleserie->result() as $itemse) { 
                        $equipos .='<tr><td>'.$estatuseq.''.$item->nombre.'</td><td>'.$itemse->serie.'</td><td>'.$item->bodega.'</td><td style="font-size:10px;">'.$direccion.'</td></tr>';
                    }
                }
                foreach ($resultadoconsumibles->result() as $item) { 
                    $equipos .='<tr><td>'.$estatuseq.''.$item->modelo.'</td><td></td><td>'.$item->bodega.'</td><td style="font-size:10px;">'.$direccion.'</td></tr>';
                }

                if($viewi==1){
                    $col_1='width="15%" style="font-size:11px;"';
                    $col_2='width="15%" style="font-size:11px;"';
                    $col_3='width="15%" style="font-size:10px;"';
                    $col_4='width="55%" style="font-size:10px;"';
                    
                }else{
                    $col_1='';
                    $col_2='';
                    $col_3='';
                    $col_4='style="font-size:10px;"';
                }


                foreach ($consumiblesventadetalles->result() as $item) { 
                    $equipos .='<tr>';
                                    $equipos .='<td '.$col_1.'>'.$estatuseq.''.$item->modelo.'</td>';
                                    $equipos .='<td '.$col_2.'>';
                                    $equipos .='<table>';
                                        $equipos .='<tr>';
                                            $equipos .='<td style="color: #625e5e; font-size: 11px;">Equipo referencia<br>'.$item->modeloeq.'</td>';
                                                    $equipos .='</tr>';
                                                $equipos .='</table>';
                                    $equipos .='</td>';
                                    $equipos .='<td '.$col_3.'>'.$item->bodega.'</td>';
                                    $equipos .='<td '.$col_4.'>'.$direccion.'</td></tr>';
                }
                foreach ($ventadetallesrefacion->result() as $item) {
                    $resuleserie=$this->ModeloCatalogos->getrefaccionserie($item->id);
                    foreach ($resuleserie->result() as $itemse) {
                        $equipos .='<tr>';
                                        $equipos .='<td '.$col_1.'>'.$estatuseq.''.$item->modelo.'</td>';
                                        $equipos .='<td '.$col_2.'>';
                                            $equipos .='<table>';
                                                $equipos .='<tr>';
                                                    $equipos .='<td rowspan="2">'.$itemse->serie.'</td>';
                                                    $equipos .='<td style="color: #625e5e; font-size: 11px;">Equipo referencia<br>'.$item->modeloeq.'</td>';
                                                $equipos .='</tr>';
                                                $equipos .='<tr>';
                                                    $equipos .='<td style="color: #625e5e; font-size: 11px;">'.$item->serieeq.'</td>';
                                                $equipos .='</tr>';
                                            $equipos .='</table>';
                                        $equipos .='</td>';
                                        $equipos .='<td '.$col_3.'>'.$item->bodega.'</td>';
                                        $equipos .='<td '.$col_4.'>'.$direccion.'</td></tr>';
                    }
                }
                $equipos.=$equipos_sr;
            //===============================================
                if($viewi==0 && $status_2==1){
                    $btn_edit_hrs=' <a class="b-btn btn-sm  b-btn-primary editar_hrs_ser_4_'.$asignacionId.'" data-ini="'.$hora.'" data-fin="'.$horafin.'" onclick="editar_hrs_ser(4,'.$asignacionId.')"><i class="fas fa-user-clock"></i></a>';
                }else{
                    $btn_edit_hrs='';
                }
                $html.='<tr><th colspan="2" class="t_title" title="4-'.$asignacionId.'">'.$empresa.'</th></tr>';
                    $html.='<tr><td class="b_l_b" width="17%">Tipo de Servicio</td><td class="b_l_b" width="83%">'.$servicio.'</td></tr>';
                    $html.='<tr><td class="b_l_b">Fecha/Hora servicio</td><td class="b_l_b">'.$fecha.' '.$hora.' '.$horafin.''.$btn_edit_hrs.'</td></tr>';
                    $html.='<tr><td class="b_l_b">Falla</td><td class="b_l_b" style="font-size:12px">'.$observaciones.'</td></tr>';
                    if($comentario_tec!=''){
                        $html.='<tr><td class="b_l_b">Comentario</td><td class="b_l_b">'.$comentario_tec.'</td></tr>';
                    }
                    $html.='<tr><td colspan="2"></td></tr>';
                    $html.='<tr><td colspan="2">';
                        $html.='<table border="0">';
                        $html.=$equipos;
                        $html.='</table>';
                    $html.='</td></tr>';
                    $html.='<tr><td colspan="2">';
                        $html.=$htmlservicio;
                    $html.='</td></tr>';
            $html.='</table>';
            $html.='<table><tr><td></td></tr></table>';
        }
        
        if($viewi==1){
            return $html;
        }else{
            echo $html;
        }
        
    }
    function getdireccionequipos_pac($equipo,$serie){
        //hay una version de esta funcion directamente en ModeloCatalogos->getdireccionequipos2 por si se va a hacer algun ajuste a este
        $resultdir=$this->ModeloCatalogos->db2_getselectwheren('contrato_equipo_direccion',array('equiposrow'=>$equipo,'serieId'=>$serie));
        $iddireccionget='g_0';
        $pro_acc='';
        foreach ($resultdir->result() as $itemdir) {
            if($itemdir->direccion!=''){
                $iddireccionget=$itemdir->direccion;
            }
        }
        //log_message('error','iddireccionget '.$iddireccionget);
        if($iddireccionget!='g_0'){
            $tipodireccion=explode('_',$iddireccionget);

            $tipodir = $tipodireccion[0];
            $iddir   = $tipodireccion[1];
            if ($tipodir=='g') {
                $resultdirc_where['idclientedirecc']=$iddir;
                
                
                $resultdirc=$this->ModeloCatalogos->db2_getselectwheren('clientes_direccion',$resultdirc_where);
                foreach ($resultdirc->result() as $item) {
                    $pro_acc=$item->pro_acc;
                }
            }
            /*
            if ($tipodir=='f') {
                $resultdirc_where['id']=$iddir;
                
                if($fecha>=$this->fechahoy){
                    //log_message('error','entra direccion f');
                    $resultdirc_where['activo']=1;
                }
                $resultdirc=$this->ModeloCatalogos->db2_getselectwheren('cliente_has_datos_fiscales',$resultdirc_where);
                foreach ($resultdirc->result() as $item) {
                    $direccion='Razon social: '.$item->razon_social.' Colonia:'.$item->colonia.' Calle:'.$item->calle.' Numero:'.$item->num_ext;
                }
            }
            */
        }
        return $pro_acc;
    }
    function editar_hrs_ser(){
        $params = $this->input->post();
        $ser = $params['ser'];
        $tipo = $params['tipo'];
        $hini = $params['hini'];
        $hfin = $params['hfin'];

        if($tipo==1){
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_contrato_a_e',array('hora'=>$hini,'horaserinicio'=>$hini,'horaserfin'=>$hfin,'horafin'=>$hfin),array('asignacionId'=>$ser));
        }
        if($tipo==2){
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_poliza_a_e',array('hora'=>$hini,'horaserinicio'=>$hini,'horaserfin'=>$hfin,'horafin'=>$hfin),array('asignacionId'=>$ser));
        }
        if($tipo==3){
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_cliente_a',array('hora'=>$hini,'horafin'=>$hfin),array('asignacionId'=>$ser));
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_cliente_a_d',array('horaserinicio'=>$hini,'horaserfin'=>$hfin),array('asignacionId'=>$ser));
        }
        if($tipo==4){
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_venta_a',array('hora'=>$hini,'horaserinicio'=>$hini,'horaserfin'=>$hfin,'horafin'=>$hfin),array('asignacionId'=>$ser));
        }
    }
    function equiposservicios(){
        $params = $this->input->post();
        $idasignacion    = $params['serv'];
        $tipo    = $params['tipo'];
        $tecnico = $params['tec'];
        $fecha = $params['fecha'];
        $html='';
        $btncstserv2='';
        $productivo=1;//0 pruebas 1 productivo
        $rowtr=0;
        if ($tipo==1) {
            $resultado=$this->ModeloAsignacion->getdatosrenta($idasignacion,$fecha,0,$tecnico);
            foreach ($resultado->result() as $item) {
                $class_red='';$ubicacion='';$in_disabled='';
                

                if($item->nr==1){
                    $class_red=' red ';
                    $in_disabled=' disabled ';
                }
                if($item->status==2){
                    $in_disabled=' disabled ';
                }
                $direccion=$this->ModeloCatalogos->getdireccionequipos2($item->id_equipo,$item->serieId,$fecha);
                if($item->ubicacion!=''){
                    $ubicacion='<br>Ubicación: '.$item->ubicacion.'';
                }
                $html.='<tr class="'.$class_red.' ser1">';
                    $html.='<td>';
                        $html.='<input type="hidden" id="asignacion" value="'.$item->asignacionIde.'" readonly style="width:50px">';
                        $html.='<input type="hidden" id="tipo" value="1">';

                        $html.='<input type="checkbox" class="filled-in serivios_tedit" id="serivios_tedit_'.$item->asignacionIde.'" value="'.$item->asignacionIde.'" '.$in_disabled.'>';
                        $html.='<label for="serivios_tedit_'.$item->asignacionIde.'"></label>';

                    $html.='</td>';
                    $html.='<td>'.$item->modelo.'</td>';
                    $html.='<td>'.$item->serie.'</td>';
                    //$html.='<td>'.$direccion.$ubicacion.'</td>';
                    $html.='<td>';
                        $html.='<label>Hora Inicio</label>';
                        $html.='<input type="time" id="horaserinicioext" class="fc-bmz inp_hi" style="margin-bottom: 0px;">';
                    //$html.='</td>';
                    //$html.='<td>';
                        $html.='<label>Hora Fin</label>';
                        $html.='<input type="time" id="horaserfinext" class="fc-bmz inp_hf" style="margin-bottom: 0px;">';
                    $html.='</td>';
                $html.='</tr>';
                        $rowtr++;
            }
        }
        if ($tipo==2) {
            $resultado=$this->ModeloAsignacion->getdatospoliza($idasignacion,0,$tecnico);
            $servicio_pol=0;
            foreach ($resultado->result() as $item) {
                $class_red=''; $in_disabled='';
                if($item->nr==1){
                    $class_red=' red ';
                    $in_disabled=' disabled ';
                }
                if($item->status==2){
                    $in_disabled=' disabled ';
                }

                $contacto=$item->atencion;
                if($item->contacto_text!=null){
                    $contacto=$item->contacto_text;
                }
                $html.='<tr class="'.$class_red.' ser2">';
                    $html.='<td>';
                            $html.='<input type="hidden"  id="asignacion"value="'.$item->asignacionIde.'" readonly style="width:50px">';
                            $html.='<input type="hidden" id="tipo" value="2">';
                            
                            $html.='<input type="checkbox" class="filled-in serivios_tedit" id="serivios_tedit_'.$item->asignacionIde.'" value="'.$item->asignacionIde.'" '.$in_disabled.'>';
                            $html.='<label for="serivios_tedit_'.$item->asignacionIde.'"></label>';

                    $html.='</td>';
                    $html.='<td >'.$item->modelo.'</td>';
                    $html.='<td>'.$item->serie.'</td>';
                    //$html.='<td>'.$item->direccionservicio.'</td>';
                    $html.='<td>';
                        $html.='<label>Hora Inicio</label>';
                        $html.='<input type="time" id="horaserinicioext" class="fc-bmz inp_hi" style="margin-bottom: 0px;">';
                    //$html.='</td>';
                    //$html.='<td>';
                        $html.='<label>Hora Fin</label>';
                        $html.='<input type="time" id="horaserfinext" class="fc-bmz inp_hf" style="margin-bottom: 0px;">';
                    $html.='</td>';
                $html.='</tr>';
                $rowtr++;
            }
        }
        if ($tipo==3) {
            $resultado=$this->ModeloAsignacion->getdatoscliente($idasignacion,$tecnico);
            foreach ($resultado->result() as $item) {
                $class_red='';$in_disabled='';
                if($item->nr==1){
                    $class_red=' red ';
                    $in_disabled=' disabled ';
                }
                if($item->status==2){
                    $in_disabled=' disabled ';
                }
                
                if($item->retiro==1){
                    $modelo_eq=$item->equipostext.' '.$item->poliza.' '.$item->modeloretiro;
                }else{
                    $modelo_eq=$item->equipostext.' '.$item->poliza.' '.$item->modelo;
                }
                $html.='<tr class="'.$class_red.' ser3">';
                    $html.='<td>';
                            $html.='<input type="hidden"  id="asignacion"value="'.$item->asignacionIdd.'" readonly style="width:50px">';
                            $html.='<input type="hidden" id="tipo" value="3">';

                            $html.='<input type="checkbox" class="filled-in serivios_tedit" id="serivios_tedit_'.$item->asignacionIdd.'" value="'.$item->asignacionIdd.'" '.$in_disabled.'>';
                            $html.='<label for="serivios_tedit_'.$item->asignacionIdd.'"></label>';

                    $html.='</td>';
                    $html.='<td>'.$modelo_eq.'</td>';
                    $html.='<td>'.$item->serie.'</td>';
                    //$html.='<td>'.$item->direccion.'</td>';
                    $html.='<td>';
                        $html.='<label>Hora Inicio</label>';
                        $html.='<input type="time" id="horaserinicioext" class="fc-bmz inp_hi" style="margin-bottom: 0px;">';
                    //$html.='</td>';
                    //$html.='<td>';
                        $html.='<label>Hora Fin</label>';
                        $html.='<input type="time" id="horaserfinext" class="fc-bmz inp_hf" style="margin-bottom: 0px;">';
                    $html.='</td>';
                $html.='</tr>';
                $rowtr++;
            }
        }
        if ($tipo==4) {
            $resultado=$this->ModeloAsignacion->getdatosventas($idasignacion);
            foreach ($resultado->result() as $item) {
                $class_red='';$in_disabled='';

                if($item->nr==1){
                    $class_red=' red ';
                    $in_disabled=' disabled ';
                }
                if($item->status==2){
                    $in_disabled=' disabled ';
                }

                $html.='<tr class="'.$class_red.' ser4">';
                    $html.='<td>';
                        $html.='<input type="hidden"  id="asignacion"value="'.$item->asignacionId.'" readonly>';
                        $html.='<input type="hidden" id="tipo" value="4">';

                        $html.='<input type="checkbox" class="filled-in serivios_tedit" id="serivios_tedit_'.$item->asignacionId.'" value="'.$item->asignacionId.'" '.$in_disabled.'>';
                        $html.='<label for="serivios_tedit_'.$item->asignacionId.'"></label>';

                    $html.='</td>';
                    $html.='<td >'.$item->equipostext.'</td>';
                    $html.='<td>'.$item->serie.'</td>';
                    //$html.='<td>'.$item->direccion.'</td>';
                    $html.='<td>';
                        $html.='<label>Hora Inicio</label>';
                        $html.='<input type="time" id="horaserinicioext" class="fc-bmz inp_hi" style="margin-bottom: 0px;">';
                    //$html.='</td>';
                    //$html.='<td>';
                        $html.='<label>Hora Fin</label>';
                        $html.='<input type="time" id="horaserfinext" class="fc-bmz inp_hf" style="margin-bottom: 0px;">';
                    $html.='</td>';
                $html.='</tr>';
                $rowtr++;
            }
        }
        echo $html;
    }
    function info_ultima_cot($idserie,$serie,$viewi){

        $btn_info_cot='';
        $btn_info_cot_table='';
        if($serie!=''){
            $res_ult_cot=$this->ModeloServicio->verificar_servicio_anterior_cot($idserie,$serie);
            foreach ($res_ult_cot->result() as $item_c_p) {
                $btn_info_cot='<a class="b-btn btn-sm b-btn-primary btn_info_cot" ';
                $btn_info_cot.='data-asig="'.$item_c_p->asignacionId.'" ';
                $btn_info_cot.='data-tipo="'.$item_c_p->tipo.'" ';
                $btn_info_cot.='data-tec="'.$item_c_p->nombre.' '.$item_c_p->apellido_paterno.'" ';
                $btn_info_cot.='data-fecha="'.$item_c_p->fecha.'" ';
                $btn_info_cot.='data-comtec="'.$item_c_p->comentario_tec.'" ';
                $btn_info_cot.='data-contr1="'.$item_c_p->cot_refaccion.'" ';
                $btn_info_cot.='data-contr2="'.$item_c_p->cot_refaccion2.'" ';
                $btn_info_cot.='data-contr3="'.$item_c_p->cot_refaccion3.'" ';

                $btn_info_cot.='data-id1="'.$item_c_p->id_dev1.'" ';
                $btn_info_cot.='data-p1="'.$item_c_p->piezas1.'" ';
                $btn_info_cot.='data-m1="'.$item_c_p->modelo1.'" ';
                $btn_info_cot.='data-md1="'.$item_c_p->motivo_dev1.'" ';
                $btn_info_cot.='data-r1="'.$item_c_p->reg_dev1.'" ';

                $btn_info_cot.='data-id2="'.$item_c_p->id_dev2.'" ';
                $btn_info_cot.='data-p2="'.$item_c_p->piezas2.'" ';
                $btn_info_cot.='data-m2="'.$item_c_p->modelo2.'" ';
                $btn_info_cot.='data-md2="'.$item_c_p->motivo_dev2.'" ';
                $btn_info_cot.='data-r2="'.$item_c_p->reg_dev2.'" ';

                $btn_info_cot.='data-id3="'.$item_c_p->id_dev3.'" ';
                $btn_info_cot.='data-p3="'.$item_c_p->piezas3.'" ';
                $btn_info_cot.='data-m3="'.$item_c_p->modelo3.'" ';
                $btn_info_cot.='data-md3="'.$item_c_p->motivo_dev3.'" ';
                $btn_info_cot.='data-r3="'.$item_c_p->reg_dev3.'" ';
                $btn_info_cot.='><i class="fas fa-file-contract"></i></a>';


                $btn_info_cot_table.='<table>';
                    $btn_info_cot_table.='<tr>';
                        $btn_info_cot_table.='<th colspan="2" class="td_cot"><b>Ultima cotización de la serie</b></th>';
                    $btn_info_cot_table.='</tr>';
                    $btn_info_cot_table.='<tr>';
                        $btn_info_cot_table.='<td width="30%" class="td_cot">Técnico:</td><td width="70%" class="td_cot">'.$item_c_p->nombre.' '.$item_c_p->apellido_paterno.'</td>';
                    $btn_info_cot_table.='</tr>';
                    $btn_info_cot_table.='<tr>';
                        $btn_info_cot_table.='<td class="td_cot">Fecha:</td><td class="td_cot">'.$item_c_p->fecha.'</td>';
                    $btn_info_cot_table.='</tr>';
                    $btn_info_cot_table.='<tr>';
                        $btn_info_cot_table.='<td class="td_cot">Comentario Técnico:</td><td class="td_cot">'.$item_c_p->comentario_tec.'</td>';
                    $btn_info_cot_table.='</tr>';
                    $btn_info_cot_table.='<tr>';
                        $btn_info_cot_table.='<td class="td_cot">Motivo de la cotizacion</td><td></td>';
                    $btn_info_cot_table.='</tr>';
                    if($item_c_p->cot_refaccion!='' and $item_c_p->cot_refaccion!=null and $item_c_p->cot_refaccion!='null'){
                        $btn_info_cot_table.='<tr>';
                            $btn_info_cot_table.='<td class="td_cot">Por desgaste</td><td class="td_cot">'.$item_c_p->cot_refaccion.'</td>';
                        $btn_info_cot_table.='</tr>';
                    }
                    if($item_c_p->id_dev1>0){
                        $btn_info_cot_table.='<tr>';
                            $btn_info_cot_table.='<td class="td_cot">Devolución</td><td class="td_cot">'.$item_c_p->piezas1.' '.$item_c_p->modelo1.' '.$item_c_p->motivo_dev1.' '.$item_c_p->reg_dev1.'</td>';
                        $btn_info_cot_table.='</tr>';
                    }
                    if($item_c_p->cot_refaccion2!='' and $item_c_p->cot_refaccion2!=null and $item_c_p->cot_refaccion2!='null'){
                        $btn_info_cot_table.='<tr>';
                            $btn_info_cot_table.='<td class="td_cot">Por daño</td><td class="td_cot">'.$item_c_p->cot_refaccion2.'</td>';
                        $btn_info_cot_table.='</tr>';
                    }
                    if($item_c_p->id_dev2>0){
                        $btn_info_cot_table.='<tr>';
                            $btn_info_cot_table.='<td class="td_cot">Devolución</td><td class="td_cot">'.$item_c_p->piezas2.' '.$item_c_p->modelo2.' '.$item_c_p->motivo_dev2.' '.$item_c_p->reg_dev2.'</td>';
                        $btn_info_cot_table.='</tr>';
                    }
                    if($item_c_p->cot_refaccion3!='' and $item_c_p->cot_refaccion3!=null and $item_c_p->cot_refaccion3!='null'){
                        $btn_info_cot_table.='<tr>';
                            $btn_info_cot_table.='<td class="td_cot">Por garantía</td><td class="td_cot">'.$item_c_p->cot_refaccion3.'</td>';
                        $btn_info_cot_table.='</tr>';
                    }
                    if($item_c_p->id_dev3>0){
                        $btn_info_cot_table.='<tr>';
                            $btn_info_cot_table.='<td class="td_cot">Devolución</td><td class="td_cot">'.$item_c_p->piezas3.' '.$item_c_p->modelo3.' '.$item_c_p->motivo_dev3.' '.$item_c_p->reg_dev3.'</td>';
                        $btn_info_cot_table.='</tr>';
                    }
                $btn_info_cot_table.='</table>';
            }
        }    
        if($viewi==1){
            $btn_info_cot=$btn_info_cot_table;
        }         
        return $btn_info_cot;              
    }
    function reporte_tec($fecha,$stec){
        if(isset($_GET['tec'])){
            $tec=$_GET['tec'];
            if($stec==0){
                $tec='[]';
            }
        }else{
            $tec='[]';
        }
        $tec = json_decode($tec);
        $data['tec']=$tec;
        $resultstec=$this->Configuraciones_model->view_session_searchTecnico(); 
        $data['resultstec']=$resultstec;


        foreach ($resultstec as $item) { 
            if($item->personalId!=49 && $item->personalId!=29 && $item->personalId!=47  && $item->personalId!=82  && $item->personalId!=81 && $item->personalId!=80 && $item->personalId!=83){
                $mostrar=$this->ModeloServicio->filtro_exit_val($tec,$item->personalId);
                if($mostrar==1){
                    //===================================
                        $info=$this->optencioninfoservicios($fecha,$item->personalId,1);
                        //log_message('error','info:'.$info);
                       $data['info_tec'.$item->personalId] = $info;
                    //===================================
                }
            }
        } 
        $this->load->view('Reportes/reporte_tec',$data);
    }
    
}