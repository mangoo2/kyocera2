<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Series extends CI_Controller {
    public function __construct()     {
        parent::__construct();
        //$this->load->model('Clientes_model', 'model');
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloSerie');
        $this->load->model('Compras_model');
        $this->load->model('Login_model');
        $this->load->helper('url');
        $this->load->library('menus');
        date_default_timezone_set('America/Mexico_City');
        $this->submenu=41;
        $this->fechahoy = date('Y-m-d G:i:s');
        if($this->session->userdata('logeado')==true){
            $this->perfilid =$this->session->userdata('perfilid');
            $this->menuss = $this->menus->getMenus($this->perfilid);
            $this->idpersonal = $this->session->userdata('idpersonal');
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,$this->submenu);// 43 es el id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
        }else{
            redirect('login'); 
        }
    }

    // Listado de series
    function index(){
        $data['MenusubId']=$this->submenu;
            // Obtiene los menús disponibles del sistema dependiendo el perfil, ayudado de la libreria "MENUS"
            //$menus = $this->menuss;
            //$data['menus'] = $menus['menus'];
            //$data['submenusArray'] = $menus['submenusArray'];
            //$data['bodegas']=$this->ModeloCatalogos->getselectwheren('bodegas',array('activo' => 1));
            $strq="SELECT * from bodegas where activo=1";
            $resul=$this->db->query($strq);
            $data['bodegas']=$resul->result();

            $data['perfilid']=$this->perfilid;
            $data['equiposrows']=$this->ModeloCatalogos->getselectwheren('equipos',array('estatus' => 1));
            $this->load->view('header');
            $this->load->view('main',$data);
            $this->load->view('series/listado');
            $this->load->view('series/listado_js');
            $this->load->view('footer');
    }
    function add(){
        $data['MenusubId']=$this->submenu;
        //$menus = $this->menuss;
        //$data['menus'] = $menus['menus'];
        //$data['submenusArray'] = $menus['submenusArray'];
        //$data['bodegas']=$this->ModeloCatalogos->getselectwheren('bodegas',array('activo' => 1));
        $strq="SELECT * from bodegas where activo=1";
        $resul=$this->db->query($strq);
        $data['bodegas']=$resul->result();
        //$data['bodegas']=$this->ModeloCatalogos->view_session_bodegas();
        $data['accesorios']=$this->ModeloCatalogos->getselectwheren('catalogo_accesorios',array('status' => 1));
        $data['equipos']=$this->ModeloCatalogos->getselectwheren('equipos',array('estatus' => 1));
        $data['refacciones']=$this->ModeloCatalogos->getselectwheren('refacciones',array('status' => 1));
        $this->load->view('header');
        $this->load->view('main',$data);
        $this->load->view('series/add');
        $this->load->view('footer');
        $this->load->view('series/addjs');
    }
    function addseries(){
        $data = $this->input->post();
        $arrayequipos = $data['arrayequipos'];
        $arrayacessorios = $data['arrayacessorios'];
        $arrayrefacciones = $data['arrayrefacciones'];

        $DATAe = json_decode($arrayequipos); 
        $dataeqarray=array();
        $DATAa = json_decode($arrayacessorios); 
        $dataacarray=array();
        $DATAr = json_decode($arrayrefacciones); 
        $databitacoraarray=array();
        for ($i=0;$i<count($DATAe);$i++) {
            $detallesequipos = array(
                'bodegaId'=>$DATAe[$i]->bodega,
                'productoid'=>$DATAe[$i]->equipo,
                'serie'=>$DATAe[$i]->serie,
                'reg'=>$this->fechahoy,
                'personalId'=>$this->idpersonal);
            $dataeqarray[]=$detallesequipos;

            //1 equipo, 2 acesorio, 3 refaccion,4 consumible 
            $databitacoraarray[]=array('entrada_salida'=>1,'descripcion_evento'=>'Entrada directa','personal'=>$this->idpersonal,'tipo'=>1,'id_item'=>0,'modeloId'=>$DATAe[$i]->equipo,'modelo'=>$DATAe[$i]->serie,'serieId'=>0,'bodega_d'=>$DATAe[$i]->bodega,'clienteId'=>0,'cantidad'=>1);
            
        }
        if (count($DATAe)>0) {
            $this->ModeloCatalogos->insert_batch('series_productos',$dataeqarray);
            //$this->Modelobitacoras->Insert(array('contenido'=>'Se creo directamente series de equipos','nombretabla'=>'series_productos','idtable'=>0,'tipo'=>'Insert','personalId'=>$this->idpersonal)); 
        }
        for ($i=0;$i<count($DATAa);$i++) {
            if ($DATAa[$i]->con_serie==1) {
                $detallesaccesorios = array(
                    'bodegaId'=>$DATAa[$i]->bodega,
                    'accesoriosid'=>$DATAa[$i]->accesorio,
                    'serie'=>$DATAa[$i]->serie,
                    'reg'=>$this->fechahoy,
                    'personalId'=>$this->idpersonal,'cantidad'=>$DATAa[$i]->cantidad);
                $dataacarray[]=$detallesaccesorios;
                
                //1 equipo, 2 acesorio, 3 refaccion,4 consumible 
                $databitacoraarray[]=array('entrada_salida'=>1,'descripcion_evento'=>'Entrada directa','personal'=>$this->idpersonal,'tipo'=>2,'id_item'=>0,'modeloId'=>$DATAa[$i]->accesorio,'modelo'=>$DATAa[$i]->serie,'serieId'=>0,'bodega_d'=>$DATAa[$i]->bodega,'clienteId'=>0,'cantidad'=>$DATAa[$i]->cantidad);
            }else{
                $accesorio=$DATAa[$i]->accesorio;
                $cantidad=$DATAa[$i]->cantidad;
                $bodegaId=$DATAa[$i]->bodega;
                $whererefacs = array(
                    'bodegaId' => $bodegaId,
                    'accesoriosid' => $accesorio,
                    'con_serie'=>0,
                    'activo'=>1
                );
                $resultaccesorios = $this->Compras_model->getselectwherenall('series_accesorios',$whererefacs);
                $existe=0;
                foreach ($resultaccesorios->result() as $item) {
                    $existe=$item->serieId;
                }
                if ($existe==0) {
                    $detallesaccesorios = array(
                        'bodegaId'=>$DATAa[$i]->bodega,
                        'accesoriosid'=>$DATAa[$i]->accesorio,
                        'con_serie'=>0,
                        'serie'=>'Sin Serie',
                        'reg'=>$this->fechahoy,
                        'personalId'=>$this->idpersonal);
                    $dataacarray[]=$detallesaccesorios;
                    
                    //1 equipo, 2 acesorio, 3 refaccion,4 consumible 
                    $databitacoraarray[]=array('entrada_salida'=>1,'descripcion_evento'=>'Entrada directa','personal'=>$this->idpersonal,'tipo'=>2,'id_item'=>0,'modeloId'=>$DATAa[$i]->accesorio,'modelo'=>'sin serie','serieId'=>0,'bodega_d'=>$DATAa[$i]->bodega,'clienteId'=>0,'cantidad'=>1);
                }else{
                    $this->ModeloCatalogos->updatestock('series_accesorios','cantidad','+',$cantidad,'serieId',$existe);
                    $this->ModeloCatalogos->updateCatalogo('series_accesorios',array('status'=>0),array('serieId'=>$existe));

                    //1 equipo, 2 acesorio, 3 refaccion,4 consumible 
                    $databitacoraarray[]=array('entrada_salida'=>1,'descripcion_evento'=>'Entrada directa','personal'=>$this->idpersonal,'tipo'=>2,'id_item'=>0,'modeloId'=>$DATAa[$i]->accesorio,'modelo'=>'','serieId'=>$existe,'bodega_d'=>$DATAa[$i]->bodega,'clienteId'=>0,'cantidad'=>$cantidad);
                }

            }

            //$id=$this->ModeloCatalogos->Insert('series_accesorios',$detallesaccesorios);
            //$this->Modelobitacoras->Insert(array('contenido'=>'Se creo directamente serie de accesorios','nombretabla'=>'series_accesorios','idtable'=>$id,'tipo'=>'Insert','personalId'=>$this->idpersonal)); 
        }
        if (count($dataacarray)>0) {
            $this->ModeloCatalogos->insert_batch('series_accesorios',$dataacarray);
            //$this->Modelobitacoras->Insert(array('contenido'=>'Se creo directamente series de accesorios','nombretabla'=>'series_productos','idtable'=>0,'tipo'=>'Insert','personalId'=>$this->idpersonal)); 
        }
        for ($i=0;$i<count($DATAr);$i++) {
            if ($DATAr[$i]->con_serie==1) {
                $detallesrefacciones = array(
                    'bodegaId'=>$DATAr[$i]->bodega,
                    'refaccionid'=>$DATAr[$i]->refaccion,
                    'serie'=>$DATAr[$i]->serie,
                    'reg'=>$this->fechahoy,
                    'personalId'=>$this->idpersonal);
                $id=$this->ModeloCatalogos->Insert('series_refacciones',$detallesrefacciones);
                //$this->Modelobitacoras->Insert(array('contenido'=>'Se creo directamente serie de refacciones','nombretabla'=>'series_refacciones','idtable'=>$id,'tipo'=>'Insert','personalId'=>$this->idpersonal));

                //1 equipo, 2 acesorio, 3 refaccion,4 consumible 
                $databitacoraarray[]=array('entrada_salida'=>1,'descripcion_evento'=>'Entrada directa','personal'=>$this->idpersonal,'tipo'=>3,'id_item'=>0,'modeloId'=>$DATAr[$i]->refaccion,'modelo'=>$DATAr[$i]->serie,'serieId'=>0,'bodega_d'=>$DATAr[$i]->bodega,'clienteId'=>0,'cantidad'=>1);
            }else{
                $refaccion=$DATAr[$i]->refaccion;
                $cantidad=$DATAr[$i]->cantidad;
                $bodegaId=$DATAr[$i]->bodega;
                $whererefacs = array(
                    'bodegaId' => $bodegaId,
                    'refaccionid' => $refaccion,
                    'con_serie'=>0
                ); 
                $resultrefaccioness = $this->Compras_model->getselectwherenall('series_refacciones',$whererefacs);
                $existe=0;
                foreach ($resultrefaccioness->result() as $item) {
                    $existe=$item->serieId;
                }
                if ($existe>=1) {
                    $this->ModeloCatalogos->updatestock('series_refacciones','cantidad','+',$cantidad,'serieId',$existe);
                    $this->ModeloCatalogos->updateCatalogo('series_refacciones',array('status'=>0),array('serieId'=>$existe));
                    //1 equipo, 2 acesorio, 3 refaccion,4 consumible 
                    $databitacoraarray[]=array('entrada_salida'=>1,'descripcion_evento'=>'Entrada directa','personal'=>$this->idpersonal,'tipo'=>3,'id_item'=>0,'modeloId'=>$DATAr[$i]->refaccion,'modelo'=>'sin serie','serieId'=>$existe,'bodega_d'=>$DATAr[$i]->bodega,'clienteId'=>0,'cantidad'=>$cantidad);
                }else{
                    $detallesconsumibles = array(
                        'bodegaId'=>$bodegaId,
                        'refaccionid'=>$refaccion,
                        'serie'=>'Sin Serie',
                        'con_serie'=>0,
                        'cantidad'=>$cantidad,
                        'status'=>0,
                        'personalId'=>$this->idpersonal
                    );
                    $serieidr=$this->ModeloCatalogos->Insert('series_refacciones',$detallesconsumibles);

                    //1 equipo, 2 acesorio, 3 refaccion,4 consumible 
                    $databitacoraarray[]=array('entrada_salida'=>1,'descripcion_evento'=>'Entrada directa','personal'=>$this->idpersonal,'tipo'=>3,'id_item'=>0,'modeloId'=>$refaccion,'modelo'=>'','serieId'=>$serieidr,'bodega_d'=>$bodegaId,'clienteId'=>0,'cantidad'=>$cantidad);
                }
            }
            
        }
        if (count($databitacoraarray)>0) {
            $this->ModeloCatalogos->insert_batch('bitacora_movimientos',$databitacoraarray);
        }
    }
    function addseriesc(){
        $data = $this->input->post();
        
        $arrayequipos = $data['arrayequipos'];
        $arrayacessorios = $data['arrayacessorios'];
        $arrayrefacciones = $data['arrayrefacciones'];
        $arrayconsumibles = $data['arrayconsumibles'];

        $datac['folio'] = $data['folio'];
        $idcompra = $data['idcompra'];
        $datac['personalId'] = $this->idpersonal;
        if($idcompra>0){
            $compraId=$idcompra;
        }else{
            $compraId=$this->ModeloCatalogos->Insert('series_compra',$datac);
        }
        
        //$this->Modelobitacoras->Insert(array('contenido'=>'Se creo una compra series','nombretabla'=>'series_compra','idtable'=>$compraId,'tipo'=>'Insert','personalId'=>$this->idpersonal));
        $DATAe = json_decode($arrayequipos); 
        $DATAa = json_decode($arrayacessorios); 
        $DATAr = json_decode($arrayrefacciones); 
        $DATAc = json_decode($arrayconsumibles); 
        for ($i=0;$i<count($DATAe);$i++) {
            $cantidade=$DATAe[$i]->serie;
            while ($cantidade >= 1){
                $detallesequipos = array(
                'compraId'=>$compraId,
                'productoid'=>$DATAe[$i]->equipo,
                'precio'=>$DATAe[$i]->precio,
                'serie'=>'',
                'reg'=>$this->fechahoy);
                $this->ModeloCatalogos->Insert('series_compra_productos',$detallesequipos);
                $cantidade--;
            } 
        }
        for ($i=0;$i<count($DATAa);$i++) {
            $cantidada=$DATAa[$i]->serie;
            if ($DATAa[$i]->con_serie==1) {
                while ($cantidada >= 1){
                    $detallesaccesorios = array(
                    'compraId'=>$compraId,
                    'accesoriosid'=>$DATAa[$i]->accesorio,
                    'precio'=>$DATAa[$i]->precio,
                    'serie'=>'',
                    'reg'=>$this->fechahoy);
                    $this->ModeloCatalogos->Insert('series_compra_accesorios',$detallesaccesorios);
                    $cantidada--;
                } 
            }else{
                $detallesaccesorios = array(
                    'compraId'=>$compraId,
                    'accesoriosid'=>$DATAa[$i]->accesorio,
                    'precio'=>$DATAa[$i]->precio,
                    'serie'=>'sin serie',
                    'cantidad'=>$DATAa[$i]->serie,
                    'con_serie'=>0,
                    'reg'=>$this->fechahoy);
                    $this->ModeloCatalogos->Insert('series_compra_accesorios',$detallesaccesorios);
            }
        }
        $datainsert_comp_refacc1=array();
        $datainsert_comp_refacc2=array();
        for ($i=0;$i<count($DATAr);$i++) {
            $cantidadr=$DATAr[$i]->serie;
            if ($DATAr[$i]->con_serie==1) {
                while ($cantidadr >= 1){
                    $detallesrefacciones = array(
                    'compraId'=>$compraId,
                    'refaccionid'=>$DATAr[$i]->refaccion,
                    'precio'=>$DATAr[$i]->precio,
                    'serie'=>'',
                    'reg'=>$this->fechahoy);
                    $datainsert_comp_refacc1[]=$detallesrefacciones;
                    //$this->ModeloCatalogos->Insert('series_compra_refacciones',$detallesrefacciones);
                    $cantidadr--;
                } 
            }else{
                $detallesrefacciones = array(
                    'compraId'=>$compraId,
                    'refaccionid'=>$DATAr[$i]->refaccion,
                    'precio'=>$DATAr[$i]->precio,
                    'serie'=>'Sin Serie',
                    'cantidad'=>$DATAr[$i]->serie,
                    'con_serie'=>0,
                    'reg'=>$this->fechahoy);
                $datainsert_comp_refacc2[]=$detallesrefacciones;
                    //$this->ModeloCatalogos->Insert('series_compra_refacciones',$detallesrefacciones);
            }
        }
        if(count($datainsert_comp_refacc1)>0){
            $this->ModeloCatalogos->insert_batch('series_compra_refacciones',$datainsert_comp_refacc1);
        }
        if(count($datainsert_comp_refacc2)>0){
            $this->ModeloCatalogos->insert_batch('series_compra_refacciones',$datainsert_comp_refacc2);
        }
        $datainsert_comp_cons=array();
        for ($i=0;$i<count($DATAc);$i++) {
                $detallesconsumibles = array(
                'compraId'=>$compraId,
                'consumibleId'=>$DATAc[$i]->consumible,
                'cantidad'=>$DATAc[$i]->cantidad,
                'precio'=>$DATAc[$i]->precio,
                'reg'=>$this->fechahoy);
                $datainsert_comp_cons[]=$detallesconsumibles;
                //$this->ModeloCatalogos->Insert('compra_consumibles',$detallesconsumibles); 
        }
        if (count($DATAc)>0) {
            $this->ModeloCatalogos->insert_batch('compra_consumibles',$datainsert_comp_cons);
        }
    }
    public function getData_serie_e() {

        $params = $this->input->post();
       
        $serie = $this->ModeloSerie->Getseries_equipos($params);

        $totalRecords=$this->ModeloSerie->TotalSerieequipo($params); 

        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totalRecords),  
            "recordsFiltered" => intval($totalRecords),
            "data"            => $serie->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    public function getData_serie_a() {

        $params = $this->input->post();

        $serie = $this->ModeloSerie->Getseries_accesorios($params);

        $totalRecords=$this->ModeloSerie->TotalSerieaccesorios($params); 

        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totalRecords),  
            "recordsFiltered" => intval($totalRecords),
            "data"            => $serie->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    public function getData_serie_r() {

        $params = $this->input->post();

        $serie = $this->ModeloSerie->Getseries_refacciones($params);

        $totalRecords=$this->ModeloSerie->TotalSerierefacciones($params); 

        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totalRecords),  
            "recordsFiltered" => intval($totalRecords),
            "data"            => $serie->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    function traspasar(){
        $params = $this->input->post();
        $serieid = $params['serieid'];
        $tipo = $params['tipo'];
        $bodega = $params['bodega'];
        $motivo = $params['motivo'];
        $bodegao = $params['bodegao'];
        $modelo = $params['modelo'];
        $consin = $params['consin'];
        $cant_tras = $params['cant_tras'];
        $tserie='';
        $modeloId='';
        if ($tipo==1) {
            $table='series_productos';
            $tserie='productos';
            $result=$this->ModeloCatalogos->getselectwheren($table,array('serieId'=>$serieid));
            foreach ($result->result() as $item) {
                $modeloId=$item->productoid;
            }
            $this->ModeloCatalogos->updateCatalogo($table,array('bodegaId'=>$bodega),array('serieId'=>$serieid));$this->Configuraciones_model->registrarmovimientosseries($serieid,1);
            //$this->Modelobitacoras->Insert(array('contenido'=>'Se realizo un traspaso de series: '.$tserie,'nombretabla'=>$table,'idtable'=>$serieid,'tipo'=>'Update','personalId'=>$this->idpersonal));
            $this->ModeloCatalogos->Insert('bitacora_movimientos',array('entrada_salida'=>2,'descripcion_evento'=>'Traspaso, Motivo: '.$motivo.'','personal'=>$this->idpersonal,'tipo'=>$tipo,'id_item'=>0,'modeloId'=>$modeloId,'modelo'=>$modelo,'serieId'=>$serieid,'bodega_o'=>$bodegao,'bodega_d'=>$bodega));
        }elseif ($tipo==2) {
            $table='series_accesorios';
            $tserie='accesorios';

            $result=$this->ModeloCatalogos->getselectwheren($table,array('serieId'=>$serieid));
            foreach ($result->result() as $item) {
                $modeloId=$item->accesoriosid;
                $cantidad=$item->cantidad;
            }
            
            if($consin==1){
                $this->ModeloCatalogos->updateCatalogo($table,array('bodegaId'=>$bodega),array('serieId'=>$serieid));$this->Configuraciones_model->registrarmovimientosseries($serieid,2);
                //$this->Modelobitacoras->Insert(array('contenido'=>'Se realizo un traspaso de series: '.$tserie,'nombretabla'=>$table,'idtable'=>$serieid,'tipo'=>'Update','personalId'=>$this->idpersonal));
                $this->ModeloCatalogos->Insert('bitacora_movimientos',array('entrada_salida'=>2,'descripcion_evento'=>'Traspaso, Motivo: '.$motivo.'','personal'=>$this->idpersonal,'tipo'=>$tipo,'id_item'=>0,'modeloId'=>$modeloId,'modelo'=>$modelo,'serieId'=>$serieid,'bodega_o'=>$bodegao,'bodega_d'=>$bodega));
            }else{
                $result=$this->ModeloCatalogos->getselectwheren('series_accesorios',array('accesoriosid'=>$modeloId,'bodegaId'=>$bodega,'con_serie'=>0));
                $existe_a=0;
                foreach ($result->result() as $item_v) {
                    $existe_a=$item_v->serieId;
                }
                
                //if ($cantidad==$cant_tras) {
                    if($existe_a>0){
                        $strq="UPDATE series_accesorios SET cantidad=cantidad-$cant_tras where serieId='$serieid'";
                        $this->db->query($strq);
                        $strq="UPDATE series_accesorios SET cantidad=cantidad+$cant_tras,status=0 where serieId='$existe_a'";
                        $this->db->query($strq);$this->Configuraciones_model->registrarmovimientosseries($serieid,2);
                        $this->ModeloCatalogos->Insert('bitacora_movimientos',array('id_item'=>0,'modeloId'=>$modeloId,'modelo'=>$modelo,'tipo'=>2,'cantidad'=>$cant_tras,'serieId'=>$serieid,'entrada_salida'=>2,'bodega_o'=>$bodegao,'bodega_d'=>$bodega,'personal'=>$this->idpersonal,'descripcion_evento'=>'Traspaso, Motivo: '.$motivo.''));
                    }else{
                        $strq="UPDATE series_accesorios SET cantidad=cantidad-$cant_tras where serieId='$serieid'";
                        $this->db->query($strq);
                        $this->ModeloCatalogos->Insert('series_accesorios',array('accesoriosid'=>$modeloId, 'bodegaId'=>$bodega,'serie'=>'Sin serie','con_serie'=>0,'cantidad'=>$cant_tras,'status'=>0,'personalId'=>$this->idpersonal));
                        $this->ModeloCatalogos->Insert('bitacora_movimientos',array('id_item'=>0,'modeloId'=>$modeloId,'modelo'=>$modelo,'tipo'=>2,'cantidad'=>$cant_tras,'serieId'=>$serieid,'entrada_salida'=>2,'bodega_o'=>$bodegao,'bodega_d'=>$bodega,'personal'=>$this->idpersonal,'descripcion_evento'=>'Traspaso, Motivo: '.$motivo.''));
                        $this->Configuraciones_model->registrarmovimientosseries($serieid,2);
                    }
                //}else{

                //}

            }
        }elseif ($tipo==3) {
            $table='series_refacciones';
            $tserie='refacciones';
            $result=$this->ModeloCatalogos->getselectwheren($table,array('serieId'=>$serieid));
            foreach ($result->result() as $item) {
                $modeloId=$item->refaccionid;
            }
            $this->ModeloCatalogos->updateCatalogo($table,array('bodegaId'=>$bodega),array('serieId'=>$serieid));$this->Configuraciones_model->registrarmovimientosseries($serieid,3);
            //$this->Modelobitacoras->Insert(array('contenido'=>'Se realizo un traspaso de series: '.$tserie,'nombretabla'=>$table,'idtable'=>$serieid,'tipo'=>'Update','personalId'=>$this->idpersonal));
            $this->ModeloCatalogos->Insert('bitacora_movimientos',array('entrada_salida'=>2,'descripcion_evento'=>'Traspaso, Motivo: '.$motivo.'','personal'=>$this->idpersonal,'tipo'=>$tipo,'id_item'=>0,'modeloId'=>$modeloId,'modelo'=>$modelo,'serieId'=>$serieid,'bodega_o'=>$bodegao,'bodega_d'=>$bodega));
        }
        
    }
    function traspasarssr(){
        $params = $this->input->post();

        $serieid = $params['serieid'];
        $cantidad = $params['cantidad'];
        $bodega = $params['bodega'];
        $motivo = $params['motivo'];
        $bodegao = $params['bodegao'];
        $modelo = $params['modelo'];

        $wherec=array('serieId'=>$serieid);
        $resultcb=$this->ModeloCatalogos->getselectwheren('series_refacciones',$wherec);
        foreach ($resultcb->result() as $item) {
            $rt_resultcb=$item->bodegaId;
            $rt_refaccionid=$item->refaccionid;
            $rt_cantidad=$item->cantidad;
        }
        if ($rt_cantidad==$cantidad) {
            $whereconsu = array( 
                'bodegaId' => $bodega, 
                'refaccionid' => $rt_refaccionid, 
                'con_serie'=>0 
            );  
            $resultconsu = $this->ModeloCatalogos->getselectwherenall('series_refacciones',$whereconsu); 
            $consubId=0; 
            foreach ($resultconsu as $item) { 
                $consubId=$item->serieId; 
            }
            if ($consubId==0) {
                $this->ModeloCatalogos->updateCatalogo('series_refacciones',array('bodegaId'=>$bodega),array('serieId'=>$serieid));$this->Configuraciones_model->registrarmovimientosseries($serieid,3);
            }else{
                $this->ModeloCatalogos->updatestock('series_refacciones','cantidad','+',$cantidad,'serieId',$consubId);$this->Configuraciones_model->registrarmovimientosseries($consubId,3);
                $this->ModeloCatalogos->updatestock('series_refacciones','cantidad','-',$cantidad,'serieId',$serieid);$this->Configuraciones_model->registrarmovimientosseries($serieid,3);
            }  
            $this->ModeloCatalogos->Insert('bitacora_movimientos',array('entrada_salida'=>2,'descripcion_evento'=>'Traspaso, Motivo: '.$motivo.'','personal'=>$this->idpersonal,'tipo'=>3,'id_item'=>0,'modeloId'=>$rt_refaccionid,'modelo'=>$modelo,'cantidad'=>$cantidad,'serieId'=>$serieid,'bodega_o'=>$bodegao,'bodega_d'=>$bodega));
        }else{
            $this->ModeloCatalogos->updatestock('series_refacciones','cantidad','-',$cantidad,'serieId',$serieid);$this->Configuraciones_model->registrarmovimientosseries($serieid,3);
            $whereconsu = array( 
                'bodegaId' => $bodega, 
                'refaccionid' => $rt_refaccionid, 
                'con_serie'=>0 
            );  
            $resultconsu = $this->ModeloCatalogos->getselectwherenall('series_refacciones',$whereconsu); 
            $consubId=0; 
            foreach ($resultconsu as $item) { 
                $consubId=$item->serieId; 
            }
            if ($consubId>=1) { 
                $this->ModeloCatalogos->updatestock('series_refacciones','cantidad','+',$cantidad,'serieId',$consubId);$this->Configuraciones_model->registrarmovimientosseries($consubId,3);
            }else{
                $detallesconsumibles = array( 
                    'bodegaId'=>$bodega, 
                    'refaccionid'=>$rt_refaccionid, 
                    'serie'=>'Sin Serie',
                    'con_serie'=>0,
                    'cantidad'=>$cantidad,
                    'personalId'=>$this->idpersonal
                ); 
                $this->ModeloCatalogos->Insert('series_refacciones',$detallesconsumibles); 
            }
            $this->ModeloCatalogos->Insert('bitacora_movimientos',array('entrada_salida'=>2,'descripcion_evento'=>'Traspaso, Motivo: '.$motivo,'personal'=>$this->idpersonal,'tipo'=>3,'id_item'=>0,'modeloId'=>$rt_refaccionid,'modelo'=>$modelo,'cantidad'=>$cantidad,'serieId'=>$consubId,'bodega_o'=>$bodegao,'bodega_d'=>$bodega));
        }
    }
    public function verificar_serie(){
        $serie = $this->input->post('ser');
        $where = array('serie' =>$serie,'activo'=>1);
        $resultconsu = $this->ModeloCatalogos->getselectwherenall('series_productos',$where); 
        $aux = 0;
        foreach ($resultconsu as $item){
          $aux=1;     
        }
        echo $aux; 
    }
    public function verificar_serie2(){
        $serie = $this->input->post('ser');
        $where = array('serie' =>$serie,'activo'=>1);
        $resultconsu = $this->ModeloCatalogos->getselectwherenall('series_accesorios',$where); 
        $aux = 0;
        foreach ($resultconsu as $item){
            if($serie!='Sin Serie'){
                $aux=1;     
            }
        }
        echo $aux; 
    }
    public function verificar_serie3(){
        $serie = $this->input->post('ser');
        $where = array('serie' =>$serie,'activo'=>1);
        $resultconsu = $this->ModeloCatalogos->getselectwherenall('series_refacciones',$where); 
        $aux = 0;
        foreach ($resultconsu as $item){
            if($serie!='Sin Serie'){
                $aux=1;     
            }
        }
        echo $aux; 
    }
    function editarseries(){
        $datas = $this->input->post();
        $serieid = $datas['serieid'];
        $serie = $datas['serie'];
        $equipo = $datas['equipo'];
        $this->ModeloCatalogos->updateCatalogo('series_productos',array('productoid'=>$equipo,'serie'=>$serie),array('serieId'=>$serieid));

        //$this->Modelobitacoras->Insert(array('contenido'=>'Se edito una serie','nombretabla'=>'series_productos','idtable'=>$serieid,'tipo'=>'Update','personalId'=>$this->idpersonal)); 
    }
    function eliminarserie($serieid){
        $this->ModeloCatalogos->updateCatalogo('series_productos',array('activo'=>0),array('serieId'=>$serieid));
        $this->Modelobitacoras->Insert(array('contenido'=>'Se Elimino una serie','nombretabla'=>'series_productos','idtable'=>$serieid,'tipo'=>'Delete','personalId'=>$this->idpersonal));
    }
    function eliminarserieaccesorio($serieid){
        $this->ModeloCatalogos->updateCatalogo('series_accesorios',array('activo'=>0),array('serieId'=>$serieid));
        $this->Modelobitacoras->Insert(array('contenido'=>'Se Elimino una serie','nombretabla'=>'series_productos','idtable'=>$serieid,'tipo'=>'Delete','personalId'=>$this->idpersonal));
    }
    function eliminarpartida(){
        $datas = $this->input->post();
        $compra = $datas['compra'];
        $serie = $datas['serie'];
        $tipo = $datas['tipo'];
        if ($tipo==1) {
            $this->ModeloCatalogos->updateCatalogo('series_compra_productos',array('activo'=>0),array('serieId'=>$serie));
        }
    }
    function historialseries(){
        $data = $this->input->post();
        $serieid = $data['serieid'];
        $tipo = $data['tipo'];//1 equipos, 2 accesorios, 3 refacciones
        $html='';
        if ($tipo==1) {
            //=============================================================
                $serieventa = $this->ModeloSerie->serieequipoubicacion($serieid);
                if ($serieventa->num_rows() > 0) {
                    $html.='<table>
                            <thead>
                                <tr>
                                    <th>Venta</th>
                                    <th>Cliente</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            ';
                        foreach ($serieventa->result() as $item) {
                            if ($item->combinada==0) {
                                $botn='<a class="btn-floating green tooltipped" data-position="top" data-delay="50" data-tooltip="Prefactura" onclick="detallev('.$item->idVenta.',0)" data-tooltip-id="3253eb7e-45e0-9f32-c3c0-d9e4b6584ff2"><i class="material-icons">assignment</i></a>';
                            }else{
                                $botn='<a class="btn-floating green tooltipped" data-position="top" data-delay="50" data-tooltip="Prefactura" onclick="detallev('.$item->idVenta.',1)" data-tooltip-id="3253eb7e-45e0-9f32-c3c0-d9e4b6584ff2"><i class="material-icons">assignment</i></a>';
                            }
                            $html.='<tr>
                                        <td>'.$item->idVenta.'</td>
                                        <td>'.$item->empresa.'</td>
                                        <td>'.$item->reg.'</td>
                                        <td>'.$botn.'</td>
                                    </tr>';
                        }
                    $html.='</tbody></table>';
                }
            //=================================================================
                $seriecontra = $this->ModeloSerie->serieequipoubicacionr($serieid);
                if ($seriecontra->num_rows() > 0) {
                    $html.='<table>
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Cliente</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            ';
                        foreach ($seriecontra->result() as $item) {
                                if($item->idcontrato>0){
                                    $idrow='Contrato: '.$item->idcontrato;
                                    $url=base_url().'index.php/Contratos/contrato/'.$item->idRenta;
                                }else{
                                    $idrow='Renta: '.$item->idRenta;
                                    $url=base_url().'index.php/Rentas/contrato/'.$item->idRenta;
                                }
                                $equipoactivo='';
                                if($item->estatus==1){
                                    if($item->activo==1){
                                        $equipoactivo='equipoactivo';
                                    }   
                                }
                                $botn='<a class="btn-floating green tooltipped" data-position="top" data-delay="50" data-tooltip="Prefactura" 
                                        href="'.$url.'" target="_blank"
                                 data-tooltip-id="3253eb7e-45e0-9f32-c3c0-d9e4b6584ff2"><i class="material-icons">assignment</i></a>';
                            
                            $html.='<tr class="'.$equipoactivo.'">
                                        <td>'.$idrow.'</td>
                                        <td>'.$item->empresa.'</td>
                                        <td>'.$item->reg.'</td>
                                        <td>'.$botn.'</td>
                                    </tr>';
                        }
                    $html.='</tbody></table>';
                }
            //=================================================================
            if($this->idpersonal==1 or $this->idpersonal==17 or $this->idpersonal==18 or $this->idpersonal==9){
                $html.='<div class="col s2"><p class="seriesalida liberarserie" style="cursor:pointer" onclick="liberarserie('.$serieid.')">Liberar</p></div>';
            }
        }
        if($tipo==2){
            $serieventa = $this->ModeloSerie->serieaccesorioubicacion($serieid);
                if ($serieventa->num_rows() > 0) {
                    $html.='<table>
                            <thead>
                                <tr>
                                    <th>Venta</th>
                                    <th>Cliente</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            ';
                        foreach ($serieventa->result() as $item) {
                            if ($item->combinada==0) {
                                $ideventa=$item->id_venta;
                                $botn='<a class="btn-floating green tooltipped" data-position="top" data-delay="50" data-tooltip="Prefactura" onclick="detallev('.$item->id_venta.',0)" data-tooltip-id="3253eb7e-45e0-9f32-c3c0-d9e4b6584ff2"><i class="material-icons">assignment</i></a>';
                            }else{
                                $ideventa=$item->combinadaId;
                                $botn='<a class="btn-floating green tooltipped" data-position="top" data-delay="50" data-tooltip="Prefactura" onclick="detallev('.$item->combinadaId.',1)" data-tooltip-id="3253eb7e-45e0-9f32-c3c0-d9e4b6584ff2"><i class="material-icons">assignment</i></a>';
                            }
                            $html.='<tr>
                                        <td>'.$ideventa.'</td>
                                        <td>'.$item->empresa.'</td>
                                        <td>'.$item->reg.'</td>
                                        <td>'.$botn.'</td>
                                    </tr>';
                        }
                    $html.='</tbody></table>';
                }
            //=================================================================
            //=================================================================
                $seriecontra = $this->ModeloSerie->serieaccesorioubicacionr($serieid);
                if ($seriecontra->num_rows() > 0) {
                    $html.='<table>
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Cliente</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            ';
                        foreach ($seriecontra->result() as $item) {
                                if($item->idcontrato>0){
                                    $idrow='Contrato: '.$item->idcontrato;
                                    $url=base_url().'index.php/Contratos/contrato/'.$item->idRenta;
                                }else{
                                    $idrow='Renta: '.$item->idrentas;
                                    $url=base_url().'index.php/Rentas/contrato/'.$item->idrentas;
                                }
                                $botn='<a class="btn-floating green tooltipped" data-position="top" data-delay="50" data-tooltip="Prefactura" 
                                        href="'.$url.'" target="_blank"
                                 data-tooltip-id="3253eb7e-45e0-9f32-c3c0-d9e4b6584ff2"><i class="material-icons">assignment</i></a>';
                            
                            $html.='<tr>
                                        <td>'.$idrow.'</td>
                                        <td>'.$item->empresa.'</td>
                                        <td>'.$item->reg.'</td>
                                        <td>'.$botn.'</td>
                                    </tr>';
                        }
                    $html.='</tbody></table>';
                }
            if($this->perfilid==1){
                $html.='<div class="col s2"><p class="seriesalida liberarserie" style="cursor:pointer" onclick="liberarseriea('.$serieid.')">Liberar</p></div>';
            }
        }
        
        echo $html;
    }
    function liberarserie(){
        $datas = $this->input->post();
        $serieid = $datas['serieid'];
        $pass   = $datas['pass'];
        
        $permiso=0;
        $permisos = $this->Login_model->permisoadminarray(array('1','17','18','9'));//1admin 17 julio 18 diana 9 israel
        foreach ($permisos as $item) {
            $permisotemp=0;

            $usuario=$item->Usuario;

            $verificar = password_verify($pass,$item->contrasena);

            if ($verificar==false) {
                $verificar = password_verify($pass,$item->clavestemp);
                if ($verificar) {
                    $permisotemp=1;
                }
            }
            if ($verificar) {
                $permiso=1;
                $this->ModeloCatalogos->updateCatalogo('series_productos',array('status'=>0),array('serieId'=>$serieid));$this->Configuraciones_model->registrarmovimientosseries($serieid,1);

                $this->ModeloCatalogos->Insert('bitacora',array('contenido'=>'Se libero directamente la serie de equipos, con permiso del usuario:'.$usuario,'nombretabla'=>'series_productos','idtable'=>$serieid,'tipo'=>'update','personalId'=>$this->idpersonal));

                if($permisotemp==1){
                    $this->ModeloCatalogos->updateCatalogo('usuarios',array('clavestemp'=>null),array('UsuarioID'=>$item->UsuarioID));
                }
            }
        }
        echo $permiso;
    }
    function liberarseriea(){
        $datas = $this->input->post();
        $serieid = $datas['serieid'];
        $pass   = $datas['pass'];

        $permiso=0;
        $permisos = $this->Login_model->permisoadminarray(array('1','17','18','9'));//1admin 17 julio 18 diana 9 israel

        foreach ($permisos as $item) {
            $permisotemp=0;

            $usuario=$item->Usuario;

            $verificar = password_verify($pass,$item->contrasena);

            if ($verificar==false) {
                $verificar = password_verify($pass,$item->clavestemp);
                if ($verificar) {
                    $permisotemp=1;
                }
            }
            if ($verificar) {
                $permiso=1;
                $this->ModeloCatalogos->updateCatalogo('series_accesorios',array('status'=>0),array('serieId'=>$serieid));$this->Configuraciones_model->registrarmovimientosseries($serieid,2);

                $this->ModeloCatalogos->Insert('bitacora',array('contenido'=>'Se libero directamente la serie de accerorio, con permiso del usuario:'.$usuario,'nombretabla'=>'series_accesorios','idtable'=>$serieid,'tipo'=>'update','personalId'=>$this->idpersonal));   

                if($permisotemp==1){
                    $this->ModeloCatalogos->updateCatalogo('usuarios',array('clavestemp'=>null),array('UsuarioID'=>$item->UsuarioID));
                }
            }
        }
        echo $permiso;
    }
    function resguardareq(){
        $params = $this->input->post();
        $serie = $params['serie'];
        $tipo = $params['tipo'];

        if($tipo==0){
            $tipor=1;
        }else{
            $tipor=0;
        }
        $this->ModeloCatalogos->updateCatalogo('series_productos',array('resguardar_cant'=>$tipor),array('serieId'=>$serie));

    }
    function resguardar_acc(){
        $params = $this->input->post();
        $serie = $params['serie'];
        $cant = $params['cant'];

        $this->ModeloCatalogos->updateCatalogo('series_accesorios',array('resguardar_cant'=>$cant),array('serieId'=>$serie));
    }
    function resguardar_acc_s(){
        $params = $this->input->post();
        $serie = $params['serie'];
        $reg = $params['reg'];
        if($reg>0){
            $cant=0;
        }else{
            $cant=1;
        }

        $this->ModeloCatalogos->updateCatalogo('series_accesorios',array('resguardar_cant'=>$cant),array('serieId'=>$serie));
    }
    function resguardar_ref(){
        $params = $this->input->post();
        $serie = $params['serie'];
        $cant = $params['cant'];

        $this->ModeloCatalogos->updateCatalogo('series_refacciones',array('resguardar_cant'=>$cant),array('serieId'=>$serie));
    }
    function resguardar_ref_s(){
        $params = $this->input->post();
        $serie = $params['serie'];
        $reg = $params['reg'];
        if($reg>0){
            $cant=0;
        }else{
            $cant=1;
        }

        $this->ModeloCatalogos->updateCatalogo('series_refacciones',array('resguardar_cant'=>$cant),array('serieId'=>$serie));
    }
    function resguardar_cons(){
        $params = $this->input->post();
        $serie = $params['serie'];
        $cant = $params['cant'];

        $this->ModeloCatalogos->updateCatalogo('consumibles_bodegas',array('resguardar_cant'=>$cant),array('consubId'=>$serie));
    }
}
?>
