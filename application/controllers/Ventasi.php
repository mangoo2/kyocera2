<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Ventasi extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->helper('url');
         $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloGeneral');//insert generales
        $this->load->model('Cotizaciones_model');
        $this->load->model('Equipos_model');
        $this->load->model('Polizas_model');
        $this->load->model('Clientes_model');
        $this->load->model('Prospectos_model');
        $this->load->model('Consumibles_model');
        $this->load->model('Refacciones_model');
        $this->load->model('ConfiguracionDocumentos_Model');
        $this->load->model('Ventas_model');
        $this->load->model('Rentas_model');
        $this->load->model('Modeloentregasdeldia');
        $this->load->model('Personal/ModeloPersonal');
        $this->load->model('ModeloAsignacion');
        $this->load->library('menus');
        $this->submenu=39;
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d H:i:s');
        $this->fechahoyc = date('Y-m-d');
        if ($this->session->userdata('logeado')!=true) {
            redirect('login');
        }else{
            $this->idpersonal=$this->session->userdata('idpersonal');
        }
    }
    public function index(){
        $data['MenusubId']=$this->submenu;
        $where = array('personalId' =>$this->idpersonal);  
        $result=$this->ModeloCatalogos->getselectwheren('personal',$where);
        foreach ($result->result() as $item) {
           $data['nombre']=$item->nombre;
           $data['apellido_paterno']=$item->apellido_paterno;
           $data['apellido_materno']=$item->apellido_materno;
        }
        $where_m = array('activo' =>1);  
        $data['get_metodo_pago']=$this->ModeloCatalogos->getselectwheren('f_metodopago',$where_m);
        $data['empleados'] = $this->ModeloCatalogos->getselectwheren('personal',array('estatus'=>1,'personalId !='=>17,'personalId !='=>18));
        //$menus = $this->menus->getMenus($this->session->userdata('perfilid'));
        //$data['menus'] = $menus['menus'];
        //$data['submenusArray'] = $menus['submenusArray'];
        //$data['fechainicial_reg'] = date("Y-m-d",strtotime($this->fechahoy."- 1 year")); 
        $data['fechainicial_reg'] = date("Y-m-d",strtotime($this->fechahoy."- 6 months")); 
        $data['pendientesentrega_reg']=$this->Modeloentregasdeldia->prefacturaspendientesdeentraga();
        $data['resultstec']=$this->Configuraciones_model->view_session_searchTecnico(); 

        $this->load->view('header');
        $this->load->view('main',$data);
        $this->load->view('ventas/listadoi');
        
        $this->load->view('footer');     
        $this->load->view('ventas/listadoi_js');
	}
    public function getListaVentasIncompletas() {
        $params = $this->input->post();
        $tipo=$params['tipo'];
        if($tipo==0){
            //solo lo del ultimo mes
            $productos = $this->Modeloentregasdeldia->getListaVentasIncompletasfactura($params);
            $totalRecords=$this->Modeloentregasdeldia->getListaVentasIncompletasfacturat($params); 
           
        }
        if($tipo==1){
            $productos = $this->Modeloentregasdeldia->getListaVentas($params);
            $totalRecords=$this->Modeloentregasdeldia->getListaVentast($params); 
        }
        if($tipo==2){
            $productos = $this->Modeloentregasdeldia->getListaRentas($params);
            $totalRecords=$this->Modeloentregasdeldia->getListaRentast($params); 
        }
        if($tipo==5){
            $productos = $this->Modeloentregasdeldia->getListaRentash($params);
            $totalRecords=$this->Modeloentregasdeldia->getListaRentasht($params); 
        }
        if($tipo==4){
            $productos = $this->Modeloentregasdeldia->getListaVentasc($params);
            $totalRecords=$this->Modeloentregasdeldia->getListaVentasct($params); 
        }
        
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totalRecords),  
            "recordsFiltered" => intval($totalRecords),
            "data"            => $productos->result(),
            "query"           =>$this->db->last_query()   
        );
        
        echo json_encode($json_data);
    }
    public function verificar_fecha_vencimiento(){
        $id = $this->input->post('id');
        $ven='';
        $aux='';
        $arraywhere = array('ventaId'=>$id);
        $result=$this->ModeloCatalogos->getselectwheren_2('vencimiento','prefactura',$arraywhere);
        foreach ($result as $item) {
           $ven=$item->vencimiento;
        }
        if($ven=='0000-00-00'){
           $aux='';
        }else{
           $aux=$ven;
        }
        echo $aux;
    }
    public function update_cfdi_registro(){
        $id_venta = $this->input->post('id_venta');

        //$data[''] = $this->input->post('tipo');
        $cfdi = $this->input->post('cfdi');
        $arraydata = array('ventaId' =>$id_venta);
        $data = array('cfdi'=>$cfdi);
        $this->ModeloCatalogos->updateCatalogo('prefactura',$data,$arraydata);
    }
    public function guardar_pago_tipo(){
        $data = $this->input->post();
        $tipo = $data['tipo'];
        $idcompra = $data['idcompra'];
        $data['idpersonal']=$this->idpersonal;
        unset($data['idcompra']);
        unset($data['tipo']);
        if($tipo==1){//venta
            $data['idventa']=$idcompra;
            $this->ModeloCatalogos->Insert('pagos_ventas',$data);
        }else if($tipo==3){//poliza
            $data['idpoliza']=$idcompra;
            $this->ModeloCatalogos->Insert('pagos_poliza',$data);
        }else if($tipo==4){//combinada
            $data['idventa']=$idcompra;
            $this->ModeloCatalogos->Insert('pagos_combinada',$data);
        }   
    }
    function searchclientes(){
        $pro = $this->input->get('search');
        $results=$this->ModeloCatalogos->getclienterazonsociallike($pro);
        //echo $results;
        echo json_encode($results->result());
    }
    function montoventas(){//ventas
        $id=$this->input->post('id');
        $totalgeneral=0;
        $rowequipos=$this->ModeloCatalogos->getselectwheren('ventas_has_detallesEquipos',array('idVenta'=>$id));
        foreach ($rowequipos->result() as $item) {
            $totalgeneral=$totalgeneral+($item->cantidad*$item->precio);
        }
        $resultadoaccesorios = $this->ModeloCatalogos->accesoriosventa($id);
        foreach ($resultadoaccesorios->result() as $item) {
            $totalgeneral=$totalgeneral+($item->cantidad*$item->costo);
        }
        $resultadoconsumibles = $this->ModeloCatalogos->consumiblesventa($id);
        foreach ($resultadoconsumibles->result() as $item) { 
            $totalgeneral=$totalgeneral+($item->cantidad*$item->costo_toner);
        }
        $consumiblesventadetalles = $this->ModeloCatalogos->consumiblesventadetalles($id);
        foreach ($consumiblesventadetalles->result() as $item) { 
            $totalgeneral=$totalgeneral+($item->cantidad*$item->costo_toner);
        }
        $ventadetallesrefacion = $this->ModeloCatalogos->ventas_has_detallesRefacciones($id);
        foreach ($ventadetallesrefacion->result() as $item) { 
            $totalgeneral=$totalgeneral+($item->cantidad*$item->precioGeneral);
        }
        $totalgeneral=$totalgeneral+($totalgeneral*0.16);

        echo round($totalgeneral, 2); 
    }
    function montorentas($id){
        //dejar pendinete este esta funcion no va en este modulo
    }
    function montopoliza(){//polizas creadas
        $id=$this->input->post('id');
        $totalgeneral=0;
        $polizaventadetalles = $this->ModeloCatalogos->polizaventadetalles3($id);
        foreach ($polizaventadetalles->result() as $item) { 
            if($item->precio_local !==NULL) {
                $totalc=$item->precio_local;
            }
            if($item->precio_semi !==NULL) {
                $totalc=$item->precio_semi;
            }
            if($item->precio_foraneo !==NULL) {
                $totalc=$item->precio_foraneo;
            }
            $totalgeneral=$totalgeneral+($totalc);
            //$totalgeneral=$totalgeneral+($totalc*item->cantidad);
        }
        $totalgeneral=$totalgeneral+($totalgeneral*0.16);

        echo round($totalgeneral, 2); 
    }
    function montocombinada(){// convinadas
        $id=$this->input->post('id');
        $where_ventas=array('combinadaId'=>$id);
        $resultadov = $this->ModeloCatalogos->getselectwheren('ventacombinada',$where_ventas);
        $equipo=0;
        $consumibles=0;
        $refacciones=0;
        $poliza=0;
        foreach ($resultadov->result() as $item) {
                $equipo=$item->equipo;
                $consumibles=$item->consumibles;
                $refacciones=$item->refacciones;
                $poliza=$item->poliza;
        }
        $totalgeneral=0;
        $rowequipos=$this->ModeloCatalogos->getselectwheren('ventas_has_detallesEquipos',array('idVenta'=>$equipo));
        foreach ($rowequipos->result() as $item) {
            $totalgeneral=$totalgeneral+($item->cantidad*$item->precio);
        }
        $resultadoaccesorios = $this->ModeloCatalogos->accesoriosventa($equipo);
        foreach ($resultadoaccesorios->result() as $item) {
            $totalgeneral=$totalgeneral+($item->cantidad*$item->costo);
        }
        $resultadoconsumibles = $this->ModeloCatalogos->consumiblesventa($equipo);
        foreach ($resultadoconsumibles->result() as $item) { 
            $totalgeneral=$totalgeneral+($item->cantidad*$item->costo_toner);
        }
        $consumiblesventadetalles = $this->ModeloCatalogos->consumiblesventadetalles($consumibles);
        foreach ($consumiblesventadetalles->result() as $item) { 
            $totalgeneral=$totalgeneral+($item->cantidad*$item->costo_toner);
        }
        $ventadetallesrefacion = $this->ModeloCatalogos->ventas_has_detallesRefacciones($refacciones);
        foreach ($ventadetallesrefacion->result() as $item) { 
            $totalgeneral=$totalgeneral+($item->cantidad*$item->precioGeneral);
        }
        $polizaventadetalles = $this->ModeloCatalogos->polizaventadetalles2($poliza);
        foreach ($polizaventadetalles->result() as $item) { 
            if($item->precio_local !==NULL) {
                $totalc=$item->precio_local;
            }
            if($item->precio_semi !==NULL) {
                $totalc=$item->precio_semi;
            }
            if($item->precio_foraneo !==NULL) {
                $totalc=$item->precio_foraneo;
            }
            $totalgeneral=$totalgeneral+($totalc);
            //$totalgeneral=$totalgeneral+($totalc*item->cantidad);
            $totalgeneral=$totalgeneral+($totalgeneral*0.16);

            echo round($totalgeneral, 2); 
        }
    }
    function updatestateentrega(){
        $params = $this->input->post();
        $id    = $params['id'];
        $table = $params['table'];
        $e_entrega_p = $params['empleado'];
        if($table>0){
            if($table==1){
                $tablev='ventacombinada';
                $tablevid='combinadaId';
                $result_vc=$this->ModeloCatalogos->getselectwheren('ventacombinada',array('combinadaId'=>$id));
                $idVenta_e=0;
                $idVenta_c=0;
                $idVenta_r=0;
                foreach ($result_vc->result() as $itemvc) {
                    $idVenta_e=$itemvc->equipo;
                    $idVenta_c=$itemvc->consumibles;
                    $idVenta_r=$itemvc->refacciones;
                }
                if($idVenta_e>0){
                    $this->ModeloCatalogos->updateCatalogo('ventas_has_detallesEquipos',array('entregado_status'=>1,'entregado_personal'=>$e_entrega_p,'entregado_fecha'=>$this->fechahoy),array('idVenta'=>$idVenta_e));
                    $this->ModeloCatalogos->updateCatalogo('ventas_has_detallesEquipos_accesorios',array('entregado_status'=>1,'entregado_personal'=>$e_entrega_p,'entregado_fecha'=>$this->fechahoy),array('id_venta'=>$idVenta_e));
                    $this->ModeloCatalogos->updateCatalogo('ventas_has_detallesequipos_consumible',array('entregado_status'=>1,'entregado_personal'=>$e_entrega_p,'entregado_fecha'=>$this->fechahoy),array('idventa'=>$idVenta_e));
                }
                if($idVenta_c>0){
                    $this->ModeloCatalogos->updateCatalogo('ventas_has_detallesConsumibles',array('entregado_status'=>1,'entregado_personal'=>$e_entrega_p,'entregado_fecha'=>$this->fechahoy),array('idVentas'=>$idVenta_c));
                }
                if($idVenta_r>0){
                    $this->ModeloCatalogos->updateCatalogo('ventas_has_detallesRefacciones',array('entregado_status'=>1,'entregado_personal'=>$e_entrega_p,'entregado_fecha'=>$this->fechahoy),array('idVentas'=>$idVenta_r));
                }

            }elseif ($table==2) {
                $tablev='ventas';
                $tablevid='id';
                $this->ModeloCatalogos->updateCatalogo('ventas_has_detallesConsumibles',array('entregado_status'=>1,'entregado_personal'=>$e_entrega_p,'entregado_fecha'=>$this->fechahoy),array('idVentas'=>$id));
                $this->ModeloCatalogos->updateCatalogo('ventas_has_detallesEquipos',array('entregado_status'=>1,'entregado_personal'=>$e_entrega_p,'entregado_fecha'=>$this->fechahoy),array('idVenta'=>$id));
                $this->ModeloCatalogos->updateCatalogo('ventas_has_detallesEquipos_accesorios',array('entregado_status'=>1,'entregado_personal'=>$e_entrega_p,'entregado_fecha'=>$this->fechahoy),array('id_venta'=>$id));
                $this->ModeloCatalogos->updateCatalogo('ventas_has_detallesequipos_consumible',array('entregado_status'=>1,'entregado_personal'=>$e_entrega_p,'entregado_fecha'=>$this->fechahoy),array('idventa'=>$id));
                $this->ModeloCatalogos->updateCatalogo('ventas_has_detallesRefacciones',array('entregado_status'=>1,'entregado_personal'=>$e_entrega_p,'entregado_fecha'=>$this->fechahoy),array('idVentas'=>$id));
            }
            $this->ModeloCatalogos->updateCatalogo($tablev,array('e_entrega'=>1,'e_entrega_p'=>$e_entrega_p,'entregado_fecha'=>$this->fechahoy),array($tablevid=>$id));
        }
    }
    function updatestateentrega2(){
        $params = $this->input->post();
        $id    = $params['id'];
        $tipotable = $params['tipotable'];
        $e_entrega_p = $params['empleado'];
        if($tipotable>0){
            if($tipotable==2){
                $tablev='rentas';
                $tablevid='id';
                $this->ModeloCatalogos->updateCatalogo('rentas_has_detallesEquipos',array('entregado_status'=>1,'entregado_personal'=>$e_entrega_p,'entregado_fecha'=>$this->fechahoy),array('idRenta'=>$id));
                $this->ModeloCatalogos->updateCatalogo('rentas_has_detallesEquipos_accesorios',array('entregado_status'=>1,'entregado_personal'=>$e_entrega_p,'entregado_fecha'=>$this->fechahoy),array('idrentas'=>$id));
                $this->ModeloCatalogos->updateCatalogo('rentas_has_detallesequipos_consumible',array('entregado_status'=>1,'entregado_personal'=>$e_entrega_p,'entregado_fecha'=>$this->fechahoy),array('idrentas'=>$id));
            }
            if ($tipotable==5) {
                $tablev='rentas_historial';
                $tablevid='id';

                $result_his=$this->ModeloCatalogos->getselectwheren('rentas_historial_accesorios',array('historialid'=>$id));
                foreach ($result_his->result() as $item) {
                    if($item->id_accesoriod>0){
                        $this->ModeloCatalogos->updateCatalogo('rentas_has_detallesEquipos_accesorios',array('entregado_status'=>1,'entregado_personal'=>$e_entrega_p,'entregado_fecha'=>$this->fechahoy),array('id_accesoriod'=>$item->id_accesoriod));
                    }
                }
                $result_his=$this->ModeloCatalogos->getselectwheren('rentas_historial_equipos',array('historialid'=>$id));
                foreach ($result_his->result() as $item) {
                    if($item->equipo>0){
                        $this->ModeloCatalogos->updateCatalogo('rentas_has_detallesEquipos',array('entregado_status'=>1,'entregado_personal'=>$e_entrega_p,'entregado_fecha'=>$this->fechahoy),array('id'=>$item->equipo));
                    }
                }
                //==============
                    $result_hisc=$this->ModeloCatalogos->rentavconsumiblesh(0,$id);
                    foreach ($result_hisc->result() as $item) {
                        $idconcumibles=$item->id;
                        if ($idconcumibles>0) {
                            $this->ModeloCatalogos->updateCatalogo('rentas_has_detallesequipos_consumible',array('entregado_status'=>1,'entregado_personal'=>$e_entrega_p,'entregado_fecha'=>$this->fechahoy),array('id'=>$idconcumibles));
                        }
                    }
                //================

            }
            if ($tipotable==3) {
                $tablev='polizasCreadas';
                $tablevid='id';
            }
            $this->ModeloCatalogos->updateCatalogo($tablev,array('e_entrega'=>1,'e_entrega_p'=>$e_entrega_p,'entregado_fecha'=>$this->fechahoy),array($tablevid=>$id));
        }
    }
    function verificarmontoventas(){
        $params = $this->input->post();
        $ventas = $params['ventas'];
        $ventasrow = json_decode($ventas);
        $ventasrowi=[];
        for ($i=0;$i<count($ventasrow);$i++) { 
            $idventa=$ventasrow[$i]->idventa;
            $combinada=$ventasrow[$i]->combinada;
            $monto=$this->verificarmontoventascalcular($idventa,$combinada);
            $ventasrowi[]=array('idventa'=>$idventa,'combinada'=>$combinada,'monto'=>$monto);
        };
        echo json_encode($ventasrowi);
    }
    function verificarmontoventascalcular($idventa,$combinada){
        $total=0;
        if($combinada==1){
            $rowventac=$this->ModeloCatalogos->getselectwheren('ventacombinada',array('combinadaId'=>$idventa));
            $idv_equipo =0;$idv_consumibles =0;$idv_refacciones =0;$idv_poliza =0;
            foreach ($rowventac->result() as $item) {
                $idv_equipo=$item->equipo;
                $idv_consumibles=$item->consumibles;
                $idv_refacciones=$item->refacciones;
                $idv_poliza=$item->poliza;
            }
            
        }else{
            $idv_equipo=$idventa;
            $idv_consumibles=$idventa;
            $idv_refacciones=$idventa;
            $idv_poliza=$idventa;
        }



        if($idv_equipo>0){
            $rowventae=$this->ModeloCatalogos->getselectwheren('ventas_has_detallesEquipos',array('idVenta'=>$idv_equipo));
            foreach ($rowventae->result() as $item) {
                $total=$total+($item->cantidad*$item->precio);
            }
            $rowventaea=$this->ModeloCatalogos->getselectwheren('ventas_has_detallesEquipos_accesorios',array('id_venta'=>$idv_equipo));
            foreach ($rowventaea->result() as $item) {
                $total=$total+($item->cantidad*$item->costo);
            }
            $rowventaec=$this->ModeloCatalogos->getselectwheren('ventas_has_detallesequipos_consumible',array('idVenta'=>$idv_equipo));
            foreach ($rowventaec->result() as $item) {
                $total=$total+($item->cantidad*$item->costo_toner);
            }
        }
        if($idv_consumibles>0){
            $rowventac=$this->ModeloCatalogos->getselectwheren('ventas_has_detallesConsumibles',array('idVentas'=>$idv_consumibles));
            foreach ($rowventac->result() as $item) {
                $total=$total+($item->piezas*$item->precioGeneral);
            }
        }
        if($idv_refacciones>0){
            $rowventar=$this->ModeloCatalogos->getselectwheren('ventas_has_detallesRefacciones',array('idVentas'=>$idv_refacciones));
            foreach ($rowventar->result() as $item) {
                $total=$total+($item->piezas*$item->precioGeneral);
            }
        }
        if($idv_poliza>0){
            $rowventap=$this->ModeloCatalogos->montototalpoliza($idv_poliza);
            $total=$total+$rowventap;
        }
        return $total;
    }
    function entregashoy($fecha){
        $data['fecha']=$fecha;
        $data['ventan']=$this->Ventas_model->entregasventasnormal($fecha);//1
        $data['ventac']=$this->Ventas_model->entregasventascombinada($fecha);//4
        $data['ventarh']=$this->Ventas_model->entregasrentashistorial($fecha);//5
        $data['ventar']=$this->Ventas_model->entregasrentas($fecha);//5
        $this->load->view('ventas/entregashoy',$data);
    }
    function vertecnicov(){
        $params = $this->input->post();
        $ventas = $params['ventas'];
        $ventasrow = json_decode($ventas);
        $ventasrowi=[];
        
        if(count($ventasrow)>0){
            $this->db->select("concat(pt.nombre,' ',pt.apellido_paterno,' ',pt.apellido_materno) as tecnico,av.ventaId,pt.personalId");
            $this->db->from('asignacion_ser_venta_a av');
            $this->db->join('personal pt', 'pt.personalId=av.tecnico');
            $this->db->where(array('av.tipov'=>0));

            $this->db->group_start();
            for ($i=0;$i<count($ventasrow);$i++) { 
                $this->db->or_like('av.ventaId',$ventasrow[$i]->idventa);
            }
            $this->db->group_end();
            $query=$this->db->get();
            foreach ($query->result() as $item) {
                $ventasrowi[]=array('tecnico'=>$item->tecnico,'venta'=>$item->ventaId,'tipo'=>1);
                $personalId=$item->personalId;
                if($personalId>0){
                    $this->ModeloCatalogos->updateCatalogo('ventas',array('tec_ser'=>$personalId),array('id'=>$item->ventaId));
                }
            }

            for ($j=0;$j<count($ventasrow);$j++) { 
                $personalId=0;
                $idventa=$ventasrow[$j]->idventa;
                $strq="SELECT v_ser_tipo,v_ser_id FROM ventas WHERE id=$idventa AND v_ser_tipo>0";
                $query_v = $this->db->query($strq);
                foreach ($query_v->result() as $itemv) {


                    if($itemv->v_ser_tipo==1){
                        $strq_1="SELECT concat(pt.nombre,' ',pt.apellido_paterno,' ',pt.apellido_materno) as tecnico,asigd.asignacionId,pt.personalId
                                    FROM asignacion_ser_contrato_a_e as asigd 
                                    INNER JOIN personal as pt on pt.personalId=asigd.tecnico
                                    WHERE asigd.asignacionId='$itemv->v_ser_id' LIMIT 1";
                        $query_v1 = $this->db->query($strq_1);
                        foreach ($query_v1->result() as $itemv_1) {
                            if($itemv_1->asignacionId>0){
                                $ventasrowi[]=array('tecnico'=>$itemv_1->tecnico,'venta'=>$idventa,'tipo'=>1);
                                $personalId=$itemv_1->personalId;
                            }
                        }
                    }
                    if($itemv->v_ser_tipo==2){
                        $strq_2="SELECT concat(pt.nombre,' ',pt.apellido_paterno,' ',pt.apellido_materno) as tecnico,asigd.asignacionId,pt.personalId
                                FROM asignacion_ser_poliza_a_e as asigd
                                INNER JOIN personal as pt on pt.personalId=asigd.tecnico
                                WHERE asigd.asignacionId='$itemv->v_ser_id' LIMIT 1";
                        $query_v2 = $this->db->query($strq_2);
                        foreach ($query_v2->result() as $itemv_2) {
                            if($itemv_2->asignacionId>0){
                                $ventasrowi[]=array('tecnico'=>$itemv_2->tecnico,'venta'=>$idventa,'tipo'=>1);
                                $personalId=$itemv_2->personalId;
                            }
                        }
                    }
                    if($itemv->v_ser_tipo==3){
                        $strq_3="SELECT concat(pt.nombre,' ',pt.apellido_paterno,' ',pt.apellido_materno) as tecnico,asig.asignacionId,pt.personalId
                                FROM asignacion_ser_cliente_a as asig 
                                INNER JOIN personal as pt on pt.personalId=asig.tecnico
                                WHERE asig.asignacionId ='$itemv->v_ser_id'";
                        $query_v3 = $this->db->query($strq_3);
                        foreach ($query_v3->result() as $itemv_3) {
                            if($itemv_3->asignacionId>0){
                                $ventasrowi[]=array('tecnico'=>$itemv_3->tecnico,'venta'=>$idventa,'tipo'=>1);
                                $personalId=$itemv_3->personalId;
                            }
                        }
                    }
                    if($itemv->v_ser_tipo==4){
                        $strq_4 = "SELECT concat(pt.nombre,' ',pt.apellido_paterno,' ',pt.apellido_materno) as tecnico,av.ventaId,pt.personalId
                        FROM asignacion_ser_venta_a av 
                        INNER JOIN personal pt on pt.personalId=av.tecnico
                        WHERE av.asignacionId=$itemv->v_ser_id";
                        $query_v4 = $this->db->query($strq_4);
                        foreach ($query_v4->result() as $itemv_4) {
                            $ventasrowi[]=array('tecnico'=>$itemv_4->tecnico,'venta'=>$idventa,'tipo'=>1);
                            $personalId=$itemv_4->personalId;
                        }
                    }
                }
                //===========================================================
                    $consumiblesventadetalles = $this->ModeloCatalogos->consumiblesventadetalles($idventa);
                    $cf_serviciocId_tipo=0;$cf_serviciocId=0;
                    foreach ($consumiblesventadetalles->result() as $item) {
                        $cf_serviciocId_tipo=$item->serviciocId_tipo;
                        $cf_serviciocId=$item->serviciocId;
                    }
                    if($cf_serviciocId>0){
                            $html_v='';
                            if($cf_serviciocId_tipo==0){
                                $resultado=$this->ModeloAsignacion->getdatosrenta($cf_serviciocId,'','',0);
                                $asignacionId_view=0;
                                foreach ($resultado->result() as $itemser) {
                                    $servicio = $itemser->servicio;
                                    $fecha = $itemser->fecha;
                                    $tecnico = $itemser->tecnico.', '.$itemser->tecnico2;
                                    if($itemser->tservicio_a>0){
                                        $servicio = $itemser->poliza;
                                    }
                                    if($asignacionId_view!=$itemser->asignacionId){
                                        $asignacionId_view=$itemser->asignacionId;
                                        $ventasrowi[]=array('tecnico'=>$tecnico,'venta'=>$idventa,'tipo'=>1);
                                        $personalId=$itemser->personalId1;
                                    }
                                }
                            }
                            if($cf_serviciocId_tipo==1){
                                $resultado=$this->ModeloAsignacion->getdatospoliza($cf_serviciocId,'',0);
                                foreach ($resultado->result() as $itemser) {
                                    $servicio = $itemser->servicio;
                                    $fecha = $itemser->fecha;
                                    $tecnico = $itemser->tecnico.', '.$itemser->tecnico2;
                                    $ventasrowi[]=array('tecnico'=>$tecnico,'venta'=>$idventa,'tipo'=>1);
                                    $personalId=$itemser->personalId1;
                                }
                            }
                            echo $html_v;
                        }
                //===========================================================
                if($personalId>0){
                    $this->ModeloCatalogos->updateCatalogo('ventas',array('tec_ser'=>$personalId),array('id'=>$idventa));
                }
            }
            
        }
        echo json_encode($ventasrowi);
    }
    function vertecnicovc(){
        $params = $this->input->post();
        $ventas = $params['ventas'];
        $ventasrow = json_decode($ventas);
        $ventasrowi=[];
        if(count($ventasrow)>0){
            $this->db->select("concat(pt.nombre,' ',pt.apellido_paterno,' ',pt.apellido_materno) as tecnico,av.ventaId,pt.personalId");
            $this->db->from('asignacion_ser_venta_a av');
            $this->db->join('personal pt', 'pt.personalId=av.tecnico');
            $this->db->where(array('av.tipov'=>1));

            $this->db->group_start();
            for ($i=0;$i<count($ventasrow);$i++) { 
                $this->db->or_like('av.ventaId',$ventasrow[$i]->idventa);
            }
            $this->db->group_end();
            $query=$this->db->get();
            foreach ($query->result() as $item) {
                $ventasrowi[]=array('tecnico'=>$item->tecnico,'venta'=>$item->ventaId,'tipo'=>1);
                $personalId=$item->personalId;
                if($personalId>0){
                    $this->ModeloCatalogos->updateCatalogo('ventacombinada',array('tec_ser'=>$personalId),array('combinadaId'=>$item->ventaId));
                }
            }
        }
        if(count($ventasrow)>0){
            $this->db->select("concat(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno) as tecnico,vc.combinadaId,ap.asignacionId,per.personalId");
            $this->db->from('asignacion_ser_poliza_a ap');
            $this->db->join('asignacion_ser_poliza_a_e apd', 'ap.asignacionId=apd.asignacionId');
            $this->db->join('personal per', 'per.personalId=apd.tecnico');
            $this->db->join('ventacombinada vc', 'vc.poliza=ap.polizaId ');

            $this->db->group_start();
            for ($i=0;$i<count($ventasrow);$i++) { 
                $this->db->or_like('vc.combinadaId',$ventasrow[$i]->idventa);
            }
            $this->db->group_end();
            $query=$this->db->get();
            foreach ($query->result() as $item) {
                $ventasrowi[]=array('tecnico'=>$item->tecnico,'venta'=>$item->combinadaId,'tipo'=>4);
                $personalId=$item->personalId;
                if($personalId>0){
                    $this->ModeloCatalogos->updateCatalogo('ventacombinada',array('tec_ser'=>$personalId),array('combinadaId'=>$item->combinadaId));
                }
            }

            for ($j=0;$j<count($ventasrow);$j++) { 
                $idventa=$ventasrow[$j]->idventa;
                $personalId=0;
                $strq="SELECT v_ser_tipo,v_ser_id,equipo FROM ventacombinada WHERE combinadaId=$idventa AND v_ser_tipo>0";
                $query_v = $this->db->query($strq);
                foreach ($query_v->result() as $itemv) {
                    $idventa_equipo=$itemv->equipo;

                    if($itemv->v_ser_tipo==1){
                        $strq_1="SELECT concat(pt.nombre,' ',pt.apellido_paterno,' ',pt.apellido_materno) as tecnico,asigd.asignacionId,pt.personalId
                                    FROM asignacion_ser_contrato_a_e as asigd 
                                    INNER JOIN personal as pt on pt.personalId=asigd.tecnico
                                    WHERE asigd.asignacionId='$itemv->v_ser_id' LIMIT 1";
                        $query_v1 = $this->db->query($strq_1);
                        foreach ($query_v1->result() as $itemv_1) {
                            if($itemv_1->asignacionId>0){
                                $ventasrowi[]=array('tecnico'=>$itemv_1->tecnico,'venta'=>$idventa,'tipo'=>4);
                                $personalId=$itemv_1->personalId;
                            }
                        }
                    }
                    if($itemv->v_ser_tipo==2){
                        $strq_2="SELECT concat(pt.nombre,' ',pt.apellido_paterno,' ',pt.apellido_materno) as tecnico,asigd.asignacionId,pt.personalId
                                FROM asignacion_ser_poliza_a_e as asigd
                                INNER JOIN personal as pt on pt.personalId=asigd.tecnico
                                WHERE asigd.asignacionId='$itemv->v_ser_id' LIMIT 1";
                        $query_v2 = $this->db->query($strq_2);
                        foreach ($query_v2->result() as $itemv_2) {
                            if($itemv_2->asignacionId>0){
                                $ventasrowi[]=array('tecnico'=>$itemv_2->tecnico,'venta'=>$idventa,'tipo'=>4);
                                $personalId=$itemv_2->personalId;
                            }
                        }
                    }
                    if($itemv->v_ser_tipo==3){
                        $strq_3="SELECT concat(pt.nombre,' ',pt.apellido_paterno,' ',pt.apellido_materno) as tecnico,asig.asignacionId,pt.personalId
                                FROM asignacion_ser_cliente_a as asig 
                                INNER JOIN personal as pt on pt.personalId=asig.tecnico
                                WHERE asig.asignacionId ='$itemv->v_ser_id'";
                        $query_v3 = $this->db->query($strq_3);
                        foreach ($query_v3->result() as $itemv_3) {
                            if($itemv_3->asignacionId>0){
                                $ventasrowi[]=array('tecnico'=>$itemv_3->tecnico,'venta'=>$idventa,'tipo'=>4);
                                $personalId=$itemv_3->personalId;
                            }
                        }
                    }
                    if($itemv->v_ser_tipo==4){
                        $strq_4 = "SELECT concat(pt.nombre,' ',pt.apellido_paterno,' ',pt.apellido_materno) as tecnico,av.ventaId,pt.personalId
                        FROM asignacion_ser_venta_a av 
                        INNER JOIN personal pt on pt.personalId=av.tecnico
                        WHERE av.asignacionId=$itemv->v_ser_id";
                        $query_v4 = $this->db->query($strq_4);
                        foreach ($query_v4->result() as $itemv_4) {
                            $ventasrowi[]=array('tecnico'=>$itemv_4->tecnico,'venta'=>$idventa,'tipo'=>4);
                            $personalId=$itemv_4->personalId;
                        }
                    }
                }
                if($personalId>0){
                    $this->ModeloCatalogos->updateCatalogo('ventacombinada',array('tec_ser'=>$personalId),array('combinadaId'=>$idventa));
                }
            }
        }

        echo json_encode($ventasrowi);
    }
    function vertecnicorh(){
        $params = $this->input->post();
        $ventas = $params['ventas'];
        $ventasrow = json_decode($ventas);
        $ventasrowi=[];
        if(count($ventasrow)>0){
            for ($i=0;$i<count($ventasrow);$i++) { 

                
                $rentaventash = $this->ModeloCatalogos->rentaventasdh($ventasrow[$i]->renta_rh,$ventasrow[$i]->id_rh);
                foreach ($rentaventash->result() as $item) {
                    $id_rh=$item->id_rh;
                    $rentaventashe = $this->ModeloCatalogos->vertecnicorh($item->id);
                    foreach ($rentaventashe->result() as $iteme) {
                        $ventasrowi[]=array('tecnico'=>$iteme->nombre.' '.$iteme->apellido_paterno.' '.$iteme->apellido_materno,'venta'=>$ventasrow[$i]->id,'tipo'=>5);
                        if($iteme->personalId>0){
                            $this->ModeloCatalogos->updateCatalogo('rentas_historial',array('tec_ser'=>$iteme->personalId),array('id'=>$id_rh));
                        }
                    }
                }
            }
        }
        echo json_encode($ventasrowi);
    }
    function vertecnicor(){
        $params = $this->input->post();
        $ventas = $params['ventas'];
        $ventasrow = json_decode($ventas);
        $ventasrowi=[];
        if(count($ventasrow)>0){
            for ($i=0;$i<count($ventasrow);$i++) { 
                $rowventae=$this->ModeloCatalogos->getselectwheren('rentas_has_detallesEquipos',array('idRenta'=>$ventasrow[$i]->id));
                $this->db->select("per.nombre,per.apellido_paterno,per.apellido_materno,per.personalId");
                $this->db->from('asignacion_ser_contrato_a ac');
                $this->db->join('asignacion_ser_contrato_a_e acd', 'acd.asignacionId=ac.asignacionId');
                $this->db->join('personal per', 'per.personalId=acd.tecnico');
                $this->db->where(array('ac.activo'=>1,'acd.activo'=>1,'ac.tpoliza'=>17));
                $this->db->group_start();
                foreach ($rowventae->result() as $item) {
                    $this->db->or_like('acd.idequipo',$item->id);
                }
                $this->db->group_end();
                $this->db->group_by("per.personalId");
                $query=$this->db->get();
                $tecnicos='';
                if($query->num_rows()>0){
                    foreach ($query->result() as $iteme) {
                        $tecnicos.=$iteme->nombre.' '.$iteme->apellido_paterno.' '.$iteme->apellido_materno.', ';
                        if($iteme->personalId>0){
                            $this->ModeloCatalogos->updateCatalogo('rentas',array('tec_ser'=>$iteme->personalId),array('id'=>$ventasrow[$i]->id));
                        }
                    }
                    $ventasrowi[]=array('tecnico'=>$tecnicos,'venta'=>$ventasrow[$i]->id,'tipo'=>2);
                }
            }
        }
        echo json_encode($ventasrowi);
    }
    function verificarstatusnrdev(){
        $params = $this->input->post();
        $ventas = $params['ventas'];
        $ventasrow = json_decode($ventas);
        $ventasrowi=[];
        if(count($ventasrow)>0){
            for ($i=0;$i<count($ventasrow);$i++) { 
                $id = $ventasrow[$i]->id;
                $idhostorial = $ventasrow[$i]->idhostorial;
                $text_series='';
                $status_series=0;
                //===========================================
                    $idcontrato=0;
                    $contrato = $this->ModeloCatalogos->db6_getselectwheren('contrato',array('idRenta'=>$id));
                    foreach ($contrato->result() as $item) {
                        $idcontrato=$item->idcontrato;
                    }
                //===========================================

                $rentaventash = $this->ModeloCatalogos->rentaventasdh($id,$idhostorial);
                foreach ($rentaventash->result() as $item) {
                    $datosseries = $this->ModeloCatalogos->rentaventasdseries($item->id);
                    foreach($datosseries->result() as $items) {
                        $serieId=$items->serieId;
                        $series=$items->serie;
                        $info_valid = $this->ModeloCatalogos->ver_not_dev_series($idcontrato,$serieId);
                        if($info_valid->num_rows()>0){
                            $text_series.=$series.'<br>';
                            $status_series=1;
                        }
                    }
                }
                if($status_series==1){
                    $ventasrowi[]=array('id'=>$id,'idhostorial'=>$idhostorial,'seriesdev'=>$text_series);
                }

                
                /*
                $rowventae=$this->ModeloCatalogos->getselectwheren('rentas_has_detallesEquipos',array('idRenta'=>$ventasrow[$i]->id));
                $this->db->select("per.nombre,per.apellido_paterno,per.apellido_materno");
                $this->db->from('asignacion_ser_contrato_a ac');
                $this->db->join('asignacion_ser_contrato_a_e acd', 'acd.asignacionId=ac.asignacionId');
                $this->db->join('personal per', 'per.personalId=acd.tecnico');
                $this->db->where(array('ac.activo'=>1,'acd.activo'=>1,'ac.tpoliza'=>17));
                $this->db->group_start();
                foreach ($rowventae->result() as $item) {
                    $this->db->or_like('acd.idequipo',$item->id);
                }
                $this->db->group_end();
                $this->db->group_by("per.personalId");
                $query=$this->db->get();
                $tecnicos='';
                if($query->num_rows()>0){
                    foreach ($query->result() as $iteme) {
                        $tecnicos.=$iteme->nombre.' '.$iteme->apellido_paterno.' '.$iteme->apellido_materno.', ';
                    }
                    $ventasrowi[]=array('tecnico'=>$tecnicos,'venta'=>$ventasrow[$i]->id,'tipo'=>2);
                }
                */
            }
        }
        echo json_encode($ventasrowi);
    }
    function checadopre(){
        $params= $this->input->post();
        $id = $params['id'];
        $tabla = $params['tabla'];
        $check = $params['check'];

        if($tabla==1){
            $this->ModeloCatalogos->updateCatalogo('ventas',array('checada'=>$check),array('id'=>$id));
        }
        if($tabla==2){
            $this->ModeloCatalogos->updateCatalogo('rentas',array('checada'=>$check),array('id'=>$id));
        }
        if($tabla==4){
            $this->ModeloCatalogos->updateCatalogo('ventacombinada',array('checada'=>$check),array('combinadaId'=>$id));
        }
        if($tabla==5){
            $this->ModeloCatalogos->updateCatalogo('rentas_historial',array('checada'=>$check),array('id'=>$id));
        }
        
    }
    function pre_pendientes(){
        
        $strq0="SELECT * FROM ventas as v where v.activo=1 and 
                    v.combinada=0 and 
                    v.viewed=1 and 
                    v.add_report=0 and
                    v.fechaentrega>=DATE_SUB(NOW(),INTERVAL '1' DAY)";
        $strq1="SELECT * FROM ventacombinada as vc 
        where vc.activo=1 and 
                    vc.viewed=1 and 
                    vc.fechaentrega>=DATE_SUB(NOW(),INTERVAL '1' DAY)";
        $strq2="SELECT *
                    from rentas as v
                    left join prefactura as pre on pre.ventaId=v.id and pre.tipo=2
                    WHERE 
                    v.add_report=0 and v.estatus !=0 and v.viewed=1 and pre.vencimiento>=DATE_SUB(NOW(),INTERVAL '1' DAY)";
        $strq3="SELECT *
                FROM rentas_historial as rh
                INNER JOIN prefactura as pre on pre.ventaId=rh.id AND pre.tipo=4
                INNER JOIN rentas as ren on ren.id=rh.rentas
                WHERE 
                    rh.add_report=0 and
                    rh.activo=1 and 
                    rh.viewed=1 and 
                    rh.fechaentrega>=DATE_SUB(NOW(),INTERVAL '1' DAY)";

        $query0 = $this->db->query($strq0);
        $query1 = $this->db->query($strq1);
        $query2 = $this->db->query($strq2);
        $query3 = $this->db->query($strq3);

        $cant0=$query0->num_rows();
        $cant1=$query1->num_rows();
        $cant2=$query2->num_rows();
        $cant3=$query3->num_rows();

        $total=$cant0+$cant1+$cant2+$cant3;

        echo $total;
    }

}

?>