<?php
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloUsuarios extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    
    public function personalnuevo($nom,$ape,$perf){
            $strq = "INSERT INTO personal (perfilId, Nombre, Apellidos) VALUES ('".$perf."','".$nom."','".$ape."')";
            $this->db->query($strq);
    }
    function getusuarios() {
        $strq = "SELECT usu.UsuarioID,usu.Usuario,per.nombre  FROM usuarios as usu,perfiles as per where usu.perfilId=per.perfilId";
        $query = $this->db->query($strq);
        return $query;
    }
    function getempreado() {
        $strq = "CALL SP_GET_ALL_PERSONAL";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function getperfiles() {
        $strq = "CALL SP_GET_ALL_PERFILES";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function mostrardatos($id){
        $strq = "SELECT * FROM usuarios  where UsuarioID=$id";
        $query = $this->db->query($strq);
        return $query;

    }
    public function usuariosinsert($id,$usu,$pass,$perf,$perfs){
            $pass = password_hash($pass, PASSWORD_BCRYPT);
            $strq = "INSERT INTO usuarios (perfilId, personalId,Usuario, contrasena) VALUES ('$perf','$perfs','$usu','$pass')";
            $this->db->query($strq);
    }
    public function usuariosupdate($id,$usu,$pass,$perf,$perfs){
            
            if ($pass=='xxxx') {
                $strq = "UPDATE usuarios SET perfilId='$perf', personalId='$perfs',Usuario='$usu' WHERE UsuarioID='$id'";
            }else{
                $pass = password_hash($pass, PASSWORD_BCRYPT);
                $strq = "UPDATE usuarios SET perfilId='$perf', personalId='$perfs',Usuario='$usu', contrasena='$pass' WHERE UsuarioID='$id'";
            }
    
            $this->db->query($strq);
    }
    function verfusu($usu) {
        $strq = "SELECT *  FROM usuarios where Usuario ='$usu'";
        $query = $this->db->query($strq);
        return $query;
    }
    function mostrarusuarios(){
        $strq = "CALL SP_GET_USUARIOS";
        $query = $this->db->query($strq);
        return $query;  
    }

    

}
