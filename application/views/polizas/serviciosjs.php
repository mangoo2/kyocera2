<link href="<?php echo base_url(); ?>public/plugins/select2/select2.min.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/select2/select2.full.min.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/fileinput/fileinput.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/plugins/fileinput/fileinput.css">
<script type="text/javascript" src="<?php echo base_url(); ?>public/js/polizas_cont.js?v=<?php echo date('YmdGis')?>" ></script>

<script type="text/javascript">
    var base_url = $('#base_url').val();
	var idPoliza = $('#idPoliza').val();
	var urlredirigie4=base_url+'/PolizasCreadas/servicios/'+idPoliza+'#test4';
	$(document).ready(function($) {
        <?php if(isset($_GET['pes'])){
            ?>$('#test<?php echo $_GET['pes']; ;?>_c').click();<?php
        } ?>
        viewagenda();
        $('#table_pol').DataTable();
        
        $('#select_eq').select2();
        
		<?php if ($rfc_id>0) { ?>
				selectrcfv(<?php echo $rfc_id;?>);
		<?php } ?>
		copiardatoscc();
		$('#table_servicio').DataTable();

		$("#filepoliza").fileinput({
            showCaption: false,
            showUpload: true,// quita el boton de upload
            dropZoneEnabled: false,//minimiza el preview y solo se despliea al cargar un archivo
            //rtl: true,
            allowedFileExtensions: ["jpg","png","webp","jpeg","pdf"],
            browseLabel: 'Seleccionar documento',
            uploadUrl: base_url+'PolizasCreadas/filepoliza',
            maxFilePreviewSize: 5000,
            previewFileIconSettings: {
                'docx': '<i class="fa fa-file-word-o text-primary"></i>',
                'xlsx': '<i class="fa fa-file-excel-o text-success"></i>',
                'pptx': '<i class="fa fa-file-powerpoint-o text-danger"></i>',
                'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
                'pdf': '<i class="fa fa-file-pdf-o text-danger"></i>',
                'zip': '<i class="fa fa-file-archive-o text-muted"></i>',
                'cer': '<i class="fas fa-file-invoice"></i>',
            },
            uploadExtraData: function (previewId, index) {
            var info = {
                        idPoliza:idPoliza
                    };
            return info;
          }
        }).on('filebatchuploadcomplete', function(event, files, extra) {
          //location.reload();
            toastr.success('Se cargo Correctamente','Hecho!');
            console.log(urlredirigie4);
              //location.reload();
            location.href =base_url+'/PolizasCreadas/servicios/'+idPoliza+'?tang=4';
        }).on('filebatchuploadsuccess', function(event, files, extra) {
          //location.reload();
          toastr.success('Se cargo Correctamente','Hecho!');
          console.log(urlredirigie4);
          //location.reload();
          location.href =base_url+'/PolizasCreadas/servicios/'+idPoliza+'?tang=4';
        });

        $('#okagregareq').click(function(event) {
            $( ".okagregareq" ).prop( "disabled", true );
            setTimeout(function(){ 
                 $(".okagregareq" ).prop( "disabled", false );
            }, 3000);

            $.confirm({
                boxWidth: '40%',
                useBootstrap: false,
                icon: 'fa fa-warning',
                title: 'Agregar Equipo',
                content: '<div style="width: 99%;">¿Desea agregar nuevos equipos?<br>Se necesita permisos de administrador<br><input type="password" id="pass_cambio" name="pass_cambio" class="form-control-bmz"></div>',
                type: 'red',
                typeAnimated: true,
                buttons:{
                    confirmar: function (){
                        var pass=$('#pass_cambio').val();
                        //============================================
                            var productos = $("#table_equipos_new tbody > tr");
                            //==============================================
                                var DATAa  = [];
                                productos.each(function(){         
                                    item = {};                    
                                    item ["eq"]   = $(this).find("input[id*='eq_s']").val();
                                    item ["serie"]  = $(this).find("input[id*='ser_s']").val();
                                    item ["dir"]  = $(this).find("input[id*='dir_s']").val();
                                    DATAa.push(item);
                                });
                                INFOa  = new FormData();
                                aInfoa   = JSON.stringify(DATAa);
                            //========================================
                            var datos='idPoliza='+idPoliza+'&conceptos='+aInfoa+'&pass='+pass;
                            $.ajax({
                                    type:'POST',
                                    url: base_url+"index.php/PolizasCreadas/agregarequiposcont",
                                    data: datos,
                                    success: function (response){
                                        var permi=parseInt(response);
                                        if(permi>0){
                                            toastr["success"]("Equipos agregados");
                                            setTimeout(function(){ 
                                                location.reload();
                                            }, 2000);
                                        }else{
                                            toastr.error("No se tiene permisos");
                                        }
                                        
                                        
                                    },
                                    error: function(response){
                                        toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema");
                                        
                                    }
                                });
                            //===========================================================
                    },
                    cancelar: function (){
                        
                    }
                }
            });

        });
        <?php 
            if(isset($_GET['tang'])){
                ?>
                    $('#test<?php echo $_GET['tang'];?>_c').click();
                <?php
            }
        ?>
	});
	function selecrfcp() {
			var rfc_id = $('#rfc_id option:selected').val();
			selectrcfv(rfc_id);	
		}
	function selectrcfv(rfc_id){
		$.ajax({
            url: base_url+'Prefactura/selecciontrcf',
            dataType: 'json',
            data:{id:rfc_id},
            statusCode:{
                    404: function(data){
                        swal("Error", "404", "error");
                    },
                    500: function(){
                        swal("Error", "500", "error"); 
                    }
                },
                success:function(data){
                	var datos=data;
                    datos.forEach(function(r) {
                    if(r.rfc=='XAXX010101000'){
                    		$('#razon_social').val(r.razon_social+' ('+r.empresa+')');
                    		$('.quitariva').attr('onclick', 'quitariva(<?php echo $ventaId ?>,2)');
                    }else{
                    		$('#razon_social').val(r.razon_social);
                    }
                    <?php if ($rfc_id==0) { ?>
                    	$('#num_ext').val(r.num_ext);
                    	$('#colonia').val(r.colonia);
                    	$('#calle').val(r.calle);
                	<?php } ?>
                    });
                }
        });
	}
	function copiardatoscc(){
			var id = $('#contactoId option:selected').val();
			var email = $('#contactoId option:selected').data('dcemail');
			var puesto = $('#contactoId option:selected').data('dcpuesto');
			$('#cargo').val(puesto);
			$('.dcemail').val(email);
			$('#telId').val(id);
	}
	function documento(idasig,tipo,equipo){
	    if(tipo==1){
	        window.open(base_url+"Listaservicios/contrato_formato/"+idasig+"/"+tipo+"/"+equipo, "Prefactura", "width=780, height=612");
	    }else if(tipo==2){
	        window.open(base_url+"Listaservicios/tipo_formato/"+idasig+"/"+tipo+"/"+equipo, "Prefactura", "width=780, height=612");
	    }else if(tipo==3){
	        window.open(base_url+"Listaservicios/tipo_formatoc/"+idasig+"/"+tipo+"/"+equipo, "Prefactura", "width=780, height=612");
	    }else if(tipo==4){
	        window.open(base_url+"Listaservicios/tipo_formatovent/"+idasig+"/"+tipo+"/"+equipo, "Prefactura", "width=780, height=612");
	    }
	} 
	function viewprefacturap(id){
    	window.open(base_url+"PolizasCreadas/view/"+id, "Prefactura", "width=780, height=612");
	}
	function dejarderealizarservicios(id){
		$.confirm({
            boxWidth: '30%',
            useBootstrap: false,
            icon: 'fa fa-warning',
            title: 'Atención!',
            content: '¿Desea de dejar de realizar servicios a la poliza?',
            type: 'red',
            typeAnimated: true,
            buttons:{
                confirmar: function (){
                    
                         $.ajax({
                            type:'POST',
                            url: base_url+"index.php/PolizasCreadas/dejarderealizarservicios",
                            data: {
                                id:id
                            },
                            success: function (response){
                                    toastr["success"]("Cambio realizado");
                            },
                            error: function(response){
                                toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema");
                            }
                        });
                        
                    
                },
                cancelar: function (){
                    
                }
            }
        });
	}
    function agregar_equipo(){
        $('#modal_addequipo').modal('open');
    }
    var row=0;
    function agregar_eq(){
        var eqid=$('#select_eq option:selected').val();
        var eqid_t=$('#select_eq option:selected').text();
        var ser=$('#select_ser').val();
        var sdir=$('#s_direccion_s option:selected').val();
        if(ser!=''){
            var html='<tr class="s_delete_equipo_'+row+'">\
                        <td><input type="hidden" id="eq_s" value="'+eqid+'">'+eqid_t+'</td>\
                        <td><input type="hidden" id="ser_s" value="'+ser+'">'+ser+'</td>\
                        <td><input type="hidden" id="dir_s" value="'+sdir+'">'+sdir+'</td>\
                        <td><a class="red btn-bmz" onclick="s_delete_equipo('+row+')"><i class="fas fa-trash"></i></a></td>\
                    </tr>';
            $('.tbody_equipos_new').append(html);
        }else{
            toastr.error('Favor de agregar una serie para el <br>modelo '+eqid_t);
        }
        row++;
    }
    function s_delete_equipo(id){
        $('.s_delete_equipo_'+id).remove();
    }
    
    function notificardeservicio(idp,fecha){
        var ht_equipos='';
        var productos = $(".table_equipos tbody > tr");
                            //==============================================
                                var DATAa  = [];
                                productos.each(function(){   
                                    var id_row=$(this).find("input[id*='id_row']").val(); 
                                    var modelo_row=$(this).find("input[id*='modelo_row']").val(); 
                                    var serie_row=$(this).find("input[id*='serie_row']").val(); 

                                    ht_equipos+='<tr><td><input type="checkbox" class="serie_check" id="serie_check_'+id_row+'" value="'+id_row+'"><label for="serie_check_'+id_row+'"></label></td><td>'+modelo_row+'</td><td>'+serie_row+'</td><td><input class="form-control-bmz" id="ser_eq_comen"></td></tr>';     
                                    
                                });
                                
                            //========================================
        //=======================================================================
        var html='';
            html+='<input name="tipo_pre_corr" type="radio" class="formt" id="tipo_pre_corr1" value="1" checked>\
                <label for="tipo_pre_corr1">Preventivo</label>\
                <input name="tipo_pre_corr" type="radio" class="formt" id="tipo_pre_corr2" value="2">\
                <label for="tipo_pre_corr2">Correctivo</label><br>';

            html+='<label>Favor de seleccionar lo equipos para el servicio!</label><br><table class="table table_equipos_selected"><tbody>';
            html+=ht_equipos
            html+='</tbody></table>';
        $.confirm({
                boxWidth: '30%',
                useBootstrap: false,
                icon: 'fa fa-warning',
                title: 'Atención!',
                content: '¿Confirma notificar que se requiere servicio? <br>Especifique la fecha para el servicio<br><input type="date" class="form-control-bmz" id="alert_date" style="width: 98%;" value="'+fecha+'">'+html,
                type: 'red',
                typeAnimated: true,
                buttons:{
                    confirmar: function (){
                                var select_eq=0;
                            var tipopc=$('input[name=tipo_pre_corr]:checked').val();
                        //=============================================
                            var productos = $(".table_equipos_selected tbody > tr");
                            //==============================================
                                var DATAa  = [];
                                productos.each(function(){         
                                    item = {};                    
                                    item ["idrow"]   = $(this).find("input[class*='serie_check']").val();
                                    item ["status"]   = $(this).find("input[class*='serie_check']").is(':checked')==true?1:0;
                                    item ["comen"]   = $(this).find("input[id*='ser_eq_comen']").val();
                                    if($(this).find("input[class*='serie_check']").is(':checked')){
                                        select_eq++;
                                    }
                                    
                                    DATAa.push(item);
                                });
                                INFOa  = new FormData();
                                aInfoa   = JSON.stringify(DATAa);
                            //========================================
                        //=============================================
                        var date=$('#alert_date').val();
                        if (date === "") {
                            toastr.error("Por favor, especifique la fecha para el servicio.");
                            return false;  // Evita que el cuadro de diálogo se cierre
                        }
                        if(select_eq==0){
                            toastr.error("Por favor, seleccione un equipo o mas.");
                            return false;  // Evita que el cuadro de diálogo se cierre
                        }
                        $.ajax({
                            type:'POST',
                            url: base_url+"index.php/PolizasCreadas/notificardeservicio",
                            data: {
                                id:idp,
                                date:date,
                                tipopc:tipopc,
                                info:aInfoa
                            },
                            success: function (response){
                                toastr["success"]("Se a realizado la notificacion");
                                
                            },
                            error: function(response){
                                toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema");
                            }
                        });
                            
                        
                    },
                    cancelar: function (){
                        
                    }
                }
            });
    }
    function activarsuspender(id,tipo){
        var titleas='';
        var titleast='';
        if(tipo==0){
            titleas='¿Desea activar el equipo?';
            titleast='Activar';
        }
        if(tipo==1){
            titleas='¿Desea Suspender el equipo?';
            titleast='Suspender';
        }
        $.confirm({
            boxWidth: '30%',
            useBootstrap: false,
            icon: 'fa fa-warning',
            title: titleast,
            content: titleas,
            type: 'red',
            typeAnimated: true,
            buttons:{
                confirmar: function (){
                    
                    $.ajax({
                        type:'POST',
                        url: base_url+"index.php/PolizasCreadas/activarsuspender",
                        data: {
                            id:id,
                            tipo:tipo
                        },
                        success: function (response){
                            toastr["success"]("Cambio realizado");
                            setTimeout(function(){ 
                                location.reload();
                            }, 2000);
                        },
                        error: function(response){
                            toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema");
                        }
                    });
                        
                    
                },
                cancelar: function (){
                    
                }
            }
        });
    }
    function viewagenda(){
        $.ajax({
            type:'POST',
            url: base_url+"index.php/PolizasCreadas/viewagenda",
            data: {
                idPoliza:idPoliza
            },
            success: function (response){
                $('.agefechastbody').html(response);
            },
            error: function(response){
                toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema");
            }
        });
    }
    function agregar_agenda(){
        var age=$('#fechaage').val();
        $.ajax({
            type:'POST',
            url: base_url+"index.php/PolizasCreadas/agregar_agenda",
            data: {
                idPoliza:idPoliza,
                age:age
            },
            success: function (response){
                viewagenda();
            },
            error: function(response){
                toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema");
            }
        });
    }
    function deleteagenda(id){
        $.confirm({
                boxWidth: '30%',
                useBootstrap: false,
                icon: 'fa fa-warning',
                title: 'Atención!',
                content: '¿Desea eliminar la agenda?',
                type: 'red',
                typeAnimated: true,
                buttons:{
                    confirmar: function (){
                        
                         $.ajax({
                            type:'POST',
                            url: base_url+"index.php/PolizasCreadas/deleteagenda",
                            data: {
                                id:id
                            },
                            success: function (response){
                                toastr["success"]("Fecha eliminada");
                                viewagenda();
                            },
                            error: function(response){
                                toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema");
                            }
                        });
                            
                        
                    },
                    cancelar: function (){
                        
                    }
                }
            });
    }

</script>