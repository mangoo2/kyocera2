<?php 
    require_once('TCPDF3/examples/tcpdf_include.php');
    require_once('TCPDF3/tcpdf.php');
    $this->load->helper('url');
    $htmlf='';
    $GLOBALS['facturasvencidas']=0;
    $GLOBALS['montovencido']=0;
    $GLOBALS['ultimafactura']=0;
    //=======================================================================================
    //$facturas = $this->ModeloCatalogos->viewprefacturaslis_general($idc);
    $datoscliente = $this->ModeloCatalogos->getselectwheren('clientes',array('id'=>$cliente));
    //$GLOBALS['folio']='';
    $GLOBALS['empresa']='';
    $GLOBALS['Direccion']='';
    foreach ($datoscliente->result() as $item) {
        //$GLOBALS['folio']=$item->folio;
        $GLOBALS['empresa']=$item->empresa;
        $GLOBALS['Direccion']=$item->direccion;
        $GLOBALS['credito']=$item->credito;
    }
    $periodosrow=json_decode($rentas);
    $ventasrow=json_decode($ventas);
    $ventascrow=json_decode($ventasc);
    $polizasrow=json_decode($polizas);
    $facsvrow=json_decode($facsv);
    
    
            
            









class MYPDF extends TCPDF {
  //Page header
  public function Header() {
      //$img_header = 'header.jpg';
      //$this->Image($img_header, 0, 0, 0, 197, '', '', '', false, 100, '', false, false, 0);
      //$this->Image($img_header, 0, 0, 210, '', 'JPG', '', false, false, 300, '', false, false, 0, false, false, false);
      $imglogo = base_url().'public/img/ops.png';
      $imglogo0 = base_url().'public/img/MDS.png';
        
      
      
          
$html = '<style type="text/css"> .titulotex{font-size:11px;} .titulotex_grey{color:grey;} .titulotex_red{color:red;} .boderb{border-bottom:1px solid black;} </style>
<table border="0" align="center"><tr><td width="25%"><img src="'.$imglogo.'"></td><td width="50%">Impresoras y Multifuncionales, Venta, Renta, Mantenimiento<br>Blvd. Norte 1831 V. San Alejandro Puebla, Pue.<br>Call. Center 273 3400, 249 5393<br>www.Kyoceraap.com</td><td width="25%"><img src="'.$imglogo0.'"></td></tr></table>
<table align="center" cellpadding="5"><tr><td colspan="2" class="boderb">ESTADO DE CUENTA AL '.date('d/m/Y').'</td></tr><tr><td class="boderb"></td><td class="boderb">Condiciones de Crédito: '.$GLOBALS['credito'].' días</td></tr><tr><td colspan="2" class="tableb">'.$GLOBALS['empresa'].'</td></tr></table>';
          
        $this->writeHTML($html, true, false, true, false, '');
  }
    // Page footer
  public function Footer() {
      $html = '<table width="100%" border="0"><tr><td align="right" class="footerpage">Pagina '.$this->getAliasNumPage().' de '.$this->getAliasNbPages().'</td></tr></table>';
      $this->writeHTML($html, true, false, true, false, '');
  }
} 
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Kyocera');
$pdf->SetTitle('Estado Cuenta');
$pdf->SetSubject('Estado Cuenta');
$pdf->SetKeywords('Estado Cuenta');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins('10', '50', '10');
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
$pdf->SetFont('dejavusans', '', 9);
// add a page
$pdf->AddPage('L', 'A4');
$html='';

//======================================================
    $html .= '<style type="text/css"> .stileback{background-color:grey;color: white;font-weight: bold;} .stileback2{background-color:#e0e0e0;font-weight: bold;} .bodertable{border: #0078d4 5px solid;} .table td{font-size:10px;} </style>';
    if(count($periodosrow)>0){
      $saldopendiente=0;
        $html .= '<table cellpadding="5" border="1" class="table bodertable" width="100%" align="center">
                <tr>
                  <td width="5%" class="stileback bodertable">ID</td>
                  <td width="9%" class="stileback">Contrato</td>
                  <td width="7%" class="stileback">Folio</td>
                  <td width="9%" class="stileback">Fecha Timbre</td>
                  <td width="9%" class="stileback">Periodo Inicial</td>
                  <td width="9%" class="stileback">Periodo Final</td>
                  
                  
                  <td width="25%" class="stileback">Descripción</td>
                  <td width="9%" class="stileback">Monto</td>
                  <td width="9%" class="stileback">Pagado</td>
                  <td width="9%" class="stileback">Saldo</td>
                  </tr>';
                  $sqlrentas=$this->ModeloEstadocuenta->sqlrentas('',$periodosrow,0,1);
                $facturas = $this->db->query($sqlrentas);
                  foreach ($facturas->result() as $item) {
                    if($item->FacturasId>0){
                        $total=$item->total;
                    }else{
                        $total=$item->total_periodo;
                    }
                    if($item->FacturasId>0){
                        $descripcion=$this->ModeloCatalogos->descripcionfactura($item->FacturasId);
                    }else{
                        $descripcion='';
                    }
                    
                    

                    
                    if ($item->fechamax==null) {
                        $fechapago='Pendiente';
                    }else{
                        $fechapago=$item->fechamax;
                    }
                    if( floatval($item->pagot)>0){
                        $pagot='$ '.number_format($item->pagot,2,'.',',');
                        $pagot_num=round($item->pagot,2);
                    }else{
                        $pagot='$ 0.00';
                        
                        $pagot_num=0;
                    }
                    $totalf=$total-$pagot_num;

                    if($totalf>0){
                        $infocomplemento=$this->ModeloCatalogos->infocomplementofactura($item->FacturasId);
                        $pagot_num=$infocomplemento[0];
                        if($infocomplemento[0]>0){
                            $fechapago=date('Y-m-d',strtotime($infocomplemento[1]));
                            $pagot='$ '.number_format($pagot_num,2,'.',',');
                            $totalf=$total-$pagot_num;
                        }    
                    }
                    if($item->info_factura_b!=null and $item->info_factura_b!=''){
                        $descripcion=$item->info_factura_b;
                    }
                    
                      $saldopendiente=$saldopendiente+$totalf;
                      $folio=$item->serie.''.$item->Folio;
                      if($item->Estado==0){
                        $folio.='<br>Cancelado';
                      }
                      $html_ren='<tr>';
                          $html_ren.='<td>'.$item->prefId.'</td>';
                          $html_ren.='<td>'.$item->confolio.'</td>';
                          $html_ren.='<td>'.$folio.'</td>';
                          $html_ren.='<td>'.$item->fechatimbre.'</td>';
                          $html_ren.='<td>'.$item->periodo_inicial.'</td>';
                          $html_ren.='<td>'.$item->periodo_final.'</td>';
                          
                          
                          $html_ren.='<td>'.$descripcion.'</td>';
                          $html_ren.='<td>$ '.number_format($total,2,'.',',').'</td>';
                          $html_ren.='<td>'.$pagot.'</td>';
                          $html_ren.='<td>$ '.number_format($totalf,2,'.',',').'</td>';
                          $html_ren.='</tr>';
                    $html.=$html_ren;
                  }
                  if($saldopendiente>0){
                    $html .= '<tr><td colspan="9"align="right" class="stileback2"><b>SALDO PENDIENTE</b></td><td class="stileback2">$ '.number_format($saldopendiente,2,'.',',').'</td></tr>';
                  }
        $html .= '</table><table><tr><td></td></tr></table>';
    }
    $ventastotales=count($ventasrow)+count($ventascrow);
    if($ventastotales>0){
      $saldopendiente=0;
        $html .= '<table cellpadding="5" border="1" class="table bodertable" width="100%" align="center">
                <tr>
                   <th width="5%" class="stileback bodertable" >Id Venta</th>
                   <th width="9%" class="stileback" >Fecha Creación</th>
                   
                   <th width="6%" class="stileback" >Folio</th>
                   <th width="10%" class="stileback" >Fecha Timbre</th>
                   <th width="43%"class="stileback" >Descripción</th>
                   <th width="9%" class="stileback" >Monto</th>
                   <th width="9%" class="stileback" >Pagado</th>
                   <th width="9%" class="stileback" >Saldo</th>
                  </tr>';
                  if(count($ventasrow)>0){
                    $strq_tv=$this->ModeloEstadocuenta->sqlventas(0,$ventasrow,0);
                    $query_tv = $this->db->query($strq_tv);

                    foreach ($query_tv->result() as $item) {
                        $pagado=$item->pago_manual+$item->ImpPagado;
                        $saldo=$item->total_general-$pagado;
                        if($item->FacturasId>0){
                            $descripcion=$this->ModeloCatalogos->descripcionfactura($item->FacturasId);
                        }else{
                            $descripcion='';
                        }
                        
                        if($item->info_factura!=null and $item->info_factura!=''){
                            $descripcion=$item->info_factura;
                        }
                        $saldopendiente=$saldopendiente+$saldo;
                        $html_ventas_tr='<tr>';
                            
                            $html_ventas_tr.='<td>'.$item->id.'</td>';
                            $html_ventas_tr.='<td>'.$item->reg.'</td>';
                            
                            $html_ventas_tr.='<td>'.$item->serie.' '.$item->Folio.'</td>';
                            $html_ventas_tr.='<td>'.$item->fechatimbre.'</td>';
                            $html_ventas_tr.='<td>'.$descripcion.'</td>';
                            $html_ventas_tr.='<td>$ '.number_format($item->total_general,2,'.',',').'</td>';
                            $html_ventas_tr.='<td>$ '.number_format($pagado,2,'.',',').'</td>';
                            $html_ventas_tr.='<td>$ '.number_format($saldo,2,'.',',').'</td>';
                            
                        $html_ventas_tr.='</tr>';
                        $html.=$html_ventas_tr;
                    }
                  }
                  if(count($ventascrow)>0){
                        $strq_tvc=$this->ModeloEstadocuenta->sqlventascom(0,$ventascrow,0);
                        $query_tvc = $this->db->query($strq_tvc);
                    foreach ($query_tvc->result() as $item) {
                        $pagado=$item->pago_manual+$item->ImpPagado;
                        $saldo=$item->total_general-$pagado;
                        if($item->FacturasId>0){
                                $descripcion=$this->ModeloCatalogos->descripcionfactura($item->FacturasId);
                            }else{
                                $descripcion='';
                            }
                        
                        if($item->info_factura!=null and $item->info_factura!=''){
                            $descripcion=$item->info_factura;
                        }
                        $saldopendiente=$saldopendiente+$saldo;
                        $html_ventas_tr='<tr>';
                            
                            $html_ventas_tr.='<td>'.$item->equipo.'</td>';
                            $html_ventas_tr.='<td>'.$item->reg.'</td>';
                            
                            $html_ventas_tr.='<td>'.$item->serie.' '.$item->Folio.'</td>';
                            $html_ventas_tr.='<td></td>';
                            $html_ventas_tr.='<td>'.$descripcion.'</td>';
                            $html_ventas_tr.='<td>$ '.number_format($item->total_general,2,'.',',').'</td>';
                            $html_ventas_tr.='<td>$ '.number_format($pagado,2,'.',',').'</td>';
                            $html_ventas_tr.='<td>$ '.number_format($saldo,2,'.',',').'</td>';
                        $html_ventas_tr.='</tr>';
                        $html.=$html_ventas_tr;
                    }
                  }
                  if($saldopendiente>0){
                    $html .= '<tr><td colspan="7"align="right" class="stileback2"><b>SALDO PENDIENTE</b></td><td class="stileback2">$ '.number_format($saldopendiente,2,'.',',').'</td></tr>';
                  }
                  
        $html .= '</table><table><tr><td></td></tr></table>';
    }
    if(count($polizasrow)>0){
      $saldopendiente=0;
      $html .= '<table cellpadding="5" border="1" class="table bodertable" width="100%" align="center">
                <tr>
                   <th width="5%" class="stileback" >Id Poliza</th>
                   <th width="9%" class="stileback" >Fecha Creación</th>
                   
                   <th width="6%" class="stileback" >Folio</th>
                   <th width="10%" class="stileback" >Fecha Timbre</th>
                   <th width="43%" class="stileback" >Descripción</th>
                   <th width="9%" class="stileback" >Monto</th>
                   <th width="9%" class="stileback" >Pagado</th>
                   <th width="9%" class="stileback" >Saldo</th>
                  </tr>';
          $strq_tp=$this->ModeloEstadocuenta->sqlpolizas(0,$polizasrow,0);
                $query_tp = $this->db->query($strq_tp);
            foreach ($query_tp->result() as $item) {
                $pagado=$item->pago_manual+$item->ImpPagado;
                if($item->siniva==1){
                    $total_general=$item->subtotalpoliza;
                }else{
                    $total_general=$item->subtotalpoliza+round(($item->subtotalpoliza*0.16),2);
                }
                if($total_general>0){
                    $saldo=$total_general-$pagado;
                    if($item->FacturasId>0){
                            $descripcion=$this->ModeloCatalogos->descripcionfactura($item->FacturasId);
                        }else{
                            $descripcion='';
                        }
                    
                    if($item->info_factura!=null and $item->info_factura!=''){
                        $descripcion=$item->info_factura;
                    }
                    $saldopendiente=$saldopendiente+$saldo;
                    $html_poliza_tr='<tr>';
                        
                        $html_poliza_tr.='<td>'.$item->id.'</td>';
                        $html_poliza_tr.='<td>'.$item->reg.'</td>';
                        
                        $html_poliza_tr.='<td>'.$item->serie.' '.$item->Folio.'</td>';
                        $html_poliza_tr.='<td>'.$item->fechatimbre.'</td>';
                        
                        $html_poliza_tr.='<td class="des_'.$item->id.'_2">'.$descripcion.'</td>';
                        $html_poliza_tr.='<td>$ '.number_format($total_general,2,'.',',').'</td>';
                        $html_poliza_tr.='<td>$ '.number_format($pagado,2,'.',',').'</td>';
                        $html_poliza_tr.='<td>$ '.number_format($saldo,2,'.',',').'</td>';
                    $html_poliza_tr.='</tr>';
                    $html.=$html_poliza_tr;
                }
            }
            if($saldopendiente>0){
                    $html .= '<tr><td colspan="7"align="right" class="stileback2"><b>SALDO PENDIENTE</b></td><td class="stileback2">$ '.number_format($saldopendiente,2,'.',',').'</td></tr>';
                  }
      $html .= '</table><table><tr><td></td></tr></table>';
    }
    if(count($facsvrow)>0){
      $saldopendiente=0;
      $html .= '<table cellpadding="5" border="1" class="table bodertable" width="100%" align="center">
                <tr>
                   <th width="5%" class="stileback" >Folio</th>
                   <th width="9%" class="stileback" >Fecha timbre</th>
                   <th width="59%" class="stileback" >Descripción</th>
                   <th width="9%" class="stileback" >Monto</th>
                   <th width="9%" class="stileback" >Pagado</th>
                   <th width="9%" class="stileback" >Saldo</th>
                  </tr>';
          $query_fsv=$this->ModeloEstadocuenta->sqlfacturassinvinculo(0,$facsvrow,0);
            foreach ($query_fsv->result() as $item) {
                $saldo=$item->total-$item->ImpPagado;
                if($item->info_factura_x!=null and $item->info_factura_x!=''){
                    $descripcion=$item->info_factura_x;
                }else{
                    $descripcion=$item->Descripcion2;
                }
                    $saldopendiente=$saldopendiente+$saldo;


                    $html_fac_sv_tr='<tr>';
                        
                        $html_fac_sv_tr.='<td>'.$item->serie.$item->Folio.'</td>';
                        $html_fac_sv_tr.='<td>'.$item->fechatimbre.'</td>';
                        
                        $html_fac_sv_tr.='<td>'.$descripcion.'</td>';
                        $html_fac_sv_tr.='<td>$ '.number_format($item->total,2,'.',',').'</td>';
                        $html_fac_sv_tr.='<td>$ '.number_format($item->ImpPagado,2,'.',',').'</td>';
                        $html_fac_sv_tr.='<td>$ '.number_format($saldo,2,'.',',').'</td>';                        
                    $html_fac_sv_tr.='</tr>';
                    $html.=$html_fac_sv_tr;
                
            }
            if($saldopendiente>0){
                    $html .= '<tr><td colspan="5"align="right" class="stileback2"><b>SALDO PENDIENTE</b></td><td class="stileback2">$ '.number_format($saldopendiente,2,'.',',').'</td></tr>';
                  }
      $html .= '</table><table><tr><td></td></tr></table>';
    }
    //log_message('error',$html);
    
    $pdf->writeHTML($html, true, false, true, false, '');
//======================================================
//$pdf->Output('Documentos.pdf', 'I');
$pdf->Output(FCPATH.'doccotizacion/EC_'.$cliente.'_'.$codi.'.pdf', 'FI');