var base_url = $('#base_url').val();
var selectpm;
$(document).ready(function(){
    table = $('#tabla_even').DataTable();
    $('#poliza').select2().on('select2:select', function (e) {
        //var data = e.params.data;
        //console.log(data);
        modelopoliza();
    });
    selectpm=$('#polizamodelo').select2().on('select2:select', function (e) {
        //var data = e.params.data;
        //console.log(data);
        var selectpolizamodelo =$('#polizamodelo option:selected');
        $("#local").val(selectpolizamodelo.data('plocal'));
        $("#semi").val(selectpolizamodelo.data('psemi'));
        $("#foraneo").val(selectpolizamodelo.data('pforaneo'));
       
    });
    loadtable();
    $("#local").TouchSpin({
                min: 0,
                max: 9999999999999999999999999999999999,
                initval: 0,
                step: 0.01,
                decimals: 2,
            });
    $("#semi").TouchSpin({
                min: 0,
                max: 9999999999999999999999999999999999,
                initval: 0,
                step: 0.01,
                decimals: 2,
            });
    $("#foraneo").TouchSpin({
                min: 0,
                max: 9999999999999999999999999999999999,
                initval: 0,
                step: 0.01,
                decimals: 2,
            });
    $("#especial").TouchSpin({
                min: 0,
                max: 9999999999999999999999999999999999,
                initval: 0,
                step: 0.01,
                decimals: 2,
            });
    var form_evens = $('#form_evens');
    form_evens.validate({
            rules: 
            {   /*
                poliza: {
                    required: true
                },
                polizamodelo: {
                    required: true
                },*/
                nombre: {
                    required: true
                },
                descripcion: {
                    required: true
                },
                local: {
                    required: true
                },
                semi: {
                    required: true
                },
                foraneo: {
                    required: true
                }
            },
            // Para mensajes personalizados
            messages: 
            {   /*
                poliza:{
                    required: "seleccione una poliza"
                },
                polizamodelo:{
                    required: "seleccione un modelo"
                },*/
                nombre:{
                    required: "Ingrese un nombre"
                },
                descripcion:{
                    required: "Ingrese una descripcion"
                },
                local:{
                    required: "Ingrese un monto"
                },
                semi:{
                    required: "Ingrese un monto"
                },
                foraneo:{
                    required: "Ingrese un monto"
                },
    
            },
        errorElement : 'div',
            // Si están correctos los campos, procesamos el formulario mediante Ajax
        submitHandler: function (form) {  
        }
    
    });
    $('.addserveven').click(function(event) {
        if ($('#form_evens').valid()) {
            var datos = form_evens.serialize();
            $.ajax({
                type:'POST',
                url: base_url+'index.php/Configuraciones/insertarEvenServ',
                data: datos,
                async: false,
                statusCode:{
                    404: function(data){
                        swal("Error", "404", "error");
                    },
                    500: function(){
                        swal("Error", "500", "error"); 
                    }
                },
                success:function(data){
                    console.log(data);
                    loadtable();
                    $('.addserveven').html('Agregar');
                    $('#form_evens')[0].reset();
                    $('.cancelarserveven').hide();
                }
            });
        }
    });
    $('.cancelarserveven').click(function(event) {
        $('#id').val(0);
        $('#poliza').val(0).change();
        modelopoliza();
        $('#polizamodelo').val(0).change();            
        $('#nombre').val('');
        $('#descripcion').val('');
        $('#local').val(0);
        $('#semi').val(0);
        $('#foraneo').val(0);
        $('.cancelarserveven').hide();
        $('.addserveven').html('Agregar');
        $('#newmodelo').val('');
    });
    $('#sinmodelo').click(function(event) {
        if ($('#sinmodelo').is(':checked')) {
            $('#newmodelo').attr('readonly', false);
            $('.newmodelo').show("slow");
            $('.polizamodelo').hide("slow");
        }else{
            $('#newmodelo').attr('readonly', true);
            $('.polizamodelo').show("slow");
            $('.newmodelo').hide("slow");
        }
        

    });

});

function modelopoliza(id){
    var id = $('#poliza option:selected').val();
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Polizas/detallespolizas",
        data: {
            idPoliza:id
        },
        success: function (response){
            //console.log(response);
            var array = $.parseJSON(response);
           //console.log(array);
           if (array.length>0) {
                var modelop ='<option value="0">Seleccionar Modelo</option>';
                    modelop +='<option value="all">Todos</option>';
                array.forEach(function(element) {
                    if (element.modelo2!=null) {
                        modelop+='<option value="'+element.id+'"\
                                data-plocal="'+element.precio_local+'"\
                                data-psemi="'+element.precio_semi+'"\
                                data-pforaneo="'+element.precio_foraneo+'"\
                              >'+element.modelo2+'</option>';    
                    }
                    
               });
                $('#polizamodelo').html(modelop);
                //selectpm.select2('updateResults');
           }else{
                $('#polizamodelo').html('');
                //selectpm.select2('updateResults');
           }
           

            
        },
        error: function(response){
            toastr.error('Algo salió mal, intente de nuevo o contacte al administrador del sistema','Error!'); 
        }
    });
}
function loadtable() {
    // Destruye la tabla y la crea de nuevo con los datos cargados 
    table.destroy();
    table = $('#tabla_even').DataTable({
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url+"index.php/Configuraciones/getListadoEvenServ",
            type: "post"
        },
        "columns": [
            {"data": "id", visible: false},
            {"data": "nombreser"},
            {"data": null,
                render:function(data,type,row){
                    var equipo='';
                    if (row.sinmodelo==1) {
                        equipo=row.newmodelo;
                    }else{
                        equipo=row.modelo;
                    }


                    return equipo;
                }
            },
            {"data": "descripcion"},
            {"data": "local"},
            {"data": "semi"},
            {"data": "foraneo"},
            {"data": "especial"},
            {"data": null,
                 render:function(data,type,row){
                    var html='<div class="btnaction">';
                        html+='<a class="waves-effect btn-bmz blue" onclick="editar('+row.id+')"><i class="material-icons">create</i></a>';
                        html+="<a class='waves-effect btn-bmz red' onclick='eliminar("+row.id+")'><i class='material-icons'>delete_forever</i></a>";
                        html+='</div >';
                    return html;
                }
            }
        ],
        "order": [[ 0, "desc" ]],
        
    });
}
function eliminar(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de Eliminar? El cambio será irreversible',
        type: 'red',
        typeAnimated: true,
        buttons: {
            confirmar: function () {
                    $.ajax({
                        type: "POST",
                        url: base_url+"index.php/Configuraciones/eliminarEvenServ",
                        data:{id:id},
                        success: function(data){
                            loadtable();
                        }
                    });
            },
            cancelar: function () {}
            
        }
    });
}
function editar(id){
    
    $.ajax({
        type: "POST",
        url: base_url+"index.php/Configuraciones/datosEvenServ",
        data:{id:id},
        success: function(data){
            console.log(data);
            var array = $.parseJSON(data);
            if (array.existe=1) {
                    $('.addserveven').html('Editar');
                    $('.cancelarserveven').show();
                    $('#id').val(array.id);
                    $('#poliza').val(array.poliza).change();
                    modelopoliza();
                    setTimeout(function(){ 
                        $('#polizamodelo').val(array.polizamodelo).change();
                    }, 1000);
                    
                    $('#nombre').val(array.nombre);
                    $('#descripcion').val(array.descripcion);
                    if (array.sinmodelo==1) {
                        if ($('#sinmodelo').is(':checked')) {

                        }else{
                            $('#sinmodelo').click();
                        }
                         $('#newmodelo').val(array.newmodelo);
                    }else{
                        if ($('#sinmodelo').is(':checked')) {
                             $('#sinmodelo').click();
                        }else{
                           
                        }
                        $('#newmodelo').val('');
                    }

                    setTimeout(function(){
                        $('#local').val(array.local);
                        $('#semi').val(array.semi);
                        $('#foraneo').val(array.foraneo);
                        $('#especial').val(array.especial);
                    }, 1500);

                   
            }
        }
    });
}