<?php
require_once dirname(__FILE__) . '/TCPDF3/examples/tcpdf_include.php'; 
require_once dirname(__FILE__) . '/TCPDF3/tcpdf.php';
  $renta_id=$idRenta;
  $logos = base_url().'public/img/alta.jpg';
  $logos2 = base_url().'public/img/kyocera.jpg';
  $result_eq=$this->Rentas_model->rentas_equipos_list($idRenta);
  $result_eq_r=$this->Rentas_model->rentas_equipos_resumen($idRenta);
  $result_ton_r=$this->Rentas_model->rentas_toner_resumen($idRenta);
  //=========================================================================
    $GLOBALS['configuracionCotizacion']=$configuracionCotizacion;
    $GLOBALS['rfc_datos']=$rfc_datos;
    $GLOBALS['dia_reg']=$dia_reg;
    $GLOBALS['mes_reg']=$mes_reg;
    $GLOBALS['ano_reg']=$ano_reg;
    $GLOBALS['ini_personal']=$ini_personal;
    $GLOBALS['proinven']=$proinven;
    $GLOBALS['cfd']=$cfd;
    //
    $GLOBALS['formacobro']=$formacobro;
    // Datos de del rfc 
    $GLOBALS['razon_social']=$razon_social;
    $GLOBALS['rfc']=$rfc;
    $GLOBALS['num_ext']=$num_ext;
    $GLOBALS['colonia']=$colonia;
    $GLOBALS['calle']=$calle;
    $GLOBALS['cp']=$cp;
    //resultprefactura
    $GLOBALS['vence']= $vence;
    $GLOBALS['cargo']=$cargo;
    //metodopagorow
    $GLOBALS['metodopago_text']=$metodopago_text;
    //formapagorow
    $GLOBALS['formapago_text']=$formapago_text;
    //cfdirow
    $GLOBALS['uso_cfdi_text']=$uso_cfdi_text;
    //resultadocli
    $GLOBALS['municipio']=$municipio;
    $GLOBALS['email']=$email;
    //estadorow
    $GLOBALS['estado']=$estado;
    //resultadoclitel
    $GLOBALS['tel_local']=$tel_local;
    //resultadoclipcontacto
    $GLOBALS['persona_contacto']=$persona_contacto;
    //rentaventas
    $GLOBALS['rentaventas']=$rentaventas;
    //resultadoc
    $GLOBALS['videncia']=$videncia;
    $GLOBALS['clicks_mono']=$clicks_mono;
    $GLOBALS['precio_c_e_mono']=$precio_c_e_mono;
    $GLOBALS['clicks_color']=$clicks_color;
    $GLOBALS['precio_c_e_color']=$precio_c_e_color;
    $GLOBALS['rentaadelantada']=$rentaadelantada;
    $GLOBALS['rentadeposito']=$rentadeposito;
    $GLOBALS['rentacolor']=$rentacolor;
    $GLOBALS['tipocontrato']=$tipocontrato;
    $GLOBALS['tiporenta']=$tiporenta;
    $GLOBALS['idcontrato']=$idcontrato;
    //contrato
    $GLOBALS['fechainicio']=$fechainicio;
    $GLOBALS['ordencompra']=$ordencompra;
    $GLOBALS['horaentregainicio']=$horaentregainicio;
    $GLOBALS['horaentregafin']=$horaentregafin;
    $GLOBALS['direccion_c']=$direccion_c;
    $GLOBALS['equipo_acceso']=$equipo_acceso;
    $GLOBALS['doc_acceso']=$doc_acceso;

    $GLOBALS['d_calle']=$d_calle;
    $GLOBALS['d_numero']=$d_numero;
    $GLOBALS['d_colonia']=$d_colonia;
    $GLOBALS['d_ciudad']=$d_ciudad;
    $GLOBALS['d_estado']=$d_estado;
    $GLOBALS['domicilio_entrega']=$domicilio_entrega;
  //=========================================================================
//====================================================
class MYPDF extends TCPDF {
 
  public function Header() {
      $logos = base_url().'public/img/alta.jpg';
      $logos2 = base_url().'public/img/kyocera.jpg';

      $html='<style type="text/css">
          .t_c{text-align:center;}
          .t_b{font-weight: bold;}
          .bor_b{border-bottom: 1px solid black;}
          .bor_l{border-left: 1px solid black;}
          .bor_r{border-right: 1px solid black;}
          .bor_t{border-top: 1px solid black;}
          .tableb th{background-color:rgb(217,217,217);}
          table td{font-size:10px;}
        </style>';
        $html.='<table border="0">';
            $html.='<tr>';
              $html.='<td width="20%" class="t_c"><img src="'.$logos.'" width="80px"></td>';
              $html.='<td width="60%" class="t_c">';
                $html.='"ALTA PRODUCTIVIDAD, S.A DE C.V."<br>';
                $html.='RFC: APR980122KZ6<br>';
                $html.='Impresoras y Multifuncionales, Venta, Renta y Mantenimiento.<br>';
                $html.='Domicilio Fiscal: Blvd. Norte 1831 Col. Villas San Alejandro. Puebla, Pue. Mexico C.P.72090<br>';
                $html.='Banco BANAMEX, Cuenta 7347132, Sucursal 826, CLABE 2668082673471320<br>';
                $html.='Call Center 273 3400, 249 5393. www.kyoceraap.com';
              $html.='</td>';
              $html.='<td width="20%" class="t_c"><img src="'.$logos2.'" width="100px"></td>';
            $html.='</tr>';
          $html.='</table>';
      
      $this->writeHTML($html, true, false, true, false, '');
  }
    // Page footer
  public function Footer() {
    $html = '';
 
      $html .= '<table width="100%" border="0">
                  <tr>
                    <td align="right" class="footerpage">Pagina '.$this->getAliasNumPage().' de '.$this->getAliasNbPages().'</td>
                  </tr>
                </table>';
      $this->writeHTML($html, true, false, true, false, '');
      
  }
} 
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Kyocera');
$pdf->SetTitle('Contrato');
$pdf->SetSubject('factura');
$pdf->SetKeywords('factura');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins('7', '30', '7');
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin('10');

// set auto page breaks
$pdf->SetAutoPageBreak(true, 10);//PDF_MARGIN_BOTTOM

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('dejavusans', '', 7);
// add a page
$pdf->AddPage('P', 'A4');
$html='<style type="text/css">
          .t_c{text-align:center;}
          .t_b{font-weight: bold;}
          .bor_b{border-bottom: 1px solid black;}
          .bor_l{border-left: 1px solid black;}
          .bor_r{border-right: 1px solid black;}
          .bor_t{border-top: 1px solid black;}
          .tableb th{background-color:rgb(217,217,217);}
          .bor_btlr{
            border-bottom: 1px solid black;
            border-top: 1px solid black;
            border-left: 1px solid black;            border-right: 1px solid black;
          }
</style>';

  $html.='<table class="tableb" border="0" align="center" cellpadding="4">';
            $html.='<tr>';
              $html.='<td class="bor_b bor_l bor_r bor_t">Fecha: '.$GLOBALS['dia_reg'].'/'.$GLOBALS['mes_reg'].'/'.$GLOBALS['ano_reg'].'</td>';
              $html.='<td class="bor_b bor_r  bor_t">Ejecutivo: '.$GLOBALS['ini_personal'].'</td>';
              $html.='<td class="bor_b  bor_r bor_t">Prefactura: '.$GLOBALS['proinven'].'</td>';
            $html.='</tr>';
            $html.='<tr>';
              $html.='<th class="bor_b bor_l bor_r bor_t t_b" colspan="3">Datos del cliente</th>';
            $html.='</tr>';
          $html.='</table>';
          $html.='<table class="tableb" border="1" align="center" cellpadding="2">';
            $html.='<tr>';
              $html.='<td class="bor_b bor_l bor_t">Cia:</td>';
              $html.='<td class="bor_b bor_t" colspan="5">'.$GLOBALS['razon_social'].'</td>';
              $html.='<td class="bor_b bor_t">RFC:</td>';
              $html.='<td class="bor_b bor_r bor_t">'.$GLOBALS['rfc'].'</td>';
            $html.='</tr>';
            $html.='<tr>';
              $html.='<td width="10%" class="bor_b bor_l bor_t">Vence:</td>';
              $html.='<td width="10%" class="bor_b bor_t">'.$GLOBALS['vence'].'</td>';
              $html.='<td width="12%" class="bor_b bor_t">Método de pago:</td>';
              $html.='<td width="16%" class="bor_b bor_t">'.$GLOBALS['metodopago_text'].'</td>';
              $html.='<td width="13%" class="bor_b bor_t">Forma de pago:</td>';
              $html.='<td width="14%" class="bor_b bor_t">'.$GLOBALS['formapago_text'].'</td>';
              $html.='<td width="11%" class="bor_b bor_t">Uso de CFDI:</td>';
              $html.='<td width="14%" class="bor_b bor_r bor_t">'.$GLOBALS['uso_cfdi_text'].'</td>';
            $html.='</tr>';
          $html.='</table>';
          $html.='<table border="1">';
            $html.='<tr>';
              $html.='<td class="t_c">Dirección Fiscal:</td>';
              $html.='<td class="t_c">Dirección de Entrega/Instalación:</td>';
            $html.='</tr>';
            $html.='<tr>';
              $html.='<td>';
                $html.='<table>';
                  $html.='<tr><td width="20%">CALLE:</td><td width="80%">'.$calle.' N° '.$num_ext.'</td></tr>';
                  $html.='<tr><td>COL.</td><td>'.$colonia.'</td></tr>';
                  $html.='<tr><td>ESTADO:</td><td>'.$rs_estado.'</td></tr>';
                  $html.='<tr><td>C.P.</td><td>'.$cp.'</td></tr>';
                $html.='</table>';
              $html.='</td>';
              $html.='<td>';
                $html.=''.$GLOBALS['domicilio_entrega'].'  ';
              $html.='</td>';
            $html.='</tr>';
          $html.='</table>';
          $html.='<table class="tableb" border="1" align="center" cellpadding="2">';
            $html.='<tr>';
              $html.='<td width="11%" class="bor_b bor_l bor_t">Contacto: </td>';
              $html.='<td width="22%" class="bor_b bor_t">'.$GLOBALS['persona_contacto'].'</td>';
              $html.='<td width="11%" class="bor_b bor_t">Cargo:</td>';
              $html.='<td width="22%" class="bor_b bor_t">'.$GLOBALS['cargo'].'</td>';
              $html.='<td width="11%" class="bor_b bor_t">E-mail:</td>';
              $html.='<td width="23%" class="bor_b bor_r bor_t">'.$GLOBALS['email'].'</td>';
            $html.='</tr>';
          $html.='</table>';
          $html.='<table class="tableb" align="center" cellpadding="2">';
            $html.='<tr>';
              $html.='<td colspan="6" class=" bor_l bor_t bor_r t_c">Datos de Entrega:</td>';
            $html.='</tr>';
            $html.='<tr>';
              $html.='<td width="15%" class="bor_l">Fecha de entrega:</td>';
              $html.='<td width="20%" >'.$GLOBALS['fechainicio'].'</td>';
              $html.='<td width="10%" >OC.</td>';
              $html.='<td width="15%" >'.$GLOBALS['ordencompra'].'</td>';
              $html.='<td width="40%"  colspan="2" class="bor_r">Referencia de Domicilio: '.$GLOBALS['direccion_c'].'. '.$GLOBALS['equipo_acceso'].'. '.$GLOBALS['doc_acceso'].'</td>';
            $html.='</tr>';
            $html.='<tr>';
              $html.='<td class="bor_l bor_b">Horario de entrega:</td>';
              $html.='<td class=" bor_b">'.$GLOBALS['horaentregainicio'].' a '.$GLOBALS['horaentregafin'].'</td>';
              $html.='<td class=" bor_b"></td>';
              $html.='<td class=" bor_b"></td>';
              $html.='<td class=" bor_b"></td>';
              $html.='<td class="bor_r bor_b"></td>';
            $html.='</tr>';
          $html.='</table>';
          $html.='<p></p>';
          $html.='<table class="tableb" align="center" cellpadding="2">';
            $html.='<tr>';
              $html.='<th colspan="2" class="bor_b bor_l bor_t bor_r t_c t_b">Especificaciones de Contrato</th>';
            $html.='</tr>';
            $html.='<tr>';
              $html.='<td width="30%" class="bor_b bor_l bor_t bor_r">Numero de contrato:</td>';
              $html.='<td width="70%" class="bor_b bor_l bor_t bor_r">'.$folio.'</td>';
            $html.='</tr>';
          $html.='</table>';
          if($GLOBALS['tiporenta']==2){ //global
              $tiposervicio ='Global';
              $v_rentadeposito='$ '.number_format($GLOBALS['rentadeposito'],2,'.',',');
              $v_clicks_mono=''.number_format($GLOBALS['clicks_mono'],2,'.',',');
              $v_precio_c_e_mono='$ '.number_format($GLOBALS['precio_c_e_mono'],2,'.',',');
              $v_rentacolor='$ '.number_format($GLOBALS['rentacolor'],2,'.',',');
              $v_clicks_color=''.number_format($GLOBALS['clicks_color'],2,'.',',');
              $v_precio_c_e_color='$ '.number_format($GLOBALS['precio_c_e_color'],2,'.',',');
              if($GLOBALS['rentacolor']==0 and $GLOBALS['clicks_color']==0 and $GLOBALS['precio_c_e_color']==0){
                $check_color='N/A';
                $v_rentacolor='N/A';
                $v_clicks_color='N/A';
                $v_precio_c_e_color='N/A';
              }else{
                $check_color='X';
              }

              $html.='<table class="tableb" border="1"  align="center" cellpadding="2">';
                          $html.='<tr><td colspan="4">Condiciones de arrendamiento</td></tr>';
                          $html.='<tr><td>Tipo de contrato</td><td>Volumen Incluido (Clicks)</td><td>Renta mensual </td><td>Excedentes (Cobro por pags.)</td></tr>';
                          $html.='<tr><td>Monocromática ( X )</td><td>'.$v_clicks_mono.'</td><td>'.$v_rentadeposito.'</td><td>'.$v_precio_c_e_mono.'</td></tr>';
                          $html.='<tr><td>Color ( '.$check_color.' )</td><td>'.$v_clicks_color.'</td><td>'.$v_rentacolor.'</td><td>'.$v_precio_c_e_color.'</td></tr>';
                      $html.='</table><p></p>';
              $tituloespecificacionesequipos='Detalle de equipos, consumibles y accesorios.';
          }else{
              $tituloespecificacionesequipos='Detalle de equipos, consumibles, accesorios y condiciones.';
          }
          $html.='<table cellpadding="4" align="center">';
                    $html.='<tr>';
                      $html.='<td width="30%"></td>';
                      $html.='<td width="40%" class="bor_b t_b ">Resumen de Equipos, Accesorios y Consumibles</td>';
                      $html.='<td width="30%"></td>';
                    $html.='</tr>';
                    $html.='<tr>';
                      $html.='<td></td>';
                      $html.='<td>';
                        $html.='<table border="1" cellpadding="4">';
                          $html.='<tr><td>Cantidad</td><td>Concepto</td><td>Modelo</td></tr>';
                          foreach ($result_eq_r->result() as $item) {
                            $html.='<tr><td>'.$item->total.'</td><td>Equipo</td><td>'.$item->modelo.'</td></tr>';
                          }
                          foreach ($result_ton_r->result() as $item) {
                            $html.='<tr><td>'.$item->cantidad.'</td><td>Toner</td><td>'.$item->modelo.'</td></tr>';
                          }
                          
                        $html.='</table>';
                      $html.='</td>';
                      $html.='<td></td></tr></table><p></p>';





          $html.='<table class="tableb" border="1"  align="center" cellpadding="2"><tr><td>'.$tituloespecificacionesequipos.'</td></tr></table>';
           $html.='<table class="tableb"  align="center" cellpadding="2">';
                    $html.='<thead>';
                      $html.='<tr>';
                        $html.='<td class="bor_b">Cantidad</td>';
                        $html.='<td class="bor_b">No. de Parte</td>';
                        $html.='<td class="bor_b">Concepto</td>';
                        $html.='<td class="bor_b">Modelo</td>';
                        $html.='<td class="bor_b">No. Serie y/o Folio</td>';
                        $html.='<td class="bor_b">Etiqueta No</td>';
                        $html.='<td class="bor_b">Almacen</td>';
                      $html.='</tr>';
                    $html.='</thead>';
                    $html.='<tbody>';
                    foreach ($result_eq->result() as $itemeq) {
                      $serieId=$itemeq->serieId;
                      $rowequipo=$itemeq->rowequipo;
                      $bodegaeq=$itemeq->bodega;
                      if($itemeq->serie_bodega==0){
                        $bodegaeq=$this->ModeloCatalogos->obtenerbodega($itemeq->bodegaId);
                      }
                      $html.='<tr>';
                              $html.='<td>1</td>';
                              $html.='<td>'.$itemeq->noparte.'</td>';
                              $html.='<td>Equipo</td>';
                              $html.='<td>'.$itemeq->modelo.'</td>';
                              $html.='<td>'.$itemeq->serie.'</td>';
                              $html.='<td>'.$itemeq->folio_equipo.'</td>';
                              $html.='<td>'.$bodegaeq.'</td>';
                              $html.='</tr>';
                              
                            //===============================================================================================

                                $rentaequipos_consumible = $this->ModeloCatalogos->rentaequipos_consumible($idRenta,$itemeq->idEquipo,$itemeq->rowequipo);
                                foreach ($rentaequipos_consumible->result() as $item) {
                                          $foliorelacion=0;
                                          //============================ cuando ya todos tengan relacion se quitara
                                          /*
                                          $contrato_folio = $this->ModeloCatalogos->selectcontrolfoliorenta($renta_id,$item->id_consumibles,$serieId);
                                          foreach ($contrato_folio->result() as $itemx) {
                                            if($itemx->idrelacion>0){
                                             
                                            }else{
                                              //$html.=$itemx->foliotext.',';
                                            }
                                          }  
                                          */
                                          //============================ cuando ya todos tengan relacion remplazara a la anterior
                                          $contrato_folio2 = $this->ModeloCatalogos->obtenerfoliosconsumiblesrenta($renta_id,$item->id_consumibles,$item->id); 
                                          foreach ($contrato_folio2->result() as $itemx) {
                                                if($itemx->idrelacion>0){
                                                  $foliorelacion=1;
                                                  $html.='<tr>';
                                                            $html.='<td>1</td>';
                                                            $html.='<td>'.$item->parte.'</td>';
                                                            $html.='<td>Toner</td>';
                                                            $html.='<td>'.$item->modelo.'</td>';
                                                            $html.='<td>'.$itemx->foliotext.'</td>';
                                                            $html.='<td></td>';
                                                            $html.='<td>'.$item->bodega.'</td>';
                                                    $html.='</tr>';
                                                }
                                                
                                          }
                                          if($foliorelacion==0){
                                            $html.='<tr>';
                                                      $html.='<td>1</td>';
                                                      $html.='<td>'.$item->parte.'</td>';
                                                      $html.='<td>Toner</td>';
                                                      $html.='<td>'.$item->modelo.'</td>';
                                                      $html.='<td></td>';
                                                      $html.='<td></td>';
                                                      $html.='<td>'.$item->bodega.'</td>';
                                                    $html.='</tr>';
                                          }
                                  
                                }
                            //===============================================================================================
                                $rentaequipos_accesorios = $this->ModeloCatalogos->rentaequipos_accesorios($renta_id,$itemeq->idEquipo,$itemeq->rowequipo);
                                foreach ($rentaequipos_accesorios->result() as $item) {
                                  $bodegaacc=$item->bodega;
                                  if($itemeq->serie_bodega>0){
                                    
                                  }else{
                                    $bodegaacc=$this->ModeloCatalogos->obtenerbodega($item->bodegaId);
                                  }
                                  $html.='<tr>';
                                            $html.='<td>'.$item->cantidad.'</td>';
                                            $html.='<td>'.$item->no_parte.'</td>';
                                            $html.='<td>Accesorio</td>';
                                            $html.='<td>'.$item->nombre.'</td>';
                                            $html.='<td>'.$item->series.'</td>';
                                            $html.='<td></td>';
                                            $html.='<td>'.$bodegaacc.'</td>';
                                          $html.='</tr>';
                               
                                }
                            //=======================================================================================================
                            //============================================
                                if($GLOBALS['tiporenta']==1){
                                  $where_ce=array('contrato'=>$GLOBALS['idcontrato'],'equiposrow'=>$rowequipo,'serieId'=>$serieId);
                                  $resultado_contrato_equipos = $this->ModeloCatalogos->getselectwheren('contrato_equipos',$where_ce);
                                  foreach ($resultado_contrato_equipos->result() as $itemmcondi) {
                                    $v_rentadeposito='$ '.number_format($itemmcondi->rentacosto,2,'.',',');
                                    $v_clicks_mono=''.number_format($itemmcondi->pag_monocromo_incluidos,2,'.',',');
                                    $v_precio_c_e_mono='$ '.number_format($itemmcondi->excedente,2,'.',',');
                                    $v_rentacolor='$ '.number_format($itemmcondi->rentacostocolor,2,'.',',');
                                    $v_clicks_color=''.number_format($itemmcondi->pag_color_incluidos,2,'.',',');
                                    $v_precio_c_e_color='$ '.number_format($itemmcondi->excedentecolor,2,'.',',');
                                    if($itemmcondi->rentacostocolor==0 and $itemmcondi->pag_color_incluidos==0 and $itemmcondi->excedentecolor==0){
                                      $check_color='N/A';
                                      $v_rentacolor='N/A';
                                      $v_clicks_color='N/A';
                                      $v_precio_c_e_color='N/A';
                                    }else{
                                      $check_color='X';
                                    }
                                    $html.='<tr>';
                                              $html.='<td colspan="7">';
                                                $html.='<table class="tableb" border="1"  align="center" cellpadding="2">';
                                                  $html.='<tr>';
                                                    $html.='<td colspan="4">Condiciones de arrendamiento</td>';
                                                  $html.='</tr>';
                                                  $html.='<tr>';
                                                    $html.='<td>Tipo de contrato</td><td>Volumen Incluido (Clicks)</td><td>Renta mensual </td><td>Excedentes (Cobro por pags.)</td>';
                                                  $html.='</tr>';
                                                  $html.='<tr>';
                                                    $html.='<td>Monocromática ( X )</td><td>'.$v_clicks_mono.'</td><td>'.$v_rentadeposito.'</td><td>'.$v_precio_c_e_mono.'</td>';
                                                  $html.='</tr>';
                                                  $html.='<tr>';
                                                    $html.='<td>Color ( '.$check_color.' )</td><td>'.$v_clicks_color.'</td><td>'.$v_rentacolor.'</td><td>'.$v_precio_c_e_color.'</td>';
                                                  $html.='</tr>';
                                              $html.='</table>';
                                              $html.='</td>';
                                            $html.='</tr>';
                                  }
                                }
                            //============================================
                            if($itemeq->ubicacion!='' and $itemeq->comenext!=''){
                              $html.='<tr>';
                                $html.='<td class="bor_btlr" >Ubicación</td>';
                                $html.='<td class="bor_btlr" colspan="2">'.$itemeq->ubicacion.'</td>';
                                $html.='<td class="bor_btlr" >Comentario</td>';
                                $html.='<td class="bor_btlr" colspan="3">'.$itemeq->comenext.'</td>';
                              $html.='</tr>'; 
                            }
                      $html.='<tr>';
                                $html.='<td class="bor_b" colspan="7"></td>';
                              $html.='</tr>';  
                    }
            $html.='</tbody>';
          $html.='</table>';

            $html.='<table cellpadding="4" align="center">';
                      $html.='<tr>';
                        $html.='<td class="bor_l bor_t" height="60px"></td>';
                        $html.='<td class="bor_t bor_l"></td>';
                        $html.='<td class="bor_t bor_l"></td>';
                        $html.='<td class="bor_t bor_l bor_r"></td>';
                      $html.='</tr>';
                      $html.='<tr>';
                        $html.='<td class="bor_l">Nombre del Cliente </td>';
                        $html.='<td class="bor_l">Asesor Comercial </td>';
                        $html.='<td class="bor_l">Almacen</td>';
                        $html.='<td class="bor_l bor_r">Gerencia</td>';
                      $html.='</tr>';
                      $html.='<tr>';
                        $html.='<td class="bor_l bor_b">Firma de conformidad.</td>';
                        $html.='<td class="bor_l bor_b">Firma de Enterado </td>';
                        $html.='<td class="bor_l bor_b">Firma de Entrega</td>';
                        $html.='<td class="bor_l bor_b bor_r">Firma de Enterado</td>';
                      $html.='</tr>';
            $html.='</table>';












//log_message('error', $html);
$pdf->writeHTML($html, true, false, true, false, '');



$pdf->IncludeJS('print(true);');
//$pdf->Output('Folios_.pdf', 'I');
$pdf->Output(FCPATH.'doccot/'.$uuid.'.pdf', 'FI');

?>