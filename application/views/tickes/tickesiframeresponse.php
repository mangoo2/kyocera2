<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title></title>
	<link href="<?php echo base_url(); ?>public/plugins/note/bootstrap.min.css" rel="stylesheet">
	<script src="<?php echo base_url(); ?>public/plugins/note/jquery-3.5.1.min.js"></script>
	<script src="<?php echo base_url(); ?>public/plugins/note/bootstrap.min.js"></script>

<!-- include summernote css/js -->
	<link href="<?php echo base_url(); ?>public/plugins/note/summernote.min.css" rel="stylesheet">
	<script src="<?php echo base_url(); ?>public/plugins/note/summernote.min.js"></script>

    <script type="text/javascript" src="<?php echo base_url(); ?>public/js/ticket.js?v=<?php echo date('YmdGis');?>" ></script>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/plugins/toastr/toastr.css">
    <script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/toastr/toastr.min.js" ></script>
    <script type="text/javascript">
      var base_url = $('#base_url').val();
      $(document).ready(function($) {
        
        cargainformacion();
         $('.responderticket').click(function(event) {
            $.ajax({
                type:'POST',
                url: '<?php echo base_url();?>Generales/ticketresponder',
                data: {
                    ticketid: <?php echo $idt;?>,
                    contenido: $('#respuesta').val()           
                    },
                    async: false,
                    statusCode:{
                        404: function(data){
                            swal("Error", "404", "error");
                        },
                        500: function(){
                            swal("Error", "500", "error"); 
                        }
                    },
                    success:function(data){
                        //$('#respuesta').val('');
                        
                        //$('.summernote').val('');
                        //cargainformacion();
                        location.reload();
                    }
            });
        });
      });
      function cargainformacion(){
        $.ajax({
            type:'POST',
            url: '<?php echo base_url();?>Generales/ticketview',
            data: {
                ticketid: <?php echo $idt;?>           
                },
                async: false,
                statusCode:{
                    404: function(data){
                        swal("Error", "404", "error");
                    },
                    500: function(){
                        swal("Error", "500", "error"); 
                    }
                },
                success:function(data){
                  console.log(data);
                  $('.descripciongeneral').html(data);
                  //$('.descripciongeneral img').addClass('materialboxed');
                  //$('.materialboxed').materialbox();
                }
        });
      }
    </script>
</head>
<body>
	<input type="hidden" id="base_url" value="<?php echo base_url();?>">
    <input type="hidden" id="idticket" value="<?php echo $idt;?>">
	<div class="container">
    <div class="row descripciongeneral" style="max-height: 400px; overflow: auto;">
      
    </div>
    <div class="row">
      
      <div class="col s12">
        <textarea class="form-control-bmz summernote" id="respuesta"></textarea>
      </div>
      <div class="col s12">
        <button class="btn btn-success right responderticket"  name="action">Responder</button>
      </div>
    </div>
	</div>
	
</body>
</html>