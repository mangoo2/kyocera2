<?php
$a=session_id();
if(empty($a)) session_start();
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloPersonal extends CI_Model {
    public function __construct() {
        parent::__construct();
        $this->DB11 = $this->load->database('other11_db', TRUE);
    }
    
    public function personalnuevo($nom,$sexo,$mail,$sucu){
            $strq = "CALL SP_ADD_PERSONAL('$nom',$sexo,'$mail',$sucu);";
            $this->db->query($strq);
    }
    public function personalupdate($id,$nom,$sexo,$mail,$sucu){
            $strq = "CALL SP_UPDATE_PERSONAL($id,'$nom',$sexo,'$mail',$sucu)";
            $this->db->query($strq);
    }
    function getpersonal() {
        $strq = "CALL SP_GET_ALL_PERSONAL";
        $query = $this->db->query($strq);
        return $query;
    }
    
    function personalview($id) {
        $strq = "SELECT *  FROM personal where personalId='$id'";
        $query = $this->DB11->query($strq);
        return $query;
    }
    public function personaldelete($id){
            $strq = "CALL SP_DEL_PERSONAL($id)";
            $this->db->query($strq);
    }
    function notificarasignacion() {
        $strq = "SELECT * FROM config";
        $query = $this->db->query($strq);
        $this->db->close();
        $notf =0;
        foreach($query->result() as $valor){
            $notf=$valor->notfig_tutor;
        }
        return $notf;
    }
    

}
