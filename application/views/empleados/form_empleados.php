<?php 

// Validamos si es creación o edición, para así cargar los datos respectivamente
if(isset($empleado))
{
    $idEmpleado = $empleado->personalId;
    $nombre = $empleado->nombre;
    $apellido_paterno = $empleado->apellido_paterno;
    $apellido_materno = $empleado->apellido_materno;
    $fecha_nacimiento = $empleado->fecha_nacimiento;
    $sexo = $empleado->sexo;
    $direccion = $empleado->direccion;
    $ciudad = $empleado->ciudad;
    $estado = $empleado->estado;
    $telefono = $empleado->telefono;
    $celular = $empleado->celular;
    $email = $empleado->email;
    $tipo = $empleado->tipo;
    $observaciones = $empleado->observaciones;
    $usuario = $empleado->Usuario;
    $contrasena = $empleado->contrasena;
    $perfilId = $empleado->perfilId;
    $usuarioId = $empleado->UsuarioID;
    $numero_ss= $empleado->numero_ss;
    $emp = $empleado->emp;
    $texuser =$empleado->texuser;
    $dep =$empleado->dep;
}
else
{
    $idEmpleado = 0;
    $nombre = '';
    $apellido_paterno = '';
    $apellido_materno = '';
    $fecha_nacimiento = '';
    $sexo = 0;
    $direccion = '';
    $ciudad = '';
    $estado = 0;
    $telefono = '';
    $celular = '';
    $email = '';
    $tipo = 0;
    $observaciones = '';  
    $perfilId = 0;
    $usuarioId = '';
    $usuario = '';
    $contrasena = '';
    $texuser ='';
    $numero_ss='';
    $emp=0;
    $dep=0;
} 

if($tipoVista==1){
  $label = 'Formulario de Alta de Empleados';
}
else if($tipoVista==2){
  $label = 'Formulario de Edición de Empleados';
}
else if($tipoVista==3){
  $label = 'Formulario de Visualización de Empleados';
}
?>
<!-- START CONTENT --> 
        <section id="content" width="100%">
          

          <!--start container-->
          <div class="container" width="100%">
            <div class="section">
              <div class="row">
                <p class="caption col s6"><?php echo $label; ?>.</p>
                
                <?php 
                  if(isset($empleado)){
                      if($contrasena!=''){
                ?>
                        <button class="btn pink waves-effect waves-light right" type="button" id="botonRestablece" name="botonRestablece" >Restablecer Contraseña
                          <i class="material-icons right">vpn_key</i>
                        </button>
                <?php
                      }   
                  }
                ?>
              </div>
              <div class="row">
                <input id="base_url" type="hidden" name="base_url" value="<?php echo base_url(); ?>">
                <input id="tipoVista" type="hidden" name="tipoVista" value="<?php echo $tipoVista; ?>">
                <!-- FORMULARIO -->
                <form class="col s12 " id="empleados-form" autocomplete="off">

                  <?php if(isset($empleado)){ ?>
                    <input id="idEmpleado" name="idEmpleado" type="hidden" value="<?php echo $idEmpleado; ?>">
                    <input id="usuarioId" name="usuarioId" type="hidden" value="<?php echo $usuarioId; ?>">
                  <?php } ?>
                  <div class="col s12 m12 24">

                    <div class="card-panel">
                      <div class="row">
                          <div class="row">
                            <div class="col s4">
                              <label>Nombre</label>
                              <input id="nombre" name="nombre" type="text" placeholder="Nombre" class="form-control-bmz" value="<?php echo $nombre; ?>" required>
                            </div>
                            <div class=" col s4">
                              <label >Apellido Paterno</label>
                              <input id="apellido_paterno" name="apellido_paterno" type="text" class="form-control-bmz" value="<?php echo $apellido_paterno; ?>" required>
                              
                            </div>
                            <div class=" col s4">
                              <label >Apellido Materno</label>
                              <input id="apellido_materno" name="apellido_materno" type="text" class="form-control-bmz" value="<?php echo $apellido_materno; ?>">
                              
                            </div>

                          </div>

                          <div class="row">
                            <div class=" col s4">
                              <label >Fecha de nacimiento</label>
                              <input id="fecha_nacimiento" name="fecha_nacimiento" type="date" class="form-control-bmz" value="<?php echo $fecha_nacimiento; ?>" required>
                              
                            </div>
                            <div class="col s4">
                              <label >Sexo</label>
                              <select id="sexo" name="sexo" class="browser-default form-control-bmz">
                                  <option value="" disabled selected>Selecciona una opción</option>
                                  <option value="1" <?php if($sexo==1){ echo 'selected'; } ?> >Hombre</option>
                                  <option value="2" <?php if($sexo==2){ echo 'selected'; } ?> >Mujer</option>
                              </select>
                              
                            </div>
                            <div class="col s4">
                              <label >Número de Seguro Social</label>
                              <input id="numero_ss" name="numero_ss" type="text" class="form-control-bmz" value="<?php echo $numero_ss; ?>">
                              
                            </div>
                            
                          </div>

                          <div class="row">
                            <div class=" col s4">
                              <label >Dirección</label>
                              <input id="direccion" name="direccion"  type="text" class="form-control-bmz" autocomplete="off" value="<?php echo $direccion; ?>" required>
                              
                            </div>

                            <div class=" col s4">
                              <label >Ciudad</label>
                              <input id="ciudad" name="ciudad" type="text" type="text" class="form-control-bmz" value="<?php echo $ciudad; ?>">
                              
                            </div>

                            <div class=" col s4">
                              <label  class="active">Estado</label>
                              <select id="estado" name="estado" class="browser-default form-control-bmz">
                                <option value="" disabled selected>Selecciona una opción</option>
                                <?php foreach($estados as $estado_elegido){ ?>
                                  <option value="<?php echo $estado_elegido->EstadoId?>" <?php if($estado_elegido->EstadoId==$estado) echo "selected";   ?> ><?php echo $estado_elegido->Nombre; ?></option>
                                <?php }?>

                                
                              </select>
                              
                            </div>
                          </div>

                          <div class="row">
                            <div class=" col s2">
                              <label for="telefono">Teléfono</label>
                              <input id="telefono" name="telefono" type="text" class="form-control-bmz"value="<?php echo $telefono; ?>" required>
                              
                            </div>

                            <div class=" col s2">
                              <label>Celular</label>
                              <input id="celular" name="celular"  type="text" class="form-control-bmz" autocomplete="off" value="<?php echo $celular; ?>">
                              
                            </div>

                            <div class="  col s4">
                              <label >Email</label>
                              <input id="email" name="email"  type="email" class="form-control-bmz"autocomplete="off" value="<?php echo $email; ?>">
                              
                            </div>
                            <div class="col s4">
                              <label>Departamento por atender</label>
                              <select id="dep" name="dep" class="browser-default form-control-bmz">
                                <option value="0"></option>
                                <option value="1" <?php if($dep==1){ echo 'selected';} ?> >Ventas</option>
                                <option value="2" <?php if($dep==2){ echo 'selected';} ?> >Rentas</option>
                                <option value="3" <?php if($dep==3){ echo 'selected';} ?> >Polizas</option>
                                <option value="4" <?php if($dep==4){ echo 'selected';} ?> >Servicio  Tecnico</option>
                              </select>
                            </div>
                          </div>

                          <div class="row">
                             <div class=" col s4">
                              <label class="active">Tipo</label>
                              <select id="tipo" name="tipo" class="browser-default form-control-bmz" required onchange="select_tipo()">
                                  <option value="" disabled selected>Selecciona una opción</option>
                                  <option value="1" <?php if($tipo==1){ echo 'selected'; } ?> >Empleado</option>
                                  <option value="2" <?php if($tipo==2){ echo 'selected'; } ?> >Usuario</option>
                              </select>
                              
                            </div>
                            <div class="col s4">
                              <label>Empresa</label>
                              <select name="emp" id="emp" class="browser-default form-control-bmz "  >
                                <option value="1" <?php if($emp==1){ echo 'selected';}?> >Alta Productividad</option>
                                <option value="2" <?php if($emp==2){ echo 'selected';}?> >D-Impresión</option>
                              </select>
                            </div>
                          </div>

                          <div class="row" id="datosUsuario" >
                            <div class=" col s4">
                              <label class="active">Perfil</label>
                              <select id="perfilId" name="perfilId" class="browser-default form-control-bmz">
                                  <option value="" disabled selected>Selecciona una opción</option>
                                  <?php foreach($perfiles as $perfil){ ?>
                                    <option value="<?php echo $perfil->perfilId?>" <?php if($perfil->perfilId==$perfilId) echo "selected";   ?> ><?php echo $perfil->nombre; ?></option>
                                  <?php }?>
                              </select>
                              
                            </div>

                            <div class=" col s4">
                              <label for="usuario">Usuario</label>
                              <input id="usuario" name="usuario" type="text" class="form-control-bmz" autocomplete="off" value="<?php echo $usuario; ?>">
                            </div>
                            <?php if(!isset($empleado)){ ?>
                            <div class=" col s4">
                              <label for="password">Contraseña</label>
                              <input id="contrasena" name="contrasena" type="password" class="form-control-bmz" autocomplete="new-password" value="<?php echo $contrasena; ?>">
                              
                            </div>
                            <?php } ?>

                          </div>

                          <div class="row">
                            <div class="col s12">
                              <label for="observaciones">Observaciones</label>
                              <textarea id="observaciones" name="observaciones" class="materialize-textarea form-control-bmz"><?php echo $observaciones; ?></textarea>
                              
                            </div>
                            <div class="row">
                              <div class="input-field col s12">
                                <?php if($tipoVista!=3) {?>
                                <a class="btn-bmz cyan waves-effect waves-light right" onclick="formsave()" name="action">Guardar
                                  <i class="material-icons right">send</i>
                                </a>
                                <?php } ?>
                              </div>
                            </div>
                          </div>
                        
                      </div>

                    </div>

                  </div>
                </form>
              </div>


              <div id="modalRestableceContra" class="modal" >
              <!--style="width: 550px;"-->
                  <div class="modal-content ">

                    <h5>Restablecer Contraseña</h5>

                    <div class="col s12 m12 l6">
                        <?php 
                            if($idpersonal==1 || $idpersonal==18 || $idpersonal==17){
                              if(isset($empleado)){
                                  if($texuser!=''){

                        ?>
                        <div class="row">
                          <div class="col s12">
                            Contraseña actual: <?php echo $texuser;?>
                          </div>
                        </div>
                        <?php 
                                  }
                              }
                            }

                        ?>
                        <div class="row">
                          <div class="input-field col s12">
                            <i class="material-icons prefix"></i>
                            <input id="contrasena_restablecida" name="contrasena_restablecida" class="form-control-bmz" type="password" >
                            <label for="contrasena_restablecida">Contraseña</label>
                          </div>
                        </div>
                    </div>
                      
                  <div class="modal-footer">
                    <a href="" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cancelar</a>
                    <a class="modal-action modal-close waves-effect waves-green green btn-flat" id="guardaContrasenaRestablecida" name="guardaContrasenaRestablecida">Restablecer</a>
                  </div>

                </div>
              </div>

          </div>
        </section>