<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Atencionclientes extends CI_Controller {
    public function __construct()     {
        parent::__construct();
        //$this->load->model('Clientes_model', 'model');
        $this->load->model('ModeloCatalogos');
        $this->load->model('Rentas_model');
        $this->load->helper('url');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->version = date('YmdH');
        $this->submenu=53;
        if($this->session->userdata('logeado')==true){
            $this->perfilid =$this->session->userdata('perfilid');
            $this->idpersonal = $this->session->userdata('idpersonal');
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,$this->submenu);// 43 es el id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
        }else{
            redirect('login'); 
        }
    }

    function index(){
        $data['MenusubId']=$this->submenu;
            $data['version']=$this->version;
            $this->load->view('header');
            $this->load->view('main',$data);
            $this->load->view('crm/Atencionclientes');
            $this->load->view('footer');
            $this->load->view('crm/Atencionclientes_js');
    }
    function consultardatos(){
        $params = $this->input->post();
        $clienteid=$params['clienteid'];
        $tipocli=$params['tipocli'];

        $result=$this->ModeloCatalogos->getselectwheren('clientes',array('id'=>$clienteid));   
        $empresa='';     
        $idUsuario=0;
        $personalId=0;
        $personaleje='';
        foreach ($result->result() as $item) {
            $empresa=$item->empresa;
            $reg=$item->reg;
            $antiguedad=$item->antiguedad;
            $idUsuario=$item->idUsuario;
        }
        $resultusu=$this->ModeloCatalogos->getselectwheren('usuarios',array('UsuarioID'=>$idUsuario));
        foreach ($resultusu->result() as $item) {
            $personalId=$item->personalId;
        }
        $resultper=$this->ModeloCatalogos->getselectwheren('personal',array('personalId'=>$personalId));
        foreach ($resultper->result() as $item) {
            $personaleje=$item->nombre.' '.$item->apellido_paterno.' '.$item->apellido_materno;
        }
        $resulpoliza=$this->ModeloCatalogos->getselectwheren('polizasCreadas',array('idCliente'=>$clienteid,'combinada'=>0,'activo'=>1));
        $resulcontrato=$this->ModeloCatalogos->contratorentacliente($clienteid);

        $resulservicioevento = $this->ModeloCatalogos->getselectwheren('asignacion_ser_cliente_a',array('clienteId'=>$clienteid));
        
        //=============================================
            $result_equipos=$this->ModeloCatalogos->getselectwheren('cotizaciones',array('idCliente'=>$clienteid,'tipo'=>1));
            $result_consumibles=$this->ModeloCatalogos->getselectwheren('cotizaciones',array('idCliente'=>$clienteid,'tipo'=>2));
            $result_refacciones=$this->ModeloCatalogos->getselectwheren('cotizaciones',array('idCliente'=>$clienteid,'tipo'=>3));
            $result_polizas=$this->ModeloCatalogos->getselectwheren('cotizaciones',array('idCliente'=>$clienteid,'tipo'=>4));
            $result_rentas=$this->ModeloCatalogos->getselectwheren('cotizaciones',array('idCliente'=>$clienteid,'rentaVentaPoliza'=>2));

            $resultv_equipos=$this->ModeloCatalogos->getselectwheren('ventas',array('idCliente'=>$clienteid,'tipo'=>1));
            $resultv_consumibles=$this->ModeloCatalogos->getselectwheren('ventas',array('idCliente'=>$clienteid,'tipo'=>2));
            $resultv_refacciones=$this->ModeloCatalogos->getselectwheren('ventas',array('idCliente'=>$clienteid,'tipo'=>3));
        //=============================================
        $html='';
        $html.='<div class="col s12 m12 l12 z-depth-1">';
        if ($tipocli==1) {//cliente
            $html.='<div class="row">
                        <div class="col s2 titleac">
                            Cliente:
                        </div>
                        <div class="col s3 resultadoac">
                        '.$empresa.'
                        </div>
                        <div class="col s2 titleac">
                            Cotización generada:
                        </div>
                        <div class="col s5 resultadoac">';
                        if ($result_equipos->num_rows()>0) {
                            $html.='<a class="btn-bmz btn-margin-topz" onclick="obtenercotizaciones('.$clienteid.',1)">Equipos</a>';
                        }
                        if ($result_consumibles->num_rows()>0) {
                            $html.='<a class="btn-bmz btn-margin-topz" onclick="obtenercotizaciones('.$clienteid.',2)">Consumibles</a>';
                        }
                        if ($result_refacciones->num_rows()>0) {
                            $html.='<a class="btn-bmz btn-margin-topz" onclick="obtenercotizaciones('.$clienteid.',3)">Refacciones</a>';
                        }
                        if ($result_polizas->num_rows()>0) {
                            $html.='<a class="btn-bmz btn-margin-topz" onclick="obtenercotizaciones('.$clienteid.',4)">Pólizas</a>';
                        }
                        if ($result_rentas->num_rows()>0) {
                            $html.='<a class="btn-bmz btn-margin-topz" onclick="obtenercotizaciones('.$clienteid.',5)">Rentas</a>';
                        }
                $html.='</div>
                    </div>';
            $html.='<div class="row">
                        <div class="col s2 titleac">
                            Fecha de captura:
                        </div>
                        <div class="col s3 resultadoac">
                        '.$reg.'
                        </div>
                        <div class="col s2 titleac">
                            Tipo de venta:
                        </div>
                        <div class="col s5 resultadoac">';
                        if ($resultv_equipos->num_rows()>0) {
                            $html.='<a class="btn-bmz btn-margin-topz" onclick="obtenerventas('.$clienteid.')">Equipos</a>';
                        }
                        if ($resultv_consumibles->num_rows()>0) {
                            $html.='<a class="btn-bmz btn-margin-topz" onclick="obtenerventas('.$clienteid.')">Consumibles</a>';
                        }
                        if ($resultv_refacciones->num_rows()>0) {
                            $html.='<a class="btn-bmz btn-margin-topz" onclick="obtenerventas('.$clienteid.')">Refacciones</a>';
                        }        
                $html.='</div>
                    </div>';
            $html.='<div class="row">
                        <div class="col s2 titleac">
                            Antigüedad
                        </div>
                        <div class="col s3 resultadoac">
                        '.$antiguedad.'
                        </div>
                        <div class="col s2 titleac">
                            Pólizas:
                        </div>
                        <div class="col s5 resultadoac">';
                        if ($resulpoliza->num_rows()>0) {
                            $html.='<a class="dropdown-button btn-bmz btn-margin-topz" data-activates="dropdown4c" style="width: 120px;">Pólizas</a>
                              <ul id="dropdown4c" class="dropdown-content">';
                              foreach ($resulpoliza->result() as $item) {
                                  $html.='<li><a onclick="detallep('.$item->id.')">'.$item->id.'</a></li>';
                              }
                                
                    $html.='</ul>';
                        }
                            
                $html.='</div>
                    </div>';
            $html.='<div class="row">
                        <div class="col s2 titleac">
                            Ejecutiva:
                        </div>
                        <div class="col s3 resultadoac">
                            '.$personaleje.'
                        </div>
                        <div class="col s2 titleac">
                            Rentas:
                        </div>
                        <div class="col s5 resultadoac">
                            <div class="col s12" style="padding-left: 0px;">
                        ';
                            if ($resulcontrato->num_rows()>0) {
                                $html.='<a class="dropdown-button btn-bmz btn-margin-topz" data-activates="dropdown5c" style="width: 168px;"> Contratos </a>
                                      <ul id="dropdown5c" class="dropdown-content dropdown5c" style="width: 168px;">';
                                      foreach ($resulcontrato->result() as $item) {
                                          $html.='<li>
                                                    <a 
                                                    
                                                    
                                                    onclick="obtenerperiodos('.$item->idcontrato.','.$item->idRenta.')">'.$item->folio.'</a></li>';
                                      }
                                        
                                      $html.='</ul>';
                            }
                            
                $html.='    </div>
                            <div class="col s12 addperiodos">
                            </div>
                        </div>
                    </div>';
            $html.='<div class="row">
                        <div class="col s2">
                        </div>
                        <div class="col s3">
                        </div>
                        <div class="col s2 titleac">
                            Servicios:
                        </div>
                        <div class="col s5 resultadoac">';
                        if ($resulservicioevento->num_rows()>0) {
                                $html.='<a class="dropdown-button btn-bmz btn-margin-topz" data-activates="dropdown6c" style="width: 120px;"> Servicios </a>
                                      <ul id="dropdown6c" class="dropdown-content dropdown6c">';
                                      foreach ($resulservicioevento->result() as $item) {
                                          $html.='<li><a onclick="documento('.$item->asignacionId.',3,0)">'.$item->fecha.'</a></li>';
                                      }
                                        
                                      $html.='</ul>';
                            }
                $html.='</div>
                    </div>';
        }elseif ($tipocli==2) {//prospecto
            $html.='<div class="row">
                        <div class="col s2 titleac">
                            Prospecto:
                        </div>
                        <div class="col s3 resultadoac">
                            '.$empresa.'
                        </div>
                        <div class="col s2 titleac">
                            Cotización generada:
                        </div>
                        <div class="col s5 resultadoac">';
                        if ($result_equipos->num_rows()>0) {
                            $html.='<a class="btn-bmz btn-margin-topz" onclick="obtenercotizaciones('.$clienteid.',1)">Equipos</a>';
                        }
                        if ($result_consumibles->num_rows()>0) {
                            $html.='<a class="btn-bmz btn-margin-topz" onclick="obtenercotizaciones('.$clienteid.',2)">Consumibles</a>';
                        }
                        if ($result_refacciones->num_rows()>0) {
                            $html.='<a class="btn-bmz btn-margin-topz" onclick="obtenercotizaciones('.$clienteid.',3)">Refacciones</a>';
                        }
                        if ($result_polizas->num_rows()>0) {
                            $html.='<a class="btn-bmz btn-margin-topz" onclick="obtenercotizaciones('.$clienteid.',4)">Pólizas</a>';
                        }
                        if ($result_rentas->num_rows()>0) {
                            $html.='<a class="btn-bmz btn-margin-topz" onclick="obtenercotizaciones('.$clienteid.',5)">Rentas</a>';
                        }       
                $html.='</div>
                    </div>';
            $html.='<div class="row">
                        <div class="col s2 titleac">
                            Fecha de captura:
                        </div>
                        <div class="col s3 resultadoac">
                        '.$reg.'
                        </div>
                        <div class="col s2">
                            
                        </div>
                        <div class="col s5">
                        </div>
                    </div>';
            $html.='<div class="row">
                        <div class="col s2 titleac">
                            Ejecutiva:
                        </div>
                        <div class="col s3 resultadoac">
                            '.$personaleje.'
                        </div>
                        <div class="col s2">
                        </div>
                        <div class="col s5">
                        </div>
                    </div>';
           
        }

        $html.='</div>';

        echo $html;
    }
    public function selectfacturas()    {  
      $idcontrato = $this->input->post('id');
       $html = '';
       $factura = $this->Rentas_model->contratorentafactura($idcontrato);
        
      foreach ($factura as $item) {
        $html.='<a class="btn-bmz btn-margin-topz periodoservicio_'.$item->prefId.'" 
                    onclick="contadores('.$item->prefId.','.$idcontrato.')" 
                    data-fechainicio="'.$item->periodo_inicial.'" data-fechafin="'.$item->periodo_final.'"
                >'.$item->periodo_inicial.' al '.$item->periodo_final.'</a>';
      

      }    
             
      echo $html;  
    }
    function listservicios(){
        $params = $this->input->post();
        $idcontrato = $params['idcontrato'];
        $inicio     = $params['inicio'];
        $fin        = $params['fin'];

        $servicios = $this->Rentas_model->listservicios($idcontrato,$inicio,$fin);
        $html='<table class="table">
                <thead>
                    <tr>
                        <th>Zona</th>
                        <th>Fecha</th>
                        <th>Hora</th>
                        <th>Modelo</th>
                        <th>Serie</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                ';
        foreach ($servicios->result() as $item) {
            $html.='<tr>
                        <td>'.$item->nombre.'</td>
                        <td>'.$item->fecha.'</td>
                        <td>'.$item->hora.' a '.$item->horafin.'</td>
                        <td>'.$item->modelo.'</td>
                        <td>'.$item->serie.'</td>
                        <td>
                            <a class="waves-effect cyan btn-bmz" onclick="documento('.$item->asignacionId.',1,'.$item->asignacionIde.')">
                                <i class="fas fa-file-alt" style="font-size: 20px;"></i>
                            </a>
                        </td>
                    </tr>';
        }
        $html.='</tbody></table>';
        echo $html;
    }
    
  
    
}
?>
