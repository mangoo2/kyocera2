<?php

defined('BASEPATH') OR exit('No direct script access allowed');
 
 require APPPATH . 'libraries/RestController.php';
 require APPPATH . 'libraries/Format.php';

use chriskacerguis\RestServer\RestController;

class Restserver extends RestController {
    public function __construct()
    {
        parent::__construct();
        $this->load->model('ModeloCatalogos');
        $this->load->model('Equipos_model');
        $this->load->model('Consumibles_model');
        $this->bodega=34; //34 almacen web
        $this->act_resg=0;//1 ventas activas 0 ventas eliminadas
    }
    function updatestock2($Tabla,$value,$masmeno,$value2,$idname1,$id1,$idname2,$id2){
        $strq = "UPDATE $Tabla SET $value = $value $masmeno $value2 where $idname1=$id1 and $idname2=$id2 ";
        $query = $this->db->query($strq);
        //return $id;
    }
    public function test_post(){
        $array=array('Hola','Mundo','CodeIgniter');
        $this->response($array);
    }
    public function consultatabla_post(){
        $params=$this->input->post();
        $tabla=$params['tabla'];
        $cols=$params['cols'];
        $orderbyname=$params['orderbyname'];
        $orderby=$params['orderby'];//DESC ASC
        $DATAc = json_decode($cols);
        
        $this->db->select('*');
        $this->db->from($tabla);
        for ($i=0;$i<count($DATAc);$i++) {
            $this->db->where(array($DATAc[$i]->name => $DATAc[$i]->value));
        }
        if($orderby!='x'){
            $this->db->order_by($orderbyname, $orderby);
        }
        $array=$this->db->get();

        $this->response($array->result());
    }
    public function inserttable_post(){
        $params=$this->input->post();
        $table=$params['table'];
        $columns=$params['columns'];
        $DATAc = json_decode($columns);

        $data=array();
        for ($i=0;$i<count($DATAc);$i++) {
            $data[$DATAc[$i]->name]=$DATAc[$i]->value;
        }

        $this->db->insert($table, $data);
        $id=$this->db->insert_id();

        $this->response($id);
    }
    public function deletecarrito_post(){
        $params=$this->input->post();
        //$table=$params['table'];
        $columns=$params['columns'];
        $DATAc = json_decode($columns);

        $data=array();
        for ($i=0;$i<count($DATAc);$i++) {
            $data[$DATAc[$i]->name]=$DATAc[$i]->value;
        }

        //$this->db->insert($table, $data);
        $this->ModeloCatalogos->getdeletewheren('cliente_carrito',$data);
        //$id=$this->db->insert_id();

        //$this->response($id);
    }
    public function updatetable_post(){
        $params=$this->input->post();
        $table=$params['table'];
        $columns=$params['columns'];
        $columns = json_decode($columns);

        $columnsw=$params['columnsw'];
        $columnsw = json_decode($columnsw);

        $data=array();
        for ($i=0;$i<count($columns);$i++) {
            $data[$columns[$i]->name]=$columns[$i]->value;
        }
        $where=array();
        for ($j=0;$j<count($columnsw);$j++) {
            $where[$columnsw[$j]->name]=$columnsw[$j]->value;
        }

        

        $this->db->set($data);
        $this->db->where($where);
        $this->db->update($table);
        
        //$this->response($id);
    }
    public function productos_post(){
        $array=$this->ModeloCatalogos->getselectwheren('equipos',array('paginaweb'=>1,'estatus'=>1));
        $this->response($array->result());
    }
    public function productosmono_get(){
        //$array=$this->ModeloCatalogos->getselectwheren('equipos',array('paginaweb'=>1,'estatus'=>1,'categoriaId'=>24));
        $strq="SELECT 
                    eq.*,
                     (SELECT group_concat('<col1>',eqc.name,'<col2>',eqc.descripcion,'<col3>' separator '') FROM equipos_caracteristicas as eqc WHERE eqc.idequipo=eq.id AND eqc.activo=1 and eqc.general=1 ORDER BY eqc.orden ASC) as caracte,
                     (SELECT (COUNT(*)-SUM(sepr.resguardar_cant)) totales FROM series_productos as sepr WHERE sepr.productoid=eq.id AND sepr.status<2 and sepr.bodegaId='$this->bodega') as stock,
                     (SELECT IFNULL(SUM(veq.cantidad),0) FROM ventas as v INNER JOIN ventas_has_detallesEquipos as veq on veq.idVenta=v.id AND veq.serie_estatus=0 AND veq.idEquipo=eq.id WHERE v.venta_web=1 AND v.activo='$this->act_resg') as s_regu
                from equipos as eq 
                where 
                    eq.paginaweb=1 and 
                    eq.estatus=1 and 
                    eq.categoriaId=24 ";
        $array = $this->db->query($strq);
        $this->response($array->result());
    }
    public function productoscolor_get(){
        //$array=$this->ModeloCatalogos->getselectwheren('equipos',array('paginaweb'=>1,'estatus'=>1,'categoriaId'=>25));
        $strq="SELECT 
                    eq.*,
                     (SELECT group_concat('<col1>',eqc.name,'<col2>',eqc.descripcion,'<col3>' separator '') FROM equipos_caracteristicas as eqc WHERE eqc.idequipo=eq.id AND eqc.activo=1 and eqc.general=1 ORDER BY eqc.orden ASC) as caracte,
                     (SELECT (COUNT(*)-SUM(sepr.resguardar_cant)) totales FROM series_productos as sepr WHERE sepr.productoid=eq.id AND sepr.status<2 and sepr.bodegaId='$this->bodega') as stock,
                     (SELECT IFNULL(SUM(veq.cantidad),0) FROM ventas as v INNER JOIN ventas_has_detallesEquipos as veq on veq.idVenta=v.id AND veq.serie_estatus=0 AND veq.idEquipo=eq.id WHERE v.venta_web=1 AND v.activo='$this->act_resg') as s_regu
                from equipos as eq 
                where 
                    eq.paginaweb=1 and 
                    eq.estatus=1 and 
                    eq.categoriaId=25";
        $array = $this->db->query($strq);
        $this->response($array->result());
    }
    public function productosmmono_get(){
        //$array=$this->ModeloCatalogos->getselectwheren('equipos',array('paginaweb'=>1,'estatus'=>1,'categoriaId'=>26));
        $strq="SELECT 
                    eq.*,
                     (SELECT group_concat('<col1>',eqc.name,'<col2>',eqc.descripcion,'<col3>' separator '') FROM equipos_caracteristicas as eqc WHERE eqc.idequipo=eq.id AND eqc.activo=1 and eqc.general=1 ORDER BY eqc.orden ASC) as caracte,
                     (SELECT (COUNT(*)-SUM(sepr.resguardar_cant)) totales FROM series_productos as sepr WHERE sepr.productoid=eq.id AND sepr.status<2 and sepr.bodegaId='$this->bodega') as stock,
                     (SELECT IFNULL(SUM(veq.cantidad),0) FROM ventas as v INNER JOIN ventas_has_detallesEquipos as veq on veq.idVenta=v.id AND veq.serie_estatus=0 AND veq.idEquipo=eq.id WHERE v.venta_web=1 AND v.activo='$this->act_resg') as s_regu
                from equipos as eq 
                where 
                    eq.paginaweb=1 and 
                    eq.estatus=1 and 
                    eq.categoriaId=26";
        $array = $this->db->query($strq);
        $this->response($array->result());
    }
    public function productosmcolor_get(){
        //$array=$this->ModeloCatalogos->getselectwheren('equipos',array('paginaweb'=>1,'estatus'=>1,'categoriaId'=>27));
        $strq="SELECT 
                    eq.*,
                     (SELECT group_concat('<col1>',eqc.name,'<col2>',eqc.descripcion,'<col3>' separator '') FROM equipos_caracteristicas as eqc WHERE eqc.idequipo=eq.id AND eqc.activo=1 and eqc.general=1 ORDER BY eqc.orden ASC) as caracte,
                     (SELECT (COUNT(*)-SUM(sepr.resguardar_cant)) totales FROM series_productos as sepr WHERE sepr.productoid=eq.id AND sepr.status<2 and sepr.bodegaId='$this->bodega') as stock,
                     (SELECT IFNULL(SUM(veq.cantidad),0) FROM ventas as v INNER JOIN ventas_has_detallesEquipos as veq on veq.idVenta=v.id AND veq.serie_estatus=0 AND veq.idEquipo=eq.id WHERE v.venta_web=1 AND v.activo='$this->act_resg') as s_regu
                from equipos as eq 
                where 
                    eq.paginaweb=1 and 
                    eq.estatus=1 and 
                    eq.categoriaId=27";
        $array = $this->db->query($strq);
        $this->response($array->result());
    }
    public function productosmhibrida_get(){
        //$array=$this->ModeloCatalogos->getselectwheren('equipos',array('paginaweb'=>1,'estatus'=>1,'categoriaId'=>28));
        $strq="SELECT 
                    eq.*,
                     (SELECT group_concat('<col1>',eqc.name,'<col2>',eqc.descripcion,'<col3>' separator '') FROM equipos_caracteristicas as eqc WHERE eqc.idequipo=eq.id AND eqc.activo=1 and eqc.general=1 ORDER BY eqc.orden ASC) as caracte,
                     (SELECT (COUNT(*)-SUM(sepr.resguardar_cant)) totales FROM series_productos as sepr WHERE sepr.productoid=eq.id AND sepr.status<2 and sepr.bodegaId='$this->bodega') as stock,
                     (SELECT IFNULL(SUM(veq.cantidad),0) FROM ventas as v INNER JOIN ventas_has_detallesEquipos as veq on veq.idVenta=v.id AND veq.serie_estatus=0 AND veq.idEquipo=eq.id WHERE v.venta_web=1 AND v.activo='$this->act_resg') as s_regu
                from equipos as eq 
                where 
                    eq.paginaweb=1 and 
                    eq.estatus=1 and 
                    eq.categoriaId=28";
        $array = $this->db->query($strq);
        $this->response($array->result());
    }
    public function productosinfo_get($consu){
        //log_message('error',$consu);
        $array=$this->ModeloCatalogos->getselectwheren('equipos',array('paginaweb'=>1,'estatus'=>1,'uuid'=>$consu));
        $this->response($array->result());
    }
    public function productosinfoid_get($consu){
        $array=$this->ModeloCatalogos->getselectwheren('equipos',array('paginaweb'=>1,'estatus'=>1,'id'=>$consu));
        $this->response($array->result());
    }
    public function productosinfocaract_get($consu){
        $strq = "SELECT * FROM `equipos_caracteristicas` WHERE idequipo=$consu AND activo=1 ORDER BY `orden` ASC";
        $query = $this->db->query($strq);
        $this->response($query->result());
    }
    public function productosimg_get($consu){
        $array=$this->ModeloCatalogos->getselectwheren('equipo_imgs',array('idequipo'=>$consu,'activo'=>1));
        $this->response($array->result());
    }
    public function productosdestacado_get(){
        //$array=$this->ModeloCatalogos->getselectwheren('equipos',array('paginaweb'=>1,'estatus'=>1,'destacado'=>1));
        $strq="SELECT eq.*,
                IFNULL((
                        SELECT JSON_ARRAYAGG(JSON_OBJECT('or', c.orden, 'name', c.name,'desc',c.descripcion))
                        FROM equipos_caracteristicas c
                        WHERE c.idequipo = eq.id ORDER BY orden ASC
                    ),'[]') AS caract_sub,
                (SELECT (COUNT(*)-SUM(sepr.resguardar_cant)) totales FROM series_productos as sepr WHERE sepr.productoid=eq.id AND sepr.status<2 and sepr.bodegaId='$this->bodega') as stock,
                
                IFNULL(SUM(veq.cantidad), 0) AS s_regu
                
                FROM equipos as eq
                LEFT JOIN ventas v ON v.venta_web = 1 AND v.activo = '$this->act_resg'
                LEFT JOIN ventas_has_detallesEquipos veq ON veq.idVenta=v.id and veq.idEquipo = eq.id AND veq.serie_estatus = 0
                


                WHERE eq.paginaweb=1 AND eq.estatus=1 AND eq.destacado=1
                group by eq.id


                ";
        $query = $this->db->query($strq);
        $this->response($query->result());
    }
    public function productosarendamiento_get(){
        //$array=$this->ModeloCatalogos->getselectwheren('equipos',array('paginaweb'=>1,'estatus'=>1,'destacado'=>1));
        $strq="SELECT eq.*,
                IFNULL((
                        SELECT JSON_ARRAYAGG(JSON_OBJECT('or', c.orden, 'name', c.name,'desc',c.descripcion))
                        FROM equipos_caracteristicas c
                        WHERE c.idequipo = eq.id ORDER BY orden ASC
                    ),'[]') AS caract_sub
                FROM equipos as eq
                WHERE eq.paginaweb=1 AND eq.estatus=1 AND eq.pw_rentas=1";
        $query = $this->db->query($strq);
        $this->response($query->result());
    }
    public function productosoutlet_get(){
        //$array=$this->ModeloCatalogos->getselectwheren('equipos',array('paginaweb'=>1,'estatus'=>1,'destacado'=>1));
        $strq="SELECT eq.*,
                IFNULL((
                        SELECT JSON_ARRAYAGG(JSON_OBJECT('or', c.orden, 'name', c.name,'desc',c.descripcion))
                        FROM equipos_caracteristicas c
                        WHERE c.idequipo = eq.id ORDER BY orden ASC
                    ),'[]') AS caract_sub,
                (SELECT (COUNT(*)-SUM(sepr.resguardar_cant)) totales FROM series_productos as sepr WHERE sepr.productoid=eq.id AND sepr.status<2 and sepr.bodegaId='$this->bodega') as stock,
                (SELECT IFNULL(SUM(veq.cantidad),0) FROM ventas as v INNER JOIN ventas_has_detallesEquipos as veq on veq.idVenta=v.id AND veq.serie_estatus=0 AND veq.idEquipo=eq.id WHERE v.venta_web=1 AND v.activo='$this->act_resg') as s_regu
                FROM equipos as eq
                WHERE eq.paginaweb=1 AND eq.estatus=1";
        $query = $this->db->query($strq);
        $this->response($query->result());
    }
    public function baner_get(){
        $array=$this->ModeloCatalogos->getselectwheren('baner',array('activo'=>1));
        $this->response($array->result());
    }
    public function sisuser_post(){
        $user=$this->input->post('user');
        $array=$this->ModeloCatalogos->getselectwheren('clientes',array('correo'=>$user,'estatus'=>1));
        $this->response($array->result());
    }
    public function accesorios_get(){
        //$array=$this->ModeloCatalogos->getselectwheren('catalogo_accesorios',array('paginaweb'=>1,'status'=>1));
        $strq="SELECT 
                    eq.*,
                     (SELECT group_concat('<col1>',eqc.name,'<col2>',eqc.descripcion,'<col3>' separator '') FROM catalogo_accesorios_caracteristicas as eqc WHERE eqc.idaccesorios=eq.id AND eqc.activo=1 and eqc.general=1 ORDER BY eqc.orden ASC) as caracte,
                     (SELECT sum(seacc.cantidad-seacc.resguardar_cant) FROM series_accesorios as seacc WHERE seacc.accesoriosid=eq.id and seacc.activo=1 and seacc.bodegaId='$this->bodega') as stock,
                     IFNULL((
                        SELECT JSON_ARRAYAGG(JSON_OBJECT('img', ai.imagen))
                        FROM catalogo_accesorios_imgs ai
                        WHERE ai.idaccesorio = eq.id 
                    ), '[]') AS imgs,
                    IFNULL(SUM(acc.cantidad),0) as s_regu
                from catalogo_accesorios as eq 

                LEFT JOIN ventas v ON v.venta_web = 1 AND v.activo = '$this->act_resg'
                LEFT JOIN ventas_has_detallesEquipos_accesorios acc ON acc.id_venta=v.id and acc.id_accesorio=eq.id AND acc.serie_estatus=0

                where 
                    eq.paginaweb=1 and 
                    eq.status=1

                group by eq.id
                ";
        $array = $this->db->query($strq);
        $this->response($array->result());
    }
    public function sisuserdireccion_get($cliente){
        $array=$this->ModeloCatalogos->getselectwheren('clientes_direccion',array('idcliente'=>$cliente,'status'=>1));
        $this->response($array->result());
    }
    public function sisusercontacto_get($cliente){
        $array=$this->ModeloCatalogos->getselectwheren('cliente_datoscontacto',array('clienteId'=>$cliente,'activo'=>1));
        $this->response($array->result());
    }
    public function sisusercontactoid_get($datosId){
        $array=$this->ModeloCatalogos->getselectwheren('cliente_datoscontacto',array('datosId'=>$datosId));
        $this->response($array->result());
    }
    public function sisuserdireccionid_get($idclientedirecc){
        $array=$this->ModeloCatalogos->getselectwheren('clientes_direccion',array('idclientedirecc'=>$idclientedirecc));
        $this->response($array->result());
    }
    public function sisuserdireccionadd_post(){
        $dir=$this->input->post('dir');
        $cli=$this->input->post('cli');
        $entrecalles=$this->input->post('entrecalles');
        $cp=$this->input->post('cp');

        $this->ModeloCatalogos->Insert('clientes_direccion',array('idcliente'=>$cli,'direccion'=>$dir,'entrecalles'=>$entrecalles,'cp'=>$cp));
    }
    public function sisusercontactoadd_post(){
        $cli=$this->input->post('cli');
        $atencionpara=$this->input->post('atencionpara');
        $telefono=$this->input->post('telefono');
        $celular=$this->input->post('celular');
        
        $this->ModeloCatalogos->Insert('cliente_datoscontacto',array('clienteId'=>$cli,'atencionpara'=>$atencionpara,'telefono'=>$telefono,'celular'=>$celular,'puesto'=>'','email'=>''));
    }
    public function addventa_post(){
        $params=$this->input->post();
        $pro=$params['pro'];
        $idCliente = $params['idCliente'];
        //log_message('error',json_encode($params));
        date_default_timezone_set('America/Mexico_City');
        $this->fechaactual=date('Y-m-d H:i:s');
        //================================================
            $datosId = $params['contacto'];$idtransaccion = $params['idtransaccion'];
            $array_c=$this->ModeloCatalogos->getselectwheren('cliente_datoscontacto',array('datosId'=>$datosId));
            $atencion='';
            $correo='';
            $telefono='';
            foreach ($array_c->result() as $item) {
                $atencion=$item->atencionpara;
                $correo='';
                if($item->telefono!=''){
                    $telefono=$item->celular;
                }else{
                    $telefono=$item->telefono;  
                }
                
            }

        //================================================
        $datosCotizacion = array('atencion' => $atencion,
                                        'correo' => $correo,
                                        'telefono' => $telefono,
                                        'idCliente' => $idCliente,
                                        'tipo' => 1,
                                        //'rentaVentaPoliza' => $tipoCotizacion,
                                        'id_personal'=>2,
                                        'estatus' => 1,
                                        'reg'=>$this->fechaactual,
                                        'combinada' => 0,
                                        'tipov' => 1,
                                        'activo'=>0,
                                        'idtransaccion'=>$idtransaccion,
                                        'total_general'=>$params['total_general'],
                                        'venta_web'=>1,
                                        'tipofp_vw'=>$params['tipofp'],
                                        'v_web_direc'=>$params['direccion']
                                    );
        $idventa = $this->ModeloCatalogos->Insert('ventas',$datosCotizacion);
        $DATAp = json_decode($pro);
        for ($i=0;$i<count($DATAp);$i++) {
            if($DATAp[$i]->tipo==1){
                $equipo = $this->Equipos_model->getEquipoPorId($DATAp[$i]->iditem);

                $detallesCotizacion = array('idVenta' => $idventa,
                                            'idEquipo' => $equipo->id,
                                            'cantidad'=>$DATAp[$i]->can,
                                            'modelo' => $equipo->modelo,
                                            'especificaciones_tecnicas' => $equipo->especificaciones_tecnicas,
                                            'precio' => $equipo->costo_renta,
                                            'costo_unidad_imagen' => 0,
                                            'rendimiento_unidad_imagen' => 0,
                                            'costo_garantia_servicio' => 0,
                                            'rendimiento_garantia_servicio' => 0,
                                            'costo_pagina_monocromatica' => 0,
                                            'costo_pagina_color' => 0,
                                            'excedente' => $equipo->excedente,
                                            'tipotoner' => 1,
                                            'costo_toner' => 0,
                                            'rendimiento_toner' => 0 ,
                                            'serie_bodega'=>$this->bodega        
                                );
                            // Se insertan en la BD
                            $this->ModeloCatalogos->Insert('ventas_has_detallesEquipos',$detallesCotizacion);
            }
            if($DATAp[$i]->tipo==2){
                $result_array=$this->ModeloCatalogos->getselectwheren('catalogo_accesorios',array('id'=>$DATAp[$i]->iditem));
                $costo=0;
                foreach ($result_array->result() as $item) {
                    $costo=$item->costo;
                }

                $detallesCotizacion = array(
                                            'id_venta'=>$idventa,
                                            'id_equipo'=>2,
                                            'id_accesorio'=>$DATAp[$i]->iditem,
                                            'serie_bodega'=>$this->bodega,
                                            'costo'=>$costo,
                                            'cantidad'=>$DATAp[$i]->can
                                        );
                $this->ModeloCatalogos->Insert('ventas_has_detallesEquipos_accesorios',$detallesCotizacion);
            }
            if($DATAp[$i]->tipo==3){
                $result_array=$this->ModeloCatalogos->getselectwheren('refacciones',array('id'=>$DATAp[$i]->iditem));
                $modelo='';
                $obs='';
                $costo=0;
                foreach ($result_array->result() as $item) {
                    $modelo=$item->nombre;
                    $obs=$item->observaciones;
                }
                $result_cr=$this->ModeloCatalogos->getselectwheren('refacciones_costos',array('refacciones_id'=>$DATAp[$i]->iditem));
                foreach ($result_cr->result() as $item_cr) {
                    $costo=$item_cr->general;
                }
                $totalgeneral=$DATAp[$i]->can*$costo;
                $detallesCotizacion = array('idVentas' => $idventa,
                                                        'idEquipo' => 2,
                                                        'serie' => '',
                                                        'idRefaccion' => $DATAp[$i]->iditem,
                                                        'piezas' => $DATAp[$i]->can, 
                                                        'modelo' => $modelo,
                                                        'rendimiento' =>'',
                                                        'descripcion' => $obs,
                                                        'precioGeneral' => $costo,
                                                        'totalGeneral' => $totalgeneral,
                                                        'serie_bodega' => $this->bodega
                                                    );
                $this->ModeloCatalogos->Insert('ventas_has_detallesRefacciones',$detallesCotizacion);
            }
            if($DATAp[$i]->tipo==4){
                $datosConsumible = $this->Consumibles_model->ConsumibleConPrecios($DATAp[$i]->iditem);
                
                $result_cr=$this->ModeloCatalogos->getselectwheren('consumibles_costos',array('consumibles_id'=>$DATAp[$i]->iditem));
                foreach ($result_cr->result() as $item_cr) {
                    $costo=$item_cr->general;
                }
                $total=$costo*$DATAp[$i]->can;
                $detallesCotizacion = array('idVentas' => $idventa,
                                                    'idEquipo' => 2,
                                                    'idConsumible' => $DATAp[$i]->iditem,
                                                    'piezas' => $DATAp[$i]->can, 
                                                    'modelo' => $datosConsumible[0]->modelo,
                                                    'rendimiento' =>'',
                                                    'descripcion' => $datosConsumible[0]->observaciones,
                                                    'precioGeneral' => $costo,
                                                    'totalGeneral' => $total,
                                                    'bodegas' => $this->bodega,
                                                );
                $this->ModeloCatalogos->Insert('ventas_has_detallesConsumibles',$detallesCotizacion);
                $this->updatestock2('consumibles_bodegas','total','-',$DATAp[$i]->can,'consumiblesId',$DATAp[$i]->iditem,'bodegaId',$this->bodega);
                //$this->ModeloCatalogos->Insert('bitacora_movimientos',array('id_item'=>$DATAp[$i]->iditem,'modeloId'=>$DATAp[$i]->iditem,'modelo'=>$datosConsumible[0]->modelo,'tipo'=>4,'entrada_salida'=>0,'personal'=>2,'descripcion_evento'=>'Salida para venta '.$idventa.'','bodega_o'=>$this->bodega,'clienteId'=>$idCliente,'cantidad'=>$DATAp[$i]->can));
            }

        }
        $this->response($idventa);
    }
    public function refaccionesall_get(){
        $strq = "SELECT 
                    refi.imagen,
                    ref.id,
                    ref.codigo,
                    ref.nombre,
                    ref.uuid,
                    refc.general as costogeneral,
                    refc.poliza as costopoliza,
                    (SELECT group_concat('<col1>',eqc.name,'<col2>',eqc.descripcion,'<col3>' separator '') FROM refacciones_caracteristicas as eqc WHERE eqc.idrefaccion=ref.id AND eqc.activo=1 and eqc.general=1 ORDER BY eqc.orden ASC) as caracte,
                    (SELECT SUM(sere.cantidad)-SUM(sere.resguardar_cant) FROM series_refacciones as sere WHERE sere.refaccionid='2' AND sere.activo=1 AND sere.status=0 AND sere.bodegaId='$this->bodega' ) as stock,
                    (SELECT IFNULL(SUM(vref.piezas),0) FROM ventas as v INNER JOIN ventas_has_detallesRefacciones as vref on vref.idVentas=v.id AND vref.idRefaccion=ref.id AND vref.serie_estatus=0 WHERE v.venta_web=1 AND v.activo='$this->act_resg') as s_regu
                FROM refacciones as ref 
                INNER JOIN refacciones_costos as refc on refc.refacciones_id=ref.id 
                LEFT JOIN refacciones_imgs as refi on refi.idrefaccion=ref.id 
                WHERE ref.paginaweb=1 AND ref.status=1 and refc.general>0 GROUP BY ref.id";
        $query = $this->db->query($strq);
        $this->response($query->result());
    }
    public function consumiblesall_get(){
        $strq = "SELECT 
                    con.*,
                    conc.general,
                    conc.poliza as costopoliza,
                    (SELECT group_concat('<col1>',eqc.name,'<col2>',eqc.descripcion,'<col3>' separator '') FROM consumibles_caracteristicas as eqc WHERE eqc.idconsumible=con.id AND eqc.activo=1 and eqc.general=1 ORDER BY eqc.orden ASC) as caracte,
                    (SELECT SUM(conbo.total)-SUM(conbo.resguardar_cant) FROM consumibles_bodegas as conbo WHERE conbo.consumiblesId=con.id AND conbo.status=1 and conbo.bodegaId='$this->bodega') as stock
                FROM consumibles as con
                LEFT JOIN consumibles_costos as conc on conc.consumibles_id = con.id
                WHERE con.paginaweb=1 AND con.status=1";
        $query = $this->db->query($strq);
        $this->response($query->result());
    }
    public function consumiblesallid_get($id){
        $strq = "SELECT con.*,conc.general FROM consumibles as con
        LEFT JOIN consumibles_costos as conc on conc.consumibles_id = con.id
        WHERE con.paginaweb=1 AND con.status=1 and con.id='$id'";
        $query = $this->db->query($strq);
        $this->response($query->result());
    }
    public function getlistconsumibles_post() {
        $params = $this->input->post();
        $getdata = $this->model->getlistconsu($params);
        $totaldata= $this->model->getlistconsut($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        //echo json_encode($json_data);
        $this->response($json_data);
    }
    public function consumibleinfo_get($consu){
        //$array=$this->ModeloCatalogos->getselectwheren('consumibles',array('paginaweb'=>1,'status'=>1,'id'=>$consu));
        $strq = "SELECT 
                    con.*,
                    conc.general
                FROM consumibles as con
                LEFT JOIN consumibles_costos as conc on conc.consumibles_id = con.id
                WHERE con.paginaweb=1 AND con.status=1 and con.uuid='$consu' ";
        $array = $this->db->query($strq);
        $this->response($array->result());
    }
    public function consumibleinfocaract_get($consu){
        $strq = "SELECT * FROM `consumibles_caracteristicas` WHERE idconsumible=$consu AND activo=1 ORDER BY `orden` ASC";
        $query = $this->db->query($strq);
        $this->response($query->result());
    }
    public function consumibleimg_get($consu){
        $array=$this->ModeloCatalogos->getselectwheren('consumibles_imgs',array('idconsumible'=>$consu,'activo'=>1));
        $this->response($array->result());
    }
    public function addcotizacion_post(){
        $params=$this->input->post();
        $pro=$params['pro'];
        $idemp=$params['idemp'];
        //log_message('error',json_encode($params));
        date_default_timezone_set('America/Mexico_City');
        $this->fechaactual=date('Y-m-d H:i:s');
        //================================================
            $array_c=$this->ModeloCatalogos->getselectwheren('cliente_datoscontacto',array('clienteId'=>$idemp));
            $atencion='';
            $correo='';
            $telefono='';
            foreach ($array_c->result() as $item) {
                $atencion=$item->atencionpara;
                $correo=$item->email;
                if($item->telefono!=''){
                    $telefono=$item->celular;
                }else{
                    $telefono=$item->telefono;  
                }
                
            }

        //================================================
        $datosCotizacion = array(
                            'atencion'=>$atencion,
                            'correo'=>$correo,
                            'telefono'=>$telefono,
                            'idCliente'=>$idemp,
                            'tipo'=>5,
                            'tiporenta'=>0,
                            'eg_rm'=>0,
                            'eg_rvm'=>0,
                            'eg_rpem'=>0,
                            'eg_rc'=>0,
                            'eg_rvc'=>0,
                            'eg_rpec'=>0,
                            'nccc'=>0,
                            'rentaVentaPoliza'=>1,
                            'id_personal'=>1,
                            'propuesta'=>0,
                            'tipov'=>1,
                            'tipog'=>0,
                            'estatus'=>1,
                            'v_ser_tipo'=>0,
                            'v_ser_id'=>0
                            );
        $idventa = $this->ModeloCatalogos->Insert('cotizaciones',$datosCotizacion);
        $DATAp = json_decode($pro);
        for ($i=0;$i<count($DATAp);$i++) {
            if($DATAp[$i]->tipo==1){
                $equipo = $this->Equipos_model->getEquipoPorId($DATAp[$i]->item);

                $detallesCotizacion = array(
                                            'idCotizacion'=>$idventa,
                                            'idEquipo'=>$DATAp[$i]->item,
                                            'cantidad'=>$DATAp[$i]->can,
                                            'modelo'=>$equipo->modelo,
                                            'especificaciones_tecnicas'=>$equipo->especificaciones_tecnicas,
                                            'precio'=>$equipo->costo_venta,
                                            'costo_unidad_imagen'=>0,
                                            'rendimiento_unidad_imagen'=>0,
                                            'costo_garantia_servicio'=>0,
                                            'rendimiento_garantia_servicio'=>0,
                                            'costo_pagina_monocromatica'=>0,
                                            'costo_pagina_color'=>0,
                                            'excedente'=>0,
                                            'tipotoner'=>1,
                                            'costo_toner'=>0,
                                            'rendimiento_toner'=>0,
                                            'garantia'=>0       
                                );
                            // Se insertan en la BD
                            $this->ModeloCatalogos->Insert('cotizaciones_has_detallesEquipos',$detallesCotizacion);
            }
            if($DATAp[$i]->tipo==2){
                $result_array=$this->ModeloCatalogos->getselectwheren('catalogo_accesorios',array('id'=>$DATAp[$i]->item));
                $costo=0;
                foreach ($result_array->result() as $item) {
                    $costo=$item->costo;
                }

                $detallesCotizacion = array(
                                            'cantidad'=>$DATAp[$i]->can,
                                            'costo'=>$costo,
                                            'garantia'=>0,
                                            'id_accesorio'=>$DATAp[$i]->item,
                                            'id_cotizacion'=>$idventa,
                                            'id_equipo'=>113
                                        );
                $this->ModeloCatalogos->Insert('cotizaciones_has_detallesEquipos_accesorios',$detallesCotizacion);
            }
            if($DATAp[$i]->tipo==3){//refaccion
                $refacciones=$this->ModeloCatalogos->getselectwheren('refacciones',array('id'=>$DATAp[$i]->item));
                $modelo='';
                foreach ($refacciones->result() as $item) {
                    $modelo=$item->nombre;
                }

                $result_array=$this->ModeloCatalogos->getselectwheren('refacciones_costos',array('refacciones_id'=>$DATAp[$i]->item));
                $costo=0;
                foreach ($result_array->result() as $item) {
                    $costo=$item->general;
                }
                $cant = $DATAp[$i]->can;

                $totalgeneral=$costo*$cant;

                $detallesCotizacion = array(
                                            'descripcion'=>'',
                                            'garantia'=>0,
                                            'idCotizacion'=>$idventa,
                                            'idEquipo'=>113,
                                            'idRefaccion'=>$DATAp[$i]->item,
                                            'modelo'=>$modelo,
                                            'piezas'=>$DATAp[$i]->can,
                                            'precioGeneral'=>$costo,
                                            'serieequipo'=>'',
                                            'totalGeneral'=>$totalgeneral
                                        );
                $this->ModeloCatalogos->Insert('cotizaciones_has_detallesRefacciones',$detallesCotizacion);
            }
            if($DATAp[$i]->tipo==4){//consumibles
                $refacciones=$this->ModeloCatalogos->getselectwheren('consumibles',array('id'=>$DATAp[$i]->item));
                $modelo='';
                foreach ($refacciones->result() as $item) {
                    $modelo=$item->modelo;
                }

                $result_array=$this->ModeloCatalogos->getselectwheren('consumibles_costos',array('consumibles_id'=>$DATAp[$i]->item));
                $costo=0;
                foreach ($result_array->result() as $item) {
                    $costo=$item->general;
                }
                $cant = $DATAp[$i]->can;

                $totalgeneral=$costo*$cant;

                $detallesCotizacion = array(
                                            'descripcion'=>'',
                                            'idConsumible'=>$DATAp[$i]->item,
                                            'idCotizacion'=>$idventa,
                                            'idEquipo'=>113,
                                            'modelo'=>$modelo,
                                            'piezas'=>$cant,
                                            'precioGeneral'=>$costo,
                                            'rendimiento'=>'',
                                            'totalGeneral'=>$totalgeneral
                                        );
                $this->ModeloCatalogos->Insert('cotizaciones_has_detallesConsumibles',$detallesCotizacion);
            }


        }
        $this->response($idventa);
    }
    public function equiposclientes_get($consu){
        //$array=$this->ModeloCatalogos->getselectwheren('consumibles',array('paginaweb'=>1,'status'=>1,'id'=>$consu));
        $strq = "SELECT * FROM(SELECT eq.foto, rene.idEquipo,rene.modelo,asere.serieId,serp.serie,1 as tiporcv,0 as viewcont,0 as viewcont_cant,0 as viewcont_realizados,0 as id,0 as polid,0 as princi,
                                CASE 
                                    WHEN ceqd.direccion LIKE 'g_%' THEN (
                                                SELECT ia.direccion
                                                FROM clientes_direccion ia
                                                WHERE ia.idclientedirecc = CAST(SUBSTRING(ceqd.direccion, 3) AS UNSIGNED)
                                            )
                                            ELSE ' '
                                END AS iddirecc,asere.ubicacion
                FROM rentas as ren
                INNER JOIN rentas_has_detallesEquipos as rene on rene.idRenta=ren.id
                INNER JOIN asignacion_series_r_equipos as asere on asere.id_equipo=rene.id
                INNER JOIN series_productos as serp on serp.serieId=asere.serieId
                INNER JOIN equipos as eq on eq.id=rene.idEquipo
                INNER JOIN contrato_equipo_direccion as ceqd on ceqd.equiposrow=rene.id
                INNER JOIN contrato as con on con.idRenta=ren.id AND con.estatus=1
                WHERE ren.idCliente='$consu' AND asere.activo=1
                union
                SELECT eq.foto,clih.modelo as idEquipo,eq.modelo, 0 as serieId,clih.serie,3 as tiporcv, 0 as viewcont,0 as viewcont_cant, 0 as viewcont_realizados,0 as id,0 as polid,0 as princi,'' as iddirecc,'' as ubicacion
                FROM cliente_has_equipo_historial as clih 
                INNER JOIN equipos as eq on eq.id=clih.modelo
                WHERE clih.clienteId='$consu' AND clih.activo=1
                union
                SELECT eq.foto,veneq.idEquipo,veneq.modelo,serpro.serieId,serpro.serie,4 as tiporcv,0 as viewcont,0 as viewcont_cant,0 as viewcont_realizados,0 as id,0 as polid,0 as princi,'' as iddirecc,'' as ubicacion
                FROM ventas as ven
                INNER JOIN ventas_has_detallesEquipos as veneq on veneq.idVenta=ven.id
                INNER JOIN asignacion_series_equipos as asereq on asereq.id_equipo=veneq.id
                INNER JOIN series_productos as serpro on serpro.serieId=asereq.serieId
                INNER JOIN equipos as eq on eq.id=veneq.idEquipo
                WHERE ven.idCliente='$consu' AND ven.activo=1
                union 
                SELECT eq.foto,pold.idEquipo,eq.modelo,0 as serieId,pold.serie,2 as tiporcv,pol.viewcont,pol.viewcont_cant,pol.viewcont_realizados,pol.id,pold.id as polid,1 as princi,'' as iddirecc,'' as ubicacion
                FROM polizasCreadas as pol
                INNER JOIN polizasCreadas_has_detallesPoliza as pold on pold.idPoliza=pol.id
                inner JOIN equipos as eq on eq.id=pold.idEquipo
                WHERE pol.idCliente='$consu' AND pol.activo=1 AND pold.serie!=''
                ORDER BY id DESC       
                ) as datos
                GROUP BY idEquipo,serie,tiporcv ORDER BY `datos`.`princi` DESC";
        $array = $this->db->query($strq);
        $this->response($array->result());
    }
    public function equiposegeneral_get($cliente,$tiporcv,$group,$idg){
        //$tiporcv 1 renta,2 poliza,3 cliente historial,4 venta 
            if ($tiporcv==1) {
                
                $strq = "SELECT eq.foto, rene.idEquipo,rene.modelo,asere.serieId,serp.serie,1 as tiporcv,0 as viewcont,0 as viewcont_cant,0 as viewcont_realizados,0 as id,0 as polid,0 as princi,ceqd.direccion as iddir,concat(rene.idEquipo,ceqd.direccion) as eq_dir,
                                CASE 
                                    WHEN ceqd.direccion LIKE 'g_%' THEN (
                                                SELECT ia.direccion
                                                FROM clientes_direccion ia
                                                WHERE ia.idclientedirecc = CAST(SUBSTRING(ceqd.direccion, 3) AS UNSIGNED)
                                            )
                                            ELSE ' '
                                END AS iddirecc,asere.ubicacion,con.folio,con.idcontrato
                        FROM rentas as ren
                        INNER JOIN rentas_has_detallesEquipos as rene on rene.idRenta=ren.id
                        INNER JOIN asignacion_series_r_equipos as asere on asere.id_equipo=rene.id
                        INNER JOIN series_productos as serp on serp.serieId=asere.serieId
                        INNER JOIN equipos as eq on eq.id=rene.idEquipo
                        INNER JOIN contrato_equipo_direccion as ceqd on ceqd.equiposrow=rene.id
                        INNER JOIN contrato as con on con.idRenta=ren.id AND con.estatus=1
                        WHERE ren.idCliente='$cliente' AND asere.activo=1 ";
                        if($idg>0){
                            $strq .=" and con.idcontrato='$idg' ORDER BY iddir DESC";
                        }
                        if($group==1){
                            $strq .=" group by con.idcontrato ";
                        }
                //log_message('error',$strq);
            }
            if ($tiporcv==2) {
                $strq = "SELECT eq.foto,pold.idEquipo,eq.modelo,0 as serieId,pold.serie,2 as tiporcv,pol.viewcont,pol.viewcont_cant,pol.viewcont_realizados,pol.id,pold.id as polid,1 as princi,'' as iddirecc,'' as ubicacion,pold.nombre,pol.viewcont_disponibles,pold.direccionservicio
                            FROM polizasCreadas as pol
                            INNER JOIN polizasCreadas_has_detallesPoliza as pold on pold.idPoliza=pol.id
                            inner JOIN equipos as eq on eq.id=pold.idEquipo
                            WHERE pol.idCliente='$cliente' AND pol.activo=1 AND pold.serie!='' and pol.viewcont=1 ";
                            if($idg>0){
                                $strq .=" and pol.id='$idg' ";
                            }
                            if($group==1){
                                $strq .=" group by pol.id ";
                            }
            }
            if ($tiporcv==3) {
                $strq = "SELECT eq.foto,clih.modelo as idEquipo,eq.modelo, 0 as serieId,clih.serie,3 as tiporcv, 0 as viewcont,0 as viewcont_cant, 0 as viewcont_realizados,0 as id,0 as polid,0 as princi,'' as iddirecc,'' as ubicacion
                            FROM cliente_has_equipo_historial as clih 
                            INNER JOIN equipos as eq on eq.id=clih.modelo
                            WHERE clih.clienteId='$cliente' AND clih.activo=1";
            }
            if ($tiporcv==4) {
                $strq = "SELECT eq.foto,veneq.idEquipo,veneq.modelo,serpro.serieId,serpro.serie,4 as tiporcv,0 as viewcont,0 as viewcont_cant,0 as viewcont_realizados,0 as id,0 as polid,0 as princi,'' as iddirecc,'' as ubicacion,veneq.idVenta
                            FROM ventas as ven
                            INNER JOIN ventas_has_detallesEquipos as veneq on veneq.idVenta=ven.id
                            INNER JOIN asignacion_series_equipos as asereq on asereq.id_equipo=veneq.id
                            INNER JOIN series_productos as serpro on serpro.serieId=asereq.serieId
                            INNER JOIN equipos as eq on eq.id=veneq.idEquipo
                            WHERE ven.idCliente='$cliente' AND ven.activo=1 
                            ORDER by ven.id desc";
            }
        $array = $this->db->query($strq);
        $this->response($array->result());
    }
    public function generalpost_post(){
        $params=$this->input->post();
        $idinsert=0;
        //log_message('error',json_encode($params));
        //$params=json_decode($params);
        $tipo=$params['tipo'];
        
        if($tipo==1){
            $idc=$params['idc'];
            $fech=$params['fech'];

            $equipos = json_decode($params['equipos'], true);
            $poliza_cont=0;
            $serv_gen=0;
            foreach ($equipos as $item) {
                if($item['vc']==1){
                    $poliza_cont++;
                }else{
                    $serv_gen++;
                }
            }
            //=====================================================================
            if($serv_gen>0){
                $idinsert = $this->ModeloCatalogos->Insert('solicitud_servicio_cli',array('idcliente'=>$idc,'fecha_sol'=>$fech));

            
                foreach ($equipos as $item) {
                    if($item['vc']==1){

                    }else{
                        $this->ModeloCatalogos->Insert('solicitud_servicio_cli_dll',array('idsol'=>$idinsert,'idequipo'=>$item['ideq'],'eq_modelo'=>$item['eqmod'],'idserie'=>$item['idser'],'serie'=>$item['ser'],'detalle'=>$item['inf']));
                    }
                    
                }
            }
            if($poliza_cont>0){
                foreach ($equipos as $item) {
                    if($item['vc']==1){
                        $idinsert=$item['idsv'];
                        $this->ModeloCatalogos->updateCatalogo('polizasCreadas',array('alert'=>1,'alert_date'=>$fech),array('id' => $item['idsv']));
                        $this->ModeloCatalogos->updateCatalogo('polizasCreadas_has_detallesPoliza',array('viewser'=>1,'comeninfo'=>$item['inf']),array('id' => $item['idsvd']));
                    }
                    
                }
            }
            
            //==========================================================================
        }
        $this->response($idinsert);
    }
    public function optencionserviciosclinte_get($cliente,$limit){
        $array = $this->ModeloCatalogos->optencion_servicios_clinte($cliente,$limit);
        $this->response($array->result());
    }
    function searchproductos_get($serch){
        $strq = "SELECT id,uuid,modelo,1 tipo,foto,noparte,categoriaId FROM `equipos` WHERE estatus=1 AND paginaweb=1 AND (modelo LIKE '%$serch%' or noparte LIKE '%$serch%')
                UNION
                SELECT id,uuid,nombre as modelo,2 tipo,'' as foto,no_parte as noparte,0 as categoriaId FROM `catalogo_accesorios` WHERE paginaweb=1 AND status=1 AND (nombre LIKE '%$serch%' or no_parte LIKE '%$serch%')
                UNION
                SELECT id,uuid,nombre as modelo,3 tipo,foto,codigo as noparte,categoria as categoriaId FROM `refacciones` WHERE status=1 AND paginaweb=1 AND (nombre LIKE '%$serch%' or codigo LIKE '%$serch%')
                UNION
                SELECT id,uuid,modelo,4 as tipo,foto,parte as noparte ,0 as categoriaId FROM `consumibles` WHERE status=1 AND paginaweb=1 AND (modelo LIKE '%$serch%' or parte LIKE '%$serch%')";
        $array = $this->db->query($strq);
        $this->response($array->result());
    }
    public function doccontrato_get($id,$idcliente){  
        if($idcliente>0){
            $w_cli=" and cli.id='$idcliente' ";
            $col_status="group_concat(`ace`.`status` separator ',')  as g_status,group_concat(ace.nr separator ',')  as g_nr,";
        }else{
            $w_cli='';
            $col_status='';
        }
        $sql="SELECT
                ace.asignacionIde,
                per.personalId,
                CONCAT(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno) as tecnico,
                cli.id,
                cli.empresa,
                cli.email,
                ace.fecha,
                ace.hora,
                ac.zonaId,
                ru.nombre,
                rde.modelo,
                serp.serie,
                ace.status,
                ac.tiposervicio,
                ace.horafin,
                ac.asignacionId,
                sereve.nombre as servicio,
                ace.t_fecha,ace.t_horainicio,ace.t_horafin,
                ace.horaserinicio,ace.horaserfin,
                ace.firma,
                ace.qrecibio,
                ace.comentario,
                ac.contratoId,
                ac.prioridad,
                ac.tserviciomotivo,
                ace.idequipo,
                ace.serieId,
                ac.reg,
                ac.tservicio_a,
                pol.nombre as poliza,
                ac.tpoliza,
                ac.comentariocal,
                ac.equipo_acceso_info,
                ac.documentacion_acceso_info,
                coeq_dir.direccion,
                CASE 
                    WHEN coeq_dir.direccion LIKE 'g_%' THEN (
                                SELECT ia.direccion
                                FROM clientes_direccion ia
                                WHERE ia.idclientedirecc = CAST(SUBSTRING(coeq_dir.direccion, 3) AS UNSIGNED)
                            )
                            ELSE ' '
                END AS iddirecc,
                ac.equipo_acceso_info,
                ac.documentacion_acceso_info,
                cli.equipo_acceso,
                cli.documentacion_acceso,
                ace.cot_refaccion,
                ace.cot_refaccion2,
                ace.cot_refaccion3,
                ace.prioridad as pro_ser,
                ace.comentario_tec,
                ren.correo,
                ren.telefono,
                con.idRenta,$col_status
                rde.idEquipo as idEquipo_r
            FROM asignacion_ser_contrato_a_e AS ace 
            INNER JOIN asignacion_ser_contrato_a AS ac ON ac.asignacionId=ace.asignacionId
            LEFT JOIN personal AS per ON per.personalId=ace.tecnico
            INNER JOIN contrato AS con ON con.idcontrato=ac.contratoId
            INNER JOIN rentas AS ren ON ren.id=con.idRenta
            INNER JOIN clientes AS cli ON cli.id=ren.idCliente
            INNER JOIN rutas AS ru ON ru.id=ac.zonaId
            INNER JOIN rentas_has_detallesEquipos AS rde ON rde.id=ace.idequipo
            INNER JOIN series_productos AS serp ON serp.serieId=ace.serieId
            LEFT JOIN servicio_evento AS sereve ON sereve.id=ac.tservicio 
            LEFT JOIN polizas AS pol ON pol.id=ac.tpoliza
            LEFT join contrato_equipo_direccion as coeq_dir on coeq_dir.contrato = con.idcontrato AND coeq_dir.equiposrow = ace.idequipo AND coeq_dir.serieId =  ace.serieId
            WHERE ace.asignacionId = '$id' $w_cli";
        $query=$this->db->query($sql);
        $this->response($query->result());
    }
    function rentaequipos_accesorios1_get($idRenta,$idEquipo,$id_equipo){
        $array = $this->ModeloCatalogos->rentaequipos_accesorios1($idRenta,$idEquipo,$id_equipo);
        $this->response($array->result());
    }
    function rentaequipos_consumible1_get($idRenta,$idEquipo,$id_equipo){
        $array = $this->ModeloCatalogos->rentaequipos_consumible1($idRenta,$idEquipo,$id_equipo);
        $this->response($array->result());
    }
    function obtenerfoliosconsumiblesrenta3_get($idRenta,$id_consumibles,$id,$serieId){
        $array = $this->ModeloCatalogos->obtenerfoliosconsumiblesrenta3($idRenta,$id_consumibles,$id,$serieId);
        $this->response($array->result());
    }
    function docpoliza_get($ser,$idcliente){
        //this->ModeloCatalogos->doc_poliza
        if($idcliente>0){
            $w_cli=" and cli.id='$idcliente' ";
            $col_status="group_concat(`ape`.`status` separator ',') as g_status,group_concat(ape.nr separator ',')  as g_nr,";
        }else{
            $w_cli='';
            $col_status='';
        }
        $sql="SELECT
            ape.asignacionIde,
            per.personalId,
            CONCAT(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno) as tecnico,
            cli.id,
            cli.empresa,
            cli.email,
            ape.fecha,
            ape.hora,
            ap.zonaId,
            ru.nombre,
            equ.modelo,
            ape.serie,
            ape.status,
            ap.tiposervicio,
            ape.horafin,
            ap.asignacionId,
            sereve.nombre as servicio,
            ape.t_fecha,ape.t_horainicio,ape.t_horafin,
            ape.horaserinicio,ape.horaserfin,
            ape.firma,
            ape.qrecibio,
            ape.comentario,
            pocde.comentario as comentariopre,
            ap.reg,
            ap.prioridad,
            ap.tserviciomotivo,
            ap.polizaId,
            pocde.direccionservicio,
            pocde.nombre as polizaser,
            ap.equipo_acceso_info,
            ap.documentacion_acceso_info,
            ap.comentariocal,
            ape.comentario_tec,
            ape.prioridad as prioridadape,
            ape.cot_refaccion,ape.cot_refaccion2,ape.cot_refaccion3,
            poc.correo,
            $col_status
            poc.telefono
        FROM asignacion_ser_poliza_a_e AS ape
        INNER JOIN asignacion_ser_poliza_a AS ap ON ap.asignacionId=ape.asignacionId
        LEFT JOIN personal AS per ON per.personalId=ape.tecnico
        INNER JOIN polizasCreadas AS poc ON poc.id=ap.polizaId
        INNER JOIN clientes AS cli ON cli.id=poc.idCliente
        INNER JOIN rutas AS ru ON ru.id=ap.zonaId
        INNER JOIN polizasCreadas_has_detallesPoliza AS pocde ON pocde.id=ape.idequipo
        INNER JOIN equipos AS equ ON equ.id=pocde.idEquipo
        LEFT JOIN servicio_evento AS sereve ON sereve.id=ap.tservicio

        WHERE ape.asignacionId = '$ser' $w_cli";
        //log_message('error',$sql);
        $query=$this->db->query($sql);
        $this->response($query->result());
    }
    function docservicio_get($id,$idcliente){
        //this->ModeloCatalogos->doc_servicio
        if($idcliente>0){
            $w_cli=" and cli.id='$idcliente' ";
            $col_status="group_concat(acld.status separator ',') as g_status,group_concat(acld.nr separator ',')  as g_nr,";
        }else{
            $w_cli='';
            $col_status='';
        }
        $sql="SELECT
            acl.asignacionId,
            per.personalId,
            CONCAT(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno) as tecnico,
            cli.id,
            cli.empresa,
            cli.email,
            acl.fecha,
            acl.hora,
            acl.horafin,
            acl.zonaId,
            acl.prioridad,
            zo.nombre,
            acl.status,
            acl.tiposervicio,
            sereve.nombre as servicio,
            acl.t_fecha,acl.t_horainicio,acl.t_horafin,
            acl.firma,
            acl.qrecibio,
            acl.comentario,
            acl.reg,
            acl.tserviciomotivo,
            acl.documentaccesorio,
            acl.equipo_acceso_info,
            acl.documentacion_acceso_info,
            acl.comentariocal,
            acld.prioridad as prioridadape,
            acld.cot_refaccion,acld.cot_refaccion2,acld.cot_refaccion3,
            $col_status
            pol.nombre as nombre_pol,
            eq.modelo,
            sereve.sinmodelo,
            sereve.newmodelo,
            acld.serie,acld.direccion,acld.ttipo,acld.comentario_tec,acl.contacto_text

        FROM asignacion_ser_cliente_a AS acl
        INNER JOIN asignacion_ser_cliente_a_d as acld on acld.asignacionId=acl.asignacionId
        INNER JOIN personal AS per ON per.personalId=acl.tecnico
        INNER JOIN clientes AS cli ON cli.id=acl.clienteId
        INNER JOIN rutas AS zo ON zo.id=acl.zonaId
        LEFT JOIN servicio_evento AS sereve ON sereve.id=acl.tservicio

        inner JOIN polizas as pol on pol.id=acld.tpoliza
        left JOIN polizas_detalles as pold on pold.id=acld.tservicio_a
        left JOIN equipos as eq on eq.id=pold.modelo

        WHERE acl.asignacionId = '$id' $w_cli
        group by acl.asignacionId ";
        $query=$this->db->query($sql);
        $this->response($query->result());
    }
    function getdatosventas_get($idasignacion,$idcliente){
        //log_message('error', 'entra al modelo');
        if($idcliente>0){
            $w_cli=" and cli.id='$idcliente' ";
            $col_status="group_concat(acl.status separator ',') as g_status,group_concat(acl.nr separator ',')  as g_nr,";
        }else{
            $w_cli='';
            $col_status='';
        }
        $strq = "SELECT `acl`.`asignacionId`, `per`.`personalId`, 
                CONCAT(per.nombre, ' ', `per`.`apellido_paterno`, ' ', per.apellido_materno) as tecnico, `cli`.`id`, `cli`.`empresa`, 
                acl.fecha, 
                acl.hora,acl.horafin,
                acl.hora_comida,acl.horafin_comida, 
                acl.horaserinicioext,
                acl.horaserfinext,
                `acl`.`horafin`, `acl`.`zonaId`, 
                acl.prioridad, 
            `zo`.`nombre` as zona, 
            `acl`.`status`, 
            acl.tiposervicio, 
            `sereve`.`nombre` as `servicio`,
            acl.equipostext,
            acl.tserviciomotivo,
            acl.documentaccesorio,
            acld.serie,
            acld.direccion,
            acl.comentariocal,
            pre.observaciones,
            pre.ventaId,
            pre.tipo,
            acl.ventaId as ventaIdd,
            pol.nombre as poliza,
            acld.tservicio_a,
            clidc.atencionpara,
            clidc.email,
            clidc.telefono,
            clidca.persona_contacto,
            acl.confirmado,
            acl.reg,
            acl.tserviciomotivo,
            acl.firma,
            acl.qrecibio,
            acl.comentario,
            acl.tipov,
            acl.contacto_text,
            acl.clienteId,
            cli.equipo_acceso,
            cli.documentacion_acceso,
            acl.equipo_acceso_info,
            acl.documentacion_acceso_info,
            pre.formadecobro,
            acl.nr,
            acl.prioridadr,
            acl.cot_refaccion,
            acl.cot_refaccion2,
            acl.cot_refaccion3,
            acl.prioridad2,
            $col_status
            pol.nombre,
            eq.modelo,
            sereve.sinmodelo,
            sereve.newmodelo,
            sereve.nombre as servicio,
            acld.ttipo,acl.comentario_tec

            FROM `asignacion_ser_venta_a` `acl`
            JOIN `personal` `per` ON `per`.`personalId`=`acl`.`tecnico`
            JOIN `clientes` `cli` ON `cli`.`id`=`acl`.`clienteId`
            JOIN `rutas` `zo` ON `zo`.`id`=`acl`.`zonaId`
            JOIN `asignacion_ser_venta_a_d` `acld` ON `acld`.`asignacionId` = `acl`.`asignacionId`
            left JOIN `servicio_evento` `sereve` ON `sereve`.`id` = `acld`.`tservicio`
            left join prefactura as pre on pre.ventaId=acl.ventaId and pre.tipo=0
            left JOIN polizas as pol on pol.id=acld.tpoliza
            left join cliente_datoscontacto as clidc on clidc.datosId=pre.contactoId
            left join cliente_has_persona_contacto as clidca on clidca.id=pre.contactoId
            left JOIN polizas_detalles as pold on pold.id=acld.tservicio_a
            left JOIN equipos as eq on eq.id=pold.modelo
            where acl.activo=1 and `acl`.`asignacionId`='$idasignacion' $w_cli GROUP by acld.asignacionIdd";
        $query = $this->db->query($strq);
        $this->response($query->result());
    }
    public function updatecantcarrito_post(){
        $params=$this->input->post();
        $cant = $params['cant'];
        $masmenos = $params['masmenos'];
        $id = $params['id'];
        
        $strq="UPDATE cliente_carrito SET cant=cant $masmenos $cant WHERE id=$id";
        $this->db->query($strq);
    }
    function getconsumiblesequipo_get($idequipo,$idconsu){
                if($idequipo>0){
                    $w_equipo=" AND ec.idEquipo='$idequipo' ";
                }else{
                    $w_equipo="";
                }
                if($idconsu==0){
                    $w_consu='';
                }else{
                    $w_consu=" AND c.uuid='$idconsu' ";
                }
        $strq = "SELECT c . * ,
                (select sum(cob.total) from consumibles_bodegas as cob where cob.consumiblesId=c.id and cob.bodegaId='$this->bodega') as stock,
                (select sum(cob.resguardar_cant) from consumibles_bodegas as cob where cob.consumiblesId=c.id and cob.bodegaId='$this->bodega') as stock_resg,
                
                IFNULL((
                        SELECT JSON_ARRAYAGG(JSON_OBJECT('img', ri.imagen))
                        FROM consumibles_imgs ri
                        WHERE ri.idconsumible = c.id
                    ), '[]') AS imgs,
                cc.general,econ.costo_pagina_monocromatica,econ.costo_pagina_color
                FROM consumibles AS c
                LEFT JOIN equipos_has_consumibles AS ec ON ec.idConsumibles = c.id
                INNER JOIN consumibles_costos as cc on cc.consumibles_id=c.id
                LEFT join equipos_consumibles as econ on econ.idequipo=ec.idEquipo and c.id=econ.idconsumible
                WHERE c.status=1 AND c.paginaweb=1 $w_equipo $w_consu
                group by c.id ";//file_put_contents('uploads/rentas_hojasestado/consulta.txt',$strq);
        $query = $this->db->query($strq);
        $this->response($query->result());
    }
    function lista_equipos_post(){
        $params = $this->input->post();
        $cat = $params['cat'];
        $limit = $params['limit'];
        $start = $params['start'];
        $all_count = $params['all_count'];
        $uuid = $params['uuid'];
        //=============================
            //log_message('error','entro a la consulta $limit:'.$limit.' $start:'.$start.' $all_count:'.$all_count);
            $col = '';
            $col.='eq.*,';
            $col.="(SELECT group_concat('<col1>',eqc.name,'<col2>',eqc.descripcion,'<col3>' separator '') FROM equipos_caracteristicas as eqc WHERE eqc.idequipo=eq.id AND eqc.activo=1 and eqc.general=1 ORDER BY eqc.orden ASC) as caracte,";
            $col.="(SELECT (COUNT(*)-SUM(sepr.resguardar_cant)) totales FROM series_productos as sepr WHERE sepr.productoid=eq.id AND sepr.status<2 and sepr.bodegaId='$this->bodega') as stock,";
            $col.="IFNULL(SUM(veq.cantidad),0) as s_regu,";
            $col.="IFNULL((
                        SELECT JSON_ARRAYAGG(JSON_OBJECT('or', c.orden, 'name', c.name,'desc',c.descripcion))
                        FROM equipos_caracteristicas c
                        WHERE c.idequipo = eq.id ORDER BY orden ASC
                    ),'[]') AS caract_sub";
            $this->db->select($col);
            $this->db->from('equipos eq');
            
            $this->db->join('ventas v', "v.venta_web = 1 AND v.activo = '$this->act_resg'",'left');
            $this->db->join('ventas_has_detallesEquipos veq', "veq.idVenta=v.id AND veq.serie_estatus=0 AND veq.idEquipo=eq.id",'left');

            $this->db->where(array('eq.paginaweb'=>1,'eq.estatus'=>1,'eq.categoriaId'=>$cat));
            if($uuid!=''){
                $this->db->where(array('eq.uuid'=>$uuid));
            }
            if($all_count==0){
                $this->db->limit($limit, $start);
            }
            $query=$this->db->get();
            if($all_count==0){
                $result=$query->result();
            }else{
                $result=$query->num_rows();
            }
            
        //=============================
            $this->response($result);
    }
    function listacontratoscli_get($idemp,$limit){
            if($limit>0){
                $w_limit=" LIMIT $limit ";
            }else{
                $w_limit='';
            }
        $strq = "SELECT con.idcontrato,con.fechasolicitud,con.fechainicio,con.folio,con.uuid
                FROM contrato as con
                INNER JOIN rentas as ren on ren.id=con.idRenta
                WHERE con.estatus=1 AND ren.idCliente='$idemp'
                ORDER BY con.idcontrato DESC $w_limit
                ";
        $query = $this->db->query($strq);
        $this->response($query->result());
    }
    function listacontratoscliequipos_get($idcont){

        $strq = "SELECT * from ( 
                    SELECT 
                        con.idcontrato,
                        con.idRenta,
                        rhde.id,
                        rhde.idEquipo,
                        rhde.modelo,
                        asre.serieId,
                        sep.serie,
                        rhde.estatus,
                        asre.orden,
                        asre.ubicacion,
                        ren.idCliente,
                        (
                            select are.precio_c_e_color 
                            from alta_rentas_equipos as are  
                            where are.id_equipo=rhde.id and are.serieId=sep.serieId
                            GROUP by are.serieId limit 1
                        ) as precio_c_e_color,
                        asre.des_click,
                        asre.des_escaneo,
                        asre.poreliminar,
                        condir.direccion as dirid,
                        CASE 
                            -- Si empieza con g_, extraemos el número después del guion bajo y lo usamos en una subconsulta
                            WHEN condir.direccion IS NOT NULL AND condir.direccion LIKE 'g_%' THEN 
                                (SELECT clidir.direccion 
                                 FROM clientes_direccion as clidir 
                                 WHERE clidir.idclientedirecc = SUBSTRING_INDEX(condir.direccion, '_', -1)
                                 LIMIT 1)

                            -- Si empieza con f_, hacemos una subconsulta diferente
                            WHEN condir.direccion IS NOT NULL AND condir.direccion LIKE 'f_%' THEN 
                                (SELECT concat(clif.calle,' ',clif.num_ext,' ',clif.colonia,' ',clif.municipio,' ',clif.estado) 
                                 FROM cliente_has_datos_fiscales as clif 
                                 WHERE clif.id = SUBSTRING_INDEX(condir.direccion, '_', -1)
                                 LIMIT 1)

                            ELSE 
                                condir.direccion
                        END AS dirid_direccion
                    FROM contrato as con
                    inner join rentas_has_detallesEquipos as rhde on rhde.idRenta=con.idRenta
                    inner join asignacion_series_r_equipos as asre on asre.id_equipo=rhde.id
                    inner JOIN series_productos as sep on sep.serieId=asre.serieId
                    inner join equipos as equi on equi.id=rhde.idEquipo
                    inner join rentas as ren on ren.id=con.idRenta
                    LEFT JOIN contrato_equipo_direccion as condir on condir.equiposrow=rhde.id AND condir.serieId=asre.serieId
                    WHERE 
                        con.idcontrato='$idcont' and 
                        asre.activo=1 
                        GROUP BY rhde.idEquipo,asre.serieId
                ) as datos 
                ORDER BY orden ASC";
        $query = $this->db->query($strq);
        $this->response($query->result());
    }
    function listaventascli_get($cli,$limit){
            if($limit>0){
                $w_limit=" LIMIT $limit ";
            }else{
                $w_limit='';
            }
        $strq = "SELECT v.id,v.atencion,v.reg,v.subtotal_general,v.iva_general,v.total_general,v.idtransaccion,fac.FacturasId,fac.serie,fac.Folio,fac.rutaXml,fac.Estado,v.file_uuid
                FROM ventas as v 
                LEFT JOIN factura_venta as facv on facv.ventaId=v.id AND facv.combinada=0
                LEFT JOIN f_facturas as fac on fac.FacturasId=facv.facturaId
                WHERE v.activo=0 AND v.idCliente='$cli' AND idtransaccion IS NOT NULL
                ORDER BY v.id DESC $w_limit";
        $query = $this->db->query($strq);
        $this->response($query->result());
    }
    function lista_refacciones_post(){
        $params = $this->input->post();
        $cat = $params['cat'];
        $limit = $params['limit'];
        $start = $params['start'];
        $all_count = $params['all_count'];
        $uuid = $params['uuid'];
        //=============================
            //log_message('error','entro a la consulta $limit:'.$limit.' $start:'.$start.' $all_count:'.$all_count);
            $col = '';
            $col.='ref.id,ref.uuid,ref.codigo,ref.nombre,ref.observaciones,ref.rendimiento,';
            $col.='rco.general,rco.frecuente,rco.especial,rco.poliza,';
            $col.="(SELECT group_concat('<col1>',rec.name,'<col2>',rec.descripcion,'<col3>' separator '') FROM refacciones_caracteristicas as rec WHERE rec.idrefaccion=ref.id AND rec.activo=1 and rec.general=1 ORDER BY rec.orden ASC) as caracte,";
            $col.="(SELECT (COUNT(*)-SUM(sepr.resguardar_cant)) totales FROM series_refacciones as sepr WHERE sepr.refaccionid=ref.id AND sepr.status<2 and sepr.con_serie=1 and sepr.bodegaId='$this->bodega') as stock,";
            $col.="(SELECT (SUM(sepr.cantidad)-SUM(sepr.resguardar_cant)) totales FROM series_refacciones as sepr WHERE sepr.refaccionid=ref.id AND sepr.status<2 and sepr.con_serie=0 and sepr.bodegaId='$this->bodega') as stock2,";
            $col.="IFNULL(SUM(vref.piezas),0) as s_regu, ";
            $col.="IFNULL((
                        SELECT JSON_ARRAYAGG(JSON_OBJECT('or', c.orden, 'name', c.name,'desc',c.descripcion))
                        FROM refacciones_caracteristicas c
                        WHERE c.idrefaccion = ref.id ORDER BY orden ASC
                    ),'[]') AS caract_sub, ";
            $col.="IFNULL((
                        SELECT JSON_ARRAYAGG(JSON_OBJECT('img', ri.imagen))
                        FROM refacciones_imgs ri
                        WHERE ri.idrefaccion = ref.id 
                    ), '[]') AS imgs ";
            $this->db->select($col);
            $this->db->from('refacciones ref');
            $this->db->join('refacciones_costos rco', 'rco.refacciones_id = ref.id','left');
          
            
            $this->db->join('ventas v', "v.venta_web = 1 AND v.activo = '$this->act_resg'",'left');
            $this->db->join('ventas_has_detallesRefacciones vref', "vref.idVentas=v.id AND vref.idRefaccion=ref.id AND vref.serie_estatus=0",'left');


            $this->db->where(array('ref.paginaweb'=>1,'ref.status'=>1));
            if($uuid!=''){
                $this->db->where(array('ref.uuid'=>$uuid));
            }
            if($all_count==0){
                $this->db->limit($limit, $start);
            }
            $this->db->group_by("ref.id");
            $query=$this->db->get();
            if($all_count==0){
                $result=$query->result();
            }else{
                $result=$query->num_rows();
            }
            
        //=============================
            $this->response($result);
    }
    function consul_items_venta_get($idventa){

        $strq = "SELECT * FROM(
                    SELECT vde.id,1 as tipo_cat, eq.foto, eqimg.imagen,vde.cantidad,eq.noparte,eq.modelo,eq.uuid,vde.precio
                    FROM ventas_has_detallesEquipos as vde 
                    INNER JOIN equipos as eq on eq.id=vde.idEquipo
                    LEFT JOIN equipo_imgs as eqimg on eqimg.idequipo=eq.id AND eqimg.activo=1
                    WHERE vde.idVenta='$idventa'

                    union

                    SELECT vdc.id,2 as tipo_cat, con.foto,conimg.imagen, vdc.piezas as cantidad,con.parte as noparte,con.modelo,con.uuid,vdc.precioGeneral as precio
                    FROM ventas_has_detallesConsumibles as vdc 
                    INNER JOIN consumibles as con on con.id=vdc.idConsumible
                    LEFT JOIN consumibles_imgs as conimg on conimg.idconsumible=con.id AND conimg.activo=1
                    WHERE vdc.idVentas='$idventa'

                    union

                    SELECT vdr.id,3 as tipo_cat,ref.foto,refimg.imagen,vdr.piezas as cantidad,ref.codigo as noparte,ref.nombre as modelo,ref.uuid,vdr.precioGeneral as precio
                    FROM ventas_has_detallesRefacciones as vdr
                    INNER JOIN refacciones as ref on ref.id=vdr.idRefaccion
                    LEFT JOIN refacciones_imgs as refimg on refimg.idrefaccion=ref.id
                    WHERE vdr.idVentas='$idventa'

                    union 

                    SELECT vda.id_accesoriod as id,4 as tipo_cat,'' as foto,accimg.imagen,vda.cantidad,acc.no_parte as noparte,acc.nombre as modelo,acc.uuid,vda.costo as precio
                    FROM ventas_has_detallesEquipos_accesorios as vda
                    INNER JOIN catalogo_accesorios as acc on acc.id=vda.id_accesorio
                    LEFT JOIN catalogo_accesorios_imgs as accimg on accimg.idaccesorio=acc.id AND accimg.activo=1
                    WHERE vda.id_venta='$idventa'

                ) as dat
                GROUP BY dat.id,dat.tipo_cat";
        $query = $this->db->query($strq);
        $this->response($query->result());
    }
    function listapolizascontrato_get($idcliente,$limit){
        if($limit>0){
            $w_limit=" LIMIT $limit ";
        }else{
            $w_limit='';
        }
        $strq="SELECT 
            pc.id, 
            c.empresa, 
            pc.prefactura, 
            pc.estatus, 
            pc.reg, 
            pre.vencimiento, 
            pc.idCliente, 
            pc.tipo, 
            group_concat(DISTINCT(fac.Folio)) facturas,
            fac.FacturasId, 

            (IFNULL(sum(pd.cantidad*pd.precio_local), 0)+ IFNULL(sum(pd.cantidad*pd.precio_semi), 0)+ IFNULL(sum(pd.cantidad*pd.precio_foraneo), 0)+ IFNULL(sum(pd.cantidad*pd.precio_especial), 0)) as totalpoliza, 

            (SELECT COUNT(*) FROM polizasCreadas_has_detallesPoliza as pd WHERE pd.idPoliza=pc.id) as total_pd, 
            pc.siniva, 

            pc.viewcont_realizados,
            pc.viewcont_disponibles,
            pd.nombre,
            pc.filuuid
        FROM `polizasCreadas` `pc`
        JOIN `clientes` `c` ON `pc`.`idCliente`=`c`.`id`
        JOIN `polizasCreadas_has_detallesPoliza` `pd` ON  pd.idPoliza=pc.id
        LEFT JOIN `prefactura` `pre` ON `pre`.`ventaId`=`pc`.`id` and `pre`.`tipo`=1
        LEFT JOIN `cliente_has_datos_fiscales` `clif` ON `clif`.`id`=`pre`.`rfc_id`
        LEFT JOIN `factura_poliza` `pol` ON `pol`.`polizaId`=`pc`.`id`
        LEFT JOIN `f_facturas` `fac` ON `fac`.`FacturasId`=`pol`.`facturaId` and `fac`.`Estado`=1
        WHERE `pc`.`idCliente` = '$idcliente'
        AND `pc`.`combinada` = 0
        AND `pc`.`activo` = '1'
        AND `pc`.`viewcont` = 1
        GROUP BY `pc`.`id`
        ORDER BY `pc`.`id` DESC
        $w_limit ";
        $query = $this->db->query($strq);
        $this->response($query->result());
    }

    
    
    
}
?>
