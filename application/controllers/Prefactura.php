<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Prefactura extends CI_Controller {

	public function __construct()    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloCatalogos');
        $this->load->model('ConfiguracionDocumentos_Model');
        $this->load->model('Clientes_model');
        $this->load->model('Ventas_model');
        $this->load->model('Compras_model');
        $this->load->model('ModeloAsignacion');
        $this->load->model('ModeloGeneral');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d');
        if($this->session->userdata('logeado')==true){
            $this->idpersonal = $this->session->userdata('idpersonal');
        }else{
            //redirect('login');
        }
    }

	public function view($id=0,$tipoview=0){
        $data['ventaId'] = $id;
        $data['dia_reg']=date('d');
        $data['mes_reg']=date('m');
        $data['ano_reg']=date('y');
        $data['proinven']=$id; // es el numero de folio de la factura aparecera cuando se guarden los campos restantes o se finalise la edicion
        $data['cfd']='';
        $data['vence']='';// sera la fecha de vencimiento sera remplazada una ves guardad
        $data['vence_block']='';//el campo de vencimiento sera bloqueado una ves guardado
        $data['formapago']=0;
        $data['formapago_block']='';
        $data['cfdi']=0;
        $data['cfdi_block']='';
        $data['formacobro']='';
        $data['formacobro_block']='';
        $data['estado']=0;
        $data['cargo']='';
        $data['cargo_block']='';
        $data['metodopagoId']=0;
        $data['metodopagoId_block']='';

        $data['telId']=0;
        $data['telId_block']='';        
        $data['contactoId']=0;
        $data['contactoId_block']='';            
        $data['cargo']=0;
        $data['cargo_block']='';
        $data['block_button']=0;
        $data['block_buttonc']=0;
        $data['observaciones']='';
        $data['observaciones_block']=''; 
        $data['rfc_id']=0;
        $data['rfc_id_button']='';
        $data['colonia']='';
        $data['calle']='';
        $data['empresa']='';
        $data['empresars']='';
        $data['num_ext']='';
        $data['municipio']='';
        $data['estadovals']='';
        $data['cfdiv']='';
        $data['domicilio_entrega']='';
        $data['comentario']='';
        $data['comentario_block']='';
        $data['cp']='';
        $data['combinada']=0;$data['siniva']=0;
        $data['prefactura']=0;
        $tipov=1;
        $data['prefacturaId']=0;
        $data['v_ser_tipo']=0;
        $data['v_ser_id']=0;
        $ventaweb=0;
        //=================================================================
        $where_ventas=array('id'=>$id);
        $resultadov = $this->ModeloCatalogos->db6_getselectwheren('ventas',$where_ventas);
        $resultadovc = $this->ModeloCatalogos->db6_getselectwheren('ventacombinada',array('equipo'=>$id));
        $idventacombinada=0;
        foreach($resultadovc->result() as $itemvc){
            $idventacombinada=$itemvc->combinadaId;
        }
        if($idventacombinada==0){
            $resultadovc = $this->ModeloCatalogos->db6_getselectwheren('ventacombinada',array('consumibles'=>$id));
            $idventacombinada=0;
            foreach($resultadovc->result() as $itemvc){
                $idventacombinada=$itemvc->combinadaId;
            }   
        }
        if($idventacombinada==0){
            $resultadovc = $this->ModeloCatalogos->db6_getselectwheren('ventacombinada',array('refacciones'=>$id));
            $idventacombinada=0;
            foreach($resultadovc->result() as $itemvc){
                $idventacombinada=$itemvc->combinadaId;
            }   
        }
        $id_personal=0;
        foreach ($resultadov->result() as $item) {
            $id_personal=$item->id_personal;
            $idCliente=$item->idCliente;
            $fechaentrega=$item->fechaentrega;
            $idtransaccion=$item->idtransaccion;
            $v_web_direc=$item->v_web_direc;
            if($item->idtransaccion!='' && $item->idtransaccion!=null && $item->idtransaccion!='null'){
                $ventaweb=1;
            }
            $data['siniva']=$item->siniva;
            $data['prefactura']=$item->prefactura;
            $data['comprarow']=$item->compras;
            $data['folio_venta']=$item->folio_venta;
            $data['tipov']=$item->tipov;
            $data['v_ser_tipo']=$item->v_ser_tipo;
            $data['v_ser_id']=$item->v_ser_id;
            $data['e_entrega']=$item->e_entrega;
            if($fechaentrega=='0000-00-00' or $fechaentrega==null){
                $fechaentrega='';
            }
            if($item->tipov>0){
                $tipov=$item->tipov;
            }
            $idCotizacion=$item->idCotizacion;
        }
        if($idCotizacion>0){
            $dircot = $this->ModeloCatalogos->db6_getselectwheren('cotizaciones_dir',array('idcotizacion'=>$idCotizacion));
            foreach ($dircot->result() as $itemdir) {
                $data['domicilio_entrega']=$itemdir->dir;
            }
        }
        if($v_web_direc>0){
            $dircot = $this->ModeloCatalogos->db6_getselectwheren('clientes_direccion',array('idclientedirecc'=>$v_web_direc));
            foreach ($dircot->result() as $itemdir) {
                $data['domicilio_entrega']=$itemdir->direccion;
            }
        }
        //============================================
            $resultadovfac = $this->ModeloCatalogos->ventas_facturas(0,$id);
            $row_vfac=0;
            foreach ($resultadovfac->result() as $itemvf) {
                $row_vfac=1;
            }
            $data['row_vfac']=$row_vfac;
        //============================================
        $data['fechaentrega']=$fechaentrega;
        $data['idCliente']=$idCliente;
        $data['datoscontacto']=$this->ModeloCatalogos->obtenerdatoscontactodir($idCliente);
        $data['rfc_datos']=$this->Clientes_model->getListadoDatosFiscales($idCliente); 
        //==========================================================================
        $where_personal=array('personalId'=>$id_personal);
        $resultadop = $this->ModeloCatalogos->db6_getselectwheren('personal',$where_personal);
        $personal_nombre='';
        $personal_apellidop='';
        $personal_apellidom='';
        foreach ($resultadop->result() as $item) {
            $personal_nombre= $item->nombre;
            $personal_apellidop= $item->apellido_paterno;
            $personal_apellidom= $item->apellido_materno;
        }
        $data['ini_personal']=substr($personal_nombre, 0, 1).substr($personal_apellidop, 0, 1).substr($personal_apellidom, 0, 1);
        //============================================
            $resultadocli = $this->ModeloCatalogos->db6_getselectwheren('clientes',array('id'=>$idCliente));
            foreach ($resultadocli->result() as $item) {
                //$data['empresa']=$item->empresa;
                $data['empresars']=$item->empresa;
                $data['cargo']=$item->puesto_contacto;
                $data['razon_social']=$item->razon_social;
                $data['rfc']=$item->rfc;
                $data['credito']=$item->credito;


                $maxcredito = strtotime ( '+'.$item->credito.' day', strtotime ($this->fechahoy));
                $maxcredito = date ( 'Y-m-d' , $maxcredito );

                $data['maxcredito']= 'max="'.$maxcredito.'"'; 
                //$data['calle']=$item->calle;
                //$data['num_ext']=$item->num_ext;
                $data['colonia']=$item->colonia;
                //$data['estado']=$item->estado;
                //$data['cp']=$item->cp;
                //$data['municipio']=$item->municipio;
                $data['email']=$item->email;
                if($item->horario_disponible!=''){
                    $horario_disponible='Horario Disponible: '.$item->horario_disponible.'';
                }else{
                    $horario_disponible='';
                }
                if($item->equipo_acceso!=''){
                    $equipo_acceso=' Equipo de acceso: '.$item->equipo_acceso.'';
                }else{
                    $equipo_acceso='';
                }
                if($item->documentacion_acceso!=''){
                    $documentacion_acceso='  Documentacion de acceso: '.$item->documentacion_acceso.'';
                }else{
                    $documentacion_acceso='';
                }
                



                $data['observaciones']=$horario_disponible.' '.$equipo_acceso.' '.$documentacion_acceso;
            }
            /*
            $resultadoclidf = $this->ModeloCatalogos->db6_getselectwheren('cliente_has_datos_fiscales',array('idCliente'=>$idCliente,'activo'=>1));
            foreach ($resultadoclidf->result() as $item) {
                $data['razon_social']=$item->razon_social;
                $data['rfc']=$item->rfc;
                $data['calle']=$item->calle;
                $data['num_ext']=$item->num_ext;
                $data['colonia']=$item->colonia;
                $data['estadoval']=$item->estado;
                $data['cp']=$item->cp;
            }
            */


            $data['resultadoclitel'] = $this->ModeloCatalogos->db6_getselectwheren('cliente_has_telefono',array('idCliente'=>$idCliente));
            $data['resultadoclipcontacto'] = $this->ModeloCatalogos->db6_getselectwheren('cliente_has_persona_contacto',array('idCliente'=>$idCliente,'status'=>1));
            $data['resultadoequipos'] = $this->ModeloCatalogos->db6_getselectwheren('ventas_has_detallesEquipos',array('idVenta'=>$id));
            $data['resultadoequipos_dev'] = $this->ModeloCatalogos->db6_getselectwheren('ventas_has_detallesEquipos_dev',array('idVenta'=>$id));
            $data['resultadoaccesorios'] = $this->ModeloCatalogos->accesoriosventa($id);
            $data['resultadoaccesorios_dev'] = $this->ModeloCatalogos->accesoriosventa_dev($id);
            $data['resultadoconsumibles'] = $this->ModeloCatalogos->consumiblesventa($id);
            $data['resultadoconsumibles_dev'] = $this->ModeloCatalogos->consumiblesventa_dev($id);
            $data['consumiblesventadetalles'] = $this->ModeloCatalogos->consumiblesventadetalles($id);
            $data['consumiblesventadetalles_dev'] = $this->ModeloCatalogos->consumiblesventadetalles_dev($id);
            //$data['polizaventadetalles'] = $this->ModeloCatalogos->polizaventadetalles($id);
            //$data['polizaventadetalles_dev'] = $this->ModeloCatalogos->polizaventadetalles_dev($id);
            $data['ventadetallesrefacion'] = $this->ModeloCatalogos->ventas_has_detallesRefacciones($id);
            $data['ventadetallesrefacion_dev'] = $this->ModeloCatalogos->ventas_has_detallesRefacciones_dev($id);
        //===========================================
            $data['metodopagorow']=$this->ModeloCatalogos->view_session_f_metodopago();
            $data['formapagorow']=$this->ModeloCatalogos->db6_getselectwheren('f_formapago',array('activo'=>1));
            $data['cfdirow']=$this->ModeloCatalogos->db6_getselectwheren('f_uso_cfdi',array('activo'=>1));
            $data['estadorow']=$this->ModeloCatalogos->db6_getselectwheren('estado',array('activo'=>1));
            $data['configuracionCotizacion'] = $this->ConfiguracionDocumentos_Model->getConfiguracion($tipov);
            $confif=$this->ModeloCatalogos->db10_getselectwheren('f_configuraciones',array('ConfiguracionesId'=>$tipov));
            $data['confif']=$confif->row();
        //============================================================================================
        $resultprefactura = $this->ModeloCatalogos->db6_getselectwheren('prefactura',array('ventaId'=>$id,'tipo'=>0));
        foreach ($resultprefactura->result() as $item) {
            if($idCotizacion>0){
                $this->ModeloCatalogos->updateCatalogo('cotizaciones',array('pre_envio'=>1),array('id'=>$idCotizacion));
            }
            
            $data['prefacturaId']=$item->prefacturaId;
            //$data['proinven']=$item->prefacturaId;
            $data['vence']=$item->vencimiento;
            $data['vence_block']='disabled';
            $data['metodopagoId']=$item->metodopagoId;
            $data['metodopagoId_block']='disabled';
            $data['formapago']=$item->formapagoId;
            $data['formapago_block']='disabled';
            $data['cfdi']=$item->usocfdiId;
            $data['cfdi_block']='disabled';
            $data['formacobro']=$item->formadecobro;
            $data['formacobro_block']='disabled';
            $data['maxcredito']='';
        
            $data['telId']=$item->telId;
            $data['telId_block']='disabled';
        
            $data['contactoId']=$item->contactoId;
            $data['contactoId_block']='disabled';
            
            $data['cargo']=$item->cargo;
            $data['cargo_block']='disabled';
            $data['block_button']=1;
            $data['block_buttonc']=1;
            $fechareg=$item->reg;
            $data['dia_reg']=date('d',strtotime($fechareg));
            $data['mes_reg']=date('m',strtotime($fechareg));
            $data['ano_reg']=date('y',strtotime($fechareg));

            $data['observaciones']=$item->observaciones;
            $data['observaciones_block']='disabled'; 

            $data['rfc_id']=$item->rfc_id;
            $data['rfc_id_button']='disabled';

            $data['colonia']=$item->d_colonia;
            $data['calle']=$item->d_calle;
            $data['num_ext']=$item->d_numero;
            $data['municipio']=$item->d_ciudad;
            $data['estadovals']=$item->d_estado;
            $data['cfdiv']=$item->cfdi;
            $data['domicilio_entrega']=$item->domicilio_entrega;
            $data['comentario_block']='readonly ondblclick="desbloquearcomentarios()" ';
                
        }
        $data['get_bodegas']=$this->ModeloCatalogos->getselectwheren('bodegas',array('activo'=>1));
        $data['ventaweb']=$ventaweb;
        //============================================================================================
        if($idventacombinada>0){
            redirect('Prefactura/viewc/'.$idventacombinada);
        }
        if($tipoview==0){
            $this->load->view('prefactura/prefactura',$data);    
        }else{
            $this->load->view('Reportes/prefactura_v',$data);
        }
        
	}
    function view_info($id){
        $data['idventa']=$id;
        $where_ventas=array('id'=>$id);
        $resultadov = $this->ModeloCatalogos->db6_getselectwheren('ventas',$where_ventas);
        $resultadovc = $this->ModeloCatalogos->db6_getselectwheren('ventacombinada',array('equipo'=>$id));
        $idventacombinada=0;
        foreach($resultadovc->result() as $itemvc){
            $idventacombinada=$itemvc->combinadaId;
        }
        if($idventacombinada==0){
            $resultadovc = $this->ModeloCatalogos->db6_getselectwheren('ventacombinada',array('consumibles'=>$id));
            $idventacombinada=0;
            foreach($resultadovc->result() as $itemvc){
                $idventacombinada=$itemvc->combinadaId;
            }   
        }
        if($idventacombinada==0){
            $resultadovc = $this->ModeloCatalogos->db6_getselectwheren('ventacombinada',array('refacciones'=>$id));
            $idventacombinada=0;
            foreach($resultadovc->result() as $itemvc){
                $idventacombinada=$itemvc->combinadaId;
            }   
        }
        $id_personal=0;
        foreach ($resultadov->result() as $item) {
            $id_personal=$item->id_personal;
            $idCliente=$item->idCliente;
            $fechaentrega=$item->fechaentrega;
            if($fechaentrega=='0000-00-00' or $fechaentrega==null){
                $fechaentrega='';
            }
            $data['tipov'] =$item->tipov;
        }
        $data['resultbodegas'] = $this->ModeloCatalogos->db6_getselectwheren('bodegas',array('activo'=>1));
        $data['resultadoequipos'] = $this->ModeloCatalogos->db6_getselectwheren('ventas_has_detallesEquipos',array('idVenta'=>$id));
        $data['resultadoequipos_dev'] = $this->ModeloCatalogos->db6_getselectwheren('ventas_has_detallesEquipos_dev',array('idVenta'=>$id));
        $data['resultadoaccesorios'] = $this->ModeloCatalogos->accesoriosventa($id);
        //$data['resultadoaccesorios_dev'] = $this->ModeloCatalogos->accesoriosventa_dev($id);
        $data['resultadoconsumibles'] = $this->ModeloCatalogos->consumiblesventa($id);
        //$data['resultadoconsumibles_dev'] = $this->ModeloCatalogos->consumiblesventa_dev($id);
        $data['consumiblesventadetalles'] = $this->ModeloCatalogos->consumiblesventadetalles($id);
        //$data['consumiblesventadetalles_dev'] = $this->ModeloCatalogos->consumiblesventadetalles_dev($id);
        //$data['polizaventadetalles'] = $this->ModeloCatalogos->polizaventadetalles($id);
        $data['ventadetallesrefacion'] = $this->ModeloCatalogos->ventas_has_detallesRefacciones($id);
        //$data['ventadetallesrefacion_dev'] = $this->ModeloCatalogos->ventas_has_detallesRefacciones_dev($id);
        if($idventacombinada>0){
            redirect('Prefactura/viewc_info/'.$idventacombinada);
        }
        $this->load->view('prefactura/prefactura_info',$data);
    }
    public function viewc($id=0,$tipoview=0){
        $data['ventaId'] = $id;
        $data['dia_reg']=date('d');
        $data['mes_reg']=date('m');
        $data['ano_reg']=date('y');
        $data['proinven']=$id; // es el numero de folio de la factura aparecera cuando se guarden los campos restantes o se finalise la edicion
        $data['cfd']='';
        $data['vence']='';// sera la fecha de vencimiento sera remplazada una ves guardad
        $data['vence_block']='';//el campo de vencimiento sera bloqueado una ves guardado
        $data['formapago']=0;
        $data['formapago_block']='';
        $data['cfdi']=0;
        $data['cfdi_block']='';
        $data['formacobro']='';
        $data['formacobro_block']='';
        $data['estado']=0;
        $data['cargo']='';
        $data['cargo_block']='';
        $data['metodopagoId']=0;
        $data['metodopagoId_block']='';
        $data['cp']='';
        $data['telId']=0;
        $data['telId_block']='';        
        $data['contactoId']=0;
        $data['contactoId_block']='';            
        $data['cargo']=0;
        $data['cargo_block']='';
        $data['block_button']=0;
        $data['block_buttonc']=0;
        $data['observaciones']='';
        $data['observaciones_block']=''; 
        $data['rfc_id']=0;
        $data['rfc_id_button']='';
        $data['colonia']='';
        $data['calle']='';
        $data['num_ext']='';
        $data['municipio']='';
        $data['estadovals']='';
        $data['comentario']='';
        $data['comentario_block']='';
        $data['empresars']='';
        $data['domicilio_entrega']='';
        $data['combinada']=1;
        $data['prefactura']=0;
        $tipov=1;
        $data['prefacturaId']=0;
        $data['v_ser_tipo']=0;
        $data['v_ser_id']=0;
        $idCotizacion=0;
        //=================================================================
            $where_ventas=array('combinadaId'=>$id);
            $resultadov = $this->ModeloCatalogos->db6_getselectwheren('ventacombinada',$where_ventas);
            $id_personal=0;$idCliente=0;
            foreach ($resultadov->result() as $item) {
                $id_personal=$item->id_personal;
                $idCliente=$item->idCliente;
                $equipo=$item->equipo;
                $consumibles=$item->consumibles;
                $refacciones=$item->refacciones;
                $poliza=$item->poliza;
                $fechaentrega=$item->fechaentrega;
                $data['siniva']=$item->siniva;
                $data['folio_venta']=$item->folio_venta;
                $data['tipov']=$item->tipov;
                $data['v_ser_tipo']=$item->v_ser_tipo;
                $data['v_ser_id']=$item->v_ser_id;
                $data['e_entrega']=$item->e_entrega;
                if($fechaentrega=='0000-00-00' or $fechaentrega==null){
                    $fechaentrega='';
                }
                if($item->equipo>0){
                    $data['proinven'] =$item->equipo;
                }else if($item->consumibles>0){
                    $data['proinven'] =$item->consumibles;
                }else if($item->refacciones>0){
                    $data['proinven'] =$item->refacciones;
                }
                if($item->tipov>0){
                    $tipov=$item->tipov;
                }
                $data['prefactura']=$item->prefactura;
                $data['comprarow']=$item->compras;
            }
            $resultadop_info = $this->ModeloCatalogos->db6_getselectwheren('polizasCreadas',array('id'=>$poliza));
            foreach ($resultadop_info->result() as $item_i_p) {
                if($item_i_p->activo==0){
                    $poliza=0;
                }
                // code...
            }
            $idCotizacion=0;
            if($equipo>0){
                $resultadov_n = $this->ModeloCatalogos->db6_getselectwheren('ventas',array('id'=>$equipo));
                foreach ($resultadov_n->result() as $itemvn) {
                    $idCotizacion=$itemvn->idCotizacion;
                }
            }
            /*
            if($idCotizacion>0){
                $dircot = $this->ModeloCatalogos->db6_getselectwheren('cotizaciones_dir',array('idcotizacion'=>$idCotizacion));
                foreach ($dircot->result() as $itemdir) {
                    $data['domicilio_entrega']=$itemdir->dir;
                }
            }
            */
            
            $data['fechaentrega']=$fechaentrega;
            $data['idCliente']=$idCliente;
            $data['datoscontacto']=$this->ModeloCatalogos->obtenerdatoscontactodir($idCliente);
            $data['rfc_datos']=$this->Clientes_model->getListadoDatosFiscales($idCliente);
        //==========================================================================
            $where_personal=array('personalId'=>$id_personal);
            $resultadop = $this->ModeloCatalogos->db6_getselectwheren('personal',$where_personal);
            $personal_nombre='';
            $personal_apellidop='';
            $personal_apellidom='';
            foreach ($resultadop->result() as $item) {
                $personal_nombre= $item->nombre;
                $personal_apellidop= $item->apellido_paterno;
                $personal_apellidom= $item->apellido_materno;
            }
            $data['ini_personal']=substr($personal_nombre, 0, 1).substr($personal_apellidop, 0, 1).substr($personal_apellidom, 0, 1);
        //============================================
            $resultadocli = $this->ModeloCatalogos->db6_getselectwheren('clientes',array('id'=>$idCliente));
            foreach ($resultadocli->result() as $item) {
                $data['empresa']=$item->empresa;
                $data['empresars']=$item->empresa;
                $data['cargo']=$item->puesto_contacto;
                $data['razon_social']=$item->razon_social;
                $data['rfc']=$item->rfc;
                $data['credito']=$item->credito;
                $maxcredito = strtotime ( '+'.$item->credito.' day', strtotime ($this->fechahoy));
                $maxcredito = date ( 'Y-m-d' , $maxcredito );

                $data['maxcredito']= 'max="'.$maxcredito.'"'; 
                //$data['calle']=$item->calle;
                //$data['num_ext']=$item->num_ext;
                ///$data['colonia']=$item->colonia;
                //$data['estado']=$item->estado;
                //$data['cp']=$item->cp;
                //$data['municipio']=$item->municipio;
                $data['email']=$item->email;

                if($item->horario_disponible!=''){
                    $horario_disponible='Horario Disponible: '.$item->horario_disponible.'';
                }else{
                    $horario_disponible='';
                }
                if($item->equipo_acceso!=''){
                    $equipo_acceso=' Equipo de acceso: '.$item->equipo_acceso.'';
                }else{
                    $equipo_acceso='';
                }
                if($item->documentacion_acceso!=''){
                    $documentacion_acceso='  Documentacion de acceso: '.$item->documentacion_acceso.'';
                }else{
                    $documentacion_acceso='';
                }
                



                $data['observaciones']=$horario_disponible.' '.$equipo_acceso.' '.$documentacion_acceso;
            }
            $resultadoclidf = $this->ModeloCatalogos->db6_getselectwheren('cliente_has_datos_fiscales',array('idCliente'=>$idCliente));
            foreach ($resultadoclidf->result() as $item) {
                $data['razon_social']=$item->razon_social;
                $data['rfc']=$item->rfc;
                //$data['calle']=$item->calle;
                //$data['num_ext']=$item->num_ext;
                //$data['colonia']=$item->colonia;
                //$data['cp']=$item->cp;
            }
            //============================================
            $resultadovfac = $this->ModeloCatalogos->ventas_facturas(1,$id);
            $row_vfac=0;
            foreach ($resultadovfac->result() as $itemvf) {
                $row_vfac=1;
            }
            $data['row_vfac']=$row_vfac;
        //============================================

            $data['resultadoclitel'] = $this->ModeloCatalogos->db6_getselectwheren('cliente_has_telefono',array('idCliente'=>$idCliente));
            $data['resultadoclipcontacto'] = $this->ModeloCatalogos->db6_getselectwheren('cliente_has_persona_contacto',array('idCliente'=>$idCliente,'status'=>1));
            $data['resultadoequipos'] = $this->ModeloCatalogos->db6_getselectwheren('ventas_has_detallesEquipos',array('idVenta'=>$equipo));
            $data['resultadoequipos_dev'] = $this->ModeloCatalogos->db6_getselectwheren('ventas_has_detallesEquipos_dev',array('idVenta'=>$equipo));
            $data['resultadoaccesorios'] = $this->ModeloCatalogos->accesoriosventa($equipo);
            $data['resultadoconsumibles'] = $this->ModeloCatalogos->consumiblesventa($equipo);
            $data['resultadoconsumibles_dev'] = $this->ModeloCatalogos->consumiblesventa_dev($equipo);
            $data['consumiblesventadetalles'] = $this->ModeloCatalogos->consumiblesventadetalles($consumibles);
            $data['consumiblesventadetalles_dev'] = $this->ModeloCatalogos->consumiblesventadetalles_dev($consumibles);
            $data['polizaventadetalles'] = $this->ModeloCatalogos->polizaventadetalles3($poliza);
            $data['polizaventadetalles_dev'] = $this->ModeloCatalogos->polizaventadetalles3_dev($poliza);
            $data['ventadetallesrefacion'] = $this->ModeloCatalogos->ventas_has_detallesRefacciones($refacciones);
            $data['ventadetallesrefacion_dev'] = $this->ModeloCatalogos->ventas_has_detallesRefacciones_dev($refacciones);

        //===========================================
            $data['metodopagorow']=$this->ModeloCatalogos->view_session_f_metodopago();
            $data['formapagorow']=$this->ModeloCatalogos->db6_getselectwheren('f_formapago',array('activo'=>1));
            $data['cfdirow']=$this->ModeloCatalogos->db6_getselectwheren('f_uso_cfdi',array('activo'=>1));
            $data['estadorow']=$this->ModeloCatalogos->db6_getselectwheren('estado',array('activo'=>1));
            $data['configuracionCotizacion'] = $this->ConfiguracionDocumentos_Model->getConfiguracion($tipov);
            $confif=$this->ModeloCatalogos->db10_getselectwheren('f_configuraciones',array('ConfiguracionesId'=>$tipov));
            $data['confif']=$confif->row();
        //============================================================================================
            $i_pol_result= $this->ModeloGeneral->info_ser_poliza($poliza);
            if($i_pol_result->num_rows()>0){
                $data['iser']=1;
            }else{
                $data['iser']=0;
            }
        //============================================================================================
            $resultprefactura = $this->ModeloCatalogos->db6_getselectwheren('prefactura',array('ventaId'=>$id,'tipo'=>3));
            foreach ($resultprefactura->result() as $item) {
                if($idCotizacion>0){
                    $this->ModeloCatalogos->updateCatalogo('cotizaciones',array('pre_envio'=>1),array('id'=>$idCotizacion));
                }
                //$data['proinven']=$item->prefacturaId;
                $data['prefacturaId']=$item->prefacturaId;
                $data['vence']=$item->vencimiento;
                $data['vence_block']='disabled';
                $data['metodopagoId']=$item->metodopagoId;
                $data['metodopagoId_block']='disabled';
                $data['formapago']=$item->formapagoId;
                $data['formapago_block']='disabled';
                $data['cfdi']=$item->usocfdiId;
                $data['cfdi_block']='disabled';
                $data['formacobro']=$item->formadecobro;
                $data['formacobro_block']='disabled';
                $data['maxcredito']='';
            
                $data['telId']=$item->telId;
                $data['telId_block']='disabled';
            
                $data['contactoId']=$item->contactoId;
                $data['contactoId_block']='disabled';
                
                $data['cargo']=$item->cargo;
                $data['cargo_block']='disabled';
                $data['block_button']=1;
                $data['block_buttonc']=1;
                $fechareg=$item->reg;
                $data['dia_reg']=date('d',strtotime($fechareg));
                $data['mes_reg']=date('m',strtotime($fechareg));
                $data['ano_reg']=date('y',strtotime($fechareg));      

                $data['observaciones']=$item->observaciones;
                $data['observaciones_block']='disabled';
                $data['rfc_id']=$item->rfc_id;
                $data['rfc_id_button']='disabled'; 

                $data['colonia']=$item->d_colonia;
                $data['calle']=$item->d_calle;
                $data['num_ext']=$item->d_numero;
                $data['municipio']=$item->d_ciudad;
                $data['estadovals']=$item->d_estado;

                $data['comentario_block']='readonly ondblclick="desbloquearcomentarios()"';
            }
            $data['get_bodegas']=$this->ModeloCatalogos->getselectwheren('bodegas',array('activo'=>1));
            $data['ventaweb']=0;
        //============================================================================================
        if($tipoview==0){
            $this->load->view('prefactura/prefacturac',$data);
        }else{
            $this->load->view('Reportes/prefactura_v',$data);
        }
    }
    function viewc_info($id){
        $data['idventa']=$id;
        $where_ventas=array('combinadaId'=>$id);
            $resultadov = $this->ModeloCatalogos->db6_getselectwheren('ventacombinada',$where_ventas);
            $id_personal=0;
            foreach ($resultadov->result() as $item) {
                $id_personal=$item->id_personal;
                $idCliente=$item->idCliente;
                $equipo=$item->equipo;
                $consumibles=$item->consumibles;
                $refacciones=$item->refacciones;
                $poliza=$item->poliza;
                $fechaentrega=$item->fechaentrega;
                if($fechaentrega=='0000-00-00' or $fechaentrega==null){
                    $fechaentrega='';
                }
                if($item->equipo>0){
                    $data['proinven'] =$item->equipo;
                }else if($item->consumibles>0){
                    $data['proinven'] =$item->consumibles;
                }else if($item->refacciones>0){
                    $data['proinven'] =$item->refacciones;
                }
                $data['tipov'] =$item->tipov;
            }
            $data['resultbodegas'] = $this->ModeloCatalogos->db6_getselectwheren('bodegas',array('activo'=>1));
        $data['resultadoequipos'] = $this->ModeloCatalogos->db6_getselectwheren('ventas_has_detallesEquipos',array('idVenta'=>$equipo));
            $data['resultadoaccesorios'] = $this->ModeloCatalogos->accesoriosventa($equipo);
            $data['resultadoconsumibles'] = $this->ModeloCatalogos->consumiblesventa($equipo);
            //$data['resultadoconsumibles_dev'] = $this->ModeloCatalogos->consumiblesventa_dev($equipo);
            $data['consumiblesventadetalles'] = $this->ModeloCatalogos->consumiblesventadetalles($consumibles);
            //$data['consumiblesventadetalles_dev'] = $this->ModeloCatalogos->consumiblesventadetalles_dev($consumibles);
            $data['polizaventadetalles'] = $this->ModeloCatalogos->polizaventadetalles3($poliza);
            $data['ventadetallesrefacion'] = $this->ModeloCatalogos->ventas_has_detallesRefacciones($refacciones);
            //$data['ventadetallesrefacion_dev'] = $this->ModeloCatalogos->ventas_has_detallesRefacciones_dev($refacciones);
            $data['personallist']=$this->ModeloCatalogos->db10_getselectwheren('personal',array('estatus'=>1));
        $this->load->view('prefactura/prefacturac_info',$data);
    }
    function viewser($idcpe=0,$tipo=0){
        //==========================================================
            $data['ventaId'] = $idcpe;
            $data['tipo'] = $tipo;
            $data['dia_reg']=date('d');
            $data['mes_reg']=date('m');
            $data['ano_reg']=date('y');
            $data['proinven']=$idcpe; // es el numero de folio de la factura aparecera cuando se guarden los campos restantes o se finalise la edicion
            $data['cfd']='';
            $data['vence']='';// sera la fecha de vencimiento sera remplazada una ves guardad
            $data['vence_block']='';//el campo de vencimiento sera bloqueado una ves guardado
            $data['formapago']=0;
            $data['formapago_block']='';
            $data['cfdi_block']='';
            $data['formacobro']='';
            $data['formacobro_block']='';
            $data['estado']=0;
            $data['cargo']='';
            $data['cargo_block']='';
            $data['metodopagoId']=0;
            $data['metodopagoId_block']='';

            $data['telId']=0;
            $data['telId_block']='';        
            $data['contactoId']=0;
            $data['contactoId_block']='';            
            $data['cargo']=0;
            $data['cargo_block']='';
            $data['block_button']=0;
            $data['observaciones']='';
            $data['observaciones_block']=''; 
            $data['rfc_id']=0;
            $data['rfc_id_button']='';
            $data['colonia']='';
            $data['calle']='';
            $data['num_ext']='';
            $data['municipio']='';
            $data['estadovals']='';
            $data['comentario']='';
            $data['cfdif']='';
            $data['comentario_block']='';
            
        //=================================================================
            $data['resultadoconsumibles']=$this->Ventas_model->datosserviciosconsumibles($idcpe,$tipo);
            $data['resultadorefacciones']=$this->Ventas_model->datosserviciosrefacciones($idcpe,$tipo);
            $data['resultadoaccesorios']=$this->Ventas_model->datosserviciosaccesorios($idcpe,$tipo);
        //=================================================================
        $firma='';
        $tservicio=0;
            $t_servicio='';
            $t_servicio_motivo='';
            $tprecio=0;
            $t_modelo='';
            $t_serie='';
            $firma='';
        if ($tipo==1) {
            
            $ventacon=$this->ModeloCatalogos->db6_getselectwheren('asignacion_ser_contrato_a',array('asignacionId'=>$idcpe));
            foreach ($ventacon->result() as $item) {
                $contratoId=$item->contratoId;
                $tservicio=$item->tservicio;
                $t_servicio_motivo=$item->tserviciomotivo;
                $data['cdfif']=$item->cdfi;
            }

            





            $ventacon_a_e=$this->ModeloCatalogos->db6_getselectwheren('asignacion_ser_contrato_a_e',array('asignacionId'=>$idcpe));

            foreach ($ventacon_a_e->result() as $item) {
                $firma=$item->firma;
            }
            //=======================
            $resultadocont = $this->ModeloCatalogos->db6_getselectwheren('contrato',array('idcontrato'=>$contratoId));
            foreach ($resultadocont->result() as $item) {
                $idRenta=$item->idRenta;
            }
            $resultadorent = $this->ModeloCatalogos->db6_getselectwheren('rentas',array('id'=>$idRenta));
            foreach ($resultadorent->result() as $item) {
                $idCliente=$item->idCliente;
            }
            $ventats=$this->ModeloCatalogos->db6_getselectwheren('servicio_evento',array('id'=>$tservicio));
            foreach ($ventats->result() as $item) {
                $t_servicio=$item->nombre;
                $tprecio=$item->local;
            }
            $ventatseries=$this->ModeloCatalogos->asignaciona_a_e($idcpe);
            foreach ($ventatseries->result() as $item){
                $t_modelo=$t_modelo.$item->modelo.':'.$item->serie.',';
            }
            
        }elseif ($tipo==2) {
            $ventacon=$this->ModeloCatalogos->db6_getselectwheren('asignacion_ser_poliza_a',array('asignacionId'=>$idcpe));
            foreach ($ventacon->result() as $item) {
                $polizaId=$item->polizaId;
                $tservicio=$item->tservicio;
                $data['cdfif']=$item->cdfi;
            }
            
            $ventacon_a_e=$this->ModeloCatalogos->db6_getselectwheren('asignacion_ser_poliza_a_e',array('asignacionId'=>$idcpe));
            foreach ($ventacon_a_e->result() as $item) {
                $firma=$item->firma;
            }
            $resultadopoli = $this->ModeloCatalogos->db6_getselectwheren('polizasCreadas',array('id'=>$polizaId));
            foreach ($resultadopoli->result() as $item) {
                $idCliente=$item->idCliente;
            }
            $ventats=$this->ModeloCatalogos->db6_getselectwheren('servicio_evento',array('id'=>$tservicio));
            foreach ($ventats->result() as $item) {
                $t_servicio=$item->nombre;
                $tprecio=$item->semi;
            }
            $ventatseries=$this->ModeloCatalogos->asignaciona_p_e($idcpe);
            
            foreach ($ventatseries->result() as $item){
                $t_modelo=$t_modelo.$item->modelo.':'.$item->serie.',';

            }
        }elseif ($tipo==3) {
            $ventacon=$this->ModeloCatalogos->db6_getselectwheren('asignacion_ser_cliente_a',array('asignacionId'=>$idcpe));
            foreach ($ventacon->result() as $item) {
                $idCliente=$item->clienteId;
                $t_modelo=$item->equipostext;
                $firma=$item->firma;
                $t_servicio_motivo=$item->tserviciomotivo;
                $data['cdfif']=$item->cdfi;
            }
            $ventacon2=$this->ModeloCatalogos->db6_getselectwheren('asignacion_ser_cliente_a_d',array('asignacionId'=>$idcpe));
            $t_modelo='';
            foreach ($ventacon2->result() as $item) {
                $tservicio=$item->tservicio;
                $t_modelo.=$item->serie.' ';
            }
            $ventats=$this->ModeloCatalogos->db6_getselectwheren('servicio_evento',array('id'=>$tservicio));
            foreach ($ventats->result() as $item) {
                $t_servicio=$item->nombre;
                $tprecio=$item->foraneo;
            }

        }
        $data['firma']=$firma;
        $data['t_servicio']=$t_servicio;
        $data['t_modelo']=$t_modelo;
        $data['t_precio']=$tprecio;
        $data['t_servicio_motivo']=$t_servicio_motivo;
        $data['idCliente']=$idCliente;
        $data['datoscontacto']=$this->ModeloCatalogos->obtenerdatoscontactodir($idCliente);
        $data['rfc_datos']=$this->Clientes_model->getListadoDatosFiscales($idCliente);
        $aux_rfc=0;
        $get_rfc=$this->Clientes_model->getListadoDatosFiscales($idCliente);
        foreach ($get_rfc as $item){
            $aux_rfc=1;
        }
        //============================================
            $resultadocli = $this->ModeloCatalogos->db6_getselectwheren('clientes',array('id'=>$idCliente));
            foreach ($resultadocli->result() as $item) {
                $data['empresa']=$item->empresa;
                $data['cargo']=$item->puesto_contacto;
                $data['razon_social']=$item->razon_social;
                $data['rfc']=$item->rfc;
                $data['calle']=$item->calle;
                $data['num_ext']=$item->num_ext;
                $data['colonia']=$item->colonia;
                $data['estado']=$item->estado;
                $data['cp']=$item->cp;
                $data['municipio']=$item->municipio;
                $data['email']=$item->email;
                if($aux_rfc==1){
                    $data['empresa_t']=''; 
                }else{
                    $data['empresa_t']=$item->empresa;
                }
            }
            $resultadoclic = $this->ModeloCatalogos->db6_getselectwheren('cliente_datoscontacto',array('clienteId'=>$idCliente));
            foreach ($resultadoclic->result() as $item) {
                $data['email']=$item->email;
            }

            $resultadoclidf = $this->ModeloCatalogos->db6_getselectwheren('cliente_has_datos_fiscales',array('idCliente'=>$idCliente));
            foreach ($resultadoclidf->result() as $item) {
                $data['razon_social']=$item->razon_social;
                $data['rfc']=$item->rfc;
                $data['calle']=$item->calle;
                $data['num_ext']=$item->num_ext;
                $data['colonia']=$item->colonia;
                $data['cp']=$item->cp;
            }
            $data['resultadoclitel'] = $this->ModeloCatalogos->db6_getselectwheren('cliente_has_telefono',array('idCliente'=>$idCliente));
            $data['resultadoclipcontacto'] = $this->ModeloCatalogos->db6_getselectwheren('cliente_has_persona_contacto',array('idCliente'=>$idCliente,'status'=>1));
        //===========================================
            $data['metodopagorow']=$this->ModeloCatalogos->view_session_f_metodopago();;
            $data['formapagorow']=$this->ModeloCatalogos->db6_getselectwheren('f_formapago',array('activo'=>1));
            $data['cfdirow']=$this->ModeloCatalogos->db6_getselectwheren('f_uso_cfdi',array('activo'=>1));
            $data['estadorow']=$this->ModeloCatalogos->db6_getselectwheren('estado',array('activo'=>1));
            $data['configuracionCotizacion'] = $this->ConfiguracionDocumentos_Model->getConfiguracion(1);
        //============================================================================================
        //==========================================================================
            $where_personal=array('personalId'=>$this->idpersonal);
            $resultadop = $this->ModeloCatalogos->db6_getselectwheren('personal',$where_personal);
            $personal_nombre='';
            $personal_apellidop='';
            $personal_apellidom='';
            foreach ($resultadop->result() as $item) {
                $personal_nombre= $item->nombre;
                $personal_apellidop= $item->apellido_paterno;
                $personal_apellidom= $item->apellido_materno;
            }
            $data['ini_personal']=substr($personal_nombre, 0, 1).substr($personal_apellidop, 0, 1).substr($personal_apellidom, 0, 1);
        //============================================
        //============================================================================================
            $resultprefactura = $this->ModeloCatalogos->db6_getselectwheren('prefactura_servicio',array('asignacionId'=>$idcpe,'tipo'=>$tipo));
            $data['cfdi']='';
            foreach ($resultprefactura->result() as $item) {
                //$data['proinven']=$item->prefacturaId;
                $data['vence']=$item->vencimiento;
                $data['vence_block']='disabled';
                $data['metodopagoId']=$item->metodopagoId;
                $data['metodopagoId_block']='disabled';
                $data['formapago']=$item->formapagoId;
                $data['formapago_block']='disabled';
                $data['cfdi']=$item->usocfdiId;
                $data['cfdi_block']='disabled';
                $data['formacobro']=$item->formadecobro;
                $data['formacobro_block']='disabled';

            
                $data['telId']=$item->telId;
                $data['telId_block']='disabled';
            
                $data['contactoId']=$item->contactoId;
                $data['contactoId_block']='disabled';
                
                $data['cargo']=$item->cargo;
                $data['cargo_block']='disabled';
                $data['block_button']=1;
                $fechareg=$item->reg;
                $data['dia_reg']=date('d',strtotime($fechareg));
                $data['mes_reg']=date('m',strtotime($fechareg));
                $data['ano_reg']=date('y',strtotime($fechareg));      

                $data['observaciones']=$item->observaciones;
                $data['observaciones_block']='disabled';
                $data['rfc_id']=$item->rfc_id;
                $data['rfc_id_button']='disabled'; 

                $data['colonia']=$item->d_colonia;
                $data['calle']=$item->d_calle;
                $data['num_ext']=$item->d_numero;
                $data['municipio']=$item->d_ciudad;
                $data['estadovals']=$item->d_estado;

                $data['comentario_block']='disabled';
            }
        //============================================================================================
        //$this->load->view('prefactura/prefacturaser',$data);
        if (isset($_GET['getpdf'])) {
            $this->load->view('Reportes/pdfser',$data);
        }else{
            $this->load->view('prefactura/prefacturaser',$data);
        }
        
    }
    public function save(){
        $data = $this->input->post();
        $ventaequipos=$data['ventaequipos'];
        $ventaeqaccesorios=$data['ventaeqaccesorios'];
        $ventaeqconsumible=$data['ventaeqconsumible'];
        $ventaconsumibles=$data['ventaconsumibles'];
        $ventarefacciones=$data['ventarefacciones'];
        $prefacturaId=$data['prefacturaId'];
        $fechaentrega=$data['fechaentrega'];
        unset($data['prefacturaId']);
        unset($data['ventaequipos']);
        unset($data['ventaeqaccesorios']);
        unset($data['ventaeqconsumible']);
        unset($data['ventaconsumibles']);
        unset($data['ventarefacciones']);
        unset($data['comentario']);
        unset($data['comentario0']);
        unset($data['comentario1']);
        unset($data['comentario2']);
        unset($data['comentario3']);
        unset($data['comentario4']);
        unset($data['comentario5']);
        unset($data['comentario6']);
        unset($data['comentario7']);
        unset($data['comentario8']);
        unset($data['comentario9']);
        unset($data['fechaentrega']);
        $id=$data['ventaId'];
        $data['tipo']=0;
        if($prefacturaId>0){
            $this->ModeloCatalogos->updateCatalogo('prefactura',array('domicilio_entrega'=>$data['domicilio_entrega']),array('prefacturaId'=>$prefacturaId));
        }else{
            $this->ModeloCatalogos->Insert('prefactura',$data);
        }
        $dataup['prefactura']=1;
        $dataup['fechaentrega']=$fechaentrega;
        $this->ModeloCatalogos->updateCatalogo('ventas',$dataup,array('id' => $id));
        $caract   = array('----');
        $caract2  = array('&');

        $DATAeq = json_decode($ventaequipos);
        for ($i=0;$i<count($DATAeq);$i++){
            if($DATAeq[$i]->comen!=''){
                $comen = strval(str_replace($caract,$caract2,$DATAeq[$i]->comen));
                $this->ModeloCatalogos->updateCatalogo('ventas_has_detallesEquipos',array('comentario'=>$comen),array('id'=>$DATAeq[$i]->id));
            }
        }
        $DATAeqa = json_decode($ventaeqaccesorios);
        for ($j=0;$j<count($DATAeqa);$j++){
            if($DATAeqa[$j]->comen!=''){
                $comen = strval(str_replace($caract,$caract2,$DATAeqa[$j]->comen));
                $this->ModeloCatalogos->updateCatalogo('ventas_has_detallesEquipos_accesorios',array('comentario'=>$comen),array('id_accesoriod'=>$DATAeqa[$j]->id));
            }
        }
        $DATAeqc = json_decode($ventaeqconsumible);
        for ($k=0;$k<count($DATAeqc);$k++){
            if($DATAeqc[$k]->comen!=''){
                $comen = strval(str_replace($caract,$caract2,$DATAeqc[$k]->comen));
                $this->ModeloCatalogos->updateCatalogo('ventas_has_detallesequipos_consumible',array('comentario'=>strval(str_replace($caract,$caract2,$DATAeqc[$k]->comen))),array('id'=>$DATAeqc[$k]->id));
            }
        }
        $DATAc = json_decode($ventaconsumibles);
        for ($l=0;$l<count($DATAc);$l++){
            if($DATAc[$l]->comen!=''){
                $comen = strval(str_replace($caract,$caract2,$DATAc[$l]->comen));
                $this->ModeloCatalogos->updateCatalogo('ventas_has_detallesConsumibles',array('comentario'=>$comen),array('id'=>$DATAc[$l]->id));
            }
        }
        $DATAr = json_decode($ventarefacciones);
        for ($n=0;$n<count($DATAr);$n++){
            if($DATAr[$n]->comen!=''){
                $comen = strval(str_replace($caract,$caract2,$DATAr[$n]->comen));
            $this->ModeloCatalogos->updateCatalogo('ventas_has_detallesRefacciones',array('comentario'=>$comen),array('id'=>$DATAr[$n]->id));
            }
        }
    }
    public function savec(){
        $data = $this->input->post();
        $ventaequipos=$data['ventaequipos'];
        $ventaeqaccesorios=$data['ventaeqaccesorios'];
        $ventaeqconsumible=$data['ventaeqconsumible'];
        $ventaconsumibles=$data['ventaconsumibles'];
        $ventarefacciones=$data['ventarefacciones'];
        $comentarios=$data["comentarios"];
        $prefacturaId=$data['prefacturaId'];
        $fechaentrega=$data['fechaentrega'];
        unset($data["prefacturaId"]);
        unset($data["comentarios"]);
        unset($data['ventaequipos']);
        unset($data['ventaeqaccesorios']);
        unset($data['ventaeqconsumible']);
        unset($data['ventaconsumibles']);
        unset($data['ventarefacciones']);
        unset($data['comentario']);
        unset($data['comentario0']);
        unset($data['comentario1']);
        unset($data['comentario2']);
        unset($data['comentario3']);
        unset($data['comentario4']);
        unset($data['comentario5']);
        unset($data['comentario6']);
        unset($data['comentario7']);
        unset($data['comentario8']);
        unset($data['comentario9']);
        unset($data['fechaentrega']);
        $id=$data['ventaId'];
        $data['tipo']=3;
        if($prefacturaId>0){
            //$this->ModeloCatalogos->updateCatalogo('prefactura',array('domicilio_entrega'=>$data['domicilio_entrega']),array('prefacturaId'=>$prefacturaId));
        }else{
            $this->ModeloCatalogos->Insert('prefactura',$data);
        }
        $dataup['prefactura']=1;
        $dataup['fechaentrega']=$fechaentrega;
        $this->ModeloCatalogos->updateCatalogo('ventacombinada',$dataup,array('combinadaId' => $id));
        $resultconb=$this->ModeloCatalogos->getselectwheren('ventacombinada',array('combinadaId' => $id));
        $poliza=0;
        foreach ($resultconb->result() as $item) {
            $poliza=$item->poliza;
        }
        if($poliza>0){
            $this->ModeloCatalogos->updateCatalogo('polizasCreadas',$dataup,array('id' => $poliza));
        }
        $DATAeq = json_decode($ventaequipos);
        for ($i=0;$i<count($DATAeq);$i++){
            if($DATAeq[$i]->comen!=''){
                $this->ModeloCatalogos->updateCatalogo('ventas_has_detallesEquipos',array('comentario'=>$DATAeq[$i]->comen),array('id'=>$DATAeq[$i]->id));
            }
        }
        $DATAeqa = json_decode($ventaeqaccesorios);
        for ($j=0;$j<count($DATAeqa);$j++){
            if($DATAeqa[$j]->comen!=''){
                $this->ModeloCatalogos->updateCatalogo('ventas_has_detallesEquipos_accesorios',array('comentario'=>$DATAeqa[$j]->comen),array('id_accesoriod'=>$DATAeqa[$j]->id));
            }
        }
        $DATAeqc = json_decode($ventaeqconsumible);
        for ($k=0;$k<count($DATAeqc);$k++){
            if($DATAeqc[$k]->comen!=''){
                $this->ModeloCatalogos->updateCatalogo('ventas_has_detallesequipos_consumible',array('comentario'=>$DATAeqc[$k]->comen),array('id'=>$DATAeqc[$k]->id));
            }
        }
        $DATAc = json_decode($ventaconsumibles);
        for ($l=0;$l<count($DATAc);$l++){
            if($DATAc[$l]->comen!=''){
                $this->ModeloCatalogos->updateCatalogo('ventas_has_detallesConsumibles',array('comentario'=>$DATAc[$l]->comen),array('id'=>$DATAc[$l]->id));
            }
        }
        $DATAr = json_decode($ventarefacciones);
        for ($n=0;$n<count($DATAr);$n++){
            if($DATAr[$n]->comen!=''){
                $this->ModeloCatalogos->updateCatalogo('ventas_has_detallesRefacciones',array('comentario'=>$DATAr[$n]->comen),array('id'=>$DATAr[$n]->id));
            }
        }
        $DATAcom = json_decode($comentarios);
        for ($f=0;$f<count($DATAcom);$f++){
            if($DATAcom[$f]->comen!=''){
                $this->ModeloCatalogos->updateCatalogo('polizasCreadas_has_detallesPoliza',array('comentario'=>$DATAcom[$f]->comen,'direccionservicio'=>$DATAcom[$f]->comen),array('id'=>$DATAcom[$f]->id));
            }
        }
    }
    public function savevd(){
        $data = $this->input->post();
        $ventaequipos=$data['ventaequipos'];
        $ventaeqaccesorios=$data['ventaeqaccesorios'];
        $ventaeqconsumible=$data['ventaeqconsumible'];
        $ventaconsumibles=$data['ventaconsumibles'];
        $ventarefacciones=$data['ventarefacciones'];
        unset($data['ventaequipos']);
        unset($data['ventaeqaccesorios']);
        unset($data['ventaeqconsumible']);
        unset($data['ventaconsumibles']);
        unset($data['ventarefacciones']);
        $id=$data['ventaId'];
        $data['tipo']=0;
        $this->ModeloCatalogos->Insert('ventasd_prefactura',$data);
        $dataup['prefactura']=1;
        $this->ModeloCatalogos->updateCatalogo('ventasd',$dataup,array('id' => $id));
        $caract   = array('----');
        $caract2  = array('&');

        $DATAeq = json_decode($ventaequipos);
        for ($i=0;$i<count($DATAeq);$i++){
            if($DATAeq[$i]->comen!=''){
                $comen = strval(str_replace($caract,$caract2,$DATAeq[$i]->comen));
                $this->ModeloCatalogos->updateCatalogo('ventasd_has_detallesequipos',array('comentario'=>$comen),array('id'=>$DATAeq[$i]->id));
            }
        }
        $DATAeqa = json_decode($ventaeqaccesorios);
        for ($j=0;$j<count($DATAeqa);$j++){
            if($DATAeqa[$j]->comen!=''){
                $comen = strval(str_replace($caract,$caract2,$DATAeqa[$j]->comen));
                $this->ModeloCatalogos->updateCatalogo('ventasd_has_detallesequipos_accesorios',array('comentario'=>$comen),array('id_accesoriod'=>$DATAeqa[$j]->id));
            }
        }
        $DATAeqc = json_decode($ventaeqconsumible);
        for ($k=0;$k<count($DATAeqc);$k++){
            if($DATAeqc[$k]->comen!=''){
                $comen = strval(str_replace($caract,$caract2,$DATAeqc[$k]->comen));
                $this->ModeloCatalogos->updateCatalogo('ventasd_has_detallesequipos_consumible',array('comentario'=>strval(str_replace($caract,$caract2,$DATAeqc[$i]->comen))),array('id'=>$DATAeqc[$k]->id));
            }
        }
        $DATAc = json_decode($ventaconsumibles);
        for ($l=0;$l<count($DATAc);$l++){
            if($DATAc[$l]->comen!=''){
                $comen = strval(str_replace($caract,$caract2,$DATAc[$l]->comen));
                $this->ModeloCatalogos->updateCatalogo('ventasd_has_detallesconsumibles',array('comentario'=>$comen),array('id'=>$DATAc[$l]->id));
            }
        }
        $DATAr = json_decode($ventarefacciones);
        for ($n=0;$n<count($DATAr);$n++){
            if($DATAr[$n]->comen!=''){
                $comen = strval(str_replace($caract,$caract2,$DATAr[$n]->comen));
            $this->ModeloCatalogos->updateCatalogo('ventasd_has_detallesrefacciones',array('comentario'=>$comen),array('id'=>$DATAr[$n]->id));
            }
        }
    }
    //f1
    public function updatevequipodetalle(){  
       $id = $this->input->post('id');
       $surt= $this->input->post('surtir');
       $data = array('surtir' => $surt);
       $idv['id'] = $id; 
       $this->ModeloCatalogos->updateCatalogo('ventas_has_detallesEquipos',$data,$idv);
    }
    public function updatevequipodetalleaccesorio(){  
       $id = $this->input->post('id');
       $surt= $this->input->post('surtir');
       $data = array('surtir' => $surt);
       $idv['id'] = $id; 
       $this->ModeloCatalogos->updateCatalogo('ventas_has_detallesequipos_consumible',$data,$idv);
    }
    public function updateconsumiblesventadetalles(){
       $id = $this->input->post('id');
       $surt= $this->input->post('surtir');
       $data = array('surtir' => $surt);
       $idv['id'] = $id; 
       $this->ModeloCatalogos->updateCatalogo('ventas_has_detallesConsumibles',$data,$idv);
    }
    public function updateaccesoriosventa()
    {
       $id = $this->input->post('id');
       $surt= $this->input->post('surtir');
       $data = array('surtir' => $surt);
       $idv['id_accesoriod'] = $id; 
       $this->ModeloCatalogos->updateCatalogo('ventas_has_detallesEquipos_accesorios',$data,$idv);
    }
    public function updatepolizasCreadas_has_detallesPoliza()
    {
       $id = $this->input->post('id');
       $surt= $this->input->post('surtir');
       $data = array('surtir' => $surt);
       $idv['id'] = $id; 
       $this->ModeloCatalogos->updateCatalogo('polizasCreadas_has_detallesPoliza',$data,$idv);
    }
    public function updateventas_has_detallesRefacciones()
    {
       $id = $this->input->post('id');
       $surt= $this->input->post('surtir');
       $data = array('surtir' => $surt);
       $idv['id'] = $id; 
       $this->ModeloCatalogos->updateCatalogo('ventas_has_detallesRefacciones',$data,$idv);
    }
    public function selecciontrcf()
    {   $rfc_id = $this->input->get('id');
        $result=$this->Clientes_model->getListadoDatosFiscalesprefactura($rfc_id); 
        echo json_encode($result);
    }
    public function saveser(){
        $data = $this->input->post();
        $this->ModeloCatalogos->Insert('prefactura_servicio',$data);
    }
    public function viewd($id=0,$tipov=0){
        $data['ventaId'] = $id;
        $data['dia_reg']=date('d');
        $data['mes_reg']=date('m');
        $data['ano_reg']=date('y');
        $data['proinven']=$id; // es el numero de folio de la factura aparecera cuando se guarden los campos restantes o se finalise la edicion
        $data['cfd']='';
        $data['vence']='';// sera la fecha de vencimiento sera remplazada una ves guardad
        $data['vence_block']='';//el campo de vencimiento sera bloqueado una ves guardado
        $data['formapago']=0;
        $data['formapago_block']='';
        $data['cfdi']=0;
        $data['cfdi_block']='';
        $data['formacobro']='';
        $data['formacobro_block']='';
        $data['estado']=0;
        $data['cargo']='';
        $data['cargo_block']='';
        $data['metodopagoId']=0;
        $data['metodopagoId_block']='';

        $data['telId']=0;
        $data['telId_block']='';        
        $data['contactoId']=0;
        $data['contactoId_block']='';            
        $data['cargo']=0;
        $data['cargo_block']='';
        $data['block_button']=0;
        $data['observaciones']='';
        $data['observaciones_block']=''; 
        $data['rfc_id']=0;
        $data['rfc_id_button']='';
        $data['colonia']='';
        $data['calle']='';
        $data['empresa']='';
        $data['empresars']='';
        $data['num_ext']='';
        $data['municipio']='';
        $data['estadovals']='';
        $data['cfdiv']='';
        $data['domicilio_entrega']='';
        $data['comentario']='';
        $data['comentario_block']='';
        $data['cp']='';
        $data['combinada']=0;$data['siniva']=0;
        //=================================================================
        $where_ventas=array('id'=>$id);
        $resultadov = $this->ModeloCatalogos->db6_getselectwheren('ventasd',$where_ventas);
       
        $id_personal=0;
        foreach ($resultadov->result() as $item) {
            $id_personal=$item->id_personal;
            $idCliente=$item->idCliente;
            $fechaentrega=$item->fechaentrega;
            $data['siniva']=$item->siniva;
            if($fechaentrega=='0000-00-00' or $fechaentrega==null){
                $fechaentrega='';
            }
        }
        $data['fechaentrega']=$fechaentrega;
        $data['idCliente']=$idCliente;
        $data['datoscontacto']=$this->ModeloCatalogos->obtenerdatoscontactodir($idCliente);
        $data['rfc_datos']=$this->Clientes_model->getListadoDatosFiscales($idCliente); 
        //==========================================================================
        $where_personal=array('personalId'=>$id_personal);
        $resultadop = $this->ModeloCatalogos->db6_getselectwheren('personal',$where_personal);
        $personal_nombre='';
        $personal_apellidop='';
        $personal_apellidom='';
        foreach ($resultadop->result() as $item) {
            $personal_nombre= $item->nombre;
            $personal_apellidop= $item->apellido_paterno;
            $personal_apellidom= $item->apellido_materno;
        }
        $data['ini_personal']=substr($personal_nombre, 0, 1).substr($personal_apellidop, 0, 1).substr($personal_apellidom, 0, 1);
        //============================================
            $resultadocli = $this->ModeloCatalogos->db6_getselectwheren('clientes',array('id'=>$idCliente));
            foreach ($resultadocli->result() as $item) {
                //$data['empresa']=$item->empresa;
                $data['empresars']=$item->empresa;
                $data['cargo']=$item->puesto_contacto;
                $data['razon_social']=$item->razon_social;
                $data['rfc']=$item->rfc;
                $data['credito']=$item->credito;


                $maxcredito = strtotime ( '+'.$item->credito.' day', strtotime ($this->fechahoy));
                $maxcredito = date ( 'Y-m-d' , $maxcredito );

                $data['maxcredito']= 'max="'.$maxcredito.'"'; 
                //$data['calle']=$item->calle;
                //$data['num_ext']=$item->num_ext;
                $data['colonia']=$item->colonia;
                //$data['estado']=$item->estado;
                //$data['cp']=$item->cp;
                //$data['municipio']=$item->municipio;
                $data['email']=$item->email;
                if($item->horario_disponible!=''){
                    $horario_disponible='Horario Disponible: '.$item->horario_disponible.'';
                }else{
                    $horario_disponible='';
                }
                if($item->equipo_acceso!=''){
                    $equipo_acceso=' Equipo de acceso: '.$item->equipo_acceso.'';
                }else{
                    $equipo_acceso='';
                }
                if($item->documentacion_acceso!=''){
                    $documentacion_acceso='  Documentacion de acceso: '.$item->documentacion_acceso.'';
                }else{
                    $documentacion_acceso='';
                }
                



                $data['observaciones']=$horario_disponible.' '.$equipo_acceso.' '.$documentacion_acceso;
            }
            /*
            $resultadoclidf = $this->ModeloCatalogos->db6_getselectwheren('cliente_has_datos_fiscales',array('idCliente'=>$idCliente,'activo'=>1));
            foreach ($resultadoclidf->result() as $item) {
                $data['razon_social']=$item->razon_social;
                $data['rfc']=$item->rfc;
                $data['calle']=$item->calle;
                $data['num_ext']=$item->num_ext;
                $data['colonia']=$item->colonia;
                $data['estadoval']=$item->estado;
                $data['cp']=$item->cp;
            }
            */


            $data['resultadoclitel'] = $this->ModeloCatalogos->db6_getselectwheren('cliente_has_telefono',array('idCliente'=>$idCliente));
            $data['resultadoclipcontacto'] = $this->ModeloCatalogos->db6_getselectwheren('cliente_has_persona_contacto',array('idCliente'=>$idCliente,'status'=>1));
            $data['resultadoequipos'] = $this->ModeloCatalogos->db6_getselectwheren('ventasd_has_detallesequipos',array('idVenta'=>$id));
            $data['resultadoequipos_dev'] = $this->ModeloCatalogos->db6_getselectwheren('ventas_has_detallesEquipos_dev',array('idVenta'=>0));//se le quito el id
            $data['resultadoaccesorios'] = $this->ModeloCatalogos->accesoriosventasd($id);
            $data['resultadoaccesorios_dev'] = $this->ModeloCatalogos->accesoriosventa_dev(0);//se le quito el id
            $data['resultadoconsumibles'] = $this->ModeloCatalogos->consumiblesventasd($id);
            $data['resultadoconsumibles_dev'] = $this->ModeloCatalogos->consumiblesventa_dev(0);//se le quito el id
            $data['consumiblesventadetalles'] = $this->ModeloCatalogos->consumiblesventasddetalles($id);
            $data['consumiblesventadetalles_dev'] = $this->ModeloCatalogos->consumiblesventadetalles_dev(0);//se le quito el id
            $data['polizaventadetalles'] = $this->ModeloCatalogos->polizaventadetalles(0);//se le quito el id
            $data['ventadetallesrefacion'] = $this->ModeloCatalogos->ventasd_has_detallesrefacciones($id);
            $data['ventadetallesrefacion_dev'] = $this->ModeloCatalogos->ventas_has_detallesRefacciones_dev(0);//se le quito el id
        //===========================================
            $data['metodopagorow']=$this->ModeloCatalogos->view_session_f_metodopago();
            $data['formapagorow']=$this->ModeloCatalogos->db6_getselectwheren('f_formapago',array('activo'=>1));
            $data['cfdirow']=$this->ModeloCatalogos->db6_getselectwheren('f_uso_cfdi',array('activo'=>1));
            $data['estadorow']=$this->ModeloCatalogos->db6_getselectwheren('estado',array('activo'=>1));
            $data['configuracionCotizacion'] = $this->ConfiguracionDocumentos_Model->getConfiguracion(1);//cuando se llegue al apartado de facturacion esta parte se cambiara por 2
        //============================================================================================
        $resultprefactura = $this->ModeloCatalogos->db6_getselectwheren('ventasd_prefactura',array('ventaId'=>$id));
        foreach ($resultprefactura->result() as $item) {
            //$data['proinven']=$item->prefacturaId;
            $data['vence']=$item->vencimiento;
            $data['vence_block']='disabled';
            $data['metodopagoId']=$item->metodopagoId;
            $data['metodopagoId_block']='disabled';
            $data['formapago']=$item->formapagoId;
            $data['formapago_block']='disabled';
            $data['cfdi']=$item->usocfdiId;
            $data['cfdi_block']='disabled';
            $data['formacobro']=$item->formadecobro;
            $data['formacobro_block']='disabled';
            $data['maxcredito']='';
        
            $data['telId']=$item->telId;
            $data['telId_block']='disabled';
        
            $data['contactoId']=$item->contactoId;
            $data['contactoId_block']='disabled';
            
            $data['cargo']=$item->cargo;
            $data['cargo_block']='disabled';
            $data['block_button']=1;
            $fechareg=$item->reg;
            $data['dia_reg']=date('d',strtotime($fechareg));
            $data['mes_reg']=date('m',strtotime($fechareg));
            $data['ano_reg']=date('y',strtotime($fechareg));

            $data['observaciones']=$item->observaciones;
            $data['observaciones_block']='disabled'; 

            $data['rfc_id']=$item->rfc_id;
            $data['rfc_id_button']='disabled';

            $data['colonia']=$item->d_colonia;
            $data['calle']=$item->d_calle;
            $data['num_ext']=$item->d_numero;
            $data['municipio']=$item->d_ciudad;
            $data['estadovals']=$item->d_estado;
            $data['cfdiv']=$item->cfdi;
            $data['domicilio_entrega']=$item->domicilio_entrega;
            $data['comentario_block']='disabled';
                
        }
        $data['get_bodegas']=$this->ModeloCatalogos->getselectwheren('bodegas',array('activo'=>1));
        //============================================================================================
        
        if($tipov==0){
            $this->load->view('prefactura/prefactura_vd',$data);    
        }else{
            $this->load->view('Reportes/prefactura_v_vd',$data);
            //$this->load->view('Reportes/prefactura_v',$data);
        }
        
    }
    function addfolioventa(){
        $params = $this->input->post();
        $ventaId = $params['ventaId'];
        $tipo = $params['tipo'];
        $folio_venta = $params['folio_venta'];

        if($tipo==0){
            $this->ModeloCatalogos->updateCatalogo('ventas',array('folio_venta'=>$folio_venta),array('id'=>$ventaId));
        }
        if($tipo==1){
            $this->ModeloCatalogos->updateCatalogo('ventacombinada',array('folio_venta'=>$folio_venta),array('combinadaId'=>$ventaId));
        }
    }
 
}