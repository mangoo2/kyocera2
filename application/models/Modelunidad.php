<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class Modelunidad extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    function get_listado($params){
        if(isset($params['tecnico'])){
            $tecnico=$params['tecnico'];
        }else{
            $tecnico=0;
        }
        $columns = array( 
            0=>'uni.id',
            1=>'uni.modelo',
            2=>'uni.marca',
            3=>'uni.placas',
            4=>'uni.kilometraje',
            5=>'uni.tipo_unidad',
            6=>'uni.vigencia_poliza_seguro',
            7=>'uni.proximo_servicio',
            8=>'per.nombre',
            9=>'per.apellido_paterno',
            10=>'per.apellido_materno',
            11=>'uni.file',
            12=>'uni.file_poliza'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('unidades uni');
        $this->db->join('personal per', 'per.personalId = uni.tecnico_asignado','left');

        if($tecnico>0){
            $this->db->where(array('uni.tecnico_asignado'=>$tecnico));
        }
        $this->db->where(array('uni.activo'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    public function total_listado($params){
        if(isset($params['tecnico'])){
            $tecnico=$params['tecnico'];
        }else{
            $tecnico=0;
        }
        $columns = array( 
            0=>'uni.id',
            1=>'uni.modelo',
            2=>'uni.marca',
            3=>'uni.placas',
            4=>'uni.kilometraje',
            5=>'uni.tipo_unidad',
            6=>'uni.vigencia_poliza_seguro',
            7=>'uni.proximo_servicio',
        );
        $this->db->select('COUNT(*) as total');
        $this->db->from('unidades uni');
        if($tecnico>0){
            $this->db->where(array('tecnico_asignado'=>$tecnico));
        }
        $this->db->where(array('activo'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            

        $query=$this->db->get();
      
        return $query->row()->total;
    }
}