<style type="text/css" class="print_css">
  .datosClienteCotizacion{
    background: <?php echo $configuracionCotizacion->color ?> !important;
    -webkit-print-color-adjust:exact;
  }
  html{
        -webkit-print-color-adjust: exact;
  }
  .tablaBordesPrincipal p{
    margin: 0px
  }
  .badgeaction{
    border-radius: 13px !important;
    min-width: 24px !important;
    cursor: pointer;
  }
  @media only screen and (max-width: 450px){
    .jconfirm-box{
      width: 100% !important;
    }
  }
  @media print {
    html{
        -webkit-print-color-adjust: exact;
    }
    .btn_imp{
      display: none;
    }
    td,th{
      font-size: 12px;
      padding: 0px;
    }
    .contenido4 h1{
      font-size: 10px;
    }
    .img_equipos{
      width: 90px !important;
    }
    .badgeaction,.button_add_cotizacion{
      display: none;
    }
  }
  .jconfirm-content input{
    width: 99% !important;
  }
</style>
<?php 
  $rowitems=0;
?>
<input type="hidden" id="estatuscot" value="<?php echo $datosCotizacion[0]->estatus;?>">
<input type="hidden" id="idCliente" value="<?php echo $datosCliente->id;?>">
<!-- START CONTENT -->
<section id="content" width="100%">
  <!--start container-->
  <div class="container" width="100%">
    <div class="section" style="margin-top: -53px;">
     <br>
      <div class="row">
        <input id="base_url" type="hidden" name="base_url" value="<?php echo base_url(); ?>">
        <input id="idCotizacion" type="hidden" name="idCotizacion" value="<?php echo $datosCotizacion[0]->id; ?>">
        
        <!-- FORMULARIO  card-panel-->
        <div class="col s12">
          <div class="">

            <!-- ENCABEZADO PRINCIPAL PDF -->
            <div class="row"> 
              <div class="col s3">
                <img src="<?php echo base_url(); ?>app-assets/images/<?php echo $configuracionCotizacion->logo1; ?>" style="height: 70px;">
              </div>
              <div class="col s6">
                <p class="headerPDF"><?php echo $configuracionCotizacion->textoHeader; ?></p>
              </div> 
              <div class="col s3">
                <p align="center"><img class="" src="<?php echo base_url(); ?>app-assets/images/1024_kyocera_logo_mds.png" alt="materialize logo"  style="max-width:200px; width: 100%;"></p>
              </div>
            </div>

            <!-- APARTADO DE INFORMACIÓN DE RECEPTOR DE COTIZACIÓN-->
            
            
            <div class="row">
              <table class="centered datosClienteCotizacion tablaBordesHeader" style="width:100%">
                  <thead>
                    
                  </thead>
                  <tbody class="tablaBordes">
                    <?php 
                          $onclick_edit_contac="";
                          if(isset($_GET['vte'])){ 
                            
                          }else{
                              if(isset($_GET['view'])){

                              }else{
                                $onclick_edit_contac=' onclick=edit_cont('.$idCotizacion.')';
                              } 
                            
                          }
                    ?>
                    <tr >
                      <td class="tablaBordesHeader info_contacto" data-atencionpara="<?php echo $datosCotizacion[0]->atencion ?>" data-correo="<?php echo $datosCotizacion[0]->correo ?>" data-tel="<?php echo $datosCotizacion[0]->telefono ?>">
                        Cotización Especial Para: <br>
                        <?php echo $datosCliente->empresa ?>
                      </td>
                      <td class="tablaBordesHeader" <?php echo $onclick_edit_contac; ?> >
                        Atención para: <br>
                        <?php echo $datosCotizacion[0]->atencion ?>
                      </td>
                      <td class="tablaBordesHeader" <?php echo $onclick_edit_contac; ?> >
                        Correo: <?php echo $datosCotizacion[0]->correo ?> <br>
                        Teléfono: <?php echo $datosCotizacion[0]->telefono ?>
                      </td>
                      <td class="tablaBordesHeader">
                        Cotización No. <?php echo $datosCotizacion[0]->id?> 
                      </td>
                    </tr>
                  </tbody>
                </table>
            </div>
            
            <div align="center"><div class="" id="textoHeader2" contenteditable="true"><?php echo $configuracionCotizacion->textoHeader2; ?></div></div>
            <?php $total=0; $GLOBALS["total"]=0;?>
            <div class="row button_add_cotizacion">
              <div class="col s3">
                <a class="btn-bmz btn cyan waves-effect waves-light obj_remove_x" type="button" href="<?php echo base_url(); ?>Cotizaciones/altaCotizacion/<?php echo $button_add_cotizacion;?>&eleccionCotizar=0&rentaventapoliza=1" >Agregar nuevo</a>
              </div>
            </div>