var base_url =$('#base_url').val();
var ac_fecha_ac = $('#ac_fecha_ac').val();
var mv_tipo;
var mv_asignacion;
var mv_tec_d;
var mv_tec_o;
var mv_tec_o_t;
$(document).ready(function($) {
	$('.click_load').click(function(event) {
        $('body').loader('show');
    });
    tableroinfo();
    if(ac_fecha_ac==1){
        setInterval(function () {
            tableroinfo();
        }, 60000);//60 seg    
    }else{
        tableroinfo();
    }
    
});
function cambiarhora(tipo,idser,hora,horaf,horac,horacf,horaf_res){
    var fecha = $('#fechaactual').val();
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: 'Desea cambiar la Fecha/hora?<br>'+
                '<div class="row rowc">\
                <div class="col s12"><label>Hora Inicial</label><input type="date" id="newfecha" class="form-control-bmz" value="'+fecha+'"/></div>\
                <div class="col s6"><label>Hora Inicial</label><input type="time" id="newhora" class="form-control-bmz" value="'+hora+'" data-horao="'+hora+'" data-fechao="'+fecha+'" /></div>\
                <div class="col s6"><label>Hora Final</label><input type="time" id="newhoraf" class="form-control-bmz" value="'+horaf+'" data-horao="'+horaf_res+'" readonly/></div>\
                <div class="col s6"><label>Hora Inicial comida</label><input type="time" id="newhorac" class="form-control-bmz" value="'+horac+'"/></div>\
                <div class="col s6"><label>Hora Final comida</label><input type="time" id="newhoracf" class="form-control-bmz" value="'+horacf+'"/></div>\
                </div>',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var newfecha=$('#newfecha').val();
                var newhora=$('#newhora').val();
                var newhoraf=$('#newhoraf').val();
                var newhorafo=$('#newhoraf').data('horao');
                var newhorac=$('#newhorac').val();
                var newhoracf=$('#newhoracf').val();

                var horao=$('#newhora').data('horao');
                var fechao=$('#newhora').data('fechao');
                var valid_horas=esMayorHora(newhorafo, newhora)
                if(valid_horas){
                    if (newhora!='') {
                         $.ajax({
                            type:'POST',
                            url: base_url+"index.php/TableroControl/cambiarhora",
                            data: {
                                tipo:tipo,
                                idser:idser,
                                nfecha:newfecha,
                                nhora:newhora,
                                nhoraf:newhoraf,
                                nhorac:newhorac,
                                nhoracf:newhoracf,
                                horao:horao,
                                fechao:fechao,
                            },
                            success: function (response){
                                toastr["success"]("Cambio realizado");
                                 tableroinfo();
                                setTimeout(function(){ 
    	                     		//window.location.reload();
    			                }, 1000);
                            },
                            error: function(response){
                                toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema");
                            }
                        });
                        
                    }else{
                        toastr["warning"]("Ingrese una hora valida");
                    }
                }else{
                    toastr.error('Fuera de horario de rango');
                }
            },
            cancelar: function (){
                
            }
        }
    });
}
function esMayorHora(hora1, hora2) {
    // Crear objetos Date para las dos horas
    const hoy = new Date(); // Obtenemos la fecha de hoy

    // Extraer año, mes y día
    const año = hoy.getFullYear();
    const mes = String(hoy.getMonth() + 1).padStart(2, '0'); // Los meses van de 0 a 11, por eso se suma 1
    const dia = String(hoy.getDate()).padStart(2, '0'); // 

    const date1 = new Date(`${año}-${mes}-${dia} ${hora1}`);
    const date2 = new Date(`${año}-${mes}-${dia} ${hora2}`);
    
    // Comparar las horas
    return date1 >= date2;
}
function notificacionrechazogeneral(tipo,asignacion){
    var htmlnr='';
    htmlnr+='<label>Tipo</label>';
    htmlnr+='<select id="comtipo" class="form-control-bmz"><option value="0">Otro</option><option value="1">backup</option><option value="2">no se acudio al sitio</option><option value="3">sin acceso</option><option value="4">oficina cerrada</option><option value="5">intercambio de servicio</option></select>';
    htmlnr+='<label>Motivo</label>';
    htmlnr+='<textarea class="form-control-bmz" id="notificarerror"></textarea>';
    $.confirm({
        boxWidth: '90%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Motivo Rechazo',
        content: htmlnr,
        type: 'red',
        typeAnimated: true,
        buttons: 
        {
            confirmar: function (){
                var info=$('#notificarerror').val();
                var comtipo=$('#comtipo  option:selected').val();
                $.ajax({
                    type:'POST',
                    url: base_url+'index.php/AsignacionesTecnico/notificacionrechazogeneral',
                    data: {
                        asignacion:asignacion,
                        tipo:tipo,
                        info:info,
                        comtipo:comtipo
                    },
                    success:function(data){
                        toastr["success"]("Solicitud Agregada");
                        //obtenerclientes();
                        tableroinfo();
                        setTimeout(function(){ 
	                     	//window.location.reload();
			             }, 1000);
                    }
                });
                
            },
            cancelar: function (){
            }
        }
    });
}
function movertr(){
    $( ".table-servicios tbody" ).sortable();
}
//--------------------------movimientos
function permitirSoltar(ev, tecd) {
    ev.preventDefault();
    mv_tec_d=parseInt(tecd);
}
function soltar(ev, n1) {
    //ev.preventDefault();
    //var data = ev.dataTransfer.getData(n1);
    //$('#cambiarasi').modal('toggle');
    if(mv_tec_d>0 && mv_tec_o>0){
    	if(mv_tec_d!=mv_tec_o){
    		confirmacionmv();
    	}
    }
    if(mv_tec_d>0 && mv_tec_o_t>0){
        if(mv_tec_d!=mv_tec_o_t){
            confirmacionmv_todo();
        }
    }
}
function arrastrar(ev,tipo,asignacion,teco) {
    ev.dataTransfer.setData("Text", ev.target.id);
    console.log('tipo: '+tipo+' asignacion: '+asignacion);
    mv_tipo=tipo;
    mv_asignacion=asignacion
    mv_tec_o=parseInt(teco);
    
}
function arrastrar_todo(ev,teco) {
    //ev.dataTransfer.setData("Text", ev.target.id);
    console.log('tecnico todo: '+teco);
    mv_tec_o_t=parseInt(teco);
    
}
//------------------------
function confirmacionmv(){
	var nametec=$('.tecnico_info_'+mv_tec_d).data('nametec');
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: 'Confirma que desea cambiar el servicio al tecnico: '+nametec+' ?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                
                     $.ajax({
                        type:'POST',
                        url: base_url+"index.php/TableroControl/cambiarserviciotec",
                        data: {
                            tipo:mv_tipo,
                            idser:mv_asignacion,
                            tecd:mv_tec_d,
                            teco:mv_tec_o
                        },
                        success: function (response){
                            toastr["success"]("cambio realizado");
                            tableroinfo();
                            setTimeout(function(){ 
	                     		//window.location.reload();
			                }, 1000);
                        },
                        error: function(response){
                            toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema");
                        }
                    });
                    
                
            },
            cancelar: function (){
                mv_tec_d=0;
                mv_tec_o=0;
                mv_tec_o_t=0;
            }
        }
    });
}
function confirmacionmv_todo(){
    var nametec=$('.tecnico_info_'+mv_tec_d).data('nametec');
    var fecha = $('#fechaactual').val();
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: 'Confirma que desea cambiar todos los servicios al tecnico: '+nametec+' ?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                
                     $.ajax({
                        type:'POST',
                        url: base_url+"index.php/TableroControl/confirmacionmv_todo",
                        data: {
                            fecha:fecha,
                            tecd:mv_tec_d,
                            teco:mv_tec_o_t
                        },
                        success: function (response){
                            toastr["success"]("cambio realizado");
                             tableroinfo();
                            setTimeout(function(){ 
                                //window.location.reload();
                            }, 1000);
                        },
                        error: function(response){
                            toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema");
                        }
                    });
                    
                
            },
            cancelar: function (){
                mv_tec_d=0;
                mv_tec_o=0;
                mv_tec_o_t=0;
            }
        }
    });
}
function obtenerdirecciones(tipo,servicio){
    var fechav=$('#fechaactual').val();
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Calendario/obtenerdirecciones",
        data: {
            tipo:tipo,
            servicio:servicio,
            fecha:fechav
        },
        success: function (response){
            console.log(response);
            var array = $.parseJSON(response);
            var html='<table class="table striped"><thead><tr><th>Modelo</th><th>Serie</th><th>Dirección</th></tr></thead><tbody>';

            $.each(array, function(index, item) {
                html+='<tr><td>'+item.modelo+'</td><td>'+item.serie+'</td><td>'+item.dir+'</td></tr>';
            });
                html+='</tbody></table>';

            $.alert({boxWidth: '30%',useBootstrap: false,title: 'Equipos',theme: 'bootstrap',content: html}); 
        },
        error: function(response){
            toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema");
        }
    });
}

function tableroinfo(){
    //$('body').loader('show');
    var fech=$('#fechaactualtablero').val();
    var mov=$('#mov_seer_activo').val();
    $.ajax({
        type:'POST',
        url: base_url+"index.php/TableroControl/tableroinfo",
        data: {
            fech:fech,
            mov:mov
        },
        success: function (response){
            //console.log(response);
            $('.info_tablero').html(response);
            //$('body').loader('hide');
        },
        error: function(response){
            toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema");
        }
    });
}