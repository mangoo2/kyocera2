var base_url = $('#base_url').val();
var idreg_dll=0;
$(document).ready(function() {
    base_url = $('#base_url').val();
  summernote();
  $('.enviodeticket').click(function(event) {
    var idreg=$('#idreg').val();
    var gurl= $('#base_url').val()+'Control_tareas/agregar_res';
    console.log(gurl);
    //console.log("Función ejecutada en la ventana secundaria.");
            
            // Ejecuta una función en la ventana principal
    /*
            if (window.opener && !window.opener.closed) {
                window.opener.funcionPrincipal();
            } else {
                alert("La ventana principal ya no está abierta.");
            }
            */
    
            $.ajax({
                type:'POST',
                url: gurl,
                data: {
                    idreg:idreg,
                    contenido: $('.summernote').val()
                },
                async: false,
                statusCode:{
                    404: function(data){
                        swal("Error", "404", "error");
                    },
                    500: function(){
                        swal("Error", "500", "error"); 
                    }
                },
                success:function(data){
                    idreg_dll=parseInt(data);
                    var input = document.getElementById('files');
                    if(input.files.length>0){
                        $('#files').fileinput('upload');
                    }
                  //$('.summernote').val('');
                  $('.summernote').summernote('reset');//para limpiar el campo
                  obtenerinfo();
                    
                    const div = document.getElementById("obtenerinfo");
                    div.scrollTo({ top: div.scrollHeight, behavior: "smooth" });
                }
            });
    
  });
  obtenerinfo();
  //setInterval(function () {obtenerinfo()}, 2000);
  //================================
  var url_fileinput=base_url+'Control_tareas/imagenes_multiple';
  console.log(url_fileinput);
    $("#files").fileinput({
        showCaption: true,
        dropZoneEnabled: false,
        showUpload: true,// quita el boton de upload
        //rtl: true,
        allowedFileExtensions: ["jpg","png","webp","jpeg","pdf","xlsx","xls"],
        browseLabel: 'Seleccionar documentos',
        uploadUrl: url_fileinput,
        maxFilePreviewSize: 6000,
        
        uploadExtraData: function (previewId, index) {
        var info = {
                    idac:idreg_dll,
                    tipo:1
                };
        return info;
      }
    }).on('filebatchuploadcomplete', function(event, files, extra) {
      //location.reload();
        
        setTimeout(function(){ 
        $('#files').fileinput('clear');
        }, 2000);
    }).on('filebatchuploadsuccess', function(event, files, extra) {
      //location.reload();
      //toastr.success('Se cargo el Certificado Correctamente','Hecho!');
    });
  //================================
});
function summernote(){
  //$('.summernote').summernote({toolbar: [ ['style', ['bold', 'italic', 'underline', 'clear']],['insert', ['link', 'picture', 'video']]]});
  $('.summernote').summernote();
}
function obtenerinfo(){
    var base_url = $('#base_url').val();
    var urlget=base_url+'Control_tareas/infoview';
    console.log(urlget);
    var idreg = $('#idreg').val();
    $.ajax({
        type:'POST',
        url: urlget,
        data: {
            idreg: idreg
        },
        async: false,
        statusCode:{
            404: function(data){
                swal("Error", "404", "error");
            },
            500: function(){
                swal("Error", "500", "error"); 
            }
        },
        success:function(data){

            $('.obtenerinfo').html(data);
            $('.obtenerinfo .card-body img').removeAttr('style');
            $('.obtenerinfo .card-body img').addClass('materialboxed');
            $('.materialboxed').materialbox();
        }
    });
}