<section id="content" width="100%">
          

          <!--start container-->
          <div class="container" width="100%">
            <div class="section">
              <div class="row">
                <p class="caption col s6">Configuración de Tipo de Cambio.</p>
                
              </div>
              <div class="row" >
                <input id="base_url" type="hidden" name="base_url" value="<?php echo base_url(); ?>">
                
                <!-- FORMULARIO -->
                <form class="col s12 " id="tipo-cambio-form" method="post">

                  <div class="col s12 m12 24" >

                    <div class="card-panel" align="center">

                      <div class="row">
                        <div class="row" >

                          <div class="input-field col s2">
                            <label>Tipo de Cambio Equipos</label>
                          </div>
                          <div class="input-field col s4">
                            <i class="material-icons prefix">attach_money</i>
                            <input id="cambio_equipos" name="cambio_equipos" type="text" placeholder="Equipos" value="<?php echo $tiposCambio[0]->cambio; ?>">
                          </div>                          

                          <div class="input-field col s2">
                            <label>Tipo de Cambio Refacciones</label>
                          </div>
                          <div class="input-field col s4">
                            <i class="material-icons prefix">attach_money</i>
                            <input id="cambio_refacciones" name="cambio_refacciones" type="text" placeholder="Refacciones" value="<?php echo $tiposCambio[1]->cambio; ?>" onchange="comienzacalculo()">
                          </div>  

                        </div>

                        <div class="row" >

                          <div class="input-field col s2">
                            <label>Tipo de Cambio Consumibles</label>
                          </div>
                          <div class="input-field col s4">
                            <i class="material-icons prefix">attach_money</i>
                            <input id="cambio_consumibles" name="cambio_consumibles" type="text" placeholder="Consumibles" value="<?php echo $tiposCambio[2]->cambio; ?>">
                          </div>                          

                          <div class="input-field col s2">
                            <label>Tipo de Cambio Accesorios</label>
                          </div>
                          <div class="input-field col s4">
                            <i class="material-icons prefix">attach_money</i>
                            <input id="cambio_accesorios" name="cambio_accesorios" type="text" placeholder="Accesorios" value="<?php echo $tiposCambio[3]->cambio; ?>">
                          </div>

                        </div>

                      </div>

                      <div class="row">
                        <div class="row" >
                          <div class="input-field col s2">
                            <button id="button" name="actualiza_cambio_equipos" id="actualiza_cambio_equipos" class="btn waves-effect waves-light green darken-1"> Actualizar</button> 
                          </div>
                        </div>
                      </div>

                    </div>

                  </div>

                </form>
              </div>

          </div>
        </div>
      </section>
<style type="text/css">
  #tabla_descuentos_refacciones th,#tabla_descuentos_refacciones td {
    border: 1px solid;
    text-align: center;
  }
  .form-control-bmz{
    margin-bottom: 0px !important;
    text-align: center;
  }
  #tabla_descuentos_refacciones td{
    padding: 0px;
  }
</style>
<input type="hidden" id="porc_gana_refa" value="<?php echo $porc_gana_refa;?>">
<section id="content" width="100%" style="margin-top: 0px;">
          

          <!--start container-->
          <div class="container" width="100%">
            <div class="section">
              <div class="row" >
                
                <!-- FORMULARIO -->

                  <div class="col s12 m12 24" >

                    <div class="card-panel" align="center">

                      <div class="row">
                       <div class="col s12 m12">
                         <table id="tabla_descuentos_refacciones" class="striped">
                           <thead>
                             <tr>
                               <th rowspan="3" colspan="2">
                                 Precio en dolares
                               </th>
                               <th colspan="4">
                                 Tabla de Descuentos
                               </th>
                             </tr>
                             <tr>
                               <th>Foráneo</th>
                               <th>Semi</th>
                               <th>Local</th>
                               <th rowspan="2">Clientes "AAA"</th>
                             </tr>
                             <tr>
                               <th>General</th>
                               <th>Frecuente</th>
                               <th>Poliza</th>
                             </tr>
                             <tr>
                               <td>de</td>
                               <td>a</td>
                               <td>0%</td>
                               <td>30%</td>
                               <td>35%</td>
                               <td>35%</td>
                             </tr>
                           </thead>
                           <tbody>
                           <?php foreach ($tablacondiciones->result() as $item): ?>
                             <tr>
                               <td>
                                 <input type="hidden" class="form-control-bmz" id="id" value="<?php echo $item->id;?>">
                                 <input type="number" class="form-control-bmz" id="pd_inicio" value="<?php echo $item->pd_inicio;?>">
                               </td>
                               <td>
                                 <input type="number" class="form-control-bmz" id="pd_fin" value="<?php echo $item->pd_fin;?>">
                               </td>
                               <td>
                                 <input type="number" 
                                 class="form-control-bmz for_gen_<?php echo $item->id;?>" 
                                 id="for_gen" 
                                 value="<?php echo $item->for_gen;?>" readonly>
                               </td>
                               <td>
                                 <input type="number" 
                                 class="form-control-bmz sem_frec_<?php echo $item->id;?>" 
                                 id="sem_frec" 
                                 value="<?php echo $item->sem_frec;?>" readonly>
                               </td>
                               <td>
                                 <input type="number" 
                                 class="form-control-bmz local_poli_<?php echo $item->id;?>" 
                                 id="local_poli" 
                                 value="<?php echo $item->local_poli;?>" readonly>
                               </td>
                               <td>
                                 <input type="number" 
                                 class="form-control-bmz aaa_<?php echo $item->id;?>" 
                                 id="aaa" 
                                 value="<?php echo $item->aaa;?>">
                               </td>
                             </tr>
                           <?php endforeach ?> 
                             
                           </tbody>
                         </table>
                       </div> 
                        

                      </div>

                     

                    </div>

                  </div>

                
              </div>

          </div>
        </div>
      </section>