<?php
if(isset($prospecto))
{
$idProspecto = $prospecto->id;
$empresa = $prospecto->empresa;
}
?>
<!-- START CONTENT -->
<section id="content" width="100%">
  <!--start container-->
  <div class="container" width="100%">
    <div class="section">
      <h4 class="caption">Cotización</h4>
      <div class="row">
        <input id="base_url" type="hidden" name="base_url" value="<?php echo base_url(); ?>">
        <!-- FORMULARIO -->
        <div class="col s12 m12 24">
          <div class="card-panel">
            <div class="row">
              <form class="form" id="clientes_from" method="post">
                <?php
                if(isset($prospecto))
                {
                ?>
                <input id="idProspecto" name="idProspecto" type="hidden" value="<?php echo $idProspecto; ?>">
                <?php
                }
                ?>
                <div class="row">
                    <div class="input-field col s4">
                      <i class="material-icons prefix">monetization_on</i>
                      <input id="tipo" name="tipo"  type="text" disabled="" value="<?php echo $tipo; ?>">
                      <label for="cliente">Tipo de cotización</label>
                    </div>
                    <div class="input-field col s4">
                      <i class="material-icons prefix">location_city</i>
                      <input id="cliente" name="cliente"  type="text" disabled="" value="<?php echo $empresa; ?>">
                      <label for="cliente">Cliente</label>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="input-field col s4">
                      <i class="material-icons prefix">person</i>
                      <input id="cotizar" name="cotizar"  type="text" disabled="" value="<?php echo $cotizar; ?>">
                      <label for="cliente">Cotización</label>
                    </div>
                </div>
                <div class="row">
                  <?php foreach ($equipos as $item){ ?>
                        <div class="input-field col s3" align="center">
                          <div class="fileinput fileinput-new" data-provides="fileinput">
                               <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 300px; height: 300px; border-radius: 12px;">
                              <?php if ($item->foto!=''){ ?>
                                <input type="file" id="foto" name="foto" class="dropify" data-default-file="<?php echo base_url(); ?>uploads/equipos/<?php echo $item->foto ?>">
                                <a class="btn btn-info" href="<?php echo base_url(); ?>uploads/equipos/<?php echo $item->foto ?>" target="_blank">visualizar</a>
                              <?php }?>  
                              </div>
                          </div>
                        </div>
                        <div class="col s1">
                          <input type="checkbox" id="test_<?php echo $item->id ?>" /><label for="test_<?php echo $item->id ?>"></label>
                        </div> 
                
                   <?php } ?>
                   
                </div>    
                <hr>
                <div class="row">
                  <div class="input-field col s12">
                    <button class="btn cyan waves-effect waves-light right" type="submit">Guardar
                    <i class="material-icons right">send</i>
                    </button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>