<?php
$a=session_id();
if(empty($a)) session_start();
defined('BASEPATH') OR exit ('No direct script access allowed');

class Modelodimpresion extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    public function getDataEquipo2(){
        $sql = "SELECT eq.*,
                (
                    SELECT count(*) as total
                    FROM series_productos as eqs 
                    WHERE 
                    eqs.productoid=eq.id and 
                    eqs.status<=1 and 
                    eqs.activo=1 and 
                    eqs.bodegaId=9
                ) as stockequipo
                FROM equipos as eq 
                WHERE eq.estatus=1";
        $query = $this->db->query($sql);
        return $query->result();
    } 

}