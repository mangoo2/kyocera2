<style type="text/css">
  #tabla_accesorios td{
    font-size: 12px;
  }
  .ui-datepicker-trigger img{
    width: 18px;
  }
  
  .modal-sm {
      width: 500px;
  }
  .prioridad_1{
    background: #63be7b;
  }
  .prioridad_2{
    background: #85c87d;
  }
  .prioridad_3{
    background: #a8d27f;
  }
  .prioridad_4{
    background: #cbdc81;
  }
  .prioridad_5{
    background: #ede683;
  }
  .prioridad_6{
    background: #ffdd82;
  }
  .prioridad_7{
    background: #fdc07c;
  }
  .prioridad_8{
    background: #fca377;
  }
  .prioridad_9{
    background: #f8696b;
  }
</style>
<input type="hidden" id="idpersonal" value="<?php echo $idpersonal ?>">
<!-- START CONTENT -->
    <section id="content">
      <!--start container-->
        <div class="container">
          <div class="section">
          
          <input id="base_url" type="hidden" name="base_url" value="<?php echo base_url(); ?>">

          <div id="table-datatables">
            <br>
                <h4 class="header">Facturas pendientes de rentas</h4>
                <div class="row">
                      <div class="col s12 m12 l12">
                        <div class="card-panel">
                          <!-- 
                          <div class="row">
                            <div class="col-md-2">
                              <input type="" id="fechaselected" style="display: none;">
                            </div>
                          </div> -->
                          <div class="row">
                            <div class="col s2">
                              <label for="tiporeporte">Fecha inicio</label>
                              <input type="date" id="f1" class="form-control-bmz">
                            </div>
                            <div class="col s2">
                              <label for="tiporeporte">Fecha final</label>
                              <input type="date" id="f2" class="form-control-bmz">
                            </div>
                            <div class="col s2">
                              <label for="tiporeporte">Cliente</label>
                              <select class="browser-default form-control-bmz" id="idcliente" ></select>
                            </div>
                            <div class="col s2">
                              <label for="tiporeporte" style="color: #ff000000;">__</label>
                              <select class="browser-default form-control-bmz" id="tipo" onchange="loadtable()">
                                   <option value="1">Vigentes</option> 
                                   <option value="0">Descartadas</option> 
                                   <option value="2">Facturadas</option> 
                              </select>
                            </div>
                            <div class="col s2 tipov_div">
                              <label for="tiporeporte" style="color: #ff000000;">__</label>
                              <select class="browser-default form-control-bmz" id="tipov" onchange="loadtable()">
                                   <option value="0">Todas</option> 
                                   <option value="1">Pendientes de capturar periodos</option> 
                                   <option value="2">Pendientes de factura</option> 
                              </select>
                            </div>
                            <div class="col s2">
                              
                              <button class="b-btn b-btn-primary" onclick="loadtable()" title="Realizar consulta" style="margin-top: 20px;"><i class="fas fa-filter"></i> <span class="btn-filtrar"></span></button>
                            </div>
                          </div>  
                          <div class="row">
                            <div class="col s12" >
                                 <table id="tabla_accesorios">
                                  <thead>
                                    <tr>
                                        <th>Contrato</th>
                                        <th>Cliente</th>
                                        <th>Periodo Inicio</th>
                                        <th>Periodo fin</th>
                                        <th>Ejecutiva</th>
                                        <th>Timbrado</th>
                                        <th>Persona timbro</th>
                                        <th>Estatus</th>
                                        <th>Asignadas</th>
                                        <th><?php if($idpersonal!=13){ 
                                          echo 'Acciones';
                                        }?></th> 
                                    </tr>
                                  </thead>
                                  <tbody>
                                        
                                  </tbody>
                                </table>
                            </div>

                          </div>
                        </div>
                      </div>
                    </div>
              </div>
            </div>  
               </div>
        </section>

<div id="modalasignar" class="modal modal-sm">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <h4>Asignar</h4>
      <div class="row">
         <div class="col s5">
          <label style="color: #ff000000;">___</label><br>
          <div class="switch">
            <label>
              <input type="checkbox" id="asignarcheck">
              <span class="lever"></span>
            </label>
          </div>
          </div> 
          <div class="col s7">
            <label for="tiporeporte">Fecha</label>
            <input type="date" id="fechaasig" class="form-control-bmz">
          </div>
      </div>
      <div class="row" align="center">
            <div class="col s2">
              <label>Prioridad</label>
            </div>
            <div class="col s5">
              <select name="priodidad2" id="prioridad2" class="form-control-bmz browser-default" wtx-context="481C0F31-218B-4167-842B-4FD3F7FE572A">
                <option value="1" class="prioridad_1">Ninguno</option>
                <option value="2" class="prioridad_2">Muy Pequeño</option>
                <option value="3" class="prioridad_3">Pequeño</option>
                <option value="4" class="prioridad_4">Muy Bajo</option>
                <option value="5" class="prioridad_5">Bajo</option>
                <option value="6" class="prioridad_6">Moderado</option>
                <option value="7" class="prioridad_7">Alto</option>
                <option value="8" class="prioridad_8">Muy alto</option>
                <option value="9" class="prioridad_9">Critico</option>
              </select>
            </div>
      </div>
      <div class="row">
        <div class="col s12">
          <label>Comentario</label>
          <textarea class="form-control-bmz" id="comentario_asig"></textarea>
        </div>
      </div>
    </div>
    <div class="modal-footer">
      <a class="modal-action modal-close waves-effect waves-red gray btn-flat " onclick="saveasignar()">Aceptar</a>
      <a class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
    </div>
  </div>
</div>

<div id="modal_asignar_t" class="modal modal-sm">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <h4>Asignar</h4>
      <div class="row">
         <div class="col s5">
          <label style="color: #ff000000;">___</label><br>
          <div class="switch">
            <label>
              <input type="checkbox" id="asignarcheckt">
              <span class="lever"></span>
            </label>
          </div>
          </div> 
          <div class="col s7">
            <label for="tiporeporte">Fecha</label>
            <input type="date" id="fechaasigt" class="form-control-bmz">
          </div>
      </div>
      <div class="row" align="center">
            <div class="col s2">
              <label>Prioridad</label>
            </div>
            <div class="col s5">
              <select name="priodidad2" id="prioridad2t" class="form-control-bmz browser-default" wtx-context="481C0F31-218B-4167-842B-4FD3F7FE572A">
                <option value="1" class="prioridad_1">Ninguno</option>
                <option value="2" class="prioridad_2">Muy Pequeño</option>
                <option value="3" class="prioridad_3">Pequeño</option>
                <option value="4" class="prioridad_4">Muy Bajo</option>
                <option value="5" class="prioridad_5">Bajo</option>
                <option value="6" class="prioridad_6">Moderado</option>
                <option value="7" class="prioridad_7">Alto</option>
                <option value="8" class="prioridad_8">Muy alto</option>
                <option value="9" class="prioridad_9">Critico</option>
              </select>
            </div>
      </div>
      <div class="row">
        <div class="col s12">
          <label>Comentario</label>
          <textarea class="form-control-bmz" id="comentario_asigt"></textarea>
        </div>
      </div>
    </div>
    <div class="modal-footer">
      <a class="modal-action modal-close waves-effect waves-red gray btn-flat " onclick="saveasignart()">Aceptar</a>
      <a class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
    </div>
  </div>
</div>
