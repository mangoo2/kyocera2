<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/plugins/toastr/toastr.css">
<script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/toastr/toastr.min.js" ></script>

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/plugins/touchspin/jquery.bootstrap-touchspin.min.css">
<script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/touchspin/jquery.bootstrap-touchspin.min.js" ></script>

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/isotope.css">
<script type="text/javascript" src="<?php echo base_url(); ?>public/js/isotope.pkgd.min.js" ></script>

<script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/tipped/tipped-master/dist/js/tipped.min.js" ></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/plugins/tipped/tipped-master/dist/css/tipped.css">

<script type="text/javascript" src="<?php echo base_url(); ?>public/js/form_ventas.js?v=<?php echo date('YmdGis');?>" ></script>
<script type="text/javascript">
	direccionesclientes(<?php echo $idCliente;?>);
	obtenerservicios2();
	obtenerventaspendientes(<?php echo $idCliente;?>);
	bodegasequipo(0);
	function bodegasequipo(id){
		if(id>0){
			var etiqueta='.notificacion_tipped_'+id;
		}else{
			var etiqueta='.notificacion_tipped';
		}
		Tipped.create(etiqueta, {
	      ajax: {
	        url: "<?php echo base_url();?>Ventas/stockporbodegas",
	        type: 'post',
  			data: { 
  				equipo: id 
  			},
	        success: function (data, textStatus, jqXHR) {
	          return jqXHR.responseText;
	        },
	      },
	    });
	}
	/*
	$(document).ready(function($) {
		Tipped.create(".notificacion_tipped", {
	      ajax: {
	        url: "<?php echo base_url();?>Ventas/stockporbodegas",
	        type: 'post',
  			data: { 
  				equipo: $(this).data('equipo') 
  			},
	        success: function (data, textStatus, jqXHR) {
	          return jqXHR.responseText + ", this is a modified response";
	        },
	      },
	    });

	});
	*/
	<?php if($fm_confirmado==0){ ?>
		$(document).ready(function($) {
			confirmarfmcredito();
		});
	<?php } ?>
	<?php if($bloqueo==1){ ?>
		$(document).ready(function($) {
			var base_url = $('#base_url').val();
			swal({
			  title: "Bloqueado",
			  text: "Cliente bloqueado, contacte a administración.",
			  type: "warning",
			  showCancelButton: false,
			  confirmButtonColor: "#DD6B55",
			  confirmButtonText: "OK",
			  closeOnConfirm: false
			},
			function(){
			  location.href =base_url+'index.php/Clientes';

			});
		});
	<?php } ?>
	<?php
		if(isset($_GET['idventaadd'])){

		}
	?>
	$(document).ready(function($) {
		<?php 
			if($idpersonal==1 or $idpersonal==63){ 
		?>
		//$('.div_tonerp').html('<input type="number" id="tonerp" name="tonerp"  min="0" class="form-control-bmz"  style="text-align: center;" onclick="funcionConsumiblespreciosv($(this))" value="0">');
		$('.precionew').prop('readonly',false);
		$('.cprecio0').hide();
		$('.cprecio1_label').show();
		$('.cprecio1').removeClass('s5').addClass('s12');
		$('#precio_acce').prop('readonly',false);
		<?php } ?>
	});
	function preciorefaccion(){
		<?php 
			if($idpersonal==1 or $idpersonal==63){ 
		?>
		$('.refaccionesp_mostrar').html('<input type="number" id="refaccionesp" name="refaccionesp"  min="0" class="form-control-bmz refaccionesp"  style="text-align: center;" value="0">');
		<?php } ?>
	}
	function precionewreadonly(){
		<?php 
			if($idpersonal==1 or $idpersonal==63){ 
		?>
		$('.precionew').prop('readonly',false);
		<?php } ?>
		
	}
</script>

<?php if(isset($_GET['idventaadd'])){ 
		$idventaadd=$_GET['idventaadd'];
			$resultadovn = $this->ModeloCatalogos->db6_getselectwheren('ventas',array('id'=>$idventaadd));
			foreach ($resultadovn->result() as $item) {
				$tipov=$item->tipov;
			}


		if($_GET['idventatipo']==1){
			$resultadov = $this->ModeloCatalogos->db6_getselectwheren('ventacombinada',array('combinadaId'=>$idventaadd));
			foreach ($resultadov->result() as $item) {
				if($item->equipo>0){
                    $idventaadd =$item->equipo;
                }else if($item->consumibles>0){
                    $idventaadd =$item->consumibles;
                }else if($item->refacciones>0){
                    $idventaadd =$item->refacciones;
                }
                $tipov=$item->tipov;
			}	
			
		}
?>
	<script type="text/javascript">
		$(document).ready(function($) {

			$('#idventaaddnew').val(<?php echo $idventaadd;?>);	
			$('#tipov').val(<?php echo $tipov;?>);
			$('#atencion').val('x');
			$('#correo').val('x');
			$('#telefono').val('x');
			$('#opcionesEquipos #tipo').val(1);
			validarpermisosbodega()
		});
	</script>
	<style type="text/css">
		<?php if($_GET['idventatipo']==1){ ?>
		.paraagregarnuevosp{
			display: none;
		}
	<?php } ?>
	.paraagregarnuevos{
			display: none;
		}
	</style>
<?php } ?>
<script type="text/javascript">
	$(document).ready(function($) {
	<?php 
		if($datoscontacto->num_rows()==1){
			$d_con=$datoscontacto->row();
			if( $d_con->telefono!=''){
				$telefono =$d_con->telefono;
			}else{
				$telefono =$d_con->celular;
			}
			?>
				
				$('#atencion').val('<?php echo $d_con->atencionpara;?>');
				$('#correo').val('<?php echo $d_con->email;?>');
				$('#telefono').val('<?php echo $telefono;?>');
					
			<?php
		}else{
			?>
			obtenercontactos();
			<?php
		}
	?>
	});
</script>