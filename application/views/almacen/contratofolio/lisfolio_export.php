<?php 
	
	header("Pragma: public");
	header("Expires: 0");
	$filename = "Folios.xls";
	header("Content-type: application/x-msdownload");
	header("Content-Disposition: attachment; filename=$filename");
	header("Pragma: no-cache");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	
?>
<table id="tabla_con" class="responsive-table display" cellspacing="0">
  <thead>
    <tr>
      <th>#</th>
      <th>Modelo</th>
      <th>Parte</th>
      <th>Vendedor</th>
      <th>Fecha</th>
      <th>Cliente</th>
      <th>Folio</th>
      <th>Estatus</th>
      <th>Porcentaje<br>Retorno</th>
      <th>Fecha Retorno</th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
  	<?php  foreach ($listf->result() as $item) { ?>
  		<tr>
	      <td><?php echo $item->idcontratofolio;?></td>
	      <td><?php echo $item->modelo;?></td>
	      <td><?php echo $item->parte;?></td>
	      <td><?php echo $item->nombre;?></td>
	      <td><?php echo $item->reg;?></td>
	      <td><?php echo $item->empresa;?></td>
	      <td><?php echo $item->foliotext;?></td>
	      <td><?php if($item->status==1){ echo 'Stock Cliente';}else{ echo 'Retorno';}?></td>
	      <td><?php if($item->status==2){ echo $item->porcentajeretorno.'%';}?></td>
	      <td><?php echo $item->fecharetorno;?></td>
	      <td><?php echo $item->comentario;?></td>
	      <td><?php if($item->idVentas>0){
	      						echo 'PROINVEN: '.$item->idVentas;
	      }else{ 
	      	if($item->preren>0){ 
	      		echo 'PROINREN: '.$item->proinren;
	      	}else{
	      		echo 'RENTA: '.$item->idrenta;
	      	}
	      };?></td>
	    </tr>
  	<?php }?>
  </tbody>
</table>