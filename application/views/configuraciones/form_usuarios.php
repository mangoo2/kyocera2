<!-- START CONTENT --> 
        <section id="content" width="100%">
          

          <!--start container-->
          <div class="container" width="100%">
            <div class="section">
               <h4 class="caption">Formulario de registro de Configuraciones.</h4>
              <div class="row">
                <input id="base_url" type="hidden" name="base_url" val="<?php echo base_url();?>">
                <!-- FORMULARIO -->
               

                  <div class="col s12 m12 24">

                      <div id="basic-form" class="section">
                        <div class="row">
                          <div class="col s12 m12 l6">
                            <div class="card-panel">
                              <h4 class="header2">Categoria de equipos</h4>
                              <div class="row">
                                <form class="form" id="categorias_equipos_form" method="post">
                                          
                                  <div class="row">
                                    <div class="input-field col s10">
                                      <i class="material-icons prefix">desktop_windows</i>
                                      <input id="nombre1" name="nombre" type="text" >
                                      <label for="nombre">Nombre</label>
                                    </div>
                                    <div class="input-field col s2">
                                     <button type="submit" class="btn-floating waves-effect waves-light cyan"><i class="material-icons">add</i></button>
                                      
                                     </a>
                                    </div>
                                  </div>

                                  <div class="row">
                                    <div class="input-field col s12" style="overflow-y: scroll; height: 250px;">
                  
                                     <table id="tabla_equipo">
                                      <thead>
                                        <tr>
                                            <th> </th>
                                            <th>Nombre</th>
                                            <th>Acciones</th>
                                            <th> </th>
                                        </tr>
                                      </thead>
                                      <tbody >
                                        <?php
                                        foreach ($equipo as $e) {
                                            echo " <tr>
                                            <td></td>
                                            <td style='display:none'>$e->id</td>
                                            <td>$e->nombre</td>
                                            <td><a class='btn btn-floating waves-effect waves-light red eliminar'><i class='material-icons'>delete_forever</i></a></td>
                                            </tr>";
                                        }
                                        ?>   
                                      </tbody>
                                    </table>
                                    </div>
                                  </div>
                                    
                                </form>
                              </div>
                            </div>
                          </div>
                          <!-- Form with placeholder -->
                          <div class="col s12 m12 l6">
                            <div class="card-panel">
                              <h4 class="header2">Categoria de refacciones</h4>
                              <div class="row">
                                <form class="form" id="categorias_refacciones_form" method="post">

                                  <div class="row">
                                    <div class="input-field col s10">
                                      <i class="material-icons prefix">build</i>
                                      <input id="nombre" name="nombre" type="text" >
                                      <label for="nombre">Nombre</label>
                                    </div>
                                    <div class="input-field col s2">
                                      <button type="submit" class="btn-floating waves-effect waves-light cyan"><i class="material-icons">add</i></button>
                                    </div>
                                  </div>

                                  <div class="row">

                                    <div class="input-field col s12" style="overflow-y: scroll; height: 250px;">
                  
                                     <table id="tabla_refacciones">
                                      <thead>
                                        <tr>
                                            <th> </th>
                                            <th>Nombre</th>
                                            <th>Acciones</th>
                                            <th> </th>
                                        </tr>
                                      </thead>

                                      <tbody >
                                        <?php
                                        foreach ($refaccion as $r) {
                                            echo " <tr>
                                            <td></td>
                                            <td style='display:none'>$r->id</td>
                                            <td>$r->nombre</td>
                                            <td><a class='btn btn-floating waves-effect waves-light red eliminar'><i class='material-icons'>delete_forever</i></a></td>
                                            </tr>";
                                        }
                                        ?> 
                                      </tbody>
                                    </table>
                                    </div>
                                  </div>

                                </form>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>  

                    <div class="row">
                      <div class="col s12 m12 l12">
                         <h4 class="header2">Catalogos de accesorios</h4>
                        <div class="card-panel">
                          <div class="row">
                            <form class="form" id="catalogos_accesorios_form" method="post">
        
                              <div class="row">
                                <div class="input-field col s3">
                                  <i class="material-icons prefix">desktop_windows</i>
                                  <input id="equipo" name="equipo"  type="text">
                                  <label for="equipo">Equipo</label>
                                </div>

                                <div class="input-field col s3">
                                  <i class="material-icons prefix">build</i>
                                  <input id="nombre" name="nombre" type="text">
                                  <label for="nombre">Nombre</label>
                                </div>

                                <div class="input-field col s3">
                                  <i class="material-icons prefix">format_list_numbered</i>
                                  <input id="no_parte" name="no_parte" type="number">
                                  <label for="no_parte">No. parte</label>
                                </div>

                                <div class="input-field col s2">
                                  <i class="material-icons prefix">attach_money</i>
                                  <input id="costo" name="costo" type="number" step="any">
                                  <label for="costo">Costo</label>
                                </div>

                                <div class="input-field col s1">
                                  <button type="submit" class="btn-floating waves-effect waves-light cyan"><i class="material-icons">add</i></button>
                                </div>
                              </div>

                              <div class="row">

                                <div class="input-field col s12" style="overflow-y: scroll; height: 250px;">
              
                                 <table id="tabla_accesorios">
                                  <thead>
                                    <tr>
                                        <th> </th>
                                        <th>Equipo</th>
                                        <th>Nombre</th>
                                        <th>No parte</th>
                                        <th>Costo</th>
                                        <th>Acciones</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                        <?php
                                        foreach ($accesorio as $a) {
                                            echo " <tr>
                                            <td></td>
                                            <td style='display:none'>$a->id</td>
                                            <td>$a->equipo</td>
                                            <td>$a->nombre</td>
                                            <td>$a->no_parte</td>
                                            <td>$a->costo</td>
                                            <td><a class='btn btn-floating waves-effect waves-light red eliminar'><i class='material-icons'>delete_forever</i></a></td>
                                            </tr>";
                                        }
                                        ?> 
                                  </tbody>
                                </table>
                                </div>
                              </div>

                            </form>
                          </div>
                        </div>
                      </div>
                    </div>

                  </div>
                
              </div>
            </div>
          </div>
        </section>