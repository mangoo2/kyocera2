<?php
if(isset($cliente))
{
$idCliente = $cliente->id;
$empresa = $cliente->empresa;
}
?>
<style type="text/css" class="print_css">
  
</style>
<!-- START CONTENT -->
<section id="content" width="100%">
  <!--start container-->
  <div class="container" width="100%">
    <div class="section" style="margin-top: -53px;">
     <br>
      <div class="row">
        <input id="base_url" type="hidden" name="base_url" value="<?php echo base_url(); ?>">
        <!-- FORMULARIO  card-panel-->
        <div class="col s12">
          <div class="">

            <!-- ENCABEZADO PRINCIPAL PDF -->
            <div class="row"> 
              <div class="col s3">
                <img src="<?php echo base_url(); ?>app-assets/images/alta.png">
              </div>
              <div class="col s6">
                <p class="headerPDF"><b>Impresoras y Multifuncionales, Venta, Renta y Mantenimiento.<br>Blvd. Norte 1831 V. San Alejandro. Puebla, Pue. Call Center 273 3400, 249 5393<br>www.kyoceraapp.com</b></p>
              </div> 
              <div class="col s3">
                <p align="center"><img class="" src="<?php echo base_url(); ?>app-assets/images/1024_kyocera_logo_mds.png" alt="materialize logo" width="200px;" height="50px;"></p>
              </div>
            </div>

            <!-- APARTADO DE INFORMACIÓN DE RECEPTOR DE COTIZACIÓN-->
            
            
            <div class="row">
              <table class="centered datosClienteCotizacion tablaBordesHeader">
                  <thead>
                    
                  </thead>
                  <tbody class="tablaBordes">
                    <tr>
                        <td class="tablaBordes">
                          Cotización Especial Para: <br>
                          Mangoo Software
                        </td>
                        <td class="tablaBordes">
                          Atención para: <br>
                          Raúl Trujeque Martínez
                        </td>
                        <td class="tablaBordes">
                          Correo raul@mangoo.com.mx <br>
                          Teléfono: 2224578961
                        </td>
                        <td class="tablaBordes">
                          Cotización No. <?php echo rand ( 1000, 5000 );?> 
                        </td>
                    </tr>
                  </tbody>
                </table>
            </div>

            <!------------ TABLA DE INFORMACIÓN DE EQUIPOS ------------>
            <div class="row">
                <table class="centered bordered letraTablaImpresion tablaBordesPrincipal">
                  <thead>
                    <tr class="">
                        <th style="font-size:10px;" class="tablaBordesPrincipal"><b>Multifuncional Laser Monocromático </b></th>
                        <th class="tablaBordesPrincipal">Equipo 1</th>
                        <th class="tablaBordesPrincipal">Equipo 2</th>
                        <th class="tablaBordesPrincipal">Equipo 3</th>
                    </tr>
                  </thead>
                  <tbody >
                    <tr>
                      <td class="tablaBordesPrincipal">
                        <img src="<?php echo base_url(); ?>app-assets/images/kyoventa.png" class="onMouseOverDisabled">
                      </td>
                      <td class="tablaBordesPrincipal" >
                        <img src="<?php echo base_url(); ?>uploads/equipos/r_-19100.png" class="onMouseOverDisabled">
                      </td>
                      <td class="tablaBordesPrincipal">
                        <img src="<?php echo base_url(); ?>uploads/equipos/r_-19100.png" class="onMouseOverDisabled">
                      </td>
                      <td class="tablaBordesPrincipal">
                        <img src="<?php echo base_url(); ?>uploads/equipos/r_-19100.png" class="onMouseOverDisabled">
                      </td>
                    </tr>
                    <tr class="letraTablaFooter">
                      <td class="tablaBordesPrincipal"><b>Precios: </b></td>
                      <td class="tablaBordesPrincipal"><b>$36,417.00</b></td>
                      <td class="tablaBordesPrincipal"><b>$58,459.00</b></td>
                      <td class="tablaBordesPrincipal"><b>$58,690.00</b></td>
                    </tr>
                    <tr>
                      <td class="tablaBordesPrincipal"></td>
                      <td class="tablaBordesPrincipal"colspan="3">Costos de Impresión</td>
                    </tr>
                    <tr>
                      <td class="tablaBordesPrincipal" style="font-size:8px;">Costo Toner Black</td>
                      <td class="tablaBordesPrincipal">$1,037.00</td>
                      <td class="tablaBordesPrincipal">$1,378.00</td>
                      <td class="tablaBordesPrincipal">$1,1670.00</td>
                    </tr>
                    <tr>
                      <td class="tablaBordesPrincipal">Rendimiento Toner Black</td>
                      <td class="tablaBordesPrincipal">12,000</td>
                      <td class="tablaBordesPrincipal">20,000</td>
                      <td class="tablaBordesPrincipal">25,000</td>
                    </tr>
                    <tr>
                      <td class="tablaBordesPrincipal">Costo Toner CMY</td>
                      <td class="tablaBordesPrincipal">$3,996.00</td>
                      <td class="tablaBordesPrincipal">$3,996.00</td>
                      <td class="tablaBordesPrincipal">$4,842.00</td>
                    </tr>
                    <tr>
                      <td class="tablaBordesPrincipal">Rendimiento Toner CMY</td>
                      <td class="tablaBordesPrincipal">6,000</td>
                      <td class="tablaBordesPrincipal">12,000</td>
                      <td class="tablaBordesPrincipal">15,000</td>
                    </tr>
                    <tr>
                      <td class="tablaBordesPrincipal">Costo Unidad Imagen</td>
                      <td class="tablaBordesPrincipal" colspan="3">Inicial sin cargo</td>
                    </tr>
                    <tr>
                      <td class="tablaBordesPrincipal">Rendimiento Unidad Imagen</td>
                      <td class="tablaBordesPrincipal">200,000</td>
                      <td class="tablaBordesPrincipal">200,000</td>
                      <td class="tablaBordesPrincipal">200,000</td>
                    </tr>
                    <tr>
                      <td class="tablaBordesPrincipal">Costo Garantia y Servicio</td>
                      <td class="tablaBordesPrincipal" colspan="3">Inicial sin cargo</td>
                    </tr>
                    <tr>
                      <td class="tablaBordesPrincipal">Rendimiento Garantia y Servicio</td>
                      <td class="tablaBordesPrincipal" colspan="3">Inicial sin cargo</td>
                    </tr>
                    <tr>
                      <td class="tablaBordesPrincipal">Costo por Página Mono</td>
                      <td class="tablaBordesPrincipal">$0.09</td>
                      <td class="tablaBordesPrincipal">$0.07</td>
                      <td class="tablaBordesPrincipal">$0.07</td>
                    </tr>
                    <tr>
                      <td class="tablaBordesPrincipal">Costo por Página Mono</td>
                      <td class="tablaBordesPrincipal">$0.67</td>
                      <td class="tablaBordesPrincipal">$0.33</td>
                      <td class="tablaBordesPrincipal">$0.32</td>
                    </tr>
                    <tr>
                      <td class="tablaBordesPrincipal letraTablaFooter" width="19%"><b>Especificaciones</b></td>
                      <td class="tablaBordesPrincipal" width="27%" height="100px">Velocidad: 24 ppm. Reducción-Ampliación: 25-400%. Procesa hasta: Doble Carta. Bypass: p/100 hojas. Alimentador: Rev 50. Resolución: 1200 x 1200 dpi. Procesador: 1,2. Memoria: 1.5 GB. Control de Usuarios: 100 Claves. Requerimientos eléctricos: 120V, 60Hz, 12A. Dimensiones: 59 x 59 x 75. Peso: 77 Kg. Capacidad Mensual: 30k. Aplicaciones HyPAS: Impresión Móvil, Conector a Google Drive Evernote, Monitoreo y Reporteador</td>
                      <td class="tablaBordesPrincipal" width="27%" height="100px">Velocidad: 24 ppm. Reducción-Ampliación: 25-400%. Procesa hasta: Doble Carta. Bypass: p/100 hojas. Alimentador: Rev 50. Resolución: 1200 x 1200 dpi. Procesador: 1,2. Memoria: 1.5 GB. Control de Usuarios: 100 Claves. Requerimientos eléctricos: 120V, 60Hz, 12A. Dimensiones: 59 x 59 x 75. Peso: 77 Kg. Capacidad Mensual: 30k. Aplicaciones HyPAS: Impresión Móvil, Conector a Google Drive Evernote, Monitoreo y Reporteador</td>
                      <td class="tablaBordesPrincipal" width="27%" height="100px">Velocidad: 24 ppm. Reducción-Ampliación: 25-400%. Procesa hasta: Doble Carta. Bypass: p/100 hojas. Alimentador: Rev 50. Resolución: 1200 x 1200 dpi. Procesador: 1,2. Memoria: 1.5 GB. Control de Usuarios: 100 Claves. Requerimientos eléctricos: 120V, 60Hz, 12A. Dimensiones: 59 x 59 x 75. Peso: 77 Kg. Capacidad Mensual: 30k. Aplicaciones HyPAS: Impresión Móvil, Conector a Google Drive Evernote, Monitoreo y Reporteador</td>
                    </tr>

                  </tbody>
                </table>
            </div>

            <!-- Condiciones de Equipos -->
            <div class="row" style="margin-bottom: -12px">
              <table class="centered letraTablaFooter">
                  <thead>
                      <tr>
                        <th class="tablaBordesPrincipal">Condiciones</th>
                        <th class="tablaBordesPrincipal">Certificaciones</th>
                        <th class="tablaBordesPrincipal"></th>
                      </tr>
                  </thead>
                  <tbody >
                    <tr>
                        <td class="tablaBordesPrincipal" width="33%"><b>Precios: </b>Sujetos a I.V.A. y a cambios.  <b>Garantía: </b>Por 12 meses contra defectos de fabricación. <b>Incluye: </b>Unidad de imagen iniciales.  <b>Opción: </b>Aumento de memoria. <b>Tiempo de entrega: </b>Equipo en existencia.  </b></td>
                        <td class="tablaBordesPrincipal" width="34%">Calidad ISO9001, responsabilidad ambiental ISO14001, rendimiento de suministros ISO19752, seguridad comprobada TUV-UE, eficiente consumo Energy Star y equipo nuevo NOM-019</td>
                        <td class="tablaBordesPrincipal" width="33%"><b>Enero de 2018</b><br><b><b>Guillermina Alvarez. </b>guille.a@kyoceraap.com</td>
                    </tr>
                  </tbody>
                </table>
            </div>

           

<!--columna 1---------------------------------------------------------------->

              <form class="form" id="" method="post">
                <?php
                if(isset($cliente))
                {
                ?>
                <input id="idCliente" name="idCliente" type="hidden" value="<?php echo $idCliente; ?>">
                <?php
                }
                ?>
                <div class="row">
                  <div class="input-field col s12">
                    <button class="btn cyan waves-effect waves-light right btn_imp" type="button" onclick="btn_guardar()">Imprimir
                    <i class="material-icons right">send</i>
                    </button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>