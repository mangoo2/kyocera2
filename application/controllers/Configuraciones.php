<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Configuraciones extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('Configuraciones_model', 'model');
        $this->load->model('login_model');
        $this->load->model('Polizas_model');
        $this->load->model('ModeloEvenServ');
        $this->load->helper('url');
        $this->load->model('ModeloCatalogos');
        $this->submenu=15;
        if($this->session->userdata('logeado')==true){
            $this->perfilid =$this->session->userdata('perfilid');
            $this->idpersonal = $this->session->userdata('idpersonal');
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,$this->submenu);// 15 es el id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
        }else{
            redirect('login');
        }
    }

    // Listado de configuraciones
    function index(){
        $data['MenusubId']=$this->submenu;
        // Obtiene los menús disponibles del sistema dependiendo el perfil, ayudado de la libreria "MENUS"
        $this->load->view('header');
        $this->load->view('main');
        $equipo = $this->model->getDataEquipo();
        $refaccion = $this->model->getDataRefacciones();
        //$accesorio = $this->model->getDataAccesorios();
        $da['equipo'] = $equipo;
        $da['refaccion'] = $refaccion;
        //$da['accesorio'] = $accesorio;
        $this->load->view('configuraciones/form_configuraciones',$da);
        $this->load->view('configuraciones/form_configuraciones_js');
        $this->load->view('footer');
    }

    public function insertarCategoriaEquipos(){
        $result = 0; 
        $data = $this->input->post();

           $result = $this->model->insertarCategoriaEquipos($data);
        echo $result;  
        //$this->Modelobitacoras->Insert(array('contenido'=>'Se Inserto la categoria de equipos: '.$data["nombre"],'nombretabla'=>'categoria_equipos','idtable'=>$result,'tipo'=>'Insert','personalId'=>$this->idpersonal));
    }

    public function insertarCategoriaRefacciones(){
        $result = 0; 
        $data = $this->input->post();
               $datosRefacciones = array(
                            'nombre' => $data["nombre"],
                            );
           $result = $this->model->insertarCategoriaRefacciones($datosRefacciones);
        echo $result;  
        //$this->Modelobitacoras->Insert(array('contenido'=>'Se Inserto la categoria de refacciones: '.$data["nombre"],'nombretabla'=>'categoria_refacciones','idtable'=>$result,'tipo'=>'Insert','personalId'=>$this->idpersonal));
    }

    public function insertarCatalogoAccesorios(){
        $result = 0; 
        
        $data = $this->input->post();
        $idaccesorio=$data["id"];
       // var_dump($data);die;
               $datosAccesorios = array(
                            //'equipo' => $data["equipo"],
                            'nombre' => $data["nombre"],
                            'no_parte' => $data["no_parte"],
                            'stock' => $data["stock"],
                            'costo' => $data["costo"],
                            'observaciones' => $data["observaciones"]
                            );
        $datos = $this->model->verificaraccesorio($data['no_parte']);
        $id=0;
        foreach ($datos as $item) {
           $id = $item->id;
        }
        if ($idaccesorio>0) {
            $id=0;
        }
        if($id>=1){
           $result = 1;           
        }else{
            if ($idaccesorio==0) {
                $id=$this->ModeloCatalogos->Insert('catalogo_accesorios',$datosAccesorios);
                //$this->Modelobitacoras->Insert(array('contenido'=>'Se Inserto accesorio: '.$data["nombre"],'nombretabla'=>'categoria_refacciones','idtable'=>$id,'tipo'=>'Insert','personalId'=>$this->idpersonal));
                $this->ModeloCatalogos->updateuuid($id,4);
            }else{
                $this->ModeloCatalogos->updateCatalogo('catalogo_accesorios',$datosAccesorios,array('id'=>$idaccesorio));
                //$this->Modelobitacoras->Insert(array('contenido'=>'Se actualizo accesorio: '.$data["nombre"],'nombretabla'=>'categoria_refacciones','idtable'=>$idaccesorio,'tipo'=>'Insert','personalId'=>$this->idpersonal));
            }
           
           $result = 2;
        }   
        echo $result;  
    }  
    public function eliminar_equipo(){
        $id = $this->input->post('id');
        $result = $this->model->eliminar_equipo($id);

        echo $result; 
         //$this->Modelobitacoras->Insert(array('contenido'=>'Se Inserto elimino la categoria de equipos: ','nombretabla'=>'categoria_equipos','idtable'=>$id,'tipo'=>'delete','personalId'=>$this->idpersonal));
    }  
    public function eliminar_refaccion(){
        $id = $this->input->post('id');
        $result = $this->model->eliminar_refaccion($id);
        echo $result;  
        //$this->Modelobitacoras->Insert(array('contenido'=>'Se Inserto elimino la categoria de refaccion: ','nombretabla'=>'categoria_refaccion','idtable'=>$id,'tipo'=>'delete','personalId'=>$this->idpersonal));
    }  
    public function eliminar_accesorio(){
        $id = $this->input->post('id');
        $result = $this->model->eliminar_accesorio($id);
        echo $result;  
        //$this->Modelobitacoras->Insert(array('contenido'=>'Se Inserto elimino la categoria de accesorios: ','nombretabla'=>'categoria_accesorios','idtable'=>$id,'tipo'=>'delete','personalId'=>$this->idpersonal));
    }
    public function verificaraccesorio(){
        $data = $this->input->post('datos');
        $datos = $this->model->verificaraccesorio($data);
        foreach ($datos as $item) {
           $id = $item->id;
        }
        if($id>=1){
           $result = 1;           
        }
        echo $result;
    } 
    public function ConfSerEvento(){
        $data['polizas'] = $this->Polizas_model->getListadoPolizas(); 
        $this->load->view('header');
        $this->load->view('main');
        $this->load->view('configuraciones/serv_evento/listado_serv_even',$data);
        $this->load->view('configuraciones/serv_evento/listado_serv_even_js');
        $this->load->view('footerck');
    } 

    public function insertarEvenServ(){
        $result = 0; 
        $data = $this->input->post();
        $id=$data['id'];
        unset($data['id']);
        if ($id==0) {
            if ($data['polizamodelo']=='all') {
                $resultpmd = $this->ModeloCatalogos->getselectwheren('polizas_detalles',array('idPoliza'=>$data['poliza'],'activo'=>1));
                foreach ($resultpmd->result() as $item) {
                    $data['polizamodelo']=$item->id;
                    $result = $this->ModeloCatalogos->Insert('servicio_evento',$data);
                    echo $result;  
                    //$this->Modelobitacoras->Insert(array('contenido'=>'Se Insertó un servicio/evento: '.$data["nombre"],'nombretabla'=>'servicio_evento','idtable'=>$result,'tipo'=>'Insert','personalId'=>$this->idpersonal));
                    unset($data['polizamodelo']);
                }
            }else{
                $result = $this->ModeloCatalogos->Insert('servicio_evento',$data);
                echo $result;  
                //$this->Modelobitacoras->Insert(array('contenido'=>'Se Insertó un servicio/evento: '.$data["nombre"],'nombretabla'=>'servicio_evento','idtable'=>$result,'tipo'=>'Insert','personalId'=>$this->idpersonal));
            }
        }else{
            $this->ModeloCatalogos->updateCatalogo('servicio_evento',$data,array('id'=>$id));
              
            //$this->Modelobitacoras->Insert(array('contenido'=>'Se Actualizo un servicio/evento: '.$data["nombre"],'nombretabla'=>'servicio_evento','idtable'=>$id,'tipo'=>'Insert','personalId'=>$this->idpersonal));
        }
           
    }

    public function eliminarEvenServ(){
        $id = $this->input->post('id');
        $result = $this->ModeloCatalogos->updateCatalogo('servicio_evento',array('status'=>'0'),array('id'=>$id));

        echo $result; 
         //$this->Modelobitacoras->Insert(array('contenido'=>'Se eliminó una ruta: ','nombretabla'=>'servicio_evento','idtable'=>$id,'tipo'=>'delete','personalId'=>$this->idpersonal));
    } 
    public function getListadoEvenServ() {
        $params = $this->input->post();
        $productos = $this->ModeloEvenServ->getListadoEvenServ($params);
       $totalRecords=$this->ModeloEvenServ->getListadoEvenServt($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totalRecords),  
            "recordsFiltered" => intval($totalRecords),
            "data"            => $productos->result(),
            "query"           =>$this->db->last_query()   
        );
        
        echo json_encode($json_data);
    }
    function datosEvenServ(){
        $params = $this->input->post();
        $datos =$this->ModeloCatalogos->getselectwheren('servicio_evento',array('id'=>$params['id']));
        $existe=0;
        $poliza=0;
        $id=0;
        $polizamodelo=0;
        $nombre=0;
        $descripcion=0;
        $local=0;
        $semi=0;
        $foraneo=0;
        $sinmodelo=0;
        $newmodelo='';
        $especial=0;

        foreach ($datos->result() as $item) {
            $existe=1;
            $poliza = $item->poliza;
            $polizamodelo = $item->polizamodelo;
            $nombre = $item->nombre;
            $descripcion = $item->descripcion;
            $local = $item->local;
            $semi = $item->semi;
            $foraneo = $item->foraneo;
            $id = $item->id;
            $sinmodelo=$item->sinmodelo;
            $newmodelo=$item->newmodelo;
            $especial=$item->especial;
        }
        $array = array(
                        'existe'=>$existe,
                        'id'=>$id,
                        'poliza'=>$poliza,
                        'polizamodelo'=>$polizamodelo,
                        'nombre'=>$nombre,
                        'descripcion'=>$descripcion,
                        'local'=>$local,
                        'semi'=>$semi,
                        'foraneo'=>$foraneo,
                        'sinmodelo'=>$sinmodelo,
                        'newmodelo'=>$newmodelo,
                        'especial'=>$especial,
                    );
        echo json_encode($array);
    }
}

?>