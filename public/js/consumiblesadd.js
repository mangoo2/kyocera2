var base_url = $('#base_url').val();
var trserieequipo=0;
var trserieaccesorio=0;
var trserierefaccion=0;
var tablee;
var tablea;
var tabler;
var addtr=0;
$(document).ready(function(){
	$('.chosen-select').chosen({width: "100%"});
	
	
	$('.addbtnconsumible').click(function(event) {
		
		var bodega=$('#bodega option:selected').val();
		var bodegat=$('#bodega option:selected').text();
		var consumible=$('#consumible option:selected').val();
		var consumiblet=$('#consumible option:selected').text();
		var cantidad=$('#cantidad4').val();
		if (cantidad!='') {
				//==========================================
				var addtre='<tr class="addtr_'+addtr+'">\
			                	<td>\
			                		<input id="bodegasadd" type="hidden"  value="'+bodega+'" readonly>\
				                	'+bodegat+'\
				                </td>\
					            <td>\
				                		<input id="consumible" type="hidden"  value="'+consumible+'" readonly>\
					                	'+consumiblet+'\
					                </td>\
				                <td>\
				                	<input id="cantidad" type="text" class="datostableinput2 seriev" value="'+cantidad+'" readonly>\
				                </td>\
				                <td><a class="btn-floating waves-effect waves-light red accent-2" onclick="deteletr('+addtr+')"><i class="material-icons">clear</i></a></td>\
			              </tr>';
			    $('.tabla_addsconsumible').append(addtre);
			    addtr++;
			    //====================================
			    
		    
			
		}else{
			alertfunction('Atención!','La cantidad no debe de estar vacio');
		}
	});
	$('.addbtnregaccion').click(function(event) {
		
		var bodega=$('#bodegar option:selected').val();
		var bodegat=$('#bodegar option:selected').text();
		var refacciones=$('#refacciones option:selected').val();
		var refaccionest=$('#refacciones option:selected').text();
		var cantidad=$('#cantidadr').val();
		if (cantidad!='') {
				//==========================================
				var addtre='<tr class="addtr_'+addtr+'">\
			                	<td>\
			                		<input id="bodegasadd" type="hidden"  value="'+bodega+'" readonly>\
				                	'+bodegat+'\
				                </td>\
					            <td>\
				                		<input id="refacciones" type="hidden"  value="'+refacciones+'" readonly>\
					                	'+refaccionest+'\
					                </td>\
				                <td>\
				                	<input id="cantidad" type="text" class="datostableinput2 seriev" value="'+cantidad+'" readonly>\
				                </td>\
				                <td><a class="btn-floating waves-effect waves-light red accent-2" onclick="deteletr('+addtr+')"><i class="material-icons">clear</i></a></td>\
			              </tr>';
			    $('.tabla_addsrefaccion').append(addtre);
			    addtr++;
			    //====================================
			    
		    
			
		}else{
			alertfunction('Atención!','La cantidad no debe de estar vacio');
		}
	});
	$('.addbtnaccesorio').click(function(event) {
		
		var bodega=$('#bodegaa option:selected').val();
		var bodegat=$('#bodegaa option:selected').text();
		var accesorios=$('#accesorios option:selected').val();
		var accesoriost=$('#accesorios option:selected').text();
		var cantidad=$('#cantidada').val();
		if (cantidad!='') {
				//==========================================
				var addtre='<tr class="addtr_'+addtr+'">\
			                	<td>\
			                		<input id="bodegasadd" type="hidden"  value="'+bodega+'" readonly>\
				                	'+bodegat+'\
				                </td>\
					            <td>\
				                		<input id="accesorios" type="hidden"  value="'+accesorios+'" readonly>\
					                	'+accesoriost+'\
					                </td>\
				                <td>\
				                	<input id="cantidad" type="text" class="datostableinput2 seriev" value="'+cantidad+'" readonly>\
				                </td>\
				                <td><a class="btn-floating waves-effect waves-light red accent-2" onclick="deteletr('+addtr+')"><i class="material-icons">clear</i></a></td>\
			              </tr>';
			    $('.tabla_addsaccesorio').append(addtre);
			    addtr++;
			    //====================================
			    
		    
			
		}else{
			alertfunction('Atención!','La cantidad no debe de estar vacio');
		}
	});



	$('.saveallseries').click(function(event) {
			

	        var DATAc  = [];
	        var TABLA   = $("#tabla_sconsumible tbody > tr");
	            TABLA.each(function(){         
	            item = {};
	            item ["bodega"]   = $(this).find("input[id*='bodegasadd']").val();
	            item ["consumible"]   = $(this).find("input[id*='consumible']").val();
	            item ["cantidad"]  = $(this).find("input[id*='cantidad']").val();
	            DATAc.push(item);
	        });
	        INFO  = new FormData();
	        arrayconsumibles  = JSON.stringify(DATAc);

	        var DATAr  = [];
	        var TABLAr   = $("#tabla_srefaccion tbody > tr");
	            TABLAr.each(function(){         
	            itemr = {};
	            itemr ["bodega"]   = $(this).find("input[id*='bodegasadd']").val();
	            itemr ["refacciones"]   = $(this).find("input[id*='refacciones']").val();
	            itemr ["cantidad"]  = $(this).find("input[id*='cantidad']").val();
	            DATAr.push(itemr);
	        });
	        INFO  = new FormData();
	        arrayrefacciones  = JSON.stringify(DATAr);


	        var DATAac  = [];
	        var TABLAac   = $("#tabla_saccesorio tbody > tr");
	            TABLAac.each(function(){         
	            itemac = {};
	            itemac ["bodega"]   = $(this).find("input[id*='bodegasadd']").val();
	            itemac ["accesorios"]   = $(this).find("input[id*='accesorios']").val();
	            itemac ["cantidad"]  = $(this).find("input[id*='cantidad']").val();
	            DATAac.push(itemac);
	        });
	        INFO  = new FormData();
	        arrayaccesorios  = JSON.stringify(DATAac);



		    var datos = 'arrayconsumibles='+arrayconsumibles+'&arrayrefacciones='+arrayrefacciones+'&arrayaccesorios='+arrayaccesorios;
		    $.ajax({
		        type:'POST',
		        url: base_url+"index.php/Aconsumibles/addconsumibles",
		        data: datos,
		        success: function (response){
		            idventatabler=response;
	                alertfunction('Éxito!','Se ha creado al stock: '+response);
	                setTimeout(function(){ 
	                    window.location.href = base_url+"index.php/Aconsumibles";
	                }, 2000);
		        },
		        error: function(response){
		            alertfunction('Atención!','Algo salió mal, intente de nuevo o contacte al administrador del sistema'); 
		        }
		    });	
	});
});
function alertfunction(titlea,contenta){
    $.alert({boxWidth: '30%',useBootstrap: false,title: titlea,theme: 'bootstrap',content: contenta});  
}
function deteletr(id){
	$('.addtr_'+id).remove();
}