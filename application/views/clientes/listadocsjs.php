<script type="text/javascript">
	$(document).ready(function($) {
		$('#tabla_clientes').DataTable({
            pagingType: 'input',
            dom: 'Blfrtip',
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'pdfHtml5',
            ]
        });
        <?php foreach ($result_vf->result() as $itemvf) { ?>
            obtenerventaspendientes_sinnot(<?php echo $itemvf->id;?>);
        <?php } ?>
		
	});
    function permitir(id){
        var per=$('#permitir_'+id).is(':checked')==true?1:0;
        var perv=$('#permitirv_'+id).is(':checked')==true?1:0;
            $.ajax({
                type:'POST',
                url: base_url+"index.php/Clientes_suspendidos/permitir",
                data: {
                    cli:id,
                    per:per,
                    perv:perv
                },
                success: function (response){                    
                },
                error: function(response){
                    toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema"); 
                     
                }
            }); 
        
    }
    
</script>