<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloGarantia extends CI_Model {
    public function __construct() {
        parent::__construct();
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d');
        $this->fechahoyl = date('Y-m-d H:i:s');
    }

    function get_equipos($params){
        $columns = array( 
            0=>'idVenta',
            1=>'empresa',
            2=>'serieId',
            3=>'productoid',
            4=>'serie',
            5=>'modelo',
            6=>'limitgarantia',
            7=>'tipop',
        );

        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $subconsulta=$this->sql_query_equipos($params);

        $this->db->from($subconsulta);

        //$this->db->where(array('facturaabierta'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    public function get_equipos_total($params){
        $columns = array( 
            0=>'idVenta',
            1=>'empresa',
            2=>'serieId',
            3=>'productoid',
            4=>'serie',
            5=>'modelo',
            6=>'limitgarantia',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('*');
        $subconsulta=$this->sql_query_equipos($params);
    
        $this->db->from($subconsulta);

        //$this->db->where(array('facturaabierta'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        //$this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        //$this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query->num_rows();
    }
    function sql_query_equipos($params){
        $tipog = $params['tipog'];
        $estatus = $params['estatus'];//0 todos, 1 sin proceso 2 en proeceso, 3 finalizado
        if($estatus==1){
            $estatus_where=' and gar.id IS NULL';
        }elseif($estatus==2){
            $estatus_where=" and gar.id>0 and gar.notacredito='' ";
        }elseif($estatus==3){
            $estatus_where=" and gar.id>0 and gar.notacredito!='' ";
        }else{
            $estatus_where='';
        }
        $strq='(';
        if($tipog==1){
            /*
            $strq.="(SELECT 
                        vde.idVenta,
                        cli.empresa,
                        1 as tipop,
                        sp.serieId,
                        sp.productoid,
                        sp.serie,
                        vde.modelo,
                        sp.reg,
                        date_add(sp.reg, interval 3 year) as limitgarantia,
                        DATE(now()) 
                        FROM ventas_has_detallesEquipos as vde 
                    INNER JOIN ventas as vn on vn.id=vde.idVenta
                    INNER JOIN clientes as cli on cli.id=vn.idCliente
                    INNER JOIN asignacion_series_equipos as ase on ase.id_equipo=vde.id
                    INNER JOIN series_productos as sp on sp.serieId=ase.serieId
                    WHERE vn.activo=1 and vn.combinada=0 AND vde.precio=0 AND DATE(now())<date_add(sp.reg, interval 3 year))
                    UNION 
                    (SELECT 
                        vde.idVenta,
                        cli.empresa,
                        1 as tipop,
                        sp.serieId,
                        sp.productoid,
                        sp.serie,
                        vde.modelo,
                        sp.reg,
                        date_add(sp.reg, interval 3 year) as limitgarantia,
                        DATE(now()) 
                        FROM ventas_has_detallesEquipos as vde 
                    INNER JOIN ventas as vn on vn.id=vde.idVenta
                    INNER JOIN clientes as cli on cli.id=vn.idCliente
                    INNER JOIN ventacombinada as vnc on vnc.equipo=vn.id
                    INNER JOIN asignacion_series_equipos as ase on ase.id_equipo=vde.id
                    INNER JOIN series_productos as sp on sp.serieId=ase.serieId
                    WHERE vn.activo=1 AND vnc.activo=1 and vn.combinada=1 AND vde.precio=0 AND DATE(now())<date_add(sp.reg, interval 3 year))";
                */
            $strq.="(SELECT 
                        vde.idVenta,
                        cli.empresa,
                        1 as tipop,
                        sp.serieId,
                        sp.productoid,
                        sp.serie,
                        vde.modelo,
                        sp.reg,
                        date_add(sp.reg, interval 3 year) as limitgarantia,
                        DATE(now()) 
                        FROM ventas_has_detallesEquipos as vde 
                    INNER JOIN ventas as vn on vn.id=vde.idVenta
                    INNER JOIN clientes as cli on cli.id=vn.idCliente
                    INNER JOIN asignacion_series_equipos as ase on ase.id_equipo=vde.id
                    INNER JOIN series_productos as sp on sp.serieId=ase.serieId
                    left JOIN garantias as gar on gar.idventa=vde.idVenta and gar.serieid=sp.serieId and gar.modeloid=sp.productoid
                    WHERE vn.activo=1 and vn.combinada=0 AND vde.garantia=1 $estatus_where
                    )
                    UNION 
                    (SELECT 
                        vde.idVenta,
                        cli.empresa,
                        1 as tipop,
                        sp.serieId,
                        sp.productoid,
                        sp.serie,
                        vde.modelo,
                        sp.reg,
                        date_add(sp.reg, interval 3 year) as limitgarantia,
                        DATE(now()) 
                        FROM ventas_has_detallesEquipos as vde 
                    INNER JOIN ventas as vn on vn.id=vde.idVenta
                    INNER JOIN clientes as cli on cli.id=vn.idCliente
                    INNER JOIN ventacombinada as vnc on vnc.equipo=vn.id
                    INNER JOIN asignacion_series_equipos as ase on ase.id_equipo=vde.id
                    INNER JOIN series_productos as sp on sp.serieId=ase.serieId
                    left JOIN garantias as gar on gar.idventa=vde.idVenta and gar.serieid=sp.serieId and gar.modeloid=sp.productoid
                    WHERE vn.activo=1 AND vnc.activo=1 and vn.combinada=1 AND vde.garantia=1 $estatus_where
                )";
        }
        if($tipog==2){
            $strq.="(SELECT 
                        vdr.idVentas as idVenta, 
                        cli.empresa,
                        2 as tipop,
                        asr.serieId,
                        asr.id_refaccion as productoid, 
                        sr.serie,
                        vdr.modelo,
                        sr.reg,
                        date_add(sr.reg, interval 3 year) as limitgarantia,
                        DATE(now()) 
                        FROM ventas_has_detallesRefacciones as vdr 
                        INNER JOIN ventas as vn on vn.id=vdr.idVentas 
                        INNER JOIN clientes as cli on cli.id=vn.idCliente 
                        INNER JOIN asignacion_series_refacciones as asr on asr.id_refaccion=vdr.id 
                        INNER JOIN series_refacciones as sr on sr.serieId=asr.serieId 
                        left JOIN garantias as gar on gar.idventa=vdr.idVentas and gar.serieid=asr.serieId and gar.modeloid=asr.id_refaccion
                        WHERE vn.activo=1 AND vn.combinada=0 AND vdr.garantia=1 $estatus_where
                    )
                    UNION
                    (SELECT 
                        IF(vc.equipo>0,vc.equipo,IF(vc.consumibles>0,vc.consumibles,IF(vc.refacciones>0,vc.refacciones,0))) as idVenta, 
                        cli.empresa,
                        2 as tipop,
                        asr.serieId,
                        asr.id_refaccion as productoid, 
                        sr.serie,
                        vdr.modelo,
                        sr.reg,
                        date_add(sr.reg, interval 3 year) as limitgarantia,
                        DATE(now())
                        FROM ventas_has_detallesRefacciones as vdr
                        INNER JOIN ventas as vn on vn.id=vdr.idVentas
                        INNER JOIN clientes as cli on cli.id=vn.idCliente
                        INNER JOIN asignacion_series_refacciones as asr on asr.id_refaccion=vdr.id
                        INNER JOIN series_refacciones as sr on sr.serieId=asr.serieId
                        INNER JOIN ventacombinada as vc on vc.refacciones=vn.id
                        left JOIN garantias as gar on gar.idventa=IF(vc.equipo>0,vc.equipo,IF(vc.consumibles>0,vc.consumibles,IF(vc.refacciones>0,vc.refacciones,0))) and gar.serieid=asr.serieId and gar.modeloid=asr.id_refaccion
                        WHERE vn.activo=1 AND vn.combinada=1 AND vdr.garantia=1 $estatus_where
                    )";
        }
        if($tipog==3){
            $strq.="(SELECT 
                        vda.id_venta as idVenta,
                        cli.empresa, 
                        3 as tipop,
                        sa.serieId,
                        vda.id_accesorio as productoid,
                        sa.serie,
                        ca.nombre as modelo,
                        sa.reg,
                        date_add(sa.reg, interval 3 year) as limitgarantia, 
                        DATE(now()) 
                    FROM ventas_has_detallesEquipos_accesorios as vda 
                    INNER JOIN ventas as vn on vn.id=vda.id_venta 
                    INNER JOIN clientes as cli on cli.id=vn.idCliente 
                    INNER JOIN asignacion_series_accesorios as asa on asa.id_accesorio=vda.id_accesoriod 
                    INNER JOIN series_accesorios as sa on sa.serieId=asa.serieId 
                    INNER JOIN catalogo_accesorios as ca on ca.id=vda.id_accesorio 
                    left JOIN garantias as gar on gar.idventa=vda.id_venta and gar.serieid=sa.serieId and gar.modeloid=vda.id_accesorio
                    WHERE vn.activo=1 AND vn.combinada=0 and vda.garantia=1 $estatus_where
                    )
                    UNION
                    (SELECT 
                        vda.id_venta as idVenta,
                        cli.empresa, 
                        3 as tipop,
                        sa.serieId,
                        vda.id_accesorio as productoid,
                        sa.serie,
                        ca.nombre as modelo,
                        sa.reg,
                        date_add(sa.reg, interval 3 year) as limitgarantia, 
                        DATE(now()) 
                    FROM ventas_has_detallesEquipos_accesorios as vda 
                    INNER JOIN ventas as vn on vn.id=vda.id_venta 
                    INNER JOIN clientes as cli on cli.id=vn.idCliente 
                    INNER JOIN asignacion_series_accesorios as asa on asa.id_accesorio=vda.id_accesoriod 
                    INNER JOIN series_accesorios as sa on sa.serieId=asa.serieId 
                    INNER JOIN catalogo_accesorios as ca on ca.id=vda.id_accesorio 
                    left JOIN garantias as gar on gar.idventa=vda.id_venta and gar.serieid=sa.serieId and gar.modeloid=vda.id_accesorio
                    WHERE vn.activo=1 AND vn.combinada=1 and vda.garantia=1 $estatus_where
                    )";
        }
        $strq.=') as datos';
        return $strq;
    }

}