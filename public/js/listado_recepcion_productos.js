var base_url = $('#base_url').val();
var table;
$(document).ready(function() {
	table = $('#tabla_equipos').DataTable();
	loadtable();
});
function loadtable(){
    var personals=$('#idpersonal option:selected').val();

	table.destroy();
	table = $('#tabla_equipos').DataTable({
        search: {
                return: true
            }, 
        //stateSave: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url+"index.php/Recepcion_producto/getlist",
            type: "post",
            "data": {
                'idpersonal':personals
            },
        },
        "columns": [
            {"data": "cantidad"},
            {"data": "modelo"},
            {"data": "noparte"},
            {"data": "tipo",
            	render:function(data,type,row){
                    var html='';
                    	if(row.tipo==1){
                    		html='Consumible';
                    	}
                    	if(row.tipo==2){
                    		html='Equipo';
                    	}
                    	if(row.tipo==3){
                    		html='Accesorio';
                    	}
                    	if(row.tipo==4){
                    		html='Refaccion';
                    	}
                    	if(row.tipo==5){
                    		html='Equipo';
                    	}
                    	if(row.tipo==6){
                    		html='Accesorio';
                    	}
                    	if(row.tipo==7){
                    		html='Consumible';
                    	}
                    return html;
                }
        	},
            {"data": "entregado_status",
            	render:function(data,type,row){
            		var html='';
            		if(row.entregado_status==1){
                		html='<span class="new badge grey">Entregado</span>';
                	}
                	if(row.entregado_status==2){
                		html='<span class="new badge green">Confirmación de entrega</span>';
                	}
                    return html;
            	}
        	},
            {"data": "entregado_fecha"},
            {"data": "id",
            	render:function(data,type,row){
            		var html='';

            		return html;
            	}
        	},
        ],
        "order": [[ 5, "desc" ]],
        "lengthMenu": [[20,25, 50, 100,200,400,-1], [20,25, 50,100,200,400,'todo']],
        //"lengthMenu": [[25, 50, 100,200,400], [25, 50,100,200,400]],
        //"searching": false
        
    }).on('draw',function(){
        

    });
}