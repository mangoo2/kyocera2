<!DOCTYPE html>
<html>
<head>
  <title>Mapa con Google Maps</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <style>
    body{
      margin: 0px;
    }
    #map {
      height: 98vh;
      width: 100%;
    }
  </style>
  <!--
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyABSaHvdLATWTIeJuvYs2Xv4utVc70gR1Y"></script>-->
  <script loading="async" async src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDD2MlZRITOyVSZ8GtlEhmNEFWpw3_73CE"></script>
  
  <!--<script src="script3.js?v=3"></script>-->
  <script type="text/javascript">
    <?php echo $pintarsegundaruta=1;?>
    /*
    const locations = [
      { lat: 19.006563, lng: -98.149418, message: 'Mensaje 1' },
      { lat: 19.014234, lng: -98.137846, message: 'Mensaje 2' },
      { lat: 19.024810, lng: -98.140679, message: 'Mensaje 3' },
    ];
    */
    const locations1 = [
      <?php 
          if($result->num_rows()==0){
            echo "{ lat: 19.08782533671139, lng: -98.22777569573624, message: 'Sin registros' }";
          }
        foreach ($result->result() as $item) { 
        echo "{ lat: ".$item->latitud.", lng: ".$item->longitud.", message: '".$item->reg."' },";
      }?>
    ];
    const locations2 = [
      <?php foreach ($arrayubicli as $itemc) {
        echo "{ lat: ".$itemc['latitud'].", lng: ".$itemc['longitud'].", message: '".str_replace("'", "", $itemc['message'])."' },";
      }

      ?>
    ];
    function initMap() {
      // Create a map centered on the first location
      const map = new google.maps.Map(document.getElementById('map'), {
        center: { lat: locations1[0].lat, lng: locations1[0].lng },
        zoom: 13
      });

      // Define a custom icon for markers
      const customIcon = {
        url: '<?php echo base_url();?>public/img/file-info_40446.png', // URL of the icon image
        scaledSize: new google.maps.Size(20, 20),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(0, 10), // punto del icono que corresponde a la posición del marcador
      };
      const customIcon2 = {
        url: '<?php echo base_url();?>public/img/file-info_40446.png', // URL of the icon image
        scaledSize: new google.maps.Size(20, 20),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(0, 10), // punto del icono que corresponde a la posición del marcador
      };

      // Add markers to the map with info windows for both sets of locations
      <?php if($pintarsegundaruta==1){ ?>
        const allLocations = [...locations1, ...locations2];
      <?php }else{ ?>
        const allLocations = locations1;
      <?php } ?>
  
  allLocations.forEach(location => {
    const marker = new google.maps.Marker({
      position: { lat: location.lat, lng: location.lng },
      map,
      icon: customIcon
    });

    // Create an info window with the message
    const infowindow = new google.maps.InfoWindow({
      content: `<p>${location.message}</p>` // Display message as HTML
    });

    // Attach a click event listener to the marker
    marker.addListener('click', () => {
      infowindow.open(map, marker); // Open the info window when clicked
    });
  });

  // Create the first DirectionsService and DirectionsRenderer
  const directionsService1 = new google.maps.DirectionsService();
  const directionsRenderer1 = new google.maps.DirectionsRenderer({
    map,
    polylineOptions: {
      strokeColor: 'blue'
    }
  });

  const waypoints1 = locations1.slice(1, -1).map(location => ({
    location: new google.maps.LatLng(location.lat, location.lng),
    stopover: true
  }));

  const request1 = {
    origin: new google.maps.LatLng(locations1[0].lat, locations1[0].lng),
    destination: new google.maps.LatLng(locations1[locations1.length - 1].lat, locations1[locations1.length - 1].lng),
    waypoints: waypoints1,
    travelMode: google.maps.TravelMode.DRIVING
  };

  // Calculate and display the first route
  directionsService1.route(request1, (result, status) => {
    if (status === google.maps.DirectionsStatus.OK) {
      directionsRenderer1.setDirections(result);
    } else {
      console.error('Error getting directions:', status);
    }
  });
  <?php if($pintarsegundaruta==1){ ?>
  // Create the second DirectionsService and DirectionsRenderer
  const directionsService2 = new google.maps.DirectionsService();
  const directionsRenderer2 = new google.maps.DirectionsRenderer({
    map,
    polylineOptions: {
      strokeColor: 'red'
    }
  });

  const waypoints2 = locations2.slice(1, -1).map(location => ({
    location: new google.maps.LatLng(location.lat, location.lng),
    stopover: true
  }));

  const request2 = {
    origin: new google.maps.LatLng(locations2[0].lat, locations2[0].lng),
    destination: new google.maps.LatLng(locations2[locations2.length - 1].lat, locations2[locations2.length - 1].lng),
    waypoints: waypoints2,
    travelMode: google.maps.TravelMode.DRIVING
  };

  // Calculate and display the second route
  directionsService2.route(request2, (result, status) => {
    if (status === google.maps.DirectionsStatus.OK) {
      directionsRenderer2.setDirections(result);
    } else {
      console.error('Error getting directions:', status);
    }
  });
  <?php } ?>
}

// Load the map when the page is fully loaded
window.onload = initMap;
  </script>
</head>
<body>
  <div id="map"></div>
</body>
</html>