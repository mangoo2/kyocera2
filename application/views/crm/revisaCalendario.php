<section id="content" width="100%">
  <div class="container" width="100%">
    <div class="section">
        <h4 class="caption">Revisión de Calendario</h4>
        <div class="row">
          <input id="base_url" type="hidden" name="base_url" value="<?php echo base_url(); ?>">
          <input id="idProspecto" type="hidden" name="idProspecto" value="<?php echo $idProspecto; ?>">
        </div>

        <div class="col s12 m8 l9">
          <div id='calendar'></div>
        </div>
    </div>
  </div>
</section>