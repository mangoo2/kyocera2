<style type="text/css" class="print_css">
  .datosClienteCotizacion{
    background: <?php echo $configuracionCotizacion->color ?> !important;
    -webkit-print-color-adjust:exact;
  }
  .badgeaction{
    border-radius: 13px !important;
    min-width: 24px !important;
    cursor: pointer;
        float: left !important;
  }
  @media print {
    html{
        -webkit-print-color-adjust: exact;
    }
    .btn_imp{
      display: none;
    }
    td,th{
      font-size: 12px;
      padding: 1px;
    }
    h1{
      margin-top: 10px;
      margin-bottom: 10px;
    }
    .contenido4 h1{
      font-size: 10px;
    }
    .badgeaction,.button_add_cotizacion{
      display: none;
    }
  }
</style>
<input type="hidden" id="estatuscot" value="<?php echo $datosCotizacion[0]->estatus;?>">
<!-- START CONTENT -->
<section id="content" width="100%">
  <!--start container-->
  <div class="container" width="100%">
    <div class="section" style="margin-top: -53px;">
     <br>
      <div class="row">
        <input id="base_url" type="hidden" name="base_url" value="<?php echo base_url(); ?>">
        <input id="idCotizacion" type="hidden" name="idCotizacion" value="<?php echo $datosCotizacion[0]->id; ?>">

        <!-- FORMULARIO  card-panel-->
        <div class="col s12">
          <div class="">

            <!-- ENCABEZADO PRINCIPAL PDF -->
            <div class="row"> 
              <div class="col s3">
                <img src="<?php echo base_url(); ?>app-assets/images/<?php echo $configuracionCotizacion->logo1; ?>">
              </div>
              <div class="col s6">
                <p class="headerPDF"><?php echo $configuracionCotizacion->textoHeader; ?></p>
              </div> 
              <div class="col s3">
                <p align="center"><img class="" src="<?php echo base_url(); ?>app-assets/images/1024_kyocera_logo_mds.png" alt="materialize logo" width="200px;" height="50px;"></p>
              </div>
            </div>

            <!-- APARTADO DE INFORMACIÓN DE RECEPTOR DE COTIZACIÓN-->
            
            
            <div class="row">
              <table class="centered datosClienteCotizacion tablaBordesHeader" style="width:100%;">
                  <thead>
                    
                  </thead>
                  <tbody class="tablaBordes">
                    <tr >
                      <td class="tablaBordesHeader" >
                        Cotización Especial Para: <br>
                        <?php echo $datosCliente->empresa ?>
                      </td>
                      <td class="tablaBordesHeader">
                        Atención para: <br>
                        <?php echo $datosCotizacion[0]->atencion ?>
                      </td>
                      <td class="tablaBordesHeader">
                        Correo: <?php echo $datosCotizacion[0]->correo ?> <br>
                        Teléfono: <?php echo $datosCotizacion[0]->telefono ?>
                      </td>
                      <td class="tablaBordesHeader">
                        Cotización No. <?php echo $datosCotizacion[0]->id?> 
                      </td>
                    </tr>
                    <tr>
                      <td colspan="4" style="font-size: 12px;">
                        <b>Suministros / Refacciones para su equipo de la marca "KYOCERA"</b>
                      </td>
                    </tr>
                  </tbody>
                </table>
            </div>
            <br><br>
            <div align="center"><div class="" id="textoHeader2" contenteditable="true"><?php echo $configuracionCotizacion->textoHeader2;?></div></div>
            <div class="row button_add_cotizacion">
              <div class="col s3">
                <a class="btn-bmz btn cyan waves-effect waves-light" type="button" href="<?php echo base_url(); ?>Cotizaciones/altaCotizacion/<?php echo $button_add_cotizacion;?>" >Agregar nuevo</a>
              </div>
            </div>
            <!------------ TABLA DE INFORMACIÓN DE EQUIPOS ------------>
            <div class="row">
                <table class="centered tablaBordesPrincipal letraTablaImpresionConsumibles" id="tablaRefacciones" style="width: 100%">
                  <thead>
                    <tr>
                        
                        <th class="tablaBordesPrincipal" width="5%">Pieza</th>
                        <th class="tablaBordesPrincipal" width="%%">Modelo</th>
                        <th class="tablaBordesPrincipal" width="5%">N° de Parte</th>
                        <th class="tablaBordesPrincipal" width="5%">Equipo</th>
                        <th class="tablaBordesPrincipal" width="5%">Rendimiento</th>
                        <th class="tablaBordesPrincipal" width="9%">Precio</th>
                        <?php if ($datosCotizacion[0]->propuesta==0) { ?>
                          <!--<th class="tablaBordesPrincipal" width="6%">SubTotal</th>-->
                        <?php } ?>
                        <?php if ($datosCotizacion[0]->propuesta==1) { ?>
                          <!--<th class="tablaBordesPrincipal" width="6%">IVA</th>-->
                          <th class="tablaBordesPrincipal" width="9%">Total</th>
                        <?php } ?>
                    </tr>
                  </thead>
                  <tbody >
                    <?php 
                      $total=0;

                    foreach ($detallesCotizacion->result() as $item) { 
                      $total=$total+($item->piezas*$item->precioGeneral);

                      ?>
                      <tr>
                        <td class="tablaBordesPrincipal"  onclick="editarp(<?php echo $item->DT_RowId;?>,<?php echo $item->piezas ?>)">
                          <?php echo $item->piezas ?>   
                        </td>
                        <td class="tablaBordesPrincipal"  ><span class="new badge red badgeaction" onclick="deleteconsumiblev(<?php echo $item->DT_RowId;?>)" >X</span> <?php echo $item->modelo ?></td>
                        <td class="tablaBordesPrincipal"  ><?php echo $item->parte ?></td>
                        <td class="tablaBordesPrincipal"  ><?php echo $item->equipo ?></td>
                        <td class="tablaBordesPrincipal"  ><?php 
                          if ($item->tipo==5) {
                            echo $item->rendimiento_unidad_imagen;
                          }else{
                            echo $item->rendimiento_toner;
                          }

                         ?></td>
                        <td class="tablaBordesPrincipal"  onclick="editar(<?php echo $item->DT_RowId;?>,<?php echo $item->precioGeneral;?>,<?php echo $item->piezas ?>)">$<?php echo number_format($item->precioGeneral,2,'.',','); ?></td>
                        <?php if ($datosCotizacion[0]->propuesta==0) { ?>
                        <!--<td class="tablaBordesPrincipal"  >$<?php echo number_format($item->totalGeneral,2,'.',','); ?></td>-->
                        <?php } ?>
                        <?php if ($datosCotizacion[0]->propuesta==1) { ?>
                          <!--<td class="tablaBordesPrincipal">$<?php echo number_format(($item->totalGeneral*0.16),2,'.',','); ?></td>-->
                          <td class="tablaBordesPrincipal">$<?php echo number_format($item->piezas*$item->precioGeneral,2,'.',','); ?></td>
                        <?php } ?>
                      </tr>
                    <?php } ?>
                    <?php if ($datosCotizacion[0]->propuesta==0) { ?>
                     
                      <tr class="tablaBordesPrincipal">
                          <td class="tablaBordesPrincipal" colspan="5" align="right" style="text-align: right;"><b>SubTotal:</b></td>
                          <td class="tablaBordesPrincipal "><b>$ <?php echo number_format($total,2,'.',',');?></b></td> 
                      </tr>
                      <tr class="tablaBordesPrincipal">
                          <td class="tablaBordesPrincipal" colspan="5" align="right" style="text-align: right;"><b>I.V.A:</b></td>
                          <td class="tablaBordesPrincipal "><b>$ <?php echo number_format($total*0.16,2,'.',',');?></b></td> 
                      </tr>
                      <tr class="tablaBordesPrincipal">
                          <td class="tablaBordesPrincipal" colspan="5" align="right" style="text-align: right;"><b>TOTAL:</b></td>
                          <td class="tablaBordesPrincipal "><b>$ <?php echo number_format($total+($total*0.16),2,'.',',');?></b></td> 
                      </tr> 
                    <?php  } ?> 
                  </tbody>
                </table>
        
            </div>

            

            <div align="center"><div contenteditable="true"><?php echo $configuracionCotizacion->leyendaPrecios; ?></div></div>
            
            <!-- Observaciones -->
            <div class="row" style="margin-bottom: -12px">
              <table class="centered letraTablaFooterEquipos" style="width:100%">
                  <thead>
                      <tr>
                        <th class="tablaBordesPrincipal letraTablaFooterEquiposHeader"><div contenteditable="true" class="info1" oninput="saveinfo_o(1)"><?php 
                          if($datosCotizacion[0]->info1!=null){
                              echo $datosCotizacion[0]->info1;
                            }else{
                              echo $configuracionCotizacion->tituloObservaciones;
                            }
                        //$configuracionCotizacion->tituloObservaciones; ?></div></th>
                        <th class="tablaBordesPrincipal letraTablaFooterEquiposHeader"><?php echo $configuracionCotizacion->titulo1Footer; ?></th>
                      </tr>
                  </thead>
                  <tbody >
                    <tr>
                        <td class="tablaBordesPrincipal" width="50%"><div contenteditable="true" class="info2" oninput="saveinfo_o(2)"><?php 
                          if($datosCotizacion[0]->info2!=null){
                              echo $datosCotizacion[0]->info2;
                            }else{
                              echo $configuracionCotizacion->contenidoObservaciones;
                            }
                        //$configuracionCotizacion->contenidoObservaciones; ?></div></td>
                        <td class="tablaBordesPrincipal" width="50%"><?php echo $configuracionCotizacion->contenido1Footer; ?></td>
                    </tr>
                  </tbody>
                </table>
            </div>
            <br>

            
            <div class="row" style="margin-bottom: -12px">
              <table class="centered letraTablaFooterEquipos" style="width:100%">
                  <thead>
                      <tr>
                        <th class="tablaBordesPrincipal"><div contenteditable="true" class="info3" oninput="saveinfo_o(3)"><?php 
                          if($datosCotizacion[0]->info3!=null){
                              echo $datosCotizacion[0]->info3;
                            }else{
                              echo $configuracionCotizacion->titulo2Footer;    
                            }
                        //$configuracionCotizacion->titulo2Footer; ?></div></th>
                        <th class="tablaBordesPrincipal"><?php echo $fechareg; ?></th>
                      </tr>
                  </thead>
                  <tbody >
                    <tr>
                        <td class="tablaBordesPrincipal " width="50%"><div contenteditable="true" class="info4" onchange="saveinfo_o(4)"><?php 
                          if($datosCotizacion[0]->info4!=null){
                              echo $datosCotizacion[0]->info4;
                            }else{
                              echo $configuracionCotizacion->contenido2Footer;  
                            }
                        //$configuracionCotizacion->contenido2Footer; ?></div></td>
                        <td class="tablaBordesPrincipal contenido4" width="50%">
                          <?php echo $configuracionCotizacion->firma;?><b><?php echo $nombreUsuario ?></b><br><?php echo $emailUsuario ?>
                          </td>
                    </tr>
                  </tbody>
                </table>
            </div>

           

<!--columna 1---------------------------------------------------------------->

              <form class="form" id="" method="post">
                <?php
                if(isset($cliente))
                {
                ?>
                <input id="idCliente" name="idCliente" type="hidden" value="<?php echo $idCliente; ?>">
                <?php
                }
                ?>
                <div class="row">
                  <div class="input-field col s12">
                    <button class="btn-bmz btn cyan waves-effect waves-light right btn_imp" type="button" onclick="btn_guardar()">Imprimir
                    <i class="material-icons right">send</i>
                    </button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<div id="modaleditor" class="modal">
  <div class="modal-content ">
    <h4>Editar Precio</h4>
    <div class="border_datos" id="fiscales_dato1">
      <div class="col s24 m24 l12">
        <div class="row ">
            <div class="row">
              <input id="idEquipon" type="hidden" value="">
              <input id="cantidad" type="hidden" value="">
              <div class="input-field col s12">
                <input id="password" type="password" class="validate" autocomplete="new-password" required>
                <label for="password">Password</label>
              </div>
            </div>
            <div class="row">
              <div class="input-field col s12">
                <input id="newprecio" type="number" class="validate" placeholder="Precio" required>
                <label for="newprecio">Nuevo Precio</label>
              </div>
            </div>
                                        

        </div>                          
      </div>
    </div>                    
  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
    <a href="#!" class="modal-action modal-close waves-effect waves-green green btn-flat editarcosto">Aceptar</a>
  </div>
</div>
<script type="text/javascript">
  function saveinfo_o(tipo){
    saveinfo(<?php echo $datosCotizacion[0]->id;?>,tipo);
  }
</script>