var base_url = $('#base_url').val();
var cli_aux=0;
var idcpv=0;
var blq_perm_ven_ser;
$(document).ready(function(){
	$('.tasign').change(function(event) {
		var tasign=$('input:radio[name=tasign]:checked').val()
		if (tasign==1) {
			preasignacion();
      $('.datos_cliente').html('');
		}else if(tasign==2){
      preasignacion();
      $('.datos_cliente').html('');
		}
	});
  $('input[name=ttipo]').click(function(event) {
    checkedtipo();
  });
  $('.modal').modal();
});
function checkedtipo(){
  var ttipo=$('input[name=ttipo]:checked').val();
    if (ttipo==1) {
      var costos=$('#tservicio option:selected').data('local');
    }
    if (ttipo==2) {
      var costos=$('#tservicio option:selected').data('semi');
    }
    if (ttipo==3) {
      var costos=$('#tservicio option:selected').data('foraneo');
    }
    if (ttipo==4) {
      var costos=$('#tservicio option:selected').data('especial');
    }
    $('.visordecosto').html('$ '+costos);
}
function preasignacion(){
  var tventas=$('#totalventas').val();
  var tpolizas=$('#totalpolizas').val();
  var tcontratos=$('#contrsinservicios').val();

  var htmlnp='<small class="notification-badge';
                  if (tpolizas>0) {
                    htmlnp+=' orange '; 
                  }
                  htmlnp+='accent-3 eux_can_asig_p"';
                  htmlnp+='style="background-color: gray; top: -43px;left: 37px;"';
                  htmlnp+='>'+tpolizas+'</small>';
                  htmlnp+='</label>';

  var htmlnc='<small class="notification-badge';
                  if (tcontratos>0) {
                    htmlnc+=' orange '; 
                  }
                  htmlnc+='accent-3 eux_can_asig_p"';
                  htmlnc+='style="background-color: gray; top: -43px;left: 37px;"';
                  htmlnc+='>'+tcontratos+'</small>';
                  htmlnc+='</label>';



	var html='<div class="row">\
                <div class="col s12">\
                  <input name="prasignaciontipo" type="radio" class="prasignaciontipo" id="prasignaciontipo1" onchange="prasignaciontipo()" value="1">\
                  <label for="prasignaciontipo1" >Rentas '+htmlnc+'</label>\
                  <input name="prasignaciontipo" type="radio" class="prasignaciontipo" id="prasignaciontipo2" onchange="prasignaciontipo()" value="2">\
                  <label for="prasignaciontipo2" >Servicio '+htmlnp+'</label>';
            var tasign=$('input:radio[name=tasign]:checked').val()
            var etiqueta_aux;
            if (tasign==1) {
              etiqueta_aux='style="display:none;"';
            }else if(tasign==2){
              etiqueta_aux='style="display:block;"';
              html+='<input name="prasignaciontipo" type="radio" class="prasignaciontipo" id="prasignaciontipo4" onchange="prasignaciontipo()" value="4">\
                  <label for="prasignaciontipo4" style="top: -24px;">Ventas';
                  html+='<small class="notification-badge';
                  if (tventas>0) {
                    html+=' orange '; 
                  }
                  html+='accent-3 eux_can_asig_p"';
                  html+='style="background-color: gray;"';
                  html+='>'+tventas+'</small>';
                  html+='</label>';
            }

            html+='<input '+etiqueta_aux+' name="prasignaciontipo" type="radio" class="prasignaciontipo" id="prasignaciontipo3" onchange="prasignaciontipo()" value="3">\
                  <label '+etiqueta_aux+' for="prasignaciontipo3" >Nuevo Cliente</label>';
          
          html+='</div>\
              </div>\
              <div class="row">\
                <div class="col s3 select_asig_cliente">\
                  <label>Seleccionar cliente:</label>\
                  <select class="browser-default" id="pcliente"></select>\
                </div>\
                <div class="col s3 divcontrato" style="display:none;">\
                  <label>Seleccionar contrato:</label>\
                  <select class="browser-default" id="pcontrato"></select>\
                </div>';
         
         html+='<div class="col s3 divpoliza" style="display:none;">\
                  <label>Seleccionar Póliza:</label>\
                  <select class="browser-default" id="ppoliza"></select>\
                </div>\
              </div>';

         html+='<div class="row listocontratos_pre">\
              </div>';

	$('.valortipoasignacion').html(html);
	selectclientesp();
}
function selectclientesp(){
	$('#pcliente').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una opción',
        allowClear: true,
        ajax: {
            url: base_url+'Ventasi/searchclientes',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.id,
                        text: element.empresa,
                        bloqueo:element.bloqueo,
                        blq_perm_ven_ser:element.bloqueo_perm_ven_ser

                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function (e) {
          var data = e.params.data;
          console.log(data.bloqueo);
          var vbloqueo=data.bloqueo;
          cli_aux=data.id;
          if(vbloqueo==1){
            bloqueov();
          }
          var tasign=$('input:radio[name=prasignaciontipo]:checked').val();
          if (tasign==1 && vbloqueo==0) {
            contratosclientep(data.id);
          }
          if (tasign==2 && vbloqueo==0) {
            polizasclientep(data.id);
          }
          if (tasign==3 && vbloqueo==0) {
            datoscliente(data.id);
          }
          obtenerventaspendientes(data.id,data.blq_perm_ven_ser);
          obtenerultimosservicios(data.id);
    });
    $('#pcontrato').select2();
    //prasignaciontipo();
}
function prasignaciontipo(){
    $('.intoner_label').hide();
    $('.listocontratos_pre').html('');
		var tasign=$('input:radio[name=prasignaciontipo]:checked').val();
		if (tasign==1) {
			$('.divcontrato').show('show');
			$('.divpoliza').hide();
      $('.datos_cliente').html('');
			setTimeout(function(){ 
				$('#pcontrato').select2();
			}, 1000);
      $('.mostrarsoloclientes').hide();
      $('.ncequiposs').hide();
			$('.select_asig_cliente').show();
      $('.intoner_label').show();
		}
    if(tasign==2){
			//$('.divpoliza').show('show');
			$('.divcontrato').hide();
      $('.datos_cliente').html('');
			setTimeout(function(){ 
				$('#ppoliza').select2();
			}, 1000);
      $('.mostrarsoloclientes').hide();
      $('.ncequiposs').hide();
      $('.select_asig_cliente').hide();
      //==========================
      $.ajax({
        type:'POST',
        url: base_url+'index.php/Asignaciones/obtenerserviciospolizas',
        success:function(data){
          //console.log(data);
            $('.listocontratos_pre').html(data);
            setTimeout(function(){ 
              $('#tableventassolicitud').DataTable({
                "lengthMenu": [[90, 100, 200, -1], [90, 100, 200, "Todo"]],
                "order": [[ 0, "desc" ]]
              });
            }, 1000);
        }
    });
		}	
    if(tasign==3){
      $('.divpoliza').hide();
      $('.divcontrato').hide();
      $('.ncequiposs').show();
      cliente_texto();
      $('.select_asig_cliente').show();
    } 
    if(tasign==4){
      $('.select_asig_cliente').hide();
      $('.datos_cliente').html('');
      $('.divcontrato').hide();
      $('.ncequiposs').hide();
      $.ajax({
        type:'POST',
        url: base_url+'index.php/Asignaciones/obtenerserviciosventas',
        success:function(data){
            $('.listocontratos_pre').html(data);
            setTimeout(function(){ 
              $('#tableventassolicitud').DataTable({
                "lengthMenu": [[90, 100, 200, -1], [90, 100, 200, "Todo"]],
                "order": [[ 0, "desc" ]]
              });
              filtrarprefacturasventas();
            }, 1000);
        }
    });
    }
}
function contratosclientep(id){
  cli_aux=id;
  var tasign = $('input[name=tasign]:checked').val();//1pre asignacion 2 asginacion
	$.ajax({
        type:'POST',
        url: base_url+"NewFolio/contratos",
        data: {
        	id:id
        },
        success: function (data){
        	$('#pcontrato').html(data); 
            $('#pcontrato').select2().on('select2:select', function (e) {
          			var data = e.params.data;
                //console.log(data);
                //console.log(data.element.dataset.estatus);
                var estatus = data.element.dataset.estatus;
                var vencido = data.element.dataset.vencido;
                var fechafinal = data.element.dataset.fechafinal;
                if (estatus==0) {
                  var personal=data.element.dataset.personal;
                  $.alert({boxWidth: '30%',useBootstrap: false,title: 'Alerta!',
                            content: 'Contrato cancelado por '+personal+' <br>No es posible agendar servicios para este contrato'});
                }else{
                  if(tasign==1){
                    if(vencido==1){
                      $.alert({boxWidth: '30%',useBootstrap: false,title: 'Alerta!',
                            content: 'El contrato se encuentra vencido : '+fechafinal});
                      $('.tabaddconsulcf').html('');
                    }else{
                      datoscontrato(data.id);  
                    }
                  }else{
                    datoscontrato(data.id);  
                  }
                  
                }
		      $('.equipospolizacontrato').html('');      	
		    });
        }
    });
}
function datoscontrato(id){
      var tasign = $('input[name=tasign]:checked').val();//1pre asignacion 2 asginacion
      var cant_us = $('#cant_ult_ser').val();
      if (parseInt(tasign)==2) {
        var disabledventa='';
      }else{
        var disabledventa='disabled';
      }
	$.ajax({
        type:'POST',
        url: base_url+"Asignaciones/datoscontrato",
        data: {
        	id:id
        },
        success: function (data){
          $('.tabaddconsulcf').html('');
        	console.log(data);
        	var array = $.parseJSON(data);
        	console.log(array);
        	console.log(array.contrato.idcontrato);
        	var html='<table id="tabla_asignar" class="responsive-table display" cellspacing="0">\
                  <thead>\
                    <tr>\
                      <th></th><th>cliente</th><th>contrato</th><th>Ejecutivo</th><th>Acciones</th>\
                    </tr>\
                  </thead>\
                  <tbody>\
                    <!------------------------------------->\
                    <tr>\
                      <td>'+array.contrato.idcontrato+'</td>\
                      <td>'+array.contrato.empresa+'</td>\
                      <td>'+array.contrato.folio+'</td>\
                      <td>'+array.contrato.ejecutivo+'</td>\
                      <td>';
                        html+='<a class="btn-floating red tooltipped datos ocultar_temp"\
                          data-position="top" data-delay="50" data-tooltip="Venta"\
                          onclick="venta('+array.contrato.idcontrato+',0)" '+disabledventa+'>\
                         <i class="fas fa-cart-plus"></i></a> ';
                      	html+='<a class="btn-floating red tooltipped datos databtndisabled fecha_servicio" data-position="top" data-delay="50" data-tooltip="Datos de Entrega" onclick="calendariop('+array.contrato.idcontrato+')">\
                         <i class="material-icons">date_range</i></a>\
                         <a class="btn-floating red tooltipped prioridad_servicio" data-position="top" data-delay="50" data-tooltip="Asignar Prioridad"  onclick="prioridad('+array.contrato.idcontrato+')">\
                          <i class="material-icons">report_problem</i></a> ';
                        html+='<a class="btn-floating red tooltipped zona_servico" data-position="top" data-delay="50" data-tooltip="Ruta" onclick="ruta('+array.contrato.idcontrato+')">\
                          <i class="material-icons">navigation</i></a> ';
                          html+='<a class="btn-floating red tooltipped prefactura condiciones_servicio" data-position="top" data-delay="50" data-tooltip="Prefactura" onclick="serviciot('+array.contrato.idcontrato+')" >\
                          <i class="fas fa-wrench"></i></a> ';
                          html+='<a class="btn-floating red tooltipped prefactura equipos_servicio" data-position="top" data-delay="50" data-tooltip="Prefactura" onclick="equipos('+array.contrato.idRenta+',0)">\
                          <h4 style="margin: 0px;text-align: center;">E</h4></a> ';
                          if(cant_us>0){
                            html+='<a class="btn-floating redd tooltipped  ultimosservicios_img" data-position="top" data-delay="50" data-tooltip="Servicios a garantia" onclick="ultimosservicios()"></a> ';
                          }
                          
                          html+='<button class="waves-effect cyan btn-bmz generar_bloqueo" onclick="generar('+array.contrato.idcontrato+')">Generar</button>';
                        html+='</td>\
                    </tr>\
                    <!------------------------------------->\
                  </tbody>\
                </table>';
			$('.listocontratos_pre').html(html);
      if ($("input:radio[name=tasign]:checked").val()==2) {
          var consrow=0;
          array.consumibles.forEach(function(element) {
            var htmlconsumibles='<tr class="consucf_'+consrow+'">\
                                  <td width="15%">1\
                                    <input type="hidden" id="contfolid" value="'+element.idcontratofolio+'" >\
                                  </td>\
                                  <td width="55%">'+element.modelo+' '+element.foliotext+'</td>\
                                  <td width="15%">$ 0.00</td>\
                                  <td width="15%">\
                                    <a class="btn-floating red tooltipped elimina" onclick="detetevenconsu('+consrow+')">\
                                      <i class="material-icons">delete_forever</i></a>\
                                  </td>\
                                </tr>';
            $('.tabaddconsulcf').append(htmlconsumibles);
            consrow++;
          });
          $('.tabaddconsulcf').html('');
      }
			contratoacciones();
        }
    });	
}
function detetevenconsu(id){
  $('.consucf_'+id).remove();
}
function polizasclientep(id){
  cli_aux=id;
	$.ajax({
        type:'POST',
        url: base_url+"Asignaciones/polizas",
        data: {
        	id:id
        },
        success: function (data){
        	$('#ppoliza').html(data); 
            $('#ppoliza').select2().on('select2:select', function (e) {
          			var data = e.params.data;
		          	datospoliza(data.id);
		    });
        }
    });
}
function datospoliza(id){
  idcpv=id;
  var tasign = $('input[name=tasign]:checked').val();//1pre asignacion 2 asginacion
      if (parseInt(tasign)==2) {
        var disabledventa='';
      }else{
        var disabledventa='disabled';
      }
	$.ajax({
        type:'POST',
        url: base_url+"Asignaciones/datospoliza",
        data: {
        	id:id
        },
        success: function (data){
        	console.log(data);
        	var array = $.parseJSON(data);
        	console.log(array);
        	console.log(array.id);
          var vcont_ser = array.vcont_ser;
          cli_aux=array.cli_pol;
          //======================================================
            if(array.pol.viewcont==1){
              var viewcont_cant=array.pol.viewcont_cant;
              var ser_rest=array.pol.ser_rest;
              if(ser_rest>0){
                 viewcont_cant=array.pol.ser_rest;
              }
              var n_ser_g=parseInt(array.n_ser)+parseInt(array.n_ser_ex);

              var viewcont='('+n_ser_g+' /'+viewcont_cant+') <a href="'+base_url+'PolizasCreadas/servicios/'+array.pol.id+'" target="_blank"><i class="fas fa-retweet" title="Poliza recurente"></i></a>';
              if(n_ser_g >= viewcont_cant){
                var onclick_generar=' onclick="limitedeservicios()" ';
              }else{
                var onclick_generar=' onclick="generar('+array.pol.id+')" ';
              }
            }else{
              var viewcont='';
              var onclick_generar=' onclick="generar('+array.pol.id+')" ';
            }
          //======================================================
            if(array.id_com>0){
              var idpre=array.id_com;
            }else{
              var idpre=array.pol.id;
            }

        	var html='<table id="tabla_asignar" class="responsive-table display" cellspacing="0">\
                  <thead>\
                    <tr>\
                      <th></th><th>cliente</th><th>Poliza</th><th>Ejecutivo</th><th>Acciones</th>\
                    </tr>\
                  </thead>\
                  <tbody>\
                    <tr>\
                      <td>'+idpre+'</td>\
                      <td>'+array.pol.empresa+'</td>\
                      <td>'+array.pol.id+' '+viewcont+'</td>\
                      <td>'+array.pol.ejecutivo+'</td>\
                      <td>\
                        <a class="btn-floating red tooltipped datos ocultar_temp"\
                          data-position="top" data-delay="50" data-tooltip="Venta"\
                          onclick="venta('+array.pol.id+',1)" '+disabledventa+'>\
                         <i class="fas fa-cart-plus"></i></a>\
                      	<a class="btn-floating red tooltipped databtndisabled fecha_servicio" data-position="top" data-delay="50" data-tooltip="Datos de Entrega" onclick="calendariop('+array.pol.id+')">\
                         <i class="material-icons">date_range</i></a>\
                         <a class="btn-floating red tooltipped prioridad_servicio" data-position="top" data-delay="50" data-tooltip="Asignar Prioridad" onclick="prioridad('+array.pol.id+')">\
                          <i class="material-icons">report_problem</i></a>  \
                        <a class="btn-floating red tooltipped ruta zona_servico" data-position="top" data-delay="50" data-tooltip="Ruta" onclick="ruta('+array.pol.id+')">\
                          <i class="material-icons">navigation</i></a>\
                          <a class="btn-floating red tooltipped prefactura condiciones_servicio" data-position="top" data-delay="50" data-tooltip="Prefactura" onclick="serviciot('+array.pol.id+')" >\
                          <i class="fas fa-wrench"></i></a>\
                          <a class="btn-floating red tooltipped prefactura equipos_servicio eq_ser_pol_'+array.pol.id+'" data-cli="'+array.cli_pol+'" data-position="top" data-delay="50" data-tooltip="Prefactura" onclick="equipos('+array.pol.id+',1)">\
                          <h4 style="margin: 0px;text-align: center;">E</h4></a>  ';
                          if(vcont_ser>0){
                            html+='<a class="btn-floating redd tooltipped ultimosservicios_img " data-position="top" data-delay="50" data-tooltip="Servicios a garantia" onclick="ultimosservicios()"></a> ';
                          }
                    html+='<button class="waves-effect cyan btn-bmz generar_bloqueo" '+onclick_generar+' >Generar</button>\
                        </td>\
                    </tr>\
                  </tbody>\
                </table>';
			$('.listocontratos_pre').html(html);
			contratoacciones();
        }
    });	
}
function deplegaraccionesp(id){
  idcpv=id;
  $('#modalDatos').modal();
  $('.acciones_poliza').html('');
  var html='<a class="btn-floating red tooltipped datos ocultar_temp"\
              data-position="top" data-delay="50" data-tooltip="Venta"\
              onclick="venta('+id+',1)" disabled>\
             <i class="fas fa-cart-plus"></i></a>\
            <a class="btn-floating red tooltipped databtndisabled fecha_servicio" data-position="top" data-delay="50" data-tooltip="Datos de Entrega"  onclick="calendariop('+id+')">\
             <i class="material-icons">date_range</i></a>\
             <a class="btn-floating red tooltipped prioridad_servicio" data-position="top" data-delay="50" data-tooltip="Asignar Prioridad"  onclick="prioridad('+id+')">\
              <i class="material-icons">report_problem</i></a>  \
            <a class="btn-floating red tooltipped ruta zona_servico" data-position="top" data-delay="50" data-tooltip="Ruta"  onclick="ruta('+id+')">\
              <i class="material-icons">navigation</i></a>\
              <a class="btn-floating red tooltipped prefactura condiciones_servicio" data-position="top" data-delay="50" data-tooltip="Prefactura" onclick="serviciot('+id+')" >\
              <i class="fas fa-wrench"></i></a>\
              <a class="btn-floating red tooltipped prefactura equipos_servicio" data-position="top" data-delay="50" data-tooltip="Prefactura" onclick="equipos('+id+',1)">\
              <h4 style="margin: 0px;text-align: center;">E</h4></a>  \
              <button class="waves-effect cyan btn-bmz generar_bloqueo" onclick="generar('+id+')">Generar</button>';
  $('.acciones_poliza_'+id+'_0').html(html);
}
function contratoacciones(){
	$('#conf_crea').click(function(event) {
		var number = $("#num_orden").val();
		$.alert({boxWidth: '30%',useBootstrap: false,title: 'Éxito!',content: 'Se ha creado la orden: '+number});
		setTimeout(function(){ 
            window.location.href = base_url+"index.php/Ordenes_asignadas"; 
        }, 2000); 
	});

	//$('#ruta').select2();

    $('#tecnico').select2();
}
function ruta(id){
  $('#modalRuta').modal('open');
  $("#id_cp_f").val(id);
  datos_servicios(id);
}
function serviciot(id){
  if ($('input:radio[name=prasignaciontipo]:checked').val()==2) {
    $('.condiciones_servicio').removeClass('red').addClass('green');
    setTimeout(function(){ 
      //$('.condiciones_servicio').removeClass('green').addClass('red');
    }, 90000);
    window.open(base_url+"PolizasCreadas/view/"+id, "Prefactura", "width=780, height=612");
  }else{
    $('#modalservicio').modal('open');
    $("#id_cp_f").val(id);
    if($('#tserviciomotivo').val().length==0){
      datos_servicios(id);
      obtenerservicios();
      checkedtipo();
    }
  }
  
}
function prioridad(id){
  var tasign = $('input[name=tasign]:checked').val();
    if(tasign==1){
      //$('.soloasgig').attr('disabled',true);
      //$('.solopreasgig').attr('disabled',false);

    }else{
      //$('.soloasgig').attr('disabled',false);
      //$('.solopreasgig').attr('disabled',true);

    }

  $('#modalPrioridad').modal('open');
  $("#id_cp_f").val(id);
  datos_servicios(id);
}
function asig_tec(id){
  $('#modalAsigna').modal('open');
  $("#id_cp_f").val(id);
  datos_servicios(id);
  $('.tecnicoselect').val(49);
}
function calendariop(id){
	$('#modalDatos').modal('open');
	$("#id_cp_f").val(id);
	datos_servicios(id);
  obtenerfechainfo();
}
function venta(id,tipo){
  $('#modalventa').modal('open');
}
function obtenerfechainfo(id){
    var cliente = $('#pcliente option:selected').val();
    $.ajax({
        type:'POST',
        url: base_url+'index.php/Clientes/obtenerdatoscliente',
        data: {
          cliente:cliente
        },
        success:function(data){
            console.log(data);
            var array = $.parseJSON(data);
            if (array.horario_disponible!=''||array.horario_disponible!='null' ) {
              $('.infohorario').html('Horario disponible: '+array.horario_disponible);
              
            }
            
        }
    });
}
function datos_servicios(id){
    var tasign = $('input[name=tasign]:checked').val();//1pre asignacion 2 asginacion
    var tipo = $('input[name=prasignaciontipo]:checked').val();//1 contrato 2 poliza
    $.ajax({
        type:'POST',
        url: base_url+'index.php/Asignaciones/get_DatosAsigna',
        data: {
          tasign:tasign,
          tipo:tipo,
          id:id
        },
        success:function(data){
            console.log(data);
            var array = $.parseJSON(data);
            if (array.length>0) {
              //$('#direccion').html(array.direccion);
              $('#id_servicio_f').val(array[0].asignacionId);
              /*
              $('#fecha').val(array[0].fecha);
              $("#hora").val(array[0].hora);
              $("#horafin").val(array[0].horafin);
              $("#hora_comida").val(array[0].hora_comida);
              $("#horafin_comida").val(array[0].horafin_comida);
              $("#tecnico").val(array[0].tecnico).change();
              $("#test"+array[0].prioridad).click();
              $("#tservicio").val(array[0].tservicio).change();
              $("#tserviciomotivo").val(array[0].tserviciomotivo);

              $("#tpoliza").val(array[0].tpoliza);
              $(".ttipo"+array[0].ttipo).attr('checked', true);
              if (array[0].generado==1) {
                $('.databtndisabled').attr('disabled',true);
              }
              */
            }else{
              $('#id_servicio_f').val(0);
              $('#fecha').val('');
              $("#hora").val('');
              $("#horafin").val('');
              $("#hora_comida").val('');
              $("#horafin_comida").val('');
              $("#tecnico").val(0).change();
              //$("#test"+array[0].prioridad).click();
              $("#tservicio").val(0).change();
              $("#tserviciomotivo").val('');
            }
            
        }
    });
}
function savefecha(){
  $('.fecha_servicio').removeClass('red').addClass('green');
  setTimeout(function(){ 
    $('.fecha_servicio').removeClass('green').addClass('red');
  }, 90000);
  var tasign = $('input[name=tasign]:checked').val();//1pre asignacion 2 asginacion
  var tipo = $('input[name=prasignaciontipo]:checked').val();//1 contrato 2 poliza
  var idconpol=$('#id_cp_f').val();//idcontradto idpoliza
  var restringido_save=1;
    if($('#fecha').val()==''){
      restringido_save=0;
      toastr["warning"]("Ingrese una fecha", "Advertencia")
    }
    if($('#hora').val()==''){
      restringido_save=0;
      toastr["warning"]("Ingrese una Hora Inicio", "Advertencia")
    }
    if($('#horafin').val()==''){
      restringido_save=0;
      toastr["warning"]("Ingrese una Hora fin", "Advertencia")
    }
    if(idconpol>0){

    }else{
      restringido_save=0;
    }
  if (restringido_save==1) {
    $.ajax({
          type:'POST',
          url: base_url+'index.php/Asignaciones/get_savefecha',
          data: {
            tasign:tasign,
            tipo:tipo,
            idconpol:idconpol,
            serviciorow:$('#id_servicio_f').val(),
            fecha:$('#fecha').val(),
            hora:$('#hora').val(),
            horafin:$('#horafin').val(),
            hora_comida:$('#hora_comida').val(),
            horafin_comida:$('#horafin_comida').val()
            },
          success:function(data){
              
          }
      });
  }
}
function savetecnico(){
  $('.tecnico_servicio').removeClass('red').addClass('green');
  setTimeout(function(){ 
    $('.tecnico_servicio').removeClass('green').addClass('red');
  }, 90000);
  var tasign = $('input[name=tasign]:checked').val();//1pre asignacion 2 asginacion
  var tipo = $('input[name=prasignaciontipo]:checked').val();//1 contrato 2 poliza
  var idconpol=$('#id_cp_f').val();//idcontradto idpoliza
  if (idconpol>0) {
    $.ajax({
          type:'POST',
          url: base_url+'index.php/Asignaciones/get_savetecnico',
          data: {
            tasign:tasign,
            tipo:tipo,
            idconpol:idconpol,
            serviciorow:$('#id_servicio_f').val(),
            tecnico:$('#tecnico option:selected').val(),
            },
          success:function(data){
              
          }
      });
  }
}
function saveprioridad(){
  $('.prioridad_servicio').removeClass('red').addClass('green');
  setTimeout(function(){ 
    $('.prioridad_servicio').removeClass('green').addClass('red');
  }, 90000);
  var tasign = $('input[name=tasign]:checked').val();//1pre asignacion 2 asginacion
  var tipo = $('input[name=prasignaciontipo]:checked').val();//1 contrato 2 poliza
  var idconpol=$('#id_cp_f').val();//idcontradto idpoliza
  var priodidad=$('input[name=priodidad]:checked').val();
  var priodidad2=$('#prioridad2 option:selected').val();
  if (idconpol>0) {
    $.ajax({
          type:'POST',
          url: base_url+'index.php/Asignaciones/get_saveprioridad',
          data: {
            tasign:tasign,
            tipo:tipo,
            idconpol:idconpol,
            serviciorow:$('#id_servicio_f').val(),
            priodidad:priodidad,
            priodidad2:priodidad2,
            hora_cri_ser:$('#hora_cri_ser').val()
            },
          success:function(data){
              
          }
      });
  }
}
function savezona(){
  $('.zona_servico').removeClass('red').addClass('green');
  setTimeout(function(){ 
    $('.zona_servico').removeClass('green').addClass('red');
  }, 90000);
  var tasign = $('input[name=tasign]:checked').val();//1pre asignacion 2 asginacion
  var tipo = $('input[name=prasignaciontipo]:checked').val();//1 contrato 2 poliza
  var idconpol=$('#id_cp_f').val();//idcontradto idpoliza
  var zona=$('#ruta option:selected').val();
  if (idconpol>0) {
      $.ajax({
          type:'POST',
          url: base_url+'index.php/Asignaciones/get_savezona',
          data: {
            tasign:tasign,
            tipo:tipo,
            idconpol:idconpol,
            serviciorow:$('#id_servicio_f').val(),
            zona:zona,
            },
          success:function(data){
              
          }
      });
  }
}
function savemotivos(){
  $('.condiciones_servicio').removeClass('red').addClass('green');
  setTimeout(function(){ 
    $('.condiciones_servicio').removeClass('green').addClass('red');
  }, 90000);
  var tasign = $('input[name=tasign]:checked').val();//1pre asignacion 2 asginacion
  var tipo = $('input[name=prasignaciontipo]:checked').val();//1 contrato 2 poliza
  var idconpol=$('#id_cp_f').val();//idcontradto idpoliza
  var tservicio=$('#tservicio option:selected').val();
  var tserviciomotivo=$('#tserviciomotivo').val();
  if (idconpol>0) {
    if(tservicio!=null || tservicio!=undefined) {
      $.ajax({
          type:'POST',
          url: base_url+'index.php/Asignaciones/get_savemotivos',
          data: {
            tasign:tasign,
            tipo:tipo,
            idconpol:idconpol,
            serviciorow:$('#id_servicio_f').val(),
            tservicio:tservicio,
            tserviciomotivo:tserviciomotivo,
            tpoliza: $('#tpoliza option:selected').val(),
            ttipo: $('input[name=ttipo]:checked').val()
            },
          success:function(data){
              
          }
      });
    }else{
      swal("Error!", "Selecciona un servicio", "error");
    }
  }
}
function generar(id){
  $('body').loading({theme: 'dark',message: 'Generando asignación de servicio...'});
  $('.generar_bloqueo').prop('disabled',true);
  setTimeout(function(){ 
    $('.generar_bloqueo').prop('disabled',false);
  }, 5000);

  $('#id_cp_f').val(id);
  datos_servicios(id);
  setTimeout(function(){
    var id_servicio_f=$('#id_servicio_f').val();
    console.log('id_servicio_f'+id_servicio_f);
    var tasign = $('input[name=tasign]:checked').val();//1pre asignacion 2 asginacion
    var tipo = $('input[name=prasignaciontipo]:checked').val();//1 contrato 2 poliza
    var idconpol=id;
    var stock_toner = $('#intoner').is(':checked')==false?0:1;
      //=============================================
        var equipos = [];
        var TABLA   = $("#tableequipos tbody > tr");
        var TABLAlength_equipos=0;
        if (TABLA.length>0) {
          TABLA.each(function(){  
              if ($(this).find("input[class*='serie_check']").is(':checked')) {
                  item = {};
                  item['equipos'] = $(this).find("input[id*='equipoid']").val();
                  item['serie'] = $(this).find("input[id*='serieid']").val();
                  item['tecnico'] = $(this).find("select[id*='tecnico'] option:selected").val();
                  item['tecnico2'] = $(this).find("select[id*='tecnico2'] option:selected").val();
                  item['tecnico3'] = $(this).find("select[id*='tecnico3'] option:selected").val();
                  item['tecnico4'] = $(this).find("select[id*='tecnico4'] option:selected").val();
                  item['idvinculo'] = $(this).find("input[id*='idvinculo']").val();
                  
                  equipos.push(item);
                  TABLAlength_equipos=1;
              }
          });
          aInfoe   = JSON.stringify(equipos);
      //=============================================
      //=============================================
        var consumi = [];
        var TABLAc   = $("#tableaddconsulmibles tbody > tr");
          TABLAc.each(function(){  
                  itemc = {};
                  itemc['cantidad'] = $(this).find("input[id*='addcantidadc']").val();
                  itemc['consumible'] = $(this).find("input[id*='addcantidadr']").val();
                  itemc['precio'] = $(this).find("input[id*='addcantidadp']").val();
                  
                  consumi.push(itemc);
          });
          aInfoc   = JSON.stringify(consumi);
      //=============================================
      //=============================================
        var refaccion = [];
        var TABLAc   = $("#tableaddrefacciones tbody > tr");
          TABLAc.each(function(){  
                  itemc = {};
                  itemc['cantidad'] = $(this).find("input[id*='addrefaccionc']").val();
                  itemc['refaccion'] = $(this).find("input[id*='addrefaccionr']").val();
                  itemc['precio'] = $(this).find("input[id*='addrefaccionp']").val();
                  
                  refaccion.push(itemc);
          });
          aInfor   = JSON.stringify(refaccion);
      //=============================================
      //=============================================
        var raccesorio = [];
        var TABLAa   = $("#tableaddaccesorios tbody > tr");
          TABLAa.each(function(){  
                  itema = {};
                  itema['cantidad'] = $(this).find("input[id*='addaccesorioc']").val();
                  itema['accesorio'] = $(this).find("input[id*='addaccesorior']").val();
                  itema['precio'] = $(this).find("input[id*='addraccesoriop']").val();
                  
                  raccesorio.push(itema);
          });
          aInfoa   = JSON.stringify(raccesorio);
      //=============================================
      //=============================================
        var cfconsumibles = [];
        var TABLAcf   = $("#tableaddconsulmiblescf tbody > tr");
          TABLAcf.each(function(){  
                  itemcf = {};
                  itemcf['cffolioid'] = $(this).find("input[id*='contfolid']").val(); 
                  cfconsumibles.push(itemcf);
          });
          aInfocf   = JSON.stringify(cfconsumibles);
      //=============================================
      //=============================================
        var servicios = [];
        var TABLAs   = $("#table_servicios_ult tbody > tr");
          TABLAs.each(function(){  
              if ($(this).find("input[class*='servicios_ckeck']").is(':checked')) {
                  item = {};
                  item['tipo'] = $(this).find("input[class*='servicios_ckeck']").data('sertipo');
                  item['servicio'] = $(this).find("input[class*='servicios_ckeck']").data('ser');
                  servicios.push(item);
              }
          });
          aInfoser   = JSON.stringify(servicios);
      //=============================================
        if(TABLAlength_equipos==1){
          $.ajax({
            type:'POST',
            url: base_url+'index.php/Asignaciones/generar',
            data: {
              tasign:tasign,
              tipo:tipo,
              idconpol:idconpol,
              serviciorow:id_servicio_f,
              equipos:aInfoe,
              vconsumible:aInfoc,
              vrefaccion:aInfor,
              vaccesorio:aInfoa,
              cfconsuf:aInfocf,
              stock_toner:stock_toner,
              servicios:aInfoser,
              idpolizaser:$('#idpolizaser').val(),
              tipopc:$('#tipopc').val()
              },
            success:function(data){
                var info = parseInt(data);
                if (info==1) {
                  $.alert({boxWidth: '30%',useBootstrap: false,
                    title: 'Atención!',content: 'Servicios Generados'}
                  );
                  //direccionar
                  setTimeout(function(){ 
                      window.location.href = base_url+"index.php/Asignaciones"; 
                      //window.location.reload();
                  }, 2000);

                }
                if (info==0){
                  $.alert({boxWidth: '30%',useBootstrap: false,
                    title: 'Atención!',content: 'falta información por completar'}
                  );
                }
                if (info==2){
                  $.alert({boxWidth: '30%',useBootstrap: false,
                    title: 'Atención!',content: 'No es posible generarse, ya se genero la asignación'}
                  );
                }
            }
          });
        }else{
          $.alert({boxWidth: '30%',useBootstrap: false,
                  title: 'Atención!',content: 'Seleccione equipos y/o técnicos antes de continuar'});
          $('body').loading('stop');
        }
      }else{
        $.alert({boxWidth: '30%',useBootstrap: false,
                  title: 'Atención!',content: 'Seleccione equipos y/o tecnicos antes de continuar'});
        $('body').loading('stop');
      }
  },2000);
}
function detalle(id){
    window.open(base_url+"Ordenes_asignadas/nuevaOrden/"+id, "Orden de Servicio", "width=780, height=612");
}
function prefacturac(id){
  window.open(base_url+"RentasCompletadas/view/"+id, "Prefactura", "width=780, height=612");
}
function prefacturap(id){
  window.open(base_url+"PolizasCreadas/view/"+id, "Prefactura", "width=780, height=612");
}
function prefacturav(id,tipo){
    if (tipo==0) {
        window.open(base_url+"Prefactura/view/"+id, "Prefactura", "width=780, height=612");
    }else{
        window.open(base_url+"Prefactura/viewc/"+id, "Prefactura", "width=780, height=612");
    }
}
function calendariop2(){
  $('#modalDatos').modal('open');
  obtenerfechainfo();
}
function asig_tec2(){
  $('#modalAsigna').modal('open');
  $('.tecnicoselect').val(49);
}
function prioridad2(){
  $('#modalPrioridad').modal('open');
}
function ruta2(){
  $('#modalRuta').modal('open');
}
function serviciot2(){
  $('#modalservicio2').modal('open');
  obtenerservicios2();
}
function equipos(ids,idconpolis){
  $('.equipos_servicio').removeClass('red').addClass('green');
  setTimeout(function(){ 
    $('.equipos_servicio').removeClass('green').addClass('red');
  }, 90000);
  $('#modalequipos').modal('open');
  //idconpoli 0 contratos 1 polizas
  if(idconpolis==0){
    var cli_c_p=$('#pcliente option:selected').val();
  }else{
    var cli_c_p=$('.eq_ser_pol_'+ids).data('cli');
  }
  if($('.serie_check').is(':checked')==false){
    $.ajax({
        type:'POST',
        url: base_url+'index.php/Asignaciones/obtenerequipos',
        data: {
          id:ids,
          idconpoli:idconpolis,
          cli:cli_c_p,
          idpolizaser:$('#idpolizaser').val()
        },
        success:function(data){
          console.log(data);
            $('.equipospolizacontrato').html(data);
            var tasign=$('input:radio[name=tasign]:checked').val()
            if (tasign==1) {
              if(idconpolis==0){
                consultarequiposcontratosocupados(ids);
              }else{
                  $('.serie_check').attr('checked',true);
                  $('.serie_check').attr('disabled',true);
              }
              
            }else if(tasign==2){
              $('.serie_check').attr('checked',false);
            }
            $('.tecnicoselect').val(49);

            $('#serie_check_0').click(function(event) {
              var checked=$('#serie_check_0').is(':checked');
              $( ".serie_check" ).prop( "checked", checked );
              v_ulimos_ser_all();
            });
        }
    });
  }
  notificacionsercot();
}
function consultarequiposcontratosocupados(idcontrato){
  $.ajax({
        type:'POST',
        url: base_url+'index.php/Asignaciones/obtenerequiposcontratos',
        data: {
          id:idcontrato,
        },
        success:function(data){
          var array = $.parseJSON(data);
              console.log(array);
              var countequipos=0;
              array.forEach(function(element) {
                $(".serie_check_"+element.idequipo+"_"+element.serieId+"").attr('disabled',true);
                $(".consultar_fechas_agenda_"+element.idequipo+"_"+element.serieId+"").show('show');
                countequipos++;
              });
              if(countequipos>0){
                $('.liberacionequipos').html('<button class="waves-effect cyan btn-bmz generar_bloqueo" onclick="liberarservicios('+idcontrato+')">Liberar</button>');
              }else{
                $('.liberacionequipos').html('');
              }
            /*
            $('.equipospolizacontrato').html(data);
            var tasign=$('input:radio[name=tasign]:checked').val()
            if (tasign==1) {
              if(idconpolis==0){
                consultarequiposcontratosocupados(ids);
              }else{
                  $('.serie_check').attr('checked',true);
                  $('.serie_check').attr('disabled',true);
              }
              
            }else if(tasign==2){
              $('.serie_check').attr('checked',false);
            }
            */
        }
    });
}
function liberarservicios(idcontrato){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de liberar los servicios ya agendados?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Contratos/eliminarserviciosposteriores2",
                    data: {
                        idcontrato:idcontrato
                    },
                    success:function(response){  
                      $('#modalequipos').modal('close');
                      setTimeout(function(){ $('.equipos_servicio').click(); }, 900);
                      
                        //consultarequiposcontratosocupados(idcontrato);

                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}
function obtenerservicios(){
  var tpoliza = $('#tpoliza').val();
  $.ajax({
        type:'POST',
        url: base_url+'index.php/Asignaciones/obtenerservicios',
        data: {
          tpoliza:tpoliza
          },
        success:function(data){
            $('#tservicio').html(data);
            $('.chosen-select').trigger("chosen:updated");

        }
    });
}
function obtenerservicios2(){
  var tpoliza = $('#tpoliza2').val();
  $.ajax({
        type:'POST',
        url: base_url+'index.php/Asignaciones/obtenerservicios',
        data: {
          tpoliza:tpoliza
          },
        success:function(data){
            $('#tservicio2').html(data);
            $('.chosen-select').trigger("chosen:updated");

        }
    });
}
function obtenerservicios3(){
  var tpoliza = $('#tpoliza3').val();
  $.ajax({
        type:'POST',
        url: base_url+'index.php/Asignaciones/obtenerservicios',
        data: {
          tpoliza:tpoliza
          },
        success:function(data){
            $('#tservicio3').html(data);
            $('.chosen-select').trigger("chosen:updated");

        }
    });
}
function generarsolicitud(){
    var restringido_save=1;
    $('body').loading({theme: 'dark',message: 'Generando asignación de servicio...'});
    $('.generarsolicitudc').prop('disabled',true);
    setTimeout(function(){ 
      $('.generarsolicitudc').prop('disabled',false);
    }, 5000);
  //=============================================
      var consumi = [];
      var TABLAc   = $("#tableaddconsulmibles tbody > tr");
        TABLAc.each(function(){  
                itemc = {};
                itemc['cantidad'] = $(this).find("input[id*='addcantidadc']").val();
                itemc['consumible'] = $(this).find("input[id*='addcantidadr']").val();
                itemc['precio'] = $(this).find("input[id*='addcantidadp']").val();
                
                consumi.push(itemc);
        });
        aInfoc   = JSON.stringify(consumi);
      //=============================================
      //=============================================
      var refaccion = [];
      var TABLAc   = $("#tableaddrefacciones tbody > tr");
        TABLAc.each(function(){  
                itemc = {};
                itemc['cantidad'] = $(this).find("input[id*='addrefaccionc']").val();
                itemc['refaccion'] = $(this).find("input[id*='addrefaccionr']").val();
                itemc['precio'] = $(this).find("input[id*='addrefaccionp']").val();
                
                refaccion.push(itemc);
        });
        aInfor   = JSON.stringify(refaccion);
      //=============================================
      //=============================================
      var raccesorio = [];
      var TABLAa   = $("#tableaddaccesorios tbody > tr");
        TABLAa.each(function(){  
                itema = {};
                itema['cantidad'] = $(this).find("input[id*='addaccesorioc']").val();
                itema['accesorio'] = $(this).find("input[id*='addaccesorior']").val();
                itema['precio'] = $(this).find("input[id*='addraccesoriop']").val();
                
                raccesorio.push(itema);
        });
        aInfoa   = JSON.stringify(raccesorio);
      //=============================================
      //=============================================
      var servicios = [];
      var TABLAss   = $("#tableserviciosevento tbody > tr");
        TABLAss.each(function(){  
                itemcs = {};
                itemcs['poliza'] = $(this).find("input[id*='tpoliza2s']").val();
                itemcs['equipos'] = $(this).find("input[id*='tservicio2s']").val();
                itemcs['serie'] = $(this).find("input[id*='tserie2s']").val();
                itemcs['direccion'] = $(this).find("input[id*='tdireccion2s']").val();
                itemcs['iddir'] = $(this).find("input[id*='tiddir2s']").val();
                itemcs['tipo'] = $(this).find("input[id*='ttipo2s']").val();
                
                servicios.push(itemcs);
        });
        aInfors   = JSON.stringify(servicios);
        if(TABLAss.length==0){
          restringido_save=0;
          toastr.warning("Seleccione un servicio", "Advertencia");
        }
      //=============================================
        //=============================================
        var servicios = []; 
        var TABLAs   = $("#table_servicios_ult tbody > tr");
          TABLAs.each(function(){  
              if ($(this).find("input[class*='servicios_ckeck']").is(':checked')) {
                  item = {};
                  item['tipo'] = $(this).find("input[class*='servicios_ckeck']").data('sertipo');
                  item['servicio'] = $(this).find("input[class*='servicios_ckeck']").data('ser');
                  servicios.push(item);
              }
          });
          aInfoser   = JSON.stringify(servicios);
      //=============================================
  var tasign = $('input[name=tasign]:checked').val();//1pre asignacion 2 asginacion
  var tipo = $('input[name=prasignaciontipo]:checked').val();//1 contrato 2 poliza
    
    if($('#fecha').val()==''){
      restringido_save=0;
      toastr["warning"]("Ingrese una fecha", "Advertencia")
    }
    if($('#hora').val()==''){
      restringido_save=0;
      toastr["warning"]("Ingrese una Hora Inicio", "Advertencia")
    }
    if($('#horafin').val()==''){
      restringido_save=0;
      toastr["warning"]("Ingrese una Hora fin", "Advertencia")
    }
    if(restringido_save==1){
      $.ajax({
            type:'POST',
            url: base_url+'index.php/Asignaciones/get_savecliente',
            data: {
              tasign:tasign,
              tipo:tipo,
              serviciorow:$('#id_servicio_f').val(),
              cliente: $('#idclientenew').val(),
              fecha:$('#fecha').val(),
              hora:$('#hora').val(),
              horafin:$('#horafin').val(),
              hora_comida:$('#hora_comida').val(),
              horafin_comida:$('#horafin_comida').val(),
              zona:$('#ruta option:selected').val(),
              priodidad:$('input[name=priodidad]:checked').val(),
              priodidad2:$('#prioridad2 option:selected').val(),
              //tservicio:$('#tservicio option:selected').val(),
              tserviciomotivo:$('#tserviciomotivo2').val(),
              //tpoliza: $('#tpoliza option:selected').val(),
              //ttipo: $('input[name=ttipo]:checked').val(),
              tecnico:$('#tecnico option:selected').val(),
              tecnico2:$('#tecnico2 option:selected').val(),
              tecnico3:$('#tecnico3 option:selected').val(),
              tecnico4:$('#tecnico4 option:selected').val(),
              documentaccesorio: $('#documentaccesorio2').val(),
              vconsumible:aInfoc,
              vrefaccion:aInfor,
              vaccesorio:aInfoa,
              datoservicio:aInfors,
              hora_cri_ser:$('#hora_cri_ser').val(),
              servicios:aInfoser
              },
            success:function(data){
                $.alert({boxWidth: '30%',useBootstrap: false,
                  title: 'Atención!',content: 'Servicios Generados'}
                );
                //direccionar
                setTimeout(function(){ 
                    window.location.href = base_url+"index.php/Asignaciones"; 
                    //window.location.reload();
                }, 2000);
            }
      });
    }else{
      $('body').loading('stop');
    }
  
}
function select_prioridad(){
  var prio = $('#prioridad2 option:selected').val();
  if(prio>7){
    $('.horaser_cri').show('show');
  }else{
    $('.horaser_cri').hide('show');
    $('#hora_cri_ser').val('');
  }
}
//======================================
function funcionConsumiblesprecios(aux){
    var toner = $('#sconcumibles option:selected').val();
    
    $.ajax({
        url: base_url+"index.php/Cotizaciones/getDatosConsumiblesPorEquipop/"+toner,
        success: function (response){
          var array = $.parseJSON(response);
            $('#sconcumiblesp').html('');
            $('#sconcumiblesp').html(array.select_option);
            //aux.find('.tonerp').html('');
            //aux.find('.tonerp').html(response);
            //$('.tonerp').trigger("chosen:updated");
            alertastockconsumibles(toner);

        },
        error: function(response){
            alertfunction('Atención!','Algo salió mal, intente de nuevo o contacte al administrador del sistema');
        }
    }); 
}
function selectprecior(){
    var id = $('#srefaccions option:selected').val();
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Cotizaciones/mostrarselectpreciosr",
        data: {id:id},
        success: function (data){
          var array = $.parseJSON(data); 
            $('#srefaccionp').html('');
            $('#srefaccionp').html(array.select_option); 
            alertastockrefaccion(id);  
        },
        error: function(data){ 
            alertfunction('Atención!','Algo salió mal, intente de nuevo o contacte al administrador del sistema');
        }
    });
}
var rowventa=0;
function addconsu(){
    var cant= $('#cantidadc').val();
    var consumi= $('#sconcumibles option:selected').val();
    var consumit= $('#sconcumibles option:selected').text();
    var precio= $('#sconcumiblesp option:selected').val();

  var tabless ='<tr class="rowventa_'+rowventa+'">\
                <td><input id="addcantidadc" value="'+cant+'" style="background: transparent;border: 0px;margin: 0px;width: 92px;" readonly></td>\
                <td><input type="hidden" id="addcantidadr" value="'+consumi+'" style="">'+consumit+'</td>\
                <td><input id="addcantidadp" value="'+precio+'" class="precio" style="background: transparent;border: 0px;margin: 0px;width: 92px;" readonly></td>\
                <td>\
                  <a class="btn-floating red tooltipped elimina" onclick="deteteven('+rowventa+')"><i class="material-icons">delete_forever</i></a></td>\
              </tr>';
  $('.tabaddconsul').append(tabless);
  rowventa++;
  calculartotal();
}
function addrefa(){
    var cant=$('#cantidadr').val();
    var refaccion=$('#srefaccions option:selected').val();
    var refacciont=$('#srefaccions option:selected').text();
    var precio=$('#srefaccionp option:selected').val();

  var tabless ='<tr class="rowventa_'+rowventa+'">\
                <td><input id="addrefaccionc" value="'+cant+'" style="background: transparent;border: 0px;margin: 0px;width: 92px;" readonly></td>\
                <td><input type="hidden" id="addrefaccionr" value="'+refaccion+'" style="">'+refacciont+'</td>\
                <td><input id="addrefaccionp" value="'+precio+'" class="precio" style="background: transparent;border: 0px;margin: 0px;width: 92px;" readonly></td>\
                <td>\
                  <a class="btn-floating red tooltipped elimina" onclick="deteteven('+rowventa+')"><i class="material-icons">delete_forever</i></a></td>\
              </tr>';
    $('.taaddrefaccion').append(tabless);
    rowventa++;
    calculartotal();
}
function deteteven(row){
  $('.rowventa_'+row).remove();
  calculartotal();
}
function calculartotal(){
    var addtp = 0;
    $(".precio").each(function() {
        var vstotal = $(this).val();
        addtp += Number(vstotal);
    });
    $('.totalg').html(new Intl.NumberFormat('es-MX').format(addtp));
}
function selectprecioa(){
  var precio = $('#saccesorios option:selected').data('costo');
  $('#saccesoriop').val(precio);
}
function addacces(){
    var cant=$('#cantidada').val();
    var refaccion=$('#saccesorios option:selected').val();
    var refacciont=$('#saccesorios option:selected').text();
    var precio=$('#saccesoriop').val();

  var tabless ='<tr class="rowventa_'+rowventa+'">\
                <td><input id="addaccesorioc" value="'+cant+'" style="background: transparent;border: 0px;margin: 0px;width: 92px;" readonly></td>\
                <td><input type="hidden" id="addaccesorior" value="'+refaccion+'" style="">'+refacciont+'</td>\
                <td><input id="addraccesoriop" value="'+precio+'" class="precio" style="background: transparent;border: 0px;margin: 0px;width: 92px;" readonly></td>\
                <td>\
                  <a class="btn-floating red tooltipped elimina" onclick="deteteven('+rowventa+')"><i class="material-icons">delete_forever</i></a></td>\
              </tr>';
    $('.taaddaccesorios').append(tabless);
    rowventa++;
    calculartotal();
}
var roweserv=0;
function addeveservi(){
  var poliza  = $('#tpoliza2 option:selected').val();
  var polizat = $('#tpoliza2 option:selected').text();

  var servicio  = $('#tservicio2 option:selected').val();
  var serviciot = $('#tservicio2 option:selected').text();

  var serie = $('#tserie2').val();

  var direccion = $('#tdireccion2 option:selected').val();
  var iddir = $('#tdireccion2 option:selected').data('iddir');

  var tipo = $('#ttipo22 option:selected').val();
  var tipot = $('#ttipo22 option:selected').text();

          var trtable='<tr class="servicioselected_'+roweserv+'">\
                    <td>\
                      <input type="hidden" name="tpoliza2s" id="tpoliza2s" class="form-control-bmz" value="'+poliza+'">\
                      '+polizat+'\
                    </td>\
                    <td>\
                      <input type="hidden" name="tservicio2s" id="tservicio2s" class="form-control-bmz" value="'+servicio+'">\
                      '+serviciot+'\
                    </td>\
                    <td>\
                      <input type="hidden" name="tserie2s" id="tserie2s" class="form-control-bmz" value="'+serie+'" >\
                      '+serie+'\
                    </td>\
                    <td>\
                      <input type="hidden" name="tdireccion2s" id="tdireccion2s" class="form-control-bmz" value="'+direccion+'" >\
                      <input type="hidden" name="tiddir2s" id="tiddir2s" class="form-control-bmz" value="'+iddir+'" >\
                      '+direccion+'\
                    </td>\
                    <td>\
                      <input type="hidden" name="ttipo2s" id="ttipo2s" class="form-control-bmz" value="'+tipo+'">\
                      '+tipot+'\
                    </td>\
                    <td>\
                      <a onclick="deletesss('+roweserv+')" style="cursor: pointer;"><i class="material-icons" style="color: red;">delete_forever</i></a>\
                    </td>\
                  </tr>';
  if(servicio>0){
      $('.ttableserviciosevento').append(trtable);

  }else{
    toastr["error"]( "Seleccione un tipo de Equipo/servicio","Atencion!");

  }
  roweserv++;
}
function deletesss(id){
  $('.servicioselected_'+id).remove();
}
function alertastockrefaccion(id){
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Aconsumibles/stockrefaccionid",
        data: {id:id},
        success: function (data){
            if (parseInt(data)==0) {
                toastr["error"]("No hay suficiente stock", "Atencion!");
                $(".addrefa" ).prop( "disabled", true );
            }else{
                $(".addrefa" ).prop( "disabled", false );
            }
            
        },
        error: function(data){ 
          toastr["error"]("Atencion!", "Algo salio mal");
        }
    });
}
function alertastockconsumibles(id){
  $.ajax({
        type:'POST',
        url: base_url+"index.php/Aconsumibles/stockconsumiblesid",
        data: {id:id},
        success: function (data){
            if (parseInt(data)==0) {
                toastr["error"]("No hay suficiente stock", "Atencion!");
                $(".addconsu" ).prop( "disabled", true );
            }else{
                $(".addconsu" ).prop( "disabled", false );
            }
            
        },
        error: function(data){ 
            toastr["error"]("Algo salio mal", "Atencion!");
        }
    });
}
function deplegaracciones(id,tipo,cli){
  idcpv=id;
  $('#pcliente').html('<option value="'+cli+'"></option>');
  direccionesclientes(cli);
  $('.acciones_ventas').html('');
  var acciones='';
      acciones+= $('.mostrarsoloclientes').html();
      acciones+='<input type="hidden" id="idventaser" value="'+id+'">';
      acciones+='<input type="hidden" id="idventasertipo" value="'+tipo+'">';
      acciones+='<input type="hidden" id="idventasercliente" value="'+cli+'">';
  $('.acciones_ventas_'+id+'_'+tipo).html(acciones);
  $('.acciones_ventas .generarsolicitudc').attr({'onclick': 'generarsolicitudventas()'});
  $('.acciones_ventas .condiciones').attr({'onclick': 'mostrarinfo('+id+','+tipo+')'});
  verificarventaserivicio(id,tipo);
  obtenerultimosservicios(cli)
}
function generarsolicitudventas(){
  $('body').loading({theme: 'dark',message: 'Generando asignación de servicio...'});
      $('.generarsolicitudc').prop('disabled',true);
      setTimeout(function(){ 
        $('.generarsolicitudc').prop('disabled',false);
      }, 5000);
  //=============================================
      var consumi = [];
      var TABLAc   = $("#tableaddconsulmibles tbody > tr");
        TABLAc.each(function(){  
                itemc = {};
                itemc['cantidad'] = $(this).find("input[id*='addcantidadc']").val();
                itemc['consumible'] = $(this).find("input[id*='addcantidadr']").val();
                itemc['precio'] = $(this).find("input[id*='addcantidadp']").val();
                
                consumi.push(itemc);
        });
        aInfoc   = JSON.stringify(consumi);
      //=============================================
      //=============================================
      var refaccion = [];
      var TABLAc   = $("#tableaddrefacciones tbody > tr");
        TABLAc.each(function(){  
                itemc = {};
                itemc['cantidad'] = $(this).find("input[id*='addrefaccionc']").val();
                itemc['refaccion'] = $(this).find("input[id*='addrefaccionr']").val();
                itemc['precio'] = $(this).find("input[id*='addrefaccionp']").val();
                
                refaccion.push(itemc);
        });
        aInfor   = JSON.stringify(refaccion);
      //=============================================
      //=============================================
      var raccesorio = [];
      var TABLAa   = $("#tableaddaccesorios tbody > tr");
        TABLAa.each(function(){  
                itema = {};
                itema['cantidad'] = $(this).find("input[id*='addaccesorioc']").val();
                itema['accesorio'] = $(this).find("input[id*='addaccesorior']").val();
                itema['precio'] = $(this).find("input[id*='addraccesoriop']").val();
                
                raccesorio.push(itema);
        });
        aInfoa   = JSON.stringify(raccesorio);
      //=============================================
      //=============================================
      var servicios = [];
      var TABLAss   = $("#tableserviciosevento tbody > tr");
        TABLAss.each(function(){  
                itemcs = {};
                itemcs['poliza'] = $(this).find("input[id*='tpoliza2s']").val();
                itemcs['equipos'] = $(this).find("input[id*='tservicio2s']").val();
                itemcs['serie'] = $(this).find("input[id*='tserie2s']").val();
                itemcs['direccion'] = $(this).find("input[id*='tdireccion2s']").val();
                itemcs['tipo'] = $(this).find("input[id*='ttipo2s']").val();
                
                servicios.push(itemcs);
        });
        aInfors   = JSON.stringify(servicios);
      //=============================================
      //=============================================
        var servicios = [];
        var TABLAs   = $("#table_servicios_ult tbody > tr");
          TABLAs.each(function(){  
              if ($(this).find("input[class*='servicios_ckeck']").is(':checked')) {
                  item = {};
                  item['tipo'] = $(this).find("input[class*='servicios_ckeck']").data('sertipo');
                  item['servicio'] = $(this).find("input[class*='servicios_ckeck']").data('ser');
                  servicios.push(item);
              }
          });
          aInfoser   = JSON.stringify(servicios);
      //=============================================


  var tasign = $('input[name=tasign]:checked').val();//1pre asignacion 2 asginacion
    //if(TABLAss.length>0){
      var restringido_save=1;
      if($('#fecha').val()==''){
        restringido_save=0;
        toastr["warning"]("Ingrese una fecha", "Advertencia")
        $('body').loading('stop');
      }
      if($('#hora').val()==''){
        restringido_save=0;
        toastr["warning"]("Ingrese una Hora Inicio", "Advertencia")
        $('body').loading('stop');
      }
      if($('#horafin').val()==''){
        restringido_save=0;
        toastr["warning"]("Ingrese una Hora fin", "Advertencia")
        $('body').loading('stop');
      }
      if(restringido_save==1){
        $.ajax({
              type:'POST',
              url: base_url+'index.php/Asignaciones/get_savesventa',
              data: {
                tasign:tasign,
                serviciorow:$('#id_servicio_f').val(),
                ventaId: $('#idventaser').val(),
                tipov:$('#idventasertipo').val(),
                cliente:$('#idventasercliente').val(),
                fecha:$('#fecha').val(),
                hora:$('#hora').val(),
                horafin:$('#horafin').val(),
                hora_comida:$('#hora_comida').val(),
                horafin_comida:$('#horafin_comida').val(),
                zona:$('#ruta option:selected').val(),
                priodidad:$('input[name=priodidad]:checked').val(),
                prioridad2:$('#prioridad2 option:selected').val(),
                hora_cri_ser:$('#hora_cri_ser').val(),
                //tservicio:$('#tservicio option:selected').val(),
                tserviciomotivo:$('#tserviciomotivo2').val(),
                //tpoliza: $('#tpoliza option:selected').val(),
                //ttipo: $('input[name=ttipo]:checked').val(),
                tecnico:$('#tecnico option:selected').val(),
                documentaccesorio: $('#documentaccesorio2').val(),
                vconsumible:aInfoc,
                vrefaccion:aInfor,
                datoservicio:aInfors,
                vaccesorio:aInfoa,
                servicios:aInfoser
                },
              success:function(data){
                  $.alert({boxWidth: '30%',useBootstrap: false,
                    title: 'Atención!',content: 'Servicios Generados'});
                  //direccionar
                  setTimeout(function(){ 
                      window.location.href = base_url+"index.php/Asignaciones"; 
                      //window.location.reload();
                  }, 2000);
              }
          });
        //}else{
        //  toastr["error"]("Faltan por completar tipo de servicio", "Atencion!");
        //}
      }
  
}

function direccionesclientes(id){
  $.ajax({
        type:'POST',
        url: base_url+"Asignaciones/datoscliente",
        data: {
          id:id
        },
        success: function (data){
          console.log(data);
          var array = $.parseJSON(data);
          array.direccion.forEach(function(element) {
                $('#tdireccion2').append('<option data-iddir="'+element.idclientedirecc+'">'+element.direccion+'</option>');
                $('#tdireccion3').append('<option>'+element.direccion+'</option>');
          });
        }
    }); 
}
var ventaservicioid=0;
function mostrarinfo(idventa,tipo){
  ventaservicioid=idventa;
  $.ajax({
            type:'POST',
            url: base_url+'Asignaciones/informacionservicioventa',
            data: {
                idventa: idventa
            },
            async: false,
            statusCode:{
                404: function(data){
                    swal("Error", "404", "error");
                },
                500: function(){
                    swal("Error", "500", "error"); 
                }
            },
            success:function(data){
                $('.infoventaiframe').html('');

                var array = $.parseJSON(data);
                console.log(array);
                $.each(array, function(index, item) {
                  var dataos='<div class="row">\
                                <div class="col m3">\
                                  Poliza: '+item.poliza+'\
                                </div>\
                                <div class="col m3">\
                                    Servicio: '+item.servicio+'\
                                </div>\
                                <div class="col m3">\
                                    Direccion: '+item.direccon+'\
                                </div>\
                                <div class="col m3">\
                                    Motivo: '+item.motivo+'\
                                </div>\
                            </div>';

                  $('.infoventaiframe').append(dataos);
                });
                console.log(array.length);
                if(array.length>0){
                  $('#modalinfoventa').modal('open');
                }else{
                  $('#modamodificarservicio').modal('open');
                  obtenerservicios3();
                }
            }
        });
}
toastr.options = {
  "closeButton": true,
  "debug": false,
  "newestOnTop": false,
  "progressBar": true,
  "positionClass": "toast-top-right",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "2000",
  "timeOut": "8000",
  "extendedTimeOut": "8000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}
function ventaserviciook(){
  $('.condiciones_servicio').removeClass('red').addClass('green');
  setTimeout(function(){ 
    $('.condiciones_servicio').removeClass('green').addClass('red');
  }, 90000);
  $.ajax({
    type:'POST',
    url: base_url+'Ventas/addseriviciovd',
    data: {
        idventa: ventaservicioid,
        poliza: $('#tpoliza3 option:selected').val(),
        servicio: $('#tservicio3 option:selected').val(),
        direccion: $('#tdireccion3 option:selected').val(),
        tipo: $('#ttipo33 option:selected').val(),
        motivo: $('#tserviciomotivo3').val()
        },
        async: false,
        statusCode:{
            404: function(data){
                swal("Error", "404", "error");
            },
            500: function(){
                swal("Error", "500", "error"); 
            }
        },
        success:function(data){
            $('#modamodificarservicio').modal('close');

        }
    });
}
function addserieshisto(row){
    $('#modalserieshistorial').modal('open');
    $.ajax({
        type:'POST',
        url: base_url+'index.php/Ventas/cliente_equipo_historial_group',
        data: {
            idcliente:$('#pcliente option:selected').val()
        },
        success:function(data){
            var array = $.parseJSON(data);
            var html_table='';
            var infocliente=$('#cliente').val();
            html_table+='<table class="table" id="table-h-e-s">\
                  <thead>\
                    <tr>\
                      <th>Modelo</th>\
                      <th>Serie</th>\
                      <th></th>\
                      <th></th>\
                      <th></th>\
                      <th></th>\
                      <th></th>\
                    </tr>\
                  </thead>\
                <tbody>';
                var seriehiqrow=0;
                array.forEach(function(element) {

                    var contrato='';
                    if(element.tipo==2){
                        //obtenerdireccionescontrato(element.equipoid,element.serieId);
                        var contrato='Contrato: '+element.idcontrato;

                        var classdireccion='contratosdireccion equipo_'+element.equipoid+'_serie_'+element.serieId;
                        var classdirecciondata=' data-equipo="'+element.equipoid+'" data-serie="'+element.serieId+'" ' ;
                        var classubicacion='u_equipo_'+element.equipoid+'_serie_'+element.serieId;
                    }else{
                        var classdireccion='';
                        var classubicacion='';
                        var classdirecciondata='';
                    }
                    html_table+='<tr>\
                                  <td style="font-size:12px;">'+element.modelot+'</td>\
                                  <td style="font-size:12px;">'+element.serie+'</td>\
                                  <td style="font-size:12px;">'+contrato+'</td>\
                                  <td style="font-size:12px;" class="'+classdireccion+'" '+classdirecciondata+'></td>\
                                  <td style="font-size:12px;" class="'+classubicacion+'"></td>\
                                  <td>\
                                    <a class="btn-bmz dataseries_historico_'+seriehiqrow+'" onclick="m_s_historico('+seriehiqrow+','+row+')" \
                                        data-modelo="'+element.modelot+'" \
                                        data-modeloid="'+element.modelo+'" \
                                        data-serie="'+element.serie+'" \
                                        data-tipo="'+element.tipo+'" \
                                        data-id="'+element.id+'" \
                                    >Agregar</a>\
                                  </td>\
                                  <td>';
                                    if(element.tipo>0){
                                        html_table+='<a class="btn-floating blue tooltipped " onclick="m_s_historico_pre('+seriehiqrow+')" ><i class="material-icons">assignment</i></a>';
                                    }
                        html_table+='</td>\
                                </tr>';
                    seriehiqrow++;
                });
                html_table+='</tbody>\
                </table>';  

            $('.table-h-e-s').html(html_table);
            $('#table-h-e-s').DataTable({
                order: [
                        [2, "desc"]
                        ]
            }).on('draw',function(){
                 //cargardireccionesubicaciones();
            });;
            //cargardireccionesubicaciones();

        }
    });
    
}
function m_s_historico(seriehiqrow,row) {
    var modelo  = $('.dataseries_historico_'+seriehiqrow).data('modelo');
    var modeloid= $('.dataseries_historico_'+seriehiqrow).data('modeloid');
    var serie   = $('.dataseries_historico_'+seriehiqrow).data('serie');
    if(modeloid==113){
        $('#sinmodelo_'+row).prop('checked',true);
    }else{
        $('#sinmodelo_'+row).prop('checked',false);
    }
    //$('#tservicio2').val(modeloid).trigger("chosen:updated")
    $('#tservicio2 > .modelo_poliza_'+modeloid).attr("selected", true).trigger("chosen:updated")
    $('#tserie2').val(serie);


    
    $('#modalserieshistorial').modal('close');
}
function consultarfechasagenda(idequipo){
    var idcontrato = $('#pcontrato option:selected').val();
    $('#modal_view_agenda').modal('open');
    $.ajax({
        type:'POST',
        url: base_url+'index.php/Asignaciones/fechasagenda',
        data: {
            idcontrato:idcontrato,
            idequipo:idequipo

        },
        success:function(data){
            console.log(data);
            $('.view_agenda_html').html(data);
            
            $('#table-agenda').DataTable({
                order: [
                        [0, "asc"]
                        ]
            }).on('draw',function(){
              //alguna funcion
            });;
            

        }
    });
}
function bloqueov(){
    swal({
      title: "Bloqueado",
      text: "Cliente bloqueado, contacte a administración.",
      type: "warning",
      showCancelButton: false,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "OK",
      closeOnConfirm: true
    },
    function(){

    });
}
function verificarservicios(serieid){
  var idcontrato = $('#pcontrato option:selected').val();
  var chekedserie=$('#serie_check_'+serieid).is(':checked');
  var ideq=$('#serie_check_'+serieid).data('ideq');
  var serie=$('#serie_check_'+serieid).data('serie');
  if(chekedserie){
    $.ajax({
        type:'POST',
        url: base_url+'index.php/Asignaciones/verificarservicios',
        data: {
            idcontrato:idcontrato,
            serie:serieid,
            ideq:ideq

        },
        success:function(data){
          console.log(data);
            var array = $.parseJSON(data);
            if(array.pasaser==0){
              /*
              $.alert({boxWidth: '30%', useBootstrap: false,title: 'Alerta!',
                        content: 'Proximo servicio sera igual o despues de '+array.fechanext+', ya que sus servicios son cada '+array.speriocidad+' Mes'});
              */
              toastr["warning"]('Próximo servicio será igual o después de '+array.fechanext+', ya que sus servicios son cada '+array.speriocidad+' Mes');
            }
            if(array.consu_alert==1){
              $.alert({boxWidth: '30%',useBootstrap: false,
                        title: 'Surtimiento!',content: 'El equipo de la serie '+serie+' se le deberá de resurtir '+array.consu_cant+' consumibles'});
            }
            

        }
    });
    var serie =$('#serie_check_'+serieid).data('serie');
    var ren =$('#serie_check_'+serieid).data('ren');
    v_ulimos_ser(serieid,serie,1,ren);
  }
}
function verificarventaserivicio(idventa,tipo){
  $.ajax({
        type:'POST',
        url: base_url+'index.php/Asignaciones/verificarventaserivicio',
        data: {
            idventa:idventa,
            tipo:tipo

        },
        success:function(data){
          var pasa = parseInt(data);
          console.log('vs:'+pasa);
          if(pasa==0){
            $.alert({boxWidth: '30%',useBootstrap: false,
                    title: 'Alerta!',content: 'hay un servicio de una serie de la venta en el mes actual'});
          }
          
            

        }
    });
}
function notificacionsercot(){
    setTimeout(function(){
        $('.notificacionsercot').click(function(event) {
            var asignacionide = $(this).data('asignacionide');
            var asignacionid = $(this).data('asignacionid');
            var cotrefaccion = $(this).data('cotrefaccion');
            var cotrefaccion2 = $(this).data('cotrefaccion2');
            var cotrefaccion3 = $(this).data('cotrefaccion3');
            var tiposer = $(this).data('tiposer');
            console.log(asignacionide+'_'+asignacionid+'_'+cotrefaccion+'_'+cotrefaccion2+'_'+cotrefaccion3+'_'+tiposer);
            var html='';
                if(cotrefaccion!=''){
                    html+='<div class="row"><div class="col s12"><b>POR DESGASTE</b></div><div class="col s12">'+cotrefaccion+'</div><div>';
                }
                if(cotrefaccion2!=''){
                    html+='<div class="row"><div class="col s12"><b>POR DAÑO</b></div><div class="col s12">'+cotrefaccion2+'</div><div>';
                }
                if(cotrefaccion3!=''){
                    html+='<div class="row"><div class="col s12"><b>POR GARANTIA</b></div><div class="col s12">'+cotrefaccion3+'</div><div>';
                }
            $.confirm({
                boxWidth: '30%',
                useBootstrap: false,
                icon: 'fa fa-warning',
                title: 'Cotizacion pendiente',
                content: html,
                type: 'red',
                typeAnimated: true,
                buttons:{
                    'Ir a la solicitud': function (){
                        
                        var urldir=base_url+'index.php/Solicitud_Refacciones?tiposer='+tiposer+'&asignacionide='+asignacionide+'&asignacionid='+asignacionid
                        window.open(urldir, '_blank');
                        
                    },
                    cancelar: function () {
                        
                    }
                }
            });
        });
    }, 1000);
}
function coun_tec(){
  var coun_tec = $('#coun_tec option:selected').val();
  if(coun_tec==1){
    $('.coun_tec_1').show('show');
    $('.coun_tec_2').hide('show');
    $('.coun_tec_3').hide('show');
    $('.coun_tec_4').hide('show');
  }
  if(coun_tec==2){
    $('.coun_tec_1').show('show');
    $('.coun_tec_2').show('show');
    $('.coun_tec_3').hide('show');
    $('.coun_tec_4').hide('show');
  }
  if(coun_tec==3){
    $('.coun_tec_1').show('show');
    $('.coun_tec_2').show('show');
    $('.coun_tec_3').show('show');
    $('.coun_tec_4').hide('show');
  }
  if(coun_tec==4){
    $('.coun_tec_1').show('show');
    $('.coun_tec_2').show('show');
    $('.coun_tec_3').show('show');
    $('.coun_tec_4').show('show');
  }
}
function limitedeservicios(){
  $.alert({boxWidth: '30%',useBootstrap: false,
          title: 'Alerta!',content: 'se alcanzo el limite de servicios para la póliza'});
}
function obtenerventaspendientes(idcliente,blq_perm_ven_ser){
    //restriccion para cuando tenga mas de 3 prefacturas pendientes de agregar pago
    $.ajax({
        type:'POST',
        url: base_url+'index.php/Reportes/rventasnum/'+idcliente,
        success:function(data){
            console.log(data);
            var numventas=parseInt(data);
            //=======================
                if(blq_perm_ven_ser==1){
                    var html_contect='El cliente tiene '+numventas+' pendientes de pago!';
                }else{
                    var html_contect='El cliente tiene '+numventas+' pendientes de pago!<br> Para continuar se necesita permisos de administrador<br>'+
                                         '<input type="text" placeholder="Contraseña" id="contrasena" name="contrasena" class="name form-control"  onpaste="return false;" required/>';
                }
            //=======================
            if(numventas>0){
                $.confirm({
                    boxWidth: '30%',
                    useBootstrap: false,
                    icon: 'fa fa-warning',
                    title: 'Atención!',
                    content: html_contect,
                    type: 'red',
                    typeAnimated: true,
                    buttons:{
                        confirmar: function (){
                            if(blq_perm_ven_ser==1){

                            }else{
                              //var pass=$('#contrasena').val();
                              var pass=$("input[name=contrasena]").val();
                              if (pass!='') {
                                   $.ajax({
                                      type:'POST',
                                      url: base_url+"index.php/Sistema/solicitarpermiso/"+idcliente,
                                      data: {
                                          pass:pass
                                      },
                                      success: function (response){
                                              var respuesta = parseInt(response);
                                              if (respuesta==1) {
                                              }else{
                                                  notificacion(2);
                                                  setTimeout(function(){
                                                      location.reload();
                                                  },1000);
                                              }
                                      },
                                      error: function(response){
                                          notificacion(1);
                                          setTimeout(function(){
                                                      location.reload();
                                          },2000);
                                      }
                                  });
                                  
                              }else{
                                  notificacion(0);
                                  setTimeout(function(){
                                                      location.reload();
                                  },2000);
                              }
                            }
                        },
                        'Ver Pendientes': function (){
                            obtenerventaspendientesinfo(idcliente);
                        },
                        cancelar: function (){
                          if(blq_perm_ven_ser==1){}else{
                            setTimeout(function(){
                                location.reload();
                            },1000);
                          }
                        }
                    }
                });
                setTimeout(function(){
                    new MaskedPassword(document.getElementById("contrasena"), '\u25CF');
                },1000);
            }
        }
    });
}
/*
function passinput(){
    $("#contrasena").change();
    var pass=$('#contrasena').val();
    if(pass==''){
        $('#contrasena').data('val','');
    }
}
*/
function filtro_dir(){
  var dir =$('#filtro_dir option:selected').val();
  if(dir=='xxx'){
    $('.dir_row').show('show');
    $('.serie_check_selected').addClass('serie_check');
  }else{
    $('.dir_row').hide();
    $('.serie_check_selected').removeClass('serie_check');
    $('.'+dir).show('show');
    $('.'+dir+' .serie_check_selected').addClass('serie_check');
  }

}
function obtenerultimosservicios(cli){

  cli_aux=cli;
  $.ajax({
        type:'POST',
        url: base_url+"index.php/Asignaciones/obtenerultimosservicios/"+cli+"/"+0,
        data: {
            cli:cli,
            tipo:0
        },
        success: function (response){
            var cant=parseInt(response);
            console.log(cant);
            $('#cant_ult_ser').val(cant);
            var tasign=$('input:radio[name=prasignaciontipo]:checked').val();
            if(cant>0){
              $('.ultimosservicios').show('show'); //asta que se complete se pasa a productivo
              //$('.ultimosservicios').hide('show');
            }else{
              $('.ultimosservicios').hide('show');
            }
        },
        error: function(response){
            notificacion(1);
            
        }
    });
}
function ultimosservicios(){
  if(cli_aux>0){
    $('#modalserviciosanteriores').modal('open');
    var tasign=$('input:radio[name=prasignaciontipo]:checked').val();
    if(tasign==1){//renta
      idcpv=$('#pcontrato option:selected').data('idrenta');
    }
    if(tasign==2){//poliza
      
    }
    if(tasign==3){// cliente
      idcpv=0;
    }
    if(tasign==4){// ventas
      
    }
    console.log('tasign:'+tasign+' idcpv:'+idcpv );
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Asignaciones/obtenerultimosservicios/"+cli_aux+"/"+1,
        data: {
            x:0,
            tasign:tasign,
            idcpv:idcpv
        },
        success: function (response){
          console.log(response);
            $('.view_servicios_10').html(response);
            filtrarseries();
        },
        error: function(response){
            notificacion(1);
            
        }
    });
  }

}
function v_ulimos_ser(id,serie,tipo){
  if(serie!=''){
    if(cli_aux>0){
      $.ajax({
          type:'POST',
          url: base_url+"index.php/Asignaciones/obtenerultimosservicios/"+cli_aux+"/"+0+"/"+serie,
          data: {
              cli:cli_aux,
              tipo:0
          },
          success: function (response){
              var cant=parseInt(response);
              console.log(cant);
              if(cant>0){
                //===================================================
                  var html_contect='La serie tiene un servicio en los ultimos dias<br> Para continuar se necesita permisos de administrador<br>'+
                                           '<input type="text" placeholder="Contraseña" id="contrasena2" name="contrasena2" class="name form-control-bmz" style="width:99%"  onpaste="return false;" required/>';
                      $.confirm({
                          boxWidth: '30%',
                          useBootstrap: false,
                          icon: 'fa fa-warning',
                          title: 'Atención!',
                          content: html_contect,
                          type: 'red',
                          typeAnimated: true,
                          buttons:{
                              'Seleccionar a garantia': function (){
                                ultimosservicios();
                              },
                              confirmar: function (){
                                  if(blq_perm_ven_ser==1){

                                  }else{
                                      //var pass=$('#contrasena').val();
                                      var pass=$("input[name=contrasena2]").val();
                                      if (pass!='') {
                                           $.ajax({
                                              type:'POST',
                                              url: base_url+"index.php/Sistema/solicitarpermiso",
                                              data: {
                                                  pass:pass
                                              },
                                              success: function (response){
                                                      var respuesta = parseInt(response);
                                                      if (respuesta==1) {
                                                      }else{
                                                          
                                                          //=============================
                                                            checked_ser_ult(id,serie,tipo);
                                                          //==================================
                                                      }
                                              },
                                              error: function(response){
                                                  notificacion(1);
                                              }
                                          });
                                          
                                      }else{
                                          notificacion(0);
                                          //=============================
                                            checked_ser_ult(id,serie,tipo);
                                          //==================================
                                      }
                                  }
                                  
                              },
                              cancelar: function (){
                                  //=============================
                                    checked_ser_ult(id,serie,tipo);
                                  //==================================
                              }
                          }
                      });
                      setTimeout(function(){
                          new MaskedPassword(document.getElementById("contrasena2"), '\u25CF');
                      },1000);
                //===================================================
              }
          },
          error: function(response){
              notificacion(1);
          }
      });
    }
  }
}
function checked_ser_ult(id,serie,tipo){
  if(tipo==1){
    $('#serie_check_'+id).prop('checked',false);
  }
  if(tipo==2){
    $('#serie_check_'+id).prop('checked',false);
  }
  if(tipo==3){

  }
}
function link_cot(id,cli){
  var url_cot =base_url+'index.php/Cotizaciones/listaCotizacionesPorCliente/'+cli+'?cot='+id;
  window.open(url_cot, '_blank');
}
function filtrarseries(){
  var fil=$('#montrar_s_2').is(':checked');
  if(fil){
    var tasign=$('input:radio[name=prasignaciontipo]:checked').val();
    var series = $('.seriesagregadas').data('series');
    if(tasign==3){}else{
      //const cadena = "VR99Y70645,VR99Y70646,VR99Y70647,VR99Y70648,VR99Y706989";
      const elementos = series.split(',');
      if(elementos.length>0){
        $('.view_series').hide();
        for (let i = 0; i < elementos.length; i++) {
          var classv='.view_series.'+elementos[i];
          console.log(classv);
          $(classv).show('show');
          //console.log(elementos[i]);
        }
      }
      
    }
  }else{
    $('.view_series').show('show');
  }
  
}
function desbloquearservicios(){
  var html_contect='Se necesita permisos de administrador<br>'+
                                           '<input type="text" placeholder="Contraseña" id="contrasena2" name="contrasena2" class="name form-control-bmz" style="width:99%"  onpaste="return false;" required/>';
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: html_contect,
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                
                    //var pass=$('#contrasena').val();
                    var pass=$("input[name=contrasena2]").val();
                    if (pass!='') {
                         $.ajax({
                            type:'POST',
                            url: base_url+"index.php/Sistema/solicitarpermiso",
                            data: {
                                pass:pass
                            },
                            success: function (response){
                                    var respuesta = parseInt(response);
                                    if (respuesta==1) {
                                      //=============================
                                          $('.servicios_ckeck.disabled').prop({"disabled": false, "onclick": ""});
                                          $('.servicios_ckeck_label.disabled').removeAttr( "onclick" );
                                        //==================================
                                    }else{
                                        notificacion(2);

                                    }
                            },
                            error: function(response){
                                notificacion(1);
                            }
                        });
                        
                    }else{
                        notificacion(0);
                        //=============================
                        //==================================
                    }
                
                
            },
            cancelar: function (){
                //=============================
                //==================================
            }
        }
    });
    setTimeout(function(){
        new MaskedPassword(document.getElementById("contrasena2"), '\u25CF');
    },1000);
}
function v_ulimos_ser_all(){
  var equipos = [];
  var TABLA   = $("#tableequipos tbody > tr");
  if (TABLA.length>0) {
    TABLA.each(function(){  
        if ($(this).find("input[class*='serie_check']").is(':checked')) {
          var serie_l_check =$(this).find("input[class*='serie_check']").data('serie');
          var serie_l_val =$(this).find("input[class*='serie_check']").val();
              v_ulimos_ser_all_op(serie_l_check,serie_l_val);
        }
    });
  }
  setTimeout(function(){
    if($('.reverificarpermiso').length>0){
      
        var checked=$('#serie_check_0').is(':checked');
        if(checked){
          var html_contect='Las series tiene un servicio en los últimos dias<br> Para continuar se necesita permisos de administrador<br>'+
                  '<input type="text" placeholder="Contraseña" id="contrasena2" name="contrasena2" class="name form-control-bmz" style="width:99%"  onpaste="return false;" required/>';
            $.confirm({
                boxWidth: '30%',
                useBootstrap: false,
                icon: 'fa fa-warning',
                title: 'Atención!',
                content: html_contect,
                type: 'red',
                typeAnimated: true,
                buttons:{
                    confirmar: function (){
                        //if(blq_perm_ven_ser==1){

                        //}else{
                            //var pass=$('#contrasena').val();
                            var pass=$("input[name=contrasena2]").val();
                            if (pass!='') {
                                 $.ajax({
                                    type:'POST',
                                    url: base_url+"index.php/Sistema/solicitarpermiso",
                                    data: {
                                        pass:pass
                                    },
                                    success: function (response){
                                            var respuesta = parseInt(response);
                                            if (respuesta==1) {
                                              //=============================
                                                  $('.reverificarpermiso').prop('checked',true);
                                                //==================================
                                            }else{
                                                notificacion(2);
                                                
                                            }
                                    },
                                    error: function(response){
                                        notificacion(1);
                                    }
                                });
                                
                            }else{
                                notificacion(0);
                                toastr.error('favor de ingresar una contraseña');
                            }
                        //}
                        
                    },
                    cancelar: function (){
                        
                    }
                }
            });
            setTimeout(function(){
                new MaskedPassword(document.getElementById("contrasena2"), '\u25CF');
            },1000);
        }
      
    }
  },1000);

}
function v_ulimos_ser_all_op(serie,serieid){
  if(serie!=''){
    if(cli_aux>0){
      $.ajax({
          type:'POST',
          url: base_url+"index.php/Asignaciones/obtenerultimosservicios/"+cli_aux+"/"+0+"/"+serie,
          data: {
              cli:cli_aux,
              tipo:0
          },
          success: function (response){
              var cant=parseInt(response);
              console.log(cant);
              if(cant>0){
                $('#serie_check_'+serieid).prop('checked',false).addClass('reverificarpermiso');
              }
              console.log('entra');
          },
          error: function(response){
              notificacion(1);
          }
      });
    }
  }
}