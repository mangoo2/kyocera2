<style type="text/css">
  .serie_instalada ,
  .serie_retirada,
  .fecha_tramite,
  .fecha_envio{    width: 120px !important;  }
  .folio_rma_rechazo{    display: none;  }
  .folio_rma_text[readonly] {    background-color: #fa4545 !important;  }
  .folio_rma_text[readonly]::placeholder {    color: white;  }
  .folio_rma_text::placeholder{    color: transparent;    text-align: center;  }
  #tableg td{    font-size: 12px;  }
  .bloqueado .folio_rma_text[readonly] {    background-color: #e9ecef !important;  }
  .btn_list{
    width: 100px;
  }
  .conred{
    font-weight: bold;
    color: red;
  }
</style>
<!-- START CONTENT -->
    <section id="content">
      <!--start container-->
        <div class="container">
          <div class="section">

          <input id="base_url" type="hidden" name="base_url" value="<?php echo base_url(); ?>">

          <div id="table-datatables">
                <h4 class="header">Lista de Solicitud de consumibles</h4>
                <div class="row">
                  <div class="col s12">
                    <div class="card-panel">
                      <div class="row">
                        <div class="col s4 m2 l2">
                          <select id="sol_con" class="form-control-bmz browser-default" onchange="loadtable()">
                            <option value="0">Pendiente</option>
                            <option value="1">Solicitados</option>
                          </select>
                        </div>
                        <div class="col s8 m10 l10" style="text-align:right;">
                          <a class="waves-effect green btn-bmz" title="Registrar pagos no reconocidos" onclick="generarfolio()">Generar folios</a>
                        </div>
                        <div class="col s12">
                          <table class="table striped" id="tableg">
                            <thead>
                              <tr>
                                <th>#</th><th>Contrato</th><th>Equipo</th><th>Serie</th><th>Consumible solicitada</th><th></th><th>Cantidad</th>
                                <th></th>
                                <th></th>
                              </tr>
                            </thead>
                            
                          </table>
                        </div>
                      </div>
                    </div>
                  </div> 
                      
                </div>
                   
              </div>
            </div>  
               </div>
        </section>



<div id="modalinfosol" class="modal" >
  <div class="modal-content" style="padding: 5px">
    <div class="row">
      <div class="col s12">
        <h4>Solicitud</h4>
      </div>
    </div>
    <div class="row htmlinfosol">
      
      
    </div>
    
  </div>  
  <div class="modal-footer">
    <a class="modal-action modal-close waves-effect waves-red gray btn-flat ssss ">Cerrar</a>
  </div>
</div>

























<div id="modalgenerafolio" class="modal" >
  <div class="modal-content" style="padding: 5px">
    <div class="row">
      <div class="col s12">
        <h4>Seleccion</h4>
      </div>
    </div>
    <div class="row">
      <div class="col s3">
        <label>Seleccion de Bodega</label>
        <select id="nbodega" class="browser-default form-control-bmz" onchange="infostockconsumibles()"><option value="0">Seleccion</option>
          <?php foreach ($get_bodegas as $item) { 
            if($item->bodegaId!=9 and $item->bodegaId!=10 and $item->bodegaId!=11){
              echo '<option value="'.$item->bodegaId.'">'.$item->bodega.'</option>';
            }
          } ?>
        </select> 
      </div>
      <div class="col s12">
        <table class="table display striped " id="table_selec_con">
          <thead>
            <tr>
              <th>Modelo</th>
              <th></th>
              <th></th>
            </tr>
          </thead>
          <tbody class="tbody_selec_con"></tbody>
          
        </table>
      </div>
      <div class="col s10"></div>
      <div class="col s2">
        <a class="waves-effect green btn-bmz" title="Registrar pagos no reconocidos" onclick="generarf()">Generar</a>
      </div>
      
    </div>
    
  </div>  
  <div class="modal-footer">
    <a class="modal-action modal-close waves-effect waves-red gray btn-flat ssss ">Cerrar</a>
  </div>
</div>