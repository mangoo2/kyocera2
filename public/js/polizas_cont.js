var base_url = $('#base_url').val();
var idPoliza = $('#idPoliza').val();
var idPoliza_dll=0;
$(document).ready(function($) {
    //$('#table_pol_ser_vinc').DataTable();
    $('#table_pol_ser_vinc').DataTable({
                "order": [[ 3, "desc" ]]
            });
	$('.modal').modal({dismissible: false});
    $('#s_direccion_s').select2();
    $('#select_eq_edit').select2();
});
function actualizarfechae(idventa){
    var fechaentrega = $('#fechaentrega').val();
    var fecha=fechaentrega;
    var fecha=fecha.split("-");
    if(fecha[0]>2021){
        $.confirm({
            boxWidth: '40%',
            useBootstrap: false,
            icon: 'fa fa-warning',
            title: 'Atención!',
            content: '¿Confirma la edición de la fecha de Inicio a <b>'+fechaentrega+'</b>?',
            type: 'red',
            typeAnimated: true,
            buttons:{
                confirmar: function (){
                    //===================================================
                        $.ajax({
                            type:'POST',
                            url: base_url+"index.php/Generales/editarfechaentrega",
                            data: {
                                ventaid:idventa,
                                fentrega:fechaentrega,
                                tipo:5
                            },
                            success:function(response){  
                                swal("Éxito!", "Se ha Modificado", "success");
                                setTimeout(function(){ 
                                    location.reload();
                                }, 1000);
                            }
                        });
                        
                    //================================================
                     
                },
                cancelar: function (){
                    
                }
            }
        });
    }
}
function addserex(idPoliza,idcliente){
    $('#montrar_s_1').prop('checked',false);
	var table_html = '<table class="table" id="table_pol_ser_ext">\
        <thead>\
          <tr><th>#</th><th>Fecha</th><th>Hora</th><th>Servicio</th><th></th><th></th></tr>\
        </thead>\
        <tbody class="table_pol_ser_ext"></tbody>\
      </table>';
	$('.row_table_pol_ser_ext').html(table_html);
	/*
	$.confirm({
            boxWidth: '40%',
            useBootstrap: false,
            icon: 'fa fa-warning',
            title: 'Atención!',
            content: '¿Confirma de agregar servicios existente?',
            type: 'red',
            typeAnimated: true,
            buttons:{
                confirmar: function (){
                	*/
                    //===================================================
                    	$('.table_pol_ser_ext').html('');
                        $('#modal_add_ser_ex').modal('open');
                            $.ajax({
                                type:'POST',
                                url: base_url+"index.php/PolizasCreadas/addserex",
                                data: {
                                    idcliente:idcliente,
                                    idPoliza:idPoliza
                                },
                                success:function(data){  
                                    console.log(data);
							        var array = $.parseJSON(data);
							        //var datos=array.datos;          
							        array.forEach(function(element) {
							        	var btn = '<a onclick="addserex_add('+element.id+','+element.tipo+')" class="b-btn b-btn-primary" title="Agregar un servicio existente">Agregar Servicio</a>';
							        	var html='<tr class="series_selecte '+element.seriesgroup+'"><td>'+element.id+'</td><td>'+element.fecha+'</td><td>'+element.hora+'</td><td>'+element.servicio+'</td><td>'+element.series+'</td><td>'+btn+'</td></tr>';
							        	$('.table_pol_ser_ext').append(html);
							        });
							        $('#table_pol_ser_ext').DataTable({
                                        "lengthMenu": [[50,60, 70, 100], [50,60, 70,100]],
                                    });
                                    filtrarservicios1();
                                }
                            });
                        
                    //================================================
                    /* 
                },
                cancelar: function () 
                {
                    
                }
            }
        });
        */
}
function filtrarservicios1(){
    var sele = $('#montrar_s_1').is(':checked')==true?1:0;//0 solo series 1 todo
    if(sele==0){
        $('.series_selecte').hide();
        var productos = $(".table_equipos tbody > tr");
        productos.each(function(){                           
            var serie_row=$(this).find("input[id*='serie_row']").val(); 
            $('.series_selecte.'+serie_row).show();
        });
    }else{
        $('.series_selecte').show();
    }
}
function filtrarservicios2(){
    var sele = $('#montrar_s_2').is(':checked')==true?1:0;//0 solo series 1 todo
    if(sele==0){
        $('.series_selecte').hide();
        var productos = $(".table_equipos tbody > tr");
        productos.each(function(){                           
            var serie_row=$(this).find("input[id*='serie_row']").val(); 
            $('.series_selecte.'+serie_row).show();
        });
    }else{
        $('.series_selecte').show();
    }
}
function addserex_add(idasig,tipo){
	$.confirm({
        boxWidth: '40%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Confirma de agregar el servicio a la poliza?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                //===================================================
                    $.ajax({
                        type:'POST',
                        url: base_url+"index.php/PolizasCreadas/addserex_add",
                        data: {
                            idasig:idasig,
                            tipo:tipo,
                            idPoliza:idPoliza
                        },
                        statusCode:{
		                    404: function(data){
		                        notificacion(9);
		                    },
		                    500: function(){
		                        notificacion(10);
		                    }
		                },
                        success:function(data){  
                            toastr.success('Se Agrego Correctamente','Hecho!');
                            setTimeout(function(){ 
						        location.href =base_url+'PolizasCreadas/servicios/'+idPoliza+'?pes=2';
						    }, 1000);

                        }
                    });
                //================================================
            },
            cancelar: function (){
                
            }
        }
    });
}
function editrestantes(poliza,vigencia,realizados){
	var maxres=parseInt(vigencia)-parseInt(realizados);
	$.confirm({
        boxWidth: '30%', 
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Desea cambiar los restantes?<br>Se necesita permisos de administrador<br>'+
                 '<input type="text" placeholder="Contraseña" id="contrasena" name="contrasena" class="name form-control-bmz" required style="width: 99%;"/><br>\
                 <label>Monto restante</label><br><input type="number" placeholder="Restante" id="monto"  class="name form-control-bmz" required style="width: 99%;"/>',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var pass=$("input[name=contrasena]").val();
                var monto=$("#monto").val();
                if (pass!='') {
                    $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Sistema/solicitarpermiso",
                        data: {
                            pass:pass
                        },
                        success: function (response){
                                var respuesta = parseInt(response);
                                if (respuesta==1) {
                    //=============================================================================
                                	//if(monto>0){
                                		if(monto<=maxres){
                                			okeditarrestante(poliza,vigencia,realizados,monto);
                                		}else{
                                			toastr.error('la cantidad rebasa lo permitido');
                                		}
                                	//}else{
                                	//	toastr.error('Monto deberá de ser mayor a 0');
                                	//}
                    //=============================================================================
                                }else{
                                    notificacion(2);
                                }
                        },
                        error: function(response){  
                             notificacion(1);
                        }
                    });

                }else{
                    notificacion(0);
                }
            },
            cancelar: function (){
                
            }
        }
    });
    setTimeout(function(){
        new MaskedPassword(document.getElementById("contrasena"), '\u25CF');
    },1000);
}
function okeditarrestante(poliza,vigencia,realizados,monto){
	$.ajax({
        type:'POST',
        url: base_url+"index.php/PolizasCreadas/editarrestante",
        data: {
            poliza:poliza,
            realizados:realizados,
            monto:monto
        },
        success: function (response){
            toastr.success('Se Agrego Correctamente','Hecho!');
            setTimeout(function(){ 
		       location.href =base_url+'PolizasCreadas/servicios/'+idPoliza+'?pes=2';
		       //location.href =base_url+'PolizasCreadas/servicios/'+idPoliza+'';
		    }, 1000);
        },
        error: function(response){  
             notificacion(1);
        }
    });
}
function deletedoc(id){
    $.confirm({
        boxWidth: '40%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Confirma la eliminacion del servicio?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                //===================================================
                    $.ajax({
                        type:'POST',
                        url: base_url+"index.php/PolizasCreadas/deletedoc",
                        data: {
                            id:id
                        },
                        statusCode:{
                            404: function(data){
                                notificacion(9);
                            },
                            500: function(){
                                notificacion(10);
                            }
                        },
                        success:function(data){  
                            toastr.success('Se Elimino correctamente','Hecho!');
                            setTimeout(function(){ 
                                location.href =base_url+'PolizasCreadas/servicios/'+idPoliza+'?pes=2';
                            }, 1000);

                        }
                    });
                //================================================
            },
            cancelar: function (){
                
            }
        }
    });
}
function deletegarantia(id){
    $.confirm({
        boxWidth: '40%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Confirma la eliminacion de la pre en garantia?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                //===================================================
                    $.ajax({
                        type:'POST',
                        url: base_url+"index.php/PolizasCreadas/deletegarantia",
                        data: {
                            id:id
                        },
                        statusCode:{
                            404: function(data){
                                notificacion(9);
                            },
                            500: function(){
                                notificacion(10);
                            }
                        },
                        success:function(data){  
                            toastr.success('Se Elimino correctamente','Hecho!');
                            setTimeout(function(){ 
                                location.href =base_url+'PolizasCreadas/servicios/'+idPoliza+'?pes=3';
                            }, 1000);

                        }
                    });
                //================================================
            },
            cancelar: function (){
                
            }
        }
    });
}
function deletedll(id){
    var modelo_del=$('.deleteitems_'+id).data('modelo');
    var serie_del=$('.deleteitems_'+id).data('serie');
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Confirma la eliminacion '+modelo_del+' '+serie_del+'?<br>Este proceso sera irreversible <br>Se necesita permisos de administrador<br>'+
                 '<input type="text" placeholder="Contraseña" id="contrasena" name="contrasena" class="name form-control-bmz" required style="width: 99%;"/>',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var pass=$("input[name=contrasena]").val();
                var monto=$("#monto").val();
                if (pass!='') {
                    $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Sistema/solicitarpermiso",
                        data: {
                            pass:pass
                        },
                        success: function (response){
                                var respuesta = parseInt(response);
                                if (respuesta==1) {
                    //=============================================================================
                                    $.ajax({
                                        type:'POST',
                                        url: base_url+"index.php/PolizasCreadas/deletedll",
                                        data: {
                                            id:id
                                        },
                                        statusCode:{
                                            404: function(data){
                                                notificacion(9);
                                            },
                                            500: function(){
                                                notificacion(10);
                                            }
                                        },
                                        success:function(data){  
                                            toastr.success('Se Elimino correctamente','Hecho!');
                                            setTimeout(function(){ 
                                                location.href =base_url+'PolizasCreadas/servicios/'+idPoliza;
                                            }, 1000);

                                        }
                                    });
                    //=============================================================================
                                }else{
                                    notificacion(2);
                                }
                        },
                        error: function(response){  
                             notificacion(1);
                        }
                    });

                }else{
                    notificacion(0);
                }
            },
            cancelar: function (){
                
            }
        }
    });
    setTimeout(function(){
        new MaskedPassword(document.getElementById("contrasena"), '\u25CF');
    },1000);
}
function modal_add_pol(){
    $('#montrar_s_2').prop('checked',false);
    $('#modal_add_pol').modal('open');
    $('.div_table_pol').html('');
    $.ajax({
        type:'POST',
        url: base_url+"index.php/PolizasCreadas/servicios_cliente",
        data: {
            idPoliza:idPoliza,
            idcliente:$('#idcliente').val()
        },
        success:function(response){  
            $('.div_table_pol').html(response);
            $('#table_pol').DataTable({
                "order": [[ 3, "desc" ]],
                "lengthMenu": [[50,70, 100], [50,70, 100]]
            });
            filtrarservicios2();
        }
    });
}
function add_pol(asig,asigtipo){
        var ser = $('#servicios_select option:selected').val();
        var ser_t = $('#servicios_select option:selected').data('tipo');
        var ser_text = $('#servicios_select option:selected').text();
        if(ser>0){
            $.confirm({
                boxWidth: '30%',
                useBootstrap: false,
                icon: 'fa fa-warning',
                title: 'Atención!',
                content: '¿Confirma agregar servicio a Garantia?',
                type: 'red',
                typeAnimated: true,
                buttons:{
                    confirmar: function (){
                        
                        $.ajax({
                            type:'POST',
                            url: base_url+"index.php/PolizasCreadas/add_pol",
                            data: {
                                idPoliza:idPoliza,
                                asig:asig,
                                asigtipo:asigtipo,

                                ser:ser,
                                sert:ser_t
                            },
                            success: function (response){
                                toastr["success"]("Cambio realizado");
                                var url_refres=base_url+'PolizasCreadas/servicios/'+idPoliza+'?tang=3';
                                console.log(url_refres);
                                setTimeout(function(){ 
                                    location.href =url_refres;
                                }, 2000);
                            },
                            error: function(response){
                                notificacion(1);
                            }
                        });
                            
                        
                    },
                    cancelar: function (){
                        
                    }
                }
            });
        }else{
            toastr.error('Favor de seleccionar un servicio para vincularlo');
        }
}

function editar_pold(idd){
    $.confirm({
        boxWidth: '30%', 
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Desea cambiar el modelo y/serie?<br>Se necesita permisos de administrador<br>'+
                 '<input type="text" placeholder="Contraseña" id="contrasena0" name="contrasena0" class="name form-control-bmz" required style="width: 99%;"/>',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var pass=$("input[name=contrasena0]").val();
                var monto=$("#monto").val();
                if (pass!='') {
                    $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Sistema/solicitarpermiso",
                        data: {
                            pass:pass
                        },
                        success: function (response){
                                var respuesta = parseInt(response);
                                if (respuesta==1) {
                                    //=======================================
                                        editar_pold_confirm(idd);
                                    //=======================================
                                }else{
                                    notificacion(2);
                                }
                        },
                        error: function(response){  
                             notificacion(1);
                        }
                    });

                }else{
                    notificacion(0);
                }
            },
            cancelar: function (){
                
            }
        }
    });
    setTimeout(function(){
        new MaskedPassword(document.getElementById("contrasena0"), '\u25CF');
    },1000);
    
}

function editar_pold_confirm(idd){
    $('#modal_edit').modal('open');
    idPoliza_dll=idd;
    var s_eq=$('.row_edit_'+idd).data('equipo');
    var s_se=$('.row_edit_'+idd).data('serie');
    console.log(s_eq);
    $('#select_eq_edit').val(s_eq).select2();
    $('#serie_edit').val(s_se);
}
function save_edit_dll(){
    $.ajax({
        type:'POST',
        url: base_url+"index.php/PolizasCreadas/editar_pold",
        data: {
            idd:idPoliza_dll,
            eq:$('#select_eq_edit option:selected').val(),
            ser:$('#serie_edit').val(),
        },
        success: function (response){
            toastr.success("Cambio realizado");
            setTimeout(function(){ 
                location.reload();
            }, 2000);
        },
        error: function(response){
            notificacion(1);
        }
    });
}