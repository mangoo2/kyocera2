<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class Refacciones_model extends CI_Model 
{
    public function __construct() 
    {
        parent::__construct();
    }

    public function insertar_refacciones($dato){
    $this->db->insert('refacciones',$dato);   
    return $this->db->insert_id();
    } 

    public function update_refacciones($data, $id) {
    $this->db->set($data);
    $this->db->where('id', $id);
    $this->db->update('refacciones');
    return $id;
    }

    public function update_foto($data, $id) {
    $this->db->set($data);
    $this->db->where('id', $id);
    return $this->db->update('refacciones');
    }

    public function getListadoRefacciones()
    {
        $sql = "SELECT
                r.id,
                r.foto,
                r.codigo,
                r.nombre,
                r.categoria,
                r.stock,
                r.observaciones,
                r.precio_usa,
                r.precio_unitario,
                r.precio_total
                FROM refacciones as r
                WHERE status=1";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getDataEquipos(){
    $sql = "SELECT * FROM equipos WHERE estatus=1";
    $query = $this->db->query($sql);
    return $query->result();
    } 

    public function getData_categoria_refacciones(){
    $sql = "SELECT * FROM categoria_refacciones WHERE status=1";
    $query = $this->db->query($sql);
    return $query->result();
    } 

          //funcion generica de insercion en un catalogo
    public function insertar_equipo($data, $catalogo) {
        return $this->db->insert('' . $catalogo, $data);
    }

    public function getRefaccionesPorId($id)
    {
        $sql = "SELECT * from refacciones
                WHERE id=".$id;
        $query = $this->db->query($sql);
        return $query->row();
    }

    public function getListado_reacciones($id)
    {
        $sql = "SELECT c.id,e.modelo,c.idRefacciones FROM equipos_has_refacciones as c left join equipos as e on e.id = c.idEquipo where idRefacciones =".$id;
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function eliminar_has_refacciones($id) {
    $this->db->where('id', $id);
    return $this->db->delete('equipos_has_refacciones');
    }

    public function truncateTable($table)
    {
        $this->db->truncate($table);
    }

    public function getListaPreciosRefacciones()
    {
        $sql = "SELECT
                r.id as 'DT_RowId',
                r.codigo, r.nombre, rc.precioDolares, 
                rc.neto4, rc.iva4,
                rc.neto3, 
                rc.poliza, rc.iva3, rc.especial, 
                rc.neto2, rc.iva2, rc.frecuente,
                rc.neto, rc.iva, rc.general
                FROM refacciones as r 
                LEFT JOIN refacciones_costos as rc ON rc.refacciones_id=r.id";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getPreciosRefaccionesPorId($idRefaccion)
    {
        $sql = "SELECT
                r.id as 'DT_RowId',
                r.codigo, r.nombre, rc.precioDolares,
                rc.neto4, rc.iva4,
                rc.neto3, 
                rc.poliza, rc.iva3, rc.especial, 
                rc.neto2, rc.iva2, rc.frecuente,
                rc.neto, rc.iva, rc.general
                FROM refacciones as r 
                LEFT JOIN refacciones_costos as rc ON rc.refacciones_id=r.id WHERE r.id=".$idRefaccion;
        $query = $this->db->query($sql);
        return $query->result();
    }
   
    public function update_precios_refacciones($data, $id) 
    {
        $this->db->set($data);
        $this->db->where('refacciones_id', $id);
        $this->db->update('refacciones_costos');
        return $id;
    }

    public function getTipoCambioRefacciones()
    {
        $sql = "SELECT *
                FROM tiposCambio WHERE categoria='refacciones'";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getPorcentajeGananciaRefacciones()
    {
        $sql = "SELECT *
                FROM porcentajeGanancia WHERE categoria='refacciones'";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getDescuentosRefacciones()
    {
        $sql = "SELECT *
                FROM descuentosCategorias WHERE categoria='refacciones'";
        $query = $this->db->query($sql);
        return $query->result();
    }
}

?>