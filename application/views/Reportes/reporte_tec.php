<?php
require_once dirname(__FILE__) . '/TCPDF4/examples/tcpdf_include.php'; 
require_once dirname(__FILE__) . '/TCPDF4/tcpdf.php';
$logos = base_url().'public/img/alta.png';
//====================================================
class MYPDF extends TCPDF {
  //Page header
  private $customHeader = '';

  public function setCustomHeader($headerText) {
      $this->customHeader = $headerText;
  }
  public function Header() {
      $namepersonal='';
      if (!empty($this->customHeader)) {
            $namepersonal=$this->customHeader;
      }
      $logos = base_url().'public/img/alta.png';
      
        $tamano1='30';//30
        $tamano2='40';//40
        $tamano3='30';//30
      $html = '';
          $html .='<style type="text/css">.info_fac{font-size: 11px;}</style>';
          $html .='<table width="100%" border="0" cellpadding="4px" class="info_fac">';
            $html .='<tr>';
              $html .='<td width="10%"><img src="'.$logos.'"></td>';
              $html .='<td width="50%" valign="top"><h2> Reporte de servicios </h2></td>';
              $html .='<td width="40%" align="right" class="info_fac">'.$namepersonal.'</td>';
            $html .='</tr>';
          $html .='</table>';

        $this->writeHTML($html, true, false, true, false, '');
  }
    // Page footer
  public function Footer() {
    $html2='';
    $styleQR = array('border' => 0, 
         'vpadding' => '0', 
         'hpadding' => '0', 
         'fgcolor' => array(0, 0, 0), 
         'bgcolor' => false, 
         'module_width' => 1, 
         'module_height' => 1);
    $html='';

    $html2 .= '<style type="text/css">.footerpage{font-size: 9px;color: #9e9e9e;}</style>';

      $html2 .='<table width="100%" border="0" cellpadding="2" class="fontFooterp">';
        $html2 .='<tr>';
          $html2 .='<td align="right" class="footerpage">';
          $html2 .='Pagina '.$this->getAliasNumPage().' de '.$this->getAliasNbPages().'</td>';
        $html2 .='</tr>';
      $html2 .='</table>';
    
      $this->writeHTML($html2, true, false, true, false, '');
      
  }
} 
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Kyocera');
$pdf->SetTitle('Reporte');
$pdf->SetSubject('factura');
$pdf->SetKeywords('factura');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins('10', '20', '10');
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$num_for=15;
$pdf->SetFooterMargin($num_for);

// set auto page breaks
$pdf->SetAutoPageBreak(true, $num_for+2);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('dejavusans', '', 10);

//====================================================
//$pdf->setPrintHeader(false);
//$pdf->setPrintFooter(false);
//$pdf->SetMargins('10', '10', '10');
//$pdf->SetFooterMargin('10');
$style='<style type="text/css">
  .t_title{
    font-weight: bold;text-align: center;background-color: #d0d0d0;
    padding-top:10px;
  }
  .bordered tr{
    border-bottom: 1px solid #d0d0d0;
  }
  .b_l_b{
    vertical-align: center;
    border-bottom: 1px solid #9e9e9e;
  }
  .td_cot{
    font-size:9px
  }
  li{
    font-size:10px
  }
</style>';
foreach ($resultstec as $item) { 
    if($item->personalId!=49 && $item->personalId!=29 && $item->personalId!=47  && $item->personalId!=82  && $item->personalId!=81 && $item->personalId!=80 && $item->personalId!=83){
      $mostrar=$this->ModeloServicio->filtro_exit_val($tec,$item->personalId);
      if($mostrar==1){
          //===================================
            // Cambiar el encabezado dinámicamente antes de agregar la página
            $pdf->setCustomHeader($item->tecnico);

          	$pdf->AddPage('P', 'A4');
              $html=$style;
             	$html.=${'info_tec'.$item->personalId};
             	log_message('error',${'info_tec'.$item->personalId});
             	//log_message('error',$info_tec27);
  			$pdf->writeHTML($html, true, false, true, false, '');
          //===================================
      }   
    }                   
} 

  

  
//=======================================================================
$pdf->Output('Folios_.pdf', 'I');
//$pdf->Output('../../facturas/'.$GLOBALS["Folio"].'.pdf', 'F');
//$pdf->Output('/facturas/'.$GLOBALS["Folio"].'.pdf', 'F');
?>