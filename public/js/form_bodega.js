// Obtener la base_url 
var base_url = $('#base_url').val();
// Saber si es solo Visualización
$(document).ready(function(){
    $('.chosen-select').chosen({width: "90%"});
	// Asignamos el formulario de envío
	var formulario = $('#form_bodega');
	// Validamos el formulario en base a las reglas mencionadas debajo
	formulario.validate({
            ignore: "",
            rules: 
            {
				bodega: {
                    required: true
                },
                direccion: {
                    required: true
                },
            },
            // Para mensajes personalizados
            messages: 
            {
                bodega:{
                    required: "Ingrese un nombre"
                },
                direccion:{
                    required: "Ingrese una dirección"
                },
            },
            errorElement : 'div',
            // Si están correctos los campos, procesamos el formulario mediante Ajax
            submitHandler: function (form){
                var datos = formulario.serialize();
                $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Bodega/insertaActualiza",
                    data: datos,
                    success:function(data){  
                        if(data !=1){
                           var texto = "Se ha actualizado correctamente";
                           table.ajax.reload();
                            $('.btn_ce').remove();
                            $('.label').html('Nueva');
                            $('.btn_n').html('Nuevo');
                        }else{
                           var texto = "Se ha registrado correctamente";
                           table.ajax.reload();
                           $('.btn_ce').remove();
                        }
                        $("#form_bodega")[0].reset();
                        $('#bodegaId').val(0);

                        swal({ title: "Éxito",
                            text: texto,
                            type: 'success',
                            showCancelButton: false,
                            allowOutsideClick: false,
                        });                  
                        setTimeout(function(){ 
                           //window.location.href = base_url+"index.php/Bodega"; 
                        }, 2000);   
                    }
                });
            }
        });
    table = $('#tabla_bodega').DataTable();
    load();
});    
function load() {
    table.destroy();
    table = $('#tabla_bodega').DataTable({
        "ajax": {
            "url": base_url+"index.php/Bodega/getListadoBodega"
        },
        "columns": [
            {"data": "bodegaId"},
            {"data": "bodega"},
            {"data": "direccion"},             
            {"data": "tipov",
                 render:function(data,type,row){
                    var html='';
                    if(row.tipov==1){
                        html='Kyocera';
                    }
                    if(row.tipov==2){
                        html='D-Impresión';   
                    }

                    
                    return html;
                }
            },
            {"data": "bodegaId",
                 render:function(data,type,row){
                    $('.tooltipped').tooltip();
                    var comillas = "'";
                    var html='';
                        html+='<div class="btn_group_btn">';
                        html+='<a class="btn-floating blue tooltipped editar_'+row.bodegaId+'" \
                                onclick="editar('+row.bodegaId+')" \
                                data-bodega="'+row.bodega+'" \
                                data-direcc="'+row.direccion+'" \
                                data-tipov="'+row.tipov+'" \
                                data-position="top" data-delay="50" data-tooltip="Editar">\
                                    <i class="material-icons">edit</i>\
                                  </a>';
                        html+='<a class="btn-floating red tooltipped elimina" data-position="top" data-delay="50" data-tooltip="Eliminar" onclick="deleteb('+row.bodegaId+')">\
                                        <i class="material-icons">delete_forever</i>\
                                    </a>';
                        html+='</div>';
                    return html;
                }
            }
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[25, 50, 100], [25, 50, 100]],
        "dom": 'Bfrtpl',
        // Muestra el botón de Excel para importar la tabla
        "buttons": [     
        ],
        // Cambiamos lo principal a Español
        "language": {
            "lengthMenu": "Desplegando _MENU_ elementos por página",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
              }
        }
    });
}
function editar(id) {
    var bodega=$('.editar_'+id).data('bodega');
    var direc=$('.editar_'+id).data('direcc');
    var tipov=$('.editar_'+id).data('tipov');
    $('#bodegaId').val(id);
    $('#bodega').val(bodega);
    $('#direccion').val(direc);
    $("input[name=tipov][value='"+tipov+"']").prop("checked",true);
    $('.label').html('Edición');
    $('.btn_n').html('Editar');
    $('.btn_c').html('<button type="button" class="btn waves-effect red gradient-shadow btn_ce" onclick="cancela()">Cancelar</button>');
}
function cancela(){
    $('.btn_ce').remove();
    $("#form_bodega")[0].reset();
    $('#bodegaId').val(0);
    $('.label').html('Nueva');
    $('.btn_n').html('Nuevo');
}
function deleteb(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de Eliminar la bodega?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Bodega/deletebodega",
                    data: {
                        id:id
                    },
                    success:function(response){  
                        
                            swal("Éxito!", "Se ha Eliminado la bodega", "success");
                            load();
                        

                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}