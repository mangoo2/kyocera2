var base_url = $('#base_url').val();
var idinsert=0;
var viewtipo = $('#viewtipo').val();//0 control de tareas 1 acuerdos
$(document).ready(function() {
    base_url = $('#base_url').val();
  summernote();
  $('.enviodeticket').click(function(event) {
    var gurl= $('#base_url').val()+'Control_tareas/registraactividad';
    console.log(gurl);
    console.log("Función ejecutada en la ventana secundaria.");
            
            // Ejecuta una función en la ventana principal
            if (window.opener && !window.opener.closed) {
                window.opener.funcionPrincipal();
            } else {
                alert("La ventana principal ya no está abierta.");
            }
            //=============================
                var personallis = $("#tablepersonal tbody > tr");
                var DATAr  = [];
                    
                personallis.each(function(){  
                    //======================================
                    item = {};            
                    item ["per"]  = $(this).find("select[id*='personal2'] option:selected").val();
                    DATAr.push(item);
                });
                aInfor   = JSON.stringify(DATAr);
            //===============================
                console.log('viewtipo:'+viewtipo);
            $.ajax({
                type:'POST',
                url: gurl,
                data: {
                    personal:$('#personal option:selected').val(),
                    personalplus:aInfor,
                    title: $('.notetitle').val(),
                    contenido: $('.summernote').val(),
                    fecha: $('#vigencia').val(),
                    tipoac:$('#viewtipo').val()
                },
                async: false,
                statusCode:{
                    404: function(data){
                        swal("Error", "404", "error");
                    },
                    500: function(){
                        swal("Error", "500", "error"); 
                    }
                },
                success:function(data){
                  console.log(data)
                    idinsert=parseInt(data);
                    //$('#cliioimg').fileinput('upload');
                    var input = document.getElementById('files');
                    if(input.files.length>0){
                        $('#files').fileinput('upload');
                    }else{
                        cerrarinfo();
                    }

                  
                }
            });
    
  });
  $('.enviocomentario').click(function(event) {
        var gurl= $('#base_url').val()+'Generales/comen_pni';
        console.log(gurl);
        $.ajax({
            type:'POST',
            url: gurl,
            data: {
                id: $('#idp').val(),
                comen: $('.summernote').val()
            },
            async: false,
            statusCode:{
                404: function(data){
                    swal("Error", "404", "error");
                },
                500: function(){
                    swal("Error", "500", "error"); 
                }
            },
            success:function(data){
              console.log(data)
              toastr["success"]("Comentario Enviado", "Hecho!");
              $('.summernote').val('');
                setTimeout(function(){ 
                    location.reload();
                }, 2000);
            }
        });
  });
  //================================
  var url_fileinput=base_url+'Control_tareas/imagenes_multiple';
  console.log(url_fileinput);
    $("#files").fileinput({
        showCaption: true,
        dropZoneEnabled: false,
        showUpload: true,// quita el boton de upload
        //rtl: true,
        allowedFileExtensions: ["jpg","png","webp","jpeg","pdf","xlsx","xls"],
        browseLabel: 'Seleccionar documentos',
        uploadUrl: url_fileinput,
        maxFilePreviewSize: 6000,
        
        uploadExtraData: function (previewId, index) {
        var info = {
                    idac:idinsert,
                    tipo:0
                };
        return info;
      }
    }).on('filebatchuploadcomplete', function(event, files, extra) {
      //location.reload();
        cerrarinfo();
    }).on('filebatchuploadsuccess', function(event, files, extra) {
      //location.reload();
      //toastr.success('Se cargo el Certificado Correctamente','Hecho!');
    });
  //================================
});
function summernote(){
  //$('.summernote').summernote({toolbar: [ ['style', ['bold', 'italic', 'underline', 'clear']],['insert', ['link', 'picture', 'video']]]});
  $('.summernote').summernote();
}
function cerrarinfo(){
    toastr["success"]("Solicitud Enviada", "Hecho!");
      $('.summernote').val('');$('.notetitle').val('');
      $('.notetitle').val('');
      // Ejecuta una función en la ventana principal
        if (window.opener && !window.opener.closed) {
            window.opener.funcionPrincipal();
        } else {
            //alert("La ventana principal ya no está abierta.");
        }
    //=================
      setTimeout(function(){ 
        window.close();
    }, 2000);
}
var row=0;
function addperplus(){
    var option_personal=$('#personal').html();
    var html='<tr class="row_'+row+'"><td><select class="form-control" id="personal2">'+option_personal+'</select></td>\
                <td><button class="btn btn-danger" onclick="deleterow('+row+')"><i class="fas fa-times"></i></button></td></tr>';
    $('.tb_tablepersonal').append(html);
    row++;
}
function deleterow(row){
    $('.row_'+row).remove();
}