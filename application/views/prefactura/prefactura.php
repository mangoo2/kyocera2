<!DOCTYPE html>
<html lang="es">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="icon" href="<?php echo base_url(); ?>app-assets/images/favicon/favicon_kyocera.png" sizes="32x32">
		<link rel="stylesheet" href="<?php echo base_url(); ?>public/plugins/bootstrap4_3_1/bootstrap.min.css" crossorigin="anonymous">
		<title>Prefactura</title>
		<!-- jQuery Library -->
    	<script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/jquery-3.3.1.min.js"></script>
    	<script src="<?php echo base_url(); ?>public/plugins/bootstrap4_3_1/bootstrap.min.js" crossorigin="anonymous"></script>
        <link href="<?php echo base_url(); ?>app-assets/vendors/sweetalert/dist/sweetalert.css" type="text/css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>public/css/fontawesome/css/all.css" type="text/css" rel="stylesheet">
        <!--
        <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/js/ckeditor/ckeditor.js"></script>
    	<script type="text/javascript" src="<?php echo base_url(); ?>app-assets/js/ckeditor/adapters/jquery.js"></script>-->
    	<input type="date" id="fechaactual" value="<?php echo date('Y-m-d')?>" style="display: none;">
    	<input type="date" id="fechasiguiente" value="<?php 
    		$fecha_actual = date("Y-m-d");
    		echo date("Y-m-d",strtotime($fecha_actual."+ 1 days"))?>" style="display: none;">
		<style type="text/css">
			.tdstitle{background: #c1bcbc; font-weight: bold; color: white; }
			html{ -webkit-print-color-adjust: exact; }
			input{padding: 0px !important; height: 28px !important; }
			td,th{ padding-top: 5px !important; padding-bottom: 5px !important; }
			select{padding: 0px !important; height: 28px !important; font-size: 11px !important; }
			.optionred{ background: #e29696; }
			@media print{
				input{ background: transparent; border: 0 !important; }
				.buttonenvio{ display: none; }
				html{ -webkit-print-color-adjust: exact; }
				.buttoimprimir{ display: none; }
				.tdstitle{ background: #c1bcbc; font-weight: bold; color: white; }
				select{ background: transparent; border: 0 !important; -moz-appearance: none; -webkit-appearance: none; appearance: none; }
				.form-control:disabled, .form-control[readonly] { background-color: transparent; opacity: 1; }
				.devolucion{ display: none; }
			}
				.devolucion{ background-color: #ff02026b; }
				.btndevolucion2{ display: none; }
				.btndevolucion3{ display: none; }
			<?php if(isset($_GET['devolucion'])){ ?>
				.table_dev{ display: none; }
			<?php }else{ ?>
				.btndevolucion{ display: none; }
			<?php } ?>
			#modaldirecciones .modal-dialog{ max-width: 90% !important; }
			.viewdirecciones{ max-height: 385px !important }
			<?php if($rfc_id_button=='disabled'){ ?>
			<?php } 
				$arrayprocompra=array();
			?>
			.form-control:disabled, .form-control[readonly] {
    			background-color: #e9ecef !important;
    			opacity: 1;
			}
			option:disabled {
				background-color: #e9ecef !important; /* Cambiar el color de fondo */
			}
			.fts-12{
				font-size: 12px;
			}
			.tx-a-c{
				text-align: center;
			}
			.tx-a-r{
				text-align: right;
			}
		</style>
	</head>
	<body>
		<input type="hidden" id="idCliente" value="<?php echo $idCliente;?>">
		<?php if($rfc_id_button==''){ 
			$ediciondf='';
			?>
			<style type="text/css">
				.nosolicitado {background: url(<?php echo base_url().'public/img/nosolicitado.png'?>);background-size: contain;background-repeat: no-repeat;background-position: center;}
			</style>
		<?php }else{
			$ediciondf='onclick="editardatosfiscales()"';
		} ?>
		<input type="hidden" id="base_url" name="base_url" value="<?php echo base_url(); ?>">
		<form method="post" id="formprefactura">
			<input type="hidden" id="prefacturaId" name="prefacturaId" value="<?php echo $prefacturaId;?>">
			<input type="hidden" id="ventaId" name="ventaId" value="<?php echo $ventaId;?>">
			<table border="1" width="100%" class="table_dev">
				<tr style="height: 25%">
					<td style="width: 20%">
						<img src="<?php echo base_url(); ?>app-assets/images/<?php echo $configuracionCotizacion->logo1; ?>" height="60px">
					</td>
					<td style="width: 55%">
						<?php echo $configuracionCotizacion->textoHeader; ?>
					</td>
					<td style="width: 20%">
						<img class="" src="<?php echo base_url(); ?>app-assets/images/1024_kyocera_logo_mds.png" alt="materialize logo"  style="max-width:200px; width: 100%;">
					</td>
				</tr>
			</table>
			<table border="1" width="100%" class="table_dev">
				<tr>
					<td class="tdstitle" style="width: 10%;">FECHA</td>
					<td style="width: 5%;"><?php echo $dia_reg;?></td>
					<td style="width: 5%;"><?php echo $mes_reg;?></td>
					<td style="width: 5%;"><?php echo $ano_reg;?></td>
					<td class="tdstitle" style="width: 5%">AC</td>
					<td style="width: 10%;"><?php echo $ini_personal;?></td>
					<td class="tdstitle" style="width: 20%">PROINVEN No.</td>
					<td style="width: 10%;"><?php echo $proinven;?></td>
					<td class="tdstitle" style="width: 10%">ENTREGA</td>
					<td style="width: 20%"><input type="date" id="fechaentrega" value="<?php echo $fechaentrega;?>" name="fechaentrega" class="form-control" onchange="actualizarfechae(<?php echo $ventaId;?>,0)" data-entrega="<?php echo $e_entrega;?>" data-iser="0"></td>
				</tr>
			</table>
			<table border="1" width="100%" class="table_dev">
				<tr>
					<td style="width: 5%;">Cia.</td>
					<td style="width: 80%;" colspan="5" <?php echo $ediciondf;?>><output id="razon_social"><?php echo $empresa;?></output></td>
					<td style="width: 5%;">RFC</td>
					<td style="width: 10%;" <?php echo $ediciondf;?>>
						<select class="form-control mm" id="rfc_id" name="rfc_id" onchange="selectrcf()" <?php echo $rfc_id_button;?> required>
						    <option value="">Seleccione</option>
						<?php foreach ($rfc_datos as $item) { ?>
								<option value="<?php echo $item->id; ?>" <?php if($item->id==$rfc_id){ echo 'selected';}?> 
								<?php if($item->rfc=='XAXX010101000'){ ?>
									data-razonsocial="<?php echo $item->razon_social.' ('.$empresars.')';?>"
								<?php }else{ ?>
									data-razonsocial="<?php echo $item->razon_social;?>"
								<?php } ?>
								data-rfc="<?php echo $item->rfc;?>"
								data-numext="<?php echo $item->num_ext;?>"
								data-colonia="<?php echo $item->colonia;?>"
								data-calle="<?php echo $item->calle;?>"
								data-dciudad="<?php echo $item->municipio;?>"
								data-destado="<?php echo $item->estado.' C.P. '.$item->estado;?>"
								><?php echo $item->rfc; ?></option>
						<?php } ?>
					    </select>	
					</td>
				</tr>
				<tr>
					<td style="width: 5%; font-size: 12px">VENCE</td>
					<td style="width: 12%;">
						<input type="date" id="vencimiento" name="vencimiento" class="form-control" value="<?php echo $vence;?>" <?php echo $vence_block;?> style="max-width: 138px;" <?php echo $maxcredito; ?> required></td>
					<td style="width: 10%; font-size: 12px; text-align: center;" <?php echo $ediciondf;?>>METODO DE PAGO</td>
					<td style="width: 20%; font-size: 12px; text-align: center;" <?php echo $ediciondf;?>>
					<select class="form-control" id="metodopagoId" name="metodopagoId" <?php echo $metodopagoId_block;?> required>
						<option></option>
						<?php foreach ($metodopagorow as $item) { ?>
								<option value="<?php echo $item->id; ?>" <?php if($item->id==$metodopagoId){ echo 'selected';} ?>    ><?php echo $item->metodopago_text; ?></option>
						<?php } ?>
					</select>
					</td>
					<td style="width: 12%; font-size: 12px; text-align: center;" <?php echo $ediciondf;?>>FORMA DE PAGO</td>
					<td style="width: 14%; font-size: 12px; text-align: center;" <?php echo $ediciondf;?>>
						<select class="form-control" id="formapagoId" name="formapagoId" <?php echo $formapago_block;?> required>
							<option></option>
						<?php foreach ($formapagorow->result() as $item) { ?>
								<option value="<?php echo $item->id; ?>" <?php if($item->id==$formapago){ echo 'selected';} ?>    ><?php echo $item->formapago_text; ?></option>
						<?php } ?>
					</select>
					</td>
					<td style="width: 10%; font-size: 12px;" <?php echo $ediciondf;?>>USO DE CFDI</td>
					<td style="width: 17%; font-size: 12px;" <?php echo $ediciondf;?>>
						<select class="form-control" id="usocfdiId" name="usocfdiId" <?php echo $cfdi_block;?>>
							<?php foreach ($cfdirow->result() as $item) { ?>
									<option value="<?php echo $item->id; ?>" <?php if($item->id==$cfdi){ echo 'selected';} ?>    ><?php echo $item->uso_cfdi_text; ?></option>
							<?php } ?>
						</select>
					</td>	
				</tr>
			</table>
			<table border="1" width="100%" class="table_dev">
				<tr>
					<td style="font-size: 12px; width: 10%;">tel.</td>
					<td style="font-size: 12px; width: 20%;" colspan="2">
						<select class="form-control" id="telId" name="telId" <?php echo $telId_block; ?>>
						<?php foreach ($resultadoclitel->result() as $item) { ?>
							<option value="<?php echo $item->id;?>"   <?php if($telId==$item->id){echo 'selected'; }?> ><?php echo $item->tel_local;?></option>		
					    <?php } 
					    	foreach ($datoscontacto->result() as $item) { ?>
					    	<option data-contacto="1"  value="<?php echo $item->datosId;?>" <?php if($telId==$item->datosId){echo 'selected'; }?>><?php echo $item->telefono;?></option>	
					    <?php }
					    ?>
						</select>
					</td>
					<td style="font-size: 12px; width: 10%; text-align: center;">FORMA DE COBRO</td>
					<td style="font-size: 12px; width: 20%;">
						<?php if($formacobro!=''){
							echo $formacobro;
						}else{ ?> 
							<select class="form-control" name="formadecobro" id="formadecobro" required>
								<option></option>
								<option>Mostrador</option>
								<option>Transferencia</option>
								<option>Deposito directo</option>
								<option>Tecnico</option>
							</select>
						<?php } ?>
						
					</td>
				</tr>
			</table>
			<table border="1" width="100%" class="table_dev">
				<tr>
					<td class="tdstitle" colspan="8">Dirección Fiscal</td>
				</tr>
				<tr>
					<td style="font-size: 12px; width: 4.5%;">Calle</td>
					<td style="font-size: 12px; width: 26%;" colspan="3"><input type="text" value="<?php echo $calle;?>" id="calle" name="d_calle" style="width: 100%; border: 0px;"  readonly></td>
					<td style="font-size: 12px; width: 4.5%;">No.</td>
					<td style="font-size: 12px; width: 5%;"><input type="text" value="<?php echo $num_ext;?>" id="num_ext" name="d_numero" style="width: 100%; border: 0px;"  readonly></td>
					<td style="font-size: 12px; width: 4.5%;">Col.</td>
					<td style="font-size: 12px; width: 25%; " colspan="2">
						<input type="text" value="<?php echo $colonia;?>" id="colonia" name="d_colonia" style="width: 100%; border: 0px;" readonly>
					</td>
					
				</tr>
				<tr>
					<td style="font-size: 12px; width: 5%;">Cd</td>
					<td style="font-size: 12px;" colspan="2">
						<input type="text" value="<?php echo $municipio;?>" id="d_ciudad" name="d_ciudad"  style="width: 100%; border: 0px;" readonly>
					</td>
					<td style="font-size: 12px; width: 5%;">Edo.</td>
					<td style="font-size: 12px; width: 5%;" colspan="4">
							<?php 
								$estadoval='';
								foreach ($estadorow->result() as $item) { 
									if($item->EstadoId==$estado){ 
									 	$estadoval = $item->Nombre;
									}
								} ?>
						<input type="text" value="<?php if($estadovals!=''){echo $estadovals.'xxx';}else{echo $estadoval.' C.P.'.$cp;}?>" id="d_estado" name="d_estado"  style="width: 100%; border: 0px;" readonly>
					</td>
				</tr>
			</table>
			<table border="1" width="100%" class="table_dev">
				<tr>
					<td class="tdstitle" colspan="8">Domicilio de entrega/Instalación</td>
				</tr>
				<tr>
					<td style="width: 15%" onclick="editardireccion(<?php echo $idCliente;?>)">Domicilio</td>
					<td style="width: 85%">
						<input type="text" name="domicilio_entrega" class="form-control" id="domicilio_entrega" value="<?php echo $domicilio_entrega;?>"  style="background-color: transparent; border:0px; font-size:14px;" onclick="editardireccion(<?php echo $idCliente;?>)" required <?php echo $comentario_block; ?> >
					</td>
				</tr>
			</table>
			<table border="1" width="100%" class="table_dev">
				<tr>
					<td style="font-size: 12px; width: 8.5%;">Contacto</td>
					<td style="font-size: 12px;">
						<select class="form-control" id="contactoId" name="contactoId" <?php echo $contactoId_block; ?>  onchange="obtenercargo()">
						<?php foreach ($datoscontacto->result() as $item) { ?>
					    	<option 
					    		data-contacto="1" 
					    		data-puesto="<?php echo $item->puesto;?>"
					    		data-email="<?php echo $item->email;?>" 
					    		data-dirid="<?php echo $item->dir;?>" 
					    		data-dir="<?php echo $item->direccion;?>" 
					    		value="<?php echo $item->datosId;?>" <?php if($contactoId==$item->datosId){echo 'selected'; }?>><?php echo $item->atencionpara;?></option>	
					    <?php 
					    		if ($contactoId==$item->datosId) {
					    			$email=$item->email;
					    		}
							}
					    ?>
					    <?php foreach ($resultadoclipcontacto->result() as $item) { ?>
							<option class="optionred" data-contacto="0" value="<?php echo $item->id;?>" <?php if($contactoId==$item->id){echo 'selected'; }?>><?php echo $item->persona_contacto;?></option>		
					    <?php } ?>
						</select>
					</td>
					<td style="font-size: 12px; width: 5%;">Cargo</td>
					<td style="font-size: 12px; width: 20%;" colspan="3">
						<input type="tex" id="cargo" name="cargo" class="form-control" value="<?php echo $cargo;?>" <?php echo $cargo_block;?> >
					</td>
					<td style="font-size: 12px; width: 6%;">Email:</td>
					<td style="font-size: 12px;" colspan="2" class="addemail"><?php echo $email;?></td>
				</tr>
				
			</table>
			<table border="1" width="100%" class="nosolicitado">
				<thead>
				<tr>
					<th style="font-size: 12px; ">Cantidad</th>
					<!--<th style="font-size: 12px; ">Surtir</th>-->
					<th style="font-size: 12px; ">No. de Parte</th>
					<th style="font-size: 12px; " colspan="3">Descripción</th>
					<th style="font-size: 12px; ">Precio Unitario</th>
					<th style="font-size: 12px; " >Total</th>
				</tr>
				</thead>
				<tbody>
					<?php 
						$totalgeneral=0;
						$block_envio=0;
						$cf_serviciocId_tipo=0;
						$cf_serviciocId=0;
						foreach ($resultadoequipos->result() as $item) { 
							$totale=$item->cantidad*$item->precio;
							$totalgeneral=$totalgeneral+$totale;
							if ($item->serie_estatus==0) {
								$btn_delete_pro='<a class="btn btn-danger btn-sm deletepro_ss" onclick="deletepro(1,'.$item->id.')" ><i class="fas fa-trash"></i></a>';
							}else{
								$btn_delete_pro='';
							}
							?>
							<tr>
								<td style="font-size: 12px; text-align: center;">
									<span onclick="editarcantidad(1,<?php echo $block_buttonc;?>,<?php echo $item->id;?>,<?php echo $item->cantidad;?>)"><?php echo $item->cantidad;?></span>
									<a class="btn btn-danger btn-sm btndevolucion" onclick="devolucion(2,<?php echo $item->id;?>,<?php echo $item->cantidad;?>)" ><i class="fas fa-trash"></i></a>
									<?php echo $btn_delete_pro;?>
								</td>
								<!--<td style="font-size: 12px; text-align: center;"><?php echo $this->ModeloCatalogos->obtenerbodega($item->serie_bodega);?></td><!--f1-->
								<td style="font-size: 12px; "><?php echo $this->ModeloCatalogos->obtenernumeroserieequipo($item->idEquipo);?></td>
								<td style="font-size: 12px; " colspan="3">
									<table border="1" width="100%">
										<tr>
											<td width="30%">TIPO</td>
											<td width="70%">IMPRESORA</td>		
										</tr>
										<tr>
											<td>MARCA</td>
											<td>KYOCERA</td>		
										</tr>
										<tr>
											<td>MODELO</td>
											<td><?php echo $item->modelo;?></td>		
										</tr>
										<?php 
													if ($item->serie_estatus==0) {
														$block_envio=1;
													}else{
														$resuleserie=$this->ModeloCatalogos->getequiposerie($item->id);
														foreach ($resuleserie->result() as $itemse) { 
															$arrayprocompra[]=array('pro'=>1,'conserie'=>1,'cantidad'=>1,'productoid'=>$item->idEquipo,'serieId'=>$itemse->serieId,'seriename'=>$itemse->serie,'bodegaId'=>$itemse->bodegaId,'precio'=>$item->precio);
															?>
															<tr>
																<td>SERIES</td>
																<td>
																	<?php echo $itemse->serie; ?>
																	<a class="btn btn-danger btn-sm btndevolucion2" onclick="devolucion2(2,<?php echo $item->id;?>,<?php echo $itemse->serieId;?>)" ><i class="fas fa-trash"></i></a>			
																</td>		
															</tr>
															<tr>
																<td>BODEGA</td>
																<td class="bodega" data-bodega="<?php echo $itemse->bodegaId; ?>"><?php echo $itemse->bodega; ?></td>		
															</tr>
														<?php }
														
													}
										?>
										<?php if ($item->garantia==1) { ?>
											<tr><td colspan="2" onclick="descartargarantia(1,<?php echo $item->id;?>)">Posible Garantia</td></tr>
										<?php } ?>

										
									</table>
								</td>
								<td style="font-size: 12px; text-align: center;" 
									onclick="editarprecio(1,<?php echo $block_button;?>,<?php echo $item->id;?>,<?php echo $item->precio;?>)" >
									$<?php echo number_format($item->precio,2,'.',',');?>
								</td>
								<td style="font-size: 12px; text-align: center;" >$<?php echo number_format($totale,2,'.',',');?></td>
							</tr>
							<tr class="table_dev">
								<td>Dirección</td>
								<td class="addequiposvtd" colspan="7">
									<input type="text" onpaste="return false;" id="comentario0" name="comentario0" class="form-control comentario comentario_<?php echo $item->id;?>" data-cliente="<?php echo $idCliente;?>" data-idrow="<?php echo $item->id;?>" value="<?php echo $item->comentario;?>" style="background: transparent; font-size:14px;" <?php echo $comentario_block; ?> onclick="editardireccion2(<?php echo $idCliente;?>,<?php echo $item->id;?>)" required>
								</td>		
							</tr>
							<?php
						}
						foreach ($resultadoequipos_dev->result() as $item) { 
							$totale=$item->cantidad*$item->precio;
							
							?>
							<tr class="table_dev devolucion">
								<td style="font-size: 12px; text-align: center;">
									<?php echo $item->cantidad;?>		
								</td>
								<!--<td style="font-size: 12px; text-align: center;"><?php echo $this->ModeloCatalogos->obtenerbodega($item->serie_bodega);?></td><!--f1-->
								<td style="font-size: 12px; "><?php echo $this->ModeloCatalogos->obtenernumeroserieequipo($item->idEquipo);?></td>
								<td style="font-size: 12px; " colspan="3">
									<table border="1" width="100%">
										<tr>
											<td width="30%">TIPO</td>
											<td width="70%">IMPRESORA</td>		
										</tr>
										<tr>
											<td>MARCA</td>
											<td>KYOCERA</td>		
										</tr>
										<tr>
											<td>MODELO</td>
											<td><?php echo $item->modelo;?></td>		
										</tr>
										<?php 
													if ($item->serie_estatus==0) {
														$block_envio=1;
													}else{
														$resuleserie=$this->ModeloCatalogos->getequiposerie($item->id);
														foreach ($resuleserie->result() as $itemse) { ?>
															<tr>
																<td>SERIES</td>
																<td>
																	<?php echo $itemse->serie; ?>			
																</td>		
															</tr>
															<tr>
																<td>BODEGA</td>
																<td><?php echo $itemse->bodega; ?></td>		
															</tr>
														<?php }
														
													}
										?>

										
									</table>
								</td>
								<td style="font-size: 12px; text-align: center;"  >
									$<?php echo number_format($item->precio,2,'.',',');?>
								</td>
								<td style="font-size: 12px; text-align: center;" >$<?php echo number_format($totale,2,'.',',');?></td>
							</tr>
							<tr class="table_dev devolucion">
								<td>Dirección</td>
								<td  colspan="7">
									<input type="text" onpaste="return false;"  class="form-control  comentario_<?php echo $item->id;?>" data-cliente="<?php echo $idCliente;?>" data-idrow="<?php echo $item->id;?>" value="<?php echo $item->comentario;?>" style="background: transparent; font-size:14px;" <?php echo $comentario_block; ?>>
								</td>		
							</tr>
							<?php
						} 
						foreach ($resultadoaccesorios->result() as $item) { 
							$totala=$item->cantidad*$item->costo;
							$totalgeneral=$totalgeneral+$totala;
							if ($item->serie_estatus==0) {
								$block_envio=1;
								$btn_delete_pro='<a class="btn btn-danger btn-sm deletepro_ss" onclick="deletepro(5,'.$item->id_accesoriod.')" ><i class="fas fa-trash"></i></a>';
							}else{
								$btn_delete_pro='';
							}
							?>
							<tr>
								<td style="font-size: 12px; text-align: center;">
									<span onclick="editarcantidad(5,<?php echo $block_buttonc;?>,<?php echo $item->id_accesoriod;?>,<?php echo $item->cantidad;?>)"><?php echo $item->cantidad;?></span>
									<a class="btn btn-danger btn-sm btndevolucion" onclick="devolucion(3,<?php echo $item->id_accesoriod;?>,<?php echo $item->cantidad;?>)" ><i class="fas fa-trash"></i></a>
									<?php echo $btn_delete_pro;?>
								</td>
								<!--<td style="font-size: 12px; text-align: center;" ><?php echo $this->ModeloCatalogos->obtenerbodega($item->serie_bodega);?></td><!--F2-->
								<td style="font-size: 12px; "><?php echo $item->no_parte;?></td>
								<td style="font-size: 12px; " colspan="3">
									<table border="1" width="100%">
										<tr>
											<td width="30%">TIPO</td>
											<td width="70%">ACCESORIO</td>		
										</tr>
										<tr>
											<td>MARCA</td>
											<td>KYOCERA</td>		
										</tr>
										<tr>
											<td>MODELO</td>
											<td><?php echo $item->nombre;?></td>		
										</tr>
										<?php if ($item->serie_estatus==0) {
														$block_envio=1;
													}else{
														$resuleserie=$this->ModeloCatalogos->getaccesorioserie($item->id_accesoriod);
														$arrayseries=array();
														foreach ($resuleserie->result() as $itemse) { 
															$arrayprocompra[]=array('pro'=>2,'conserie'=>$itemse->con_serie,'cantidad'=>$item->cantidad,'productoid'=>$item->id_accesorio,'serieId'=>$itemse->serieId,'seriename'=>$itemse->serie,'bodegaId'=>$item->serie_bodega,'precio'=>$item->costo);
														?>
															<tr>
																<td>SERIES</td>
																<td>
																	<?php echo $itemse->serie;?>
																	<a class="btn btn-danger btn-sm btndevolucion3" onclick="devolucion2(3,<?php echo $item->id_accesoriod;?>,<?php echo $itemse->serieId;?>)" ><i class="fas fa-trash"></i></a>
																</td>		
															</tr>
															<tr>
																<td>BODEGA</td>
																<td class="bodega" data-bodega="<?php echo $item->bodegaId; ?>">
																	<?php echo $this->ModeloCatalogos->obtenerbodega($item->serie_bodega);?>
																</td>		
															</tr>
														<?php
														}
														echo implode(" / ", $arrayseries);
													}?>
										<?php if ($item->garantia==1) { ?>
											<tr><td colspan="2" onclick="descartargarantia(3,<?php echo $item->id_accesoriod;?>)">Posible Garantia</td></tr>
										<?php } ?>
									</table>
								</td>
								<td style="font-size: 12px; text-align: center;" 
								onclick="editarprecio(5,<?php echo $block_button;?>,<?php echo $item->id_accesoriod;?>,<?php echo $item->costo;?>)"
								 >$<?php echo number_format($item->costo,2,'.',',');?></td>
								<td style="font-size: 12px; text-align: center;" >$<?php echo number_format($totala,2,'.',',');?></td>
							</tr>
							<tr class="table_dev">
								<td>Dirección</td>
								<td class="addaccesoriosvtd" colspan="7">
									<input type="text" onpaste="return false;" id="comentario2" name="comentario2" class="form-control comentario comentario_<?php echo $item->id_accesoriod;?>" data-cliente="<?php echo $idCliente;?>" data-idrow="<?php echo $item->id_accesoriod;?>" value="<?php echo $item->comentario;?>" style="background: transparent; font-size:14px;" <?php echo $comentario_block; ?> onclick="editardireccion2(<?php echo $idCliente;?>,<?php echo $item->id_accesoriod;?>)" required>
								</td>		
							</tr>
							<?php	
						}
						foreach ($resultadoaccesorios_dev->result() as $item) { 
							$totala=$item->cantidad*$item->costo;
							if ($item->serie_estatus==0) {
								$block_envio=1;
							}
							?>
							<tr class="table_dev devolucion">
								<td style="font-size: 12px; text-align: center;">
									<?php echo $item->cantidad;?>			
								</td>
								<!--<td style="font-size: 12px; text-align: center;" ><?php echo $this->ModeloCatalogos->obtenerbodega($item->serie_bodega);?></td><!--F2-->
								<td style="font-size: 12px; "><?php echo $item->no_parte;?></td>
								<td style="font-size: 12px; " colspan="3">
									<table border="1" width="100%">
										<tr>
											<td width="30%">TIPO</td>
											<td width="70%">ACCESORIO</td>		
										</tr>
										<tr>
											<td>MARCA</td>
											<td>KYOCERA</td>		
										</tr>
										<tr>
											<td>MODELO</td>
											<td><?php echo $item->nombre;?></td>		
										</tr>
										<?php if ($item->serie_estatus==0) {
														$block_envio=1;
													}else{
														$resuleserie=$this->ModeloCatalogos->getaccesorioserie($item->id_accesoriod);
														$arrayseries=array();
														foreach ($resuleserie->result() as $itemse) { ?>
															<tr>
																<td>SERIES</td>
																<td>
																	<?php echo $itemse->serie;?>
																</td>		
															</tr>
															<tr>
																<td>BODEGA</td>
																<td>
																	<?php echo $this->ModeloCatalogos->obtenerbodega($item->serie_bodega);?>
																</td>		
															</tr>
														<?php
														}
														echo implode(" / ", $arrayseries);
													}?>
									</table>
								</td>
								<td style="font-size: 12px; text-align: center;" >$<?php echo number_format($item->costo,2,'.',',');?></td>
								<td style="font-size: 12px; text-align: center;" >$<?php echo number_format($totala,2,'.',',');?></td>
							</tr>
							<tr class="table_dev devolucion">
								<td>Dirección</td>
								<td class="addaccesoriosvtd" colspan="7">
									<input type="text" onpaste="return false;" id="comentario" name="comentario3" class="form-control comentario_<?php echo $item->id_accesoriod;?>" data-cliente="<?php echo $idCliente;?>" data-idrow="<?php echo $item->id_accesoriod;?>" value="<?php echo $item->comentario;?>" style="background: transparent; font-size:14px;" <?php echo $comentario_block; ?> >
								</td>		
							</tr>
							<?php	
						} 
						foreach ($resultadoconsumibles->result() as $item) { 
							$costo=$item->costo_toner;

							$totalc=$item->cantidad*$costo;
							$totalgeneral=$totalgeneral+$totalc;
							$arrayprocompra[]=array('pro'=>3,'conserie'=>0,'cantidad'=>$item->cantidad,'productoid'=>$item->id_consumibles,'serieId'=>0,'seriename'=>$item->modelo,'bodegaId'=>0,'precio'=>$costo);
							?>
							<tr>
								<td style="font-size: 12px; text-align: center;">
									<span onclick="editarcantidad(4,<?php echo $block_buttonc;?>,<?php echo $item->id;?>,<?php echo $item->cantidad;?>)"><?php echo $item->cantidad;?></span>
									<a class="btn btn-danger btn-sm btndevolucion" onclick="devolucion(4,<?php echo $item->id;?>,<?php echo $item->cantidad;?>)" ><i class="fas fa-trash"></i></a>
								</td>
								<!--<td style="font-size: 12px; text-align: center;" onclick="editar_c(<?php echo $item->id;?>,'<?php echo $item->surtir;?>');"><?php echo $item->surtir;?></td><!--f3-->
								<td style="font-size: 12px; "><?php echo $item->parte;?></td>
								<td style="font-size: 12px; " colspan="3">
									<table border="1" width="100%">
										<tr>
											<td width="30%">TIPO</td>
											<td width="70%">Consumible</td>		
										</tr>
										<tr>
											<td>MARCA</td>
											<td>KYOCERA</td>		
										</tr>
										<tr>
											<td>MODELO</td>
											<td><?php echo $item->modelo;?></td>		
										</tr>
										<tr>
											<td>BODEGA</td>
											<td class="bodega" data-bodega="<?php echo $item->bodegaId; ?>"><?php echo $item->bodega;?></td>		
										</tr>
										
									</table>
								</td>
								<td style="font-size: 12px; text-align: center;" 
									onclick="editarprecio(4,<?php echo $block_button;?>,<?php echo $item->id;?>,<?php echo $costo;?>)" 
									>$<?php echo number_format($costo,3,'.',',');?></td>
								<td style="font-size: 12px; text-align: center;" >$<?php echo number_format($totalc,3,'.',',');?></td>
							</tr>
							<tr>
								<td>Dirección</td>
								<td class="addconsumiblevtd" colspan="7">
									<input type="text" onpaste="return false;" id="comentario4" name="comentario4" class="form-control comentario comentario_<?php echo $item->id;?>" data-cliente="<?php echo $idCliente;?>" data-idrow="<?php echo $item->id;?>" value="<?php echo $item->comentario;?>" style="background: transparent; font-size:14px;" <?php echo $comentario_block; ?> onclick="editardireccion2(<?php echo $idCliente;?>,<?php echo $item->id;?>)" required>
								</td>		
							</tr>
							<?php	
						}
						foreach ($resultadoconsumibles_dev->result() as $item) { 
							$costo=$item->costo_toner;

							$totalc=$item->cantidad*$costo;
							//$totalgeneral=$totalgeneral+$totalc;
							?>
							<tr class="table_dev devolucion">
								<td style="font-size: 12px; text-align: center;"><?php echo $item->cantidad;?>
									<a class="btn btn-danger btn-sm btndevolucion" onclick="devolucion(4,<?php echo $item->id;?>,<?php echo $item->cantidad;?>)" ><i class="fas fa-trash"></i></a>
								</td>
								<!--<td style="font-size: 12px; text-align: center;" onclick="editar_c(<?php echo $item->id;?>,'<?php echo $item->surtir;?>');"><?php echo $item->surtir;?></td><!--f3-->
								<td style="font-size: 12px; "><?php echo $item->parte;?></td>
								<td style="font-size: 12px; " colspan="3">
									<table border="1" width="100%">
										<tr>
											<td width="30%">TIPO</td>
											<td width="70%">Consumible</td>		
										</tr>
										<tr>
											<td>MARCA</td>
											<td>KYOCERA</td>		
										</tr>
										<tr>
											<td>MODELO</td>
											<td><?php echo $item->modelo;?></td>		
										</tr>
										
									</table>
								</td>
								<td style="font-size: 12px; text-align: center;" >$<?php echo number_format($costo,3,'.',',');?></td>
								<td style="font-size: 12px; text-align: center;" >$<?php echo number_format($totalc,3,'.',',');?></td>
							</tr>
							<tr class="table_dev devolucion">
								<td>Dirección</td>
								<td class="addconsumiblevtd" colspan="7">
									<input type="text" onpaste="return false;" id="comentario4" name="comentario4" class="form-control  comentario_<?php echo $item->id;?>" data-cliente="<?php echo $idCliente;?>" data-idrow="<?php echo $item->id;?>" value="<?php echo $item->comentario;?>" style="background: transparent; font-size:14px;" <?php echo $comentario_block; ?> >
								</td>		
							</tr>
							<?php	
						}

						// Tabla de ventasdetallesconsumibles   		
						foreach ($consumiblesventadetalles->result() as $item) { 
							$cf_serviciocId_tipo=$item->serviciocId_tipo;
							$cf_serviciocId=$item->serviciocId;
							$costo=$item->costo_toner;

							$totalc=$item->cantidad*$costo;
							$totalgeneral=$totalgeneral+$totalc;
							$arrayprocompra[]=array('pro'=>3,'conserie'=>0,'cantidad'=>$item->cantidad,'productoid'=>$item->idConsumible,'serieId'=>0,'seriename'=>$item->modelo,'bodegaId'=>0,'precio'=>$costo);
							?>
							<tr>
								<td style="font-size: 12px; text-align: center;">
									<span onclick="editarcantidad(2,<?php echo $block_buttonc;?>,<?php echo $item->id;?>,<?php echo $item->cantidad;?>)"><?php echo $item->cantidad;?></span>
									<?php if($item->foliotext==''){ ?>
										<a class="btn btn-danger btn-sm btndevolucion" onclick="devolucion(1,<?php echo $item->id;?>,<?php echo $item->cantidad;?>)" ><i class="fas fa-trash"></i></a>
									<?php }else{ ?>
										<a class="btn btn-danger btn-sm btndevolucion" href="<?php echo base_url().'index.php/ListadoFolio?foliotext='.$item->foliotext?>" target="_blank"><i class="fas fa-trash"></i></a>
									<?php } ?>
								</td>
								<!--<td style="font-size: 12px; text-align: center;"onclick="editar_cvd(<?php echo $item->id;?>,'<?php echo $item->surtir;?>');"><?php echo $item->surtir;?></td><!-- f4-->
								<td style="font-size: 12px; text-align: center;"><?php echo $item->parte;?></td>
								<td style="font-size: 12px; " colspan="3">
									<table border="1" width="100%">
										<tr>
											<td width="30%">TIPO</td>
											<td width="70%">Consumible</td>		
										</tr>
										<tr>
											<td>MARCA</td>
											<td>KYOCERA</td>		
										</tr>
										<tr>
											<td>MODELO</td>
											<td><?php echo $item->modelo;?></td>		
										</tr>
										<?php if($item->foliotext!=''){ ?>
											<tr>
												<td>FOLIO</td>
												<td><?php echo $item->foliotext;?></td>		
											</tr>
										<?php } ?>
										<tr>
											<td>BODEGA</td>
											<td class="bodega" data-bodega="<?php echo $item->bodegaId; ?>"><?php echo $item->bodega;?></td>		
										</tr>
										<?php if($item->idserie!=null){ 
											$resultse=$this->ModeloCatalogos->db3_getselectwheren('series_productos',array('serieId'=>$item->idserie));
											$seriee='';
											foreach ($resultse->result() as $itemse) {
												$seriee=$itemse->serie;
											}
											$resultseu=$this->ModeloCatalogos->db3_getselectwheren('asignacion_series_r_equipos',array('serieId'=>$item->idserie,'id_equipo'=>$item->idEquipo));
											$ubicacione='';
											foreach ($resultseu->result() as $itemseu) {
												$ubicacione=$itemseu->ubicacion;
											}
										?>
										<tr>
											<td>SERIE EQUIPO</td>
											<td><?php echo $seriee;?></td>		
										</tr>
										<tr>
											<td>UBICACION EQUIPO</td>
											<td><?php echo $ubicacione;?></td>		
										</tr>
										<?php }?>
										
									</table>
								</td>
								<td style="font-size: 12px; text-align: center;"  
									onclick="editarprecio(2,<?php echo $block_button;?>,<?php echo $item->id;?>,<?php echo $costo;?>)">
									$<?php echo number_format($costo,3,'.',',');?></td>
								<td style="font-size: 12px; text-align: center;" >$<?php echo number_format($totalc,2,'.',',');?></td>
							</tr>
							<tr class="table_dev">
								<td>Dirección</td>
								<td class="addconsumibletd" colspan="7">
									<input type="text" onpaste="return false;" id="comentario6" name="comentario6" class="form-control comentario comentario_<?php echo $item->id;?>" data-cliente="<?php echo $idCliente;?>" data-idrow="<?php echo $item->id;?>" value="<?php echo $item->comentario;?>" style="background: transparent; font-size:14px;" <?php echo $comentario_block; ?> onclick="editardireccion2(<?php echo $idCliente;?>,<?php echo $item->id;?>)" required>
								</td>		
							</tr>
						<?php	}
						foreach ($consumiblesventadetalles_dev->result() as $item) { 
							$costo=$item->costo_toner;
							$totalc=$item->cantidad*$costo;
							?>
							<tr class="table_dev devolucion">
								<td style="font-size: 12px; text-align: center;">
									<?php echo $item->cantidad;?>
								</td>
								<!--<td style="font-size: 12px; text-align: center;"onclick="editar_cvd(<?php echo $item->id;?>,'<?php echo $item->surtir;?>');"><?php echo $item->surtir;?></td><!-- f4-->
								<td style="font-size: 12px; text-align: center;"><?php echo $item->parte;?></td>
								<td style="font-size: 12px; " colspan="3">
									<table border="1" width="100%">
										<tr>
											<td width="30%">TIPO</td>
											<td width="70%">Consumible</td>		
										</tr>
										<tr>
											<td>MARCA</td>
											<td>KYOCERA</td>		
										</tr>
										<tr>
											<td>MODELO</td>
											<td><?php echo $item->modelo;?></td>		
										</tr>
										<?php if($item->idserie!=null){ 
											$resultse=$this->ModeloCatalogos->db3_getselectwheren('series_productos',array('serieId'=>$item->idserie));
											$seriee='';
											foreach ($resultse->result() as $itemse) {
												$seriee=$itemse->serie;
											}
											$resultseu=$this->ModeloCatalogos->db3_getselectwheren('alta_rentas_equipos',array('serieId'=>$item->idserie,'id_equipo'=>$item->idEquipo));
											$ubicacione='';
											foreach ($resultseu->result() as $itemseu) {
												$ubicacione=$itemseu->ubicacion;
											}
										?>
										<tr>
											<td>SERIE EQUIPO</td>
											<td><?php echo $seriee;?></td>		
										</tr>
										<tr>
											<td>UBICACION EQUIPO</td>
											<td><?php echo $ubicacione;?></td>		
										</tr>
										<?php }?>
										<?php if($item->foliotext!=null){ ?>
											<tr>
												<td>FOLIO</td>
												<td><?php echo $item->foliotext;?></td>		
											</tr>
										<?php }?>
										
									</table>
								</td>
								<td style="font-size: 12px; text-align: center;" >
									$<?php echo number_format($costo,3,'.',',');?></td>
								<td style="font-size: 12px; text-align: center;" >$<?php echo number_format($totalc,2,'.',',');?></td>
							</tr>
							<tr class="table_dev devolucion">
								<td>Dirección</td>
								<td class="addconsumibletd" colspan="7">
									<input type="text" onpaste="return false;" id="comentario" name="comentario7" class="form-control  comentario_<?php echo $item->id;?>" data-cliente="<?php echo $idCliente;?>" data-idrow="<?php echo $item->id;?>" value="<?php echo $item->comentario;?>" style="background: transparent; font-size:14px;" <?php echo $comentario_block; ?> >
								</td>		
							</tr>
						<?php	}	
					    foreach ($ventadetallesrefacion->result() as $item) { 
                                        
							$totalc=round($item->cantidad*$item->precioGeneral,2);
							$totalgeneral=$totalgeneral+$totalc;
							if ($item->serie_estatus==0) {
								$block_envio=1;
								$btn_delete_pro='<a class="btn btn-danger btn-sm deletepro_ss" onclick="deletepro(3,'.$item->id.')" ><i class="fas fa-trash"></i></a>';
							}else{
								$btn_delete_pro='';
							}
							?>
							<tr>
								<td style="font-size: 12px; text-align: center;">
									<span onclick="editarcantidad(3,<?php echo $block_buttonc;?>,<?php echo $item->id;?>,<?php echo $item->cantidad;?>)"><?php echo $item->cantidad;?></span>
									<a class="btn btn-danger btn-sm btndevolucion" onclick="devolucion(5,<?php echo $item->id;?>,<?php echo $item->cantidad;?>)" ><i class="fas fa-trash"></i></a>
									<?php echo $btn_delete_pro;?>	
								</td>
								<!--<td style="font-size: 12px; text-align: center;" onclick="editar_vdr(<?php echo $item->id;?>,'<?php echo $item->surtir;?>');"><?php echo $item->surtir;?></td>-->
								<td style="font-size: 12px; text-align: center;"><?php echo $item->codigo;?></td>
								<td style="font-size: 12px; " colspan="3">
									<table border="1" width="100%">
										<tr>
											<td width="30%">TIPO</td>
											<td width="70%">Refacción</td>		
										</tr>
										<tr>
											<td>MARCA</td>
											<td>KYOCERA</td>		
										</tr>
										<tr>
											<td>MODELO</td>
											<td><?php echo $item->modelo;?>
												
											</td>		
										</tr>
										
										<?php if ($item->serie_estatus==0) {
												$block_envio=1;
											}else{
												$resuleserie=$this->ModeloCatalogos->getrefaccionserie($item->id);
												$arrayseries=array();
												foreach ($resuleserie->result() as $itemse) { 
													$arrayprocompra[]=array('pro'=>4,'conserie'=>$itemse->con_serie,'cantidad'=>$item->cantidad,'productoid'=>$item->idRefaccion,'serieId'=>$itemse->serieId,'seriename'=>$itemse->serie,'bodegaId'=>$itemse->bodegaId,'precio'=>$item->precioGeneral);
												?>
													<tr>
														<td>SERIES</td>
														<td><?php echo $itemse->serie;?> <?php if($itemse->usado==1){ echo '<b>Usado</b>';}?><br> 
															<div class="btndevolucion tx-a-r">
																<div class="custom-control custom-switch">
																  <input type="checkbox" class="custom-control-input" id="ref_usado_<?php echo $itemse->id_asig;?>" onclick="ref_usado(<?php echo $itemse->id_asig;?>,1)" <?php if($itemse->usado==1){ echo 'checked';}?>>
																  <label class="custom-control-label" for="ref_usado_<?php echo $itemse->id_asig;?>">Usado</label>
																</div>
															</div>
														</td>
													</tr> 
													<tr>
														<td>BODEGA</td>
														<td class="bodega" data-bodega="<?php echo $itemse->bodegaId; ?>"><?php echo $itemse->bodega;?></td>
													</tr> 
												<?php }
												
											}
										?>	
										<tr>
											<td>EQUIPO</td>
											<td ondblclick="editarseriepol(<?php echo $item->id;?>,1)" ><?php echo $item->modeloeq.' '.$item->serieeq;?></td>		
										</tr>
										<?php if($item->garantia==1){?>
											<tr>
												<td colspan="2" onclick="descartargarantia(2,<?php echo $item->id;?>)">Posible Garantia</td>	
											</tr>
										<?php } ?>
									</table>
								</td>
								<td style="font-size: 12px; text-align: center;"  
									onclick="editarprecio(3,<?php echo $block_button;?>,<?php echo $item->id;?>,<?php echo $item->precioGeneral;?>)" >
									$<?php echo number_format($item->precioGeneral,2,'.',',');?></td>
								<td style="font-size: 12px; text-align: center;" >$<?php echo number_format($totalc,2,'.',',');?></td>
							</tr>
							<tr class="table_dev">
								<td>Dirección</td>
								<td class="addrefacciontd" colspan="7">
									<input type="text" onpaste="return false;" id="comentario8" name="comentario8" class="form-control  comentario comentario_<?php echo $item->id;?>" data-cliente="<?php echo $idCliente;?>" data-idrow="<?php echo $item->id;?>" value="<?php echo $item->comentario;?>" style="background: transparent;" <?php echo $comentario_block; ?> onclick="editardireccion2(<?php echo $idCliente;?>,<?php echo $item->id;?>)" required>
								</td>		
							</tr>
						<?php	}
						foreach ($ventadetallesrefacion_dev->result() as $item) { 
                                        
							$totalc=round($item->cantidad*$item->precioGeneral,2);
							if ($item->serie_estatus==0) {
								$block_envio=1;
							}
							?>
							<tr class="table_dev devolucion">
								<td style="font-size: 12px; text-align: center;">
									<?php echo $item->cantidad;?>	
								</td>
								<!--<td style="font-size: 12px; text-align: center;" onclick="editar_vdr(<?php echo $item->id;?>,'<?php echo $item->surtir;?>');"><?php echo $item->surtir;?></td>-->
								<td style="font-size: 12px; text-align: center;"><?php echo $item->codigo;?></td>
								<td style="font-size: 12px; " colspan="3">
									<table border="1" width="100%">
										<tr>
											<td width="30%">TIPO</td>
											<td width="70%">Refacción</td>		
										</tr>
										<tr>
											<td>MARCA</td>
											<td>KYOCERA</td>		
										</tr>
										<tr>
											<td>MODELO</td>
											<td><?php echo $item->modelo;?></td>		
										</tr>
										
										<?php if ($item->serie_estatus==0) {
												$block_envio=1;
											}else{
												$resuleserie=$this->ModeloCatalogos->getrefaccionserie($item->id);
												$arrayseries=array();
												foreach ($resuleserie->result() as $itemse) { ?>
													<tr>
														<td>SERIES</td>
														<td><?php echo $itemse->serie;?></td>
													</tr> 
													<tr>
														<td>BODEGA</td>
														<td><?php echo $itemse->bodega;?></td>
													</tr> 
												<?php }
												
											}
										?>	
										<tr>
											<td>EQUIPO</td>
											<td><?php echo $item->modeloeq.' '.$item->serieeq;?></td>		
										</tr>
									</table>
								</td>
								<td style="font-size: 12px; text-align: center;">
									$<?php echo number_format($item->precioGeneral,2,'.',',');?></td>
								<td style="font-size: 12px; text-align: center;" >$<?php echo number_format($totalc,2,'.',',');?></td>
							</tr>
							<tr class="table_dev devolucion">
								<td>Dirección</td>
								<td class="addrefacciontd" colspan="7">
									<input type="text" onpaste="return false;" id="comentario" name="comentario9" class="form-control  comentario_<?php echo $item->id;?>" data-cliente="<?php echo $idCliente;?>" data-idrow="<?php echo $item->id;?>" value="<?php echo $item->comentario;?>" style="background: transparent;" <?php echo $comentario_block; ?> >
								</td>		
							</tr>
						<?php	}
						
						if($v_ser_id>0){
							$html_v='';
							if($v_ser_tipo==1){//contrato
								$resultado=$this->ModeloAsignacion->getdatosrenta($v_ser_id,'','',0);
								$asignacionId_view=0;
								foreach ($resultado->result() as $itemser) {
									$servicio = $itemser->servicio;
									$fecha = $itemser->fecha;
									$tecnico = $itemser->tecnico.', '.$itemser->tecnico2;
									if($itemser->tservicio_a>0){
					                    $servicio = $itemser->poliza;
					                }
					                if($asignacionId_view!=$itemser->asignacionId){
					                	$asignacionId_view=$itemser->asignacionId;
										$html_v.='<tr>';
											$html_v.='<td style="font-size: 12px;">1</td>';
											$html_v.='<td style="font-size: 12px;">N/A</td>';
											$html_v.='<td colspan="3" style="font-size: 12px;">';
												$html_v.='<table border="1" width="100%">';
													$html_v.='<tr><td>TIPO SERVICIO<!--1--></td><td>'.$servicio.'</td></tr>';
													$html_v.='<tr><td>FECHA DEL SERVICIO</td><td>'.$fecha .'</td></tr>';
													$html_v.='<tr><td>TECNICO</td><td>'.$tecnico.'</td></tr>';
												$html_v.='</table>';
											$html_v.='</td>';
											$html_v.='<td style="font-size: 12px;">$ 0.00</td>';
											$html_v.='<td style="font-size: 12px;">$ 0.00</td></tr>';
										if($itemser->nr==0){
											//if($itemser->status<1){
											if($prefacturaId>0){
												?>
													<script type="text/javascript">
														$(document).ready(function($) {
															$('#fechaentrega').data('iser',1).attr({'readonly':true,'onclick':'notserentrega()'});
														});
													</script>
												<?php
											}
											//}
										}
									}
								}
							}
							if($v_ser_tipo==2){//poliza
								$resultado=$this->ModeloAsignacion->getdatospoliza($v_ser_id,'',0);
					            foreach ($resultado->result() as $itemser) {
					            	$servicio = $itemser->servicio;
									$fecha = $itemser->fecha;
									$tecnico = $itemser->tecnico.', '.$itemser->tecnico2;
									$html_v='';
									$html_v.='<tr>';
										$html_v.='<td style="font-size: 12px;">1</td>';
										$html_v.='<td style="font-size: 12px;">N/A</td>';
										$html_v.='<td colspan="3" style="font-size: 12px;">';
											$html_v.='<table border="1" width="100%">';
												$html_v.='<tr><td>TIPO SERVICIO<!--2--></td><td>'.$servicio.'</td></tr>';
												$html_v.='<tr><td>FECHA DEL SERVICIO</td><td>'.$fecha .'</td></tr>';
												$html_v.='<tr><td>TECNICO</td><td>'.$tecnico.'</td></tr>';
											$html_v.='</table>';
										$html_v.='</td>';
										$html_v.='<td style="font-size: 12px;">$ 0.00</td>';
										$html_v.='<td style="font-size: 12px;">$ 0.00</td></tr>';
									if($itemser->nr==0){
										//if($itemser->status<1){
										if($prefacturaId>0){
											?>
												<script type="text/javascript">
													$(document).ready(function($) {
														$('#fechaentrega').data('iser',1).attr({'readonly':true,'onclick':'notserentrega()'});
													});
												</script>
											<?php
										}
										//}
									}
					            }
							}
							if($v_ser_tipo==3){//Evento
								$resultado=$this->ModeloAsignacion->getdatoscliente($v_ser_id);
								foreach ($resultado->result() as $itemser) {
					            	$servicio = $itemser->servicio;
									$fecha = $itemser->fecha;
									$tecnico = $itemser->tecnico.', '.$itemser->tecnico2;
									if($itemser->tservicio_a>0){
					                    $servicio = $itemser->poliza;
					                }
					                $html_v='';
									$html_v.='<tr>';
										$html_v.='<td style="font-size: 12px;">1</td>';
										$html_v.='<td style="font-size: 12px;">N/A</td>';
										$html_v.='<td colspan="3" style="font-size: 12px;">';
											$html_v.='<table border="1" width="100%">';
												$html_v.='<tr><td>TIPO SERVICIO<!--3--></td><td>'.$servicio.'</td></tr>';
												$html_v.='<tr><td>FECHA DEL SERVICIO</td><td>'.$fecha .'</td></tr>';
												$html_v.='<tr><td>TECNICO</td><td>'.$tecnico.'</td></tr>';
											$html_v.='</table>';
										$html_v.='</td>';
										$html_v.='<td style="font-size: 12px;">$ 0.00</td>';
										$html_v.='<td style="font-size: 12px;">$ 0.00</td></tr>';
									if($itemser->nr==0){
										//if($itemser->status<1){
										if($prefacturaId>0){
											?>
												<script type="text/javascript">
													$(document).ready(function($) {
														$('#fechaentrega').data('iser',1).attr({'readonly':true,'onclick':'notserentrega()'});
													});
												</script>
											<?php
										}
										//}
									}
					            }
							}
							if($v_ser_tipo==4){//venta
								$resultado=$this->ModeloAsignacion->getdatosventas($v_ser_id);
								foreach ($resultado->result() as $itemser) {
					            	$servicio = $itemser->servicio;
									$fecha = $itemser->fecha;
									$tecnico = $itemser->tecnico;
									if($itemser->tservicio_a>0){
					                    $servicio = $itemser->poliza;
					                }
					                $html_v='';
									$html_v.='<tr>';
										$html_v.='<td style="font-size: 12px;">1</td>';
										$html_v.='<td style="font-size: 12px;">N/A</td>';
										$html_v.='<td colspan="3">';
											$html_v.='<table border="1" width="100%" style="font-size: 12px;">';
												$html_v.='<tr><td>TIPO SERVICIO<!--4--></td><td>'.$servicio.'</td></tr>';
												$html_v.='<tr><td>FECHA DEL SERVICIO</td><td>'.$fecha .'</td></tr>';
												$html_v.='<tr><td>TECNICO</td><td>'.$tecnico.'</td></tr>';
											$html_v.='</table>';
										$html_v.='</td>';
										$html_v.='<td style="font-size: 12px;">$ 0.00</td>';
										$html_v.='<td style="font-size: 12px;">$ 0.00</td></tr>';
									if($itemser->nr==0){
										//if($itemser->status<1){
										if($prefacturaId>0){
											?>
												<script type="text/javascript">
													$(document).ready(function($) {
														$('#fechaentrega').data('iser',1).attr({'readonly':true,'onclick':'notserentrega()'});
													});
												</script>
											<?php
										}
										//}
									}
					            }
							}
							echo $html_v;
						}
						if($cf_serviciocId>0){
							$html_v='';
							if($cf_serviciocId_tipo==0){
								$resultado=$this->ModeloAsignacion->getdatosrenta($cf_serviciocId,'','',0);
								$asignacionId_view=0;
								foreach ($resultado->result() as $itemser) {
									$servicio = $itemser->servicio;
									$fecha = $itemser->fecha;
									$tecnico = $itemser->tecnico.', '.$itemser->tecnico2;
									if($itemser->tservicio_a>0){
					                    $servicio = $itemser->poliza;
					                }
					                if($asignacionId_view!=$itemser->asignacionId){
					                	$asignacionId_view=$itemser->asignacionId;
										$html_v.='<tr>';
											$html_v.='<td style="font-size: 12px;">1</td>';
											$html_v.='<td style="font-size: 12px;">N/A</td>';
											$html_v.='<td colspan="3" style="font-size: 12px;">';
												$html_v.='<table border="1" width="100%">';
													$html_v.='<tr><td>TIPO SERVICIO<!--5--></td><td>'.$servicio.'</td></tr>';
													$html_v.='<tr><td>FECHA DEL SERVICIO</td><td>'.$fecha .'</td></tr>';
													$html_v.='<tr><td>TECNICO</td><td>'.$tecnico.'</td></tr>';
												$html_v.='</table>';
											$html_v.='</td>';
											$html_v.='<td style="font-size: 12px;">$ 0.00</td>';
											$html_v.='<td style="font-size: 12px;">$ 0.00</td></tr>';
										if($itemser->nr==0){
											//if($itemser->status<1){
											if($prefacturaId>0){
												?>
													<script type="text/javascript">
														$(document).ready(function($) {
															$('#fechaentrega').data('iser',1).attr({'readonly':true,'onclick':'notserentrega()'});
														});
													</script>
												<?php
											}
											//}
										}
									}
								}
							}
							if($cf_serviciocId_tipo==1){
								$resultado=$this->ModeloAsignacion->getdatospoliza($cf_serviciocId,'',0);
					            foreach ($resultado->result() as $itemser) {
					            	$servicio = $itemser->servicio;
									$fecha = $itemser->fecha;
									$tecnico = $itemser->tecnico.', '.$itemser->tecnico2;
									$html_v='';
									$html_v.='<tr>';
										$html_v.='<td style="font-size: 12px;">1</td>';
										$html_v.='<td style="font-size: 12px;">N/A</td>';
										$html_v.='<td colspan="3" style="font-size: 12px;">';
											$html_v.='<table border="1" width="100%">';
												$html_v.='<tr><td>TIPO SERVICIO<!--6--></td><td>'.$servicio.'</td></tr>';
												$html_v.='<tr><td>FECHA DEL SERVICIO</td><td>'.$fecha .'</td></tr>';
												$html_v.='<tr><td>TECNICO</td><td>'.$tecnico.'</td></tr>';
											$html_v.='</table>';
										$html_v.='</td>';
										$html_v.='<td style="font-size: 12px;">$ 0.00</td>';
										$html_v.='<td style="font-size: 12px;">$ 0.00</td></tr>';
									if($itemser->nr==0){
										//if($itemser->status<1){
										if($prefacturaId>0){
											?>
												<script type="text/javascript">
													$(document).ready(function($) {
														$('#fechaentrega').data('iser',1).attr({'readonly':true,'onclick':'notserentrega()'});
													});
												</script>
											<?php
										}
										//}
									}
					            }
							}
							echo $html_v;
						}
					?>
					
				</tbody>
				<tfoot>
					<tr><?php $tg_iva=$totalgeneral*0.16; ?>
						<td style="font-size: 12px; " colspan="1">Subtotal</td>
						<td style="font-size: 12px; " colspan="<?php if($siniva==1){ echo '4';}else{echo '2';}?>">$<?php echo number_format($totalgeneral,2,'.',',');?></td>
						<?php if($siniva!=1){ 
							$addiva='';
						?>
							<td style="font-size: 12px; "  class="quitariva">Iva</td>
							<td style="font-size: 12px; "  class="quitariva">$<?php echo number_format($tg_iva,2,'.',',');?></td>
						<?php }else{$tg_iva=0;
							$addiva='onclick="quitariva('.$ventaId.',0,1)" ';
						} ?>
						<td style="font-size: 12px; " <?php echo $addiva;?> >Total</td>
						<td style="font-size: 12px; " >$<?php 
															$total_general=round($tg_iva, 2)+round($totalgeneral, 2);
															$this->ModeloCatalogos->updateCatalogo('ventas',array('subtotal_general'=>round($totalgeneral,2),'iva_general'=>round($tg_iva,2),'total_general'=>$total_general),array('id'=>$proinven));
															echo number_format($total_general,2,'.',',');

														?></td>
					</tr>
				</tfoot>
			</table>
			<?php if($tipov==2){ ?>
				<label>Folio</label>
				<input type="text" class="form-control" id="folio_venta" onchange="folioventa(0)" value="<?php echo $folio_venta;?>" <?php echo $observaciones_block;?>>
			<?php } ?>
			<label for="observaciones" class="table_dev">Observaciones:</label>
			<textarea id="observaciones" class="form-control table_dev" <?php echo $observaciones_block;?> style="min-height: 150px;"><?php echo $observaciones;?></textarea>
		</form>
		<div class="row table_dev">
		    <div class="col-md-12">
		        <div class="col-md-12 text-right buttoresave">
		        	<?php 
		        		if($block_button==0){ 
		        			if($block_envio==0){ 
		        				echo '<button type="button" class="btn btn-success buttonenvio">Enviar</button>';
		        			}else{
		        				echo '<button type="button" class="btn btn-success" onclick="confir_envio()">Enviar</button>';
		        			} 
		        		}else{  	
		        			echo'<button type="button" class="btn btn-success buttoimprimir">Imprimir</button>';
		        	  	}  ?> 		
		        </div>
		    </div>
		</div>
		<div class="modal fade" id="modalconfirmacionenvio" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h5 class="modal-title" id="exampleModalLabel">Enviar</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      <div class="modal-body">
		        <div class="row">
		        	<div class="col-md-12">
		        		<h4>¿Desea enviar la prefactura?</h4>
		        	</div>
		        </div>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
		        <button type="button" class="btn btn-primary buttonenvioconfirm">Enviar</button>
		      </div>
		    </div>
		  </div>
		</div>
		<!-- f1 -->
		<div class="modal fade" id="modaleditar_d_equipo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h5 class="modal-title" id="exampleModalLabel">Modificar Surtir</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      <div class="modal-body">
		        <div class="row">
	              <div class="input-field col s12">
	              	<label for="newprecio">Nuevo surtir</label>
	              	<input id="idvdetalle" type="hidden" class="validate" placeholder="Texto" required>
	                <input id="surtir" type="text" class="validate" placeholder="Texto" required>
	                
	              </div>
	            </div>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
		        <button type="button" class="btn btn-primary" onclick="aceptar_d_e()">Aceptar</button>
		      </div>
		    </div>
		  </div>
		</div>
		<!-- f3 -->
		<div class="modal fade" id="modaleditarConsumible" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h5 class="modal-title" id="exampleModalLabel">Modificar Surtir</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      <div class="modal-body">
		        <div class="row">
	              <div class="input-field col s12">
	              	<label for="newprecio">Nuevo surtir</label>
	              	<input id="idadetalle" type="hidden" class="validate" placeholder="Texto" required>
	                <input id="surtira" type="text" class="validate" placeholder="Texto" required>
	                
	              </div>
	            </div>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
		        <button type="button" class="btn btn-primary" onclick="aceptar_c()">Aceptar</button>
		      </div>
		    </div>
		  </div>
		</div>
		<div class="modal fade" id="modaleditarvdc" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h5 class="modal-title" id="exampleModalLabel">Modificar Surtir</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      <div class="modal-body">
		        <div class="row">
	              <div class="input-field col s12">
	              	<label for="newprecio">Nuevo surtir</label>
	              	<input id="idvdcdetalle" type="hidden" class="validate" placeholder="Texto" required>
	                <input id="surtirvdc" type="text" class="validate" placeholder="Texto" required>
	                
	              </div>
	            </div>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
		        <button type="button" class="btn btn-primary" onclick="aceptar_vdc()">Aceptar</button>
		      </div>
		    </div>
		  </div>
		</div>
		<div class="modal fade" id="modaleditar_ra" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h5 class="modal-title" id="exampleModalLabel">Modificar Surtir</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      <div class="modal-body">
		        <div class="row">
	              <div class="input-field col s12">
	              	<label for="newprecio">Nuevo surtir</label>
	              	<input id="idradetalle" type="hidden" class="validate" placeholder="Texto" required>
	                <input id="surtirra" type="text" class="validate" placeholder="Texto" required>
	                
	              </div>
	            </div>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
		        <button type="button" class="btn btn-primary" onclick="aceptar_ra()">Aceptar</button>
		      </div>
		    </div>
		  </div>
		</div>
		<!-- f6-->
		<div class="modal fade" id="modaleditar_vdr" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h5 class="modal-title" id="exampleModalLabel">Modificar Surtir</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      <div class="modal-body">
		        <div class="row">
	              <div class="input-field col s12">
	              	<label for="newprecio">Nuevo surtir</label>
	              	<input id="idvdrdetalle" type="hidden" class="validate" placeholder="Texto" required>
	                <input id="surtirvdr" type="text" class="validate" placeholder="Texto" required>
	                
	              </div>
	            </div>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
		        <button type="button" class="btn btn-primary" onclick="aceptar_vdr()">Aceptar</button>
		      </div>
		    </div>
		  </div>
		</div>
		<div class="modal fade" id="modaldirecciones" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		  <div class="modal-dialog modal-xl" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h5 class="modal-title" id="exampleModalLabel">Direcciones</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      <div class="modal-body">
		        <div class="row">
		        	<div class="col-md-12 viewdirecciones" style="font-size: 12px;">
		        		
		        	</div>
		        </div>
		        
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
		      </div>
		    </div>
		  </div>
		</div>
	</body>
	<style type="text/css">
		.error{color: red;font-size: 10px;}
	</style>
	<script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/sweetalert/dist/sweetalert.min.js"></script>
	<link href="<?php echo base_url(); ?>app-assets/vendors/confirm/jquery-confirm.min.css" type="text/css" rel="stylesheet">
	<script src="<?php echo base_url(); ?>app-assets/vendors/confirm/jquery-confirm.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/jquery-validation/jquery.validate.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/jquery-validation/additional-methods.min.js"></script>
    <link href="<?php echo base_url(); ?>app-assets/vendors/data-tables/css/jquery.dataTables.min.1.11.4.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/data-tables/js/jquery.dataTables.min.1.11.4.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/MaskFocus/MaskedPassword.js"></script>
	<script src="<?php echo base_url(); ?>public/js/validaprefactura.js?v=<?php echo date('YmdGi')?>"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>public/js/prefacturasg.js?v=<?php echo date('YmdGi');?>" ></script>
	<script type="text/javascript">
		var base_url = $('#base_url').val();
		$(document).ready(function(){ 
			$('.buttonenvio').click(function(event) {

				var srfc=$('#rfc_id option:selected').val();
				if($('#formprefactura').valid()){
					<?php if($this->idpersonal==50){ ?>
						var validp=1;
					<?php }else{ ?>
						var validp=validpre();
					<?php } ?>
					if(validp==1){
						$('#modalconfirmacionenvio').modal();
					}
					
				}else{
					swal("Advertencia", "Seleccione los campos requeridos", "error");

				}
			});
			$('.buttonenvioconfirm').click(function(event) {
				actualizarfechae(<?php echo $ventaId;?>,1);
				$( ".buttonenvioconfirm" ).prop( "disabled", true );
		        setTimeout(function(){ 
		             $(".buttonenvioconfirm" ).prop( "disabled", false );
		        }, 10000);
				//========================================
					var comen_e_row = [];
					var comentarios_e_row   = $(".addequiposvtd");
					comentarios_e_row.each(function(){ 
						item = {};
						item['id'] = $(this).find("input[id*='comentario0']").data('idrow');
						item['comen'] = $(this).find("input[id*='comentario0']").val().replace('&','----');
						comen_e_row.push(item);
					});
					aInfocome_e   = JSON.stringify(comen_e_row);
				//=========================================
				//========================================
					var comen_ea_row = [];
					var comentarios_ea_row   = $(".addaccesoriosvtd");
					comentarios_ea_row.each(function(){ 
						item = {};
						item['id'] = $(this).find("input[id*='comentario2']").data('idrow');
						item['comen'] = $(this).find("input[id*='comentario2']").val().replace('&','----');
						comen_ea_row.push(item);
					});
					aInfocome_ea   = JSON.stringify(comen_ea_row);
				//=========================================
				//========================================
					var comen_ec_row = [];
					var comentarios_ec_row   = $(".addconsumiblevtd");
					comentarios_ec_row.each(function(){ 
						item = {};
						item['id'] = $(this).find("input[id*='comentario4']").data('idrow');
						item['comen'] = $(this).find("input[id*='comentario4']").val().replace('&','----');
						comen_ec_row.push(item);
					});
					aInfocome_ec   = JSON.stringify(comen_ec_row);
				//=========================================
				//========================================
					var comen_c_row = [];
					var comentarios_c_row   = $(".addconsumibletd");
					comentarios_c_row.each(function(){ 
						item = {};
						item['id'] = $(this).find("input[id*='comentario6']").data('idrow');
						item['comen'] = $(this).find("input[id*='comentario6']").val().replace('&','----');
						comen_c_row.push(item);
					});
					aInfocome_c   = JSON.stringify(comen_c_row);
				//=========================================
				//========================================
					var comen_r_row = [];
					var comentarios_r_row   = $(".addrefacciontd");
					comentarios_r_row.each(function(){ 
						item = {};
						item['id'] = $(this).find("input[id*='comentario8']").data('idrow');
						item['comen'] = $(this).find("input[id*='comentario8']").val().replace('&','----');
						comen_r_row.push(item);
					});
					aInfocome_r   = JSON.stringify(comen_r_row);
				//=========================================
					var obser=$('#observaciones').val();
						obser=obser.replace(/[&'"]/g, ' ');
				var datos = $('#formprefactura').serialize()+'&ventaequipos='+aInfocome_e+'&ventaeqaccesorios='+aInfocome_ea+'&ventaeqconsumible='+aInfocome_ec+'&ventaconsumibles='+aInfocome_c+'&ventarefacciones='+aInfocome_r+'&observaciones='+obser;
				$.ajax({
                    type:'POST',
                    url: base_url+'Prefactura/save',
                    data: datos,
                    async: false,
                    statusCode:{
                            404: function(data){
                                swal("Error", "404", "error");
                            },
                            500: function(){
                                swal("Error", "500", "error"); 
                            }
                        },
                        success:function(data){
                            swal("Éxito", "Pre factura enviada. Por favor recargue la vista de listado de ventas", "success");
                            setTimeout(function(){ window.location.href=''; }, 3000);
                         

                        }
                    });   
			});

			$('.buttoimprimir').click(function(event) {
				//window.print();
				var ventaId = $('#ventaId').val();
				window.location.href = base_url+"index.php/Prefactura/view/"+ventaId+"/1";
			});
			
			<?php if ($rfc_id>0) { ?>
				selectrcfv(<?php echo $rfc_id;?>);
			<?php }else{ ?>
				obtenercargo();
			<?php } ?>
			<?php
				if($prefacturaId==0){
					$row_rfc=0;
					$row_rfc_v=0; 
					foreach ($rfc_datos as $item) { 
						$row_rfc++;
						$row_rfc_v=$item->id;
					}
					if($row_rfc==1){
						?>
							$('#rfc_id').val(<?php echo $row_rfc_v?>).change();
						<?php

					} 
				}
			?>
		});
		//f1
		function editar_d_e(id,text) {
		    $('#modaleditar_d_equipo').modal();
		    $('#idvdetalle').val(id);
		    $('#surtir').val(text);
		}
		//f2
		function editar_ra(id,text) {
			$('#modaleditar_ra').modal();
		    $('#idradetalle').val(id);
		    $('#surtirra').val(text);
		}
	    // f3
		function editar_c(id,text) {
		    $('#modaleditarConsumible').modal();
		    $('#idadetalle').val(id);
		    $('#surtira').val(text);	    
		}
		// f4
		function editar_cvd(id,text) {
			$('#modaleditarvdc').modal();
		    $('#idvdcdetalle').val(id);
		    $('#surtirvdc').val(text);
		}
		// f6
		function editar_vdr(id,text) {
		 	$('#modaleditar_vdr').modal();
		    $('#idvdrdetalle').val(id);
		    $('#surtirvdr').val(text);
		 }
		// f1
		function aceptar_d_e() {
		    var idv = $('#idvdetalle').val();
		    var sur = $('#surtir').val();	
			$.ajax({
                type:'POST',
                url: base_url+'Prefactura/updatevequipodetalle',
                data:{id:idv,surtir:sur},
                async: false,
                statusCode:{
                        404: function(data){
                            swal("Error", "404", "error");
                        },
                        500: function(){
                            swal("Error", "500", "error"); 
                        }
                    },
                    success:function(data){
                        swal("Éxito", "Se actulizo correctamente", "success");
                        setTimeout(function(){ window.location.href=''; }, 3000);
                    }
            });  
        }    
        // f2
        function aceptar_ra(argument) {
        	var idv = $('#idradetalle').val();
		    var sur = $('#surtirra').val(); 	
			$.ajax({
                type:'POST',
                url: base_url+'Prefactura/updateaccesoriosventa',
                data:{id:idv,surtir:sur},
                async: false,
                statusCode:{
                        404: function(data){
                            swal("Error", "404", "error");
                        },
                        500: function(){
                            swal("Error", "500", "error"); 
                        }
                    },
                    success:function(data){
                        swal("Éxito", "Se actulizo correctamente", "success");
                        setTimeout(function(){ window.location.href=''; }, 3000);
                    }
            });
        }
        //f3
        function aceptar_c(){ 
		    var idv = $('#idadetalle').val();
		    var sur = $('#surtira').val(); 	
			$.ajax({
                type:'POST',
                url: base_url+'Prefactura/updatevequipodetalleaccesorio',
                data:{id:idv,surtir:sur},
                async: false,
                statusCode:{
                        404: function(data){
                            swal("Error", "404", "error");
                        },
                        500: function(){
                            swal("Error", "500", "error"); 
                        }
                    },
                    success:function(data){
                        swal("Éxito", "Se actulizo correctamente", "success");
                        setTimeout(function(){ window.location.href=''; }, 3000);
                    }
            });
		}
		//f4
        function aceptar_vdc(){ 
		    var idv = $('#idvdcdetalle').val();
		    var sur = $('#surtirvdc').val(); 	
			$.ajax({
                type:'POST',
                url: base_url+'Prefactura/updateconsumiblesventadetalles',
                data:{id:idv,surtir:sur},
                async: false,
                statusCode:{
                        404: function(data){
                            swal("Error", "404", "error");
                        },
                        500: function(){
                            swal("Error", "500", "error"); 
                        }
                    },
                    success:function(data){
                        swal("Éxito", "Se actulizo correctamente", "success");
                        setTimeout(function(){ window.location.href=''; }, 3000);
                    }
            });
		}
		//f6
        function aceptar_vdr(){ 
		    var idv = $('#idvdrdetalle').val();
		    var sur = $('#surtirvdr').val(); 	
			$.ajax({
                type:'POST',
                url: base_url+'Prefactura/updateventas_has_detallesRefacciones',
                data:{id:idv,surtir:sur},
                async: false,
                statusCode:{
                        404: function(data){
                            swal("Error", "404", "error");
                        },
                        500: function(){
                            swal("Error", "500", "error"); 
                        }
                    },
                    success:function(data){
                        swal("Éxito", "Se actulizo correctamente", "success");
                        setTimeout(function(){ window.location.href=''; }, 3000);
                    }
            });
		}
		function selectrcf() {
			var rfc_id = $('#rfc_id option:selected').val();
			
			var rfc = $('#rfc_id option:selected').data('rfc');
			if(rfc=='XAXX010101000'){
				$('.quitariva').attr('onclick', 'quitariva(<?php echo $ventaId ?>,0,0)');
				$('#metodopagoId').val(1);
                $('#formapagoId').val(21);
                $('#usocfdiId').val(22);
			}else{
				$('.quitariva').attr('onclick', 'quitariva(<?php echo $ventaId ?>,0,0)');
			}
			var razonsocial = $('#rfc_id option:selected').data('razonsocial');
			$('#razon_social').val(razonsocial);
			var numext = $('#rfc_id option:selected').data('numext');
			$('#num_ext').val(numext);
			var colonia = $('#rfc_id option:selected').data('colonia');
			$('#colonia').val(colonia);
			var calle = $('#rfc_id option:selected').data('calle');
			$('#calle').val(calle);
			var dciudad = $('#rfc_id option:selected').data('dciudad');
			$('#d_ciudad').val(dciudad);
			var destado = $('#rfc_id option:selected').data('destado');
			$('#d_estado').val(destado);
			//selectrcfv(rfc_id);
		}
		function selectrcfv(rfc_id){
			$.ajax({
                url: base_url+'Prefactura/selecciontrcf',
                dataType: 'json',
                data:{id:rfc_id},
                statusCode:{
                        404: function(data){
                            swal("Error", "404", "error");
                        },
                        500: function(){
                            swal("Error", "500", "error"); 
                        }
                    },
                    success:function(data){
                    	var datos=data;
                        datos.forEach(function(r) {
                        	if(r.rfc=='XAXX010101000'){
                        		$('#razon_social').val(r.razon_social+' ('+r.empresa+')');
                        		$('.quitariva').attr('onclick', 'quitariva(<?php echo $ventaId ?>,0,0)');
                        		<?php if ($rfc_id>0) { }else{ ?>
                        		$('#metodopagoId').val(1);
                        		$('#formapagoId').val(21);
                        		$('#usocfdiId').val(22);
                        		<?php } ?>
                        	}else{
                        		$('#razon_social').val(r.razon_social);
                        	}
                        
                        <?php //if ($rfc_id==0) { ?>
	                        $('#num_ext').val(r.num_ext);
	                        $('#colonia').val(r.colonia);
	                        $('#calle').val(r.calle);
	                        $('#d_ciudad').val(r.municipio);
	                        $('#d_estado').val(r.estado+' C.P.'+r.cp);
                        <?php //} ?>
                        });
                    }
            });
		}
		function editardireccion(cliente){
			if($('#domicilio_entrega').val().length == 0){
				$('#modaldirecciones').modal();
			}
			$.ajax({
				type:'POST',
                url: base_url+'PolizasCreadas/pintardireccionesclientes',
                data:{clienteid:cliente},
                statusCode:{
                        404: function(data){
                            swal("Error", "404", "error");
                        },
                        500: function(){
                            swal("Error", "500", "error"); 
                        }
                    },
                    success:function(data){
                    	$('.viewdirecciones').html(data);
                    	$('.adddirecciones').click(function(event) {
                    		var direccion = $(this).html();
                    		//console.log(direccion);
                    		$('#domicilio_entrega').val(direccion);
                    		$('.comentario').val(direccion);
                    	});
                    	$('#table_pinta_dir').dataTable();

                    }
            });
           	
		}
		function editardireccion2(cliente,id){
			if($('.comentario_'+id).prop("readonly")==false){
				if($('.comentario_'+id).val().length == 0){
					$('#modaldirecciones').modal();
					//$('#modaldirecciones').modal('open');
				}
				$.ajax({
					type:'POST',
	                url: base_url+'PolizasCreadas/pintardireccionesclientes',
	                data:{clienteid:cliente},
	                statusCode:{
	                        404: function(data){
	                            swal("Error", "404", "error");
	                        },
	                        500: function(){
	                            swal("Error", "500", "error"); 
	                        }
	                    },
	                    success:function(data){
	                    	$('.viewdirecciones').html(data);
	                    	$('.adddirecciones').click(function(event) {
	                    		var direccion = $(this).html();
	                    		//console.log(direccion);
	                    		$('.comentario_'+id).val(direccion);
	                    	});
	                    	$('#table_pinta_dir').dataTable();

	                    }
	            });
			}
           	
		}
		function obtenercargo(){
			var contacto=$('#contactoId option:selected').data('contacto');
			if (contacto==1) {
				var contactoid=$('#contactoId option:selected').val();
				//$('#telId option:selected').val(contactoid);
				$("#telId option").removeAttr("selected");
				$("#telId option[value="+contactoid+"]").attr('selected', 'selected');

				var puesto=$('#contactoId option:selected').data('puesto');

				$('#cargo').val(puesto);

				var email=$('#contactoId option:selected').data('email');
				$('.addemail').html(email);
				var dirid=$('#contactoId option:selected').data('dirid');
				var dir=$('#contactoId option:selected').data('dir');
				if(dirid>0){
					console.log(dir);
					$('.comentario').val(dir);
				}
			}
		}
		function editarprecio(tipo,editar,id,precio){
			var idCliente=$('#idCliente').val();
			if (editar==0) {
				editarpreciorow(tipo,editar,id,precio);
			}else{
				<?php if($row_vfac==0){ ?>
					editarpreciorow(tipo,editar,id,precio);
				<?php }else{ ?>
					$.alert({boxWidth: '40%',useBootstrap: false,title: 'Advertencia!',content: 'Tiene una factura relacionada cancele antes de continuar'}); 
				<?php } ?>
			}
		}
		function editarpreciorow(tipo,editar,id,precio){
			var idCliente=$('#idCliente').val();				
				$.confirm({
					        boxWidth: '40%',
					        useBootstrap: false,
					        icon: 'fa fa-warning',
					        title: 'Atención!',
					        content: 'Se necesita permisos de administrador<br>'+
					                 '<input type="text" placeholder="Contraseña" id="contrasena" name="contrasena" class="name form-control" required /><br>'+
					                 'Precio<br>'+
					                 '<input type="number" placeholder="Precio" id="newprecio" class="form-control" value="'+precio+'"  /><br>',
					        type: 'red',
					        typeAnimated: true,
					        buttons:{
					            confirmar: function (){
					                //var pass=$('#contrasena').val();
					                var pass=$("input[name=contrasena]").val();
					                precio=$('#newprecio').val();
					                if (pass!='') {
					                     $.ajax({
					                        type:'POST',
					                        url: base_url+"index.php/Sistema/solicitarpermiso/"+idCliente,
					                        data: {
					                            pass:pass
					                        },
					                        success: function (response){
					                                var respuesta = parseInt(response);
					                                if (respuesta==1) {
					                //===================================================
					                    
					                    //console.log(aInfoa);
					                        $.ajax({
					                            type:'POST',
					                            url: base_url+"index.php/Generales/editarprecios",
					                            data: {
					                                tipot:tipo,
					                                idrow:id,
					                                newprecio:precio
					                            },
					                            success:function(response){  
					                                
					                                swal("Éxito!", "Se ha Modificado", "success");
					                                setTimeout(function(){ 
														location.reload();
													}, 1000);

					                            }
					                        });
					                    
					                //================================================
					                                }else{
					                                    $.alert({
					                                    boxWidth: '30%',
					                                    useBootstrap: false,
					                                    title: 'Advertencia!',
					                                    content: 'No tiene permisos'}); 
					                                }
					                        },
					                        error: function(response){
					                            $.alert({
					                                    boxWidth: '30%',
					                                    useBootstrap: false,
					                                    title: 'Error!',
					                                    content: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema'});   
					                             
					                        }
					                    });
					                    
					                }else{
					                    $.alert({
					                                    boxWidth: '30%',
					                                    useBootstrap: false,
					                                    title: 'Advertencia!',
					                                    content: 'Ingrese una contraseña'}); 
					                }
					            },
					            cancelar: function () 
					            {
					                
					            }
					        }
					    });
				setTimeout(function(){
		            new MaskedPassword(document.getElementById("contrasena"), '\u25CF');
		            
		        },1000);
			
		}
		function editarcantidad(tipo,editar,id,cantidad){
			var idCliente=$('#idCliente').val();
			if (editar==0) {
				editarcantidadrow(tipo,editar,id,cantidad);
			}else{
				<?php if($row_vfac==0){ ?>
					//editarcantidadrow(tipo,editar,id,cantidad);
				<?php }else{ ?>
					//$.alert({boxWidth: '40%',useBootstrap: false,title: 'Advertencia!',content: 'Tiene una factura relacionada cancele antes de continuar'}); 
				<?php } ?>
			}
		}
		function editarcantidadrow(tipo,editar,id,cantidad){
			var idCliente=$('#idCliente').val();				
				$.confirm({
					        boxWidth: '40%',
					        useBootstrap: false,
					        icon: 'fa fa-warning',
					        title: 'Atención!',
					        content: 'Se necesita permisos de administrador<br>'+
					                 '<input type="text" placeholder="Contraseña" id="contrasena" name="contrasena" class="name form-control" required /><br>'+
					                 'Cantidad<br>'+
					                 '<input type="number" placeholder="Cantidad" id="newcantidad" class="form-control" value="'+cantidad+'"  /><br>',
					        type: 'red',
					        typeAnimated: true,
					        buttons:{
					            confirmar: function (){
					                //var pass=$('#contrasena').val();
					                var pass=$("input[name=contrasena]").val();
					                cantidad=$('#newcantidad').val();
					                if (pass!='') {
					                     $.ajax({
					                        type:'POST',
					                        url: base_url+"index.php/Sistema/solicitarpermiso/"+idCliente,
					                        data: {
					                            pass:pass
					                        },
					                        success: function (response){
					                                var respuesta = parseInt(response);
					                                if (respuesta==1) {
					                //===================================================
					                    
					                    //console.log(aInfoa);
					                        $.ajax({
					                            type:'POST',
					                            url: base_url+"index.php/Generales/editarcantidad",
					                            data: {
					                                tipot:tipo,
					                                idrow:id,
					                                newcantidad:cantidad
					                            },
					                            success:function(response){  
					                                
					                                swal("Éxito!", "Se ha Modificado", "success");
					                                setTimeout(function(){ 
														location.reload();
													}, 1000);

					                            }
					                        });
					                    
					                //================================================
					                                }else{
					                                    $.alert({
					                                    boxWidth: '30%',
					                                    useBootstrap: false,
					                                    title: 'Advertencia!',
					                                    content: 'No tiene permisos'}); 
					                                }
					                        },
					                        error: function(response){
					                            $.alert({
					                                    boxWidth: '30%',
					                                    useBootstrap: false,
					                                    title: 'Error!',
					                                    content: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema'});   
					                             
					                        }
					                    });
					                    
					                }else{
					                    $.alert({
					                                    boxWidth: '30%',
					                                    useBootstrap: false,
					                                    title: 'Advertencia!',
					                                    content: 'Ingrese una contraseña'}); 
					                }
					            },
					            cancelar: function () 
					            {
					                
					            }
					        }
					    });
				setTimeout(function(){
		            new MaskedPassword(document.getElementById("contrasena"), '\u25CF');
		            
		        },1000);
			
		}
		function quitariva(id,tipo,sincon){
			//sincon 0 sin iva 1 con iva
			if(sincon==0){
				var label='Desea quitar el iva? ';
			}else{
				var label='Desea agregar el iva? ';
			}
			$.confirm({
		        boxWidth: '41%',
		        useBootstrap: false,
		        icon: 'fa fa-warning',
		        title: 'Quitar iva',
		        content: '<label>'+label+'<br>Se necesita permisos de administrador</label><br><input id="password" type="password" class="validate form-control" autocomplete="new-password" placeholder="Contraseña" required>',
		        type: 'red',
		        typeAnimated: true,
		        buttons:{
		            confirmar: function (){
		                var pass=$('#password').val();
		                if (pass!='') {
		                    //if (precio>0) {
		                        $.ajax({
		                            type:'POST',
		                            url: base_url+'Cotizaciones/quitariva',
		                            data: {
				                        id: id,
				                        pass: pass,
				                        tipo: tipo,
				                        sincon:sincon
		                                },
		                                async: false,
		                                statusCode:{
		                                    404: function(data){
		                                        swal("Error", "404", "error");
		                                    },
		                                    500: function(){
		                                        swal("Error", "500", "error"); 
		                                    }
		                                },
		                                success:function(data){
		                                  if (data==1) {
		                                    swal("Hecho!", "Realizado", "success");
		                                    setTimeout(function(){ window.location.href=''; }, 3000);
		                                  }else{
		                                    swal("Error", "No tiene permiso", "error"); 
		                                  }

		                                }
		                            });
		                    //}else{
		                    //    swal("Error", "El precio no debe de ser menor o igual a cero", "error"); 
		                    //}
		                }else{
		                    swal("Error", "Debe de ingresar una contraseña", "error"); 
		                }  
		            },
		            cancelar: function (){
		            }
		        }
		    });
			
		}
		function actualizarfechae(idventa,save){
			var fechaentrega = $('#fechaentrega').val();
			var fecha=fechaentrega;
			var fecha=fecha.split("-");
			//if(fecha[0]>2021){
				if(save==0){
					$.confirm({
				        boxWidth: '40%',
				        useBootstrap: false,
				        icon: 'fa fa-warning',
				        title: 'Atención!',
				        content: '¿Confirma la edición de la fecha de entrega a <b>'+fechaentrega+'</b>?',
				        type: 'red',
				        typeAnimated: true,
				        buttons:{
				            confirmar: function (){
				                //===================================================
				                    
				                    //console.log(aInfoa);
				                        $.ajax({
				                            type:'POST',
				                            url: base_url+"index.php/Generales/editarfechaentrega",
				                            data: {
				                                ventaid:idventa,
				                                fentrega:fechaentrega,
				                                tipo:1
				                            },
				                            success:function(response){  
				                                
				                                swal("Éxito!", "Se ha Modificado", "success");
				                                setTimeout(function(){ 
													location.reload();
												}, 1000);

				                            }
				                        });
				                    
				                //================================================
				                 
				            },
				            cancelar: function () 
				            {
				                
				            }
				        }
				    });
				}
				if(save==1){
					$.ajax({
                        type:'POST',
                        url: base_url+"index.php/Generales/editarfechaentrega",
                        data: {
                            ventaid:idventa,
                            fentrega:fechaentrega,
                            tipo:1
                        },
                        success:function(response){  
                            

                        }
                    });
				}
			//}
		}
		function editardatosfiscales(){
			<?php if($row_vfac==0){ ?>
				var idCliente=$('#idCliente').val();
					var rfc_id = $('#rfc_id').html();
					var metodopagoId = $('#metodopagoId').html();
					var formapagoId = $('#formapagoId').html();
					var usocfdiId = $('#usocfdiId').html();
					$.confirm({
					        boxWidth: '60%',
					        useBootstrap: false,
					        icon: 'fa fa-warning',
					        title: 'Atención!',
					        content: 'Se necesita permisos de administrador<br>'+
					                 '<input type="text" placeholder="Contraseña" id="contrasena" name="contrasena" class="name form-control" required />'+
					                 '<div class="row">\
					                 	<div class="col-md-6">\
					                 		RFC:<br>\
					                 		<select id="new_rfc_id" class="form-control" onchange="selectrcf2()">'+rfc_id+'</select>\
					                 	</div>\
					                 	<div class="col-md-6">\
					                 		Metodo de pago:<br>\
					                 		<select id="new_metodopagoId" class="form-control">'+metodopagoId+'</select>\
					                 	</div>\
					                 </div>\
					                 <div class="row">\
					                 	<div class="col-md-6">\
					                 		Forma de pago:<br>\
					                 		<select id="new_formapagoId" class="form-control">'+formapagoId+'</select>\
					                 	</div>\
					                 	<div class="col-md-6">\
					                 		Uso de CFDI:<br>\
					                 		<select id="new_usocfdiId" class="form-control">'+usocfdiId+'</select>\
					                 	</div>\
					                 </div>',
					        type: 'red',
					        typeAnimated: true,
					        buttons:{
					            confirmar: function (){
					            	var new_rfc_id=$('#new_rfc_id option:selected').val();
					            	var new_metodopagoId=$('#new_metodopagoId option:selected').val();
					            	var new_formapagoId=$('#new_formapagoId option:selected').val();
					            	var new_usocfdiId=$('#new_usocfdiId option:selected').val();
					                //var pass=$('#contrasena').val();
					                var pass=$("input[name=contrasena]").val();
					                precio=$('#newprecio').val();
					                if (pass!='') {
					                     $.ajax({
					                        type:'POST',
					                        url: base_url+"index.php/Sistema/solicitarpermiso/"+idCliente,
					                        data: {
					                            pass:pass
					                        },
					                        success: function (response){
					                                var respuesta = parseInt(response);
					                                if (respuesta==1) {
					                //===================================================
					                    
					                    //console.log(aInfoa);
					                        $.ajax({
					                            type:'POST',
					                            url: base_url+"index.php/Generales/editardatosfiscales",
					                            data: {
					                                prefacturaId:<?php echo $prefacturaId;?>,
					                                rfc:new_rfc_id,
					                                metodopagoId:new_metodopagoId,
													formapagoId:new_formapagoId,
													usocfdiId:new_usocfdiId,
					                            },
					                            success:function(response){  
					                                
					                                swal("Éxito!", "Se ha Modificado", "success");
					                                setTimeout(function(){ 
														location.reload();
													}, 1000);

					                            }
					                        });
					                    
					                //================================================
					                                }else{
					                                    $.alert({
					                                    boxWidth: '30%',
					                                    useBootstrap: false,
					                                    title: 'Advertencia!',
					                                    content: 'No tiene permisos'}); 
					                                }
					                        },
					                        error: function(response){
					                            $.alert({
					                                    boxWidth: '30%',
					                                    useBootstrap: false,
					                                    title: 'Error!',
					                                    content: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema'});   
					                             
					                        }
					                    });
					                    
					                }else{
					                    $.alert({
					                                    boxWidth: '30%',
					                                    useBootstrap: false,
					                                    title: 'Advertencia!',
					                                    content: 'Ingrese una contraseña'}); 
					                }
					            },
					            cancelar: function () 
					            {
					                
					            }
					        }
					    });
				setTimeout(function(){
		            new MaskedPassword(document.getElementById("contrasena"), '\u25CF');
		            
		        },1500);
			<?php }else{ ?>
				$.alert({boxWidth: '40%',useBootstrap: false,title: 'Advertencia!',content: 'Tiene una factura relacionada cancele antes de continuar'}); 
			<?php } ?>
		}
		function selectrcf2(){
			var rfc_id = $('#new_rfc_id option:selected').val();
			selectrcfv(rfc_id);
		}
	</script>
	<?php if(isset($_GET['devolucion'])){ ?>
		<script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/mask/jquery.mask.min.js"></script>
    	<script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/mask/jquery.passwordify.js"></script>

		<script type="text/javascript" src="<?php echo base_url(); ?>public/js/devolucion.js?v=<?php echo date('YmdGi');?>" ></script>
		<style type="text/css">
			.deletepro_ss{
				display: none;
			}
		</style>
	<?php } ?>
	<div style="display:none;">
          <select id="bodegaselect_none">
            <?php 
              	foreach ($get_bodegas->result() as $item) { 
              		if($tipov==$item->tipov){
              			echo '<option value="'.$item->bodegaId.'" class="bodegas_tipo_'.$item->tipov.'">'.$item->bodega.'</option>';
              		}
              	}
            ?>
          </select>
        </div>
</html>
<?php 

	if($idCliente==708){
		if($prefactura==1){
			if($comprarow==0){
				$compraId=$this->ModeloCatalogos->Insert('series_compra',array('folio'=>$proinven,'completada'=>1,'personalId'=>$this->idpersonal));

				foreach ($arrayprocompra as $itemc) {
					//8 TAMAULIPAS
					//10 D-Impresión Tlaxcala
					//11 D-Impresión Puebla
					if($itemc['pro']==1){
						//if($itemc['bodegaId']==8){
						//	$new_bodega=10;
						//}else{
							$new_bodega=11;
						//}
						$this->ModeloCatalogos->updateCatalogo('series_productos',array('bodegaId'=>$new_bodega,'status'=>0,'activo'=>1),array('serieId'=>$itemc['serieId']));
						$this->ModeloCatalogos->Insert('series_compra_productos',array('compraId'=>$compraId,'productoid'=>$itemc['productoid'],'serie'=>$itemc['seriename'],'bodega'=>$new_bodega,'precio'=>$itemc['precio'],'status'=>1));
					}
					if($itemc['pro']==2){
						//if($itemc['bodegaId']==8){
							//$new_bodega=10;
						//}else{
							$new_bodega=11;
						//}
						if($itemc['conserie']==1){
							$this->ModeloCatalogos->updateCatalogo('series_accesorios',array('bodegaId'=>$new_bodega,'status'=>0,'activo'=>1),array('serieId'=>$itemc['serieId']));
						}else{
							$whererefacs = array(
			                    'bodegaId' => $new_bodega,
			                    'accesoriosid' => $itemc['productoid'],
			                    'con_serie'=>0
			                ); 
			                $resultaccess = $this->Compras_model->getselectwherenall('series_accesorios',$whererefacs);
			                $existe=0;
			                foreach ($resultaccess->result() as $item) {
			                    $existe=$item->serieId;
			                }
			                $cantidad=$itemc['cantidad'];
			                if ($existe>=1) {
			                    //$this->ModeloCatalogos->updateCatalogo('consumibles_bodegas',array('total' =>'total+'.$cantidad),$whereconsu);
			                    $this->ModeloCatalogos->updatestock('series_accesorios','cantidad','+',$cantidad,'serieId',$existe);
			                }else{
			                    $detallesconsumibles = array(
			                        'bodegaId'=>$new_bodega,
			                        'accesoriosid'=>$itemc['productoid'],
			                        'serie'=>'Sin Serie',
			                        'con_serie'=>0,
			                        'cantidad'=>$cantidad,
			                        'status'=>0,
			                        'personalId'=>$this->idpersonal
			                    );
			                    $this->ModeloCatalogos->Insert('series_accesorios',$detallesconsumibles);
			                }
						}
						$this->ModeloCatalogos->Insert('series_compra_accesorios',array('compraId'=>$compraId,'accesoriosid'=>$itemc['productoid'],'serie'=>$itemc['seriename'],'cantidad'=>$itemc['cantidad'],'con_serie'=>$itemc['conserie'],'precio'=>$itemc['precio'],'entraron'=>$itemc['cantidad'],'bodega'=>$new_bodega,'status'=>1));
					}
					if($itemc['pro']==3){
						//if($itemc['bodegaId']==8){
							//$new_bodega=10;
						//}else{
							$new_bodega=11;
						//}
						$cantidad=$itemc['cantidad'];
						$whereconsu = array(
		                    'bodegaId' => $new_bodega,
		                    'consumiblesId' => $itemc['productoid'],
		                    'status'=>1
		                ); 
		                $resultconsu = $this->Compras_model->getselectwherenall('consumibles_bodegas',$whereconsu);
		                $existe=0;
		                foreach ($resultconsu->result() as $item) {
		                    $existe=1;
		                }
		                if ($existe==1) {
		                    //$this->ModeloCatalogos->updateCatalogo('consumibles_bodegas',array('total' =>'total+'.$cantidad),$whereconsu);
		                    $this->ModeloCatalogos->updatestock3('consumibles_bodegas','total','+',$cantidad,'bodegaId',$new_bodega,'consumiblesId',$itemc['productoid'],'status',1);
		                }else{
		                    $detallesconsumibles = array(
		                        'bodegaId'=>$new_bodega,
		                        'consumiblesId'=>$itemc['productoid'],
		                        'total'=>$cantidad);
		                    $this->ModeloCatalogos->Insert('consumibles_bodegas',$detallesconsumibles);
		                }
		                $this->ModeloCatalogos->Insert('compra_consumibles',array('compraId'=>$compraId,'consumibleId'=>$itemc['productoid'],'cantidad'=>$itemc['cantidad'],'precio'=>$itemc['precio'],'recibidos'=>$itemc['cantidad']));
					}
					if($itemc['pro']==4){
						//if($itemc['bodegaId']==8){
							//$new_bodega=10;
						//}else{
							$new_bodega=11;
						//}
						if($itemc['conserie']==1){
							$this->ModeloCatalogos->updateCatalogo('series_refacciones',array('bodegaId'=>$new_bodega,'status'=>0,'activo'=>1),array('serieId'=>$itemc['serieId']));
						}else{
							$whererefacs = array(
			                    'bodegaId' => $new_bodega,
			                    'refaccionid' => $itemc['productoid'],
			                    'con_serie'=>0
			                ); 
			                $resultrefaccioness = $this->Compras_model->getselectwherenall('series_refacciones',$whererefacs);
			                $existe=0;
			                foreach ($resultrefaccioness->result() as $item) {
			                    $existe=$item->serieId;
			                }
			                //log_message('error', 'existe consulta refaccion: '.$existe);
			                if ($existe>=1) {
			                    //$this->ModeloCatalogos->updateCatalogo('consumibles_bodegas',array('total' =>'total+'.$cantidad),$whereconsu);
			                    $this->ModeloCatalogos->updatestock('series_refacciones','cantidad','+',$itemc['cantidad'],'serieId',$existe);
			                }else{
			                    $detallesconsumibles = array(
			                        'bodegaId'=>$new_bodega,
			                        'refaccionid'=>$itemc['productoid'],
			                        'serie'=>'Sin Serie',
			                        'con_serie'=>0,
			                        'cantidad'=>$itemc['cantidad'],
			                        'status'=>0,
			                        'personalId'=>$this->idpersonal
			                    );
			                    $this->ModeloCatalogos->Insert('series_refacciones',$detallesconsumibles);
			                }	
						}
						
		                $this->ModeloCatalogos->Insert('series_compra_refacciones',array('compraId'=>$compraId,'refaccionid'=>$itemc['productoid'],'serie'=>$itemc['seriename'],'cantidad'=>$itemc['cantidad'],'con_serie'=>0,'precio'=>$itemc['precio'],'entraron'=>$itemc['cantidad'],'bodega'=>$new_bodega,'status'=>1));
					}

				}

				$this->ModeloCatalogos->updateCatalogo('ventas',array('compras'=>1),array('id'=>$proinven));
			}	
		}	
	}
	
?>