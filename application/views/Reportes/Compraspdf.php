<?php 
    require_once('TCPDF4/examples/tcpdf_include.php');
    require_once('TCPDF4/tcpdf.php');
    $this->load->helper('url');
    //=======================================================================================
class MYPDF extends TCPDF {
  //Page header
  public function Header() {
      //$img_header = 'header.jpg';
      //$this->Image($img_header, 0, 0, 0, 197, '', '', '', false, 100, '', false, false, 0);
      //$this->Image($img_header, 0, 0, 210, '', 'JPG', '', false, false, 300, '', false, false, 0, false, false, false);
      $imglogo = base_url().'public/img/ops.png';
      $html = '
          
          
          <table width="100%" border="0" >
            <tr>
              <td ></td>
              <td align="center"><img src="'.$imglogo.'" width="100px" ></td>
              <td></td>
            </tr>
          </table>';
        $this->writeHTML($html, true, false, true, false, '');
  }
    // Page footer
  public function Footer() {
      $html = ' 
      <table width="100%" border="0">
        <tr>
          <td align="right" class="footerpage">Pagina '.$this->getAliasNumPage().' de '.$this->getAliasNbPages().'</td>
        </tr>
      </table>';
      $this->writeHTML($html, true, false, true, false, '');
  }
} 
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Cuestionario');
$pdf->SetTitle('Cuestionario');
$pdf->SetSubject('Cuestionario');
$pdf->SetKeywords('Cuestionario');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins('10', '30', '10');
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
$pdf->SetFont('dejavusans', '', 9);
// add a page
$pdf->AddPage('P', 'A4');

$totalgeneral=0;
        $compraId = array('compraId' => $id,'activo'=>1);
        $html = '';
        
        //====================================== PRODUCTOS ==================================================
            //$totalequipo = $this->Compras_model->getselectwherenall('series_compra_productos',$compraId); // editar esta consulta para colocar el modelo de la serie
            $html.='
                    <table class="striped" border="0" width="100%">            
                        <tr>
                            <td style="width: 20%">Folio:</td>
                            <td style="width: 80%">'.$folio.'</td>
                        </tr>
                        <tr>
                            <td>Fecha:</td>
                            <td>'.$reg.'</td>
                        </tr>
                        <tr>
                            <td>Comprador:</td>
                            <td>'.$personalnombre.'</td>
                        </tr>
                    </table> <p></p>
                                    


            ';
            if($totalequipo->num_rows()>0){
                $html.='
                                <table id="tabla_produto" class="striped" border="1" width="100%" cellpadding="2">
                                    <thead>
                                        <tr>
                                            <th style="width: 30%"></th>
                                            <th style="width: 25%">Serie Equipo</th>
                                            <th style="width: 25%">Bodega</th>
                                            <th style="width: 10%">Factura</th>
                                            <th style="width: 10%">Precio</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
            }
            foreach ($totalequipo->result() as $item) {
                $totalgeneral=$totalgeneral+$item->precio;
                            if($item->status){
                                $disabled ='disabled';
                            }else{
                                $disabled ='';
                            }
                            $html.='<tr class="cserienew">
                                        <td  style="width: 30%">'.$item->modelo.'</td>
                                        <td  style="width: 25%">'.$item->serie.'</td>
                                        <td style="width: 25%">';
                                            $selected='';
                                            foreach ($bodegas->result() as $itemb) {
                                                if ($itemb->bodegaId==$item->bodega) {
                                                    $selected=$itemb->bodega;
                                                }
                                             } 
                                            $html.=$selected;
                                $html.='</td>
                                        <td  style="width: 10%">'.$item->factura.'</td>
                                        <td  style="width: 10%" align="center">'.$item->precio.'</td>
                                    </tr>';
            }
            if($totalequipo->num_rows()>0){
                        $html.='</tbody>
                            </table><p></p>';
            }
        //====================================== PRODUCTOS ==================================================
        //====================================== ACCESORIOS ==================================================
            //$totalaccesorios = $this->Compras_model->getselectwherenall('series_compra_accesorios',$compraId); 
            
            if ($totalaccesorios->num_rows()>0) {
                $html.='
                                <table id="tabla_accesorio" class="striped" border="1" width="100%">
                                    <thead>
                                        <tr>
                                            <th style="width: 30%"></th>
                                            <th style="width: 25%">Serie Accesorio</th>
                                            <th style="width: 25%">Bodega</th>
                                            <th style="width: 10%">Factura</th>
                                            <th style="width: 10%">Precio</th>

                                        </tr>
                                    </thead>
                                    <tbody>';
            }
            foreach ($totalaccesorios->result() as $item) {
                $totalgeneral=$totalgeneral+$item->precio;
                           $html.='<tr class="cserienew">
                                        <td style="width: 30%">'.$item->nombre.'</td>
                                        <td style="width: 25%">'.$item->serie.'</td>
                                        <td style="width: 25%">';
                                            $selected='';
                                            foreach ($bodegas->result() as $itemb) {
                                                if ($itemb->bodegaId==$item->bodega) {
                                                    $selected=$itemb->bodega;
                                                }
                                             } 
                                            $html.=$selected;
                                            $html.='</td>
                                            <td  style="width: 10%">'.$item->factura.'</td>
                                            <td  style="width: 10%" align="center">'.$item->precio.'</td>
                                    </tr>';               
            }
            if ($totalaccesorios->num_rows()>0) {
                           $html.='</tbody>
                            </table><p></p>'; 
            }
            if($totalaccesoriosss->num_rows()>0){
                $html.='
                                <table id="tabla_refaccionesss" class="striped" border="1" width="100%">
                                    <thead>
                                        <tr>
                                            <th style="width: 30%">Refaccion</th>
                                            <th style="width: 30%">Cantidad</th>
                                            <th style="width: 20%">Factura</th>
                                            <th style="width: 20%">Precio</th>
                                        </tr>';
            }
            
            foreach ($totalaccesoriosss->result() as $item) {
                $totalgeneral=$totalgeneral+($item->precio*$item->entraron);
                $html.='
                                        <tr>
                                            <th>'.$item->nombre.'</th>
                                            <th>'.$item->entraron.'</th>
                                            <th>'.$item->factura.'</th>
                                            <th>'.$item->precio.'</th>
                                        </tr>
                                    
                               
                            
                        
                        ';
            }
            if($totalaccesoriosss->num_rows()>0){
                $html.='</table><p></p>';
            }
        //====================================== ACCESORIOS ==================================================
        //====================================== REFACCIONES ==================================================
            //$compraId = array('compraId' => $id,'activo'=>1,'con_serie'=>1);
            //$totalrefacciones = $this->Compras_model->getselectwherenall('series_compra_refacciones',$compraId); 
            
            if($totalrefacciones->num_rows()>0){
                $html.='<div class="row">
                            <div class="col s12 m12 12 tablacompras">
                                <table id="tabla_refaccion" class="striped" border="1" width="100%">
                                    <thead>
                                        <tr>
                                            <th style="width: 30%"></th>
                                            <th style="width: 25%">Serie refaccion</th>
                                            <th style="width: 25%">Bodega</th>
                                            <th style="width: 10%">Factura</th>
                                            <th style="width: 10%">Precio</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
            }
            foreach ($totalrefacciones->result() as $item) {
                $totalgeneral=$totalgeneral+$item->precio;
                $html.='<tr class="cserienew">
                            <td  style="width: 30%">'.$item->nombre.'</td>
                            <td  style="width: 25%">'.$item->serie.'</td>
                            <td style="width: 25%">';
                                    $selected='';
                                foreach ($bodegas->result() as $itemb) {
                                    if ($itemb->bodegaId==$item->bodega) {
                                        $selected=$itemb->bodega;
                                    }
                                 } 
                                $html.=$selected;
                    $html.='</td>
                            <td  style="width: 10%">'.$item->factura.'</td>
                            <td  style="width: 10%" align="center">'.$item->precio.'</td>
                        </tr>'; 
            }
            if($totalrefacciones->num_rows()>0){
                        $html.='</tbody>
                            </table>
                        </div>
                    </div>';
            } 
            //=========================================================================================
            if($totalrefaccionesss->num_rows()>0){
                $html.='
                                <table id="tabla_refaccionesss" class="striped" border="1" width="100%">
                                    <thead>
                                        <tr>
                                            <th style="width: 30%">Refaccion</th>
                                            <th style="width: 30%">Cantidad</th>
                                            <th style="width: 20%">Factura</th>
                                            <th style="width: 20%">Precio</th>
                                        </tr>';
            }
            
            foreach ($totalrefaccionesss->result() as $itemc) {
                $totalgeneral=$totalgeneral+($itemc->precio*$itemc->entraron);
                $html.='
                                        <tr>
                                            <th>'.$itemc->codigo.' ('.$itemc->nombre.')</th>
                                            <th>'.$itemc->entraron.'</th>
                                            <th>'.$itemc->factura.'</th>
                                            <th>'.$itemc->precio.'</th>
                                        </tr>
                                    
                               
                            
                        
                        ';
            }
            if($totalrefaccionesss->num_rows()>0){
                $html.='</table>';
            }
        //====================================== REFACCIONES ==================================================
        //====================================== CONSUMIBLES ==================================================
           
            foreach ($totalconsumibles->result() as $itemc) {
                $totalgeneral=$totalgeneral+($itemc->recibidos*$itemc->precio);
                $html.='<div class="row">
                            <div class="col s12 m12 12 tablacompras">
                                <table id="tabla_consumibles" class="striped" border="1" width="100%">
                                    <thead>
                                        <tr>
                                            <th style="width: 50%">Consumible</th>
                                            <th style="width: 25%">Cantidad</th>
                                            <th style="width: 25%">Cantidad</th>
                                        </tr>
                                        <tr>
                                            <th>'.$itemc->modelo.' ('.$itemc->parte.')</th>
                                            <th>'.$itemc->recibidos.'</th>
                                            <th>'.$itemc->precio.'</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody class="consumible_'.$itemc->cconsumibleId.' consumibles_add">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        ';
            }
            $html.='
                    <table class="striped" border="1" width="100%">            
                        <tr>
                            <td style="width: 20%">Total:</td>
                            <th style="width: 80%">'.$totalgeneral.'</th>
                        </tr>
                    </table> ';

        //====================================== CONSUMIBLES ==================================================
        //echo $html;
$pdf->writeHTML($html, true, false, true, false, '');
$pdf->Output('Documentos.pdf', 'I');