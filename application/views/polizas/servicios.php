<style type="text/css">
  .filepoliza{
    width: 100%;
    border: 0px;
    min-height: 400px;
  }
  .select2-container.select2-container--default{
    width: 100% !important;
  }
  .table_equipos_selected input{
    margin-bottom:0px;
  }
  .table_equipos_selected td{ 
    padding-top: 5px;
    padding-bottom: 5px;
    font-size: 13px;
  }
   #table_servicio{
    font-size: 13px;
  }
</style><?php
$vigencia_clicks=0;
  $poliza_name='';
  foreach ($polizaventadetalles->result() as $item) { 
    $vigencia_clicks=$item->vigencia_clicks;
    $vigencia_clicks_o=$item->vigencia_clicks;
    if($ser_rest>0){
      $vigencia_clicks=$ser_rest;
    }
  }
  //$vigenciasuma = "+$vigencia_clicks month";
  $vigenciasuma = "+12 month";
  $vencimiento = date("Y-m-d",strtotime($fechareg.$vigenciasuma)); 
?>
<input type="hidden" id="idcliente" value="<?php echo $idCliente;?>">
<!-- START CONTENT -->
<section id="content ">
  <!--start container-->
  <div class="container">
    <div class="section">
      <!--<div class="card-panel">-->
      <ul class="tabs tab-demo z-depth-1" style="margin-top: 59px;">
        <li class="tab col s3"><a class="active" href="#test1" id="test1_c">Información de póliza</a></li>
        <li class="tab col s3"><a href="#test2" id="test2_c" >Servicios</a></li>
        <li class="tab col s3"><a href="#test3" id="test3_c" >Garantias</a></li>
        <li class="tab col s3"><a href="#test4" id="test4_c" >Documento</a></li>
        <li class="tab col s3"><a href="#test5" id="test5_c" >Agendas</a></li>
      </ul>
    <!------------------------------------------------------------------------------------------------------------------------------------------------->
      <div id="test1" class="col s12 card-panel">
        <!------------------------------------------------------->
        <div class="row">
          <div class="col s3">
            <label>Folio</label>
            <input  class="form-control-bmz" value="<?php echo $proinven;?>"  readonly>
            <input  type="hidden" value="<?php echo $idPoliza;?>" id="idPoliza" readonly>
          </div>
          <div class="col s3">
            <label>Fecha creación</label>
            <input  class="form-control-bmz" value="<?php echo $fechareg;?>" readonly>
          </div>
          <div class="col s3">
            <label>Fecha de inicio</label>
            <input type="date" class="form-control-bmz" value="<?php echo $fechaentrega;?>" id="fechaentrega" ondblclick="$('#fechaentrega').prop('readonly',false)" onchange="actualizarfechae(<?php echo $idPoliza;?>)" readonly>
          </div>
          <div class="col s3">
            <label>Fecha de Vencimiento</label>
            <input type="date" class="form-control-bmz" value="<?php echo $vencimiento;?>"  readonly>
          </div>
        </div>
        <div class="row">
          <div class="col s12">
            <h5 class="header">Datos de cliente</h5>
          </div>
          <div class="col s8">
            <label>Cia</label>
            <input id="razon_social" class="form-control-bmz" readonly></input>
          </div>
          <div class="col s4">
            <label>RFC</label>
            <select class="browser-default form-control-bmz" id="rfc_id" name="rfc_id"  disabled onchange="selecrfcp()" required>
                <option value="">Seleccione</option>
            <?php foreach ($rfc_datos as $item) { ?>
                <option value="<?php echo $item->id; ?>" <?php if($item->id==$rfc_id){ echo 'selected';}?> ><?php echo $item->rfc; ?></option>
            <?php } ?>
              </select> 
          </div>
        </div>
        <div class="row">
          <div class="col s4">
            <label>METODO DE PAGO</label>
            <select class="form-control-bmz browser-default" id="metodopagoId" name="metodopagoId" <?php echo $metodopagoId_block;?> >
            <?php foreach ($metodopagorow as $item) { ?>
                <option value="<?php echo $item->id; ?>" <?php if($item->id==$metodopagoId){ echo 'selected';} ?>    ><?php echo $item->metodopago_text; ?></option>
            <?php } ?>
          </select>
          </div>
          <div class="col s4">
            <label>FORMA DE PAGO</label>
            <select class="form-control-bmz browser-default " id="formapagoId" name="formapagoId" <?php echo $formapago_block;?> >
            <?php foreach ($formapagorow->result() as $item) { 
                if($item->id==$formapago){ 
                  $selected= 'selected';
                }else{
                  $selected= '';
                }
                echo '<option value="'.$item->id.'" '.$selected.'>'.$item->formapago_text.'</option>';
             } ?>
          </select>
          </div>
          <div class="col s4">
            <label>USO DE CFDI</label>
            <select class="form-control-bmz browser-default " id="usocfdiId" name="usocfdiId" <?php echo $cfdi_block;?>>
              <?php foreach ($cfdirow->result() as $item) { 
                if($item->id==$cfdi){ 
                  $selected='selected';
                }else{
                  $selected= '';
                }
                echo '<option value="'.$item->id.'" '.$selected.'>'.$item->uso_cfdi_text.'</option>';
               } ?>
            </select>
          </div>
        </div>
        <div class="row">
          <div class="col s3">
            <label>Calle</label>
            <input class="form-control-bmz" value="<?php echo $calle;?>" readonly >
          </div>
          <div class="col s3">
            <label>No.</label>
            <input class="form-control-bmz" value="<?php echo $num_ext;?>" readonly >
          </div>
          <div class="col s3">
            <label>Col.</label>
            <input class="form-control-bmz" value="<?php echo $colonia;?>" readonly >
          </div>
          <div class="col s3">
            <label>Cd</label>
            <input class="form-control-bmz" value="<?php echo $municipio;?>" readonly >
          </div>
          <div class="col s3">
            <?php 
                $estadoval='';
                foreach ($estadorow->result() as $item) { 
                  if($item->EstadoId==$estado){ 
                    $estadoval = $item->Nombre;
                  }
                } 
            ?>
            <label>Edo..</label>
            <input class="form-control-bmz" value="<?php if($estadovals!=''){echo $estadovals;}else{echo $estadoval.' C.P.'.$cp;}?>" readonly >
          </div>
          <div class="col s3">
            <label>FORMA DE COBRO </label>
            <input class="form-control-bmz" value="<?php echo $formacobro;?>" readonly >
          </div>
        </div>
        <div class="row">
          <div class="col s3">
            <label>tel.</label>
            <select class="form-control-bmz browser-default" id="telId" name="telId" <?php echo $telId_block; ?>>
            <?php foreach ($cliente_datoscontacto->result() as $item) { ?>
              <option value="<?php echo $item->datosId;?>"   <?php if($telId==$item->datosId){echo 'selected'; }?> ><?php echo $item->telefono;?></option>    
              <?php } ?>
            </select>
          </div>
          <div class="col s3">
            <label>Contacto</label>
            <select class="form-control-bmz browser-default " id="contactoId" name="contactoId" <?php echo $contactoId_block; ?> onchange="copiardatoscc()">
            <?php foreach ($cliente_datoscontacto->result() as $item) { ?>
              <option value="<?php echo $item->datosId;?>" <?php if($contactoId==$item->datosId){echo 'selected'; }?> data-dcemail="<?php echo $item->email;?>" data-dcpuesto="<?php echo $item->puesto;?>" ><?php echo $item->atencionpara;?></option>
              <?php } ?>
            </select>
          </div>
          <div class="col s3">
            <label>Cargo</label>
            <input class="form-control-bmz" value="<?php echo $cargo;?>" readonly >
          </div>
          <div class="col s3">
            <label>email</label>
            <input class="form-control-bmz dcemail"  value="<?php echo $email;?>" readonly >
          </div>
          <div class="col s12" style="text-align: end;">
            <a class="green btn-bmz"  onclick="agregar_equipo()">Agregar Equipo</a>
          </div>
        </div>

        <!------------------------------------------------------>
          
        <table class="table striped " style="font-size: 14px;">
          <thead>
            <tr>
              <th>Servicio</th>
              <th>Equipo</th>
              <th>Serie</th>
              <th>Cubre</th>
              <th>Domicilio</th>
              <th>Activar / suspender</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            <?php 
            $vigencia_clicks=0;
            foreach ($polizaventadetalles->result() as $item) { 
              $poliza_name=$item->nombre; 
              $vigencia_clicks=$item->vigencia_clicks;
              $vigencia_clicks_o=$item->vigencia_clicks;
              if($ser_rest>0){
                $vigencia_clicks=$ser_rest;
              }
            ?>
              <tr>
                <td><?php echo $item->nombre;?></td>
                <td><?php echo $item->modeloe;?></td>
                <td><?php 
                          $seriesall=$this->ModeloCatalogos->seriespolizas($item->id);
                          $label_series='';
                          if ($seriesall!='') {
                            $label_series=$seriesall;
                          }else{
                            $label_series=$item->serie;  
                          }
                          echo $label_series;
                          ?></td>
                <td><?php echo $item->cubre;?></td>
                <td><?php echo $item->direccionservicio;?></td>
                <td>
                  <?php
                    if($item->suspender==0){
                      $acsus=1;
                      $icon='<i class="fas fa-check" style="color:#7bde7f"></i>';
                      $title='Equipo Activo';
                    }else{
                      $acsus=0;
                      $icon='<i class="fas fa-exclamation-triangle" style="color:red"></i>';
                      $title='Equipo Suspendido';
                    }
                  ?>
                  <a class="b-btn b-btn-primary tooltipped" data-position="top" data-delay="50" data-tooltip="<?php echo $title;?>" onclick="activarsuspender(<?php echo $item->id;?>,<?php echo $acsus;?>)"><?php echo $icon;?></a>
                </td>
                <td>
                  <a class="b-btn b-btn-primary tooltipped row_edit_<?php echo $item->id;?>" data-position="top" data-delay="50" data-tooltip="Editar" data-equipo="<?php echo $item->idEquipo;?>" data-serie="<?php echo str_replace(array('<div>','</div>'), '', $label_series) ;?>" onclick="editar_pold(<?php echo $item->id;?>)"><i class="fas fa-pencil-alt"></i></a>
                  <button class="b-btn b-btn-danger tooltipped deleteitems_<?php echo $item->id;?>" data-modelo="<?php echo $item->modeloe;?>" data-serie="<?php echo $label_series;?>" data-position="top" data-delay="50" data-tooltip="Eliminar" onclick="deletedll(<?php echo $item->id;?>)" ><i class="fas fa-times"></i></button>
                </td>
              </tr>
            <?php } ?>
          </tbody>
        </table>


        <table class="table striped table_equipos " style="display: none;">
          <thead>
            <tr>
              <th></th>
              <th>Equipo</th>
              <th>Serie</th>
              
            </tr>
          </thead>
          <tbody class="">
            <?php 
            $vigencia_clicks=0;
            foreach ($polizaventadetalles->result() as $item) { 
              $vigencia_clicks=$item->vigencia_clicks;
              $vigencia_clicks_o=$item->vigencia_clicks;
              if($ser_rest>0){
                $vigencia_clicks=$ser_rest;
              }
              if($item->suspender==0){
            ?>
              <tr>
                <td>
                  <input type="number" id="id_row" value="<?php echo $item->id;?>">
                  
                    <?php 
                            $seriesall=$this->ModeloCatalogos->seriespolizas($item->id);
                            $label_series='';
                            if ($seriesall!='') {
                              $label_series=$seriesall;
                            }else{
                              $label_series=$item->serie;  
                            }

                            ?>
                  
                </td>
                <td><input type="text" id="modelo_row" value="<?php echo $item->modeloe;?>"></td>
                <td><input type="text" id="serie_row" value="<?php echo $label_series;?>"></td>
                
              </tr>
            <?php } } ?>
          </tbody>
        </table>
        <div class="row">
          <div class="col s12">
            <label for="observaciones">Observaciones:</label>
            <textarea name="observaciones" id="observaciones" class="form-control" <?php echo $observaciones_block;?> style="min-height: 170px;"><?php echo $observaciones;?></textarea>
          </div>
          
        </div>
        <!------------------------------------------------------->
      </div>
    <!------------------------------------------------------------------------------------------------------------------------------------------------->
      <div id="test2" class="col s12 card-panel ">
        <?php 
          $servicios=0;
          $servicios_f=0;
          $serviciosfecha='';
          foreach ($query_result->result() as $item) {
            //if($item->fecha==$serviciosfecha){
              
              
            //}else{
              //$serviciosfecha=$item->fecha;
              if($item->status==2){
                $servicios++;
              }elseif($item->status<2){
                $servicios_f++;
              }
            //}
          }
          foreach ($rows_ser_eve->result() as $item) {

              if($item->status==2){
                $servicios++;
              }elseif($item->status<2){
                $servicios_f++;
              }
          }
          foreach ($rows_ser_pol->result() as $item) {
              if($item->status==2){
                $servicios++;
              }elseif($item->status<2){
                $servicios_f++;
              }
          }
        ?>
        <div class="row">
          <div class="col s10">
            <h5><span style="color:red;margin-left: 39px;">Póliza contratada:</span> <?php echo $poliza_name;?></h5>
            <h5><span style="color:red;margin-left: 39px;">Servicios realizados:</span> <?php echo $servicios;  ?></h5>
            <h5>
              <a onclick="editrestantes(<?php echo $idPoliza.','.$vigencia_clicks_o.','.$servicios;?>)" style="font-size: 14px;padding-left: 9px;padding-top: 5px;padding-right: 9px;padding-bottom: 5px;" class="b-btn b-btn-primary"><i class="fas fa-pencil-alt"></i></a>
              <span style="color:red;">Servicios restantes:</span> <?php 
              if($ser_rest>0){
                $vigencia_clicks=$ser_rest;
              }
              if($polizaventadetalles->num_rows()>1){
                if($ser_rest>0){
                  $restantesservicios=($vigencia_clicks)-$servicios;
                }else{
                  //$restantesservicios=($vigencia_clicks*$polizaventadetalles->num_rows())-$servicios;//se realizo solo para esta poliza por la configuracion, reajustar este metodo para las polizas que tengan mas de un equipo 
                  $restantesservicios=($vigencia_clicks)-$servicios; 
                }
                
                if($idPoliza==10818){
                  $restantesservicios=($vigencia_clicks*5)-$servicios;
                }
              }else{
                $restantesservicios=$vigencia_clicks-$servicios;
              }

              if($restantesservicios<0){
                $restantesservicios=0;
              }
              echo $restantesservicios;
              $this->ModeloCatalogos->updateCatalogo('polizasCreadas',array('viewcont_realizados'=>$servicios,'viewcont_disponibles'=>$restantesservicios),array('id'=>$idPoliza));
            ?></h5>
          </div>
          <div class="col s2">
            <?php $disabled_n_s='';
                  if($serviciocount==1){ 
                    if($vencimiento<date('Y-m-d')){
                      $disabled_n_s=' disabled ';
                    }
                    if($restantesservicios<=0){
                      $disabled_n_s=' disabled ';
                    }
            ?> 
            <!--<a href="<?php echo base_url().'Asignaciones?tasign=2&prasignaciontipo=2&servicio='.$idPoliza.'&tipo=0&cliente='.$idCliente;?>" class="waves-effect cyan btn-bmz">Realizar nuevo servicio</a>-->
            <button onclick="notificardeservicio(<?php echo $idPoliza;?>,'')" class="waves-effect cyan btn-bmz" <?php echo $disabled_n_s;?> data-vencimiento="<?php echo $vencimiento;?>" >Realizar nuevo servicio</button>
            <?php } ?>
            <?php if($restantesservicios==0){ ?>
            <a class="b-btn b-btn-danger tooltipped" data-position="top" data-delay="50" data-tooltip="Dejar de realizar servicios a la pre" onclick="dejarderealizarservicios(<?php echo $idPoliza;?>)">Dejar de realizar</a>
            <?php } ?>
            <a onclick="addserex(<?php echo $idPoliza,','.$idCliente;?>)" class="b-btn b-btn-primary" title="Agregar un servicio existente"><i class="fas fa-plus"></i></a>
          </div>
        </div>
        <!------------------------------------------------------->
        <table class="table striped" id="table_servicio">
          <thead>
            <tr>
              <th>#</th>
              <th>#</th>
              <th>Técnico</th>
              <th>Cliente</th>
              <th>Fecha</th>
              <th>Hora</th>
              <th>Equipo</th>
              <th>Serie</th>
              <th>Status</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            <?php 

              foreach ($query_result->result() as $item) { 

            ?>
              <tr>
                <td><?php echo $item->asignacionId;?></td>
                <td><?php echo $item->asignacionIde;?></td>
                <td><?php 
                    $tecnico=$item->tecnico;
                    if($item->tecnico2){
                      $tecnico.='<br>'.$item->tecnico2;
                    }
                    if($item->tecnico3){
                      $tecnico.='<br>'.$item->tecnico3;
                    }
                    if($item->tecnico4){
                      $tecnico.='<br>'.$item->tecnico4;
                    }
                echo $tecnico;
                  ?></td>
                <td><?php echo $item->empresa;?></td>
                <td><?php echo $item->fecha;?></td>
                <td><?php echo $item->hora.' a '.$item->horafin;?></td>
                <td><?php echo $item->modelo;?></td>
                <td><?php echo $item->serie;?></td>
                <td><?php 
                    if ($item->status==0) {
                        $status='<p></p>';
                    }
                    if ($item->status==1) {
                        $status='<p>En proceso</p>';
                    }
                    if ($item->status==2) {
                        $status='<p style="color:green">Finalizado</p>';
                    }
                    if ($item->status==3) {
                        $status='<p style="color:red">Suspendido</p>';
                    }
                    echo $status;
                ?></td>
                <td><?php 
                  $html='';
                  $html.='<a class="waves-effect cyan btn-bmz" onclick="documento('.$item->asignacionId.',2,'.$item->asignacionIde.')"><i class="fas fa-file-alt" style="font-size: 20px;"></i></a>';  

                  $html.='<a class="waves-effect cyan btn-bmz" title="Prefactura" onclick="viewprefacturap('.$item->polizaId.')"><i class="fas fa-file" style="font-size: 20px;"></i></a>';
                  echo $html;
                ?></td>
              </tr>
            <?php } ?>
            <?php 
              foreach ($rows_ser_eve->result() as $item) {
                ?>
                  <tr>
                <td><?php echo $item->asignacionId;?></td>
                <td><?php echo $item->asignacionIdd;?></td>
                <td><?php 
                    $tecnico=$item->tecnico;
                    if($item->tecnico2){
                      $tecnico.='<br>'.$item->tecnico2;
                    }
                    if($item->tecnico3){
                      $tecnico.='<br>'.$item->tecnico3;
                    }
                    if($item->tecnico4){
                      $tecnico.='<br>'.$item->tecnico4;
                    }
                echo $tecnico;
                  ?></td>
                <td><?php echo $item->empresa;?></td>
                <td><?php echo $item->fecha;?></td>
                <td><?php echo $item->hora.' a '.$item->horafin;?></td>
                <td ></td>
                <td ><?php echo $item->equiposserie;?></td>
                <td><?php 
                    if ($item->status==0) {
                        $status='<p></p>';
                    }
                    if ($item->status==1) {
                        $status='<p>En proceso</p>';
                    }
                    if ($item->status==2) {
                        $status='<p style="color:green">Finalizado</p>';
                    }
                    if ($item->status==3) {
                        $status='<p style="color:red">Suspendido</p>';
                    }
                    echo $status;
                ?></td>
                <td><?php 
                  $html='';
                  $html.='<a class="waves-effect cyan btn-bmz" onclick="documento('.$item->asignacionId.',3,0)"><i class="fas fa-file-alt" style="font-size: 20px;"></i></a>';  
                  $html.='<button class="b-btn b-btn-danger tooltipped" data-position="top" data-delay="50" data-tooltip="Eliminar" onclick="deletedoc('.$item->pesaid.')"><i class="fas fa-times"></i></button>';
                  //$html.='<a class="waves-effect cyan btn-bmz" title="Prefactura" onclick="viewprefacturap('.$item->polizaId.')"><i class="fas fa-file" style="font-size: 20px;"></i></a>';
                  echo $html;
                ?></td>
              </tr>
                <?php

              }
              foreach ($rows_ser_pol->result() as $item) {
                ?>
                  <tr>
                <td><?php echo $item->asignacionId;?></td>
                <td><?php echo $item->asignacionIde;?></td>
                <td><?php 
                    $tecnico=$item->tecnico;
                    if($item->tecnico2){
                      $tecnico.='<br>'.$item->tecnico2;
                    }
                    if($item->tecnico3){
                      $tecnico.='<br>'.$item->tecnico3;
                    }
                    if($item->tecnico4){
                      $tecnico.='<br>'.$item->tecnico4;
                    }
                echo $tecnico;
                  ?></td>
                <td><?php echo $item->empresa;?></td>
                <td><?php echo $item->fecha;?></td>
                <td><?php echo $item->hora.' a '.$item->horafin;?></td>
                <td ></td>
                <td ><?php echo $item->equiposserie;?></td>
                <td><?php 
                    if ($item->status==0) {
                        $status='<p></p>';
                    }
                    if ($item->status==1) {
                        $status='<p>En proceso</p>';
                    }
                    if ($item->status==2) {
                        $status='<p style="color:green">Finalizado</p>';
                    }
                    if ($item->status==3) {
                        $status='<p style="color:red">Suspendido</p>';
                    }
                    echo $status;
                ?></td>
                <td><?php 
                  $html='';
                  $html.='<a class="waves-effect cyan btn-bmz" onclick="documento('.$item->asignacionId.',2,'.$item->asignacionIde.')"><i class="fas fa-file-alt" style="font-size: 20px;"></i></a>';  
                  $html.='<button class="b-btn b-btn-danger tooltipped" data-position="top" data-delay="50" data-tooltip="Eliminar" onclick="deletedoc('.$item->pesaid.')"><i class="fas fa-times"></i></button>';
                  //$html.='<a class="waves-effect cyan btn-bmz" title="Prefactura" onclick="viewprefacturap('.$item->polizaId.')"><i class="fas fa-file" style="font-size: 20px;"></i></a>';
                  echo $html;
                ?></td>
              </tr>
                <?php
              }
            ?>
          </tbody>
        </table>
        <!------------------------------------------------------->
      </div>
    <!------------------------------------------------------------------------------------------------------------------------------------------------->
      <div id="test3" class="col s12 card-panel">
        <div class="row">
          <div class="col s12" style="text-align: end;">
            <a class="green btn-bmz"  onclick="modal_add_pol()">Agregar servicio a garantia</a>
          </div>
        </div>
        <div class="row">
          <div class="col s12">
            <table class="table" id="table_pol_ser_vinc">
              <thead>
                <tr>
                  <th>Id Servicio viculado</th>
                  <th>Id Servicio en garantia</th>
                  <th>Cliente</th>
                  <th>Fecha</th>
                  <th></th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                <?php  
                  $html='';
                  foreach ($query_result->result() as $item) {
                    $tiposer=2;//poliza
                    $result = $this->Polizas_model->info_servicios_vinculados_servicios($item->asignacionId,$tiposer);
                    foreach ($result->result() as $item) {
                      $html.='<tr>';
                        $html.='<td>'.$item->id_ser.'</td>';
                        $html.='<td>'.$item->asignacionId.'</td>';
                        $html.='<td>'.$item->empresa.'</td>';
                        $html.='<td>'.$item->fecha.'</td>';
                        $html.='<td>';
                        $html.='<a class="btn-bmz blue " onclick="obternerdatosservicio('.$item->asignacionId.','.$item->tiposer.')"><i class="fas fa-info-circle"></i></a>';
                        $html.='</td>';
                        $html.='<td>';
                        $html.='<button class="b-btn b-btn-danger tooltipped" data-position="top" data-delay="50" data-tooltip="Eliminar" onclick="deletegarantia('.$item->pes_id.')"><i class="fas fa-times"></i></button>';
                        $html.='</td>';
                      $html.='</tr>';
                    }
                  }
                  foreach ($rows_ser_eve->result() as $item) {
                    $tiposer=3;//cliente
                    $result = $this->Polizas_model->info_servicios_vinculados_servicios($item->asignacionId,$tiposer);
                    foreach ($result->result() as $item) {
                      $html.='<tr>';
                        $html.='<td>'.$item->id_ser.'</td>';
                        $html.='<td>'.$item->asignacionId.'</td>';
                        $html.='<td>'.$item->empresa.'</td>';
                        $html.='<td>'.$item->fecha.'</td>';
                        $html.='<td>';
                        $html.='<a class="btn-bmz blue " onclick="obternerdatosservicio('.$item->asignacionId.','.$item->tiposer.')"><i class="fas fa-info-circle"></i></a>';
                        $html.='</td>';
                        $html.='<td>';
                        $html.='<button class="b-btn b-btn-danger tooltipped" data-position="top" data-delay="50" data-tooltip="Eliminar" onclick="deletegarantia('.$item->pes_id.')"><i class="fas fa-times"></i></button>';
                        $html.='</td>';
                      $html.='</tr>';
                    }
                  }
                  foreach ($rows_ser_pol->result() as $item) {
                    $tiposer=2;//poliza
                    $result = $this->Polizas_model->info_servicios_vinculados_servicios($item->asignacionId,$tiposer);
                    foreach ($result->result() as $item) {
                      $html.='<tr>';
                        $html.='<td>'.$item->id_ser.'</td>';
                        $html.='<td>'.$item->asignacionId.'</td>';
                        $html.='<td>'.$item->empresa.'</td>';
                        $html.='<td>'.$item->fecha.'</td>';
                        $html.='<td>';
                        $html.='<a class="btn-bmz blue " onclick="obternerdatosservicio('.$item->asignacionId.','.$item->tiposer.')"><i class="fas fa-info-circle"></i></a>';
                        $html.='</td>';
                        $html.='<td>';
                        $html.='<button class="b-btn b-btn-danger tooltipped" data-position="top" data-delay="50" data-tooltip="Eliminar" onclick="deletegarantia('.$item->pes_id.')"><i class="fas fa-times"></i></button>';
                        $html.='</td>';
                      $html.='</tr>';
                    }
                  }
                  echo $html;
                ?>
              </tbody>
            </table>
          </div>
        </div>
        
      </div>
    <!------------------------------------------------------------------------------------------------------------------------------------------------->
      <div id="test4" class="col s12 card-panel">
        <div class="row">
          <div class="col s12">
            <?php
            if($file!=''){
              
              $url_file=FCPATH.'uploads/poliza/'.$file;
              $url_file2=base_url().'uploads/poliza/'.$file;

              if(file_exists($url_file)){
                //echo filetype($url_file);
                
                ?>
                <iframe src="<?php echo $url_file2;?>" class="filepoliza"></iframe>
                <?php
              }
            }
          
        ?>
          </div>
          <div class="col s12">
            <input type="file" name="filepoliza" id="filepoliza">
          </div>
        </div>
      </div>
    <!------------------------------------------------------------------------------------------------------------------------------------------------->
      <div id="test5" class="col s12 card-panel">
        <div class="row">
          <div class="col s2">
            <label>Fecha</label>
            <input type="date" class="form-control-bmz" id="fechaage">
          </div>
          <div class="col s2">
            <a class="green btn-bmz" onclick="agregar_agenda()">Agregar</a>
          </div>
        </div>
        <div class="row">
          <div class="col s6">
            <table class="table">
              <thead>
                <tr>
                  <th>Fecha</th>
                  <th></th>
                </tr>
              </thead>
              <tbody class="agefechastbody">
                
              </tbody>
            </table>
          </div>
          
        </div>
        
      </div>
    <!------------------------------------------------------------------------------------------------------------------------------------------------->
      <!--</div>-->
    </div>
  </div>
</section>

<div id="modal_addequipo" class="modal"  >
  <div class="modal-content ">
    <h5>¿Desea agregar un equpo nuevo?</h5>
    <div class="row">
      <div class="col s3">
        <label>Equipo</label>
        <select id="select_eq" class="browser-default form-control-bmz">
          <?php  foreach ($row_eq->result() as $item) {
              echo '<option value="'.$item->id.'">'.$item->modelo.'</option>';
            } ?>
        </select>
      </div>
      <div class="col s3">
        <label>Serie</label>
        <input type="text" id="select_ser" class="form-control-bmz">
      </div>
      <div class="col s4">
        <label>Dirección</label>
        <select class="form-control-bmz browser-default" id="s_direccion_s">
          <?php 
            foreach ($r_direcciones->result() as $itemsd) {
              echo '<option>'.$itemsd->direccion.'</option>';
            }
          ?>
        </select>
      </div>
      <div class="col s2">
        <a class="green btn-bmz" onclick="agregar_eq()"><i class="material-icons">system_update_alt</i></a>
      </div>
    </div>
    <div class="row">
      <table class="table" id="table_equipos_new">
        <thead>
          <tr>
            <th>Equipo</th>
            <th>Serie</th>
            <th></th>
          </tr>
        </thead>
        <tbody class="tbody_equipos_new">
          
        </tbody>
      </table>
    </div>
    <div class="modal-footer">
      <button class="modal-action modal-close waves-effect waves-red gray btn-flat " id="okagregareq">Agregar</button>
      <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
    </div>
  </div>
</div>

<div id="modal_add_pol" class="modal"  >
  <div class="modal-content ">
    <h5>¿Desea agregar una póliza por garantía?</h5>
    <div class="row">
      <div class="col s12">
        <div class="switch">
          <label>
            Solo las series
            <input id="montrar_s_2" type="checkbox" onclick="filtrarservicios2()">
            <span class="lever"></span>
            Todo
          </label>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col s2">
        <label>Servicio</label>
        <select class="form-control-bmz browser-default" id="servicios_select">
          <option value="0"></option>
          <?php 
              $option_id=0;
              foreach ($query_result->result() as $item) { 
                if($item->asignacionId!=$option_id){
                  echo '<option value="'.$item->asignacionId.'" data-tipo="2">'.$item->asignacionId.' / fecha:'.$item->fecha.'</option>';
                  $option_id=$item->asignacionId;
                }
              }
              foreach ($rows_ser_pol->result() as $item) {
                echo '<option value="'.$item->asignacionId.'" data-tipo="2">'.$item->asignacionId.' / fecha:'.$item->fecha.'</option>';
              }
              foreach ($rows_ser_eve->result() as $item) {
                echo '<option value="'.$item->asignacionId.'" data-tipo="3">'.$item->asignacionId.' / fecha:'.$item->fecha.'</option>';
              }

            ?>
        </select>
      </div>
      
    </div>
    <div class="row div_table_pol">
      
    </div>

    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
    </div>
  </div>
</div>


<div id="modal_add_ser_ex" class="modal"  >
  <div class="modal-content ">
    <h5>Seleccione el servicio a agregar</h5>
    
    <div class="row">
      <div class="col s12">
        <div class="switch">
                    <label>
                      Solo las series
                      <input id="montrar_s_1" type="checkbox" onclick="filtrarservicios1()">
                      <span class="lever"></span>
                      Todo
                    </label>
                  </div>
      </div>
    </div>
    <div class="row row_table_pol_ser_ext">
      
    </div>

    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
    </div>
  </div>
</div>


<div id="modal_edit" class="modal"  >
  <div class="modal-content ">
    <h5>Editar Equipo/serie</h5>
    
    <div class="row">
      <div class="col s6">
        <label>Equipo</label>
        <select id="select_eq_edit" class="browser-default form-control-bmz">
          <?php  foreach ($row_eq->result() as $item) {
              echo '<option value="'.$item->id.'">'.$item->modelo.'</option>';
            } ?>
        </select>
      </div>
      <div class="col s6">
        <label>Serie</label>
        <input type="text" id="serie_edit" class="form-control-bmz">
      </div>
      <div class="col s2">
        
      </div>
    </div>


    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect waves-green gray btn-flat " onclick="save_edit_dll()">Guardar</a>
      <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
    </div>
  </div>
</div>

