<?php 
	
	header("Pragma: public");
	header("Expires: 0");
	$filename = "Facturas.xls";
	header("Content-type: application/x-msdownload");
	header("Content-Disposition: attachment; filename=$filename");
	header("Pragma: no-cache");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");

	
	$html='<table id="tabla_facturacion" name="tabla_facturacion_cliente" class="responsive-table display" cellspacing="0" border="1">
  <thead>
    <tr>
      <th>Serie</th>
      <th>Folio</th>
      <th>RFC</th>
      <th>Razón Social</th>
      <th>Fecha timbre</th>
      <th>Tipo</th>
      <th>#</th>
      <th>RFC PRE</th>
      <th>Razón Social pre</th>
    </tr>
  </thead>
  <tbody>';
  foreach ($getdata->result() as $item) {
  	$wtipo='';
  	if($item->tipo==0){
  		$wtipo='Venta';
  	}
  	if($item->tipo==1){
  		$wtipo='Poliza';
  	}
  	if($item->tipo==2){
  		$wtipo='Renta';
  	}
  	if($item->tipo==3){
  		$wtipo='Venta Combinada';
  	}
  	if($item->Rfc!=$item->rfcv){
  		$color=' style="color:red"';
  	}else{
  		$color='';
  	}

  		$html.='<tr><td>'.$item->serie.'</td><td>'.$item->Folio.'</td><td'.$color.'>'.$item->Rfc.'</td><td>'.$item->Nombre.'</td><td>'.$item->fechatimbre.'</td><td>'.$wtipo.'</td><td>'.$item->ventaId.'</td><td'.$color.' >'.$item->rfcv.'</td><td>'.$item->razon_social.'</td></tr>';
  }
  $html.='</tbody></table>';

  echo $html;
?>