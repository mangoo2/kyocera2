var base_url = $('#base_url').val();
$(document).ready(function($) {
	$('#solenvio').click(function(event) {
		base_url = $('#base_url').val();
		var productos = $("#table_sol tbody > tr");
        //==============================================
            var DATAa  = [];
            productos.each(function(){  
            	var idenvio = $(this).find("input[id*='idenvio']").val();
                var con = $(this).find("input[id*='con']").val();
				var modelo = $(this).find("input[id*='modelo']").val();
				var serie = $(this).find("input[id*='serie']").val();
				var consumible = $(this).find("select[id*='consumible'] option:selected").val();
				var cantidadcon = $(this).find("select[id*='cantidadcon'] option:selected").val();

                var tipo = $(this).find("input[id*='tipo']").val();
                var comen = $(this).find("textarea[id*='comen']").val();
                var row = $(this).find("input[id*='row']").val();

				if(consumible>0 && cantidadcon>0 ){
					item = {}; 
					item ["idenvio"]  =idenvio;
                    item ["contratoid"]  =con;
					item ["equipoid"] =modelo;
                    item ["equiporow"] =row;
					item ["serieid"] =serie;
					item ["consumibleid"] =consumible;
					item ["cantidad"] =cantidadcon;
                    item ["tipo"]=tipo;
                    item ["comen"]=comen;
					DATAa.push(item);
				}
                
            });
            INFOa  = new FormData();
            aInfoa   = JSON.stringify(DATAa);
            //==================================================
                var pro_cot = $("#table_cot tbody > tr");
                var DATAc  = [];
                pro_cot.each(function(){  
                    item = {}; 
                    item ["idEquipo"]  =$(this).find("input[id*='eq_s']").val();
                    item ["piezas"]  =$(this).find("input[id*='ca_s']").val();
                    item ["idConsumible"] =$(this).find("input[id*='co_s']").val();
                    DATAc.push(item);
                });
                aInfoc   = JSON.stringify(DATAc);
            //==================================================
            if(DATAa.length>0){
                var cotdes = $('#comentariocot').val();
            	var datos='productos='+aInfoa+'&cot='+aInfoc+'&cotdes='+cotdes;
            	var urldata=base_url+"index.php/Consumibles_sol/enviosol";
            	console.log(urldata);
            	$.ajax({
                    type:'POST',
                    url: urldata,
                    data: datos,
                    async: false,
			        statusCode:{
			            404: function(data){
			                alerterror();
			            },
			            500: function(data){
			                alerterror();
			            }
			        },
                    success: function (response){
                    	console.log(response);
                        swal({
                            title:"Éxito!", 
                            text:'Solicitud enviada', 
                            type:"success"
                            },
                            function(){
                                window.location.reload()
                            });
                        //setTimeout(function(){ 
                            //window.location.href = base_url+"index.php/Facturaslis/"; 
                        //}, 5000);
                        
                    },
                    error: function(response){
                        alerterror();
                    }
                });
            }else{
            	toastr["warning"]("Seleccione consumibles y especifique la cantidad");
            }
	});
});
function alerterror(){
	swal({
        title:"Advertencia!", 
        text:'Algo salió mal, intente de nuevo o contacte al administrador del sistema', 
        type:"error"
        });
}
function funcionConsumibles(){
    var equiporow=$('#modeloselect option:selected').val();
    cargaConsumiblesPorEquipo(equiporow);
}
function cargaConsumiblesPorEquipo(idEquipo){
    base_url = $('#base_url').val();
    var urll=base_url+"index.php/Consumibles_sol/getDatosConsumiblesPorEquipo/"+idEquipo;
    console.log(urll);
    $.ajax({
        url: urll,
        success: function (response){
            console.log(response);
            $('#consumibleselect').html('');
            $('#consumibleselect').html(response);
        },
        error: function(response){
            toastr["error"]("Algo salio mal, intente de nuevo o contacte al administrador del sistema");
        }
    }); 
}
var roweq=0;
function agregarrow(){
    var eq=$('#modeloselect option:selected').val();
    var eq_t=$('#modeloselect option:selected').text();
    var co=$('#consumibleselect option:selected').val();
    var co_t=$('#consumibleselect option:selected').text();
    var ca=$('#cantidadselect option:selected').val();

  var html='<tr class="add_row_'+roweq+'">';
      html+='<td>';
      html+='<input type="hidden" id="eq_s" value="'+eq+'">';
      html+='<input type="hidden" id="co_s" value="'+co+'">';
      html+='<input type="hidden" id="ca_s" value="'+ca+'">';
      html+=eq_t;
      html+='</td>'; 
      html+='<td>';
      html+=co_t;
      html+='</td>'; 
      html+='<td>';
      html+=ca;
      html+='</td>'; 
      html+='<td>';
      html+='<button type="button" class="btn btn-danger" onclick="deleterrow('+roweq+')">X</button>';
      html+='</td>';  
      html+='</tr>';
      roweq++;
    $('.tbody_cot').append(html);
}
function deleterrow(row){
    $('.add_row_'+row).remove();
}