var base_url = $('#base_url').val();
var tabla ='';
$(document).ready(function() {
	    $('.formsubmitsave').click(function(event) {
        $( "#equipos_form" ).submit();
    });
    
    $('.dropify').dropify({
        messages: {default: 'Arrastra y suelta un archivo aquí o haz clic',replace: 'Arrastre y suelte un archivo o haga clic para reemplazar',remove:  'Eliminar',error:  'Lo sentimos, el archivo es demasiado grande.'}
    });
});
function mostrarhojaestado() {
    $.ajax({
	    type:'POST',
	    url: base_url+"index.php/Configuracionrentas/gethojaestado",
	    data: {
	    	idcontrato:$('#idcontrato').val(),
            fechainicio:$('#fechainicio').val(),
            fechafin:$('#fechafin').val(),
            tipo:1
	   },
	    success:function(data){  
	       $('.mostrar_hojaestado').html(data);
	        tabla = $('#tabla_estadorefaccion').DataTable();
            $('.materialboxed').materialbox();
	    }
    });
}
function imgeditar(id,ti) {
	$('#modal_img').modal();
    $('#modal_img').modal('open');
    $('#idestado_r').val(id);
    $('#ti').val(ti);
}
function editarimg() {
	var ti = parseFloat($('#ti').val());
    if ($('#foto')[0].files.length > 0) {
        var inputFileImage = document.getElementById('foto');
        var file = inputFileImage.files[0];
        var data = new FormData();
        data.append('foto',file);
        data.append('idestado',$('#idestado_r').val());
        $.ajax({
            url:base_url+'index.php/Configuracionrentas/cargafiles',
            type:'POST',
            contentType:false,
            data:data,
            processData:false,
            cache:false,
            success: function(data) {
                var array = $.parseJSON(data);
                    //console.log(array.ok);
                    if (array.ok=true) {
                      swal("Éxito!", "Se guardo el archivo correctamente", "success");

                    }else{
                      swal("Error", "Error", "error"); 
                    }
                if(ti == 1){
                   $('.mostrar_hojaestado2').remove();  
                   mostrarhojaestado();
                }else{
                   $('.mostrar_hojaestado2').remove();  
                   mostrarrefacciones();
                } 
            },
            error: function(jqXHR, textStatus, errorThrown) {
                var data = JSON.parse(jqXHR.responseText);
            }
        });
    }
}
function detalle(idestado){
    window.open(base_url+"Configuracionrentas/imgrentas/"+idestado, "Foto", "width=780, height=612");
}
function detalleimg(idequipo,idserie) {
    var fechainicio = $('#periodo_inicial').val();
    var fechafin = $('#periodo_final').val();
    window.open(base_url+"Configuracionrentas/imgrentasfactura/"+idequipo+"/"+idserie+"/"+fechainicio+"/"+fechafin, "Foto", "width=780, height=612");
}