<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/******** Esta parte es para poder leer archivo Excel ***********/
require_once APPPATH.'/third_party/Spout/Autoloader/autoload.php';
use Box\Spout\Reader\ReaderFactory;  
use Box\Spout\Common\Type;
/****************************************************************/

class Equipos extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->model('ModeloCatalogos');
        $this->load->model('Equipos_model', 'model');
        $this->load->model('login_model');
        $this->load->helper('url');
        $this->idpersonal = $this->session->userdata('idpersonal');
        $this->perfilid =$this->session->userdata('perfilid');
        $this->submenu=3;
    }

    // Listado de Equipos
    function index(){
        $data['perfilid']=$this->perfilid;
        $data['MenusubId']=$this->submenu;
        $this->load->view('header');
        $this->load->view('main',$data);
        $this->load->view('equipos/listado');
        $this->load->view('equipos/listado_js');
        $this->load->view('footer');     
    }

    // Alta de Equipo
    function alta(){
        $data['perfilid']=$this->perfilid;
        $data['tipoVista'] = 1;
        $data['categoria_equipo'] = $this->model->getDataEquipo();
        $data['accesorio'] = $this->model->getAccesorios();
        $data['familia'] = $this->model->getDataFamilia();
        $wherec=array('status'=>1);
        $data['consumibles'] = $this->ModeloCatalogos->getselectwheren('consumibles',$wherec);
        $data['equipotipo']=0;
        $data['MenusubId']=$this->submenu;
        $this->load->view('header');
        $this->load->view('main',$data);
        $this->load->view('equipos/form_equipos');
        //$this->load->view('panel');
        $this->load->view('equipos/form_equipos_js');
        $this->load->view('footer');
        
    }

    // Edicion de Equipo
    function edicion($idEquipo){
        $data['perfilid']=$this->perfilid;
        $data['categoria_equipo'] = $this->model->getDataEquipo();
        $data['equipo'] = $this->model->getEquipoPorId($idEquipo);
        $data['tipoVista'] = 2;
        $data['accesorio'] = $this->model->getAccesorios();
        $data['familia'] = $this->model->getDataFamilia();
        $data['consumibles'] = $this->ModeloCatalogos->consumibleshasequipos($idEquipo);
        $data['equipotipo']=$idEquipo;
        $data['MenusubId']=$this->submenu;
        $this->load->view('header');
        $this->load->view('main',$data);
        $this->load->view('equipos/form_equipos');
        $this->load->view('equipos/form_equipos_js');
        $this->load->view('footer');
    }

    // Visualización de Equipo
    function visualizar($idEquipo){
        $data['perfilid']=$this->perfilid;
        $data['categoria_equipo'] = $this->model->getDataEquipo();
        $data['equipo'] = $this->model->getEquipoPorId($idEquipo);
        $data['tipoVista'] = 3;
        $data['accesorio'] = $this->model->getAccesorios();
        $data['familia'] = $this->model->getDataFamilia();
        $data['consumibles'] = $this->ModeloCatalogos->consumibleshasequipos($idEquipo);
        $data['equipotipo']=$idEquipo;
        $data['MenusubId']=$this->submenu;
        $this->load->view('header');
        $this->load->view('main',$data);
        $this->load->view('equipos/form_equipos');
        $this->load->view('equipos/form_equipos_js');
        $this->load->view('footer');
    }

    function getMenus($idPerfil){
        // Obtenemos los menús del perfil
        $menus = $this->login_model->getMenus($idPerfil);
        $submenusArray = array();
        // Obtenemos y asignamos los submenus 
        foreach ($menus as $item)
        {
            array_push($submenusArray, $this->login_model->submenus($idPerfil,$item->MenuId));
        }
        // Los asignamos al arary que se envía a la vista
        $data['menus'] = $menus;
        $data['submenusArray'] = $submenusArray;
        return $data;
    }

    function getListadoEquipos(){
        $equipos = $this->model->getListadoEquipos();
        $json_data = array("data" => $equipos);
        echo json_encode($json_data);
    }

    function insertaActualizaEquipo(){
        $data = $this->input->post();
        //var_dump($data);die;
        $accesorios = $this->input->post('accesorios2');
        //var_dump($accesorios);die;
        //$costo_pagina_color=$data["costo_pagina_color"];
        //log_message('error', 'costo_pagina_color'.$costo_pagina_color);
        $result = 0; 
        // Ordenamos los datos principales del cliente
                       $datosEquipos = array(
                            'modelo' => $data["modelo"],
                            'noparte' => $data["noparte"],
                            'especificaciones' => $data["especificaciones"],
                            'especificaciones_tecnicas' => $data["especificaciones_tecnicas"],
                            'pag_monocromo_incluidos' => $data["pag_monocromo_incluidos"],
                            'excedente' => $data["excedente"],
                            'pag_color_incluidos' => $data["pag_color_incluidos"],
                            'excedentecolor' => $data["excedentecolor"],
                            'categoriaId' => $data["categoriaId"],
                            'stock' => $data["stock"],
                            'idfamilia' => $data["idfamilia"],
                            'estatus' => 1
                            /*,
                            'costo_toner_black' => $data["costo_toner_black"],
                            'rendimiento_toner_black' => $data["rendimiento_toner_black"],
                            'costo_toner_cmy' => $data["costo_toner_cmy"],
                            'rendimiento_toner_cmy' => $data["rendimiento_toner_cmy"],
                            'costo_unidad_imagen' => $data["costo_unidad_imagen"],
                            'rendimiento_unidad_imagen' => $data["rendimiento_unidad_imagen"],
                            'costo_garantia_servicio' => $data["costo_garantia_servicio"],
                            'rendimiento_garantia_servicio' => $data["rendimiento_garantia_servicio"],
                            'costo_pagina_monocromatica' => $data["costo_pagina_monocromatica"],
                            'costo_pagina_color' => $costo_pagina_color
                            */


                        );
                       
        if(!isset($data['idEquipo']))
        {   
            $id = $this->model->insertar_equipo($datosEquipos); 
            $result = $id;
            //$this->Modelobitacoras->Insert(array('contenido'=>'Se Creo Equipo','nombretabla'=>'equipos','idtable'=>$id,'tipo'=>'Insert','personalId'=>$this->idpersonal)); 
            $this->ModeloCatalogos->updateuuid($id,1);
        }
        else
        {
           $id=$data['idEquipo'];
           unset($data['idEquipo']);
           $result = $this->model->update_equipo($datosEquipos,$id); 
           //$this->Modelobitacoras->Insert(array('contenido'=>'Se Modifico Equipo','nombretabla'=>'equipos','idtable'=>$id,'tipo'=>'Insert','personalId'=>$this->idpersonal)); 
           
        }

        $DATA = json_decode($accesorios);
           /// var_dump($DATA);die;
                for ($i = 0; $i < count($DATA); $i++) {
                    $accesorio = $DATA[$i]->accesorio;
                        $temp=array(
                            "idcatalogo_accesorio"=>$accesorio,
                            "idEquipo"=>$id
                        );
                        if($accesorio>=1){
                           $this->model->insertarToCatalogo($temp,"accesorios");
                        } 
                        
                    } 
            
        echo $result; 
    }

    function cargafiles(){
        $consumibles = $this->input->post('equipos');
        $upload_folder ='uploads/equipos';
        $nombre_archivo = $_FILES['foto']['name'];
        $tipo_archivo = $_FILES['foto']['type'];
        $tamano_archivo = $_FILES['foto']['size'];
        $tmp_archivo = $_FILES['foto']['tmp_name'];
        //$archivador = $upload_folder . '/' . $nombre_archivo;
        $fecha=date('ymd-His');
        $newfile='r_-'.$fecha.'-'.$nombre_archivo;        
        $archivador = $upload_folder . '/'.$newfile;
        if (!move_uploaded_file($tmp_archivo, $archivador)) {
            $return = Array('ok' => FALSE, 'msg' => 'Ocurrió un error al subir el archivo. No pudo guardarse.', 'status' => 'error');
        }else{
            $arrayfoto = array('foto' => $newfile, );
            $this->model->update_foto($arrayfoto,$consumibles);
            $return = Array('ok'=>TRUE);
        }
        echo json_encode($return);
    }
    
    function getListadoAcesoriosEquipo($idEquipo,$bodega=0){
        $accesorios = $this->model->getListadoAcesoriosEquipo($idEquipo,$bodega);
        $json_data = array("data" => $accesorios);
        echo json_encode($json_data);
    }

    function eliminarEquipo($idEquipo){
        $datosEquipo = array('estatus' => 0);
        $eliminado = $this->model->update_equipo($datosEquipo, $idEquipo);
        echo $eliminado;
    }

    function eliminarAccesorio($idAccesorio){
        $eliminado = $this->model->eliminar_accesorio($idAccesorio);
        echo $eliminado;
    }

    // Función para guardar el archivo de Excel en la BD
    function cargaArchivo(){
        // Borramos la tabla donde se encuentran las asignaciones hoy
        // no se pueden borrar o truncar estas tablas por que hay tablas relacionadas
        //$this->model->truncateTable('equipos');
                    
        /***** Acá empieza la parte para poder leer el archivo e insertarlo en la BD *****/
        
        // Nombre Temporal del Archivo
        $inputFileName = $_FILES['inputFile']['tmp_name']; 
        
        //Lee el Archivo usando ReaderFactory
        $reader = ReaderFactory::create(Type::XLSX);
        
        //Esta linea mantiene el formato de nuestras horas y fechas
        //Sin esta linea Spout convierte la hora y fecha a su propio formato predefinido como DataTime
        $reader->setShouldFormatDates(true);

        // Abrimos el archivo
        $reader->open($inputFileName);
        $count = 1;

        // Numero de hojas en el documento EXCEL 
        foreach ($reader->getSheetIterator() as $sheet) 
        {
            // Numero de filas en el documento EXCEL
            foreach ($sheet->getRowIterator() as $row) 
            {
                // Lee los Datos despues del encabezado
                // El encabezado se encuentra en la primera fila y debe omitirse, 
                // por eso la variable $count se inicializa en 1 y no en 0
                if($count > 1) 
                {
                    $data = array(
                        'modelo' => $row[0],
                        'especificaciones' => $row[1],
                        'foto' => '',
                        'especificaciones_tecnicas' => $row[2],
                        'costo_venta' => $row[3],
                        'costo_renta' => $row[4],
                        'costo_poliza' => $row[5],
                        'categoriaId' => $row[6],
                        'stock' => $row[7],
                        //'costo_total' => $row[8],
                        'estatus' => 1,
                        'costo_revendedor' => $row[9],
                        'pag_monocromo_incluidos' => $row[10],
                        'pag_color_incluidos' => $row[11],
                        'excedente' => $row[12],
                        'excedentecolor' => $row[13],
                        'idfamilia' => 0,
                    ); 
                    $this->model->insertar_equipo($data);
                } 
                $count++;
            }
        }
        // cerramos el archivo EXCEL
        $reader->close();
        /***** Acá termina la parte para poder leer el archivo e insertarlo en la BD *****/  
        //$this->Modelobitacoras->Insert(array('contenido'=>'Se Cargo archivo Equipos','nombretabla'=>'equipos','idtable'=>0,'tipo'=>'Insert','personalId'=>$this->idpersonal));       
    }
    function accesorios(){
        $id = $this->input->post('id');
        $accesorios = $this->model->getListadoAcesoriosEquipo($id,0);
        $consumibles = $this->ModeloCatalogos->consumiblesequipos($id,0);//en el sero puede ser sustituido para consultar stock de bodegas
        $equiporow = $this->model->getEquipoPorId($id);

        $table='
        <table class="striped tablemaccesorios">
                      <thead>
                        <tr>
                          <th></th>
                          <th>Accesorio</th>
                          <th>Stock</th>
                        </tr>
                      </thead>
                      <tbody>';
                        foreach ($accesorios as $item) {
                            if (($item->stock+$item->stock2)>0) {
                                $bloqueo=0;
                                $stock=$item->stock+$item->stock2;
                            }else{
                                $bloqueo=1;
                                $stock=0;
                            }
                            $button=$id.",".$item->idcatalogo_accesorio.",'".$item->nombre."',".$bloqueo;
                            $table.='<tr>
                                          <td>
                                                <a class="btn-floating waves-effect waves-light amber darken-4" onclick="addaccesorio('.$button.')">
                                                    <i class="material-icons">play_for_work</i>
                                                </a></td>
                                          <td>'.$item->nombre.'</td>
                                          <td>'.$stock.'</td>
                                    </tr>';
                        }
                        
                     $table.=' </tbody>
                    </table>';
        $tablec='
        <table class="striped tableconsu">
                      <thead>
                        <tr>
                          <th></th>
                          <th>Consumible</th>
                          <th>Stock</th>
                        </tr>
                      </thead>
                      <tbody>';
                        foreach ($consumibles->result() as $item) {
                            if ($item->stock>0) {
                                $bloqueo=0;
                                $stock=$item->stock;
                            }else{
                                $bloqueo=1;
                                $stock=0;
                            }
                            $button=$id.",".$item->idconsumible.",'".$item->modelo."',".$bloqueo;
                            $tablec.='<tr>
                                          <td>
                                                <a class="btn-floating waves-effect waves-light amber darken-4" onclick="addconsu('.$button.')">
                                                    <i class="material-icons">play_for_work</i>
                                                </a></td>
                                          <td>'.$item->modelo.' </td>
                                          <td>'.$stock.'</td>
                                    </tr>';
                        }
                        
                     $tablec.=' </tbody>
                    </table>';
        //=====================================================================================
        $pag_monocromo_incluidos=$equiporow->pag_monocromo_incluidos;
        $pag_color_incluidos=$equiporow->pag_color_incluidos;

        $rowcatequi=$this->ModeloCatalogos->getselectwheren('categoria_equipos',array('id'=>$equiporow->categoriaId));
        $tipoce=0;
        foreach ($rowcatequi->result() as $itemce) {
            $tipoce=$itemce->tipo;
        }
        if ($tipoce==1) {
            $color=0;
            $mono=1;
        }elseif ($tipoce==2) {
            $color=1;
            $mono=0;
        }elseif ($tipoce==3) {
            $color=1;
            $mono=1;
        }else{
            $color=1;
            $mono=1;
        }

        $rendimientoarray= array(
                            'r_mono'=>$pag_monocromo_incluidos,
                            'r_color'=>$pag_color_incluidos,
                            'color'=>$color,
                            'mono'=>$mono
                        );

        $array = array("accesorios"=>$table,
                        "consumibles"=>$tablec,
                        "rendimiento"=>$rendimientoarray
                    );
        echo json_encode($array);
    }
    function preciosaccesorios(){
        $consu = $this->input->post('consu');
        $where=array('consumibles_id'=>$consu);
        $resultados=$this->ModeloCatalogos->getselectwheren('consumibles_costos',$where);
        $poliza=0;
        foreach ($resultados->result() as $item) {
            $poliza=$item->poliza;
        }
        $wherec=array('id'=>$consu);
        $resultadosc=$this->ModeloCatalogos->getselectwheren('consumibles',$wherec);
        $rendimiento_unidad_imagen=0;
        $rendimiento_toner=0;
        foreach ($resultadosc->result() as $item) {
            $rendimiento_unidad_imagen=$item->rendimiento_unidad_imagen;
            $rendimiento_toner=$item->rendimiento_toner;
            $tipo=$item->tipo;
        }





        $array = array("poliza"=>$poliza,
                        "rendimiento_toner"=>$rendimiento_toner,
                        "rendimiento_unidad_imagen"=>$rendimiento_unidad_imagen,
                        "tipo"=>$tipo
                    );
        echo json_encode($array);
    }
    function addconstocunsumibles(){
        $datos = $this->input->post('data');
        $DATA = json_decode($datos);
        for ($i=0;$i<count($DATA);$i++) { 
            $id= $DATA[$i]->id;
            $datas['idequipo']= $DATA[$i]->idequipo;
            $datas['idconsumible']= $DATA[$i]->idconsumible;
            //$datas['rendimiento_toner_black']= $DATA[$i]->rendimiento_toner_black;
            //$datas['rendimiento_toner_cmy']= $DATA[$i]->rendimiento_toner_cmy;
            $datas['costo_unidad_imagen']= $DATA[$i]->costo_unidad_imagen;
            //$datas['rendimiento_unidad_imagen']= $DATA[$i]->rendimiento_unidad_imagen;
            $datas['costo_garantia_servicio']= $DATA[$i]->costo_garantia_servicio;
            $datas['rendimiento_garantia_servicio']= $DATA[$i]->rendimiento_garantia_servicio;
            $datas['costo_pagina_monocromatica']= $DATA[$i]->costo_pagina_monocromatica;
            $datas['costo_pagina_color']= $DATA[$i]->costo_pagina_color;
            if ($id>0) {
                $where = array('id'=>$id);
                $this->ModeloCatalogos->updateCatalogo('equipos_consumibles',$datas,$where);
            }else{
                $this->ModeloCatalogos->Insert('equipos_consumibles',$datas);
            }

        }
    }
    function deleteconsumible(){
        $id = $this->input->post('consu');
        $where = array('id'=>$id);
         $datas['activo']= 0;
        $this->ModeloCatalogos->updateCatalogo('equipos_consumibles',$datas,$where);
    }
    function export(){
        $strq = "SELECT equ.*,cequ.nombre as categoria
                FROM equipos as equ 
                inner join categoria_equipos as cequ on cequ.id=equ.categoriaId
                where equ.estatus=1";
        $data['query'] = $this->db->query($strq);
        $this->load->view('equipos/export',$data);
    }
    function verificarmodeloexist(){
        $params = $this->input->post();
        $id=$params['id'];
        if($id>0){
            $whereid=" and  id!= $id";
        }else{
            $whereid="";
        }
        $modelo=$params['modelo'];
        if($modelo!=''){
            $wheremodelo=" and modelo='$modelo' ";
        }else{
            $wheremodelo="";
        }
        
        $strq = "SELECT * FROM equipos where estatus=1 $whereid  $wheremodelo ";
        $query = $this->db->query($strq);
        $idequipo=0;
        foreach ($query->result() as $item) {
            $idequipo=$item->id;
        }
        echo $idequipo; 
    }
    function verificarmodeloexist2(){
        $params = $this->input->post();
        $id=$params['id'];
        if($id>0){
            $whereid=" and  id!= $id";
        }else{
            $whereid="";
        }
        
        $noparte=$params['noparte'];
        if($noparte==0){
            $wherenoparte="";
        }elseif ($noparte!='') {
            $wherenoparte=" and noparte='$noparte'";
        }else{
            $wherenoparte="";
        }
        $strq = "SELECT * FROM equipos where estatus=1 $whereid  $wherenoparte ";
        $query = $this->db->query($strq);
        $idequipo=0;
        foreach ($query->result() as $item) {
            $idequipo=$item->id;
        }
        echo $idequipo; 
    }
    function procesarconsumibles(){
        $resultequipos=$this->ModeloCatalogos->getselectwheren('equipos',array('estatus'=>1));
        foreach ($resultequipos->result() as $itemeq) {
            $idEquipo = $itemeq->id;
            $resultconsumibles = $this->ModeloCatalogos->consumibleagregado($idEquipo);
            foreach ($resultconsumibles->result() as $itemc) {
                echo $itemc->id.' '.$itemc->modelo.'<br>';
                $idrow = $itemc->id;
                $idconsumible = $itemc->idconsumible;
                //======================================================
                    
                    $resultados=$this->ModeloCatalogos->getselectwheren('consumibles_costos',array('consumibles_id'=>$idconsumible));
                    $poliza=0;
                    foreach ($resultados->result() as $item) {
                        $poliza=$item->poliza;
                    }
                //======================================================
                    $resultadosc=$this->ModeloCatalogos->getselectwheren('consumibles',array('id'=>$idconsumible));
                    //$rendimiento_unidad_imagen=0;
                    $rendimiento_toner=0;
                    foreach ($resultadosc->result() as $item) {
                        //$rendimiento_unidad_imagen=$item->rendimiento_unidad_imagen;
                        $rendimiento_toner=$item->rendimiento_toner;
                        //$tipo=$item->tipo;
                    }
                //======================================================
                //$poliza=$itemc->poliza;
                //$rendimiento_toner=$itemc->rendimiento_toner;
                if($rendimiento_toner>0){
                    $costoporpagina=$poliza/$rendimiento_toner;
                    $costoporpagina=round($costoporpagina, 2);

                    $this->ModeloCatalogos->updateCatalogo('equipos_consumibles',array('costo_pagina_color'=>$costoporpagina),array('id'=>$idrow,'costo_pagina_color'=>0));
                    $this->ModeloCatalogos->updateCatalogo('equipos_consumibles',array('costo_pagina_monocromatica'=>$costoporpagina),array('id'=>$idrow,'costo_pagina_monocromatica'=>0));
                }
            }
        }
    }
    function imagenes_multiple(){
        $idequipo=$_POST['idequipo'];
      $data = [];
   
      $count = count($_FILES['files']['name']);
      $output = [];
      for($i=0;$i<$count;$i++){
    
        if(!empty($_FILES['files']['name'][$i])){
    
          $_FILES['file']['name'] = $_FILES['files']['name'][$i];
          $_FILES['file']['type'] = $_FILES['files']['type'][$i];
          $_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
          $_FILES['file']['error'] = $_FILES['files']['error'][$i];
          $_FILES['file']['size'] = $_FILES['files']['size'][$i];
          $DIR_SUC=FCPATH.'uploads/equipos';

          $config['upload_path'] = $DIR_SUC; 
          //$config['allowed_types'] = 'docx|xlsx|pdf|doc|xls|pptx';
          $config['allowed_types'] = '*';

          //$config['allowed_types'] = 'docx|xlsx|pdf|jpg|jpeg|png|gif';
          $config['max_size'] = 5000;
          $file_names=date('Ymd_His').'_'.rand(0, 500);
          //$config['file_name'] = $file_names.'_'.str_replace(' ', '_', $_FILES['files']['name'][$i]);
          $config['file_name'] = $file_names;
          //$config['file_name'] = $file_names;
   
          $this->load->library('upload',$config); 
    
          if($this->upload->do_upload('file')){
            $uploadData = $this->upload->data();
            $filename = $uploadData['file_name'];
            $filenametype = $uploadData['file_ext'];
   
            $data['totalFiles'][] = $filename;
            //$this->ModeloGeneral->tabla_inserta('equipo_imgs',array('idequipo'=>$idequipo,'imagen'=>$filename,'filetype'=>$filenametype,'usuario'=>$this->idpersonal));
            $this->ModeloCatalogos->Insert('equipo_imgs',array('idequipo'=>$idequipo,'imagen'=>$filename));
          }else{
            $data = array('error' => $this->upload->display_errors());
            log_message('error', json_encode($data)); 
          }
        }
   
      }
      echo json_encode($output);
    }
    function viewimages(){
        $params = $this->input->post();
        $id=$params['idequipo'];
        $resultados=$this->ModeloCatalogos->getselectwheren('equipo_imgs',array('idequipo'=>$id,'activo'=>1));
        $html='';
                foreach ($resultados->result() as $item) {
                    $html.='<div class="card_img"><div class="card_img_c"><img class="materialboxed" src="'.base_url().'uploads/equipos/'.$item->imagen.'"></div><div class="card_img_a">
                                <a class="btn-floating red tooltipped" data-position="top" data-delay="50" data-tooltip="Eliminar" onclick="deleteimg('.$item->id.')"><i class="material-icons">delete_forever</i></a></div></div>';
                }
        echo $html;
    }
    function deleteimg(){
        $params = $this->input->post();
        $id=$params['idequipo'];
        $this->ModeloCatalogos->updateCatalogo('equipo_imgs',array('activo'=>0),array('id'=>$id));
    }
    function insertaActualizacaracteristicas(){
        $params=$this->input->post();
        $id = $params['id'];
        unset($params['id']);
        if($id>0){
            $this->ModeloCatalogos->updateCatalogo('equipos_caracteristicas',$params,array('id'=>$id));
        }else{
            $this->ModeloCatalogos->Insert('equipos_caracteristicas',$params);
        }
    }
    function viewcaracte(){
        $params = $this->input->post();
        $id=$params['idequipo'];
        //$resultados=$this->ModeloCatalogos->getselectwheren('equipos_caracteristicas',array('idequipo'=>$id,'activo'=>1));
        
        $strq = "SELECT * FROM equipos_caracteristicas where idequipo=$id and activo=1 ORDER BY orden ASC";
        $query = $this->db->query($strq);

        $html='<table class="table striped"><thead><tr><th>Tipo</th><th>Descripcion</th><th>General</th><th>Orden</th><th></th></tr></thead><tbody>';
        foreach ($query->result() as $item) {
            if($item->general==1){
                $general='SI';
            }else{
                $general='';
            }
            $html.='<tr><td>'.$item->name.'</td><td>'.$item->descripcion.'</td><td>'.$general.'</td><td>'.$item->orden.'</td>
                        <td>
                            <a class="btn-floating green tooltipped edit soloadministradores eq_cact_'.$item->id.'" 
                                data-name="'.$item->name.'"
                                data-descripcion="'.$item->descripcion.'"
                                data-general="'.$item->general.'"
                                data-orden="'.$item->orden.'"
                                onclick="editar_ec('.$item->id.')" data-position="top" data-delay="50" data-tooltip="Editar" data-tooltip-id="f5aed0ec-5db2-aa68-1319-23959ebc0465"><i class="material-icons">mode_edit</i></a>
                            <a class="btn-floating red tooltipped elimina soloadministradores" data-position="top" data-delay="50" data-tooltip="Eliminar" data-tooltip-id="7817ec6e-15c1-ab9a-76f3-cff8bc1c9ab4" onclick="delete_ec('.$item->id.')"><i class="material-icons">delete_forever</i></a>
                        </td>
                    </tr>';
        }
        $html.='</tbody></table>';
        echo $html;
    }
    function deleteec(){
        $params = $this->input->post();
        $id=$params['idequipo'];
        $this->ModeloCatalogos->updateCatalogo('equipos_caracteristicas',array('activo'=>0),array('id'=>$id));
    }
    function mostrarweb(){
        $params = $this->input->post();
        $id=$params['idequipo'];
        $mostrar = $params['mostrar'];
        $this->ModeloCatalogos->updateCatalogo('equipos',array('paginaweb'=>$mostrar),array('id'=>$id));
    }
    function favorito(){
        $params = $this->input->post();
        $id=$params['equipo'];
        $mostrar = $params['status'];
        $this->ModeloCatalogos->updateCatalogo('equipos',array('destacado'=>$mostrar),array('id'=>$id));
        echo $mostrar;
    }
}