<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/******** Esta parte es para poder leer archivo Excel ***********/
require_once APPPATH.'/third_party/Spout/Autoloader/autoload.php';
use Box\Spout\Reader\ReaderFactory;  
use Box\Spout\Common\Type;
/****************************************************************/

class Refacciones extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('Refacciones_model', 'model');
        $this->load->model('login_model');
        $this->load->model('ModeloCatalogos');
        $this->load->helper('url');
        $this->idpersonal = $this->session->userdata('idpersonal');
        $this->perfilid =$this->session->userdata('perfilid');
    }

    // Listado de Empleados
    function index() {
        $data['MenusubId']=5;
        $this->load->view('header');
        $this->load->view('main',$data);
        $this->load->view('refacciones/listado');
        $this->load->view('refacciones/listado_js');
        $this->load->view('footer');
    }
 
    // Listado de Empleados
    function alta() {
        $data['MenusubId']=5;
        $data['perfilid']=$this->perfilid;
        $data['tipoVista'] = 1;
        $equipo = $this->model->getDataEquipos();
        $data['equipo'] = $equipo;
        $data['categoria_refaccion'] = $this->model->getData_categoria_refacciones();
        $this->load->view('header');
        $this->load->view('main',$data);
        $this->load->view('refacciones/form_refacciones');
        $this->load->view('refacciones/form_refacciones_js');
        $this->load->view('footer');
        
    }

    // Vista de la Edición de Clientes
    function edicion($id) {
        $data['MenusubId']=5;
        $data['perfilid']=$this->perfilid;
        $equipo = $this->model->getDataEquipos();
        $data['equipo'] = $equipo;
        $data['refaciones_categoria'] = $this->model->getRefaccionesPorId($id);
        $data['categoria_refaccion'] = $this->model->getData_categoria_refacciones();
        $data['tipoVista'] = 2;
        $this->load->view('header');
        $this->load->view('main',$data);
        $this->load->view('refacciones/form_refacciones');
        //$this->load->view('panel');
        $this->load->view('refacciones/form_refacciones_js');
        $this->load->view('footer');
        
    }


     // Vista de la Visualización de Refacciones
    function visualizar($id) {
        $data['MenusubId']=5;
        $data['perfilid']=$this->perfilid;
        $equipo = $this->model->getDataEquipos();
        $data['equipo'] = $equipo;
        $data['refaciones_categoria'] = $this->model->getRefaccionesPorId($id);
        $data['categoria_refaccion'] = $this->model->getData_categoria_refacciones();
        $data['tipoVista'] = 3;
        
        $this->load->view('header');
        $this->load->view('main',$data);
        $this->load->view('refacciones/form_refacciones');
        //$this->load->view('panel');
        $this->load->view('refacciones/form_refacciones_js');
        $this->load->view('footer');
    }

    // Eliminar Refaccion
    function eliminarRefaccion($id) 
    {
        $datos = array('status' => 0);
        $eliminado = $this->model->update_refacciones($datos, $id);
        echo $eliminado;
        //$this->Modelobitacoras->Insert(array('contenido'=>'Se Elimino Refaccion','nombretabla'=>'refacciones','idtable'=>$id,'tipo'=>'Delete','personalId'=>$this->idpersonal));
    }

    function getMenus($idPerfil)
    {
        // Obtenemos los menús del perfil
        $menus = $this->login_model->getMenus($idPerfil);
        $submenusArray = array();
        // Obtenemos y asignamos los submenus 
        foreach ($menus as $item)
        {
            array_push($submenusArray, $this->login_model->submenus($idPerfil,$item->MenuId));
        }
        // Los asignamos al arary que se envía a la vista
        $data['menus'] = $menus;
        $data['submenusArray'] = $submenusArray;
        return $data;
    }

    public function insertaActualizaRefaciones()
    {
        $result = 0; 
        $data = $this->input->post();
        $equipos = $this->input->post('equipos2');
           // var_dump($data);die;
            // var_dump($_FILES);die;
                   // Ordenamos los datos principales del cliente
            $datosRefacciones = array(
                                'codigo' => $data["codigo"],
                                'nombre' => $data["nombre"],
                                'categoria' => $data["categoria"],
                                'stock' => $data["stock"],
                                'observaciones' => $data["observaciones"],
                                'rendimiento' => $data["rendimiento"],
                                'precio_usa' => 0,
                                'precio_unitario' => 0,
                                'precio_total' => 0
                                );
            if(!isset($data['idRefacciones'])){
                $datosRefacciones['uuid']='UUID()';
               $id = $this->model->insertar_refacciones($datosRefacciones);
               $result = $id;
               $data = array(
                    'general' => 0,
                    'frecuente' => 0,
                    'especial' => 0,
                    'poliza' => 0,
                    'refacciones_id' => $id,
                ); 
                $this->model->insertar_refacciones_costos($data);
                //$this->Modelobitacoras->Insert(array('contenido'=>'Se Creo Refaccion','nombretabla'=>'refacciones','idtable'=>$id,'tipo'=>'Insert','personalId'=>$this->idpersonal));

                $DATA = json_decode($equipos);
                
                for ($i = 0; $i < count($DATA); $i++) {
                    $idEquipo = $DATA[$i]->idEquipo;
                    if ($idEquipo>0) {
                        $temp=array(
                            "idEquipo"=>$idEquipo,
                            "idRefacciones"=>$id
                        );
                        $this->model->insertar_equipo($temp,"equipos_has_refacciones");
                    }
                        
                } 
                $this->ModeloCatalogos->updateuuid($id,2);
            }
            else{
                $DAT = json_decode($equipos);
                for ($i = 0; $i < count($DAT); $i++) {
                    $equipo_1 = $DAT[$i]->idEquipo;  
                    if ($equipo_1!=0) {
                           $e = $equipo_1;
                        }else{
                           $e = 0; 
                        }      
                }
                $datos = $this->model->verificarequipo($e,$data['idRefacciones']);
                $equi=0;
                foreach ($datos as $item) {
                   $equi = $item->id;
                }
                if($equi>=1){
                $result = 'existe';           
                }else{
                // update
                $id=$data['idRefacciones'];
                $result = $id;
                unset($data['idRefacciones']);
                $this->model->update_refacciones($datosRefacciones,$id); 
                //$this->Modelobitacoras->Insert(array('contenido'=>'Se Actualizo Refaccion','nombretabla'=>'refacciones','idtable'=>$id,'tipo'=>'Update','personalId'=>$this->idpersonal));
            }

       
                $DATA = json_decode($equipos);
                
                for ($i = 0; $i < count($DATA); $i++) {
                    $idEquipo = $DATA[$i]->idEquipo;
                    if ($idEquipo !='') {
                        $temp=array(
                            "idEquipo"=>$idEquipo,
                            "idRefacciones"=>$id
                        );
                        $this->model->insertar_equipo($temp,"equipos_has_refacciones");
                    }
                        
                } 
        }
        echo $result;  

    }


    function cargafiles(){
        $refacciones = $this->input->post('refacciones');
        $upload_folder ='uploads/refacciones';
        $nombre_archivo = $_FILES['foto']['name'];
        $tipo_archivo = $_FILES['foto']['type'];
        $tamano_archivo = $_FILES['foto']['size'];
        $tmp_archivo = $_FILES['foto']['tmp_name'];
        //$archivador = $upload_folder . '/' . $nombre_archivo;
        $fecha=date('ymd-His');
        $newfile='r_-'.$fecha.'-'.$nombre_archivo;        
        $archivador = $upload_folder . '/'.$newfile;
        if (!move_uploaded_file($tmp_archivo, $archivador)) {
            $return = Array('ok' => FALSE, 'msg' => 'Ocurrió un error al subir el archivo. No pudo guardarse.', 'status' => 'error');
        }else{
            $arrayfoto = array('foto' => $newfile, );
            $this->model->update_foto($arrayfoto,$refacciones);
            $return = Array('ok'=>TRUE);
        }
        echo json_encode($return);
    }

    public function getListadoRefacciones()
    {
        $refacciones = $this->model->getListadoRefacciones();
        $json_data = array("data" => $refacciones);
        echo json_encode($json_data);
    }
   
    function getListado_reacciones($id)
    {
        $refaccion = $this->model->getListado_reacciones($id);
        $json_data = array("data" => $refaccion);
        echo json_encode($json_data);
    }

    function eliminar_has_refacciones($id) 
    {
        $eliminado = $this->model->eliminar_has_refacciones($id);
        echo $eliminado;
        //$this->Modelobitacoras->Insert(array('contenido'=>'Se Elimino Refaccion','nombretabla'=>'refacciones','idtable'=>$id,'tipo'=>'Delete','personalId'=>$this->idpersonal));
    }

    // Función para guardar el archivo de Excel en la BD
    function cargaArchivo()
    {
        // Borramos la tabla donde se encuentran las asignaciones hoy
        $this->model->truncateTable('refacciones');
                    
        /***** Acá empieza la parte para poder leer el archivo e insertarlo en la BD *****/
        
        // Nombre Temporal del Archivo
        $inputFileName = $_FILES['inputFile']['tmp_name']; 
        
        //Lee el Archivo usando ReaderFactory
        $reader = ReaderFactory::create(Type::XLSX);
        
        //Esta linea mantiene el formato de nuestras horas y fechas
        //Sin esta linea Spout convierte la hora y fecha a su propio formato predefinido como DataTime
        $reader->setShouldFormatDates(true);

        // Abrimos el archivo
        $reader->open($inputFileName);
        $count = 1;

        // Numero de hojas en el documento EXCEL 
        foreach ($reader->getSheetIterator() as $sheet) 
        {
            // Numero de filas en el documento EXCEL
            foreach ($sheet->getRowIterator() as $row) 
            {
                // Lee los Datos despues del encabezado
                // El encabezado se encuentra en la primera fila y debe omitirse, 
                // por eso la variable $count se inicializa en 1 y no en 0
                if($count > 1) 
                {
                    $data = array(
                        'foto' => '',
                        'codigo' => $row[0],
                        'nombre' => $row[1],
                        'categoria' => $row[2],
                        //'stock' => $row[3],
                        'observaciones' => $row[3],
                        'precio_usa' => $row[4],
                        //'precio_unitario' => $row[5],
                        //'precio_total' => $row[6],
                        'status' => 1
                    ); 
                    $insertado = $this->model->insertar_refacciones($data);

                    if($insertado > 0)
                    {
                        $data = array(
                            'general' => 0,
                            'frecuente' => 0,
                            'especial' => 0,
                            'poliza' => 0,
                            'refacciones_id' => $insertado,
                        ); 
                        $this->model->insertar_refacciones_costos($data);
                    }
                } 
                $count++;
            }
        }
        // cerramos el archivo EXCEL
        $reader->close();
        /***** Acá termina la parte para poder leer el archivo e insertarlo en la BD *****/        
    }

    
    public function verificarequipo(){
        $data = $this->input->post('datos');
        $id = $this->input->post('idrefac');
        $equipo=0;
        $result=0;
        if($id>=1){
            $datos = $this->model->verificarequipo($data,$id);
            
            foreach ($datos as $item) {
                $equipo = $item->id;
            }
            if($equipo>=1){
               $result = 1;           
            }
        }
        echo $result;
    }
    function verificasiyaexiste(){
        $params = $this->input->post();
        $id=$params['id'];
        $codigo=$params['codigo'];
        $datos = $this->model->verificasiyaexiste($codigo,$id);
        $existe=0;
        foreach ($datos as $item) {
            $existe=1;
        }
        echo $existe;
    } 
    //========================================================================
    function imagenes_multiple(){
        $idequipo=$_POST['idequipo'];
      $data = [];
   
      $count = count($_FILES['files']['name']);
      $output = [];
      for($i=0;$i<$count;$i++){
    
        if(!empty($_FILES['files']['name'][$i])){
    
          $_FILES['file']['name'] = $_FILES['files']['name'][$i];
          $_FILES['file']['type'] = $_FILES['files']['type'][$i];
          $_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
          $_FILES['file']['error'] = $_FILES['files']['error'][$i];
          $_FILES['file']['size'] = $_FILES['files']['size'][$i];
          $DIR_SUC=FCPATH.'uploads/equipos';

          $config['upload_path'] = $DIR_SUC; 
          //$config['allowed_types'] = 'docx|xlsx|pdf|doc|xls|pptx';
          $config['allowed_types'] = '*';

          //$config['allowed_types'] = 'docx|xlsx|pdf|jpg|jpeg|png|gif';
          $config['max_size'] = 5000;
          $file_names=date('Ymd_His').'_'.rand(0, 500);
          //$config['file_name'] = $file_names.'_'.str_replace(' ', '_', $_FILES['files']['name'][$i]);
          $config['file_name'] = $file_names;
          //$config['file_name'] = $file_names;
   
          $this->load->library('upload',$config); 
    
          if($this->upload->do_upload('file')){
            $uploadData = $this->upload->data();
            $filename = $uploadData['file_name'];
            $filenametype = $uploadData['file_ext'];
   
            $data['totalFiles'][] = $filename;
            //$this->ModeloGeneral->tabla_inserta('equipo_imgs',array('idequipo'=>$idequipo,'imagen'=>$filename,'filetype'=>$filenametype,'usuario'=>$this->idpersonal));
            $this->ModeloCatalogos->Insert('refacciones_imgs',array('idrefaccion'=>$idequipo,'imagen'=>$filename));
          }else{
            $data = array('error' => $this->upload->display_errors());
            log_message('error', json_encode($data)); 
          }
        }
   
      }
      echo json_encode($output);
    }
    function favorito(){
        $params = $this->input->post();
        $id=$params['equipo'];
        $mostrar = $params['status'];
        $this->ModeloCatalogos->updateCatalogo('refacciones',array('destacado'=>$mostrar),array('id'=>$id));
        echo $mostrar;
    }
    function mostrarweb(){
        $params = $this->input->post();
        $id=$params['idequipo'];
        $mostrar = $params['mostrar'];
        $this->ModeloCatalogos->updateCatalogo('refacciones',array('paginaweb'=>$mostrar),array('id'=>$id));
    }
    function viewimages(){
        $params = $this->input->post();
        $id=$params['idequipo'];
        $resultados=$this->ModeloCatalogos->getselectwheren('refacciones_imgs',array('idrefaccion'=>$id,'activo'=>1));
        $html='';
                foreach ($resultados->result() as $item) {
                    $html.='<div class="card_img"><div class="card_img_c"><img class="materialboxed" src="'.base_url().'uploads/equipos/'.$item->imagen.'"></div><div class="card_img_a">
                                <a class="btn-floating red tooltipped" data-position="top" data-delay="50" data-tooltip="Eliminar" onclick="deleteimg('.$item->id.')"><i class="material-icons">delete_forever</i></a></div></div>';
                }
        echo $html;
    }
    function viewcaracte(){
        $params = $this->input->post();
        $id=$params['idequipo'];
        //$resultados=$this->ModeloCatalogos->getselectwheren('equipos_caracteristicas',array('idequipo'=>$id,'activo'=>1));
        
        $strq = "SELECT * FROM refacciones_caracteristicas where idrefaccion=$id and activo=1 ORDER BY orden ASC";
        $query = $this->db->query($strq);

        $html='<table class="table striped"><thead><tr><th>Tipo</th><th>Descripcion</th><th>General</th><th>Orden</th><th></th></tr></thead><tbody>';
        foreach ($query->result() as $item) {
            if($item->general==1){
                $general='SI';
            }else{
                $general='';
            }
            $html.='<tr><td>'.$item->name.'</td><td>'.$item->descripcion.'</td><td>'.$general.'</td><td>'.$item->orden.'</td>
                        <td>
                            <a class="btn-floating green tooltipped edit soloadministradores eq_cact_'.$item->id.'" 
                                data-name="'.$item->name.'"
                                data-descripcion="'.$item->descripcion.'"
                                data-general="'.$item->general.'"
                                data-orden="'.$item->orden.'"
                                onclick="editar_ec('.$item->id.')" data-position="top" data-delay="50" data-tooltip="Editar" data-tooltip-id="f5aed0ec-5db2-aa68-1319-23959ebc0465"><i class="material-icons">mode_edit</i></a>
                            <a class="btn-floating red tooltipped elimina soloadministradores" data-position="top" data-delay="50" data-tooltip="Eliminar" data-tooltip-id="7817ec6e-15c1-ab9a-76f3-cff8bc1c9ab4" onclick="delete_ec('.$item->id.')"><i class="material-icons">delete_forever</i></a>
                        </td>
                    </tr>';
        }
        $html.='</tbody></table>';
        echo $html;
    }
    function deleteimg(){
        $params = $this->input->post();
        $id=$params['idequipo'];
        $this->ModeloCatalogos->updateCatalogo('refacciones_imgs',array('activo'=>0),array('id'=>$id));
    }
    function insertaActualizacaracteristicas(){
        $params=$this->input->post();
        $id = $params['id'];
        unset($params['id']);
        if($id>0){
            $this->ModeloCatalogos->updateCatalogo('refacciones_caracteristicas',$params,array('id'=>$id));
        }else{
            $this->ModeloCatalogos->Insert('refacciones_caracteristicas',$params);
        }
    }
    function deleteec(){
        $params = $this->input->post();
        $id=$params['idequipo'];
        $this->ModeloCatalogos->updateCatalogo('refacciones_caracteristicas',array('activo'=>0),array('id'=>$id));
    }
    function actualizarendimiento(){
        $data = $this->input->post();
        // La variable KEY traerá el ID del equipo a actualizar
        $key = key($data["data"]);

        if(key($data["data"][$key])=='rendimiento') {
            $rendimiento = $data["data"][$key]['rendimiento'];
            $data["data"][$key]['rendimiento']=$rendimiento;

            $idActualizado = $this->model->update_refacciones($data["data"][$key], $key);
        }
        if($idActualizado==$key){
            // Se ibtienene todos los datos del listado
            $preciosActualizados = $this->model->getRefaccionesdatos($idActualizado);
            $preciosActualizadosAux = (array) $preciosActualizados[0];
            
            // Se arma el array
            $dataRetorno = array($preciosActualizadosAux ); 
            
            // Se crea el data y se regresa en JSON
            $datosRetorno = array("data" => $dataRetorno);
            echo json_encode($datosRetorno);
        }
    }
}

?>