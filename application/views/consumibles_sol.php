<!doctype html>
<html lang="es">
  <head>
    <?php 
      $favicons=base_url().'app-assets/images/favicon/favicon_kyocera.png';
    ?>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta content="width=device-width,initial-scale=1.0" name=viewport>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-tap-highlight" content="no">
    <meta name="description" content="Sistema Kyocera">
    <meta name="keywords" content="Sistema Kyocera">
    <meta name="theme-color" content="#e31a2f" />
    <title>Alta productividad</title>
    <link rel="icon" href="<?php echo $favicons; ?>" sizes="32x32">
    <link rel="apple-touch-icon" href="<?php echo $favicons; ?>">
    <link href="<?php echo base_url(); ?>public/plugins/note/bootstrap.min.css" rel="stylesheet">
    <script src="<?php echo base_url(); ?>public/plugins/note/jquery-3.5.1.min.js"></script>
    <script src="<?php echo base_url(); ?>public/plugins/note/bootstrap.min.js"></script>
    <!-- include summernote css/js -->
    <!--<link href="<?php echo base_url(); ?>public/plugins/note/summernote.min.css" rel="stylesheet">
    <script src="<?php echo base_url(); ?>public/plugins/note/summernote.min.js"></script>-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/plugins/toastr/toastr.css">
    <script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/toastr/toastr.min.js" ></script>
    <link href="<?php echo base_url(); ?>app-assets/vendors/sweetalert/dist/sweetalert.css" type="text/css" rel="stylesheet">
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/sweetalert/dist/sweetalert.min.js"></script>
    
    <script type="text/javascript" src="<?php echo base_url(); ?>public/js/consumiblesol.js?v=<?php echo date('YmdGis');?>" ></script>

    <input type="hidden" id="base_url" value="<?php echo base_url(); ?>">
    <style type="text/css">
      body{background: url(https://altaproductividadapr.com/public/img/fondo.webp) #eaeaea;      }
      .colorblue{color:#2e4064;      }
      .container{background: white;border-top: 3px solid #ff0101;max-width: 825px; -webkit-box-shadow: 10px 10px 42px -6px rgba(0,0,0,0.75);-moz-box-shadow: 10px 10px 42px -6px rgba(0,0,0,0.75);box-shadow: 10px 10px 42px -6px rgba(0,0,0,0.75);      }
      .border-b-red{border-bottom: 3px solid #ff0101; }
      .imglogos{width: 100%; max-height: 100px; }
    </style>
  </head>
  <body>
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div style="width: 100%; float: left;">
              <div style="width: 25%;float: left;"><img src="https://altaproductividadapr.com/public/img/cotheader2.webp" class="imglogos" alt="print"></div>
              <div style="width: 50%;float: left; min-height:10px; font-style: italic; font-size: 14px; color:#2e4064;">Impresoras y Multifunciones, Venta, Renta y Mantenimiento. Blvd. Norte 1831 Col. Villas San Alejandro. Puebla, Pue. <br>Call Center 273 3400, 249 5393. www.kyoceraap.com</div>
              <div style="width: 25%;float: left;"><img src="https://altaproductividadapr.com/public/img/alta.webp" class="imglogos" alt="empresa"></div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6">
          
        </div>
        <div class="col-md-6" style="border-bottom: 3px solid #ff0101; font-size:18px">
          <b>Solicitud de consumibles</b>
        </div>
      </div>
      <div class="row colorblue" style="margin-top:15px;">
        <div class="col-md-3">Empresa:</div>
        <div class="col-md-9"><?php echo $empresa;?><!--<?php echo 'con: '.$idcontrato.'/ ren:'.$idRenta;?>--></div>
      </div>
      <div class="row colorblue" >
        <div class="col col-4 col-sm-4 col-md-3">Periodo:</div>
        <div class="col col-8 col-sm-8 col-md-9"><?php echo $periodo;?></div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <?php 
            $cot_color=0;
            $equiposarray=array();
            //$equiposarray_id=0;
            foreach ($query_cont->result() as $itemcr) {
              $equiposerie = $this->Rentas_model->equiposrentas2($itemcr->idRenta);
              foreach ($equiposerie as $iteme) {
                if($iteme->categoriaId==25){//Impresora de color
                  $equiposarray[]=array('id'=>$iteme->idEquipo,'modelo'=>$iteme->modelo);
                }
                if($iteme->categoriaId==27){//Multifuncional de color
                  $equiposarray[]=array('id'=>$iteme->idEquipo,'modelo'=>$iteme->modelo);
                }
                if($iteme->categoriaId==28){//Multifuncional Híbrido de color
                  $equiposarray[]=array('id'=>$iteme->idEquipo,'modelo'=>$iteme->modelo);
                }
                
              }
            }
            sort($equiposarray);

            //$equiposarray=array_unique($equiposarray);
          ?>
          <?php if($tipo==0){ ?>
          <table class="table" id="table_sol">
              <thead>
                  <tr class="border-b-red"><th>Modelo</th><th >Serie</th><th >Ubicación</th><th >Consumibles</th><th >Cantidad</th></tr>
              </thead>
              <tbody>
                <?php
                    
                    foreach ($query_cont->result() as $itemcr) {
                      $mostrarcolor=0; 
                        if($itemcr->tiporenta==2){
                          if($itemcr->rentacolor>0 || $itemcr->precio_c_e_color>0){
                            $mostrarcolor=1;
                            $cot_color=1;
                          }
                        }


                        $equiposerie = $this->Rentas_model->equiposrentas2($itemcr->idRenta);
                        foreach ($equiposerie as $iteme) {

                          if($itemcr->tiporenta==1){
                            if($iteme->rentacostocolor>0 || $iteme->excedentecolor>0){
                              $mostrarcolor=1;
                              $cot_color=1;
                            }
                          }
                          if($cot_color==0){
                            if($iteme->categoriaId==25){//Impresora de color
                              $cot_color=1;
                            }
                            if($iteme->categoriaId==27){//Multifuncional de color
                              $cot_color=1;
                            }
                            if($iteme->categoriaId==28){//Multifuncional Híbrido de color
                              $cot_color=1;
                            }

                          }
                ?>
                  <tr>
                      
                      <td>
                        <input id="idenvio" type="hidden" value="<?php echo $idenvio; ?>">
                        <input id="con" type="hidden" value="<?php echo $itemcr->contratoid; ?>">
                        <input id="row" type="hidden" value="<?php echo $iteme->id; ?>">
                        <input id="modelo" type="hidden" value="<?php echo $iteme->idEquipo; ?>">
                        <input id="tipo" type="hidden" value="<?php echo $tipo; ?>">
                        <textarea id="comen" style="display:none;"></textarea>
                        <?php echo $iteme->modelo;?>
                      </td>
                      <td>
                        <input id="serie" type="hidden" value="<?php echo $iteme->serieId; ?>">
                        <?php echo $iteme->serie;?> </td>
                      <td><?php echo $iteme->ubicacion;?></td>
                      <td>
                        <select class="form-control" id="consumible"><option value="0"></option>
                          <?php 
                            $idEquipo = $iteme->idEquipo;
                            $detallesConsumibles = $this->Consumibles_model->getConsumiblesPorIdEquipo($idEquipo,0);
                            foreach ($detallesConsumibles as $itemc) {
                              if($itemc->tipo==1){
                                echo '<option value="'.$itemc->id.'" >'.$itemc->modelo.'</option>';
                              }else{
                                if($mostrarcolor==1){
                                  echo '<option value="'.$itemc->id.'" >'.$itemc->modelo.'</option>';
                                }
                              }
                            }
                          ?>
                        </select>
                      </td>
                      <td>
                        <select class="form-control" id="cantidadcon">
                          <option value="0"></option>
                          <option value="1">1</option>
                          <option value="2">2</option>
                          <option value="3">3</option>
                          <option value="4">4</option>
                          <option value="5">5</option>
                        </select>
                      </td>
                  </tr>
                <?php } } ?>
              </tbody>
          </table>
          <?php }else{ ?>
            <!---------------------------------------->
              <table class="table" id="table_sol">
                <thead>
                    <tr class="border-b-red"><th>Descripción de lo solicitado</th></tr>
                </thead>
                <tbody>
                    <tr> 
                        <td>
                          <input id="idenvio" type="hidden" value="<?php echo $idenvio; ?>">
                          <input id="con" type="hidden" value="<?php echo $idcontrato; ?>">
                          <input id="row" type="hidden" value="0">
                          <input id="modelo" type="hidden" value="0">
                          <input id="tipo" type="hidden" value="<?php echo $tipo; ?>">
                          <input id="serie" type="hidden" value="0">
                          <select class="form-control" id="consumible" style="display:none;"><option value="346"></option></select>
                          
                          <select class="form-control" id="cantidadcon" style="display:none;"><option value="1">1</option></select>
                          <textarea id="comen" class="form-control"></textarea>
                          
                        </td>
                    </tr>
                </tbody>
            </table>
            <!---------------------------------------->
          <?php } ?>
          
        </div>
      </div>
      <?php if($cot_color==0){ ?>
        <style type="text/css">
          .camposcotizacion{
            display: none;
          }
        </style>
      <?php } ?>
      <div class="row camposcotizacion">
        <div class=" col-md-12">
          <table class="table" id="table_cot">
              <thead>
                  <tr><td colspan="4">¿Te gustaría cotizar algún consumible adicional?</td></tr>
                  <tr class="border-b-red">
                    <th><label>Modelo</label><select class="form-control" id="modeloselect" onchange="funcionConsumibles()"><option value="0">Seleccione</option><?php 
                          $equiposarray_id=0;
                          foreach ($equiposarray as $items) { 
                            if($equiposarray_id!=$items['id']){
                              $equiposarray_id=$items['id'];
                              echo '<option value="'.$items['id'].'">'.$items['modelo'].'</option>';
                            }
                      
                          }?></select></th>
                    <th><label>Consumibles</label><select class="form-control" id="consumibleselect"></select></th>
                    <th><label>Cantidad</label><select class="form-control" id="cantidadselect"><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option></select></th>
                    <th><button type="button" class="btn btn-info" onclick="agregarrow()">Agregar</button></th></tr>
              </thead>
              <tbody class="tbody_cot"></tbody></table>
        </div>
        <div class="col-md-12">
          <label>Agregar comentario</label>
          <textarea class="form-control" id="comentariocot"></textarea>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12" style="text-align:center;"><button type="button" class="btn btn-success" id="solenvio">Enviar solicitud</button></div>
      </div>
      <div class="row">
        <div class="col-md-12" style="padding: 0;"><img src="https://altaproductividadapr.com/public/img/cot_footer.jpg" width="100%"></div>
      </div>
    </div>
  </body>
</html>