<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class Polizas_model extends CI_Model 
{
    public function __construct() 
    {
        parent::__construct();
        $this->DB6 = $this->load->database('other6_db', TRUE);
    }
    
    public function getListadoPolizas()
    {
        $sql = "SELECT
                *
                FROM polizas WHERE estatus = 1";
        $query = $this->DB6->query($sql);
        return $query->result();
    }

    public function insertaPoliza($data) 
    {
        $this->db->insert('polizas', $data);
        return $this->db->insert_id();
    }

    public function insertaPolizaCreada($data) 
    {
        $this->db->insert('polizasCreadas', $data);
        return $this->db->insert_id();
    }

    public function insertaDetallesPoliza($data) 
    {
        $this->db->insert('polizas_detalles', $data);
        $this->db->insert_id();
    }

    public function insertaDetallesPolizaCreada($data) 
    {
        $this->db->insert('polizasCreadas_has_detallesEquipos', $data);
        return $this->db->insert_id();
    }

    public function insertaPolizaDetallesPolizaCreada($data) 
    {
        $this->db->insert('polizasCreadas_has_detallesPoliza', $data);
        return $this->db->insert_id();
    }

    public function getPoliza($idPoliza)
    {
        $sql = "SELECT *
                FROM polizas
                WHERE id=".$idPoliza;
        $query = $this->DB6->query($sql);
        return $query->row();
    }

    public function getDetallesPoliza($idPoliza){
        $sql = "SELECT *
                FROM polizas_detalles
                WHERE idPoliza=$idPoliza and activo=1";
        $query = $this->DB6->query($sql);
        return $query->result();
    }

    public function getDetallesPolizacosto($idPoliza)
    {
        $sql = "SELECT *
                FROM polizas_detalles
                WHERE id=".$idPoliza;
        $query = $this->DB6->query($sql);
        return $query->result();
    }

    public function actualizaPoliza($data, $idPoliza) 
    {
        $this->db->set($data);
        $this->db->where('id', $idPoliza);
        return $this->db->update('polizas');
    }

    public function actualizaDetallesPoliza($data, $idDetallePoliza) 
    {
        $this->db->set($data);
        $this->db->where('id', $idDetallePoliza);
        return $this->db->update('polizas_detalles');
    }

    public function getPolizaConUnDetalle($idPoliza, $idDetallePoliza)
    {
        $sql = "SELECT p.nombre,
                p.cubre,
                p.vigencia_meses,
                p.vigencia_clicks,
                pd.id as 'idDetallePoliza',
                pd.modelo,
                pd.precio_local,pd.precio_semi,pd.precio_foraneo,pd.precio_especial,
                pd.idPoliza FROM polizas as p LEFT JOIN polizas_detalles AS pd ON pd.idPoliza=p.id
                WHERE p.id='$idPoliza' AND pd.id='$idDetallePoliza'";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getListadocontratosPolizasIncompletas($data){
        $cliente=$data['cliente'];
        $status=$data['status'];
        if ($cliente>0) {
            $wherecli=' and pc.idCliente='.$cliente;
        }else{
            $wherecli='';
        }
        if ($status==1) {
            $wherestatatus=' and pc.statuspago=1';
        }elseif ($status==2) {
            $wherestatatus=' and pc.statuspago=0';
        }else{
            $wherestatatus='';
        }

        $sql = "SELECT pc.id,
                pc.idCliente,
                c.empresa,
                pc.atencion,
                pc.correo,
                pc.telefono,
                pc.plazo,
                pc.estatus,
                pc.idCotizacion,
                pc.area,
                pc.prefactura,
                pc.tipo 
                FROM
                polizasCreadas as pc
                INNER JOIN clientes as c ON pc.idCliente=c.id
                where pc.id>0 $wherecli $wherestatatus
                ";
            //pc.estatus=1
        $query = $this->DB6->query($sql);
        return $query->result();
    }
    function getlistadopolizasc($id){
        $wherecli=' and pc.idCliente='.$id;
        $sql = "SELECT pc.id,
                pc.idCliente,
                c.empresa,
                pc.atencion,
                pc.correo,
                pc.telefono,
                pc.plazo,
                pc.estatus,
                pc.idCotizacion,
                pc.area,
                pc.prefactura,
                pc.tipo 
                FROM
                polizasCreadas as pc
                INNER JOIN clientes as c ON pc.idCliente=c.id
                where pc.activo=1 $wherecli
                ";
            //pc.estatus=1
        $query = $this->DB6->query($sql);
        return $query->result();
    }
    function datospolizap($id){
        $wherecli=' and pc.id='.$id;
        $sql = "SELECT pc.id,
                pc.idCliente,
                c.empresa,
                pc.atencion,
                pc.correo,
                pc.telefono,
                pc.plazo,
                pc.estatus,
                pc.idCotizacion,
                pc.area,
                pc.prefactura,
                pc.tipo,
                p.nombre as ejecutivo,
                pc.viewcont,
                pc.viewcont_cant,
                pc.ser_rest
                FROM
                polizasCreadas as pc
                INNER JOIN clientes as c ON pc.idCliente=c.id
                left join personal as p on p.personalId=pc.id_personal
                where pc.id>0 $wherecli
                ";
            //pc.estatus=1
        $query = $this->DB6->query($sql);
        return $query->result();
    }
    function getListadocontratosPolizasIncompletasasi($params){
        $cliente=$params['cliente'];
        $status=$params['status'];
        $viewp=$params['viewp'];
        if(isset($params['rfc'])){
            $rfc=$params['rfc'];
        }else{
            $rfc='';
        }

        $personal=$params['personal'];
        $fec_ini_reg=$params['fec_ini_reg'];
        if(isset($params['fechafinal_reg'])){
            $fechafinal_reg=$params['fechafinal_reg'];
        }else{
            $fechafinal_reg='';
        }
        $emp=$params['emp'];
        $activo=$params['activo'];
        if(isset($params['pre'])){
            $pre=$params['pre'];
        }else{
            $pre='x';    
        }
        $columns = array( 
                0=>'pc.id',
                1=>'c.empresa',
                2=>'pc.prefactura',
                3=>'per.nombre',

                4=>'pc.estatus',
                5=>'pc.reg',
                6=>'pre.vencimiento',
                7=>'pc.idCliente',
                8=>'pc.tipo',
                /*
                9=>"(
                                SELECT GROUP_CONCAT(CASE
        WHEN fac.Estado > 0 THEN
            CASE 
                WHEN fac.Estado = 2 THEN 'ST'
                ELSE ''
            END
        ELSE ''
    END,' ',fac.Folio)
                                FROM `factura_poliza` as pol 
                                INNER JOIN f_facturas as fac on fac.FacturasId=pol.facturaId and fac.Estado=1
                                WHERE pol.polizaId=pc.id
                                    ) as facturas",
                                    */
                9=>'group_concat(fac.Folio) facturas',
                10=>'pc.activo',
                11=>'(SELECT IFNULL(sum(pd.cantidad*pd.precio_local),0)+ IFNULL(sum(pd.cantidad*pd.precio_semi),0)+ IFNULL(sum(pd.cantidad*pd.precio_foraneo),0)+ IFNULL(sum(pd.cantidad*pd.precio_especial),0) FROM `polizasCreadas_has_detallesPoliza` as pd WHERE pd.idPoliza=pc.id) as totalpoliza',
                12=>'pc.tipov',
                13=>'pc.viewcont',
                14=>'(SELECT COUNT(*) FROM polizasCreadas_has_detallesPoliza as pd WHERE pd.idPoliza=pc.id) as total_pd',
                15=>'pc.siniva',
                16=>'pc.requ_fac'
        );
        $columnsss = array( 
                0=>'pc.id',
                1=>'c.empresa',
                2=>'pc.prefactura',
                3=>'per.nombre',

                4=>'pc.estatus',
                5=>'pc.reg',
                6=>'pre.vencimiento',
                7=>'pc.idCliente',
                8=>'pc.tipo',
                9=>'fac.Folio',
                /*
                10=>'(SELECT IFNULL(sum(pd.precio_local),0)+ IFNULL(sum(pd.precio_semi),0)+ IFNULL(sum(pd.precio_foraneo),0)+ IFNULL(sum(pd.precio_especial),0) FROM `polizasCreadas_has_detallesPoliza` as pd WHERE pd.idPoliza=vps.id)'
                */
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('polizasCreadas pc');
        $this->db->join('clientes c', 'pc.idCliente=c.id');
        $this->db->join('prefactura pre', 'pre.ventaId=pc.id and pre.tipo=1','left');
        $this->db->join('personal per', 'per.personalId=pc.id_personal','left');
        $this->db->join('cliente_has_datos_fiscales clif ', 'clif.id=pre.rfc_id','left');

        $this->db->join('factura_poliza pol ', 'pol.polizaId=pc.id','left');
        $this->db->join('f_facturas fac ', 'fac.FacturasId=pol.facturaId and fac.Estado=1','left');

            if ($cliente>0) {
                $this->db->where('pc.idCliente',$cliente);
            }
            if($status==1){
                $this->db->where('pc.statuspago',1);
            }elseif ($status==2) {
                $this->db->where('pc.statuspago',0);
            }
            $this->db->where('pc.combinada',0);
            $this->db->where('pc.activo',$activo);

            if($personal>0){
                $this->db->where('pc.id_personal',$personal);
            }
            if($fec_ini_reg!=''){
                $this->db->where('pc.reg >=',$fec_ini_reg);
            }
            if($fechafinal_reg!=''){
                $this->db->where('pc.reg <=',$fechafinal_reg.' 23:59:59');
            }
            if($emp>0){
                $this->db->where('pc.tipov',$emp);
            }
            if($rfc!=''){
                $this->db->where('clif.rfc',$rfc);
            }
            if($pre!='x'){
                $this->db->where('pc.prefactura',$pre);
            }
            if($viewp==1){
                $this->db->where('pc.viewcont',1);
            }
            $this->db->group_by("pc.id");

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columnsss as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    function getListadocontratosPolizasIncompletasasit($params){
        $cliente=$params['cliente'];
        $status=$params['status'];
        $viewp=$params['viewp'];
        $personal=$params['personal'];
        $fec_ini_reg=$params['fec_ini_reg'];
        if(isset($params['fechafinal_reg'])){
            $fechafinal_reg=$params['fechafinal_reg'];
        }else{
            $fechafinal_reg='';
        }
        $emp=$params['emp'];
        $activo=$params['activo'];
        if(isset($params['rfc'])){
            $rfc=$params['rfc'];
        }else{
            $rfc='';
        }
        if(isset($params['pre'])){
            $pre=$params['pre'];
        }else{
            $pre='x';    
        }
        $columns = array( 
                0=>'pc.id',
                1=>'c.empresa',
                2=>'pc.prefactura',
                3=>'per.nombre',

                4=>'pc.estatus',
                5=>'pc.reg',
                6=>'pre.vencimiento',
                7=>'pc.idCliente',
                8=>'pc.tipo',
        );
        $columnsss = array( 
                0=>'pc.id',
                1=>'c.empresa',
                2=>'pc.prefactura',
                3=>'per.nombre',

                4=>'pc.estatus',
                5=>'pc.reg',
                6=>'pre.vencimiento',
                7=>'pc.idCliente',
                8=>'pc.tipo',
                9=>'fac.Folio',
        );
        
        $this->DB6->select('COUNT(*) as total');
        $this->DB6->from('polizasCreadas pc');
        $this->DB6->join('clientes c', 'pc.idCliente=c.id');
        $this->DB6->join('prefactura pre', 'pre.ventaId=pc.id and pre.tipo=1','left');
        $this->DB6->join('personal per', 'per.personalId=pc.id_personal','left');
        $this->DB6->join('cliente_has_datos_fiscales clif ', 'clif.id=pre.rfc_id','left');

        $this->DB6->join('factura_poliza pol ', 'pol.polizaId=pc.id','left');
        $this->DB6->join('f_facturas fac ', 'fac.FacturasId=pol.facturaId and fac.Estado=1','left');
            
            if ($cliente>0) {
                $this->DB6->where('pc.idCliente',$cliente);
            }
            if($status==1){
                $this->DB6->where('pc.statuspago',1);
            }elseif ($status==2) {
                $this->DB6->where('pc.statuspago',0);
            }
            $this->DB6->where('pc.combinada',0);
            $this->DB6->where('pc.activo',$activo);

            if($personal>0){
                $this->DB6->where('pc.id_personal',$personal);
            }
            if($fec_ini_reg!=''){
                $this->DB6->where('pc.reg >=',$fec_ini_reg);
            }
            if($fechafinal_reg!=''){
                $this->db->where('pc.reg <=',$fechafinal_reg.' 23:59:59');
            }
            if($emp>0){
                $this->DB6->where('pc.tipov',$emp);
            }
            if($rfc!=''){
                    $this->DB6->where('clif.rfc',$rfc);
                }
            if($pre!='x'){
                $this->DB6->where('pc.prefactura',$pre);
            }
            if($viewp==1){
                $this->DB6->where('pc.viewcont',1);
            }
            $this->DB6->group_by("pc.id");

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->DB6->group_start();
            foreach($columnsss as $c){
                $this->DB6->or_like($c,$search);
            }
            $this->DB6->group_end();  
        }            
        //$this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        ///$this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->DB6->get();
        // print_r($query); die;
        return $query->num_rows();
    }
    //===========================================================================================================
    public function getDetallesPoliza2($idPoliza){
        $sql = "SELECT pd.*,eq.modelo as modelo2
                FROM polizas_detalles as pd
                left join equipos as eq on eq.id=pd.modelo
                WHERE pd.idPoliza=$idPoliza and pd.activo=1
                order by eq.modelo DESC
                ";
        $query = $this->db->query($sql);
        return $query->result();
    }
    function verificarfactura($idPoliza){
        $sql = "SELECT *
                    FROM `factura_poliza` as pol 
                    INNER JOIN f_facturas as fac on fac.FacturasId=pol.facturaId and fac.Estado=1
                    WHERE pol.polizaId=$idPoliza";
        $query = $this->db->query($sql);
        return $query;   
    }
    function servicioseventocliente($idcliente,$conidpoliza){
        if($conidpoliza>0){
            $w_pol=" and pesa.idpoliza='$conidpoliza'";
        }else{
            $w_pol='';
        }
        // consulta copiada del los listado de servicio de evento de cliente
        $sql="SELECT `acl`.`asignacionId`, `per`.`personalId`, CONCAT(per.nombre, ' ', `per`.`apellido_paterno`, ' ', per.apellido_materno) as tecnico, `cli`.`id`, `cli`.`empresa`, `acl`.`fecha`, `acl`.`hora`, `acl`.`horafin`, `acl`.`zonaId`, `acl`.`prioridad`, `zo`.`nombre`, `acld`.`status`, `acl`.`tiposervicio`, `pol`.`nombre` as `servicio`, `acl`.`fecha`, `acld`.`horaserinicio`, `acld`.`horaserfin`, 
            GROUP_CONCAT(IFNULL(acld.modelo, ''),' ',`acld`.`serie` SEPARATOR ',<br>') as equiposserie,
            GROUP_CONCAT(acld.serie SEPARATOR ' ') as equiposseries_group, 
            `acld`.`asignacionIdd`, `acld`.`horaserinicioext`, `acld`.`horaserfinext`, `acl`.`editado`, `acld`.`tservicio_a`, `acl`.`hora_comida`, `acl`.`horafin_comida`, `acld`.`activo`, `acld`.`infodelete`, `acld`.`direccion`, `acld`.`prioridad`, `acld`.`notificarerror_estatus`, `acld`.`notificarerror`, `acld`.`asignacionIdd`, `acld`.`nr`, `acld`.`nr_coment`, `acld`.`nr_coment_tipo`, `per2`.`personalId` as `personalId2`, CONCAT(per2.nombre, ' ', `per2`.`apellido_paterno`, ' ', per2.apellido_materno) as tecnico2, CONCAT(per3.nombre, ' ', `per3`.`apellido_paterno`, ' ', per3.apellido_materno) as tecnico3, CONCAT(per4.nombre, ' ', `per4`.`apellido_paterno`, ' ', per4.apellido_materno) as tecnico4, `acl`.`comentario`,pesa.id as pesaid
            FROM `asignacion_ser_cliente_a` `acl`
            JOIN `personal` `per` ON `per`.`personalId`=`acl`.`tecnico`
            LEFT JOIN `personal` `per2` ON `per2`.`personalId`=`acl`.`tecnico2`
            LEFT JOIN `personal` `per3` ON `per3`.`personalId`=`acl`.`tecnico3`
            LEFT JOIN `personal` `per4` ON `per4`.`personalId`=`acl`.`tecnico4`
            JOIN `clientes` `cli` ON `cli`.`id`=`acl`.`clienteId`
            JOIN `rutas` `zo` ON `zo`.`id`=`acl`.`zonaId`
            JOIN `asignacion_ser_cliente_a_d` `acld` ON `acld`.`asignacionId`=`acl`.`asignacionId`
            JOIN `polizas` `pol` ON `pol`.`id`=`acld`.`tpoliza`
            LEFT JOIN `servicio_evento` `sereve` ON `sereve`.`id`=`acld`.`tservicio`
            LEFT JOIN `polizas_detalles` `pold` ON `pold`.`id`=`acld`.`tservicio_a`
            left join polizascreadas_ext_ser_add as pesa on pesa.idservicio= acl.asignacionId and pesa.tipo_ser=3 and pesa.activo=1
            WHERE 
            `acld`.`status` = 2 and
            `cli`.`id` = '$idcliente' and
            `acl`.`activo` = 1 and
            `acld`.`activo` = '1'
            $w_pol
            group by acl.asignacionId
            ORDER BY `acl`.`asignacionId` DESC LIMIT 20";
        $query = $this->db->query($sql);
        return $query;  
    }
    function servicioseventocliente_dll($idcliente,$conidpoliza,$id_row_eq,$idroweq_serie){
        if($conidpoliza>0){
            $w_pol=" and pesa.idpoliza='$conidpoliza'";
        }else{
            $w_pol='';
        }
        // consulta copiada del los listado de servicio de evento de cliente
        $sql="SELECT `acl`.`asignacionId`, `per`.`personalId`, CONCAT(per.nombre, ' ', `per`.`apellido_paterno`, ' ', per.apellido_materno) as tecnico, `cli`.`id`, `cli`.`empresa`, `acl`.`fecha`, `acl`.`hora`, `acl`.`horafin`, `acl`.`zonaId`, `acl`.`prioridad`, `zo`.`nombre`, `acld`.`status`, `acl`.`tiposervicio`, `pol`.`nombre` as `servicio`, `acl`.`fecha`, `acld`.`horaserinicio`, `acld`.`horaserfin`, 
            GROUP_CONCAT(IFNULL(acld.modelo, ''),' ',`acld`.`serie` SEPARATOR ',<br>') as equiposserie,
            GROUP_CONCAT(acld.serie SEPARATOR ' ') as equiposseries_group, 
            `acld`.`asignacionIdd`, `acld`.`horaserinicioext`, `acld`.`horaserfinext`, `acl`.`editado`, `acld`.`tservicio_a`, `acl`.`hora_comida`, `acl`.`horafin_comida`, `acld`.`activo`, `acld`.`infodelete`, `acld`.`direccion`, `acld`.`prioridad`, `acld`.`notificarerror_estatus`, `acld`.`notificarerror`, `acld`.`asignacionIdd`, `acld`.`nr`, `acld`.`nr_coment`, `acld`.`nr_coment_tipo`, `per2`.`personalId` as `personalId2`, CONCAT(per2.nombre, ' ', `per2`.`apellido_paterno`, ' ', per2.apellido_materno) as tecnico2, CONCAT(per3.nombre, ' ', `per3`.`apellido_paterno`, ' ', per3.apellido_materno) as tecnico3, CONCAT(per4.nombre, ' ', `per4`.`apellido_paterno`, ' ', per4.apellido_materno) as tecnico4, `acl`.`comentario`,pesa.id as pesaid
            FROM `asignacion_ser_cliente_a` `acl`
            JOIN `personal` `per` ON `per`.`personalId`=`acl`.`tecnico`
            LEFT JOIN `personal` `per2` ON `per2`.`personalId`=`acl`.`tecnico2`
            LEFT JOIN `personal` `per3` ON `per3`.`personalId`=`acl`.`tecnico3`
            LEFT JOIN `personal` `per4` ON `per4`.`personalId`=`acl`.`tecnico4`
            JOIN `clientes` `cli` ON `cli`.`id`=`acl`.`clienteId`
            JOIN `rutas` `zo` ON `zo`.`id`=`acl`.`zonaId`
            JOIN `asignacion_ser_cliente_a_d` `acld` ON `acld`.`asignacionId`=`acl`.`asignacionId`
            JOIN `polizas` `pol` ON `pol`.`id`=`acld`.`tpoliza`
            LEFT JOIN `servicio_evento` `sereve` ON `sereve`.`id`=`acld`.`tservicio`
            LEFT JOIN `polizas_detalles` `pold` ON `pold`.`id`=`acld`.`tservicio_a`
            left join polizascreadas_ext_ser_add as pesa on pesa.idservicio= acl.asignacionId and pesa.tipo_ser=3 and pesa.activo=1
            WHERE 
            acld.status = 2 and
            cli.id = '$idcliente' and
            acl.activo = 1 and
            acld.activo = '1'
            $w_pol
            and acld.serie='$idroweq_serie'
            group by acl.asignacionId
            ORDER BY `acl`.`asignacionId` DESC LIMIT 20";
        $query = $this->db->query($sql);
        return $query;  
    }
    function serviciospolizacliente($idcliente,$idPoliza,$conidpoliza){
        if($conidpoliza>0){
            $w_pol=" and pesa.idpoliza='$conidpoliza'";
        }else{
            $w_pol='';
        }
        // consulta copiada del los listado de servicio de poliza
        $sql="SELECT `ape`.`asignacionIde`, `per`.`personalId`, CONCAT(per.nombre, ' ', `per`.`apellido_paterno`, ' ', per.apellido_materno) as tecnico, `cli`.`id`, `cli`.`empresa`, `ape`.`fecha`, `ape`.`hora`, `ap`.`zonaId`, `ru`.`nombre`, `equ`.`modelo`, 
            GROUP_CONCAT(equ.modelo,' ',ape.serie SEPARATOR ', ') as equiposserie,
            GROUP_CONCAT(ape.serie SEPARATOR ' ') as equiposseries_group,
            `ape`.`status`, `ap`.`tiposervicio`, `ape`.`horafin`, `ap`.`asignacionId`, `sereve`.`nombre` as `servicio`, `ape`.`t_fecha`, `ape`.`horaserinicio`, `ape`.`horaserfin`, `ape`.`horaserinicio`, `ape`.`horaserfin`, `ape`.`cot_referencia`, `ape`.`horaserinicioext`, `ape`.`horaserfinext`, `ape`.`editado`, `ape`.`hora_comida`, `ape`.`horafin_comida`, `ape`.`t_horainicio`, `ape`.`t_horafin`, `ape`.`activo`, `ape`.`infodelete`, `pocde`.`direccionservicio`, `ap`.`polizaId`, `ape`.`prioridad`, `ape`.`notificarerror_estatus`, `ape`.`notificarerror`, `ape`.`nr`, `ape`.`nr_coment`, `ape`.`nr_coment_tipo`, CONCAT(per2.nombre, ' ', `per2`.`apellido_paterno`, ' ', per2.apellido_materno) as tecnico2, `per2`.`personalId` as `personalId2`, CONCAT(per3.nombre, ' ', `per3`.`apellido_paterno`, ' ', per3.apellido_materno) as tecnico3, `per3`.`personalId` as `personalId3`, CONCAT(per4.nombre, ' ', `per4`.`apellido_paterno`, ' ', per4.apellido_materno) as tecnico4, `per4`.`personalId` as `personalId4`, `ape`.`comentario`,pocde.nombre as serviciopol,pesa.id as pesaid
            FROM `asignacion_ser_poliza_a_e` `ape`
            JOIN `asignacion_ser_poliza_a` `ap` ON `ap`.`asignacionId`=`ape`.`asignacionId`
            LEFT JOIN `personal` `per` ON `per`.`personalId`=`ape`.`tecnico`
            LEFT JOIN `personal` `per2` ON `per2`.`personalId`=`ape`.`tecnico2`
            LEFT JOIN `personal` `per3` ON `per3`.`personalId`=`ape`.`tecnico3`
            LEFT JOIN `personal` `per4` ON `per4`.`personalId`=`ape`.`tecnico4`
            JOIN `polizasCreadas` `poc` ON `poc`.`id`=`ap`.`polizaId`
            JOIN `clientes` `cli` ON `cli`.`id`=`poc`.`idCliente`
            JOIN `rutas` `ru` ON `ru`.`id`=`ap`.`zonaId`
            left JOIN `polizasCreadas_has_detallesPoliza` `pocde` ON `pocde`.`id`=`ape`.`idequipo`
            left JOIN `equipos` `equ` ON `equ`.`id`=`pocde`.`idEquipo`
            LEFT JOIN `servicio_evento` `sereve` ON `sereve`.`id`=`ap`.`tservicio`
            left join polizascreadas_ext_ser_add as pesa on pesa.idservicio= ap.asignacionId and pesa.tipo_ser=2 and pesa.activo=1
            WHERE `ape`.`status` = 2
            AND `cli`.`id` = '$idcliente'
            AND `poc`.`id`!= '$idPoliza'
            AND `ap`.`activo` = 1
            AND `ape`.`activo` = '1'
            $w_pol
            GROUP BY ap.asignacionId
            ORDER BY ap.asignacionId DESC
            LIMIT 30;

            ";
        $query = $this->db->query($sql);
        return $query; 
    }
    function serviciospolizacliente_dll($idcliente,$idPoliza,$conidpoliza,$idroweq_eq,$idroweq_serie){
        if($conidpoliza>0){
            $w_pol=" and pesa.idpoliza='$conidpoliza'";
        }else{
            $w_pol='';
        }
        // consulta copiada del los listado de servicio de poliza
        $sql="SELECT `ape`.`asignacionIde`, `per`.`personalId`, CONCAT(per.nombre, ' ', `per`.`apellido_paterno`, ' ', per.apellido_materno) as tecnico, `cli`.`id`, `cli`.`empresa`, `ape`.`fecha`, `ape`.`hora`, `ap`.`zonaId`, `ru`.`nombre`, `equ`.`modelo`, 
            GROUP_CONCAT(equ.modelo,' ',ape.serie SEPARATOR ', ') as equiposserie,
            GROUP_CONCAT(ape.serie SEPARATOR ' ') as equiposseries_group,
            `ape`.`status`, `ap`.`tiposervicio`, `ape`.`horafin`, `ap`.`asignacionId`, `sereve`.`nombre` as `servicio`, `ape`.`t_fecha`, `ape`.`horaserinicio`, `ape`.`horaserfin`, `ape`.`horaserinicio`, `ape`.`horaserfin`, `ape`.`cot_referencia`, `ape`.`horaserinicioext`, `ape`.`horaserfinext`, `ape`.`editado`, `ape`.`hora_comida`, `ape`.`horafin_comida`, `ape`.`t_horainicio`, `ape`.`t_horafin`, `ape`.`activo`, `ape`.`infodelete`, `pocde`.`direccionservicio`, `ap`.`polizaId`, `ape`.`prioridad`, `ape`.`notificarerror_estatus`, `ape`.`notificarerror`, `ape`.`nr`, `ape`.`nr_coment`, `ape`.`nr_coment_tipo`, CONCAT(per2.nombre, ' ', `per2`.`apellido_paterno`, ' ', per2.apellido_materno) as tecnico2, `per2`.`personalId` as `personalId2`, CONCAT(per3.nombre, ' ', `per3`.`apellido_paterno`, ' ', per3.apellido_materno) as tecnico3, `per3`.`personalId` as `personalId3`, CONCAT(per4.nombre, ' ', `per4`.`apellido_paterno`, ' ', per4.apellido_materno) as tecnico4, `per4`.`personalId` as `personalId4`, `ape`.`comentario`,pocde.nombre as serviciopol,pesa.id as pesaid
            FROM `asignacion_ser_poliza_a_e` `ape`
            JOIN `asignacion_ser_poliza_a` `ap` ON `ap`.`asignacionId`=`ape`.`asignacionId`
            LEFT JOIN `personal` `per` ON `per`.`personalId`=`ape`.`tecnico`
            LEFT JOIN `personal` `per2` ON `per2`.`personalId`=`ape`.`tecnico2`
            LEFT JOIN `personal` `per3` ON `per3`.`personalId`=`ape`.`tecnico3`
            LEFT JOIN `personal` `per4` ON `per4`.`personalId`=`ape`.`tecnico4`
            JOIN `polizasCreadas` `poc` ON `poc`.`id`=`ap`.`polizaId`
            JOIN `clientes` `cli` ON `cli`.`id`=`poc`.`idCliente`
            JOIN `rutas` `ru` ON `ru`.`id`=`ap`.`zonaId`
            left JOIN `polizasCreadas_has_detallesPoliza` `pocde` ON `pocde`.`id`=`ape`.`idequipo`
            left JOIN `equipos` `equ` ON `equ`.`id`=`pocde`.`idEquipo`
            LEFT JOIN `servicio_evento` `sereve` ON `sereve`.`id`=`ap`.`tservicio`
            left join polizascreadas_ext_ser_add as pesa on pesa.idservicio= ap.asignacionId and pesa.tipo_ser=2 and pesa.activo=1
            WHERE `ape`.`status` = 2
            AND `cli`.`id` = '$idcliente'
            AND `poc`.`id`!= '$idPoliza'
            AND `ap`.`activo` = 1
            AND `ape`.`activo` = '1'
            $w_pol
            and pocde.idEquipo='idroweq_eq' and pocde.serie='$idroweq_serie'
            GROUP BY ap.asignacionId
            ORDER BY ap.asignacionId DESC
            LIMIT 30;

            ";
        $query = $this->db->query($sql);
        return $query; 
    }
    public function info_servicios_cliente($cliente,$idPoliza){
        if($idPoliza>0){
            $where_p=" pol.id !='$idPoliza' AND ";
        }else{
            $where_p="";
        }
        $sql = "SELECT * FROM (

                SELECT asig.asignacionId,1 as tiposer, cli.empresa,asigd.fecha,GROUP_CONCAT(ser.serie SEPARATOR ' ') as equiposseries_group 
                FROM asignacion_ser_contrato_a as asig
                INNER JOIN contrato as con on con.idcontrato=asig.contratoId
                INNER JOIN rentas as ren on ren.id=con.idRenta
                INNER JOIN asignacion_ser_contrato_a_e as asigd on asigd.asignacionId=asig.asignacionId
                INNER JOIN series_productos as ser on ser.serieId=asigd.serieId
                INNER JOIN clientes as cli on cli.id=ren.idCliente
                WHERE ren.idCliente='$cliente' AND asig.activo=1 AND asigd.activo=1 AND asigd.status=2 AND asigd.fecha>DATE_SUB(NOW(),INTERVAL '12' MONTH)
                GROUP BY asig.asignacionId
                union

                SELECT asig.asignacionId, 2 as tiposer, cli.empresa, asigd.fecha,GROUP_CONCAT(asigd.serie SEPARATOR ' ') as equiposseries_group 
                FROM asignacion_ser_poliza_a as asig
                INNER JOIN asignacion_ser_poliza_a_e as asigd on asigd.asignacionId=asig.asignacionId
                INNER JOIN polizasCreadas as pol on pol.id=asig.polizaId
                INNER JOIN clientes as cli on cli.id=pol.idCliente
                WHERE $where_p pol.idCliente='$cliente' AND asig.activo=1 AND asigd.activo=1 AND asigd.status=2 AND asigd.fecha>DATE_SUB(NOW(),INTERVAL '12' MONTH)
                GROUP BY asig.asignacionId
                union

                SELECT asig.asignacionId, 3 as  tiposer,cli.empresa, asig.fecha,GROUP_CONCAT(asigd.serie SEPARATOR ' ') as equiposseries_group 
                FROM asignacion_ser_cliente_a as asig
                INNER JOIN asignacion_ser_cliente_a_d as asigd on asigd.asignacionId=asig.asignacionId
                INNER JOIN clientes as cli on cli.id=asig.clienteId
                WHERE asig.clienteId='$cliente' AND asigd.status=2 AND asigd.activo=1 AND asig.activo=1 AND asig.fecha>DATE_SUB(NOW(),INTERVAL '12' MONTH)
                GROUP BY asig.asignacionId
                union

                SELECT asig.asignacionId, 4 as tiposer,cli.empresa,asig.fecha,GROUP_CONCAT(asigd.serie SEPARATOR ' ') as equiposseries_group 
                FROM asignacion_ser_venta_a as asig
                INNER join asignacion_ser_venta_a_d as asigd on asigd.asignacionId=asig.asignacionId
                INNER JOIN ventas as ven on ven.id=asig.ventaId
                INNER JOIN clientes as cli on cli.id=ven.idCliente
                WHERE asig.activo=1 AND asig.status=2 AND asig.tipov=0 AND cli.id='$cliente' AND asig.fecha>DATE_SUB(NOW(),INTERVAL '12' MONTH) 
                GROUP BY asig.asignacionId
                 ) as datos GROUP BY asignacionId,tiposer
                ";
        $query = $this->db->query($sql);
        return $query;
    }
    public function info_servicios_vinculados_servicios($ser,$setipo){
                if($setipo==1){
                    $sql="SELECT asig.asignacionId,1 as tiposer, cli.empresa,asigd.fecha,pes.id_ser,pes.id_tipo,pes.id as pes_id
                FROM asignacion_ser_contrato_a as asig
                INNER JOIN contrato as con on con.idcontrato=asig.contratoId
                INNER JOIN rentas as ren on ren.id=con.idRenta
                INNER JOIN asignacion_ser_contrato_a_e as asigd on asigd.asignacionId=asig.asignacionId
                INNER JOIN clientes as cli on cli.id=ren.idCliente
                INNER join polizascreadas_ext_servisios as pes on pes.id_ser_garantia=asig.asignacionId and pes.id_tipo_garantia=1
                WHERE asig.activo=1 AND asigd.activo=1 AND asigd.status=2 AND pes.id_ser='$ser' and pes.activo=1 ";
                }
                if($setipo==2){
                    $sql="SELECT asig.asignacionId, 2 as tiposer, cli.empresa, asigd.fecha,pes.id_ser,pes.id_tipo,pes.id as pes_id
                FROM asignacion_ser_poliza_a as asig
                INNER JOIN asignacion_ser_poliza_a_e as asigd on asigd.asignacionId=asig.asignacionId
                INNER JOIN polizasCreadas as pol on pol.id=asig.polizaId
                INNER JOIN clientes as cli on cli.id=pol.idCliente
                INNER join polizascreadas_ext_servisios as pes on pes.id_ser_garantia=asig.asignacionId and pes.id_tipo_garantia=2
                WHERE asig.activo=1 AND asigd.activo=1 AND asigd.status=2 AND pes.id_ser='$ser' and pes.activo=1";
                }
                if($setipo==3){
                    $sql="SELECT asig.asignacionId, 3 as  tiposer,cli.empresa, asig.fecha,pes.id_ser,pes.id_tipo,pes.id as pes_id
                FROM asignacion_ser_cliente_a as asig
                INNER JOIN asignacion_ser_cliente_a_d as asigd on asigd.asignacionId=asig.asignacionId
                INNER JOIN clientes as cli on cli.id=asig.clienteId
                INNER join polizascreadas_ext_servisios as pes on pes.id_ser_garantia=asig.asignacionId and pes.id_tipo_garantia=3
                WHERE asigd.status=2 AND asigd.activo=1 AND asig.activo=1 AND pes.id_ser='$ser' and pes.activo=1";
                }
                if($setipo==4){
                    $sql="SELECT asig.asignacionId, 4 as tiposer,cli.empresa,asig.fecha,pes.id_ser,pes.id_tipo,pes.id as pes_id
                FROM asignacion_ser_venta_a as asig
                INNER JOIN ventas as ven on ven.id=asig.ventaId
                INNER JOIN clientes as cli on cli.id=ven.idCliente
                INNER join polizascreadas_ext_servisios as pes on pes.id_ser_garantia=asig.asignacionId and pes.id_tipo_garantia=4
                WHERE asig.activo=1 AND asig.status=2 AND asig.tipov=0  AND pes.id_ser='$ser' and pes.activo=1";
                }
        
        
        $query = $this->db->query($sql);
        return $query;
    }
}

?>