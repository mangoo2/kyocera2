<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class Modelochat extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    /*
        SELECT ch.id,ch.reg,IF(ch.tipo=0,ch.nombre,cli.empresa) as clientename, per.nombre,ch.departamento,per.apellido_paterno,per.apellido_materno,ch.status,ch.nombre,ch.correo,ch.telefono,ch.motivo
        FROM chat_ini as ch
        LEFT JOIN clientes as cli on cli.id=ch.idcliente
        LEFT JOIN personal as per on per.personalId=ch.idpersonal_inicio
        WHERE ch.activo=1;
    */
    function get_listado_chat($params){
        //log_message('error', json_encode($params));
        $personal=$params['personal'];
        $cliente=$params['cliente'];
        
        $columns = array( 
            0=>'ch.id',
            1=>'ch.reg',
            2=>'IF(ch.tipo=0,ch.nombre,cli.empresa) as clientename',
            3=>'per.nombre as personal',
            4=>'ch.departamento',
            5=>'ch.status',
            6=>'per.apellido_paterno',
            7=>'per.apellido_materno',
            8=>'ch.nombre',
            9=>'ch.correo',
            10=>'ch.telefono',
            11=>'ch.motivo',
            12=>'ch.tipo'
        );
        $columnsss = array( 
            0=>'ch.id',
            1=>'ch.reg',
            2=>'IF(ch.tipo=0,ch.nombre,cli.empresa)',
            3=>'per.nombre',
            4=>'ch.departamento',
            5=>'ch.status',
            6=>'per.apellido_paterno',
            7=>'per.apellido_materno',
            8=>'ch.nombre',
            9=>'ch.correo',
            10=>'ch.telefono',
            11=>'ch.motivo'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('chat_ini ch');

        $this->db->join('clientes cli', 'cli.id=ch.idcliente','left');
        $this->db->join('personal per', 'per.personalId = ch.idpersonal_inicio','left');
        if($personal>0){
            $this->db->where(array('ch.idpersonal_inicio'=>$personal));
        }
        if($cliente>0){
            $this->db->where(array('ch.idcliente'=>$cliente));
        }
        
        $this->db->where(array('ch.activo'=>1));
        //$this->db->where(array('facturaabierta'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columnsss as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    public function get_listado_chat_t($params){
        $personal=$params['personal'];
        $cliente=$params['cliente'];
        $columns = array( 
            0=>'ch.id',
            1=>'ch.reg',
            2=>'IF(ch.tipo=0,ch.nombre,cli.empresa)',
            3=>'per.nombre',
            4=>'ch.departamento',
            5=>'per.apellido_paterno',
            6=>'per.apellido_materno',
            7=>'ch.status',
            8=>'ch.nombre',
            9=>'ch.correo',
            10=>'ch.telefono',
            11=>'ch.motivo'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(*) as total');
        $this->db->from('chat_ini ch');

        $this->db->join('clientes cli', 'cli.id=ch.idcliente','left');
        $this->db->join('personal per', 'per.personalId = ch.idpersonal_inicio','left');

        if($personal>0){
            $this->db->where(array('ch.idpersonal_inicio'=>$personal));
        }
        if($cliente>0){
            $this->db->where(array('ch.idcliente'=>$cliente));
        }
        $this->db->where(array('ch.activo'=>1));
        //$where = ;
        //$this->db->where(array('facturaabierta'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        //$this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        //$this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query->row()->total;
    }
    

}