var base_url = $('#base_url').val();
var tablef;
var tablep;
var perfilid = $('#perfilid').val();
var idfaccomp;
var tipofaccomp;
var fechahoy = $('#fechahoy').val();
$(document).ready(function() {	
	tablef = $('#tabla_facturacion').DataTable();
    tablep = $('#tablependientesf').DataTable();
	loadtable();
    load_pni(0);
    select2_idcliente();
    
    idcliente_rfc();
    
    $.switcher('#tipoview');
    /*
    new DG.OnOffSwitch({
        el: '#tipoview',
        textOn: 'Usuario',
        textOff: 'Todas',
        trackColorOn:'#F57C00',
        trackColorOff:'#df3039',
        listener:function(name, checked){
            setTimeout(function(){ 
                if($('#tipoview').is(':checked')){
                    var tipoview=$('#tipoview').val();
                }else{
                    var tipoview=0;
                }
                $('#personals').val(tipoview).change();
            }, 500);
            
        }
    });
    */
    $('.filtrosdatatable').change(function(event) {
        tablef.state.clear();
    });
    $("#fileacuse").fileinput({
            showCaption: false,
            showUpload: true,// quita el boton de upload
            dropZoneEnabled: false,//minimiza el preview y solo se despliea al cargar un archivo
            //rtl: true,
            allowedFileExtensions: ["jpg","png","webp","jpeg","pdf"],
            browseLabel: 'Seleccionar documento',
            uploadUrl: base_url+'Facturaslis/fileacuse',
            maxFilePreviewSize: 5000,
            previewFileIconSettings: {
                'docx': '<i class="fa fa-file-word-o text-primary"></i>',
                'xlsx': '<i class="fa fa-file-excel-o text-success"></i>',
                'pptx': '<i class="fa fa-file-powerpoint-o text-danger"></i>',
                'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
                'pdf': '<i class="fa fa-file-pdf-o text-danger"></i>',
                'zip': '<i class="fa fa-file-archive-o text-muted"></i>',
                'cer': '<i class="fas fa-file-invoice"></i>',
            },
            uploadExtraData: function (previewId, index) {
            var info = {
                        idfaccomp:idfaccomp,
                        tipofaccomp:tipofaccomp
                    };
            return info;
          }
        }).on('filebatchuploadcomplete', function(event, files, extra) {
          //location.reload();
            toastr.success('Se cargo Correctamente','Hecho!');
              if(tipofaccomp==0){
                loadtable();
              }
              if(tipofaccomp==1){
                loadtable_c();
              }
        }).on('filebatchuploadsuccess', function(event, files, extra) {
          //location.reload();
          toastr.success('Se cargo Correctamente','Hecho!');
          if(tipofaccomp==0){
            loadtable();
          }
          if(tipofaccomp==1){
            loadtable_c();
          }
        });
});
function loadtableswitch(){
    setTimeout(function(){ 
        if($('#tipoview').is(':checked')){
            var tipoview=$('#tipoview').val();
        }else{
            var tipoview=0;
        }
        $('#personals').val(tipoview).change();
    }, 500);
}
function loadtable(){
    var personals=$('#personals option:selected').val();
    var idcliente=$('#idcliente option:selected').val()==undefined?0:$('#idcliente option:selected').val();
    var idclienterfc=$('#idcliente_rfc option:selected').val()==undefined?0:$('#idcliente_rfc option:selected').val();
    var metodopago=$('#rs_metodopago option:selected').val()==undefined?0:$('#rs_metodopago option:selected').val();
    var tcfdi=$('#rs_tcfdi option:selected').val()==undefined?0:$('#rs_tcfdi option:selected').val();
    var pensav=$('#rs_pendsave option:selected').val();
    var inicio=$('#finicial').val();
    var fin=$('#ffinal').val();
    var rs_folios=$('#rs_folios option:selected').val();
	tablef.destroy();
	tablef = $('#tabla_facturacion').DataTable({
        pagingType: 'input',
        dom: 'Blfrtip',
        buttons: [
            {
                extend: 'excelHtml5',
                text: 'Excel',
                action: function (e, dt, button, config) {
                    exportacionloadtable('excel', dt);
                }
            },
            {
                extend: 'pdfHtml5',
                text: 'PDF',
                action: function (e, dt, button, config) {
                    exportacionloadtable('pdf', dt);
                }
            }
        ],
        search: {
                return: true
            }, 
        //stateSave: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url+"index.php/Facturaslis/getlistfacturas",
            type: "post",
            "data": {
                'personal':personals,
                'cliente':idcliente,
                'finicio':inicio,
                'ffin':fin,
                'idclienterfc':idclienterfc,
                'rs_folios':rs_folios,
                'metodopago':metodopago,
                'tcfdi':tcfdi,
                'pensav':pensav
            },
        },
        "columns": [
            {"data": "FacturasId",
            	render:function(data,type,row){
            		var html='';
            		if(row.Estado==1){
                        html='<input type="checkbox" class="filled-in facturas_checked" id="factura_'+row.FacturasId+'" value="'+row.FacturasId+'" data-clienteid="'+row.Clientes_ClientesId+'">\
				              <label for="factura_'+row.FacturasId+'"></label>';
                    }else{
                    	html='<input type="checkbox" class="filled-in facturas_checked" id="factura_'+row.FacturasId+'" value="'+row.FacturasId+'" disabled>\
				              <label for="factura_'+row.FacturasId+'"></label>';
                    }
                    return html;
            	}
        	},
            {"data": "Folio",
                render:function(data,type,row){
                    var html ='';
                        var serie=row.cadenaoriginal;
                        serie=serie.split('|');
                        if (serie[3]==undefined) {
                            var seriev='';
                        }else{
                            var seriev=serie[3];
                        }
                        html=seriev+''+row.Folio.padStart(4, 0);
                        html=row.Folio;
                    return html;
                }
            },
            {"data": null,
                render:function(data,type,row){
                    var html='';
                    if(row.Rfc=='XAXX010101000'){ 
                        html=row.Nombre+'<br>'+row.empresa;
                    }else{
                        html=row.Nombre;
                    }
                    return html;
                }
            },
            {"data": "Rfc",},
            {"data": "total",
                render:function(data,type,row){
                    var html='';
                        
                    html=new Intl.NumberFormat('es-MX',{style: "currency", currency: "MXN"}).format(row.total);
                    return html;
                }
            },
            {"data": "Estado",
                render:function(data,type,row){
                    var html='';
                    if(row.Estado==0){
                        html='Cancelada';
                        html+='<div class="validarcancelacion '+row.uuid+'" data-uuid="'+row.uuid+'" style="color:red"></div>';
                    }else if(row.Estado==1){
                        html='Facturado';
                        if(row.fechatimbre=='0000-00-00 00:00:00'){
                            html='Sin Facturar';
                        }
                    }else if(row.Estado==2){
                        html='Sin Facturar';
                    }else{
                    	html='';
                    }
                    return html;
                }
            },
            {"data": "fechatimbre"},
            {"data": "FormaPago"},
            {"data": null,
                render:function(data,type,row){
                    var html='';
                        html=row.nombre+' '+row.apellido_paterno+' '+row.apellido_materno;
                    return html;
                }
            },
            {"data": null,
                render:function(data,type,row){
                    var html='Factura';
                    
                    if(row.f_relacion==1 && row.f_r_tipo=='01'){
                        html='Factura 01 Nota Credito';
                    }
                    if(row.f_relacion==1 && row.f_r_tipo=='04'){
                        html='Factura 04 Sustitucion';
                    }
                    if(row.f_relacion==0 && row.f_r_tipo=='07'){
                        html='Factura de Anticipo recibido';
                    }
                    if(row.f_relacion==1 && row.f_r_tipo=='07' && row.TipoComprobante=='I'){
                        html='Factura con relacion de Anticipo';
                    }
                    if(row.f_relacion==1 && row.f_r_tipo=='07' && row.TipoComprobante=='E'){
                        html='Factura Egreso de Anticipo';
                    }

                        
                    return html;
                }
            },
            {"data": null,
                 render:function(data,type,row){
                    $('.tooltipped').tooltip();
                    var html='<div class="btns-factura">';
                    var estadofactura=row.Estado;
                    if(estadofactura==1){
                        if(row.fechatimbre=='0000-00-00 00:00:00'){
                            estadofactura=2;
                        } 
                    }
                        var complementoclass='';
                        var classes_pue='';
                    //if(row.FormaPago=='PUE'){
                        //var classes_pue='consultar_fac_pue consultar_fac_pue_'+row.FacturasId+'';
                    //}else{
                        //var classes_pue='';
                    //}
                        if(estadofactura==1){
                            if(row.FormaPago=='PPD'){
                                var complementoclass=' complementops_files_'+row.FacturasId+' ';
                            }
                            var classes_pue='consultar_fac_pue consultar_fac_pue_'+row.FacturasId+'';
                        }
                        var btn_pdf ='<a class="b-btn b-btn-primary tooltipped '+classes_pue+' '+complementoclass+'" \
                                href="'+base_url+'Configuracionrentas/generarfacturas/'+row.FacturasId+'/1" \
                                target="_blank"\
                                data-facturasid="'+row.FacturasId+'" data-motofactura="'+row.total+'"\
                                data-position="top" data-delay="50" data-tooltip="Factura"\
                              ><i class="fas fa-file-alt"></i></a> ';
                        var btn_xml ='<a class="b-btn b-btn-primary tooltipped" \
                                    href="'+base_url+row.rutaXml+'" \
                                    target="_blank"\
                                    data-position="top" data-delay="50" data-tooltip="XML"\
                                  download ><i class="far fa-file-code"></i> </a> ';
                        var btn_retimbrar='<button type="button"\
                          class="b-btn b-btn-primary tooltipped btn-retimbrar" \
                          onclick="retimbrar('+row.FacturasId+',0)"\
                          data-position="top" data-delay="50" data-tooltip="Retimbrar" id="refacturar">\
                          <span class="fa-stack">\
                          <i class="fas fa-file fa-stack-2x"></i>\
                          <i class="fas fa-sync fa-stack-1x" style="color: red;"></i>\
                          </span>\
                        </button> ';
                        var btn_edit='<a type="button"\
                            href="'+base_url+'Facturaslis/add/'+row.FacturasId+'"\
                          class="b-btn b-btn-primary tooltipped btn-retimbrar" \
                          data-position="top" data-delay="50" data-tooltip="Editar"><i class="fas fa-pencil-alt"></i></a> ';

                    if(estadofactura==0){ // cancelado
                        html+=btn_pdf;
		                html+=btn_xml;
                        html+='<a class="b-btn b-btn-primary tooltipped" \
                                    href="'+base_url+'kyocera/facturas/'+row.rutaAcuseCancelacion+'" \
                                    target="_blank"\
                                    data-position="top" data-delay="50" data-tooltip="Acuse de cancelación"\
                                  download ><i class="fas fa-file-invoice" style="color: #ef7e4f;"></i> </a> ';
                        html+='<a class="b-btn b-btn-primary tooltipped insertaracuse_'+row.FacturasId+'_0 " \
                                    onclick="insertaracuse('+row.FacturasId+',0)" \
                                    data-docacuse="'+row.doc_acuse_cancelacion+'" \
                                    data-position="top" data-delay="50" data-tooltip="subir Acuse de cancelación"\
                                  download ><i class="fas fa-file-pdf" style="color: #ef7e4f;"></i> </a> ';
                        if(perfilid==1){/*
                            html+='<a\
                                    class="b-btn b-btn-primary tooltipped" \
                                    onclick="regresarfacturaavigente('+row.FacturasId+')"  \
                                    target="_blank"\
                                    data-position="top" data-delay="50" title="Regresar a vigente" data-tooltip="Regresar a vigente"\
                                   ><i class="material-icons dp48" style="color: #ef7e4f;">replay</i>\
                                    </a> ';
                            */
                        }
                        //html+='<div class="no_tienerelacion no_tienerelacion_'+row.FacturasId+'"></div>';
                    }else if(estadofactura==1){

                        html+=btn_pdf;
		                html+=btn_xml;
                                if(row.FormaPago=='PPD'){
                                    html+='<a class="b-btn b-btn-primary tooltipped complementops_files complementops_files_'+row.FacturasId+'" \
                                            onclick="complementop('+row.FacturasId+')"\
                                            data-position="top" data-delay="50" data-tooltip="Complemento de pago" data-complementofac="'+row.FacturasId+'" \
                                          download ><i class="fas fa-receipt " ></i></a> ';
                                }
                            if(row.totalnc>0){
                                html+='<a class="b-btn b-btn-success tooltipped notascreditos_'+row.FacturasId+'" \
                                            onclick="notascreditos('+row.FacturasId+')"\
                                            data-position="top" data-delay="50" data-tooltip="Notas credito" data-uuid="'+row.uuid+'" \
                                          download ><i class="fas fa-object-group " ></i></a> ';
                            }
                        
                        html+='<a class="no_tienerelacion no_tienerelacion_'+row.FacturasId+'"></a>';
                    }else if(estadofactura==2){
                        html+='<!--program '+row.program+' '+row.program_date+' = '+fechahoy+'-->';
                        if(row.program==1 && row.program_proc==0){
                            if(row.program_date==fechahoy){
                                btn_retimbrar='';
                                btn_edit='';
                            }
                        }
                        html+=btn_retimbrar;
                        html+=btn_edit;
                    }else{
                    	html+='';
                    }
                    if(estadofactura==1){
                        html+=' <a href="'+base_url+'Facturaslis/add/0?idfac='+row.FacturasId+'&uuid='+row.uuid+'&tr=04" class="b-btn b-btn-primary"  title="Refacturar para cancelacion"><i class="fas fa-file-prescription"></i></a>';
                        //============================
                        html_nota=' <a href="'+base_url+'Facturaslis/add/0?idfac='+row.FacturasId+'&uuid='+row.uuid+'&tr=01" class="b-btn b-btn-primary"  title="Nota Credito"><i class="fas fa-file-invoice-dollar"></i></a>';
                        if(row.f_relacion==0 && row.f_r_tipo=='07'){
                            //html='Factura de Anticipo recibido';
                            html_nota=' <a href="'+base_url+'Facturaslis/add/0?idfac='+row.FacturasId+'&uuid='+row.uuid+'&tr=07&tc=I" class="b-btn b-btn-primary"  title="Relación al anticipo">RA</a>';
                        }
                        if(row.f_relacion==1 && row.f_r_tipo=='07' && row.TipoComprobante=='I'){
                            //html='Factura con relacion de Anticipo';
                            html_nota=' <a href="'+base_url+'Facturaslis/add/0?idfac='+row.FacturasId+'&uuid='+row.uuid+'&tr=07&tc=E" class="b-btn b-btn-primary"  title="Relación al anticipo Egreso">EA</a>';
                        }
                        if(row.f_relacion==1 && row.f_r_tipo=='07' && row.TipoComprobante=='E'){
                            //html='Factura Egreso de Anticipo';
                            html_nota='';
                        }
                        //=========================================
                        html+=html_nota;
                    }
                    html+=' <a class="b-btn b-btn-primary tooltipped" data-position="top" data-delay="50" data-tooltip="Cambiar personal" onclick="cambiar_per('+row.FacturasId+',4)"><i class="fas fa-user fa-fw"></i></a>';
                    if(row.Estado==1){
                        if(row.envio_m_p>0){
                            var envmail='enviado tooltipped';
                            var envmailt=' title="'+row.per_envio+' '+row.envio_m_d+'" data-position="top" data-delay="50" data-tooltip="'+row.per_envio+' '+row.envio_m_d+'"';
                        }else{
                            var envmail='';
                            var envmailt='';
                        }
                       html+=' <button type="button" class="b-btn b-btn-primary email '+envmail+'" '+envmailt+' onclick="enviarmailfac('+row.FacturasId+','+row.Clientes_ClientesId+')"><i class="fas fa-mail-bulk"></i></button>'; 
                    }
                    if(row.Folio==0){
                        html+=' <a class="waves-effect red btn-bmz" onclick="eliminarfac('+row.FacturasId+')"><i class="fas fa-trash"></i></a>';
                    }
                    if(perfilid==1){
                            html+='<a class="b-btn b-btn-primary tooltipped" onclick="cambiostatusfactura('+row.FacturasId+','+row.Estado+')" data-position="top" data-delay="50" title="Regresar a vigente" data-tooltip="Cambio de estatus"><i class="fas fa-exchange-alt"></i></a> ';
                    }
                    
                    html+='</div>';
                    return html;
                }
            }
        ],
        "order": [[ 1, "desc" ]],
        "lengthMenu": [[20,25, 50], [20,25, 50]],
        //"lengthMenu": [[25, 50, 100,200,400], [25, 50,100,200,400]],
        //"searching": false
        columnDefs: [ { orderable: false, targets: [8,9] }],
        
    }).on('draw',function(){
        $('.dataTables_filter').append('<i class="fa fa-search buttonsearch" onclick="searchtable_vf()"></i>');
        
        verificarpagos();
        verificarcancelacion();
        verificarcomplementos();
        setTimeout(function(){ 
            verificarpagos();
        }, 1000);
    });
}
function select2_idcliente(){
    $('#idcliente').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una opción',
        allowClear: true,
        ajax: {
            url: base_url+'Ventasi/searchclientes',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    var html='';
                    if(element.empresa==element.razon_social){
                        html=element.empresa;
                    }else{
                        html=element.empresa+' ('+element.razon_social+')';
                    }
                    itemscli.push({
                        id: element.id,
                        text: html

                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function (e) {
        var data = e.params.data;
        //obtenerrfc(data.id)
        //loadtable();
    });
}
function cambiostatusfactura(id,est){
        var checkedfac1='';
        var checkedfac0='';
        if(est==1){
            checkedfac1=' checked ';
        }
        if(est==0){
            checkedfac0=' checked ';
        }
    var html='';
        html+='¿Confirma el cambio de estatus de la factura?<br>Se necesita permisos de administrador<br><input type="text" placeholder="Contraseña" id="contrasenaa" name="contrasenaa" class="name form-control"/>';
        html+='<div class="row"><div class="col s6">\
                <input name="tasign" type="radio" class="tasign" id="asign1" value="1" '+checkedfac1+'>\
                <label for="asign1">Timbrada</label>\
                <input name="tasign" type="radio" class="tasign" id="asign2" value="0" '+checkedfac0+'>\
                <label for="asign2">Cancelada</label>\
              </div></div>';
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: html,
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                //var pass=$('#contrasena').val();
                var pass=$("input[name=contrasenaa]").val();
                var estatus=$('input:radio[name=tasign]:checked').val()
                if (pass!='') {
                    $.ajax({
                        type:'POST',
                        url: base_url+"Generales/cambiostatusfactura",
                        data: {
                            idf:id,
                            pass:pass,
                            estatus:estatus
                        },
                        success: function (data){
                            if(parseInt(data)==1){
                                toastr["success"]("Cambio Realizado");  
                                loadtable();  
                            }else{
                                toastr["error"]("No se tiene permiso"); 
                            }
                        }
                    });
                }else{
                    notificacion(0);
                }
            },
            cancelar: function () 
            {
                
            }
        }
    });
    setTimeout(function(){
            new MaskedPassword(document.getElementById("contrasenaa"), '\u25CF');
    },900);
}
function searchtable_vf(){
        var searchv =$("#tabla_facturacion_filter input[type=search]").val();
        tablef.search(searchv).draw();
}
function retimbrar(idfactura,contrato){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de generar el retimbrado?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Configuracionrentas/retimbrar",
                    data: {
                        factura:idfactura
                    },
                    success:function(response){  
                        console.log(response);
                        var array = $.parseJSON(response);
                        if (array.resultado=='error') {
                            swal({    title: "Error "+array.CodigoRespuesta+"!",
                                          text: array.MensajeError+' '+array.info.TimbrarCFDIResult.MensajeErrorDetallado,
                                          type: "warning",
                                          showCancelButton: false
                             });
                        }else{
                            swal("Éxito!", "Se ha creado la factura", "success");
                            loadtable();
                        }

                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}
function retimbrar2(idfactura){//esta se genera internamente
    
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Configuracionrentas/retimbrar",
        data: {
            factura:idfactura
        },
        success:function(response){  
            console.log(response);
            var array = $.parseJSON(response);
            if (array.resultado=='error') {
                /*
                swal({    title: "Error "+array.CodigoRespuesta+"!",
                              text: array.MensajeError+' '+array.info.TimbrarCFDIResult.MensajeErrorDetallado,
                              type: "warning",
                              showCancelButton: false
                 });
                 */
                notificardeerror(idfactura);
            }else{
                //swal("Éxito!", "Se ha creado la factura", "success");
                loadtable();
            }

        }
    });
            
}
function notificardeerror(idfactura){
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Mailenvio/envio_fac_error",
        data: {
            idfac:idfactura
        },
        success:function(data){ 
            console.log(data);
        }
    });
}
function retimbrarcomplemento(idcomplemento){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de generar el retimbrado?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Configuracionrentas/retimbrarcomplemento",
                    data: {
                        complemento:idcomplemento
                    },
                    success:function(response){  
                        console.log(response);
                        var array = $.parseJSON(response);
                        if (array.resultado=='error') {
                            swal({    title: "Error "+array.CodigoRespuesta+"!",
                                          text: array.MensajeError,
                                          type: "warning",
                                          showCancelButton: false
                             });
                        }else{
                            swal("Éxito!", "Se ha creado la factura", "success");
                            loadtable();
                        }

                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}
function cancelarfacturas(){
    var facturaslis = $("#tabla_facturacion tbody > tr");
    var num_facturas_selected=0;
    facturaslis.each(function(){  
        if ($(this).find("input[class*='facturas_checked']").is(':checked')) {
            num_facturas_selected++;
            
        }       
        
    });
    if(num_facturas_selected==1){
        var html='<div class="row">\
                    <div class="col s12 m12">\
                        ¿Está seguro de realizar la Cancelacion de la factura?<br>Se necesita permisos de administrador\
                    </div>\
                    <div class="col s12 m12">\
                        <input type="password" placeholder="Contraseña" id="contrasena" class="name form-control" required/>\
                    </div>\
                  </div>\
                  <div class="row">\
                    <div class="col s6 m6">\
                        <label>Motivo Cancelación</label>\
                        <select id="motivocancelacion" class="form-control-bmz" onchange="mocance()">\
                            <option value="00" disabled selected> Seleccione una opción</option>\
                            <option value="01" >01 Comprobante emitido con errores con relación</option>\
                            <option value="02" >02 Comprobante emitido con errores sin relación</option>\
                            <option value="03" >03 No se llevó a cabo la operación</option>\
                            <option value="04" >04 Operación nominativa relacionada en una factura global</option>\
                        </select>\
                    </div>\
                    <div class="col s6 m6 class_foliorelacionado" style="display:none">\
                        <label>Folio Relacionado</label>\
                        <input type="text" id="foliorelacionado" class="form-control-bmz" placeholder="111AAA1A-1AA1-1A11-11A1-11A1AA111A11">\
                    </div>\
                  </div>';
    	$.confirm({
            boxWidth: '55%',
            useBootstrap: false,
            icon: 'fa fa-warning',
            title: 'Atención!',
            content: html,
            type: 'red',
            typeAnimated: true,
            buttons:{
                confirmar: function (){
                    $('body').loading({theme: 'dark',message: 'Cancelando Factura...'});
                    var pass=$('#contrasena').val();
                    var motivo = $('#motivocancelacion option:selected').val();
                    var uuidrelacionado=$('#foliorelacionado').val();
                    if (pass!='') {
                         $.ajax({
                            type:'POST',
                            url: base_url+"index.php/Sistema/solicitarpermiso",
                            data: {
                                pass:pass
                            },
                            success: function (response){
                                    var respuesta = parseInt(response);
                                    if (respuesta==1) {
                	//===================================================
                    	
                    	var DATAa  = [];
                    	
        		        facturaslis.each(function(){  
        		        	if ($(this).find("input[class*='facturas_checked']").is(':checked')) {
        		        		item = {};                    
        		            	item ["FacturasIds"]  = $(this).find("input[class*='facturas_checked']").val();
                                
        		            	DATAa.push(item);
        		        	}       
        		            
        		        });
        		        INFOa  = new FormData();
        		        aInfoa   = JSON.stringify(DATAa);
        		        //console.log(aInfoa);
        		        if (num_facturas_selected==1) {
                            $.ajax({
                                type:'POST',
                                url: base_url+"index.php/Configuracionrentas/cancelarCfdi",
                                data: {
                                    facturas:aInfoa,
                                    motivo:motivo,
                                    uuidrelacionado:uuidrelacionado
                                },
                                success:function(response){  
                                    console.log(response);
                                    var array = $.parseJSON(response);
                                    if (array.resultado=='error') {
                                        swal({    title: "Error "+array.CodigoRespuesta+"!",
                                                      text: array.MensajeError,
                                                      type: "warning",
                                                      showCancelButton: false
                                         });
                                        $('body').loading('stop');
                                    }else{
                                        swal("Éxito!", "Se ha Cancelado la factura", "success");
                                        loadtable();
                                        $('body').loading('stop');
                                    }

                                }
                            });
        		        }else{
        		        	alertfunction('Atención!','Seleccione solo una factura');
                            $('body').loading('stop');
        		        }
                    //================================================
                                    }else{
                                        notificacion(2); 
                                        $('body').loading('stop');
                                    }
                            },
                            error: function(response){
                                notificacion(1);
                                $('body').loading('stop');
                                 
                            }
                        });
                        
                    }else{
                        notificacion(0);
                        $('body').loading('stop');
                    }
                },
                salir: function () 
                {
                    
                }
            }
        });
    }else{
        alertfunction('Atención!','Seleccione una factura');
    }
}
function mocance(){
    var motivo=$('#motivocancelacion option:selected').val();
    if(motivo=='01'){
        $('.class_foliorelacionado').show('show');
    }else{
        $('.class_foliorelacionado').hide('show');
        $('#foliorelacionado').val('');
    }
}
function cancelarcomplemento(idcomplemento){
    var html='<div class="row">\
                <div class="col s12 m12">\
                    ¿Está seguro de realizar la Cancelación de un complemento?<br>Se necesita permisos de administrador\
                </div>\
                <div class="col s12 m12">\
                    <input type="password" placeholder="Contraseña" id="contrasena" class="name form-control" required/>\
                </div>\
              </div>\
              <div class="row">\
                <div class="col s6 m6">\
                    <label>Motivo Cancelación</label>\
                    <select id="motivocancelacion" class="form-control-bmz" onchange="mocance()">\
                        <option value="00" disabled selected> Seleccione una opcion</option>\
                        <option value="01" >01 Comprobante emitido con errores con relación</option>\
                        <option value="02" >02 Comprobante emitido con errores sin relación</option>\
                        <option value="03" >03 No se llevó a cabo la operación</option>\
                        <option value="04" >04 Operación nominativa relacionada en una factura global</option>\
                    </select>\
                </div>\
                <div class="col s6 m6 class_foliorelacionado" style="display:none">\
                    <label>Folio Relacionado</label>\
                    <input type="text" id="foliorelacionado" class="form-control-bmz" placeholder="111AAA1A-1AA1-1A11-11A1-11A1AA111A11">\
                </div>\
              </div>';
    $.confirm({
        boxWidth: '55%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: html,
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $('body').loading({theme: 'dark',message: 'Cancelando Complemento...'});
                var pass=$('#contrasena').val();
                var motivo = $('#motivocancelacion option:selected').val();
                var uuidrelacionado=$('#foliorelacionado').val();
                if (pass!='') {
                     $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Sistema/solicitarpermiso",
                        data: {
                            pass:pass
                        },
                        success: function (response){
                                var respuesta = parseInt(response);
                                if (respuesta==1) {
                //===================================================
                    
                    //console.log(aInfoa);
                        $.ajax({
                            type:'POST',
                            url: base_url+"index.php/Configuracionrentas/cancelarCfdicomplemento",
                            data: {
                                facturas:idcomplemento,
                                motivo:motivo,
                                uuidrelacionado:uuidrelacionado
                            },
                            success:function(response){  
                                console.log(response);
                                var array = $.parseJSON(response);
                                if (array.resultado=='error') {
                                    swal({    title: "Error "+array.CodigoRespuesta+"!",
                                                  text: array.MensajeError,
                                                  type: "warning",
                                                  showCancelButton: false
                                     });
                                    $('body').loading('stop');
                                }else{
                                    swal("Éxito!", "Se ha Cancelado el complemento", "success");
                                    loadtable_c();
                                    $('body').loading('stop');
                                }

                            },
                            error: function(response){
                                $.alert({
                                        boxWidth: '30%',
                                        useBootstrap: false,
                                        title: 'Error!',
                                        content: 'falla de comunicacion, intentar mas tarde'});   
                                  $('body').loading('stop');
                            }
                        });
                    
                //================================================
                                }else{
                                    notificacion(2);
                                     $('body').loading('stop');
                                }
                        },
                        error: function(response){
                            notificacion(1);
                                     $('body').loading('stop'); 
                             
                        }
                    });
                    
                }else{
                    notificacion(0);
                     $('body').loading('stop');
                }
            },
            salir: function () 
            {
                
            }
        }
    });
}
function alertfunction(titlea,contenta){
    $.alert({boxWidth: '30%',useBootstrap: false,title: titlea,theme: 'bootstrap',content: contenta});  
}
var facturacomplemtoid;
function complementop(facturacomplemto){
    facturacomplemtoid=facturacomplemto;
    $('#modalcomplementos').modal();
    $('#modalcomplementos').modal('open');
    $('.listadocomplementos').html('');
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Facturaslis/listasdecomplementos",
        data: {
            facturas:facturacomplemto
        },
        success:function(response){  
           $('.listadocomplementos').html(response);
           $('#tableliscomplementos').DataTable();
        }
    });
}
function addcomplemento(){
    window.location.href = base_url+'Facturaslis/complemento/'+facturacomplemtoid;
}
function loadtablep(){
    var ptipo =$('#ptipo option:selected').val();
    if (ptipo==1) {
        //Venta
        loadtablepv();
    }else if (ptipo==2) {
        //Venta combinada
        loadtablepvc();
    }else if (ptipo==3) {
        //Poliza
        loadtablepp();
    }else if (ptipo==4) {
        //Servicio
        loadtableps();
    }else if (ptipo==5) {
        //Renta
        loadtablepr();
    }else{
        tablep.destroy();
        tablep = $('#tablependientesf').DataTable();
    }

}
function loadtablepv(){
    var personals=$('#personals option:selected').val();
    tablep.destroy();
    tablep = $('#tablependientesf').DataTable({
        //stateSave: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url+"index.php/Facturaslis/getlistpendientesv",
            type: "post",
            "data": {
                'personal':personals
            },
        },
        "columns": [
            {"data": "id"},
            {"data": "empresa"},
            {"data": "vendedor"},
            {"data": "reg"},            
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[25, 50, 100], [25, 50,100]],
        "columnDefs":[{
            "orderable": false,
            "searchable": false,
            "targets": 2
        }]
        
    });
}
function loadtablepvc(){
    var personals=$('#personals option:selected').val();
    tablep.destroy();
    tablep = $('#tablependientesf').DataTable({
        //stateSave: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url+"index.php/Facturaslis/getlistpendientesvc",
            type: "post",
            "data": {
                'personal':personals
            },
        },
        "columns": [
            {"data": "combinadaId"},
            {"data": "empresa"},
            {"data": "vendedor"},
            {"data": "reg",},            
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[25, 50, 100], [25, 50,100]],
        "columnDefs":[{
            "orderable": false,
            "searchable": false,
            "targets": 2
        }]
        
    });
}
function loadtablepp(){
    var personals=$('#personals option:selected').val();
    tablep.destroy();
    tablep = $('#tablependientesf').DataTable({
        //stateSave: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url+"index.php/Facturaslis/getlistpendientesp",
            type: "post",
            "data": {
                'personal':personals
            },
        },
        "columns": [
            {"data": "id"},
            {"data": "empresa"},
            {"data": "vendedor"},
            {"data": "reg",},            
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[25, 50, 100], [25, 50,100]],
        "columnDefs":[{
            "orderable": false,
            "searchable": false,
            "targets": 2
        }]
        
    });
}
function loadtableps(){
    var personals=$('#personals option:selected').val();
    tablep.destroy();
    tablep = $('#tablependientesf').DataTable({
        //stateSave: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url+"index.php/Facturaslis/getlistpendientess",
            type: "post",
            "data": {
                'personal':personals
            },
        },
        "columns": [
            {"data": "id"},
            {"data": "cliente"},
            {"data": "vendedor"},
            {"data": "reg",},            
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[25, 50, 100], [25, 50,100]],
        "columnDefs":[{
            "orderable": false,
            "searchable": false,
            "targets": 2
        }]
        
    });
}
function loadtablepr(){
    var personals=$('#personals option:selected').val();
    tablep.destroy();
    tablep = $('#tablependientesf').DataTable({
        //stateSave: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url+"index.php/Facturaslis/getlistpendientesr",
            type: "post",
            "data": {
                'personals':personals
            },
        },
        "columns": [
            {"data": "prefId"},
            {"data": null,
                render:function(data,type,row){
                    return row.arrendataria+' <br>Contrato: '+row.idcontrato+' periodo inicial:'+row.periodo_inicial+' periodo final:'+row.periodo_final ;
                }
            },
            {"data": null,
                render:function(data,type,row){
                    return row.nombre+' '+row.apellido_paterno+''+row.apellido_materno ;
                }
            },
            {"data": null,
                render:function(data,type,row){
                    return '';
                }
            },            
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[25, 50, 100], [25, 50,100]],
        "columnDefs":[{
            "orderable": false,
            "searchable": false,
            "targets": 2
        }]
        
    });
}
function verificarcomplementos() {
    
    var DATAf  = [];
    $(".complementops_files").each(function() {
        item = {};
        item ["idfactura"] = $(this).data('complementofac');
        DATAf.push(item);
    });
    aInfo   = JSON.stringify(DATAf);
    //console.log(aInfo);
    var datos=aInfo;
    
    $.ajax({
        type:'POST',
        url: base_url+"Generales/viewcomplementos",
        data: {
            datos:aInfo
        },
        success: function (data){
            //console.log(data);
            var array = $.parseJSON(data);
            //console.log(array);
            $.each(array, function(index, item) {
                //console.log(item);
                if(item.complemento>0){
                    if(parseFloat(item.totalfac)<=parseFloat(item.imppagado)){
                        $(".complementops_files_"+item.factura ).removeClass( "b-btn-primary" ).addClass( "b-btn-success comp-success" );
                    }else{
                        $(".complementops_files_"+item.factura ).removeClass( "b-btn-primary" ).addClass( "b-btn-warning comp-success" );
                    }
                    
                }
            });
        }
    });
}
function regresarfacturaavigente(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Confirma de regresar a vigente?<br>Se necesita permisos de administrador<br><input type="password" placeholder="Contraseña" id="contrasena" class="name form-control"/>',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var pass=$('#contrasena').val();
                if (pass!='') {
                    $.ajax({
                        type:'POST',
                        url: base_url+"Generales/regresarfacturaavigente",
                        data: {
                            idf:id,
                            pass:pass
                        },
                        success: function (data){
                            if(parseInt(data)==1){
                                toastr["success"]("Cambio Realizado");  
                                loadtable();  
                            }else{
                                toastr["error"]("No se tiene permiso"); 
                            }
                        }
                    });
                }else{
                    notificacion(0);
                }
            },
            cancelar: function () 
            {
                
            }
        }
    });
}
function verificarpagos(){
    var DATAf  = [];
    $(".consultar_fac_pue").each(function() {
        item = {};
        item ["idfactura"] = $(this).data('facturasid');
        item ["montofactura"] = $(this).data('motofactura');
        DATAf.push(item);
    });
    aInfo   = JSON.stringify(DATAf);
    //console.log(aInfo);
    var datos=aInfo;
    
    $.ajax({
        type:'POST',
        url: base_url+"Generales/verificarpagosafactura",
        data: {
            datos:aInfo
        },
        success: function (data){
            //console.log(data);
            var array = $.parseJSON(data);
            console.log(array);
            $.each(array, function(index, item) {
                if(item.prefactura==0){
                    var btn_html='<a class="b-btn b-btn-primary tooltipped" title="Agregar Pago" onclick="addpagos('+item.factura+')"><i class="fas fa-dollar-sign"></i></a>';
                    $('.no_tienerelacion_'+item.factura).html(btn_html);
                    if(item.pagado==3){
                      $(".consultar_fac_pue_"+item.factura ).removeClass( "b-btn-primary" ).addClass( "b-btn-success" );  
                    }
                }else{
                    if(item.pagado<3){
                        $(".consultar_fac_pue_"+item.factura ).removeClass( "b-btn-primary comp-success b-btn-warning" ).addClass( "b-btn-danger" );
                    }else{
                        $(".consultar_fac_pue_"+item.factura ).removeClass( "b-btn-primary" ).addClass( "b-btn-success" );
                    }
                }
                
            });
        }
    });
}
function addpagos(idfactura){
    $('#idservicio').val(idfactura);
    $('#fecha_p_s').val('');
    $('#pago_p_s').val('');
    $('#observacion_p').val('');

    var montofactura=$('.consultar_fac_pue_'+idfactura).data('motofactura');
    $('.monto_prefactura_s').html('$ <span class="totalpolizas">'+montofactura+'</span>');
    $('#modal_pago_s').modal({dismissible: true});
    $('#modal_pago_s').modal('open');

    table_tipo_servicio(idfactura)
    setTimeout(function(){ 
        calcularrestantes();
    }, 1000);
}
function table_tipo_servicio(idfactura){
  $('.text_tabla_pago').html('');
    $.ajax({
      type: "POST",
      url: base_url+"index.php/Facturaslis/table_get_pagos_servicios",
      data:{id:idfactura},
      success: function(data){
        $('.text_tabla_pago_servicios').html(data);
      }
    });  
}
function calcularrestantes(){
    var totalpoliza=parseFloat($('.totalpolizas').html());
    var pagos = 0;
    $(".montoagregados").each(function() {
        var vstotal = parseFloat($(this).html());
        pagos += Number(vstotal);
    });
    var restante = parseFloat(totalpoliza)-parseFloat(pagos);
    if(restante<=0){
        restante=0;
        var idfactura=$('#idservicio').val();
        actualizaestatuspagado(idfactura);
    }
    $('.restante_prefactura_s').html('$ <span class="restantepoliza">'+restante.toFixed(2)+'</span>');
}
function guardar_pago_servicios(){
    var form_pago = $('#form_pago_servicio');
    form_pago.validate({
            rules: 
            { 
                fecha: {
                    required: true
                },
                idmetodo: {
                    required: true
                },
                pago: {
                    required: true
                }
            },
            // Para mensajes personalizados
            messages: 
            {   
                fecha:{
                    required: "Campo obligatirio"
                },
                idmetodo:{
                    required: "Campo obligatirio"
                },
                pago:{
                    required: "Campo obligatirio"
                }
            },
        errorElement : 'div'
    });
    var valid = $("#form_pago_servicio").valid();
    if(valid){
        var restante =parseFloat($('.restantepoliza').html());
        var pago_p =parseFloat($('#pago_p_s').val());
        if (pago_p>0 && pago_p<=restante) {
            var datos = $('#form_pago_servicio').serialize();
            $.ajax({
              type: "POST",
              url: base_url+"index.php/Facturaslis/guardar_pago_factura",
              data:datos,
              success: function(data){
                 $.alert({
                    boxWidth: '30%',
                    useBootstrap: false,
                    title: 'Atención!',
                    content: 'Guardado correctamente'}
                );
                $('#fecha_p').val('');
                $('#pago_p').val('');
                $('#observacion_p').val('');
                table_tipo_servicio($('#idservicio').val());
                setTimeout(function(){ 
                    calcularrestantes();
                }, 1000);
              }
            });
        }else{
            $.alert({
                    boxWidth: '30%',
                    useBootstrap: false,
                    title: 'Atención!',
                    content: 'Monto mayor a 0 y menor o igual al restante'}
                );
        }
    }   
}
function actualizaestatuspagado(idfactura){
    $.ajax({
          type: "POST",
          url: base_url+"index.php/Facturaslis/actualizaestatuspagado",
          data:{
            factura:idfactura
          },
          success: function(data){
             
          }
        });
}
function deletepagoser(idpago){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: 'Se necesita permisos de administrador<br>'+
                 '<input type="password" placeholder="Contraseña" id="contrasena" class="name form-control" required/>',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var pass=$('#contrasena').val();
                if (pass!='') {
                    $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Sistema/solicitarpermiso",
                        data: {
                            pass:pass
                        },
                        success: function (response){
                                var respuesta = parseInt(response);
                                if (respuesta==1) {
                                    //===============================================================
                                        $.ajax({
                                            type:'POST',
                                            url: base_url+'index.php/Facturaslis/deletepagofac',
                                            data: {
                                                idpago:idpago
                                            },
                                            success:function(data){
                                                    
                                                    
                                                    swal({ title: "Éxito",
                                                        text: "Se Elimino correctamente",
                                                        type: 'success',
                                                        showCancelButton: false,
                                                        allowOutsideClick: false,
                                                    });
                                                    table_tipo_servicio($('#idservicio').val());
                                                    setTimeout(function(){ 
                                                        calcularrestantes();
                                                    }, 1000);
                                                    
                                                
                                                
                                            }
                                        });
                                    //===============================================================
                                 }else{
                                    notificacion(2);
                                }
                        },
                        error: function(response){
                            notificacion(1);
                             
                        }
                    });

                }else{
                    notificacion(0);
                }
                
            },
            cancelar: function () 
            {
                
            }
        }
    });
}
function obtenerrfc(id){
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Facturaslis/obtenerrfc2",
        data: {
            id:id
        },
        success:function(response){
            var array = $.parseJSON(response);
            $('#idcliente_rfc').html('<option value="0">Todos</option>');
            $.each(array, function(index, item) {
                $('#idcliente_rfc').append('<option value="'+item.rfc+'">'+item.rfc+'</option>');
            });
            $('#idcliente_rfc').select2();
            //idcliente_rfc();
        }
    });
}
function idcliente_rfc(){
   $('#idcliente_rfc').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una opción',
        allowClear: true,
        ajax: {
            url: base_url+'index.php/Facturaslis/obtenerrfc3',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.rfc,
                        text: element.rfc+' ('+element.razon_social+')'

                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function (e) {
        //var data = e.params.data;
        //obtenerrfc(data.id)
        //loadtable();
    }); 
}
function verificarcancelacion() {
    
    var DATAvc  = [];
    $(".validarcancelacion").each(function() {
        item = {};
        //item ["idfactura"] = $(this).data('uuid');
        item ["uuid"] = $(this).data('uuid');
        DATAvc.push(item);
    });
    console.log(DATAvc);
    aInfovc   = JSON.stringify(DATAvc);
    console.log(aInfovc);
    console.log('Cancelados:'+aInfovc.length);
    var datos=aInfovc;
    
    $.ajax({
        type:'POST',
        url: base_url+"Configuracionrentas/estatuscancelacionCfdi_all",
        data: {
            datos:aInfovc
        },
        success: function (data){
            console.log(data);
            var array = $.parseJSON(data);
            console.log(array);
            $.each(array, function(index, item) {
                
                    $("."+item.uuid ).html(item.resultado);
                
            });
        }
    });
}
function notascreditos(idf) {
    var uuid=$('.notascreditos_'+idf).data('uuid');
    
    $.ajax({
        type:'POST',
        url: base_url+"Configuracionrentas_ext/verificarnotascredito",
        data: {
            uuid:uuid
        },
        success: function (data){
            $('#modalnotascredito').modal();
            $('#modalnotascredito').modal('open');
            $('.listadonotascredito').html(data);
            $('#table_notacredito').DataTable();
        }
    });
}
function cambiar_per(id,tipo){
    var personal=$('#personals').html();
    $.confirm({
        boxWidth: '40%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Comentario',
        content: '<div style="width: 99%;">¿Desea cambiar al personal?<br>Se necesita permisos de administrador<br><input type="password" id="pass_cambio" class="form-control-bmz"><label>Ejecutivo</label><select id="ejecutivoselect_new" class="browser-default form-control-bmz">'+personal+'</select></div>',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var eje=$('#ejecutivoselect_new').val();
                var pass=$('#pass_cambio').val();
                $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Generales/cambiopersonal",
                    data: {
                        id:id,
                        tipo:tipo,
                        eje:eje,
                        pass:pass
                    },
                    success: function (response){
                        if(parseInt(response)==1){
                            toastr["success"]("Cambio Realizado");  
                            loadtable();  
                        }else{
                            toastr["error"]("No se tiene permiso"); 
                        }
                        
                    },
                    error: function(response){
                        toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema"); 
                         
                    }
                }); 
            },
            cancelar: function (){
                
            }
        }
    });
    setTimeout(function(){ 
        $('#ejecutivoselect_new option[value=0]').remove();
    }, 1000);
    
}
function insertaracuse(id,tipo){
    idfaccomp=id;
    tipofaccomp=tipo;
    $('#modaldocacuse').modal();
    $('#modaldocacuse').modal('open');
    var doc=$('.insertaracuse_'+id+'_'+tipo).data('docacuse');
    $('.docfileacuse').html('');
    if(doc!=null && doc!='null'){
        $('.docfileacuse').html('<iframe src="'+base_url+'kyocera/acuses/'+doc+'" ></iframe>');
    }
}
//===============================================================================
var codigocarga = $('#codigocarga').val();
function enviarmailfac(idfac,cliente){
    //var cliente = $('#idCliente').val();
    obtenercontacto(cliente);
    $('.input_envio_s_c').html('');
    $('.table_mcr_files_s_c').html('');
    $('.addbtnservicio_c').html('');
    $('.input_envio_s').html('<input type="file" id="files" name="files[]" multiple accept="*">');
    $('.table_mcr_files_s').html('<table class="table bordered striped" id="table_mcr_files"><tbody class="tabbodycr_files"></tbody></table>');
    $('.addbtnservicio').html('<button type="button" class="waves-effect btn-bmz  red btn-envioser" onclick="enviarmailfac_mail('+idfac+')">Enviar</button>');
    $('.iframefac').html('<iframe src="'+base_url+'Configuracionrentas/generarfacturas/'+idfac+'/1?savefac=1">');
    inputfileadd();

    $('#modal_enviomail').modal();
    $('#modal_enviomail').modal('open');
}
function obtenercontacto(cliente){
    $('.m_s_contac_c').html('');
    $('.m_s_contac').html('');
    $('.m_s_contac_c_all').html('');
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Calendario/obtenercontactos2",
        data: {
            cliente:cliente
        },
        success: function (response){
            $('.m_s_contac').html('<select class="browser-default form-control-bmz" id="ms_contacto" multiple style="min-height: 72px;" title="Selector multiple, seleccione las opciones con Ctrl+ click">'+response+'</select>');
                
        },
        error: function(response){
            notificacion(1); 
        }
    });
    //=====================================================================
    
}
function inputfileadd(){
    $("#files").fileinput({
        showCaption: true,
        dropZoneEnabled: false,
        showUpload: true,// quita el boton de upload
        //rtl: true,
        //allowedFileExtensions: ["jpg","png","webp","jpeg"],
        browseLabel: 'Seleccionar documentos',
        uploadUrl: base_url+'Calendario/file_multiple',
        maxFilePreviewSize: 5000,
        previewFileIconSettings: {
            'docx': '<i class="fa fa-file-word-o text-primary"></i>',
            'xlsx': '<i class="fa fa-file-excel-o text-success"></i>',
            'pptx': '<i class="fa fa-file-powerpoint-o text-danger"></i>',
            'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
            'pdf': '<i class="fa fa-file-pdf-o text-danger"></i>',
            'zip': '<i class="fa fa-file-archive-o text-muted"></i>',
            'cer': '<i class="fas fa-file-invoice"></i>',
        },
        uploadExtraData: function (previewId, index) {
        var info = {
                    code:codigocarga
                };
        return info;
      }
    }).on('filebatchuploadcomplete', function(event, files, extra) {
      //location.reload();
        upload_data_view(codigocarga);
        $('#files').fileinput('clear');
    }).on('filebatchuploadsuccess', function(event, files, extra) {
      //location.reload();
      //toastr.success('Se cargo el Certificado Correctamente','Hecho!');
        upload_data_view(codigocarga);
        $('#files').fileinput('clear');
    });
}
var idrowcc=0;
function upload_data_view(codigo){
    $.ajax({
            type:'POST',
            url: base_url+"index.php/Calendario/upload_data_view",
            data: {
                code : codigo
            },
            success: function (response){
                console.log(response);
                var array = $.parseJSON(response);
                array.forEach(function(r){
                    var html ='<tr class="idrowcc_'+idrowcc+'">';
                        html+='<td><input type="hidden" id="ec_file" value="'+r.id+'">'+r.file+'</td>';
                        html+='<td><a class="b-btn b-btn-danger" onclick="delete_mcr('+idrowcc+')"><i class="fas fa-trash-alt"></i></a></td>';
                        html+='</tr>';
                        $('.tabbodycr_files').append(html);
                        idrowcc++;

                });
                
            },
            error: function(response){
                toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema");
                
            }
        });
}
function delete_mcr(id){
    $('.idrowcc_'+id).remove();
}
function enviarmailfac_mail(idfac){
    var email =$('#ms_contacto').val();
    $.ajax({
        type:'POST',
        url: base_url+'index.php/Mailenvio/envio_fac',
        data: {
            idfac:idfac,
            code:codigocarga,
            email:JSON.stringify(email),
            asunto:$('#ms_asucto2').val(),
            comen:$('#ms_coment2').val(),
            per:$('#firma_m_f option:selected').val(),
            perbbc:$('#bbc_m_f option:selected').val()
        },
        success:function(data){
            console.log(data);
            var array = $.parseJSON(data);
            codigocarga=array.newcodigo;
            $('#codigocarga').val(codigocarga);
            toastr["success"]("Correo enviado");
            $('#modal_enviomail').modal('close');
            $('#ms_coment2').val('');
        }
    });
}
//==============================================================================
function enviarmailfacall(){
    $('.iframefac_c_all').html('');
    var cliente=0;
    var facturaslis = $("#tabla_facturacion tbody > tr");
    var DATAa  = [];
    var num_facturas_selected=0;
    facturaslis.each(function(){  
        if ($(this).find("input[class*='facturas_checked']").is(':checked')) {
            num_facturas_selected++;
            item = {};                    
            var FacturasIds = $(this).find("input[class*='facturas_checked']").val();
            item ["FacturasIds"]  = FacturasIds;
            
            cliente = $(this).find("input[class*='facturas_checked']").data('clienteid');
            
            $('.iframefac_c_all').append('<iframe src="'+base_url+'Configuracionrentas/generarfacturas/'+FacturasIds+'/1?savefac=1">');

            DATAa.push(item);
        }       
        
    });
    INFOa  = new FormData();
    aInfoa   = JSON.stringify(DATAa);

    if (num_facturas_selected>0) {
        //var cliente = $('#idCliente').val();
        obtenercontacto2(cliente);
        $('.input_envio_s_c').html('');
        $('.table_mcr_files_s_c').html('');
        $('.addbtnservicio_c').html('');

        $('.input_envio_s').html('');
        $('.table_mcr_files_s').html('');
        $('.addbtnservicio').html('');
        $('.iframefac').html('');

        $('.input_envio_s_all').html('<input type="file" id="files" name="files[]" multiple accept="*">');
        $('.table_mcr_files_s_all').html('<table class="table bordered striped" id="table_mcr_files"><tbody class="tabbodycr_files"></tbody></table>');
        $('.addbtnservicio_all').html('<button type="button" class="waves-effect btn-bmz  red btn-envioser" onclick="enviarmailfac_mail_all()">Enviar</button>');
        //$('.iframefac_c_all').html('<iframe src="'+base_url+'Configuracionrentas/generarfacturas/'+idfac+'/1?savefac=1">');
        inputfileadd();

        $('#modal_enviomail_all').modal();
        $('#modal_enviomail_all').modal('open');
    }else{
        toastr.error('Seleccione por lo menos una factura');
    }
}
function enviarmailfac_mail_all(){
    var facturaslis = $("#tabla_facturacion tbody > tr");
    var DATAa  = [];
    var num_facturas_selected=0;
    facturaslis.each(function(){  
        if ($(this).find("input[class*='facturas_checked']").is(':checked')) {
            num_facturas_selected++;
            item = {};                    
            var FacturasIds = $(this).find("input[class*='facturas_checked']").val();
            item ["FacturasIds"]  = FacturasIds;
            

            DATAa.push(item);
        }       
        
    });
    INFOa  = new FormData();
    aInfoa   = JSON.stringify(DATAa);
    var email =$('#ms_contacto').val();
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Mailenvio/envio_fac_all",
        data: {
            idfac:aInfoa,
            code:codigocarga,
            email:JSON.stringify(email),
            asunto:$('#ms_asucto2_c_all').val(),
            comen:$('#ms_coment2_c_all').val(),
            per:$('#firma_m_c_all option:selected').val(),
            perbbc:$('#bbc_m_c_all option:selected').val()
        },
        success:function(data){  
            console.log(data);
            var array = $.parseJSON(data);
            codigocarga=array.newcodigo;
            $('#codigocarga').val(codigocarga);
            toastr["success"]("Correo enviado");
            $('#modal_enviomail_all').modal('close');
            $('#ms_coment2_c_all').val('');

        }
    });
}
function obtenercontacto2(cliente){
    $('.m_s_contac_c').html('');
    $('.m_s_contac').html('');
    $('.m_s_contac_c_all').html('');
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Calendario/obtenercontactos2",
        data: {
            cliente:cliente
        },
        success: function (response){
            $('.m_s_contac_c_all').html('<select class="browser-default form-control-bmz" id="ms_contacto" multiple style="min-height: 72px;" title="Selector multiple, seleccione las opciones con Ctrl+ click">'+response+'</select>');
                
        },
        error: function(response){
            notificacion(1);
        }
    });
    //=====================================================================
    
}
function eliminarfac(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Eliminar',
        content: '¿Desea eliminar la factura guardada?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                
                     $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Facturaslis/eliminarfac",
                        data: {
                            id:id
                        },
                        success: function (response){
                            toastr["success"]("Factura eliminada");
                            loadtable();
                            loadtable_save();
                        },
                        error: function(response){
                            toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema");
                        }
                    });
                    
                
            },
            cancelar: function (){
                
            }
        }
    });
}
function msearchpre(){
    $('#modalsearchpre').modal();
    $('#modalsearchpre').modal('open');
}
function searchpre(){
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Facturaslis/buscartimbre",
        data: {
            pre:$('#searchpre').val()
        },
        success: function (response){
            $('.table_result_search').html(response);
            $('#table_search_pre').DataTable();
        },
        error: function(response){
            toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema"); 
             
        }
    }); 
}
function searchprelimpiar(){
    $('#searchpre').val('');
    $('.table_result_search').html('');
}
function viewpreir(row){
    var fecha =$('.viewpreir_'+row).data('fecha');
    $('#finicial').val(fecha);
    $('#ffinal').val(fecha);

    var tipopre =$('.viewpreir_'+row).data('tipopre');
    $('#rs_folios').val(tipopre);

    var idcliente =$('.viewpreir_'+row).data('idcliente');
    var empresa =$('.viewpreir_'+row).data('empresa');

    $('#idcliente').html('<option value="'+idcliente+'">'+empresa+'</option>');

    select2_idcliente();
    
    var folio =$('.viewpreir_'+row).data('folio');

    
    
    loadtable();
    setTimeout(function(){ 
        tablef.search(folio).draw();
    }, 1000);
}
function exportacionloadtable(tipo,dat){
    console.log(dat.ajax.params());

    var personals=$('#personals option:selected').val();
    var idcliente=$('#idcliente option:selected').val()==undefined?0:$('#idcliente option:selected').val();
    var idclienterfc=$('#idcliente_rfc option:selected').val()==undefined?0:$('#idcliente_rfc option:selected').val();
    var metodopago=$('#rs_metodopago option:selected').val()==undefined?0:$('#rs_metodopago option:selected').val();
    var tcfdi=$('#rs_tcfdi option:selected').val()==undefined?0:$('#rs_tcfdi option:selected').val();
    var pensav=$('#rs_pendsave option:selected').val();
    var inicio=$('#finicial').val();
    var fin=$('#ffinal').val();
    var rs_folios=$('#rs_folios option:selected').val();


    var url=base_url+'index.php/Facturaslis/exportarlist';
        url+='?tipo='+tipo+'&personal='+personals+'&cliente='+idcliente+'&finicio='+inicio+'&ffin='+fin+'&idclienterfc='+idclienterfc+'&rs_folios='+rs_folios+'&metodopago='+metodopago+'&tcfdi='+tcfdi+'&pensav='+pensav;
    window.open(url, '_blank');            
    
}