<?php 
  $pfectivo=0;
  $r_p_e=$this->ModeloCatalogos->getselectwheren('personal',array('p_efectivo'=>1,'personalId'=>$idpersonal));
  if($r_p_e->num_rows()>0){
      $pfectivo=1;
  }
?>
<style type="text/css">
  @media only screen and (max-width: 550px){/* solo si es menor o igual a 450px */
    #modal_pago_s.modal {width: 95%;}
  }
  .view_person{display: none;}
</style>
<div id="modal_pago_s" class="modal"  > 
    <div class="modal-content "> 
      <div class="row"> 
        <div class="col s6"> 
          <h5>Registro de nuevo pago</h5> 
        </div> 
        <div class="col s6" align="right"> 
          <div class="row" style="margin-bottom: 0px;">
            <div class="col s7">
              Monto de prefactura
            </div>
            <div style="color: red" class="col s5 monto_prefactura_s"></div>
          </div>
          <!--<div class="row" style="margin-bottom: 0px;">
            <div class="col s7">
              Restante
            </div>
            <div class="col s5 restante_prefactura_s" style="color: red"></div>
          </div>-->
        </div> 
      </div>   
      <h6>Vendedor: <?php echo($_SESSION['usuario']); ?></h6> 
      <form class="form" id="form_pago_servicio" method="post">  
        <input type="hidden" id="idservicio" name="idcompra"><!--id que se va aguardar dependiendo si es venta, renta, poliza y combinada --> 
        <input type="hidden" id="tipos" name="tipo"><!-- tipo para verificar en que tabla se va insertar -->  
        <div class="row"> 
          <div class="col s6 m3 l2"> 
            <label for="fecha" class="active">Fecha</label> 
            <input type="date" name="fecha" id="fecha_p_s" class="form-control-bmz" value="<?php echo date('Y-m-d');?>"> 
          </div> 
          <div class="col s6 m4 l4"> 
            <label for="idmetodo" class="active">Método de pago</label><br> 
            <select name="idmetodo" id="idmetodo_p_s" class="browser-default form-control-bmz" onchange="fp_pago_s()"> 
               <?php
                  if($pfectivo==1){
                    echo '<option value="1">01 Efectivo</option>';
                  }
                  echo '<option value="2">02 Cheque nominativo</option><option value="3">03 Transferencia electrónica de fondos</option><option value="4">04 Tarjetas de Crédito</option><option value="5">05 Monedero Electrónico</option>';
                  echo '<option value="6">06 Dinero Electrónico</option><option value="7">08 Vales de despensa</option><option value="8">12 Dación en pago</option><option value="9">13 Pago por subrogación</option>';
                  echo '<option value="10">14 Pago por consignación</option><option value="11">15 Condonación</option><option value="12">17 Compensación</option><option value="13">23 Novación</option>';
                  echo '<option value="14">24 Confusión</option><option value="15">25 Remisión de deuda</option><option value="16">26 Prescripción o caducidad</option><option value="17">27 A satisfacción del acreedor</option>';
                  echo '<option value="18">28 Tarjeta de Débito</option><option value="19">29 Tarjeta de Servicio</option><option value="20">30 Aplicación de anticipos</option><option value="21">99 Por definir</option>';
              ?>
            </select>
          </div>
          <div class="input-field col s12 m4 l4"> 
            <input type="number" name="pago" id="pago_p" class="form-control-bmz"> 
            <label for="pago" class="active">Monto</label> 
          </div> 
        </div>
        <div class="row"> 
          <div class="input-field col s12 m9 l9"> 
            <textarea name="observacion" ></textarea> 
            <label for="observacion_p" class="active">Comentario</label> 
          </div>
          <div class="col s12 m3 l3">
            <a class="waves-effect waves-light btn modal-triggert" onclick="guardar_pagos_tec()">Guardar</a> 
          </div>
        </div>
      </form>
      <div class="row">
        <div class="col s12">
          <span class="text_tabla_pago"></span>
        </div>
      </div>
    </div>
  </div>

  <div id="modal_addfactura" class="modal"  style="width: 90%">
    <div class="modal-content ">
        <h4>Agregar una factura</h4>
        <input type="hidden" id="addfactcontrato" value="160">
        <input type="hidden" id="addfactperiodo">
        <input type="hidden" id="addfactpersonal" value="1">
        <input type="hidden" id="totalgeneralvalor" value="0">
        <div class="row">
          <div class="col s12">
            <table id="tabla_facturacion" name="tabla_facturacion_cliente" class="responsive-table display" cellspacing="0">
                <thead>
                  <tr>
                    <th>#</th><th>Folio</th><th>Cliente</th><th>RFC</th><th>Monto</th><th>Estatus</th><th>Fecha</th><th></th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
          </div>
        </div>
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
        <a href="#!" class="modal-action modal-close waves-effect waves-red red btn-flat" onclick="agregarfacturas()" style="color: white;">Aceptar</a>
    </div>
    <!-- Modal finaliza fin-->
</div> 
<script type="text/javascript">
  function fp_pago_s(){
    var met = $('#idmetodo_p_s option:selected').val();
    var fecha = $('#fechaactual').val();
    if(met=='01'){
      $('#fecha_p_s').val(fecha).prop({'readonly':true});
    }else{
      $('#fecha_p_s').prop({'readonly':false});
    }
  }
</script>