<?php
if(isset($cliente))
{
$idCliente = $cliente->id;
$empresa = $cliente->empresa;
}
?>
<style type="text/css" class="print_css">
  
</style>
<!-- START CONTENT -->
<section id="content" width="100%">
  <!--start container-->
  <div class="container" width="100%">
    <div class="section" style="margin-top: -53px;">
     <br>
      <div class="row">
        <input id="base_url" type="hidden" name="base_url" value="<?php echo base_url(); ?>">
        <!-- FORMULARIO  card-panel-->
        <div class="col s12">
          <div class="">

            <!-- ENCABEZADO PRINCIPAL PDF -->
            <div class="row"> 
              <div class="col s3">
                <img src="<?php echo base_url(); ?>app-assets/images/alta.png">
              </div>
              <div class="col s6">
                <p class="headerPDF"><b>Impresoras y Multifuncionales, Venta, Renta y Mantenimiento.<br>Blvd. Norte 1831 V. San Alejandro. Puebla, Pue. Call Center 273 3400, 249 5393<br>www.kyoceraapp.com</b></p>
              </div> 
              <div class="col s3">
                <p align="center"><img class="" src="<?php echo base_url(); ?>app-assets/images/1024_kyocera_logo_mds.png" alt="materialize logo" width="200px;" height="50px;"></p>
              </div>
            </div>

            <!-- APARTADO DE INFORMACIÓN DE RECEPTOR DE COTIZACIÓN-->
            
            
            <div class="row">
              <table class="centered datosClienteCotizacion tablaBordesHeader">
                  <thead>
                    
                  </thead>
                  <tbody>
                    <tr >
                      <td class="tablaBordesHeader" >
                        Cotización Especial Para: <br>
                        Mangoo Software
                      </td>
                      <td class="tablaBordesHeader">
                        Atención para: <br>
                        Raúl Trujeque Martínez
                      </td>
                      <td class="tablaBordesHeader">
                        Correo raul@mangoo.com.mx <br>
                        Teléfono: 2224578961
                      </td>
                      <td class="tablaBordesHeader">
                        Cotización No. <?php echo rand ( 1000, 5000 );?> 
                      </td>
                    </tr>
                    <tr>
                      <td colspan="4" style="font-size: 14px;">
                        <b>Cotización de Servicio de Mantenimiento</b>
                      </td>
                    </tr>
                  </tbody>
                </table>
            </div>
            <br><br>
            <!------------ TABLA DE INFORMACIÓN DE EQUIPOS ------------>
            <div class="row">
                <table class="centered bordered letraTablaImpresionPoliza tablaBordesPrincipal">
                  <thead >
                    <tr class="tablaBordesPrincipal">
                        <th class="tablaBordesPrincipal">Tipo de Póliza</th>
                        <th class="tablaBordesPrincipal">Cobertura</th>
                        <th class="tablaBordesPrincipal">Vigencia</th>
                        <th class="tablaBordesPrincipal">Visitas</th>
                        <th class="tablaBordesPrincipal">Tipo de Servicios</th>
                        <th class="tablaBordesPrincipal">Precio Unitario</th>
                        <th class="tablaBordesPrincipal">Subtotal</th>
                        <th class="tablaBordesPrincipal">I.V.A.</th>
                        <th class="tablaBordesPrincipal">Total</th>
                    </tr>
                  </thead>
                  <tbody >
                    <tr class="tablaBordesPrincipal">
                      <td class="tablaBordesPrincipal">MAX 100</td>
                      <td class="tablaBordesPrincipal">Mano de Obra</td>
                      <td class="tablaBordesPrincipal">12 Meses</td>
                      <td class="tablaBordesPrincipal">Ilimitadas</td>
                      <td class="tablaBordesPrincipal">Preventivo, Correctivo, Soporte o Asesoria</td>
                      <td class="tablaBordesPrincipal">$6,000.00</td>
                      <td class="tablaBordesPrincipal">$6,000.00</td>
                      <td class="tablaBordesPrincipal">$960.00</td>
                      <td class="tablaBordesPrincipal">$6,960.00</td>
                    </tr>
                    <tr>
                      <td class="tablaBordesPrincipal">EXPRESS 12</td>
                      <td class="tablaBordesPrincipal">Mano de Obra</td>
                      <td class="tablaBordesPrincipal">12 Meses</td>
                      <td class="tablaBordesPrincipal">12 (Una mensual)</td>
                      <td class="tablaBordesPrincipal">Preventivo, Correctivo, Soporte o Asesoria</td>
                      <td class="tablaBordesPrincipal">$5,100.00</td>
                      <td class="tablaBordesPrincipal">$5,100.00</td>
                      <td class="tablaBordesPrincipal">$816.00</td>
                      <td class="tablaBordesPrincipal">$5,916.00</td>
                    </tr>
                    <tr>
                      <td class="tablaBordesPrincipal">SUPER 6</td>
                      <td class="tablaBordesPrincipal">Mano de Obra</td>
                      <td class="tablaBordesPrincipal">12 Meses</td>
                      <td class="tablaBordesPrincipal">6 (A solicitud del cliente)</td>
                      <td class="tablaBordesPrincipal">Preventivo, Correctivo, Soporte o Asesoria</td>
                      <td class="tablaBordesPrincipal">$2,700.00</td>
                      <td class="tablaBordesPrincipal">$2,700.00</td>
                      <td class="tablaBordesPrincipal">$432.00</td>
                      <td class="tablaBordesPrincipal">$3,132.00</td>
                    </tr>
                  </tbody>
                </table>
            </div>

            <br><br><br><br><br>
            <!-- Condiciones de Equipos -->
            <div class="row" style="margin-bottom: -10px">
              <table class="centered letraTablaFooterPoliza">
                  <thead>
                      <tr>
                        <th>Garantía y Beneficios</th>
                        <th></th>
                        <th>Responsable</th>
                      </tr>
                  </thead>
                  <tbody >
                    <tr>
                        <td colspan="2">
                          <ul>
                            <li>
                              <b>Servicio Garantizado.</b> Atención con personal Certificado por "KYOCERA".
                            </li>
                            <li>
                              <b>Consumibles y Refacciones.</b> Para su mayor confianza, solo manejamos originales de "KYOCER.
                            </li>
                            <li>
                              <b>Tiempo de Solución (en otros casos tiempo de respuesta).</b> Dentro de las 24 hrs.
                            </li>
                            <li>
                              <b>Operación Óptima.</b> Calidad de Impresión permanente.
                            </li>
                            <li>
                              <b>Valor de la inversión.</b> Mayor vida útil y mayor valor de reventa.
                            </li>
                            <li>
                              <b>Precios.</b> Descuento especial del 30% en Refacciones, Toner y Kit de mantemiento para clientes con Póliza vigente.
                            </li>
                            <li>
                              <b>Respaldo.</b> En caso de robo o perdida total, apoyo con equipo similar (sujeto a disponibilidad).
                            </li>
                            <li>
                              <b>Soporte.</b> Instalación de KM-Net Viewer para control y monitoreo.
                            </li>
                            <li>
                              <b>Servicio Extraordinario.</b> 30% de Descueto sobre tarifa por evento extraordinario requerido durante la vigencia del contrato.
                            </li>
                            <li>
                              <b>Servicio Urgente en Taller.</b> $0.00 de mano de obra por evento urgente, en nuestro taller con previa cita, durante la vigencia del contrato.
                            </li>
                          </ul>
                        </td>
                        <!--<td width="33%"><b>Precios: </b>De contado, sujetos a I.V.A. y/o cambios sin previo aviso.  </td>-->
                        <td width="33%"><b>Enero de 2018</b><br><b><b>Susana Guerrero. </b>ventas@kyoceraap.com</td>
                    </tr>
                    <tr>
                        <td colspan="3">Depositar a nombre de <b>"Alta Productividad, S.A. de C.V."</b> Banco Banamex, Suc 0826, Cuenta 7347132, CLABE 002668082673471327 (para transferencia).<br><b>Mandar comprobante a este correo por favor: kyorent@kyoceraapp.com</b></td>
                    </tr>
                  </tbody>
                </table>
            </div>

           

<!--columna 1---------------------------------------------------------------->

              <form class="form" id="" method="post">
                <?php
                if(isset($cliente))
                {
                ?>
                <input id="idCliente" name="idCliente" type="hidden" value="<?php echo $idCliente; ?>">
                <?php
                }
                ?>
                <div class="row">
                  <div class="input-field col s12">
                    <button class="btn cyan waves-effect waves-light right btn_imp" type="button" onclick="btn_guardar()">Imprimir
                    <i class="material-icons right">send</i>
                    </button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>