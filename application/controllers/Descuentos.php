<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Descuentos extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->model('Descuentos_model', 'model');
        $this->load->model('login_model');
        $this->load->helper('url');
        $this->load->library('menus');
        $this->load->model('ModeloCatalogos');
        $this->submenu=22;
        if($this->session->userdata('logeado')==true){
            $this->perfilid =$this->session->userdata('perfilid');
            $this->idpersonal = $this->session->userdata('idpersonal');
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,$this->submenu);// 22 es el id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
        }else{
            redirect('login');
        }
    }

    // Listado de porcentajes
    function index()     {
        $data['MenusubId']=$this->submenu; 
            // Obtiene los menús disponibles del sistema dependiendo el perfil, ayudado de la libreria "MENUS"
            $menus = $this->menus->getMenus($this->session->userdata('perfilid'));
            $data['menus'] = $menus['menus'];
            $data['submenusArray'] = $menus['submenusArray'];
            $data['porcentajes'] = $this->model->getDescuentos();
            
            $this->load->view('header');
            $this->load->view('main',$data);
            $this->load->view('configuraciones/descuentos/form_descuentos');
            $this->load->view('configuraciones/descuentos/form_descuentos_js');
            $this->load->view('footer');
        
    }

    function actualizarPorcentajesDescuento(){
        // Obtenemos el POST que viene del formulario
        $data = $this->input->post();
        
        // Organizamos los datos, para dentro de un ciclo actualizar
        $datos[1] = array('descuento' => $data["porcentaje_descuentos_equipos"]);

        $datos[2] = array('descuento' => $data["porcentaje_descuentos_refacciones"]);

        $datos[3] = array('descuento' => $data["porcentaje_descuentos_consumibles"]);
        
        $datos[4] = array('descuento' => $data["porcentaje_descuentos_accesorios"]);


        // Como los ID's sono fijos, los vamos pasando conforme se ejecute el ciclo
        for ($i=1; $i <=4 ; $i++){ 
            // Actualiza cada registro
            $this->model->actualizaPorcentajes($i,$datos[$i]);
        }
        //$this->Modelobitacoras->Insert(array('contenido'=>'Se realizo una actualizacion del porcentale de descuentos','nombretabla'=>'descuentosCategorias','idtable'=>0,'tipo'=>'Update','personalId'=>$this->idpersonal));
    }

   
}

?>