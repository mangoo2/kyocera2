var base_url = $('#base_url').val();

$(document).ready(function(){
    table = $('#tabla_prospectos').DataTable();
    load();

    $('#tabla_prospectos tbody').on('click', 'a.minimodal', function () {
        var tr = $(this).closest('tr');
        var row = table.row(tr);
        var data = row.data();

        $('.modal').modal({
            dismissible: true
        });
 
        //call the specific div (modal)
        $('#modalAcciones').modal('open');
        $('#idProspectoModal').val(data.id);
    });

    $('#lista_editar').on('click', function () {
        var id = $('#idProspectoModal').val();
        window.location = 'Prospectos/edicion/'+id;
    });
    /*
    $('#tabla_prospectos tbody').on('click', 'a.edit', function () {
        var tr = $(this).closest('tr');
        var row = table.row(tr);
        var data = row.data();
        window.location = 'Prospectos/edicion/'+data.id;
    });
    */
    // Listener para Visualizar 
    $('#tabla_prospectos tbody').on('click', 'a.visualiza', function () 
    {
        var tr = $(this).closest('tr');
        var row = table.row(tr);
        var data = row.data();
        window.location = 'Prospectos/visualizar/'+data.id;
    });

    // Listener para eliminar 
    $('#tabla_prospectos tbody').on('click', 'a.elimina', function () 
    {
        var tr = $(this).closest('tr');
        var row = table.row(tr);
        var data = row.data();
        confirmaEliminarProspecto(data.id);
    });

    // Listener para Convertir 
    $('#tabla_prospectos tbody').on('click', 'a.convertir', function () 
    {
        var tr = $(this).closest('tr');
        var row = table.row(tr);
        var data = row.data();
        confirmaConvertirCliente(data.id);
    });

});
function confirmaEliminarProspecto(idProspecto)
{
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de Eliminar este Prospecto? El cambio será irreversible',
        type: 'red',
        typeAnimated: true,
        buttons: 
        {
            confirmar: function () 
            {
                $.ajax({
                    url: base_url+"index.php/prospectos/eliminarProspecto/"+idProspecto,
                    success: function (response) 
                    {
                        if(response==1)
                        {
                            $.alert({
                                boxWidth: '30%',
                                useBootstrap: false,
                                title: 'Éxito!',
                                content: 'Prospecto Eliminado!'});
                            setTimeout(function(){ 
                                window.location.href = base_url+"index.php/prospectos"; 
                            }, 2000);
                            
                        }
                        else
                        {
                            $.alert({
                                boxWidth: '30%',
                                useBootstrap: false,
                                title: 'Error!',
                                content: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema'}); 
                            setTimeout(function(){ 
                                window.location.href = base_url+"index.php/prospectos"; 
                            }, 2000);      
                        }
                    },
                    error: function(response)
                    {
                        $.alert({
                                boxWidth: '30%',
                                useBootstrap: false,
                                title: 'Error!',
                                content: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema'});   
                        setTimeout(function(){ 
                                window.location.href = base_url+"index.php/prospectos";
                            }, 2000);  
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}

function confirmaConvertirCliente(idProspecto)
{
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de querer convertire este Prospecto en Cliente? Una vez realizado el cambio, deberá acceder al apartado de clientes y capturar sus datos fiscales',
        type: 'blue',
        typeAnimated: true,
        buttons: 
        {
            confirmar: function () 
            {
                $.ajax({
                    url: base_url+"index.php/prospectos/convertirProspectoCliente/"+idProspecto,
                    success: function (response) 
                    {
                        console.log(response);
                        if(response==1)
                        {
                            $.alert({
                                boxWidth: '30%',
                                useBootstrap: false,
                                title: 'Éxito!',
                                content: 'Cliente Creado!'});
                            setTimeout(function(){ 
                                window.location.href = base_url+"index.php/prospectos"; 
                            }, 2000);
                            
                        }
                        else
                        {
                            $.alert({
                                boxWidth: '30%',
                                useBootstrap: false,
                                title: 'Error!',
                                content: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema'}); 
                            setTimeout(function(){ 
                                window.location.href = base_url+"index.php/prospectos"; 
                            }, 2000);      
                        }
                    },
                    error: function(response)
                    {
                        console.log(response);
                        $.alert({
                                boxWidth: '30%',
                                useBootstrap: false,
                                title: 'Error!',
                                content: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema'});   
                        setTimeout(function(){ 
                                window.location.href = base_url+"index.php/prospectos";
                            }, 2000);
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}

function load() {
    // Destruye la tabla y la crea de nuevo con los datos cargados 
    table.destroy();
    table = $('#tabla_prospectos').DataTable({
        "ajax": {
            "url": base_url+"index.php/Prospectos/getListadoProspectos"
        },
        "columns": [
            {"data": "id"},
            {"data": "empresa"},
          //  {"data": "persona_contacto"},
            {"data": "puesto_contacto"},
            {"data": "email"},
            {"data": "estado"},
            {"data": "municipio"},
            {"data": "giro"},
            {"data": "direccion"},
            {"data": "observaciones"},            
            {"data": null,
                 render:function(data,type,row){
                    $('.tooltipped').tooltip();
                    var html='<ul>\
                                <li>\
                                  <a class="btn-floating red tooltipped minimodal" data-position="top" data-delay="50" data-tooltip="Editar">\
                                    <i class="material-icons">mode_edit</i>\
                                  </a>\
                                </li>\
                              </ul>';
                    return html;
                }
            }
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[25, 50, 100], [25, 50, 100]],
        "dom": 'Bfrtpl',
        // Muestra el botón de Excel para importar la tabla
        "buttons": [
            {   
                extend: 'excelHtml5',
                text: ' Descargar Excel <i class="fa fa-download"></i>',
                className: 'btn classBotonFratsa'
            }
        ],
        // Cambiamos lo principal a Español
        "language": {
            "lengthMenu": "Desplegando _MENU_ elementos por página",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
              }
        }
    });
}

/*
<a class="btn-floating green tooltipped edit" data-position="top" data-delay="50" data-tooltip="Editar">\
    <i class="material-icons">mode_edit</i>\
</a>\
<a class="btn-floating blue tooltipped visualiza" data-position="top" data-delay="50" data-tooltip="Visualizar">\
    <i class="material-icons">remove_red_eye</i>\
</a>\
<a class="btn-floating red tooltipped elimina" data-position="top" data-delay="50" data-tooltip="Eliminar">\
    <i class="material-icons">delete_forever</i>\
</a>\
<a class="btn-floating purple tooltipped convertir" data-position="top" data-delay="50" data-tooltip="Convertir">\
    <i class="material-icons">compare_arrows</i>\
</a>\
*/