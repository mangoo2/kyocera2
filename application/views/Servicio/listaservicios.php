<style type="text/css">
  .chosen-container{margin-left: 0px;}
  td{font-size: 12px;}
  #tabla_asignar{margin: 0px;}
  #tabla_asignar td{padding: 5px;}
  .card-panel{padding-left: 10px;padding-right: 10px;}
  .activopintado{background-color: #ffd3b5 !important;}
  @media (max-width:550px){
    .header{color: red;}
    h4, h5{font-size: 20px;}
    td{font-size: 11px;}
    .card-panel{padding-top: 0px;}
  }
  .seach_serie_tbody .collapsible-body{padding: 1rem;}
  .seach_serie_tbody td{font-size: 11px;}
  .minheight1{
    min-height: 60.25px !important;
  }
  .infoerror{
    padding: 5px 10px 0px 10px;
  }
</style>
<input type="hidden" id="fechaactual" value="<?php echo date('Y-m-d'); ?>">
<!-- START CONTENT -->
<section id="content">
  <!--start container-->
  <div class="container">
    <div class="section">
      <div class="card-panel">
      		<div id="table-datatables">
             <div class="row">
              <div class="col s8 m8">
                <h4 class="header">Listado de Servicios</h4>
              </div>            
              <div class="col s4 m4" align="right">
                <button class="b-btn b-btn-primary" onclick="imprimir_ser()" title="Imprecion de Servicios del dia">Imprimir Servicios</button>
                <button class="btn-bmz cyan waves-effect waves-light right savepoliza" type="button" onclick="load()">Actualizar
                </button>
              </div>   
            </div>
            <div class="row">
              <div class="col s6 m2 l2">
                <label>Servicio</label>
                <select  id="serv" class="browser-default  form-control-bmz" >
                  <option value="0">Seleccionar</option><option value="1">Contrato</option><option value="2">Poliza</option><option value="3">Evento</option><option value="4">Ventas</option>
                </select>
              </div>
              <div class="col s6 m2 l2">
                <label>Tipo de Servicio</label>
                <select  id="tiposer" class="browser-default  form-control-bmz" >
                  <option value="0">Todos</option><option value="1">Regular</option><option value="2">Asignación diaria</option>
                </select>
              </div>
              <div class="col s6 m2 l2">
                <label>Técnico/Especialista</label>
                <select id="tecnico" class="browser-default  form-control-bmz" >
                  <option value="0">Todos</option>
                  <?php foreach ($resultstec as $item) { ?>
                      <option value="<?php echo $item->personalId;?>"><?php echo $item->tecnico;?></option>
                  <?php } ?>
                  <option value="XXX" title="servicio generado automaticamente de contrato">Sin tecnico</option>
                </select>
              </div>
              <div class="col s6 m2 l2">
                <label>Proceso</label>
                <select id="status" class="browser-default  form-control-bmz" >
                  <option value="0">Todos</option><option value="1">En espera</option><option value="2">Proceso</option><option value="3">Finalizados</option><option value="4">Sin Atención</option>
                </select>
              </div>
              <div class="col s6 m2 l2" style="display:none;">
                <label>Zona</label>
                <select id="zona" class="browser-default  form-control-bmz" >
                  <option value="0">Todos</option>
                    <?php foreach ($resultzonas as $item) { ?>
                      <option value="<?php echo $item->id;?>"><?php echo $item->nombre;?></option>
                    <?php  } ?>
                </select>
              </div>
              <div class="col s6 m2 l2">
                <label>Cliente</label>
                <select id="pcliente" class="browser-default" >
                  <?php if (isset($_GET['servicio'])) { 
                      echo '<option value="'.$_GET['idcliente'].'">'.$_GET['cliente'].'</option>';
                  }?>
                  <option value="0">Todos</option>
                </select>
              </div>
              <div class="col s6 m2 l2">
                <label>Estatus</label>
                <select id="estatus_ad" class="browser-default form-control-bmz" >
                  <option value="1">Activos</option><option value="0">Eliminados</option>
                </select>
              </div>
            </div>
            <div class="row">
              <div class="col s6 m2 l2 minheight1">
                <label>Fecha inicio</label>
                <input type="date" id="fechainicio" class="form-control-bmz" value="<?php echo date('Y-m-d');?>" onchange="load();" style="margin-bottom: 0px;">
              </div>
              <div class="col s6 m2 l2 minheight1">
                <label>Fecha Fin</label>
                <input type="date" id="fechafin" class="form-control-bmz"  style="margin-bottom: 0px;">
              </div>
              <div class="col s6 m2 l2 minheight1">
                <label>Notificación</label>
                <select id="noti" class="browser-default form-control-bmz" >
                  <option value="0">Todos</option><option value="1">Solo las notificaciones</option>
                </select>
              </div>
              <div class="col s6 m2 l2 minheight1">
                <button class="btn-bmz cyan waves-effect waves-light" style="margin-top: 21px;" type="button" onclick="exportar()"><i class="fa fa-file-excel"></i> Resumen del dia</button>
              </div>
              <div class="col s6 m2 l2 minheight1">
                <button class="btn-bmz cyan waves-effect waves-light" style="margin-top: 21px;" type="button" onclick="modalbuscarserie()"><i class="fa fa-search"></i> Buscar Serie</button>
              </div>
              <div class="col s6 m2 l2 minheight1">
                <?php
                  $result_noerror_v=$this->ModeloCatalogos->getselectwheren('asignacion_ser_venta_a',array('notificarerror_estatus'=>1,'activo'=>1));
                  $result_noerror_c=$this->ModeloCatalogos->getselectwheren('asignacion_ser_contrato_a_e',array('notificarerror_estatus'=>1,'activo'=>1));
                  $result_noerror_p=$this->ModeloCatalogos->getselectwheren('asignacion_ser_poliza_a_e',array('notificarerror_estatus'=>1,'activo'=>1));
                  $result_noerror_cl=$this->ModeloCatalogos->getselectwheren('asignacion_ser_cliente_a_d',array('notificarerror_estatus'=>1,'activo'=>1));
                  $result_total=$result_noerror_v->num_rows()+$result_noerror_c->num_rows()+$result_noerror_p->num_rows()+$result_noerror_cl->num_rows();
                  if($result_total>0){
                    $btn_color='red';
                  }else{
                    $btn_color='cyan';
                  }
                ?>
                <a class='dropdown-button btn-bmz  <?php echo $btn_color;?>' style="margin-top: 21px;"  data-activates='dropdown1'>Notificaciones <?php echo $result_total;?></a>
                <!-- Dropdown Structure -->
                <ul id='dropdown1' class='dropdown-content'>
                  <?php 
                    if($result_noerror_v->num_rows()>0){
                      ?><li onclick="noerror(4)"><a >Ventas <?php echo $result_noerror_v->num_rows();?></a></li><?php
                    }
                    if($result_noerror_c->num_rows()>0){
                      ?><li onclick="noerror(1)"><a >Contratos <?php echo $result_noerror_c->num_rows();?></a></li><?php
                    }
                    if($result_noerror_p->num_rows()>0){
                      ?><li onclick="noerror(2)"><a >Poliza <?php echo $result_noerror_p->num_rows();?></a></li><?php
                    }
                    if($result_noerror_cl->num_rows()>0){
                      ?><li onclick="noerror(3)"><a >Eventos <?php echo $result_noerror_cl->num_rows();?></a></li><?php
                    }
                  ?>
                 
                  
                </ul>
              </div>
              <div class="col s4 m1 l1">
                  <button class="b-btn b-btn-primary" onclick="load()" style="margin-top: 20px;" title="Realizar consulta"><i class="fas fa-filter"></i> Filtrar</button>
              </div>
            </div>
            <div class="row">
              <br>
            </div>
            <div class="row">
              <div class="col s12 tabla_asignar_1">
                <table id="tabla_asignar_1" class="table display" cellspacing="0">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th></th>
                      <th>Técnico</th>
                      <th>Cliente</th>
                      <th>Fecha</th>
                      <th>Hora</th>
                      <!--<th>Zona</th>-->
                      <th class="cambiartitulo_8">Equipo</th>
                      <th>Serie</th>
                      <th>Status</th>
                      <th></th>
                      <!--<th></th>-->
                      <th>Acciones</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
              <div class="col s12 tabla_asignar_2" style="display:none;">
                <table id="tabla_asignar_2" class="responsive-table display" cellspacing="0">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th></th>
                      <th>Técnico</th>
                      <th>Cliente</th>
                      <th>Fecha</th>
                      <th>Hora</th>
                      <!--<th>Zona</th>-->
                      <th class="cambiartitulo_8">Equipo</th>
                      <th>Serie</th>
                      <th>Status</th>
                      <th></th>
                      <!--<th></th>-->
                      <th>Acciones</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
              <div class="col s12 tabla_asignar_3" style="display:none;">
                <table id="tabla_asignar_3" class="responsive-table display" cellspacing="0">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th></th>
                      <th>Técnico</th>
                      <th>Cliente</th>
                      <th>Fecha</th>
                      <th>Hora</th>
                      <!--<th>Zona</th>-->
                      <th class="cambiartitulo_8">Equipo</th>
                      <th>Serie</th>
                      <th>Status</th>
                      <th></th>
                      <!--<th></th>-->
                      <th>Acciones</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
              <div class="col s12 tabla_asignar_4" style="display:none;">
                <table id="tabla_asignar_4" class="responsive-table display" cellspacing="0">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th></th>
                      <th>Técnico</th>
                      <th>Cliente</th>
                      <th>Fecha</th>
                      <th>Hora</th>
                      <th>Zona</th>
                      <th class="cambiartitulo_8">Equipo</th>
                      <th>Serie</th>
                      <th>Status</th>
                      <th></th>
                      <th></th>
                      <th>Acciones</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
      </div>
    </div>  
  </div>
</section>
<input type="hidden" id="idrow">
<input type="hidden" id="idtable"><!--1 contrato 2 pencion 3 asignacion dia-->
<input type="hidden" id="asignacionId">
<div id="modaleditfecha" class="modal">
    <div class="modal-content ">
      <h4>Editar fecha y hora</h4>
      <span class="re_agendar"></span>
      <div class="col s12 m12 l6">
          <div class="row" align="center">
             <div class="col s12 m12 l12">
               <label class="col s12 m2 l2">Fecha</label>
               <div class="col s12 m8 l8">
                 <input type="date" name="fecha" id="fecha" class="form-control-bmz" min="<?php echo date('Y-m-d');?>">
               </div>
             </div>
             <div class="col s6 m6 l6">
               <label class="col s12 m12 l12">Hora Inicio</label>
               <div class="col s12 m12 l12">
                 <input type="time" name="hora" id="hora" class="form-control-bmz">
               </div>
             </div> 
             <div class="col s6 m6 l6">
               <label class="col s12 m12 l12">Hora Fin</label>
               <div class="col s12 m12 l12">
                 <input type="time" name="horafin" id="horafin" class="form-control-bmz">
               </div>
             </div>
             <div class="col s6 m6 l6">
               <label class="col s12 m12 l12">Hora Inicio comida</label>
               <div class="col s12 m12 l12">
                 <input type="time" name="hora_comida" id="hora_comida" class="form-control-bmz">
               </div>
             </div> 
             <div class="col s6 m6 l6">
               <label class="col s12 m12 l12">Hora Fin comida</label>
               <div class="col s12 m12 l12">
                 <input type="time" name="horafin_comida" id="horafin_comida" class="form-control-bmz">
               </div>
             </div>
             <div class="col s12 m12 l12">
                  <input name="modifife" type="radio" id="modifife0" value="0"/>
                  <label for="modifife0">General</label>
                  <input name="modifife" type="radio" id="modifife1" value="1" checked/>
                  <label for="modifife1">Individual</label>
               </div> 
          </div>
      </div>  
    <div class="modal-footer">
      <a  class="modal-action modal-close waves-effect waves-red gray btn-flat " onclick="editarfecha()">Editar</a>
      <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
    </div>
  </div>
</div>
<div id="modalAsigna" class="modal">
    <div class="modal-content ">
      <h4>Asignar Técnico </h4>
      <div class="col s12" style="text-align: center;">
        <h5>Seleccionar técnico / especialista</h5>
      </div>
      <div class="col s12 m12 l6">
          <div class="row" align="center">
             <div class="col s12 m12 l12">
               <label class="col s12 m2 l2">Técnico</label>
               <div class="col s12 m8 l8" >
                  <input type="hidden" id="tecnicooriginal">
                 <select name="tecnicose" id="tecnicose" class="browser-default form-control-bmz">
                  <?php 
                    foreach ($resultstec as $item) { ?>
                      <option value="<?php echo $item->personalId;?>"><?php echo $item->tecnico;?></option>
                    <?php  } ?>
                   <!--<option>Juan Perez</option>
                   <option>Casimiro Lejos</option>-->
                 </select>
                 <select name="tecnicos2" id="tecnicos2" class="browser-default form-control-bmz"><option value="0"></option>
                  <?php 
                    foreach ($resultstec as $item) { ?>
                      <option value="<?php echo $item->personalId;?>"><?php echo $item->tecnico;?></option>
                    <?php  } ?>
                   <!--<option>Juan Perez</option>
                   <option>Casimiro Lejos</option>-->
                 </select>
                 <select name="tecnicos3" id="tecnicos3" class="browser-default form-control-bmz"><option value="0"></option>
                  <?php 
                    foreach ($resultstec as $item) { ?>
                      <option value="<?php echo $item->personalId;?>"><?php echo $item->tecnico;?></option>
                    <?php  } ?>
                   <!--<option>Juan Perez</option>
                   <option>Casimiro Lejos</option>-->
                 </select>
                 <select name="tecnicos4" id="tecnicos4" class="browser-default form-control-bmz"><option value="0"></option>
                  <?php 
                    foreach ($resultstec as $item) { ?>
                      <option value="<?php echo $item->personalId;?>"><?php echo $item->tecnico;?></option>
                    <?php  } ?>
                   <!--<option>Juan Perez</option>
                   <option>Casimiro Lejos</option>-->
                 </select>
               </div>
               <div class="col s12 m12 l12">
                  <input name="modifi" type="radio" id="modifi0" value="0"/>
                  <label for="modifi0">Total</label>
                  <input name="modifi" type="radio" id="modifi1" value="1" checked/>
                  <label for="modifi1">Individual</label>
               </div>
               <div class="col s12 m12 l12" style="text-align: center;">
                <p>Histórico de asignaciones</p>
               </div>
               <div class="col s12 m12 l12 historicotecnicos">
               </div>
             </div> 
          </div>
      </div>  
    <div class="modal-footer">
      <a class="modal-action modal-close waves-effect waves-red gray btn-flat " onclick="editartecnicos()">Aceptar</a>
      <a class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
    </div>
  </div>
</div>

<div id="modalsuspender" class="modal">
    <div class="modal-content ">
      <h4>Suspención de servicio:</h4>
      <div class="col s12 m12 l6">
          <div class="row" align="center">
             <div class="col s12 m12 l12">
               <label class="col s12 m2 l2">Motivo</label>
               <div class="col s12 m8 l8">
                <textarea id="tserviciomotivo"></textarea>
               </div>
             </div>
          </div>
      </div>  
    <div class="modal-footer">
      <a  class="modal-action modal-close waves-effect waves-red gray btn-flat " onclick="suspenderok()">Suspender</a>
      <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
    </div>
  </div>
</div>
<div id="modalventa" class="modal">
    <div class="modal-content ">
      <h4> <i class="material-icons">settings_applications</i>Venta</h4>
      <hr>
      <div class="col s12 m12 l6">
        <div class="row">
          <input type="hidden" id="vmidasignacion">
          <input type="hidden" id="vmidasignaciontipo">
          <div class="col s12 m2 l2">
            <label>Cantidad</label>
            <input type="number" name="cantidadc" id="cantidadc" class="form-control-bmz" value="1" min="1">
          </div>
          <div class="col s12 m4 l4">
            <label>Consumible</label>
            <select name="sconcumibles" id="sconcumibles" class="browser-default form-control-bmz sconcumibles chosen-select" onchange="funcionConsumiblesprecios($(this))">
              <option></option>
              <?php foreach ($consumiblesresult->result() as $item) { ?>
                  <option value="<?php echo $item->id;?>"><?php echo $item->modelo;?> (<?php echo $item->parte;?>)</option>
              <?php } ?>
            </select>
          </div>
          <div class="col s12 m3 l3">
            <label>Precio</label>
            <select name="sconcumiblesp" id="sconcumiblesp" class="browser-default form-control-bmz">
              
            </select>
          </div>
          <div class="col s12 m3 l3">
            <a class="waves-effect cyan btn-bmz" onclick="addconsu()">Agregar</a>
          </div>
        </div>
        <div class="row ">
          <table class="table striped" id="tableaddconsulmibles">
            <thead>
              <tr>
                <th>Cantidad</th>
                <th>consumible</th>
                <th>Precio</th>
                <th></th>
              </tr>
            </thead>
            <tbody class="tabaddconsul">
              
            </tbody>
            
          </table>
        </div>
        <div class="row">
          <div class="col s12 m2 l2">
            <label>Cantidad</label>
            <input type="number" name="cantidadr" id="cantidadr" class="form-control-bmz" value="1" min="1">
          </div>
          <div class="col s12 m4 l4">
            <label>Refaccion</label>
            <select name="srefaccions" id="srefaccions" class="browser-default form-control-bmz chosen-select" onchange="selectprecior()">
              <option></option>
              <?php foreach ($refaccionesresult->result() as $item) { ?>
                  <option value="<?php echo $item->id;?>"><?php echo $item->nombre;?></option>
              <?php } ?>
            </select>
          </div>
          <div class="col s12 m3 l3">
            <label>Precio</label>
            <select name="srefaccionp" id="srefaccionp" class="browser-default form-control-bmz">
              
            </select>
          </div>
          <div class="col s12 m3 l3">
            <a class="waves-effect cyan btn-bmz" onclick="addrefa()">Agregar</a>
          </div>
        </div>
        <div class="row ">
          <table class="table striped" id="tableaddrefacciones">
            <thead>
              <tr>
                <th>Cantidad</th>
                <th>Refaccion</th>
                <th>Precio</th>
                <th></th>
              </tr>
            </thead>
            <tbody class="taaddrefaccion">
              
            </tbody>
            
          </table>
        </div>
        <div class="row">
          <div class="col s12 m8 l8">
          </div>
          <div class="col s12 m3 l3">
            <p>Total: <b>$<span class="totalg">0.00</span></b></p>
          </div>
        </div>
      </div> 
      <div class="modal-footer">
          <a class="modal-action modal-close waves-effect waves-red gray btn-flat">Cerrar</a>
          <a class=" waves-effect waves-red gray btn-flat cancelarmodalventa">Cancelar pedido</a>
          <a class=" waves-effect waves-red gray btn-flat savemodalventa">Edicion</a>
          
          
      </div>
    </div>
</div>
<div id="modal_lis_ser" class="modal">
    <div class="modal-content ">
      <div class="row">
        <div class="col s12">
          <table class="table">
            <thead>
              <tr>
                <th></th>
                <th>Hora inicio</th>
                <th>Hora Fin</th>
                <th>Tiempo</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td></td>
                <td class="text_h_i"></td>
                <td class="text_h_f"></td>
                <td class="text_h_t"></td>
              </tr>
              <tr class="tiempoextras">
                <td>Extraordinario</td>
                <td class="text_h_iex"></td>
                <td class="text_h_fex"></td>
                <td class="text_h_tex"></td>
              </tr>
            </tbody>
          </table>
        </div>
        
      </div>  
    <div class="modal-footer">
      <a class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
    </div>
  </div>
</div>

<div id="modal_seach_serie" class="modal">
    <div class="modal-content ">
      <div class="row">
        <div class="col s12">
          <h4>Buscar Serie</h4>
        </div>
      </div>
      <div class="row">
          <div class="col s2" style="padding-right: 0px;">
            <input type="search" class="form-control-bmz" id="input_search">
          </div>
          <div class="col s2" style="padding-left: 0px;">
            <button class="btn-bmz cyan waves-effect waves-light" style="" type="button" onclick="buscarserie()"><i class="fa fa-search"></i> Buscar
                </button>
          </div>
          <div class="col s2" style="padding-left: 0px;">
            <button class="btn-bmz cyan waves-effect waves-light" style="" type="button" onclick="buscarserielimpiar()">Limpiar</button>
          </div>
          
        </div>
        <div class="row">
          <div class="col s12 seach_serie_tbody">
            
          </div>
          
        </div> 
    </div>
       
    <div class="modal-footer">
      <a class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
    </div>
  </div>
</div>

<div id="modal_procesando" class="modal" style="background-color: #fafafa00 !important; box-shadow: 0 8px 10px 1px rgb(0 0 0 / 0%), 0 3px 14px 2px rgb(0 0 0 / 0%), 0 5px 5px -3px rgb(0 0 0 / 0%) !important;">
    <div class="modal-content ">
      <div class="row">
        <div class="col s12" align="center">
          <h1 style="color: white !important;">Procesando...</h1>
        </div>
      </div>
  </div>
</div>


<div id="modal_notificacion" class="modal">
    <div class="modal-content ">
      <div class="row">
        <div class="col s12">
          <h4>Notificación</h4>
        </div>
      </div>
      <div class="row">
          <div class="col s12 detallenotificacion">
            
          </div>
          <div class="col s12 funcionesnotificacion">
            
          </div>
          <div class="col s12 funciones2notificacion">
            
          </div>
          
      </div>
    </div>
       
    <div class="modal-footer">
      <a class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
    </div>
  </div>
</div>