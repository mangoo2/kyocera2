<style type="text/css">
  #tabla_empleados td{
    font-size: 13px;
  }
</style>
<!-- START CONTENT -->
    <section id="content">
    	<!--start container-->
      	<div class="container">
      		<div class="section">

      		<input id="base_url" type="hidden" name="base_url" value="<?php echo base_url(); ?>">

      		<div id="table-datatables">
      			<br>
                <h4 class="header">Listado de Empleados</h4>
                <div class="row">
                  
                  <div class="col s12"><a class="btn waves-effect waves-light gradient-45deg-green-teal gradient-shadow col s1" href="<?php echo base_url(); ?>index.php/empleados/alta">Nuevo</a>
                  
                      <div class="col s2">
                      
                      </div>
                      <div class="form-group col s6"  align="right">
                        
                      </div>

                      <div class="col s3" align="right">
                        <a href="<?php echo base_url(); ?>Empleados/export" target="_blank" class="btn waves-effect waves-light purple lightrn-1" >Descargar Excel <i class="material-icons">file_download</i></a>
                      </div>
                  </div>

                  <div class="col s12">

                    <table id="tabla_empleados" class="responsive-table display" cellspacing="0">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Nombre</th>
                          <th>Apellido</th>
                          <th>Sexo</th>
                          <th>Email</th>
                          <th>Perfil</th>
                          <th>Tipo</th>
                          <th>Acciones</th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>  
               </div>
        </section>
