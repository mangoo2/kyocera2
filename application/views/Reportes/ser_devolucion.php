<?php 
	if(isset($_GET['viewexcel'])){
		$start_time = microtime(true);
	}else{
		header("Pragma: public");
		header("Expires: 0");
		$filename = "ventas.xls";
		header("Content-type: application/x-msdownload");
		header("Content-Disposition: attachment; filename=$filename");
		header("Pragma: no-cache");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	}
	
?>
<table border="1">
	<thead>
		<tr>
			<th>Tipo servicio</th>
			<th>Cliente</th>
			<th>Fecha</th>
			<th>Comentarios tecnico</th>
			<th>Tecnico</th>
			<th>Refaccion</th>
			<th>Motivo devoculcion</th>
			<th>Fecha devolucion</th>
		</tr>
	</thead>
	<tbody>
		<?php 
			$html='';
			$res_1_1 = $this->ModeloServicio->view_servicios_dev_rentas($fechainicio,$fechafin,1);
			$res_1_2 = $this->ModeloServicio->view_servicios_dev_rentas($fechainicio,$fechafin,2);
			$res_1_3 = $this->ModeloServicio->view_servicios_dev_rentas($fechainicio,$fechafin,3);
			
			$res_2_1 = $this->ModeloServicio->view_servicios_dev_polizas($fechainicio,$fechafin,1);
			$res_2_2 = $this->ModeloServicio->view_servicios_dev_polizas($fechainicio,$fechafin,2);
			$res_2_3 = $this->ModeloServicio->view_servicios_dev_polizas($fechainicio,$fechafin,3);
			
			$res_3_1 = $this->ModeloServicio->view_servicios_dev_cliente($fechainicio,$fechafin,1);
			$res_3_2 = $this->ModeloServicio->view_servicios_dev_cliente($fechainicio,$fechafin,2);
			$res_3_3 = $this->ModeloServicio->view_servicios_dev_cliente($fechainicio,$fechafin,3);
			
			$res_4_1 = $this->ModeloServicio->view_servicios_dev_venta($fechainicio,$fechafin,1);
			$res_4_2 = $this->ModeloServicio->view_servicios_dev_venta($fechainicio,$fechafin,2);
			$res_4_3 = $this->ModeloServicio->view_servicios_dev_venta($fechainicio,$fechafin,3);

			foreach ($res_1_1->result() as $item) {
				$html.='<tr>';
					$html.='<td class="1 '.$item->asignacionId.'">'.$item->poliza.'</td>';
					$html.='<td>'.$item->empresa.'</td>';
					$html.='<td>'.$item->fecha.'</td>';
					$html.='<td>'.$item->comentario_tec.'</td>';
					$html.='<td>'.$item->nombre.' '.$item->apellido_paterno.'</td>';
					$html.='<td>'.$item->refaccion.'</td>';
					$html.='<td>'.$item->motivo_dev.'</td>';
					$html.='<td>'.$item->reg_dev.'</td>';
				$html.='</tr>';
			}
			foreach ($res_1_2->result() as $item) {
				$html.='<tr>';
					$html.='<td class="1 '.$item->asignacionId.'">'.$item->poliza.'</td>';
					$html.='<td>'.$item->empresa.'</td>';
					$html.='<td>'.$item->fecha.'</td>';
					$html.='<td>'.$item->comentario_tec.'</td>';
					$html.='<td>'.$item->nombre.' '.$item->apellido_paterno.'</td>';
					$html.='<td>'.$item->refaccion.'</td>';
					$html.='<td>'.$item->motivo_dev.'</td>';
					$html.='<td>'.$item->reg_dev.'</td>';
				$html.='</tr>';
			}
			foreach ($res_1_3->result() as $item) {
				$html.='<tr>';
					$html.='<td class="1 '.$item->asignacionId.'">'.$item->poliza.'</td>';
					$html.='<td>'.$item->empresa.'</td>';
					$html.='<td>'.$item->fecha.'</td>';
					$html.='<td>'.$item->comentario_tec.'</td>';
					$html.='<td>'.$item->nombre.' '.$item->apellido_paterno.'</td>';
					$html.='<td>'.$item->refaccion.'</td>';
					$html.='<td>'.$item->motivo_dev.'</td>';
					$html.='<td>'.$item->reg_dev.'</td>';
				$html.='</tr>';
			}
			foreach ($res_2_1->result() as $item) {
				$html.='<tr>';
					$html.='<td class="2 '.$item->asignacionId.'">'.$item->poliza.'</td>';
					$html.='<td>'.$item->empresa.'</td>';
					$html.='<td>'.$item->fecha.'</td>';
					$html.='<td>'.$item->comentario_tec.'</td>';
					$html.='<td>'.$item->nombre.' '.$item->apellido_paterno.'</td>';
					$html.='<td>'.$item->refaccion.'</td>';
					$html.='<td>'.$item->motivo_dev.'</td>';
					$html.='<td>'.$item->reg_dev.'</td>';
				$html.='</tr>';
			}
			foreach ($res_2_2->result() as $item) {
				$html.='<tr>';
					$html.='<td class="2 '.$item->asignacionId.'">'.$item->poliza.'</td>';
					$html.='<td>'.$item->empresa.'</td>';
					$html.='<td>'.$item->fecha.'</td>';
					$html.='<td>'.$item->comentario_tec.'</td>';
					$html.='<td>'.$item->nombre.' '.$item->apellido_paterno.'</td>';
					$html.='<td>'.$item->refaccion.'</td>';
					$html.='<td>'.$item->motivo_dev.'</td>';
					$html.='<td>'.$item->reg_dev.'</td>';
				$html.='</tr>';
			}
			foreach ($res_2_3->result() as $item) {
				$html.='<tr>';
					$html.='<td class="2 '.$item->asignacionId.'">'.$item->poliza.'</td>';
					$html.='<td>'.$item->empresa.'</td>';
					$html.='<td>'.$item->fecha.'</td>';
					$html.='<td>'.$item->comentario_tec.'</td>';
					$html.='<td>'.$item->nombre.' '.$item->apellido_paterno.'</td>';
					$html.='<td>'.$item->refaccion.'</td>';
					$html.='<td>'.$item->motivo_dev.'</td>';
					$html.='<td>'.$item->reg_dev.'</td>';
				$html.='</tr>';
			}
			foreach ($res_3_1->result() as $item) {
				$html.='<tr>';
					$html.='<td class="3 '.$item->asignacionId.'">'.$item->poliza.'</td>';
					$html.='<td>'.$item->empresa.'</td>';
					$html.='<td>'.$item->fecha.'</td>';
					$html.='<td>'.$item->comentario_tec.'</td>';
					$html.='<td>'.$item->nombre.' '.$item->apellido_paterno.'</td>';
					$html.='<td>'.$item->refaccion.'</td>';
					$html.='<td>'.$item->motivo_dev.'</td>';
					$html.='<td>'.$item->reg_dev.'</td>';
				$html.='</tr>';
			}
			foreach ($res_3_2->result() as $item) {
				$html.='<tr>';
					$html.='<td class="3 '.$item->asignacionId.'">'.$item->poliza.'</td>';
					$html.='<td>'.$item->empresa.'</td>';
					$html.='<td>'.$item->fecha.'</td>';
					$html.='<td>'.$item->comentario_tec.'</td>';
					$html.='<td>'.$item->nombre.' '.$item->apellido_paterno.'</td>';
					$html.='<td>'.$item->refaccion.'</td>';
					$html.='<td>'.$item->motivo_dev.'</td>';
					$html.='<td>'.$item->reg_dev.'</td>';
				$html.='</tr>';
			}
			foreach ($res_3_3->result() as $item) {
				$html.='<tr>';
					$html.='<td class="3 '.$item->asignacionId.'">'.$item->poliza.'</td>';
					$html.='<td>'.$item->empresa.'</td>';
					$html.='<td>'.$item->fecha.'</td>';
					$html.='<td>'.$item->comentario_tec.'</td>';
					$html.='<td>'.$item->nombre.' '.$item->apellido_paterno.'</td>';
					$html.='<td>'.$item->refaccion.'</td>';
					$html.='<td>'.$item->motivo_dev.'</td>';
					$html.='<td>'.$item->reg_dev.'</td>';
				$html.='</tr>';
			}
			echo $html;
		?>
	</tbody>
</table>
<?php 
	if(isset($_GET['viewexcel'])){
		// Al final del script
		$end_time = microtime(true);

		// Calcula el tiempo de carga
		$load_time = $end_time - $start_time;

		echo "Tiempo de carga de la página: " . $load_time . " segundos.";
	}
	/*
		// Al final del script
	$end_time = microtime(true);

	// Calcula el tiempo de carga
	$load_time = $end_time - $start_time;

	echo "Tiempo de carga de la página: " . $load_time . " segundos.";
	*/
?>