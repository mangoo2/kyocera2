
var base_url = $('#base_url').val();
$(document).ready(function(){ 

    table = $('#tabla_con').DataTable();
    load();
}); 
function load() {
    // Destruye la tabla y la crea de nuevo con los datos cargados 
    var tecnico = $('#tecnico option:selected').val();
    var finicial=$('#finicial').val();
    var ffinal=$('#ffinal').val();
    table.destroy();
    table = $('#tabla_con').DataTable({
        pagingType: 'input',
        stateSave: true,
        responsive: !0,
        "bProcessing": true,
         search: {
                return: true
            }, 
        "serverSide": true,
        "ajax": {
            "url": base_url+"index.php/ControlFolios/getListadocontratofolio2",
            type: "post",
            "data": {
                
                "tecnico":tecnico,
                'fi':finicial,
                'ff':ffinal
                
            },
        },
        "columns": [
            {"data": "idcontratofolio",
                render:function(data,type,row){
                    var html=row.idcontratofolio;
                        html='<input type="checkbox" class="filled-in folios_checked" id="folios_'+row.idcontratofolio+'" value="'+row.idcontratofolio+'">\
                              <label for="folios_'+row.idcontratofolio+'">'+row.idcontratofolio+'</label>';
                    return html;
                }
            },
            {"data": "modelo"},
            {"data": "parte"},
            {"data": "nombre"},
            {"data": "fechagenera"},
            {"data": "empresa"},
            {"data": "tecnico"},
            {"data": "foliotext"},
            {"data": "estatus",
                render:function(data,type,row){
                    var html='';
                   if(row.estatus==0){
                        html+='<span style="font-weight: bold;color: red;" class="'+row.idrenta+'">Cancelado</span>';
                    }
                    return html; 
                }
            },
            {"data": "idVentas",
                render:function(data,type,row){
                    var html='';
                   if(row.idVentas>0){
                        html='<a class="btn-floating green tooltipped" data-position="top" data-delay="50" data-tooltip="Prefactura" onclick="detalle('+row.idVentas+',0)"><i class="material-icons">assignment</i></a>';
                        html+='PROINVEN: '+row.idVentas;
                    }else{
                        if(row.preren>0){
                            html='<a class="btn-floating amber tooltipped" data-position="top" data-delay="50" data-tooltip="Prefactura" onclick="prefacturah('+row.idrenta+','+row.preren+')"><i class="material-icons">assignment</i></a>';  
                            if(row.proinren>0){
                                html+='PROINREN: '+row.proinren;     
                            }else{
                                html+='PROINREN: ';  
                            }
                        }else{
                            html='<a class="btn-floating green tooltipped" data-position="top" data-delay="50" data-tooltip="Prefactura" onclick="detaller('+row.idrenta+')"><i class="material-icons">assignment</i></a>';
                        }
                        
                    }
                    return html; 
                }
            },
            {"data": "idcontratofolio",      
                 render:function(data,type,row){
                    var html='<div style="width:143px">';
                    html+='<button class="btn-bmz waves-effect waves-light facturar" type="button" onclick="modaletiqueta('+row.idcontratofolio+')" >Etiqueta\
                    </button>';
                    html+=' <button class="btn-bmz waves-effect waves-light facturar" \
                            type="button" \
                            onclick="cambiarstatus('+row.idcontratofolio+')"  style="padding: 3px;" title="Cambiar status">\
                                <span class="fa-stack fa-1x">\
                                    <i class="fa fa-print fa-stack-1x"></i>\
                                    <i class="fa fa-check fa-stack-1x" style="color:green;margin-left: 7px;"></i>\
                                </span>\
                            </button>';
                    html+='</div>';
                    
                    return html;
                }
            }
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[25, 50, 100], [25, 50, 100]],
        
    }).on('draw',function(){
        $('.dataTables_filter').append('<i class="fa fa-search buttonsearch" onclick="searchtable_v()"></i>')

    });
}
function searchtable_v(){
    var contenedor = document.getElementById('tabla_con_filter');

    // Selecciona el input dentro del contenedor
    var input = contenedor.querySelector('input.form-control-bmz');

    // Obtén el valor del input
    var searchv = input.value;

    // Muestra el valor en la consola
    console.log(searchv);
    //var searchv =$("input[type=search]").val();
    table.search(searchv).draw();
}

function modaletiqueta(id){
    $('.modal').modal({
            dismissible: true
        });
    $('#modalAcciones').modal('open');
    $('#idcontratofolio').val(id);
    $('.iframe_etiqueta').html('<iframe  style="height: 350px; width: 700px; border:0px;" id="imprimiriframe" src="'+base_url+'index.php/ControlFolios/viewetiqueta/'+id+'"></iframe>');
}
function modaletiquetas(){
    $('.modal').modal({
            dismissible: true
        });
    $('#modalAccionesn').modal('open');
    var facturaslis = $("#tabla_con tbody > tr");
    var DATAa  = [];
        facturaslis.each(function(){  
            if ($(this).find("input[class*='folios_checked']").is(':checked')) {
                item = {};                    
                item ["folio"]  = $(this).find("input[class*='folios_checked']").val();
                DATAa.push(item);
            }       
        });
        INFOa  = new FormData();
        aInfoa   = JSON.stringify(DATAa);



    $('#idscontratofolio').val(aInfoa);
    var urlpdf=base_url+"index.php/ControlFolios/viewetiquetas?etique="+aInfoa;
    var htmliframe="<iframe  style='height: 350px; width: 700px; border:0px;' id='imprimiriframe' src='"+urlpdf+"'></iframe>";
    console.log(urlpdf);
     console.log(htmliframe);
    $('.iframe_etiquetan').html(htmliframe);
}
function statusetiquetan(){
    folios = $('#idscontratofolio').val();
    $.ajax({
        type:'POST',
        url: base_url+"index.php/ControlFolios/updatstatusfolios",
        data: {idsfolio:folios},
        success:function(data){   
            load();
        }
    });
    document.getElementById("imprimiriframe").contentWindow.print();
}
function statusetiqueta(){
    idcontrato = $('#idcontratofolio').val();
    $.ajax({
        type:'POST',
        url: base_url+"index.php/ControlFolios/updatstatusfolio",
        data: {id:idcontrato},
        success:function(data){ 
            load();  
        }
    });
    document.getElementById("imprimiriframe").contentWindow.print();
    /// imprimir etiqueta
}
function modalretorno(){
    $('.modal').modal({
            dismissible: true
        });
    $('#modalRetorno').modal('open');
}
function modaladdservi(idcontrato,idfolio){
    $('.modal').modal({
            dismissible: true
        });
    $('#modalservicios').modal('open');
    $.ajax({
        type:'POST',
        url: base_url+"NewFolio/cargaequipos",
        data: {
            contrato:idcontrato
        },
        success: function (data){
            console.log(data);
            var array = $.parseJSON(data);
            $('#servicioadd').html(array.servicio);
        }
    });
    $('#idcf').val(idfolio);
}
function addserviciotoner(){
    $.ajax({
        type:'POST',
        url: base_url+"NewFolio/addserviciotoner",
        data: {
            idfolio:$('#idcf').val(),
            idservicio:$('#servicioadd option:selected').val(),
        },
        success: function (data){
            $('#modalservicios').modal('close');
            load();
        }
    });
}
function cambiarstatus(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Desea confirmar que ya se encuentra impresa?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                
                     $.ajax({
                        type:'POST',
                        url: base_url+"index.php/ControlFolios/updatstatusfolio",
                        data: {
                            id:id
                        },
                        success: function (response){
                            toastr["success"]("Se cambio el estatus de la etiqueta", "Hecho!"); 
                            load(); 
                        },
                        error: function(response){
                            toastr["error"]("No se a podido procesar", "Alerta!");  
                             
                        }
                    });
                    
               
                
            },
            cancelar: function () 
            {
                
            }
        }
    });
}
toastr.options = {
  "closeButton": true,
  "debug": false,
  "newestOnTop": false,
  "progressBar": true,
  "positionClass": "toast-top-right",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "4000",
  "timeOut": "5000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}
function detalle(id,tipo){
    if (tipo==0) {
        window.open(base_url+"Prefactura/view/"+id, "Prefactura", "width=780, height=612");
    }else{
        window.open(base_url+"Prefactura/viewc/"+id, "Prefactura", "width=780, height=612");
    }
}
function prefacturah(idrenta,idhistorial){
    window.open(base_url+"RentasCompletadas/viewh/"+idrenta+'/'+idhistorial, "Prefactura", "width=780, height=612");
}
function detaller(id){
    window.open(base_url+"RentasCompletadas/view/"+id, "Prefactura", "width=780, height=612");
}