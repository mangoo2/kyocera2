<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Productos extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloCatalogos');
        $this->submenu=14;
        if($this->session->userdata('logeado')==true){
            $this->perfilid =$this->session->userdata('perfilid');
            $this->idpersonal = $this->session->userdata('idpersonal');
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,$this->submenu);// 52 es el id del submenu
            if ($permiso==0) {
                redirect('Sistema');
            }
        }else{
            redirect('Sistema'); 
        }
    }

    public function index(){
        $data['MenusubId']=$this->submenu;
    	$this->load->view('header');
        $this->load->view('main',$data);
        $this->load->view('Reportes/Productos');
        $this->load->view('footer');
        $this->load->view('Reportes/Productosjs');
    }
    function exportar($fechaini,$fechafin,$tipo,$cliente){
        $data['fechaini']=$fechaini;
        $data['fechafin']=$fechafin;
        $data['tipo']=$tipo;
        $data['cliente']=$cliente;

        $this->load->view('Reportes/productosexport',$data);
    }
    function entradassalidas(){
        $params=$this->input->get();
        if(isset($params['fechainicio'])){
            $fechaini=$params['fechainicio'];
        }else{
            $fechaini='';    
        }

        if(isset($params['fechafinal'])){
            $fechafin=$params['fechafinal'];
        }else{
            $fechafin='';    
        }
        $tipo=$params['tipo'];
        $producto=$params['producto'];
        $tipopgg=$params['tipopgg'];
        
        header("Pragma: public");
        header("Expires: 0");
        $filename = "Entradas salidas.xls";
        header("Content-type: application/x-msdownload");
        header("Content-Disposition: attachment; filename=$filename");
        header("Pragma: no-cache");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        
        
        $html='';
        if($tipopgg==0){
            $result = $this->ModeloCatalogos->get_reporte_entradas_salidas($fechaini,$fechafin,$tipo,$producto);




            $html='<table border="1">
            <thead>
                        <tr>
                            <th>Entrada/salida</th>
                            <th>Tipo</th>
                            <th>Usuario</th>
                            <th>Producto</th>
                            <th>No Parte</th>
                            <th>Item</th>
                            <th>Serie</th>
                            <th>Bodega origen</th>
                            <th>bodega Destino</th>
                            <th>Cantidad</th>
                            <th>Cliente</th>
                            <th>Movimiento</th>
                        </tr></thead><tbody>';
                    foreach ($result->result() as $item) {
                        if($item->entrada_salida==0){
                            $entrada_salida='Salida';
                        }else{
                            $entrada_salida='Entrada';
                        }
                        $producto='';
                        if($item->tipo==1){
                            $producto='Equipos';
                            $serie=$item->serie;
                        }
                        if($item->tipo==2){
                            $producto='Accesorios';
                            if($item->serie==null){
                                $serie=$item->modeloext;
                            }else{
                                $serie=$item->serie;
                            }
                            
                        }
                        if($item->tipo==3){
                            $producto='Refacciones';
                            $serie=$item->serie;
                        }
                        if($item->tipo==4){
                            $producto='Consumibles';
                            $serie=$item->serie;
                        }

                        $html.='<tr>
                            <td>'.$entrada_salida.'</td>
                            <td>'.$item->descripcion_evento.'</td>
                            <td>'.utf8_decode($item->nombre.' '.$item->apellido_paterno.' '.$item->apellido_materno).'</td>
                            <td>'.$producto.'</td>
                            <td>'.$item->noparte.'</td>
                            <td>'.$item->modelo.'</td>
                            <td>'.$serie.'</td>
                            <td>'.utf8_decode($item->bodegao).'</td>
                            <td>'.utf8_decode($item->bodegad).'</td>
                            <td>'.$item->cantidad.'</td>
                            <td>'.utf8_decode($item->empresa).'</td>
                            <td>'.$item->reg.'</td>
                        </tr>';
                    }

            $html.='</tbody><table>';
        }else{
            $html='<table border="1">
            <thead>
                        <tr>
                            <th>Tipo</th>
                            <th>No Parte</th>
                            <th>Item</th>
                            <th>Inicial</th>
                            <th>Entrada</th>
                            <th>Salida</th>
                            <th>Final</th>
                            
                        </tr></thead><tbody>';
            if($tipo==1 || $tipo==0){//equipo
                $result = $this->ModeloCatalogos->reg_entrada_salida_g_eq($fechaini,$fechafin,$producto);
                foreach ($result->result() as $item) {
                    $final=$item->stockinicial+$item->cantidadentrada-$item->cantidadsalida;
                    $html.='<tr>
                            <td>Equipo</td>
                            <td>'.$item->noparte.'</td>
                            <td>'.$item->modelo.'</td>
                            <td>'.$item->stockinicial.'</td>
                            <td>'.$item->cantidadentrada.'</td>
                            <td>'.$item->cantidadsalida.'</td>
                            <td>'.$final.'</td>
                        </tr>';
                }
            }
            if($tipo==2 || $tipo==0){//Refacciones
                $result = $this->ModeloCatalogos->reg_entrada_salida_g_ref($fechaini,$fechafin,$producto);
                foreach ($result->result() as $item) {
                    $final=$item->stockinicial+$item->cantidadentrada-$item->cantidadsalida;
                    $html.='<tr>
                            <td>Refacciones</td>
                            <td>'.$item->codigo.'</td>
                            <td>'.$item->nombre.'</td>
                            <td>'.$item->stockinicial.'</td>
                            <td>'.$item->cantidadentrada.'</td>
                            <td>'.$item->cantidadsalida.'</td>
                            <td>'.$final.'</td>
                        </tr>';
                }
            }
            if($tipo==3 || $tipo==0){//Accesorios
                $result = $this->ModeloCatalogos->reg_entrada_salida_g_acc($fechaini,$fechafin,$producto);
                foreach ($result->result() as $item) {
                    $final=$item->stockinicial+$item->cantidadentrada-$item->cantidadsalida;
                    $html.='<tr>
                            <td>Accesorios</td>
                            <td>'.$item->no_parte.'</td>
                            <td>'.$item->nombre.'</td>
                            <td>'.$item->stockinicial.'</td>
                            <td>'.$item->cantidadentrada.'</td>
                            <td>'.$item->cantidadsalida.'</td>
                            <td>'.$final.'</td>
                        </tr>';
                }
            }
            if($tipo==4 || $tipo==0){//consumibles
                $result = $this->ModeloCatalogos->reg_entrada_salida_g_cons($fechaini,$fechafin,$producto);
                foreach ($result->result() as $item) {
                    $final=$item->stockinicial+$item->cantidadentrada-$item->cantidadsalida;
                    $html.='<tr>
                            <td>Consumibles</td>
                            <td>'.$item->parte.'</td>
                            <td>'.$item->modelo.'</td>
                            <td>'.$item->stockinicial.'</td>
                            <td>'.$item->cantidadentrada.'</td>
                            <td>'.$item->cantidadsalida.'</td>
                            <td>'.$final.'</td>
                        </tr>';
                }
            }

            $html.='</tbody><table>';
        }
        echo $html;
        
    }



    

}
?>