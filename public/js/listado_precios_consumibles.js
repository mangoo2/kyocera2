var base_url = $('#base_url').val();
var perfilid = $('#perfilid').val();
var editor; 

$(document).ready(function(){

	if(perfilid==1)
    {
        editor = new $.fn.dataTable.Editor( {
            ajax: base_url+"index.php/Listapreciosconsumibles/actualizaPrecio",
            table: "#tabla_precios",
            fields: [ {
                    label: "Precio Dólares:",
                    name: "precioDolares"
                },
                {
                    label: "Costo general:",
                    name: "general"
                },  {
                    label: "Iva General:",
                    name: "iva"
                },  {
                    label: "Costo Neto General:",
                    name: "neto"
                },  {
                    label: "Costo Frecuente:",
                    name: "frecuente"
                }, {
                    label: "Iva Frecuente:",
                    name: "iva2"
                }, {
                    label: "Costo Neto Frecuente:",
                    name: "neto2"
                },  {
                    label: "Costo Especial:",
                    name: "especial"
                }, {
                    label: "Iva Especial:",
                    name: "iva3"
                }, {
                    label: "Costo Neto Especial:",
                    name: "neto3"
                },  {
                    label: "Costo Póliza:",
                    name: "poliza"
                }, {
                    label: "Iva Especial:",
                    name: "iva4"
                }, {
                    label: "Costo Neto Especial:",
                    name: "neto4"
                },

            ]
        } );
    }    

    // Activa la edición por casilla en cada ROW de la tabla
    $('#tabla_precios').on( 'click', 'tbody td:not(:first-child)', function (e){
        editor.inline(this);
    });

    table = $('#tabla_precios').DataTable();
    load();
});


function load() {
    // Destruye la tabla y la crea de nuevo con los datos cargados 
    table.destroy();
    table = $('#tabla_precios').DataTable({
        dom: 'Blfrtip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'pdfHtml5',
        ],
        "ajax": {
            "url": base_url+"index.php/Listapreciosconsumibles/getListaPreciosConsumibles"
        },
        "columns": [
            {"data": "DT_RowId"},
            {"data": "modelo"},
            {"data": "precioDolares"},
            {"data": "general"},
            {"data": "iva", visible: false},
            {"data": "neto", visible: false},
            {"data": "frecuente"},
            {"data": "iva2", visible: false},
            {"data": "neto2", visible: false},
            {"data": "especial"},
            {"data": "iva3", visible: false},
            {"data": "neto3", visible: false},
            {"data": "poliza"},
            {"data": "iva4", visible: false},
            {"data": "neto4", visible: false}
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[25, 50, 100], [25, 50, 100]],
        
        // Cambiamos lo principal a Español
        "language": {
            "lengthMenu": "Desplegando _MENU_ elementos por página",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
              }
        }
    });
}

