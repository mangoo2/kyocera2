<script type="text/javascript" src="<?php echo base_url().'public/js/servicio_g.js?v='.date('YmdGis');?>"></script>
<style type="text/css">
  .t_title{font-weight: bold;text-align: center;background: #d0d0d0;}
  @media only screen and (max-width: 500px){
    .modal{width: 99%;top: 2% !important;max-height: 93%;}
    .modal-fixed-footer{height: 93% !important;    }
    .info_servicios_report table td {/*font-size: 12px;*/padding: 4px 5px ;}
    .info_servicios_report{
      padding: 0 5px !important;
    }
  }
  .qvalid{
    margin-left: 20px;
  }
  .colordeiconofinalizado{
    color: #80d580;
    font-size: 20px;
  }
</style>
<div id="modalser_report" class="modal modal-fixed-footer" >
  <div class="modal-content ">
    <h5>Reporte</h5>
    <div class="row ">
      <div class="col s12 info_servicios_report"></div>
    </div>
  </div>
  <div class="modal-footer">
    <button class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</button>
  </div>
</div>