<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class Facturacion_model extends CI_Model 
{
    public function __construct() 
    {
        parent::__construct();
    }

    public function getfacturasPorCliente($idCliente)
    {
    	$sql = "SELECT
                r.*,
                re.id as 'idDetalle',
                re.idEquipo,
                re.modelo,
                re.especificaciones_tecnicas,
                re.precio,
                re.costo_toner_black,
                re.rendimiento_toner_black,
                re.costo_toner_cmy,
                re.rendimiento_toner_cmy,
                re.costo_unidad_imagen,
                re.rendimiento_unidad_imagen,
                re.costo_garantia_servicio,
                re.rendimiento_garantia_servicio,
                re.costo_pagina_monocromatica as 'costoporclick',
                re.costo_pagina_color,
                re.excedente
                FROM rentas as r
                INNER JOIN rentas_has_detallesEquipos as re ON re.idRenta=r.id WHERE r.idCliente=".$idCliente;
        $query = $this->db->query($sql);
        return $query->result();
    } 

}