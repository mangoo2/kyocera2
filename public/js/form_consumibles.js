// Saber si es solo Visualización
var tipoVista = $("#tipoVista").val();
var base_url = $('#base_url').val();
$(document).ready(function(){
    var base_url = $('#base_url').val();
    var formulario_consumibles = $('#consumibles_form');
    $('.chosen-select').chosen({width: "90%"});
    // En caso de que sea solo visualizar, deshabilita todo
   
    if (tipoVista == 2) {
        var ConsumibleID = $('#idConsumible').val(); 
        loadTabla_has_consumibles(ConsumibleID);
    }
    else if(tipoVista == 3)
    {   var ConsumibleID = $('#idConsumible').val(); 
        loadTabla_has_consumibles(ConsumibleID);
        $("input").prop('disabled', true);
        $("textarea").prop('disabled', true);
        $("select").prop('disabled', true);
        $("checkbox").prop('disabled', true);
        $("button").prop('disabled', true);
        $(".btn_agregar").attr('style', 'pointer-events: none;');
    }

     formulario_consumibles.validate({
            rules: 
            {
                
                modelo: {
                    required: true
                },
                parte: {
                    required: true
                },
                stock: {
                    required: true
                },
                general: {
                    required: true
                },
                iva: {
                    required: true
                },
                neto: {
                    required: true
                },
                frecuente: {
                    required: true
                },
                iva2: {
                    required: true
                },
                neto2: {
                    required: true
                },
                especial: {
                    required: true
                },
                iva3: {
                    required: true
                },
                neto3: {
                    required: true
                },
                poliza: {
                    required: true
                },
                iva4: {
                    required: true
                },
                neto4: {
                    required: true
                },
                rendimiento_toner:{
                    required: true
                },
                rendimiento_unidad_imagen:{
                    required: true
                }
            
            },
            // Para mensajes personalizados
            messages: 
            {
                 
                modelo:{
                    required: "Ingrese un codigo"
                },
                parte:{
                    required: "Ingrese un nombre"
                },
                stock:{
                    required: "Ingrese una categoria"
                },
                general:{
                    required: "Ingrese un observación"
                },
                iva:{
                    required: "Seleccione un precio USA"
                },
                neto:{
                    required: "Ingrese un precio unitario"
                },
                frecuente:{
                    required: "Ingrese un precio total"
                },
                iva2:{
                    required: "Ingrese un codigo"
                },
                neto2:{
                    required: "Ingrese un nombre"
                },
                especial:{
                    required: "Ingrese una categoria"
                },
                iva3:{
                    required: "Ingrese un stock"
                },
                neto3:{
                    required: "Ingrese un observación"
                },
                poliza:{
                    required: "Seleccione un precio USA"
                },
                iva4:{
                    required: "Ingrese un precio unitario"
                },
                neto4:{
                    required: "Ingrese un precio total"
                }
            
            },
        errorElement : 'div',
            // Si están correctos los campos, procesamos el formulario mediante Ajax
        submitHandler: function (form) 
        {
        
            var DATA  = [];
                    var TABLA   = $(".border");
                        TABLA.each(function(){         
                             item = {};
                             item ["idEquipo"]   = $(this).find("select[id*='equipos']").val();
                             DATA.push(item);
                        });
            equipos   = JSON.stringify(DATA);
           
            
            var datos = formulario_consumibles.serialize()+'&equipos2='+equipos;
            $.ajax({
                type:'POST',
                url: base_url+'index.php/consumibles/insertaActualizaConsumibles',
                data: datos,
                success:function(data)
                {
                    if(data=='existe'){
                        swal({ title: "Error",
                            text: "Compatibilidad de impresoras ya existe, ingrese otro diferente.",
                            type: 'error',
                            showCancelButton: false,
                            allowOutsideClick: false,
                        });
                    } 
                    // Si existe el usuario, mostramos el mensaje de entrada y redirigimos
                    else{
                        cargaimage(data);
                        swal({ title: "Éxito",
                            text: "Se insertó el consumible",
                            type: 'success',
                            showCancelButton: false,
                            allowOutsideClick: false,
                        });
                        
                        setTimeout(function(){ 
                          window.location.href = base_url+"index.php/consumibles"; 
                        }, 2000);
                    
                    }
                }
            });
            
        }
     });

    
        // Basic
    $('.dropify').dropify({
        messages: {
            default: 'Arrastra y suelta un archivo aquí o haz clic',
            replace: 'Arrastre y suelte un archivo o haga clic para reemplazar',
            remove:  'Eliminar',
            error:  'Lo sentimos, el archivo es demasiado grande.'
        }
    });
    

    // Used events
    var drEvent = $('.dropify-event').dropify();

    drEvent.on('dropify.beforeClear', function(event, element){
        return confirm("Do you really want to delete \"" + element.filename + "\" ?");
    });

    drEvent.on('dropify.afterClear', function(event, element){
        alert('File deleted');
    });
    
     $(".btn_agregar").click(function(){
        $('.chosen-select').chosen('destroy');
      var contador = $('.border').length; 
      var contadoraux = contador+1; 
      var $template = $("#datos1"),
      $clone = $template
     .clone()
     .show()
     .removeAttr('id')
     .attr('id',"datos"+contadoraux)
     .insertAfter($("#datos"+contador));

     $clone.find($('#categoria'));
     $clone.find(".div_btn").html('<a class="btn btn-floating waves-effect waves-light red btn_eliminar"><i class="material-icons">delete_forever</i></a>');
     $clone.find("select").val("");
     $clone.find("input").val("");
     eliminar();
     $('.chosen-select').chosen({width: "90%"});

    });

           // Listener para Eliminar 
    $('#tabla_has_consumible tbody').on('click', 'a.elimina', function () 
    {
        var tr = $(this).closest('tr');
        var row = table3.row(tr);
        var data = row.data();
        confirmaEliminarEquipo(data.id);
    });

});
function eliminar(){
    $(".btn_eliminar").click(function(){
                    var $row = $(this).parents('.border');
   
                    // Remove element containing the option
                    $row.remove();

    });
}


function confirmaEliminarEquipo(idAccesorio)
{
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de Eliminar este Equipo? El cambio será irreversible',
        type: 'red',
        typeAnimated: true,
        buttons: 
        {
            confirmar: function () 
            {
                $.ajax({
                    url: base_url+"index.php/consumibles/eliminar_has_consumibles/"+idAccesorio,
                    success: function (response) 
                    {

                        if(response==1)
                        {
                            $.alert({
                                boxWidth: '30%',
                                useBootstrap: false,
                                title: 'Éxito!',
                                content: 'Equipo Eliminado!'});
                            location.reload();
                        }
                        else
                        {
                            $.alert({
                                boxWidth: '30%',
                                useBootstrap: false,
                                title: 'Error!',
                                content: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema'}); 
                             
                        }
                    },
                    error: function(response)
                    {
                        $.alert({
                                boxWidth: '30%',
                                useBootstrap: false,
                                title: 'Error!',
                                content: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema'});   
                        
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}
function loadTabla_has_consumibles(id) 
{   
    table3 = $('#tabla_has_consumible').DataTable({
            "ajax":{ "url": base_url+"index.php/Consumibles/getListadoHas_consumibles/"+id,
        },
        "columns": [
          {"data": "id"},
          {"data": "modelo"},
          {"data": "id", visible: false},
          {"data":null,title:"Acciones",orderable:false,class:"centrado_text",
                render:function(data,type,row){
                    $('.tooltipped').tooltip();
                    if(tipoVista == 3){
                        var html='<ul>\
                                    <li>\
                                      <a class="btn-floating red soloadministradores"  data-delay="50">\
                                        <i class="material-icons">delete_forever</i>\
                                      </a>\
                                    </li>\
                                  </ul>';
                        return html;
                        }else{
                        var html='<ul>\
                                    <li>\
                                      <a class="btn-floating red tooltipped elimina soloadministradores" data-position="top" data-delay="50" data-tooltip="Eliminar">\
                                        <i class="material-icons">delete_forever</i>\
                                      </a>\
                                    </li>\
                                  </ul>';
                        return html;    
                        }
                }
            }
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[25, 50, 100], [25, 50, 100]],
        "dom": 'Bfrtpl',
        // Muestra el botón de Excel para importar la tabla
        "buttons": [
            /*{   
                extend: 'excelHtml5',
                text: ' Descargar Excel <i class="fa fa-download"></i>',
                className: 'btn classBotonFratsa'
            }
            */
        ],
        // Cambiamos lo principal a Español
        "language": {
            "lengthMenu": "Desplegando _MENU_ elementos por página",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
              }
        }
    });
}

/*
function loadTabla_has_consumibles(id) 
{   

        table3 = $('#tabla_has_consumible').DataTable({
                "ajax":{ "url": base_url+"index.php/Consumibles/getListadoHas_consumibles/"+id,
            },
            "columns": [
              {"data": "id"},
              {"data": "modelo"},
              {"data": "idConsumibles", visible: false},
              {"data":null,title:"Acciones",orderable:false,class:"centrado_text",
                    render:function(data,type,row){
                        $('.tooltipped').tooltip();
                        if(tipoVista == 3){
                        var html='<ul>\
                                    <li>\
                                      <a class="btn-floating red"  data-delay="50">\
                                        <i class="material-icons">delete_forever</i>\
                                      </a>\
                                    </li>\
                                  </ul>';
                        return html;
                        }else{
                        var html='<ul>\
                                    <li>\
                                      <a class="btn-floating red tooltipped elimina" data-position="top" data-delay="50" data-tooltip="Eliminar">\
                                        <i class="material-icons">delete_forever</i>\
                                      </a>\
                                    </li>\
                                  </ul>';
                        return html;    
                        }
                    } 
                }
            ],
            "order": [[ 0, "desc" ]],
            "lengthMenu": [[25, 50, 100], [25, 50, 100]],
            "dom": 'Brtpl',
            // Muestra el botón de Excel para importar la tabla
            "buttons": [
                {   
                    extend: 'excelHtml5',
                    text: ' Descargar Excel <i class="fa fa-download"></i>',
                    className: 'btn classBotonFratsa'
                }
            ],
            // Cambiamos lo principal a Español
            "language": {
                "lengthMenu": "Desplegando _MENU_ elementos por página",
                "zeroRecords": "Lo sentimos - No se han encontrado elementos",
                "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
                "info": "Mostrando página _PAGE_ de _PAGES_",
                "infoEmpty": "No hay registros disponibles",
                "infoFiltered": "(Filtrado de _MAX_ registros totales)",
                "search": "Buscar : _INPUT_",
                "paginate": {
                    "previous": "Página previa",
                    "next": "Siguiente página"
                  }
            }
        });
}
*/
function cargaimage(id){
    if ($('#foto')[0].files.length > 0) {
                var inputFileImage = document.getElementById('foto');
                var file = inputFileImage.files[0];
                var data = new FormData();
                data.append('foto',file);
                data.append('consumibles',id);
                $.ajax({
                    url:base_url+'index.php/Consumibles/cargafiles',
                    type:'POST',
                    contentType:false,
                    data:data,
                    processData:false,
                    cache:false,
                    success: function(data) {
                        var array = $.parseJSON(data);
                            console.log(array.ok);
                            if (array.ok=true) {
                                //$(".fileinput").fileinput("clear");
                               // toastr.success('Se ha cargado el nuevo documento correctamente','Hecho!');
                                //cargafiles();
                            }else{
                               // toastr.error('Error', data.msg);
                            }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                            var data = JSON.parse(jqXHR.responseText);
                            //toastr.error('Error', data.msg);
                        }
                });
            }
}
function validarequipo(){

    $.ajax({
        type:'POST',
        url: base_url+'index.php/Consumibles/verificarequipo',
        data: {datos:$('#equipos').val(),idconsu:$('#idConsumible').val()},
        success:function(data)
        {
            if(data == 1){
                swal({ title: "Error",
                    text: "Compatibilidad de impresoras ya existe, ingrese otro diferente.",
                    type: 'error',
                    showCancelButton: false,
                    allowOutsideClick: false,
                });
            }
        }
    });
}