<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/plugins/toastr/toastr.css">
<script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/toastr/toastr.min.js" ></script>

<link href="<?php echo base_url(); ?>public/plugins/select2/select2.min.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/select2/select2.full.min.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/fileinput/fileinput.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/plugins/fileinput/fileinput.css">

<script type="text/javascript" src="<?php echo base_url(); ?>public/js/operaciones/contrato.js?v=<?php echo date('YmdGi');?>" ></script>
<script type="text/javascript" src="<?php echo base_url(); ?>public/js/notificacion.js?v=<?php echo date('Ymd');?>"></script>
<script type="text/javascript">
	$(document).ready(function($) {
		<?php if($idcontrato>0){ ?>
			$('#collap_equipos').click();
		<?php }else{ ?>
			$('.collapsible-header').click();
		<?php } ?>
		
	});
</script>