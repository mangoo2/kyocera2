var base_url = $('#base_url').val();
var tablef;
$(document).ready(function() {	
	tablef = $('#tabla_chats').DataTable();
	loadtable();
    $('#idcliente').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una opción',
        allowClear: true,
        ajax: {
            url: base_url+'Ventasi/searchclientes',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    var html='';
                    if(element.empresa==element.razon_social){
                        html=element.empresa;
                    }else{
                        html=element.empresa+' ('+element.razon_social+')';
                    }
                    itemscli.push({
                        id: element.id,
                        text: html

                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function (e) {
        var data = e.params.data;
        //obtenerrfc(data.id)
        //loadtable();
    });
});
function loadtable(){
    var idcliente=$('#idcliente option:selected').val()==undefined?0:$('#idcliente option:selected').val();
    var eje=$('#eje option:selected').val()==undefined?0:$('#eje option:selected').val();

	tablef.destroy();
	tablef = $('#tabla_chats').DataTable({
        
        search: {
                return: true
            }, 
        //stateSave: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url+"index.php/Rchat/getlistchat",
            type: "post",
            "data": {
                'personal':eje,
                'cliente':idcliente
            },
        },
        "columns": [
        	{"data": "id"},
        	{"data": "reg"},
        	{"data": "clientename",
                render:function(data,type,row){
                    var html=row.clientename;
                        if(row.tipo==0){
                            html+='<button type="button" class="b-btn b-btn-primary btn-sm info_clipro info_cli_pro_'+row.id+'" onclick="view_prospecto('+row.id+')" ';
                            html+=' data-nom="'+row.nombre+'" ';
                            html+=' data-mail="'+row.correo+'" ';
                            html+=' data-tel="'+row.telefono+'" ';
                            html+=' ><i class="fas fa-eye"></i></button>';
                        }
                    return html;
                }
            },
            {"data": "personal",
            	render:function(data,type,row){
            		var html='';
            			if(row.personal==null){
            				html='';
            			}else{
            				html=row.personal+' '+row.apellido_paterno+' '+row.apellido_materno;
            			}
                    	
                    
                    return html;
            	}
        	},
            {"data": "departamento",
                render:function(data,type,row){
                    var html ='';
                        var dep=row.departamento;
                        if(dep==1){
                        	html='Venta';
                        }
                        if(dep==2){
                        	html='Renta';
                        }
                        if(dep==3){
                        	html='Servicio';//poliza
                        }
                        if(dep==4){
                        	html='Servicio Tecnico';
                        }
                    return html;
                }
            },
            {"data": null,
                render:function(data,type,row){
                    var html='';
                    	if(row.status==0){
                    		html+='<button type="button" class="b-btn b-btn-secondary btn-sm ">Solicitud</button>';
                    	}
                    	if(row.status==1){
                    		html+='<button type="button" class="b-btn b-btn-primary btn-sm ">En proceso</button>';
                    	}
                    	if(row.status==2){
                    		html+='<button type="button" class="b-btn b-btn-success btn-sm ">Finalizado</button>';
                    	}
                    	
                    return html;
                }
            },
            {"data": null,
                render:function(data,type,row){
                    var html='';
                    if(row.status>0){
                    		html+='<button type="button" class="b-btn b-btn-danger btn-sm " onclick="ver('+row.id+')">Ver chat</button>';
                    	}
                    return html;
                }
            }
        ],
        "order": [[ 0, "desc" ]],
        //"lengthMenu": [[25, 50, 100,200,400], [25, 50,100,200,400]],
        //"searching": false
        //columnDefs: [ { orderable: false, targets: [8,9] }],
        
    }).on('draw',function(){
        
    });
}
function ver(id){
	window.open(base_url+"Rchat/viewchat/"+id, "Prefactura", "width=330, height=580");
}
function view_prospecto(id){
    var nom = $('.info_clipro').data('nom');
    var mail = $('.info_clipro').data('mail');
    var tel = $('.info_clipro').data('tel');

    var html='';
        html+='<div class="row">';
            html+='<div class="col s4">Cliente:</div>';
            html+='<div class="col s8 " style="color:red">'+nom+'</div>';

            html+='<div class="col s4">Telefono</div>';
            html+='<div class="col s8 " style="color:red">'+tel+'</div>';

            html+='<div class="col s4">Correo</div>';
            html+='<div class="col s8 " style="color:red">'+mail+'</div>';
        html+='</div>';
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Información del prospecto',
        content: html,
        type: 'red',
        typeAnimated: true,
        buttons:{
            
            cerrar: function () 
            {
                
            }
        }
    });
}