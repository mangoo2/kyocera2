var base_url = $('#base_url').val();
var tablef;
$(document).ready(function($) {
	tablef = $('#tabla_amo').DataTable();
	loadtable();
});
function loadtable(){
    var eje=$('#eje option:selected').val();
    var fecha=$('#fecha').val();

	tablef.destroy();
	tablef = $('#tabla_amo').DataTable({
        autoWidth: true,
        dom: 'Bfrtip',
    buttons: [
        {
            extend: 'excelHtml5',
            text: 'Exportar a Excel',
            action: function (e, dt, button, config) {
                if(fecha==''){
                    fecha=0;
                }
                var urldir=base_url+'index.php/R_amonestaciones/exportar/'+eje+'/'+fecha
                window.open(urldir, '_blank');
                // Deshabilitar botón mientras se carga la data
                //button.disable();

                // Realizar una solicitud AJAX para obtener todos los datos
                /*
                $.ajax({
                    url: base_url + "index.php/R_amonestaciones/exportar",
                    type: "post",
                    data: {
                        eje: eje,
                        fecha: fecha,
                    },
                    success: function (data) {
                        console.log(data);
                        // Crear un DataTable temporal con todos los datos
                        var tempTable = $('#tempTable').DataTable({
                            data: data.data,
                            columns: dt.settings().init().columns,
                            destroy: true,
                            paging: false
                        });

                        // Usar el DataTable temporal para exportar
                        $.fn.dataTable.ext.buttons.excelHtml5.action.call(this, e, tempTable, button, config);

                        // Destruir la tabla temporal después de la exportación
                        tempTable.destroy();
                        button.enable(); // Habilitar botón nuevamente
                    }
                });
                */
            }
        }
    ],
        //stateSave: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url+"index.php/R_amonestaciones/getlistamo",
            type: "post",
            "data": {
                'eje':eje,
                'fecha':fecha
            },
        },
        "columns": [
            {"data": "id",
            	render:function(data,type,row){
            		var html=row.fecha_reg;
                    return html;
            	}
        	},
            {"data": "idpersonal",
                render:function(data,type,row){
                    var html ='';
                        html=row.nombre+' '+row.apellido_paterno+' '+row.apellido_materno;
                    return html;
                }
            },
            {"data": "sercli"},
            {"data": "sername"},
            {"data": "hora",
                render:function(data,type,row){
                    var html='';
                        html+=row.fecha_reg+' '+row.hora;
                    return html;
                }
            },
            {"data": "reg",
                render:function(data,type,row){
                    var html='';
                    	html+=row.fecha_reg+' '+row.hora_reg;
                    return html;
                }
            },
            {"data": null,
                render:function(data,type,row){
                    var html='<div class="serinfo">';
                        if(row.serinfo!=null){
                            html+=row.serinfo;
                        }
                        
                        html+='</div>';
                    return html;
                }
            },
            {"data": null,
                render:function(data,type,row){
                    var html='';
                    return html;
                }
            }
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[20,25, 50, 100,200,400,-1], [20,25, 50,100,200,400,'todo']],
        //"lengthMenu": [[25, 50, 100,200,400], [25, 50,100,200,400]],
        //"searching": false
        //columnDefs: [ { orderable: false, targets: [8,9] }],
        
    }).on('draw',function(){
        
    });
    loadamoentacion_ser();
}
function loadamoentacion_ser(){
    var eje=$('#eje option:selected').val();
    var fecha=$('#fecha').val();
    $.ajax({
        type:'POST',
        url: base_url+'R_amonestaciones/amones_servicios',
        data: {
            eje: eje,
            fecha: fecha
            },
            async: false,
            statusCode:{
                404: function(data){
                    swal("Error", "404", "error");
                },
                500: function(){
                    swal("Error", "500", "error"); 
                }
            },
            success:function(data){
                console.log(data);
                $('.info_r_rechazo').html(data);
                $('#tabla_amo_rechazo').DataTable({
                        pagingType: 'input',
                        dom: 'Blfrtip',
                        buttons: [
                            'copyHtml5',
                            'excelHtml5',
                            'pdfHtml5',
                        ],
                        "order": [[ 6, "desc" ]],
                    });
            }
        });
}