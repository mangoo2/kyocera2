<?php 
	header("Content-Type: application/vnd.ms-excel");
    header("Content-Disposition: attachment; filename=empleados.xls");
?>

<table border="1">
	<thead>
		<tr>
			<th>#</th>
			<th>Nombre</th>
			<th>Apellidos</th>
			<th>Fecha de nacimiento</th>
			<th>Sexo</th>
			<th>Numero de seguro social</th>
			<th>Direccion</th>
			<th>Ciudad</th>
			<th>Estado</th>
			<th>Telefono</th>
			<th>Celular</th>
			<th>Email</th>
			<th>Usuario</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($query->result() as $item) { ?>
			<tr>
				<td><?php echo $item->personalId; ?></td>
				<td><?php echo $item->nombre; ?></td>
				<td><?php echo $item->apellido_paterno.' '.$item->apellido_materno; ?></td>
				<td><?php echo $item->fecha_nacimiento; ?></td>
				<td>
					<?php
						if ($item->sexo==1) {
						 	echo 'Masculino';
						}else{
							echo 'Femenino';
						} 
					?>
				</td>
				<td><?php echo $item->numero_ss; ?></td>
				<td><?php echo $item->direccion; ?></td>
				<td><?php echo $item->ciudad; ?></td>
				<td><?php echo $item->estados; ?></td>
				<td><?php echo $item->telefono; ?></td>
				<td><?php echo $item->celular; ?></td>
				<td>
					<?php echo $item->email; ?>
				</td>
				<td><?php echo $item->Usuario; ?></td>
			</tr>
		<?php } ?>
	</tbody>
</table>