var base_url=$('#base_url').val();
$(document).ready(function($) {
	$('#selecte').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un equipo',
          ajax: {
            url: base_url+'Inventario_productos/selecte',
            dataType: "json",
            data: function (params) {
              var query = {
                search: params.term,
                type: 'public'
              }
              return query;
          },
          processResults: function(data){
            var clientes=data;
            var itemscli = [];
            data.forEach(function(element) {
                    itemscli.push({
                        id: element.id,
                        text: element.modelo
                    });
                });
                return {
                    results: itemscli
                };        
          },  
        }
    });
    $('#selectr').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una refaccion',
          ajax: {
            url: base_url+'Inventario_productos/selectr',
            dataType: "json",
            data: function (params) {
              var query = {
                search: params.term,
                type: 'public'
              }
              return query;
          },
          processResults: function(data){
            var clientes=data;
            var itemscli = [];
            data.forEach(function(element) {
                    itemscli.push({
                        id: element.id,
                        text: element.nombre
                    });
                });
                return {
                    results: itemscli
                };        
          },  
        }
    });	
    $('#selecta').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un accesorio',
          ajax: {
            url: base_url+'Inventario_productos/selecta',
            dataType: "json",
            data: function (params) {
              var query = {
                search: params.term,
                type: 'public'
              }
              return query;
          },
          processResults: function(data){
            var clientes=data;
            var itemscli = [];
            data.forEach(function(element) {
                    itemscli.push({
                        id: element.id,
                        text: element.nombre
                    });
                });
                return {
                    results: itemscli
                };        
          },  
        }
    });	
    $('#selectc').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un consumible',
          ajax: {
            url: base_url+'Inventario_productos/selectc',
            dataType: "json",
            data: function (params) {
              var query = {
                search: params.term,
                type: 'public'
              }
              return query;
          },
          processResults: function(data){
            var clientes=data;
            var itemscli = [];
            data.forEach(function(element) {
                    itemscli.push({
                        id: element.id,
                        text: element.modelo
                    });
                });
                return {
                    results: itemscli
                };        
          },  
        }
    });	
});
function generarreporte(){
	var tipo=$('#tipop option:selected').val();
  var bode=$('#bodega option:selected').val();
  var series = $('#mostrar_serie').is(':checked')==true?1:0;
	if(tipo==0){
		var producto=0;
	}
	if(tipo==1){
		var producto=$('#selecte option:selected').val()==undefined?0:$('#selecte option:selected').val();
	}
	if(tipo==2){
		var producto=$('#selectr option:selected').val()==undefined?0:$('#selectr option:selected').val();
	}
	if(tipo==3){
		var producto=$('#selecta option:selected').val()==undefined?0:$('#selecta option:selected').val();
	}
	if(tipo==4){
		var producto=$('#selectc option:selected').val()==undefined?0:$('#selectc option:selected').val();
	}
	window.open(base_url+'Inventario_productos/inventario?tipo='+tipo+'&producto='+producto+'&series='+series+'&bodega='+bode, '_blank');
}
function tipopselec(){
  var tipo=$('#tipop option:selected').val();
  if(tipo==1){
    $('.selecte').show('show');
    $('.selectr').hide();
    $('.selecta').hide();
    $('.selectc').hide();
    $('.mostrar_serie').show('show');
  }
  if(tipo==2){
    $('.selecte').hide();
    $('.selectr').show('show');
    $('.selecta').hide();
    $('.selectc').hide();
    $('.mostrar_serie').hide('show');
    $('#mostrar_serie').prop('checked', false);

  }
  if(tipo==3){
    $('.selecte').hide();
    $('.selectr').hide();
    $('.selecta').show('show');
    $('.selectc').hide();
    $('.mostrar_serie').show('show');
  }
  if(tipo==4){
    $('.selecte').hide();
    $('.selectr').hide();
    $('.selecta').hide();
    $('.selectc').show('show');
    $('.mostrar_serie').hide('show');
    $('#mostrar_serie').prop('checked', false);

  }
  if(tipo==0){
    $('.selecte').hide();
    $('.selectr').hide();
    $('.selecta').hide();
    $('.selectc').hide();
    $('.mostrar_serie').show('show');
  }
}