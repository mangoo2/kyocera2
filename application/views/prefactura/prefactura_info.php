<!DOCTYPE html>
<html lang="es">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="icon" href="<?php echo base_url(); ?>app-assets/images/favicon/favicon_kyocera.png" sizes="32x32">
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<title>Prefactura</title>
		<!-- jQuery Library -->
    	<script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/jquery-3.3.1.min.js"></script>
    	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <link href="<?php echo base_url(); ?>app-assets/vendors/sweetalert/dist/sweetalert.css" type="text/css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>public/css/fontawesome/css/all.css" type="text/css" rel="stylesheet">
        <!--
        <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/js/ckeditor/ckeditor.js"></script>
    	<script type="text/javascript" src="<?php echo base_url(); ?>app-assets/js/ckeditor/adapters/jquery.js"></script>-->
		<style type="text/css">
			.tdstitle{
				background: #c1bcbc;
				font-weight: bold;
				color: white;
			}
			html{
			        -webkit-print-color-adjust: exact;
			}
			input{
				
				padding: 0px !important;
				height: 28px !important;
			}
			td,th{
				padding-top: 5px !important;
				padding-bottom: 5px !important;

			}
			select{
				
				padding: 0px !important;
				height: 28px !important;
				
				 font-size: 11px !important;
			}
			.optionred{
				background: #e29696;
			}
			@media print{
				input{
					background: transparent;
					border: 0 !important;
					
				}
				.buttonenvio{
					display: none;
				}
				html{
			        -webkit-print-color-adjust: exact;
				}
				.buttoimprimir{
					display: none;
				}
				.tdstitle{
					background: #c1bcbc;
					font-weight: bold;
					color: white;
				}
				select{
					background: transparent;
					border: 0 !important;
					-moz-appearance: none;
					 -webkit-appearance: none;
					 appearance: none;
				}
				.form-control:disabled, .form-control[readonly] {
				    background-color: transparent;
				    opacity: 1;
				}
				.devolucion{
					display: none;
				}
			}
				.devolucion{
					background-color: #ff02026b;
				}
				.btndevolucion2{
					display: none;
				}
				.btndevolucion3{
					display: none;
				}
			<?php if(isset($_GET['devolucion'])){ ?>
				.table_dev{
					display: none;
				}
			<?php }else{ ?>
				.btndevolucion{
					display: none;
				}
			<?php } ?>
			#modaldirecciones .modal-dialog{
				max-width: 90% !important;
			}
			.viewdirecciones{
				max-height: 385px !important
			}
			table td{
				text-align: center;
			}
			option:disabled{
				background-color: #dadada;
			}
		</style>
		<?php $finalizadogeneral=1;?>
	</head>
	<body>
		<input type="hidden" id="base_url" name="base_url" value="<?php echo base_url(); ?>">
		<form method="post" id="formprefactura">
			<table border="1" width="100%" class="table table-bordered table-striped">
				<thead>
				<tr>
					<th style="font-size: 12px; ">Cantidad</th>
					<th style="font-size: 12px; ">No. de Parte</th>
					<th style="font-size: 12px; ">Modelo</th>
					<th style="font-size: 12px; ">Serie</th>
					<th style="font-size: 12px; ">Precio Unitario</th>
					<th style="font-size: 12px; " >Total</th>
					<th></th>
				</tr>
				</thead>
				<tbody>
					<?php 
						$totalgeneral=0;
						$block_envio=0;
						$entregado_fecha_g='';
						foreach ($resultadoequipos->result() as $item) { 
							$totale=$item->cantidad*$item->precio;
							$totalgeneral=$totalgeneral+$totale;
							
							?>
							<tr>
								<td style="font-size: 12px; text-align: center;">
									<?php echo $item->cantidad;?>
									<a class="btn btn-danger btn-sm btndevolucion" onclick="devolucion(2,<?php echo $item->id;?>,<?php echo $item->cantidad;?>)" ><i class="fas fa-trash"></i></a>		
								</td>
								<td style="font-size: 12px; "><?php echo $this->ModeloCatalogos->obtenernumeroserieequipo($item->idEquipo);?></td>
								<td style="font-size: 12px;"><?php echo $item->modelo;?></td>
								<td style="font-size: 12px;"><?php 
													if ($item->serie_estatus==0) {
													}else{
														$resuleserie=$this->ModeloCatalogos->getequiposerie($item->id);
														foreach ($resuleserie->result() as $itemse) { 
														 	echo $itemse->serie;  
														}
														
													}
								?></td>

								<td style="font-size: 12px; text-align: center;"  >
									$<?php echo number_format($item->precio,2,'.',',');?>
								</td>
								<td style="font-size: 12px; text-align: center;" >$<?php echo number_format($totale,2,'.',',');?></td>
								<td style="font-size: 12px; text-align: center;">
									<?php 
										if($item->entregado_status>0){
											$btn_status='btn-secondary';
											if($item->entregado_status>1){
												$btn_status='btn-success';	
											}
											$perresolt=$this->ModeloCatalogos->db10_getselectwheren('personal',array('personalId'=>$item->entregado_personal));
											$info_btn='';
											foreach ($perresolt->result() as $itemp) {
												$info_btn='<br>'.$itemp->nombre.' '.$itemp->apellido_paterno.'<br>'.$item->entregado_fecha;
												$entregado_fecha_g=$item->entregado_fecha;
											}
											
										}else{
											$finalizadogeneral=0;
											$btn_status='btn-primary';
											$info_btn='';
										}
									?>
									<button type="button" class="btn <?php echo $btn_status;?>" onclick="entregas(<?php echo $item->id;?>,2,<?php if($item->serie_bodega>0){echo $item->serie_bodega;}else{echo 0;}?>,0)"><i class="fas fa-handshake fa-fw"></i></button><?php echo $info_btn;?></td>
							</tr>
							
							<?php
						}
						 
						foreach ($resultadoaccesorios->result() as $item) { 
							$totala=$item->cantidad*$item->costo;
							$totalgeneral=$totalgeneral+$totala;
							if ($item->serie_estatus==0) {
								$block_envio=1;
							}
							?>
							<tr>
								<td style="font-size: 12px; text-align: center;">
									<?php echo $item->cantidad;?>
									<a class="btn btn-danger btn-sm btndevolucion" onclick="devolucion(3,<?php echo $item->id_accesoriod;?>,<?php echo $item->cantidad;?>)" ><i class="fas fa-trash"></i></a>			
								</td>
								<!--<td style="font-size: 12px; text-align: center;" ><?php echo $this->ModeloCatalogos->obtenerbodega($item->serie_bodega);?></td><!--F2-->
								<td style="font-size: 12px; "><?php echo $item->no_parte;?></td>
								<td style="font-size: 12px; " ><?php echo $item->nombre;?></td>
								<td style="font-size: 12px; " ><?php if ($item->serie_estatus==0) {
													}else{
														$resuleserie=$this->ModeloCatalogos->getaccesorioserie($item->id_accesoriod);
														$arrayseries=array();
														foreach ($resuleserie->result() as $itemse) { 
															echo $itemse->serie.'<br>';
														}
													}?></td>
								<td style="font-size: 12px; text-align: center;" 
								 >$<?php echo number_format($item->costo,2,'.',',');?></td>
								<td style="font-size: 12px; text-align: center;" >$<?php echo number_format($totala,2,'.',',');?></td>
								<td style="font-size: 12px;"><?php 
										if($item->entregado_status>0){
											$btn_status='btn-secondary';
											if($item->entregado_status>1){
												$btn_status='btn-success';	
											}
											$info_btn='<br>'.$item->personal.' '.$item->apellido_paterno.'<br>'.$item->entregado_fecha;
											$entregado_fecha_g=$item->entregado_fecha;
										}else{
											$finalizadogeneral=0;
											$btn_status='btn-primary';
											$info_btn='';
										}
									?>
									<button type="button" class="btn <?php echo $btn_status;?>" onclick="entregas(<?php echo $item->id_accesoriod;?>,3,<?php if($item->serie_bodega>0){ echo $item->serie_bodega;}else{echo 0;}?>,<?php echo $item->id_accesorio;?>)"><i class="fas fa-handshake fa-fw"></i></button><?php echo $info_btn;?></td>
							</tr>
							
							<?php	
						}
						 
						foreach ($resultadoconsumibles->result() as $item) { 
							$costo=$item->costo_toner;

							$totalc=$item->cantidad*$costo;
							$totalgeneral=$totalgeneral+$totalc;
							?>
							<tr>
								<td style="font-size: 12px; text-align: center;"><?php echo $item->cantidad;?>
								</td>
								<td style="font-size: 12px; "><?php echo $item->parte;?></td>
								<td style="font-size: 12px; "><?php echo $item->modelo;?></td>
								<td style="font-size: 12px; "></td>
								<td style="font-size: 12px; text-align: center;" 
									>$<?php echo number_format($costo,3,'.',',');?></td>
								<td style="font-size: 12px; text-align: center;" >$<?php echo number_format($totalc,3,'.',',');?></td>
								
								<td style="font-size: 12px;"><?php 
									if($item->entregado_status>0){
										$btn_status='btn-secondary';
										if($item->entregado_status>1){
											$btn_status='btn-success';	
										}
										$info_btn='<br>'.$item->personal.' '.$item->apellido_paterno.'<br>'.$item->entregado_fecha;
										$entregado_fecha_g=$item->entregado_fecha;
									}else{
										$finalizadogeneral=0;
										$btn_status='btn-primary';
										$info_btn='';
									}
								?>
								<button type="button" class="btn <?php echo $btn_status;?>" onclick="entregasb(<?php echo $item->id;?>,4,<?php if($item->serie_bodega>0){ echo $item->serie_bodega;}else{ echo 0;}?>,<?php echo $item->id_consumibles;?>)"><i class="fas fa-handshake fa-fw"></i></button><?php echo $info_btn;?></td>
								
							</tr>
							
							<?php	
						}
						

						// Tabla de ventasdetallesconsumibles   		
						foreach ($consumiblesventadetalles->result() as $item) { 
							$costo=$item->costo_toner;

							$totalc=$item->cantidad*$costo;
							$totalgeneral=$totalgeneral+$totalc;
							?>
							<tr>
								<td style="font-size: 12px; text-align: center;">
									<?php echo $item->cantidad;?>
									<a class="btn btn-danger btn-sm btndevolucion" onclick="devolucion(1,<?php echo $item->id;?>,<?php echo $item->cantidad;?>)" ><i class="fas fa-trash"></i></a>
								</td>
								<!--<td style="font-size: 12px; text-align: center;"onclick="editar_cvd(<?php echo $item->id;?>,'<?php echo $item->surtir;?>');"><?php echo $item->surtir;?></td><!-- f4-->
								<td style="font-size: 12px; text-align: center;"><?php echo $item->parte;?></td>
								<td style="font-size: 12px; "><?php echo $item->modelo;?></td>
								<td style="font-size: 12px; ">
									<?php 	if($item->idserie!=null){ 
												$resultse=$this->ModeloCatalogos->db3_getselectwheren('series_productos',array('serieId'=>$item->idserie));
											
												foreach ($resultse->result() as $itemse) {
													echo $itemse->serie;
												}
											}
										?>
								</td>

								<td style="font-size: 12px; text-align: center;"  >
									$<?php echo number_format($costo,3,'.',',');?></td>
								<td style="font-size: 12px; text-align: center;" >$<?php echo number_format($totalc,2,'.',',');?></td>
								<td style="font-size: 12px; text-align: center;">
									<?php 
										if($item->entregado_status>0){
											$btn_status='btn-secondary';
											if($item->entregado_status>1){
												$btn_status='btn-success';	
											}
											$info_btn='<br>'.$item->nombre.' '.$item->apellido_paterno.'<br>'.$item->entregado_fecha;
											$entregado_fecha_g=$item->entregado_fecha;
										}else{
											$finalizadogeneral=0;
											$btn_status='btn-primary';
											$info_btn='';
										}
									?>
									<button type="button" class="btn <?php echo $btn_status;?>" onclick="entregasb(<?php echo $item->id;?>,1,<?php if($item->bodegas>0){ echo $item->bodegas;}else{ echo 0;}?>,<?php echo $item->idConsumible;?>)"><i class="fas fa-handshake fa-fw"></i></button><?php echo $info_btn;?></td>
							</tr>
							
						<?php	}
							
					    foreach ($ventadetallesrefacion->result() as $item) { 
                                        
							$totalc=round($item->cantidad*$item->precioGeneral,2);
							$totalgeneral=$totalgeneral+$totalc;
							if ($item->serie_estatus==0) {
								$block_envio=1;
							}
							?>
							<tr>
								<td style="font-size: 12px; text-align: center;">
									<?php echo $item->cantidad;?>
									<a class="btn btn-danger btn-sm btndevolucion" onclick="devolucion(5,<?php echo $item->id;?>,<?php echo $item->cantidad;?>)" ><i class="fas fa-trash"></i></a>		
								</td>
								<!--<td style="font-size: 12px; text-align: center;" onclick="editar_vdr(<?php echo $item->id;?>,'<?php echo $item->surtir;?>');"><?php echo $item->surtir;?></td>-->
								<td style="font-size: 12px; text-align: center;"><?php echo $item->codigo;?></td>
								<td style="font-size: 12px;"><?php echo $item->modelo;?></td>
								<td style="font-size: 12px;">
									<?php if ($item->serie_estatus==0) {
											}else{
												$resuleserie=$this->ModeloCatalogos->getrefaccionserie($item->id);
												$arrayseries=array();
												foreach ($resuleserie->result() as $itemse) { 
												 echo $itemse->serie;
												}
												
											}
									?>
								</td>
								
								<td style="font-size: 12px; text-align: center;"   >
									$<?php echo number_format($item->precioGeneral,2,'.',',');?></td>
								<td style="font-size: 12px; text-align: center;" >$<?php echo number_format($totalc,2,'.',',');?></td>
								<td style="font-size: 12px;"><?php 
										if($item->entregado_status>0){
											$btn_status='btn-secondary';
											if($item->entregado_status>1){
												$btn_status='btn-success';	
											}
											$info_btn='<br>'.$item->personal.' '.$item->apellido_paterno.'<br>'.$item->entregado_fecha;
											$entregado_fecha_g=$item->entregado_fecha;
										}else{
											$finalizadogeneral=0;
											$btn_status='btn-primary';
											$info_btn='';
										}
									?>
									<button type="button" class="btn <?php echo $btn_status;?>" onclick="entregas(<?php echo $item->id;?>,5,<?php if($item->serie_bodega>0){ echo $item->serie_bodega;}else{ echo 0;}?>,<?php echo $item->idRefaccion?>)"><i class="fas fa-handshake fa-fw"></i></button><?php echo $info_btn;?></td>
							</tr>
							
						<?php	}
						
					?>
					
				</tbody>
				<tfoot>
					<tr>
						<td style="font-size: 12px; " >Subtotal</td>
						<td style="font-size: 12px; " >$<?php echo number_format($totalgeneral,2,'.',',');?></td>
						<td style="font-size: 12px; ">Iva</td>
						<td style="font-size: 12px; " >$<?php echo number_format($totalgeneral*0.16,2,'.',',');?></td>
						<td style="font-size: 12px; ">Total</td>
						<td style="font-size: 12px; " >$<?php echo number_format(round($totalgeneral*0.16, 2)+round($totalgeneral, 2),2,'.',',');?></td>
						<td></td>
					</tr>
				</tfoot>
			</table>
		</form>
		
	</body>
	<style type="text/css">
		.error{
			color: red;
			font-size: 10px;
		}
	</style>
	<script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/sweetalert/dist/sweetalert.min.js"></script>
	<link href="<?php echo base_url(); ?>app-assets/vendors/confirm/jquery-confirm.min.css" type="text/css" rel="stylesheet">
	<script src="<?php echo base_url(); ?>app-assets/vendors/confirm/jquery-confirm.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/jquery-validation/jquery.validate.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/jquery-validation/additional-methods.min.js"></script>
	<script type="text/javascript">
		function entregas(idrow,tipo,bodegai,itemid){
			var htmlselect=$('#personalresult').html();
			var htmlselectb=$('#bodegasresult').html();
			if(tipo==5){//refacciones
				$.ajax({
                    type:'POST',
                    url: "<?php echo base_url()?>index.php/Generales/bodegarefacciones",
                    data: {
                        id:itemid,
                        bodegai:bodegai
                    },
                    success: function (response){
                        $('#bodegasresult2').html(response);
                        var htmlselectb=$('#bodegasresult2').html();

                        setTimeout(function () {
			  				$('#bodegaselect').html(response);
			  				$("#bodegaselect option[value='"+bodegai+"']").attr("selected", true);
						},1000);
                    },
                    error: function(response){
                        $.alert({
                                boxWidth: '30%',
                                useBootstrap: false,
                                title: 'Error!',
                                content: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema'});   
                         
                    }
                });
			}
			if(tipo==3){
				$.ajax({
                    type:'POST',
                    url: "<?php echo base_url()?>index.php/Generales/bodegaaccesorios",
                    data: {
                        id:itemid,
                        bodegai:bodegai
                    },
                    success: function (response){
                        $('#bodegasresult2').html(response);
                        var htmlselectb=$('#bodegasresult2').html();

                        setTimeout(function () {
			  				$('#bodegaselect').html(response);
			  				$("#bodegaselect option[value='"+bodegai+"']").attr("selected", true);
						},1000);
                    },
                    error: function(response){
                        $.alert({
                                boxWidth: '30%',
                                useBootstrap: false,
                                title: 'Error!',
                                content: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema'});   
                         
                    }
                });
			}
			if(tipo==2){
				$.ajax({
                    type:'POST',
                    url: "<?php echo base_url()?>index.php/Generales/bodegaequipos",
                    data: {
                        id:itemid,
                        bodegai:bodegai
                    },
                    success: function (response){
                        $('#bodegasresult2').html(response);
                        var htmlselectb=$('#bodegasresult2').html();

                        setTimeout(function () {
			  				$('#bodegaselect').html(response);
			  				$("#bodegaselect option[value='"+bodegai+"']").attr("selected", true);
						},1000);
                    },
                    error: function(response){
                        $.alert({
                                boxWidth: '30%',
                                useBootstrap: false,
                                title: 'Error!',
                                content: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema'});   
                         
                    }
                });
			}
			setTimeout(function () {
  				$("#bodegaselect option[value='"+bodegai+"']").attr("selected", true);
			},1000);
			$.confirm({
		        boxWidth: '35%',
		        useBootstrap: false,
		        icon: 'fa fa-warning',
		        title: 'Atención!',
		        content: '¿Desea registrar la entrega?<br>seleccione el tecnico<br><select id="tecnicoselect" class="form-control">'+htmlselect+'</select><br>seleccione bodega<br><select id="bodegaselect" class="form-control">'+htmlselectb+'</select>',
		        type: 'red',
		        typeAnimated: true,
		        buttons:{
		            confirmar: function (){
		                var tec=$('#tecnicoselect option:selected').val();
		                var bode=$('#bodegaselect option:selected').val();
	                     $.ajax({
	                        type:'POST',
	                        url: "<?php echo base_url()?>index.php/Generales/entregas",
	                        data: {
	                            id:idrow,
	                            table:tipo,
	                            personal:tec,
	                            bode:bode
	                        },
	                        success: function (response){
	                            location.reload();
	                        },
	                        error: function(response){
	                            $.alert({
	                                    boxWidth: '30%',
	                                    useBootstrap: false,
	                                    title: 'Error!',
	                                    content: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema'});   
	                             
	                        }
	                    });
		                    
		            },
		            cancelar: function (){
		                
		            }
		        }
		    });
		    bloqueoseries();
		}
		function entregasb(idrow,tipo,bodegai,itemid){
			if(tipo==1 || tipo==4 || tipo==7 || tipo==11){
				$.ajax({
                    type:'POST',
                    url: "<?php echo base_url()?>index.php/Cotizaciones/consumiblesbodegas",
                    data: {
                        id:itemid,
                        bodegai:bodegai
                    },
                    success: function (response){
                        $('#bodegasresult2').html(response);
                        var htmlselectb=$('#bodegasresult2').html();

                        setTimeout(function () {
			  				$('#bodegaselect').html(response);
			  				$("#bodegaselect option[value='"+bodegai+"']").attr("selected", true);
						},1000);
                    },
                    error: function(response){
                        $.alert({
                                boxWidth: '30%',
                                useBootstrap: false,
                                title: 'Error!',
                                content: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema'});   
                         
                    }
                });
			}else{
				var htmlselectb=$('#bodegasresult').html();
			}
			var htmlselect=$('#personalresult').html();
			setTimeout(function () {
  				$("#bodegaselect option[value='"+bodegai+"']").attr("selected", true);
			},1000);
			$.confirm({
		        boxWidth: '35%',
		        useBootstrap: false,
		        icon: 'fa fa-warning',
		        title: 'Atención!',
		        content: '¿Desea registrar la entrega?<br>seleccione el tecnico<br><select id="tecnicoselect" class="form-control">'+htmlselect+'</select><br>seleccione bodega<br><select id="bodegaselect" class="form-control" onchange="bodegaselect()">'+htmlselectb+'</select>',
		        type: 'red',
		        typeAnimated: true,
		        buttons:{
		            confirmar: { 
		            	text: 'Confirmar',
		            	btnClass: 'btn-default btn-default-confirm btn-confirmar-b',
		            	action: function (){
			                var tec=$('#tecnicoselect option:selected').val();
			                var bode=$('#bodegaselect option:selected').val();
		                     $.ajax({
		                        type:'POST',
		                        url: "<?php echo base_url()?>index.php/Generales/entregas",
		                        data: {
		                            id:idrow,
		                            table:tipo,
		                            personal:tec,
		                            bode:bode
		                        },
		                        success: function (response){
		                            location.reload();
		                        },
		                        error: function(response){
		                            $.alert({
		                                    boxWidth: '30%',
		                                    useBootstrap: false,
		                                    title: 'Error!',
		                                    content: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema'});   
		                             
		                        }
		                    });
	                 	}
		                    
		            },
		            cancelar: function (){
		                
		            }
		        }
		    });
		    bloqueoseries();
		}
		function bloqueoseries(){
			setTimeout(function(){ 
				$.ajax({
		        type:'POST',
		        url: '<?php echo base_url()?>AsignacionesSeries/bodegasview',
		        data: {
		            accesorio: 0
		            },
		            async: false,
		            statusCode:{
		                404: function(data){
		                    swal("Error", "404", "error");
		                },
		                500: function(){
		                    swal("Error", "500", "error"); 
		                }
		            },
		            success:function(data){
		                var array = $.parseJSON(data);
		                $.each(array, function(index, item) {
		                    if(parseInt(item.tipov)==parseInt(<?php echo $tipov;?>)){
		                    	$('.bodega_'+item.bodegaId).attr('disabled',false);
		                    }else{
		                        $('.bodega_'+item.bodegaId).attr('disabled',true);
		                    }
		                
		            });
		            }
		        });
		        console.log('sssss');
			}, 1000);
			
		    
		}
	</script>
</html>
<select id="personalresult" style="display: none;">
	<?php 
	$resultstec=$this->Configuraciones_model->view_session_searchTecnico();
	foreach ($resultstec as $item) { ?>
     	<option value="<?php echo $item->personalId;?>"><?php echo $item->tecnico;?></option>
    <?php } ?>
</select>
<select id="bodegasresult" style="display: none;">
	<?php foreach ($resultbodegas->result() as $item) { ?>
		<option value="<?php echo $item->bodegaId;?>" class="bodega_<?php echo $item->bodegaId;?>" data-stock="1"><?php echo $item->bodega;?></option>
	<?php } ?>
</select>
<?php 
	if($finalizadogeneral==1){
		$this->ModeloCatalogos->updateCatalogo('ventas',array('e_entrega'=>1,'entregado_fecha'=>$entregado_fecha_g),array('id'=>$idventa));
	}
?>