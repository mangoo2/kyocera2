<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class R_desfase extends CI_Controller {
    public function __construct()     {
        parent::__construct();

        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloGeneral');
        $this->load->model('ModeloAsignacion');
        $this->load->model('Modelodesfase');
        $this->load->helper('url');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->submenu=83;
        if($this->session->userdata('logeado')==true){
            $this->perfilid =$this->session->userdata('perfilid');
            $this->idpersonal = $this->session->userdata('idpersonal');
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,$this->submenu);// 52 es el id del submenu
            if ($permiso==0) {
                //redirect('Sistema');
            }
        }else{
            redirect('Sistema'); 
        }
    }

    function index(){
        $data['MenusubId']=$this->submenu;
        $data['perfilid']=$this->perfilid;

        $data['personalrows']=$this->Configuraciones_model->view_session_searchTecnico(); 
            $this->load->view('header');
            $this->load->view('main');
            $this->load->view('desfase/des_index',$data);
            $this->load->view('footer');
            $this->load->view('desfase/des_indexjs');
    }
    function cargainfo(){
        $params = $this->input->post();
        $tec = $params['eje'];
        $fecha = $params['fecha'];
        $fechaf = $params['fechaf'];
        $html='';
        $periodos=$this->ModeloGeneral->obtencion_dias_de_rango($fecha,$fechaf);
        $html.='<div class="row">';
            $html.='<div class="col s12">';
                $html.='<h4 class="header">Tablero de desfase</h4>';
            $html.='</div>';
            $html.='<div class="col s12">';
                $html.='<table class="table striped" id="t_iti">';
                    $html.='<thead>';
                        $html.='<tr>';
                            $html.='<th>Fecha</th>';
                            $html.='<th>Cliente</th>';
                            $html.='<th>Servicio</th>';
                            $html.='<th>Horario disponible de cliente</th>';
                            $html.='<th>Itinerario establecido por el tecnico</th>';
                            $html.='<th>Hora de inicio</th>';
                            $html.='<th>Hora fin</th>';
                            $html.='<th>Tiempo de servicio</th>';
                            $html.='<th>Historial</th>';
                            $html.='<th>Tiempo de desfase</th>';
                            $html.='<th></th>';
                        $html.='</tr>';
                    $html.='</thead>';
                    $html.='<tbody>';
                            $row_tr=0;
                            foreach ($periodos as $fecha_items) {
                                $fecha_consult=$fecha_items->format('Y-m-d');
                                //===============================================
                                    $result=$this->ModeloGeneral->consultar_servicos_itinerario_2($tec,$fecha_consult,0);
                                    foreach ($result->result() as $item) {
                                        $html_ser='';$view_tr=1;
                                        $empresa='';$hr_disp=''; $hora_ini=''; $hora_fin='';$horaserinicio='';$horaserfin='';$btn_desfase=''; $btn_des=0;$btn_ubi='';$servicio_name='';$historial_hrs='';
                                        if($item->tiposer==1){
                                            $resultado=$this->ModeloAsignacion->getdatosrenta($item->asignacionId,$fecha,0,0);
                                            $row_co=0;if($resultado->num_rows()==0){$view_tr=0;}
                                            foreach ($resultado->result() as $item1) {
                                                $empresa=$item1->empresa;
                                                $hr_disp=$item1->horario_disponible;
                                                $servicio_name = $item1->servicio;
                                                $historial_hrs = $item1->historial_hrs;
                                                if($item1->tservicio_a>0){
                                                    $servicio_name = $item1->poliza;
                                                }
                                                if($item1->nr==0){
                                                    if($row_co==0){
                                                        $horaserinicio=$item1->horaserinicio;
                                                    }
                                                    
                                                    $horaserfin=$item1->horaserfin;
                                                    $row_co++;
                                                }
                                            }
                                        }
                                        if($item->tiposer==2){
                                            $resultado=$this->ModeloAsignacion->getdatospoliza($item->asignacionId,0,0);
                                            $row_po=0;if($resultado->num_rows()==0){$view_tr=0;}
                                            foreach ($resultado->result() as $item1) {
                                                $empresa=$item1->empresa;
                                                $hr_disp=$item1->horario_disponible;
                                                $servicio_name = $item1->servicio;
                                                $historial_hrs = $item1->historial_hrs;
                                                if($row_po==0){
                                                    $horaserinicio=$item1->horaserinicio;
                                                }
                                                
                                                $horaserfin=$item1->horaserfin;
                                                $row_po++;
                                            }
                                        }
                                        if($item->tiposer==3){
                                            $resultado=$this->ModeloAsignacion->getdatoscliente($item->asignacionId);
                                            $row_cli=0;if($resultado->num_rows()==0){$view_tr=0;}
                                            foreach ($resultado->result() as $item1) {
                                                $empresa=$item1->empresa;
                                                $hr_disp=$item1->horario_disponible;
                                                $servicio_name = $item1->servicio;
                                                $historial_hrs = $item1->historial_hrs;
                                                if($item1->tservicio_a>0){
                                                    $servicio_name = $item1->poliza;
                                                }
                                                if($row_cli==0){
                                                    $horaserinicio=$item1->horaserinicio;
                                                }
                                                
                                                $horaserfin=$item1->horaserfin;
                                                $row_cli++;
                                            }
                                        }
                                        if($item->tiposer==4){
                                            $resultado=$this->ModeloAsignacion->getdatosventas($item->asignacionId);
                                            if($resultado->num_rows()==0){$view_tr=0;}
                                            foreach ($resultado->result() as $item1) {
                                                $empresa=$item1->empresa;
                                                $hr_disp=$item1->horario_disponible;
                                                $horaserinicio=$item1->horaserinicio;
                                                $horaserfin=$item1->horaserfin;
                                                $servicio_name = $item1->servicio;
                                                $historial_hrs = $item1->historial_hrs;
                                                if($item1->tservicio_a>0){
                                                    $servicio_name = $item1->poliza;
                                                }
                                            }
                                        }

                                        if($horaserinicio!=''){
                                            // Definir las horas
                                            $horainitec = $item->hora_inicio;
                                            $horainicioreal = $horaserinicio;

                                            // Convertir las horas a objetos DateTime
                                            $horaInicioTec = new DateTime($horainitec);
                                            $horaInicioReal = new DateTime($horainicioreal);

                                            // Verificar si $horainitec es menor que $horainicioreal
                                            if ($horaInicioTec < $horaInicioReal) {
                                                //echo "$horainitec es menor que $horainicioreal\n";
                                                
                                                // Calcular la diferencia
                                                $diferencia = $horaInicioTec->diff($horaInicioReal);
                                                
                                                // Mostrar la diferencia
                                                //echo "La diferencia es de: " . $diferencia->format('%H horas, %I minutos y %S segundos');
                                                if(intval($diferencia->format('%H'))==0){
                                                    if(intval($diferencia->format('%I'))>2){
                                                        //$btn_desfase='<div class="des con">'.$diferencia->format('%H:%I hrs').'</div>';
                                                        $btn_des=2;
                                                    }else{
                                                        //$btn_desfase='<div class="des sin">sin desfase</div>';
                                                        $btn_des=1;
                                                    }
                                                }else{
                                                    //$btn_desfase='<div class="des con">'.$diferencia->format('%H:%I hrs').'</div>';
                                                    $btn_des=2;
                                                }
                                                
                                            } else {
                                                //echo "$horainitec no es menor que $horainicioreal";
                                                //$btn_desfase='<div class="des sin">sin desfase</div>';
                                                $btn_des=1;
                                            }
                                        }else{

                                        }
                                        if($btn_des>0){
                                            $result_ubi=$this->ModeloAsignacion->ubicacion_if_ser_info($item->tiposer,$item->asignacionId);
                                            $row_ubi=0;$ubiinicial='';$ubifinal='';
                                            foreach ($result_ubi->result() as $item_ubi) {
                                                if($item_ubi->tipo==1){
                                                    if($row_ubi==0){
                                                        $ubiinicial=$item_ubi->latitud.','.$item_ubi->longitud;
                                                    }
                                                }
                                                if($item_ubi->tipo==2){
                                                    $ubifinal=$item_ubi->latitud.','.$item_ubi->longitud;
                                                }
                                                $row_ubi++;
                                            }
                                            if($ubiinicial==''){
                                                $ubiinicial=$ubifinal;
                                            }
                                            if($ubifinal==''){
                                                $ubifinal=$ubiinicial;
                                            }
                                            if($btn_des==2){
                                                $btn_desfase='<div class="des con">'.$diferencia->format('%H:%I hrs').'</div>';
                                                if($ubiinicial!='' && $ubifinal!=''){
                                                    $btn_ubi='<div class="des con ubi_ser_'.$item->tiposer.'_'.$item->asignacionId.' row_tr_'.$row_tr.'" onclick="viem_map('.$row_tr.')" data-ubiinicial="'.$ubiinicial.'" data-ubifinal="'.$ubifinal.'">Ver ruta</div>';
                                                }
                                            }else{
                                                $btn_desfase='<div class="des sin">sin desfase</div>';
                                                if($ubiinicial!='' && $ubifinal!=''){
                                                    $btn_ubi='<div class="des sin ubi_ser_'.$item->tiposer.'_'.$item->asignacionId.' row_tr_'.$row_tr.'" onclick="viem_map('.$row_tr.')" data-ubiinicial="'.$ubiinicial.'" data-ubifinal="'.$ubifinal.'">Ver ruta</div>';
                                                }
                                            }
                                        }
                                        $html_ser.='<tr>';
                                            $html_ser.='<td>'.$item->fecha.'</td>';
                                            $html_ser.='<td>'.$empresa.' <!--('.$item->tiposer.','.$item->asignacionId.')--></td>';
                                            $html_ser.='<td>'.$servicio_name.'</td>';
                                            $html_ser.='<td>'.$hr_disp.'</td>';
                                            $html_ser.='<td><!--'.$item->hora_inicio.' ('.$item->tiposer.','.$item->asignacionId.')-->'.date('h:i A',strtotime($item->hora_inicio)).'</td>';
                                            if($horaserinicio!=''){
                                                $horaserinicio_l=date('h:i A',strtotime($horaserinicio));
                                            }else{
                                                $horaserinicio_l='';
                                            }
                                            if($horaserfin!=''){
                                                $horaserfin_l=date('h:i A',strtotime($horaserfin));
                                            }else{
                                                $horaserfin_l='';
                                            }
                                            if($horaserinicio!='' && $horaserfin!=''){
                                                $horatiempo = $this->intervaloentrehoras($horaserinicio,$horaserfin);
                                            }else{
                                                $horatiempo='';
                                            }
                                            $html_ser.='<td><!--'.$horaserinicio.'-->'.$horaserinicio_l.'</td>';
                                            $html_ser.='<td><!--'.$horaserfin.'-->'.$horaserfin_l.'</td>';
                                            $html_ser.='<td>'.$horatiempo.'</td>';
                                            $html_ser.='<td class="h_hrs">'.$historial_hrs.'</td>';
                                            $html_ser.='<td>'.$btn_desfase.'</td>';
                                            $html_ser.='<td>'.$btn_ubi.'</td>';
                                        $html_ser.='</tr>';
                                        if($view_tr==1){
                                            $html.=$html_ser;    
                                        }
                                        
                                        $row_tr++;
                                    }
                                //===================================================
                            }

                    $html.='</tbody>';
                $html.='</table>';
            $html.='</div>';
        $html.='</div>';

        echo $html;
    }
    function intervaloentrehoras($hi,$hf){
        $horaInicio = new DateTime($hi);
        $horaFinal = new DateTime($hf);

        $diferencia = $horaFinal->diff($horaInicio);

        return $diferencia->format('%H:%I hrs');
    }

    


    
}
?>
