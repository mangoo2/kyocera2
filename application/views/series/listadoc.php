<style type="text/css">
.chosen-container{margin-left: 0px;}
.seriedisponible,
.serieretorno,
.serierenta,
.seriesalida{
text-align: center;  color: #fff;  padding: 1px;    border-radius: 6px;
box-shadow: 0 2px 2px 0 rgba(0,0,0,0.14), 0 1px 5px 0 rgba(0,0,0,0.12), 0 3px 1px -2px rgba(0,0,0,0.2);
}
.seriedisponible{background-color: #26d79a;}
.serieretorno{background-color: #ff9100;}
.serierenta{background-color: #26d744;}
.seriesalida{background-color: #e31a2f;}
.div_button{width: 170px;}
.ocultaritem{display: none;}
[data-badge].has-badge-danger::after {
position: inherit;
}
#cant_reguardar{  width: 99%;}
</style>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/bulma-badge.min.css">
<!-- START CONTENT -->
<section id="content">
  <!--start container-->
  <div class="container" style="padding-top: 0px;">
    <div class="section">
      <input id="idUsuario" type="hidden" name="perfilid" value="<?php echo $_SESSION["usuarioid"]; ?>">
      <input id="perfilid_select" type="hidden" name="perfilid_select" value="<?php echo $perfilid; ?>">
      
      <div id="table-datatables">
        <h4 class="header">Inventario</h4>
        <div class="row">
          <div class="col s2">
            <select id="tipoconsulta" name="tipoconsulta" class="browser-default  chosen-select" onchange="loadtable()">
              <option value="0">Seleccionar</option>
              <option value="1">Consumibles</option>
              <option value="2">Refacciones</option>
              <option value="3">Accesorios</option>
              <option value="4">Equipo</option>
            </select>
            <a class="btn waves-effect waves-light gradient-45deg-green-teal gradient-shadow addnuevoserie" href="<?php echo base_url(); ?>index.php/Aconsumibles/add">Nuevo</a>
          </div>
          <div class="col s8">
            <!------------------------------------------------------------------------------>
            <div class="col s12 m6 l3 ocultaritem">
              <div class="card">
                <div class="card-content deep-orange accent-2 white-text">
                  <p class="card-stats-title">Total Equipos</p>
                  <h4 class="card-stats-number"><?php echo $totalequipos;?></h4>
                </div>
              </div>
            </div>
            <div class="col s12 m6 l3 ocultaritem">
              <div class="card">
                <div class="card-content deep-orange accent-2 white-text">
                  <p class="card-stats-title">Total Consumibles</p>
                  <h4 class="card-stats-number"><?php echo $totalconsumible;?></h4>
                </div>
              </div>
            </div>
            <div class="col s12 m6 l3 ocultaritem">
              <div class="card">
                <div class="card-content deep-orange accent-2 white-text">
                  <p class="card-stats-title">Total Accesorios</p>
                  <h4 class="card-stats-number"><?php echo $totalaccesorio;?></h4>
                </div>
              </div>
            </div>
            <div class="col s12 m6 l3 ocultaritem">
              <div class="card">
                <div class="card-content deep-orange accent-2 white-text">
                  <p class="card-stats-title">Total Refacciones</p>
                  <h4 class="card-stats-number"><?php echo $totalrefacciones;?></h4>
                </div>
              </div>
            </div>
            <!-------------------------------------------------------------------------------->
          </div>
          <div class="col s2">
            <select id="bodegaId" name="bodegaId" class="browser-default  form-control-bmz" onchange="loadtable()">
              <option value="0">Todas las Bodega</option>
              <?php foreach ($bodegasresult as $item){
              
              ?>
              <option value="<?php echo $item->bodegaId; ?>" class="bodegaId_<?php echo $item->bodegaId; ?>" ><?php echo $item->bodega ?></option>
              <?php } ?>
            </select>
          </div>
        </div>
        
      </div>
      <div class="row">
        <div class="col s12">
          <table id="tabla_consumibles" class="tabla_equipo" cellspacing="0">
            <thead>
              <tr>
                <th>#</th>
                <th>Bodega</th>
                <th>No. Parte</th>
                <th class="titulocra"></th>
                <th>Stock</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
</div>

</section>

<div class="modal fade" id="modal_traspasos" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
  <div class="modal-header">
    <h5 class="modal-title" id="exampleModalLabel">Traspasar</h5>
  </div>
  <div class="modal-body">
    <input type="hidden" id="total" readonly>
    <input type="hidden" id="trasserieid" readonly>
    <div class="row">
      <div class="col s12">
        <h2 class="productotras modelotras1"></h2>
      </div>
    </div>
    <div class="row">
      <div class="col s6">
        <label>Bodega Original</label>
        <select class="browser-default chosen-select" id="bodegao" readonly>
          <?php foreach ($bodegasresult as $item) { ?>
          <option value="<?php echo $item->bodegaId; ?>"
          data-bodegatipo="<?php echo $item->tipov; ?>"><?php echo $item->bodega; ?></option>
          <?php } ?>
        </select>
      </div>
      <div class="col s6">
        <label>Bodega Nueva</label>
        <select id="bodeganew" name="bodega1" class="browser-default chosen-select">
          <option value="0" >Seleccione Bodega</option>
          <?php foreach ($bodegasresult as $item) { ?>
          <option value="<?php echo $item->bodegaId; ?>"
          class="bodegatipo_<?php echo $item->tipov; ?>" ><?php echo $item->bodega; ?></option>
          <?php } ?>
        </select>
      </div>
    </div>
    <div class="row">
      <div class="col s6">
        <label>Cantidad</label>
        <input type="number" id="cantidad" class="form-control-bmz " >
        
      </div>
    </div>
    <div class="col s12">
      <label>Motivo</label>
      <textarea class="form-control-bmz" id="motivo1"></textarea>
    </div>
  </div>
  <div class="modal-footer">
    <button type="button" class="modal-action modal-close waves-effect waves-red gray btn-flat">Cancelar</button>
    <button type="button" class="btn blue" onclick="aceptar_traspaso()">Aceptar</button>
  </div>
</div>
</div>
</div>