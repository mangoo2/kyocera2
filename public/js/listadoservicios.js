var base_url = $('#base_url').val();
var table1;
var table2;
var table3;
var table4;
var editarar=0;
var fechaactual=$('#fechaactual').val();
$(document).ready(function($) {
    $('.modal').modal();
	$('.chosen-select').chosen({width: "100%"});
	$('#pcliente').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una opción',
        allowClear: true,
        ajax: {
            url: base_url+'Ventasi/searchclientes',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.id,
                        text: element.empresa

                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function (e) {
          load();
    });	
    table1 = $('#tabla_asignar_1').DataTable({responsive: !0});
    table2 = $('#tabla_asignar_2').DataTable({responsive: !0});
    table3 = $('#tabla_asignar_3').DataTable({responsive: !0});
    table4 = $('#tabla_asignar_4').DataTable({responsive: !0});
    load();
    $('.savemodalventa').click(function(event) {
        if (editarar==1) {
            $.confirm({
                boxWidth: '30%',
                useBootstrap: false,
                icon: 'fa fa-warning',
                title: 'Atención!',
                content: '¿Está seguro de Cancelar?<br>Se necesita permisos de administrador<br>'+
                     '<input type="password" placeholder="Contraseña" id="contrasena" class="name form-control" required/>',
                type: 'red',
                typeAnimated: true,
                buttons:{
                    confirmar: function (){
                        var pass=$('#contrasena').val();
                        if (pass!='') {
                            $.ajax({
                                type:'POST',
                                url: base_url+"index.php/Sistema/solicitarpermiso",
                                data: {
                                    pass:pass
                                },
                                success: function (response){
                                        var respuesta = parseInt(response);
                                        if (respuesta==1) {
                                            //===============
                                                savemodalventa();
                                            //=============
                                        }else{
                                            notificacion(2);
                                        }
                                },
                                error: function(response){
                                    notificacion(1); 
                                     
                                }
                            });
                                        
                        }else{
                            notificacion(0);
                        }
                },
                    cancelar: function () {
                    }
                }
            });
        }else{
           savemodalventa(); 
        }
    });
    $('.cancelarmodalventa').click(function(event) {
        $.confirm({
            boxWidth: '30%',
            useBootstrap: false,
            icon: 'fa fa-warning',
            title: 'Atención!',
            content: '¿Está seguro de Cancelar?<br>Se necesita permisos de administrador<br>'+
                 '<input type="password" placeholder="Contraseña" id="contrasena" class="name form-control" required/>',
            type: 'red',
            typeAnimated: true,
            buttons: 
            {
                confirmar: function (){
                    var pass=$('#contrasena').val();
                    if (pass!='') {
                        $.ajax({
                            type:'POST',
                            url: base_url+"index.php/Sistema/solicitarpermiso",
                            data: {
                                pass:pass
                            },
                            success: function (response){
                                    var respuesta = parseInt(response);
                                    if (respuesta==1) {
                                        //===============
                                            $.ajax({
                                                  type:'POST',
                                                  url: base_url+'index.php/Listaservicios/cancelartotalvs',
                                                  data: {
                                                    tasign:$('#vmidasignacion').val(),
                                                    },
                                                  success:function(data){
                                                      $.alert({
                                                        boxWidth: '30%',
                                                        useBootstrap: false,
                                                        title: 'Atención!',
                                                        content: 'Cancelacion correcta'}
                                                      );
                                                      $('#modalventa').modal('close');
                                                  }
                                              });
                                        //=============
                                    }else{
                                        notificacion(2);
                                    }
                            },
                            error: function(response){
                                notificacion(1); 
                                 
                            }
                        });
                                    
                    }else{
                        notificacion(0);
                    }
            },
                cancelar: function () 
                {
                }
            }
        });
    });
});

function load(){
	var serv = $('#serv option:selected').val();
	if (serv==1) {
		loadcontrato();
	}else if(serv==2){
		loadpoliza();
	}else if(serv==3){
        loadcliente();
    }else if(serv==4){
        loadventas();
    }else{
        //table.destroy();
        table = $('#tabla_asignar').DataTable();
    }
}
function loadcontrato(){
    $('.tabla_asignar_1').show();
    $('.tabla_asignar_2').hide();
    $('.tabla_asignar_3').hide();
    $('.tabla_asignar_4').hide();
    table1.destroy();
    //table2.destroy();
    //table3.destroy();
    //table4.destroy();
    var tiposer = $('#tiposer option:selected').val();
	var tecnico = $('#tecnico option:selected').val();
	var status = $('#status option:selected').val();
    var estatus_ad = $('#estatus_ad option:selected').val();
	var zona = $('#zona option:selected').val();
	var pcliente = $('#pcliente option:selected').val();
    var noti = $('#noti option:selected').val();
		if(pcliente==undefined){
		    pcliente=0;
		}
    var fechainicio = $('#fechainicio').val();
    var fechafin = $('#fechafin').val();
    table1 = $('#tabla_asignar_1').DataTable({
        fixedHeader: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        search: {
                return: true
            }, 
        "ajax": {
            url: base_url + "index.php/Listaservicios/getlistaservicioc",
            type: "post",
            "data": {
                "tiposer":tiposer,
				"tecnico":tecnico,
				"status":status,
				"zona":zona,
				"pcliente":pcliente,
                "fechainicio":fechainicio,
                "fechafin":fechafin,
                "estatus_ad":estatus_ad,
                noti:noti
            },
            error: function() {
                $("#tabla_asignar").css("display", "none");
            }
        },
        "columns": [
            {"data": "asignacionIde"},
            {"data": "servicio",
                render: function(data,type,row){
                    if(row.tservicio_a>0){
                        var html=row.poliza;
                    }else{
                        var html=row.servicio;
                    } 
                        
                    return html;
                }
            },
            {"data": null,
                render: function(data,type,row){ 
                    var html='';
                    if(row.tecnico!=null){
                        html=row.tecnico;
                    }
                    if(row.tecnico2!=null){
                        html+='<br>'+row.tecnico2;
                    }
                    if(row.tecnico3!=null){
                        html+='<br>'+row.tecnico3;
                    }
                    if(row.tecnico4!=null){
                        html+='<br>'+row.tecnico4;
                    }
                    if(row.personalIdf>0){
                        html=row.tecnicof;
                    }

                    return html;
                }
            },
            {"data": "empresa",
                render: function(data,type,row){ 
                    var html = row.empresa+' ('+row.folio+')';
                    return html;
                }
            },
            {"data": "fecha"},
            {"data": null,
                render: function(data,type,row){ 
                    var html='';
                        html+=row.hora+' a '+row.horafin;
                        if(row.hora_comida!=null && row.horafin_comida!=null){
                            if(row.hora_comida!='' && row.horafin_comida!=''){
                                html+='<br>Comida: '+row.hora_comida+' a '+row.horafin_comida;
                            }
                        }
                    return html;
                }
            },
            //{"data": "nombre"},
            {"data": "modelo"},
            {"data": "serie",
                render: function(data,type,row){ 
                    var html='';
                        if(row.serie!=''){
                            html=row.serie+' ('+row.contadores+')';
                        }else{
                            html=row.serie;
                        }
                        
                    return html;
                }
            },
            {"data": null,
                render: function(data,type,row){ 
                    if (row.status==0) {
                        var status='<p></p>';
                    }
                    if (row.status==1) {
                        var status='<p>En proceso</p>';
                    }
                    if (row.status==2) {
                        var status='<p style="color:green">Finalizado</p>';
                    }
                    if (row.status==3) {
                        var status='<p style="color:red">Suspendido</p>';
                    }
                        //var nombre="'"+row.modelo+"'";
                        var html=status;
                    

                    return html;
                }
            },
            {"data": null,
                render: function(data,type,row){ 
                    var html='';
                    if (row.consumibles!=null) {
                       html+=row.consumibles; 
                    }
                        
                    return html;
                }
            },
            //{"data": null,"visible": false},//aqui ira la direccion
            //{"data": null},//aqui ira la direccion
            {"data": null,
                render: function(data,type,row){ 
                        
                        //var nombre="'"+row.modelo+"'";
                        var color_btn='';
                        if(row.t_fecha==null){
                            color_btn='cyan';
                        }else{
                            color_btn='red';
                        }
                        if(row.editado==1){
                            color_btn=' orange ';
                        }
                        //===========================
                        var disabled_date_edit='';
                        if(row.fecha==fechaactual){
                            if(row.personalId!=49){
                                if(row.nr==0){
                                    disabled_date_edit=' disabled ';
                                }
                            }
                        }
                        //=================================
                        var html='';
                        	html+='<button class="waves-effect '+color_btn+' btn-bmz servicio_'+row.asignacionIde+'_1" onclick="editfecha('+row.asignacionIde+',1,'+row.asignacionId+')"';
                                html+='data-fecha="'+row.fecha+'" data-horalimite="'+row.hora_limite+'" data-horainicio="'+row.hora+'" data-horafin="'+row.horafin+'" data-horainiciocomida="'+row.hora_comida+'" data-horafincomida="'+row.horafin_comida+'" ';
                                html+='data-t_fecha="'+row.t_fecha+'" data-t_horainicio="'+row.t_horainicio+'" data-t_horafin="'+row.t_horafin+'" '+disabled_date_edit+'>';
                                html+='<i class="fas fa-calendar-alt" style="font-size: 20px;"></i>';
                            html+='</button>';
                            if (row.status==3) {
                                html+='<a class="waves-effect cyan btn-bmz" onclick="suspenderactivar('+row.asignacionIde+','+row.asignacionId+',1)" title="Activar">\
                                    <i class="fas fa-ban" style="font-size: 20px;"></i></a>';
                            }else{
                                html+='<a class="waves-effect cyan btn-bmz" onclick="suspender('+row.asignacionIde+','+row.asignacionId+',1)" title="Suspender">\
                                    <i class="fas fa-ban" style="font-size: 20px;color:red"></i></a>';
                            }
                        	
                        	html+='<button class="waves-effect cyan btn-bmz servicio_" onclick="edittecnico('+row.asignacionIde+','+row.asignacionId+','+row.personalId+','+row.personalId2+',1)" '+disabled_date_edit+'>\
                                    <i class="fas fa-users" style="font-size: 20px;"></i></button>';
                                    /*
                            html+='<a class="waves-effect cyan btn-bmz" onclick="venta('+row.asignacionId+',1)">\
                                    <i class="fas fa-cart-plus" style="font-size: 20px;"></i></a>';
                                    */
                            if(row.status==1 || row.status==2){
                                html+='<a class="waves-effect cyan btn-bmz btn-bmz s_status_'+row.asignacionIde+'_1"\
                                 data-h_ser_i="'+row.horaserinicio+'" data-h_ser_f="'+row.horaserfin+'"\
                                 data-h_ser_iex="'+row.horaserinicioext+'" data-h_ser_fex="'+row.horaserfinext+'"\
                                   onclick="modal_estatus('+row.asignacionIde+',1)">\
                                    <i class="fas fa-exclamation-circle" style="font-size: 20px;"></i></a>';        
                            }        
                            html+='<a class="waves-effect cyan btn-bmz" onclick="documento('+row.asignacionId+',1,'+row.asignacionIde+')">\
                                    <i class="fas fa-file-alt" style="font-size: 20px;"></i></a>';
                            html+='<!--row.notificarerror_estatus:'+row.notificarerror_estatus+'-->';
                            if(row.notificarerror_estatus==1){
                                html+='<a class="waves-effect red btn-bmz infoerror infoerror_1_'+row.asignacionIde+'" onclick="infoerror('+row.asignacionIde+',1)" data-detalle="'+row.notificarerror+'"><i class="material-icons">error</i></a>'; 
                            }else{
                                html+='<a class="waves-effect cyan btn-bmz infoerror infoerror_1_'+row.asignacionIde+'" onclick="infoerror_confirm('+row.asignacionIde+',1)" data-detalle="" title="Edicion de Equipo/serie"><i class="fas fa-exchange-alt"></i></a>'; 
                            }
                            if(row.nr==1){
                               html+='<a class="waves-effect red btn-bmz nrcoment_'+row.asignacionIde+'" onclick="nrcoment('+row.asignacionIde+',1,1,'+row.not_dev+')" data-nrcoment="'+row.nr_coment+'" data-nrcomentipo="'+row.nr_coment_tipo+'">Rechazo</a>'; 
                            }
                            

                            html+='<a class="waves-effect cyan btn-bmz" onclick="deleteservicio('+row.asignacionIde+','+row.asignacionId+',1)">\
                                    <i class="fas fa-trash-alt" style="font-size: 20px;color:red"></i></a>';
                            if (row.status==2) {//finalizado
                                html+='<a class="waves-effect cyan btn-bmz editcomentariotec_'+row.asignacionId+'" data-comen="'+row.comentario+'" onclick="editcomentariotec('+row.asignacionId+','+row.asignacionIde+',1)"><i class="fas fa-comment"></i></a>';
                            }
                            if(row.activo==0){
                                html=row.infodelete;
                            }
                            if (row.stock_toner==1) {
                                html+='<a class="waves-effect cyan btn-bmz stock_toner_'+row.asignacionId+'" data-comen="'+row.stock_toner_comen+'" onclick="viewstock('+row.asignacionId+')" title="informacion estock"><i class="fas fa-print"></i></a>';
                            }
                            if (row.status==2) {
                                //html+='<a class="waves-effect cyan btn-bmz" onclick="suspenderactivar('+row.asignacionIde+','+row.asignacionId+',1)" title="Activar"><i class="fas fa-ban" style="font-size: 20px;"></i></a>';
                                html+='<a class="waves-effect cyan btn-bmz" onclick="reactivar('+row.asignacionIde+','+row.asignacionId+',1)" title="Activar"><i class="fas fa-check" style="font-size: 20px;"></i></a>';
                            }

                    return html;
                }
            },
          ],
          columnDefs: [
            { orderable: false, targets: 2 },
            { orderable: false, targets: 10 }
          ],
        order: [
            [0, "desc"]
          ],
        createdRow:function(row,data,dataIndex){
            console.log(row);
            console.log(data);
            console.log(dataIndex);
            /*
            $(row).find('td:eq(11)')
                .addClass('contratosdireccion equipo_'+data.idequipo+'_serie_'+data.serieId)
                .attr('data-equipo',data.idequipo).attr('data-serie',data.serieId);
            */
        }
        
    }).on('draw',function(){
        /*
        var timedraw=1000;
        $(".contratosdireccion").each(function() {
            var equipo = parseFloat($(this).data('equipo'));
            var serie = parseFloat($(this).data('serie'));
            setTimeout(function(){ 
                obtenerdireccionescontrato(equipo,serie);
            }, timedraw);
            timedraw=timedraw+300;
        });
        */
        setTimeout(function(){ 
            //tabla_asignar_1
            var tablesearch = $('#tabla_asignar_1_filter input').val();
            $(".tabla_asignar_1").mark(tablesearch);
        }, 2000);
        $('.dataTables_filter').append('<i class="fa fa-search buttonsearch" onclick="searchtable_v1()"></i>')
    });
    $('.cambiartitulo_8').html('Equipo');
}
function loadpoliza(){
    $('.tabla_asignar_2').show();
    $('.tabla_asignar_1').hide();
    $('.tabla_asignar_3').hide();
    $('.tabla_asignar_4').hide();
    //table1.destroy();
    table2.destroy();
    //table3.destroy();
    //table4.destroy();
    var tiposer = $('#tiposer option:selected').val();
	var tecnico = $('#tecnico option:selected').val();
	var status = $('#status option:selected').val();
    var estatus_ad = $('#estatus_ad option:selected').val();
	var zona = $('#zona option:selected').val();
	var pcliente = $('#pcliente option:selected').val();
    var noti = $('#noti option:selected').val();
		if(pcliente==undefined){
		    pcliente=0;
		}
	var fechainicio = $('#fechainicio').val();
    var fechafin = $('#fechafin').val();
    table2 = $('#tabla_asignar_2').DataTable({
        fixedHeader: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        search: {
                return: true
            }, 
        "ajax": {
            url: base_url + "index.php/Listaservicios/getlistaserviciop",
            type: "post",
            "data": {
                "tiposer":tiposer,
				"tecnico":tecnico,
				"status":status,
				"zona":zona,
				"pcliente":pcliente,
                "fechainicio":fechainicio,
                "fechafin":fechafin,
                "estatus_ad":estatus_ad,
                noti:noti
            },
            error: function() {
                $("#tabla_asignar").css("display", "none");
            }
        },
        "columns": [
            {"data": "asignacionIde"},
            {"data": "servicio"},
            {"data": null,
                render: function(data,type,row){ 
                    var html='';
                    if(row.tecnico!=null){
                        html=row.tecnico;
                    }
                    if(row.tecnico2!=null){
                        html+='<br>'+row.tecnico2;
                    }
                    if(row.tecnico3!=null){
                        html+='<br>'+row.tecnico3;
                    }
                    if(row.tecnico4!=null){
                        html+='<br>'+row.tecnico4;
                    }
                    if(row.personalIdf>0){
                        html=row.tecnicof;
                    }
                    return html;
                }
            },
            {"data": "empresa"},
            {"data": "fecha"},
            {"data": null,
                render: function(data,type,row){ 
                    var html='';
                        html+=row.hora+' a '+row.horafin;
                        if(row.hora_comida!=null && row.horafin_comida!=null){
                            if(row.hora_comida!='' && row.horafin_comida!=''){
                                html+='<br>Comida: '+row.hora_comida+' a '+row.horafin_comida;
                            }
                        }
                    return html;
                }
            },
            //{"data": "nombre"},
            {"data": "modelo"},
            {"data": "serie"},
            {"data": null,
                render: function(data,type,row){ 
                    if (row.status==0) {
                        var status='<p></p>';
                    }
                    if (row.status==1) {
                        var status='<p>En proceso</p>';
                    }
                    if (row.status==2) {
                        var status='<p style="color:green">Finalizado</p>';
                    }
                    if (row.status==3) {
                        var status='<p style="color:red">Suspendido</p>';
                    }
                        //var nombre="'"+row.modelo+"'";
                        var html=status;
                    

                    return html;
                }
            },
            {"data": null,"visible": false},
            //{"data": "direccionservicio"},//aqui ira la direccion
            {"data": null,
                render: function(data,type,row){ 
                        var color_btn='';
                        if(row.t_fecha==null){
                            color_btn='cyan';
                        }else{
                            color_btn='red';
                        }
                        if(row.editado==1){
                            color_btn=' orange ';
                        }
                        //var nombre="'"+row.modelo+"'";
                        //===========================
                        var disabled_date_edit='';
                        if(row.fecha==fechaactual){
                            if(row.personalId!=49){
                                if(row.nr==0){
                                    disabled_date_edit=' disabled ';
                                }
                            }
                        }
                        //=================================
                        var html='';
                    		html+='<button class="waves-effect '+color_btn+' btn-bmz servicio_'+row.asignacionIde+'_2" onclick="editfecha('+row.asignacionIde+',2,'+row.asignacionId+')"\
                                    data-fecha="'+row.fecha+'" data-horalimite="'+row.hora_limite+'" data-horainicio="'+row.hora+'" data-horafin="'+row.horafin+'" data-horainiciocomida="'+row.hora_comida+'" data-horafincomida="'+row.horafin_comida+'"';
                                html+='data-t_fecha="'+row.t_fecha+'" data-t_horainicio="'+row.t_horainicio+'" data-t_horafin="'+row.t_horafin+'" '+disabled_date_edit+'>';
                                html+='<i class="fas fa-calendar-alt" style="font-size: 20px;"></i>';
                            html+='</button>';
                            if (row.status==3) {
                                html+='<a class="waves-effect cyan btn-bmz" onclick="suspenderactivar('+row.asignacionIde+','+row.asignacionId+',2)" title="Activar">\
                                    <i class="fas fa-ban" style="font-size: 20px;"></i></a>';
                            }else{
                                html+='<a class="waves-effect cyan btn-bmz" onclick="suspender('+row.asignacionIde+','+row.asignacionId+',2)" title="Suspender">\
                                    <i class="fas fa-ban" style="font-size: 20px;color:red"></i></a>';
                            }
                        	
                        	html+='<button class="waves-effect cyan btn-bmz" onclick="edittecnico('+row.asignacionIde+','+row.asignacionId+','+row.personalId+','+row.personalId2+',2)" '+disabled_date_edit+'>\
                                    <i class="fas fa-users" style="font-size: 20px;"></i></button>';
                                    /*
                            html+='<a class="waves-effect cyan btn-bmz" onclick="venta('+row.asignacionId+',2)">\
                                    <i class="fas fa-cart-plus" style="font-size: 20px;"></i></a>';
                                    */
                            if(row.status==1 || row.status==2){
                                html+='<a class="waves-effect cyan btn-bmz btn-bmz s_status_'+row.asignacionIde+'_2"\
                                 data-h_ser_i="'+row.horaserinicio+'" data-h_ser_f="'+row.horaserfin+'"\
                                  data-h_ser_iex="'+row.horaserinicioex+'" data-h_ser_fex="'+row.horaserfinex+'"\
                                   onclick="modal_estatus('+row.asignacionIde+',2)">\
                                    <i class="fas fa-exclamation-circle" style="font-size: 20px;"></i></a>';        
                            } 
                            html+='<a class="waves-effect cyan btn-bmz" onclick="documento('+row.asignacionId+',2,'+row.asignacionIde+')">\
                                    <i class="fas fa-file-alt" style="font-size: 20px;"></i></a>';  

                            html+='<a class="waves-effect cyan btn-bmz" title="Prefactura" onclick="viewprefacturap('+row.polizaId+')">\
                                    <i class="fas fa-file" style="font-size: 20px;"></i></a>';
                            if(row.notificarerror_estatus==1){
                               html+='<a class="waves-effect red btn-bmz infoerror infoerror_2_'+row.asignacionIde+'" onclick="infoerror('+row.asignacionIde+',2)" data-detalle="'+row.notificarerror+'"><i class="material-icons">error</i></a>'; 
                            }else{
                                html+='<a class="waves-effect cyan btn-bmz infoerror infoerror_2_'+row.asignacionIde+'" onclick="infoerror_confirm('+row.asignacionIde+',2)" data-detalle="'+row.notificarerror+'" title="Edicion de Equipo/serie"><i class="fas fa-exchange-alt"></i></a>';
                            }
                            html+='<a class="waves-effect cyan btn-bmz" onclick="deleteservicio('+row.asignacionIde+','+row.asignacionId+',2)">\
                                    <i class="fas fa-trash-alt" style="font-size: 20px;color:red"></i></a>'; 
                            if (row.status==2) {
                                html+='<a class="waves-effect cyan btn-bmz editcomentariotec_'+row.asignacionId+'" data-comen="'+row.comentario+'" onclick="editcomentariotec('+row.asignacionId+','+row.asignacionIde+',2)"><i class="fas fa-comment"></i></a>'; 
                            }
                            if(row.nr==1){
                               html+='<a class="waves-effect red btn-bmz nrcoment_'+row.asignacionId+'" onclick="nrcoment('+row.asignacionId+',2,'+row.combinada+','+row.not_dev+')" data-nrcoment="'+row.nr_coment+'" data-nrcomentipo="'+row.nr_coment_tipo+'">Rechazo</a>'; 
                            } 
                            if(row.activo==0){
                                html=row.infodelete;
                            }
                            if (row.status==2) {
                                //html+='<a class="waves-effect cyan btn-bmz" onclick="suspenderactivar('+row.asignacionIde+','+row.asignacionId+',1)" title="Activar"><i class="fas fa-ban" style="font-size: 20px;"></i></a>';
                                html+='<a class="waves-effect cyan btn-bmz" onclick="reactivar('+row.asignacionIde+','+row.asignacionId+',2)" title="Activar"><i class="fas fa-check" style="font-size: 20px;"></i></a>';
                            }
                    return html;
                }
            },
          ],
        order: [
            [0, "desc"]
          ],
        createdRow:function(row,data,dataIndex){
            $(row).addClass('poliza_'+data.polizaId);
        }
        
    }).on('draw',function(){
        setTimeout(function(){ 
            //tabla_asignar_1
            var tablesearch = $('#tabla_asignar_2_filter input').val();
            $(".tabla_asignar_2").mark(tablesearch);

            $('.poliza_'+tablesearch).addClass('activopintado');
        }, 2000);
        $('.dataTables_filter').append('<i class="fa fa-search buttonsearch" onclick="searchtable_v2()"></i>')
    });
    $('.cambiartitulo_8').html('Equipo');
}
function loadcliente(){
    $('.tabla_asignar_3').show();
    $('.tabla_asignar_2').hide();
    $('.tabla_asignar_1').hide();
    $('.tabla_asignar_4').hide();
    //table1.destroy();
    //table2.destroy();
    table3.destroy();
    //table4.destroy();
    var tiposer = $('#tiposer option:selected').val();
    var tecnico = $('#tecnico option:selected').val();
    var status = $('#status option:selected').val();
    var estatus_ad = $('#estatus_ad option:selected').val();
    var zona = $('#zona option:selected').val();
    var pcliente = $('#pcliente option:selected').val();
    var noti = $('#noti option:selected').val();
        if(pcliente==undefined){
            pcliente=0;
        }
    var fechainicio = $('#fechainicio').val();
    var fechafin = $('#fechafin').val();
    table3 = $('#tabla_asignar_3').DataTable({
        fixedHeader: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        search: {
                return: true
            }, 
        "ajax": {
            url: base_url + "index.php/Listaservicios/getlistaserviciocl",
            type: "post",
            "data": {
                "tiposer":tiposer,
                "tecnico":tecnico,
                "status":status,
                "zona":zona,
                "pcliente":pcliente,
                "fechainicio":fechainicio,
                "fechafin":fechafin,
                "estatus_ad":estatus_ad,
                noti:noti
            },
            error: function() {
                $("#tabla_asignar").css("display", "none");
            }
        },
        "columns": [
            {"data": "asignacionId"},
            {"data": "servicio"},
            {"data": null,
                render: function(data,type,row){ 
                    var html='';
                    if(row.tecnico!=null){
                        html=row.tecnico;
                    }
                    if(row.tecnico2!=null){
                        html+='<br>'+row.tecnico2;
                    }
                    if(row.tecnico3!=null){
                        html+='<br>'+row.tecnico3;
                    }
                    if(row.tecnico4!=null){
                        html+='<br>'+row.tecnico4;
                    }
                    if(row.personalIdf>0){
                        html=row.tecnicof;
                    }
                    return html;
                }
            },
            {"data": "empresa"},
            {"data": "fecha"},
            {"data": null,
                render: function(data,type,row){ 
                    var html='';
                        html+=row.hora+' a '+row.horafin;
                        if(row.hora_comida!=null && row.horafin_comida!=null){
                            if(row.hora_comida!='' && row.horafin_comida!=''){
                                html+='<br>Comida: '+row.hora_comida+' a '+row.horafin_comida;
                            }
                        }
                    return html;
                }
            },
            //{"data": "nombre"},
            {"data": "servicio"},
            {"data": 'serie'},
            {"data": null,
                render: function(data,type,row){ 
                    if (row.status==0) {
                        var status='<p></p>';
                    }
                    if (row.status==1) {
                        var status='<p>En proceso</p>';
                    }
                    if (row.status==2) {
                        var status='<p style="color:green">Finalizado</p>';
                    }
                    if (row.status==3) {
                        var status='<p style="color:red">Suspendido</p>';
                    }
                        //var nombre="'"+row.modelo+"'";
                        var html=status;
                    

                    return html;
                }
            },
            {"data": null,
                render: function(data,type,row){ 
                    var html='';
                    return html;
                }
            },
            //{"data": 'direccion'},//aqui ira la direccion
            {"data": null,
                render: function(data,type,row){ 
                        var color_btn='';
                        if(row.t_fecha==null){
                            color_btn='cyan';
                        }else{
                            color_btn='red';
                        }
                        if(row.editado==1){
                            color_btn=' orange ';
                        }
                        //===========================
                        var disabled_date_edit='';
                        if(row.fecha==fechaactual){
                            if(row.personalId!=49){
                                if(row.nr==0){
                                    disabled_date_edit=' disabled ';
                                }
                            }
                        }
                        //=================================
                        //var nombre="'"+row.modelo+"'";
                        var html='';
                            html+='<button class="waves-effect '+color_btn+' btn-bmz servicio_'+row.asignacionId+'_3" onclick="editfecha('+row.asignacionId+',3,0)"';
                                html+='data-fecha="'+row.fecha+'" data-horalimite="'+row.hora_limite+'" data-horainicio="'+row.hora+'" data-horafin="'+row.horafin+'" data-horainiciocomida="'+row.hora_comida+'" data-horafincomida="'+row.horafin_comida+'" ';
                                html+='data-t_fecha="null" data-t_horainicio="" data-t_horafin="" '+disabled_date_edit+'>';
                                html+='<i class="fas fa-calendar-alt" style="font-size: 20px;"></i>';
                            html+='</button>';
                            if (row.status==3) {
                                html+='<a class="waves-effect cyan btn-bmz" onclick="suspenderactivar('+row.asignacionId+',0,3)" title="Activar">\
                                    <i class="fas fa-ban" style="font-size: 20px;"></i></a>';
                            }else{
                                html+='<a class="waves-effect cyan btn-bmz" onclick="suspender('+row.asignacionId+',0,3)" title="Suspender">\
                                    <i class="fas fa-ban" style="font-size: 20px;color:red"></i></a>';
                            }
                            
                            html+='<button class="waves-effect cyan btn-bmz" onclick="edittecnico('+row.asignacionIdd+','+row.asignacionId+','+row.personalId+','+row.personalId2+',3)" '+disabled_date_edit+'>\
                                    <i class="fas fa-users" style="font-size: 20px;"></i></button>';
                                    /*
                            html+='<a class="waves-effect cyan btn-bmz" onclick="venta('+row.asignacionId+',3)">\
                                    <i class="fas fa-cart-plus" style="font-size: 20px;"></i></a>';
                                    */
                            if(row.status==1 || row.status==2){
                                html+='<a class="waves-effect cyan btn-bmz btn-bmz s_status_'+row.asignacionIdd+'_3"\
                                         data-h_ser_i="'+row.horaserinicio+'" data-h_ser_f="'+row.horaserfin+'"\
                                         data-h_ser_iex="'+row.horaserinicio+'" data-h_ser_fex="'+row.horaserfin+'"\
                                         onclick="modal_estatus('+row.asignacionIdd+',3)">\
                                    <i class="fas fa-exclamation-circle" style="font-size: 20px;"></i></a>';        
                            }     
                            html+='<a class="waves-effect cyan btn-bmz" onclick="documento('+row.asignacionId+',3,'+row.asignacionIdd+')">\
                                    <i class="fas fa-file-alt" style="font-size: 20px;"></i></a>';
                            if(row.notificarerror_estatus==1){
                               html+='<a class="waves-effect red btn-bmz infoerror infoerror_3_'+row.asignacionIdd+'" onclick="infoerror('+row.asignacionIdd+',3)" data-detalle="'+row.notificarerror+'"><i class="material-icons">error</i></a>'; 
                            }else{
                                html+='<a class="waves-effect cyan btn-bmz infoerror infoerror_3_'+row.asignacionIdd+'" onclick="infoerror_confirm('+row.asignacionIdd+',3)" data-detalle="" title="Edicion de Equipo/serie"><i class="fas fa-exchange-alt"></i></a>'; 
                            }
                            html+='<a class="waves-effect cyan btn-bmz" onclick="deleteservicio('+row.asignacionId+',0,3)">\
                                    <i class="fas fa-trash-alt" style="font-size: 20px;color:red"></i></a>';
                            if (row.status==2) {
                                html+='<a class="waves-effect cyan btn-bmz editcomentariotec_'+row.asignacionId+'" data-comen="'+row.comentario+'" onclick="editcomentariotec('+row.asignacionId+',0,3)"><i class="fas fa-comment"></i></a>';
                            }
                            if(row.nr==1){
                               
                               html+='<a class="waves-effect red btn-bmz nrcoment_'+row.asignacionIdd+'" onclick="nrcoment('+row.asignacionIdd+',3,0,0)" data-nrcoment="'+row.nr_coment+'" data-nrcomentipo="'+row.nr_coment_tipo+'">Rechazo</a>'; 
                            }
                            if (row.status==2) {
                                //html+='<a class="waves-effect cyan btn-bmz" onclick="suspenderactivar('+row.asignacionIde+','+row.asignacionId+',1)" title="Activar"><i class="fas fa-ban" style="font-size: 20px;"></i></a>';
                                html+='<a class="waves-effect cyan btn-bmz" onclick="reactivar(0,'+row.asignacionId+',3)" title="Activar"><i class="fas fa-check" style="font-size: 20px;"></i></a>';
                            }
                            if(row.activo==0){
                                html=row.infodelete;
                            }
                    return html;
                }
            },
          ],
        order: [
            [0, "desc"]
          ]
        
    }).on('draw',function(){
        setTimeout(function(){ 
            //tabla_asignar_1
            var tablesearch = $('#tabla_asignar_3_filter input').val();
            $(".tabla_asignar_3").mark(tablesearch);
        }, 2000);
        $('.dataTables_filter').append('<i class="fa fa-search buttonsearch" onclick="searchtable_v3()"></i>')
    });
    $('.cambiartitulo_8').html('Equipo/Servicio');
}
function loadventas(){
    $('.tabla_asignar_2').hide();
    $('.tabla_asignar_3').hide();
    $('.tabla_asignar_1').hide();
    $('.tabla_asignar_4').show();
    //table1.destroy();
    //table2.destroy();
    //table3.destroy();
    table4.destroy();
    var tiposer = $('#tiposer option:selected').val();
    var tecnico = $('#tecnico option:selected').val();
    var status = $('#status option:selected').val();
    var estatus_ad = $('#estatus_ad option:selected').val();
    var zona = $('#zona option:selected').val();
    var pcliente = $('#pcliente option:selected').val();
    var noti = $('#noti option:selected').val();
        if(pcliente==undefined){
            pcliente=0;
        }
    var fechainicio = $('#fechainicio').val();
    var fechafin = $('#fechafin').val();
    table4 = $('#tabla_asignar_4').DataTable({
        fixedHeader: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        search: {
                return: true
            }, 
        "ajax": {
            url: base_url + "index.php/Listaservicios/getlistaserviciovent",
            type: "post",
            "data": {
                "tiposer":tiposer,
                "tecnico":tecnico,
                "status":status,
                "zona":zona,
                "pcliente":pcliente,
                "fechainicio":fechainicio,
                "fechafin":fechafin,
                "estatus_ad":estatus_ad,
                noti:noti
            },
            error: function() {
                $("#tabla_asignar").css("display", "none");
            }
        },
        "columns": [
            {"data": "asignacionId"},
            {"data": "servicio"},
            {"data": null,
                render: function(data,type,row){ 
                    var html='';
                    if(row.tecnico!=null){
                        html=row.tecnico;
                    }
                    if(row.tecnico2!=null){
                        html+='<br>'+row.tecnico2;
                    }
                    if(row.tecnico3!=null){
                        html+='<br>'+row.tecnico3;
                    }
                    if(row.tecnico4!=null){
                        html+='<br>'+row.tecnico4;
                    }
                    if(row.personalIdf>0){
                        html=row.tecnicof;
                    }

                    return html;
                }
            },
            {"data": "empresa"},
            {"data": "fecha"},
            {"data": null,
                render: function(data,type,row){ 
                        var html='';
                        html+=row.hora+' a '+row.horafin;
                        if(row.hora_comida!=null && row.horafin_comida!=null){
                            if(row.hora_comida!='' && row.horafin_comida!=''){
                                html+='<br>Comida: '+row.hora_comida+' a '+row.horafin_comida;
                            }
                        }
                    return html;
                }
            },
            {"data": "nombre","visible": false},
            {"data": null,"visible": false},
            {"data": null,"visible": false},
            {"data": null,
                render: function(data,type,row){ 
                    if (row.status==0) {
                        var status='<p></p>';
                    }
                    if (row.status==1) {
                        var status='<p>En proceso</p>';
                    }
                    if (row.status==2) {
                        var status='<p style="color:green">Finalizado</p>';
                    }
                    if (row.status==3) {
                        var status='<p style="color:red">Suspendido</p>';
                    }
                        //var nombre="'"+row.modelo+"'";
                        var html=status;
                    

                    return html;
                }
            },
            {"data": null,
                render: function(data,type,row){ 
                    var html='';
                    return html;
                }
            },
            {"data": null,"visible": false},//aqui ira la direccion
            {"data": null,
                render: function(data,type,row){ 
                        var color_btn='';
                        if(row.t_fecha==null){
                            color_btn='cyan';
                        }else{
                            color_btn='red';
                        }
                        if(row.editado==1){
                            color_btn=' orange ';
                        }
                        //var nombre="'"+row.modelo+"'";
                        //===========================
                        var disabled_date_edit='';
                        if(row.fecha==fechaactual){
                            if(row.personalId!=49){
                                if(row.nr==0){
                                    disabled_date_edit=' disabled ';
                                }
                            }
                        }
                        //=================================
                        var html='';
                            html+='<button class="waves-effect '+color_btn+' btn-bmz servicio_'+row.asignacionId+'_4" onclick="editfecha('+row.asignacionId+',4,0)"';
                                html+='data-fecha="'+row.fecha+'" data-horalimite="'+row.hora_limite+'" data-horalimite="'+row.hora_limite+'" data-horainicio="'+row.hora+'" data-horafin="'+row.horafin+'" data-horainiciocomida="'+row.hora_comida+'" data-horafincomida="'+row.horafin_comida+'" ';
                                html+='data-t_fecha="'+row.t_fecha+'" data-t_horainicio="'+row.t_horainicio+'" data-t_horafin="'+row.t_horafin+'" '+disabled_date_edit+'>';
                                html+='<i class="fas fa-calendar-alt" style="font-size: 20px;"></i>';
                            html+='</button>';
                            if (row.status==3) {
                                html+='<a class="waves-effect cyan btn-bmz" onclick="suspenderactivar('+row.asignacionId+',0,4)" title="Activar">\
                                    <i class="fas fa-ban" style="font-size: 20px;"></i></a>';
                            }else{
                                html+='<a class="waves-effect cyan btn-bmz" onclick="suspender('+row.asignacionId+',0,4)" title="Suspender">\
                                    <i class="fas fa-ban" style="font-size: 20px;color:red"></i></a>';    
                            }
                            
                            html+='<button class="waves-effect cyan btn-bmz" onclick="edittecnico(0,'+row.asignacionId+','+row.personalId+','+row.personalId2+',4)" '+disabled_date_edit+'>\
                                    <i class="fas fa-users" style="font-size: 20px;"></i></button>';
                                    /*
                            html+='<a class="waves-effect cyan btn-bmz" onclick="venta('+row.asignacionId+',4)">\
                                    <i class="fas fa-cart-plus" style="font-size: 20px;"></i></a>';
                                    */
                            if(row.status==1 || row.status==2){
                                html+='<a class="waves-effect cyan btn-bmz btn-bmz s_status_'+row.asignacionId+'_4"\
                                 data-h_ser_i="'+row.horaserinicio+'"\
                                  data-h_ser_f="'+row.horaserfin+'"\
                                 data-h_ser_iex="'+row.horaserinicioext+'"\
                                  data-h_ser_fex="'+row.horaserfinext+'"\
                                    onclick="modal_estatus('+row.asignacionId+',4)">\
                                    <i class="fas fa-exclamation-circle" style="font-size: 20px;"></i></a>';        
                            }     
                            html+='<a class="waves-effect cyan btn-bmz" onclick="documento('+row.asignacionId+',4,0)">\
                                    <i class="fas fa-file-alt" style="font-size: 20px;"></i></a>';
                            html+='<a class="waves-effect cyan btn-bmz" title="Prefactura" onclick="viewprefacturav('+row.ventaId+','+row.tipov+')">\
                                    <i class="fas fa-file" style="font-size: 20px;"></i></a>';
                            if(row.notificarerror_estatus==1){
                               html+='<a class="waves-effect red btn-bmz infoerror infoerror_4_'+row.asignacionId+'" onclick="infoerror('+row.asignacionId+',4)" data-detalle="'+row.notificarerror+'"><i class="material-icons">error</i></a>'; 
                            }else{
                                html+='<a class="waves-effect cyan btn-bmz infoerror infoerror_4_'+row.asignacionId+'" onclick="infoerror_confirm('+row.asignacionId+',4)" data-detalle="" title="Edicion de Equipo/serie"><i class="fas fa-exchange-alt"></i></a>'; 
                            }
                            html+='<a class="waves-effect cyan btn-bmz" onclick="deleteservicio('+row.asignacionId+',0,4)">\
                                    <i class="fas fa-trash-alt" style="font-size: 20px;color:red"></i></a>';
                            if (row.status==2) {
                                html+='<a class="waves-effect cyan btn-bmz editcomentariotec_'+row.asignacionId+'" data-comen="'+row.comentario+'" onclick="editcomentariotec('+row.asignacionId+',0,4)"><i class="fas fa-comment"></i></a>';
                            }
                            if(row.nr==1){
                               html+='<a class="waves-effect red btn-bmz nrcoment_'+row.asignacionId+'" onclick="nrcoment('+row.asignacionId+',4,1,'+row.not_dev+')" data-nrcoment="'+row.nr_coment+'" data-nrcomentipo="'+row.nr_coment_tipo+'">Rechazo</a>'; 
                            }
                            if(row.activo==0){
                                html=row.infodelete;
                            }
                    return html;
                }
            },
          ],
        order: [
            [0, "desc"]
          ],
        createdRow:function(row,data,dataIndex){
            $(row).addClass('poliza_'+data.ventaId);
        }

        
    }).on('draw',function(){
        setTimeout(function(){ 
            //tabla_asignar_1
            var tablesearch = $('#tabla_asignar_4_filter input').val();
            $(".tabla_asignar_4").mark(tablesearch);

            $('.poliza_'+tablesearch).addClass('activopintado');
        }, 2000);
        $('.dataTables_filter').append('<i class="fa fa-search buttonsearch" onclick="searchtable_v4()"></i>')
    });
}
function viewstock(id){
    var comen = $('.stock_toner_'+id).data('comen');
    $.alert({boxWidth: '30%',useBootstrap: false,title: 'Información del stock del cliente',theme: 'bootstrap',content: comen}); 
}
function nrcoment(row,tipo,apdev,statusdev){
    var coment = $('.nrcoment_'+row).data('nrcoment');
    var comentipo = $('.nrcoment_'+row).data('nrcomentipo');

    var htmlcompet='';
        if(comentipo==0){
            htmlcompet='Otro';
        }
        if(comentipo==1){
            htmlcompet='Backup';
        }
        if(comentipo==2){
            htmlcompet='No acudieron al sitio';
        }
        if(comentipo==3){
            htmlcompet='Sin acceso';
        }
        if(comentipo==4){
            htmlcompet='Oficina cerrada';
        }
        if(comentipo==5){
            htmlcompet='Intercambio de servicio';
        }
        var html='';
            html+='<label>Tipo de rechazo</label><br>'+htmlcompet+'<br>';
            html+='<label>Motivo</label><br>'+coment+'<br>';
            
            



            if(apdev==1){
                var estatus1='';
                if(statusdev==1){
                    estatus1=' checked ';
                }
                var estatus2='';
                if(statusdev==0){
                    estatus2=' checked ';
                }
            html+='<div class="row">';
                    html+='<div class="col s12"><label>Desea solicitar cancelación/Devolución de la pre</label></div>';
                    html+='<div class="col s12">';
                        html+='<input name="tasign" type="radio" class="tasign" id="asign1" value="1" '+estatus1+'>';
                        html+='<label for="asign1">Solicitar</label>';
                        html+='<input name="tasign" type="radio" class="tasign" id="asign2" value="2" '+estatus2+'>';
                        html+='<label for="asign2">No solicitar</label>';
                    html+='</div>';
            html+='</div>';
            }
        $.confirm({
            boxWidth: '30%',
            useBootstrap: false,
            icon: 'fa fa-warning',
            title: 'Motivo del rechazo',
            content: html,
            type: 'red',
            typeAnimated: true,
            buttons: 
            {
                confirmar: function (){
                    //===============
                    var tasign=$('input:radio[name=tasign]:checked').val();
                    if(tasign>0){
                        $.ajax({
                            type:'POST',
                            url: base_url+'index.php/Listaservicios/statuspredev',
                            data: {
                                status:tasign,
                                row:row,
                                tipo:tipo
                            },
                            success:function(data){
                                toastr.success('Se realizo la solicitud');      
                            }
                        });
                    }
                    
                    //=============
            },
                cancelar: function () 
                {
                }
            }
        });

}
function searchtable_v1(){
    var searchv =$("input[type=search]").val();
    table1.search(searchv).draw();
}
function searchtable_v2(){
    var searchv =$("input[type=search]").val();
    table2.search(searchv).draw();
}
function searchtable_v3(){
    var searchv =$("input[type=search]").val();
    table3.search(searchv).draw();
}
function searchtable_v4(){
    var searchv =$("input[type=search]").val();
    table4.search(searchv).draw();
}
function editfecha(idrow,table,asignacion){
    $('#asignacionId').val(asignacion);
    $('#modaleditfecha').modal('open');
    $('#idrow').val(idrow);
    $('#idtable').val(table);
    var fecha = $('.servicio_'+idrow+'_'+table).data('fecha');
    var hora = $('.servicio_'+idrow+'_'+table).data('horainicio');
    var horafin = $('.servicio_'+idrow+'_'+table).data('horafin');

    var hora_comida = $('.servicio_'+idrow+'_'+table).data('horainiciocomida');
    var horafin_comida = $('.servicio_'+idrow+'_'+table).data('horafincomida');
    if(hora_comida=='00:00:00'){
        hora_comida='';
    }
    if(horafin_comida=='00:00:00'){
        horafin_comida='';
    }

    var t_fecha = $('.servicio_'+idrow+'_'+table).data('t_fecha');
    var t_horainicio = $('.servicio_'+idrow+'_'+table).data('t_horainicio');
    var t_horafin = $('.servicio_'+idrow+'_'+table).data('t_horafin');
    $('#fecha').val(fecha);
    $('#hora').val(hora);
    $('#horafin').val(horafin);

    $('#hora_comida').val(hora_comida);
    $('#horafin_comida').val(horafin_comida);

    if(t_fecha==null){
        $('.re_agendar').html(''); 
    }else{
        $('.re_agendar').html('<h5>Re agendación</h5><div class="row">\
             <div class="col s4">\
              <strong>Fecha: '+ChangeDate(t_fecha)+'</strong>\
             </div>\
             <div class="col s4">\
              <strong>Hora incio: '+tConvert(t_horainicio)+'</strong>\
             </div>\
             <div class="col s4">\
              <strong>Hora fin: '+tConvert(t_horafin)+'</strong>\
             </div>\
          </div>');
    }    
}
function ChangeDate(data){
  var parte=data.split('-');
  return parte[2]+"-"+parte[1]+"-"+parte[0];
}
function tConvert(time){
  // Check correct time format and split into components
  time = time.toString ().match (/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

  if (time.length > 1) { // If time format correct
    time = time.slice (1);  // Remove full string match value
    time[5] = +time[0] < 12 ? 'AM' : 'PM'; // Set AM/PM
    time[0] = +time[0] % 12 || 12; // Adjust hours
  }
  return time.join (''); // return adjusted time or original string
}
function editarfecha(){
    $.ajax({
        type:'POST',
        url: base_url+"Listaservicios/editarfecha",
        data: {
            idrow: $('#idrow').val(),
            idasignacion: $('#asignacionId').val(),
            table: $('#idtable').val(),
            fecha: $('#fecha').val(),
            hora: $('#hora').val(),
            horafin: $('#horafin').val(),
            hora_comida: $('#hora_comida').val(),
            horafin_comida: $('#horafin_comida').val(),
            tipo: $('input:radio[name=modifife]:checked').val(),
        },
        success: function (data){
            load();
        }
    });
}
function edittecnico(idrow,idrowt,personal,personal2,table){
    var idsertr=idrowt;
    if(table==1){
        idsertr=idrow;
    }
    if(table==2){
        idsertr=idrow;
    }
    if(table==3){
        idsertr=idrowt;
    }
    if(table==4){
        idsertr=idrowt;
    }
    var horalimite = $('.servicio_'+idsertr+'_'+table).data('horalimite');
    var fechaservicio = $('.servicio_'+idsertr+'_'+table).data('fecha');
    console.log(horalimite);
    var status_h=verificar_lahoralimite(horalimite,fechaservicio);
    if(status_h==0){
        $('#modalAsigna').modal('open');
        $('#idrow').val(idrow);
        $('#asignacionId').val(idrowt);
        $('#idtable').val(table);
        $('#tecnicose').val(personal).change();
        $('#tecnicose2').val(personal2).change();
        $('#tecnicooriginal').val(personal);
        if(table==1){
            idrowt=idrow;
        }
        viewhistorial(idrowt,table);
    }else{
        $.alert({boxWidth: '30%',useBootstrap: false,title: 'Advertencia',theme: 'bootstrap',content: 'El servicio esta por concluir'});
    }
}
function suspender(idrow,idrowt,table){
    $('#modalsuspender').modal('open');
    $('#idrow').val(idrow);
    $('#idtable').val(table);
}
function editartecnicos(){
    var tecnicose2=$('#tecnicos2 option:selected').val();
    var tecnicose3=$('#tecnicos3 option:selected').val();
    var tecnicose4=$('#tecnicos4 option:selected').val();
    console.log(tecnicose2);
    $.ajax({
        type:'POST',
        url: base_url+"Listaservicios/edittecnico",
        data: {
            idrow: $('#idrow').val(),
            idrowt: $('#asignacionId').val(),
            table: $('#idtable').val(),
            tipo: $('input:radio[name=modifi]:checked').val(),
            tecnicoo: $('#tecnicooriginal').val(),
            tecnico: $('#tecnicose option:selected').val(),
            tecnico2: tecnicose2,
            tecnico3: tecnicose3,
            tecnico4: tecnicose4,
        },
        success: function (data){
            load();
            $.alert({boxWidth: '30%',useBootstrap: false,title: '',theme: 'bootstrap',content: 'Cambio de tecnico exitoso'});  
            verificar_nuevos_servicios_ext();
        }
    });
}
/*
function editartecnicos(){
    $.ajax({
        type:'POST',
        url: base_url+"Listaservicios/edittecnico",
        data: {
            idrow: $('#idrow').val(),
            idrowt: $('#idtable').val(),
            fecha: $('#fecha').val(),
            hora: $('#hora').val(),
            horafin: $('#horafin').val(),
        },
        success: function (data){
            load();
        }
    });
} */
function viewhistorial(idrow,table){
    $.ajax({
        type:'POST',
        url: base_url+"Listaservicios/viewhistorial",
        data: {
            idrow: idrow,
            table: table
        },
        success: function (data){
            $('.historicotecnicos').html(data);
        }
    });
}
function suspenderok(){
    var motivo = $('#tserviciomotivo').val();
    if (motivo!='') {
        $.ajax({
            type:'POST',
            url: base_url+"Listaservicios/suspender",
            data: {
                idrow: $('#idrow').val(),
                table: $('#idtable').val(),
                tserviciomotivo: motivo,
            },
            success: function (data){
                load();
            }
        });
    }else{
        swal("Error", "Motivo no debe de estar", "Advertencia"); 
    }
    
}
function suspenderactivar(idrow,idrowt,table){
        $.ajax({
            type:'POST',
            url: base_url+"Listaservicios/suspenderactivar",
            data: {
                idrow: idrow,
                table: table,
            },
            success: function (data){
                toastr["success"]("Activando Servicio", "Hecho");
                load();
            }
        });
}
function deleteservicio(idrow,idrowt,table){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Desea eliminar el servicio?<br>'+
                 ' <input type="radio" class="delete_check" name="delete_check" id="delete_check_1" value="1" checked><label for="delete_check_1" class="delete_check_label">Individual</label> '+
                 ' <input type="radio" class="delete_check" name="delete_check" id="delete_check_2" value="2"><label for="delete_check_2" class="delete_check_label">General</label>',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var delete_check = $('.delete_check:checked').val();
                 $.ajax({
                    type:'POST',
                    url: base_url+"Listaservicios/deleteservicios",
                    data: {
                        idrow: idrow,
                        idrowt:idrowt,
                        table: table,
                        tipodelete: delete_check,
                    },
                    success: function (data){
                        console.log(data);
                        var array = $.parseJSON(data);
                        if(array.estado==0){
                            toastr.error('No se pudo eliminar');
                        }
                        if(array.estado==1){
                            toastr.success('Servicio eliminado');
                        }
                        if(array.estado==2){
                            toastr.error('El servicio tiene una factura, no se elimino la partida de la prefactura');
                        }
                        load();
                    }
                });
                        
            },
            cancelar: function () 
            {
                
            }
        }
    });
    setTimeout(function(){ 
        if(table==3 || table==4){
            $('.delete_check_label').hide();
        }
    }, 1000); 
}
//======================================
function venta(id,tipo){
    $('.totalg').html(0);
    $('#modalventa').modal('open');
    viewdatosventaservicio(id,tipo);
}
function viewdatosventaservicio(id,tipo){
    editarar=0;
    $('#vmidasignacion').val(id);
    $('#vmidasignaciontipo').val(tipo);
    $('.tabaddconsul').html('');
    $('.taaddrefaccion').html('');
    $.ajax({
        type:'POST',
        url: base_url+"Listaservicios/datosventaservicio",
        data: {
          id:id,
          tipo:tipo
        },
        success: function (data){
          console.log(data);
          var array = $.parseJSON(data);
          var datos=array.datos;          
          array.consumibles.forEach(function(element) {
                editarar=1;
                var descripcion="'"+element.modelo+" ("+element.parte+")'";
                addconsuview(element.asId,element.cantidad,element.id,descripcion,element.costo);
          });
          array.refacciones.forEach(function(element) {
                editarar=1;
                var descripcion="'"+element.nombre+" ("+element.codigo+")'";
                addrefaview(element.asId,element.cantidad,element.id,descripcion,element.costo);
          });
   
      
        }
    }); 
}
function funcionConsumiblesprecios(aux){
    var toner = $('#sconcumibles option:selected').val();
    
    $.ajax({
        url: base_url+"index.php/Cotizaciones/getDatosConsumiblesPorEquipop/"+toner,
        success: function (response){
            var array = $.parseJSON(response); 
            $('#sconcumiblesp').html('');
            $('#sconcumiblesp').html(array.select_option);
            //aux.find('.tonerp').html('');
            //aux.find('.tonerp').html(response);
            $('#sconcumiblesp').trigger("chosen:updated");
        },
        error: function(response){
            alertfunction('Atención!','Algo salió mal, intente de nuevo o contacte al administrador del sistema');
        }
    }); 
}
function selectprecior(){
    var id = $('#srefaccions option:selected').val();
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Cotizaciones/mostrarselectpreciosr",
        data: {id:id},
        success: function (data){
            var array = $.parseJSON(data); 
            $('#srefaccionp').html('');
            $('#srefaccionp').html(array.select_option);   
        },
        error: function(data){ 
            alertfunction('Atención!','Algo salió mal, intente de nuevo o contacte al administrador del sistema');
        }
    });
}
var rowventa=0;
function addconsu(){
    var cant= $('#cantidadc').val();
    var consumi= $('#sconcumibles option:selected').val();
    var consumit= $('#sconcumibles option:selected').text();
    var precio= $('#sconcumiblesp option:selected').val();
    addconsuview(0,cant,consumi,consumit,precio);
}
function addconsuview(row,cant,con,cont,precio){
    var tabless ='<tr class="rowventa_'+rowventa+'_'+row+'">\
                <td><input id="addcantidadc" value="'+cant+'" style="background: transparent;border: 0px;margin: 0px;width: 92px;" >\
                <input type="hidden" id="idtable" value="'+row+'" style="">\
                </td>\
                <td><input type="hidden" id="addcantidadr" value="'+con+'" style="">'+cont+'</td>\
                <td><input id="addcantidadp" value="'+precio+'" class="precio" style="background: transparent;border: 0px;margin: 0px;width: 92px;" readonly></td>\
                <td>\
                  <a class="btn-floating red tooltipped elimina" onclick="deteteven('+rowventa+','+row+',1)"><i class="material-icons">delete_forever</i></a></td>\
              </tr>';
  $('.tabaddconsul').append(tabless);
  rowventa++;
  calculartotal();
}
function addrefa(){
    var cant=$('#cantidadr').val();
    var refaccion=$('#srefaccions option:selected').val();
    var refacciont=$('#srefaccions option:selected').text();
    var precio=$('#srefaccionp option:selected').val();
    addrefaview(0,cant,refaccion,refacciont,precio);
}
function addrefaview(row,cant,ref,reft,precio){
    var tabless ='<tr class="rowventa_'+rowventa+'_'+row+'">\
                <td><input id="addrefaccionc" value="'+cant+'" style="background: transparent;border: 0px;margin: 0px;width: 92px;" >\
                <input type="hidden" id="idtable" value="'+row+'" style="">\
                </td>\
                <td><input type="hidden" id="addrefaccionr" value="'+ref+'" style="">'+reft+'</td>\
                <td><input id="addrefaccionp" value="'+precio+'" class="precio" style="background: transparent;border: 0px;margin: 0px;width: 92px;" readonly></td>\
                <td>\
                  <a class="btn-floating red tooltipped elimina" onclick="deteteven('+rowventa+','+row+',2)"><i class="material-icons">delete_forever</i></a></td>\
              </tr>';
    $('.taaddrefaccion').append(tabless);
    rowventa++;
    calculartotal();
}
function deteteven(row,rowid,tipo){
  
  
  if (rowid>0) {
    deleteacre(row,rowid,tipo);
  }else{
    $('.rowventa_'+row+'_'+rowid).remove();
    calculartotal();
  }
}
function calculartotal(){
    var addtp = 0;
    $(".precio").each(function() {
        var vstotal = $(this).val();
        addtp += Number(vstotal);
    });
    $('.totalg').html(new Intl.NumberFormat('es-MX').format(addtp));
}
function deleteacre(row,rowid,tipo){
    $.confirm({
            boxWidth: '30%',
            useBootstrap: false,
            icon: 'fa fa-warning',
            title: 'Atención!',
            content: '¿Está seguro de Eliminar?',
            type: 'red',
            typeAnimated: true,
            buttons: 
            {
                confirmar: function (){
                    //===============
                    $.ajax({
                          type:'POST',
                          url: base_url+'index.php/Listaservicios/deleteacre',
                          data: {
                            rowid:rowid,
                            tipo:tipo
                            },
                          success:function(data){
                              $.alert({
                                boxWidth: '30%',
                                useBootstrap: false,
                                title: 'Atención!',
                                content: 'Eliminado correctamente'}
                              );
                                $('.rowventa_'+row+'_'+rowid).remove();
                                calculartotal();
                          }
                      });
                    //=============
            },
                cancelar: function () 
                {
                }
            }
        });
}
function savemodalventa(){
    //=============================================
              var consumi = [];
              var TABLAc   = $("#tableaddconsulmibles tbody > tr");
                TABLAc.each(function(){  
                        itemc = {};
                        itemc['id'] = $(this).find("input[id*='idtable']").val();
                        itemc['cantidad'] = $(this).find("input[id*='addcantidadc']").val();
                        itemc['consumible'] = $(this).find("input[id*='addcantidadr']").val();
                        itemc['precio'] = $(this).find("input[id*='addcantidadp']").val();
                        
                        consumi.push(itemc);
                });
                aInfoc   = JSON.stringify(consumi);
              //=============================================
              //=============================================
              var refaccion = [];
              var TABLAc   = $("#tableaddrefacciones tbody > tr");
                TABLAc.each(function(){  
                        itemc = {};
                        itemc['id'] = $(this).find("input[id*='idtable']").val();
                        itemc['cantidad'] = $(this).find("input[id*='addrefaccionc']").val();
                        itemc['refaccion'] = $(this).find("input[id*='addrefaccionr']").val();
                        itemc['precio'] = $(this).find("input[id*='addrefaccionp']").val();
                        
                        refaccion.push(itemc);
                });
                aInfor   = JSON.stringify(refaccion);
              //=============================================
          var tasign = $('input[name=tasign]:checked').val();//1pre asignacion 2 asginacion
          var tipo = $('input[name=prasignaciontipo]:checked').val();//1 contrato 2 poliza
            $.ajax({
                  type:'POST',
                  url: base_url+'index.php/Listaservicios/editarventaservicio',
                  data: {
                    tasign:$('#vmidasignacion').val(),
                    tipo:$('#vmidasignaciontipo').val(),
                    vconsumible:aInfoc,
                    vrefaccion:aInfor
                    },
                  success:function(data){
                      $.alert({
                        boxWidth: '30%',
                        useBootstrap: false,
                        title: 'Atención!',
                        content: 'Edicion correcta'}
                      );
                      $('#modalventa').modal('close');
                  }
              });
}
function modal_estatus(idrow,tipo){
    $('.text_h_i').html('');
    $('.text_h_f').html('');
    $('.text_h_t').html('');

    $('.text_h_iex').html('');
    $('.text_h_fex').html('');
    $('.text_h_t').html('');

    var text_h_i = $('.s_status_'+idrow+'_'+tipo).data('h_ser_i');
    var text_h_f = $('.s_status_'+idrow+'_'+tipo).data('h_ser_f');
    

    var text_h_iex = $('.s_status_'+idrow+'_'+tipo).data('h_ser_iex');
    var text_h_fex = $('.s_status_'+idrow+'_'+tipo).data('h_ser_fex');
    if (text_h_iex==null || text_h_iex=='undefined') {
        $('.tiempoextras').hide();
    }else{
        $('.tiempoextras').show();
    }
    $('#modal_lis_ser').modal('open');
    if(text_h_i!=null || text_h_i!='undefined'){
        $('.text_h_i').html(tConvert(text_h_i));
    }
    if(text_h_f!=null || text_h_f!='undefined'){
        $('.text_h_f').html(tConvert(text_h_f));
        var diferencia = diferenciastiempo(text_h_f,text_h_i);
        $('.text_h_t').html(diferencia);
    }

    if(text_h_iex!=null || text_h_iex!='undefined'){
        $('.text_h_iex').html(tConvert(text_h_iex));
    }
    if(text_h_fex!=null || text_h_fex!='undefined'){
        $('.text_h_fex').html(tConvert(text_h_fex));
        var diferenciaex = diferenciastiempo(text_h_fex,text_h_iex);
        $('.text_h_tex').html(diferenciaex);
    }
}
function documento(idasig,tipo,equipo){
    if(tipo==1){
        window.open(base_url+"Listaservicios/contrato_formato/"+idasig+"/"+tipo+"/"+equipo, "Prefactura", "width=780, height=612");
    }else if(tipo==2){
        window.open(base_url+"Listaservicios/tipo_formato/"+idasig+"/"+tipo+"/"+equipo, "Prefactura", "width=780, height=612");
    }else if(tipo==3){
        window.open(base_url+"Listaservicios/tipo_formatoc/"+idasig+"/"+tipo+"/"+equipo, "Prefactura", "width=780, height=612");
    }else if(tipo==4){
        window.open(base_url+"Listaservicios/tipo_formatovent/"+idasig+"/"+tipo+"/"+equipo, "Prefactura", "width=780, height=612");
    }
} 
function diferenciastiempo(horainicio,horafin){
    var hora1 = horainicio.split(":"),
        hora2 = horafin.split(":"),
        
        t1 = new Date(),
        t2 = new Date();
 
        t1.setHours(hora1[0], hora1[1], hora1[2]);
        t2.setHours(hora2[0], hora2[1], hora2[2]);
 
        //Aquí hago la resta
        t1.setHours(t1.getHours() - t2.getHours(), t1.getMinutes() - t2.getMinutes(), t1.getSeconds() - t2.getSeconds());
 
        //Imprimo el resultado
       return "" + (t1.getHours() ? t1.getHours() + (t1.getHours() > 1 ? " horas" : " hora") : "") + (t1.getMinutes() ? ", " + t1.getMinutes() + (t1.getMinutes() > 1 ? " minutos" : " minuto") : "") + (t1.getSeconds() ? (t1.getHours() || t1.getMinutes() ? " y " : "") + t1.getSeconds() + (t1.getSeconds() > 1 ? " segundos" : " segundo") : "");
}
function exportar(){
    var tipo = $('#serv option:selected').val();
    var tiposer = $('#tiposer option:selected').val();
    var status = $('#status option:selected').val();
    var zona = $('#zona option:selected').val();
    var tecnico = $('#tecnico option:selected').val();
    var cliente = $('#pcliente option:selected').val();
    var fechainicio = $('#fechainicio').val();
    var fechafin = $('#fechafin').val();
    //if(tipo>0){
        //window.open(base_url+'Listaservicios/exportar/'+tipo+'/'+tecnico+'/'+cliente+'/'+fechainicio+'/'+fechafin, '_blank');
        window.open(base_url+'Listaservicios/exportar?tipo='+tipo+'&tecnico='+tecnico+'&cliente='+cliente+'&finicio='+fechainicio+'&ffin='+fechafin+'&tiposer='+tiposer+'&status='+status+'&zona='+zona, '_blank');
    /*
    }else{
        $.alert({
            boxWidth: '30%',
            useBootstrap: false,
            title: 'Atención!',
            content: 'Seleccione un servicio'}
          );
    }
    */
}
function obtenerdireccionescontrato(equipo,serie){
    $.ajax({
      type:'POST',
      url: base_url+'index.php/Calendario/obtenerdireccionescontrato',
      data: {
        equipo:equipo,
        serie:serie
        },
      success:function(data){
          $('.equipo_'+equipo+'_serie_'+serie).html(data);
      }
  });
}
toastr.options = {
  "closeButton": false,
  "debug": false,
  "newestOnTop": false,
  "progressBar": true,
  "positionClass": "toast-top-right",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "1000",
  "timeOut": "5000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}
function viewprefacturav(id,tipo){
    if (tipo==0) {
        window.open(base_url+"Prefactura/view/"+id, "Prefactura", "width=780, height=612");
    }else{
        window.open(base_url+"Prefactura/viewc/"+id, "Prefactura", "width=780, height=612");
    }
}
function viewprefacturap(id){
    window.open(base_url+"PolizasCreadas/view/"+id, "Prefactura", "width=780, height=612");
}
function sombreartexto(texto){
    $("#tabla_asignar_1").mark('R4P1682408');
}
function modalbuscarserie(){

    $('#modal_seach_serie').modal('open');
    //$('.seach_serie_tbody').html('');
}
function buscarserielimpiar(){
    $('.seach_serie_tbody').html('');
    $('#input_search').val('')
}
function buscarserie(){
    if($('#input_search').val().length>0){

        $('#modal_procesando').modal('open');
        
        $.ajax({
              type:'POST',
              url: base_url+'index.php/Listaservicios/buscarserie',
              data: {
                serie:$('#input_search').val(),
                tipoview:0
                },
              success:function(data){
                  $('.seach_serie_tbody').html(data);
                  $('#tabla_seach').DataTable();
                  $('#tabla_seach0').DataTable();
                  $('#tabla_seach1').DataTable();
                  $('.collapsible').collapsible();
                  setTimeout(function(){ 

                    $('#modal_procesando').modal('close');
                  }, 1000);
                  setTimeout(function(){ 

                    //$('#modal_seach_serie').modal('open');
                  }, 1000);   
              }
        });
        
    }
}
function dirigiraservicio(row){
    var stiposervicio = $('.dirigiraservicio_'+row).data('stiposervicio');
    var scliente = $('.dirigiraservicio_'+row).data('scliente');
    var sclienteid = $('.dirigiraservicio_'+row).data('sclienteid');
    var sfecha = $('.dirigiraservicio_'+row).data('sfecha');

    $('#fechainicio').val(sfecha);
    $('#fechafin').val(sfecha);
    $('#pcliente').html('<option value="'+sclienteid+'">'+scliente+'</option>');
    setTimeout(function(){ 
        $('#serv').val(stiposervicio).change().trigger("chosen:updated");
        load();
        $('#modal_seach_serie').modal('close');
    }, 1000);

}
function infoerror(id,tipo){
    var detalle=$('.infoerror_'+tipo+'_'+id).data('detalle');
    $('#modal_notificacion').modal('open');
    $('.detallenotificacion').html(detalle);
    var html_btn='';
    if(tipo==1){
        html_btn+='<a class="waves-effect cyan btn-bmz" onclick="cambiar('+id+',1)">Cambiar Equipo</a>';
    }
    if(tipo==2){
        html_btn+='<a class="waves-effect cyan btn-bmz" onclick="cambiar('+id+',2)">Cambiar Serie</a>';
    }
    if(tipo==3){
        html_btn+='<a class="waves-effect cyan btn-bmz" onclick="cambiar('+id+',3)">Cambiar Serie</a>';
    }
    if(tipo==4){
        html_btn+='<a class="waves-effect cyan btn-bmz" onclick="cambiar('+id+',4)">Cambiar Serie</a>';
    }
        html_btn+='<a class="waves-effect cyan btn-bmz" onclick="descartar('+id+',1)">Descartar</a>';
    $('.funcionesnotificacion').html(html_btn); 
}
function infoerror_confirm(id,tipo){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Desea realizar un cambio de equipo/serie?<br>Se necesita permisos de administrador<br>'+
                 '<input type="text" placeholder="Contraseña" id="contrasena1" name="contrasena1" class="name form-control-bmz" style="width:99%" required/>',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){

                //var pass=$('#contrasena').val();
                var pass=$("input[name=contrasena1]").val();
                if (pass!='') {
                    $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Sistema/solicitarpermiso",
                        data: {
                            pass:pass
                        },
                        success: function (response){
                                var respuesta = parseInt(response);
                                if (respuesta==1) {
                    //=============================================================================
                                infoerror(id,tipo)
                    //=============================================================================
                                }else{
                                    notificacion(2);
                                }
                        },
                        error: function(response){  
                             notificacion(1);
                        }
                    });

                }else{
                    notificacion(0);
                }
                
            },
            cancelar: function (){
                
            }
        }
    });
    setTimeout(function(){
            new MaskedPassword(document.getElementById("contrasena1"), '\u25CF');
        },1000);
}
function descartar(id,tipo){        
        $.ajax({
              type:'POST',
              url: base_url+'index.php/AsignacionesTecnico/descartarcomentarios_error',
              data: {
                id:id,
                tipo:tipo
                },
              success:function(data){
                    $('#modal_notificacion').modal('close');
              }
        });
}
function cambiar(id,tipo){
    $.ajax({
          type:'POST',
          url: base_url+'index.php/Listaservicios/cambiarnotificacion',
          data: {
            id:id,
            tipo:tipo
            },
          success:function(data){
            $('.funciones2notificacion').html(data); 
          }
    });
}
function cambiarequipocontrato(id){
    $.confirm({
            boxWidth: '30%',
            useBootstrap: false,
            icon: 'fa fa-warning',
            title: 'Atención!',
            content: '¿confirma el cambio de equipo?',
            type: 'red',
            typeAnimated: true,
            buttons: 
            {
                confirmar: function (){
                    //===============
                    //=============================================
                    var TABLA   = $("#tableequipos tbody > tr");
                    var TABLAlength_equipos=0;
                    var select_cambio_equipo;
                    var select_cambio_serie;
                      TABLA.each(function(){  
                          if ($(this).find("input[class*='serie_check']").is(':checked')) {
                              select_cambio_equipo = $(this).find("input[id*='equipoid']").val();
                              select_cambio_serie = $(this).find("input[id*='serieid']").val();
                              
                              TABLAlength_equipos++;
                          }
                      });
                  //=============================================
                      if(TABLAlength_equipos==1){
                            var urlv=base_url+'index.php/Listaservicios/cambiarequipocontrato';
                            console.log(urlv);
                            $.ajax({
                                  type:'POST',
                                  url: urlv,
                                  data: {
                                    id:id,
                                    equipo:select_cambio_equipo,
                                    serie:select_cambio_serie
                                    },
                                  success:function(data){
                                    console.log(data);
                                      toastr["success"]("Cambiado correctamente", "Hecho");
                                  }
                              });
                        }else{
                            toastr["error"]("Solo seleccionar un equipo", "Error");
                        }
                    //=============
                },
                cancelar: function () 
                {
                }
            }
        });
}
function cambiarseriepoliza(id){
    $.confirm({
            boxWidth: '30%',
            useBootstrap: false,
            icon: 'fa fa-warning',
            title: 'Atención!',
            content: '¿confirma el cambio de serie?',
            type: 'red',
            typeAnimated: true,
            buttons: 
            {
                confirmar: function (){
                    
                  //=============================================
                            var urlv=base_url+'index.php/Listaservicios/cambiarseriepoliza';
                            console.log(urlv);
                            $.ajax({
                                  type:'POST',
                                  url: urlv,
                                  data: {
                                    id:id,
                                    serie:$('#nuevaserie').val()
                                    },
                                  success:function(data){
                                    console.log(data);
                                      toastr["success"]("Cambiado correctamente", "Hecho");
                                  }
                              });
                        
                    //=============
                },
                cancelar: function () 
                {
                }
            }
        });
}
function cambiarserieevento(id){
    $.confirm({
            boxWidth: '30%',
            useBootstrap: false,
            icon: 'fa fa-warning',
            title: 'Atención!',
            content: '¿confirma el cambio de serie?',
            type: 'red',
            typeAnimated: true,
            buttons: 
            {
                confirmar: function (){
                    
                  //=============================================
                            var urlv=base_url+'index.php/Listaservicios/cambiarserieevento';
                            console.log(urlv);
                            $.ajax({
                                  type:'POST',
                                  url: urlv,
                                  data: {
                                    id:id,
                                    serie:$('#nuevaserie').val()
                                    },
                                  success:function(data){
                                    console.log(data);
                                      toastr["success"]("Cambiado correctamente", "Hecho");
                                  }
                              });
                        
                    //=============
                },
                cancelar: function () 
                {
                }
            }
        });
}
function cambiarserieventa(id){
    $.confirm({
            boxWidth: '30%',
            useBootstrap: false,
            icon: 'fa fa-warning',
            title: 'Atención!',
            content: '¿confirma el cambio de serie?',
            type: 'red',
            typeAnimated: true,
            buttons: 
            {
                confirmar: function (){
                    
                  //=============================================
                            var urlv=base_url+'index.php/Listaservicios/cambiarserieventa';
                            console.log(urlv);
                            $.ajax({
                                  type:'POST',
                                  url: urlv,
                                  data: {
                                    id:id,
                                    serie:$('#nuevaserie').val()
                                    },
                                  success:function(data){
                                    console.log(data);
                                      toastr["success"]("Cambiado correctamente", "Hecho");
                                  }
                              });
                        
                    //=============
                },
                cancelar: function () 
                {
                }
            }
        });
}
function noerror(tipo){
    $('#noti').val(1);
    $('#fechafin').val('');
    $('#fechainicio').val('');
    setTimeout(function(){ 
        $('#serv').val(tipo).change();
        load();
    }, 1000);

}
function editcomentariotec(asig,asigd,tipo){
    var comen=$('.editcomentariotec_'+asig).data('comen');
    var inputcom='<br><textarea class="form-control-bmz" id="complemento"></textarea>';
    $.confirm({
            boxWidth: '30%',
            useBootstrap: false,
            icon: 'fa fa-warning',
            title: 'Atención!',
            content: '¿Desea complementar el comentario?<br>'+comen+inputcom,
            type: 'red',
            typeAnimated: true,
            buttons: 
            {
                confirmar: function (){
                    
                  //=============================================
                    var urlv=base_url+'index.php/Listaservicios/editcomentariotec';
                    console.log(urlv);
                    $.ajax({
                          type:'POST',
                          url: urlv,
                          data: {
                            asig:asig,
                            asigd:asigd,
                            tipo:tipo,
                            com:$('#complemento').val()
                            },
                          success:function(data){
                            console.log(data);
                              toastr["success"]("Cambiado correctamente", "Hecho");
                          }
                      });
                        
                    //=============
                },
                cancelar: function () 
                {
                }
            }
        });
}
function reactivar(ide,id,tipo){
    var html='';
        html+='¿Confirma de reactivar el servicio?<br>';
        html+='<div class="col s12 m12 l12">\
                  <input name="modireac" type="radio" id="modireac0" value="0">\
                  <label for="modireac0">General</label>\
                  <input name="modireac" type="radio" id="modireac1" value="1" checked>\
                  <label for="modireac1">Individual</label>\
               </div>';
        html+='<div class="switch"><br>\
                                  <label>Agregar rechazo\
                                    <input type="checkbox" name="re_rechazo" id="re_rechazo"  onclick="re_rechazo()">\
                                    <span class="lever"></span>\
                                  </label>\
                            </div>'; 
        html+='<div class="col s12 m12 l12 seespecificarechazo" style="display:none"><div>\
                <label>Tipo</label>\
                <select id="comtipo" class="form-control-bmz">\
                <option value="0">Otro</option>\
                <option value="1">backup</option>\
                <option value="2">no se acudio al sitio</option>\
                <option value="3">sin acceso</option>\
                <option value="4">oficina cerrada</option>\
                <option value="5">intercambio de servicio</option>\
                </select>\
                <label>Motivo</label>\
                <textarea class="form-control-bmz" id="notificarerror"></textarea>\
                </div></div>';
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: html,
        type: 'red',
        typeAnimated: true,
        buttons: 
        {
            confirmar: function (){
                
              //=============================================
                    var info=$('#notificarerror').val();
                    var comtipo=$('#comtipo  option:selected').val();
                    var indiglob=$('input:radio[name=modireac]:checked').val()
                    var re_rechazo = $('#re_rechazo').is(':checked')==true?1:0;
                        var urlv=base_url+'index.php/AsignacionesTecnico/reactivarser';
                        console.log(urlv);
                        $.ajax({
                              type:'POST',
                              url: urlv,
                              data: {
                                serv:id,
                                serve:ide,
                                tipo:tipo,
                                info:info,
                                comtipo:comtipo,
                                indiglob:indiglob,
                                rechazo:re_rechazo
                                },
                              success:function(data){
                                console.log(data);
                                  toastr["success"]("Cambiado correctamente", "Hecho");
                                  load();
                              }
                          });
                    
                //=============
            },
            cancelar: function () 
            {
            }
        }
    });
}
function re_rechazo(){
    setTimeout(function(){ 
        var re_rechazo = $('#re_rechazo').is(':checked')==true?1:0;
        if(re_rechazo==1){
            $('.seespecificarechazo').show('show');
        }else{
            $('.seespecificarechazo').hide('show');
        }
    }, 800);
}
function verificar_lahoralimite(horastring,fechaser){
    var estatus=0;
    var fechaactual=$('#fechaactual').val();
    if(fechaactual==fechaser){
        var horaActual = new Date();

        // Crear una cadena con la hora que deseas comparar (en formato 'HH:mm:ss')
        var horaCompararStr = horastring; // Cambia esto por la hora que necesites comparar

        // Dividir la cadena en horas, minutos y segundos
        var partesHora = horaCompararStr.split(':');
        var horas = parseInt(partesHora[0]);
        var minutos = parseInt(partesHora[1]);
        var segundos = parseInt(partesHora[2]);

        // Crear un nuevo objeto Date con la hora deseada
        var horaComparar = new Date();
        horaComparar.setHours(horas);
        horaComparar.setMinutes(minutos);
        horaComparar.setSeconds(segundos);

        // Verificar si la hora deseada es menor que la hora actual
        if (horaComparar < horaActual) {
            console.log("La hora deseada es menor que la hora actual.");
            estatus=1;
        } else {
            console.log("La hora deseada es igual o mayor que la hora actual.");
            estatus=0;
        }
    }
    return estatus;
}
//-----------------------------
function imprimir_ser(){
    var input_check='';
    $('#tecnico option').each(function() {
        var value = $(this).val();
        var text = $(this).text();

        // Crear el checkbox y su label

        var checkbox = $('<input type="checkbox" id="tec_' + value + '" name="tec[]" value="' + value + '">');
        var label = $('<label for="tec_' + value + '">' + text + '</label><br>'); // <br> para salto de línea

        // Agregar al contenedor
        if(value!=0 && value!='XXX'){
            input_check += '<li>'+checkbox.prop('outerHTML') + label.prop('outerHTML')+'</li>';
        }
    });
    var html='';
        html+='<div class="row">';
            html+='<div class="col s12">';
                html+='<label>Seleccione fecha</label>';
                html+='<input type="date" id="report_date" class="fc-bmz" value="'+fechaactual+'" />';
            html+='</div>';
            html+='<div class="col s6">';
                
                html+='<input type="radio" id="select_tec_0" name="select_tec" value="0" class="fc-bmz" checked onchange="ser_opt(0)" />';
                html+='<label for="select_tec_0">Todos los tecnicos</label>';
            html+='</div>';
            html+='<div class="col s6">';
                
                html+='<input type="radio" id="select_tec_1" name="select_tec" value="1" class="fc-bmz" onchange="ser_opt(1)"/>';
                html+='<label for="select_tec_1">Seleccionar tecnicos</label>';
            html+='</div>';
            html+='<div class="col s12 ">';
                html+='<ul class="view_selec_tec" style="display:none">';
                html+=input_check;
                html+='</ul>';
            html+='</div>';
        html+='</div>';
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Generando reporte del dia',
        content: html,
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var fech = $('#report_date').val();
                var stec = $('input[name=select_tec]:checked').val();

                var arr = $('[name="tec[]"]:checked').map(function(){
                        return this.value;
                    }).get();
                aI_arr   = JSON.stringify(arr);
                console.log(aI_arr);
                var url = base_url+'AsignacionesTecnico/reporte_tec/'+fech+'/'+stec+'?tec='+aI_arr;
                window.open(url, '_blank');
            },
            cancelar: function () {
            }
        }
    });
}


function ser_opt(tip){
    if(tip==1){
        $('.view_selec_tec').show('show');
    }else{
        $('.view_selec_tec').hide('show');
    }
}