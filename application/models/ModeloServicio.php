<?php
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloServicio extends CI_Model {
    public function __construct() {
        parent::__construct();
        $this->DB4 = $this->load->database('other4_db', TRUE); 
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fechaactual = date('Y-m-d');
        $this->horaactual = date('G:i:s');
    }
//==============contratos==========
    function getlistaservicioc($params){
        $tiposer = $params['tiposer'];
        $tecnico = $params['tecnico'];
        $status = $params['status'];//
        $estatus_ad = $params['estatus_ad'];//
        $zona = $params['zona'];
        $pcliente = $params['pcliente'];
        $fechainicio = $params['fechainicio'];
        $fechafin = $params['fechafin'];
        $noti = $params['noti'];
        $columns = array( 
            0=>'ace.asignacionIde',
            1=>'per.personalId',
            2=>"CONCAT(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno) as tecnico",
            3=>'cli.empresa',
            4=>'ace.fecha',
            5=>'ace.hora',
            6=>'ac.zonaId',
            7=>'rde.modelo',
            8=>'serp.serie',            
            9=>'ace.status',
            10=>'
                (
                    SELECT 
                            group_concat(cons.modelo," ",cf.foliotext) 
                    from contrato_folio as cf 
                    inner join consumibles as cons on cons.id=cf.idconsumible
                    where cf.serviciocId =ac.asignacionId and cf.serieId=serp.serieId
                ) as consumibles
                ',
            11=>'ace.t_fecha',
            12=>'cli.id',
            13=>'ru.nombre',
            14=>'ac.tiposervicio',
            15=>'ace.horafin',
            16=>'ac.asignacionId',
            17=>'sereve.nombre as servicio',
            18=>'ace.t_horainicio',
            19=>'ace.t_horafin',
            20=>'ace.horaserinicio',
            21=>'ace.horaserfin',
            22=>'ace.horaserinicioext',
            23=>'ace.horaserfinext',
            24=>'ace.editado',
            25=>'pol.nombre as poliza',
            26=>'ac.tservicio_a',
            27=>'ace.hora_comida',
            28=>'ace.horafin_comida',
            29=>'con.folio',
            30=>'ace.activo',
            31=>'ace.infodelete',
            32=>'serp.serieId',
            33=>'ace.idequipo',
            34=>'ace.notificarerror_estatus',
            35=>'ace.notificarerror',
            36=>'aser.contadores',
            37=>'ace.nr',
            38=>'ace.nr_coment',
            39=>'ace.nr_coment_tipo',
            40=>"CONCAT(per2.nombre,' ',per2.apellido_paterno,' ',per2.apellido_materno) as tecnico2",
            41=>'per2.personalId as personalId2',
            42=>"CONCAT(per3.nombre,' ',per3.apellido_paterno,' ',per3.apellido_materno) as tecnico3",
            43=>'per3.personalId as personalId3',
            44=>"CONCAT(per4.nombre,' ',per4.apellido_paterno,' ',per4.apellido_materno) as tecnico4",
            45=>'per4.personalId as personalId4',
            46=>'ace.comentario',
            47=>'ac.stock_toner',
            48=>'ac.stock_toner_comen',
            49=>"CONCAT(perf.nombre,' ',perf.apellido_paterno,' ',perf.apellido_materno) as tecnicof",
            50=>'perf.personalId as personalIdf',
            51=>"SUBTIME(ace.horafin, '00:30:00') hora_limite",
            52=>'ace.not_dev'
        );
        $columnss = array( 
            0=>'ace.asignacionIde',
            1=>'per.personalId',
            2=>"CONCAT(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno)",
            3=>'cli.empresa',
            4=>'ace.fecha',
            5=>'ace.hora',
            6=>'ac.zonaId',
            7=>'rde.modelo',
            8=>'serp.serie',
            9=>'ace.status',
            10=>'(
                    SELECT 
                            group_concat(cons.modelo," ",cf.foliotext) 
                    from contrato_folio as cf 
                    inner join consumibles as cons on cons.id=cf.idconsumible
                    where cf.serviciocId =ac.asignacionId and cf.serieId=serp.serieId
                )',

            11=>'cli.id',
            12=>'ru.nombre',
            13=>'ac.tiposervicio',
            14=>'ace.horafin',
            15=>'sereve.nombre',
            16=>'pol.nombre',
            17=>'ace.hora_comida',
            18=>'ace.horafin_comida',
            29=>'con.folio',
            
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('asignacion_ser_contrato_a_e ace');
        $this->db->join('asignacion_ser_contrato_a ac','ac.asignacionId=ace.asignacionId');
        $this->db->join('personal per','per.personalId=ace.tecnico','left');
        $this->db->join('personal per2','per2.personalId=ace.tecnico2','left');
        $this->db->join('personal per3','per3.personalId=ace.tecnico3','left');
        $this->db->join('personal per4','per4.personalId=ace.tecnico4','left');
        $this->db->join('personal perf','perf.personalId=ace.per_fin','left');
        $this->db->join('contrato con','con.idcontrato=ac.contratoId');
        $this->db->join('rentas ren','ren.id=con.idRenta');
        $this->db->join('clientes cli','cli.id=ren.idCliente');
        $this->db->join('rutas ru','ru.id=ac.zonaId');
        $this->db->join('rentas_has_detallesEquipos rde','rde.id=ace.idequipo');
        $this->db->join('series_productos serp','serp.serieId=ace.serieId');
        $this->db->join('polizas pol','pol.Id=ac.tpoliza','left');
        $this->db->join('servicio_evento sereve','sereve.id=ac.tservicio','left');
        $this->db->join('asignacion_series_r_equipos aser','aser.id_equipo=rde.id and aser.serieId=serp.serieId','left');
        /*
        $where = array('ca.status'=>1);
        $this->DB4->where($where);
        */
        if ($tiposer==1) {
            $this->db->where(array('ac.tiposervicio'=>0));
        }
        if ($tiposer==2) {
            $this->db->where(array('ac.tiposervicio'=>1));
        }
        if ($tecnico>0) {
            $this->db->where("(per.personalId='$tecnico' or per2.personalId='$tecnico' or perf.personalId='$tecnico')");
        }elseif ($tecnico=='XXX') {
            $this->db->where(array('ace.tecnico'=>0));
        }
        //======================================
        if ($status==1) {//en espera
            $this->db->where(array('ace.status'=>0));
        }
        if ($status==2) {//proceso
            $this->db->where(array('ace.status'=>1));
        }
        if ($status==3) {//finalizados
            $this->db->where(array('ace.status'=>2));
        }
        if ($status==4) {//sin atencion
            $this->db->where(array('ace.status'=>0));
            if($this->horaactual>='17:00:00'){
              $this->db->where('ace.fecha <=',$this->fechaactual);  
          }else{
            $this->db->where('ace.fecha <',$this->fechaactual);
          }
        }
        //======================================
        if ($zona>0) {
            $this->db->where(array('ac.zonaId'=>$zona));
        }
        if ($pcliente>0) {
            $this->db->where(array('cli.id'=>$pcliente));
        }
        if ($fechainicio!='') {
            $this->db->where('ace.fecha >=',$fechainicio);
        }
        if ($fechafin!='' and $status!=4) {
            $this->db->where('ace.fecha <=',$fechafin);
        }
        if($noti>0){
            $this->db->where(array('ace.notificarerror_estatus'=>1));
        }
        $this->db->where(array('ac.activo'=>1));
        $this->db->where(array('ace.activo'=>$estatus_ad));

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columnss as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->DB4->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    function getlistaservicioct($params){
        $tiposer = $params['tiposer'];
        $tecnico = $params['tecnico'];
        $status = $params['status'];
        $estatus_ad = $params['estatus_ad'];
        $zona = $params['zona'];
        $pcliente = $params['pcliente'];
        $fechainicio = $params['fechainicio'];
        $fechafin = $params['fechafin'];
        $noti = $params['noti'];
        $columns = array( 
            0=>'ace.asignacionIde',
            1=>'per.personalId',
            2=>"CONCAT(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno) as tecnico",
            3=>'cli.id',
            4=>'cli.empresa',
            5=>'ace.fecha',
            6=>'ace.hora',
            7=>'ace.zonaId',
            8=>'ru.nombre',
            9=>'rde.modelo',
            10=>'serp.serie',
            11=>'ace.status',
            12=>'ac.tiposervicio',
            13=>'ace.horafin',
            14=>'sereve.nombre as servicio',
            15=>'ace.hora_comida',
            16=>'ace.horafin_comida',
            29=>'con.folio'
        );
        $columnss = array( 
            0=>'ace.asignacionIde',
            1=>'per.personalId',
            2=>"CONCAT(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno)",
            3=>'cli.id',
            4=>'cli.empresa',
            5=>'ace.fecha',
            6=>'ace.hora',
            7=>'ac.zonaId',
            8=>'ru.nombre',
            9=>'rde.modelo',
            10=>'serp.serie',
            11=>'ace.status',
            12=>'ac.tiposervicio',
            13=>'ace.horafin',
            14=>'sereve.nombre',
            15=>'ace.hora_comida',
            16=>'ace.horafin_comida',
            29=>'con.folio',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->DB4->select('COUNT(*) as total');
        $this->DB4->from('asignacion_ser_contrato_a_e ace');
        $this->DB4->join('asignacion_ser_contrato_a ac','ac.asignacionId=ace.asignacionId');
        $this->DB4->join('personal per','per.personalId=ace.tecnico','left');
        $this->DB4->join('contrato con','con.idcontrato=ac.contratoId');
        $this->DB4->join('rentas ren','ren.id=con.idRenta');
        $this->DB4->join('clientes cli','cli.id=ren.idCliente');
        $this->DB4->join('rutas ru','ru.id=ac.zonaId');
        $this->DB4->join('rentas_has_detallesEquipos rde','rde.id=ace.idequipo');
        $this->DB4->join('series_productos serp','serp.serieId=ace.serieId');
        $this->DB4->join('polizas pol','pol.Id=ac.tpoliza','left');
        $this->DB4->join('servicio_evento sereve','sereve.id=ac.tservicio','left');
        
        if ($tiposer==1) {
            $this->DB4->where(array('ac.tiposervicio'=>0));
        }
        if ($tiposer==2) {
            $this->DB4->where(array('ac.tiposervicio'=>1));
        }
        if ($tecnico>0) {
            $this->DB4->where(array('per.personalId'=>$tecnico));
        }elseif ($tecnico=='XXX') {
            $this->DB4->where(array('ace.tecnico'=>0));
        }
        //==============================================
        if ($status==1) {//en espera
            $this->DB4->where(array('ace.status'=>0));
        }
        if ($status==2) {//proceso
            $this->DB4->where(array('ace.status'=>1));
        }
        if ($status==3) {//finalizados
            $this->DB4->where(array('ace.status'=>2));
        }
        if ($status==4) {//sin atencion
            $this->DB4->where(array('ace.status'=>0));
            if($this->horaactual>='17:00:00'){
              $this->DB4->where('ace.fecha <=',$this->fechaactual);  
          }else{
            $this->DB4->where('ace.fecha <',$this->fechaactual);
          }
        }
        //==============================================
        if ($zona>0) {
            $this->DB4->where(array('ac.zonaId'=>$zona));
        }
        if ($pcliente>0) {
            $this->DB4->where(array('cli.id'=>$pcliente));
        }
        if ($fechainicio!='') {
            $this->DB4->where('ace.fecha >=',$fechainicio);
        }
        if ($fechafin!='' and $status!=4) {
            $this->DB4->where('ace.fecha <=',$fechafin);
        }
        if($noti>0){
            $this->DB4->where(array('ace.notificarerror_estatus'=>1));
        }
        $this->DB4->where(array('ac.activo'=>1));
        $this->DB4->where(array('ace.activo'=>$estatus_ad));
        /*
        $where = array('ca.status'=>1);
        $this->DB4->where($where);
        */
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->DB4->group_start();
            foreach($columnss as $c){
                $this->DB4->or_like($c,$search);
            }
            $this->DB4->group_end();  
        }            
        //$this->DB4->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        //$this->DB4->limit($params['length'],$params['start']);
        //echo $this->DB4->get_compiled_select();
        $query=$this->DB4->get();
        // print_r($query); die;
        return $query->row()->total;
    }
    function getlistaservicioc_reporte($params){
        $tiposer = $params['tiposer'];
        $tecnico = $params['tecnico'];
        $status = $params['status'];
        $zona = $params['zona'];
        $pcliente = $params['pcliente'];
        $fechainicio = $params['fechainicio'];
        $fechafin = $params['fechafin'];
        $columns = array(
            0=>'ace.asignacionIde', 
            1=>"CONCAT(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno) as tecnico",
            2=>'cli.empresa',
            3=>'ace.fecha',
            4=>'ace.hora',
            5=>'ace.horafin',
            6=>'con.folio',
            7=>'ace.idequipo',
            8=>'ace.serieId',
            9=>'serp.serie',
            10=>'rde.modelo',
            11=>'ac.comentariocal',
            12=>'ac.tserviciomotivo',
            13=>'ace.status',
            14=>"CONCAT(per2.nombre,' ',per2.apellido_paterno,' ',per2.apellido_materno) as tecnico2",
            //15=>"CONCAT(per3.nombre,' ',per3.apellido_paterno,' ',per3.apellido_materno) as tecnico3",
            //16=>"CONCAT(per4.nombre,' ',per4.apellido_paterno,' ',per4.apellido_materno) as tecnico4",
            17=>'ace.horaserinicio',
            18=>'ace.horaserfin',
            19=>'ac.asignacionId',
            20=>'ren.idCliente',
            21=>'ac.reg',
            22=>'ace.comentario',
            23=>'sereve.nombre as servicio',
            24=>'ac.tservicio_a',
            25=>'pol.nombre as poliza',
            26=>'ace.per_fin',
            27=>"CONCAT(per5.nombre,' ',per5.apellido_paterno,' ',per5.apellido_materno) as tecnicofin",
            28=>'pexs.id_ser idserpexs',
            29=>'pexs.id_tipo tipopexs',
            30=>'ace.horaserinicioext',
            31=>'ace.horaserfinext',
            32=>"CASE 
        WHEN ace.fecha = CURDATE() THEN (
            SELECT 
                AVG(arpd.produccion) as produccion 
            FROM alta_rentas_prefactura_d as arpd
            INNER JOIN alta_rentas_prefactura as arp ON arp.prefId = arpd.prefId
            WHERE arpd.productoId = ace.idequipo 
              AND arpd.serieId = ace.serieId 
              AND arpd.tipo = 1
        ) 
        ELSE '' 
    END as promedio_prod",
    
            /*
            32=>'(
                    SELECT 
                    AVG(arpd.produccion) as produccion 
                    FROM alta_rentas_prefactura_d as arpd
                    inner join alta_rentas_prefactura as arp on arp.prefId=arpd.prefId
                    where arpd.productoId=ace.idequipo and arpd.serieId=ace.serieId and arpd.tipo=1
                    ) as promedio_prod',

            */

                    /*
            33=>"CASE 
                    WHEN ven.id > 0 THEN (
                        SELECT GROUP_CONCAT('Cant: ',ve.cantidad,' ',ve.modelo,' ',sp.serie SEPARATOR ' / ')
                        FROM ventas as v 
                        INNER JOIN ventas_has_detallesEquipos as ve on ve.idVenta=v.id
                        inner join asignacion_series_equipos as ase on ase.id_equipo=ve.id
                        inner join series_productos as sp on sp.serieId=ase.serieId
                        WHERE v.v_ser_tipo='1' AND v.v_ser_id=ac.asignacionId
                    )
                    ELSE ''
                END AS productosadds",
            34=>"CASE 
                    WHEN ven.id > 0 THEN (
                        SELECT GROUP_CONCAT('Cant: ',ve.cantidad,' ',cata.nombre,' ',sea.serie SEPARATOR ' / ') 
                        FROM ventas as v
                        INNER JOIN ventas_has_detallesEquipos_accesorios as ve on ve.id_venta=v.id
                        inner join catalogo_accesorios as cata on cata.id=ve.id_accesorio
                        left JOIN asignacion_series_accesorios as asa on asa.id_accesorio=ve.id_accesoriod
                        left join series_accesorios as sea on sea.serieId=asa.serieId
                        WHERE v.v_ser_tipo='1' AND v.v_ser_id=ac.asignacionId
                    )
                    ELSE ''
                END AS productosadds2",
            35=>"CASE 
                    WHEN ven.id > 0 THEN (
                        SELECT GROUP_CONCAT('Cant: ',ve.piezas,' ',ve.modelo SEPARATOR ' / ') 
                        FROM ventas as v 
                        INNER JOIN ventas_has_detallesConsumibles as ve on ve.idVentas=v.id
                        WHERE v.v_ser_tipo='1' AND v.v_ser_id=ac.asignacionId
                    )
                    ELSE ''
                END AS productosadds3",
            36=>"CASE 
                    WHEN ven.id > 0 THEN (
                        SELECT GROUP_CONCAT('Cant: ',ve.piezas,' ',ve.modelo,' ',ser.serie SEPARATOR ' / ')
                        FROM ventas as v 
                        INNER JOIN ventas_has_detallesRefacciones as ve on ve.idVentas=v.id
                        LEFT JOIN asignacion_series_refacciones as assr on assr.id_refaccion=ve.id
                        LEFT JOIN series_refacciones as ser on ser.serieId=assr.serieId
                        WHERE v.v_ser_tipo='1' AND v.v_ser_id=ac.asignacionId
                    )
                    ELSE ''
                END AS productosadds4",
                */
            37=>'ace.comentario_tec'
            /*
            33=>"(SELECT GROUP_CONCAT('Cant: ',ve.cantidad,' ',ve.modelo,' ',sp.serie SEPARATOR ' / ')
                    FROM ventas as v 
                    INNER JOIN ventas_has_detallesEquipos as ve on ve.idVenta=v.id
                    inner join asignacion_series_equipos as ase on ase.id_equipo=ve.id
                    inner join series_productos as sp on sp.serieId=ase.serieId
                    WHERE v.v_ser_tipo='1' AND v.v_ser_id=ac.asignacionId AND v.prefactura=1) as productosadds",
            34=>"(SELECT GROUP_CONCAT('Cant: ',ve.cantidad,' ',cata.nombre,' ',sea.serie SEPARATOR ' / ') 
                    FROM ventas as v
                    INNER JOIN ventas_has_detallesEquipos_accesorios as ve on ve.id_venta=v.id
                    inner join catalogo_accesorios as cata on cata.id=ve.id_accesorio
                    left JOIN asignacion_series_accesorios as asa on asa.id_accesorio=ve.id_accesoriod
                    left join series_accesorios as sea on sea.serieId=asa.serieId
                    WHERE v.v_ser_tipo='1' AND v.v_ser_id=ac.asignacionId AND v.prefactura=1) as productosadds2",
            35=>"(SELECT GROUP_CONCAT('Cant: ',ve.piezas,' ',ve.modelo SEPARATOR ' / ') 
                    FROM ventas as v 
                    INNER JOIN ventas_has_detallesConsumibles as ve on ve.idVentas=v.id
                    WHERE v.v_ser_tipo='1' AND v.v_ser_id=ac.asignacionId AND v.prefactura=1) as productosadds3",
            36=>"(SELECT GROUP_CONCAT('Cant: ',ve.piezas,' ',ve.modelo,' ',ser.serie SEPARATOR ' / ')
                    FROM ventas as v 
                    INNER JOIN ventas_has_detallesRefacciones as ve on ve.idVentas=v.id
                    LEFT JOIN asignacion_series_refacciones as assr on assr.id_refaccion=ve.id
                    LEFT JOIN series_refacciones as ser on ser.serieId=assr.serieId
                    WHERE v.v_ser_tipo='1' AND v.v_ser_id=ac.asignacionId AND v.prefactura=1) as productosadds4",
                    */
            
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('asignacion_ser_contrato_a_e ace');
        $this->db->join('asignacion_ser_contrato_a ac','ac.asignacionId=ace.asignacionId');
        $this->db->join('personal per','per.personalId=ace.tecnico','left');
        $this->db->join('personal per2','per2.personalId=ace.tecnico2','left');
        //$this->DB4->join('personal per3','per3.personalId=ace.tecnico3','left');
        //$this->DB4->join('personal per4','per4.personalId=ace.tecnico4','left');
        $this->db->join('contrato con','con.idcontrato=ac.contratoId');
        $this->db->join('rentas ren','ren.id=con.idRenta');
        $this->db->join('clientes cli','cli.id=ren.idCliente');
        //$this->DB4->join('rutas ru','ru.id=ac.zonaId');
        $this->db->join('rentas_has_detallesEquipos rde','rde.id=ace.idequipo');
        $this->db->join('series_productos serp','serp.serieId=ace.serieId');
        $this->db->join('polizas pol','pol.Id=ac.tpoliza','left');
        $this->db->join('servicio_evento sereve','sereve.id=ac.tservicio','left');
        $this->db->join('personal per5','per5.personalId=ace.per_fin','left');
        $this->db->join('polizascreadas_ext_servisios as pexs','pexs.id_ser_garantia=ac.asignacionId and pexs.id_tipo_garantia=1','left');

        $this->db->join('ventas ven','ven.v_ser_tipo=1 AND ven.v_ser_id=ac.asignacionId AND ven.prefactura=1','left');
        /*
        $where = array('ca.status'=>1);
        $this->DB4->where($where);
        */
        if ($tiposer==1) {
            $this->db->where(array('ac.tiposervicio'=>0));
        }
        if ($tiposer==2) {
            $this->db->where(array('ac.tiposervicio'=>1));
        }
        if ($tecnico>0) {
            $this->db->where(array('per.personalId'=>$tecnico));
        }elseif ($tecnico=='XXX') {
            $this->db->where(array('ace.tecnico'=>0));
        }
        if ($status==1) {
            $this->db->where('ace.status >=',0);
            $this->db->where('ace.status <=',1);
        }
        if ($status==2) {
            $this->db->where(array('ace.status'=>2));
        }
        if ($zona>0) {
            $this->db->where(array('ac.zonaId'=>$zona));
        }
        if ($pcliente>0) {
            $this->db->where(array('cli.id'=>$pcliente));
        }
        if ($fechainicio!='') {
            $this->db->where('ace.fecha >=',$fechainicio);
        }
        if ($fechafin!='') {
            $this->db->where('ace.fecha <=',$fechafin);
        }
        
        $this->db->where(array('ac.activo'=>1));
        $this->db->where(array('ace.activo'=>1));
        //$this->DB4->group_by(array("ac.asignacionId", "ac.asignacionId"));
        $this->db->order_by('ace.asignacionIde', 'DESC');
        $query=$this->db->get(); 
        return $query;
    }
//==============contratos solicitud refacciones==========
    function getlistasolicitud_Refacciones($params){
        $tiposer = $params['tiposer'];
        $tecnico = $params['tecnico'];
        $status = $params['status'];
        $zona = $params['zona'];
        $pcliente = $params['pcliente'];
        $fechainicio = $params['fechainicio'];
        $fechafin = $params['fechafin'];
        $columns = array( 
            0=>'ace.asignacionIde',
            1=>'per.personalId',
            2=>"per.personalId",
            3=>'cli.id',
            4=>'ace.fecha',
            5=>'ace.hora',
            6=>'rde.modelo',
            7=>'serp.serie',
            8=>'ace.cot_refaccion',
            9=>'ace.priorizar_cot',
            10=>'ace.cot_status',
            11=>'ace.status',
            12=>'ac.tiposervicio',
            13=>'ace.horafin',
            14=>'ac.asignacionId',
            15=>'sereve.nombre as servicio',
            16=>'ace.cot_tipo',
            17=>'ace.cot_referencia',
            18=>'ace.cot_referencia_tipo',
            19=>'ace.hora_comida',
            20=>'ace.horafin_comida',
            21=>'rde.idEquipo',
            22=>'ace.cot_refaccion2',
            23=>'ace.cot_refaccion3',
            24=>'ace.prioridad',
            25=>"CONCAT(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno) as tecnico",
            26=>'cli.empresa',
            27=>'ru.nombre',
            28=>'ace.cot_status_ob',
            29=>'ace.serieId',
            30=>'rde.id as idrow',
            31=>'ace.cot_contador',
            32=>'ace.per_fin',
            33=>"CONCAT(perf.nombre,' ',perf.apellido_paterno,' ',perf.apellido_materno) as tecnicof",
            34=>'ace.prioridad2',
            35=>'ace.prioridad3',
            36=>'ace.cot_referencia2',
            37=>'ace.cot_referencia3',
        );
        $columnss = array( 
            0=>'ace.asignacionIde',
            1=>'per.personalId',
            2=>"CONCAT(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno)",
            3=>'cli.id',
            4=>'cli.empresa',
            5=>'ace.fecha',
            6=>'ace.hora',
            7=>'ac.zonaId',
            8=>'ru.nombre',
            9=>'rde.modelo',
            10=>'serp.serie',
            11=>'ace.status',
            12=>'ac.tiposervicio',
            13=>'ace.horafin',
            14=>'sereve.nombre',
            15=>'ac.contratoId',
            16=>'ace.hora_comida',
            17=>'ace.horafin_comida',
            18=>'rde.idEquipo',
            19=>'ace.cot_contador'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->DB4->select($select);//per_fin
        $this->DB4->from('asignacion_ser_contrato_a_e ace');
        $this->DB4->join('asignacion_ser_contrato_a ac','ac.asignacionId=ace.asignacionId');
        $this->DB4->join('personal per','per.personalId=ace.tecnico','left');
        $this->DB4->join('contrato con','con.idcontrato=ac.contratoId');
        $this->DB4->join('rentas ren','ren.id=con.idRenta');
        $this->DB4->join('clientes cli','cli.id=ren.idCliente');
        $this->DB4->join('rutas ru','ru.id=ac.zonaId');
        $this->DB4->join('rentas_has_detallesEquipos rde','rde.id=ace.idequipo');
        $this->DB4->join('series_productos serp','serp.serieId=ace.serieId');
        $this->DB4->join('servicio_evento sereve','sereve.id=ac.tservicio','left');
        $this->DB4->join('personal perf','perf.personalId=ace.per_fin','left');
        /*
        $where = array('ca.status'=>1);
        $this->DB4->where($where);
        */
        if ($tiposer==1) {
            $this->DB4->where(array('ac.tiposervicio'=>0));
        }
        if ($tiposer==2) {
            $this->DB4->where(array('ac.tiposervicio'=>1));
        }
        if ($tecnico>0) {
            $this->DB4->where("(per.personalId='$tecnico' or perf.personalId='$tecnico')");
        }
        if ($status==1) {
            $this->DB4->where('ace.status >=',0);
            $this->DB4->where('ace.status <=',1);
        }
        if ($status==2) {
            $this->DB4->where(array('ace.status'=>2));
        }
        if ($zona>0) {
            $this->DB4->where(array('ac.zonaId'=>$zona));
        }
        if ($pcliente>0) {
            $this->DB4->where(array('cli.id'=>$pcliente));
        }
        $this->DB4->where(array('ace.cot_status !='=>0));
        
        if ($fechainicio!='') {
            $this->DB4->where('ace.fecha >=',$fechainicio);
        }
        if ($fechafin!='') {
            $this->DB4->where('ace.fecha <=',$fechafin);
        }
        


        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->DB4->group_start();
            foreach($columnss as $c){
                $this->DB4->or_like($c,$search);
            }
            $this->DB4->group_end();  
        }            
        $this->DB4->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->DB4->limit($params['length'],$params['start']);
        //echo $this->DB4->get_compiled_select();
        $query=$this->DB4->get();
        // print_r($query); die;
        return $query;
    }
    function getlistasolicitud_Refaccionest($params){
        $tiposer = $params['tiposer'];
        $tecnico = $params['tecnico'];
        $status = $params['status'];
        $zona = $params['zona'];
        $pcliente = $params['pcliente'];
        $fechainicio = $params['fechainicio'];
        $fechafin = $params['fechafin'];
        $columns = array( 
            0=>'ace.asignacionIde',
            1=>'per.personalId',
            2=>"CONCAT(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno) as tecnico",
            3=>'cli.id',
            4=>'cli.empresa',
            5=>'ace.fecha',
            6=>'rde.modelo',
            7=>'serp.serie',
            8=>'ace.status',
            9=>'ac.tiposervicio',
            10=>'ace.horafin',
            11=>'sereve.nombre as servicio',
            12=>'ac.contratoId',
            13=>'ace.hora_comida',
            14=>'ace.horafin_comida',
            15=>'ace.idequipo',
            16=>'serp.serieId',
            17=>'ace.cot_contador'
        );
        $columnss = array( 
            0=>'ace.asignacionIde',
            1=>'per.personalId',
            2=>"CONCAT(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno)",
            3=>'cli.id',
            4=>'cli.empresa',
            5=>'ace.fecha',
            6=>'ace.hora',
            7=>'rde.modelo',
            8=>'serp.serie',
            9=>'ace.status',
            10=>'ac.tiposervicio',
            11=>'ace.horafin',
            12=>'sereve.nombre',
            13=>'ac.contratoId',
            14=>'ace.hora_comida',
            15=>'ace.horafin_comida',
            16=>'ace.cot_contador'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->DB4->select('COUNT(*) as total');
        $this->DB4->from('asignacion_ser_contrato_a_e ace');
        $this->DB4->join('asignacion_ser_contrato_a ac','ac.asignacionId=ace.asignacionId');
        $this->DB4->join('personal per','per.personalId=ace.tecnico','left');
        $this->DB4->join('contrato con','con.idcontrato=ac.contratoId');
        $this->DB4->join('rentas ren','ren.id=con.idRenta');
        $this->DB4->join('clientes cli','cli.id=ren.idCliente');
        //$this->DB4->join('rutas ru','ru.id=ac.zonaId');
        $this->DB4->join('rentas_has_detallesEquipos rde','rde.id=ace.idequipo');
        $this->DB4->join('series_productos serp','serp.serieId=ace.serieId');
        $this->DB4->join('servicio_evento sereve','sereve.id=ac.tservicio','left');
        $this->DB4->join('personal perf','perf.personalId=ace.per_fin','left');
        
        if ($tiposer==1) {
            $this->DB4->where(array('ac.tiposervicio'=>0));
        }
        if ($tiposer==2) {
            $this->DB4->where(array('ac.tiposervicio'=>1));
        }
        if ($tecnico>0) {
            $this->DB4->where("(per.personalId='$tecnico' or perf.personalId='$tecnico')");
        }
        if ($status==1) {
            $this->DB4->where('ace.status >=',0);
            $this->DB4->where('ace.status <=',1);
        }
        if ($status==2) {
            $this->DB4->where(array('ace.status'=>2));
        }
        if ($zona>0) {
            $this->DB4->where(array('ac.zonaId'=>$zona));
        }
        if ($pcliente>0) {
            $this->DB4->where(array('cli.id'=>$pcliente));
        }
        $this->DB4->where(array('ace.cot_status !='=>0));
        
        if ($fechainicio!='') {
            $this->DB4->where('ace.fecha >=',$fechainicio);
        }
        if ($fechafin!='') {
            $this->DB4->where('ace.fecha <=',$fechafin);
        }
        
        /*
        $where = array('ca.status'=>1);
        $this->DB4->where($where);
        */
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->DB4->group_start();
            foreach($columnss as $c){
                $this->DB4->or_like($c,$search);
            }
            $this->DB4->group_end();  
        }            
        //$this->DB4->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        //$this->DB4->limit($params['length'],$params['start']);
        //echo $this->DB4->get_compiled_select();
        $query=$this->DB4->get();
        // print_r($query); die;
        return $query->row()->total;
    }    
//==============poliza==========
    function getlistaserviciop($params){
        $tiposer = $params['tiposer'];
        $tecnico = $params['tecnico'];
        $status = $params['status'];
        $estatus_ad = $params['estatus_ad'];
        $zona = $params['zona'];
        $pcliente = $params['pcliente'];
        $fechainicio = $params['fechainicio'];
        $fechafin = $params['fechafin'];
        $noti = $params['noti'];
        $columns = array( 
            0=>'ape.asignacionIde',
            1=>'per.personalId',
            2=>"CONCAT(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno) as tecnico",
            3=>'cli.id',
            4=>'cli.empresa',
            5=>'ape.fecha',
            6=>'ape.hora',
            7=>'ap.zonaId',
            8=>'ru.nombre',
            9=>'equ.modelo',
            10=>'ape.serie',
            11=>'ape.status',
            12=>'ap.tiposervicio',
            13=>'ape.horafin',
            14=>'ap.asignacionId',
            15=>'sereve.nombre as servicio',
            16=>'ape.t_fecha',
            17=>'ape.horaserinicio',
            18=>'ape.horaserfin',
            19=>'ape.horaserinicio',
            20=>'ape.horaserfin',
            21=>'ape.cot_referencia',
            22=>'ape.horaserinicioext',
            23=>'ape.horaserfinext',
            24=>'ape.editado',
            25=>'ape.hora_comida',
            26=>'ape.horafin_comida',
            27=>'ape.t_horainicio',
            28=>'ape.t_horafin',
            29=>'ape.activo',
            30=>'ape.infodelete',
            31=>'pocde.direccionservicio',
            32=>'ap.polizaId',
            33=>'ape.prioridad',
            34=>'ape.notificarerror_estatus',
            35=>'ape.notificarerror',
            37=>'ape.nr',
            38=>'ape.nr_coment',
            39=>'ape.nr_coment_tipo',
            40=>"CONCAT(per2.nombre,' ',per2.apellido_paterno,' ',per2.apellido_materno) as tecnico2",
            41=>'per2.personalId as personalId2',
            42=>"CONCAT(per3.nombre,' ',per3.apellido_paterno,' ',per3.apellido_materno) as tecnico3",
            43=>'per3.personalId as personalId3',
            44=>"CONCAT(per4.nombre,' ',per4.apellido_paterno,' ',per4.apellido_materno) as tecnico4",
            45=>'per4.personalId as personalId4',
            46=>'ape.comentario',
            47=>"CONCAT(perf.nombre,' ',perf.apellido_paterno,' ',perf.apellido_materno) as tecnicof",
            48=>'perf.personalId as personalIdf',
            49=>"SUBTIME(ape.horafin, '00:30:00') hora_limite",
            50=>'poc.combinada',
            51=>'poc.not_dev'
        );
        $columnsss = array( 
            0=>'ape.asignacionIde',
            1=>'per.personalId',
            2=>"per.nombre",
            3=>'cli.id',
            4=>'cli.empresa',
            5=>'ape.fecha',
            6=>'ape.hora',
            7=>'ap.zonaId',
            8=>'ru.nombre',
            9=>'equ.modelo',
            10=>'ape.serie',
            11=>'ape.status',
            12=>'ap.tiposervicio',
            13=>'ape.horafin',
            14=>'sereve.nombre',
            15=>'ape.hora_comida',
            16=>'ape.horafin_comida',
            17=>'pocde.direccionservicio',
            18=>'ap.polizaId'

        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('asignacion_ser_poliza_a_e ape');
        $this->db->join('asignacion_ser_poliza_a ap','ap.asignacionId=ape.asignacionId');
        $this->db->join('personal per','per.personalId=ape.tecnico','left');
        $this->db->join('personal per2','per2.personalId=ape.tecnico2','left');
        $this->db->join('personal per3','per3.personalId=ape.tecnico3','left');
        $this->db->join('personal per4','per4.personalId=ape.tecnico4','left');
        $this->db->join('personal perf','perf.personalId=ape.per_fin','left');
        $this->db->join('polizasCreadas poc','poc.id=ap.polizaId');
        $this->db->join('clientes cli','cli.id=poc.idCliente');
        $this->db->join('rutas ru','ru.id=ap.zonaId');
        $this->db->join('polizasCreadas_has_detallesPoliza pocde','pocde.id=ape.idequipo');
        $this->db->join('equipos equ','equ.id=pocde.idEquipo');
        $this->db->join('servicio_evento sereve','sereve.id=ap.tservicio','left');
        if ($tiposer==1) {
            $this->db->where(array('ap.tiposervicio'=>0));
        }
        if ($tiposer==2) {
            $this->db->where(array('ap.tiposervicio'=>1));
        }
        if ($tecnico>0) {
            $this->db->where("(per.personalId='$tecnico' or per2.personalId='$tecnico')");
        }
        //================================================
        if ($status==1) {//en espera
            $this->db->where(array('ape.status'=>0));
        }
        if ($status==2) {//proceso
            $this->db->where(array('ape.status'=>1));
        }
        if ($status==3) {//finalizados
            $this->db->where(array('ape.status'=>2));
        }
        if ($status==4) {//sin atencion
            $this->db->where(array('ape.status'=>0));
            if($this->horaactual>='17:00:00'){
              $this->db->where('ape.fecha <=',$this->fechaactual);  
          }else{
            $this->db->where('ape.fecha <',$this->fechaactual);
          }
        }
        //========================================
        if ($zona>0) {
            $this->db->where(array('ap.zonaId'=>$zona));
        }
        if ($pcliente>0) {
            $this->db->where(array('cli.id'=>$pcliente));
        }
        if ($fechainicio!='') {
            $this->db->where('ape.fecha >=',$fechainicio);
        }
        if ($fechafin!='' and $status!=4) {
            $this->db->where('ape.fecha <=',$fechafin);
        }
        if($noti>0){
            $this->db->where(array('ape.notificarerror_estatus'=>1));
        }
        $this->db->where(array('ap.activo'=>1));
        $this->db->where(array('ape.activo'=>$estatus_ad));
        /*
        $where = array('ca.status'=>1);
        $this->DB4->where($where);
        */
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columnsss as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->DB4->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    function getlistaserviciopt($params){
        $tiposer = $params['tiposer'];
        $tecnico = $params['tecnico'];
        $status = $params['status'];
        $estatus_ad = $params['estatus_ad'];
        $zona = $params['zona'];
        $pcliente = $params['pcliente'];
        $fechainicio = $params['fechainicio'];
        $fechafin = $params['fechafin'];
        $noti = $params['noti'];

        $columns = array( 
            0=>'ape.asignacionIde',
            1=>'per.personalId',
            2=>"CONCAT(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno)",
            3=>'cli.id',
            4=>'cli.empresa',
            5=>'ape.fecha',
            6=>'ape.hora',
            7=>'ap.zonaId',
            8=>'ru.nombre',
            9=>'equ.modelo',
            10=>'ape.serie',
            11=>'ape.status',
            12=>'ap.tiposervicio',
            13=>'ape.horafin',
            14=>'sereve.nombre as servicio',
            27=>'ape.hora_comida',
            28=>'ape.horafin_comida',
            29=>'pocde.direccionservicio',
            30=>'ap.polizaId'
        );
        $columnsss = array( 
            0=>'ape.asignacionIde',
            1=>'per.personalId',
            2=>"per.nombre",
            3=>'cli.id',
            4=>'cli.empresa',
            5=>'ape.fecha',
            6=>'ape.hora',
            7=>'ap.zonaId',
            8=>'ru.nombre',
            9=>'equ.modelo',
            10=>'ape.serie',
            11=>'ape.status',
            12=>'ap.tiposervicio',
            13=>'ape.horafin',
            14=>'sereve.nombre',
            15=>'ape.hora_comida',
            16=>'ape.horafin_comida',
            17=>'pocde.direccionservicio',
            18=>'ap.polizaId'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->DB4->select('COUNT(*) as total');
        $this->DB4->from('asignacion_ser_poliza_a_e ape');
        $this->DB4->join('asignacion_ser_poliza_a ap','ap.asignacionId=ape.asignacionId');
        $this->DB4->join('personal per','per.personalId=ape.tecnico','left');
        $this->DB4->join('polizasCreadas poc','poc.id=ap.polizaId');
        $this->DB4->join('clientes cli','cli.id=poc.idCliente');
        $this->DB4->join('rutas ru','ru.id=ap.zonaId');
        $this->DB4->join('polizasCreadas_has_detallesPoliza pocde','pocde.id=ape.idequipo');
        $this->DB4->join('equipos equ','equ.id=pocde.idEquipo');
        $this->DB4->join('servicio_evento sereve','sereve.id=ap.tservicio','left');
        if ($tiposer==1) {
            $this->DB4->where(array('ap.tiposervicio'=>0));
        }
        if ($tiposer==2) {
            $this->DB4->where(array('ap.tiposervicio'=>1));
        }
        if ($tecnico>0) {
            $this->DB4->where(array('per.personalId'=>$tecnico));
        }
        //================================================
        if ($status==1) {//en espera
            $this->DB4->where(array('ape.status'=>0));
        }
        if ($status==2) {//proceso
            $this->DB4->where(array('ape.status'=>1));
        }
        if ($status==3) {//finalizados
            $this->DB4->where(array('ape.status'=>2));
        }
        if ($status==4) {//sin atencion
            $this->DB4->where(array('ape.status'=>0));
            if($this->horaactual>='17:00:00'){
              $this->DB4->where('ape.fecha <=',$this->fechaactual);  
          }else{
            $this->DB4->where('ape.fecha <',$this->fechaactual);
          }
        }
        //========================================
        if ($zona>0) {
            $this->DB4->where(array('ap.zonaId'=>$zona));
        }
        if ($pcliente>0) {
            $this->DB4->where(array('cli.id'=>$pcliente));
        }
        if ($fechainicio!='') {
            $this->DB4->where('ape.fecha >=',$fechainicio);
        }
        if ($fechafin!='' and $status!=4) {
            $this->DB4->where('ape.fecha <=',$fechafin);
        }
        if($noti>0){
            $this->DB4->where(array('ape.notificarerror_estatus'=>1));
        }
        $this->DB4->where(array('ap.activo'=>1));
        $this->DB4->where(array('ape.activo'=>$estatus_ad));
        /*
        $where = array('ca.status'=>1);
        $this->DB4->where($where);
        */
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->DB4->group_start();
            foreach($columnsss as $c){
                $this->DB4->or_like($c,$search);
            }
            $this->DB4->group_end();  
        }            
        //$this->DB4->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        //$this->DB4->limit($params['length'],$params['start']);
        //echo $this->DB4->get_compiled_select();
        $query=$this->DB4->get();
        // print_r($query); die;
        return $query->row()->total;
    }
    function getlistaserviciop_reporte($params){
        $tiposer = $params['tiposer'];
        $tecnico = $params['tecnico'];
        $status = $params['status'];
        $zona = $params['zona'];
        $pcliente = $params['pcliente'];
        $fechainicio = $params['fechainicio'];
        $fechafin = $params['fechafin'];
        if(isset($params['tipoc'])){
            $tipoc=$params['tipoc'];
        }else{
            $tipoc=0;
        }
        $columns = array( 
            0=>'ape.asignacionIde',
            1=>"CONCAT(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno) as tecnico",
            2=>'cli.empresa',
            3=>'ape.fecha',
            4=>'ape.hora',
            5=>'ape.horafin',
            6=>'pocde.direccionservicio',
            7=>'equ.modelo',
            8=>'pocde.serie',
            9=>'ap.tserviciomotivo',
            10=>'ap.comentariocal',
            11=>'ape.status',
            12=>"CONCAT(per2.nombre,' ',per2.apellido_paterno,' ',per2.apellido_materno) as tecnico2",
            13=>'poc.viewcont',
            14=>'poc.viewcont_cant',
            15=>'ap.asignacionId',
            16=>'ap.reg',
            17=>'ape.comentario',
            18=>'pocde.nombre as servicio',
            19=>'ap.polizaId',
            20=>'ape.per_fin',
            21=>"CONCAT(per3.nombre,' ',per3.apellido_paterno,' ',per3.apellido_materno) as tecnicofin",
            22=>'pexs.id_ser idserpexs',
            23=>'pexs.id_tipo tipopexs',
            24=>'ape.horaserinicioext',
            25=>'ape.horaserfinext',
            /*
            26=>"CASE 
                    WHEN ven.id > 0 THEN (
                        SELECT GROUP_CONCAT('Cant: ',ve.cantidad,' ',ve.modelo,' ',sp.serie SEPARATOR ' / ')
                        FROM ventas as v 
                        INNER JOIN ventas_has_detallesEquipos as ve on ve.idVenta=v.id
                        inner join asignacion_series_equipos as ase on ase.id_equipo=ve.id
                        inner join series_productos as sp on sp.serieId=ase.serieId
                        WHERE v.v_ser_tipo='2' AND v.v_ser_id=ap.asignacionId
                    )
                    ELSE ''
                END AS productosadds",
            27=>"CASE 
                    WHEN ven.id > 0 THEN (
                        SELECT GROUP_CONCAT('Cant: ',ve.cantidad,' ',cata.nombre,' ',sea.serie SEPARATOR ' / ') 
                        FROM ventas as v
                        INNER JOIN ventas_has_detallesEquipos_accesorios as ve on ve.id_venta=v.id
                        inner join catalogo_accesorios as cata on cata.id=ve.id_accesorio
                        left JOIN asignacion_series_accesorios as asa on asa.id_accesorio=ve.id_accesoriod
                        left join series_accesorios as sea on sea.serieId=asa.serieId
                        WHERE v.v_ser_tipo='2' AND v.v_ser_id=ap.asignacionId
                    )
                    ELSE ''
                END AS productosadds2",
            28=>"CASE 
                    WHEN ven.id > 0 THEN (
                        SELECT GROUP_CONCAT('Cant: ',ve.piezas,' ',ve.modelo SEPARATOR ' / ') 
                        FROM ventas as v 
                        INNER JOIN ventas_has_detallesConsumibles as ve on ve.idVentas=v.id
                        WHERE v.v_ser_tipo='2' AND v.v_ser_id=ap.asignacionId
                    )
                    ELSE ''
                END AS productosadds3",
            29=>"CASE 
                    WHEN ven.id > 0 THEN (
                        SELECT GROUP_CONCAT('Cant: ',ve.piezas,' ',ve.modelo,' ',ser.serie SEPARATOR ' / ')
                        FROM ventas as v 
                        INNER JOIN ventas_has_detallesRefacciones as ve on ve.idVentas=v.id
                        LEFT JOIN asignacion_series_refacciones as assr on assr.id_refaccion=ve.id
                        LEFT JOIN series_refacciones as ser on ser.serieId=assr.serieId
                        WHERE v.v_ser_tipo='2' AND v.v_ser_id=ap.asignacionId
                    )
                    ELSE ''
                END AS productosadds4",
                */

            30=>"(SELECT GROUP_CONCAT(vdc.modelo SEPARATOR ' / ')
                FROM ventacombinada as vc 
                INNER JOIN ventas_has_detallesConsumibles as vdc on vdc.idVentas=vc.consumibles
                WHERE vc.poliza=ap.polizaId ) as op_pro",
            31=>"( SELECT GROUP_CONCAT(con.modelo SEPARATOR ' / ') 
                FROM ventacombinada as vc 
                INNER JOIN ventas_has_detallesequipos_consumible as vdc on vdc.idventa=vc.equipo
                inner join consumibles as con on con.id=vdc.id_consumibles
                WHERE vc.poliza=ap.polizaId) as op_pro2",
            32=>"(SELECT GROUP_CONCAT(vde.modelo,' ',sep.serie  SEPARATOR ' / ') 
                FROM ventacombinada as vc 
                INNER JOIN ventas_has_detallesEquipos as vde on vde.idVenta=vc.equipo
                LEFT JOIN asignacion_series_equipos as ase on ase.id_equipo=vde.id
                LEFT JOIN series_productos as sep on sep.serieId=ase.serieId
                WHERE vc.poliza=ap.polizaId) as op_pro3",
            33=>"(SELECT GROUP_CONCAT(vdr.modelo,' ',ser.serie  SEPARATOR ' / ')
                FROM ventacombinada as vc 
                INNER JOIN ventas_has_detallesRefacciones as vdr on vdr.idVentas=vc.refacciones
                INNER JOIN asignacion_series_refacciones as asr on asr.id_refaccion=vdr.id
                inner join series_refacciones as ser on ser.serieId=asr.serieId
                WHERE vc.poliza=ap.polizaId) as op_pro4",
            34=>"(SELECT GROUP_CONCAT(cata.nombre,' ',seacc.serie  SEPARATOR ' / ') 
                FROM ventacombinada as vc 
                INNER JOIN ventas_has_detallesEquipos_accesorios as vdea on vdea.id_venta=vc.equipo
                inner join catalogo_accesorios as cata on cata.id=vdea.id_accesorio
                inner join series_accesorios as seacc on seacc.accesoriosid=cata.id
                WHERE vc.poliza=ap.polizaId) as op_pro5",
            35=>'ape.comentario_tec'

        );
        if($tipoc==1){
            $columns['24']='count(*) as numequipos';
            $columns['25']='GROUP_CONCAT(DISTINCT(per.nombre)) as tecnicogroup';
        }
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('asignacion_ser_poliza_a_e ape');
        $this->db->join('asignacion_ser_poliza_a ap','ap.asignacionId=ape.asignacionId');
        $this->db->join('personal per','per.personalId=ape.tecnico','left');
        $this->db->join('personal per2','per2.personalId=ape.tecnico2','left');
        $this->db->join('polizasCreadas poc','poc.id=ap.polizaId');
        $this->db->join('clientes cli','cli.id=poc.idCliente');
        $this->db->join('rutas ru','ru.id=ap.zonaId');
        $this->db->join('polizasCreadas_has_detallesPoliza pocde','pocde.id=ape.idequipo');
        $this->db->join('equipos equ','equ.id=pocde.idEquipo');
        $this->db->join('servicio_evento sereve','sereve.id=ap.tservicio','left');
        $this->db->join('personal per3','per3.personalId=ape.per_fin','left');
        $this->db->join('polizascreadas_ext_servisios as pexs','pexs.id_ser_garantia=ap.asignacionId and pexs.id_tipo_garantia=2','left');

        $this->db->join('ventas ven','ven.v_ser_tipo=2 AND ven.v_ser_id=ap.asignacionId AND ven.prefactura=1','left');
        
        if($tipoc==1){
            $this->db->group_by(array("ap.asignacionId", "pocde.direccionservicio"));
        }
        
        
        if ($tiposer==1) {
            $this->db->where(array('ap.tiposervicio'=>0));
        }
        if ($tiposer==2) {
            $this->db->where(array('ap.tiposervicio'=>1));
        }
        if ($tecnico>0) {
            $this->db->where(array('per.personalId'=>$tecnico));
        }
        if ($status==1) {
            $this->db->where('ape.status >=',0);
            $this->db->where('ape.status <=',1);
        }
        if ($status==2) {
            $this->db->where(array('ape.status'=>2));
        }
        if ($zona>0) {
            $this->db->where(array('ap.zonaId'=>$zona));
        }
        if ($pcliente>0) {
            $this->db->where(array('cli.id'=>$pcliente));
        }
        if ($fechainicio!='') {
            $this->db->where('ape.fecha >=',$fechainicio);
        }
        if ($fechafin!='') {
            $this->db->where('ape.fecha <=',$fechafin);
        }
        $this->db->where(array('ap.activo'=>1));
        $this->db->where(array('ape.activo'=>1));
        $this->db->order_by('ape.asignacionIde', 'DESC');

        $query=$this->db->get(); 
        return $query;
    }
    //=========poliza Solicud de refacciones==========
    function getlistaserviciop_solicitud($params){
        $tiposer = $params['tiposer'];
        $tecnico = $params['tecnico'];
        $status = $params['status'];
        $zona = $params['zona'];
        $pcliente = $params['pcliente'];
        $fechainicio = $params['fechainicio'];
        $fechafin = $params['fechafin'];
        $columns = array( 
            0=>'ape.asignacionIde',
            1=>'sereve.id',
            2=>'per.personalId',
            3=>'cli.id',
            4=>'ape.fecha',
            5=>'equ.modelo',
            6=>'ape.serie',
            7=>'ape.cot_refaccion',
            8=>'ape.priorizar_cot',
            9=>'ape.cot_tipo',
            10=>'ape.priorizar_cot',
            11=>'ape.cot_status',
            12=>"CONCAT(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno) as tecnico",
            13=>'cli.empresa',
            14=>'ape.status',
            15=>'ap.tiposervicio',
            16=>'ape.horafin',
            17=>'ap.asignacionId',
            18=>'sereve.nombre as servicio',
            
            19=>'ape.cot_referencia',
            20=>'ape.cot_referencia_tipo',
            21=>'ape.hora_comida',
            22=>'ape.horafin_comida',
            23=>'pocde.idEquipo',
            24=>'ape.cot_refaccion2',
            25=>'ape.cot_refaccion3',
            26=>'ape.cot_status_ob',
            27=>'pocde.direccionservicio',
            28=>"CONCAT(per2.nombre,' ',per2.apellido_paterno,' ',per2.apellido_materno) as tecnico2",
            29=>'ape.cot_contador',
            30=>'ape.per_fin',
            31=>"CONCAT(perf.nombre,' ',perf.apellido_paterno,' ',perf.apellido_materno) as tecnicof",
            32=>'ape.prioridad',
            33=>'ape.prioridad2',
            34=>'ape.prioridad3',
            35=>'ape.cot_referencia2',
            36=>'ape.cot_referencia3',
            
        );
        $columnsss = array( 
            0=>'ape.asignacionIde',
            1=>'per.personalId',
            2=>"per.nombre",
            3=>'cli.id',
            4=>'cli.empresa',
            5=>'ape.fecha',
            6=>'equ.modelo',
            7=>'ape.serie',
            8=>'ape.status',
            9=>'ap.tiposervicio',
            10=>'ape.horafin',
            11=>'sereve.nombre',
            12=>'ape.hora_comida',
            13=>'ape.horafin_comida',
            14=>'pocde.idEquipo',
            15=>'ape.cot_contador'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->DB4->select($select);
        $this->DB4->from('asignacion_ser_poliza_a_e ape');
        $this->DB4->join('asignacion_ser_poliza_a ap','ap.asignacionId=ape.asignacionId');
        $this->DB4->join('personal per','per.personalId=ape.tecnico','left');
        $this->DB4->join('personal per2','per2.personalId=ape.tecnico2','left');
        $this->DB4->join('polizasCreadas poc','poc.id=ap.polizaId');
        $this->DB4->join('clientes cli','cli.id=poc.idCliente');
        //$this->DB4->join('rutas ru','ru.id=ap.zonaId');
        $this->DB4->join('polizasCreadas_has_detallesPoliza pocde','pocde.id=ape.idequipo');
        $this->DB4->join('equipos equ','equ.id=pocde.idEquipo');
        $this->DB4->join('servicio_evento sereve','sereve.id=ap.tservicio','left');
        $this->DB4->join('personal perf','perf.personalId=ape.per_fin','left');
        if ($tiposer==1) {
            $this->DB4->where(array('ap.tiposervicio'=>0));
        }
        if ($tiposer==2) {
            $this->DB4->where(array('ap.tiposervicio'=>1));
        }
        if ($tecnico>0) {
            $this->DB4->where("(per.personalId='$tecnico' or perf.personalId='$tecnico')");
        }
        if ($status==1) {
            $this->DB4->where('ape.status >=',0);
            $this->DB4->where('ape.status <=',1);
        }
        if ($status==2) {
            $this->DB4->where(array('ape.status'=>2));
        }
        if ($zona>0) {
            $this->DB4->where(array('ap.zonaId'=>$zona));
        }
        if ($pcliente>0) {
            $this->DB4->where(array('cli.id'=>$pcliente));
        }
        $this->DB4->where(array('ape.cot_status !='=>0));
        
        if ($fechainicio!='') {
            $this->DB4->where('ape.fecha >=',$fechainicio);
        }
        if ($fechafin!='') {
            $this->DB4->where('ape.fecha <=',$fechafin);
        }
        
        /*
        $where = array('ca.status'=>1);
        $this->DB4->where($where);
        */
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->DB4->group_start();
            foreach($columnsss as $c){
                $this->DB4->or_like($c,$search);
            }
            $this->DB4->group_end();  
        }            
        $this->DB4->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->DB4->limit($params['length'],$params['start']);
        //echo $this->DB4->get_compiled_select();
        $query=$this->DB4->get();
        // print_r($query); die;
        return $query;
    }
    function getlistaserviciopt_solicitud($params){
        $tiposer = $params['tiposer'];
        $tecnico = $params['tecnico'];
        $status = $params['status'];
        $zona = $params['zona'];
        $pcliente = $params['pcliente'];
        $fechainicio = $params['fechainicio'];
        $fechafin = $params['fechafin'];
        

        $columns = array( 
            0=>'ape.asignacionIde',
            1=>'per.personalId',
            2=>"CONCAT(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno) as tecnico",
            3=>'cli.id',
            4=>'cli.empresa',
            5=>'ape.fecha',
            6=>'equ.modelo',
            7=>'ape.serie',
            8=>'ape.status',
            9=>'ap.tiposervicio',
            10=>'ape.horafin',
            11=>'sereve.nombre as servicio',
            12=>'ape.hora_comida',
            13=>'ape.horafin_comida',
            14=>'ape.cot_contador'
        );
        $columnsss = array( 
            0=>'ape.asignacionIde',
            1=>'per.personalId',
            2=>"per.nombre",
            3=>'cli.id',
            4=>'cli.empresa',
            5=>'ape.fecha',
            6=>'equ.modelo',
            7=>'ape.serie',
            8=>'ape.status',
            9=>'ap.tiposervicio',
            10=>'ape.horafin',
            11=>'sereve.nombre',
            12=>'ape.hora_comida',
            13=>'ape.horafin_comida',
            14=>'ape.cot_contador'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->DB4->select('COUNT(*) as total');
        $this->DB4->from('asignacion_ser_poliza_a_e ape');
        $this->DB4->join('asignacion_ser_poliza_a ap','ap.asignacionId=ape.asignacionId');
        $this->DB4->join('personal per','per.personalId=ape.tecnico','left');
        $this->DB4->join('polizasCreadas poc','poc.id=ap.polizaId');
        $this->DB4->join('clientes cli','cli.id=poc.idCliente');
        //$this->DB4->join('rutas ru','ru.id=ap.zonaId');
        $this->DB4->join('polizasCreadas_has_detallesPoliza pocde','pocde.id=ape.idequipo');
        $this->DB4->join('equipos equ','equ.id=pocde.idEquipo');
        $this->DB4->join('servicio_evento sereve','sereve.id=ap.tservicio','left');
        if ($tiposer==1) {
            $this->DB4->where(array('ap.tiposervicio'=>0));
        }
        if ($tiposer==2) {
            $this->DB4->where(array('ap.tiposervicio'=>1));
        }
        if ($tecnico>0) {
            $this->DB4->where("(per.personalId='$tecnico' or perf.personalId='$tecnico')");
        }
        if ($status==1) {
            $this->DB4->where('ape.status >=',0);
            $this->DB4->where('ape.status <=',1);
        }
        if ($status==2) {
            $this->DB4->where(array('ape.status'=>2));
        }
        if ($zona>0) {
            $this->DB4->where(array('ap.zonaId'=>$zona));
        }
        if ($pcliente>0) {
            $this->DB4->where(array('cli.id'=>$pcliente));
        }
        $this->DB4->where(array('ape.cot_status !='=>0));
        
        if ($fechainicio!='') {
            $this->DB4->where('ape.fecha >=',$fechainicio);
        }
        if ($fechafin!='') {
            $this->DB4->where('ape.fecha <=',$fechafin);
        }
        
        /*
        $where = array('ca.status'=>1);
        $this->DB4->where($where);
        */
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->DB4->group_start();
            foreach($columnsss as $c){
                $this->DB4->or_like($c,$search);
            }
            $this->DB4->group_end();  
        }            
        //$this->DB4->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        //$this->DB4->limit($params['length'],$params['start']);
        //echo $this->DB4->get_compiled_select();
        $query=$this->DB4->get();
        // print_r($query); die;
        return $query->row()->total;
    }
//==============clientes==========
    function getlistaserviciocl($params){
        $tiposer = $params['tiposer'];
        $tecnico = $params['tecnico'];
        $status = $params['status'];
        $estatus_ad = $params['estatus_ad'];
        $zona = $params['zona'];
        $pcliente = $params['pcliente'];
        $fechainicio = $params['fechainicio'];
        $fechafin = $params['fechafin'];
        $noti = $params['noti'];
        $columns = array( 
            0=>'acl.asignacionId',
            1=>'per.personalId',
            2=>"CONCAT(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno) as tecnico",
            3=>'cli.id',
            4=>'cli.empresa',
            5=>'acl.fecha',
            6=>'acl.hora',
            7=>'acl.horafin',
            8=>'acl.zonaId',
            9=>'acl.prioridad',
            10=>'zo.nombre',
            11=>'acld.status',
            12=>'acl.tiposervicio',
            13=>'pol.nombre as servicio',
            14=>'acl.fecha',
            15=>'acld.horaserinicio',
            16=>'acld.horaserfin',
            17=>'acld.serie',
            18=>'acld.asignacionIdd',
            19=>'acld.horaserinicioext',
            20=>'acld.horaserfinext',
            21=>'acl.editado',
            22=>'acld.tservicio',
            22=>'acld.tservicio_a',
            23=>'acl.hora_comida',
            24=>'acl.horafin_comida',
            25=>'acld.activo',
            26=>'acld.infodelete',
            27=>'acld.direccion',
            28=>'acld.prioridad',
            29=>'acld.notificarerror_estatus',
            30=>'acld.notificarerror',
            31=>'acld.asignacionIdd',
            32=>'acld.nr',
            33=>'acld.nr_coment',
            34=>'acld.nr_coment_tipo',
            35=>'per2.personalId as personalId2',
            36=>"CONCAT(per2.nombre,' ',per2.apellido_paterno,' ',per2.apellido_materno) as tecnico2",
            37=>"CONCAT(per3.nombre,' ',per3.apellido_paterno,' ',per3.apellido_materno) as tecnico3",
            38=>"CONCAT(per4.nombre,' ',per4.apellido_paterno,' ',per4.apellido_materno) as tecnico4",
            39=>'acl.comentario',
            40=>"CONCAT(perf.nombre,' ',perf.apellido_paterno,' ',perf.apellido_materno) as tecnicof",
            41=>'perf.personalId as personalIdf',
            42=>"SUBTIME(acl.horafin, '00:30:00') hora_limite"
        );
        $columnss = array( 
            0=>'acl.asignacionId',
            1=>'per.personalId',
            2=>"CONCAT(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno)",
            3=>'cli.id',
            4=>'cli.empresa',
            5=>'acl.fecha',
            6=>'acl.hora',
            7=>'acl.horafin',
            8=>'acl.zonaId',
            9=>'acl.prioridad',
            10=>'zo.nombre',
            11=>'acld.status',
            12=>'acl.tiposervicio',
            13=>'pol.nombre',
            14=>'acl.fecha',
            15=>'acld.horaserinicio',
            16=>'acld.horaserfin',
            17=>'acld.serie',
            18=>'acld.asignacionIdd',
            19=>'acld.horaserinicioext',
            20=>'acld.horaserfinext',
            21=>'acl.hora_comida',
            22=>'acl.horafin_comida',
            23=>'acld.direccion',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('asignacion_ser_cliente_a acl');
        $this->db->join('personal per','per.personalId=acl.tecnico');
        $this->db->join('personal per2','per2.personalId=acl.tecnico2','left');
        $this->db->join('personal per3','per3.personalId=acl.tecnico3','left');
        $this->db->join('personal per4','per4.personalId=acl.tecnico4','left');
        $this->db->join('personal perf','perf.personalId=acl.per_fin','left');
        $this->db->join('clientes cli','cli.id=acl.clienteId');
        $this->db->join('rutas zo','zo.id=acl.zonaId');
        $this->db->join('asignacion_ser_cliente_a_d acld','acld.asignacionId=acl.asignacionId');
        $this->db->join('polizas pol','pol.id=acld.tpoliza');
        $this->db->join('servicio_evento sereve','sereve.id=acld.tservicio','left');
        $this->db->join('polizas_detalles pold','pold.id=acld.tservicio_a','left');

        if ($tiposer==1) {
            $this->db->where(array('acl.tiposervicio'=>0));
        }
        if ($tiposer==2) {
            $this->db->where(array('acl.tiposervicio'=>1));
        }
        if ($tecnico>0) {
            $this->db->where("(per.personalId='$tecnico' or per2.personalId='$tecnico')");
        }
        //================================================
        if ($status==1) {//en espera
            $this->db->where(array('acld.status'=>0));
        }
        if ($status==2) {//proceso
            $this->db->where(array('acld.status'=>1));
        }
        if ($status==3) {//finalizados
            $this->db->where(array('acld.status'=>2));
        }
        if ($status==4) {//sin atencion
            $this->db->where(array('acld.status'=>0));
            if($this->horaactual>='17:00:00'){
              $this->db->where('acl.fecha <=',$this->fechaactual);  
          }else{
            $this->db->where('acl.fecha <',$this->fechaactual);
          }
        }
        //========================================
        if ($zona>0) {
            $this->db->where(array('acl.zonaId'=>$zona));
        }
        if ($pcliente>0) {
            $this->db->where(array('cli.id'=>$pcliente));
        }
        if ($fechainicio!='') {
            $this->db->where('acl.fecha >=',$fechainicio);
        }
        if ($fechafin!='' and $status!=4) {
            $this->db->where('acl.fecha <=',$fechafin);
        }
        if($noti>0){
            $this->db->where(array('acld.notificarerror_estatus'=>1));
        }
        $this->db->where(array('acl.activo'=>1));
        $this->db->where(array('acld.activo'=>$estatus_ad));


        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columnss as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->DB4->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    function getlistaservicioclt($params){
        $tiposer = $params['tiposer'];
        $tecnico = $params['tecnico'];
        $status = $params['status'];
        $estatus_ad = $params['estatus_ad'];
        $zona = $params['zona'];
        $pcliente = $params['pcliente'];
        $fechainicio = $params['fechainicio'];
        $fechafin = $params['fechafin'];
        $noti = $params['noti'];
        $columns = array( 
            0=>'acl.asignacionId',
            1=>'per.personalId',
            2=>"CONCAT(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno) as tecnico",
            3=>'cli.id',
            4=>'cli.empresa',
            5=>'acl.fecha',
            6=>'acl.hora',
            7=>'acl.horafin',
            8=>'acl.zonaId',
            9=>'acl.prioridad',
            10=>'zo.nombre',
            11=>'acld.status',
            12=>'acl.tiposervicio',
            13=>'pol.nombre as servicio',
            14=>'acl.fecha',
            15=>'acld.horaserinicio',
            16=>'acld.horaserfin',
            17=>'acld.serie',
            18=>'acld.asignacionIdd',
            19=>'acld.horaserinicioext',
            20=>'acld.horaserfinext',
            21=>'acl.hora_comida',
            22=>'acl.horafin_comida',
            23=>'acld.direccion',
        );
        $columnss = array( 
            0=>'acl.asignacionId',
            1=>'per.personalId',
            2=>"CONCAT(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno)",
            3=>'cli.id',
            4=>'cli.empresa',
            5=>'acl.fecha',
            6=>'acl.hora',
            7=>'acl.horafin',
            8=>'acl.zonaId',
            9=>'acl.prioridad',
            10=>'zo.nombre',
            11=>'acld.status',
            12=>'acl.tiposervicio',
            13=>'pol.nombre',
            14=>'acl.fecha',
            15=>'acld.horaserinicio',
            16=>'acld.horaserfin',
            17=>'acld.serie',
            18=>'acld.asignacionIdd',
            19=>'acld.horaserinicioext',
            20=>'acld.horaserfinext',
            21=>'acl.hora_comida',
            22=>'acl.horafin_comida',
            23=>'acld.direccion',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->DB4->select('COUNT(*) as total');
        $this->DB4->from('asignacion_ser_cliente_a acl');
        $this->DB4->join('personal per','per.personalId=acl.tecnico');
        $this->DB4->join('clientes cli','cli.id=acl.clienteId');
        $this->DB4->join('rutas zo','zo.id=acl.zonaId');
        $this->DB4->join('asignacion_ser_cliente_a_d acld','acld.asignacionId=acl.asignacionId');
        $this->DB4->join('polizas pol','pol.id=acld.tpoliza');
        $this->DB4->join('servicio_evento sereve','sereve.id=acld.tservicio','left');
        $this->DB4->join('polizas_detalles pold','pold.id=acld.tservicio_a','left');
        if ($tiposer==1) {
            $this->DB4->where(array('acl.tiposervicio'=>0));
        }
        if ($tiposer==2) {
            $this->DB4->where(array('acl.tiposervicio'=>1));
        }
        if ($tecnico>0) {
            $this->DB4->where(array('per.personalId'=>$tecnico));
        }
        //================================================
        if ($status==1) {//en espera
            $this->DB4->where(array('acld.status'=>0));
        }
        if ($status==2) {//proceso
            $this->DB4->where(array('acld.status'=>1));
        }
        if ($status==3) {//finalizados
            $this->DB4->where(array('acld.status'=>2));
        }
        if ($status==4) {//sin atencion
            $this->DB4->where(array('acld.status'=>0));
            if($this->horaactual>='17:00:00'){
              $this->DB4->where('acl.fecha <=',$this->fechaactual);  
          }else{
            $this->DB4->where('acl.fecha <',$this->fechaactual);
          }
        }
        //========================================
        if ($zona>0) {
            $this->DB4->where(array('acl.zonaId'=>$zona));
        }
        if ($pcliente>0) {
            $this->DB4->where(array('cli.id'=>$pcliente));
        }
        if ($fechainicio!='') {
            $this->DB4->where('acl.fecha >=',$fechainicio);
        }
        if ($fechafin!='' and $status!=4) {
            $this->DB4->where('acl.fecha <=',$fechafin);
        }
        if($noti>0){
            $this->DB4->where(array('acld.notificarerror_estatus'=>1));
        }
        $this->DB4->where(array('acl.activo'=>1));
        $this->DB4->where(array('acld.activo'=>$estatus_ad));
        /*
        $where = array('ca.status'=>1);
        $this->DB4->where($where);
        */
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->DB4->group_start();
            foreach($columnss as $c){
                $this->DB4->or_like($c,$search);
            }
            $this->DB4->group_end();  
        }            
        //$this->DB4->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        //$this->DB4->limit($params['length'],$params['start']);
        //echo $this->DB4->get_compiled_select();
        $query=$this->DB4->get();
        // print_r($query); die;
        return $query->row()->total;
    }
    function getlistaserviciocl_reporte($params){
        $tiposer = $params['tiposer'];
        $tecnico = $params['tecnico'];
        $status = $params['status'];
        $zona = $params['zona'];
        $pcliente = $params['pcliente'];
        $fechainicio = $params['fechainicio'];
        $fechafin = $params['fechafin'];
        if(isset($params['tipoc'])){
            $tipoc = $params['tipoc'];
        }else{
            $tipoc=0;
        }

        $columns = array( 
            0=>'acl.asignacionId',
            1=>"CONCAT(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno) as tecnico",
            2=>'cli.empresa',
            3=>'acl.fecha',
            4=>'acl.hora',
            5=>'acl.horafin',
            6=>'acld.direccion',
            7=>'acl.tserviciomotivo',
            8=>'acl.comentariocal',
            9=>'acld.status',
            10=>'acld.serie',
            11=>'eq.modelo',
            12=>"CONCAT(per2.nombre,' ',per2.apellido_paterno,' ',per2.apellido_materno) as tecnico2",
            13=>"acl.reg",
            14=>'acl.comentario',
            15=>'sereve.nombre as servicio',
            16=>'acld.tservicio_a',
            17=>'pol.nombre as poliza',
            18=>'acl.per_fin',
            19=>"CONCAT(per3.nombre,' ',per3.apellido_paterno,' ',per3.apellido_materno) as tecnicofin",
            20=>'pexs.id_ser idserpexs',
            21=>'pexs.id_tipo tipopexs',
            22=>'acld.horaserinicio',
            23=>'acld.horaserfin',
            24=>'acld.tecnico as idtecnico_ex',
            25=>"CONCAT(per4.nombre,' ',per4.apellido_paterno,' ',per4.apellido_materno) as tecnico_ex",
            26=>'acld.horaserinicioext',
            27=>'acld.horaserinicioext',
            28=>'acld.comentario_tec'
        );
            if($tipoc==1){
                $columns['24']='count(*) as numequipos';
                $columns['25']='GROUP_CONCAT(DISTINCT(per.nombre)) as tecnicogroup';
            }
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('asignacion_ser_cliente_a acl');
        $this->db->join('personal per','per.personalId=acl.tecnico');
        $this->db->join('personal per2','per2.personalId=acl.tecnico2','left');
        $this->db->join('clientes cli','cli.id=acl.clienteId');
        $this->db->join('rutas zo','zo.id=acl.zonaId');
        $this->db->join('asignacion_ser_cliente_a_d acld','acld.asignacionId=acl.asignacionId');
        $this->db->join('polizas pol','pol.id=acld.tpoliza');
        $this->db->join('servicio_evento sereve','sereve.id=acld.tservicio','left');
        $this->db->join('polizas_detalles pold','pold.id=acld.tservicio_a','left');
        $this->db->join('equipos eq','eq.id=pold.modelo','left');
        $this->db->join('personal per3','per3.personalId=acl.per_fin','left');
        $this->db->join('polizascreadas_ext_servisios as pexs','pexs.id_ser_garantia=acl.asignacionId and pexs.id_tipo_garantia=3','left');
        $this->db->join('personal per4','per4.personalId=acld.tecnico','left');
        if($tipoc==1){
            $this->db->group_by(array("cli.empresa", "acld.direccion"));
        }
        if ($tiposer==1) {
            $this->db->where(array('acl.tiposervicio'=>0));
        }
        if ($tiposer==2) {
            $this->db->where(array('acl.tiposervicio'=>1));
        }
        if ($tecnico>0) {
            $this->db->where(array('per.personalId'=>$tecnico));
        }
        if ($status==1) {
            $this->db->where('acl.status >=',0);
            $this->db->where('acl.status <=',1);
        }
        if ($status==2) {
            $this->db->where(array('acl.status'=>2));
        }
        if ($zona>0) {
            $this->db->where(array('acl.zonaId'=>$zona));
        }
        if ($pcliente>0) {
            $this->db->where(array('cli.id'=>$pcliente));
        }
        if ($fechainicio!='') {
            $this->db->where('acl.fecha >=',$fechainicio);
        }
        if ($fechafin!='') {
            $this->db->where('acl.fecha <=',$fechafin);
        }

        $this->db->where(array('acl.activo'=>1));
        $this->db->where(array('acld.activo'=>1));
        $this->db->order_by('acl.asignacionId', 'DESC');

        $query=$this->db->get(); 
        return $query;
    }
//==============clientes solicitudes==========
    function getlistaserviciocl_solicitudes($params){
        $tiposer = $params['tiposer'];
        $tecnico = $params['tecnico'];
        $status = $params['status'];
        $zona = $params['zona'];
        $pcliente = $params['pcliente'];
        $fechainicio = $params['fechainicio'];
        $fechafin = $params['fechafin'];
        $columns = array( 
            0=>'acl.asignacionId',
            1=>'sereve.id',
            2=>'per.personalId',
            3=>'cli.id',
            4=>'acl.fecha',
            5=>'equ.modelo',
            6=>'acld.serie',
            7=>'acld.cot_refaccion',
            8=>'acld.cot_status',
            9=>'acld.priorizar_cot',
            10=>'acld.prioridad',
            11=>"CONCAT(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno) as tecnico",
            12=>'cli.empresa',
            13=>'acl.zonaId',
            14=>'acl.prioridad',
            15=>'acl.status',
            16=>'acl.tiposervicio',
            17=>'sereve.nombre as servicio',
            18=>'acld.cot_tipo',
            19=>'acld.cot_referencia',
            20=>'acld.cot_referencia_tipo',
            21=>'acl.hora_comida',
            22=>'acl.horafin_comida',
            23=>'equ.id idEquipo',
            24=>'acld.asignacionIdd',
            25=>'acld.cot_refaccion2',
            26=>'acld.cot_refaccion3',
            27=>'acld.cot_status_ob',
            28=>'acld.direccion as direccionservicio',
            29=>"CONCAT(per2.nombre,' ',per2.apellido_paterno,' ',per2.apellido_materno) as tecnico2",
            30=>'acld.cot_contador',
            31=>'acl.per_fin',
            32=>"CONCAT(perf.nombre,' ',perf.apellido_paterno,' ',perf.apellido_materno) as tecnicof",
            33=>'acld.prioridad as prioridad1',
            34=>'acld.prioridad2',
            35=>'acld.prioridad3',
            36=>'acld.cot_referencia2',
            37=>'acld.cot_referencia3',
            
        );
        $columnss = array( 
            0=>'acl.asignacionId',
            1=>'per.personalId',
            2=>"CONCAT(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno)",
            3=>'cli.id',
            4=>'cli.empresa',
            5=>'acl.fecha',
            6=>'acl.prioridad',
            7=>'acl.status',
            8=>'acl.tiposervicio',
            9=>'sereve.nombre',
            10=>'acl.hora_comida',
            11=>'acl.horafin_comida',
            12=>'acld.serie',
            13=>'equ.id',
            14=>'equ.modelo',
            15=>'acld.asignacionIdd',
            16=>'acld.cot_contador'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->DB4->select($select);
        $this->DB4->from('asignacion_ser_cliente_a acl');
        $this->DB4->join('personal per','per.personalId=acl.tecnico');
        $this->DB4->join('clientes cli','cli.id=acl.clienteId');
        //$this->DB4->join('rutas zo','zo.id=acl.zonaId');
        $this->DB4->join('servicio_evento sereve','sereve.id=acl.tservicio','left');
        $this->DB4->join('asignacion_ser_cliente_a_d acld','acld.asignacionId=acl.asignacionId','left');
        $this->DB4->join('polizas_detalles polizad','polizad.id=acld.tservicio_a','left');
        $this->DB4->join('equipos equ','equ.id=polizad.modelo','left');
        $this->DB4->join('personal per2','per2.personalId=acl.tecnico2','left');
        $this->DB4->join('personal perf','perf.personalId=acl.per_fin','left');

        if ($tiposer==1) {
            $this->DB4->where(array('acl.tiposervicio'=>0));
        }
        if ($tiposer==2) {
            $this->DB4->where(array('acl.tiposervicio'=>1));
        }
        if ($tecnico>0) {
            $this->DB4->where("(per.personalId='$tecnico' or perf.personalId='$tecnico')");
        }
        if ($status==1) {
            $this->DB4->where('acl.status >=',0);
            $this->DB4->where('acl.status <=',1);
        }
        if ($status==2) {
            $this->DB4->where(array('acl.status'=>2));
        }
        if ($zona>0) {
            $this->DB4->where(array('acl.zonaId'=>$zona));
        }
        if ($pcliente>0) {
            $this->DB4->where(array('cli.id'=>$pcliente));
        }
        $this->DB4->where(array('acld.cot_status !='=>0));
        
        if ($fechainicio!='') {
            $this->DB4->where('acl.fecha >=',$fechainicio);
        }
        if ($fechafin!='') {
            $this->DB4->where('acl.fecha <=',$fechafin);
        }
        

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->DB4->group_start();
            foreach($columnss as $c){
                $this->DB4->or_like($c,$search);
            }
            $this->DB4->group_end();  
        }            
        $this->DB4->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->DB4->limit($params['length'],$params['start']);
        //echo $this->DB4->get_compiled_select();
        $query=$this->DB4->get();
        // print_r($query); die;
        return $query;
    }
    function getlistaservicioclt_solicitudes($params){
        $tiposer = $params['tiposer'];
        $tecnico = $params['tecnico'];
        $status = $params['status'];
        $zona = $params['zona'];
        $pcliente = $params['pcliente'];
        $fechainicio = $params['fechainicio'];
        $fechafin = $params['fechafin'];
        $columns = array( 
            0=>'acl.asignacionId',
            1=>'per.personalId',
            2=>"CONCAT(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno) as tecnico",
            3=>'cli.id',
            4=>'cli.empresa',
            5=>'acl.fecha',
            6=>'acl.prioridad',
            7=>'acl.status',
            8=>'acl.tiposervicio',
            9=>'sereve.nombre as servicio',
            10=>'acl.hora_comida',
            11=>'acl.horafin_comida',
            12=>'acld.asignacionIdd',
            13=>'acld.cot_contador'
        );
        $columnss = array( 
            0=>'acl.asignacionId',
            1=>'per.personalId',
            2=>"CONCAT(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno)",
            3=>'cli.id',
            4=>'cli.empresa',
            5=>'acl.fecha',
            6=>'acl.prioridad',
            7=>'acl.status',
            8=>'acl.tiposervicio',
            9=>'sereve.nombre',
            10=>'acl.hora_comida',
            11=>'acl.horafin_comida',
            12=>'acld.asignacionIdd',
            13=>'acld.cot_contador'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->DB4->select('COUNT(*) as total');
        $this->DB4->from('asignacion_ser_cliente_a acl');
        $this->DB4->join('personal per','per.personalId=acl.tecnico');
        $this->DB4->join('clientes cli','cli.id=acl.clienteId');
        //$this->DB4->join('rutas zo','zo.id=acl.zonaId');
        $this->DB4->join('servicio_evento sereve','sereve.id=acl.tservicio','left');
        $this->DB4->join('asignacion_ser_cliente_a_d acld','acld.asignacionId=acl.asignacionId','left');
        $this->DB4->join('personal perf','perf.personalId=acl.per_fin','left');

        if ($tiposer==1) {
            $this->DB4->where(array('acl.tiposervicio'=>0));
        }
        if ($tiposer==2) {
            $this->DB4->where(array('acl.tiposervicio'=>1));
        }
        if ($tecnico>0) {
            $this->DB4->where("(per.personalId='$tecnico' or perf.personalId='$tecnico')");
        }
        if ($status==1) {
            $this->DB4->where('acl.status >=',0);
            $this->DB4->where('acl.status <=',1);
        }
        if ($status==2) {
            $this->DB4->where(array('acl.status'=>2));
        }
        if ($zona>0) {
            $this->DB4->where(array('acl.zonaId'=>$zona));
        }
        if ($pcliente>0) {
            $this->DB4->where(array('cli.id'=>$pcliente));
        }
        $this->DB4->where(array('acld.cot_status !='=>0));
        
        if ($fechainicio!='') {
            $this->DB4->where('acl.fecha >=',$fechainicio);
        }
        if ($fechafin!='') {
            $this->DB4->where('acl.fecha <=',$fechafin);
        }
        
        /*
        $where = array('ca.status'=>1);
        $this->DB4->where($where);
        */
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->DB4->group_start();
            foreach($columnss as $c){
                $this->DB4->or_like($c,$search);
            }
            $this->DB4->group_end();  
        }            
        //$this->DB4->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        //$this->DB4->limit($params['length'],$params['start']);
        //echo $this->DB4->get_compiled_select();
        $query=$this->DB4->get();
        // print_r($query); die;
        return $query->row()->total;
    }
//=========================================
//==============ventas==========
    function getlistaserviciovent($params){
        $tiposer = $params['tiposer'];
        $tecnico = $params['tecnico'];
        $status = $params['status'];
        $estatus_ad = $params['estatus_ad'];
        $zona = $params['zona'];
        $pcliente = $params['pcliente'];
        $fechainicio = $params['fechainicio'];
        $fechafin = $params['fechafin'];
        $noti = $params['noti'];

        $columns = array( 
            0=>'acl.asignacionId',
            1=>'per.personalId',
            2=>"CONCAT(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno) as tecnico",
            3=>'cli.id',
            4=>'cli.empresa',
            5=>'acl.fecha',
            6=>'acl.hora',
            7=>'acl.horafin',
            8=>'acl.zonaId',
            9=>'acl.prioridad',
            10=>'zo.nombre',
            11=>'acl.status',
            12=>'acl.tiposervicio',
            13=>'sereve.nombre as servicio',
            14=>'acl.t_fecha',
            15=>'acl.horaserinicio',
            16=>'acl.horaserfin',
            17=>'acl.horaserinicioext',
            18=>'acl.horaserfinext',
            19=>'acl.editado',
            20=>'acl.hora_comida',
            21=>'acl.horafin_comida',
            22=>'acl.activo',
            23=>'acl.infodelete',
            24=>'acl.ventaId',
            25=>'acl.tipov',
            26=>'acl.prioridadr',
            27=>'acl.notificarerror_estatus',
            28=>'acl.notificarerror',
            29=>'per2.personalId as personalId2',
            30=>"CONCAT(per2.nombre,' ',per2.apellido_paterno,' ',per2.apellido_materno) as tecnico2",
            31=>"CONCAT(per3.nombre,' ',per3.apellido_paterno,' ',per3.apellido_materno) as tecnico3",
            32=>"CONCAT(per4.nombre,' ',per4.apellido_paterno,' ',per4.apellido_materno) as tecnico4",
            33=>'acl.comentario',
            34=>"CONCAT(perf.nombre,' ',perf.apellido_paterno,' ',perf.apellido_materno) as tecnicof",
            35=>'perf.personalId as personalIdf',
            36=>"SUBTIME(acl.horafin, '00:30:00') hora_limite",
            37=>'acl.nr',
            38=>'acl.nr_coment',
            39=>'acl.nr_coment_tipo',
            40=>'ven.not_dev',
            41=>'acl.prioridadr2',
            42=>'acl.prioridadr3',

        );
        $columnss = array( 
            0=>'acl.asignacionId',
            1=>'per.personalId',
            2=>"CONCAT(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno)",
            3=>'cli.id',
            4=>'cli.empresa',
            5=>'acl.fecha',
            6=>'acl.hora',
            7=>'acl.horafin',
            8=>'acl.zonaId',
            9=>'acl.prioridad',
            10=>'zo.nombre',
            11=>'acl.status',
            12=>'acl.tiposervicio',
            13=>'sereve.nombre',
            14=>'acl.horaserinicio',
            15=>'acl.horaserfin',
            16=>'acl.hora_comida',
            17=>'acl.horafin_comida',
            18=>'acl.ventaId',
            19=>'acl.tipov'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->DB4->select($select);
        $this->DB4->from('asignacion_ser_venta_a acl');
        $this->DB4->join('personal per','per.personalId=acl.tecnico');
        $this->DB4->join('personal per2','per2.personalId=acl.tecnico2','left');
        $this->DB4->join('personal per3','per3.personalId=acl.tecnico3','left');
        $this->DB4->join('personal per4','per4.personalId=acl.tecnico4','left');
        $this->DB4->join('personal perf','perf.personalId=acl.per_fin','left');
        $this->DB4->join('clientes cli','cli.id=acl.clienteId');
        $this->DB4->join('rutas zo','zo.id=acl.zonaId');
        $this->DB4->join('servicio_evento sereve','sereve.id=acl.tservicio','left');
        $this->DB4->join('ventas ven','ven.id=acl.ventaId','left');
        
        if ($tiposer==1) {
            $this->DB4->where(array('acl.tiposervicio'=>0));
        }
        if ($tiposer==2) {
            $this->DB4->where(array('acl.tiposervicio'=>1));
        }
        if ($tecnico>0) {
            $this->DB4->where("(per.personalId='$tecnico' or per2.personalId='$tecnico')");
        }
        //================================================
        if ($status==1) {//en espera
            $this->DB4->where(array('acl.status'=>0));
        }
        if ($status==2) {//proceso
            $this->DB4->where(array('acl.status'=>1));
        }
        if ($status==3) {//finalizados
            $this->DB4->where(array('acl.status'=>2));
        }
        if ($status==4) {//sin atencion
            $this->DB4->where(array('acl.status'=>0));
            if($this->horaactual>='17:00:00'){
              $this->DB4->where('acl.fecha <=',$this->fechaactual);  
          }else{
            $this->DB4->where('acl.fecha <',$this->fechaactual);
          }
        }
        //========================================
        if ($zona>0) {
            $this->DB4->where(array('acl.zonaId'=>$zona));
        }
        if ($pcliente>0) {
            $this->DB4->where(array('cli.id'=>$pcliente));
        }
        if ($fechainicio!='') {
            $this->DB4->where('acl.fecha >=',$fechainicio);
        }
        if ($fechafin!='' and $status!=4) {
            $this->DB4->where('acl.fecha <=',$fechafin);
        }
        if($noti>0){
            $this->DB4->where(array('acl.notificarerror_estatus'=>1));
        }
        $this->DB4->where(array('acl.activo'=>$estatus_ad));


        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->DB4->group_start();
            foreach($columnss as $c){
                $this->DB4->or_like($c,$search);
            }
            $this->DB4->group_end();  
        }            
        $this->DB4->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->DB4->limit($params['length'],$params['start']);
        //echo $this->DB4->get_compiled_select();
        $query=$this->DB4->get();
        // print_r($query); die;
        return $query;
    }
    function getlistaservicioventt($params){
        $tiposer = $params['tiposer'];
        $tecnico = $params['tecnico'];
        $status = $params['status'];
        $estatus_ad = $params['estatus_ad'];
        $zona = $params['zona'];
        $pcliente = $params['pcliente'];
        $fechainicio = $params['fechainicio'];
        $fechafin = $params['fechafin'];
        $noti = $params['noti'];
        $columns = array( 
            0=>'acl.asignacionId',
            1=>'per.personalId',
            2=>"CONCAT(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno) as tecnico",
            3=>'cli.id',
            4=>'cli.empresa',
            5=>'acl.fecha',
            6=>'acl.hora',
            7=>'acl.horafin',
            8=>'acl.zonaId',
            9=>'acl.prioridad',
            10=>'zo.nombre',
            11=>'acl.status',
            12=>'acl.tiposervicio',
            13=>'sereve.nombre as servicio',
            14=>'acl.hora_comida',
            15=>'acl.horafin_comida',
            16=>'acl.ventaId',
            17=>'acl.tipov'
        );
        $columnss = array( 
            0=>'acl.asignacionId',
            1=>'per.personalId',
            2=>"CONCAT(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno)",
            3=>'cli.id',
            4=>'cli.empresa',
            5=>'acl.fecha',
            6=>'acl.hora',
            7=>'acl.horafin',
            8=>'acl.zonaId',
            9=>'acl.prioridad',
            10=>'zo.nombre',
            11=>'acl.status',
            12=>'acl.tiposervicio',
            13=>'sereve.nombre',
            14=>'acl.hora_comida',
            15=>'acl.horafin_comida',
            16=>'acl.ventaId',
            17=>'acl.tipov'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->DB4->select('COUNT(*) as total');
        $this->DB4->from('asignacion_ser_venta_a acl');
        $this->DB4->join('personal per','per.personalId=acl.tecnico');
        $this->DB4->join('clientes cli','cli.id=acl.clienteId');
        $this->DB4->join('rutas zo','zo.id=acl.zonaId');
        $this->DB4->join('servicio_evento sereve','sereve.id=acl.tservicio','left');

        if ($tiposer==1) {
            $this->DB4->where(array('acl.tiposervicio'=>0));
        }
        if ($tiposer==2) {
            $this->DB4->where(array('acl.tiposervicio'=>1));
        }
        if ($tecnico>0) {
            $this->DB4->where(array('per.personalId'=>$tecnico));
        }
        //================================================
        if ($status==1) {//en espera
            $this->DB4->where(array('acl.status'=>0));
        }
        if ($status==2) {//proceso
            $this->DB4->where(array('acl.status'=>1));
        }
        if ($status==3) {//finalizados
            $this->DB4->where(array('acl.status'=>2));
        }
        if ($status==4) {//sin atencion
            $this->DB4->where(array('acl.status'=>0));
            if($this->horaactual>='17:00:00'){
              $this->DB4->where('acl.fecha <=',$this->fechaactual);  
          }else{
            $this->DB4->where('acl.fecha <',$this->fechaactual);
          }
        }
        //========================================
        if ($zona>0) {
            $this->DB4->where(array('acl.zonaId'=>$zona));
        }
        if ($pcliente>0) {
            $this->DB4->where(array('cli.id'=>$pcliente));
        }
        if ($fechainicio!='') {
            $this->DB4->where('acl.fecha >=',$fechainicio);
        }
        if ($fechafin!='' and $status!=4) {
            $this->DB4->where('acl.fecha <=',$fechafin);
        }
        if($noti>0){
            $this->DB4->where(array('acl.notificarerror_estatus'=>1));
        }
        $this->DB4->where(array('acl.activo'=>$estatus_ad));
        /*
        $where = array('ca.status'=>1);
        $this->DB4->where($where);
        */
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->DB4->group_start();
            foreach($columnss as $c){
                $this->DB4->or_like($c,$search);
            }
            $this->DB4->group_end();  
        }            
        //$this->DB4->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        //$this->DB4->limit($params['length'],$params['start']);
        //echo $this->DB4->get_compiled_select();
        $query=$this->DB4->get();
        // print_r($query); die;
        return $query->row()->total;
    }
    function getlistaserviciovent_reporte($params){
        $tiposer = $params['tiposer'];
        $tecnico = $params['tecnico'];
        $status = $params['status'];
        $zona = $params['zona'];
        $pcliente = $params['pcliente'];
        $fechainicio = $params['fechainicio'];
        $fechafin = $params['fechafin'];
        if(isset($params['tipoc'])){
            $tipoc = $params['tipoc'];
        }else{
            $tipoc=0;
        }
        $columns = array( 
            0=>'acl.asignacionId',
            1=>"CONCAT(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno) as tecnico",
            2=>'cli.empresa',
            3=>'acl.fecha',
            4=>'acl.hora',
            5=>'acl.horafin',
            6=>'acl.ventaId',
            7=>'acl.tipov',
            8=>'acld.direccion',
            9=>'acl.comentariocal',
            10=>'acl.tserviciomotivo',
            11=>'acl.status',
            12=>'acld.serie',
            13=>'eq.modelo',
            14=>"CONCAT(per2.nombre,' ',per2.apellido_paterno,' ',per2.apellido_materno) as tecnico2",
            15=>'acl.comentario',
            16=>"acl.reg",
            17=>'sereve.nombre as servicio',
            18=>'acld.tservicio_a',
            19=>'pol.nombre as poliza',
            20=>'acl.per_fin',
            21=>"CONCAT(per3.nombre,' ',per3.apellido_paterno,' ',per3.apellido_materno) as tecnicofin",
            22=>'pexs.id_ser idserpexs',
            23=>'pexs.id_tipo tipopexs',
            24=>'acl.horaserinicioext',
            25=>'acl.horaserfinext',
            26=>'acl.comentario_tec'
        );
            if($tipoc==1){
                $columns['24']='count(*) as numequipos';
                //$columns['20']='1 as numequipos';
                //$columns['14']='GROUP_CONCAT(DISTINCT(per.nombre)) as tecnicogroup';
            }
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('asignacion_ser_venta_a acl');
        $this->db->join('personal per','per.personalId=acl.tecnico');
        $this->db->join('personal per2','per2.personalId=acl.tecnico2','left');
        $this->db->join('clientes cli','cli.id=acl.clienteId');
        //$this->db->join('rutas zo','zo.id=acl.zonaId');
        $this->db->join('asignacion_ser_venta_a_d acld','acld.asignacionId=acl.asignacionId');
        $this->db->join('servicio_evento sereve','sereve.id=acl.tservicio','left');
        $this->db->join('polizas_detalles pold','pold.id=acld.tservicio_a','left');
        $this->db->join('equipos eq','eq.id=pold.modelo','left');
        $this->db->join('polizas pol','pol.id=acld.tpoliza','left');
        $this->db->join('personal per3','per3.personalId=acl.per_fin','left');
        $this->db->join('polizascreadas_ext_servisios as pexs','pexs.id_ser_garantia=acl.asignacionId and pexs.id_tipo_garantia=4','left');
        
        if ($tiposer==1) {
            $this->db->where(array('acl.tiposervicio'=>0));
        }
        if ($tiposer==2) {
            $this->db->where(array('acl.tiposervicio'=>1));
        }
        if ($tecnico>0) {
            $this->db->where(array('per.personalId'=>$tecnico));
        }
        if ($status==1) {
            $this->db->where('acl.status >=',0);
            $this->db->where('acl.status <=',1);
        }
        if ($status==2) {
            $this->db->where(array('acl.status'=>2));
        }
        if ($zona>0) {
            $this->db->where(array('acl.zonaId'=>$zona));
        }
        if ($pcliente>0) {
            $this->db->where(array('cli.id'=>$pcliente));
        }
        if ($fechainicio!='') {
            $this->db->where('acl.fecha >=',$fechainicio);
        }
        if ($fechafin!='') {
            $this->db->where('acl.fecha <=',$fechafin);
        }

        $this->db->where(array('acl.activo'=>1));
        $this->db->order_by('acl.asignacionId', 'DESC');
        $query=$this->db->get(); 
        
        return $query;
    }
//==============clientes solicitudes==========
    function getlistaserviciovent_solicitudes($params){
        $tiposer = $params['tiposer'];
        $tecnico = $params['tecnico'];
        $status = $params['status'];
        $zona = $params['zona'];
        $pcliente = $params['pcliente'];
        $fechainicio = $params['fechainicio'];
        $fechafin = $params['fechafin'];
        $columns = array( 
            0=>'acl.asignacionId',
            1=>'sereve.id',
            2=>'per.personalId',
            3=>'cli.id',
            4=>'acl.fecha',
            5=>'equ.modelo',
            6=>'acld.serie',
            7=>'acl.cot_refaccion',
            8=>'acl.prioridadr',
            9=>'acl.cot_status',
            10=>'acl.priorizar_cot',
            11=>"CONCAT(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno) as tecnico",
            12=>'cli.empresa',
            13=>'acl.horafin',
            14=>'acl.zonaId',
            15=>'acl.prioridad',
            16=>'acl.status',
            17=>'acl.tiposervicio',
            18=>'sereve.nombre as servicio',
            19=>'acl.cot_tipo',
            20=>'acl.cot_referencia',
            21=>'acl.cot_referencia_tipo',
            22=>'acl.hora_comida',
            23=>'acl.horafin_comida',
            24=>'equ.id idEquipo',
            25=>'acl.cot_refaccion2',
            26=>'acl.cot_refaccion3',
            27=>'acl.cot_status_ob',
            28=>'acl.cot_contador',
            29=>'acl.per_fin',
            30=>"CONCAT(perf.nombre,' ',perf.apellido_paterno,' ',perf.apellido_materno) as tecnicof",
            31=>'acl.prioridadr2',
            32=>'acl.prioridadr3',
            33=>'acl.cot_referencia2',
            34=>'acl.cot_referencia3',
            

        );
        $columnss = array( 
            0=>'acl.asignacionId',
            1=>'per.personalId',
            2=>"CONCAT(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno)",
            3=>'cli.id',
            4=>'cli.empresa',
            5=>'acl.fecha',
            6=>'acl.prioridad',
            7=>'acl.status',
            8=>'acl.tiposervicio',
            9=>'sereve.nombre',
            10=>'acl.hora_comida',
            11=>'acl.horafin_comida',
            12=>'acld.serie',
            13=>'equ.id',
            14=>'equ.modelo',
            15=>'acl.cot_contador'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->DB4->select($select);
        $this->DB4->from('asignacion_ser_venta_a acl');
        $this->DB4->join('personal per','per.personalId=acl.tecnico');
        $this->DB4->join('clientes cli','cli.id=acl.clienteId');
        //$this->DB4->join('rutas zo','zo.id=acl.zonaId');
        $this->DB4->join('servicio_evento sereve','sereve.id=acl.tservicio','left');
        $this->DB4->join('asignacion_ser_venta_a_d acld','acld.asignacionId=acl.asignacionId','left');
        $this->DB4->join('polizas_detalles pold','pold.id=acld.tservicio_a','left');
        $this->DB4->join('equipos equ','equ.id=pold.modelo','left');
        $this->DB4->join('personal perf','perf.personalId=acl.per_fin','left');

        if ($tiposer==1) {
            $this->DB4->where(array('acl.tiposervicio'=>0));
        }
        if ($tiposer==2) {
            $this->DB4->where(array('acl.tiposervicio'=>1));
        }
        if ($tecnico>0) {
            $this->DB4->where("(per.personalId='$tecnico' or perf.personalId='$tecnico')");
        }
        if ($status==1) {
            $this->DB4->where('acl.status >=',0);
            $this->DB4->where('acl.status <=',1);
        }
        if ($status==2) {
            $this->DB4->where(array('acl.status'=>2));
        }
        if ($zona>0) {
            $this->DB4->where(array('acl.zonaId'=>$zona));
        }
        if ($pcliente>0) {
            $this->DB4->where(array('cli.id'=>$pcliente));
        }
        $this->DB4->where(array('acl.cot_status !='=>0));
        
        if ($fechainicio!='') {
            $this->DB4->where('acl.fecha >=',$fechainicio);
        }
        if ($fechafin!='') {
            $this->DB4->where('acl.fecha <=',$fechafin);
        }
        

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->DB4->group_start();
            foreach($columnss as $c){
                $this->DB4->or_like($c,$search);
            }
            $this->DB4->group_end();  
        }            
        $this->DB4->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->DB4->limit($params['length'],$params['start']);
        //echo $this->DB4->get_compiled_select();
        $query=$this->DB4->get();
        // print_r($query); die;
        return $query;
    }
    function getlistaservicioventt_solicitudes($params){
        $tiposer = $params['tiposer'];
        $tecnico = $params['tecnico'];
        $status = $params['status'];
        $zona = $params['zona'];
        $pcliente = $params['pcliente'];
        $fechainicio = $params['fechainicio'];
        $fechafin = $params['fechafin'];
        $columns = array( 
            0=>'acl.asignacionId',
            1=>'per.personalId',
            2=>"CONCAT(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno) as tecnico",
            3=>'cli.id',
            4=>'cli.empresa',
            5=>'acl.fecha',
            6=>'acl.prioridad',
            7=>'acl.status',
            8=>'acl.tiposervicio',
            9=>'sereve.nombre as servicio',
            10=>'acl.hora_comida',
            11=>'acl.horafin_comida',
            12=>'acl.cot_contador'
        );
        $columnss = array( 
            0=>'acl.asignacionId',
            1=>'per.personalId',
            2=>"CONCAT(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno)",
            3=>'cli.id',
            4=>'cli.empresa',
            5=>'acl.fecha',
            6=>'acl.prioridad',
            7=>'acl.status',
            8=>'acl.tiposervicio',
            9=>'sereve.nombre',
            10=>'acl.hora_comida',
            11=>'acl.horafin_comida',
            12=>'acl.cot_contador'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->DB4->select('COUNT(*) as total');
        $this->DB4->from('asignacion_ser_venta_a acl');
        $this->DB4->join('personal per','per.personalId=acl.tecnico');
        $this->DB4->join('clientes cli','cli.id=acl.clienteId');
        //$this->DB4->join('rutas zo','zo.id=acl.zonaId');
        $this->DB4->join('servicio_evento sereve','sereve.id=acl.tservicio','left');
        $this->DB4->join('personal perf','perf.personalId=acl.per_fin','left');

        if ($tiposer==1) {
            $this->DB4->where(array('acl.tiposervicio'=>0));
        }
        if ($tiposer==2) {
            $this->DB4->where(array('acl.tiposervicio'=>1));
        }
        if ($tecnico>0) {
            $this->DB4->where("(per.personalId='$tecnico' or perf.personalId='$tecnico')");
        }
        if ($status==1) {
            $this->DB4->where('acl.status >=',0);
            $this->DB4->where('acl.status <=',1);
        }
        if ($status==2) {
            $this->DB4->where(array('acl.status'=>2));
        }
        if ($zona>0) {
            $this->DB4->where(array('acl.zonaId'=>$zona));
        }
        if ($pcliente>0) {
            $this->DB4->where(array('cli.id'=>$pcliente));
        }
        $this->DB4->where(array('acl.cot_status !='=>0));
        
        if ($fechainicio!='') {
            $this->DB4->where('acl.fecha >=',$fechainicio);
        }
        if ($fechafin!='') {
            $this->DB4->where('acl.fecha <=',$fechafin);
        }
        
        /*
        $where = array('ca.status'=>1);
        $this->DB4->where($where);
        */
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->DB4->group_start();
            foreach($columnss as $c){
                $this->DB4->or_like($c,$search);
            }
            $this->DB4->group_end();  
        }            
        //$this->DB4->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        //$this->DB4->limit($params['length'],$params['start']);
        //echo $this->DB4->get_compiled_select();
        $query=$this->DB4->get();
        // print_r($query); die;
        return $query->row()->total;
    }

//=========================================
    //==================================================================
    function historiatecnicos($asi,$table){
        $strq = "SELECT aht.reg, per.nombre, per.apellido_paterno,per.apellido_materno 
                from asignacion_ser_historial_t as aht
                inner JOIN personal as per on per.personalId=aht.tecnicoId
                WHERE aht.asignacionId=$asi and aht.tabless=$table";
        $query = $this->DB4->query($strq);
        $this->DB4->close();
        return $query;

    }
    function reportetecnicose($fechaini,$fechafi,$tecnico){
        /*
        $strq = "SELECT 
                COUNT(*) as setotal,
                SEC_TO_TIME(SUM(TIME_TO_SEC(hora))) as hinicia,
                SEC_TO_TIME(SUM(TIME_TO_SEC(horafin))) as hfin,
                SEC_TO_TIME(
                (SUM(TIME_TO_SEC(horafin))-SUM(TIME_TO_SEC(hora)))
                ) as tiempohabil
                FROM asignacion_ser_cliente_a as asca 
                WHERE asca.tecnico=$tecnico and asca.status=2 and asca.fecha BETWEEN '$fechaini' and '$fechafi' 
                GROUP BY asca.tecnico";
        */
        $strq = "
                SELECT 
                COUNT(*) as setotal,
                SEC_TO_TIME(SUM(TIME_TO_SEC(datos.hora))) as hinicia,
                SEC_TO_TIME(SUM(TIME_TO_SEC(datos.horafin))) as hfin,
                SEC_TO_TIME(
                            (SUM(TIME_TO_SEC(datos.horafin))-SUM(TIME_TO_SEC(datos.hora)))
                            ) as tiempohabil,
                SEC_TO_TIME(
                            SUM(TIME_TO_SEC(datos.tiempolaboral))-(
                                                                        SUM(TIME_TO_SEC(datos.horafin))-
                                                                        SUM(
                                                                            TIME_TO_SEC(datos.hora)
                                                                            )
                                                                    )
                            )
                         as tiempomuerto,
                sum(datos.cot_status) as cot_status
                from(
                    SELECT 
                    asca.hora,
                    asca.horafin,
                    asca.tecnico,
                    '09:30:00' as tiempolaboral,
                    IF(asca.cot_status = 2, 1, 0) as cot_status
                    FROM asignacion_ser_cliente_a as asca 
                    WHERE asca.tecnico=$tecnico and asca.status=2 and asca.fecha BETWEEN '$fechaini' and '$fechafi' 
                    union
                    SELECT 
                    asca.hora,
                    asca.horafin,
                    asca.tecnico,
                    '09:30:00' as tiempolaboral,
                    IF(asca.cot_status = 2, 1, 0) as cot_status
                    FROM asignacion_ser_contrato_a_e as asca 
                    WHERE asca.tecnico=$tecnico and asca.status=2 and asca.fecha BETWEEN '$fechaini' and '$fechafi' 
                    union
                    SELECT 
                    asca.hora,
                    asca.horafin,
                    asca.tecnico,
                    '09:30:00' as tiempolaboral,
                    IF(asca.cot_status = 2, 1, 0) as cot_status
                    FROM asignacion_ser_poliza_a_e as asca 
                    WHERE asca.tecnico=$tecnico and asca.status=2 and asca.fecha BETWEEN '$fechaini' and '$fechafi' 
                    )as datos

                ";
        $query = $this->DB4->query($strq);

        return $query;
    }
    function reportetecnicosecorrectivos($fechaini,$fechafi,$tecnico){
        $strq = "
                SELECT 
                sum(cantidad) as cantidad
                from(
                    SELECT 
                    count(asca.asignacionId) as cantidad
                    FROM asignacion_ser_cliente_a as asca
                    inner join servicio_evento as serev on serev.id=asca.tservicio
                    WHERE serev.nombre like '%correctivo%' and  asca.tecnico=$tecnico and asca.status=2 and asca.fecha BETWEEN '$fechaini' and '$fechafi' 
                    union
                    SELECT 
                    COUNT( DISTINCT ascaa.asignacionId) as cantidad
                    FROM asignacion_ser_contrato_a_e as asca
                    inner join asignacion_ser_contrato_a as ascaa on ascaa.asignacionId=asca.asignacionId
                    inner join servicio_evento as serev on serev.id=ascaa.tservicio
                    WHERE  serev.nombre like '%correctivo%' and asca.tecnico=$tecnico and asca.status=2 and asca.fecha BETWEEN '$fechaini' and '$fechafi' 
                    union
                    SELECT 
                    COUNT( DISTINCT ascaa.asignacionId) as cantidad
                    FROM asignacion_ser_poliza_a_e as asca 
                    inner join asignacion_ser_poliza_a as ascaa on ascaa.asignacionId=asca.asignacionId
                    inner join servicio_evento as serev on serev.id=ascaa.tservicio
                    WHERE  serev.nombre like '%correctivo%' and asca.tecnico=$tecnico and asca.status=2 and asca.fecha BETWEEN '$fechaini' and '$fechafi' 
                    )as datos

                ";
        $query = $this->DB4->query($strq);
        $cantidad=0;
        foreach ($query->result() as $item) {
            $cantidad=$item->cantidad;
        }
        if ($cantidad>0) {
            $cantidad=$cantidad;
        }else{
            $cantidad=0;
        }

        return $cantidad;
    }
    function reportetecnicosegarantias($fechaini,$fechafi,$tecnico){
        $strq = "
                SELECT 
                sum(cantidad) as cantidad
                from(
                    SELECT 
                    count(asca.asignacionId) as cantidad
                    FROM asignacion_ser_cliente_a as asca
                    inner join servicio_evento as serev on serev.id=asca.tservicio
                    WHERE serev.nombre like '%garantia%' and  asca.tecnico=$tecnico and asca.status=2 and asca.fecha BETWEEN '$fechaini' and '$fechafi' 
                    union
                    SELECT 
                    COUNT( DISTINCT ascaa.asignacionId) as cantidad
                    FROM asignacion_ser_contrato_a_e as asca
                    inner join asignacion_ser_contrato_a as ascaa on ascaa.asignacionId=asca.asignacionId
                    inner join servicio_evento as serev on serev.id=ascaa.tservicio
                    WHERE  serev.nombre like '%garantia%' and asca.tecnico=$tecnico and asca.status=2 and asca.fecha BETWEEN '$fechaini' and '$fechafi' 
                    union
                    SELECT 
                    COUNT( DISTINCT ascaa.asignacionId) as cantidad
                    FROM asignacion_ser_poliza_a_e as asca 
                    inner join asignacion_ser_poliza_a as ascaa on ascaa.asignacionId=asca.asignacionId
                    inner join servicio_evento as serev on serev.id=ascaa.tservicio
                    WHERE  serev.nombre like '%garantia%' and asca.tecnico=$tecnico and asca.status=2 and asca.fecha BETWEEN '$fechaini' and '$fechafi' 
                    )as datos

                ";
        $query = $this->DB4->query($strq);
        $cantidad=0;
        foreach ($query->result() as $item) {
            $cantidad=$item->cantidad;
        }
        if ($cantidad>0) {
            $cantidad=$cantidad;
        }else{
            $cantidad=0;
        }

        return $cantidad;
    }
    function reporteatencioncorrectivo($fechaini,$fechafi){
        $strq = "SELECT 
                cl.empresa,
                aserc.reg,
                CONCAT(aserc.fecha,' ',aserc.hora) as llegada,
                '' as tiempoatencion,
                per.nombre,
                SEC_TO_TIME(TIME_TO_SEC(horafin-hora)) as tiempoenservicio,
                3 as tipo
                FROM `asignacion_ser_cliente_a` as aserc
                inner JOIN clientes as cl on cl.id=aserc.clienteId
                inner join personal as per on per.personalId=aserc.tecnico
                where aserc.status=2 and aserc.fecha between '$fechaini' and '$fechafi'
                union
                SELECT 
                cli.empresa,
                asrc.reg,
                CONCAT(aserc.fecha,' ',aserc.hora) as llegada,
                '' as tiempoatencion,
                per.nombre,
                SEC_TO_TIME(TIME_TO_SEC(horafin-hora)) as tiempoenservicio,
                1 as tipo
                FROM `asignacion_ser_contrato_a_e` as aserc
                inner join asignacion_ser_contrato_a as asrc on asrc.asignacionId=aserc.asignacionId
                inner join contrato as con on con.idcontrato=asrc.contratoId
                inner join rentas as ren on ren.id=con.idRenta
                inner JOIN clientes as cli on cli.id=ren.idCliente
                inner join personal as per on per.personalId=aserc.tecnico
                where aserc.status=2 and aserc.fecha between '$fechaini' and '$fechafi'
                union
                SELECT 
                cli.empresa,
                asrc.reg,
                CONCAT(aserc.fecha,' ',aserc.hora) as llegada,
                '' as tiempoatencion,
                per.nombre,
                SEC_TO_TIME(TIME_TO_SEC(horafin-hora)) as tiempoenservicio,
                2 as tipo
                FROM `asignacion_ser_poliza_a_e` as aserc
                inner join asignacion_ser_poliza_a as asrc on asrc.asignacionId=aserc.asignacionId
                inner join polizasCreadas as pol on pol.id=asrc.polizaId
                inner JOIN clientes as cli on cli.id=pol.idCliente
                inner join personal as per on per.personalId=aserc.tecnico
                where aserc.status=2 and aserc.fecha between '$fechaini' and '$fechafi'
                
                ";
        $query = $this->DB4->query($strq);
        return $query;
    }
    function reportefallas($fechaini,$fechafi){
        $strq = "SELECT 
                  cl.empresa,
                  aserc.fecha,
                  aserc.hora,
                  aserc.horafin,
                  SEC_TO_TIME(TIME_TO_SEC(aserc.horafin-aserc.hora)) as tiempoenservicio,
                  sere.sinmodelo,
                  sere.newmodelo,
                  equ.modelo,
                  asercd.serie,
                  sere.descripcion,
                  per.nombre,
                  aserc.tserviciomotivo,
                3 as tipo
                FROM `asignacion_ser_cliente_a` as aserc
                inner JOIN asignacion_ser_cliente_a_d as asercd on asercd.asignacionId=aserc.asignacionId 
                inner JOIN servicio_evento as sere on sere.id=asercd.tservicio
                left join polizas_detalles as poldll on sere.polizamodelo=poldll.id
                left join equipos as equ on equ.id=poldll.modelo
                inner JOIN clientes as cl on cl.id=aserc.clienteId
                inner join personal as per on per.personalId=aserc.tecnico
                where aserc.status=2 and aserc.fecha between '$fechaini' and '$fechafi'
                union

                SELECT 
                 cli.empresa,
                 aserc.fecha,
                 aserc.hora,
                 aserc.horafin,
                 SEC_TO_TIME(TIME_TO_SEC(aserc.horafin-aserc.hora)) as tiempoenservicio,
                 0 as sinmodelo,
                 '' as newmodelo,
                 equ.modelo,
                 sepr.serie,
                 sere.descripcion,
                 per.nombre,
                 asrc.tserviciomotivo,
                1 as tipo
                FROM `asignacion_ser_contrato_a_e` as aserc
                inner join asignacion_ser_contrato_a as asrc on asrc.asignacionId=aserc.asignacionId
                inner join contrato as con on con.idcontrato=asrc.contratoId
                inner join rentas as ren on ren.id=con.idRenta
                inner join rentas_has_detallesEquipos as rend on rend.id=aserc.idequipo
                inner JOIN servicio_evento as sere on sere.id=asrc.tservicio
                inner join equipos as equ on equ.id=rend.idEquipo
                inner JOIN clientes as cli on cli.id=ren.idCliente
                inner join series_productos as sepr on sepr.serieId=aserc.serieId
                inner join personal as per on per.personalId=aserc.tecnico
                where aserc.status=2 and aserc.fecha between '$fechaini' and '$fechafi'

                union
                SELECT 
                  cli.empresa,
                  aserc.fecha,
                  aserc.hora,
                  aserc.horafin,
                  SEC_TO_TIME(TIME_TO_SEC(aserc.horafin-aserc.hora)) as tiempoenservicio,
                  0 as sinmodelo,
                  '' as newmodelo,
                  equ.modelo,
                  aserc.serie,
                  sere.descripcion,
                  per.nombre,
                  asrc.tserviciomotivo,
                2 as tipo
                FROM `asignacion_ser_poliza_a_e` as aserc
                inner join asignacion_ser_poliza_a as asrc on asrc.asignacionId=aserc.asignacionId
                inner join polizasCreadas as pol on pol.id=asrc.polizaId
                inner join polizasCreadas_has_detallesPoliza as pold on pold.id=aserc.idequipo
                inner join equipos as equ on equ.id=pold.idEquipo
                inner JOIN clientes as cli on cli.id=pol.idCliente
                inner JOIN servicio_evento as sere on sere.id=asrc.tservicio
                inner join personal as per on per.personalId=aserc.tecnico
                where aserc.status=2 and aserc.fecha between '$fechaini' and '$fechafi'
                
                ";
        $query = $this->DB4->query($strq);
        return $query;
    }
    function reporteservicioscount($fechaini,$fechafi){
        $strq = "
                SELECT 
                sum(cantidad) as cantidad
                from(
                    SELECT 
                    count(asca.asignacionId) as cantidad
                    FROM asignacion_ser_cliente_a as asca
                    inner join servicio_evento as serev on serev.id=asca.tservicio
                    WHERE asca.status=2 and asca.fecha BETWEEN '$fechaini' and '$fechafi' 
                    union
                    SELECT 
                    COUNT( DISTINCT ascaa.asignacionId) as cantidad
                    FROM asignacion_ser_contrato_a_e as asca
                    inner join asignacion_ser_contrato_a as ascaa on ascaa.asignacionId=asca.asignacionId
                    inner join servicio_evento as serev on serev.id=ascaa.tservicio
                    WHERE  asca.status=2 and asca.fecha BETWEEN '$fechaini' and '$fechafi' 
                    union
                    SELECT 
                    COUNT( DISTINCT ascaa.asignacionId) as cantidad
                    FROM asignacion_ser_poliza_a_e as asca 
                    inner join asignacion_ser_poliza_a as ascaa on ascaa.asignacionId=asca.asignacionId
                    inner join servicio_evento as serev on serev.id=ascaa.tservicio
                    WHERE  asca.status=2 and asca.fecha BETWEEN '$fechaini' and '$fechafi' 
                    )as datos

                ";
        $query = $this->DB4->query($strq);
        $cantidad=0;
        foreach ($query->result() as $item) {
            $cantidad=$item->cantidad;
        }
        if ($cantidad>0) {
            $cantidad=$cantidad;
        }else{
            $cantidad=0;
        }

        return $cantidad;
    }
    function reporteservicioscountcancelados($fechaini,$fechafi){
        $strq = "
                SELECT 
                sum(cantidad) as cantidad
                from(
                    SELECT 
                    count(asca.asignacionId) as cantidad
                    FROM asignacion_ser_cliente_a as asca
                    inner join servicio_evento as serev on serev.id=asca.tservicio
                    WHERE asca.status=3 and asca.fecha BETWEEN '$fechaini' and '$fechafi' 
                    union
                    SELECT 
                    COUNT( DISTINCT ascaa.asignacionId) as cantidad
                    FROM asignacion_ser_contrato_a_e as asca
                    inner join asignacion_ser_contrato_a as ascaa on ascaa.asignacionId=asca.asignacionId
                    inner join servicio_evento as serev on serev.id=ascaa.tservicio
                    WHERE  asca.status=3 and asca.fecha BETWEEN '$fechaini' and '$fechafi' 
                    union
                    SELECT 
                    COUNT( DISTINCT ascaa.asignacionId) as cantidad
                    FROM asignacion_ser_poliza_a_e as asca 
                    inner join asignacion_ser_poliza_a as ascaa on ascaa.asignacionId=asca.asignacionId
                    inner join servicio_evento as serev on serev.id=ascaa.tservicio
                    WHERE  asca.status=3 and asca.fecha BETWEEN '$fechaini' and '$fechafi' 
                    )as datos

                ";
        $query = $this->DB4->query($strq);
        $cantidad=0;
        foreach ($query->result() as $item) {
            $cantidad=$item->cantidad;
        }
        if ($cantidad>0) {
            $cantidad=$cantidad;
        }else{
            $cantidad=0;
        }

        return $cantidad;
    }
    function reporteservicioscountcotizaciones($fechaini,$fechafi){
        $strq = "
                SELECT 
                sum(cantidad) as cantidad
                from(
                    SELECT 
                    count(asca.asignacionId) as cantidad
                    FROM asignacion_ser_cliente_a as asca
                    inner join servicio_evento as serev on serev.id=asca.tservicio
                    WHERE asca.cot_status=2 and asca.fecha BETWEEN '$fechaini' and '$fechafi' 
                    union
                    SELECT 
                    COUNT( DISTINCT ascaa.asignacionId) as cantidad
                    FROM asignacion_ser_contrato_a_e as asca
                    inner join asignacion_ser_contrato_a as ascaa on ascaa.asignacionId=asca.asignacionId
                    inner join servicio_evento as serev on serev.id=ascaa.tservicio
                    WHERE  asca.cot_status=2 and asca.fecha BETWEEN '$fechaini' and '$fechafi' 
                    union
                    SELECT 
                    COUNT( DISTINCT ascaa.asignacionId) as cantidad
                    FROM asignacion_ser_poliza_a_e as asca 
                    inner join asignacion_ser_poliza_a as ascaa on ascaa.asignacionId=asca.asignacionId
                    inner join servicio_evento as serev on serev.id=ascaa.tservicio
                    WHERE  asca.cot_status=2 and asca.fecha BETWEEN '$fechaini' and '$fechafi' 
                    )as datos

                ";
        $query = $this->DB4->query($strq);
        $cantidad=0;
        foreach ($query->result() as $item) {
            $cantidad=$item->cantidad;
        }
        if ($cantidad>0) {
            $cantidad=$cantidad;
        }else{
            $cantidad=0;
        }

        return $cantidad;
    }
    function reporteservicioscountg($fechaini,$fechafi,$tipo,$servicio){
        if ($servicio==1) {
            $search='preventivo';
            //$search='333';
        }
        if ($servicio==2) {
            $search='correctivo';
        }
        if ($servicio==3) {
            $search='asesoria';
        }
        if ($servicio==4) {
            $search='garantia';
        }

        if ($tipo==1) {
            $strq="SELECT 
                    COUNT( DISTINCT ascaa.asignacionId) as cantidad
                    FROM asignacion_ser_contrato_a_e as asca
                    inner join asignacion_ser_contrato_a as ascaa on ascaa.asignacionId=asca.asignacionId
                    inner join servicio_evento as serev on serev.id=ascaa.tservicio
                    WHERE serev.nombre like '%$search%' and asca.cot_status=2 and asca.fecha BETWEEN '$fechaini' and '$fechafi' ";
        }
        if ($tipo==2) {
            $strq="SELECT 
                    COUNT( DISTINCT ascaa.asignacionId) as cantidad
                    FROM asignacion_ser_poliza_a_e as asca 
                    inner join asignacion_ser_poliza_a as ascaa on ascaa.asignacionId=asca.asignacionId
                    inner join servicio_evento as serev on serev.id=ascaa.tservicio
                    WHERE serev.nombre like '%$search%' and asca.fecha BETWEEN '$fechaini' and '$fechafi' ";
        }
        if ($tipo==3) {
            $strq="SELECT 
                    count(asca.asignacionId) as cantidad
                    FROM asignacion_ser_cliente_a as asca
                    inner join servicio_evento as serev on serev.id=asca.tservicio
                    WHERE serev.nombre like '%$search%' and asca.fecha BETWEEN '$fechaini' and '$fechafi' ";
        }


       
        $query = $this->DB4->query($strq);
        $cantidad=0;
        foreach ($query->result() as $item) {
            $cantidad=$item->cantidad;
        }
        if ($cantidad>0) {
            $cantidad=$cantidad;
        }else{
            $cantidad=0;
        }

        return $cantidad;
    }
    function reportetecnicosegeneral($fechaini,$fechafi){
        $strq = "
                SELECT 
                COUNT(*) as setotal,
                SEC_TO_TIME(SUM(TIME_TO_SEC(datos.hora))) as hinicia,
                SEC_TO_TIME(SUM(TIME_TO_SEC(datos.horafin))) as hfin,
                SEC_TO_TIME(
                            (SUM(TIME_TO_SEC(datos.horafin))-SUM(TIME_TO_SEC(datos.hora)))
                            ) as tiempohabil,
                SEC_TO_TIME(
                            SUM(TIME_TO_SEC(datos.tiempolaboral))-(
                                                                        SUM(TIME_TO_SEC(datos.horafin))-
                                                                        SUM(
                                                                            TIME_TO_SEC(datos.hora)
                                                                            )
                                                                    )
                            )
                         as tiempomuerto,
                sum(datos.cot_status) as cot_status
                from(
                    SELECT 
                    asca.hora,
                    asca.horafin,
                    asca.tecnico,
                    '09:30:00' as tiempolaboral,
                    IF(asca.cot_status = 2, 1, 0) as cot_status
                    FROM asignacion_ser_cliente_a as asca 
                    WHERE  asca.status=2 and asca.fecha BETWEEN '$fechaini' and '$fechafi' 
                    union
                    SELECT 
                    asca.hora,
                    asca.horafin,
                    asca.tecnico,
                    '09:30:00' as tiempolaboral,
                    IF(asca.cot_status = 2, 1, 0) as cot_status
                    FROM asignacion_ser_contrato_a_e as asca 
                    WHERE asca.status=2 and asca.fecha BETWEEN '$fechaini' and '$fechafi' 
                    union
                    SELECT 
                    asca.hora,
                    asca.horafin,
                    asca.tecnico,
                    '09:30:00' as tiempolaboral,
                    IF(asca.cot_status = 2, 1, 0) as cot_status
                    FROM asignacion_ser_poliza_a_e as asca 
                    WHERE  asca.status=2 and asca.fecha BETWEEN '$fechaini' and '$fechafi' 
                    )as datos

                ";
        $query = $this->DB4->query($strq);

        return $query;
    }
    function getfoliosservicios($servicio,$serieId){
        if($serieId==0){
            $whereserie="";
        }else{
            $whereserie=" cf.serieId=$serieId and ";
        }
        if ($servicio==0) {
            $serviciowhere =" (cf.serviciocId=0 or cf.serviciocId is null) and ".$whereserie;
        }else{
            $serviciowhere =" cf.serviciocId=$servicio and ".$whereserie;
        }
        $sql = "SELECT cf.*,c.modelo,c.parte,p.nombre,r.reg,cli.empresa,bod.bodega
                FROM contrato_folio AS cf
                INNER JOIN consumibles AS c ON c.id = cf.idconsumible
                INNER JOIN rentas AS r ON r.id = cf.idrenta
                INNER JOIN personal AS p ON p.personalId = r.id_personal
                INNER JOIN clientes AS cli ON cli.id = r.idCliente
                LEFT join bodegas as bod on bod.bodegaId=cf.bodegaId
                where $serviciowhere cf.status<=1 and cf.serviciocId_tipo=0
                ";
                //log_message('error', 'view sql getfoliosservicios: '.$sql);
        $query = $this->DB4->query($sql);
        return $query;
    }
    function getfoliosservicios2($servicio){
        
        $sql = "SELECT 
                    cf.*,
                    c.modelo,
                    c.parte 
                FROM contrato_folio AS cf 
                INNER JOIN consumibles AS c ON c.id = cf.idconsumible 
                where cf.serviciocId=$servicio and 
                cf.status<=1 and 
                cf.serviciocId_tipo=1
                ";
                //log_message('error', 'view sql: '.$sql);
        $query = $this->DB4->query($sql);
        return $query;
    }
    function getfoliosserviciosserie($servicio,$serie){
        if ($servicio==0) {
            $serviciowhere =" (cf.serviciocId=0 or cf.serviciocId is null) and";
        }else{
            $serviciowhere =" cf.serviciocId=$servicio and ";
        }
        $sql = "SELECT cf.*,c.modelo,c.parte,p.nombre,r.reg,cli.empresa 
                FROM contrato_folio AS cf
                INNER JOIN consumibles AS c ON c.id = cf.idconsumible
                INNER JOIN rentas AS r ON r.id = cf.idrenta
                INNER JOIN personal AS p ON p.personalId = r.id_personal
                INNER JOIN clientes AS cli ON cli.id = r.idCliente
                where $serviciowhere cf.status=0 and cf.serieId=$serie
                ";
        $query = $this->DB4->query($sql);
        return $query;
    }
    function getobtenerservicios($fechaactual,$serie,$tipo,$tipoview){
        $fechainicio1='';
        $fechainicio2='';
        $fechainicio3='';
        $fechainicio4='';
        if($tipo==1){
            $fechacondicion='>=';
        }else{
            $fechacondicion='<';
            if($tipoview==1){
                $fechaactualmenos4 = date("Y-m-d",strtotime($fechaactual."- 4 month"));
                $fechainicio1=" ace.fecha >='$fechaactualmenos4' and ";
                $fechainicio2=" ape.fecha >='$fechaactualmenos4' and ";
                $fechainicio3=" acl.fecha >='$fechaactualmenos4' and ";
                $fechainicio4=" acl.fecha >='$fechaactualmenos4' and ";
            }
        }
        $sql='';
        $sql.="SELECT 
                    `ace`.`asignacionIde`, 
                    `per`.`personalId`, 
                    CONCAT(per.nombre, ' ', `per`.`apellido_paterno`, ' ', per.apellido_materno) as tecnico, 
                    `cli`.`empresa`, 
                    `ace`.`fecha`, 
                    `ace`.`hora`, 
                    `ac`.`zonaId`, 
                    `rde`.`modelo`, 
                    `serp`.`serie`, 
                    `cli`.`id`, 
                    `ac`.`asignacionId`, 
                    `con`.`folio`,
                    1 as tiposervicio
                    FROM `asignacion_ser_contrato_a_e` `ace`
                    JOIN `asignacion_ser_contrato_a` `ac` ON `ac`.`asignacionId`=`ace`.`asignacionId`
                    LEFT JOIN `personal` `per` ON `per`.`personalId`=`ace`.`tecnico`
                    JOIN `contrato` `con` ON `con`.`idcontrato`=`ac`.`contratoId`
                    JOIN `rentas` `ren` ON `ren`.`id`=`con`.`idRenta`
                    JOIN `clientes` `cli` ON `cli`.`id`=`ren`.`idCliente`
                    JOIN `rutas` `ru` ON `ru`.`id`=`ac`.`zonaId`
                    JOIN `rentas_has_detallesEquipos` `rde` ON `rde`.`id`=`ace`.`idequipo`
                    JOIN `series_productos` `serp` ON `serp`.`serieId`=`ace`.`serieId`
                    LEFT JOIN `polizas` `pol` ON `pol`.`Id`=`ac`.`tpoliza`
                    LEFT JOIN `servicio_evento` `sereve` ON `sereve`.`id`=`ac`.`tservicio`
                    WHERE $fechainicio1 `ace`.`fecha` $fechacondicion '$fechaactual'
                    AND `ac`.`activo` = 1
                    AND `ace`.`activo` = 1
                    AND `serp`.`serie` = '$serie' ";
        $sql.=" UNION ";

        $sql.="SELECT 
                    `ape`.`asignacionIde`, 
                    `per`.`personalId`, 
                    CONCAT(per.nombre, ' ', `per`.`apellido_paterno`, ' ', per.apellido_materno) as tecnico, 
                    `cli`.`empresa`, 
                    `ape`.`fecha`, 
                    `ape`.`hora`, 
                    `ap`.`zonaId`, 
                    `equ`.`modelo`, 
                    `ape`.`serie`, 
                    `cli`.`id`,
                    `ap`.`asignacionId`, 
                    '' as folio,
                    2 as tiposervicio
                FROM `asignacion_ser_poliza_a_e` `ape`
                JOIN `asignacion_ser_poliza_a` `ap` ON `ap`.`asignacionId`=`ape`.`asignacionId`
                LEFT JOIN `personal` `per` ON `per`.`personalId`=`ape`.`tecnico`
                JOIN `polizasCreadas` `poc` ON `poc`.`id`=`ap`.`polizaId`
                JOIN `clientes` `cli` ON `cli`.`id`=`poc`.`idCliente`
                JOIN `rutas` `ru` ON `ru`.`id`=`ap`.`zonaId`
                JOIN `polizasCreadas_has_detallesPoliza` `pocde` ON `pocde`.`id`=`ape`.`idequipo`
                JOIN `equipos` `equ` ON `equ`.`id`=`pocde`.`idEquipo`
                LEFT JOIN `servicio_evento` `sereve` ON `sereve`.`id`=`ap`.`tservicio`
                WHERE $fechainicio2 ape.fecha $fechacondicion '$fechaactual'
                AND `ap`.`activo` = 1
                AND `ape`.`activo` = 1
                AND `ape`.`serie` = '$serie' ";
                $sql.=" UNION ";
        $sql.=" SELECT 
                    acld.asignacionIdd as asignacionIde,
                    `per`.`personalId`, 
                    CONCAT(per.nombre, ' ', `per`.`apellido_paterno`, ' ', per.apellido_materno) as tecnico,
                    `cli`.`empresa`,
                    `acl`.`fecha`,
                    `acl`.`hora`, 
                    `acl`.`zonaId`,
                    pol.nombre as modelo,
                    `acld`.`serie`,
                    `cli`.`id`,
                    `acl`.`asignacionId`,
                    '' as folio,
                    3 as tiposervicio
                FROM `asignacion_ser_cliente_a` `acl`
                JOIN `personal` `per` ON `per`.`personalId`=`acl`.`tecnico`
                JOIN `clientes` `cli` ON `cli`.`id`=`acl`.`clienteId`
                JOIN `rutas` `zo` ON `zo`.`id`=`acl`.`zonaId`
                JOIN `asignacion_ser_cliente_a_d` `acld` ON `acld`.`asignacionId`=`acl`.`asignacionId`
                JOIN `polizas` `pol` ON `pol`.`id`=`acld`.`tpoliza`
                LEFT JOIN `servicio_evento` `sereve` ON `sereve`.`id`=`acld`.`tservicio`
                LEFT JOIN `polizas_detalles` `pold` ON `pold`.`id`=`acld`.`tservicio_a`
                WHERE $fechainicio3 acl.fecha $fechacondicion '$fechaactual'
                AND `acl`.`activo` = 1
                AND `acld`.`activo` = '1'
                AND `acld`.`serie`='$serie' ";
        $sql.=" UNION ";
        $sql.="
                SELECT 
                    asignacionIde,personalId,tecnico,empresa,fecha,hora,zonaId,'' as modelo,concat(seriea,serier,seriee) as serie,id,asignacionId,folio,tiposervicio FROM(SELECT 
                acl.asignacionId as asignacionIde,
                per.personalId,
                CONCAT(per.nombre, ' ', per.apellido_paterno, ' ', per.apellido_materno) as tecnico,
                cli.empresa,
                acl.fecha,
                acl.hora,
                acl.zonaId,
                IFNULL(GROUP_CONCAT(sacc.serie),'') as seriea,
                IFNULL(GROUP_CONCAT(sref.serie),'') as serier,
                IFNULL(GROUP_CONCAT(spro.serie),'') as seriee,
                cli.id,
                '' folio,
                4 as tiposervicio,
                acl.asignacionId
            FROM asignacion_ser_venta_a as acl
            INNER JOIN personal as per on per.personalId=acl.tecnico
            INNER JOIN clientes as cli on cli.id=acl.clienteId
            INNER JOIN rutas as zo on zo.id=acl.zonaId
            LEFT JOIN servicio_evento as sereve on sereve.id=acl.tservicio
            LEFT JOIN ventas_has_detallesEquipos_accesorios as vacc on vacc.id_venta=acl.ventaId
            LEFT JOIN asignacion_series_accesorios as saacc on saacc.id_accesorio=vacc.id_accesoriod
            LEFT JOIN series_accesorios as sacc on sacc.serieId=saacc.serieId

            LEFT JOIN ventas_has_detallesRefacciones as vref on vref.idVentas=acl.ventaId
            LEFT JOIN asignacion_series_refacciones as saref on saref.id_refaccion=vref.id
            LEFT JOIN series_refacciones as sref on sref.serieId=saref.serieId

            LEFT JOIN ventas_has_detallesEquipos as veq on veq.idVenta=acl.ventaId
            LEFT JOIN asignacion_series_equipos as saeq on saeq.id_equipo=veq.id
            LEFT JOIN series_productos as spro on spro.serieId=saeq.serieId
            WHERE acl.tipov=0 and acl.activo=1 and $fechainicio4 acl.fecha $fechacondicion '$fechaactual' and (sacc.serie='$serie' or sref.serie='$serie' or spro.serie='$serie' or vref.serie='$serie')
            )dat";
        /*
                    $sql.=" UNION ";
                $sql.="SELECT `acl`.`asignacionId`, `per`.`personalId`, CONCAT(per.nombre, ' ', `per`.`apellido_paterno`, ' ', per.apellido_materno) as tecnico, `cli`.`id`, `cli`.`empresa`, `acl`.`fecha`, `acl`.`hora`, `acl`.`horafin`, `acl`.`zonaId`, `acl`.`prioridad`, `zo`.`nombre`, `acl`.`status`, `acl`.`tiposervicio`, `sereve`.`nombre` as `servicio`, `acl`.`t_fecha`, `acl`.`horaserinicio`, `acl`.`horaserfin`, `acl`.`horaserinicioext`, `acl`.`horaserfinext`, `acl`.`editado`, `acl`.`hora_comida`, `acl`.`horafin_comida`, `acl`.`activo`, `acl`.`infodelete`, `acl`.`ventaId`, `acl`.`tipov`
            FROM `asignacion_ser_venta_a` `acl`
            JOIN `personal` `per` ON `per`.`personalId`=`acl`.`tecnico`
            JOIN `clientes` `cli` ON `cli`.`id`=`acl`.`clienteId`
            JOIN `rutas` `zo` ON `zo`.`id`=`acl`.`zonaId`
            LEFT JOIN `servicio_evento` `sereve` ON `sereve`.`id`=`acl`.`tservicio`
            WHERE `acl`.`fecha` >= '2022-02-16'
            AND `acl`.`fecha` <= '2022-02-16'
            AND `acl`.`activo` = '1'";
        */
        $query = $this->db->query($sql);
        return $query;
    }
    function getobtenercontratosseries($serie){
        $sql="
            SELECT cont.idcontrato,cli.empresa,cont.estatus,ser.serie,seq.activo
            FROM `asignacion_series_r_equipos` as seq 
            INNER JOIN series_productos as ser on ser.serieId=seq.serieId
            INNER JOIN rentas_has_detallesEquipos as renq on renq.id=seq.id_equipo
            inner join rentas as rent on rent.id=renq.idRenta
            inner join contrato as cont on cont.idRenta=renq.idRenta
            inner join clientes as cli on cli.id=rent.idCliente
            WHERE ser.serie='$serie'";
        $query = $this->db->query($sql);
        return $query;
    }
    function getobtenerventaseries($serie){
        $sql="
            SELECT 
                vent.id,vent.combinada ,cli.empresa,ser.serie,vent.activo
            FROM asignacion_series_equipos as asig 
            INNER JOIN ventas_has_detallesEquipos as veq on veq.id=asig.id_equipo 
            INNER JOIN ventas as vent on vent.id=veq.idVenta 
            INNER JOIN clientes as cli on cli.id=vent.idCliente 
            INNER JOIN series_productos as ser on ser.serieId=asig.serieId 
            WHERE ser.serie='$serie'
            union
            SELECT 
                vent.id,
                vent.combinada,
                cli.empresa,
                ser.serie,
                vent.activo
            FROM asignacion_series_accesorios as asig 
            INNER JOIN ventas_has_detallesEquipos_accesorios as vacc on vacc.id_accesoriod=asig.id_accesorio 
            inner JOIN ventas as vent on vent.id=vacc.id_venta 
            INNER JOIN clientes as cli on cli.id=vent.idCliente 
            INNER JOIN series_accesorios as ser on ser.serieId=asig.serieId 
            WHERE ser.serie='$serie'
            union
            SELECT 
                vent.id,vent.combinada,cli.empresa,ser.serie,vent.activo
            FROM asignacion_series_refacciones as asig 
            INNER JOIN ventas_has_detallesRefacciones as vref on vref.id=asig.id_refaccion 
            INNER JOIN ventas as vent on vent.id=vref.idVentas 
            INNER JOIN clientes as cli on cli.id=vent.idCliente 
            INNER JOIN series_refacciones as ser on ser.serieId=asig.serieId
            WHERE ser.serie='$serie' or vref.serie='$serie'

            ";
        $query = $this->db->query($sql);
        return $query;
    }
    function obtenerseriesconsumibles($idventa){
        $sql="  SELECT * FROM(
                    SELECT sp.serie 
                    FROM ventas_has_detallesConsumibles as vc 
                    INNER JOIN series_productos as sp on sp.serieId=vc.idserie 
                    WHERE vc.idVentas=$idventa
                    
                    union 
                    
                    SELECT vr.serie
                    FROM ventas_has_detallesRefacciones as vr 
                    WHERE vr.idVentas=$idventa
                ) as datos GROUP BY serie
                ";
        $query = $this->db->query($sql);
        return $query;
    }
    function obtenerserviciovinculado($idser){
        $sql="SELECT asp.asignacionId,aspe.fecha FROM polizascreadas_ext_ser as pes
                INNER JOIN asignacion_ser_poliza_a as asp on asp.polizaId=pes.idpoliza
                INNER JOIN asignacion_ser_poliza_a_e as aspe on aspe.asignacionId=asp.asignacionId
                WHERE pes.idser='$idser'
                GROUP BY asp.asignacionId ";
        $query = $this->db->query($sql);
        $ser='';
        foreach ($query->result() as $item) {
            $ser.='Servicio Poliza: <b>'.$item->asignacionId.'</b> / '.$item->fecha.'<br>';
        }

        return $ser;
    }
    function obtenerproductospoliza($idpoliza){
        $sql="SELECT vdc.modelo,'' as serie FROM ventacombinada as vc 
                INNER JOIN ventas_has_detallesConsumibles as vdc on vdc.idVentas=vc.consumibles
                WHERE vc.poliza='$idpoliza'
                union
                SELECT con.modelo,'' as serie FROM ventacombinada as vc 
                INNER JOIN ventas_has_detallesequipos_consumible as vdc on vdc.idventa=vc.equipo
                inner join consumibles as con on con.id=vdc.id_consumibles
                WHERE vc.poliza='$idpoliza'

                UNION

                SELECT vde.modelo, sep.serie FROM ventacombinada as vc 
                INNER JOIN ventas_has_detallesEquipos as vde on vde.idVenta=vc.equipo
                LEFT JOIN asignacion_series_equipos as ase on ase.id_equipo=vde.id
                LEFT JOIN series_productos as sep on sep.serieId=ase.serieId
                WHERE vc.poliza='$idpoliza'

                UNION

                SELECT vdr.modelo, ser.serie FROM ventacombinada as vc 
                INNER JOIN ventas_has_detallesRefacciones as vdr on vdr.idVentas=vc.refacciones
                INNER JOIN asignacion_series_refacciones as asr on asr.id_refaccion=vdr.id
                inner join series_refacciones as ser on ser.serieId=asr.serieId
                WHERE vc.poliza='$idpoliza'

                UNION

                SELECT cata.nombre as modelo,seacc.serie FROM ventacombinada as vc 
                INNER JOIN ventas_has_detallesEquipos_accesorios as vdea on vdea.id_venta=vc.equipo
                inner join catalogo_accesorios as cata on cata.id=vdea.id_accesorio
                inner join series_accesorios as seacc on seacc.accesoriosid=cata.id
                WHERE vc.poliza='$idpoliza'";
        $query = $this->db->query($sql);
        return $query;
    }
    function view_estatus_tecnicos($fecha,$tipo){
        if($tipo==0){
            $left_inner=' INNER ';
        }else{
            $left_inner=' LEFT ';
        }
        /*
        $strq="SELECT per.personalId, per.nombre, per.apellido_paterno, per.apellido_materno, usu.perfilId , COUNT(bitsr.id) as counr_ser
                FROM personal as per
                INNER JOIN usuarios as usu on usu.personalId=per.personalId
                $left_inner JOIN bitacora_servicios_retraso as bitsr on bitsr.tecnico=per.personalId AND bitsr.fecha='$fecha' and bitsr.activo=1
                WHERE per.estatus=1 AND (usu.perfilId=5 or usu.perfilId=12 or per.personalId=29 or per.personalId=19 or per.personalId=39) 
                GROUP BY per.personalId";*/
        $strq="";
        $strq.="SELECT tec.personalId, tec.nombre, tec.apellido_paterno, tec.apellido_materno,tec.perfilId,COUNT(bitsr.id) as counr_ser ";
        $strq.=" FROM vista_tecnicos as tec ";
        $strq.=" $left_inner JOIN bitacora_servicios_retraso as bitsr on bitsr.tecnico=tec.personalId AND bitsr.fecha='$fecha' and bitsr.activo=1 ";
        $strq.=" GROUP BY tec.personalId";
        $query = $this->db->query($strq);
        return $query;
    }
    function view_servicios_dev_rentas($fechaini,$fechafin,$tipo){
        $w_fi='';$w_ff='';$cot_ref='';
        if($fechaini!=''){
            $w_fi=" and ace.fecha>='$fechaini' ";
        }
        if($fechafin!=''){
            $w_ff=" and ace.fecha<='$fechafin' ";
        }
        if($tipo==1){
            $cot_ref='ace.cot_referencia';
        }
        if($tipo==2){
            $cot_ref='ace.cot_referencia2';
        }
        if($tipo==3){
            $cot_ref='ace.cot_referencia3';
        }

        $strq="SELECT ace.asignacionId,ace.asignacionIde,pol.nombre as poliza, cli.empresa, ace.fecha, ace.comentario_tec, per.nombre, per.apellido_paterno,v.id,v.combinada,ref.nombre as refaccion,vref.motivo_dev,vref.reg_dev
                FROM asignacion_ser_contrato_a_e as ace
                INNER JOIN asignacion_ser_contrato_a as ac on ac.asignacionId=ace.asignacionId
                INNER JOIN contrato con ON con.idcontrato=ac.contratoId
                INNER JOIN rentas ren ON ren.id=con.idRenta
                INNER JOIN clientes cli ON cli.id=ren.idCliente
                INNER JOIN ventas as v ON v.idCotizacion=$cot_ref
                INNER JOIN ventas_has_detallesRefacciones_dev as vref on vref.idVentas=v.id
                INNER JOIN personal as per on per.personalId=ace.tecnico
                INNER JOIN refacciones as ref on ref.id=vref.idRefaccion
                left JOIN polizas as pol on pol.id=ac.tpoliza

                WHERE $cot_ref>0  $w_fi  $w_ff
                ORDER BY `ace`.`asignacionIde` DESC";
        $query = $this->db->query($strq);
        return $query;
    }
    function view_servicios_dev_polizas($fechaini,$fechafin,$tipo){
        $w_fi='';$w_ff='';$cot_ref='';
        if($fechaini!=''){
            $w_fi=" and ape.fecha>='$fechaini' ";
        }
        if($fechafin!=''){
            $w_ff=" and ape.fecha<='$fechafin' ";
        }
        if($tipo==1){
            $cot_ref='ape.cot_referencia';
        }
        if($tipo==2){
            $cot_ref='ape.cot_referencia2';
        }
        if($tipo==3){
            $cot_ref='ape.cot_referencia3';
        }

        $strq="SELECT ape.asignacionId,ape.asignacionIde,pocde.nombre as poliza,cli.empresa,ape.fecha,ape.comentario_tec,per.nombre, per.apellido_paterno,v.id,v.combinada,ref.nombre as refaccion,vref.motivo_dev,vref.reg_dev
                FROM asignacion_ser_poliza_a_e as ape
                INNER JOIN asignacion_ser_poliza_a as ap ON ap.asignacionId=ape.asignacionId
                INNER JOIN polizasCreadas_has_detallesPoliza as pocde ON pocde.id=ape.idequipo
                INNER JOIN `polizasCreadas` `poc` ON poc.id=ap.polizaId
                INNER JOIN `clientes` `cli` ON cli.id=poc.idCliente
                INNER JOIN personal as per on per.personalId=ape.tecnico
                INNER JOIN ventas as v ON v.idCotizacion=$cot_ref
                INNER JOIN ventas_has_detallesRefacciones_dev as vref on vref.idVentas=v.id
                INNER JOIN refacciones as ref on ref.id=vref.idRefaccion
                WHERE $cot_ref>0  $w_fi $w_ff
                ORDER BY `ape`.`asignacionIde` DESC;";
        $query = $this->db->query($strq);
        return $query;
    }
    function view_servicios_dev_cliente($fechaini,$fechafin,$tipo){
        $w_fi='';$w_ff='';$cot_ref='';
        if($fechaini!=''){
            $w_fi=" and acl.fecha>='$fechaini' ";
        }
        if($fechafin!=''){
            $w_ff=" and acl.fecha<='$fechafin' ";
        }
        if($tipo==1){
            $cot_ref='acld.cot_referencia';
        }
        if($tipo==2){
            $cot_ref='acld.cot_referencia2';
        }
        if($tipo==3){
            $cot_ref='acld.cot_referencia3';
        }

        $strq="SELECT acld.asignacionId,acld.asignacionIdd ,pol.nombre as poliza,cli.empresa,acl.fecha,acld.comentario_tec,per.nombre, per.apellido_paterno,v.id,v.combinada,ref.nombre as refaccion,vref.motivo_dev,vref.reg_dev
                FROM asignacion_ser_cliente_a_d as acld
                INNER JOIN asignacion_ser_cliente_a as acl ON acld.asignacionId = acl.asignacionId
                INNER JOIN polizas as pol on pol.id=acld.tpoliza
                INNER JOIN `clientes` `cli` ON `cli`.`id`=`acl`.`clienteId`
                INNER JOIN personal as per on per.personalId=acl.tecnico
                INNER JOIN ventas as v ON v.idCotizacion=$cot_ref
                INNER JOIN ventas_has_detallesRefacciones_dev as vref on vref.idVentas=v.id
                INNER JOIN refacciones as ref on ref.id=vref.idRefaccion
                WHERE $cot_ref>0  $w_fi $w_ff
                ORDER BY `acld`.`asignacionId` DESC";
        $query = $this->db->query($strq);
        return $query;
    }
    function view_servicios_dev_venta($fechaini,$fechafin,$tipo){
        $w_fi='';$w_ff='';$cot_ref='';
        if($fechaini!=''){
            $w_fi=" and acl.fecha>='$fechaini' ";
        }
        if($fechafin!=''){
            $w_ff=" and acl.fecha<='$fechafin' ";
        }
        if($tipo==1){
            $cot_ref='acl.cot_referencia';
        }
        if($tipo==2){
            $cot_ref='acl.cot_referencia2';
        }
        if($tipo==3){
            $cot_ref='acl.cot_referencia3';
        }

        $strq="SELECT acl.asignacionId,acld.tservicio_a,pol.nombre as poliza,`sereve`.`nombre` as `servicio`,cli.empresa,acl.fecha,acl.comentario_tec,per.nombre, per.apellido_paterno,v.id,v.combinada, ref.nombre as refaccion,vref.motivo_dev,vref.reg_dev
            FROM asignacion_ser_venta_a as acl
            JOIN `asignacion_ser_venta_a_d` `acld` ON `acld`.`asignacionId` = `acl`.`asignacionId`
            JOIN `clientes` `cli` ON `cli`.`id`=`acl`.`clienteId`
            left JOIN `servicio_evento` `sereve` ON `sereve`.`id` = `acld`.`tservicio`
            left JOIN polizas as pol on pol.id=acld.tpoliza
            INNER JOIN personal as per on per.personalId=acl.tecnico
            INNER JOIN ventas as v ON v.idCotizacion=$cot_ref
            INNER JOIN ventas_has_detallesRefacciones_dev as vref on vref.idVentas=v.id
            INNER JOIN refacciones as ref on ref.id=vref.idRefaccion
            WHERE $cot_ref>0  $w_fi $w_ff
            ORDER BY `acl`.`asignacionId` DESC";
        $query = $this->db->query($strq);
        return $query;
    }
    function verificar_servicio_anterior_cot($serieId,$serie){
        if($serieId>0){
            $w_ser=" ser.serieId='$serieId' ";
        }else{
            $w_ser=" ser.serie='$serie' ";
        }
        $strq="
        SELECT dat.*,
                vdr1.id_dev as id_dev1,
                vdr1.piezas as piezas1,
                vdr1.modelo as modelo1,
                vdr1.motivo_dev as motivo_dev1,
                vdr1.reg_dev as reg_dev1,

                vdr2.id_dev as id_dev2,
                vdr2.piezas as piezas2,
                vdr2.modelo as modelo2,
                vdr2.motivo_dev as motivo_dev2,
                vdr2.reg_dev as reg_dev2,

                vdr3.id_dev as id_dev3,
                vdr3.piezas as piezas3,
                vdr3.modelo as modelo3,
                vdr3.motivo_dev as motivo_dev3,
                vdr3.reg_dev as reg_dev3

            from (
            SELECT asigd.asignacionId, 1 as tipo,per.nombre,per.apellido_paterno,asigd.fecha,asigd.serieId,ser.serie, asigd.comentario_tec,asigd.cot_refaccion,asigd.cot_refaccion2,asigd.cot_refaccion3,
            asigd.cot_referencia,asigd.cot_referencia2,asigd.cot_referencia3
            FROM asignacion_ser_contrato_a_e as asigd
            INNER JOIN personal as per on per.personalId=asigd.tecnico
            INNER JOIN series_productos as ser on ser.serieId=asigd.serieId
            WHERE asigd.fecha>=DATE_SUB(NOW(),INTERVAL '4' MONTH) AND 
            (asigd.cot_referencia>0 or asigd.cot_referencia2>0 OR asigd.cot_referencia3>0) and $w_ser

            union

            SELECT asigd.asignacionId, 2 as tipo,per.nombre, per.apellido_paterno,asigd.fecha,ser.serieId, asigd.serie,asigd.comentario_tec,asigd.cot_refaccion,asigd.cot_refaccion2,asigd.cot_refaccion3,
            asigd.cot_referencia,asigd.cot_referencia2,asigd.cot_referencia3
            FROM asignacion_ser_poliza_a_e as asigd
            INNER JOIN personal as per on per.personalId=asigd.tecnico
            LEFT JOIN series_productos as ser on ser.serie=asigd.serie AND ser.activo=1
            WHERE asigd.fecha>=DATE_SUB(NOW(),INTERVAL '4' MONTH) AND
            (asigd.cot_referencia>0 or asigd.cot_referencia2>0 OR asigd.cot_referencia3>0) and $w_ser

            union

            SELECT asigd.asignacionId, 3 as tipo,per.nombre, per.apellido_paterno,asig.fecha, ser.serieId, asigd.serie,asigd.comentario_tec,asigd.cot_refaccion,asigd.cot_refaccion2,asigd.cot_refaccion3,
            asigd.cot_referencia,asigd.cot_referencia2,asigd.cot_referencia3
            FROM asignacion_ser_cliente_a_d as asigd
            INNER JOIN asignacion_ser_cliente_a as asig on asig.asignacionId=asigd.asignacionId
            INNER JOIN personal as per on per.personalId=asig.tecnico
            LEFT JOIN series_productos as ser on ser.serie=asigd.serie AND ser.activo=1
            WHERE asig.fecha>=DATE_SUB(NOW(),INTERVAL '4' MONTH) AND
            (asigd.cot_referencia>0 or asigd.cot_referencia2>0 OR asigd.cot_referencia3>0) and $w_ser

            union

            SELECT asig.asignacionId,4 as tipo,per.nombre,per.apellido_paterno,asig.fecha,ser.serieId, asigd.serie, asig.comentario_tec, asig.cot_refaccion, asig.cot_refaccion2, asig.cot_refaccion3,
            asig.cot_referencia,asig.cot_referencia2,asig.cot_referencia3
            FROM asignacion_ser_venta_a as asig
            INNER JOIN personal as per on per.personalId=asig.tecnico
            INNER JOIN asignacion_ser_venta_a_d as asigd on asigd.asignacionId=asig.asignacionId
            LEFT JOIN series_productos as ser on ser.serie=asigd.serie AND ser.activo=1
            WHERE asig.fecha>=DATE_SUB(NOW(),INTERVAL '4' MONTH) AND 
            (asig.cot_referencia>0 or asig.cot_referencia2>0 OR asig.cot_referencia3>0) and $w_ser

            ORDER BY fecha DESC LIMIT 1
        ) as dat
        left join ventas as v1 on v1.idCotizacion=dat.cot_referencia
        left join ventas_has_detallesRefacciones_dev as vdr1 on vdr1.idVentas=v1.id

        left join ventas as v2 on v2.idCotizacion=dat.cot_referencia2
        left join ventas_has_detallesRefacciones_dev as vdr2 on vdr2.idVentas=v2.id

        left join ventas as v3 on v3.idCotizacion=dat.cot_referencia3
        left join ventas_has_detallesRefacciones_dev as vdr3 on vdr3.idVentas=v3.id
         
            ";
        $query = $this->db->query($strq);
        return $query;
    }
    function filtro_exit_val($array,$rid){

        // Si el array está vacío, el estatus es 1
        if (empty($array)) {
            return 1;
        }

        // Si el array no está vacío, buscar si '117' existe en el array
        if (in_array($rid, $array)) {
            return 1;
        }

        // Si '117' no existe en el array, el estatus es 0
        return 0;
    }
}