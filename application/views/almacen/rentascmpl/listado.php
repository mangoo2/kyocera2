<!-- START CONTENT -->
    <section id="content">
    	<!--start container-->
      	<div class="container">
      		<div class="section">
<div class="card-panel">
      		<input id="base_url" type="hidden" name="base_url" value="<?php echo base_url(); ?>">

      		<div>
                <h4 class="header">Listado Solicitudes de Contrato en Renta</h4>
                <div class="row">
                  <div class="col s12">

                    <table id="tabla_solicitudcontrato" class="responsive-table display" cellspacing="0">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Cliente</th>
                          <th>Atención</th>
                          <th>Teléfono</th>
                          <th>Modelo</th>
                          <th>Área</th>
                          <th>Plazo</th>
                          <td>Estatus</td>
                          <td>Cotización #:</td>
                          <td>IdDetalleRenta</td>
                          <td>IdEquipo</td>
                          <th>Costo por click Mono</th>
                          <th>Excedente</th>
                          <td></td>
                          <td>Pre factura</td>
                          <td>Acciones</td>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            </div>  
               </div>
        </section>