<style type="text/css">
  table td{
    font-size: 12px;
    padding: 6px 7px;
  }
</style>
<!-- START CONTENT -->
    <section id="content">
      <!--start container-->
        <div class="container">
          <div class="section">

          <input id="base_url" type="hidden" name="base_url" value="<?php echo base_url(); ?>">
          <input id="idUsuario" type="hidden" name="perfilid" value="<?php echo $_SESSION["usuarioid"]; ?>">

          <div id="table-datatables">
            <br>
                <h4 class="header">Listado de Prospectos</h4>
                <div class="row">
                  
                  <div class="col s12"><a class="btn waves-effect waves-light gradient-45deg-green-teal gradient-shadow" href="<?php echo base_url(); ?>index.php/Prospectos/alta">Nuevo</a>
                
                    <table id="tabla_prospectos" class="responsive-table display" cellspacing="0">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Empresa</th>
                        <!--  <th>Persona contacto</th> -->
                          <th>Puesto contacto</th>
                          <th>Email</th>
                          <th>Estado</th>
                          <th>Municipio</th>
                          <th>Giro empresa</th>
                          <th>Dirección</th>
                          <th>Observaciones</th>
                          <th>Vendedor</th>
                          <th>Acciones</th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
          </div>  
        </div>

        <div id="modalAcciones" class="modal"  >
          <div class="modal-content ">

            <h4> <i class="material-icons">mode_edit</i> Acciones </h4>

            <div class="col s12 m12 l6">
                  <div class="row" align="center">
                      <ul>
                        <li>
                          <a class="btn-floating green tooltipped edit" data-position="top" data-delay="50" data-tooltip="Editar" id="lista_editar">
                              <i class="material-icons">mode_edit</i>
                          </a>
                          <a class="btn-floating blue tooltipped visualiza" data-position="top" data-delay="50" data-tooltip="Visualizar" id="lista_visualizar">
                              <i class="material-icons">remove_red_eye</i>
                          </a>
                          <a class="btn-floating red tooltipped elimina" data-position="top" data-delay="50" data-tooltip="Eliminar" id="lista_eliminar">
                              <i class="material-icons">delete_forever</i>
                          </a>
                          <a class="btn-floating light-blue darken-4 tooltipped" data-position="top" data-delay="50" data-tooltip="Cotizar" id="lista_cotiza">
                              <i class="material-icons">monetization_on</i>
                          </a>
                          <a class="btn-floating deep-orange darken-4 tooltipped" data-position="top" data-delay="50" data-tooltip="Lista de Cotizaciones" id="lista_cotiza_historico">
                              <i class="material-icons">format_list_numbered</i>
                          </a> 
                          <a class="btn-floating purple tooltipped convertir" data-position="top" data-delay="50" data-tooltip="Convertir" id="lista_convertir">
                              <i class="material-icons">compare_arrows</i>
                          </a>
                          <a class="btn-floating orange tooltipped llamadas disabled" data-position="top" data-delay="50" data-tooltip="Agenda Llamada" id="lista_llamadas">
                              <i class="material-icons">phone_in_talk</i>
                          </a>
                          <a class="btn-floating pink tooltipped correos disabled" data-position="top" data-delay="50" data-tooltip="Agendar Correo" id="lista_correos">
                              <i class="material-icons">email</i>
                          </a>
                          <a class="btn-floating amber tooltipped visitas disabled" data-position="top" data-delay="50" data-tooltip="Agendar Visita" id="lista_visitas">
                              <i class="material-icons">add_alert</i>
                          </a>
                          <a class="btn-floating cyan tooltipped agenda disabled" data-position="top" data-delay="50" data-tooltip="Revisar Agenda" id="lista_agenda">
                              <i class="material-icons">date_range</i>
                          </a>
                          <a class="btn-floating light tooltipped timeline disabled" data-position="top" data-delay="50" data-tooltip="Revisar Timeline" id="lista_timeline">
                              <i class="material-icons">timeline</i>
                          </a>
                        </li>
                      </ul>
                      <input type="hidden" name="idProspectoModal" id="idProspectoModal">
                      <input type="hidden" name="empresa" id="empresa">
                  </div>
              </div>
              
          <div class="modal-footer">
            <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
          </div>
        </div>
     </div>
    </section>
<!--
    <div id="modalCotiza" class="modal"  >
    <div class="modal-content ">
        <div class="modal-header">
            <h4 class="modal-title">Cotizar</h4>
        </div>
         <div class="modal-body">
         <div class="row">
                    <div class="input-field col s6">
                      <i class="material-icons prefix">monetization_on</i>
                        <select id="tipo" name="tipo">
                            <option value="" disabled selected>Selecciona tipo de cotización</option>
                            <option value="1">Venta</option>
                            <option value="2">Renta</option>
                            <option value="3">Póliza</option>
                        </select>
                      <label for="estado">Tipo de Cotización</label>
                    </div>
                    <div class="input-field col s6">
                      <i class="material-icons prefix">location_city</i>
                      <input id="prospecto" name="prospecto"  type="text" value="prospecto" disabled="">
                      <label for="prospecto"></label>
                    </div>
          </div>
          <div class="row">
                    <div class="input-field col s6">
                      <i class="material-icons prefix">monetization_on</i>
                        <select id="cotizar" name="cotizar" onchange="selectcotiza()">
                            <option value="" disabled selected>Selecciona cotización</option>
                            <option value="1">Manual</option>
                            <option value="2">Familia</option>
                        </select>
                      <label for="estado">Cotización</label>
                    </div>
                    <div class="input-field col s6 select_familia" style="display: none;">
                          <i class="material-icons prefix">person</i>
                          <select id="idfamilia" name="idfamilia">
                                   <option value="" disabled selected>Selecciona una opción</option>
                                     <?php foreach($familia as $item)
                                    { 
                                    ?>
                                      <option value="<?php echo $item->id?>"><?php echo $item->nombre; ?></option>
                                    <?php }?>
                          </select>
                          <label for="estado">Familiar</label>
                    </div>       
          </div>
          <input type="hidden" name="idProspecto_Modal" id="idProspecto_Modal">
        </div>
        <div class="modal-footer">
          <a class="btn waves-effect waves-light cyan" id="btn_cotizar">Aceptar</a>
          <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
        </div>
    </div>
</div>                
-->