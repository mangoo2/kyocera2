<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Estadocuenta extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloEstadocuenta');
        $this->load->model('login_model');
        $this->load->model('ModeloGeneral');
        $this->load->model('ConfiguracionDocumentos_Model');
        $this->load->model('Clientes_model');
        $this->load->model('Rentas_model');
        $this->load->helper('url');
        $this->submenu=78;
        if($this->session->userdata('logeado')==true){
            $this->perfilid =$this->session->userdata('perfilid');
            $this->idpersonal = $this->session->userdata('idpersonal');
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,$this->submenu);// 40 es el id del submenu
            if ($permiso==0) {
                redirect('Sistema');
            }
        }else{
            redirect('Sistema');
        }
    }

    function index(){
        $data['MenusubId']=$this->submenu;
        $data['perfirma']=$this->ModeloCatalogos->seleccionejecutivofirma();
        //$data['personalrows']=$this->ModeloCatalogos->getselectwheren('personal',array('estatus'=>1));
        $data['personalrows']=$this->ModeloGeneral->listadopersonal();
        $this->load->view('header');
        $this->load->view('main');
        $this->load->view('estadocuenta/ec_index',$data);
        $this->load->view('estadocuenta/ec_index_js');
        $this->load->view('footer');  
    }
    function generar(){
        $params=$this->input->post();
        $idcliente = $params['idcliente'];
        $tipo = $params['tipo'];
        $eje = $params['eje'];

        $html_renta='';
        $html_ventas='';
        $html_poliza='';
        $html_facturas='';
        //============================================================================
            $array_contratos=array();
            $strq="SELECT con.* FROM rentas as ren INNER JOIN contrato as con on con.idRenta=ren.id WHERE con.estatus>0 AND ren.idCliente='$idcliente' ";
            $query = $this->db->query($strq);
            foreach ($query->result() as $itemc) {
                //log_message('error','idcontrato: '.$itemc->idcontrato);
                $array_contratos[]=array('con'=>$itemc->idcontrato,'fol'=>$itemc->folio);
            }
            foreach ($array_contratos as $itemc) {
                //log_message('error','Contrato: '.$itemc['con']);
                $htmlf='';
                $idcontrato = $itemc['con'];
                $fol = $itemc['fol'];//folio contrato
                //$facturas = $this->ModeloCatalogos->viewprefacturaslis_general2($idcontrato);
                $sqlrentas=$this->ModeloEstadocuenta->sqlrentas($idcontrato,'',$eje,0);
                $facturas = $this->db->query($sqlrentas);
                //==========================================================================
                
                $montovencido=0;
                $prefIdg = 0;
                $row=0;
                foreach ($facturas->result() as $item) {
                    //log_message('error','Periodo: '.$item->prefId);
                    if($item->FacturasId>0){
                        $total=round($item->total,2);

                        //=============================================
                            $sql_nt="SELECT * FROM f_facturas as fac WHERE f_r_uuid LIKE'%$item->uuid%' AND Estado=1 and f_r_tipo='01'";
                            $query_nt = $this->db->query($sql_nt);
                            //$pagosx=0;
                            foreach ($query_nt->result() as $itemnt) {
                                $total=$total-$itemnt->total;
                            } 
                        //=============================================
                        
                    }else{
                        $total=round($item->total_periodo,2);
                        //log_message('error','total: '.$item->total_periodo);
                    }
                    if($item->FacturasId>0){
                        $descripcion=$this->ModeloCatalogos->descripcionfactura($item->FacturasId);
                    }else{
                        $descripcion='';
                    }
                    
                    

                    
                    if ($item->fechamax==null) {
                        $fechapago='Pendiente';
                    }else{
                        $fechapago=$item->fechamax;
                    }
                    if( floatval($item->pagot)>0){//if($item->prefId==10887){log_message('error','prefId:'.$item->prefId.' pagot:'.$item->pagot);}
                        $pagot='$ '.number_format($item->pagot,2,'.',',');
                        $pagot_num=round($item->pagot,2);
                        //if($item->prefId==10887){log_message('error','prefId:'.$item->prefId.' pagot:'.$pagot_num.' round');}
                    }else{
                        $pagot='$ 0.00';
                        
                        $pagot_num=0;
                    }
                    $sqlpago="SELECT * FROM `pagos_factura` WHERE factura='$item->FacturasId'";
                    $query_pf = $this->db->query($sqlpago);
                    //$pagosx=0;
                    foreach ($query_pf->result() as $itempf) {
                        $pagot_num=$pagot_num+$itempf->pago;
                        //if($item->prefId==10887){log_message('error','prefId:'.$item->prefId.' pagot:'.$itempf->pago.' itempf');}
                    }  
                    $totalf=$total-$pagot_num;

                    if($totalf>0){
                        $infocomplemento=$this->ModeloCatalogos->infocomplementofactura($item->FacturasId);
                        $pagot_num+=floatval($infocomplemento[0]);
                        //if($item->prefId==10887){log_message('error','prefId:'.$item->prefId.' pagot:'.$pagot_num.' complemento');}
                        if($infocomplemento[0]>0){
                            $fechapago=date('Y-m-d',strtotime($infocomplemento[1]));
                            $pagot='$ '.number_format($pagot_num,2,'.',',');
                            $totalf=floatval($total)-floatval($pagot_num);
                            //log_message('error', '(1) prefId('.$item->prefId.') $totalf('.$totalf.')=$total('.$total.')-$pagot_num '.$pagot_num);
                        }

                          
                    }
                    if($item->info_factura_b!=null and $item->info_factura_b!=''){
                        $descripcion=$item->info_factura_b;
                    }
                    
                    //$montovencido=$montovencido+$totalf;
                    
                    
                    /*$htmlf .= '<tr nobr="true"><td'.$td1.'>U'.$item->Folio.'</td><td'.$td2.'>'.$item->fechatimbre.'</td><td'.$td3.'>'.$descripcion.'</td><td'.$td4.'>'.$fechapago.'</td><td'.$td5.'>$ '.number_format($item->total,2,'.',',').'</td><td'.$td6.'>'.$pagot.'</td><td'.$td7.'>$ '.number_format($totalf,2,'.',',').'</td></tr>';*/
                    
                    if($item->FacturasId>0){
                        $btn_fac='<a class="b-btn b-btn-primary" href="'.base_url().'Configuracionrentas/generarfacturas/'.$item->FacturasId.'/1" target="_blank"><i class="fas fa-file-alt"></i></a>';
                        $btn_fac_edit='<a class="b-btn b-btn-primary" onclick="edit_factura_prefactura('.$item->fp_facId.',3)"><i class="fas fa-pencil-alt"></i></a>';
                    }else{
                        $btn_fac='';
                        $btn_fac_edit='';
                    }
                    $infopagos=' total:'.$total.'<br>pagos:'.$pagot.'<br>$totalf: '.$totalf;
                    $infopagos='';
                    if($item->Estado=='0'){
                        $s_estado='<br>Cancelado';
                    }else{
                        $s_estado='';
                    }
                    if($item->Folio>0){
                        $label_folio='U'.$item->Folio;
                    }else{
                        $label_folio='';
                    }
                    $html_renta_tr='<tr>';
                        $html_renta_tr.='<td>';
                            $html_renta_tr.='<input type="checkbox" class="filled-in facturas_r_checked" id="'.$row.'_factura_'.$item->prefId.'" value="'.$item->prefId.'" data-value="'.$item->prefId.'" data-factu="'.$item->FacturasId.'" data-clienteid="'.$idcliente.'">';
                            $html_renta_tr.='<label for="'.$row.'_factura_'.$item->prefId.'"></label>';
                        $html_renta_tr.='</td><td>'.$item->prefId.'</td><td>'.$fol.'</td><td>'.$label_folio.' '.$s_estado.'</td><td>'.$item->periodo_inicial.'</td><td>'.$item->periodo_final.'</td><td>$ '.number_format($total,2,'.',',').'</td><td>'.$pagot.'</td><td>$ '.number_format($totalf,2,'.',',').'</td><td>'.$item->fechatimbre.'</td><td>'.$btn_fac.'</td><td class="des_'.$item->fp_facId.'_3">'.$descripcion.'</td><td>'.$btn_fac_edit.$infopagos.'</td><tr>';
                    if($tipo==0){
                        if($totalf>0){//log_message('error', '(1) prefId('.$item->prefId.') $totalf'.$totalf);
                            $html_renta.=$html_renta_tr;
                        }
                    }else{//log_message('error', '(2) prefId('.$item->prefId.') $totalf'.$totalf);
                        $html_renta.=$html_renta_tr;
                    }
                    $row++;
                }
            }
        //============================================================================
            /*
            $strq_tv="SELECT v.id,v.reg,v.combinada,v.total_general,fac.serie,fac.Folio,fac.FacturasId, fac.FormaPago , IFNULL(SUM(pagv.pago),0) as pago_manual,IFNULL(SUM(fcompd.ImpPagado),0) as ImpPagado,v.info_factura
                    FROM ventas as v LEFT JOIN factura_venta as facv on facv.ventaId=v.id AND facv.combinada=0 
                    LEFT JOIN f_facturas as fac on fac.FacturasId=facv.facturaId AND fac.Estado=1 
                    LEFT JOIN pagos_ventas as pagv on pagv.idventa=v.id
                    LEFT JOIN f_complementopago_documento as fcompd on fcompd.facturasId=fac.FacturasId
                    LEFT JOIN f_complementopago as fcomp on fcomp.complementoId=fcompd.complementoId AND fcomp.Estado=1
                    WHERE v.activo=1 AND v.combinada=0 AND v.total_general>0 AND v.idCliente='$idcliente' 
                    GROUP BY v.id";
                    */
            $strq_tv=$this->ModeloEstadocuenta->sqlventas($idcliente,'',$eje);
            $query_tv = $this->db->query($strq_tv);

            foreach ($query_tv->result() as $item) {
                //$pagado=$item->pago_manual+$item->ImpPagado;
                $pagado=0;
                //===========================================
                    $resultfactura=$this->ModeloCatalogos->ontenerdatosfacturaventa($item->id,0);
                    $result = $this->ModeloCatalogos->pagos_ventas($item->id);
                    foreach ($result as $item1) {   
                       $pagado=$pagado+$item1->pago; 
                    }
                    //esta esta mas conpleto si en dado caso falta en ventacombinada o en poliza
                    foreach($resultfactura->result() as $item2){
                        $FacturasIdselected=$item2->FacturasId;
                        $resultvcp = $this->ModeloCatalogos->obtencionventapolizafactura($item2->FacturasId);

                        foreach ($resultvcp->result() as $itemvcp) {
                            if($itemvcp->combinada==0){
                                //=======================================
                                    $result = $this->ModeloCatalogos->pagos_ventas($itemvcp->ventaId); 
                                        foreach ($result as $item3) { 
                                            $pagado=$pagado+$item3->pago; 
                                        } 
                                //=======================================
                            }elseif ($itemvcp->combinada==1) {
                                //========================================
                                    $result = $this->ModeloCatalogos->pagos_combinada($itemvcp->ventaId); 
                                    foreach ($result as $item4) { 
                                        $pagado=$pagado+$item4->pago; 
                                    } 
                                //========================================
                            }elseif ($itemvcp->combinada==2) {
                                //========================================
                                    $result = $this->ModeloCatalogos->pagos_poliza($itemvcp->ventaId);
                                    foreach ($result as $item5) {
                                        $pagado=$pagado+$item5->pago;
                                    }
                                //========================================
                            }
                        }
                        $resultvcp_cp = $this->ModeloCatalogos->obteciondecomplememtofactura($FacturasIdselected);
                        foreach ($resultvcp_cp->result() as $itemcp) {
                            $pagado=$pagado+$itemcp->ImpPagado; 
                        }
                    }
                //===========================================
                $saldo=$item->total_general-$pagado;
                if($item->FacturasId>0){
                    $descripcion=$this->ModeloCatalogos->descripcionfactura($item->FacturasId);
                }else{
                    $descripcion='';
                }
                if($item->FacturasId>0){
                        $btn_fac='<a class="b-btn b-btn-primary" href="'.base_url().'Configuracionrentas/generarfacturas/'.$item->FacturasId.'/1" target="_blank"><i class="fas fa-file-alt"></i></a>';
                        $btn_fac_edit='<a class="b-btn b-btn-primary" onclick="edit_factura_prefactura('.$item->id.',0)"><i class="fas fa-pencil-alt"></i></a>';
                }else{
                    $btn_fac='';
                    $btn_fac_edit='';
                }
                if($item->info_factura!=null and $item->info_factura!=''){
                    $descripcion=$item->info_factura;
                }
                $html_ventas_tr='<tr>';
                    $html_ventas_tr.='<td><input type="checkbox" class="filled-in facturas_v_checked" id="facturav_0_'.$item->id.'" value="'.$item->id.'" data-clienteid="'.$idcliente.'" data-tipovc="0"><label for="facturav_0_'.$item->id.'"></label></td>';
                    $html_ventas_tr.='<td>'.$item->id.'</td>';
                    $html_ventas_tr.='<td>'.$item->reg.'</td>';
                    $html_ventas_tr.='<td>$ '.number_format($item->total_general,2,'.',',').'</td>';
                    $html_ventas_tr.='<td>$ '.number_format($pagado,2,'.',',').'</td>';
                    $html_ventas_tr.='<td>$ '.number_format($saldo,2,'.',',').'</td>';
                    $html_ventas_tr.='<td>'.$item->serie.' '.$item->Folio.'</td>';
                    $html_ventas_tr.='<td>'.$btn_fac.'</td>';
                    $html_ventas_tr.='<td class="des_'.$item->id.'_0">'.$descripcion.'</td>';
                    $html_ventas_tr.='<td>'.$btn_fac_edit.'</td>';
                $html_ventas_tr.='</tr>';
                if($tipo==0){
                    if($saldo>0){
                        $html_ventas.=$html_ventas_tr;
                    }
                }else{
                    $html_ventas.=$html_ventas_tr;
                }
            }

        //============================================================================
            /*
            $strq_tvc="SELECT vc.combinadaId,vc.equipo,vc.reg,vc.total_general,vc.info_factura,fac.FacturasId,fac.serie,fac.Folio,fac.fechatimbre,fac.FormaPago, IFNULL(SUM(pagc.pago),0) as pago_manual,IFNULL(SUM(fcompd.ImpPagado),0) as ImpPagado 
                        FROM ventacombinada as vc 
                        LEFT JOIN factura_venta as facv on facv.ventaId=vc.combinadaId AND facv.combinada=1 
                        LEFT JOIN f_facturas as fac on fac.FacturasId=facv.facturaId AND fac.Estado=1 
                        LEFT JOIN pagos_combinada as pagc on pagc.idventa=vc.combinadaId 
                        LEFT JOIN f_complementopago_documento as fcompd on fcompd.facturasId=fac.FacturasId 
                        LEFT JOIN f_complementopago as fcomp on fcomp.complementoId=fcompd.complementoId AND fcomp.Estado=1 
                        WHERE vc.idCliente='$idcliente' AND vc.activo=1 AND vc.total_general>0 
                        GROUP BY vc.combinadaId ORDER BY vc.combinadaId DESC";
                        */
                $strq_tvc=$this->ModeloEstadocuenta->sqlventascom($idcliente,'',$eje);
                $query_tvc = $this->db->query($strq_tvc);
            foreach ($query_tvc->result() as $item) {
                //==========================================================
                    $pagop=0;
                    $resultfactura=$this->ModeloCatalogos->ontenerdatosfacturaventa($item->combinadaId,1);
                    foreach($resultfactura->result() as $item_rf){
                        $FacturasIdselected=$item_rf->FacturasId;
                        $resultvcp = $this->ModeloCatalogos->obtencionventapolizafactura($FacturasIdselected);
                        foreach ($resultvcp->result() as $itemvcp) {
                            if($itemvcp->combinada==0){

                                        
                                //=======================================
                                     $result_pp0 = $this->ModeloCatalogos->pagos_ventas($itemvcp->ventaId); 
                                       foreach ($result_pp0 as $item_pp0) { 
                                            $pagop=$pagop+$item_pp0->pago; 
                                        } 
                                //=======================================
                            }
                            if ($itemvcp->combinada==1) {
                            //========================================
                                $result = $this->ModeloCatalogos->pagos_combinada($itemvcp->ventaId); 
                                foreach ($result as $item_pc) { 
                                    $pagop=$pagop+$item_pc->pago; 
                                } 
                            //========================================
                        }
                            if ($itemvcp->combinada==2) {
                                    
                                //========================================
                                    $result_pp = $this->ModeloCatalogos->pagos_poliza($itemvcp->ventaId);
                                    foreach ($result_pp as $item_pp) {
                                        $pagop=$pagop+$item_pp->pago;
                                    }
                                //========================================
                            }
                        }
                    }
                //===================================================
                $pagado=$item->pago_manual+$item->ImpPagado+$pagop;
                $saldo=$item->total_general-$pagado;
                if($item->FacturasId>0){
                        $descripcion=$this->ModeloCatalogos->descripcionfactura($item->FacturasId);
                    }else{
                        $descripcion='';
                    }
                if($item->FacturasId>0){
                        $btn_fac='<a class="b-btn b-btn-primary" href="'.base_url().'Configuracionrentas/generarfacturas/'.$item->FacturasId.'/1" target="_blank"><i class="fas fa-file-alt"></i></a>';
                        $btn_fac_edit='<a class="b-btn b-btn-primary" onclick="edit_factura_prefactura('.$item->combinadaId.',1)"><i class="fas fa-pencil-alt"></i></a>';
                }else{
                    $btn_fac='';
                    $btn_fac_edit='';
                }
                if($item->info_factura!=null and $item->info_factura!=''){
                    $descripcion=$item->info_factura;
                }
                $html_ventas_tr='<tr>';
                    $html_ventas_tr.='<td><input type="checkbox" class="filled-in facturas_vc_checked" id="facturav_1_'.$item->combinadaId.'" value="'.$item->combinadaId.'" data-clienteid="'.$idcliente.'" data-tipovc="1"><label for="facturav_1_'.$item->combinadaId.'"></label></td>';
                    $html_ventas_tr.='<td>'.$item->equipo.'</td>';
                    $html_ventas_tr.='<td>'.$item->reg.'</td>';
                    $html_ventas_tr.='<td>$ '.number_format($item->total_general,2,'.',',').'</td>';
                    $html_ventas_tr.='<td>$ '.number_format($pagado,2,'.',',').'</td>';
                    $html_ventas_tr.='<td>$ '.number_format($saldo,2,'.',',').'</td>';
                    $html_ventas_tr.='<td>'.$item->serie.' '.$item->Folio.'</td>';
                    $html_ventas_tr.='<td>'.$btn_fac.'</td>';
                    $html_ventas_tr.='<td class="des_'.$item->combinadaId.'_1">'.$descripcion.'</td>';
                    $html_ventas_tr.='<td>'.$btn_fac_edit.'</td>';
                $html_ventas_tr.='</tr>';
                if($tipo==0){
                    if($saldo>0){
                        $html_ventas.=$html_ventas_tr;
                    }
                }else{
                    $html_ventas.=$html_ventas_tr;
                }
            }
        //============================================================================
        //============================================================================
           
                $strq_tp=$this->ModeloEstadocuenta->sqlpolizas($idcliente,'',$eje);
                $query_tp = $this->db->query($strq_tp);
            foreach ($query_tp->result() as $item) {
                $pago_manual=0;
                //=============================================
                    $resultfactura=$this->ModeloCatalogos->ontenerdatosfacturapoliza($item->id);
                    if($resultfactura->num_rows()==0){
                        $result = $this->ModeloCatalogos->pagos_poliza($item->id);
                        foreach ($result as $itempp) {
                            $pago_manual=$itempp->pago;
                        }
                    }
                    foreach($resultfactura->result() as $item_rf){
                        $FacturasIdselected=$item_rf->FacturasId;
                        $resultvcp = $this->ModeloCatalogos->obtencionventapolizafactura($item_rf->FacturasId);

                        foreach ($resultvcp->result() as $itemvcp) {
                            if ($itemvcp->combinada==2) {
                                    
                                //========================================
                                    $result = $this->ModeloCatalogos->pagos_poliza($itemvcp->ventaId);
                                    foreach ($result as $itempp) {
                                        $pago_manual=$itempp->pago;
                                    }
                                //========================================
                            }
                        }
                        $resultvcp_cp = $this->ModeloCatalogos->obteciondecomplememtofactura($FacturasIdselected);
                        foreach ($resultvcp_cp->result() as $itemcp) {
                            //$pago_manual=$itemcp->ImpPagado;
                        }
                    }
                //=============================================
                //==========================================================
                    $pagop=0;
                    $resultfactura=$this->ModeloCatalogos->ontenerdatosfacturapoliza($item->id);
                    foreach($resultfactura->result() as $item_rf){
                        $FacturasIdselected=$item_rf->FacturasId;
                        $resultvcp = $this->ModeloCatalogos->obtencionventapolizafactura($FacturasIdselected);
                        foreach ($resultvcp->result() as $itemvcp) {
                            if($itemvcp->combinada==0){

                                        
                                //=======================================
                                     $result_pp0 = $this->ModeloCatalogos->pagos_ventas($itemvcp->ventaId); 
                                       foreach ($result_pp0 as $item_pp0) { 
                                            $pagop=$pagop+$item_pp0->pago; 
                                        } 
                                //=======================================
                            }
                            if ($itemvcp->combinada==2) {
                                    
                                //========================================
                                    $result_pp = $this->ModeloCatalogos->pagos_poliza($itemvcp->ventaId);
                                    foreach ($result_pp as $item_pp) {
                                        $pagop=$pagop+$item_pp->pago;
                                    }
                                //========================================
                            }

                        }
                    }
                //===================================================
                $pagado=$pago_manual+$item->ImpPagado+$pagop;
                if($item->siniva==1){
                    $total_general=$item->subtotalpoliza;
                }else{
                    $total_general=$item->subtotalpoliza+round(($item->subtotalpoliza*0.16),2);
                }
                if($total_general>0){
                    $saldo=$total_general-$pagado;
                    if($item->FacturasId>0){
                            $descripcion=$this->ModeloCatalogos->descripcionfactura($item->FacturasId);
                        }else{
                            $descripcion='';
                        }
                    if($item->FacturasId>0){
                            $btn_fac='<a class="b-btn b-btn-primary" href="'.base_url().'Configuracionrentas/generarfacturas/'.$item->FacturasId.'/1" target="_blank"><i class="fas fa-file-alt"></i></a>';
                            $btn_fac_edit='<a class="b-btn b-btn-primary" onclick="edit_factura_prefactura('.$item->id.',2)"><i class="fas fa-pencil-alt"></i></a>';
                    }else{
                        $btn_fac='';
                        $btn_fac_edit='';
                    }
                    if($item->info_factura!=null and $item->info_factura!=''){
                        $descripcion=$item->info_factura;
                    }
                    $html_poliza_tr='<tr>';
                        $html_poliza_tr.='<td><input type="checkbox" class="filled-in facturas_p_checked" id="facturav_2_'.$item->id.'" value="'.$item->id.'" data-clienteid="'.$idcliente.'" data-tipovc="2"><label for="facturav_2_'.$item->id.'"></label></td>';
                        $html_poliza_tr.='<td>'.$item->id.'</td>';
                        $html_poliza_tr.='<td>'.$item->reg.'</td>';
                        $html_poliza_tr.='<td>$ '.number_format($total_general,2,'.',',').'</td>';
                        $html_poliza_tr.='<td>$ '.number_format($pagado,2,'.',',').'<!--'.$pago_manual.'--><!--'.$pagop.'--></td>';
                        $html_poliza_tr.='<td>$ '.number_format($saldo,2,'.',',').'</td>';
                        $html_poliza_tr.='<td>'.$item->serie.' '.$item->Folio.'</td>';
                        $html_poliza_tr.='<td>'.$btn_fac.'</td>';
                        $html_poliza_tr.='<td class="des_'.$item->id.'_2">'.$descripcion.'</td>';
                        $html_poliza_tr.='<td>'.$btn_fac_edit.'</td>';
                    $html_poliza_tr.='</tr>';
                    if($tipo==0){
                        if($saldo>0){
                            $html_poliza.=$html_poliza_tr;
                        }
                    }else{
                        $html_poliza.=$html_poliza_tr;
                    }
                }
            }
        //============================================================================
            $query_fac_sv=$this->ModeloEstadocuenta->sqlfacturassinvinculo($idcliente,'',$eje);
            foreach ($query_fac_sv->result() as $item) {
                $result_pg=$this->ModeloCatalogos->getselectwheren('pagos_factura',array('factura'=>$item->FacturasId,'activo'=>1));
                $pago_manual=0;
                foreach ($result_pg->result() as $item_pg) {
                    $pago_manual=$pago_manual+$item_pg->pago;
                }
                $saldo=$item->total-($item->ImpPagado+$pago_manual);
                if($item->info_factura_x!=null and $item->info_factura_x!=''){
                    $descripcion=$item->info_factura_x;
                }else{
                    $descripcion=$item->Descripcion2;
                }
                $btn_fac='<a class="b-btn b-btn-primary" href="'.base_url().'Configuracionrentas/generarfacturas/'.$item->FacturasId.'/1" target="_blank"><i class="fas fa-file-alt"></i></a>';
                $btn_fac_edit='<a class="b-btn b-btn-primary" onclick="edit_factura_prefactura('.$item->FacturasId.',4)"><i class="fas fa-pencil-alt"></i></a>';

                $html_fac_sv_tr='<tr>';
                    $html_fac_sv_tr.='<td><input type="checkbox" class="filled-in facturas_fac_sv_checked" id="facturav_4_'.$item->FacturasId.'" value="'.$item->FacturasId.'" data-clienteid="'.$idcliente.'" data-tipovc="1"><label for="facturav_4_'.$item->FacturasId.'"></label></td>';
                    $html_fac_sv_tr.='<td>'.$item->serie.$item->Folio.'</td>';
                    $html_fac_sv_tr.='<td>'.$item->Nombre.'</td>';
                    $html_fac_sv_tr.='<td>'.$item->Rfc.'</td>';
                    $html_fac_sv_tr.='<td>$ '.number_format($item->total,2,'.',',').'</td>';
                    $html_fac_sv_tr.='<td>$ '.number_format($item->ImpPagado,2,'.',',').'</td>';
                    $html_fac_sv_tr.='<td>$ '.number_format($saldo,2,'.',',').'</td>';
                    $html_fac_sv_tr.='<td>'.$item->fechatimbre.'</td>';
                    $html_fac_sv_tr.='<td>'.$btn_fac.'</td>';
                    $html_fac_sv_tr.='<td class="des_'.$item->FacturasId.'_4">'.$descripcion.'</td>';
                    $html_fac_sv_tr.='<td>'.$btn_fac_edit.'</td>';
                $html_fac_sv_tr.='</tr>';
                if($saldo>0){
                   $html_facturas.=$html_fac_sv_tr; 
                }
                
            }

        //============================================================================
        

        $resul=array(
                    'renta'=>$html_renta,
                    'ventas'=>$html_ventas,
                    'poliza'=>$html_poliza,
                    'facturas'=>$html_facturas,
                    'codigo'=>date('YmdGis')
                );

        echo json_encode($resul);
    }
    function editardesfactura(){
        $params = $this->input->post();
        $id=$params['id'];
        $tipo=$params['tipo'];
        $des=$params['des'];

        if($tipo==0){//ventas normal
            $this->ModeloCatalogos->updateCatalogo('ventas',array('info_factura'=>$des),array('id'=>$id));
        }
        if($tipo==1){//ventas combinadas
            $this->ModeloCatalogos->updateCatalogo('ventacombinada',array('info_factura'=>$des),array('combinadaId'=>$id));
        }
        if($tipo==2){//polizas
            $this->ModeloCatalogos->updateCatalogo('polizasCreadas',array('info_factura'=>$des),array('id'=>$id));
        }
        if($tipo==3){//rentas
            $this->ModeloCatalogos->updateCatalogo('factura_prefactura',array('info_factura'=>$des),array('facId'=>$id));
        }
        if($tipo==4){//rentas
            $this->ModeloCatalogos->updateCatalogo('f_facturas',array('info_factura_x'=>$des),array('FacturasId'=>$id));
        }
        $resul=array(
                    'des'=>$des
                );

        echo json_encode($resul);
    }

    
    function reporte(){
        $data = $this->input->get();
        $params['rentas']=$data['rentas'];
        $params['ventas']=$data['ventas'];
        $params['ventasc']=$data['ventasc'];
        $params['polizas']=$data['polizas'];
        $params['cliente']=$data['cliente'];
        $params['facsv']=$data['facsv'];
        $params['codi']=$data['codi'];

        $this->load->view('Reportes/estadodecuentageneral',$data);
    }
    function reporte_excedente($prefId,$idcliente){
        $idcondicionextra=0;
        $data['prefId']=$prefId;
        $data['configuracionCotizacion'] = $this->ConfiguracionDocumentos_Model->getConfiguracion(1);

        $nombreUsuario = $this->ModeloCatalogos->getselectwheren('personal',array('personalId'=>$this->idpersonal));
        $nombreAux = $nombreUsuario->result();
        $data['nombreUsuario'] = $nombreAux[0]->nombre.' '.$nombreAux[0]->apellido_paterno.' '.$nombreAux[0]->apellido_materno;
        $data['emailUsuario'] = $nombreAux[0]->email;

        $data['idCotizacion']=$prefId;

        $result_pre = $this->ModeloCatalogos->getselectwheren('alta_rentas_prefactura',array('prefId'=>$prefId));
        $data['result_pre']=$result_pre;
        foreach ($result_pre->result() as $r_pre) {
            $data['fechareg']=$this->reformateofecha($r_pre->periodo_inicial);
            $data['fechareg2']=$this->reformateofecha($r_pre->periodo_final);

            $data['p_ini']=$r_pre->periodo_inicial;
            $data['p_fin']=$r_pre->periodo_final;
            $id_renta = $r_pre->id_renta;
            $idcondicionextra = $r_pre->idcondicionextra;
        }
        $result_ar = $this->ModeloCatalogos->getselectwheren('alta_rentas',array('id_renta'=>$id_renta));
        foreach ($result_ar->result() as $item_ar) {
            $indi_global=$item_ar->tipo2;//1 individual 2 global
            $clicks_mono = $item_ar->clicks_mono;
            $clicks_color = $item_ar->clicks_color;
            $precio_c_e_mono = $item_ar->precio_c_e_mono;
            $precio_c_e_color = $item_ar->precio_c_e_color;
            $monto = $item_ar->monto;
            $montoc = $item_ar->montoc;
        }
        if($idcondicionextra >0){
            $condiciones = $this->ModeloCatalogos->getselectwheren('rentas_condiciones',array('id'=>$idcondicionextra));
            foreach ($condiciones->result() as $itemrc) {
                $indi_global = $itemrc->tipo;//1 individual 2 global
                $monto = $itemrc->renta_m;
                $montoc = $itemrc->renta_c;
                $clicks_mono = $itemrc->click_m;
                $precio_c_e_mono = $itemrc->excedente_m;
                $clicks_color = $itemrc->click_c;
                $precio_c_e_color = $itemrc->excedente_c;
            }
        }
        $data['indi_global']=$indi_global;
        $data['monto']=$monto;
        $data['montoc']=$montoc;
        $data['clicks_mono']=$clicks_mono;
        $data['precio_c_e_mono']=$precio_c_e_mono;
        $data['clicks_color']=$clicks_color;
        $data['precio_c_e_color']=$precio_c_e_color;
        $data['tipov']=1;
        $data['datosCliente'] = $this->Clientes_model->getClientePorId($idcliente);

        $data['detalleperido']= $this->Rentas_model->detalleperido($prefId);
        $data['idcondicionextra']=$idcondicionextra;
        $this->load->view('Reportes/reporteexcedente',$data);
    }
    function reformateofecha($fecha){
        $dia=date("d",strtotime(date($fecha))); 
        $mes = date("n",strtotime(date($fecha)));
        $yyyy = date("Y",strtotime(date($fecha)));
        if ($mes=='1') { $mesl ="Enero"; }
        if ($mes=='2') { $mesl ="Febrero"; }
        if ($mes=='3') { $mesl ="Marzo"; }
        if ($mes=='4') { $mesl ="Abril"; }
        if ($mes=='5') { $mesl ="Mayo"; }
        if ($mes=='6') { $mesl ="Junio"; }
        if ($mes=='7') { $mesl ="Julio"; }
        if ($mes=='8') { $mesl ="Agosto"; }
        if ($mes=='9') { $mesl ="Septiembre"; }
        if ($mes=='10') { $mesl ="Octubre"; }
        if ($mes=='11') { $mesl ="Noviembre"; }
        if ($mes=='12') { $mesl ="Diciembre"; }
        $fecha=$dia.' de '.$mesl.' de '.$yyyy;
        return $fecha; 
    }
}