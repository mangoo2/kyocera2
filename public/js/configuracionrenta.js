var base_url = $('#base_url').val();
var addtr=0;
var idCliente = $('#idCliente').val();
$(document).ready(function(){
    $('.chosen-select').chosen({width: "100%"});
    $("#tabla_contratos").DataTable();
    $('.modal').modal();
    $("#filecontadores").fileinput({
            showCaption: false,
            //showUpload: false,// quita el boton de upload
            //rtl: true,
            allowedFileExtensions: ["csv"],
            browseLabel: 'Seleccionar archivo',
            uploadUrl: base_url+'Configuracionrentas_ext/cargacontadores',
            maxFilePreviewSize: 5000,
            previewFileIconSettings: {
                'docx': '<i class="fa fa-file-word-o text-primary"></i>',
                'xlsx': '<i class="fa fa-file-excel-o text-success"></i>',
                'pptx': '<i class="fa fa-file-powerpoint-o text-danger"></i>',
                'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
                'pdf': '<i class="fa fa-file-pdf-o text-danger"></i>',
                'zip': '<i class="fa fa-file-archive-o text-muted"></i>',
                'cer': '<i class="fas fa-file-invoice"></i>',
            },
            uploadExtraData: function (previewId, index) {
            var info = {
                        Id:0
                    };
            return info;
          }
        }).on('filebatchuploadcomplete', function(event, files, extra) {
            cargafilecontadoresreg(1);
            
            
          //location.reload();
        }).on('filebatchuploadsuccess', function(event, files, extra) {
            cargafilecontadoresreg(1);
            
          //location.reload();
          toastr.success('Se cargo el Archivo Correctamente','Hecho!');
        });
    obtenerventaspendientes();
});
function clientecontrato() {
	var idcliente = $('#idcliente option:selected').val();
    var idejecutivo = $('#idejecutivo option:selected').val();
    var tipo_estatus = $('#tipo_estatus option:selected').val();
	$.ajax({
        type:'POST',
        url: base_url+"index.php/Configuracionrentas_ext/contratorentacliente",
        data: {
            id:idcliente,
            ideje:idejecutivo,
            estatus:tipo_estatus
        },
        success: function (data){
        	$('.mostrar_tabla').html(data); 
            $("#tabla_contratos").DataTable({
                responsive: true,
                columnDefs: [
                    { responsivePriority: 1, targets: 0 },
                    { responsivePriority: 2, targets: -1 }
                ]
            });
        }
    });
}
function  vercontrato(idcontrato) {
     window.location.href = base_url+"index.php/Configuracionrentas/ContratosPagos/"+idcontrato;
}
function buscarseriefolio(){
    var idcliente = $('#idcliente option:selected').val();
    if(idcliente>0){
        $.ajax({
        type:'POST',
        url: base_url+"index.php/Generales/buscarseriefolio",
        data: {
            id:idcliente,
            buscar:$('#buscarseriefolio').val()
        },
        success: function (data){
            var array = $.parseJSON(data);
            //console.log(array);
            $('.contratossearch').removeClass('consearch_active');
            array.forEach(function(element) {
                $('.contratossearch_'+element.idcontrato).addClass('consearch_active');
            });
        }
    });
    }else{
        toastr["warning"]("Seleccione un cliente");
    }
}
function listclientecontratos(idcliente){
    $("#idcliente").attr('disabled', false).trigger("chosen:updated");
    $("#idejecutivo").attr('disabled', false).trigger("chosen:updated");
    $("#tipo_estatus").attr('disabled', false).trigger("chosen:updated");
    $("#idcliente").val(idcliente).trigger("chosen:updated");
    clientecontrato();
}
function cargafilecontadores(){
    $('#modal_file').modal('open');
    cargafilecontadoresreg(0);
}
function cargafilecontadoresreg(view){
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Configuracionrentas_ext/contreg",
        data: {
            id:0
        },
        success: function (data){
            $('.table_cont_reg').html(data);
            if(view==1){
                setTimeout(function(){
                    importarcontadoresall(0);
                },1100);
            }
            
        }
    });
}
function importarcontadores(row){
    var reg=$('.import_cont_'+row).data('reg');
    var idcontrato = $('#idcontrato').val()==undefined?0:$('#idcontrato').val();
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Configuracionrentas_ext/contregall",
        data: {
            reg:reg,
            idcontrato:idcontrato
        },
        success: function (data){
            var array = $.parseJSON(data);
            //console.log(array);
            array.forEach(function(element) {
                $('.add_cont_'+element.serie+'_mono').val(element.cont_mono).change();
                $('.add_cont_'+element.serie+'_color').val(element.cont_color).change();
                $('.add_cont_'+element.serie+'_escaner').val(element.cont_escaner).change();
            });
        }
    });
}
function importarcontadoresall(row){
    $('#modal_file_info').modal('open');
    $('.table_cont_reg_inf').html('');
    var reg=$('.import_cont_'+row).data('reg');
    var idcontrato = $('#idcontrato').val()==undefined?0:$('#idcontrato').val();
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Configuracionrentas_ext/contregall",
        data: {
            reg:reg,
            idcontrato:idcontrato
        },
        success: function (data){
            var array = $.parseJSON(data);
            //console.log(array);
            array.forEach(function(item) {
                var contadorusado='';
                if(item.cm>0){
                    contadorusado='style="background: yellow;"';
                }
                if(item.ccol>0){
                    contadorusado='style="background: yellow;"';
                }
                if(item.cscan>0){
                    contadorusado='style="background: yellow;"';
                }
                var hrefcontrato='';
                var hrefcontratof='';
                var hrefcontratoe='';
                if(item.idcontrato>0){
                    hrefcontrato='<a href="'+base_url+'index.php/Configuracionrentas/ContratosPagos/'+item.idcontrato+'" target="_black">'+item.idcontrato+'</a>';
                    hrefcontratof=item.folio;
                    hrefcontratoe=item.empresa;
                }
                $('.table_cont_reg_inf').append('<tr '+contadorusado+'><td>'+item.modelo+'</td><td>'+item.serie+'</td><td>'+item.ubicacion+'</td><td>'+item.fecha_obtencion+'</td><td>'+item.cont_mono+'</td><td>'+item.cont_color+'</td><td>'+item.cont_escaner+'</td><td>'+hrefcontrato+'</td><td>'+hrefcontratof+'</td><td>'+hrefcontratoe+'</td></tr>');
            });
            if(array.length==0){
                $('.table_cont_reg_inf').append('<tr ><td colspan="10">No hay contadores registrados para el contrato actual</td></tr>');
            }
        }
    });
}
function deletecontadoresall(row){
    var reg=$('.import_cont_'+row).data('reg');
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Confirma la eliminacion de los datos?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                //===================================================
                $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Configuracionrentas_ext/deletecontadoresall",
                    data: {
                        reg:reg
                    },
                    success: function (data){
                        cargafilecontadoresreg(0);
                    }
                });
                //================================================
            },
            cancelar: function () 
            {
                
            }
        }
    });
}
function obtenerventaspendientes(){
    var idcliente = $('#idCliente').val();
    //restriccion para cuando tenga mas de 3 prefacturas pendientes de agregar pago
    if(idcliente>0){
        $.ajax({
            type:'POST',
            url: base_url+'index.php/Reportes/rventasnum/'+idcliente,
            success:function(data){
                //console.log(data);
                var numventas=parseInt(data);
                if(numventas>0){
                    /*
                    $.confirm({
                        boxWidth: '30%',
                        useBootstrap: false,
                        icon: 'fa fa-warning',
                        title: 'Atención!',
                        content: 'No se tiene permitido tiene '+numventas+' pendientes de pago!<br> Para continuar se necesita permisos de administrador<br>'+
                                 '<input type="text" placeholder="Contraseña" id="contrasena" class="name form-control" data-val="" maxlength="10"/>',
                        type: 'red',
                        typeAnimated: true,
                        buttons:{
                            confirmar: function (){
                                //var pass=$('#contrasena').val();
                                var pass=$('#contrasena').data('val')
                                if (pass!='') {
                                     $.ajax({
                                        type:'POST',
                                        url: base_url+"index.php/Sistema/solicitarpermiso",
                                        data: {
                                            pass:pass
                                        },
                                        success: function (response){
                                                var respuesta = parseInt(response);
                                                if (respuesta==1) {
                                                }else{
                                                    notificacion(2);
                                                    setTimeout(function(){
                                                        location.reload();
                                                    },1000);
                                                }
                                        },
                                        error: function(response){
                                            notificacion(1);
                                            setTimeout(function(){
                                                        location.reload();
                                            },2000);
                                        }
                                    });
                                    
                                }else{
                                    notificacion(0);
                                    setTimeout(function(){
                                        window.location.href = base_url+"index.php/Clientes";
                                    },2000);
                                }
                            },
                            cancelar: function (){
                                setTimeout(function(){
                                    window.location.href = base_url+"index.php/Clientes";
                                },1000);
                            }
                        }
                    });
                    */
                    
                }
            }
        });
        /*
        setTimeout(function(){
            $("#contrasena").passwordify({
                maxLength: 10,
                onlyNumbers: true
              }).focus();
        },1000);
        */
        //console.log('obtenerventaspendientes()');
    }
}
function aplasarnoti(idcon){
    var vig=$('.aplasarnoti_256').data('vigencia');
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Configuracionrentas_ext/aplasarnoti",
        data: {
            idcon:idcon,
            vig:vig
        },
        success: function (data){
        }
    });
}
function upload_data_view_oc(idcon){
    $.ajax({
        type:'POST',
        url: base_url+"Contratos/upload_data_view_oc",
        data: {
            idcon:idcon
        },
        statusCode:{
            404: function(data){
                toastr["error"]("No se pudo pocesar la solicitud","Error"); 
            },
            500: function(data){
                toastr["error"]("No se pudo pocesar la solicitud","Error"); 
                console.log(data);
            }
        },
        success: function (data){
            $('.files_orden').html(data);
            
        }
    });
} 
function reporte_contadores(idcont){
    var html='';
        html+='<div class="row">';
            html+='<div class="col s6">';
                html+='<label>Fecha Inicial</label>';
                html+='<input type="date"  id="rc_f_i" class="form-control-bmz">';
            html+='</div>';
            html+='<div class="col s6">';
                html+='<label>Fecha Inicial</label>';
                html+='<input type="date"  id="rc_f_f" class="form-control-bmz">';
            html+='</div>';
        html+='</div>';
        html+='<div class="row">';
            html+='<div class="col s12">';
                html+='<input name="view_fol" type="radio" id="view_fol_0" value="0">';
                html+='<label for="view_fol_0">Todas las series</label>';
                html+='<input name="view_fol" type="radio" id="view_fol_1" value="1" checked="">';
                html+='<label for="view_fol_1">Series vigentes</label>';
            html+='</div>';
        html+='</div>';
     $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Generacion de reporte de contadores',
        content: html,
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var serie=$('input:radio[name=view_fol]:checked').val();
                var fi=$('#rc_f_i').val();
                var ff=$('#rc_f_f').val();
                //===================================================
                window.open(base_url+"index.php/Configuracionrentas_ext/reportecontadores/"+idcont+"?ser="+serie+"&fi="+fi+"&ff="+ff, '_blank');
                //================================================
            },
            cancelar: function () 
            {
                
            }
        }
    });
}
function fm_pago_se(){
    var met = $('#idmetodo option:selected').val();
      var fecha = $('#fechaactual').val();
      if(met==1){
        $('#fecha_p').val(fecha).prop({'readonly':true});
      }else{
        $('#fecha_p').prop({'readonly':false});
      }
}