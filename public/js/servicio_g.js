var base_url = $('#base_url').val();
//var tecnico = $('#tecnico').val();
var fecha = $('#calendarinput').val();
$(document).ready(function($) {
	
});
function obtenerserviciosreport(){
	$('#modalser_report').modal('open');
	var tecnico = $('#tecnico').val();
    $.ajax({
        type:'POST',
        url: base_url+'index.php/AsignacionesTecnico/optencioninfoservicios/'+fecha+'/'+tecnico,
        data: {
            tecnico: tecnico,
            fecha: fecha,
        },
        async: false,
        statusCode:{
            404: function(data){
                toastr["error"]("Error 404");
            },
            500: function(){
                toastr["error"]("Error 500");
            }
        },
        success:function(data){
            console.log(data);
            $('.info_servicios_report').html(data);
            setTimeout(function(){
                info_u_cot();
            },1000)
        }
    });
}
function editar_qrecibio(tipo,idser,idsered){
    var nom=$('.data_qrecivio_'+tipo+'_'+idser+'_'+idsered).data('nom');
    var html='';
    html+='<div class="row">';
            html+='<div class="col s12">';
                html+='<label >Nuevo Nombre de quien valido</label>';
                html+='<input name="nqrecibio" type="text" id="nqrecibio" value="'+nom+'" class="form-control-bmz" />';
            html+='</div>';
        html+='</div>';
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Editar quien valido',
        content: html,
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var nqrecibio =$('#nqrecibio').val();
               
                $.ajax({
                    type:'POST',
                    url: base_url+'index.php/AsignacionesTecnico/editar_qrecibio',
                    data: {
                        ser : idser,
                        sere: idsered,
                        tipo: tipo,
                        nqrecibio: nqrecibio
                    },
                    async: false,
                    statusCode:{
                        404: function(data){
                            toastr["error"]("Error 404");
                        },
                        500: function(){
                            toastr["error"]("Error 500");
                        }
                    },
                    success:function(data){
                        toastr["success"]("Editado Correctamente");
                        obtenerserviciosreport();
                    }
                });
                   
            },/*
            'Ver Pendientes': function (){
                obtenerventaspendientesinfo(idcliente);
            },*/
            cancelar: function (){

            }
        }
    });
}
function info_u_cot(){
    $('.btn_info_cot').click(function(event) {
        var info=$(this);
        var tec = info.data('tec');
        var fecha = info.data('fecha');
        var comtec = info.data('comtec');
        var contr1 = info.data('contr1');
        var contr2 = info.data('contr2');
        var contr3 = info.data('contr3');

        var id1 = info.data('id1');
        var p1 = info.data('p1');
        var m1 = info.data('m1');
        var md1 = info.data('md1');
        var r1 = info.data('r1');


        var id2 = info.data('id2');
        var p2 = info.data('p2');
        var m2 = info.data('m2');
        var md2 = info.data('md2');
        var r2 = info.data('r2');


        var id3 = info.data('id3');
        var p3 = info.data('p3');
        var m3 = info.data('m3');
        var md3 = info.data('md3');
        var r3 = info.data('r3');
        var html='';
            html+='<table>';
                html+='<tr><th>Tecnico</th><td colspan="4">'+tec+'</td></tr>';
                html+='<tr><th>Fecha</th><td colspan="4">'+fecha+'</td></tr>';
                html+='<tr><th>Comentario Tecnico</th><td colspan="4">'+comtec+'</td></tr>';
                html+='<tr><td></td><td colspan="4"></td></tr>';
                html+='<tr><th colspan="5">Motivo de la cotizacion</th></tr>';
                if(contr1!='' && contr1!='null' && contr1!=null){
                    html+='<tr><th>Por desgaste</th><td colspan="4">'+contr1+'</td></tr>';
                }
                if(id1>0){
                    html+='<tr><th>Devolución</th><td>'+p1+'</td><td>'+m1+'</td><td>'+md1+'</td><td>'+r1+'</td></tr>';
                }
                if(contr2!='' && contr2!='null' && contr2!=null){
                    html+='<tr><th>Por daño</th><td colspan="4">'+contr2+'</td></tr>';
                }
                if(id2>0){
                    html+='<tr><th>Devolución</th><td>'+p2+'</td><td>'+m2+'</td><td>'+md2+'</td><td>'+r2+'</td></tr>';
                }
                if(contr3!='' && contr3!='null' && contr3!=null){
                    html+='<tr><th>Por garantía</th><td colspan="4">'+contr3+'</td></tr>';
                }
                if(id3>0){
                    html+='<tr><th>Devolución</th><td>'+p3+'</td><td>'+m3+'</td><td>'+md3+'</td><td>'+r3+'</td></tr>';
                }
                
            html+='</table>';
        $.alert({boxWidth: '90%',useBootstrap: false,title: 'Ultima solicitud de Cotización',theme: 'bootstrap',content: html});  
    });
}