<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contadores extends CI_Controller {
	public function __construct(){
        parent::__construct();
        $this->load->model('ModeloCatalogos');
        $this->load->model('Rentas_model');
    }
	public function index(){
        redirect('Sistema');
	}
    function periodoletras($inicial,$final){
        $dia_inicial=date('d' , strtotime($inicial));
        $dia_final=date('d' , strtotime($final));
        $mes_inicial=date('m' , strtotime($inicial));
        $mes_final=date('m' , strtotime($final));
        $ano_inicial=date('Y' , strtotime($inicial));
        $ano_final=date('Y' , strtotime($final));
        $texto='RENTA DEL ';
        if($ano_inicial==$ano_final){
          if($mes_inicial==$mes_final){
            $texto.=$dia_inicial.' AL '.$dia_final.' '.$this->messpanol($mes_inicial).' DE '.$ano_final;
          }else{
            $texto.=$dia_inicial.' DE '.$this->messpanol($mes_inicial).' AL '.$dia_final.' DE '.$this->messpanol($mes_final).' DE '.$ano_final;
          }
        }else{
          $texto.=$dia_inicial.' DE '.$this->messpanol($mes_inicial).' DE '.$ano_inicial.' AL '.$dia_final.' DE '.$this->messpanol($mes_final).' DE '.$ano_final;
        }
        return $texto;
    }
    function periodoletrase($inicial,$final){
        $dia_inicial=date('d' , strtotime($inicial));
        $dia_final=date('d' , strtotime($final));
        $mes_inicial=date('m' , strtotime($inicial));
        $mes_final=date('m' , strtotime($final));
        $ano_inicial=date('Y' , strtotime($inicial));
        $ano_final=date('Y' , strtotime($final));
        $texto='EXCEDENTE DEL ';
        if($ano_inicial==$ano_final){
          if($mes_inicial==$mes_final){
            $texto.=$dia_inicial.' AL '.$dia_final.' '.$this->messpanol($mes_inicial).' DE '.$ano_final;
          }else{
            $texto.=$dia_inicial.' DE '.$this->messpanol($mes_inicial).' AL '.$dia_final.' DE '.$this->messpanol($mes_final).' DE '.$ano_final;
          }
        }else{
          $texto.=$dia_inicial.' DE '.$this->messpanol($mes_inicial).' DE '.$ano_inicial.' AL '.$dia_final.' DE '.$this->messpanol($mes_final).' DE '.$ano_final;
        }
        return $texto;
    }
    function messpanol($mes){
      switch ($mes) {
        case '01':
          $mes ="ENERO";
          break;
        case '02':
          $mes ="FEBRERO";
          break;
        case '03':
          $mes ="MARZO";
          break;
        case '04':
          $mes ="ABRIL";
          break;
        case '05':
          $mes ="MAYO";
          break;
        case '06':
          $mes ="JUNIO";
          break;
        case '07':
          $mes ="JULIO";
          break;
        case '08':
          $mes ="AGOSTO";
          break;
        case '09':
          $mes ="SEPTIEMBRE";
          break;
        case '10':
          $mes ="OCTUBRE";
          break;
        case '11':
          $mes ="NOVIEMBRE";
          break;
        case '12':
          $mes ="DICIEMBRE";
          break;
        
        default:
          $mes ="";
          break;
      }
      return $mes;
    }
    function consultarcontadores(){
        $idcontrato = $this->input->post('idcontrato'); 
        $idpoliza = $this->input->post('idpoliza'); 
        $tipoperiodo = $this->input->post('tipo'); //0 ultimoperiodo 1 anteriores
        $contadoreshtml='';

        $where_ventas=array('prefId'=>$idpoliza);
        $resultadov = $this->ModeloCatalogos->db14_getselectwheren('alta_rentas_prefactura',$where_ventas);
        $id_personal=0;
        foreach ($resultadov->result() as $item) {
          $periodoinicial=$item->periodo_inicial;
          $periodofinal=$item->periodo_final;
          //==============================================================
                  $periodoiniciale = strtotime ( '- 1 month', strtotime ( $periodoinicial ) ) ;
                  $periodoiniciale = date ( 'Y-m-d' , $periodoiniciale);

                  //$periodoinicialef = strtotime ( '+ 1 month', strtotime ( $periodofinal ) ) ;
                  //$periodoinicialef = date ( 'Y-m-d' , $periodoinicialef);
                  $diainie = date ( 'd' , strtotime($periodoiniciale));
                  if ($diainie==1) {
                    $periodoinicialef = new DateTime($periodoiniciale);
                    $periodoinicialef->modify('last day of this month');
                    $periodoinicialef = $periodoinicialef->format('Y-m-d');
                  }else{
                    $periodoinicialef = strtotime ( '- 1 month', strtotime ( $periodofinal ) ) ;
                    $periodoinicialef = date ( 'Y-m-d' , $periodoinicialef);

                    //$periodoinicialef = strtotime ( '-1 day', strtotime ( $periodoinicialef ) ) ;
                    //$periodoinicialef = date ( 'Y-m-d' , $periodoinicialef );
                  }
                  
    
            //==============================================================
          $fechaperiodo=$this->periodoletras($item->periodo_inicial,$item->periodo_final);
          $perriodone=$this->periodoletrase($periodoiniciale,$periodoinicialef);
          $contadoreshtml.='<input type="hidden" id="idpolizaedit" value="'.$idpoliza.'" readonly>
                            <div class="row">
                              <div class="col s3 m1"><label>PERIODO</label></div>
                              <div class="col s6 m3"><input id="periodo_inicial_edit" name="periodo_inicial_edit" class="form-control-bmz" type="date" value="'.$item->periodo_inicial.'"></div>
                              <div class="col s2 m1"><label>AL</label></div>
                              <div class="col s6 m3"><input id="periodo_final_edit" name="periodo_final_edit" class="form-control-bmz" type="date" value="'.$item->periodo_final.'" ></div>
                            </div>
                            <div class="row">
                            <div class="col s3 m2"><label>PERIODO RENTA MONOCROMATICO</label></div>
                            <div class="col s3 m5"><input type="text" name="" value="'.$fechaperiodo.'" readonly=""></div>
                            <div class="col s3 m1"><input type="text" id="costoperiodo_edit" value="'.$item->subtotal.'" readonly=""></div>
                            <div class="col s3 m2"><label>TOTAL FACTURA</label></div>
                            <div class="col s3 m2"><input type="text" id="totalperiodo_edit" value="0" readonly=""></div>
                          </div>';
            $contadoreshtml.='<div class="row removecostoperiodocolor_edit" style="display: none;">
                              <div class="col s3 m2"><label>PERIODO RENTA COLOR</label></div>
                              <div class="col s3 m5"><input type="text" name="" value="'.$fechaperiodo.'" readonly=""></div>
                              <div class="col s3 m1"><input type="text" id="costoperiodocolor_edit" value="'.$item->subtotalcolor.'" readonly=""></div>
                            </div>';
            $contadoreshtml.='<div class="row excedentesmono_edit">
                                <div class="col s3 m2"><label>EXCEDENTES MONOCROMATICO</label></div>
                                <div class="col s3 m5"><input type="text" value="'.$perriodone.'" readonly=""></div>
                                <div class="col s3 m1"><input type="text" id="costoperiodoexcedente_edit" name="" value="0" readonly=""></div>
                              </div>';
            $contadoreshtml.='<div class="row excedentescolor_edit" style="display: none;">
                                <div class="col s3 m2"><label>EXCEDENTES COLOR</label></div>
                                <div class="col s3 m5"><input type="text" value="'.$perriodone.'" readonly=""></div>
                                <div class="col s3 m1"><input type="text" id="costoperiodoexcedente_c_edit" name="" value="0" readonly=""></div>
                              </div>';
        }

        $contadoreshtml.='';
      
      //=========================================================================
        if($tipoperiodo==0){
            $equiposrenta = $this->Rentas_model->equiposrentas($idcontrato);
        }
        if($tipoperiodo==1){
            $equiposrenta = $this->Rentas_model->detalleperido($idpoliza);
            $equiposrenta=$equiposrenta->result();
            //$equiposrenta = $this->Rentas_model->equiposrentas0($idpoliza);
        }
      //$equiposrenta = $this->Rentas_model->equiposrentas($idcontrato);
      $contadoreshtml.='
        <table class="responsive-table display centered" id="profactura_edit">
          <thead>
            <tr>
              <!--<th rowspan="2">Img</th>-->
              <th rowspan="2">TIPO</th>
              <th rowspan="2">MODELO</th>
              <th rowspan="2">SERIE</th>
              <th colspan="3">COPIA / IMPRESIÓN</th>
              <th colspan="3">ESCANEO</th>
              
              <th rowspan="2" class="removeproduccion">PRODUCCIÓN</th>
              <th rowspan="2">TONER CONSUMIDO</th>
              <th rowspan="2">DESCUENTO CLICKS</th>
              <th rowspan="2">DESCUENTO ESCANEO</th>
              <th rowspan="2">PRODUCCIÓN TOTAL</th>
              <th rowspan="2"># TONER CONSUMIDO</th>
              <th rowspan="2" class="removetipo2">EXCEDENTES</th>
              <th rowspan="2" class="removetipo2">COSTO EXCEDENTES</th>
              <th rowspan="2" class="removetipo2">TOTAL EXCEDENTE</th>
            </tr>
            <tr>
              <th>CONTADOR INICIAL</th>
              <th>CONTADOR FINAL</th>
              <th>PRODUCCIÓN</th>
              <th>CONTADOR INICIAL</th>
              <th>CONTADOR FINAL</th>
              <th>PRODUCCIÓN</th>
            </tr>
          </thead>
          <tbody>';
            $equiporow=1;
          foreach ($equiposrenta as $item){ 
            if ($item->tipo==1||$item->tipo==3) { 
              $contadoreshtml.='<tr>
                <!--<td class="img_captura_'.$item->id.'_'.$item->serieId.'_edit">
                </td>-->
                <td>
                    Monocromáticos
                    <input type="hidden" class="fd_equipo_edit" value="'.$item->id.'" readonly>
                    <input type="hidden" class="fd_serie_edit" value="'.$item->serieId.'" readonly>
                    <input type="hidden" class="fd_tipo_edit" value="1" readonly>
                    <input type="hidden" class="prefdId_edit prefdId_'.$item->id.'_'.$item->serieId.'_1_edit" readonly>
                </td>
                <td>'.$item->modelo.'</td>
                <td class="color_serie serie_edit_'.$item->serieId.'" id="select_serie_edit_'.$item->serieId.'">'.$item->serie.'</td>
                <td><input type="number" class=" f_c_c_i_edit f_c_cont_ini_'.$item->id.'_'.$item->serieId.'_1_edit form-control-bmz" readonly></td>
                <td>
                  <input type="number" 
                    class=" f_c_c_f_edit f_c_cont_fin_'.$item->id.'_'.$item->serieId.'_1_edit prfprocontador_edit form-control-bmz" 
                    data-equipo="'.$item->id.'" 
                    data-serie="'.$item->serieId.'" 
                    data-moco="1"
                    data-tomarescaner="1" 
                    data-tipo="1" 
                    value="0" >
                </td>
                <td><input type="number" class=" f_c_c_p_edit f_c_cont_pro_'.$item->id.'_'.$item->serieId.'_1_edit form-control-bmz" readonly></td>
                <td><input type="number" class=" f_e_c_i_edit f_e_cont_ini_'.$item->id.'_'.$item->serieId.'_1_edit form-control-bmz" readonly></td>
                <td>
                  <input type="number" 
                    class=" f_e_c_f_edit f_e_cont_fin_'.$item->id.'_'.$item->serieId.'_1_edit prfprocontador_edit form-control-bmz" 
                    data-equipo="'.$item->id.'" 
                    data-serie="'.$item->serieId.'" 
                    data-moco="1"
                    data-tomarescaner="1"
                    data-tipo="1"  
                    value="0" >
                </td>
                <td><input type="number" class=" f_e_c_p_edit f_e_cont_pro_'.$item->id.'_'.$item->serieId.'_1_edit form-control-bmz" readonly></td>
                <td class="removeproduccion"><input type="number" class="f_produccion_edit f_produccion_'.$item->id.'_'.$item->serieId.'_1_edit form-control-bmz" readonly></td>
                <td><input type="number" placeholder="%" class="f_toner_consumido_edit f_toner_consumido_'.$item->id.'_'.$item->serieId.'_1_edit form-control-bmz"  readonly></td>
                <td>
                  <input type="number" 
                    class=" descuento_edit descuento_'.$item->id.'_'.$item->serieId.'_1_edit prfprocontador_edit form-control-bmz" 
                    data-equipo="'.$item->id.'" 
                    data-serie="'.$item->serieId.'" 
                    data-moco="1"
                    data-tomarescaner="1"
                    data-tipo="1"  
                    value="0" >
                </td>
                <td>
                  <input type="number" 
                    class=" descuentoe_edit descuentoe_'.$item->id.'_'.$item->serieId.'_1_edit prfprocontador_edit form-control-bmz" 
                    data-equipo="'.$item->id.'" 
                    data-serie="'.$item->serieId.'" 
                    data-moco="1"
                    data-tomarescaner="1"
                    data-tipo="1"  
                    value="0" >
                </td>
                <td><input type="number" class="produccion_total_edit produccion_mono_edit produccion_total_'.$item->id.'_'.$item->serieId.'_1_edit form-control-bmz" readonly></td>
                <td><input type="number" class="total_toner_edit produccion_monot_edit total_toner_'.$item->id.'_'.$item->serieId.'_1_edit form-control-bmz" readonly></td>
                <td class="removetipo2">
                  <input type="number" class="excedentes_edit excedentes_'.$item->id.'_'.$item->serieId.'_1_edit form-control-bmz" readonly>
                </td>
                <td class="removetipo2">
                  <input type="number" class="excedentescosto_edit excedentescosto_'.$item->id.'_'.$item->serieId.'_1_edit form-control-bmz" readonly>
                </td>
                <td class="removetipo2">
                  <input type="number" class="excedente_mono_edit excedentestotal_edit excedentetotal_'.$item->id.'_'.$item->serieId.'_1_edit form-control-bmz" value="0" readonly>
                </td>
              </tr>';
                $equiporow++; 
            }
          } 
        foreach ($equiposrenta as $item){ 
            if ($item->tipo==2||$item->tipo==3) { 
              if ($item->tipo==3) {
                $readonlyh='readonly';
                $display='style="display:none"';
                $tomarescaner=0;
              }else{
                $display='';
                $tomarescaner=1;
                $readonlyh='';
              }
            $contadoreshtml.='
              <tr>
                <!--<td class="img_captura_'.$item->id.'_'.$item->serieId.'_edit">
                </td>-->
                <td>
                    Color
                    <input type="hidden" class="fd_equipo_edit" value="'.$item->id.'" readonly>
                    <input type="hidden" class="fd_serie_edit" value="'.$item->serieId.'" readonly>
                    <input type="hidden" class="fd_tipo_edit" value="2" readonly>
                    <input type="hidden" class="prefdId_edit prefdId_'.$item->id.'_'.$item->serieId.'_2_edit" readonly>
                </td>
                <td>'.$item->modelo.'</td>
                <td class="color_serie serie_edit_'.$item->serieId.'" id="select_serie_edit_'.$item->serieId.'">'.$item->serie.'</td>
                <td>
                  <input type="number" class=" f_c_c_i_edit f_c_cont_ini_'.$item->id.'_'.$item->serieId.'_2_edit form-control-bmz" readonly>
                </td>
                <td>
                  <input type="number" 
                    class=" f_c_c_f_edit f_c_cont_fin_'.$item->id.'_'.$item->serieId.'_2_edit prfprocontador_edit form-control-bmz" 
                    data-equipo="'.$item->id.'" 
                    data-serie="'.$item->serieId.'"
                    data-moco="2"
                    data-tipo="2" 
                    data-tomarescaner="'.$tomarescaner.'" 
                    value="0" >
                </td>
                <td><input type="number" class=" f_c_c_p_edit f_c_cont_pro_'.$item->id.'_'.$item->serieId.'_2_edit form-control-bmz" readonly></td>
                <td><input type="number" class=" f_e_c_i_edit f_e_cont_ini_'.$item->id.'_'.$item->serieId.'_2_edit form-control-bmz" readonly '.$display.'></td>
                <td>
                  <input type="number" 
                    class=" f_e_c_f_edit f_e_cont_fin_'.$item->id.'_'.$item->serieId.'_2_edit prfprocontador_edit form-control-bmz" 
                    data-equipo="'.$item->id.'" 
                    data-serie="'.$item->serieId.'" 
                    data-moco="2"
                    data-tipo="2" 
                    data-tomarescaner="'.$tomarescaner.'" 
                    value="0" '.$readonlyh.' '.$display.'>
                </td>
                <td><input type="number" class=" f_e_c_p_edit f_e_cont_pro_'.$item->id.'_'.$item->serieId.'_2_edit form-control-bmz" '.$display.'  readonly></td>
                <td class="removeproduccion">
                  <input type="number" class="f_produccion_edit f_produccion_'.$item->id.'_'.$item->serieId.'_2_edit form-control-bmz" readonly>
                </td>
                <td><input type="number" placeholder="%" class="f_toner_consumido_edit f_toner_consumido_'.$item->id.'_'.$item->serieId.'_2_edit form-control-bmz"  readonly></td>
                <td>
                  <input type="number" 
                    class=" descuento_edit descuento_'.$item->id.'_'.$item->serieId.'_2_edit prfprocontador_edit form-control-bmz" 
                    data-equipo="'.$item->id.'" 
                    data-serie="'.$item->serieId.'"
                    data-moco="2"
                    data-tipo="2" 
                    data-tomarescaner="'.$tomarescaner.'" 
                    value="0" >
                </td>
                <td>
                  <input type="number" 
                    class="descuentoe_edit descuentoe_'.$item->id.'_'.$item->serieId.'_2_edit prfprocontador_edit form-control-bmz" 
                    data-equipo="'.$item->id.'" 
                    data-serie="'.$item->serieId.'"
                    data-moco="2"
                    data-tipo="2" 
                    data-tomarescaner="'.$tomarescaner.'" 
                    value="0" >
                </td>
                <td><input type="number" class="produccion_total_edit produccion_color_edit produccion_total_'.$item->id.'_'.$item->serieId.'_2_edit form-control-bmz" readonly></td>
                <td>
                  <input type="number" class="total_toner_edit produccion_colort_edit total_toner_'.$item->id.'_'.$item->serieId.'_2_edit form-control-bmz" readonly>
                </td>
                <td class="removetipo2">
                  <input type="number" class="excedentes_edit excedentes_'.$item->id.'_'.$item->serieId.'_2_edit form-control-bmz" readonly>
                </td>
                <td class="removetipo2">
                  <input type="number" class="excedentescosto_edit excedentescosto_'.$item->id.'_'.$item->serieId.'_2_edit form-control-bmz" readonly>
                </td>
                <td class="removetipo2">
                  <input type="number" class="excedente_color_edit  excedentestotal_edit excedentetotal_'.$item->id.'_'.$item->serieId.'_2_edit form-control-bmz" value="0" readonly>
                </td>
              </tr>';
            $equiporow++;
          }
        } 
          
      $contadoreshtml.='</tbody>
     </table>';
      $contadoreshtml.='
          <div class="row">
            <div class="col s9"></div>
            <div class="col s3"><button class="btn waves-effect waves-light" type="button" style=" background-color: #56ea36;" onclick="editarcontadoress()">Editar</button></div>
          </div>';
      //=========================================================================
      $array  = array(
        'contadoreshtml' =>$contadoreshtml );
      echo json_encode($array);
    }
    function consultarcontadoresanteriores(){

    }
    
}