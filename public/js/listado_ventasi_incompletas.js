var base_url = $('#base_url').val();
var fechades = $('#fechades').val();

$(document).ready(function(){ 
    $('.modal').modal();
    $('.chosen-select').chosen({width: "90%"});
    table = $('#tabla_ventas_incompletas').DataTable();
	load();
    $('#cliente').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una opción',
        allowClear: true,
        ajax: {
            url: base_url+'Ventasi/searchclientes',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.id,
                        text: element.empresa

                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    });
    pre_pendientes();
}); 
function load() {
    var tipo= $('#tipoventa option:selected').val();
    var cliente= $('#cliente option:selected').val()==undefined?0:$('#cliente option:selected').val();
    var estatus= $('#tipoestatus option:selected').val();
    var fechaentrega=$('#fecha').val();
    var fec_ini_reg=$('#fechainicial_reg').val();
    var emp=$('#empresaselect option:selected').val();
    var tec=$('#tecnico option:selected').val();
    // Destruye la tabla y la crea de nuevo con los datos cargados 
    if(tipo==0||tipo==1||tipo==4){
        $('.titulo_fechaentraga').html('Solicitud<br>Fecha Entrega');
        $('.soloventas').show('show');
    }else{
        $('.titulo_fechaentraga').html('');
        $('.soloventas').hide('show');
    }
    table.destroy();
    table = $('#tabla_ventas_incompletas').DataTable({
        //pagingType: 'full_numbers',
           pagingType: 'input',
           //"sPaginationType": "listbox",
        //stateSave: true,
        responsive: !0,
        "bProcessing": true,
         search: {
                return: true
            }, 
        "serverSide": true, 
        "ajax": {
            "url": base_url+"index.php/Ventasi/getListaVentasIncompletas",
            type: "post",
            "data": {
                "tipo": tipo,
                'cliente':cliente,
                'status':estatus,
                'fecha':fechaentrega,
                'fec_ini_reg':fec_ini_reg,
                'emp':emp,
                'tec':tec
            },
        },
        "columns": [
            /* 
            {"data": null,
                render: function(data,type,row){
                    var id = row.id; 
                    var html='<input type="checkbox" class="filled-in ventaid" id="ventaid_'+id+'" value=""><label for="ventaid_'+id+'"></label>';
                    return html;

                }
            },
            */
            {"data": "checada",
                render:function(data,type,row){
                    var html='';
                        //html=row.checada;
                        html+='<!--'+row.checada+'--><!--'+row.id+'-->';
                        if(row.checada==1){
                            var l_che=' checked ';
                        }else{
                            var l_che='';
                        }
                        var id_l='press_ed_'+row.tipotabla+'_'+row.id;
                        html+='<input type="checkbox" class="filled-in press_ed_checked" id="'+id_l+'" onclick="checadopre('+row.tipotabla+','+row.id+')" '+l_che+'>\
                              <label for="'+id_l+'"></label>';
                        //html+=id_l;

                    return html;
                }
            },
            {"data": "id",
                render:function(data,type,row){
                    var html='';
                        html='';
                        if(row.tipotabla==4){
                             if(row.equipo>0){
                                html=row.equipo;
                            }else if(row.consumibles>0){
                                html=row.consumibles;
                            }else if(row.refacciones>0){
                                html=row.refacciones;
                            }
                        }else if(row.tipotabla==2){
                            html=row.equipo;
                        }else{
                            html=row.id;
                        }
                        html+='<!--'+row.equipo+'--><!--'+row.id+'-->';
                    return html;
                }
            },
            {"data": "empresa"},
            {"data": "id",
                render:function(data,type,row){
                    var html='';
                    if (row.prefactura==1) {
                        var comillas = "'";
                        html+='<a class="btn-floating green tooltipped prefacturas_h_'+row.id+'" data-idrh="'+row.id_rh+'" data-rentarh="'+row.renta_rh+'"  data-position="top" data-delay="50" data-tooltip="Prefactura" onclick="detalle('+row.id+','+comillas+row.tabla+comillas+',0)">\
                                    <i class="material-icons">assignment</i>\
                                  </a>';
                        if(row.tabla!='polizasCreadas'){
                            html+='<a class="btn-bmz cyan detalleprefactura tooltipped prefacturas_h_'+row.id+'" data-idrh="'+row.id_rh+'" data-rentarh="'+row.renta_rh+'"  data-position="top" data-delay="50" data-tooltip="Detalle prefactura" onclick="detalleinfo('+row.id+','+comillas+row.tabla+comillas+',0)">\
                                    <i class="material-icons">assignment</i>\
                                  </a>';
                        }
                    }
                    return html;
                }
            },
            {"data": "tabla",
                className: "tipo_tabla",
                render:function(data,type,row){
                    var htmlinfo='';
                        htmlinfo=row.tabla;
                    return htmlinfo;
                }
            },
            {"data": "vendedor"},
            {"data": "estatus",
                 render:function(data,type,row){
                        var html='';
                    if(data==1){
                        html='Pendiente';    
                    } 
                    else if(data==2){
                        html='Facturado';    
                    }
                    else if(data==3){
                        html='Finalizado';    
                    }  
                    //if(row.tabla=='combinada'||row.tabla=='ventas'){
                        if(row.e_entrega==1){
                            if(row.nombre!=null){
                                var persona=row.nombre+' '+row.apellido_paterno;    
                            }else{
                                var persona='';
                            }
                            if(row.entregado_fecha!=null){
                                var entregado_fecha=row.entregado_fecha;    
                            }else{
                                var entregado_fecha='';
                            }
                            
                            html='<div style="height: 24px;"><span class="new badge green">Entregada</span></div><div class="personaentregada">'+persona+'<br>'+entregado_fecha+'</div>';
                        }else{
                            html='<div style="height: 24px;"><span class="new badge red">Pendiente</span></div>';
                        }

                        html+='<div class="verificar_dev ver_dev_'+row.id+'_'+row.tipotabla+'" data-id="'+row.id+'" data-tipotabla="'+row.tipotabla+'" data-id_rh="'+row.id_rh+'" data-renta_rh="'+row.renta_rh+'"></div>';
                    //}
                    return html;
                }
            }, 
            {"data": "reg"},
            {"data": "estatus",
                 render:function(data,type,row){
                    

                    var html='<div class="ver_bod_'+row.id+'_'+row.tipotabla+'"></div>';
                    
                    return html;
                }
            }, 
            {"data": "fechaentrega",
                render:function(data,type,row){
                        var html='';
                        if(row.fechaentregah!='null' && row.fechaentregah!=null){
                            html+='<span class="fechaentregah" title="Fecha anterior">'+row.fechaentregah+'</span><br>';    
                        }
                        if(row.fechaentrega!='null' && row.fechaentrega!=null && row.fechaentrega!='0000-00-00'){
                            html+=row.fechaentrega;    
                        }else{
                            html+='En espera de la ejecutiva';
                        }
                        return html;
                    }
            },
            {"data": "estatus",
                 render:function(data,type,row){
                    var html='';
                    var muestraopciones=0;
                    if(row.tec_ser>0){
                        if(fechades>row.reg){
                            muestraopciones=0;
                            html+='<!--si-->';
                            html=row.nombre2+' '+row.apellido_paterno2+' '+row.apellido_materno2;
                        }else{
                            muestraopciones=1;
                            
                        }
                    }else{
                        muestraopciones=1;
                    }
                    if(muestraopciones==1){
                        if(row.tipotabla==1){
                            html+='<div class="venta_ver_tecnico venta_ver_tecnico_'+row.id+'_'+row.tipotabla+'" data-id="'+row.id+'" data-tipotabla="'+row.tipotabla+'"></div>';
                        }
                        if(row.tipotabla==2){
                            html+='<div class="renta_ver_tecnico renta_ver_tecnico_'+row.id+'_'+row.tipotabla+'" data-id="'+row.id+'" data-tipotabla="'+row.tipotabla+'"></div>';
                        }
                        
                        if(row.tipotabla==4){
                            html+='<div class="ventac_ver_tecnico ventac_ver_tecnico_'+row.id+'_'+row.tipotabla+'" data-id="'+row.id+'" data-tipotabla="'+row.tipotabla+'"></div>';
                        }
                        if(row.tipotabla==5){
                            html+='<div class="rentash_ver_tecnico rentash_ver_tecnico_'+row.id+'_'+row.tipotabla+'" data-id="'+row.id+'" data-tipotabla="'+row.tipotabla+'" data-id_rh="'+row.id_rh+'" data-renta_rh="'+row.renta_rh+'"></div>';
                        }
                    }
                    html+='<!--'+row.tec_ser+' '+fechades+' > '+row.reg+'-->';
                    return html;
                }
            }, 
            {"data": "id",
                render:function(data,type,row){
                    var html='';
                    if (row.prefactura==1) {
                        if(row.tipotabla!=3){
                            var comillas = "'";
                            html+='<a class="btn-floating red tooltipped prefacturas_h_'+row.id+'" data-idrh="'+row.id_rh+'" data-rentarh="'+row.renta_rh+'" data-position="top" data-delay="50" data-tooltip="Devolucion" onclick="detalle('+row.id+','+comillas+row.tabla+comillas+',1)">\
                                    <i class="material-icons">replay</i>\
                                  </a>';
                        }
                        
                    }
                    return html;
                }
            }, 

            {"data": "id",
                 render:function(data,type,row){
                    $('.tooltipped').tooltip();
                    var html='';
                    //if(row.estatus==1){
                        if(row.tipotabla==1||row.tipotabla==4){
                            var tableupdate=0;
                            if(row.tabla=='combinada'){
                                tableupdate=1;
                            }
                            if(row.tabla=='ventas'){
                                tableupdate=2;
                            }
                            html='<a class="btn-floating red tooltipped"  data-position="top" data-delay="50" \
                                     data-tooltip="Concluir" onclick="finalizaentrega('+row.id+','+tableupdate+')">\
                                    <i class="material-icons">stop</i>\
                                  </a>';
                        }else{
                            var idupdate=row.id;
                            if(row.tipotabla==5){
                                var idupdate=row.id_rh;
                            }
                            html='<a class="btn-floating red tooltipped"  data-position="top" data-delay="50" \
                                     data-tooltip="Concluir" onclick="finalizaentrega2('+idupdate+','+row.tipotabla+')">\
                                    <i class="material-icons">stop</i>\
                                  </a>';
                        }
                        //html+=' not_dev('+row.not_dev+')';
                        if(row.not_dev==1){
                            html+='<a class="waves-effect red btn-bmz c_can_ser" >Solicitud de Devolución</a>';
                        }
                         if(row.tipotabla==5){
                            html+='<div class="verificar_status verificar_status_'+row.renta_rh+'_'+row.id_rh+'" data-idrh="'+row.id_rh+'" data-rentarh="'+row.renta_rh+'"></div>';
                         }
                    //}
                    //else{
                    //    html='Sin acciones por ejecutar';
                    //}
                    return html;
                }
            }
        ],
        "order": [[ 7, "desc" ]],
        "lengthMenu": [[10,15, 50, 100], [10,15, 50, 100]],
        columnDefs: [
                { orderable: false, targets: [0,2,3,5,8,9] }
            ],
        // Cambiamos lo principal a Español
        createdRow:function(row,data,dataIndex){
            if (data.tipotabla==1 || data.tipotabla==4) {
                if (data.tipotabla==1) {
                    var combinada=0;
                }
                if (data.tipotabla==4) {
                    var combinada=1;   
                }
                $(row).find('td:eq(4)')
                .addClass('tipo_tabla_venta venta_tipo_'+combinada+'_'+data.id)
                .attr('data-idventa',data.id).attr('data-ventanc',combinada);    
            }
            if(fechaentrega!=''){
                if(fechaentrega!=data.fechaentrega){
                    $(row).addClass("fechadiferente");
                }
            }
            
        }
    }).on('draw',function(){
        $('.dataTables_filter').append('<i class="fa fa-search buttonsearch" onclick="searchtable_v()"></i>');
        verificarmontosvc();
        verificardev();
        vertecnicov();//1
        vertecnicovc();//4
        vertecnicorh();//5
        vertecnicor();//2
        verificarstatusnrdev();
        pre_pendientes();
    });
}
function searchtable_v(){
    var contenedor = document.getElementById('tabla_ventas_incompletas_filter');

    // Selecciona el input dentro del contenedor
    var input = contenedor.querySelector('input.form-control-bmz');

    // Obtén el valor del input
    var searchv = input.value;

    // Muestra el valor en la consola
    console.log(searchv);
    //var searchv =$("input[type=search]").val();
    table.search(searchv).draw();
}
function detalle(id,tabla,devol){
    if(devol==1){
        var getdevol='?devolucion=1';
    }else{
        var getdevol='';
    }
    if(tabla == 'ventas'){
        window.open(base_url+"Prefactura/view/"+id+getdevol, "Prefactura", "width=780, height=612");
    }else if(tabla == 'polizasCreadas'){
        window.open(base_url+"PolizasCreadas/view/"+id+getdevol, "Prefactura", "width=780, height=612");
    }else if(tabla == 'rentas'){
        window.open(base_url+"RentasCompletadas/view/"+id+'/1'+getdevol, "Prefactura", "width=780, height=612");
    }else if(tabla == 'combinada'){
        window.open(base_url+"Prefactura/viewc/"+id+getdevol, "Prefactura", "width=780, height=612");
    }else if(tabla == 'rentas historial'){
        var idrh= $('.prefacturas_h_'+id).data('idrh');
        var rentarh= $('.prefacturas_h_'+id).data('rentarh');
        window.open(base_url+"RentasCompletadas/viewh/"+rentarh+"/"+idrh+getdevol, "Prefactura", "width=780, height=612");
    }
    //window.open(base_url+"Prefactura/view/"+id, "Prefactura", "width=780, height=612");
}
function detalleinfo(id,tabla,devol){
    if(tabla == 'ventas'){
        var url=base_url+"Prefactura/view_info/"+id;
        //window.open(base_url+"Prefactura/view_info/"+id, "Prefactura", "width=780, height=612");
    }else if(tabla == 'polizasCreadas'){
        var url=base_url+"PolizasCreadas/view_info/"+id;
        //window.open(base_url+"PolizasCreadas/view_info/"+id, "Prefactura", "width=780, height=612");
    }else if(tabla == 'rentas'){
        var url=base_url+"RentasCompletadas/view_info/"+id;
        //window.open(base_url+"RentasCompletadas/view_info/"+id, "Prefactura", "width=780, height=612");
    }else if(tabla == 'combinada'){
        var url=base_url+"Prefactura/viewc_info/"+id;
        //window.open(base_url+"Prefactura/viewc_info/"+id, "Prefactura", "width=780, height=612");
    }else if(tabla == 'rentas historial'){
        var idrh= $('.prefacturas_h_'+id).data('idrh');
        var rentarh= $('.prefacturas_h_'+id).data('rentarh');
        var url=base_url+"RentasCompletadas/viewh_info/"+rentarh+"/"+idrh;
        //window.open(base_url+"RentasCompletadas/viewh_info/"+rentarh+"/"+idrh, "Prefactura", "width=780, height=612");
    }
    //window.open(url, "Prefactura", "width=780, height=612");
    $('#modaldetallep').modal('open');
    $('.modaliframedetalles').html('<iframe src="'+url+'"></iframe>');
}
function finaliza(id){
    $('#id_venta').val(id);
    
    $('#modalFinaliza').modal('open');
}
function finalizar_factura(){
    var id_v = $('#id_venta').val();  
    $.ajax({
      type: "POST",
      url: base_url+"index.php/Ventas/finalizar_factura",
      data:{id:id_v},
      success: function(data){
        $.alert({
            boxWidth: '30%',
            useBootstrap: false,
            title: 'Atención!',
            content: 'Finalizado correctamente'}
        );
        table.ajax.reload(); 
      }
    });
}
///////////////////////////////// 
function fecha_vencimiento(id_v){
    var result='';
    $.ajax({
      type: "POST",
      url: base_url+"index.php/Ventasi/verificar_fecha_vencimiento",
      data:{id:id_v},
      async:false,
      success: function(data){
        result=data;
      }
    });   
    return result;
}
function modal_cdfi(id,tipo) {
    $('#id_venta_e').val(id);
    $('#tipo_e').val(tipo);
    $('#modal_cfdi').modal('open');
}
function update_cfdi(){
    var id_venta=$('#id_venta_e').val();
    var tipo=$('#tipo_e').val();
    var cfdi=$('#cdfi_e').val();
    $.ajax({
      type: "POST",
      url: base_url+"index.php/Ventasi/update_cfdi_registro",
      data:{id_venta:id_venta,tipo:tipo,cfdi:cfdi},
      success: function(data){
         $.alert({
            boxWidth: '30%',
            useBootstrap: false,
            title: 'Atención!',
            content: 'Guardador correctamente'}
        );
      }
    }); 
    var tipo=$('#tipo_e').val('');
    var cfdi=$('#cdfi_e').val('');  
}
function modal_pago(id,tipo){
    $('#id_venta_p').val(id);
    $('#tipo_p').val(tipo);
    $('#fecha_p').val('');
    $('#pago_p').val('');
    $('#observacion_p').val('');
    $('#modal_pago').modal('open');
    if(tipo==1){//ventas
       $('.monto_prefactura').html('$'+monto_ventas(id));
    }else if(tipo==3){//póliza creadas
       $('.monto_prefactura').html('$'+monto_poliza(id));
    }else if(tipo==4){// Combinada
       $('.monto_prefactura').html('$'+monto_combinada(id));
    }
}
function monto_ventas(id_v){//ventas monto de prefactura
    var result='';
    $.ajax({
      type: "POST",
      url: base_url+"index.php/Ventasi/montoventas",
      data:{id:id_v},
      async:false,
      success: function(data){
        result=data;
      }
    });   
    return result;
}
function monto_poliza(id_v){//poliza monto de prefactura
    var result='';
    $.ajax({
      type: "POST",
      url: base_url+"index.php/Ventasi/montopoliza",
      data:{id:id_v},
      async:false,
      success: function(data){
        result=data;
      }
    });   
    return result;
}
function monto_combinada(id_v){//Combinada monto de prefactura
    var result='';
    $.ajax({
      type: "POST",
      url: base_url+"index.php/Ventasi/montocombinada",
      data:{id:id_v},
      async:false,
      success: function(data){
        result=data;
      }
    });   
    return result;
}
function guardar_pago_compras(){
    var form_pago = $('#form_pago');
    form_pago.validate({
            rules: 
            { 
                fecha: {
                    required: true
                },
                idmetodo: {
                    required: true
                },
                pago: {
                    required: true
                }
            },
            // Para mensajes personalizados
            messages: 
            {   
                fecha:{
                    required: "Campo obligatirio"
                },
                idmetodo:{
                    required: "Campo obligatirio"
                },
                pago:{
                    required: "Campo obligatirio"
                }
            },
        errorElement : 'div'
    });
    var valid = $("#form_pago").valid();
    if(valid){
        var datos = $('#form_pago').serialize();
        $.ajax({
          type: "POST",
          url: base_url+"index.php/Ventasi/guardar_pago_tipo",
          data:datos,
          success: function(data){
             $.alert({
                boxWidth: '30%',
                useBootstrap: false,
                title: 'Atención!',
                content: 'Guardador correctamente'}
            );
          }
        });
    }   
}
function finalizaentrega(id,table) {
    var htmlselect=$('#empleados_pre').html();
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Desea concluir esta entrega? <br> <label>Persona a la que se le entrega</label><select id="empleados_select" class="browser-default form-control-bmz">'+htmlselect+'</select>',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                    var emple = $('#empleados_select option:selected').val();
                     $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Ventasi/updatestateentrega",
                        data: {
                            id:id,
                            table:table,
                            empleado:emple
                        },
                        success: function (response){
                                
                                    $.alert({
                                    boxWidth: '30%',
                                    useBootstrap: false,
                                    title: 'Hecho!',
                                    content: 'Estatus actualizado'}); 
                                    load();
                                
                        },
                        error: function(response){
                            $.alert({
                                    boxWidth: '30%',
                                    useBootstrap: false,
                                    title: 'Error!',
                                    content: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema'});   
                             
                        }
                    });
                    
                
            },
            cancelar: function () 
            {
                
            }
        }
    });
}
function finalizaentrega2(id,tipotable) {
    var htmlselect=$('#empleados_pre').html();
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Desea concluir esta entrega? <br> <label>Persona a la que se le entrega</label><select id="empleados_select" class="browser-default form-control-bmz">'+htmlselect+'</select>',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var emple = $('#empleados_select option:selected').val();
                     $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Ventasi/updatestateentrega2",
                        data: {
                            id:id,
                            tipotable:tipotable,
                            empleado:emple
                        },
                        success: function (response){
                                
                                    $.alert({
                                    boxWidth: '30%',
                                    useBootstrap: false,
                                    title: 'Hecho!',
                                    content: 'Estatus actualizado'}); 
                                    load();
                                
                        },
                        error: function(response){
                            $.alert({
                                    boxWidth: '30%',
                                    useBootstrap: false,
                                    title: 'Error!',
                                    content: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema'});   
                             
                        }
                    });
                    
                
            },
            cancelar: function () 
            {
                
            }
        }
    });
}
function verificarmontosvc(){
    var TABLA   = $(".tipo_tabla_venta");
    var DATA  = [];
    TABLA.each(function(){
        item = {};
        item ["idventa"] = $(this).data('idventa');
        item ["combinada"] = $(this).data('ventanc');
        DATA.push(item);
    });
    dataventas   = JSON.stringify(DATA);
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Ventasi/verificarmontoventas",
        data: {
            ventas:dataventas
        },
        success:function(data){  
            console.log(data);
            var array = $.parseJSON(data);
            console.log(array);
            array.forEach(function(element) {
                if(element.monto>0){

                }else{
                    $('.venta_tipo_'+element.combinada+'_'+element.idventa).html('rentas');
                }
            });
        }
    });
}
function generarreporte(){
    var fecha=$('#fecha').val();
    if(fecha==''){
        fecha=$('.btn_entregas_dia').data('fecha');
    }

    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: 'Favor de confirmar la fecha!<br>'+
                 '<input type="date" id="re_fecha" class="name form-control-bmz" value="'+fecha+'" style="width:99%" />',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var new_fecha=$('#re_fecha').val();

                window.open(base_url+'Ventasi/entregashoy/'+new_fecha, '_blank');
                setTimeout(function(){ 
                    pre_pendientes();
                }, 7000);
                
            },
            cancelar: function (){
                
            }
        }
    });
}
function verificardev(){
    var DATAf  = [];
    $(".verificar_dev").each(function() {
        item = {};
        item ["id"] = $(this).data('id');
        item ["tipotabla"] = $(this).data('tipotabla');
        item ["id_rh"] = $(this).data('id_rh');
        item ["renta_rh"] = $(this).data('renta_rh');
        DATAf.push(item);
    });
    aInfo   = JSON.stringify(DATAf);
    //console.log(aInfo);
    var datos=aInfo;
    
    $.ajax({
        type:'POST',
        url: base_url+"Generales/verificardev",
        data: {
            datos:aInfo
        },
        success: function (data){
            console.log(data);
            var array = $.parseJSON(data);
            console.log(array);
            $.each(array, function(index, item) {
                    $(".ver_dev_"+item.id+'_'+item.tipotabla).html(item.mensaje);
                    $(".ver_bod_"+item.id+'_'+item.tipotabla).html(item.bodegas);
                
            });
        }
    });
}
function info_dev(id,tipotabla,id_rh,renta_rh){
    $('#modaldetalledev').modal('open');
    
    $.ajax({
        type:'POST',
        url: base_url+"Generales/verificardev_info",
        data: {
            id:id,
            tipotabla:tipotabla,
            id_rh:id_rh,
            renta_rh:renta_rh
        },
        success: function (data){
            $('.detalles_dev').html(data);
        }
    });
}
function vertecnicov(){
    var TABLAv   = $(".venta_ver_tecnico");
    var DATAv  = [];
    TABLAv.each(function(){
        item = {};
        item ["idventa"] = $(this).data('id');
        item ["tipo"] = $(this).data('tipotabla');
        DATAv.push(item);
    });
    datav   = JSON.stringify(DATAv);
    if(TABLAv.length>0){
        $.ajax({
            type:'POST',
            url: base_url+"index.php/Ventasi/vertecnicov",
            data: {
                ventas:datav
            },
            success:function(data){  
                console.log(data);
                var array = $.parseJSON(data);
                console.log(array);
                array.forEach(function(element) {
                    $('.venta_ver_tecnico_'+element.venta+'_'+element.tipo).html(element.tecnico);
                });
            }
        });
    }
}
function vertecnicovc(){
    var TABLAvc   = $(".ventac_ver_tecnico");
    var DATAvc  = [];
    TABLAvc.each(function(){
        item = {};
        item ["idventa"] = $(this).data('id');
        item ["tipo"] = $(this).data('tipotabla');
        DATAvc.push(item);
    });
    datavc   = JSON.stringify(DATAvc);
    if(TABLAvc.length>0){
        $.ajax({
            type:'POST',
            url: base_url+"index.php/Ventasi/vertecnicovc",
            data: {
                ventas:datavc
            },
            success:function(data){  
                console.log(data);
                var array = $.parseJSON(data);
                console.log(array);
                array.forEach(function(element) {
                    $('.ventac_ver_tecnico_'+element.venta+'_'+element.tipo).html(element.tecnico);
                });
            }
        });
    }
}

function vertecnicorh(){
    var DATArh  = [];
    var TABLArh   = $(".rentash_ver_tecnico");
    TABLArh.each(function() {
        item = {};
        item ["id"] = $(this).data('id');
        item ["tipotabla"] = $(this).data('tipotabla');
        item ["id_rh"] = $(this).data('id_rh');
        item ["renta_rh"] = $(this).data('renta_rh');
        DATArh.push(item);
    });
    aInfo   = JSON.stringify(DATArh);
    
    if(TABLArh.length>0){
        $.ajax({
            type:'POST',
            url: base_url+"Ventasi/vertecnicorh",
            data: {
                ventas:aInfo
            },
            success: function (data){
                console.log(data);
                var array = $.parseJSON(data);
                console.log(array);
                $.each(array, function(index, item) {
                        $('.rentash_ver_tecnico_'+item.venta+'_'+item.tipo).html(item.tecnico);
                    
                });
            }
        });
    }
}
function vertecnicor(){
    var DATArh  = [];
    var TABLArh   = $(".renta_ver_tecnico");
    TABLArh.each(function() {
        item = {};
        item ["id"] = $(this).data('id');
        item ["tipotabla"] = $(this).data('tipotabla');
        DATArh.push(item);
    });
    aInfo   = JSON.stringify(DATArh);
    
    if(TABLArh.length>0){
        $.ajax({
            type:'POST',
            url: base_url+"Ventasi/vertecnicor",
            data: {
                ventas:aInfo
            },
            success: function (data){
                console.log(data);
                var array = $.parseJSON(data);
                console.log(array);
                $.each(array, function(index, item) {
                        $('.renta_ver_tecnico_'+item.venta+'_'+item.tipo).html(item.tecnico);
                    
                });
            }
        });
    }
}
function ejecutaranimacion(){
    //$(".camposanimados h5").removeClass("animate__animated animate__slideInRight");
    //setTimeout(function(){ $(".camposanimados h5").show().addClass("animate__animated animate__slideInRight"); }, 1000);
    $(".camposanimados h6").show().addClass("animate__animated animate__slideInRight");
    
}
function verificarstatusnrdev(){
    var TABLAvc   = $(".verificar_status");

    if(TABLAvc.length>0){
        var DATAvc  = [];
        TABLAvc.each(function(){
            item = {};
            item ["id"] = $(this).data('rentarh');
            item ["idhostorial"] = $(this).data('idrh');
            DATAvc.push(item);
        });
        datavc   = JSON.stringify(DATAvc);
        $.ajax({
            type:'POST',
            url: base_url+"index.php/Ventasi/verificarstatusnrdev",
            data: {
                ventas:datavc
            },
            success:function(data){  
                console.log(data);
                var array = $.parseJSON(data);
                console.log(array);
                array.forEach(function(element) {
                    $('.verificar_status_'+element.id+'_'+element.idhostorial).html('<a class="waves-effect red btn-bmz c_can_ser" >Solicitud de Devolución series '+element.seriesdev+'</a>');
                });
            }
        });
    }
}
function checadopre(tabla,id){
    setTimeout(function(){ 
        var check = $('#press_ed_'+tabla+'_'+id).is(':checked')==true?1:0;
        console.log('check: '+check);
        $.ajax({
                type:'POST',
                url: base_url+"Ventasi/checadopre",
                data: {
                    tabla:tabla,
                    id:id,
                    check:check
                },
                success: function (data){
                    
                }
            });
        pre_pendientes();
    }, 1000);
    
}
function pre_pendientes(){
    $.ajax({
        type:'POST',
        url: base_url+"Ventasi/pre_pendientes",
        data: {
            tabla:0
        },
        success: function (data){
           var cont_p =parseInt(data);
           if(cont_p>0){
            $('.count_pre_pen').html(cont_p);
           }else{
            $('.count_pre_pen').html(0);
           }
        }
    });
}