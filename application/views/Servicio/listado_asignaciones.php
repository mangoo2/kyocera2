<style type="text/css">
  .select2-container{width: 100% !important;}
  #tableaddrefacciones td,#tableaddconsulmibles td{padding: 0px;}
  .obtenerservicios{margin-left: 0px;}
  .chosen-container{margin-left: 0px;}
  .collpadding{padding: 3px !important;}
  #modalservicio2{width: 80% !important;}
  .prioridad_1{background: #63be7b;}
  .prioridad_2{background: #85c87d;}
  .prioridad_3{background: #a8d27f;}
  .prioridad_4{background: #cbdc81;}
  .prioridad_5{background: #ede683;}
  .prioridad_6{background: #ffdd82;}
  .prioridad_7{background: #fdc07c;}
  .prioridad_8{background: #fca377;}
  .prioridad_9{background: #f8696b;}
  .zona_servico{display: none;}
  #modalequipos{
    width: 76%;
  }
  #modalequipos .modal-content{
    padding: 9px;
  }
  #modalequipos td{
    font-size: 12px;
  }
  .ultimosservicios{
    
    /*display: none;*/
  }
  .ultimosservicios_img,.ultimosservicios{
    background: url(<?php echo base_url()?>public/img/garantia.png);
    /*display: none;/* quitar asta que ya sea productivo */
  }
  .iconcot{
    font-size: 12px;
    padding-top: 3.625px;
    padding-bottom: 3.625px;
    padding-left: 7.25px;
    padding-right: 7.25px;
  }
  input{
    margin-bottom: 0px !important;
  }
  .tabla_dir td,#tabledatoscontacto td{
    padding: 5px 5px;
  }
  .ocultar_temp{
    display: none;
  }
</style>
<input type="hidden" id="totalventas" value="<?php echo $serviciosdeventas;?>">
<input type="number" id="cant_ult_ser" value="0" style="display: none;">
<input type="hidden" id="totalpolizas" value="<?php echo $serviciosdepolizas;?>">
<input type="hidden" id="contrsinservicios" value="<?php echo $contrsinservicios->num_rows();?>">
<input type="hidden" id="idpolizaser" value="<?php echo $idpolizaser;?>">
<input type="hidden" id="tipopc" value="<?php echo $tipopc;?>">
<!-- START CONTENT -->
<section id="content">
  <!--start container-->
  <div class="container">
    <div class="section">
      <div class="card-panel">
          <div id="table-datatables">
            <br>
            <h4 class="header">Asignaciones de Servicios</h4>
            <div class="row">
              <div class="col s6"><h4>Seleccionar tipo de asignación:</h4></div>
              <div class="col s6">
                <input name="tasign" type="radio" class="tasign" id="asign1" value="1">
                <label for="asign1">Pre- Asignación</label>
                <input name="tasign" type="radio" class="tasign" id="asign2" value="2">
                <label for="asign2">Asignación del día</label>
              </div>
            </div>
            <div class="row valortipoasignacion"></div>
            <span class="datos_cliente"></span>
            <div class="row">
              <div class="col s12 mostrarsoloclientes" style="display: none;">
                <a class="btn-floating red tooltipped datos ocultar_temp" data-position="top" data-delay="50" data-tooltip="Venta" onclick="venta(0,3)"><i class="fas fa-cart-plus"></i></a>
                <a class="btn-floating red tooltipped fecha_servicio" data-position="top" data-delay="50" data-tooltip="Datos de Entrega" onclick="calendariop2()"><i class="material-icons">date_range</i></a>
                <a class="btn-floating red tooltipped tecnico_servicio" data-position="top" data-delay="50" data-tooltip="Asignar Tecnico" onclick="asig_tec2()"><i class="material-icons">person_add</i></a>
                <a class="btn-floating red tooltipped prioridad_servicio" data-position="top" data-delay="50" data-tooltip="Asignar Prioridad" onclick="prioridad2()"><i class="material-icons">report_problem</i></a>
                <a class="btn-floating red tooltipped zona_servico" data-position="top" data-delay="50" data-tooltip="Ruta" onclick="ruta2()"><i class="material-icons">navigation</i></a>
                <a class="btn-floating red tooltipped condiciones condiciones_servicio" data-position="top" data-delay="50" data-tooltip="Prefactura" onclick="serviciot2()"><i class="fas fa-wrench"></i></a> 
                <a class="btn-floating redd tooltipped  ultimosservicios" data-position="top" data-delay="50" data-tooltip="Servicios a garantia" onclick="ultimosservicios()"></a> 
                <button class="waves-effect cyan btn-bmz generarsolicitudc" onclick="generarsolicitud()">Generar</button>
              </div>
            </div>
            
          </div>
      </div>
    </div>  
  </div>
</section>
<input type="hidden" name="id_cp_f" id="id_cp_f" value="0"><!--id de contrato o id de poliza-->
<input type="hidden" name="id_servicio_f" id="id_servicio_f" value="0"><!--id del registro si es que existe-->
<div id="modalAsigna" class="modal">
    <div class="modal-content ">
      <h4>Asignar Técnico </h4>
      <div class="col s12" style="text-align: center;"><h5>Seleccionar técnico / especialista</h5></div>
      <div class="col s12 m12 l6" style="min-height: 239px;">
          <div class="row" align="center">
             <div class="col s12 m12 l12">
               <label class="col s12 m2 l2">Técnico</label>
               <div class="col s12 m8 l8" >
                 <select name="tecnico" id="tecnico" class="browser-default tecnicoselect form-control-bmz">
                  <?php 
                    foreach ($resultstec as $item) { ?>
                      <option value="<?php echo $item->personalId;?>"><?php echo $item->tecnico;?></option>
                    <?php  } ?>
                   <!--<option>Juan Perez</option>
                   <option>Casimiro Lejos</option>-->
                 </select>
                 <select name="tecnico2" id="tecnico2" class="browser-default form-control-bmz"><option value="0"></option>
                  <?php 
                    foreach ($resultstec as $item) { ?>
                      <option value="<?php echo $item->personalId;?>"><?php echo $item->tecnico;?></option>
                    <?php  } ?>
                   <!--<option>Juan Perez</option>
                   <option>Casimiro Lejos</option>-->
                 </select>
                 <select name="tecnico3" id="tecnico3" class="browser-default form-control-bmz"><option value="0"></option>
                  <?php 
                    foreach ($resultstec as $item) { ?>
                      <option value="<?php echo $item->personalId;?>"><?php echo $item->tecnico;?></option>
                    <?php  } ?>
                   <!--<option>Juan Perez</option>
                   <option>Casimiro Lejos</option>-->
                 </select>
                 <select name="tecnico4" id="tecnico4" class="browser-default form-control-bmz"><option value="0"></option>
                  <?php 
                    foreach ($resultstec as $item) { ?>
                      <option value="<?php echo $item->personalId;?>"><?php echo $item->tecnico;?></option>
                    <?php  } ?>
                   <!--<option>Juan Perez</option>
                   <option>Casimiro Lejos</option>-->
                 </select>
               </div>
             </div> 
          </div>
      </div>  
    <div class="modal-footer">
      <a class="modal-action modal-close waves-effect waves-red gray btn-flat " onclick="savetecnico()">Aceptar</a>
      <a class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
    </div>
  </div>
</div>
<div id="modalRuta" class="modal">
    <div class="modal-content ">
      <h4>Zona</h4>
      <div class="col s12" align="center"><h5>Seleccionar zona</h5></div>
      <div class="col s12 m12 l6">
          <div class="row" align="center">
             <div class="col s12 m12 l12">
               
               <div class="col s12 m12 l12">
                 <select name="ruta" id="ruta" class="browser-default form-control-bmz">
                  <?php foreach ($resultzonas as $item) { ?><option value="<?php echo $item->id;?>"><?php echo $item->nombre;?></option><?php  } ?>
                 </select>
               </div>
             </div> 
          </div>
      </div> 
    <div class="modal-footer">
      <a class="modal-action modal-close waves-effect waves-red gray btn-flat " onclick="savezona()">Aceptar</a>
      <a class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
    </div>
  </div>
</div>
<div id="modalPrioridad" class="modal">
    <div class="modal-content ">
      <h4> <i class="material-icons">mode_edit</i> Asignar Prioridad </h4>
      <div class="col s12 m12 l6">
          <div class="row" align="center">
               <label class="label col s12 m2 l2" style="font-size: 16px;">Tipo servicio</label>
               <div class="col s12 m12 l12">
                <input name="priodidad" type="radio" id="test1" class="soloasgig" value="1" />
                <label for="test1">Servicios Correctivos<!--Prioridad 1--></label>
                <input name="priodidad" type="radio" id="test2" class="soloasgig"  value="2"/>
                <label for="test2">Aseroría - Conexión remota<!--Prioridad 2--></label>
                <input name="priodidad" type="radio" id="test3" class="soloasgig"  value="3"/>
                <label for="test3">Pólizas<!--Prioridad 3--></label>
                <input name="priodidad" type="radio" id="test4" class="solopreasgig" value="4"/>
                <label for="test4">Servicio Regular</label>
                <input name="priodidad" type="radio" id="test5" class="soloasgig" value="5"/>
                <label for="test5">Mensajería</label>
               </div>
               
          </div>
          <div class="row" align="center">
            <div class="col s2"><label>Prioridad</label></div>
            <div class="col s5">
              <select name="priodidad2" id="prioridad2" class="form-control-bmz browser-default" onchange="select_prioridad()">
                <option value="1" class="prioridad_1">Ninguno</option>
                <option value="2" class="prioridad_2">Muy Pequeño</option>
                <option value="3" class="prioridad_3">Pequeño</option>
                <option value="4" class="prioridad_4">Muy Bajo</option>
                <option value="5" class="prioridad_5">Bajo</option>
                <option value="6" class="prioridad_6">Moderado</option>
                <option value="7" class="prioridad_7">Alto</option>
                <option value="8" class="prioridad_8">Muy alto</option>
                <option value="9" class="prioridad_9">Critico</option>
              </select>
            </div>
            <div class="col s2 horaser_cri" style="display:none;"><label>Hora de servicio</label></div>
            <div class="col s3 horaser_cri" style="display:none;">
              
              <input type="time" class="form-control-bmz" id="hora_cri_ser">
            </div>
          </div>
      </div>  
    <div class="modal-footer">
      <a class="modal-action modal-close waves-effect waves-red gray btn-flat " onclick="saveprioridad()">Aceptar</a>
      <a class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
    </div>
  </div>
</div>
<div id="modalDatos" class="modal">
    <div class="modal-content ">
      <h4>Seleccionar fecha y hora</h4>
      <div class="col s12 m12 l6">
          <div class="row" align="center">
            <div class="col s12 m12 l12 infohorario"></div>
          </div>
          <div class="row" align="center">
             <div class="col s12 m12 l12">
               <label class="col s12 m2 l2">Fecha</label>
               <div class="col s12 m8 l8">
                 <input type="date" name="fecha" id="fecha" class="form-control-bmz">
               </div>
             </div>
             <div class="col s6 m6 l6">
               <label class="col s12 m12 l12">Hora Inicio</label>
               <div class="col s12 m12 l12">
                 <input type="time" name="hora" id="hora" class="form-control-bmz">
               </div>
             </div> 
             <div class="col s6 m6 l6">
               <label class="col s12 m12 l12">Hora Fin</label>
               <div class="col s12 m12 l12">
                 <input type="time" name="horafin" id="horafin" class="form-control-bmz">
               </div>
             </div> 
             <div class="col s6 m6 l6">
               <label class="col s12 m12 l12">Hora Inicio Comida</label>
               <div class="col s12 m12 l12">
                 <input type="time" name="hora_comida" id="hora_comida" class="form-control-bmz">
               </div>
             </div> 
             <div class="col s6 m6 l6">
               <label class="col s12 m12 l12">Hora Fin comida</label>
               <div class="col s12 m12 l12">
                 <input type="time" name="horafin_comida" id="horafin_comida" class="form-control-bmz">
               </div>
             </div> 
          </div>
      </div>  
    <div class="modal-footer">
      <a  class="modal-action modal-close waves-effect waves-red gray btn-flat " onclick="savefecha()">Aceptar</a>
      <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
    </div>
  </div>
</div>

<div id="modalOrden" class="modal">
    <div class="modal-content ">
      <h4> <i class="material-icons">settings_applications</i> Crear Orden </h4>
      <hr>
      <div class="col s12 m12 l6">
          <div class="row" align="center">
             <h5>Desea crear la orden de servicio 146?</h5>
             <input type="hidden" name="num_orden" id="num_orden" value="146">
          </div>
      </div> 
    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat" id="conf_crea">Crear</a>
      <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat">Cancelar</a>
    </div>
  </div>
</div>
<div id="modalservicio" class="modal">
    <div class="modal-content ">
      <h4> <i class="material-icons">settings_applications</i> Seleccionar tipo de servicio</h4>
      <hr>
      <div class="col s12 m12 l6">
        <div class="row">
          <label for="tpoliza">Poliza</label>
          <select id="tpoliza" class="browser-default chosen-select form-control-bmz" onchange="obtenerservicios();">
            <?php foreach ($resultpolizas->result() as $item) { ?>
                <option value="<?php echo $item->id;?>"><?php echo $item->nombre;?></option>
            <?php } ?>
          </select>
        </div>
        <div class="row">
          <label for="tservicio">Equipo / Servicio</label>
          <select id="tservicio" class="browser-default chosen-select form-control-bmz"></select>
        </div>
        <div class="row">
          <div class="col s9">
            <input name="ttipo" type="radio" class="ttipo1" id="ttipo1"  value="1" >
            <label for="ttipo1">Local</label>
            <input name="ttipo" type="radio" class="ttipo2" id="ttipo2"  value="2" >
            <label for="ttipo2">Semi</label>
            <input name="ttipo" type="radio" class="ttipo3" id="ttipo3"  value="3" >
            <label for="ttipo3">Foráneo</label>
            <input name="ttipo" type="radio" class="ttipo4" id="ttipo4"  value="4" >
            <label for="ttipo4">Especial</label>
          </div>
          <div class="col s3 visordecosto"></div>
        </div>
        <div class="row">
          <label for="tserviciomotivo">Motivo del Servicio</label>
          <textarea id="tserviciomotivo" ></textarea>
        </div>
        <div class="row ncequiposs" style="display: none;">
          <label for="documentaccesorio">documentos y accesorios</label>
          <textarea id="documentaccesorio" ></textarea>
        </div>
          
      </div> 
    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat" onclick="savemotivos()">Aceptar</a>
      <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat">Cancelar</a>
    </div>
  </div>
</div>

<div id="modalequipos" class="modal">
    <div class="modal-content ">
      <h4> <i class="material-icons">settings_applications</i>Equipos</h4>
      <hr>
      
      <div class="col s12 m12 l6 liberacionequipos"></div>
      <div class="col s12 m12 l6">
        <div class="row equipospolizacontrato">
          
        </div>
      </div> 
    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat">Cerrar</a>
    </div>
  </div>
</div>
<div id="modalventa" class="modal">
    <div class="modal-content " style=" padding-left: 0px; padding-right: 0px;">
      <h4> <i class="material-icons">settings_applications</i>Venta</h4>
      <hr>
      <dir class="row" style="padding: 0px">
        <div class="col s12 m12 l12">
          <ul class="collapsible collapsible-accordion" data-collapsible="accordion">
            <li class="">
              <div class="collapsible-header">
                <i class="material-icons">subtitles</i>Consumibles</div>
              <div class="collapsible-body collpadding" >
                <!---------------------------------------->
                  <div class="row">
                    <div class="col s12 m3 l3">
                      <label>Cantidad</label>
                      <input type="number" name="cantidadc" id="cantidadc" class="form-control-bmz" value="1" min="1">
                    </div>
                    <div class="col s12 m4 l4">
                      <label>Consumible</label>
                      <select name="sconcumibles" id="sconcumibles" class="browser-default form-control-bmz sconcumibles chosen-select" onchange="funcionConsumiblesprecios($(this))">
                        <option></option>
                        <?php foreach ($consumiblesresult->result() as $item) { ?><option value="<?php echo $item->id;?>"><?php echo $item->modelo;?> (<?php echo $item->parte;?>)</option><?php } ?>
                      </select>
                    </div>
                    <div class="col s12 m3 l3">
                      <label>Precio</label>
                      <select name="sconcumiblesp" id="sconcumiblesp" class="browser-default form-control-bmz"></select>
                    </div>
                    <div class="col s12 m2 l2">
                      <button class="waves-effect cyan btn-bmz addconsu" onclick="addconsu()">Agregar</button>
                    </div>
                  </div>
                  <div class="row ">
                    <table class="table striped" id="tableaddconsulmibles"><thead><tr><th>Cantidad</th><th>consumible</th><th>Precio</th><th></th></tr></thead><tbody class="tabaddconsul"></tbody></table>
                    <table class="table striped" id="tableaddconsulmiblescf"><tbody class="tabaddconsulcf"> </tbody></table>
                  </div>
                <!---------------------------------------->
              </div>
            </li>
            <li class="">
              <div class="collapsible-header">
                <i class="material-icons">subtitles</i>Refacciones</div>
              <div class="collapsible-body collpadding" >
                <!----------------------------------------->
                  <div class="row">
                    <div class="col s12 m3 l3">
                      <label>Cantidad</label>
                      <input type="number" name="cantidadr" id="cantidadr" class="form-control-bmz" value="1" min="1">
                    </div>
                    <div class="col s12 m4 l4">
                      <label>Refaccion</label>
                      <select name="srefaccions" id="srefaccions" class="browser-default form-control-bmz chosen-select" onchange="selectprecior()">
                        <option></option>
                        <?php foreach ($refaccionesresult->result() as $item) { ?><option value="<?php echo $item->id;?>"><?php echo $item->nombre;?></option><?php } ?>
                      </select>
                    </div>
                    <div class="col s12 m3 l3">
                      <label>Precio</label>
                      <select name="srefaccionp" id="srefaccionp" class="browser-default form-control-bmz"></select>
                    </div>
                    <div class="col s12 m2 l2">
                      <button class="waves-effect cyan btn-bmz addrefa"  onclick="addrefa()">Agregar</button>
                    </div>
                  </div>
                  <div class="row ">
                    <table class="table striped" id="tableaddrefacciones"><thead><tr><th>Cantidad</th><th>Refacción</th><th>Precio</th><th></th></tr></thead><tbody class="taaddrefaccion"> </tbody></table>
                  </div>
                <!----------------------------------------->
              </div>
            </li>
            <li>
              <div class="collapsible-header"><i class="material-icons">subtitles</i>Accesorios</div>
              <div class="collapsible-body collpadding" >
                <!---------------------------------------------->
                  <div class="row">
                      <div class="col s12 m3 l3">
                        <label>Cantidad</label>
                        <input type="number" name="cantidada" id="cantidada" class="form-control-bmz" value="1" min="1">
                      </div>
                      <div class="col s12 m4 l4">
                        <label>Accesorio</label>
                        <select name="saccesorios" id="saccesorios" class="browser-default form-control-bmz chosen-select" onchange="selectprecioa()">
                          <option></option>
                          <?php foreach ($accesoriosresult->result() as $item) { ?><option value="<?php echo $item->id;?>" data-costo="<?php echo $item->costo;?>"><?php echo $item->nombre;?></option><?php } ?>
                        </select>
                      </div>
                      <div class="col s12 m3 l3">
                        <label>Precio</label>
                        <input type="number" name="saccesoriop" id="saccesoriop" class="browser-default form-control-bmz" >
                      </div>
                      <div class="col s12 m2 l2">
                        <button class="waves-effect cyan btn-bmz addacces"  onclick="addacces()">Agregar</button>
                      </div>
                  </div>
                  <div class="row ">
                    <table class="table striped" id="tableaddaccesorios"><thead><tr><th>Cantidad</th><th>Accesorio</th><th>Precio</th><th></th></tr></thead><tbody class="taaddaccesorios"></tbody></table>
                  </div>
                <!---------------------------------------------->
              </div>
            </li>
          </ul>
        </div>  
      </dir>
      <div class="col s12 m12 l6">
        <div class="row">
          <div class="col s12 m8 l8"></div>
          <div class="col s12 m3 l3">
            <p>Total: <b>$<span class="totalg">0.00</span></b></p>
          </div>
        </div>
      </div> 
      <div class="modal-footer">
         <!--
          <a class=" waves-effect waves-red gray btn-flat">Edicion</a>
          <a class=" waves-effect waves-red gray btn-flat">Cancelar pedido</a>-->
          <a class="modal-action modal-close waves-effect waves-red gray btn-flat">Cerrar</a>
          
      </div>
    </div>
</div>


<div id="modalservicio2" class="modal">
    <div class="modal-content ">
      <h4> <i class="material-icons">settings_applications</i> Seleccionar tipo de servicio</h4>
      <hr>
      <div class="row">
        <div class="col s1 m1 l1">
          <a class="btn-bmz material-icons prefix" onclick="addserieshisto()" title="Equipos registrados" style="margin-top: 18px;">format_list_bulleted</a>
        </div>
        <div class="col s2 m2 l2">
          <label for="tpoliza2">Póliza</label>
          <select id="tpoliza2" class="browser-default chosen-select form-control-bmz" onchange="obtenerservicios2();">
            <?php foreach ($resultpolizas->result() as $item) { ?>
                <option value="<?php echo $item->id;?>"><?php echo $item->nombre;?></option>
            <?php } ?>
          </select>
        </div>
        <div class="col s2 m2 l2">
          <label for="tservicio2">Equipo / Servicio</label>
            <select id="tservicio2" class="browser-default chosen-select form-control-bmz"></select>
        </div>
        <div class="col s2 m2 l2">
          <label for="tserie2">Serie</label>
          <input type="text" name="tserie2" id="tserie2" class="form-control-bmz">
        </div>
        <div class="col s2 m2 l2">
          <label for="tdireccion2">Dirección</label>
          <select id="tdireccion2" name="tdireccion2" class="browser-default  form-control-bmz"></select>
        </div>
        <div class="col s2 m2 l2">
          <label class="ttipo2">Tipo Servicio</label>
          <select class="browser-default  form-control-bmz" name="ttipo22" id="ttipo22">
            <option value="1">Local</option><option value="2">Semi</option><option value="3">Foráneo</option><option value="4">Especial</option>
          </select>
        </div>
        <div class="col s1 m1 l1">
          <a class="btn-floating waves-effect waves-light purple lightrn-1 btn tooltipped" data-position="top" data-tooltip="Agregar"  onclick="addeveservi()"><i class="material-icons">system_update_alt</i></a>
        </div>
      </div>
      <div class="row">
        <div class="col s12 m12 l12">
          <table class="striped" id="tableserviciosevento">
            <thead>
              <tr><th>Poliza</th><th>Equipo/Servicio</th><th>Serie</th><th>Dirección</th><th>Tipo</th><th></th></tr>
            </thead>
            <tbody class="ttableserviciosevento"></tbody>
          </table>
        </div>
      </div>
      <div class="col s12 m12 l6">
        <div class="row">
          <label for="tserviciomotivo2">Motivo del Servicio</label>
          <textarea id="tserviciomotivo2" ></textarea>
        </div>
        <div class="row ncequiposs" style="display: none;">
          <label for="documentaccesorio2">documentos y accesorios</label>
          <textarea id="documentaccesorio2" ></textarea>
        </div>
      </div> 
    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat" onclick="savemotivos()">Aceptar</a>
      <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat">Cancelar</a>
    </div>
  </div>
</div>

<div id="modalinfoventa" class="modal">
  <div class="modal-content ">
    <h4> <i class="material-icons">settings_applications</i> Información del servicio</h4>
    <hr>
    <div class="row infoventaiframe"></div>
    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat">Cancelar</a>
    </div>
  </div>
</div>
<div id="modamodificarservicio" class="modal">
  <div class="modal-content ">
    <div class="row ventaservicio">
        <div class="col s3 m3 l3">
          <label for="tpoliza3">Póliza</label>
          <select id="tpoliza3" class="browser-default chosen-select form-control-bmz" onchange="obtenerservicios3();">
            <?php foreach ($resultpolizas->result() as $item) { ?><option value="<?php echo $item->id;?>"><?php echo $item->nombre;?></option><?php } ?>
          </select>
        </div>
        <div class="col s3 m3 l3">
          <label for="tservicio3">Equipo / Servicio</label>
            <select id="tservicio3" class="browser-default chosen-select form-control-bmz"></select>
        </div>
        <div class="col s4 m4 l4">
          <label for="tdireccion3">Dirección</label>
          <select id="tdireccion3" class="browser-default  form-control-bmz"></select>
        </div>
        <div class="col s2 m2 l2">
          <label class="ttipo2">Tipo Servicio</label>
          <select class="browser-default  form-control-bmz" name="ttipo3" id="ttipo33">
            <option value="1">Local</option><option value="2">Semi</option><option value="3">Foráneo</option><option value="4">Especial</option>
          </select>
        </div>
    </div>
    <div class="row ventaservicio">
      <label for="tserviciomotivo3">Motivo del Servicio</label>
      <textarea id="tserviciomotivo3"></textarea>
    </div>                 
  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-action  waves-effect waves-green green btn-flat ventaserviciook " onclick="ventaserviciook()">Aceptar</a>
  </div>
</div>

<div id="modalserieshistorial" class="modal" style="min-height: 497px;">
  <div class="modal-content " style="padding-top: 10px;">
    <div class="row">
      <div class="col s12 m12 12 table-h-e-s"></div>                
    </div>                 
  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat">Cerrar</a>
  </div>
</div>
<div id="modal_view_agenda" class="modal">
  <div class="modal-content " >
    <div class="row">
      <div class="col s12 m12 12 view_agenda_html"></div>                
    </div>                 
  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat">Cerrar</a>
  </div>
</div>




<div id="modalserviciosanteriores" class="modal">
    <div class="modal-content ">
      <h4> <i class="material-icons">settings_applications</i> Últimos servicios</h4>
      <hr>
      <div class="row ">
        <div class="col s12">
        <div class="row">
          <div class="col s12">
            <div class="switch">
              <label>
                Todo
                <input id="montrar_s_2" type="checkbox" checked onclick="filtrarseries()">
                <span class="lever"></span>
                Solo las series
              </label>
            </div>
          </div>
        </div>
      </div>
        <div class="col s12">
          <table class="table striped bordered" id="table_servicios_ult">
            <thead>
              <tr>
                <th>#</th><th>Tipo</th><th></th><th>Técnico</th><th>Fecha</th><th></th>
              </tr>
            </thead>
            <tbody class="view_servicios_10"></tbody>
          </table>
        </div>
      </div>
      
    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat">Cancelar</a>
    </div>
  </div>
</div>