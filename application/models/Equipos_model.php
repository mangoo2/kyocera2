<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class Equipos_model extends CI_Model 
{
    public function __construct() 
    {
        parent::__construct();
        $this->DB19 = $this->load->database('other19_db', TRUE);
        $this->DB12 = $this->load->database('other12_db', TRUE);
    }

    public function getListadoEquipos()
    {
        $sql = "SELECT
                equipos.id,
                equipos.foto,
                equipos.modelo,
                equipos.especificaciones,
                equipos.especificaciones_tecnicas,
                categoria_equipos.nombre,
                equipos.stock,
                equipos.paginaweb,
                equipos.destacado
                FROM equipos 
                LEFT JOIN categoria_equipos ON categoria_equipos.id=equipos.categoriaId
                WHERE equipos.estatus=1";
        $query = $this->DB19->query($sql);
        return $query->result();
    }
    public function getEquipoPorId($idEquipo)
    {
        $sql = "SELECT * FROM equipos
                WHERE id=".$idEquipo;
        $query = $this->DB12->query($sql);
        return $query->row();
    }
    public function insertar_equipo($dato)
    {
        $this->db->insert('equipos',$dato);   
        return $this->db->insert_id();
    }
    public function insertar_accesorio($dato)
    {
        $this->db->insert('accesorios',$dato);   
        return $this->db->insert_id();
    }
    public function insertarToCatalogo($data, $catalogo) {
    return $this->db->insert('' . $catalogo, $data);
    }
    public function getListadoAcesoriosEquipo($idEquipo,$bodega){
        if($bodega>0){
            $whereb=" and sacc.bodegaId='$bodega' ";
        }else{
            $whereb='';
        }
        $sql = "SELECT a.id,c.nombre,c.no_parte, c.costo,a.idEquipo,a.idcatalogo_accesorio,
                        (select count(*) from  series_accesorios as sacc where sacc.accesoriosid=a.idcatalogo_accesorio and sacc.status<2 and sacc.con_serie=1 and sacc.activo=1 and sacc.resguardar_cant=0 $whereb) as stock,
                        (select sum(sacc.cantidad-sacc.resguardar_cant) from  series_accesorios as sacc where sacc.accesoriosid=a.idcatalogo_accesorio and sacc.status<2 and sacc.con_serie=0 and sacc.activo=1 $whereb) as stock2
                FROM accesorios as a
                left join catalogo_accesorios as c on c.id = a.idcatalogo_accesorio
                WHERE idEquipo=$idEquipo and c.status=1";
                $query = $this->db->query($sql);
        return $query->result();
    }
    public function eliminar_accesorio($id) {
    $this->db->where('id', $id);
    return $this->db->delete('accesorios');
    }
    public function update_equipo($data, $id) {
    $this->db->set($data);
    $this->db->where('id', $id);
    $this->db->update('equipos');
    return $id;
    }
    public function update_foto($data, $id) {
    $this->db->set($data);
    $this->db->where('id', $id);
    return $this->db->update('equipos');
    }
    //Obtiene todos los accesorios de la vista de configuraciones 
    public function getAccesorios()
    {
        $sql = "SELECT * FROM catalogo_accesorios WHERE status=1";
        $query = $this->DB12->query($sql);
        return $query->result();
    }
    function getDataEquipo(){
    $sql = "SELECT * FROM categoria_equipos WHERE status=1";
    $query = $this->DB19->query($sql);
    return $query->result();
    } 
    public function truncateTable($table)
    {
        $this->db->truncate($table);
    }
    public function getListaPreciosEquipo()
    {
        $sql = "SELECT
                e.id as 'DT_RowId',
                e.modelo,
                e.precioDolares,
                e.costo_pesos,
                e.costo_venta,
                e.costo_renta,
                e.costo_poliza,
                e.costo_revendedor,
                e.descuento,
                e.porcentaje_ganancia,
                ce.nombre AS categoria
                FROM equipos as e
                LEFT JOIN categoria_equipos as ce ON e.categoriaId=ce.id WHERE e.estatus=1
";
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function getPreciosEquipoPorId($idEquipo)
    {
        $sql = "SELECT
                e.id as 'DT_RowId', e.modelo, e.precioDolares, e.costo_pesos, e.costo_venta,
                e.costo_renta, e.costo_poliza, 
                e.costo_revendedor, e.descuento, e.porcentaje_ganancia, ce.nombre as 'categoria'
                FROM equipos as e
                LEFT JOIN categoria_equipos as ce ON e.categoriaId=ce.id WHERE e.id=".$idEquipo;
        $query = $this->DB12->query($sql);
        return $query->result();
    }
    public function getTipoCambioEquipos()
    {
        $sql = "SELECT *
                FROM tiposCambio WHERE categoria='equipos'";
        $query = $this->DB19->query($sql);
        return $query->result();
    }
    public function getPorcentajeGananciaEquipos()
    {
        $sql = "SELECT *
                FROM porcentajeGanancia WHERE categoria='equipos'";
        $query = $this->DB12->query($sql);
        return $query->result();
    }
    public function getDescuentosEquipos()
    {
        $sql = "SELECT *
                FROM descuentosCategorias WHERE categoria='equipos'";
        $query = $this->DB19->query($sql);
        return $query->result();
    }
    function getDataFamilia(){
    $sql = "SELECT * FROM familia WHERE status=1";
    $query = $this->DB12->query($sql);
    return $query->result();
    }

    public function getEquiposParaSelect()
    {
        $sql = "SELECT
                equipos.id,
                equipos.modelo
                FROM equipos 
                WHERE equipos.estatus=1";
        $query = $this->DB19->query($sql);
        return $query->result();
    }

    public function getEquiposPorFamilia($idFamilia)
    {
        $sql = "SELECT * FROM equipos
                WHERE idFamilia = ".$idFamilia." AND estatus=1";
        $query = $this->DB12->query($sql);
        return $query->result();
    }
}