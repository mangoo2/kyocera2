<style type="text/css">
  .kv-file-upload{
    display: none;
  }
  .error{
    color: red;
  }
</style>
<!-- START CONTENT --> 
<section id="content" width="100%">
  <!--start container-->
  <div class="container" width="100%">
    <div class="section">
      <h4 class="caption">Configuración de facturación unidades servicios default.</h4>
      <div class="row">
        <div class="col s12">
          <ul class="collapsible collapsible-accordion" data-collapsible="accordion">
            <li class="active">
              <div class="collapsible-header active">
                Servicios</div>
              <div class="collapsible-body" style="display: block;">
                <!------------------------------------------------------------------>
                <table class="table" id="tableus">
                  <tbody>
                    
                          <?php 
                            foreach ($usdefault->result() as $item) { ?>
                              <tr>
                                <td>
                              <div class="row">
                                <div class="col s12">
                                  <input type="hidden" name="id" id="id" value="<?php echo $item->id;?>">
                                  <h4><?php echo $item->tipo;?></h4>
                                </div>
                                <div class="col s3">
                                  <label>Unidad SAT</label>
                                  <select name="unidadsat" id="unidadsat" class="browser-default unidaddefault">
                                    <option value="<?php echo $item->unidadsat;?>"><?php echo $item->unidadsat_text;?></option>
                                  </select>
                                </div>
                                <div class="col s5">
                                  <label>Concepto SAT </label>
                                  <select name="conseptosat" id="conceptosat" class="browser-default conceptodefault">
                                    <option value="<?php echo $item->conceptosat;?>"><?php echo $item->conceptosat_text;?></option>
                                  </select>
                                </div>
                              </div>
                              </td>
                    </tr
                          <?php } ?>
                    
                        
                      >
                  </tbody>
                </table>
                <div class="row">
                  <div class="input-field col s9">
                  </div>
                  <div class="input-field col s3">
                    <button class="btn cyan waves-effect waves-light right serviciodefault_save" type="button" style="margin-top: 10px;">Guardar<i class="material-icons right">save</i></button>
                  </div>
                </div>
                <!------------------------------------------------------------------>
                
              </div>
            </li>
          </ul>
        </div>
       

          
        
      </div>
    </div>
  </div>
</section>