<?php 
	if(isset($_GET['viewexcel'])){
		$viewexcel=1;
		$tiempo_inicio = microtime(true);
	}else{
		header("Pragma: public");
		header("Expires: 0");
		$filename = "reporte.xls";
		header("Content-type: application/x-msdownload");
		header("Content-Disposition: attachment; filename=$filename");
		header("Pragma: no-cache");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		$viewexcel=0;
	}
	date_default_timezone_set('America/Mexico_City');
    $this->fechahoy = date('Y-m-d G:i:s');
    $this->fechaactual = date('Y-m-d');
    $this->horaactual = date('G:i:s');
    
    echo $tabla;


    if($viewexcel==1){
		// Fin del cronómetro
		$tiempo_fin = microtime(true);
		$tiempo_total = $tiempo_fin - $tiempo_inicio;

		echo "El script tardó " . $tiempo_total . " segundos en ejecutarse.";
	}
?>