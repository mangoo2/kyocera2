var base_url = $('#base_url').val();
var tablec;
var perfilid = $('#perfilid').val();
$(document).ready(function() {
	tablec = $('#tabla_complementos').DataTable();
	//loadtable_c();
    $('#idclientec').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una opción',
        allowClear: true,
        ajax: {
            url: base_url+'Ventasi/searchclientes',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.id,
                        text: element.empresa

                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function (e) {
        //var data = e.params.data;
        //obtenerrfc(data.id)
        //loadtable();
        loadtable_c();
    });
});
function loadtable_c(){
    var inicio=$('#finicial_c').val();
    var fin=$('#ffinal_c').val();
    var personals=$('#personalsc option:selected').val();
    var idcliente=$('#idclientec option:selected').val()==undefined?0:$('#idclientec option:selected').val();
    var rs_foliosc=$('#rs_foliosc option:selected').val();
	tablec.destroy();
	tablec = $('#tabla_complementos').DataTable({
        pagingType: 'input',
        dom: 'Blfrtip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'pdfHtml5',

        
        ],
        search: {
                return: true
            }, 
        //stateSave: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url+"index.php/Facturaslis/getlistcomplementos",
            type: "post",
            "data": {
                'finicio':inicio,
                'ffin':fin,
                'personal':personals,
                'idcliente':idcliente,
                'rs_foliosc':rs_foliosc
            },
        },
        "columns": [
            {"data": "complementoId",
            	render:function(data,type,row){
            		var html='';
            		if(row.Estado==1){
                        html='<input type="checkbox" class="filled-in complementos_checked" id="complemento_'+row.complementoId+'" value="'+row.complementoId+'">\
				              <label for="complemento_'+row.complementoId+'"></label>';
                    }else{
                    	html='<input type="checkbox" class="filled-in complementos_checked" id="complemento_'+row.complementoId+'" value="'+row.complementoId+'" disabled>\
				              <label for="complemento_'+row.complementoId+'"></label>';
                    }
                    return html;
            	}
        	},
            {"data": "R_nombre"},
            {"data": "folios",},
            {"data": "Monto",
                render:function(data,type,row){
                    var html='';
                        html=new Intl.NumberFormat('es-MX').format(row.Monto);
                    return html;
                }
            },
            {"data": "Estado",
                render:function(data,type,row){
                    var html='';
                    if(row.Estado==0){
                        html='Cancelada';
                        html+='<div class="validarcancelacion '+row.uuid+'" data-uuid="'+row.uuid+'" style="color:red"></div>';
                    }else if(row.Estado==1){
                        html='Timbrado';
                    }else if(row.Estado==2){
                        html='Sin Timbrar';
                    }else{
                    	html='';
                    }
                    return html;
                }
            },
            {"data": "fechatimbre"},
            {"data": null,
                render:function(data,type,row){
                    var html='';
                        if(row.nombre==null){
                            html+='';
                        }else{
                            html+=row.nombre+' ';
                        }
                        if(row.apellido_paterno==null){
                            html+='';
                        }else{
                            html+=row.apellido_paterno+' ';
                        }
                        if(row.apellido_materno==null){
                            html+='';
                        }else{
                            html+=row.apellido_materno+' ';
                        }

                    
                    return html;
                }
            },
            {"data": null,
                 render:function(data,type,row){
                    $('.tooltipped').tooltip();
                    var html='<div class="btns-factura">';
                    if (row.Estado==0) {//cancelado
                    	html+='<a class="b-btn b-btn-primary tooltipped" href="'+base_url+'Facturaslis/complementodoc/'+row.complementoId+'" target="_blank" data-position="top" data-delay="50" data-tooltip="XML" title="PDF">\
                                <i class="fas fa-file-pdf"></i>\
                                </a> ';
                        html+='<a class="b-btn b-btn-primary tooltipped" href="'+base_url+'kyocera/facturas/'+row.rutaXml+'" target="_blank" data-position="top" data-delay="50" data-tooltip="XML" download="" title="XML">\
                        		<i class="far fa-file-code"></i></a> ';
                        html+='<a class="b-btn b-btn-primary tooltipped" href="'+base_url+'kyocera/facturas/'+row.rutaAcuseCancelacion+'" target="_blank" data-position="top" data-delay="50" data-tooltip="Acuse de cancelación" download="" title="Acuse de cancelación">\
                        		<i class="far fa-file-code" style="color: #ef7e4f;"></i></a> ';

                        html+='<a\
                                    class="b-btn b-btn-primary tooltipped insertaracuse_'+row.complementoId+'_1 " \
                                    onclick="insertaracuse('+row.complementoId+',1)" \
                                    data-docacuse="'+row.doc_acuse_cancelacion+'" \
                                    data-position="top" data-delay="50" data-tooltip="subir Acuse de cancelación" title="subir Acuse de cancelación" \
                                  download ><i class="fas fa-file-pdf" style="color: #ef7e4f;"></i>\
                                    </a> ';
                        if(perfilid==1){
                            html+='<a\
                                    class="b-btn b-btn-primary tooltipped" \
                                    onclick="regresarcomplementoavigente('+row.complementoId+')"  \
                                    data-position="top" data-delay="50" title="Regresar a vigente" data-tooltip="Regresar a vigente"\
                                   ><i class="material-icons dp48" style="color: #ef7e4f;">replay</i>\
                                    </a> ';
                        }

                    }
                    if (row.Estado==1) {//
                    	html+='<a class="b-btn b-btn-primary tooltipped" href="'+base_url+'Facturaslis/complementodoc/'+row.complementoId+'" target="_blank" data-position="top" data-delay="50" data-tooltip="XML" title="PDF">\
                                <i class="fas fa-file-pdf "></i>\
                                </a> ';
                        html+='<a class="b-btn b-btn-primary tooltipped" href="'+base_url+''+row.rutaXml+'" target="_blank" data-position="top" data-delay="50" data-tooltip="XML" download="" title="XML">\
                        		<i class="far fa-file-code "></i></a> ';
                    }
                    if (row.Estado==2) {
                    	html+='<button type="button"\
                          class="b-btn b-btn-primary tooltipped btn-retimbrar" \
                          onclick="retimbrarcomplemento('+row.complementoId+')"\
                          data-position="top" data-delay="50" data-tooltip="Retimbrar" id="refacturar">\
                          <span class="fa-stack">\
                          <i class="fas fa-file fa-stack-2x"></i>\
                          <i class="fas fa-sync fa-stack-1x" style="color: red;"></i>\
                          </span>\
                        </button> ';
                    }
                    if (row.Estado==1) {
                        if(row.envio_m_p>0){
                            var envmail='enviado tooltipped';
                            var envmailt=' title="'+row.per_envio+' '+row.envio_m_d+'" data-position="top" data-delay="50" data-tooltip="'+row.per_envio+' '+row.envio_m_d+'"';
                        }else{
                            var envmail='';
                            var envmailt='';
                        }
                        html+=' <button type="button" class="b-btn b-btn-primary email '+envmail+'" '+envmailt+' onclick="enviarmailcompl('+row.complementoId+','+row.clienteId+')"><i class="fas fa-mail-bulk"></i></button>'; 
                    }
                    if(perfilid==1){
                            html+='<a class="b-btn b-btn-primary tooltipped" onclick="cambiostatuscomplemento('+row.complementoId+','+row.Estado+')" data-position="top" data-delay="50" title="Regresar a vigente" data-tooltip="Cambio de estatus"><i class="fas fa-exchange-alt"></i></a> ';
                    }
                    if (row.Estado==1) {
                        html+=' <a  onclick="refacturacion_comp('+row.complementoId+')" class="b-btn b-btn-primary"  title="Refacturar para cancelacion"><i class="fas fa-file-prescription"></i></a>';
                    }
                    html+='</div>';
                    return html;
                }
            }
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[25, 50, 100], [25, 50,100]]
        
    }).on('draw',function(){
        verificarcancelacion();
    });
}
function cancelarcomplementos(){
	var complementoslis = $("#tabla_complementos tbody > tr");
	var DATAc  = [];
	var num_complemento_selected=0;
	var c_complementoId=0;
    complementoslis.each(function(){  
    	if ($(this).find("input[class*='complementos_checked']").is(':checked')) {
    		num_complemento_selected++;
    		                  
        	c_complementoId  = $(this).find("input[class*='complementos_checked']").val();
        	
    	}       
        
    });
    if (num_complemento_selected==1) {
    	//console.log(c_complementoId);
    	cancelarcomplemento(c_complementoId);
    	setTimeout(function(){ 
    		$('#contrasena').val('');
    	 }, 1000);
    	
    }else{
    	alertfunction('Atención!','Seleccione solo un complemento');
    }
}
//===============================================================================
var codigocarga = $('#codigocarga').val();
function enviarmailcompl(idfac,cliente){
    //var cliente = $('#idCliente').val();
    obtenercontactoc(cliente);
    $('.input_envio_s').html('');
    $('.table_mcr_files_s').html('');
    $('.addbtnservicio').html('');
    $('.input_envio_s_c').html('<input type="file" id="files" name="files[]" multiple accept="*">');
    $('.table_mcr_files_s_c').html('<table class="table bordered striped" id="table_mcr_files"><tbody class="tabbodycr_files"></tbody></table>');
    $('.addbtnservicio_c').html('<button type="button" class="waves-effect btn-bmz  red btn-envioser" onclick="enviarmailcomp_mail('+idfac+')">Enviar</button>');
    $('.iframefac_c').html('<iframe src="'+base_url+'Facturaslis/complementodoc/'+idfac+'?savefac=1">');
    inputfileadd();

    $('#modal_enviomailcomp').modal();
    $('#modal_enviomailcomp').modal('open');
}
function obtenercontactoc(cliente){
    $('.m_s_contac_c').html('');
    $('.m_s_contac').html('');
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Calendario/obtenercontactos2",
        data: {
            cliente:cliente
        },
        success: function (response){
            $('.m_s_contac_c').html('<select class="browser-default form-control-bmz" id="ms_contacto" multiple style="min-height: 72px;" title="Selector multiple, seleccione las opciones con Ctrl+ click">'+response+'</select>');
                
        },
        error: function(response){
            notificacion(1);
             
        }
    });
    //=====================================================================
    
}

function enviarmailcomp_mail(idfac){
    var email =$('#ms_contacto').val();
    $.ajax({
        type:'POST',
        url: base_url+'index.php/Mailenvio/envio_comp',
        data: {
            idfac:idfac,
            code:codigocarga,
            email:JSON.stringify(email),
            asunto:$('#ms_asucto2_c').val(),
            comen:$('#ms_coment2_c').val(),
            per:$('#firma_m_c option:selected').val(),
            perbbc:$('#bbc_m_c option:selected').val()
        },
        success:function(data){
            console.log(data);
            var array = $.parseJSON(data);
            codigocarga=array.newcodigo;
            $('#codigocarga').val(codigocarga);
            toastr["success"]("Correo enviado");
            $('#modal_enviomailcomp').modal('close');
            $('#ms_coment2_c').val('');

        }
    });
}
function regresarcomplementoavigente(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Confirma de regresar a vigente?<br>Se necesita permisos de administrador<br><input type="password" placeholder="Contraseña" id="contrasena" class="name form-control"/>',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var pass=$('#contrasena').val();
                if (pass!='') {
                    $.ajax({
                        type:'POST',
                        url: base_url+"Generales/regresarcomplementoavigente",
                        data: {
                            idf:id,
                            pass:pass
                        },
                        success: function (data){
                            if(parseInt(data)==1){
                                toastr["success"]("Cambio Realizado");  
                                loadtable_c();  
                            }else{
                                toastr["error"]("No se tiene permiso"); 
                            }
                        }
                    });
                }else{
                    $.alert({
                                    boxWidth: '30%',
                                    useBootstrap: false,
                                    title: 'Advertencia!',
                                    content: 'Ingrese una contraseña'}); 
                }
            },
            cancelar: function () 
            {
                
            }
        }
    });
}
function cambiostatuscomplemento(id,est){
        var checkedfac1='';
        var checkedfac0='';
        if(est==1){
            checkedfac1=' checked ';
        }
        if(est==0){
            checkedfac0=' checked ';
        }
    var html='';
        html+='¿Confirma el cambio de estatus del complemento?<br>Se necesita permisos de administrador<br><input type="text" placeholder="Contraseña" id="contrasenaa" name="contrasenaa" class="name form-control"/>';
        html+='<div class="row"><div class="col s6">\
                <input name="tasign" type="radio" class="tasign" id="asign1" value="1" '+checkedfac1+'>\
                <label for="asign1">Timbrada</label>\
                <input name="tasign" type="radio" class="tasign" id="asign2" value="0" '+checkedfac0+'>\
                <label for="asign2">Cancelada</label>\
              </div></div>';
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: html,
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                //var pass=$('#contrasena').val();
                var pass=$("input[name=contrasenaa]").val();
                var estatus=$('input:radio[name=tasign]:checked').val()
                if (pass!='') {
                    $.ajax({
                        type:'POST',
                        url: base_url+"Generales/cambiostatuscomplemento",
                        data: {
                            idf:id,
                            pass:pass,
                            estatus:estatus
                        },
                        success: function (data){
                            if(parseInt(data)==1){
                                toastr["success"]("Cambio Realizado");  
                                loadtable_c();  
                            }else{
                                toastr["error"]("No se tiene permiso"); 
                            }
                        }
                    });
                }else{
                    notificacion(0);
                }
            },
            cancelar: function () 
            {
                
            }
        }
    });
    setTimeout(function(){
            new MaskedPassword(document.getElementById("contrasenaa"), '\u25CF');
    },900);
}
function refacturacion_comp(id){
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Facturaslis/infocomplemento",
        data: {
            idc:id
        },
        success: function (response){
            var array = $.parseJSON(response); 
            console.log(array);     
            $(location).attr('href',base_url+'Facturaslis/complemento/'+array.compdr.facturasId+'?idcomp='+id+'&uuid='+array.comp.uuid+'&tr=04');
            //href="'+base_url+'Facturaslis/add/0?idfac='+row.complementoId+'&uuid='+row.uuid+'&tr=04"          
        },
        error: function(response){
            notificacion(1);
        }
    });
}