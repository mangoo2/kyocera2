var base_url = $('#base_url').val();
var tablef_s;
$(document).ready(function() {	
	tablef_s = $('#tabla_facturacion_save').DataTable();
	//loadtable_save();
 	select2_idcliente_save();
    
    idcliente_rfc_save();
    $('.filtrosdatatable_s').change(function(event) {
        tablef_s.state.clear();
    });
});
function loadtable_save(){
    var personals=$('#personals_save option:selected').val();
    var idcliente=$('#idcliente_save option:selected').val()==undefined?0:$('#idcliente_save option:selected').val();
    var idclienterfc=$('#idcliente_rfc_save option:selected').val()==undefined?0:$('#idcliente_rfc_save option:selected').val();
    var metodopago=$('#rs_metodopago_save option:selected').val()==undefined?0:$('#rs_metodopago_save option:selected').val();
    var tcfdi=$('#rs_tcfdi_save option:selected').val()==undefined?0:$('#rs_tcfdi_save option:selected').val();
    var pensav=$('#rs_pendsave_save option:selected').val();
    var inicio=$('#finicial_save').val();
    var fin=$('#ffinal_save').val();
    var rs_folios=$('#rs_folios_save option:selected').val();
	tablef_s.destroy();
	tablef_s = $('#tabla_facturacion_save').DataTable({
        pagingType: 'input',
        dom: 'Blfrtip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'pdfHtml5',
        ],
        search: {
                return: true
            }, 
        //stateSave: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url+"index.php/Facturaslis/getlistfacturas",
            type: "post",
            "data": {
                'personal':personals,
                'cliente':idcliente,
                'finicio':inicio,
                'ffin':fin,
                'idclienterfc':idclienterfc,
                'rs_folios':rs_folios,
                'metodopago':metodopago,
                'tcfdi':tcfdi,
                'pensav':pensav
            },
        },
        "columns": [
            {"data": "FacturasId",
            	render:function(data,type,row){
            		var html='';
            		if(row.Estado==1){
                        html='<input type="checkbox" class="filled-in facturas_checked" id="factura_'+row.FacturasId+'" value="'+row.FacturasId+'" data-clienteid="'+row.Clientes_ClientesId+'">\
				              <label for="factura_'+row.FacturasId+'"></label>';
                    }else{
                    	html='<input type="checkbox" class="filled-in facturas_checked" id="factura_'+row.FacturasId+'" value="'+row.FacturasId+'" disabled>\
				              <label for="factura_'+row.FacturasId+'"></label>';
                    }
                    return html;
            	}
        	},
            {"data": "Folio",
                render:function(data,type,row){
                    var html ='';
                        var serie=row.cadenaoriginal;
                        serie=serie.split('|');
                        if (serie[3]==undefined) {
                            var seriev='';
                        }else{
                            var seriev=serie[3];
                        }
                        html=seriev+''+row.Folio.padStart(4, 0);
                        html=row.Folio;
                    return html;
                }
            },
            {"data": null,
                render:function(data,type,row){
                    var html='';
                    if(row.Rfc=='XAXX010101000'){ 
                        html=row.Nombre+'<br>'+row.empresa;
                    }else{
                        html=row.Nombre;
                    }
                    return html;
                }
            },
            {"data": "Rfc",},
            {"data": "total",
                render:function(data,type,row){
                    var html='';
                        
                    html=new Intl.NumberFormat('es-MX',{style: "currency", currency: "MXN"}).format(row.total);
                    return html;
                }
            },
            {"data": "Estado",
                render:function(data,type,row){
                    var html='';
                    if(row.Estado==0){
                        html='Cancelada';
                        html+='<div class="validarcancelacion '+row.uuid+'" data-uuid="'+row.uuid+'" style="color:red"></div>';
                    }else if(row.Estado==1){
                        html='Facturado';
                        if(row.fechatimbre=='0000-00-00 00:00:00'){
                            html='Sin Facturar';
                        }
                    }else if(row.Estado==2){
                        html='Sin Facturar';
                    }else{
                    	html='';
                    }
                    return html;
                }
            },
            {"data": "fechatimbre"},
            {"data": "FormaPago"},
            {"data": null,
                render:function(data,type,row){
                    var html='';
                        html=row.nombre+' '+row.apellido_paterno+' '+row.apellido_materno;
                    return html;
                }
            },
            {"data": null,
                render:function(data,type,row){
                    var html='Factura';
                    
                    if(row.f_relacion==1 && row.f_r_tipo=='01'){
                        html='Factura 01 Nota Credito';
                    }
                    if(row.f_relacion==1 && row.f_r_tipo=='04'){
                        html='Factura 04 Sustitucion';
                    }
                    if(row.f_relacion==0 && row.f_r_tipo=='07'){
                        html='Factura de Anticipo recibido';
                    }
                    if(row.f_relacion==1 && row.f_r_tipo=='07' && row.TipoComprobante=='I'){
                        html='Factura con relacion de Anticipo';
                    }
                    if(row.f_relacion==1 && row.f_r_tipo=='07' && row.TipoComprobante=='E'){
                        html='Factura Egreso de Anticipo';
                    }

                        
                    return html;
                }
            },
            {"data": null,
                 render:function(data,type,row){
                    
                    var htmlx='';
                    if(row.program==1){
                    	htmlx+='<b>'+row.program_date+'</b>';
                    }
                    	htmlx+='<span class="b-btn b-btn-primary btn_edit_i btn-sm" onclick="edit_program('+row.FacturasId+')" style="min-width: 33px;padding-left: 0px !important;float: inline-end">';
                        //htmlx+='<i class="fas fa-pencil-alt"></i>';
                        htmlx+='<span class="fa-stack">';
                          htmlx+='<i class="fas fa-clock "></i>';
                          htmlx+='<i class="fas fa-pen fa-stack-1x icon_seg_2" style="margin-left: 16px;"></i>';
                        htmlx+='</span>';
                        htmlx+='</span>';
                    
                    return htmlx;
                }
            },
            {"data": null,
                 render:function(data,type,row){
                    $('.tooltipped').tooltip();
                    var html='<div class="btns-factura">';
                    var estadofactura=row.Estado;
                    if(estadofactura==1){
                        if(row.fechatimbre=='0000-00-00 00:00:00'){
                            estadofactura=2;
                        } 
                    }
                   

                    
                        
		                
                                
                            
                        
                    
                   
                    if(row.Folio==0){
                        html+=' <a class="waves-effect red btn-bmz" onclick="eliminarfac('+row.FacturasId+')"><i class="fas fa-trash"></i></a>';
                    }
                     html+='<a \
                                class="b-btn b-btn-primary tooltipped" \
                                href="'+base_url+'Configuracionrentas/generarfacturas/'+row.FacturasId+'/1" \
                                target="_blank"\
                                data-facturasid="'+row.FacturasId+'" data-motofactura="'+row.total+'"\
                                data-position="top" data-delay="50" data-tooltip="Preview de Factura"\
                              ><i class="fas fa-file-alt"></i>\
                            </a> ';
                    
                    html+='</div>';
                    return html;
                }
            }
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[20,25, 50, 100,200,400,-1], [20,25, 50,100,200,400,'todo']],
        //"lengthMenu": [[25, 50, 100,200,400], [25, 50,100,200,400]],
        //"searching": false
        columnDefs: [ 
        	{ orderable: false, targets: [8,9] },
        	{
                targets: [5,6], // Número de la columna que quieres ocultar, 0 es la primera columna
                visible: false // Establece visible en false para ocultarla
            }
        	],
        
    }).on('draw',function(){
        $('.dataTables_filter').append('<i class="fa fa-search buttonsearch" onclick="searchtable_vf_s()"></i>');
    });
}
function select2_idcliente_save(){
    $('#idcliente_save').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una opción',
        allowClear: true,
        ajax: {
            url: base_url+'Ventasi/searchclientes',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    var html='';
                    if(element.empresa==element.razon_social){
                        html=element.empresa;
                    }else{
                        html=element.empresa+' ('+element.razon_social+')';
                    }
                    itemscli.push({
                        id: element.id,
                        text: html

                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function (e) {
        var data = e.params.data;
        //obtenerrfc(data.id)
        //loadtable();
    });
}
function searchtable_vf_s(){
    var searchv =$("#tabla_facturacion_save_filter input[type=search]").val();
    tablef_s.search(searchv).draw();
}
function idcliente_rfc_save(){
   $('#idcliente_rfc_save').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una opción',
        allowClear: true,
        ajax: {
            url: base_url+'index.php/Facturaslis/obtenerrfc3',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.rfc,
                        text: element.rfc+' ('+element.razon_social+')'

                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function (e) {
        //var data = e.params.data;
        //obtenerrfc(data.id)
        //loadtable();
    }); 
}
function edit_program(idfac){
	$.confirm({
        boxWidth: '40%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Editar fecha',
        content: '¿Desea editar la fecha de programacion de timbre?<br><input type="date" id="program_date" class="form-control-bmz">',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var date=$('#program_date').val();
                $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Facturaslis/edit_program",
                    data: {
                        idfac:idfac,
                        date:date
                    },
                    success: function (response){
                            toastr["success"]("Cambio Realizado");  
                            loadtable_save();
                        
                    },
                    error: function(response){
                        toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema"); 
                         
                    }
                }); 
            },
            cancelar: function (){
                
            }
        }
    });
}