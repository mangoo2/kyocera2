<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Clientes extends CI_Controller {
    public function __construct()     {
        parent::__construct();
        $this->load->model('Clientes_model', 'model');
        $this->load->model('Estados_model', 'estados');
        $this->load->model('ModeloCatalogos');
        $this->load->model('Ventas_model');
        $this->load->helper('url');
        $this->idpersonal = $this->session->userdata('idpersonal');
        $this->perfilid =$this->session->userdata('perfilid');
        $this->version = date('YmdHi');
        $this->fechahoyc = date('Y-m-d H:i:s');
        $this->submenu=2;
        $this->fechavencimiento = date("Y-m-d",strtotime($this->fechahoyc."+ 1 month")); 
    }

    // Listado de Empleados
    function index()     {
        $data['MenusubId']=$this->submenu;
        $this->ModeloCatalogos->view_session_bodegas0();
        if($this->session->userdata('logeado')==true){
            $data['perfilid'] = $this->perfilid;
            // Obtiene los menús disponibles del sistema dependiendo el perfil, ayudado de la libreria "MENUS"
            //$data['familia'] = $this->model->getDataFamilia();
            $data['version'] = $this->version;
            $this->load->view('header');
            $this->load->view('main',$data);
            $this->load->view('clientes/listado');
            $this->load->view('clientes/listado_js');
            $this->load->view('footerl');
        }
        else
        {
            redirect('login');
        }
    }
    // Vista del Alta de Clientes
    function alta()    {
        $data['MenusubId']=$this->submenu;
        if($this->session->userdata('logeado')==true)
        {
            $data['idCliente']=0;
            // Obtiene los menús disponibles del sistema dependiendo el perfil, ayudado de la libreria "MENUS"
            $data['datoscontacto']=$this->ModeloCatalogos->db13_getselectwheren('cliente_datoscontacto',array('clienteId'=>0,'activo'=>1));
            $data['tipoVista'] = 1;
            $data['estados'] = $this->estados->getEstados();
            $data['cliente_dire'] = array();
            $data['version'] = $this->version;
            $data['datosequipos']=$this->ModeloCatalogos->db13_getselectwheren('equipos',array('estatus'=>1));
            $data['equiposclienterow']=$this->ModeloCatalogos->cliente_has_equipo_historial(0);
            $data['idpersonal']=$this->idpersonal;
            $data['perfilid']=$this->perfilid;
            $data['fechavencimiento']=$this->fechavencimiento;
            $this->load->view('header');
            $this->load->view('main',$data);
            $this->load->view('clientes/form_clientes');
            //$this->load->view('panel');
            $this->load->view('clientes/form_clientes_js');
            $this->load->view('footer');
        }
        else
        {
            redirect('login');
        }
    }
    // Vista de la Edición de Clientes
    function edicion($idCliente)    { 
    $data['MenusubId']=$this->submenu;  
        if($this->session->userdata('logeado')==true)
        {
            $data['idCliente']=$idCliente;
            // Obtiene los menús disponibles del sistema dependiendo el perfil, ayudado de la libreria "MENUS"
            $data['datoscontacto']=$this->ModeloCatalogos->getselectwheren('cliente_datoscontacto',array('clienteId'=>$idCliente,'activo'=>1));
            $data['cliente'] = $this->model->getClientePorId($idCliente);
            $data['cliente_dire'] = $this->model->getClientedirecPorId($idCliente);
            $data['tipoVista'] = 2;
            $data['estados'] = $this->estados->getEstados();
            $data['version'] = $this->version;
            $data['datosequipos']=$this->ModeloCatalogos->db13_getselectwheren('equipos',array('estatus'=>1));
            $data['equiposclienterow']=$this->ModeloCatalogos->cliente_has_equipo_historial($idCliente);
            $data['idpersonal']=$this->idpersonal;
            $data['perfilid']=$this->perfilid;
            $data['fechavencimiento']=$this->fechavencimiento;
            $this->load->view('header');
            $this->load->view('main',$data);
            $this->load->view('clientes/form_clientes');
            //$this->load->view('panel');
            $this->load->view('clientes/form_clientes_js');
            $this->load->view('footer');
        }
        else
        {
            redirect('login');
        }  
    }
    // Vista de la Visualización de Clientes
    function visualizar($idCliente)     {
        $data['MenusubId']=$this->submenu;
        if($this->session->userdata('logeado')==true)
        {
            // Obtiene los menús disponibles del sistema dependiendo el perfil, ayudado de la libreria "MENUS"
            $data['datoscontacto']=$this->ModeloCatalogos->getselectwheren('cliente_datoscontacto',array('clienteId'=>$idCliente,'activo'=>1));
            $data['estados'] = $this->estados->getEstados();
            $data['cliente'] = $this->model->getClientePorId($idCliente);
            $data['cliente_dire'] = $this->model->getClientedirecPorId($idCliente);
            $data['tipoVista'] = 3;
            $data['version'] = $this->version;
            $data['datosequipos']=$this->ModeloCatalogos->db13_getselectwheren('equipos',array('estatus'=>1));
            $data['equiposclienterow']=$this->ModeloCatalogos->cliente_has_equipo_historial($idCliente);
            $data['idpersonal']=$this->idpersonal;
            $data['idCliente']=$idCliente;
            $data['perfilid']=$this->perfilid;
            $data['fechavencimiento']=$this->fechavencimiento;
            $this->load->view('header');
            $this->load->view('main',$data);
            $this->load->view('clientes/form_clientes');
            //$this->load->view('panel');
            $this->load->view('clientes/form_clientes_js');
            $this->load->view('footer');
        }
        else
        {
            redirect('login');
        }
    }
    // Eliminar cliente
    function eliminarClientes($idCliente)     {
        $datosCliente = array('estatus' => 0);
        $eliminado = $this->model->update_cliente($datosCliente, $idCliente);
        $this->ModeloCatalogos->updateCatalogo('cliente_has_datos_fiscales',array('activo'=>0),array('idCliente'=>$idCliente));
        echo $eliminado;
        //$this->Modelobitacoras->Insert(array('contenido'=>'Se Elimino Cliente','nombretabla'=>'clientes','idtable'=>$idCliente,'tipo'=>'Delete','personalId'=>$this->idpersonal));
    }
    public function insertaActualizaClientes()    {
        $result = 0; 
        $data = $this->input->post();
        //$telefono_local = $this->input->post('telefono_local');
        //$telefono_celular = $this->input->post('telefono_celular');
        $persona_contacto = $this->input->post('persona_contacto');
        $datos_fiscales = $this->input->post('datos_fiscales');
        $datos_direccion = $this->input->post('datos_direccion');
        $datos_equipos = $this->input->post('datos_equipos');
        $antiguedad = $this->input->post('antiguedad');
        $aaa = $this->input->post('aaa');
        $soc = $this->input->post('soc');
        $bloqueo = $this->input->post('bloqueo');
        $credito = $this->input->post('credito');
        $aaa_x = 0;
        $horario_disponible = $this->input->post('horario_disponible');
        $equipo_acceso = $this->input->post('equipo_acceso');
        $documentacion_acceso = $this->input->post('documentacion_acceso');
        // Ordenamos los datos principales del cliente
        $datosCliente = array(
                            'empresa' => $data["empresa"],
                         //   'persona_contacto' => $data["persona_contacto"],
                            //'email' => $data["email"],
                            'estado' => $data["estado"],
                            'municipio' => $data["municipio"],
                            'giro' => $data["giro"],
                            'observaciones' => $data["observaciones"],
                            //'planta_sucursal' => $data['planta_sucursal'],
                            //'planta_sucursal_dir' => $data['planta_sucursal_dir'],
                            //'atencionpara' => $data['atencionpara'],
                            //'puesto_contacto'=>$data['puesto_contacto'],
                            'condicion'=>$data['condicion'],
                            'tipo' => 1,
                            'estatus' => 1,
                            'antiguedad'=>$antiguedad,
                            'aaa'=>$aaa,
                            'bloqueo'=>$bloqueo,
                            'horario_disponible'=>$horario_disponible,
                            'equipo_acceso'=>$equipo_acceso,
                            'documentacion_acceso'=>$documentacion_acceso,
                            'credito'=>$credito,
                            'fisicamoral'=>$data["fm"],
                            'fm_confirmado'=>1,
                            'sol_orden_com'=>$soc
                            );
        if($data['mrow']>0){
            if($data['mcorreo']==''){
                $data['mcorreo']=null;
            }else{
                $datosCliente['correo']=$data['mcorreo'];
            }
            if($data['mpass']=='xxxxx'){
                //$data['m_correo']=null;
            }else{
                $pass=$data['mpass'];
                $datosCliente['ssap_i']=$pass;
                $pass=password_hash($pass, PASSWORD_BCRYPT);

                $datosCliente['pass']=$pass;
            }
        }

        if(!isset($data['idCliente'])) {
            $datosCliente['idUsuario']=$_SESSION["usuarioid"];
            $datosCliente['fm_confirm_personal']=$this->idpersonal;
            $id = $this->model->insertar_cliente($datosCliente); 
            $result = $id;
            //$this->Modelobitacoras->Insert(array('contenido'=>'Se Inserto Cliente','nombretabla'=>'clientes','idtable'=>$id,'tipo'=>'Insert','personalId'=>$this->idpersonal));
            $this->ModeloCatalogos->updateuuid($id,5);
            $tipo=1;      
        }
        else
        {
           $id=$data['idCliente'];
           unset($data['idCliente']);
           if($bloqueo==1){
            $datosCliente['bloqueo_reg']=$this->fechahoyc;
           }
           $result = $id;
           $this->model->update_cliente($datosCliente,$id); 
           //$this->Modelobitacoras->Insert(array('contenido'=>'Se Actualizo Cliente','nombretabla'=>'clientes','idtable'=>$id,'tipo'=>'Update','personalId'=>$this->idpersonal)); 
           $tipo=0;
        }
            
            $DATA_CONTACTO = json_decode($persona_contacto);
                
            for ($i = 0; $i < count($DATA_CONTACTO); $i++) {
                $datosId=$DATA_CONTACTO[$i]->datosId;
                $dir=$DATA_CONTACTO[$i]->dir;
                if ($DATA_CONTACTO[$i]->datosId==0) {

                    $puesto=$DATA_CONTACTO[$i]->puesto;
                    $puesto = str_replace("---", "&", $puesto);
                    
                    $temp=array(
                        'clienteId'=>$id,
                        'atencionpara'=>$DATA_CONTACTO[$i]->atencionpara,
                        'puesto'=>$puesto,
                        'email'=>$DATA_CONTACTO[$i]->email,
                        'telefono'=>$DATA_CONTACTO[$i]->telefono,
                        'celular'=>$DATA_CONTACTO[$i]->celular,
                        'dir'=>$dir
                    );
                    $this->ModeloCatalogos->Insert('cliente_datoscontacto',$temp);
                }else{
                    $this->ModeloCatalogos->updateCatalogo('cliente_datoscontacto',array('dir'=>$dir),array('datosId'=>$datosId));
                }
            }
            
            $DATA_FISCALES = json_decode($datos_fiscales);
                $caract   = array('----','****','....','ñ','°','á','é','í','ó','ú','  ');
                $caract2  = array('&','+','"','n','','a','e','i','o','u',' ');
                $caractb   = array('----','****','....','ñ','°','á','é','í','ó','ú',' ');
                $caractb2  = array('&','+','"','n','','a','e','i','o','u','');

                for ($i = 0; $i < count($DATA_FISCALES); $i++) {
                    $idf = $DATA_FISCALES[$i]->id;
                    //log_message('error',$DATA_FISCALES[$i]->razon_social);
                    $razon_social = strval(str_replace($caract,$caract2,$DATA_FISCALES[$i]->razon_social));
                    //log_message('error',$razon_social);
                    $rfc = strval(str_replace($caractb,$caractb2,$DATA_FISCALES[$i]->rfc));
                    $cp = str_replace(' ','',$DATA_FISCALES[$i]->cp);
                    $calle = $DATA_FISCALES[$i]->calle;
                    $num_ext = $DATA_FISCALES[$i]->num_ext;
                    $num_int = $DATA_FISCALES[$i]->num_int;
                    $colonia = $DATA_FISCALES[$i]->colonia;

                    $estado = $DATA_FISCALES[$i]->estado;
                    $municipio= $DATA_FISCALES[$i]->municipio;
                    $localidad= $DATA_FISCALES[$i]->localidad;
                    $RegimenFiscalReceptor=$DATA_FISCALES[$i]->RegimenFiscalReceptor;
                        if($rfc=='XAXX010101000'){
                            //$razon_social='PUBLICO EN GENERAL';
                            $RegimenFiscalReceptor=616;
                        }
                        if($razon_social!=''){   
                            $idarray = array('id' => $idf);    
                            $temp=array(
                                "razon_social"=>$razon_social,
                                "rfc"=>$rfc,
                                "cp"=>$cp,
                                "calle"=>$calle,
                                "num_ext"=>$num_ext,
                                "num_int"=>$num_int,
                                "colonia"=>$colonia,
                                "estado" =>$estado,
                                "municipio" =>$municipio,
                                "localidad" =>$localidad,
                                "RegimenFiscalReceptor"=>$RegimenFiscalReceptor,
                                "idCliente"=>$id
                            );
                            if($idf==0){
                                if($razon_social!='' and $rfc!='' ){
                                    $this->model->insertarToCatalogo($temp,"cliente_has_datos_fiscales");
                                }
                            }else{
                                $this->ModeloCatalogos->updateCatalogo("cliente_has_datos_fiscales",$temp,$idarray);
                            }
                            
                        }
                }
            $datos_direcc = json_decode($datos_direccion);

                for ($i = 0; $i < count($datos_direcc); $i++) {
                    $idclientedirecc = $datos_direcc[$i]->id;
                    $direccion = $datos_direcc[$i]->direccion;
                    $pro_acc = $datos_direcc[$i]->pro_acc;
                    if(isset($datos_direcc[$i]->longitud)){
                        $longitud = $datos_direcc[$i]->longitud;
                    }else{
                        $longitud='';
                    }
                    if (isset($datos_direcc[$i]->latitud)) {
                        $latitud = $datos_direcc[$i]->latitud;
                    }else{
                        $latitud='';
                    }
                    
                    
                        if($direccion!=''){   
                            $idarrayd = array('idclientedirecc' => $idclientedirecc);    
                            $tempd=array(
                                "idcliente"=>$id,
                                "direccion"=>$direccion,
                                'pro_acc'=>$pro_acc,
                                'longitud'=>$longitud,
                                'latitud'=>$latitud
                            );
                            if($idclientedirecc==0){
                                $this->model->insertarToCatalogo($tempd,"clientes_direccion");
                            }else{
                                $this->ModeloCatalogos->updateCatalogo("clientes_direccion",$tempd,$idarrayd);
                            }
                            
                        }
                }
            $datos_equipos = json_decode($datos_equipos);
                $dataarrayinsert_ch=array();
                for ($j = 0; $j < count($datos_equipos); $j++) {

                    $cliequId = $datos_equipos[$j]->id;
                    $modelo = $datos_equipos[$j]->modelo;
                    $serie = $datos_equipos[$j]->serie;
                    $clienteId =$id;

                    if($cliequId>0){

                    }else{
                        $dataarrayinsert_ch[]=array('clienteId'=>$clienteId,'modelo'=>$modelo,'serie'=>$serie);
                        //$this->ModeloCatalogos->Insert('cliente_has_equipo_historial',array('clienteId'=>$clienteId,'modelo'=>$modelo,'serie'=>$serie));
                    }
                }
                if(count($dataarrayinsert_ch)>0){
                    $this->ModeloCatalogos->insert_batch('cliente_has_equipo_historial',$dataarrayinsert_ch);
                }
           $arrayresult=array('result'=>$result,'tipo'=>$tipo);
        echo json_encode($arrayresult);;  
    }
    public function getListadoClientes(){
        $clientes = $this->model->getListadoClientes();
        $json_data = array("data" => $clientes);
        echo json_encode($json_data);
    }
    /*
    public function getListadoClientesPorUsuario($idUsuario){
        if($idUsuario==1)
        {   
            $clientes = $this->model->getListadoClientes();
        }
        else
        {
            $clientes = $this->model->getListadoClientesPorUsuario($idUsuario);
        }
        $json_data = array("data" => $clientes);
        echo json_encode($json_data);
    }*/
    public function getListadoClientesPorUsuario_asic($idUsuario) {
        $params = $this->input->post();
        $params['idUsuario']=$idUsuario;
        $productos = $this->model->getListadoClientesPorUsuario_asic($params);
       $totalRecords=$this->model->getListadoClientesPorUsuario_asict($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totalRecords),  
            "recordsFiltered" => intval($totalRecords),
            "data"            => $productos->result(),
            "query"           =>$this->db->last_query()   
        );
        
        echo json_encode($json_data);
    }
    // lisado delas tablas del form de cliente
    function getListadoTel_local($idTelefono)    {
        $tel_local = $this->model->getListadoTel_local($idTelefono);
        $json_data = array("data" => $tel_local);
        echo json_encode($json_data);
    }
    function getListadoTel_celular($idTelefono)    {
        $tel_celular = $this->model->getListadoTel_celular($idTelefono);
        $json_data = array("data" => $tel_celular);
        echo json_encode($json_data);
    }
    function getListadoPersona_contacto($idContacto)    {
        $contacto = $this->model->getListadoPersona_contacto($idContacto);
        $json_data = array("data" => $contacto);
        echo json_encode($json_data);
    }
    function eliminar_tel_local($id_tel)     {
        $eliminado = $this->model->eliminar_tel_local($id_tel);
        echo $eliminado;
    }
    function eliminar_tel_celular($id_tel)     {
        $eliminado = $this->model->eliminar_tel_celular($id_tel);
        echo $eliminado;
    }
    function eliminar_persona_contacto($id_contacto)     {
        $this->ModeloCatalogos->updateCatalogo('cliente_datoscontacto',array('activo'=>0),array('datosId'=>$id_contacto));
        echo 1;
    }
    /*
    function getListadoDatosFiscales($idCliente)
    {
        $contacto = $this->model->getListadoDatosFiscales($idCliente);
        $json_data = array("data" => $contacto);
        echo json_encode($json_data);
    }
    */

    function eliminar_datos_fiscales($idDatos)     {
        $idarray = array('id' => $idDatos);
        $datos = array('activo' => 0);
        $this->ModeloCatalogos->updateCatalogo("cliente_has_datos_fiscales",$datos,$idarray);
    }
    function delete_direccion($id){
        $idarray = array('idclientedirecc' => $id);
        $datos = array('status' => 0);
        $this->ModeloCatalogos->updateCatalogo("clientes_direccion",$datos,$idarray);
    }
    function obtenerdatoscliente(){
        $cliente = $this->input->post('cliente');
        $resultcli=$this->ModeloCatalogos->getselectwheren('clientes',array('id' => $cliente));
        $resultcli=$resultcli->result();
        echo json_encode($resultcli[0]);
    }
    function rfcexiste(){
        $data = $this->input->post();
        $cli = $data['cli'];
        $rfc = $data['vrfc'];
        $rfcrow=0;
        $html='';
        if($rfc!='XAXX010101000' and $rfc!='SEP210905778' and $rfc!='GE98501011S6' and $rfc!='GEP8501011S6' and $rfc!='USE9205217S8'){
            //$resultcli=$this->ModeloCatalogos->getselectwheren('cliente_has_datos_fiscales',array('rfc' => $rfc,'activo'=>1,'idCliente !=' =>$cli));
            
            $strq = "SELECT cli.empresa,clifi.* from 
                     cliente_has_datos_fiscales as clifi 
                     inner join clientes as cli on cli.id=clifi.idCliente
                     WHERE 
                     clifi.rfc='$rfc' and 
                     clifi.activo=1 and 
                     clifi.idCliente!='$cli'";
            $resultcli = $this->db->query($strq);
            $html.='<thead><tr><th>Cliente</th><th>Datos</th><th>ir</th></tr></thead>';
            foreach ($resultcli->result() as $item) {
                $html.='<tr><td>'.$item->empresa.'</td>';
                                $html.='<td class="datosf_'.$item->id.'"';
                                    $html.=' data-razon="'.$item->razon_social.'" ';
                                    $html.=' data-rfc="'.$item->rfc.'" ';
                                    $html.=' data-cp="'.$item->cp.'" ';
                                    $html.=' data-estado="'.$item->estado.'" ';
                                    $html.=' data-municipio="'.$item->municipio.'" ';
                                    $html.=' data-localidad="'.$item->localidad.'" ';
                                    $html.=' data-calle="'.$item->calle.'" ';
                                    $html.=' data-num_ext="'.$item->num_ext.'" ';
                                    $html.=' data-num_int="'.$item->num_int.'" ';
                                    $html.=' data-rfr="'.$item->RegimenFiscalReceptor.'" ';
                                    $html.=' data-colonia="'.$item->colonia.'" ';
                                    $html.='>';
                                    $html.='<p style="margin:0px;"><b>Razon Social:</b>'.$item->razon_social.'</p>';
                                    $html.='<p style="margin:0px;"><b>RFC:</b>'.$item->rfc.'</p>';
                                    $html.='<p style="margin:0px;"><b>Codigo postal:</b>'.$item->cp.'</p>';
                                    $html.='<p style="margin:0px;"><b>Direccion:</b>'.$item->calle.' '.$item->num_ext.' '.$item->num_int.' '.$item->colonia.' '.$item->localidad.' '.$item->municipio.' '.$item->estado.'</p>';
                                $html.='</td>';
                                $html.='<td>';
                                    $html.='<a href="'.base_url().'Clientes/edicion/'.$item->idCliente.'" target="_blank">';
                                        $html.='<button class="b-btn b-btn-primary tooltipped" data-position="top" data-tooltip="Ir" title="IR">';
                                            $html.='<i class="fas fa-reply"></i>';
                                        $html.='</button>';
                                    $html.='</a>';
                                    $html.='<button class="b-btn b-btn-primary tooltipped" data-position="top" title="Usar Estos Datos" onclick="usardatosf('.$item->id.')">';
                                            $html.='<i class="far fa-clipboard"></i>';
                                        $html.='</button>';
                            $html.='</tr>';
               $rfcrow++;
            }
        }
        $datos=array('resul'=>$rfcrow,'datos'=>$html);
        echo json_encode($datos);
    }
    function clientexls(){
        
        header("Pragma: public");
        header("Expires:0");
        header("Cache-Control:must-revalidate,post-check=0, pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Type: application/vnd.ms-excel;");
        header("Content-Disposition: attachment; filename=clientes.xls");
        
        $html='<table border="1">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Empresa</th>
                        <th>Municipio</th>
                        <th>Giro empresa</th>
                        <th>Observaciones</th>
                    </tr>
                </thead>
                <tbody>';
            $this->db->select('c.*');
            $this->db->from('clientes AS c');
            $this->db->join('usuarios u', 'c.idUsuario = u.UsuarioID');
            $this->db->join('personal p', 'u.personalId = p.personalId');
            $this->db->where('c.tipo',1);
            $this->db->where('c.estatus',1);
            $result=$this->db->get();

            //$result=$this->ModeloCatalogos->getselectwheren('clientes',array('tipo'=>1,'estatus'=>1));
            foreach ($result->result() as $item) {
                $html.='<tr>
                            <td>'.$item->id.'</td>
                            <td>'.$item->empresa.'</td>
                            <td>'.$item->municipio.'</td>
                            <td>'.$item->giro.'</td>
                            <td>'.$item->observaciones.'</td>
                        </tr>';
            }
        $html.='</tbody></table>';
        echo $html;
    }
    function existecliente(){
            $data = $this->input->post();
            $nombre = $data['nombre'];
            $idCliente = $data['idCliente'];
            /*
            $this->db->select('c.*');
            $this->db->from('clientes AS c');
            $this->db->where('c.tipo',1);
            $this->db->where('c.estatus',1);
             $this->db->where('c.id !=',$idCliente);
            $this->db->like('empresa', $nombre);
            $result=$this->db->get();
            */
            $sql = "SELECT * FROM clientes WHERE tipo=1 and estatus=1 and id!='$idCliente' and empresa like '%$nombre%'";
            $query = $this->db->query($sql);

        echo json_encode($query->result());
    }
    function deleteequipo(){
        $params = $this->input->post();
        $id=$params['id'];
        $this->ModeloCatalogos->updateCatalogo('cliente_has_equipo_historial',array('activo'=>0),array('cliequId'=>$id));
    }
    function search_cot(){
        $cot=$this->input->post('cot');
        $sql = "SELECT 
                    cot.id,cot.idCliente,cli.empresa,cot.atencion,cot.correo,cot.telefono 
                FROM cotizaciones as cot 
                INNER JOIN clientes as cli on cli.id=cot.idCliente 
                WHERE cot.id='$cot'";
        $query = $this->db->query($sql);
        if($query->num_rows()==0){
            $html='No se encontro registro';
        }else{
           $html='<table class="table table-striped bordered">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Cliente</th>
                            <th>Atención</th>
                            <th>Correo</th>
                            <th>Teléfono</th>
                            <th>SubTotal</th>
                            <th>Iva</th>
                            <th>Total</th>
                            <th></th>
                        <tr></thead>';
            foreach ($query->result() as $item) {
                $subtotal=$this->ModeloCatalogos->subtotal_cotizacion($item->id);
                $iva=$subtotal*0.16;
                $total=$subtotal+$iva;
                $html.='<tr>
                            <td>'.$item->id.'</td>
                            <td>'.$item->empresa.'</td>
                            <td>'.$item->atencion.'</td>
                            <td>'.$item->correo.'</td>
                            <td>'.$item->telefono.'</td>
                            <td>$ '.number_format($subtotal,2,'.',',').'</td>
                            <td>$ '.number_format($iva,2,'.',',').'</td>
                            <td>$ '.number_format($total,2,'.',',').'</td>
                            <td>
                                <a class="waves-effect cyan btn-bmz" onclick="table_search_cli('.$item->idCliente.')" style="margin-bottom: 10px;"><i class="fa fa-search"></i> Cliente</a>
                                <a class="waves-effect cyan btn-bmz" href="'.base_url().'index.php/Cotizaciones/listaCotizacionesPorCliente/'.$item->idCliente.'" style="margin-bottom: 10px;"><i class="material-icons">format_list_numbered</i> Listado</a>
                                <a class="btn-floating blue tooltipped" data-position="top" data-delay="50" data-tooltip="Vizualizar" href="'.base_url().'index.php/Cotizaciones/documento/'.$item->id.'"data-tooltip-id="ef3093ca-7b20-1cbd-addf-bb83ee77b444" target="_blank"><i class="material-icons">remove_red_eye</i></a>
                                </td>
                                </tr>';
            }
           $html.='</tbody></table>'; 
        }

        echo $html;
    }
    function updateclifm(){
        $params = $this->input->post();
        $fm = $params['fm'];
        $cre = $params['cre'];
        $cli = $params['cli'];
        if($fm>0){
            $this->ModeloCatalogos->updateCatalogo('clientes',array('fisicamoral'=>$fm,'credito'=>$cre,'fm_confirm_personal'=>$this->idpersonal,'fm_confirmado'=>1),array('id'=>$cli));
        }
    }
    function verificarrelaciones($idcliente){
        $datosfac=$this->ModeloCatalogos->getselectwheren('f_facturas',array('Clientes_ClientesId'=>$idcliente));
        $datosven=$this->ModeloCatalogos->getselectwheren('ventas',array('idCliente'=>$idcliente,'activo'=>1));
        $datosvenc=$this->ModeloCatalogos->getselectwheren('ventacombinada',array('idCliente'=>$idcliente,'activo'=>1));
        $datospol=$this->ModeloCatalogos->getselectwheren('polizasCreadas',array('idCliente'=>$idcliente,'activo'=>1));
        $datosren=$this->ModeloCatalogos->getselectwheren('rentas',array('idCliente'=>$idcliente,'estatus'=>2));
        $datoscot=$this->ModeloCatalogos->getselectwheren('cotizaciones',array('idCliente'=>$idcliente));
        $html='<table border="1" class="table table-striped bordered">
                    <thead>
                        <tr>
                            <th>Tipo</th>
                            <th>Cantidad relacionadas</th>
                            
                        <tr></thead><tbody>';
            $html.='<tr><td>Facturas</td><td>'.$datosfac->num_rows().'</td></tr>';
            $html.='<tr><td>Ventas</td><td>'.$datosven->num_rows().'</td></tr>';
            $html.='<tr><td>VentasC</td><td>'.$datosvenc->num_rows().'</td></tr>';
            $html.='<tr><td>Poliza</td><td>'.$datospol->num_rows().'</td></tr>';
            $html.='<tr><td>Renta</td><td>'.$datosren->num_rows().'</td></tr>';
            $html.='<tr><td>Cotizaciones</td><td>'.$datoscot->num_rows().'</td></tr>';
            $html.='</tbody></table>';
        echo $html;
    }
    function save_contacto(){
        $params = $this->input->post();
        $dc_cliente = $params['dc_cliente'];
        $dc_atencionpara = $params['dc_atencionpara'];
        $dc_puesto = $params['dc_puesto'];
        $dc_email = $params['dc_email'];
        $dc_telefono = $params['dc_telefono'];
        $dc_celular = $params['dc_celular'];
        $temp=array(
                    'clienteId'=>$dc_cliente,
                    'atencionpara'=>$dc_atencionpara,
                    'puesto'=>$dc_puesto,
                    'email'=>$dc_email,
                    'telefono'=>$dc_telefono,
                    'celular'=>$dc_celular,
                    'dir'=>''
                );
        $this->ModeloCatalogos->Insert('cliente_datoscontacto',$temp);
    }
    function establecerpass(){
        $params = $this->input->post();
        $pass = $params['pass'];
        $fecha = $params['fecha'];
        $cli = $params['cli'];
        $this->ModeloCatalogos->updateCatalogo('clientes',array('pass_estatus'=>1,'pass_value'=>$pass,'pass_vigencia'=>$fecha),array('id'=>$cli));
    }
    function addaccesoweb(){
        $params = $this->input->post();
        $correo = $params['correo'];
        $pass = $params['pass'];
        $idcli = $params['idcli'];
        $pass2=password_hash($pass, PASSWORD_BCRYPT);
        $this->ModeloCatalogos->updateCatalogo('clientes',array('correo'=>$correo,'pass'=>$pass2,'ssap_i'=>$pass),array('id'=>$idcli));
    }
    function consult_df(){
        $datos = $this->input->post('datos');
        $DATA = json_decode($datos);

            $this->db->select('cli.id,cli.empresa, clidf.razon_social');
            $this->db->from('clientes cli');
            $this->db->join('cliente_has_datos_fiscales clidf', 'clidf.idCliente=cli.id AND clidf.activo=1');
            $this->db->where('clidf.razon_social!=cli.empresa');
            $this->db->group_start();

            for ($i=0;$i<count($DATA);$i++) {
                $this->db->or_like('cli.id',$DATA[$i]->cli);
            }
            $this->db->group_end(); 

            $query=$this->db->get();

            echo json_encode($query->result());
    }
    function presscli(){
        $params = $this->input->post();
        $idcli = $params['cli'];
        $data['cliente']=$idcli;
        $data['status']=0;
        $data['tipoventa']=0;
        $data['estatusventa']=1;
        $data['idpersonal']=0;
        $data['perfilid']=0;
        $fechainicial_reg=date("Y-m-d",strtotime($this->fechahoyc."- 12 month"));
        $data['fechainicial_reg']=$fechainicial_reg;
        $data['emp']=0;
        $data['filf']=0;
        $data['pagosview']=0;
        $subconsulta = $this->Ventas_model->sqlgarantias($data);
        
        $this->db->select('*');
        $this->db->from($subconsulta);
        $this->db->limit(10);
        $this->db->order_by("reg", "DESC");
        $query=$this->db->get();
        $html='';
        $html.='<table class="table bordered striped">';
                $html.='<thead>';
                    $html.='<tr>';
                        $html.='<th>#</th>';
                        $html.='<th>PRE FACTURA</th>';
                        $html.='<th>Fecha de creacion</th>';
                        $html.='<th>Empresa</th>';
                        $html.='<th>Factura</th>';
                    $html.='</tr>';
                $html.='</thead>';
                $html.='<tbody>';
                $empresa='';
                $row=1;
                foreach ($query->result() as $item) {
                    $empresa=$item->empresa;
                    $html.='<tr class="'.$row.' '.$item->id.'_'.$item->combinada.'">';
                        $html.='<td>';
                            if ($item->combinada==1) {
                                if($item->equipo>0){
                                    $html.=$item->equipo;
                                }else if($item->consumibles>0){
                                    $html.=$item->consumibles;
                                }else if($item->refacciones>0){
                                    $html.=$item->refacciones;
                                }
                            }else{
                                $html.=$item->id;
                            }
                        $html.='</td>';
                        $html.='<td>';
                            $html.='<a class="btn-floating blue" onclick="detalle_vp('.$item->id.','.$item->combinada.')"><i class="material-icons">assignment</i></a>';
                        $html.='</td>';
                        $html.='<td>'.$item->reg.'</td>';
                        $html.='<td>';
                            if ($item->tipov==1) {
                                $html.='Alta Productividad';    
                            }
                            if ($item->tipov==2) {
                                $html.='D-Impresión';    
                            }
                        $html.='</td>';
                        $html.='<td>'.$item->facturas.'</td>';
                    $html.='</tr>';
                    $row++;
                }
                $html.='</tbody>';
        $html.='</table>';
        if($row>=10){
            $html.='<a href="'.base_url().'index.php/Ventas?fechaview='.$fechainicial_reg.'&tipoventafil=5&idcli='.$idcli.'&emp='.$empresa.'&eje=0" target="_blank">Ver mas...</a>';
        }
        echo $html;
    }
    
}
?>
