<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class Modeloamonestacion extends CI_Model 
{
    public function __construct() 
    {
        parent::__construct();
    }
//==============Personal==========
    function getlist_amonestacion($params){
        $eje=$params['eje'];
        $fecha=$params['fecha'];
        $columns = array(
            0=>"asiga.id",
            1=>"asiga.idpersonal",
            2=>"asiga.sercli",
            3=>"asiga.sername",
            4=>"asiga.hora",
            5=>"asiga.reg",
            6=>"per.nombre",
            7=>"per.apellido_paterno",
            8=>"per.apellido_materno",
            9=>"DATE_FORMAT(asiga.reg, '%Y-%m-%d') as fecha_reg",
            10=>"DATE_FORMAT(asiga.reg, '%H:%i:%s') as hora_reg",
            11=>'asiga.serinfo'
        );
        $columns2 = array(
            0=>"asiga.id",
            1=>"asiga.idpersonal",
            2=>"asiga.sercli",
            3=>"asiga.sername",
            4=>"asiga.hora",
            5=>"asiga.reg",
            6=>"per.nombre",
            7=>"per.apellido_paterno",
            8=>"per.apellido_materno",
            9=>'asiga.serinfo'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('asignacion_ser_amonestacion asiga');
        $this->db->join('personal per', 'per.personalId=asiga.idpersonal');
        
        $where = array('asiga.activo'=>1);
        $this->db->where($where);
        if($eje>0){
            $this->db->where(array('asiga.idpersonal'=>$eje));
        }
        if($fecha!=''){
            $this->db->where("asiga.reg >='$fecha 00:00:00'");
            $this->db->where("asiga.reg <='$fecha 23:59:59'");
        }
        
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns2 as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns2[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $this->db->group_by("asiga.asig,asiga.tipo");
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    public function getlist_amonestacion_t($params){
        $eje=$params['eje'];
        $fecha=$params['fecha'];
        $columns = array( 
            0=>"asiga.id",
            1=>"asiga.idpersonal",
            2=>"asiga.sercli",
            3=>"asiga.sername",
            4=>"asiga.hora",
            5=>"asiga.reg",
            6=>"per.nombre",
            7=>"per.apellido_paterno",
            8=>"per.apellido_materno"
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        //$this->db->select('COUNT(*) as total');
        $this->db->select('*');
        $this->db->from('asignacion_ser_amonestacion asiga');
        $this->db->join('personal per', 'per.personalId=asiga.idpersonal');
        
        $where = array('asiga.activo'=>1);
        $this->db->where($where);
        if($eje>0){
            $this->db->where(array('asiga.idpersonal'=>$eje));
        }
        if($fecha!=''){
            $this->db->where("asiga.reg >='$fecha 00:00:00'");
            $this->db->where("asiga.reg <='$fecha 23:59:59'");
        }
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        //$this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        //$this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $this->db->group_by("asiga.asig,asiga.tipo");
        $query=$this->db->get();
        // print_r($query); die;
        return $query->num_rows();
    }







}