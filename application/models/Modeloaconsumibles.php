<?php
$a=session_id();
if(empty($a)) session_start();
defined('BASEPATH') OR exit ('No direct script access allowed');

class Modeloaconsumibles extends CI_Model {
    public function __construct() {
        parent::__construct();
        $this->DB9 = $this->load->database('other9_db', TRUE);
    }

    function getconsumibles_bodega($params){
            $bodegaId=$params['bodegaId'];
            $perfilid=$params['perfilid'];
            
            $columns = array( 
                0=>'co.id',
                1=>'cobo.bodegaId',
                2=>'co.parte',
                3=>'co.modelo',
                4=>'cobo.total',
                5=>'cobo.total',
                6=>'bo.bodega',
                7=>'cobo.consubId',
                8=>'cobo.resguardar_cant'
            );

            $select="";
            foreach ($columns as $c) {
                $select.="$c, ";
            }
            $this->db->select($select);
            $this->db->from('consumibles co');
            $this->db->join('consumibles_bodegas cobo', 'co.id=cobo.consumiblesId and cobo.status = 1','left');
            $this->db->join('bodegas bo', 'bo.bodegaId=cobo.bodegaId','left');
            $this->db->where(array('co.status'=>1));
            if($bodegaId>0){
                $this->db->where(array('cobo.bodegaId'=>$bodegaId));
            }else{
                if($perfilid>1){
                    $this->db->where('(cobo.bodegaId=1 or cobo.bodegaId=8 or cobo.bodegaId=10 or cobo.bodegaId=5)');
                }
            }
            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->db->group_start();
                foreach($columns as $c){
                    $this->db->or_like($c,$search);
                }
                $this->db->group_end();
                
            }
            if($bodegaId!=0){
                $this->db->where("cobo.bodegaId=".$bodegaId);
            }
            
            $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
            $this->db->limit($params['length'],$params['start']);
            //echo $this->db->get_compiled_select();
            $query=$this->db->get();
            // print_r($query); die;
            return $query;
    }
    function totalconsumiblesbodega($params){
            $bodegaId=$params['bodegaId'];
            $perfilid=$params['perfilid'];
            $columns = array( 
                0=>'co.id',
                1=>'cobo.bodegaId',
                2=>'co.modelo',
                3=>'cobo.total',
                4=>'cobo.total',
                5=>'bo.bodega',
                6=>'co.parte',
            );

            $select="";
            foreach ($columns as $c) {
                $select.="$c, ";
            }
            $this->db->select("'1' as total");
            $this->db->from('consumibles co');
            $this->db->join('consumibles_bodegas cobo', 'co.id=cobo.consumiblesId and cobo.status = 1','left');
            $this->db->join('bodegas bo', 'bo.bodegaId=cobo.bodegaId','left');
            $this->db->where(array('co.status'=>1));
            if($bodegaId>0){
                $this->db->where(array('cobo.bodegaId'=>$bodegaId));
            }else{
                if($perfilid>1){
                    $this->db->where('(cobo.bodegaId=1 or cobo.bodegaId=8 or cobo.bodegaId=10 or cobo.bodegaId=5)');
                }
            }
            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->db->group_start();
                foreach($columns as $c){
                    $this->db->or_like($c,$search);
                }
                $this->db->group_end();
                
            }
            if($bodegaId!=0){
                $this->db->where("cobo.bodegaId=".$bodegaId);
            }
            
            //$this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
            //$this->db->limit($params['length'],$params['start']);
            //echo $this->db->get_compiled_select();
            $query=$this->db->get();
            // print_r($query); die;
            return $query->num_rows();
    }
    //==================================
    function getrefacciones_bodega($params){
        $bodegaId=$params['bodegaId'];
        $perfilid=$params['perfilid'];
        $columns = array( 
            0=>'cata.id',
            1=>'vire.bodega',
            2=>'cata.codigo',
            3=>'cata.nombre',
            4=>'vire.cantidad',
            5=>'vire.cantidad',
            6=>'vire.con_serie',
            7=>'vire.serieId',
            8=>'vire.resguardar_cant'
        );        
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->DB9->select($select);
        $this->DB9->from('refacciones cata');
        $this->DB9->join('vista_inventario_refacciones vire', 'vire.id=cata.id','left');
        if ($bodegaId>0) {
            $this->DB9->where(array('vire.bodegaId'=>$bodegaId));
        }else{
            if($perfilid>1){
                $this->DB9->where('(vire.bodegaId=1 or vire.bodegaId=8 or vire.bodegaId=10 or vire.bodegaId=5)');
            }
        }
        $this->DB9->where(array('cata.status'=>1));
        
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->DB9->group_start();
            foreach($columns as $c){
                $this->DB9->or_like($c,$search);
            }
            $this->DB9->group_end();  
        }            
        $this->DB9->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->DB9->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->DB9->get();
        // print_r($query); die;
        return $query;
    }
    function totalrefaccionesbodega($params){
        $bodegaId=$params['bodegaId'];
        $perfilid=$params['perfilid'];
        $columns = array( 
            0=>'cata.id',
            1=>'vire.bodega',
            2=>'cata.nombre',
            3=>'vire.cantidad',
            4=>'vire.cantidad',  
            5=>'cata.codigo',
        );
        
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->DB9->select(" '1' as total");
        $this->DB9->from('refacciones cata');
        $this->DB9->join('vista_inventario_refacciones vire', 'vire.id=cata.id','left');
        if ($bodegaId>0) {
            $this->DB9->where(array('vire.bodegaId'=>$bodegaId));
        }else{
            if($perfilid>1){
                $this->DB9->where('(vire.bodegaId=1 or vire.bodegaId=8 or vire.bodegaId=10 or vire.bodegaId=5)');
            }
        }
        $this->DB9->where(array('cata.status'=>1));
        
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->DB9->group_start();
            foreach($columns as $c){
                    $this->DB9->or_like($c,$search);
            }
            $this->DB9->group_end();  
        }            
        //$this->db->order_by($columnsss[$params['order'][0]['column']], $params['order'][0]['dir']);
        //$this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->DB9->get();
        // print_r($query); die;
        return $query->num_rows();
    }
   
    //=============================================
        function getaccesorio_bodega($params){
            $bodegaId=$params['bodegaId'];
            $perfilid=$params['perfilid'];
            if($perfilid>1){
                $w_resguardo=' and sacc.resguardar_cant = 0 ';
            }else{
                $w_resguardo='';
            }
            $columns = array( 
                0=>'cata.id',
                1=>'via.bodega',
                2=>'cata.nombre',
                3=>'via.stock',
                4=>'via.stock',
                5=>'cata.no_parte',
                6=>'(select sum(sacc.cantidad) from  series_accesorios as sacc where sacc.accesoriosid=cata.id and sacc.bodegaId=via.bodegaId and sacc.status<2 and sacc.con_serie=0 and sacc.activo=1 '.$w_resguardo.') as stock2',
                7=>'(select count(*) from  series_accesorios as sacc where sacc.accesoriosid=cata.id and sacc.bodegaId=via.bodegaId and sacc.status<2 and sacc.con_serie=1 and sacc.activo=1 '.$w_resguardo.') as stock1'
            );
            $columnsss = array( 
                0=>'cata.id',
                1=>'via.bodega',
                2=>'cata.nombre',
                3=>'via.stock',
                4=>'via.stock',
                5=>'cata.no_parte',
                
            );
            $select="";
            foreach ($columns as $c) {
                $select.="$c, ";
            }
            $this->db->select($select);
            $this->db->from('catalogo_accesorios cata');
            $this->db->join('vista_inventario_accesorios via', 'via.id=cata.id','left');
            if ($bodegaId>0) {
                $this->db->where(array('via.bodegaId'=>$bodegaId));
            }else{
                if($perfilid>1){
                    $this->db->where('(via.bodegaId=1 or via.bodegaId=8 or via.bodegaId=10 or via.bodegaId=5)');
                }
            }
            $this->db->where(array('cata.status'=>1));

            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->db->group_start();
                foreach($columnsss as $c){
                    $this->db->or_like($c,$search);
                }
                $this->db->group_end();  
            }            
            $this->db->order_by($columnsss[$params['order'][0]['column']], $params['order'][0]['dir']);
            $this->db->limit($params['length'],$params['start']);
            //echo $this->db->get_compiled_select();
            $query=$this->db->get();
            // print_r($query); die;
            return $query;
        }
        function totalaccesoriobodega($params){
            $bodegaId=$params['bodegaId'];
            $perfilid=$params['perfilid'];
            $columns = array( 
                0=>'cata.id',
                1=>'via.bodega',
                2=>'cata.nombre',
                3=>'via.stock',
                4=>'via.stock',
            );
            $select="";
            foreach ($columns as $c) {
                $select.="$c, ";
            }
            $this->DB9->select(" '1' as total");
            $this->DB9->from('catalogo_accesorios cata');
            $this->DB9->join('vista_inventario_accesorios via', 'via.id=cata.id','left');
            if ($bodegaId>0) {
                $this->DB9->where(array('via.bodegaId'=>$bodegaId));
            }else{
                if($perfilid>1){
                    $this->db->where('(via.bodegaId=1 or via.bodegaId=8 or via.bodegaId=10 or via.bodegaId=5)');
                }
            }
            $this->DB9->where(array('cata.status'=>1));
            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->DB9->group_start();
                foreach($columns as $c){
                    $this->DB9->or_like($c,$search);
                }
                $this->DB9->group_end();  
            }            
            //$this->db->order_by($columnss[$params['order'][0]['column']], $params['order'][0]['dir']);
            //$this->db->limit($params['length'],$params['start']);
            //echo $this->db->get_compiled_select();
            $query=$this->DB9->get();
            // print_r($query); die;
            return $query->num_rows();
        }
    //==============================================
    function totalconsumible(){
        $this->DB9->select('sum(total) as total');
        $this->DB9->from('consumibles_bodegas');
        $this->DB9->where("status", 1);
        $query=$this->DB9->get();
        $total=0;
        foreach ($query->result() as $item) {
            $total = $item->total;
        }
        return $total;
    }
    function totalconsumibleid($id){
        $this->DB9->select('sum(total) as total');
        $this->DB9->from('consumibles_bodegas');
        $this->DB9->where(array("status"=>1,'consumiblesId'=>$id));
        $query=$this->DB9->get();
        $total=0;
        foreach ($query->result() as $item) {
            $total = $item->total;
        }
        if ($total>0) {
            $totals=$total;
        }else{
            $totals=0;
        }
        return $totals;
    }
    function totalconsumibleid2($id,$bodega){
        $this->DB9->select('sum(total)-sum(resguardar_cant) as total');
        $this->DB9->from('consumibles_bodegas');
        $this->DB9->where(array("status"=>1,'consumiblesId'=>$id,'bodegaId'=>$bodega));
        $query=$this->DB9->get();
        $total=0;
        foreach ($query->result() as $item) {
            $total = $item->total;
        }
        if ($total>0) {
            $totals=$total;
        }else{
            $totals=0;
        }
        return $totals;
    }
    function totalrefacciones(){
        $this->DB9->select('sum(cantidad) as total');
        $this->DB9->from('series_refacciones sa');
        $this->DB9->where("sa.activo", 1);
        $this->DB9->where("sa.status", 0);
       
        $query=$this->DB9->get();
        return $query->row()->total;
    }
    function totalrefaccionesid($id){
        $this->DB9->select('sum(cantidad) as total');
        $this->DB9->from('series_refacciones sa');
        $this->DB9->where("sa.activo", 1);
        $this->DB9->where("sa.status", 0);
        $this->DB9->where("sa.refaccionid", $id);
       
        $query=$this->DB9->get();
        $totals =$query->row()->total;
        if ($totals>0) {
            $total=$totals;
        }else{
            $total=0;
        }
        return $total;
    }
    function totalrefaccionesid2($id,$bodega){
        $this->db->select('sum(cantidad) as total');
        $this->db->from('series_refacciones sa');
        $this->db->where("sa.activo", 1);
        $this->db->where("sa.status", 0);
        $this->db->where("sa.refaccionid", $id);
        $this->db->where("sa.bodegaId", $bodega);
       
        $query=$this->db->get();
        $totals =$query->row()->total;
        if ($totals>0) {
            $total=$totals;
        }else{
            $total=0;
        }
        return $total;
    }
    function totalaccesorio(){
        $this->DB9->select('COUNT(*) as total');
        $this->DB9->from('series_accesorios sa');
        $this->DB9->join('catalogo_accesorios cata', 'cata.id=sa.accesoriosid');
        $this->DB9->where(array("sa.activo"=>1,'cata.status'=>1,'sa.con_serie'=>1));
        
        $query=$this->DB9->get();
        $total1=$query->row()->total;

        $this->DB9->select('sum(sa.cantidad) as total');
        $this->DB9->from('series_accesorios sa');
        $this->DB9->join('catalogo_accesorios cata', 'cata.id=sa.accesoriosid');
        $this->DB9->where(array("sa.activo"=>1,'cata.status'=>1,'sa.con_serie'=>0));
        
        $query=$this->DB9->get();
        $total2=$query->row()->total;

         $totalg=$total1+$total2;
         return $totalg;
    }
    function totalaccesorioid($id){
        $this->DB9->select('COUNT(*) as total');
        $this->DB9->from('series_accesorios sa');
        $this->DB9->where("sa.activo", 1);
        $this->DB9->where("sa.accesoriosid", $id);
        
        $query=$this->DB9->get();
        $totals =$query->row()->total;
        if ($totals>0) {
            $total=$totals;
        }else{
            $total=0;
        }
        return $total;
    }
    function totalequipos(){
        $this->db->select('COUNT(*) as total');
        $this->db->from('series_productos');
        $this->db->where("activo", 1);
        $this->db->where("status < 2");
        $query=$this->db->get();
        $total=0;
        foreach ($query->result() as $item) {
            $total = $item->total;
        }
        return $total;
    }
    //=============================================
        function getequipos_bodega($params){
            $bodegaId=$params['bodegaId'];
            $perfilid=$params['perfilid'];
            $columns = array( 
                0=>'eq.id',
                1=>'sp.bodega',
                2=>'eq.modelo',
                3=>'sp.stock',
                4=>'eq.noparte',
                5=>'sp.resguardar_cant'
            );
            $select="";
            foreach ($columns as $c) {
                $select.="$c, ";
            }
        
            $this->db->select($select);
            $this->db->from('equipos eq');
            $this->db->join('vista_inventario_equipos sp', 'eq.id=sp.productoid','left');
            if ($bodegaId>0) {
                $this->db->where(array('sp.bodegaId'=>$bodegaId));
            }else{
                if($perfilid>1){
                    $this->db->where('(sp.bodegaId=1 or sp.bodegaId=8 or sp.bodegaId=10 or sp.bodegaId=5)');
                }
            }
            $this->db->where(array('eq.estatus'=>1));
            $this->db->group_by(array("eq.id", "sp.bodega"));
            

            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->db->group_start();
                //$colum=0;
                foreach($columns as $c){
                    //if($colum==3){
                    
                    //}else{
                        $this->db->or_like($c,$search);
                    //}
                    //$colum=$colum+1;
                }
                $this->db->group_end();  
            }            
            $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
            $this->db->limit($params['length'],$params['start']);
            //echo $this->db->get_compiled_select();
            $query=$this->db->get();
            // print_r($query); die;
            return $query;
        }
        function totalequiposbodega($params){
            $bodegaId=$params['bodegaId'];
            $perfilid=$params['perfilid'];
            $columns = array( 
                0=>'eq.id',
                1=>'sp.bodega',
                2=>'eq.modelo',
                3=>'sp.stock',
                4=>'sp.stock',
            );
            $select="";
            foreach ($columns as $c) {
                $select.="$c, ";
            }
            $this->db->select(" '1' as total");
            $this->db->from('equipos eq');
            $this->db->join('vista_inventario_equipos sp', 'eq.id=sp.productoid','left');
            if ($bodegaId>0) {
                $this->db->where(array('sp.bodegaId'=>$bodegaId));
            }else{
                if($perfilid>1){
                    $this->db->where('(sp.bodegaId=1 or sp.bodegaId=8 or sp.bodegaId=10 or sp.bodegaId=5)');
                }
            }
            $this->db->where(array('eq.estatus'=>1));
            $this->db->group_by(array("eq.id", "sp.bodega"));

            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->db->group_start();
                $colum=0;
                foreach($columns as $c){
                    //if($colum==3){
                    
                    //}else{
                        $this->db->or_like($c,$search);
                    //}
                    //$colum=$colum+1;
                }
                $this->db->group_end();  
            }            
            //$this->db->order_by($columnsss[$params['order'][0]['column']], $params['order'][0]['dir']);
            //$this->db->limit($params['length'],$params['start']);
            //echo $this->db->get_compiled_select();
            $query=$this->db->get();
            // print_r($query); die;
            return $query->num_rows();
        }
    //==============================================
    function servicioscontratosactivos($consumibleId){
        $strq = "SELECT cc.* FROM compra_consumibles as cc 
                inner join series_compra as com on com.compraId=cc.compraId
                WHERE cc.consumibleId=$consumibleId AND cc.cantidad>cc.recibidos AND cc.activo=1 and com.activo=1"; 
        $query = $this->db->query($strq);
        return $query;
    }
    function consultrefapendient($refaccionId){
        $strq = "SELECT * 
                FROM series_compra_refacciones as scr 
                INNER JOIN series_compra as com on com.compraId=scr.compraId 
                WHERE scr.refaccionid=$refaccionId AND scr.activo=1 AND com.activo=1 AND scr.con_serie=1 AND scr.status=0 
                UNION 
                SELECT * FROM series_compra_refacciones as scr 
                INNER JOIN series_compra as com on com.compraId=scr.compraId 
                WHERE scr.refaccionid=$refaccionId AND scr.activo=1 AND com.activo=1 AND scr.con_serie=0 AND scr.cantidad>scr.entraron"; 
        $query = $this->db->query($strq);
        return $query;
    }
    function consultaccependient($acceId){
        $strq = "SELECT * 
                FROM series_compra_accesorios as scr 
                INNER JOIN series_compra as com on com.compraId=scr.compraId 
                WHERE scr.accesoriosid=$acceId AND scr.activo=1 AND com.activo=1 AND scr.con_serie=1 AND scr.status=0 
                UNION SELECT * FROM series_compra_accesorios as scr 
                INNER JOIN series_compra as com on com.compraId=scr.compraId 
                WHERE scr.accesoriosid=$acceId AND scr.activo=1 AND com.activo=1 AND scr.con_serie=0 AND scr.cantidad>scr.entraron"; 
        $query = $this->db->query($strq);
        return $query;
    }
    function consultequipopendient($equiId){
        $strq = "SELECT * FROM series_compra_productos as scp 
                INNER JOIN series_compra as com on com.compraId=scp.compraId 
                WHERE scp.productoid=$equiId and scp.activo=1 AND scp.status=0 and com.activo=1"; 
        $query = $this->db->query($strq);
        return $query;
    }
    function verificarprioridad_r($id){
        $strq = "SELECT * FROM cotizaciones as cot
                INNER JOIN cotizaciones_has_detallesRefacciones as conr on conr.idCotizacion=cot.id
                WHERE cot.priorizar_cot=1 AND cot.estatus=1 AND conr.idRefaccion='$id'"; 
        $query = $this->db->query($strq);
        return $query;
    }
    /*
    function verificarprioridad_c($id){
        $strq = "SELECT * FROM cotizaciones as cot
                INNER JOIN cotizaciones_has_detallesConsumibles as cotd on cotd.idCotizacion=cot.id
                WHERE cot.priorizar_cot=1 AND cot.estatus=1 AND cotd.idConsumible='$id'"; 
        $query = $this->db->query($strq);
        return $query;
    }
    function verificarprioridad_c2($id){
        $strq = "SELECT * FROM cotizaciones as cot
                INNER JOIN cotizaciones_has_detallesequipos_consumibles as cotd on cotd.id_cotizacion=cot.id
                WHERE cot.priorizar_cot=1 AND cot.estatus=1 AND cotd.id_consumibles='$id'"; 
        $query = $this->db->query($strq);
        return $query;
    }
    function verificarprioridad_a($id){
        $strq = "SELECT * FROM cotizaciones as cot
                INNER JOIN cotizaciones_has_detallesEquipos_accesorios as cotd on cotd.id_cotizacion=cot.id
                WHERE cot.priorizar_cot=1 AND cot.estatus=1 AND cotd.id_accesorio='$id'"; 
        $query = $this->db->query($strq);
        return $query;
    }
    function verificarprioridad_e($id){
        $strq = "SELECT * FROM cotizaciones as cot
                INNER JOIN cotizaciones_has_detallesEquipos as cotd on cotd.idCotizacion=cot.id
                WHERE cot.priorizar_cot=1 AND cot.estatus=1 AND cotd.idEquipo='$id'"; 
        $query = $this->db->query($strq);
        return $query;
    }
    */

}