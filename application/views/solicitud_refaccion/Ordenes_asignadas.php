<!-- START CONTENT -->
<section id="content">
  <!--start container-->
  <div class="container">
    <div class="section">
      <div class="card-panel">
      		<div id="table-datatables">
      			<br>
            <h4 class="header">Listado de Ordenes Asignadas</h4>
            <div class="row">
              <div class="col s12">
                <a class="btn waves-effect waves-light gradient-45deg-green-teal gradient-shadow" href="#" onclick="nueva_asigna()">Nuevo</a>
                <table id="tabla_asignar" class="responsive-table display" cellspacing="0">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Técnico</th>
                      <th>Ruta</th>
                      <th>Prioridad</th>
                      <th>Estatus</th>
                      <th>Fecha de inicio</th>
                      <th>Acciones</th>
                    </tr>
                  </thead>
                  <tbody>
                    <!------------------------------------->
                    <tr>
                      <td>144</td>
                      <td>Arthur Gerardo</td>
                      <td>Ruta 1</td>
                      <td align="center" style="background-color: #F02F2F">Mismo día</td>
                      <td>Por realizar</td>
                      <td>09:00 am</td>
                      <td>
                        <a class="btn-floating red tooltipped prefactura" data-position="top" data-delay="50" data-tooltip="Orden de Servicio" data-tooltip-id="dca448ed-3a90-3ed2-3922-f974f10908a4" onclick="detalle('1')">
                        <i class="material-icons">description</i></a>  
                        <a class="btn-floating red tooltipped reagenda" data-position="top" data-delay="50" data-tooltip="Reagendar Fecha" data-tooltip-id="dca448ed-3a90-3ed2-3922-f974f10908a4">
                        <i class="material-icons">date_range</i></a>
                        <a class="btn-floating red tooltipped asig_tec" data-position="top" data-delay="50" data-tooltip="Reasignar Tecnico" data-tooltip-id="dca448ed-3a90-3ed2-3922-f974f10908a4">
                        <i class="material-icons">person_add</i></a>
                        <a class="btn-floating red tooltipped iniciar" onclick="iniciar(1);" data-position="top" data-delay="50" data-tooltip="Iniciar Servicio" data-tooltip-id="dca448ed-3a90-3ed2-3922-f974f10908a4">
                        <i class="material-icons">power_settings_new</i></a>
                        <button class="btn red tooltipped solicita" data-position="top" data-delay="50" data-tooltip="Solicitar Refacción" data-tooltip-id="dca448ed-3a90-3ed2-3922-f974f10908a4">Solicitudes</button>
                        <a class="btn-floating red tooltipped pdf" data-position="top" data-delay="50" data-tooltip="PDF de Cotización" data-tooltip-id="dca448ed-3a90-3ed2-3922-f974f10908a4">
                        <i class="material-icons">picture_as_pdf</i></a>
                      </td>
                    </tr>
                    <tr>
                      <td>145</td>
                      <td>Arthur Gerardo</td>
                      <td>Ruta 2</td>
                      <td style="background-color: #FE9A64">Día siguiente</td>
                      <td>En Proceso</td>
                      <td>10:00 am</td>
                      <td>
                        <a class="btn-floating red tooltipped prefactura" data-position="top" data-delay="50" data-tooltip="Orden de servicio" data-tooltip-id="dca448ed-3a90-3ed2-3922-f974f10908a4" onclick="detalle('1')">
                        <i class="material-icons">description</i></a>  
                        <a class="btn-floating red tooltipped reagenda" data-position="top" data-delay="50" data-tooltip="Reagendar Fecha" data-tooltip-id="dca448ed-3a90-3ed2-3922-f974f10908a4">
                        <i class="material-icons">date_range</i></a>
                        <a class="btn-floating red tooltipped asig_tec" data-position="top" data-delay="50" data-tooltip="Reasignar Tecnico" data-tooltip-id="dca448ed-3a90-3ed2-3922-f974f10908a4">
                        <i class="material-icons">person_add</i></a>
                        <a class="btn-floating green tooltipped iniciar" onclick="iniciar(0);" data-position="top" data-delay="50" data-tooltip="Iniciar Servicio" data-tooltip-id="dca448ed-3a90-3ed2-3922-f974f10908a4">
                        <i class="material-icons">power_settings_new</i></a>
                        <button class="btn red tooltipped solicita" data-position="top" data-delay="50" data-tooltip="Solicitar Refacción" data-tooltip-id="dca448ed-3a90-3ed2-3922-f974f10908a4">Solicitudes</button>
                        <a class="btn-floating red tooltipped pdf" data-position="top" data-delay="50" data-tooltip="PDF de Cotización" data-tooltip-id="dca448ed-3a90-3ed2-3922-f974f10908a4">
                        <i class="material-icons">picture_as_pdf</i></a>
                      </td>
                    </tr>
                    <tr>
                      <td>146</td>
                      <td>Arthur Gerardo</td>
                      <td>Ruta 2</td>
                      <td style="background-color: #66FE64">15 días</td>
                      <td>Incidente</td>
                      <td>11:00 am</td>
                      <td>
                        <a class="btn-floating red tooltipped prefactura" data-position="top" data-delay="50" data-tooltip="Orden de servicio" data-tooltip-id="dca448ed-3a90-3ed2-3922-f974f10908a4" onclick="detalle('1')">
                        <i class="material-icons">description</i></a>  
                        <a class="btn-floating red tooltipped reagenda" data-position="top" data-delay="50" data-tooltip="Reagendar Fecha" data-tooltip-id="dca448ed-3a90-3ed2-3922-f974f10908a4">
                        <i class="material-icons">date_range</i></a>
                        <a class="btn-floating red tooltipped asig_tec" data-position="top" data-delay="50" data-tooltip="Reasignar Tecnico" data-tooltip-id="dca448ed-3a90-3ed2-3922-f974f10908a4">
                        <i class="material-icons">person_add</i></a>
                        <a class="btn-floating red tooltipped iniciar" onclick="iniciar(1);" data-position="top" data-delay="50" data-tooltip="Iniciar Servicio" data-tooltip-id="dca448ed-3a90-3ed2-3922-f974f10908a4">
                        <i class="material-icons">power_settings_new</i></a>
                        <button class="btn red tooltipped solicita" data-position="top" data-delay="50" data-tooltip="Solicitar Refacción" data-tooltip-id="dca448ed-3a90-3ed2-3922-f974f10908a4">Solicitudes</button>
                        <a class="btn-floating red tooltipped pdf" data-position="top" data-delay="50" data-tooltip="PDF de Cotización" data-tooltip-id="dca448ed-3a90-3ed2-3922-f974f10908a4">
                        <i class="material-icons">picture_as_pdf</i></a>
                      </td>
                    </tr>
                    <tr>
                      <td>147</td>
                      <td>Arthur Gerardo</td>
                      <td>Ruta 3</td>
                      <td style="background-color: #64D4FE">Siguiente servicio</td>
                      <td>Finalizado</td>
                      <td>12:00 pm</td>
                      <td>
                        <a class="btn-floating red tooltipped prefactura" data-position="top" data-delay="50" data-tooltip="Orden de servicio" data-tooltip-id="dca448ed-3a90-3ed2-3922-f974f10908a4" onclick="detalle('1')">
                        <i class="material-icons">description</i></a>  
                        <a class="btn-floating red tooltipped reagenda" data-position="top" data-delay="50" data-tooltip="Reagendar Fecha" data-tooltip-id="dca448ed-3a90-3ed2-3922-f974f10908a4">
                        <i class="material-icons">date_range</i></a>
                        <a class="btn-floating red tooltipped asig_tec" data-position="top" data-delay="50" data-tooltip="Reasignar Tecnico" data-tooltip-id="dca448ed-3a90-3ed2-3922-f974f10908a4">
                        <i class="material-icons">person_add</i></a>
                        <a class="btn-floating red tooltipped iniciar" onclick="iniciar(1);" data-position="top" data-delay="50" data-tooltip="Iniciar Servicio" data-tooltip-id="dca448ed-3a90-3ed2-3922-f974f10908a4">
                        <i class="material-icons">power_settings_new</i></a>
                        <button class="btn red tooltipped solicita" data-position="top" data-delay="50" data-tooltip="Solicitar Refacción" data-tooltip-id="dca448ed-3a90-3ed2-3922-f974f10908a4">Solicitudes</button>
                        <a class="btn-floating red tooltipped pdf" data-position="top" data-delay="50" data-tooltip="PDF de Cotización" data-tooltip-id="dca448ed-3a90-3ed2-3922-f974f10908a4">
                        <i class="material-icons">picture_as_pdf</i></a>
                      </td>
                    </tr>
                    <!------------------------------------->
                  </tbody>
                </table>
              </div>
            </div>
          </div>
      </div>
    </div>  
  </div>
</section>
<div id="modalAsigna" class="modal">
    <div class="modal-content ">
      <h4> <i class="material-icons">mode_edit</i> Reasignar Técnico </h4>
      <div class="col s12 m12 l6">
        <input type="hidden" name="id_asig" id="id_asig">
          <div class="row" align="center">
             <div class="col s12 m12 l12">
               <label class="col s12 m2 l2">Técnico</label>
               <div class="col s12 m8 l8" >
                 <select name="tecnico" id="tecnico" class="browser-default chosen-select">
                 </select>
               </div>
             </div> 
          </div>
      </div>  
    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat" onclick="tecnico()">Aceptar</a>
      <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
    </div>
  </div>
</div>

<div id="modalReagenda" class="modal">
    <div class="modal-content ">
      <h4> <i class="material-icons">mode_edit</i> Reagendar Servicio </h4>
      <input type="hidden" name="id_asig" id="id_asig">
      <div class="col s12 m12 l6">
          <div class="row" align="center">
             <div class="col s12 m12 l12">
               <label class="col s12 m2 l2">Fecha</label>
               <div class="col s12 m8 l8">
                 <input type="date" name="fecha" id="fecha">
               </div>
             </div>
             <div class="col s12 m12 l12">
               <label class="col s12 m2 l2">Hora</label>
               <div class="col s12 m8 l8">
                 <input type="time" name="hora" id="hora">
               </div>
             </div> 
          </div>
          <div class="col s12 m12 l12">
            <label class="col s12 m2 l2">Motivo</label>
            <div class="col s12 m8 l8">
              <input type="text" name="motivo" id="motivo">
            </div>
          </div> 
      </div>  
    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat " onclick="reagenda()">Reagendar</a>
      <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
    </div>
  </div>
</div>

<div id="modalNvo" class="modal">
    <div class="modal-content ">
      <h4> <i class="material-icons">build</i>Tipo de Servicio</h4>
      <input type="hidden" name="id_asig" id="id_asig">
      <div class="col s12 m12 l6">
        <div class="row" align="center">
           <div class="col s12 m12 l12">
             <label class="col s12 m2 l2">Servicio o evento</label>
             <select id="serv_selec" name="serv_selec" class="browser-default chosen-select">
                <option></option>
                <?php foreach($serv as $s){ ?>
                  <option value="<?php echo $s->id?>"><?php echo $s->nombre; ?></option>
                <?php }?>
              </select>
           </div>
        </div>  
        <div class="modal-footer">
          <a href="#!" id="acepta_serv" class="modal-action modal-close waves-effect waves-red gray btn-flat">Aceptar</a>
          <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat">Cerrar</a>
        </div>
      </div>
    </div>
</div>

<div id="modal_addref" class="modal">
    <div class="modal-content " style="padding-left: 0px; padding-right: 0px;">
        <div class="row" style="margin-bottom: 0px;">
          <ul class="collapsible collapsible-accordion" data-collapsible="accordion">
            <li class="">
              <div class="collapsible-header">
                Equipo</div>
              <div class="collapsible-body" style="display: none;">
                <!----------------------------------->
                 <div class="row" >
                   <div class="col s2 m2 l2">
                      <label for="selected_newequipo_cantidad" class="active">Cantidad</label>
                      <input id="selected_newequipo_cantidad" class="disabledquitar" type="number" value="1">
                      
                    </div>
                    <div class="col s3 m3 l3">
                      <label for="selected_newequipo" class="active" style="margin-left: 3rem">Equipo</label>
                      <select id="selected_newequipo" name="selected_newequipo" class="browser-default disabledquitar chosen-select" onchange="selected_newequipo()">
                        <option value="" selected disabled></option>
                       <?php foreach ($equiposrow as $item) { ?>
                          <option value="<?php echo $item->id; ?>"><?php echo $item->modelo; ?></option>
                      <?php } ?> 
                      </select>
                    </div>
                    <div class="col s3 m3 l3">
                      <label for="selected_newequipo_b" class="active" style="margin-left: 3rem">Bodega</label>
                      <select id="selected_newequipo_b" name="selected_newequipo_b" class="browser-default disabledquitar chosen-select">
                       <?php foreach ($bodegasrow as $item) { ?>
                          <option value="<?php echo $item->bodegaId; ?>"><?php echo $item->bodega; ?></option>
                      <?php } ?> 
                      </select>
                    </div>
                    <div class="col s4 m4 l4">
                      <div class="col s12 m12 l12">
                        <input type="checkbox" id="temp" class="disabledquitar" onchange="temp()" />
                        <label for="temp">Temporal</label>
                      </div>
                      <div class="col s12 m12 l12 divtemp_reg" style="display: none;">
                        <label for="temp_reg">Fecha Termino</label>
                        <input type="date" id="temp_reg" class="disabledquitar" />
                        
                      </div>

                      
                    </div>
                 </div> 
                
                <!----------------------------------->
              </div>
            </li>
            <li class="">
              <div class="collapsible-header">
                Consumibles</div>
              <div class="collapsible-body" style="display: none;">
                <!----------------------------------->
                  
                  <div class="row">
                    <div class="col s2 m2 l2">
                      <label for="selected_newconsumible_cantidad" class="active">Cantidad</label>
                      <input id="selected_newconsumible_cantidad" class="disabledquitar" type="number" value="1">
                      
                    </div>
                    <div class="col s3 m3 l3">
                      <label for="selected_newconsumible" class="active" style="margin-left: 3rem">Consumibles</label>
                      <select id="selected_newconsumible" name="selected_newconsumible" class="browser-default disabledquitar chosen-select"></select>
                    </div>
                    <div class="col s3 m3 l3">
                      <label for="selected_newconsumible_b" class="active" style="margin-left: 3rem">Bodega</label>
                      <select id="selected_newconsumible_b" name="selected_newconsumible_b" class="browser-default disabledquitar chosen-select">
                       <?php foreach ($bodegasrow as $item) { ?>
                          <option value="<?php echo $item->bodegaId; ?>"><?php echo $item->bodega; ?></option>
                      <?php } ?> 
                      </select>
                    </div>
                    <div class="col s4 m4 l4" style="text-align: center;">
                      <a class="btn-floating waves-effect waves-light green darken-1" onclick="addconsumible()">
                          <i class="material-icons">vertical_align_bottom</i>
                        </a>
                    </div>
                  </div>
                  <div class="row">
                    <table class="table" id="new_table_consumible">
                      <thead>
                        <tr>
                          <th>Cantidad</th>
                          <th>Consumible</th>
                          <th>Bodega</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody class="nttbodyc">
                        
                      </tbody>
                    </table>
                  </div> 
                <!----------------------------------->
              </div>
            </li>
            <li class="">
              <div class="collapsible-header">
                Accesorios</div>
              <div class="collapsible-body" style="display: none;">
                <!----------------------------------->
                  <div class="row">
                    <div class="col s2 m2 l2">
                      <label for="selected_newaccesorio_cantidad" class="active">Cantidad</label>
                      <input id="selected_newaccesorio_cantidad" class="disabledquitar" type="number" value="1">
                      
                    </div>
                    <div class="col s3 m3 l3">
                      <label for="selected_newaccesorio" class="active" style="margin-left: 3rem">Accesorios</label>
                      <select id="selected_newaccesorio" name="selected_newaccesorio" class="browser-default disabledquitar chosen-select"></select>
                    </div>
                    <div class="col s3 m3 l3">
                      <label for="selected_newaccesorio_b" class="active" style="margin-left: 3rem">Bodega</label>
                      <select id="selected_newaccesorio_b" name="selected_newaccesorio_b" class="browser-default disabledquitar chosen-select">
                       <?php foreach ($bodegasrow as $item) { ?>
                          <option value="<?php echo $item->bodegaId; ?>"><?php echo $item->bodega; ?></option>
                      <?php } ?> 
                      </select>
                    </div>
                    <div class="col s4 m4 l4" style="text-align: center;">
                      <a class="btn-floating waves-effect waves-light green darken-1" onclick="addaccesorio();">
                          <i class="material-icons">vertical_align_bottom</i>
                        </a>
                    </div>
                  </div>
                  <div class="row">
                    <table class="table" id="new_table_accesorios">
                      <thead>
                        <tr>
                          <th>Cantidad</th>
                          <th>Accesorios</th>
                          <th>Bodega</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody class="nttbodya">
                        
                      </tbody>
                    </table>
                  </div> 
                <!----------------------------------->
              </div>
            </li>
          </ul>
        </div>
    </div>
    <div class="modal-footer">
        <a class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
        <a class="waves-effect modal-close waves-red red btn-flat" style="color: white;" onclick="savenewequipo()">Agregar</a>
    </div>
    <!-- Modal finaliza fin-->
</div>
