<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require 'vendor/autoload.php';//php 7.1 como minimo
//use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Listaservicios extends CI_Controller {
    public function __construct()     {
        parent::__construct();

        $this->load->model('ModeloCatalogos');
        $this->load->model('Configuraciones_model');
        $this->load->model('ConfiguracionDocumentos_Model');
        $this->load->model('ModeloServicio');
        $this->load->model('Ventas_model');
        $this->load->model('ModeloAsignacion');
        $this->load->model('Rentas_model');
        $this->load->model('ModeloGeneral');
        
        $this->load->helper('url');
        $this->version = date('YmdHi');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fechaactual = date('Y-m-d');
        $this->submenu=55;
        if($this->session->userdata('logeado')==true){
            $this->perfilid =$this->session->userdata('perfilid');
            $this->idpersonal = $this->session->userdata('idpersonal');
            $this->usuario = $this->session->userdata('usuario');
            /*
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,52);// 52 es el id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
            */
        }else{
            redirect('login'); 
        }
    }

    function index(){
        $data['MenusubId']=$this->submenu;        
        //$data['resultzonas']=$this->ModeloCatalogos->getselectwheren('rutas',array('status'=>1));
        $data['resultzonas']=$this->ModeloCatalogos->view_session_rutas();
        //$data['resultstec']=$this->Configuraciones_model->searchTecnico();
        $data['resultstec']=$this->Configuraciones_model->view_session_searchTecnico(); 
        $data['consumiblesresult']=$this->ModeloCatalogos->getselectwheren('consumibles',array('status'=>1)); 
        $data['refaccionesresult']=$this->ModeloCatalogos->getselectwheren('refacciones',array('status'=>1)); 
        $data['version']=$this->version;
        $this->load->view('header');
        $this->load->view('main');
        $this->load->view('Servicio/listaservicios',$data);
        $this->load->view('footer');
        $this->load->view('Servicio/listaserviciosjs');
    }
    
    public function getlistaservicioc() {
        $params = $this->input->post();
        $getdata = $this->ModeloServicio->getlistaservicioc($params);
        $totaldata= $this->ModeloServicio->getlistaservicioct($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    public function getlistaserviciop() {
        $params = $this->input->post();
        $getdata = $this->ModeloServicio->getlistaserviciop($params);
        $totaldata= $this->ModeloServicio->getlistaserviciopt($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    public function getlistaserviciocl() {
        $params = $this->input->post();
        $getdata = $this->ModeloServicio->getlistaserviciocl($params);
        $totaldata= $this->ModeloServicio->getlistaservicioclt($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    public function getlistaserviciovent() {
        $params = $this->input->post();
        $getdata = $this->ModeloServicio->getlistaserviciovent($params);
        $totaldata= $this->ModeloServicio->getlistaservicioventt($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    function editarfecha(){
        $params = $this->input->post();
        $idrow  = $params['idrow'];
        $table  = $params['table'];
        $fecha  = $params['fecha'];
        $hora   = $params['hora'];
        $horafin= $params['horafin'];
        $hora_comida   = $params['hora_comida'];
        $horafin_comida= $params['horafin_comida'];

        $hora = $this->ModeloCatalogos->verificarhora($hora);
        $horafin = $this->ModeloCatalogos->verificarhora($horafin);
        
        $idasignacion= $params['idasignacion'];
        
        $tipo   = $params['tipo'];
        if($table<=2){
            $asig_id=$idasignacion;
        }else{
            $asig_id=$idrow;
        }
        if ($table==1) {
            $tables='asignacion_ser_contrato_a_e';
            $idname='asignacionIde';
            if($tipo==0){
                $idname='asignacionId';
                $idrow = $idasignacion;
            }

        }elseif ($table==2) {
            $tables='asignacion_ser_poliza_a_e';
            $idname='asignacionIde';

            if($tipo==0){
                $idname='asignacionId';
                $idrow = $idasignacion;
            }
        }elseif ($table==3) {
            $tables='asignacion_ser_cliente_a';
            $idname='asignacionId';
        }elseif ($table==4) {
            $tables='asignacion_ser_venta_a';
            $idname='asignacionId';
            
        }
        $datas=array(
            'fecha'=>$fecha,
            'hora'=>$hora,
            'horafin'=>$horafin,
            'hora_comida'=>$hora_comida,
            'horafin_comida'=>$horafin_comida,
            't_fecha'=>null,
            't_horainicio'=>null,
            't_horafin'=>null,
            'editado'=>1,
            'confirmado'=>0
        );

        if ($tipo==0) {
            $wherearray=array($idname=>$idrow,'status <'=>2);
            $infogi='General';
            //============== clonacion y separacion =================
                if ($table==1) {
                    $rowresult = $this->ModeloCatalogos->getselectwheren('asignacion_ser_contrato_a_e',array('asignacionId'=>$idrow));
                    $numequipos=$rowresult->num_rows();
                    $rowresult2 = $this->ModeloCatalogos->getselectwheren('asignacion_ser_contrato_a_e',array('asignacionId'=>$idrow,'status <'=>2));
                    $numequipos2=$rowresult2->num_rows();

                    if($numequipos2<$numequipos){
                        $rowresultd = $this->ModeloCatalogos->getselectwheren('asignacion_ser_contrato_a',array('asignacionId'=>$idrow));
                        $rowdata = $rowresultd->row();
                        unset($rowdata->asignacionId);
                        unset($rowdata->reg);

                        $asignacionId_new=$this->ModeloCatalogos->Insert('asignacion_ser_contrato_a',$rowdata);
                        $datas['asignacionId']=$asignacionId_new;
                    }
                }
            //===============================
        }else{
            $infogi='individual';
            $wherearray=array($idname=>$idrow);
            if($table!=3){
                $datas['nr']=0;
                $datas['nr_coment']='';
            }else{
                $this->ModeloCatalogos->db4_updateCatalogo('asignacion_ser_cliente_a_d',array('nr'=>0,'nr_coment'=>''),array($idname=>$idrow));
            }
        }
        if($table!=3){
                $datas['nr']=0;
                $datas['nr_coment']='';
            }
        $this->ModeloCatalogos->updateCatalogo($tables,$datas,$wherearray);
        $this->ModeloCatalogos->db5_Insert('bitacora_confirmacion',array('tipo'=>$table,'personal'=>$this->idpersonal,'fecha'=>$this->fechahoy,'idser'=>$idrow,'info'=>'Reagendacion '.$infogi));
        //====================================================
            $array_where=array('v_ser_tipo'=>$table,'v_ser_id'=>$asig_id,'activo'=>1,'combinada'=>0);
            $rowresult_v = $this->ModeloCatalogos->getselectwheren('ventas',$array_where);
            foreach ($rowresult_v->result() as $item) {
                $idventa=$item->id;
                $fechaentregah=$item->fechaentregah.'<br> '.$item->fechaentrega;
                $this->ModeloCatalogos->updateCatalogo('ventas',array('fechaentrega'=>$fecha,'fechaentregah'=>$fechaentregah),array('id'=>$idventa));
            }
            $array_where=array('v_ser_tipo'=>$table,'v_ser_id'=>$asig_id,'activo'=>1);
            $rowresult_vc = $this->ModeloCatalogos->getselectwheren('ventacombinada',$array_where);
            foreach ($rowresult_vc->result() as $item) {
                $combinadaId=$item->combinadaId;
                $fechaentregah=$item->fechaentregah.'<br> '.$item->fechaentrega;
                $this->ModeloCatalogos->updateCatalogo('ventacombinada',array('fechaentrega'=>$fecha,'fechaentregah'=>$fechaentregah),array('combinadaId'=>$combinadaId));
            }
            if($table==1){
                /* esta misma consulta esta en el archivo asignacionestecnico.php y en el controlador calendario*/
                $sql2r="SELECT cf.*,c.modelo,c.parte,p.nombre,r.reg,cli.empresa,vdc.idVentas,ven.total_general,ven.combinada
                      FROM contrato_folio AS cf 
                      INNER JOIN consumibles AS c ON c.id = cf.idconsumible 
                      INNER JOIN rentas AS r ON r.id = cf.idrenta 
                      INNER JOIN personal AS p ON p.personalId = r.id_personal 
                      INNER JOIN clientes AS cli ON cli.id = r.idCliente 
                      INNER JOIN ventas_has_detallesConsumibles as vdc on vdc.foliotext=cf.foliotext
                      INNER JOIN ventas as ven on ven.id=vdc.idVentas
                      where cf.serviciocId='$asig_id' and cf.status<=1 and cf.serviciocId_tipo=0
                      group by vdc.idVentas";

                      $query2r = $this->db->query($sql2r);
                      foreach ($query2r->result() as $itemcv) {
                            $rowresult_v = $this->ModeloCatalogos->getselectwheren('ventas',array('id'=>$itemcv->idVentas));
                            foreach ($rowresult_v->result() as $item) {
                                $idventa=$item->id;
                                $fechaentregah=$item->fechaentregah.'<br> '.$item->fechaentrega;
                                $this->ModeloCatalogos->updateCatalogo('ventas',array('fechaentrega'=>$fecha,'fechaentregah'=>$fechaentregah),array('id'=>$itemcv->idVentas));
                            }
                        
                      }
            }
            if($table==2){
                $rowresultd = $this->ModeloCatalogos->getselectwheren('asignacion_ser_poliza_a',array('asignacionId'=>$asig_id));
                foreach ($rowresultd->result() as $item_ser) {
                    $polizaId=$item_ser->polizaId;
                    $rowresult_p = $this->ModeloCatalogos->getselectwheren('polizasCreadas',array('id'=>$polizaId));
                    foreach ($rowresult_p->result() as $itemp) {
                        $fechaentregah=$itemp->fechaentregah.'<br> '.$itemp->fechaentrega;
                        if($itemp->combinada==0){
                            $this->ModeloCatalogos->updateCatalogo('polizasCreadas',array('fechaentrega'=>$fecha,'fechaentregah'=>$fechaentregah),array('id'=>$polizaId));
                        }else{
                            $array_where=array('poliza'=>$polizaId,'activo'=>1);
                            $rowresult_vc = $this->ModeloCatalogos->getselectwheren('ventacombinada',$array_where);
                            foreach ($rowresult_vc->result() as $item) {
                                $combinadaId=$item->combinadaId;
                                $fechaentregah=$item->fechaentregah.'<br> '.$item->fechaentrega;
                                $this->ModeloCatalogos->updateCatalogo('ventacombinada',array('fechaentrega'=>$fecha,'fechaentregah'=>$fechaentregah),array('combinadaId'=>$combinadaId));
                            }
                        }
                    }
                }
            }
            
        //====================================================
    }
    function edittecnico(){
        $params = $this->input->post();
        $idrow = $params['idrow'];
        $idrowt = $params['idrowt'];
        $table = $params['table']; //1 contrato 2 poliza 3 cliente
        $tipo = $params['tipo'];//0 total 1 individual
        $tecnicoo = $params['tecnicoo'];
        $tecnico = $params['tecnico'];
        $tecnico2 = $params['tecnico2'];
        $tecnico3 = $params['tecnico3'];
        $tecnico4 = $params['tecnico4'];
        $tecnicos = $params['tecnico'];
        if ($table==1) {
            $tables='asignacion_ser_contrato_a_e';
            $idname='asignacionIde';
        }elseif ($table==2) {
            $tables='asignacion_ser_poliza_a_e';
            $idname='asignacionIde';
        }elseif ($table==3) {
            $tables='asignacion_ser_cliente_a';
            $idname='asignacionId';
            $idrow=$idrowt;
        }elseif ($table==4) {
            $tables='asignacion_ser_venta_a';
            $idname='asignacionId';
            $idrow=$idrowt;
        }
        $datarray_insert_historial_t=array();
        if ($tipo==1) {//individual
            $this->ModeloCatalogos->updateCatalogo($tables,array('tecnico'=>$tecnico,'tecnico2'=>$tecnico2,'tecnico3'=>$tecnico3,'tecnico4'=>$tecnico4),array($idname=>$idrow));
            //$this->ModeloCatalogos->db5_Insert('asignacion_ser_historial_t',array('asignacionId'=>$idrow,'tecnicoId'=>$tecnicoo,'tabless'=>$table));
            $datarray_insert_historial_t[]=array('asignacionId'=>$idrow,'tecnicoId'=>$tecnicoo,'tabless'=>$table);
            $this->ModeloAsignacion->agregaraitinerario($idrowt,$table);
            if ($table==3) {
                //log_message('error','idrow: '.$idrow.' idrowrow: '.$idrowrow);
                $this->ModeloCatalogos->updateCatalogo('asignacion_ser_cliente_a_d',array('tecnico'=>$tecnicos,'tecnico2'=>$tecnico2,'tecnico3'=>$tecnico3,'tecnico4'=>$tecnico4),array('asignacionIdd'=>$params['idrow']));
            }
        }else{//total
            $resultrow=$this->ModeloCatalogos->getselectwheren($tables,array('asignacionId'=>$idrowt));
            foreach ($resultrow->result() as $item) {
                if ($table==1) {
                    $idrowrow=$item->asignacionIde;
                }elseif ($table==2) {
                    $idrowrow=$item->asignacionIde;
                }elseif ($table==3) {
                    $idrowrow=$item->asignacionId;
                }elseif ($table==4) {
                    $idrowrow=$item->asignacionId;
                }
                if ($table==1) {
                    $this->ModeloCatalogos->db4_updateCatalogo($tables,array('nr'=>0,'nr_coment'=>''),array($idname=>$idrowrow));
                }elseif ($table==2) {
                    $this->ModeloCatalogos->db4_updateCatalogo($tables,array('nr'=>0,'nr_coment'=>''),array($idname=>$idrowrow));
                }elseif ($table==3) {
                    $this->ModeloCatalogos->db4_updateCatalogo('asignacion_ser_cliente_a_d',array('nr'=>0,'nr_coment'=>''),array($idname=>$idrowrow));
                    $this->ModeloCatalogos->updateCatalogo('asignacion_ser_cliente_a_d',array('tecnico'=>$tecnicos,'tecnico2'=>$tecnico2,'tecnico3'=>$tecnico3,'tecnico4'=>$tecnico4),array('asignacionId'=>$idrowrow));
                }elseif ($table==4) {
                    $this->ModeloCatalogos->db4_updateCatalogo($tables,array('nr'=>0,'nr_coment'=>''),array($idname=>$idrowrow));
                }
                $tecnico=$item->tecnico;
                $this->ModeloCatalogos->updateCatalogo($tables,array('tecnico'=>$tecnicos,'tecnico2'=>$tecnico2,'tecnico3'=>$tecnico3,'tecnico4'=>$tecnico4),array($idname=>$idrowrow));
                //$this->ModeloCatalogos->db5_Insert('asignacion_ser_historial_t',array('asignacionId'=>$idrowrow,'tecnicoId'=>$tecnicoo,'tabless'=>$table));
                $datarray_insert_historial_t[]=array('asignacionId'=>$idrowrow,'tecnicoId'=>$tecnicoo,'tabless'=>$table);

                $this->ModeloAsignacion->agregaraitinerario($idrowt,$table);
            }

            $inf_iti= $this->ModeloCatalogos->getselectwheren('unidades_bitacora_itinerario',array('personal'=>$tecnicos,'fecha'=>$this->fechaactual));
            if($inf_iti->num_rows()>0){
                $strq = "UPDATE unidades_bitacora_itinerario SET personal='$tecnicos' where asignacion_gen like '%|$table,$idrowt|%' and fecha='$this->fechaactual'";
                $this->db->query($strq);
            }else{
                $strq = "UPDATE unidades_bitacora_itinerario SET activo=0 where asignacion_gen like '%|$table,$idrowt|%' and fecha='$this->fechaactual'";
                $this->db->query($strq);
            }

            
        }
        if(count($datarray_insert_historial_t)>0){
            $this->ModeloCatalogos->db5_insert_batch('asignacion_ser_historial_t',$datarray_insert_historial_t);
        }
    }
    function deleteservicios(){
        $params     = $this->input->post();
        $idrow      = $params['idrow'];//individual
        $idrowt     = $params['idrowt'];//global
        $table      = $params['table']; //1 contrato 2 poliza 3 cliente 4 ventas
        $tipodelete = $params['tipodelete'];//1 individual 2 General
        $infodelete=$this->usuario.' '.$this->fechahoy;
        $estatusdelete=0;// 0 no se pudo eliminar 1 eliminado 2 tiene una factura
        if($table==1){
            if($tipodelete==1){
                $this->ModeloCatalogos->updateCatalogo('asignacion_ser_contrato_a_e',array('activo'=>0,'infodelete'=>$infodelete),array('asignacionIde'=>$idrow));

                $this->Modelobitacoras->Insert(array('contenido'=>'Se Elimini servicio: '.$idrow,'nombretabla'=>'asignacion_ser_contrato_a_e','idtable'=>$idrow,'tipo'=>'Delete','personalId'=>$this->idpersonal));
            }else{
                $this->ModeloCatalogos->updateCatalogo('asignacion_ser_contrato_a_e',array('activo'=>0,'infodelete'=>$infodelete),array('asignacionId'=>$idrowt));

                $this->Modelobitacoras->Insert(array('contenido'=>'Se Elimini servicios: '.$idrowt,'nombretabla'=>'asignacion_ser_contrato_a_e','idtable'=>$idrowt,'tipo'=>'Delete','personalId'=>$this->idpersonal));
            }
            $estatusdelete=1;
        }elseif ($table==2) {
            if($tipodelete==1){
                $this->ModeloCatalogos->updateCatalogo('asignacion_ser_poliza_a_e',array('activo'=>0,'infodelete'=>$infodelete),array('asignacionIde'=>$idrow));

                $this->Modelobitacoras->Insert(array('contenido'=>'Se Elimino servicio: '.$idrow,'nombretabla'=>'asignacion_ser_poliza_a_e','idtable'=>$idrow,'tipo'=>'Delete','personalId'=>$this->idpersonal));

                $datoinfoser= $this->ModeloCatalogos->getselectwheren('asignacion_ser_poliza_a_e',array('asignacionIde'=>$idrow));

            }else{
                $this->ModeloCatalogos->updateCatalogo('asignacion_ser_poliza_a_e',array('activo'=>0,'infodelete'=>$infodelete),array('asignacionId'=>$idrowt));

                $this->Modelobitacoras->Insert(array('contenido'=>'Se Elimino servicios: '.$idrow,'nombretabla'=>'asignacion_ser_poliza_a_e','idtable'=>$idrowt,'tipo'=>'Delete','personalId'=>$this->idpersonal));

                $datoinfoser= $this->ModeloCatalogos->getselectwheren('asignacion_ser_poliza_a_e',array('asignacionId'=>$idrowt));
            }
            $estatusdelete=1;
            $asignacionId=0;
            //==================================================
                foreach ($datoinfoser->result() as $itemser) {
                    $idvinculo=$itemser->idvinculo;
                    $asignacionId=$itemser->asignacionId;
                    if($idvinculo>0){
                        //if($itemser->nr==1){
                            //===============================================
                                $dat_dll_pol= $this->ModeloCatalogos->getselectwheren('polizasCreadas_has_detallesPoliza',array('id'=>$idvinculo));
                                foreach ($dat_dll_pol->result() as $itemserpol) {
                                    $vigencia_clicks = $itemserpol->vigencia_clicks;
                                    $idPoliza = $itemserpol->idPoliza;
                                    $dat_pol= $this->ModeloCatalogos->getselectwheren('ventacombinada',array('poliza'=>$idPoliza,'activo'=>1));
                                    $tiene_fac=0;
                                    if($dat_pol->num_rows()>0){
                                        foreach ($dat_pol->result() as $itemp) {
                                            $combinadaId=$itemp->combinadaId;
                                            $strq="SELECT fv.*, fac.FacturasId,fac.serie, fac.Folio, fac.Estado
                                            FROM factura_venta as fv 
                                            INNER JOIN f_facturas as fac on fac.FacturasId=fv.facturaId
                                            WHERE fv.ventaId='$combinadaId' AND fv.combinada=1 AND fac.Estado=1";
                                            $query_fv = $this->db->query($strq);
                                            if($query_fv->num_rows()>0){
                                                $tiene_fac=1;
                                                $estatusdelete=2;
                                            }
                                        }
                                    }else{
                                        $strq="SELECT fp.*, fac.FacturasId,fac.serie,fac.Folio,fac.Estado 
                                                FROM factura_poliza as fp 
                                                INNER JOIN f_facturas as fac on fac.FacturasId=fp.facturaId
                                                WHERE fp.polizaId='$idPoliza' AND fac.Estado=1";
                                            $query_fp = $this->db->query($strq);
                                            if($query_fp->num_rows()>0){
                                                $tiene_fac=1;
                                                $estatusdelete=2;
                                            }
                                    }
                                    if($vigencia_clicks>1){
                                        $tiene_fac=1;
                                    }
                                    if($tiene_fac==0){
                                        // esta comentada asta por el momento para que se suba
                                        $this->ModeloCatalogos->Insert('polizasCreadas_has_detallesPoliza_dev',$itemserpol);
                                        // esta comentada asta por el momento para que se suba
                                        $this->ModeloCatalogos->getdeletewheren('polizasCreadas_has_detallesPoliza',array('id'=>$idvinculo));
                                        $estatusdelete=1;
                                    }


                                    
                                }
                                
                            //================================================
                        //}
                    }else{

                    }
                    // code...
                }
                if($estatusdelete==1){
                    $dat_info_ser_d= $this->ModeloCatalogos->getselectwheren('asignacion_ser_poliza_a_e',array('asignacionId'=>$asignacionId,'activo'=>1));
                    if($dat_info_ser_d->num_rows()==0){
                        $dat_info_ser= $this->ModeloCatalogos->getselectwheren('asignacion_ser_poliza_a',array('asignacionId'=>$asignacionId));
                            foreach ($dat_info_ser->result() as $item_ser_p) {
                                $polizaId = $item_ser_p->polizaId;
                                //===================================================
                                    $dat_pol= $this->ModeloCatalogos->getselectwheren('ventacombinada',array('poliza'=>$polizaId,'activo'=>1));
                                    $tiene_fac=0;
                                    if($dat_pol->num_rows()>0){
                                        foreach ($dat_pol->result() as $itemp) {
                                            $combinadaId=$itemp->combinadaId;
                                            $strq="SELECT fv.*, fac.FacturasId,fac.serie, fac.Folio, fac.Estado
                                            FROM factura_venta as fv 
                                            INNER JOIN f_facturas as fac on fac.FacturasId=fv.facturaId
                                            WHERE fv.ventaId='$combinadaId' AND fv.combinada=1 AND fac.Estado=1";
                                            $query_fv = $this->db->query($strq);
                                            if($query_fv->num_rows()>0){
                                                $tiene_fac=1;
                                                $estatusdelete=2;
                                            }
                                        }
                                    }else{
                                        $strq="SELECT fp.*, fac.FacturasId,fac.serie,fac.Folio,fac.Estado 
                                                FROM factura_poliza as fp 
                                                INNER JOIN f_facturas as fac on fac.FacturasId=fp.facturaId
                                                WHERE fp.polizaId='$polizaId' AND fac.Estado=1";
                                            $query_fp = $this->db->query($strq);
                                            if($query_fp->num_rows()>0){
                                                $tiene_fac=1;
                                                $estatusdelete=2;
                                            }
                                    }
                                //===================================================
                                if($estatusdelete==1){
                                    $this->ModeloCatalogos->updateCatalogo('polizasCreadas',array('activo'=>0,'info5'=>$infodelete.' Eliminacion automatica por eliminacion de servicio'),array('id'=>$polizaId,'viewcont'=>0));
                                }
                            }
                    }
                }
            //==================================================
        }elseif ($table==3) {
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_cliente_a_d',array('activo'=>0,'infodelete'=>$infodelete),array('asignacionId'=>$idrow));

            $this->Modelobitacoras->Insert(array('contenido'=>'Se Elimini servicios: '.$idrow,'nombretabla'=>'asignacion_ser_cliente_a_d','idtable'=>$idrow,'tipo'=>'Delete','personalId'=>$this->idpersonal));
            $estatusdelete=1;
        }elseif ($table==4) {
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_venta_a',array('activo'=>0,'infodelete'=>$infodelete),array('asignacionId'=>$idrow));

            $this->Modelobitacoras->Insert(array('contenido'=>'Se Elimini servicios: '.$idrow,'nombretabla'=>'asignacion_ser_venta_a','idtable'=>$idrow,'tipo'=>'Delete','personalId'=>$this->idpersonal));
            $estatusdelete=1;
        }
        $array =array(
                    'estado'=>$estatusdelete
                    );
        echo json_encode($array);

    }
    function viewhistorial(){
        $params = $this->input->post();
        $asi =$params['idrow'];
        $table =$params['table'];
        $resultrow = $this->ModeloServicio->historiatecnicos($asi,$table);
        $html='<table class="table">';
        foreach ($resultrow->result() as $item) {
            $html.='<tr>
                        <td>'.$item->nombre.' '.$item->apellido_paterno.' '.$item->apellido_materno.'</td>
                        <td>'.$item->reg.'</td>
                    </tr>
                    ';
        }
        $html.='</table>';
        echo $html;
    }
    function suspender(){
        $params = $this->input->post();
        $idrow =$params['idrow'];
        $table =$params['table'];
        $tserviciomotivo =$params['tserviciomotivo'];
        if ($table==1) {
            $tables='asignacion_ser_contrato_a_e';
            $idname='asignacionIde';
        }elseif ($table==2) {
            $tables='asignacion_ser_poliza_a_e';
            $idname='asignacionIde';
        }elseif ($table==3) {
            $tables='asignacion_ser_cliente_a';
            $idname='asignacionId';
        }elseif ($table==4) {
            $tables='asignacion_ser_venta_a';
            $idname='asignacionId';
        }
        $datas=array(
            'motivosuspencion'=>$tserviciomotivo,
            'status'=>3
        );
        $this->ModeloCatalogos->updateCatalogo($tables,$datas,array($idname=>$idrow));
    }
    function suspenderactivar(){
        $params = $this->input->post();
        $idrow =$params['idrow'];
        $table =$params['table'];
        $tserviciomotivo =$params['tserviciomotivo'];
        if ($table==1) {
            $tables='asignacion_ser_contrato_a_e';
            $idname='asignacionIde';
        }elseif ($table==2) {
            $tables='asignacion_ser_poliza_a_e';
            $idname='asignacionIde';
        }elseif ($table==3) {
            $tables='asignacion_ser_cliente_a';
            $idname='asignacionId';
        }elseif ($table==4) {
            $tables='asignacion_ser_venta_a';
            $idname='asignacionId';
        }
        $datas=array(
            'motivosuspencion'=>$tserviciomotivo,
            'status'=>0
        );
        $this->ModeloCatalogos->updateCatalogo($tables,$datas,array($idname=>$idrow));
    }
    function datosventaservicio(){
        $params = $this->input->post();
        $id =$params['id'];
        $tipo =$params['tipo'];
        $resultadoconsumibles=$this->Ventas_model->datosserviciosconsumibles($id,$tipo);
        $resultadorefacciones=$this->Ventas_model->datosserviciosrefacciones($id,$tipo);
        $array = array(
                    'consumibles'=>$resultadoconsumibles->result(),
                    'refacciones'=>$resultadorefacciones->result()
        );
        echo json_encode($array);
    }
    function editarventaservicio(){
        $params = $this->input->post();
        $tasign=$params['tasign'];
        $tipo=$params['tipo'];
        $vconsumible=$params['vconsumible'];
        $vrefaccion  =$params['vrefaccion'];

        $DATAvc = json_decode($vconsumible);
        for ($s=0;$s<count($DATAvc);$s++){
            if ($DATAvc[$s]->id > 0) {

                $this->ModeloCatalogos->updateCatalogo('asignacion_ser_avc',array('cantidad'=>$DATAvc[$s]->cantidad),array('asId'=>$DATAvc[$s]->id));
            }else{
                $datavca =array(
                    'asignacionId'=>$tasign,
                    'cantidad'=>$DATAvc[$s]->cantidad,
                    'consumible'=>$DATAvc[$s]->consumible,
                    'costo'=>$DATAvc[$s]->precio,
                    'tipo'=>$tipo
                );
                $this->ModeloCatalogos->Insert('asignacion_ser_avc',$datavca);
            }
            
        }
        $DATAvr = json_decode($vrefaccion);
        for ($c=0;$c<count($DATAvr);$c++){
            if ($DATAvr[$c]->id > 0) {

                $this->ModeloCatalogos->updateCatalogo('asignacion_ser_avr',array('cantidad'=>$DATAvr[$c]->cantidad),array('asId'=>$DATAvr[$c]->id));
            }else{
                $datavcra =array(
                    'asignacionId'=>$tasign,
                    'cantidad'=>$DATAvr[$c]->cantidad,
                    'refacciones'=>$DATAvr[$c]->refaccion,
                    'costo'=>$DATAvr[$c]->precio,
                    'tipo'=>$tipo
                );
                $this->ModeloCatalogos->Insert('asignacion_ser_avr',$datavcra);
            }
            
        }
    }
    function deleteacre(){
        $params = $this->input->post();
        $rowid = $params['rowid'];
        $tipo = $params['tipo'];
        if ($tipo==1) {//consumible
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_avc',array('activo'=>0),array('asId'=>$rowid));
        }
        if ($tipo==2) {//refaccion
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_avr',array('activo'=>0),array('asId'=>$rowid));
        }
        
    }
    function cancelartotalvs(){
        $params = $this->input->post();
        $tasign = $params['tasign'];
        $this->ModeloCatalogos->updateCatalogo('asignacion_ser_avc',array('activo'=>0),array('asignacionId'=>$tasign));
        $this->ModeloCatalogos->updateCatalogo('asignacion_ser_avr',array('activo'=>0),array('asignacionId'=>$tasign));
    }
    public function contrato_formato($id,$tipo,$equipo,$tipov=0){
        $data['tpoliza']=0;
        $data['idRenta']=0;$bodega_or='';$bodega_or_name='';$bodegaId=0;
        $data['configuracionCotizacion'] = $this->ConfiguracionDocumentos_Model->getConfiguracion(1);
        $datosequiporow = $this->ModeloCatalogos->getselectwheren('asignacion_ser_contrato_a_e',array('asignacionIde'=>$equipo));
        $comentariotecnico='';
        foreach ($datosequiporow->result() as $item) {
            $idequipo = $item->idequipo;
            $serieId = $item->serieId;
            $asignacionId = $item->asignacionId;
            $lectu_ini = $item->contadorini;
            $lectur_fin = $item->contadorfin;
            if($item->prioridad>0){
                if($item->cot_refaccion!=''){
                    $comentariotecnico.='Cotizacion: Prioridad '.$item->prioridad.' '.$item->cot_refaccion.'<br>';
                }
                if($item->cot_refaccion2!=''){
                    $comentariotecnico.='Cotizacion: Prioridad '.$item->prioridad.' '.$item->cot_refaccion2.'<br>';
                }
                if($item->cot_refaccion3!=''){
                    $comentariotecnico.='Cotizacion: Prioridad '.$item->prioridad.' '.$item->cot_refaccion3.'<br>';
                }
                
            }
            if($item->comentario!=''){
                $comentariotecnico.='Observación General: '.$item->comentario.'<br>';
            }
            if($item->comentario_tec!=''){
                $comentariotecnico.='Observación Equipo: '.$item->comentario_tec.'<br>';
            }
            $qrecibio = $item->qrecibio;
        }
        //=====================================
            $datosequiporowa = $this->ModeloCatalogos->getselectwheren('asignacion_ser_contrato_a',array('asignacionId'=>$asignacionId));
            $comentariocal='';
            $servicio_equipo_i='';
            $servicio_doc_i='';
            foreach ($datosequiporowa->result() as $item) {
                $comentariocal = $item->comentariocal;
                $data['tpoliza']=$item->tpoliza;
                if($item->equipo_acceso_info!=null){
                   $servicio_equipo_i = $item->equipo_acceso_info;
                   
                }
                if($item->documentacion_acceso_info!=null){ 
                   $servicio_doc_i = $item->documentacion_acceso_info;
                }
            }
        //=====================================
        $data['id_equipo']=$idequipo;
        $rowequipos = $this->ModeloCatalogos->getselectwheren('rentas_has_detallesEquipos',array('id'=>$idequipo));
        foreach ($rowequipos->result() as $item) {
            $data['idRenta']= $item->idRenta;
            $data['idEquipo']=$item->idEquipo;
            $modelo = $item->modelo;
        }
        $data['serieId']=$serieId;
        $rowseries = $this->ModeloCatalogos->getselectwheren('series_productos',array('serieId'=>$serieId));
        foreach ($rowseries->result() as $item) {
            $serie = $item->serie;
            $bodegaId=$item->bodegaId;
        }
        if($bodegaId>0){
             $rowbod = $this->ModeloCatalogos->getselectwheren('bodegas',array('bodegaId'=>$bodegaId));
            foreach ($rowbod->result() as $item) {
                $bodega_or_name = $item->bodega;
            }
        }
        $rowseriesinfo = $this->ModeloCatalogos->getselectwheren('asignacion_series_r_equipos',array('serieId'=>$serieId,'id_equipo'=>$idequipo));
        $comentarioinfo='';
        foreach ($rowseriesinfo->result() as $item) {
            $comentarioinfo = $item->comentario;
            $bodega_or = $item->bodega_or;
        }
        if($bodega_or>0){
             $rowbod = $this->ModeloCatalogos->getselectwheren('bodegas',array('bodegaId'=>$bodega_or));
            foreach ($rowbod->result() as $item) {
                $bodega_or_name = $item->bodega;
            }
        }
        $datos = $this->ModeloCatalogos->doc_contrato($id);
        //$datosconsumiblesfolio = $this->ModeloServicio->getfoliosservicios($id);
        //$te_cel = $this->ModeloCatalogos->doc_cliente_tel_cel($datos->id);
        $te_cel = $this->ModeloCatalogos->doc_cliente_tel_cel2($datos->id);
        $doc_ren = $this->ModeloCatalogos->doc_rectas($datos->contratoId);
        $get_contrato=$this->ModeloCatalogos->contrato_equipo($datos->contratoId,$idequipo,$serieId);
        $text_prioridad='';
        if ($datos->prioridad==1){
            $text_prioridad='Prioridad 1';
        }else if($datos->prioridad==2){
            $text_prioridad='Prioridad 2';
        }else if($datos->prioridad==3){
            $text_prioridad='Prioridad 3';
        }else if($datos->prioridad==4){
            $text_prioridad='Servicio Regular';
        }else{
            $text_prioridad='';
        } 
        $getdire = '_';
        if(isset($get_contrato->direccion)){
            $porciones = explode("_", $get_contrato->direccion);
            if($porciones[0]=='g'){
                $getdire = $this->ModeloCatalogos->clientes_direccion($porciones[1]);
                $getdire=$getdire->direccion;
            }else if($porciones[0]=='f'){
                $getdire = $this->ModeloCatalogos->clientes_direcc_fiscal($porciones[1]);
                $getdire=$getdire->direccion;
            }
        }
        $get_reg = explode(" ", $datos->reg);

        $contacto=$datos->contacto;
        $puesto=$datos->cargoarea;
        $tel_local=$datos->telefono;
        $celular='';
        if($datos->id_contacto_new>0){
            $row_dc = $this->ModeloCatalogos->getselectwheren('cliente_datoscontacto',array('datosId'=>$datos->id_contacto_new));
            foreach ($row_dc->result() as $item_dc) {
                $contacto=$item_dc->atencionpara;
                $puesto=$item_dc->puesto;
                $tel_local=$item_dc->telefono;
                $celular=$item_dc->celular;
            }
        }
        if (isset($doc_ren->servicio_equipo)) {
            $servicio_equipo=$doc_ren->servicio_equipo;
        }else{
            $servicio_equipo='';
        }
        if (isset($doc_ren->servicio_doc)) {
            $servicio_doc=$doc_ren->servicio_doc;
        }else{
            $servicio_doc='';
        }
        if(isset($servicio_equipo_i)){
            $servicio_equipo=$servicio_equipo_i;
        }
        if(isset($servicio_doc_i)){
            $servicio_doc=$servicio_doc_i;
        }

        //==============================================================================
            /*
            $lectu_ini='';
            $lectur_fin='';
            $sqlr="SELECT ard.c_c_i,ard.c_c_f 
                    FROM alta_rentas_prefactura_d as ard
                    inner JOIN alta_rentas_prefactura as ar on ar.prefId=ard.prefId
                    WHERE ard.productoId=$idequipo and '".$datos->fecha."' BETWEEN ar.periodo_inicial AND ar.periodo_final";
            $query = $this->db->query($sqlr);
            foreach ($query->result() as $item) {
                $lectu_ini=$item->c_c_i;
                $lectur_fin=$item->c_c_f;
            }
            */
        //==============================================================================
            $data['idpdf']=$id;
            $data['tipopdf']=$tipo;
            $data['equipopdf']=$equipo;

            $data['fecha_crea']=$get_reg[0];
            $data['fecha_asig']=$datos->fecha;
            if($datos->tservicio_a>0){
                $data['tipo_serv']=$datos->poliza;
                //$tipoppp=0;
            }else{
                $data['tipo_serv']=$datos->servicio;
                //$tipoppp=1;
            }
            //$data['tipo_serv']=$tipoppp;
            $data['responsable_asig']=$datos->tecnico;
            if($datos->per_fin>0){
                $data['responsable_asig']=$datos->tecnicofin;
            }
            $data['hora_asig']=$datos->hora;
            $data['prioridad']=$text_prioridad;
            $data['descripcion_serv_fall']=$datos->tserviciomotivo;
            $data['modelo']=$modelo;
            $data['serie']=$serie;
            $data['serieId']=$serieId;
            $data['nombre_empresa']=$datos->empresa;
            $data['direcc_inst_serv']=$getdire;
            
            $data['contacto']=$contacto;
            $data['puesto']=$puesto;
            $data['tel']=$tel_local;
            $data['cel']=$celular;
            $equipo_acceso=$datos->equipo_acceso;//info clientes
            $documentacion_acceso=$datos->documentacion_acceso;//info clientes
            if($servicio_equipo_i!=''){
                $equipo_acceso=$servicio_equipo_i;//info clientes
            }
            if($servicio_doc_i!=''){
                $documentacion_acceso=$servicio_doc_i;//info clientes
            }
            $docum_acce=$equipo_acceso.' '.$documentacion_acceso;
            $data['docum_acce']=$docum_acce;
            $data['observa_edi']=$datos->comentario;
            $data['lectu_ini']=$lectu_ini;
            $data['km_veh_i']='';
            $data['lectur_fin']=$lectur_fin;
            $data['km_veh_f']='';
            $data['asesor_ser']=$datos->tecnico;
            $data['gerencia']='';
            $data['firma']=$datos->firma;
            $data['qrecibio']=$qrecibio;
            $data['comentarioejecutiva']=$comentariocal;
            $data['comentariotecnico']=$comentariotecnico;
            $data['comentarioinfo']=$comentarioinfo;
            $data['bodega_or']=$bodega_or;
            $data['bodega_or_name']=$bodega_or_name;
        //$data['datosconsumiblesfolio'] = $datosconsumiblesfolio;
            if($tipov==1){
                $this->load->view('Reportes/contrato',$data);       
            }else{
                $this->load->view('Reportes/prefactura_servicio_c',$data);     
            }
        
    }    
    public function tipo_formato($id,$tipo,$ide){
        $data['configuracionCotizacion'] = $this->ConfiguracionDocumentos_Model->getConfiguracion(1);
        if($tipo==2){//Poliza
            $datos = $this->ModeloCatalogos->doc_poliza($id,$ide);
            //$doc_polizas_cr = $this->ModeloCatalogos->doc_polizas_c($datos->polizaId);
            //$te_cel = $this->ModeloCatalogos->doc_cliente_tel_cel($datos->id);
            $te_cel = $this->ModeloCatalogos->doc_cliente_tel_cel2($datos->id);
            $text_prioridad='';
            if ($datos->prioridad==1){
                $text_prioridad='Prioridad 1';
            }else if($datos->prioridad==2){
                $text_prioridad='Prioridad 2';
            }else if($datos->prioridad==3){
                $text_prioridad='Prioridad 3';
            }else if($datos->prioridad==4){
                $text_prioridad='Servicio Regular';
            }else{
                $text_prioridad='';
            }
            if (isset($te_cel->atencionpara)) {
                $atencionpara=$te_cel->atencionpara;
            }else{
                $atencionpara='';
            }
            if (isset($te_cel->puesto)) {
                $puesto=$te_cel->puesto;
            }else{
                $puesto='';
            }
            if (isset($te_cel->telefono)) {
                $tel_local=$te_cel->telefono;
            }else{
                $tel_local='';
            }
            if (isset($te_cel->celular)) {
                $celular=$te_cel->celular;
            }else{
                $celular='';
            }
             if (isset($te_cel->email)) {
                $email=$te_cel->email;
            }else{
                $email='';
            }
            if($datos->id_contacto_new>0){
                $row_dc = $this->ModeloCatalogos->getselectwheren('cliente_datoscontacto',array('datosId'=>$datos->id_contacto_new));
                foreach ($row_dc->result() as $item_dc) {
                    $atencionpara=$item_dc->atencionpara;
                    $puesto=$item_dc->puesto;
                    $tel_local=$item_dc->telefono;
                    $celular=$item_dc->celular;
                }
            }
            $get_reg = explode(" ", $datos->reg);
            $data['reg']=$datos->reg;
            $data['fecha_crea']=$get_reg[0];
            $data['fecha_asig']=$datos->fecha;
            $data['tipo_serv']=$datos->servicio.''.$datos->polizaser;
            $data['responsable_asig']=$datos->tecnico;
            if($datos->per_fin>0){
                $data['responsable_asig']=$datos->tecnicofin;
            }
            $data['hora_asig']=$datos->hora;
            $data['prioridad']=$text_prioridad;
            $data['descripcion_serv_fall']=$datos->comentariopre;
            $data['modelo']=$datos->modelo;
            $data['serie']=$datos->serie;
            $data['nombre_empresa']=$datos->empresa;
            $data['direcc_inst_serv']=$datos->direccionservicio;
            
            $data['atencionpara']=$atencionpara;
            $data['puesto']=$puesto;
            $data['telefono']=$tel_local;
            $data['celular']=$celular;

            $data['tel']=$tel_local;
            $data['cel']=$celular;
            $data['correo']=$email;
            
            $equipo_acceso=$datos->equipo_acceso;//info clientes
            $documentacion_acceso=$datos->documentacion_acceso;//info clientes
            //log_message('error','$equipo_acceso:'.$equipo_acceso.' $documentacion_acceso:'.$documentacion_acceso);
            if($datos->equipo_acceso_info!=null){
                $equipo_acceso=$datos->equipo_acceso_info;//info clientes
            }
            if($datos->documentacion_acceso_info!=null){
                $documentacion_acceso=$datos->documentacion_acceso_info;//info clientes
            }
            //log_message('error','$equipo_acceso:'.$equipo_acceso.' $documentacion_acceso:'.$documentacion_acceso);
            $docum_acce=$equipo_acceso.' '.$documentacion_acceso;
            $data['docum_acce']=$docum_acce;
            $data['observa_edi']=$datos->comentario;
            $data['lectu_ini']='';
            $data['km_veh_i']='';
            $data['lectur_fin']='';
            $data['km_veh_f']='';
            $data['asesor_ser']=$datos->tecnico;
            $data['gerencia']='';
            $data['firma']=$datos->firma;
            $data['qrecibio']=$datos->qrecibio;
            $data['comentarioejecutiva']=$datos->comentariocal;
            $data['comentariocal']=$datos->comentariocal;
            $data['poliza']=$datos->polizaser;
            $data['tserviciomotivo']=$datos->tserviciomotivo;
            $data['comentario']=$datos->comentario;
            $comentariotecnico='';
            if($datos->prioridadape>0){
                if($datos->cot_refaccion!=''){
                    $comentariotecnico.='Cotizacion: Prioridad '.$datos->prioridadape.' '.$datos->cot_refaccion.'<br>';
                }
                if($datos->cot_refaccion2!=''){
                    $comentariotecnico.='Cotizacion: Prioridad '.$datos->prioridadape.' '.$datos->cot_refaccion2.'<br>';
                }
                if($datos->cot_refaccion3!=''){
                    $comentariotecnico.='Cotizacion: Prioridad '.$datos->prioridadape.' '.$datos->cot_refaccion3.'<br>';
                }
            }
            if($datos->comentario!=''){
                //$comentariotecnico.='Observación General: '.$datos->comentario.'<br>';
            }
            if($datos->comentario_tec!=''){
                //$comentariotecnico.='Observación Equipo: '.$datos->comentario_tec.'<br>';
                $comentariotecnico.=$datos->comentario_tec;
            }
            //log_message('error',$comentariotecnico);
            $data['comentariotecnico']=$comentariotecnico;
            $data['comentario_tec']=$comentariotecnico;

        }else{
            $datos = $this->ModeloCatalogos->doc_servicio($id);
            $te_cel = $this->ModeloCatalogos->doc_cliente_tel_cel($datos->id);
            $text_prioridad='';
            if ($datos->prioridad==1){
                $text_prioridad='Mismo día';
            }else if($datos->prioridad==1){
                $text_prioridad='Día siguiente';
            }else if($datos->prioridad==1){
                $text_prioridad='15 días';
            }else if($datos->prioridad==1){
                $text_prioridad='Servicio Regular';
            }else{
                $text_prioridad='';
            }
            $get_reg = explode(" ", $datos->reg);
            $data['fecha_crea']=$get_reg[0];
            $data['fecha_asig']=$datos->fecha;
            $data['tipo_serv']=$datos->servicio;
            $data['responsable_asig']=$datos->tecnico;
            $data['hora_asig']=$datos->hora;
            $data['prioridad']=$text_prioridad;
            $data['descripcion_serv_fall']=$datos->tserviciomotivo;
            $data['modelo']='';
            $data['serie']='';
            $data['nombre_empresa']=$datos->empresa;
            $data['direcc_inst_serv']='';
            $data['tel']=$te_cel->tel_local;
            $data['cel']=$te_cel->celular;
            $data['correo']=$datos->email;
            $data['docum_acce']='';
            $data['observa_edi']='';
            $data['lectu_ini']='';
            $data['km_veh_i']='';
            $data['lectur_fin']='';
            $data['km_veh_f']='';
            $data['asesor_ser']='';
            $data['gerencia']='';
            $data['firma']=$datos->firma;
        }
        
        $this->load->view('Reportes/porevento',$data); 
    } 
    public function tipo_formatoc($id,$tipo,$serve){
        $data['configuracionCotizacion'] = $this->ConfiguracionDocumentos_Model->getConfiguracion(1);
        $data['datosdeta'] = $this->ModeloCatalogos->descripcionpolizadetalle($id,$serve);
        $comentariotecnico='';
        if($tipo==2){//Poliza
            $datos = $this->ModeloCatalogos->doc_poliza($id);

            $doc_polizas_cr = $this->ModeloCatalogos->doc_polizas_c($datos->polizaId);
            $te_cel = $this->ModeloCatalogos->doc_cliente_tel_cel($datos->id);
            $text_prioridad='';
            if ($datos->prioridad==1){
                $text_prioridad='Prioridad 1';
            }else if($datos->prioridad==2){
                $text_prioridad='Prioridad 2';
            }else if($datos->prioridad==3){
                $text_prioridad='Prioridad 3';
            }else if($datos->prioridad==4){
                $text_prioridad='Servicio Regular';
            }else{
                $text_prioridad='';
            }
            if (isset($te_cel->tel_local)) {
                $tel_local=$te_cel->tel_local;
            }else{
                $tel_local='';
            }
            if (isset($te_cel->celular)) {
                $celular=$te_cel->celular;
            }else{
                $celular='';
            }
            
            $get_reg = explode(" ", $datos->reg);
            $data['fecha_crea']=$get_reg[0];
            $data['fecha_asig']=$datos->fecha;
            $data['tipo_serv']=$datos->servicio;
            $data['responsable_asig']=$datos->tecnico;
            $data['hora_asig']=$datos->hora;
            $data['prioridad']=$text_prioridad;
            $data['descripcion_serv_fall']=$datos->tserviciomotivo;
            $data['modelo']=$datos->modelo;
            $data['serie']=$datos->serie;
            $data['nombre_empresa']=$datos->empresa;
            $data['direcc_inst_serv']=$doc_polizas_cr->direccionservicio;
            $data['tel']=$tel_local;
            $data['cel']=$celular;
            $data['correo']=$datos->email;
            $data['docum_acce']='';
            $data['observa_edi']=$datos->comentario;
            $data['lectu_ini']='';
            $data['km_veh_i']='';
            $data['lectur_fin']='';
            $data['km_veh_f']='';
            $data['asesor_ser']=$datos->tecnico;
            $data['gerencia']='';
            $data['firma']=$datos->firma;
            $data['qrecibio']=$datos->qrecibio;
        }else{
            $datos = $this->ModeloCatalogos->doc_servicio($id,$serve);
            $te_cel = $this->ModeloCatalogos->doc_cliente_tel_cel2($datos->id);
            $text_prioridad='';
            if ($datos->prioridad==1){
                $text_prioridad='Prioridad 1';
            }else if($datos->prioridad==2){
                $text_prioridad='Prioridad 2';
            }else if($datos->prioridad==3){
                $text_prioridad='Prioridad 3';
            }else if($datos->prioridad==4){
                $text_prioridad='Servicio Regular';
            }else{
                $text_prioridad='';
            }
            $get_reg = explode(" ", $datos->reg);
            $data['reg']=$datos->reg;
            $data['fecha_crea']=$get_reg[0];
            $data['fecha_asig']=$datos->fecha;
            $data['tipo_serv']=$datos->servicio;
            $data['responsable_asig']=$datos->tecnico;

            $data['hora_asig']=$datos->hora;
            $data['prioridad']=$text_prioridad;
            $data['descripcion_serv_fall']=$datos->tserviciomotivo;
            $data['tserviciomotivo']=$datos->tserviciomotivo;
            $data['modelo']='';
            $data['serie']='';
            $data['nombre_empresa']=$datos->empresa;
            $data['direcc_inst_serv']='';
            $data['poliza']=$datos->poliza;
            if (isset($te_cel->telefono)) {
                $telefono=$te_cel->telefono;
            }else{
                $telefono='';
            }
            if (isset($te_cel->celular)) {
                $celular=$te_cel->celular;
            }else{
                $celular='';
            }
            if (isset($te_cel->email)) {
                $email=$te_cel->email;
            }else{
                $email='';
            }
            $data['atencionpara']='';
            $data['email']='';
            $data['telefono']='';
            $data['celular']='';
            $data['puesto']='';
            if($datos->id_contacto_new>0){
                $res_dc = $this->ModeloCatalogos->getselectwheren('cliente_datoscontacto',array('datosId'=>$datos->id_contacto_new));
                foreach ($res_dc->result() as $item_dc) {
                    $data['atencionpara']=$item_dc->atencionpara;
                    $data['email']=$item_dc->email;
                    $data['telefono']=$item_dc->telefono;
                    $data['celular']=$item_dc->celular;
                    $data['puesto']=$item_dc->puesto;
                }
            }
            $equipo_acceso=$datos->equipo_acceso;//info clientes
            $documentacion_acceso=$datos->documentacion_acceso;//info clientes
            if($datos->equipo_acceso_info!=null){
                    $equipo_acceso=$item->equipo_acceso_info;//info clientes
            }
            if($datos->documentacion_acceso_info!=null){
                $documentacion_acceso=$item->documentacion_acceso_info;//info clientes
            }

            $docum_acce=$datos->documentaccesorio;
            $docum_acce.=$equipo_acceso.' '.$documentacion_acceso;

            $data['tel']=$telefono;
            $data['cel']=$celular;
            //$data['tel']='';//componer
            //$data['cel']='';//componer
            $data['correo']=$email;
            $data['docum_acce']=$docum_acce;
            $data['observa_edi']=$datos->comentario;
            $data['comentario']=$datos->comentario;
            $data['lectu_ini']='';
            $data['km_veh_i']='';
            $data['lectur_fin']='';
            $data['km_veh_f']='';
            $data['asesor_ser']=$datos->tecnico;
            if($datos->per_fin>0){
                $data['asesor_ser']=$datos->tecnicofin;
            }
            $data['gerencia']='';
            $data['firma']=$datos->firma;
            //log_message('error','q1:'.$datos->qrecibio.'  q2:'.$datos->qrecibio2);
            if($datos->qrecibio2!='null' && $datos->qrecibio2!=null && $datos->qrecibio2!=''){
                $qrecibio=$datos->qrecibio2;

            }else{
                $qrecibio=$datos->qrecibio;
            }
            $data['qrecibio']=$qrecibio;
            $data['comentariocal']=$datos->comentariocal;
            $data['comentario_tec']=$datos->comentario_tec;

            $data['comentarioejecutiva']=$datos->comentariocal;
            if($datos->prioridadape>0){
                if($datos->cot_refaccion!=''){
                    $comentariotecnico.='Cotizacion: Prioridad '.$datos->prioridadape.' '.$datos->cot_refaccion.'<br>';
                }
                if($datos->cot_refaccion2!=''){
                    $comentariotecnico.='Cotizacion: Prioridad '.$datos->prioridadape.' '.$datos->cot_refaccion2.'<br>';
                }
                if($datos->cot_refaccion3!=''){
                    $comentariotecnico.='Cotizacion: Prioridad '.$datos->prioridadape.' '.$datos->cot_refaccion3.'<br>';
                }
            }
            
            $comentariotecnico.=$datos->comentario;
            $data['comentariotecnico']=$comentariotecnico;
        }
        
        $this->load->view('Reportes/poreventoc',$data); 
    }  
    public function tipo_formatovent($id,$tipo){
        $data['configuracionCotizacion'] = $this->ConfiguracionDocumentos_Model->getConfiguracion(1);
        $data['datosdeta'] = $this->ModeloCatalogos->descripcionpolizaventadetalle($id);
        $comentariotecnico='';
        if($tipo==4){//Poliza
            //$datos = $this->ModeloCatalogos->doc_poliza($id);
            $datos = $this->ModeloAsignacion->getdatosventas($id);
            $datos=$datos->row();
            //$doc_polizas_cr = $this->ModeloCatalogos->doc_polizas_c($datos->polizaId);
            $te_cel = $this->ModeloCatalogos->doc_cliente_tel_cel($datos->id);
            $text_prioridad='';
            if ($datos->prioridad==1){
                $text_prioridad='Prioridad 1';
            }else if($datos->prioridad==2){
                $text_prioridad='Prioridad 2';
            }else if($datos->prioridad==3){
                $text_prioridad='Prioridad 3';
            }else if($datos->prioridad==4){
                $text_prioridad='Servicio Regular';
            }else{
                $text_prioridad='';
            }
            if (isset($te_cel->tel_local)) {
                $tel_local=$te_cel->tel_local;
            }else{
                $tel_local='';
            }
            if (isset($te_cel->celular)) {
                $celular=$te_cel->celular;
            }else{
                $celular='';
            }
            
            $get_reg = explode(" ", $datos->reg);
            $data['reg']=$datos->reg;
            $data['fecha_crea']=$get_reg[0];
            $data['fecha_asig']=$datos->fecha;
            $data['tipo_serv']=$datos->servicio;
            $data['responsable_asig']=$datos->tecnico;
            $data['hora_asig']=$datos->hora;
            $data['prioridad']=$text_prioridad;
            $data['descripcion_serv_fall']=$datos->tserviciomotivo;
            //$data['modelo']=$datos->modelo;
            $data['serie']=$datos->serie;
            $data['nombre_empresa']=$datos->empresa;
            $data['direcc_inst_serv']=$datos->direccion;
            $data['tel']=$tel_local;
            $data['cel']=$celular;
            $data['correo']=$datos->email;
            
            $equipo_acceso=$datos->equipo_acceso;//info clientes
            $documentacion_acceso=$datos->documentacion_acceso;//info clientes
            if($datos->equipo_acceso_info!=null){
                $equipo_acceso=$datos->equipo_acceso_info;//info clientes
            }
            if($datos->documentacion_acceso_info!=null){
                $documentacion_acceso=$datos->documentacion_acceso_info;//info clientes
            }
            $docum_acce=$equipo_acceso.' '.$documentacion_acceso;
            $data['docum_acce']=$docum_acce;
            $data['observa_edi']=$datos->tserviciomotivo;
            $data['lectu_ini']='';
            $data['km_veh_i']='';
            $data['lectur_fin']='';
            $data['km_veh_f']='';
            $data['asesor_ser']=$datos->tecnico;
            if($datos->per_fin>0){
                $data['asesor_ser']=$datos->tecnicofin;
            }
            $data['gerencia']='';
            $data['firma']=$datos->firma;
            $data['qrecibio']=$datos->qrecibio;
            $data['obsevacionesadicionales']=$datos->comentariocal.'<br>'.$datos->comentario;
            $data['comentarioejecutiva']=$datos->comentariocal;
            $data['poliza']=$datos->poliza;
            $data['tserviciomotivo']=$datos->tserviciomotivo;

            $data['cot_refaccion']=$datos->cot_refaccion;
            $data['cot_refaccion2']=$datos->cot_refaccion2;
            $data['cot_refaccion3']=$datos->cot_refaccion3;

            $data['comentario']=$datos->comentario;
            $data['comentario_tec']=$datos->comentario_tec;
            $data['comentariocal']=$datos->comentariocal;
            $data['atencionpara']=$datos->atencionpara;
            $data['email']=$datos->email;
            $data['telefono']=$datos->telefono;
            $data['celular']=$datos->celular;
            $data['puesto']=$datos->puesto;
            if($datos->id_contacto_new>0){
                $res_dc = $this->ModeloCatalogos->getselectwheren('cliente_datoscontacto',array('datosId'=>$datos->id_contacto_new));
                foreach ($res_dc->result() as $item_dc) {
                    $data['atencionpara']=$item_dc->atencionpara;
                    $data['email']=$item_dc->email;
                    $data['telefono']=$item_dc->telefono;
                    $data['celular']=$item_dc->celular;
                    $data['puesto']=$item_dc->puesto;
                }
            }
            if($datos->prioridadr>0){
                if($datos->cot_refaccion!=''){
                    $comentariotecnico.='Cotizacion: Prioridad '.$datos->prioridadr.' '.$datos->cot_refaccion.'<br>';
                }
                if($datos->cot_refaccion2!=''){
                    $comentariotecnico.='Cotizacion: Prioridad '.$datos->prioridadr.' '.$datos->cot_refaccion2.'<br>';
                }
                if($datos->cot_refaccion3!=''){
                    $comentariotecnico.='Cotizacion: Prioridad '.$datos->prioridadr.' '.$datos->cot_refaccion3.'<br>';
                }
            }
            
            //$comentariotecnico.=$datos->comentario;
            $data['comentariotecnico']=$comentariotecnico;

        }else{
            $datos = $this->ModeloCatalogos->doc_servicio($id);
            $te_cel = $this->ModeloCatalogos->doc_cliente_tel_cel2($datos->id);
            $text_prioridad='';
            if ($datos->prioridad==1){
                $text_prioridad='Prioridad 1';
            }else if($datos->prioridad==1){
                $text_prioridad='Prioridad 2';
            }else if($datos->prioridad==1){
                $text_prioridad='Prioridad 3';
            }else if($datos->prioridad==1){
                $text_prioridad='Servicio Regular';
            }else{
                $text_prioridad='';
            }
            $get_reg = explode(" ", $datos->reg);
            $data['fecha_crea']=$get_reg[0];
            $data['fecha_asig']=$datos->fecha;
            $data['tipo_serv']=$datos->servicio;
            $data['responsable_asig']=$datos->tecnico;
            $data['hora_asig']=$datos->hora;
            $data['prioridad']=$text_prioridad;
            $data['descripcion_serv_fall']=$datos->tserviciomotivo;
            $data['modelo']='';
            $data['serie']='';
            $data['nombre_empresa']=$datos->empresa;
            $data['direcc_inst_serv']='';
            if (isset($te_cel->telefono)) {
                $telefono=$te_cel->telefono;
            }else{
                $telefono='';
            }
            if (isset($te_cel->celular)) {
                $celular=$te_cel->celular;
            }else{
                $celular='';
            }
            if (isset($te_cel->email)) {
                $email=$te_cel->email;
            }else{
                $email='';
            }


            $data['tel']=$telefono;
            $data['cel']=$celular;
            //$data['tel']='';//componer
            //$data['cel']='';//componer
            $data['correo']=$email;
            $data['docum_acce']=$datos->documentaccesorio;
            $data['observa_edi']=$datos->comentario;
            $data['lectu_ini']='';
            $data['km_veh_i']='';
            $data['lectur_fin']='';
            $data['km_veh_f']='';
            $data['asesor_ser']=$datos->tecnico;
            $data['gerencia']='';
            $data['firma']=$datos->firma;
            $data['qrecibio']=$datos->qrecibio;
            $data['obsevacionesadicionales']=$datos->comentario;
        }
        
        $this->load->view('Reportes/poreventovent',$data); 
    }   
    function exportar(){
        $tipo           =$_GET['tipo'];
        $tiposer        =$_GET['tiposer'];
        $status         =$_GET['status'];
        $zona           =$_GET['zona'];
        $tecnico        =$_GET['tecnico'];
        $pcliente       =$_GET['cliente'];
        $fechainicio    =$_GET['finicio'];
        $fechafin       =$_GET['ffin'];

        $params['tiposer']=$tiposer;
        $params['tecnico']=$tecnico;
        $params['pcliente']=$pcliente;
        $params['status']=$status;
        $params['zona']=$zona;

        //$this->load->view('Reportes/exportarservicios',$data);
        //$this->load->view('Reportes/exportarservicios_xlsx',$data);
        //=====================================================================
            $spreadsheet = new Spreadsheet();
            $fecha_inicial = new DateTime($fechainicio);
            $fecha_final = new DateTime($fechafin);
            // Necesitamos modificar la fecha final en 1 día para que aparezca en el bucle
            $fecha_final = $fecha_final ->modify('+1 day');

            $intervalo = DateInterval::createFromDateString('1 day');
            $periodo = new DatePeriod($fecha_inicial , $intervalo, $fecha_final);
            $row_h=0;
            foreach ($periodo as $dt) {
                $fecha_de_consulta=$dt->format("Y-m-d");
                
                $params['fechainicio']=$fecha_de_consulta;
                $params['fechafin']=$fecha_de_consulta;

                if($row_h==0){
                    $sheet = $spreadsheet->getActiveSheet();
                }else{
                    $spreadsheet->createSheet();
                    $sheet = $spreadsheet->setActiveSheetIndex($row_h);
                }
                $sheet->setTitle($fecha_de_consulta);
                //echo $fecha_de_consulta;
                //===========================================
                    $sheet->setCellValue('A1', 'Tecnico');
                    $sheet->setCellValue('B1', 'Cliente');
                    $sheet->setCellValue('C1', 'Servicio');
                    $sheet->setCellValue('D1', 'Domicilio');
                    $sheet->setCellValue('E1', 'Fecha');
                    $sheet->setCellValue('F1', 'Hora inicio');
                    $sheet->setCellValue('G1', 'Hora fin');
                    $sheet->setCellValue('H1', 'No de Equipos');
                    $sheet->setCellValue('J1', 'Fecha de creación');
                //===========================================
                    $rc=2;
                    //==========================================
                        if($tipo==1 or $tipo==0){//contrato
                            $datos=$this->ModeloServicio->getlistaservicioc_reporte($params);
                            $array_info_ser=array();
                            foreach ($datos->result() as $item) { 
                                $asignacionId = $item->asignacionId;
                                $direccionequipo= $this->ModeloCatalogos->getdireccionequipos($item->idequipo,$item->serieId);
                                $servicio = $item->servicio;
                                if($item->tservicio_a>0){
                                    $servicio = $item->poliza;
                                }
                                $array_info_ser[]=array(
                                    'tec'=>$item->tecnico,
                                    'emp'=>$item->empresa.' ('.$item->folio.')',
                                    'dir'=>$direccionequipo,
                                    'fecha'=>$item->fecha,
                                    'hora'=>$item->hora,
                                    'horafin'=>$item->horafin,
                                    'reg'=>$item->reg,
                                    'ser'=>$servicio,
                                    'serid'=>$asignacionId
                                );
                                
                                $html_tr='<tr><td>'.utf8_decode($item->tecnico).'</td><td>'.$item->empresa.' ('.$item->folio.')</td><td>'.$direccionequipo.'</td><td>'.$item->fecha.'</td><td>'.$item->hora.'</td><td>'.$item->horafin.'</td></tr>';
                                //echo $html_tr;
                            }
                            $grouped_array = array();
                            foreach ($array_info_ser as $element) {
                                $grouped_array[$element['dir']][] = $element;
                            }
                            //var_dump($grouped_array);
                            foreach ($grouped_array as $item) {
                                $tec_array=array();
                                foreach($item as $itemte) {
                                    $tec_array[]=$itemte['tec'];
                                }
                                $res_array_tec=array_unique($tec_array);
                                $tecnicos=implode(',',$res_array_tec);
                                //echo $tecnicos;
                                //var_dump($res_array_tec);
                                //echo '<tr><td>'.utf8_decode($tecnicos).'</td><td>'.$item[0]['emp'].'</td><td>'.$item[0]['dir'].'</td><td>'.$item[0]['fecha'].'</td><td>'.$item[0]['hora'].'</td><td>'.$item[0]['horafin'].'</td><td>'.count($item).'</td></tr>';
                                    $serid = $item[0]['serid'];
                                    $productos='';
                                    //=============================================
                                        $resultado=$this->ModeloAsignacion->getdatosrenta($serid,'',0,0);
                                        foreach ($resultado->result() as $item0) {
                                            $datosconsumiblesfolio = $this->ModeloServicio->getfoliosservicios($serid,$item0->serieId);
                                            
                                            //log_message('error','$serid '.$serid.''.$item0->serieId.' datos:'.$datosconsumiblesfolio->num_rows());
                                            foreach ($datosconsumiblesfolio->result() as $item1) {
                                                    $productos.=''.$item1->modelo.' '.$item1->foliotext.' / ';
                                            }
                                        }
                                        //============================================ esto aplica para las optciones 1 2 3
                                            $datosres_equi = $this->ModeloCatalogos->obtenerproducventavinculada($serid,1);
                                            foreach ($datosres_equi->result() as $itemre) {
                                                $productos.='Cant: '.$itemre->cantidad.' '.$itemre->modelo.' '.$itemre->serie.' / ';
                                            }
                                        //============================================
                                    //=============================================

                                    $sheet->setCellValue('A'.$rc, $tecnicos);
                                    $sheet->setCellValue('B'.$rc, $item[0]['emp']);
                                    $sheet->setCellValue('C'.$rc, $item[0]['ser']);
                                    $sheet->setCellValue('D'.$rc, $item[0]['dir']);
                                    $sheet->setCellValue('E'.$rc, $item[0]['fecha']);
                                    $sheet->setCellValue('F'.$rc, $item[0]['hora']);
                                    $sheet->setCellValue('G'.$rc, $item[0]['horafin']);
                                    $sheet->setCellValue('H'.$rc, count($item));
                                    $sheet->setCellValue('I'.$rc, $productos);
                                    $sheet->setCellValue('J'.$rc, $item[0]['reg']);

                                $rc++;
                            }
                        }
                        if($tipo==2 or $tipo==0){//poliza
                            //log_message('error','Tipo: 2');
                            $params_p=$params;
                            $params_p['tipoc']=1;
                            $datos=$this->ModeloServicio->getlistaserviciop_reporte($params_p);
                            $array_info_ser=array();
                            foreach ($datos->result() as $item) { 
                                $asignacionId = $item->asignacionId;
                                $polizaId=$item->polizaId;
                                $servicio = $item->servicio;
                                $dat_pol_eq=$this->ModeloServicio->obtenerproductospoliza($polizaId);
                                $equiposgroup='';
                                foreach ($dat_pol_eq->result() as $item_pe) {
                                    $equiposgroup.=$item_pe->modelo.' '.$item_pe->serie.' / ';
                                }
                                //============================================ esto aplica para las optciones 1 2 3
                                    $datosres_equi = $this->ModeloCatalogos->obtenerproducventavinculada($asignacionId,2);
                                    foreach ($datosres_equi->result() as $itemre) {
                                        $equiposgroup.='Cant: '.$itemre->cantidad.' '.$itemre->modelo.' '.$itemre->serie.' / ';
                                    }
                                //============================================
                                /*
                                $html_tr='<tr>
                                    <td>'.utf8_decode($item->tecnicogroup).'</td>
                                    <td>'.$item->empresa.'</td>
                                    <td>'.$item->direccionservicio.'</td>
                                    <td>'.$item->fecha.'</td>
                                    <td>'.$item->hora.'</td>
                                    <td>'.$item->horafin.'</td>
                                    <td>'.$item->numequipos.'</td>
                                </tr>';
                                    echo $html_tr;
                                */

                                    $sheet->setCellValue('A'.$rc, 'P'.$item->tecnicogroup);
                                    $sheet->setCellValue('B'.$rc, $item->empresa);
                                    $sheet->setCellValue('C'.$rc, $servicio);
                                    $sheet->setCellValue('D'.$rc, $item->direccionservicio);
                                    $sheet->setCellValue('E'.$rc, $item->fecha);
                                    $sheet->setCellValue('F'.$rc, $item->hora);
                                    $sheet->setCellValue('G'.$rc, $item->horafin);
                                    $sheet->setCellValue('H'.$rc, $item->numequipos);
                                    $sheet->setCellValue('I'.$rc, $equiposgroup);
                                    $sheet->setCellValue('J'.$rc, $item->reg);
                                $rc++;
                            }
                        }
                        if($tipo==3 or $tipo==0){//evento
                            //log_message('error','Tipo: 3');
                            $params_c=$params;
                            $params_c['tipoc']=1;
                            $datos=$this->ModeloServicio->getlistaserviciocl_reporte($params_c);
                            foreach ($datos->result() as $item) { 
                                    $asignacionId=$item->asignacionId;

                                    $servicio = $item->servicio;
                                    if($item->tservicio_a>0){
                                        $servicio = $item->poliza;
                                    }
                                    //============================================ esto aplica para las optciones 1 2 3
                                        $datosres_equi = $this->ModeloCatalogos->obtenerproducventavinculada($asignacionId,3);
                                        $productos='';
                                        foreach ($datosres_equi->result() as $itemre) {
                                            $productos.='Cant: '.$itemre->cantidad.' '.$itemre->modelo.' '.$itemre->serie.' / ';
                                        }
                                    //====================================================
                                    $sheet->setCellValue('A'.$rc, $item->tecnicogroup);
                                    $sheet->setCellValue('B'.$rc, $item->empresa);
                                    $sheet->setCellValue('C'.$rc, $servicio);
                                    $sheet->setCellValue('D'.$rc, $item->direccion);
                                    $sheet->setCellValue('E'.$rc, $item->fecha);
                                    $sheet->setCellValue('F'.$rc, $item->hora);
                                    $sheet->setCellValue('G'.$rc, $item->horafin);
                                    $sheet->setCellValue('H'.$rc, $item->numequipos);
                                    $sheet->setCellValue('I'.$rc, $productos);
                                    $sheet->setCellValue('J'.$rc, $item->reg);
                                $rc++; 
                            }
                        }
                        if($tipo==4 or $tipo==0){//evento
                            $params_c=$params;
                            $params_c['tipoc']=1;
                            $datosvent=$this->ModeloServicio->getlistaserviciovent_reporte($params_c);
                            foreach ($datosvent->result() as $item) { 
                                    $servicio = $item->servicio;
                                    if($item->tservicio_a>0){
                                        $servicio = $item->poliza;
                                    }
                                    $sheet->setCellValue('A'.$rc, $item->tecnico.' '.$item->tecnico2);
                                    $sheet->setCellValue('B'.$rc, $item->empresa);
                                    $sheet->setCellValue('C'.$rc, $servicio);
                                    $sheet->setCellValue('D'.$rc, $item->direccion);
                                    $sheet->setCellValue('E'.$rc, $item->fecha);
                                    $sheet->setCellValue('F'.$rc, $item->hora);
                                    $sheet->setCellValue('G'.$rc, $item->horafin);
                                    $sheet->setCellValue('H'.$rc, $item->numequipos);
                                    $sheet->setCellValue('J'.$rc, $item->reg);
                                $rc++; 
                            }
                        }
                    //==========================================
                //===========================================
                    
                    
                    
                $row_h++;
            }
        
        //=====================================================================
            $tiposave=0;//0 guarde directamente 1 descargue
            if($tiposave==1){
                /*
                $writer = new Xlsx($spreadsheet);

                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header('Content-Disposition: attachment;filename="archivo.xlsx"');
                header('Cache-Control: max-age=0');

                // Guardar el archivo en la salida del búfer de salida (output buffer)
                $writer->save('php://output');
                */
            }else{
                // Guardar el archivo XLSX
                $writer = new Xlsx($spreadsheet);
                $urlarchivo0='fichero_reporte/archivo.xlsx';
                $urlarchivo=FCPATH.$urlarchivo0;
                $writer->save($urlarchivo);

                redirect(base_url().$urlarchivo0); 

            }
            // Configurar encabezados para la descarga
            
    } 
    //===================== esta solo es para consultas comentarla cuando se terminen las pruebas es la misma exportar()
        function exportar_info(){
            $data['tipo']           =$_GET['tipo'];
            $data['tiposer']        =$_GET['tiposer'];
            $data['status']         =$_GET['status'];
            $data['zona']           =$_GET['zona'];
            $data['tecnico']        =$_GET['tecnico'];
            $data['pcliente']       =$_GET['cliente'];
            $data['fechainicio']    =$_GET['finicio'];
            //$data['fechafin']       =$_GET['ffin'];
            $data['fechafin']       =$_GET['finicio'];
            $this->load->view('Reportes/exportarservicios',$data);
        } 
    //========================
    function exportar2(){
        $data['tipo']           =$_GET['tipo'];
        $data['tiposer']        =$_GET['tiposer'];
        $data['status']         =$_GET['status'];
        $data['zona']           =$_GET['zona'];
        $data['tecnico']        =$_GET['tecnico'];
        $data['pcliente']       =$_GET['cliente'];
        $data['fechainicio']    =$_GET['finicio'];
        $data['fechafin']       =$_GET['ffin'];
        $this->load->view('Reportes/exportarservicios2',$data);
    } 
    function buscarserie(){
        $params = $this->input->post();
        $serie=$params['serie'];
        $tipoview=$params['tipoview'];
        $result = $this->ModeloServicio->getobtenerservicios($this->fechaactual,$serie,1,$tipoview);
        $result0 = $this->ModeloServicio->getobtenerservicios($this->fechaactual,$serie,0,$tipoview);
        $result1 = $this->ModeloServicio->getobtenercontratosseries($serie);
        $result2 = $this->ModeloServicio->getobtenerventaseries($serie);
        $html='<ul class="collapsible collapsible-accordion">
                  <li class="active">
                    <div class="collapsible-header" tabindex="0">
                      Servicios Actuales</div>
                    <div class="collapsible-body" style="display: block;">';
                        //========================================================
                            $html.="<table id='tabla_seach' class='table display' cellspacing='0'>
                                  <thead>
                                    <tr>
                                      <th>#</th>
                                      <th>Técnico</th>
                                      <th>Cliente</th>
                                      <th>Fecha</th>
                                      <th>Hora</th>
                                      <th >Equipo</th>
                                      <th>Serie</th>
                                      <th>Acciones</th>
                                    </tr>
                                  </thead>
                                  <tbody>";
                                  $row=0;
                                  foreach ($result->result() as $item) {
                                    if($item->asignacionIde>0){
                                        if($item->tiposervicio==1){
                                            $serie_eq=$this->ModeloGeneral->ocultar_ult_dig(4,$item->serie);
                
                                        }else{
                                            $serie_eq=$item->serie;
                                        }
                                        if($tipoview==0){
                                            $btn="<button 
                                                        class='btn-bmz cyan waves-effect waves-light dirigiraservicio_".$row."' 
                                                        data-stiposervicio='".$item->tiposervicio."'
                                                        data-scliente='".$item->empresa."'
                                                        data-sclienteid='".$item->id."'
                                                        data-sfecha='".$item->fecha."'
                                                        type='button' 
                                                        onclick='dirigiraservicio(".$row.")'>Ir al servicio</button>";
                                        }else{
                                            $btn="";
                                        }
                                      $html.="<tr>
                                                  <td>".$item->asignacionIde."</td>
                                                  <td>".$item->tecnico."</td>
                                                  <td>".$item->empresa."</td>
                                                  <td>".$item->fecha."</td>
                                                  <td>".$item->hora."</td>
                                                  <td>".$item->modelo."</td>
                                                  <td>".$serie_eq."</td>
                                                  <td>".$btn."</td>
                                                </tr>";
                                    }
                                    $row++;
                                  }
                        $html.=" </tbody>
                                </table>";
                        //======================================================

            $html.='</div>
                  </li>
                  <li class="">
                    <div class="collapsible-header" tabindex="0">
                      Servicios Pasados</div>
                    <div class="collapsible-body" style="">';
                        //========================================================
                            $html.="<table id='tabla_seach0' class='table display' cellspacing='0'>
                                  <thead>
                                    <tr>
                                      <th>#</th>
                                      <th>Técnico</th>
                                      <th>Cliente</th>
                                      <th>Fecha</th>
                                      <th>Hora</th>
                                      <th >Equipo</th>
                                      <th>Serie</th>
                                      <th>Acciones</th>
                                    </tr>
                                  </thead>
                                  <tbody>";
                                  
                                  foreach ($result0->result() as $item) {
                                    if($item->asignacionIde>0){
                                        if($item->tiposervicio==1){
                                            $serie_eq=$this->ModeloGeneral->ocultar_ult_dig(4,$item->serie);
                
                                        }else{
                                            $serie_eq=$item->serie;
                                        }
                                        if($tipoview==0){
                                            $btn="<button 
                                                        class='btn-bmz cyan waves-effect waves-light dirigiraservicio_".$row."' 
                                                        data-stiposervicio='".$item->tiposervicio."'
                                                        data-scliente='".$item->empresa."'
                                                        data-sclienteid='".$item->id."'
                                                        data-sfecha='".$item->fecha."'
                                                        type='button' 
                                                        onclick='dirigiraservicio(".$row.")'>Ir al servicio</button>";
                                        }else{
                                            if($item->tiposervicio==1){
                                                $urlarchivo='Listaservicios/contrato_formato/'.$item->asignacionId.'/1/'.$item->asignacionIde;
                                            }
                                            if($item->tiposervicio==2){
                                                $urlarchivo='Listaservicios/tipo_formato/'.$item->asignacionId.'/2/'.$item->asignacionIde;
                                            }
                                            if($item->tiposervicio==3){
                                                $urlarchivo='Listaservicios/tipo_formatoc/'.$item->asignacionId.'/3/'.$item->asignacionIde;
                                            }
                                            if($item->tiposervicio==4){
                                                $urlarchivo='Listaservicios/tipo_formatovent/'.$item->asignacionId.'/4/'.$item->asignacionIde;
                                            }
                                            $btn='<a href="'.base_url().'index.php/Pdfview?file='.$urlarchivo.'" class="waves-effect cyan btn-bmz" onclick="documento(1679,3,0)" target="_blank">                                    <i class="fas fa-file-alt" style="font-size: 20px;"></i></a>';
                                        }
                                      $html.="<tr>
                                                  <td>".$item->asignacionIde."</td>
                                                  <td>".$item->tecnico."</td>
                                                  <td>".$item->empresa."</td>
                                                  <td>".$item->fecha."</td>
                                                  <td>".$item->hora."</td>
                                                  <td>".$item->modelo."</td>
                                                  <td>".$serie_eq."</td>
                                                  <td>".$btn."</td>
                                                </tr>";
                                    }
                                    $row++;
                                  }
                        $html.=" </tbody>
                                </table>";
                        //======================================================

                    $html.='</div>
                  </li>
                  <li>
                    <div class="collapsible-header" tabindex="0">
                      Contratos</div>
                    <div class="collapsible-body">';
                      //========================================================
                            $html.="<table id='tabla_seach1' class='table display' cellspacing='0'>
                                  <thead>
                                    <tr>
                                      <th># contrato</th>
                                      <th>Cliente</th>
                                      <th>Estatus contrato</th>
                                      <th>serie</th>
                                      <th>Estatus Serie</th>
                                    </tr>
                                  </thead>
                                  <tbody>";
                                  
                                  foreach ($result1->result() as $item) {
                                    if($item->activo==1){
                                        $activo='Activo';
                                    }else{
                                        $activo='Eliminado';
                                    }

                                    if($item->estatus==1){
                                        $itemestatus='Activo';
                                    }else{
                                        $itemestatus='Cancelado';
                                        $activo='Eliminado';
                                    }
                                    
                                        $serie_eq=$this->ModeloGeneral->ocultar_ult_dig(4,$item->serie);
                                      $html.="<tr>
                                                  <td>".$item->idcontrato."</td>
                                                  <td>".$item->empresa."</td>
                                                  <td>".$itemestatus."</td>
                                                  <td>".$serie_eq."</td>
                                                  <td>".$activo."</td>
                                                  
                                                </tr>";
                                   
                                    $row++;
                                  }
                        $html.=" </tbody>
                                </table>";
                        //======================================================

                    $html.='</div>
                  </li>
                  <li>
                    <div class="collapsible-header" tabindex="0">
                      Ventas</div>
                    <div class="collapsible-body">';
                        //========================================================
                            $html.="<table id='tabla_seach4' class='table display' cellspacing='0'>
                                  <thead>
                                    <tr>
                                      <th></th>
                                      <th>Cliente</th>
                                      <th>Serie</th>
                                      <th></th>
                                      <th>Acciones</th>
                                    </tr>
                                  </thead>
                                  <tbody>";
                                  foreach ($result2->result() as $item) {
                                            if($item->combinada==0){
                                                $tipov='Venta';
                                            }else{
                                                $tipov='Venta Combinada';
                                            }
                                            if($item->activo==1){
                                                $activo='Activo';
                                            }else{
                                                $activo='Eliminado';
                                            }
                                      $html.='<tr>
                                                    <td>'.$tipov.'</td>
                                                    <td>'.$item->empresa.'</td>
                                                    <td>'.$item->serie.'</td>
                                                    <td>'.$activo.'</td>
                                                    <td>
                                                        <a class="waves-effect cyan btn-bmz" title="Prefactura" onclick="viewprefacturav('.$item->id.',0)"><i class="fas fa-file" style="font-size: 20px;"></i></a>
                                                    </td>
                                              </tr>';
                                  }
                            $html.=" </tbody>
                                </table>";
                        //======================================================
                    $html.='</div>
                  </li>
                </ul>';
        
        echo $html;
    }
    function cambiarnotificacion(){
        $datax = $this->input->post();
        $id = $datax['id'];
        $tipo = $datax['tipo'];
        $html='';
        if ($tipo==1) {
            $asignacionId=0;
            $contratoId=0;
            $idRenta=0;
           //$this->ModeloCatalogos->updateCatalogo('asignacion_ser_contrato_a_e',array('notificarerror_estatus'=>0),array('asignacionIde'=>$id));
            $resultser=$this->ModeloCatalogos->getselectwheren('asignacion_ser_contrato_a_e',array('asignacionIde'=>$id));
            foreach ($resultser->result() as $item) {
                $asignacionId=$item->asignacionId;
            }
            $resultser=$this->ModeloCatalogos->getselectwheren('asignacion_ser_contrato_a',array('asignacionId'=>$asignacionId));
            foreach ($resultser->result() as $item) {
                $contratoId=$item->contratoId;
            }
            $resultser=$this->ModeloCatalogos->getselectwheren('contrato',array('idcontrato'=>$contratoId));
            foreach ($resultser->result() as $item) {
                $idRenta=$item->idRenta;
            }

            $html.='<table class="table bordered" id="tableequipos">';
            $respuestarentae=$this->ModeloCatalogos->db10_getselectwheren('rentas_has_detallesEquipos',array('idRenta'=>$idRenta));
            foreach ($respuestarentae->result() as $item) {
                $respuestarentaess=$this->ModeloCatalogos->db2_getselectwheren('asignacion_series_r_equipos',array('id_equipo'=>$item->id,'activo'=>1));
                    $modeloe=$item->modelo;
                foreach ($respuestarentaess->result() as $itemss) {
                    $serie=$this->obtenerequiposserie($itemss->serieId);
                    $direccionequipo= $this->getdireccionequipos($itemss->id_equipo,$itemss->serieId);
                    $html.='
                            <tr>
                                <td style="width: 95px;">
                                                
                                    <input type="checkbox" class="serie_check serie_check_'.$itemss->id_equipo.'_'.$itemss->serieId.'" id="serie_check_'.$itemss->serieId.'" value="'.$itemss->serieId.'">
                                    <label for="serie_check_'.$itemss->serieId.'"></label>
                                              
                                    
                                    <a class="waves-effect cyan btn-bmz consultar_fechas_agenda_'.$itemss->id_equipo.'_'.$itemss->serieId.'" 
                                        onclick="consultarfechasagenda('.$itemss->id_equipo.')" style="display:none"><i class="fas fa-calendar-alt" style="font-size: 20px;"></i></a>

                                    <input type="hidden" class="ss" value="'.$itemss->id_equipo.'" id="equipoid">
                                    <input type="hidden" value="'.$itemss->serieId.'" id="serieid">
                                </td>
                                <td>
                                    '.$modeloe.'
                                </td>
                                <td>
                                    '.$serie.'
                                </td>
                                
                                <td style="font-size:12px;">'.$direccionequipo.'
                                </td>
                            </tr>
                            ';
                }
                
            }
            $html.='</table><a class="waves-effect cyan btn-bmz" onclick="cambiarequipocontrato('.$id.')">Confirmar</a>';
        }
        if ($tipo==2) {
            //$this->ModeloCatalogos->updateCatalogo('asignacion_ser_poliza_a_e',array('notificarerror_estatus'=>0),array('asignacionIde'=>$id));
            $html.='<label>Nueva serie</label><input class="form-control-bmz" id="nuevaserie"><a class="waves-effect cyan btn-bmz" onclick="cambiarseriepoliza('.$id.')">Confirmar</a>';
        }
        if ($tipo==3) {
           // $this->ModeloCatalogos->updateCatalogo('asignacion_ser_cliente_a_d',array('notificarerror_estatus'=>0),array('asignacionIdd'=>$id));
            $html.='<label>Nueva serie</label><input class="form-control-bmz" id="nuevaserie"><a class="waves-effect cyan btn-bmz" onclick="cambiarserieevento('.$id.')">Confirmar</a>';
        }
        if ($tipo==4) {
           // $this->ModeloCatalogos->updateCatalogo('asignacion_ser_venta_a',array('notificarerror_estatus'=>0),array('asignacionId'=>$id));
            $html.='<label>Nueva serie</label><input class="form-control-bmz" id="nuevaserie"><a class="waves-effect cyan btn-bmz" onclick="cambiarserieventa('.$id.')">Confirmar</a>';
        }
        echo $html;
    }
    // esta funcion tiene que ser igual al controlador de Asignaciones
    function obtenerequiposserie($id){
        $respuestarentae=$this->ModeloCatalogos->db2_getselectwheren('series_productos',array('serieId'=>$id));
        $serie='';
        foreach ($respuestarentae->result() as $item) {
            $serie=$item->serie;
        }
        return $serie;
    }
    // esta funcion tiene que ser igual al controlador de Asignaciones
    function getdireccionequipos($equipo,$serie){
            $resultdir=$this->ModeloCatalogos->db2_getselectwheren('contrato_equipo_direccion',array('equiposrow'=>$equipo,'serieId'=>$serie));
            $iddireccionget='g_0';
            $direccion='';
            foreach ($resultdir->result() as $itemdir) {
                $iddireccionget=$itemdir->direccion;
            }
            if($iddireccionget!='g_0'){
                $tipodireccion=explode('_',$iddireccionget);

                $tipodir = $tipodireccion[0];
                $iddir   = $tipodireccion[1];
                if ($tipodir=='g') {
                    $resultdirc=$this->ModeloCatalogos->db2_getselectwheren('clientes_direccion',array('idclientedirecc'=>$iddir));
                    foreach ($resultdirc->result() as $item) {
                        $direccion=$item->direccion;
                    }
                }
                if ($tipodir=='f') {
                    $resultdirc=$this->ModeloCatalogos->db2_getselectwheren('cliente_has_datos_fiscales',array('id'=>$iddir));
                    foreach ($resultdirc->result() as $item) {
                        $direccion='Razon social: '.$item->razon_social.' Colonia:'.$item->colonia.' Calle:'.$item->calle.' Numero:'.$item->num_ext;
                    }
                }
            }
            return $direccion;
    }
    function cambiarequipocontrato(){
        $datax = $this->input->post();
        $id = $datax['id'];
        $equipo = $datax['equipo'];
        $serie = $datax['serie'];
        $this->ModeloCatalogos->updateCatalogo('asignacion_ser_contrato_a_e',array('idequipo'=>$equipo,'serieId'=>$serie,'notificarerror_estatus'=>0),array('asignacionIde'=>$id));
    }
    function cambiarseriepoliza(){
        $datax = $this->input->post();
        $id = $datax['id'];
        $serie = $datax['serie'];
        $serie_o='';$idPoliza=0;

        $a_poliza_d=$this->ModeloCatalogos->getselectwheren('asignacion_ser_poliza_a_e',array('asignacionIde'=>$id));
        $id_pd=0;
        foreach ($a_poliza_d->result() as $itempd) {
            $id_pd=$itempd->idequipo;
        }

        $res_pold=$this->ModeloCatalogos->getselectwheren('polizasCreadas_has_detallesPoliza',array('id'=>$id_pd));
        foreach ($res_pold->result() as $itemp) {
            $serie_o = $itemp->serie;
            $idPoliza=$itemp->idPoliza;
        }

        $this->ModeloCatalogos->updateCatalogo('asignacion_ser_poliza_a_e',array('serie'=>$serie,'notificarerror_estatus'=>0),array('asignacionIde'=>$id));

        
        $this->ModeloCatalogos->updateCatalogo('polizasCreadas_has_detallesPoliza',array('serie'=>$serie),array('id'=>$id_pd));
        $this->ModeloCatalogos->getdeletewheren('polizasCreadas_has_detallesPoliza_series',array('iddetalle'=>$id_pd));
        $this->ModeloCatalogos->Insert('polizasCreadas_has_detallesPoliza_series',array('iddetalle'=>$id_pd,'serie'=>$serie));

        $a_poliza_d=$this->ModeloCatalogos->getselectwheren('polizasCreadas_has_detallesPoliza',array('id'=>$id));
        foreach ($a_poliza_d->result() as $itempd) {
            if($itempd->idvinculo>0){
                $this->ModeloCatalogos->updateCatalogo('cotizaciones_has_detallesPoliza',array('serie'=>$serie),array('id'=>$itemp->idvinculo));
            }
        }
        //==========================================================================================================
            $strq="SELECT vc.* 
                FROM polizasCreadas as pol
                INNER JOIN ventacombinada as vc on vc.poliza=pol.id
                WHERE pol.id='$idPoliza'";
                $query = $this->db->query($strq);
                $idventa=0;
            foreach ($query->result() as $item_vc) {
                $idventa=$item_vc->equipo;
            }
            if($idventa>0){
                $this->ModeloCatalogos->updateCatalogo('ventas_has_detallesRefacciones',array('serie'=>$serie),array('idVentas'=>$idventa,'serie'=>$serie_o));
            }
        //==========================================================================================================
    }
    function cambiarserieevento(){
        $datax = $this->input->post();
        $id = $datax['id'];
        $serie = $datax['serie'];
        $this->ModeloCatalogos->updateCatalogo('asignacion_ser_cliente_a_d',array('serie'=>$serie,'notificarerror_estatus'=>0),array('asignacionIdd'=>$id));
    }
    function cambiarserieventa(){
        $datax = $this->input->post();
        $id = $datax['id'];
        $serie = $datax['serie'];
        $this->ModeloCatalogos->updateCatalogo('asignacion_ser_venta_a',array('notificarerror_estatus'=>0),array('asignacionId'=>$id));

        $this->ModeloCatalogos->updateCatalogo('asignacion_ser_venta_a_d',array('serie'=>$serie),array('asignacionId'=>$id));
    }
    function exportar6(){
        $data['fechainicio']    =$_GET['finicio'];
        $data['fechafin']       =$_GET['ffin'];
        $data['resultstec']=$this->Configuraciones_model->view_session_searchTecnico();
        $this->load->view('Reportes/exportarservicios6',$data);
    } 
    function exportar7(){
        $data['fechainicio']    =$_GET['finicio'];
        $data['fechafin']       =$_GET['ffin'];
        $this->load->view('Reportes/exportarservicios7',$data);
    } 
    function ser_devolucion(){
        $data['fechainicio']    =$_GET['finicio'];
        $data['fechafin']       =$_GET['ffin'];
        $this->load->view('Reportes/ser_devolucion',$data);
    } 
    function editcomentariotec(){
        $params = $this->input->post();
        $fsrid = $params['asig'];
        $asigd = $params['asigd'];
        $com = $params['com'];
        $tipo = $params['tipo'];
        if ($tipo==1) {
            $a_poliza_d=$this->ModeloCatalogos->getselectwheren('asignacion_ser_contrato_a_e',array('asignacionId'=>$fsrid));
            $comentario='';
            foreach ($a_poliza_d->result() as $itempd) {
                $comentario=$itempd->comentario;
            }
            $comentario=$comentario.'<br><!--'.$this->usuario.' '.$this->fechahoy.'--><br>'.$com;
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_contrato_a_e',array('comentario'=>$comentario),array('asignacionId'=>$fsrid));
        }
        if ($tipo==2) {
            $a_poliza_d=$this->ModeloCatalogos->getselectwheren('asignacion_ser_poliza_a_e',array('asignacionId'=>$fsrid));
            $comentario='';
            foreach ($a_poliza_d->result() as $itempd) {
                $comentario=$itempd->comentario;
            }
            $comentario=$comentario.'<br><!--'.$this->usuario.' '.$this->fechahoy.'--><br>'.$com;
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_poliza_a_e',array('comentario'=>$comentario),array('asignacionId'=>$fsrid));
        }
        if ($tipo==3) {
            $a_poliza_d=$this->ModeloCatalogos->getselectwheren('asignacion_ser_cliente_a',array('asignacionId'=>$fsrid));
            $comentario='';
            foreach ($a_poliza_d->result() as $itempd) {
                $comentario=$itempd->comentario;
            }
            $comentario=$comentario.'<br><!--'.$this->usuario.' '.$this->fechahoy.'--><br>'.$com;
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_cliente_a',array('comentario'=>$comentario),array('asignacionId'=>$fsrid));
        }
        if ($tipo==4) {
            $a_poliza_d=$this->ModeloCatalogos->getselectwheren('asignacion_ser_venta_a',array('asignacionId'=>$fsrid));
            $comentario='';
            foreach ($a_poliza_d->result() as $itempd) {
                $comentario=$itempd->comentario;
            }
            $comentario=$comentario.'<br><!--'.$this->usuario.' '.$this->fechahoy.'--><br>'.$com;
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_venta_a',array('comentario'=>$comentario),array('asignacionId'=>$fsrid));
        }
    }
    function statuspredev(){
        $params = $this->input->post();
        $status = $params['status'];
        $row = $params['row'];
        $tipo = $params['tipo'];
        if($status==2){
            $status=0;
        }

        if ($tipo==1) { //cotizacion
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_contrato_a_e',array('not_dev'=>$status),array('asignacionIde'=>$row));
        }
        if ($tipo==2) { //poliza
            $result=$this->ModeloCatalogos->getselectwheren('asignacion_ser_poliza_a',array('asignacionId'=>$row));
            $idpoliza=0;
            foreach ($result->result() as $item) {
                $idpoliza=$item->polizaId;
            }
            if($idpoliza>0){
                $this->ModeloCatalogos->updateCatalogo('polizasCreadas',array('not_dev'=>$status),array('id'=>$idpoliza));
            }
        }
        if ($tipo==3) { //evento

        }
        if ($tipo==4) { //venta
            $result=$this->ModeloCatalogos->getselectwheren('asignacion_ser_venta_a',array('asignacionId'=>$row));
            $ventaId=0;
            foreach ($result->result() as $item) {
                $ventaId=$item->ventaId;
            }
            if($ventaId>0){
                $this->ModeloCatalogos->updateCatalogo('ventas',array('not_dev'=>$status),array('id'=>$ventaId));
            }
        }
    }
}
    

?>