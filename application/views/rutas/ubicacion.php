<style type="text/css">
  .addmaps{
    height: 79vh;
    /*
    background-image: url(<?php echo base_url()?>public/img/mapa.webp);
    background-size: cover;
    background-position: center;
    */
    /*filter: grayscale(100%) blur(1px);*/
    padding: 0px;
  }
  .addmaps iframe{
    width: 100%;
    height: 79vh;
    border: 0;
  }
</style>

<!-- START CONTENT -->
    <section id="content">
      <!--start container-->
        <div class="container">
          <div class="section">

            <input id="base_url" type="hidden" name="base_url" value="<?php echo base_url(); ?>">
            <input id="perfilid" type="hidden"  value="<?php echo $perfilid; ?>">
            <div class="row">
              <div class="col s2">
                <p>Ubicaciones Técnicos</label>
              </div>
              <div class="col s2">
                <select class="browser-default form-control-bmz" id="tecnico" onchange="cargarmaps()">
                  <option value="0">Seleccione una opción</option>
                  <?php foreach ($resultstec as $item) { ?>
                      <option value="<?php echo $item->personalId;?>"><?php echo $item->tecnico;?></option>
                  <?php }  ?>
                </select>
              </div>
              <div class="col s2">
                <input type="date" id="fecha" class="form-control-bmz" value="<?php echo date('Y-m-d')?>" onchange="cargarmaps()">
              </div>
              <div class="col s12 addmaps"></div>
            </div>
          
          
          </div>  
        </div>
    </section>

<script type="text/javascript">
  var base_url = $('#base_url').val();
  function cargarmaps() {
    var tecnico = $('#tecnico option:selected').val();
    var fecha = $('#fecha').val();
    if(tecnico>0){
      if(fecha!=''){
        var url =base_url+'Rutas_tecnicos/mapaview/'+tecnico+'/'+fecha; 
        $('.addmaps').html('<iframe src="'+url+'"></iframe>');
      }
    }
  }
</script>