<link href="<?php echo base_url(); ?>public/plugins/select2/select2.min.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/select2/select2.full.min.js"></script>

<script type="text/javascript">
  function generarreporte(){
    var form =$('#form_reporte');
    var tipof=$('#tipof option:selected').val();
    var fechainicio = $('#fechainicio').val();
    var fechafinal = $('#fechafinal').val();
    var tiporeporte = $('#tiporeporte').val();
    var idcliente = $('#idcliente option:selected').val();
    var vendedor = $('#idvendedor option:selected').val();
    var tipovp = $('#tipovp option:selected').val();
    if(idcliente>0){
        idcliente=idcliente;
    }else{
        idcliente=0;
    }
    if(form.valid()){
        var url ='<?php echo base_url();?>Reportes/rventas/'+fechainicio+'/'+fechafinal+'/'+idcliente+'/'+vendedor+'/'+tipovp+'/'+tipof;
            //url+='?viewexcel=1';
        window.open(url, '_blank');
    }
        
    
  }
  function recarcular(){
    var fini=$('#fechafinal').val();
    var idcliente = $('#idcliente option:selected').val();
    if(idcliente>0){
        idcliente=idcliente;
    }else{
        idcliente=0;
    }
    if(fini!=''){
        if(idcliente==0){
            var menosmeses=1;
        }else{
            var menosmeses=12;
        } 

        var fechaFinal = moment(fini, "YYYY-MM-DD").subtract(menosmeses, 'months');

        // Formatear la fecha en formato YYYY-MM-DD
        var fechaInicialFormateada = fechaFinal.format("YYYY-MM-DD");

        // Actualizar el elemento con la fecha inicial
        console.log(fechaInicialFormateada);
        $('#fechainicio').prop('min',fechaInicialFormateada);
        $('#fechainicio').prop('max',fini);
    }
    
  }
  $(document).ready(function($) {
    $('#idcliente').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una opción',
        allowClear: true,
        ajax: {
            url: '<?php echo base_url();?>Ventasi/searchclientes',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.id,
                        text: element.empresa

                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }); 
  });
</script> 