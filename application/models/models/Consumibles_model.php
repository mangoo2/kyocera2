<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class Consumibles_model extends CI_Model 
{
    public function __construct() 
    {
        parent::__construct();
    }
    
    public function insertar_consumibles($dato){
    $this->db->insert('consumibles',$dato);   
    return $this->db->insert_id();
    } 

    public function insertar_consumibles_costos($dato){
    $this->db->insert('consumibles_costos',$dato);   
    return $this->db->insert_id();
    } 

    public function update_consumibles($data, $id) {
    $this->db->set($data);
    $this->db->where('id', $id);
    $this->db->update('consumibles');
    return $id;
    }
    public function update_foto($data, $id) {
    $this->db->set($data);
    $this->db->where('id', $id);
    return $this->db->update('consumibles');
    }

    public function update_consumibles_costos($data, $id) {
    $this->db->set($data);
    $this->db->where('id', $id);
    return $this->db->update('consumibles_costos');
    }

    public function getListadoConsumibles()
    {
        $sql = "SELECT
                c.id,
                c.foto,
                c.modelo,
                c.parte,
                c.stock,
                c.observaciones
                FROM consumibles as c
                WHERE status=1";
        $query = $this->db->query($sql);
        return $query->result();
    }


    public function getDataEquipos(){
    $sql = "SELECT * FROM equipos WHERE estatus=1";
    $query = $this->db->query($sql);
    return $query->result();
    } 

    public function getConsumiblePorId($idConsumible)
    {
        $sql = "SELECT
                c.*,
                cc.general,
                cc.iva,
                cc.neto,
                cc.frecuente,
                cc.iva2,
                cc.neto2,
                cc.especial,
                cc.iva3,
                cc.neto3,
                cc.poliza,
                cc.iva4,
                cc.neto4,
                cc.id as 'idConsumiblesCostos'
                FROM consumibles as c 
                INNER JOIN consumibles_costos as cc ON cc.consumibles_id=c.id
                WHERE c.id=".$idConsumible;
        $query = $this->db->query($sql);
        return $query->row();
    }

    public function insertar_equipos_has_consumibles($dato)
    {
        $this->db->insert('equipos_has_consumibles',$dato);   
        return $this->db->insert_id();
    }

    public function getListadoHas_consumibles($id)
    {
        $sql = "SELECT c.id,e.modelo,c.idConsumibles FROM equipos_has_consumibles as c left join equipos as e on e.id = c.idEquipo where idConsumibles =".$id;
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function eliminar_has_consumibles($id) {
    $this->db->where('id', $id);
    return $this->db->delete('equipos_has_consumibles');
    }

    public function truncateTable($table)
    {
        $this->db->truncate($table);
    }
   
    public function getListaPreciosConsumibles()
    {
        $sql = "SELECT
                c.id AS DT_RowId,
                c.modelo,
                cc.neto4,
                cc.iva4,
                cc.neto3,
                cc.poliza,
                cc.iva3,
                cc.especial,
                cc.neto2,
                cc.iva2,
                cc.frecuente,
                cc.neto,
                cc.iva,
                cc.general
                FROM consumibles as c
                LEFT JOIN consumibles_costos as cc ON cc.consumibles_id=c.id";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getPreciosConsumiblesPorId($idConsumible)
    {
        $sql = "SELECT
                c.id AS DT_RowId,
                c.modelo,
                cc.neto4,
                cc.iva4,
                cc.neto3,
                cc.poliza,
                cc.iva3,
                cc.especial,
                cc.neto2,
                cc.iva2,
                cc.frecuente,
                cc.neto,
                cc.iva,
                cc.general
                FROM consumibles as c
                LEFT JOIN consumibles_costos as cc ON cc.consumibles_id=c.id WHERE c.id=".$idConsumible;
        $query = $this->db->query($sql);
        return $query->result();
    }
   
    public function update_precios_consumibles($data, $id) 
    {
        $this->db->set($data);
        $this->db->where('consumibles_id', $id);
        $this->db->update('consumibles_costos');
        return $id;
    }

}

?>