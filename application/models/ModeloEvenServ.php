<?php
defined('BASEPATH') OR exit ('No direct script access allowed');
class ModeloEvenServ extends CI_Model{
    public function __construct(){
        parent::__construct();
    }
    //==============================================
    /*
    SELECT seev.id,seev.poliza,po.nombre,seev.polizamodelo, eq.modelo,seev.nombre,seev.descripcion,seev.local,seev.semi,seev.foraneo
    from servicio_evento as seev 
    inner join polizas as po on po.id=seev.poliza
    inner JOIN polizas_detalles as pod on pod.id=seev.polizamodelo
    inner join equipos as eq on eq.id=pod.modelo
    WHERE seev.status=1
    */
    function getListadoEvenServ($params){
        $columns = array( 
                    0=>'seev.id',
                    1=>'seev.poliza',
                    2=>'po.nombre',
                    3=>'seev.polizamodelo',
                    4=>'eq.modelo',
                    5=>'seev.nombre nombreser',
                    6=>'seev.descripcion',
                    7=>'seev.local',
                    8=>'seev.semi',
                    9=>'seev.foraneo',
                    10=>'seev.especial',
                    11=>'seev.sinmodelo',
                    12=>'seev.newmodelo'
        );
        $columnsss = array( 
                    0=>'seev.id',
                    1=>'seev.poliza',
                    2=>'po.nombre',
                    3=>'seev.polizamodelo',
                    4=>'eq.modelo',
                    5=>'seev.nombre',
                    6=>'seev.descripcion',
                    7=>'seev.local',
                    8=>'seev.semi',
                    9=>'seev.foraneo',
                    10=>'seev.especial',
                    11=>'seev.sinmodelo',
                    12=>'seev.newmodelo'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('servicio_evento as seev');
        $this->db->join('polizas po', 'po.id=seev.poliza','left');
        $this->db->join('polizas_detalles pod', 'pod.id=seev.polizamodelo','left');
        $this->db->join('equipos eq', 'eq.id=pod.modelo','left');
            
        $this->db->where('seev.status',1);
            
            
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columnsss as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columnsss[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    function getListadoEvenServt($params){
        $columns = array( 
                    0=>'seev.id',
                    1=>'seev.poliza',
                    2=>'po.nombre',
                    3=>'seev.polizamodelo',
                    4=>'eq.modelo',
                    5=>'seev.nombre',
                    6=>'seev.descripcion',
                    7=>'seev.local',
                    8=>'seev.semi',
                    9=>'seev.foraneo',
                    10=>'seev.especial',
                    11=>'seev.sinmodelo',
                    12=>'seev.newmodelo'
        );
        $this->db->select('COUNT(*) as total');
        $this->db->from('servicio_evento as seev');
        $this->db->join('polizas po', 'po.id=seev.poliza','left');
        $this->db->join('polizas_detalles pod', 'pod.id=seev.polizamodelo','left');
        $this->db->join('equipos eq', 'eq.id=pod.modelo','left');
            
        $this->db->where('seev.status',1);
            
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        //$this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        ///$this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query->row()->total;
    }
    //==============================================
   
}

?>