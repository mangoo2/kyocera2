var base_url=$('#base_url').val();
$(document).ready(function($) {
	$('#nfcontratos').select2();
	$('#nfequipos').select2();
	$('#nfconsumibles').select2();
	$('#nfclientes').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una opción',
        allowClear: true,
        ajax: {
            url: base_url+'Ventasi/searchclientes',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.id,
                        text: element.empresa,
                        blq_perm_ven_ser:element.bloqueo_perm_ven_ser

                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function (e) {
          var data = e.params.data;
          contratoscliente(data.id);
          obtenerventaspendientes(data.id,data.blq_perm_ven_ser);
    });
    $('.addconsumibles').click(function(event) {
        validar_ultimos_servicios();
    	//addconsumibles();
    });
    $('.addconsumibles_poliza').click(function(event) {
        addconsumibles_poliza();
    });
    $('.agregarconsumibles').click(function(event) {
        $( ".agregarconsumibles" ).prop( "disabled", true );
        setTimeout(function(){ 
             $(".agregarconsumibles" ).prop( "disabled", false );
        }, 10000);
    	if ($("#tabla_cons tbody > tr").length>0) {
    		agregarconsumibles();
    	}else{
    		
    	}
    	
    });
    $('.agregarconsumibles_poliza').click(function(event) {
        if ($("#tabla_cons tbody > tr").length>0) {
            agregarconsumibles_poliza();
        }else{
            
        }
        
    });
    new DG.OnOffSwitch({
        el: '#tipoview',
        textOn: 'Poliza',
        textOff: 'Contratos',
        trackColorOn:'#F57C00',
        trackColorOff:'#df3039',
        listener:function(name, checked){
            setTimeout(function(){ 
                if($('#tipoview').is(':checked')){
                    $('#table-datatables').hide('show');
                    $('#table-datatables_polizas').show('show');
                    serviciospolizas();
                }else{
                    $('#table-datatables').show('show');
                    $('#table-datatables_polizas').hide('show');
                }
            }, 500);
            
        }
    });
});
function contratoscliente(id){
	$.ajax({
        type:'POST',
        url: base_url+"NewFolio/contratos",
        data: {
        	id:id
        },
        success: function (data){
        	$('#nfcontratos').html(data); 
            $('#nfcontratos').select2();
        }
    });
}
function cargaequipos(){
	var idcontrato = $('#nfcontratos option:selected').val();
	$.ajax({
        type:'POST',
        url: base_url+"NewFolio/cargaequipos",
        data: {
        	contrato:idcontrato
        },
        success: function (data){
            console.log(data);
            var array = $.parseJSON(data);
        	$('#nfequipos').html(array.equipos); 
            $('#nfequipos').select2();
            $('#nfservicios').html(array.servicio);
        }
    });
}
function cargaconsumibles(){
    var costocolor=$('#nfcontratos option:selected').data('rentacolor');
	var idequipo = $('#nfequipos option:selected').data('equipo');
    var idequipoec = $('#nfequipos option:selected').data('excedentecolor');
    var bodega = $('#nbodega option:selected').val();
	$.ajax({
        type:'POST',
        url: base_url+"NewFolio/cargaconsumibles",
        data: {
        	id:idequipo,
            bod:bodega
        },
        success: function (data){
        	$('#nfconsumibles').html(data); 
            console.log('costocolor>0:'+costocolor+' !! idequipoec>0:'+idequipoec)
            if(costocolor>0 || idequipoec>0){
                $('.datatipo_2').prop( "disabled", false );
                $('.datatipo_3').prop( "disabled", false );
                $('.datatipo_4').prop( "disabled", false );
            }else{
                $('.datatipo_2').prop( "disabled", true );
                $('.datatipo_3').prop( "disabled", true );
                $('.datatipo_4').prop( "disabled", true );
            }
            $('#nfconsumibles').select2();
        }
    });

}
var rowconsumible=0;
function addconsumibles(){
	var vcontrato = $('#nfcontratos option:selected').val();
	var vequipo = $('#nfequipos option:selected').val();
    var vequipo_modelo = $('#nfequipos option:selected').data('equipo');
    var vserieid = $('#nfequipos option:selected').data('serieid');
	var vconsumible= $('#nfconsumibles option:selected').val();
	var vclientes= $('#nfclientes option:selected').val();
    var vfservicios= $('#nfservicios option:selected').val();
    var nfecha= $('#nfecha').val();
    var nbodega= $('#nbodega option:selected').val();
    var nbodega_text= $('#nbodega option:selected').text();

	var tcontrato = $('#nfcontratos option:selected').text();
	var tequipo = $('#nfequipos option:selected').text();
	var tconsumible= $('#nfconsumibles option:selected').text();
	var tclientes= $('#nfclientes option:selected').text();
    var tfservicios= $('#nfservicios option:selected').text();
    var vcon_stock= $('#nfconsumibles option:selected').data('stock');

	if(vconsumible>0){
        if(vcon_stock>0){
    	var html='<tr class="add_new_consumible_'+rowconsumible+'">\
                      <td>\
                      	<input type="hidden" id="addnf_cliente" value="'+vclientes+'">'+tclientes+'\
                      </td>\
                      <td>\
                      	<input type="hidden" id="addnf_contrato" value="'+vcontrato+'">'+tcontrato+'\
                      </td>\
                      <td>\
                        <input type="hidden" id="addnf_servicio" value="'+vfservicios+'">'+tfservicios+'\
                      </td>\
                      <td>\
                      	<input type="hidden" id="addnf_equipo_modelo" value="'+vequipo_modelo+'">\
                        <input type="hidden" id="addnf_equiporow" value="'+vequipo+'">\
                        <input type="hidden" id="addnf_serie" value="'+vserieid+'">\
                      '+tequipo+'</td>\
                      <td>\
                      	<input type="hidden" id="addnf_consumible" value="'+vconsumible+'">\
                      	<input type="hidden" id="addnf_consumiblett" value="'+tconsumible+'">\
                      	'+tconsumible+'\
                      </td>\
                      <td>\
                        <input type="hidden" id="addnf_fecha" value="'+nfecha+'">'+nfecha+'\
                      </td>\
                      <td>\
                        <input type="hidden" id="addnf_bodega" value="'+nbodega+'">'+nbodega_text+'\
                      </td>\
                      <td><a class="btn-floating waves-effect waves-light red accent-2" onclick="deleteconsu('+rowconsumible+')"><i class="material-icons">clear</i></a></td>\
                    </tr>';
        $('.addconsumiblesbody').append(html);
        }else{
            alertfunction('Atención!','Consumible sin stock');
        }
    }else{
        alertfunction('Atención!','Seleccione un consumible');
    }
    rowconsumible++;
}
function deleteconsu(id){
	$('.add_new_consumible_'+id).remove();
}
function agregarconsumibles(){
    var TABLAc   = $("#tabla_cons tbody > tr");
    var DATAc  = [];
        TABLAc.each(function(){         
            item = {};
            item ["cliente"]   = $(this).find("input[id*='addnf_cliente']").val();
            item ["equipom"]  = $(this).find("input[id*='addnf_equipo_modelo']").val();
            item ["equipo"]  = $(this).find("input[id*='addnf_equiporow']").val();
            item ["serie"]  = $(this).find("input[id*='addnf_serie']").val();
            item ["contrato"]  = $(this).find("input[id*='addnf_contrato']").val();
            item ["servicio"]  = $(this).find("input[id*='addnf_servicio']").val();
            item ["consumible"]  = $(this).find("input[id*='addnf_consumible']").val();
            item ["consumiblett"]  = $(this).find("input[id*='addnf_consumiblett']").val();
            item ["fecha"]  = $(this).find("input[id*='addnf_fecha']").val();
            item ["bodega"]  = $(this).find("input[id*='addnf_bodega']").val();
            DATAc.push(item);
        });
        INFOc  = new FormData();
        aInfoc   = JSON.stringify(DATAc);
    //===========================================
    datos = 'consumibles='+aInfoc;
        $.ajax({
            type:'POST',
            url: base_url+"index.php/NewFolio/addconsumibles",
            data: datos,
            success: function (response){
                    alertfunction('Éxito!','Se han agregados los consumibles: ');  
                    setTimeout(function(){ 
                        location.reload();
                    }, 2000);
                    
            },
            error: function(response){
                alertfunction('Atención!','Algo salió mal, intente de nuevo o contacte al administrador del sistema');
            }
        });

}
function alertfunction(titlea,contenta){
    $.alert({boxWidth: '30%',useBootstrap: false,title: titlea,theme: 'bootstrap',content: contenta});  
}
//=============================================================================================
function serviciospolizas(){
    $('#nfclientes_poliza').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una opción',
        allowClear: true,
        ajax: {
            url: base_url+'Ventasi/searchclientes',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.id,
                        text: element.empresa

                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function (e) {
          var data = e.params.data;
          polizascliente(data.id);
    });
}
function polizascliente(id){
    console.log(id);
    $.ajax({
        type:'POST',
        url: base_url+"NewFolio/polizas",
        data: {
            id:id
        },
        success: function (data){
            $('#nfcontratos_poliza').html(data); 
            $('#nfcontratos_poliza').select2();
        }
    });
}
function cargaequipos_poliza(){
    var idcontrato = $('#nfcontratos_poliza option:selected').val();
    $.ajax({
        type:'POST',
        url: base_url+"NewFolio/cargaequipospoliza",
        data: {
            contrato:idcontrato
        },
        success: function (data){
            console.log(data);
            var array = $.parseJSON(data);
            $('#nfequipos_poliza').html(array.equipos); 
            $('#nfequipos_poliza').select2();
            $('#nfservicios_poliza').html(array.servicio);
        }
    });
}
function cargaconsumibles_poliza(){
    var idequipo = $('#nfequipos_poliza option:selected').data('equipo');
    var bodega = $('#nbodega_poliza option:selected').val();
    $.ajax({
        type:'POST',
        url: base_url+"NewFolio/cargaconsumibles",
        data: {
            id:idequipo,
            bod:bodega
        },
        success: function (data){
            $('#nfconsumibles_poliza').html(data); 
            $('#nfconsumibles_poliza').select2();
        }
    });
}
function addconsumibles_poliza(){
    var vcontrato = $('#nfcontratos_poliza option:selected').val();
    var vequipo = $('#nfequipos_poliza option:selected').val();
    var vserieid = $('#nfequipos_poliza option:selected').data('serieid');
    var vconsumible= $('#nfconsumibles_poliza option:selected').val();
    var vclientes= $('#nfclientes_poliza option:selected').val();
    var vfservicios= $('#nfservicios_poliza option:selected').val();
    var nfecha= $('#nfecha_poliza').val();
    var nbodega= $('#nbodega_poliza option:selected').val();
    var nbodega_text= $('#nbodega_poliza option:selected').text();

    var tcontrato = $('#nfcontratos_poliza option:selected').text();
    var tequipo = $('#nfequipos_poliza option:selected').text();
    var tconsumible= $('#nfconsumibles_poliza option:selected').text();
    var tclientes= $('#nfclientes_poliza option:selected').text();
    var tfservicios= $('#nfservicios_poliza option:selected').text();

    var vcon_stock= $('#nfconsumibles_poliza option:selected').data('stock');
    if(vconsumible>0){
        if(vcon_stock>0){
        var html='<tr class="add_new_consumible_'+rowconsumible+'">\
                      <td>\
                        <input type="hidden" id="addnf_cliente" value="'+vclientes+'">'+tclientes+'\
                      </td>\
                      <td>\
                        <input type="hidden" id="addnf_contrato" value="'+vcontrato+'">'+tcontrato+'\
                      </td>\
                      <td>\
                        <input type="hidden" id="addnf_servicio" value="'+vfservicios+'">'+tfservicios+'\
                      </td>\
                      <td>\
                        <input type="hidden" id="addnf_equipo" value="'+vequipo+'">\
                        <input type="hidden" id="addnf_serie" value="'+vserieid+'">\
                      '+tequipo+'</td>\
                      <td>\
                        <input type="hidden" id="addnf_consumible" value="'+vconsumible+'">\
                        <input type="hidden" id="addnf_consumiblett" value="'+tconsumible+'">\
                        '+tconsumible+'\
                      </td>\
                      <td>\
                        <input type="hidden" id="addnf_fecha" value="'+nfecha+'">'+nfecha+'\
                      </td>\
                      <td>\
                        <input type="hidden" id="addnf_bodega" value="'+nbodega+'">'+nbodega_text+'\
                      </td>\
                      <td><a class="btn-floating waves-effect waves-light red accent-2" onclick="deleteconsu('+rowconsumible+')"><i class="material-icons">clear</i></a></td>\
                    </tr>';
        $('.addconsumiblesbody_poliza').append(html);
        }else{
            alertfunction('Atención!','consumible sin stock');
        }
    }else{
        alertfunction('Atención!','Seleccione un consumible');
    }
    rowconsumible++;
}
function agregarconsumibles_poliza(){
    var TABLAc   = $("#tabla_cons tbody > tr");
    var DATAc  = [];
        TABLAc.each(function(){         
            item = {};
            item ["cliente"]   = $(this).find("input[id*='addnf_cliente']").val();
            item ["equipo"]  = $(this).find("input[id*='addnf_equipo']").val();
            item ["serie"]  = $(this).find("input[id*='addnf_serie']").val();
            item ["contrato"]  = $(this).find("input[id*='addnf_contrato']").val();
            item ["servicio"]  = $(this).find("input[id*='addnf_servicio']").val();
            item ["consumible"]  = $(this).find("input[id*='addnf_consumible']").val();
            item ["consumiblett"]  = $(this).find("input[id*='addnf_consumiblett']").val();
            item ["fecha"]  = $(this).find("input[id*='addnf_fecha']").val();
            item ["bodega"]  = $(this).find("input[id*='addnf_bodega']").val();
            DATAc.push(item);
        });
        INFOc  = new FormData();
        aInfoc   = JSON.stringify(DATAc);
    //===========================================
    datos = 'consumibles='+aInfoc;
        $.ajax({
            type:'POST',
            url: base_url+"index.php/NewFolio/addconsumibles_poliza",
            data: datos,
            success: function (response){
                    alertfunction('Éxito!','Se han agregados los consumibles: ');  
                    setTimeout(function(){ 
                        location.reload();
                    }, 2000);
                    
            },
            error: function(response){
                alertfunction('Atención!','Algo salió mal, intente de nuevo o contacte al administrador del sistema');
            }
        });

}
function obtenerventaspendientes(idcliente,blq_perm_ven_ser){
    
    //restriccion para cuando tenga mas de 3 prefacturas pendientes de agregar pago
    $.ajax({
        type:'POST',
        url: base_url+'index.php/Reportes/rventasnum/'+idcliente,
        success:function(data){
            console.log(data);
            var numventas=parseInt(data);
            //=======================
                if(blq_perm_ven_ser==1){
                    var html_contect='El cliente tiene '+numventas+' pendientes de pago!';
                }else{
                    var html_contect='El cliente tiene '+numventas+' pendientes de pago!<br> Para continuar se necesita permisos de administrador<br>'+
                                         '<input type="text" placeholder="Contraseña" id="contrasena" name="contrasena" class="name form-control"  onpaste="return false;" required/>';
                }
            //=======================
            if(numventas>0){
                $.confirm({
                    boxWidth: '30%',
                    useBootstrap: false,
                    icon: 'fa fa-warning',
                    title: 'Atención!',
                    content: html_contect,
                    type: 'red',
                    typeAnimated: true,
                    buttons:{
                        confirmar: function (){
                            if(blq_perm_ven_ser==1){

                            }else{
                              //var pass=$('#contrasena').val();
                              var pass=$("input[name=contrasena]").val();
                              if (pass!='') {
                                   $.ajax({
                                      type:'POST',
                                      url: base_url+"index.php/Sistema/solicitarpermiso/"+idcliente,
                                      data: {
                                          pass:pass
                                      },
                                      success: function (response){
                                              var respuesta = parseInt(response);
                                              if (respuesta==1) {
                                              }else{
                                                  notificacion(2);
                                                  setTimeout(function(){
                                                      location.reload();
                                                  },1000);
                                              }
                                      },
                                      error: function(response){
                                          notificacion(1);
                                          setTimeout(function(){
                                                      location.reload();
                                          },2000);
                                      }
                                  });
                                  
                              }else{
                                  notificacion(0);
                                  setTimeout(function(){
                                                      location.reload();
                                  },2000);
                              }
                            }
                        },
                        'Ver Pendientes': function (){
                            obtenerventaspendientesinfo(idcliente);
                        },
                        cancelar: function (){
                          if(blq_perm_ven_ser==1){}else{
                            setTimeout(function(){
                                location.reload();
                            },1000);
                          }
                        }
                    }
                });
                setTimeout(function(){
                    new MaskedPassword(document.getElementById("contrasena"), '\u25CF');
                },1000);
            }
        }
    });
    
    
}
validar_ultimos_servicios();
        //
function validar_ultimos_servicios(){
    var nser = $('#nfservicios option:selected').val();
    if(nser==1){
        //====================================
            var serie = $('#nfequipos option:selected').data('serie');
            var cli_aux = $('#nfclientes option:selected').val();
            if(serie!=''){
                if(cli_aux>0){
                  $.ajax({
                      type:'POST',
                      url: base_url+"index.php/Asignaciones/obtenerultimosservicios/"+cli_aux+"/"+0+"/"+serie,
                      data: {
                          cli:cli_aux,
                          tipo:0
                      },
                      success: function (response){
                          var cant=parseInt(response);
                          console.log(cant);
                            if(cant>0){
                                //===================================================
                                  var html_contect='La serie tiene un servicio en los ultimos dias<br> Para continuar se necesita permisos de administrador<br>'+
                                                           '<input type="text" placeholder="Contraseña" id="contrasena2" name="contrasena2" class="name form-control-bmz" style="width:99%"  onpaste="return false;" required/>';
                                      $.confirm({
                                          boxWidth: '30%',
                                          useBootstrap: false,
                                          icon: 'fa fa-warning',
                                          title: 'Atención!',
                                          content: html_contect,
                                          type: 'red',
                                          typeAnimated: true,
                                          buttons:{
                                              confirmar: function (){
                                                  
                                                      //var pass=$('#contrasena').val();
                                                      var pass=$("input[name=contrasena2]").val();
                                                      if (pass!='') {
                                                           $.ajax({
                                                              type:'POST',
                                                              url: base_url+"index.php/Sistema/solicitarpermiso",
                                                              data: {
                                                                  pass:pass
                                                              },
                                                              success: function (response){
                                                                      var respuesta = parseInt(response);
                                                                      if (respuesta==1) {
                                                                            //=============================
                                                                                addconsumibles();
                                                                            //==================================
                                                                      }else{
                                                                          
                                                                          
                                                                      }
                                                              },
                                                              error: function(response){
                                                                  notificacion(1);
                                                              }
                                                          });
                                                          
                                                      }else{
                                                          notificacion(0);
                                                          
                                                      }
                                                  
                                                  
                                              },
                                              cancelar: function (){
                                                  
                                              }
                                          }
                                      });
                                      setTimeout(function(){
                                          new MaskedPassword(document.getElementById("contrasena2"), '\u25CF');
                                      },1000);
                                //===================================================
                            }else{
                                addconsumibles();
                            }
                      },
                      error: function(response){
                          notificacion(1);
                      }
                  });
                }
            }
        //====================================
    }else{
        addconsumibles();
    }
}
function seleccionser(){
    var ser = $('#nfservicios option:selected').val();
    if(ser>1){
        $('.nfservicios .b-input-group-append').html('<button class="b-btn b-btn-primary" type="button" onclick="obternerdatosservicio('+ser+',1)"><i class="fas fa-info-circle infoicon"></i></button>');
    }else{
        $('.nfservicios .b-input-group-append').html('');
    }
}