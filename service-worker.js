/*
 Copyright 2016 Google Inc. All Rights Reserved.
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
     http://www.apache.org/licenses/LICENSE-2.0
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/

// Names of the two caches used in this version of the service worker.
// Change to v2, etc. when you update any of the local resources, which will
// in turn trigger the install event again.
const PRECACHE = 'kyocera_app_v11';
const RUNTIME = 'runtime';
const VERSION = 'v11'; // Cambia la versión
//let idper = localStorage.getItem('idper');
let urlParams = new URLSearchParams(self.location.search);
let idper = urlParams.get('idper');
//let base_url = urlParams.get('base_url');idperf
let idperf = urlParams.get('idperf');

//const base_url = document.getElementById('base_url').value;
//var idper=0;
// A list of local resources we always want to be cached.
/*const PRECACHE_URLS = [
  //'Inicio', // Alias for index.html
  'public/images/favicon.ico',
  'build/js/custom.min.js',
  'vendors/Chart.js/dist/Chart.min.js',
  'vendors/DateJS/build/date.js',
  'Login',
  'vendors/Flot/jquery.flot.js',
  'vendors/bootstrap/dist/css/bootstrap.min.css',
  'public/js/formvalidation/formValidation.min.js',
  'vendors/jquery/dist/jquery.min.js',
  'vendors/datatables.net/js/jquery.dataTables.min.js'
];
*/

const PRECACHE_URLS = [
  '/Login',
  '/app-assets/vendors/perfect-scrollbar/perfect-scrollbar.css',
  '/app-assets/css/icon.css',
  '/app-assets/vendors/sweetalert/dist/sweetalert.css',
  '/app-assets/vendors/flag-icon/css/flag-icon.min.css',
  '/app-assets/css/themes/horizontal-menu/materialize.css',
  '/app-assets/css/themes/horizontal-menu/style.css',
  '/app-assets/css/layouts/style-horizontal.css',
  '/app-assets/vendors/jvectormap/jquery-jvectormap.css',
  '/app-assets/vendors/prism/prism.css',
  '/public/css/fontawesome/css/all.css',
  '/offline.html'
];

//const PRECACHE_URLS = []

// The install handler takes care of precaching the resources we always need.
self.addEventListener('install', event => {
  event.waitUntil(
    caches.open(PRECACHE)
      .then(cache => cache.addAll(PRECACHE_URLS))
      
  );
});

// The activate handler takes care of cleaning up old caches.

self.addEventListener('activate', event => {
  const currentCaches = [PRECACHE, RUNTIME];
  event.waitUntil(
    caches.keys().then(cacheNames => {
      return cacheNames.filter(cacheName => !currentCaches.includes(cacheName));
    }).then(cachesToDelete => {
      return Promise.all(cachesToDelete.map(cacheToDelete => {
        return caches.delete(cacheToDelete);
      }));
    }).then(() => self.clients.claim())
  );
});
let isIntervalSet = false;
self.addEventListener('fetch', function(event){

  event.respondWith(
    caches.match(event.request)
    .then(function(response){
      if (response) {
        return response;
      }
      //return fetch(event.request);
      return fetch(event.request).catch(() => {
        // If fetch fails (e.g. no network), return the offline page
        return caches.match('/offline.html');
      });
    })
  );
  if (!isIntervalSet) {
    if(idperf==5 || idperf==6 || idperf==10 || idperf==12 || idper==1){
    //if(idper==1){
      isIntervalSet = true;
      console.log('xxx1');
      setInterval(() => {
        console.log('xxx idper 0:'+idper);
        if(idper>0){
          console.log('xxx si es major 0:'+idper);
        }else{
          //idper = document.getElementById('idper').value;
          //idper = $('#idper').val();
        }
        console.log('xxx idper:'+idper);
        
        fetch('https://altaproductividadapr.com/index.php/Notificacion/datos/'+idper+'/'+idperf) // Reemplaza '/api/datos' con tu endpoint
        //fetch('http://localhost/kyocera2/index.php/Notificacion/datos/'+idper+'/'+idperf) // Reemplaza '/api/datos' con tu endpoint
          .then(response => response.json())
          .then(data => {
            // Procesar los datos de la respuesta
            console.log(data);
            //var array = JSON.parse(data);
            var array=data;
            if(array.new_ser_add>0){
              console.log(array.new_ser_add);
              var body = "Se ha agregado un nuevo servicio, favor de actualizar su itinerario";
              var icon = "https://altaproductividadapr.com/app-assets/images/favicon/favicon_kyocera.png";
              var title = "Notificación";
              var options = {
                  body: body,      //El texto o resumen de lo que deseamos notificar.
                  icon: icon,      //El URL de una imágen para usarla como icono.
                  lang: "ES",      //El idioma utilizado en la notificación.
                  tag: 'notify',   //Un ID para el elemento para hacer get/set de ser necesario.
                  dir: 'auto',     // izquierda o derecha (auto).
                  renotify: "true" //Se puede volver a usar la notificación, default: false.
              }
              // Creamos la notificación con las opciones que pusimos arriba.
              //var notification = new Notification(title,options);
              self.registration.showNotification(title, options);
            }
          })
          .catch(error => {
            console.error('Error al realizar la consulta:', error);
            console.log(error);
          });
      }, 400000); // 60000 milisegundos = 60 segundos 300000 5 min
    }
  }

});

self.addEventListener('push', (event) => {
  const notif = event.data.json().notification;
  event.waitUntil(self.registration.showNotification(notif.title,{
    body: notif.body,
    icon:notif.image,
    data:{
      url:notif.click_action
    }
  }));
});
self.addEventListener("notificationclick" , (event)=>{
  event.waitUntil(clients.openWindow(event.notification.data.url));
});

// The fetch handler serves responses for same-origin resources from a cache.
// If no response is found, it populates the runtime cache with the response
// from the network before returning it to the page.
/*
self.addEventListener('fetch', event => {
  // Skip cross-origin requests, like those for Google Analytics.
  if (event.request.url.startsWith(self.location.origin)) {
    event.respondWith(
      caches.match(event.request).then(cachedResponse => {
        if (cachedResponse) {
          return cachedResponse;
        }

        return caches.open(RUNTIME).then(cache => {
          return fetch(event.request).then(response => {
            // Put a copy of the response in the runtime cache.
            return cache.put(event.request, response.clone()).then(() => {
              return response;
            });
          });
        });
      })
    );
  }
});
*/