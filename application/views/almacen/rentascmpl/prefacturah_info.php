<?php 
	$addassesorios=1;
?><!DOCTYPE html>
<html>
	<head>
	    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="icon" href="<?php echo base_url(); ?>app-assets/images/favicon/favicon_kyocera.png" sizes="32x32">
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<title>Prefactura</title>
		<!-- jQuery Library -->
    	<script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/jquery-3.3.1.min.js"></script>
    	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <link href="<?php echo base_url(); ?>app-assets/vendors/sweetalert/dist/sweetalert.css" type="text/css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>public/css/fontawesome/css/all.css" type="text/css" rel="stylesheet">

		<style type="text/css">
			.tdstitle{
				background: #c1bcbc;
				font-weight: bold;
				color: white;
			}
			html{
			        -webkit-print-color-adjust: exact;
			}
			input{
				
				padding: 0px !important;
				height: 28px !important;
			}
			select{
				
				padding: 0px !important;
				height: 28px !important;
				
				 font-size: 11px !important;
			}
			@media print{
				input{
					background: transparent;
					border: 0 !important;
					
				}
				.buttonenvio{
					display: none;
				}
				html{
			        -webkit-print-color-adjust: exact;
				}
				.tdstitle{
					background: #c1bcbc;
					font-weight: bold;
					color: white;
				}
				.buttoimprimir{
					display: none;
				}
				select{
					background: transparent;
					border: 0 !important;
					-moz-appearance: none;
					 -webkit-appearance: none;
					 appearance: none;
				}
				.form-control:disabled, .form-control[readonly] {
				    background-color: transparent;
				    opacity: 1;
				}
			}
			table td{
				text-align: center;
				font-size: 11px;
			}
			option:disabled{
				background-color: #dadada;
			}
			<?php $finalizadogeneral=1;?>
		</style>
	</head>
	<body>
		<input type="hidden" id="base_url" name="base_url" value="<?php echo base_url(); ?>">
		<form method="post" id="formprefactura">
			
			<table border="1" width="100%" id="table_productos" class="table table-bordered table-striped">
				<thead>
					<th style="font-size: 11px; ">Cantidad</th>
					<th style="font-size: 11px; ">No. de Parte</th>
					<th style="font-size: 11px; ">Modelo</th>
					<th style="font-size: 11px; ">Serie/Folio</th>
					<th style="font-size: 11px; ">Precio Unitario</th>
					<th style="font-size: 11px; " >Total</th>
					<th></th>
				</tr>
				</thead>
				<tbody>
					<?php 
						$arrays_tr=array();
						$arrays_tr[]=array('equipo'=>0,'html'=>'');
						$totalgeneral=0;
						foreach ($rentaventash->result() as $item) { 
							//=================================
								if($item->entregado_status>0){
									$btn_status='btn-secondary';
									if($item->entregado_status>1){
										$btn_status='btn-success';	
									}
									$info_btn='<br>'.$item->personal.' '.$item->apellido_paterno.'<br>'.$item->entregado_fecha;
								}else{
									$finalizadogeneral=0;
									$btn_status='btn-primary';
									$info_btn='';
								}
							//=================================
							//$totalgeneral=$totalgeneral+($item->precio*$item->cantidad);
							$totalgeneral=$totalgeneral+0;
							$html='';
							$seriesall='';
							$datosseries = $this->ModeloCatalogos->rentaventasdseries($item->id); 
							$usado=0;
							foreach($datosseries->result() as $items) {
								$seriesall.=$items->serie.'<br>';
								$usado=$items->usado;
							}
							$vusado='';
							if($usado==1){
								$vusado='-mc';
							}
							if($item->serie_bodega==2){
								$vusado='-mc';
							}	
					$html.='<tr>
							<td>'.$item->cantidad.'<!--'.$item->id.'--></td>
							<td>'.$item->noparte.'</td>
							<td>'.$item->modelo.$vusado.'</td>
							<td>'.$seriesall.'</td>
							<td>$'.number_format(0,2,'.',',').'</td>
							<td>$'.number_format(0,2,'.',',').'</td>
							<td><button type="button" class="btn '.$btn_status.'" onclick="entregas('.$item->id.',9,'.$item->bodegaId.')"><i class="fas fa-handshake fa-fw"></i></button>'.$info_btn.'</td>
						</tr>';
						$arrays_tr[]=array('equipo'=>$item->id,'html'=>$html);
						}
					?>
					<?php 
						$totalgeneral=0;
						foreach ($rentavacessoriosh->result() as $item) { 
							//=================================
								if($item->entregado_status>0){
									$btn_status='btn-secondary';
									if($item->entregado_status>1){
										$btn_status='btn-success';	
									}
									$info_btn='<br>'.$item->personal.' '.$item->apellido_paterno.'<br>'.$item->entregado_fecha;
								}else{
									$finalizadogeneral=0;
									$btn_status='btn-primary';
									$info_btn='';
								}
							//=================================		
							//$totalgeneral=$totalgeneral+($item->precio*$item->cantidad);
							$totalgeneral=$totalgeneral+0;
							$html='';
							$datosseries = $this->ModeloCatalogos->rentaventasaccessoriosseries($item->id_accesoriod);
							$seriesall='';
							foreach($datosseries->result() as $items) { 
								$seriesall.=$items->serie.'<br>';
							}
						$html.='<tr>
							<td style="font-size: 11px; text-align: center;">'.$item->cantidad.'<!--'.$item->rowequipo.'--></td>
							<td style="font-size: 11px; ">'.$item->no_parte.'</td>
							<td style="font-size: 11px; ">'.$item->nombre.'</td>
							<td style="font-size: 11px; ">'.$seriesall.'</td>
							<td style="font-size: 11px; text-align: center;"  >$'.number_format(0,2,'.',',').'</td>
							<td style="font-size: 11px; text-align: center;" >$'.number_format(0,2,'.',',').'</td>
							<td><button type="button" class="btn '.$btn_status.'" onclick="entregas('.$item->id_accesoriod.',10,'.$item->bodegaId.')"><i class="fas fa-handshake fa-fw"></i></button>'.$info_btn.'</td>
						</tr>';
						$arrays_tr[]=array('equipo'=>$item->rowequipo,'html'=>$html);
						}
					 
						$totalgeneral=0;
						foreach ($rentavconsumiblesh->result() as $item) { 
							//=================================
								if($item->entregado_status>0){
									$btn_status='btn-secondary';
									if($item->entregado_status>1){
										$btn_status='btn-success';	
									}
									$info_btn='<br>'.$item->personal.' '.$item->apellido_paterno.'<br>'.$item->entregado_fecha;
								}else{
									$finalizadogeneral=0;
									$btn_status='btn-primary';
									$info_btn='';
								}
							//=================================	
							//$totalgeneral=$totalgeneral+($item->precio*$item->cantidad);
							$totalgeneral=$totalgeneral+0;
							$html='';
							$html.='<tr>
							<td style="font-size: 11px; text-align: center;">'.$item->cantidad.'<!--'.$item->rowequipo.'--></td>
							<td style="font-size: 11px; ">'.$item->parte.'</td>
							<td style="font-size: 11px; ">'.$item->modelo.'</td>
							<td style="font-size: 11px; ">'.$item->folios.'</td>
							<td style="font-size: 11px; text-align: center;"  >$'.number_format(0,2,'.',',').'</td>
							<td style="font-size: 11px; text-align: center;" >$'.number_format(0,2,'.',',').'</td>
							<td><button type="button" class="btn '.$btn_status.'" onclick="entregasb('.$item->id.',11,'.$item->bodegaId.')"><i class="fas fa-handshake fa-fw"></i></button>'.$info_btn.'</td>
							</tr>';
							$arrays_tr[]=array('equipo'=>$item->rowequipo,'html'=>$html);
						}

						foreach ($arrays_tr as $key => $row) {
						    $aux[$key] = $row['equipo'];
						}
						array_multisort($aux, SORT_ASC, $arrays_tr);
						foreach ($arrays_tr as $key => $row) {
    							echo $row['html'];
						}

					?>	
					
				</tbody>
				<tfoot>
					<tr>
						<td style="font-size: 11px; " >Subtotal</td>
						<td style="font-size: 11px; " >$<?php echo number_format($totalgeneral,2,'.',',');?></td>
						<td style="font-size: 11px; ">Iva</td>
						<td style="font-size: 11px; " >$<?php echo number_format($totalgeneral*0.16,2,'.',',');?></td>
						<td style="font-size: 11px; ">Total</td>
						<td style="font-size: 11px; " >$<?php echo number_format(($totalgeneral*1.16),2,'.',',');?></td>
						<td></td>
					</tr>
				</tfoot>

			</table>
			
			
		</form>
		 
		
	</body>
	<script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/sweetalert/dist/sweetalert.min.js"></script>
	<link href="<?php echo base_url(); ?>app-assets/vendors/confirm/jquery-confirm.min.css" type="text/css" rel="stylesheet">
	<script src="<?php echo base_url(); ?>app-assets/vendors/confirm/jquery-confirm.min.js"></script>
	<!--
<script src="<?php echo base_url(); ?>app-assets/vendors/js/formvalidation/formValidation.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>app-assets/vendors/js/formvalidation/framework/bootstrap.min.js"></script>
-->	<script type="text/javascript">
		function entregas(idrow,tipo,bodegai){
			var htmlselect=$('#personalresult').html();
			var htmlselectb=$('#bodegasresult').html();
			setTimeout(function () {
  				$("#bodegaselect option[value='"+bodegai+"']").attr("selected", true);
  				console.log(bodegai);
			},1000);
			$.confirm({
		        boxWidth: '35%',
		        useBootstrap: false,
		        icon: 'fa fa-warning',
		        title: 'Atención!',
		        content: '¿Desea registrar la entrega?<br>seleccione el tecnico<br><select id="tecnicoselect" class="form-control">'+htmlselect+'</select><br>seleccione bodega<br><select id="bodegaselect" class="form-control">'+htmlselectb+'</select>',
		        type: 'red',
		        typeAnimated: true,
		        buttons:{
		            confirmar: function (){
		                var tec=$('#tecnicoselect option:selected').val();
		                var bode=$('#bodegaselect option:selected').val();
	                     $.ajax({
	                        type:'POST',
	                        url: "<?php echo base_url()?>index.php/Generales/entregas",
	                        data: {
	                            id:idrow,
	                            table:tipo,
	                            personal:tec,
	                            bode:bode

	                        },
	                        success: function (response){
	                            location.reload();
	                        },
	                        error: function(response){
	                            $.alert({
	                                    boxWidth: '30%',
	                                    useBootstrap: false,
	                                    title: 'Error!',
	                                    content: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema'});   
	                             
	                        }
	                    });
		                    
		            },
		            cancelar: function (){
		                
		            }
		        }
		    });
		    bloqueoseries();
		}
		function entregasb(idrow,tipo,bodegai){
			var htmlselect=$('#personalresult').html();
			var htmlselectb=$('#bodegasresult').html();
			setTimeout(function () {
  				$("#bodegaselect option[value='"+bodegai+"']").attr("selected", true);
  				console.log(bodegai);
			},1000);
			$.confirm({
		        boxWidth: '35%',
		        useBootstrap: false,
		        icon: 'fa fa-warning',
		        title: 'Atención!',
		        content: '¿Desea registrar la entrega?<br>seleccione el tecnico<br><select id="tecnicoselect" class="form-control">'+htmlselect+'</select><br>seleccione bodega<br><select id="bodegaselect" class="form-control">'+htmlselectb+'</select>',
		        type: 'red',
		        typeAnimated: true,
		        buttons:{
		            confirmar: function (){
		                var tec=$('#tecnicoselect option:selected').val();
		                var bode=$('#bodegaselect option:selected').val();
	                     $.ajax({
	                        type:'POST',
	                        url: "<?php echo base_url()?>index.php/Generales/entregas",
	                        data: {
	                            id:idrow,
	                            table:tipo,
	                            personal:tec,
	                            bode:bode
	                        },
	                        success: function (response){
	                            location.reload();
	                        },
	                        error: function(response){
	                            $.alert({
	                                    boxWidth: '30%',
	                                    useBootstrap: false,
	                                    title: 'Error!',
	                                    content: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema'});   
	                             
	                        }
	                    });
		                    
		            },
		            cancelar: function (){
		                
		            }
		        }
		    });
		    bloqueoseries();
		}
		function bloqueoseries(){
			setTimeout(function(){ 
				$.ajax({
		        type:'POST',
		        url: '<?php echo base_url()?>AsignacionesSeries/bodegasview',
		        data: {
		            accesorio: 0
		            },
		            async: false,
		            statusCode:{
		                404: function(data){
		                    swal("Error", "404", "error");
		                },
		                500: function(){
		                    swal("Error", "500", "error"); 
		                }
		            },
		            success:function(data){
		                var array = $.parseJSON(data);
		                $.each(array, function(index, item) {
		                    if(parseInt(item.tipov)==parseInt(1)){
		                    	$('.bodega_'+item.bodegaId).attr('disabled',false);
		                    }else{
		                        $('.bodega_'+item.bodegaId).attr('disabled',true);
		                    }
		                
		            });
		            }
		        });
		        console.log('sssss');
			}, 1000);
			
		    
		}
	</script>
	
</html>
<select id="personalresult" style="display: none;">
	<?php 
	$resultstec=$this->Configuraciones_model->view_session_searchTecnico();
	foreach ($resultstec as $item) { ?>
     	<option value="<?php echo $item->personalId;?>"><?php echo $item->tecnico;?></option>
    <?php } ?>
</select>
<select id="bodegasresult" style="display: none;">
	<?php foreach ($resultbodegas->result() as $item) { ?>
		<option value="<?php echo $item->bodegaId;?>" class="bodega_<?php echo $item->bodegaId;?>"><?php echo $item->bodega;?></option>
	<?php } ?>
</select>
<?php 
	if($finalizadogeneral==1){
		$this->ModeloCatalogos->updateCatalogo('rentas_historial',array('e_entrega'=>1,'entregado_fecha'=>date('Y-m-d H:i:s')),array('id'=>$ventaId));
	}
?>