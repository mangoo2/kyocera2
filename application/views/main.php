<?php 
  //$serviciosdeventas=$this->Configuraciones_model->serviciosdeventas(); //
  $resultrefaccionestecnico=$this->Configuraciones_model->refacciones_tecnico();
  $idpersonal = $this->session->userdata('idpersonal');
  $perfilid=$this->session->userdata('perfilid');
  $menusl = $this->menus->getMenus($perfilid);
  $menus = $menusl['menus'];
  $submenusArray = $menusl['submenusArray'];

  $n_p_t =0;//status para visualizar o oculta la notificacion

  if($idpersonal==1 or $idpersonal==18 or $idpersonal==17) {
    $not_pag_tec = $this->Configuraciones_model->notificacion_pagostecnicos();
    $n_p_t =1;
  }

  if ($perfilid==1 or $perfilid==8 or $idpersonal==29) { 
     $serventasinfo=$this->Configuraciones_model->serviciosdeventas_info('','',0); //
     $ser_sol_cli=$this->Configuraciones_model->servicios_solicitados_cli_info();
  }
  if($perfilid==1){
    $resultporpagar=$this->Configuraciones_model->porpagartec(); //
  }
  //$r_not_act=$this->Configuraciones_model->notificacionactidadesporacer($idpersonal);
  //$r_not_act_total=$r_not_act->num_rows();
  $r_not_act_leer=$this->Configuraciones_model->actividadesporleer($idpersonal);
  $r_not_act_total=$r_not_act_leer->num_rows();

  $n_v_web_p=0;
  if ($perfilid==1 or $perfilid==2 or $perfilid==3){
    $not_vent_web_p = $this->Configuraciones_model->ventas_vew_pendientes(0);//0 los elimindos(solo para las pruebas) 1 los activos
    $n_v_web_p=1;
  } 


?>
<style type="text/css">
  #notifications-dropdown_sol_ser li>time {font-size: .8rem; font-weight: 400; margin-left: 38px;}
  .notification-badge{font-size: 11px;}
  .iconosnotificaciones li a{padding-left: 10px; padding-right: 10px;}
  <?php
  if($perfilid==11){ ?>
    .gradient-45deg-light-red-orange {background: #ffffff; background: -webkit-linear-gradient(45deg, #ffffff 0%, #DB1E34 100%); background: linear-gradient(45deg, #fdfdfd 0%, #DB1E34 100%);}
  <?php }
  ?>
  .iframeitinerario iframe{width: 100%;height: 100vh;border: 0px;max-height: 820px;}
  .divmenugroup{display: none;  }
  .divmenugroup{/*background: red;*/width: 70px;height: 70px;z-index: 999;position: absolute;right: 0;top: 64px;}
  .chat-out-ticket, .chat-out-itinerario,.chat-out-registro,.vacaciones-button{width: 55px;height: 55px;border: 0px;}
  .chat-out-ticket{background: url(<?php echo base_url()?>public/img/20.webp);background-size: 100%;}
  .chat-out-itinerario{background: url(<?php echo base_url()?>public/img/21.webp);background-size: 100%;}
  <?php 
    // igual se agrego en el footer
    $fecha = date('Y-m-d');
    $idpersonal = $this->session->userdata('idpersonal');
    $res_asi = $this->ModeloSession->verifi_asistencia($idpersonal,$fecha);
    if($res_asi->num_rows()>0){
      $registro='activo';
    }else{
      $registro='';
      ?>

      <?php
    }
  ?>
  .chat-out-registro{background: url(<?php echo base_url()?>public/img/19.webp);background-size: 100%;}
  .chat-out-registro.activo{background: url(<?php echo base_url()?>public/img/18.webp);background-size: 100%;}
  .vacaciones-button{background: url(<?php echo base_url()?>public/img/vaca.webp);background-size: 100%;}
  .vacaciones-button.v_green{background: url(<?php echo base_url()?>public/img/vaca_black.svg) #4caf50;background-size: 100%;background-size: 100%;border-radius: 37px;}
  .vacaciones-button.v_yellow {background: url(<?php echo base_url()?>public/img/vaca_black.svg) #ff9100;background-size: 100%;background-size: 100%;border-radius: 37px;}
  #chat-out2 {top: 64px;overflow: hidden;}
  .divmenugroup-btn{float: right;}
  .divmenugroup-btn:focus{background-color: transparent;}
  @media only screen and (max-width: 900px) {
    .chat-out-ticket, .chat-out-itinerario,.chat-out-registro,.vacaciones-button{width: 23%;height: 36px;}
    .divmenugroup{display: block;width: 100%;top: auto;height: auto;}
    nav .brand-logo{left: 35%;}
    .divmenugroup-btn{float: inline-end;}
    .chat-out-ticket,.chat-out-itinerario,.chat-out-registro,.vacaciones-button,.chat-out-registro.activo{
      background-size: contain;background-repeat: no-repeat;background-color: #cc0000;margin: 1px 2px 5px 2px;border-radius: 5px;

      transition: color .15s ease-in-out, background-color .15s ease-in-out, border-color .15s ease-in-out, box-shadow .15s ease-in-out;
      box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 1px 5px 0 rgba(0, 0, 0, 0.12), 0 3px 1px -2px rgba(0, 0, 0, 0.2);
    }
    .vacaciones-button.v_yellow{
      background-size: contain;background-repeat: no-repeat;background-color: #ff9100;margin: 1px 2px 5px 2px;border-radius: 5px;

      transition: color .15s ease-in-out, background-color .15s ease-in-out, border-color .15s ease-in-out, box-shadow .15s ease-in-out;
      box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 1px 5px 0 rgba(0, 0, 0, 0.12), 0 3px 1px -2px rgba(0, 0, 0, 0.2);
    }
    .vacaciones-button.v_green{
      background-size: contain;background-repeat: no-repeat;background-color: #4caf50;margin: 1px 2px 5px 2px;border-radius: 5px;

      transition: color .15s ease-in-out, background-color .15s ease-in-out, border-color .15s ease-in-out, box-shadow .15s ease-in-out;
      box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 1px 5px 0 rgba(0, 0, 0, 0.12), 0 3px 1px -2px rgba(0, 0, 0, 0.2);
    }
    .divmenugroup{
      position: relative;z-index: 996;
    }
    .jconfirm-box.jconfirm-hilight-shake.jconfirm-type-red.jconfirm-type-animated{
      width: 90% !important;
    }
  }
  .red_resaltar{box-shadow: 0px 0px 20px rgba(250,10,10,1);background-color: #fbb4b4;animation: infinite resplandorAnimation_alert 2s;}
  @keyframes resplandorAnimation_alert {
  0%,100%{
    box-shadow: 0px 0px 20px rgba(250,10,10,1);
    background-color: #fbb4b4;
  }
  50%{
    background-color:#f9f9f9;
    box-shadow: 0px 0px 0px;
  }
}
time{font-style: italic;font-size: 14px;color: gray;}
.resaltar_none{display:none;}
.ifrafac{border: 0;width: 100%;height: 93vh;}
i.resaltar_icon{
  animation: infinite resaltar_Animation_alert_i 2s;
}
@keyframes resaltar_Animation_alert_i {
  0%,100%{
    color:red;
  }
  50%{
    color:white;
  }
}
</style>
<?php
  if($perfilid==11){
    $url_logo=base_url().'public/img/dimpre.png';
    $url_logo_w='';
  }else{
    $url_logo=base_url().'app-assets/images/1024_kyocera_logo.svg';
    $url_logo_w='width="200px;"';
  }
?>
<body id="layouts-horizontal" style="width: 100%" class="<?php 
      if(isset($_SESSION['darkonly'])){
        echo $_SESSION['darkonly'].' ';
        if($_SESSION['darkonly']==1){
          echo 'dark-only';
        }
      }?>">
    <!-- Start Page Loading -->
    <div id="loader-wrapper">
      <div id="loader"></div>
    </div>
    <!-- End Page Loading -->
    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- START HEADER -->
    <header id="header" class="page-topbar">
      <div class="navbar-fixed">
        <!--------------------------------->
        <!-- BARRA DE NAVEGACIÓN SUPERIOR-->
        <nav class="navbar-color gradient-45deg-light-red-orange">
          <div class="nav-wrapper">
            <ul class="left">
              <!-- LOGO -->
              <li>
                <h1 class="logo-wrapper">
                  <a href="<?php echo base_url(); ?>index.php/Sistema" style="margin-top: 0px;padding: 0px;" class="log brand-logo darken-1"><img class="kyoc" src="<?php echo $url_logo; ?>" alt="materialize logo" <?php echo $url_logo_w; ?> height="50px;" style="height:50px;"></a>
                </h1>
              </li><li ></li>
            </ul>
            <ul class="right hide-on-med-and-down iconosnotificaciones">
              <?php 
                  $html_notif='';
                  if($n_v_web_p==1){
                      $sta_not='grey';
                      $icon='';
                    if($not_vent_web_p->num_rows()>0){
                      $sta_not='orange';
                      $icon='resaltar_icon';
                    }



                    $html_notif.='<li>';
                      $html_notif.='<a href="javascript:void(0);" class="waves-effect waves-block waves-light notification-button" data-activates="notifications-dropdown_n_v_web" title="Ventas web">';
                        $html_notif.='<i class="fas fa-shopping-cart '.$icon.'"><small class="notification-badge '.$sta_not.' accent-3 eux_can_asig_p">'.$not_vent_web_p->num_rows().'</small></i>';
                      $html_notif.='</a>';
                    $html_notif.='</li>';
                  }
                  echo $html_notif;// se puede incluir los otros iconos a esta forma
              ?>
              <?php if($n_p_t==1){ ?>
                <li>
                  <a href="javascript:void(0);" class="waves-effect waves-block waves-light notification-button" data-activates="notifications-dropdown_n_p_t" title="Pagos realizados por tecnicos">
                    <i class="fas fa-wallet"><small class="notification-badge <?php if($not_pag_tec->num_rows()>0){ echo 'orange';}else{ echo 'grey';} ?> accent-3 eux_can_asig_p"><?php echo $not_pag_tec->num_rows() ?></small></i>
                  </a>
                </li>
              <?php } ?>
              <?php if ($perfilid==1 or $perfilid==8 or $idpersonal==29) { ?>
                <li>
                <a href="javascript:void(0);" class="waves-effect waves-block waves-light notification-button" data-activates="notifications-dropdown_sor_sol_cli" title="Solicitudes de servicio">
                  <i class="material-icons">blur_circular<small class="notification-badge <?php if($ser_sol_cli->num_rows()>0){ echo 'orange';}else{ echo 'grey';} ?> accent-3 eux_can_asig_p"><?php echo $ser_sol_cli->num_rows() ?></small></i>
                </a>
              </li>
              <?php } ?>
              <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light notification-button" data-activates="notifications-dropdown_r_not_act" title="Actividades por hacer"><i class="fas fa-user-check"><small class="notification-badge <?php if($r_not_act_total>0){ echo 'orange';}else{ echo 'grey';} ?>  accent-3 eux_can_asig_p"><?php echo $r_not_act_total; ?></small></i></a></li>
              <?php if ($perfilid==1) { ?>
              <li>
                <a href="javascript:void(0);" class="waves-effect waves-block waves-light notification-button" data-activates="notifications-dropdown_pagostecnicos" title="Pagos a Tecnicos">
                  <i class="material-icons">build<small class="notification-badge orange accent-3 eux_can_asig_p"><?php echo $resultporpagar->num_rows() ?></small></i>
                </a>
              </li>
              <?php 
                  }
              if ($perfilid==1 or $perfilid==8) { 
                        //$serviciossintenicos=$this->Configuraciones_model->serviciossintenicos(); 
                    ?>
                    <!--<li>
                      <a href="<?php echo base_url(); ?>Listaservicios?tec=0" class="waves-effect waves-block waves-light notification-button" title="Servicios sin tecnicos">
                        <i class="material-icons">build
                          <small class="notification-badge orange accent-3 eux_can_asig_p"></small>
                        </i>
                      </a>
                    </li>-->
                    <?php 
                  }
              if ($perfilid==1 or $perfilid==8 or $idpersonal==29) { ?>
              
              <li>
                <a href="javascript:void(0);" class="waves-effect waves-block waves-light notification-button" data-activates="notifications-dropdown_sol_ser" title="Solicitudes de servicio">
                  <i class="material-icons">build<small class="notification-badge orange accent-3 eux_can_asig_p"><?php echo $serventasinfo->num_rows() ?></small></i>
                </a>
              </li>
              <?php 
                  }
                  if ($perfilid==0 or $perfilid==0) { //if ($perfilid==1 or $perfilid==3) {  
                    $notificaciones = $this->ModeloSession->notificaciones();
                    $auxcontador = $notificaciones->num_rows(); 
                    $notifi_ser_av_ = $this->ModeloSession->notifi_ser_av_();
                    $aux_cont=$notifi_ser_av_->num_rows();
                  }
              ?>
              
              <!-- Notificaciones -->
              <?php if ($perfilid==0 or $perfilid==0) { //if ($perfilid==1 or $perfilid==3) {  ?>
              <li>
                <a href="javascript:void(0);" class="waves-effect waves-block waves-light notification-button" data-activates="notifications-dropdown_av" title="Consumibles / refacciones agregadas a servicios">
                  <i class="material-icons">notifications_none<small class="notification-badge orange accent-3 eux_can_asig_p"><?php echo $aux_cont ?></small></i></a>
              </li>
              <li>
                <a href="javascript:void(0);" class="waves-effect waves-block waves-light notification-button" data-activates="notifications-dropdown" title="Por asignar series">
                  <i class="material-icons">notifications_none<small class="notification-badge orange accent-3"><?php echo $auxcontador ?></small></i></a>
              </li>
              <?php } ?>
              <?php if ($perfilid==0 or $perfilid==0) {  //if ($perfilid==1 or $perfilid==8) { 
                $listsolconsu=$this->ModeloServicio->getfoliosservicios(0,0);
              ?>
                <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light notification-button" data-activates="notifications-dropdown_con" title="Folios por imprimir"><i class="material-icons">local_printshop<small class="notification-badge orange accent-3"><?php echo $listsolconsu->num_rows() ?></small></i></a></li>
              <?php } ?>
              <!-- PANTALLA COMPLETA -->
              <li class="pantalla"><a href="javascript:void(0);" class="waves-effect waves-block waves-light toggle-fullscreen"><i class="material-icons">settings_overscan</i></a></li>
              <!-- NOTIFICACIONES -->
              <!-- MENU USUARIO -->
              <li>
                <div class="mode"><?php
                $darkonly='fa-moon';
      if(isset($_SESSION['darkonly'])){
        if($_SESSION['darkonly']==1){
          $darkonly='fa-lightbulb';
        }
      }?><i class="fas <?php echo $darkonly;?>"></i></div>
              </li>
              <li class="usuario"><a href="javascript:void(0);" class="waves-effect waves-block waves-light profile-button" data-activates="profile-dropdown">Usuario: <?php echo($_SESSION['usuario']); ?></a></li>
            </ul>
            <!-- --->
            <?php 
                $html_nd='';
                if ($n_v_web_p==1) {
                  $html_nd.='<ul id="notifications-dropdown_n_v_web" class="dropdown-content">';
                    $html_nd.='<li>';
                      $html_nd.='<h6><span style="color: white">__</span>Ventas Pendientes web <span class="new badge eux_can_asig">'.$not_vent_web_p->num_rows().'</span></h6>';
                    $html_nd.='</li>';
                    $html_nd.='<li class="divider"></li>';

                      foreach ($not_vent_web_p->result() as $item_ntp) {
                        //$tipo_filtro=0;
                        //if($item_ntp->tipovcp==0){
                          $tipo_filtro=1;//ventas
                        //}
                        //if($item_ntp->tipovcp==1){
                          //$tipo_filtro=2;//combinadas
                        //}
                        //if($item_ntp->tipovcp==2){
                          //$tipo_filtro=4;//polizas
                        //}
                        $html_nd.='<li>';
                                        $html_nd.='<a style="font-size: 15px;" href="'.base_url().'Ventas?idcli='.$item_ntp->idCliente.'&emp='.$item_ntp->empresa.'&tipoventafil='.$tipo_filtro.'&eje=0&viewidpre='.$item_ntp->id.'&fechaview='.$item_ntp->fecha.'&activo='.$item_ntp->activo.'" class="grey-text text-darken-2" title="">';
                                          $html_nd.='<div class="row" style="margin: 0px;">';
                                            $html_nd.='<div class="col s1">';
                                              $html_nd.='<span class="material-icons icon-bg-circle red  small">blur_circular</span>';
                                            $html_nd.='</div>';
                                            $html_nd.='<div class="col s11">';
                                              $html_nd.='<b>Pre: </b>'.$item_ntp->idventa.'</br>';
                                              $html_nd.='<b>Cliente: </b>'.$item_ntp->empresa.'</br>';
                                              $html_nd.='<b>Fecha: </b>'.$item_ntp->fecha.'</br>';
                                            $html_nd.='</div>';
                                            $html_nd.='<div class="col s11">';
                                              $html_nd.='<time class="media-meta" datetime="2022-11-23 14:26:09">'.$item_ntp->reg.'</time>';
                                            $html_nd.='</div>';
                                          $html_nd.='</div>';
                                        $html_nd.='</a>';
                                        
                        $html_nd.='</li>';
                      }
                  $html_nd.='</ul>';

               } 
               echo $html_nd;

            ?>

            <?php if ($n_p_t==1) {?>
            <ul id="notifications-dropdown_n_p_t" class="dropdown-content">
              <li>
                <h6><span style="color: white">__</span>Pagos realizados por tecnicos<span class="new badge eux_can_asig"><?php echo $not_pag_tec->num_rows() ?></span></h6>
              </li>
              <li class="divider"></li>
              <?php
                $html_npt='';
                foreach ($not_pag_tec->result() as $item_ntp) {
                  $tipo_filtro=0;
                  if($item_ntp->tipovcp==0){
                    $tipo_filtro=1;
                  }
                  if($item_ntp->tipovcp==1){
                    $tipo_filtro=2;
                  }
                  if($item_ntp->tipovcp==2){
                    $tipo_filtro=4;
                  }
                  $html_npt.='<li>';
                                  $html_npt.='<a style="font-size: 15px;" href="'.base_url().'Ventas?idcli='.$item_ntp->idCliente.'&emp='.$item_ntp->empresa.'&tipoventafil='.$tipo_filtro.'&eje=0" class="grey-text text-darken-2" title="'.$item_ntp->tipovcpname.'">';
                                    $html_npt.='<div class="row" style="margin: 0px;">';
                                      $html_npt.='<div class="col s1">';
                                        $html_npt.='<span class="material-icons icon-bg-circle red  small">blur_circular</span>';
                                      $html_npt.='</div>';
                                      $html_npt.='<div class="col s11">';
                                        $html_npt.='<b>Pre: </b>'.$item_ntp->idventa.'</br>';
                                        $html_npt.='<b>Cliente: </b>'.$item_ntp->empresa.'</br>';
                                        $html_npt.='<b>Fecha: </b>'.$item_ntp->fecha.'</br>';
                                        $html_npt.='<b>Tipo: </b>'.$item_ntp->tipovcpname.'</br>';
                                      $html_npt.='</div>';
                                      $html_npt.='<div class="col s11">';
                                        $html_npt.='<time class="media-meta" datetime="2022-11-23 14:26:09">'.$item_ntp->reg.'</time>';
                                      $html_npt.='</div>';
                                    $html_npt.='</div>';
                                  $html_npt.='</a>';
                                  
                  $html_npt.='</li>';
                }
                echo $html_npt;
              ?>
              
            </ul>
          <?php } ?>
            <?php if ($perfilid==1 or $perfilid==8 or $idpersonal==29) { //if ($perfilid==1 or $perfilid==8) { ?>
            <ul id="notifications-dropdown_sor_sol_cli" class="dropdown-content">
              <li>
                <h6><span style="color: white">__</span>Solicitudes de servicio de clientes<span class="new badge eux_can_asig"><?php echo $ser_sol_cli->num_rows() ?></span></h6>
              </li>
              <li class="divider"></li>
              <?php
                $html_ssc='';
                foreach ($ser_sol_cli->result() as $item_ssc) {
                  $html_ssc.='<li>';
                              $html_ssc.='<a style="font-size: 15px;" href="#" class="grey-text text-darken-2" title="Venta">';
                                $html_ssc.='<div class="row">';
                                  $html_ssc.='<div class="col s1">';
                                    $html_ssc.='<span class="material-icons icon-bg-circle red  small">blur_circular</span>';
                                  $html_ssc.='</div>';
                                  $html_ssc.='<div class="col s11">';
                                    $html_ssc.='<b>Cliente: </b>'.$item_ssc->empresa.'</br>';
                                    $html_ssc.='<b>Fecha: </b>'.$item_ssc->fecha_sol.'</br>';
                                    $html_ssc.='<b>Modelo: </b>'.$item_ssc->eq_modelo.'</br>';
                                    $html_ssc.='<b>Serie: </b>'.$item_ssc->serie.'</br>';
                                    $html_ssc.='<b>Falla: </b>'.$item_ssc->detalle.'</br>';
                                  $html_ssc.='</div>';
                              $html_ssc.='</a>';
                              $html_ssc.='<time class="media-meta" datetime="2022-11-23 14:26:09">'.$item_ssc->reg.'</time>';
                  $html_ssc.='</li>';
                }
                echo $html_ssc;
              ?>
            </ul>
          <?php } ?>
            <?php if ($perfilid==1 or $perfilid==8 or $idpersonal==29) { //if ($perfilid==1 or $perfilid==8) { ?>
            <ul id="notifications-dropdown_sol_ser" class="dropdown-content">
              <li>
                <h6><span style="color: white">__</span>Solicitudes de servicio<span class="new badge eux_can_asig"><?php echo $serventasinfo->num_rows() ?></span></h6>
              </li>
              <li class="divider"></li>
              <?php 
                $fecha_ser = date("Y-m-d");
                $fecha_ser = date("Y-m-d",strtotime($fecha_ser."- 5 days"));
                $html='';
                foreach ($serventasinfo->result() as $itemvs){ 
                  $idventas=$itemvs->idventa;
                  $servicio=$itemvs->servicio;
                  $fechaentrega=$itemvs->fechaentrega;
                  $notificar_resaltar='';
                  if($itemvs->view==1){
                    $idventas=$itemvs->idventac;
                  }
                  if ($itemvs->prasignaciontipo==2) {
                    if($itemvs->alert==1){
                      $notificar_resaltar='red_resaltar';
                      $fechaentrega=$itemvs->alert_date;
                      if($fechaentrega<$fecha_ser){
                        $notificar_resaltar.=' resaltar_none ';
                      }
                    }
                    
                  }
                  if($itemvs->tipopc==1){
                    $servicio.='  (Preventivo)';
                  }
                  if($itemvs->tipopc==2){
                    $servicio.='  (Correctivo)';
                  }

                  $html.='<li class="avc__1 '.$notificar_resaltar.'">';
                    $html.='<a  style="font-size: 15px;" ';
                  
                              $surl=base_url().'Asignaciones?';
                              $surl.='tasign=2';
                              $surl.='&prasignaciontipo='.$itemvs->prasignaciontipo;//4 ventas
                              $surl.='&servicio='.$itemvs->idventa;//ids ventas ids polisas
                              $surl.='&tipo='.$itemvs->tipo;//individual o comvidana
                              $surl.='&cliente='.$itemvs->idCliente;
                              $surl.='&idpolizaser='.$itemvs->idpolizaser;
                              $surl.='&tipopc='.$itemvs->tipopc;
                              $surl.='';
                              $surl.='';
                              if ($itemvs->prasignaciontipo==4) {
                                  $colornt='cyan';
                                  $titleservicio='Venta';
                              }

                              if ($itemvs->prasignaciontipo==2) {
                                  $colornt='red';
                                  $titleservicio='Poliza';
                              }
                              if ($itemvs->view==1) {
                                  $colornt='red';
                                  $titleservicio='Venta Combinada';
                              }
                      $html.=' href="'.$surl.'" class="grey-text text-darken-2" title="'.$titleservicio.'">';
                        $html.='<span class="material-icons icon-bg-circle '.$colornt.' small">add_shopping_cart</span>';
                        $html.=$idventas.' '.$itemvs->empresa.'<!--</a>-->';
                        $html.='<p class="media-meta new_ser" datetime="">'.$servicio.'</p>';
                        $html.='<time class="media-meta" datetime="'.$itemvs->reg.'">'.$itemvs->reg.' '.$itemvs->vendedor.'</time>'; 
                            if($itemvs->fechaentrega!=''){
                              $html.=' <time class="media-meta" datetime="0">Fecha de solicitud de entrega '.$fechaentrega.'</time>';
                            }
                    $html.='</a>';
                  $html.='</li>';
                }
                echo $html; 
              ?>
            </ul>
          <?php } ?>
            <ul id="notifications-dropdown_r_not_act" class="dropdown-content">
              <li><h6><span style="color: white">__</span>Actividades por hacer<span class="new badge eux_can_asig"><?php echo $r_not_act_total; ?></span></h6></li>
              <li class="divider"></li>
              <?php 
                  $html_act='';
                  foreach ($r_not_act_leer->result() as $itemna){ 
                    $html_act.='<li class="avc__1">';
                      $html_act.='<a style="font-size: 15px;" class="grey-text text-darken-2" title="'.$itemna->titulo.'" onclick="view_actividad('.$itemna->id_actividad.')">';
                        $html_act.='<span class="material-icons icon-bg-circle cyan small">add_shopping_cart</span>'.$itemna->titulo;
                      $html_act.='</a>';
                      $html_act.='<time class="media-meta" datetime="'.$itemna->vigencia.'">'.$itemna->vigencia.'</time>';
                    $html_act.='</li>';
                  } 
                  echo $html_act;
                  ?>
            </ul>
          <?php if ($perfilid==1) {  ?>
            <ul id="notifications-dropdown_pagostecnicos" class="dropdown-content">
              <li><h6><span style="color: white">__</span>Pagos Tecnicos<span class="new badge eux_can_asig"><?php echo $resultporpagar->num_rows() ?></span></h6></li>
              <li class="divider"></li>
              <?php foreach ($resultporpagar->result() as $itemvs){ 
                ?><li class="avc__1"><a style="font-size: 15px;" <?php 
                            $surl=base_url();
                            $surl.='index.php/Ventas?';
                            if($itemvs->tipo==3){
                              //$surl.='index.php/PolizasCreadas?';
                              $colornt='red';
                              $titleservicio='Poliza';
                              $idventapoliza=$itemvs->id;
                            }else{
                              $colornt='cyan';
                              //$surl.='index.php/Ventas?';
                              $titleservicio='Venta';
                              if($itemvs->tipo==1){
                                if($itemvs->equipo>0){
                                    $idventapoliza=$itemvs->equipo;
                                }else if($itemvs->consumibles>0){
                                    $idventapoliza=$itemvs->consumibles;
                                }else if($itemvs->refacciones>0){
                                    $idventapoliza=$itemvs->refacciones;
                                }
                              }else{
                                $idventapoliza=$itemvs->id;
                              }
                            }
                            $surl.='idcli='.$itemvs->idCliente;
                            $surl.='&emp='.$itemvs->empresa;
                            $surl.='&tipoventafil='.$itemvs->tv_fil;
                            $surl.='&viewidpre='.$itemvs->id;
                            $surl.='&eje=0';
                            $surl.='&fechaview='.$itemvs->reg;

                    ?> href="<?php echo $surl;?>" class="grey-text text-darken-2" title="<?php echo $titleservicio;?>"><span class="material-icons icon-bg-circle <?php echo $colornt;?> small">add_shopping_cart</span><?php echo $idventapoliza;?> <?php echo $itemvs->empresa;?></a><time class="media-meta" datetime="<?php echo $itemvs->reg;?>"><?php echo $itemvs->reg;?> <?php echo $itemvs->nombre.' '.$itemvs->apellido_paterno.' '.$itemvs->apellido_materno;?></time></li><?php 
                  } ?>
              
            </ul>
          <?php } ?>
            <?php if ($perfilid==0 or $perfilid==0) { //if ($perfilid==1 or $perfilid==3) {  ?>
            <ul id="notifications-dropdown_av" class="dropdown-content">
              <li>
                <h6><span style="color: white">__</span>Notificaciones<span class="new badge eux_can_asig"><?php echo $aux_cont ?></span></h6>
              </li>
              <li class="divider"></li>
              <?php 
               // consumibles
              foreach ($notifi_ser_av_->result() as $iteme) { 
                if ($iteme->tipo=='c') {
                  ?><li class="avc_<?php echo $iteme->asId ?>_1"><a onclick="eliminar_notifi(<?php echo $iteme->asId ?>,1)" style="font-size: 15px;"><span class="material-icons icon-bg-circle small" style="background-color: #00e676">local_printshop</span>Consumible: <?php echo $iteme->modelo ?> | Cantidad: <?php echo $iteme->cantidad ?><br><span style="color: white">___</span> <time class="media-meta">Fecha: <?php echo date("d/m/Y", strtotime($iteme->reg)); ?> | Hora: <?php echo date("g:i A", strtotime($iteme->reg)); ?></time></a></li><?php
                }
                if ($iteme->tipo=='r') {
                  ?><li class="avc_<?php echo $iteme->asId ?>_2"><a onclick="eliminar_notifi(<?php echo $iteme->asId ?>,2)" style="font-size: 15px;"><span class="material-icons icon-bg-circle small" style="background-color: #00e676">local_printshop</span>Refacción: <?php echo $iteme->modelo ?> | Cantidad: <?php echo $iteme->cantidad ?><br><span style="color: white">___</span> <time class="media-meta">Fecha: <?php echo date("d/m/Y", strtotime($iteme->reg)); ?> | Hora: <?php echo date("g:i A", strtotime($iteme->reg)); ?></time></a></li><?php
                }
              } ?>
            </ul>
            <!-- notifications-dropdown -->
            <ul id="notifications-dropdown" class="dropdown-content">
              <li>
                <h6>Notificaciones<span class="new badge"><?php echo $auxcontador ?></span></h6>
              </li>
              <li class="divider ss"></li>
              <?php foreach ($notificaciones->result() as $iteme) { 
                      if ($iteme->tipo==1) { ?>
                        <li>
                          <a href="<?php echo base_url(); ?>AsignacionesSeries" style="font-size: 15px;"><span class="material-icons icon-bg-circle cyan small">local_printshop</span>Cliente: <?php echo $iteme->cliente ?> | Equipo: <?php echo $iteme->equipo ?> | Vendedor: <?php echo $iteme->vendedor ?></a>
                          <time class="media-meta">Fecha: <?php echo date("d/m/Y", strtotime($iteme->reg)); ?> | Hora: <?php echo date("g:i A", strtotime($iteme->reg)); ?></time>
                        </li>
                    <?php  }
                      if ($iteme->tipo==2) { ?>
                        <li>
                          <a href="<?php echo base_url(); ?>AsignacionesSeries" style="font-size: 15px;"><span class="material-icons icon-bg-circle amber small">local_printshop</span>Cliente: <?php echo $iteme->cliente ?> | Equipo: <?php echo $iteme->equipo ?> | Vendedor: <?php echo $iteme->vendedor ?></a>
                          <time class="media-meta">Fecha: <?php echo date("d/m/Y", strtotime($iteme->reg)); ?> | Hora: <?php echo date("g:i A", strtotime($iteme->reg)); ?></time>
                        </li>
                    <?php  }
                      if ($iteme->tipo==3) { ?>
                        <li>
                          <a href="<?php echo base_url(); ?>AsignacionesSeries" style="font-size: 15px;"><span class="material-icons icon-bg-circle  small" style="background-color: #ff6d00">local_printshop</span>Cliente: <?php echo $iteme->cliente ?> | Equipo: <?php echo $iteme->equipo ?> | Vendedor: <?php echo $iteme->vendedor ?></a>
                          <time class="media-meta">Fecha: <?php echo date("d/m/Y", strtotime($iteme->reg)); ?> | Hora: <?php echo date("g:i A", strtotime($iteme->reg)); ?></time>
                        </li>
                    <?php  }
                      if ($iteme->tipo==4) { ?>
                        <li>
                          <a href="<?php echo base_url(); ?>AsignacionesSeries" style="font-size: 15px;"><span class="material-icons icon-bg-circle small" style="background-color: #00e676">local_printshop</span>Cliente: <?php echo $iteme->cliente ?> | Equipo: <?php echo $iteme->equipo ?> | Vendedor: <?php echo $iteme->vendedor ?></a>
                          <time class="media-meta">Fecha: <?php echo date("d/m/Y", strtotime($iteme->reg)); ?> | Hora: <?php echo date("g:i A", strtotime($iteme->reg)); ?></time>
                        </li>
                    <?php  }
                      if ($iteme->tipo==5) { ?>
                        <li>
                          <a href="<?php echo base_url(); ?>AsignacionesSeries" style="font-size: 15px;"><span class="material-icons icon-bg-circle purple small">local_printshop</span>Cliente: <?php echo $iteme->cliente ?> | Equipo: <?php echo $iteme->equipo ?> | Vendedor: <?php echo $iteme->vendedor ?></a>
                          <time class="media-meta">Fecha: <?php echo date("d/m/Y", strtotime($iteme->reg)); ?> | Hora: <?php echo date("g:i A", strtotime($iteme->reg)); ?></time>
                        </li>
                    <?php  }


                    } ?>
            </ul>
            <?php } ?>
            <?php if ($perfilid==0 or $perfilid==0) { //if ($perfilid==1 or $perfilid==8) { ?>
              <ul id="notifications-dropdown_con" class="dropdown-content">
              <li>
                <h6><span style="color: white">__</span>Notificaciones<span class="new badge eux_can_asig"><?php echo $listsolconsu->num_rows() ?></span></h6>
              </li>
              <li class="divider"></li>
              <?php 
               // consumibles
              foreach ($listsolconsu->result() as $iteme) { 
                  ?><li class="">
                      <a style="font-size: 15px;">
                        <span class="material-icons icon-bg-circle small" style="background-color: #00e676">local_printshop</span>
                        Consumible: <?php echo $iteme->modelo ?> Folio: <?php echo $iteme->foliotext ?><br>
                        <span style="font-size: 13px; color: grey"><?php echo $iteme->empresa ?></span> </a>
                    </li><?php
                
              } ?>
            <?php } ?>
            <!-- DESPLEGADO MENU USUARIO -->
            <ul id="profile-dropdown" class="dropdown-content">
              <?php 
                      if($idpersonal==1 || $idpersonal==9 || $idpersonal==18 || $idpersonal==17 || $idpersonal==12 || $idpersonal==54){//1 administrador 9 israel 18 diana 17 julio 12 guadalupe 54 mayra 
                        echo '<li><a class="grey-text text-darken-1" onclick="permisotemp(30)" ><i class="material-icons">lock_outline</i> Permiso temporal</a></li>';
                      }
                    


              ?>
              <li class="divider"></li>
              <li><a href="<?php echo base_url(); ?>index.php/login/cerrar_sesion" class="grey-text text-darken-1"><i class="material-icons">keyboard_tab</i> Cerrar sesión</a></li>
            </ul>

          </div>
        </nav>
        
        <!-- HORIZONTL NAV START-->
        <nav id="horizontal-nav" class="white hide-on-med-and-down menu">

          <div class="nav-wrapper">
            <ul id="ul-horizontal-nav" class="left hide-on-med-and-down">
              <?php 
              // Crear los menús del perfil
              foreach($menus as $menu){ 
                $class_submenu='';
                foreach($submenusArray as $submenu){
                  foreach($submenu as $item){ 
                    if($item->MenuId==$menu->MenuId){ 
                      $class_submenu.=' _su_m_'.$item->MenusubId.' ';
                    }
                  } 
                }
              ?><li><a class="dropdown-menu active <?php echo $class_submenu;?>" href="#!" data-activates="<?php echo $menu->MenuId; ?>"><i class="material-icons"><?php echo $menu->Icon; ?></i><span><?php echo $menu->Nombre; ?><i class="material-icons right">keyboard_arrow_down</i></span></a></li><?php 
              } ?>
              <li>
                <a class="dropdown-menu active" href="#!" data-activates="20">
                  <i class="material-icons">insert_drive_file</i>
                  <span>Tickets<i class="material-icons right">keyboard_arrow_down</i></span>
                </a>
              </li>
            </ul>
            <?php 
              $btn_flotantes='';
              $n_v_status='';
              if($idpersonal==1 || $idpersonal==17 || $idpersonal==18){
                $strq="SELECT * FROM `asistencias_vacaciones` WHERE activo=1 AND tipo=3 AND aceptada_rechzada IS NULL";
                $query = $this->db->query($strq);
                if($query->num_rows()>0){
                  $n_v_status=' v_yellow ';
                }
              }else{
                $strq="SELECT * FROM `asistencias_vacaciones` WHERE idpersonal='$idpersonal' AND fechafin>now()  AND  activo=1 AND tipo=3 AND aceptada_rechzada IS NULL";
                $query = $this->db->query($strq);
                if($query->num_rows()>0){
                  $n_v_status=' v_yellow ';
                }
                $strq = "SELECT * FROM asistencias_vacaciones as asi WHERE asi.codigog>0 AND asi.activo=1 AND asi.fechafin>now() AND asi.idpersonal='$idpersonal' AND asi.aceptada_rechzada=1";
                $query = $this->db->query($strq);
                if($query->num_rows()>0){
                  $n_v_status=' v_green ';
                }
              }
              $btn_flotantes.='<button data-activates="chat-out" class="waves-effect waves-block waves-light chat-collapse enviodeticket-button chat-out-ticket divmenugroup-btn"  title="Tickets"></button>';
              $btn_flotantes.='<button data-activates="chat-out2" class="waves-effect waves-block waves-light chat-collapse enviodeticket-button chat-out2 chat-out-itinerario divmenugroup-btn"  title="Itinerario"></button>';
              $btn_flotantes.='<button  class="waves-effect waves-block waves-light  enviodeticket-button  chat-out-registro divmenugroup-btn '.$registro.'"  title="Registro Entrada/salida"></button>';
              $btn_flotantes.='<button class="waves-effect waves-block waves-light divmenugroup-btn vacaciones-button '.$n_v_status.'" onclick="vacas()" title="Vacaciones"></button>';
              echo $btn_flotantes;
            ?>
          </div>

        </nav>
        <!-- Dashboarddropdown -->
        <?php 
        
        // Crear los submenús del perfil
        foreach($menus as $menu){ 
            echo '<ul id="'.$menu->MenuId.'" class="dropdown-content dropdown-horizontal-list">';
          
            foreach($submenusArray as $submenu){
              foreach($submenu as $item){ 
                if($item->MenuId==$menu->MenuId){ 
                    if($item->Pagina=='Configuracionrentas/contadores'){
                      $target=' target="_blank" ';
                    }else{
                      $target='';
                    }
                    echo '<li class="_su_m_'.$item->MenusubId.'"><a href="'.base_url().'index.php/'.$item->Pagina.'" '.$target.' >'.$item->Nombre.'</a></li>';
                  
                }
              } 
            }
            echo '</ul>';
        }
        ?>
          <ul id="20" class="dropdown-content dropdown-horizontal-list"><li><a href="<?php echo base_url()."index.php/Tickets"; ?>">Tickets</a></li></ul>
      </div>
    </header>
    <input id="base_url" type="hidden" name="base_url" value="<?php echo base_url(); ?>">
    <!-- END HEADER -->
    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- START MAIN -->
    <div id="main" width="100%">
      <!-- START WRAPPER -->
      <div class="wrapper" width="100%">
        <!-------------------------------------------------->
          <aside id="left-sidebar-nav hide-on-large-only">
          <ul id="slide-out" class="side-nav leftside-navigation">
            <?php // menu movil 
                foreach($menus as $menu){ 
                      $class_submenu='';
                      foreach($submenusArray as $submenu){
                        foreach($submenu as $item){ 
                          if($item->MenuId==$menu->MenuId){ 
                            $class_submenu.=' _su_m_'.$item->MenusubId.' ';
                          }
                        } 
                      }
                      $html_nav='';
                      $html_nav.='<li class="no-padding">';
                        $html_nav.='<ul class="collapsible" data-collapsible="accordion">';
                          $html_nav.='<li class="bold">';
                            $html_nav.='<a class="collapsible-header waves-effect waves-cyan '.$class_submenu.'"><i class="material-icons">'.$menu->Icon.'</i><span>'.$menu->Nombre.'</span></a>';
                            $html_nav.='<div class="collapsible-body">';
                        $html_nav.='<ul>'; 
                                foreach($submenusArray as $submenu){
                                  foreach($submenu as $item){ 
                                    if($item->MenuId==$menu->MenuId){ 
                                      if($item->Pagina=='Configuracionrentas/contadores'){
                                        $target=' target="_blank" ';
                                      }else{
                                        $target='';
                                      }
                                      $html_nav.='<li><a href="'.base_url().'index.php/'.$item->Pagina.'" '.$target.' ><i class="material-icons">keyboard_arrow_right</i><span>'.$item->Nombre.'</span></a></li>';
                                    }
                                  } 
                                }
                  
                          $html_nav.='</ul>';
                        $html_nav.='</div>';
                      $html_nav.='</li>';
                    $html_nav.='</ul>';
                  $html_nav.='</li>';
                  echo $html_nav;

                } ?>
            <?php 
                if($idpersonal==1 || $idpersonal==9 || $idpersonal==18 || $idpersonal==17 || $idpersonal==12 || $idpersonal==54){ 
                  ?><li><a  onclick="permisotemp(70)" ><i class="material-icons">lock_outline</i> Permiso temporal</a></li><?php

                }
              

              ?>
            <li><a href="<?php echo base_url(); ?>index.php/Tickets"><i class="material-icons">insert_drive_file</i><span> Tickets</span></a></li>
            <li><a href="<?php echo base_url(); ?>index.php/login/cerrar_sesion"><i class="material-icons">keyboard_tab</i><span> Cerrar sesión</span></a></li>
            
          </ul>
          <a href="#" data-activates="slide-out" class="sidebar-collapse btn-floating btn-medium waves-effect waves-light hide-on-large-only"><i class="material-icons">menu</i></a>
        </aside>
<!----------------------------------xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx---------------------------------->
<div class="divmenugroup">
  <?php echo $btn_flotantes;?>
</div>
<aside id="right-sidebar-nav">
          <ul id="chat-out" class="side-nav rightside-navigation right-aligned ps-container ps-active-y" style="transform: translateX(100%);">
            <li class="li-hover">
              <div class="row">
                <div class="col s12 border-bottom-1 mt-5">
                  <ul class="tabs">
                    <li class="tab col s5">
                      <a href="#activity" class="active"><span class="material-icons">graphic_eq</span>Ticket</a>
                    </li>
                    
                  <li class="indicator" style="right: 185px; left: 0px;"></li></ul>
                </div>
                <div id="activity" class="col s12 active iframeticket" style="padding: 0px;">

                </div>
              </div>
            </li>
          <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 3px;"><div class="ps-scrollbar-x" style="left: 0px; width: 0px;"></div></div><div class="ps-scrollbar-y-rail" style="top: 0px; height: 546px; right: 3px;"><div class="ps-scrollbar-y" style="top: 0px; height: 298px;"></div></div></ul>
          <ul id="chat-out2" class="side-nav rightside-navigation right-aligned ps-container ps-active-y" style="transform: translateX(100%); width: 400px;">
            <li class="li-hover" style="height: inherit;">
              <div class="row" style="height: inherit;">
                <div class="col s12 border-bottom-1 mt-5">
                  <ul class="tabs">
                    <li class="tab col s5"><a href="#activity" class="active"><span class="material-icons">graphic_eq</span>itinerario</a></li>
                    
                  <li class="indicator" style="right: 185px; left: 0px;"></li></ul>
                </div>
                <div id="activity" class="col s12 active" style="padding: 0px; height: inherit;">
                  <div class="row" style="height: inherit;">
                    <div class="col s12 iframeitinerario" ></div>
                  </div>
                  <!---------------------------------------------------------------------->
                  <!---------------------------------------------------------------------->
                </div>
              </div>
            </li>
          <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 3px;"><div class="ps-scrollbar-x" style="left: 0px; width: 0px;"></div></div><div class="ps-scrollbar-y-rail" style="top: 0px; height: 546px; right: 3px;"><div class="ps-scrollbar-y" style="top: 0px; height: 298px;"></div></div></ul>
          <ul id="chat-out3" class="side-nav rightside-navigation right-aligned ps-container ps-active-y" style="transform: translateX(100%); width: 350px;">
            <li class="li-hover" style="height: inherit;">
              <div class="row" style="height: inherit;">
                <div class="col s12 border-bottom-1 mt-5">
                  <ul class="tabs">
                    <li class="tab col s12"><a href="#activity" class="active"><span class="material-icons">graphic_eq</span>Refacciones</a></li>
                    
                  <li class="indicator" style="right: 185px; left: 0px;"></li></ul>
                </div>
                <div id="activity" class="col s12 active" style="padding: 0px; height: inherit;">
                  <div class="row" style="height: inherit;">
                    <div class="col s12" >
                      <table class="table" id="tablerefaccionestecnico" style="font-size: 12px;">
                        <thead>
                          <tr>
                            <th>Refaccion</th>
                            <th>N. parte</th>
                            <th>Rendimiento</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php foreach ($resultrefaccionestecnico->result() as $item) {
                            echo '<tr><td>'.$item->nombre.'</td><td>'.$item->codigo.'</td><td>'.$item->rendimiento.'</td></tr>';
                            // code...
                          }?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                  <!---------------------------------------------------------------------->
                  <!---------------------------------------------------------------------->
                </div>
              </div>
            </li>
          <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 3px;"><div class="ps-scrollbar-x" style="left: 0px; width: 0px;"></div></div><div class="ps-scrollbar-y-rail" style="top: 0px; height: 546px; right: 3px;"><div class="ps-scrollbar-y" style="top: 0px; height: 298px;"></div></div></ul>

          <ul id="chat-out4" class="side-nav rightside-navigation right-aligned ps-container ps-active-y" style="transform: translateX(100%); width: 350px;">
            <li class="li-hover" style="height: inherit;">
              <div class="row" style="height: inherit;">
                <div class="col s12 border-bottom-1 mt-5">
                  <ul class="tabs">
                    <li class="tab col s12"><a href="#activity" class="active"><span class="material-icons">graphic_eq</span>Prefacturas</a></li>
                    
                  <li class="indicator" style="right: 185px; left: 0px;"></li></ul>
                </div>
                <div id="activity" class="col s12 active" style="padding: 0px; height: inherit;">
                  <div class="row view_info_pre" style="height: inherit;">
                    
                  </div>
                  <!---------------------------------------------------------------------->
                  <!---------------------------------------------------------------------->
                </div>
              </div>
            </li>
          <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 3px;"><div class="ps-scrollbar-x" style="left: 0px; width: 0px;"></div></div><div class="ps-scrollbar-y-rail" style="top: 0px; height: 546px; right: 3px;"><div class="ps-scrollbar-y" style="top: 0px; height: 298px;"></div></div></ul>

          <ul id="chat-out5" class="side-nav rightside-navigation right-aligned ps-container ps-active-y" style="transform: translateX(100%); width: 350px;">
            <li class="li-hover" style="height: inherit;">
              <div class="row" style="height: inherit;">
                <div class="col s12 border-bottom-1 mt-5">
                  <ul class="tabs">
                    <li class="tab col s12"><a href="#activity" class="active"><span class="material-icons">graphic_eq</span>Productos</a></li>
                    
                  <li class="indicator" style="right: 185px; left: 0px;"></li></ul>
                </div>
                <div id="activity" class="col s12 active" style="padding: 0px; height: inherit;">
                  <div class="row view_info_pre5" style="height: inherit;">
                    
                  </div>
                  <!---------------------------------------------------------------------->
                  <!---------------------------------------------------------------------->
                </div>
              </div>
            </li>
          <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 3px;"><div class="ps-scrollbar-x" style="left: 0px; width: 0px;"></div></div><div class="ps-scrollbar-y-rail" style="top: 0px; height: 546px; right: 3px;"><div class="ps-scrollbar-y" style="top: 0px; height: 298px;"></div></div></ul>

        </aside>

  <iframe src="<?php echo base_url(); ?>main/inicio2" style="display: none;"></iframe>
<style type="text/css">
  .des{
    width: 100px;height: 28px;
    color: white;text-align: center;
    padding: 1px 2px 3px 4px;border-radius: 9px;
  }
  .des.con{background: #e44c41;}
  .des.sin{background: #4bc14b;}
</style>
<div id="modalvaca" class="modal" style="z-index: 1003; display: none; opacity: 0; transform: scaleX(0.7); top: 4%;">
  <div class="modal-content ">
    <h5>Seleccionar dias para solicitar vacaciones</h5>
    <div class="row">
      <form id="form_vaca">
        <div class="col s6 m2  l2">
          <label>Fecha Inicio</label>
          <input type="date" class="form-control-bmz" id="vc_fecha_i" name="vc_fecha_i" min="<?php echo date("Y-m-d",strtotime(date("Y-m-d")."+ 6 days"));?>" max="<?php echo date("Y-m-d",strtotime(date("Y-m-d")."+ 15 days"));?>" required>
        </div>
        <div class="col s6 m2  l2">
          <label>Fecha Fin</label>
          <input type="date" class="form-control-bmz" id="vc_fecha_f" name="vc_fecha_f" min="<?php echo date("Y-m-d",strtotime(date("Y-m-d")."+ 6 days"));?>"  max="<?php echo date("Y-m-d",strtotime(date("Y-m-d")."+ 15 days"));?>" required>
        </div>
      </form>
      <div class="col s12 m2  l2">
        <button class="b-btn b-btn-success" onclick="okvaca()" style="margin-top: 20px;" title="agregar">Agregar</button>
      </div>
    </div>
    <div class="row view_sol_vacas">
      
    </div>
  </div>
  <div class="modal-footer">
    <button class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</button>
  </div>
</div>