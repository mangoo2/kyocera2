<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class Clientes_model extends CI_Model 
{
    public function __construct() 
    {
        parent::__construct();
    }

    public function insertar_cliente($dato){
    $this->db->insert('clientes',$dato);   
    return $this->db->insert_id();
    } 

    public function update_cliente($data, $id) {
    $this->db->set($data);
    $this->db->where('id', $id);
    return $this->db->update('clientes');
    }


    public function getListadoClientes()
    {
        $sql = "SELECT
                c.id,
                c.empresa,
               
                c.email,
                c.estado,
                c.municipio,
                c.giro,
                c.direccion,
                c.observaciones
                FROM clientes as c
                WHERE tipo=1 AND estatus=1";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getClientePorId($idCliente)
    {
        $sql = "SELECT * FROM clientes
                WHERE tipo=1 AND id=".$idCliente;
        $query = $this->db->query($sql);
        return $query->row();
    }

    public function insertarToCatalogo($data, $catalogo) {
    return $this->db->insert('' . $catalogo, $data);
    }


    public function getListadoTel_local($idCliente)
    {
        $sql = "SELECT * FROM cliente_has_telefono WHERE idCliente=".$idCliente;
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getListadoTel_celular($idCliente)
    {
        $sql = "SELECT * FROM cliente_has_celular WHERE idCliente=".$idCliente;
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getListadoPersona_contacto($idCliente)
    {
        $sql = "SELECT * FROM cliente_has_persona_contacto WHERE idCliente=".$idCliente;
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function eliminar_tel_local($id) {
    $this->db->where('id', $id);
    return $this->db->delete('cliente_has_telefono');
    }

    public function eliminar_tel_celular($id) {
    $this->db->where('id', $id);
    return $this->db->delete('cliente_has_celular');
    }

    public function eliminar_persona_contacto($id) {
    $this->db->where('id', $id);
    return $this->db->delete('cliente_has_persona_contacto');
    }
   
}

?>