<link href="<?php echo base_url(); ?>public/plugins/select2/select2.min.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/select2/select2.full.min.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>public/js/listado_ventasi_incompletas.js?v=<?php echo date('YmdGi');?>" ></script>
<script type="text/javascript">
	$(document).ready(function($) {
		mostrarpendientes();
		setInterval(function () {mostrarpendientes()}, 10000);//10 seg
	});
	function mostrarpendientes(){
		var letras = [
				<?php
					foreach ($pendientesentrega_reg->result() as $item) {
						$idpre=0;
						if($item->tipotabla==4){
                             if($item->equipo>0){
                                $idpre=$item->equipo;
                            }else if($item->consumibles>0){
                                $idpre=$item->consumibles;
                            }else if($item->refacciones>0){
                                $idpre=$item->refacciones;
                            }
                        }else if($item->tipotabla==2){
                            $idpre=$item->equipo;
                        }else{
                            $idpre=$item->id;
                        }
						?>
						'<?php echo $idpre.' / '.$item->empresa;?>',
						<?php
					}
				?>
						];

// Generar un índice aleatorio
var indiceAleatorio = Math.floor(Math.random() * letras.length);

// Obtener la letra correspondiente al índice aleatorio
var letraAleatoria = letras[indiceAleatorio];

// Mostrar la letra aleatoria en la consola o en tu página web
	//console.log("Letra aleatoria: " + letraAleatoria);
	$('.camposanimados').html('<h6>'+letraAleatoria+'</h6>');
	ejecutaranimacion();
	}
</script>