var base_url = $('#base_url').val();
var statusrow_retrasos=0;
var class_btn='btn waves-effect waves-light btn-bmz';
var tec_v=$('#tecnico').val();
var fecha_hoy = $('#calendarinput').val();
var table;
$(document).ready(function($) {
    $('.modal').modal();
    setTimeout(function(){ 
        cargaclientes();
        verificardesfaces();
    }, 1000);
    servicio_siguiente();
    $('#tableservicioequipos').DataTable({responsive: true});
    $('.mconserok').click(function(event) {
        var refa =$('#solicitudrefaccioncpe').val();
        var prioridadr=$('input:radio[name=prioridadr]:checked').val();
        if (refa!='') {
            if(prioridadr>0){
                $.ajax({
                    type:'POST',
                    url: base_url+'index.php/AsignacionesTecnico/solicitudrefaccion',
                    data: {
                        ser: $('#srid').val(),
                        serve: $('#sried').val(),
                        tipo: $('#srtipo').val(),
                        tipod: $('#srtipod').val(),
                        refaccion: $('#solicitudrefaccioncpe').val(),
                        prioridad: prioridadr
                    },
                    async: false,
                    statusCode:{
                        404: function(data){
                            toastr["error"]("Error 404");
                        },
                        500: function(){
                            toastr["error"]("Error 500");
                        }
                    },
                    success:function(data){
                        toastr["success"]("Solicitud se a enviado!");
                        //$('#modalconsumibleservicio').modal();
                        $('#modalconsumibleservicio').modal('close');

                    }
                });
            }else{
                swal("Debe de seleccionar el tipo de prioridad")
            }
        }else{
            swal("No debe de contener campos vacíos!")
        }
    });
    $('.firmacliente').click(function(event) {
        firmacliente();
    });
    $('.okcambiartiempos').click(function(event) {
        okcambiartiempos();
    });
    obtenernoti();
    $('.iconsearch').click(function(event) {
        var classe = '.search_series';
        var div = $(classe + '.activo');

        // Verifica si el div existe y haz algo con él
        if (div.length > 0) {
            $(classe).removeClass('activo');
        } else {
            $(classe).addClass('activo');
            setTimeout(function(){ 
                $('#searchserie').val('').focus();
            }, 500); //1000 1seg 500 0.5seg 
        }
    });
    $('#searchserie').keypress(function(e) {
        if(e.which == 13) {
            searchserie();
        }
    });
   
});
function cargaclientes(){
    $.ajax({
        type:'POST',
        url: base_url+'index.php/AsignacionesTecnico/clientes',
        data: {
            tecnico: tec_v,
            tipocpe: $('#tipocpe').val(),
            fechaselect:$('#calendarinput').val()
        },
        async: false,
        statusCode:{
            404: function(data){
                toastr["error"]("Error 404");
            },
            500: function(){
                toastr["error"]("Error 500");
            }
        },
        success:function(data){
            console.log(data);
            $('#planta_sucursal').html(data);
        }
    });
    $.ajax({
        type:'POST',
        url: base_url+'index.php/AsignacionesTecnico/clientes2',
        data: {
            tecnico: tec_v,
            tipocpe: $('#tipocpe').val(),
            fechaselect:$('#calendarinput').val()
        },
        async: false,
        statusCode:{
            404: function(data){
                toastr["error"]("Error 404");
            },
            500: function(){
                toastr["error"]("Error 500");
            }
        },
        success:function(data){
            console.log(data);
            $('.addseviciosclientes').html(data);
        }
    });
    $.ajax({
        type:'POST',
        url: base_url+'index.php/AsignacionesTecnico/clientes3',
        data: {
            tecnico: tec_v,
            tipocpe: $('#tipocpe').val(),
            fechaselect:$('#calendarinput').val()
        },
        async: false,
        statusCode:{
            404: function(data){
                toastr["error"]("Error 404");
            },
            500: function(){
                toastr["error"]("Error 500");
            }
        },
        success:function(data){
            console.log('Servicios '+data);
            var ser_count=parseInt(data);
            if(ser_count>0){
                console.log('Con servicios pendientes');
                $.confirm({
                    boxWidth: '90%',
                    useBootstrap: false,
                    icon: 'fa fa-warning',
                    title: 'Advertencia!',
                    content: 'No puede Continuar tiene servicios pendientes por finalizar.',
                    type: 'red',
                    typeAnimated: true,
                    buttons:{
                        confirmar: function (){
                            //=============================================
                                //window.location.href = base_url+'index.php/Configuracionrentas/servicios';
                            //=============================================
                            
                        }
                    }
                });
            }else{
                console.log('Sin servicios pendientes');
            }
            
        }
    });
}
function cargaequipos(){
    var asignacion = $('#planta_sucursal option:selected').val();
    var tipo = $('#planta_sucursal option:selected').data('tipo');
    var fecha = $('#calendarinput').val();
    $.ajax({
        type:'POST',
        url: base_url+'index.php/AsignacionesTecnico/clientesequipos',
        data: {
            asignacion: asignacion,
            tipo: tipo,
            fecha:fecha,
            tecnico:tec_v
        },
        async: false,
        statusCode:{
            404: function(data){
                toastr["error"]("Error 404");
            },
            500: function(){
                toastr["error"]("Error 500");
            }
        },
        success:function(data){
            console.log(data);
            $('#infoser').html('');
            $('#solref').html('');
            $('#tableservicioequipos').DataTable().destroy();
            $('.tableservicioequipostbody').html(data);
            
            $('#tableservicioequipos').DataTable({responsive: true,stateSave: true});
        }
    });
    obtenernoti();
}
function cargaequipos2(asignacion,tipo,solstock){
    servicio_siguiente();
    $('#planta_sucursal').html('<option value="'+asignacion+'" data-tipo="'+tipo+'"></option>');
    $('.estatusservicio').html('');
    $('.infoser').html('');
    $('.solref').html('');
    $('.infocontadores').html('');
    $('.solventas').html('');
    if(solstock==1){
        var contenta='Favor de realizar un conteo del stock de toner del cliente';
        $.alert({boxWidth: '90%',useBootstrap: false,title: 'Stock toner',theme: 'bootstrap',content: contenta});  
    }
    //var asignacion = $('#planta_sucursal option:selected').val();
    //var tipo = $('#planta_sucursal option:selected').data('tipo');
    var fecha = $('#calendarinput').val();
    $.ajax({
        type:'POST',
        url: base_url+'index.php/AsignacionesTecnico/clientesequipos',
        data: {
            asignacion: asignacion,
            tipo: tipo,
            fecha:fecha,
            tecnico:tec_v
        },
        async: false,
        statusCode:{
            404: function(data){
                toastr["error"]("Error 404");
            },
            500: function(){
                toastr["error"]("Error 500"); 
            }
        },
        success:function(data){
            console.log(data);
            $('#infoser').html('');
            $('#solref').html('');
            $('#tableservicioequipos').DataTable().destroy();
            $('.tableservicioequipostbody').html(data);
            
            table=$('#tableservicioequipos').DataTable({
                "lengthMenu": [ 15, 30, 50, 75, 100 ],
                responsive: true,
                "columnDefs": [
                                { "searchable": false, "targets": [0,1,2,3,4,5] },
                                { "orderable": false, "targets": [0,1,2] }
                                ]
            }).on('draw',function(){
                habilitarbtnsearch();
                habilitarconbotonmas();
            });
            var solventas='<a class="'+class_btn+' red col s12" onclick="notificacionrechazogeneral('+asignacion+','+tipo+')" style="margin-top: 5px;"><i class="material-icons">error</i>Rechazo general servicio </a>';
            $('.solventas').html(solventas);
        }
    });
    verificardesfaces();
    //verificar_obtenernoti();
    obtenernoti();
    //saveubi();
}
function click_ser(serv,serve,tipo,equipo,serie,status,statushext,serietext,gara){
    if(gara==1){
        var contenta='Servicio con garantía, Favor de recabar la siguiente documentación:<br>Envento Garantia<br>Actualizacion Firmware<br>Pagina de Estado<br>Simulacion 000<br>Event Log<br>Status Page';
        $.alert({boxWidth: '90%',useBootstrap: false,title: 'GARANTIA',theme: 'bootstrap',content: contenta});  
    }
    if (status==1) {
        var checkedstatus='checked';
        var disabledinicio='disabled';
        var disabledfinalizar='';
    }else{
        var disabledinicio='';
        var disabledfinalizar='disabled';
    }
    if(status==2){
        var disabledinicio='disabled';
        var disabledfinalizar='disabled';
    }
    if(statushext==1){
        var statushextdisabled='disabled';
    }else{
        var statushextdisabled='';
    }
    var statusser='<div class="switch" style="text-align: center; width: 80%; float: left;">\
                        <button class=" green waves-effect waves-light btn-bmz disabledinicio" data-serieinicio="'+serietext+'" onclick="cl_ser_statusser('+serv+','+serve+','+tipo+',0)" '+disabledinicio+'>Iniciar</button>\
                        <button class=" red waves-effect waves-light btn-bmz disabledfinalizar" onclick="cl_ser_statusser('+serv+','+serve+','+tipo+',1)" '+disabledfinalizar+'>Finalizar</button>';
                        if(disabledfinalizar=='disabled'){
                            //statusser+='<button class=" red waves-effect waves-light btn-bmz disabledfinalizar" onclick="cl_ser_statusser2('+serv+','+serve+','+tipo+',1)">Finalizar Servicio</button>';
                        }
        statusser+='</div>\
                    <div style="text-align: center; width: 20%; float: left;">\
                        <label>Tiempo de servicio</label>\
                        <button type="button" '+statushextdisabled+' class="btn-bmz btn-raised btn-icon btn-info mr-1 tiempo_servicio" onclick="cl_ser_tiempo('+serv+','+serve+','+tipo+')"><i class="fas fa-clock"></i></button>\
                    </div>\
                    ';
    var contadores='';
    if (tipo==1) {
        //contadores ='<a class="btn waves-effect waves-light btn-bmz col s12" href="'+base_url+'Configuracionrentas/addcontadores/'+equipo+'/'+serie+'">Capturar contadores</a>';
        var textcontador='/Contador';
    }else{
        var textcontador='';
    }
    
    var infoser ='<a class="'+class_btn+' col s12" onclick="cl_ser_infoser('+serv+','+serve+','+tipo+')">Información del servicio</a>';
    var solref ='<a class="'+class_btn+' col s12" onclick="cl_ser_solref('+serv+','+serve+','+tipo+')">Solicitud de refacción</a>';
    var solventas = '';
        //solventas+='<a class="'+class_btn+' col s12" onclick="cl_ser_ventas('+serv+','+serve+','+tipo+')">Consumibles/Refacciones/Accessorios</a>';
        solventas+='<a class="'+class_btn+' col s12" onclick="cl_ser_comentario('+serv+','+serve+','+tipo+',0)" style="margin-top: 5px;">Agregar Comentario'+textcontador+'</a>';
        solventas+='<a class="'+class_btn+' col s12" onclick="cl_ser_comentario_error('+serv+','+serve+','+tipo+')" style="margin-top: 5px;"><i class="material-icons">error</i>Notificar error</a>';
        solventas+='<a class="'+class_btn+' red col s12" onclick="notificacionrechazo('+serv+','+serve+','+tipo+')" style="margin-top: 5px;"><i class="material-icons">error</i>Rechazo servicio</a>';
    $('.estatusservicio').html(statusser);
    $('.infoser').html(infoser);
    $('.solref').html(solref);
    $('.infocontadores').html(contadores);
    $('.solventas').html(solventas);
    obtenernoti();
}
function cl_ser_infoser(ser,serve,tipo){
    //$('#modalinfoser').modal();
    $('#modalinfoser').modal('open');
    obternerdatosservicio(ser,tipo,serve);
}
function obternerdatosservicio(asignacion,tipo,serve){
    $('.comentariocal').html('');
    $('.equipoacceso').html('');
    var fecha = $('#calendarinput').val();
    $.ajax({
        type:'POST',
        url: base_url+'index.php/calendario/obternerdatosservicio',
        data: {
          asignacion:asignacion,
          tipo:tipo,
          fecha:fecha,
          serve:serve,
          oserier:1
        },
        success:function(data){
            console.log(data);
            var array = $.parseJSON(data);
            $('.datoscliente').html(array.cliente);
            if (tipo==1) {
                $('.datosclientetipo').html('Renta');
            }else if (tipo==2) {
                $('.datosclientetipo').html('Poliza');
            }else if (tipo==3){
                $('.datosclientetipo').html('Evento');
            }else if (tipo==4){
                $('.datosclientetipo').html('Venta');
            }
            if (array.tiposervicio==0) {
                $('.datosclienteasignaciones').html('Asignaciones Mensual');
            }else{
                $('.datosclienteasignaciones').html('Asignaciones diarias');
            }
            $('.datosclienteservicio').html(array.servicio);
            $('.datosclientezona').html(array.zona);
            $('.datosclientetecnico').html(array.tecnico);
            $('.datosclienteprioridad').html(array.prioridad);
            $('.datosclienteahorario').html(array.fecha+' '+array.hora+' '+array.horafin);
            $('.tablerentapoliza').html(array.equipos);
            $('.comentariocal').html(array.comentariocal);
            $('.equipoacceso').html(array.equipoacceso);
            
            ///motivo
            if(array.m_tipo==3){
                $('.motivo_texto').css('display','block');
            }else{
                $('.motivo_texto').css('display','none');
            }
            $('.t_motivo_serv').html(array.m_servicio);
            $('.t_doc_acces').html(array.m_doc_acce);
            $('.id_asigna').html(asignacion);
            $('.id_tipo').html(tipo);
            $('.t_doc_equipo').html(array.equipos);
            $('.t_dconsumibles').html(array.dconsumibles);
            
            
            /*
            if (array.length>0) {
              //
              $('#id_servicio_f').val(array[0].asignacionId);
              $('#fecha').val(array[0].fecha);
              $("#hora").val(array[0].hora);
              $("#horafin").val(array[0].horafin);
              $("#tecnico").val(array[0].tecnico).change();
              $("#test"+array[0].prioridad).click();
              $("#tservicio").val(array[0].tservicio).change();
              $("#tserviciomotivo").val(array[0].tserviciomotivo);

              $("#tpoliza").val(array[0].tpoliza);
              $(".ttipo"+array[0].ttipo).attr('checked', true);
              if (array[0].generado==1) {
                $('.databtndisabled').attr('disabled',true);
              }
            }else{
              $('#id_servicio_f').val(0);
              $('#fecha').val('');
              $("#hora").val('');
              $("#horafin").val('');
              $("#tecnico").val(0).change();
              //$("#test"+array[0].prioridad).click();
              $("#tservicio").val(0).change();
              $("#tserviciomotivo").val('');
            }*/
        }
    });
}
function cl_ser_solrefregresar(){
   $('#modalinfoser').modal('close');
}
function cl_ser_solref(ser,serve,tipo){
    $('.infoser').html('');
    $('.solref').html('');
    var cot1=0;var cot2=0;var cot3=0;
    if(tipo==4){//venta
        cot1=$('.equipo_'+ser).data('cotref1');
        cot2=$('.equipo_'+ser).data('cotref2');
        cot3=$('.equipo_'+ser).data('cotref3');
    }else{
        cot1=$('.equipo_'+serve).data('cotref1');
        cot2=$('.equipo_'+serve).data('cotref2');
        cot3=$('.equipo_'+serve).data('cotref3');
    }
    var class_btn1=' b-btn-warning ';
    var class_btn2=' b-btn-warning ';
    var class_btn3=' b-btn-warning ';
    if(cot1==1){
        class_btn1=' b-btn-success ';
    }
    if(cot2==1){
        class_btn2=' b-btn-success ';
    }
    if(cot3==1){
        class_btn3=' b-btn-success ';
    }
    var opciones ='<div class="row" style="margin-bottom: 5px;">\
                        <div class="col s12">\
                            <a class="b-btn btn-sm '+class_btn1+' col s12 solicitudr_1" onclick="solicitudr('+ser+','+serve+','+tipo+',1)" style="height: 36px;">POR DESGASTE</a>\
                        </div>\
                  </div>\
                  <div class="row" style="margin-bottom: 5px;">\
                        <div class="col s12">\
                            <a class="b-btn btn-sm '+class_btn2+' col s12 solicitudr_2" onclick="solicitudr('+ser+','+serve+','+tipo+',2)" style="height: 36px;">POR DAÑO</a>\
                        </div>\
                  </div>\
                  <div class="row">\
                        <div class="col s12">\
                            <a class="b-btn btn-sm '+class_btn3+' col s12 solicitudr_3" onclick="solicitudr('+ser+','+serve+','+tipo+',3)" style="height: 36px;">POR GARANTIA</a>\
                        </div>\
                  </div>';
    $('.solref').html(opciones);
}
//===================================================================================
function click_servicio(){
	window.location.href = base_url+"index.php/AsignacionesTecnico/servicio"; 
}
function click_servicio2(){
	window.location.href = base_url+"index.php/AsignacionesTecnico/servicio3"; 
}
function click_servicio3(){
	window.location.href = base_url+"index.php/AsignacionesTecnico/servicio4"; 
}
function click_servicio4(){
	window.location.href = base_url+"index.php/AsignacionesTecnico/servicio5"; 
}
function modal_enviar(){
	   /*
       $('.modal').modal({
            dismissible: true
        });
        */
        $('#modal_envio').modal('open');
}
function enviar(){
    toastr["success"]("¡Solicitud generada!");
    setTimeout(function(){ 
        $('#modal_envio').modal('close');
    }, 2000);
}
function click_guardar(){
    toastr["success"]("¡Servicio finalizado!");
}
function solicitudr(ser,serve,tipo,tipod){
    var modelo='';
    var serie='';
    if(tipo==1){//contrato
        modelo=$('.equipo_'+serve).data('modelo');
        serie=$('.equipo_'+serve).data('serie');
    }
    if(tipo==2){//poliza
        modelo=$('.equipo_'+serve).data('modelo');
        serie=$('.equipo_'+serve).data('serie');
    }
    if(tipo==3){//evento
        modelo=$('.equipo_'+serve).data('modelo');
        serie=$('.equipo_'+serve).data('serie');
    }
    if(tipo==4){//venta
        modelo=$('.equipo_'+ser).data('modelo');
        serie=$('.equipo_'+ser).data('serie');
    }
    
    var title_ser='';
    if(tipod==1){
        title_ser=' por desgaste';
    }
    if(tipod==2){
        title_ser='por daño';
    }
    if(tipod==3){
        title_ser=' por garantia';
    }
    var html='';
        html+='<div class="row">';
            html+='<div class="col s12 laben_prio">';
                html+='<input name="prioridadr" type="radio" id="prioridadr_1" value="1" /><label for="prioridadr_1">Prioridad 1</label>';
                html+='<input name="prioridadr" type="radio" id="prioridadr_2" value="2" /><label for="prioridadr_2">Prioridad 2</label>';
                html+='<input name="prioridadr" type="radio" id="prioridadr_3" value="3" /><label for="prioridadr_3">Prioridad 3</label>';
            html+='</div>';
        html+='</div>';
        html+='<div class="row">';
            html+='<div class="col s12">';
                html+='<label>Descripcion</label>';
                html+='<textarea id="solicitudrefaccioncpe"></textarea>';
            html+='</div>';
        html+='</div>';
        html+='<div class="row">';
            html+='<div class="col s12">';
                html+='<label>Contador Final General</label>';
                html+='<input type="number" class="form-control-bmz" id="cot_contador">';
            html+='</div>';
        html+='</div>';
   
    $.confirm({
        boxWidth: '90%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Solicitud de refaccion '+title_ser+' '+modelo+' '+serie,
        content: html,
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                //=============================================
                    var cot_contador = $('#cot_contador').val();
                    if(cot_contador==''){
                        $.alert({boxWidth: '30%',useBootstrap: false,
                            title: 'Advertencia!',
                            content: 'Debe de ingresar el contador!'});
                        return false;
                    }
                    var refa =$('#solicitudrefaccioncpe').val();
                    var prioridadr=$('input:radio[name=prioridadr]:checked').val();
                    if (refa!='') {
                        if(prioridadr>0){
                            $.ajax({
                                type:'POST',
                                url: base_url+'index.php/AsignacionesTecnico/solicitudrefaccion',
                                data: {
                                    ser: ser,
                                    serve: serve,
                                    tipo: tipo,
                                    tipod: tipod,
                                    refaccion: $('#solicitudrefaccioncpe').val(),
                                    prioridad: prioridadr,
                                    cot_contador:cot_contador
                                },
                                async: false,
                                statusCode:{
                                    404: function(data){
                                        toastr["error"]("Error 404");
                                    },
                                    500: function(){
                                        toastr["error"]("Error 500");
                                    }
                                },
                                success:function(data){
                                    var rows = parseInt(data);
                                    if(rows>0){
                                        var class_eq='.solicitudr_'+tipod;
                                        console.log(class_eq);
                                        $(class_eq).removeClass('b-btn-warning').addClass('b-btn-success');
                                    }
                                    toastr["success"]("Solicitud se a enviado!");
                                }
                            });
                        }else{
                            solicitudr(ser,serve,tipo,tipod);
                            swal("Debe de seleccionar el tipo de prioridad")
                        }
                    }else{
                        solicitudr(ser,serve,tipo,tipod);
                        $.alert({boxWidth: '30%',useBootstrap: false,
                            title: 'Advertencia!',
                            content: 'No debe de contener campos vacíos!'});
                        return false;
                    }
                //=============================================
                
            },
            cancelar: function () 
            {
                
            }
        }
    });
    
    setTimeout(function(){ 
        obtenerinfosolicitudrefaccion(ser,serve,tipo,tipod);
    }, 900);
}
function obtenerinfosolicitudrefaccion(ser,serve,tipo,tipod){
    $.ajax({
        type:'POST',
        url: base_url+'index.php/AsignacionesTecnico/obtenerinfosolicitudrefaccion',
        data: {
            ser: ser,
            serve: serve,
            tipo: tipo,
            tipod: tipod
        },
        async: false,
        statusCode:{
            404: function(data){
                toastr["error"]("Error 404");
            },
            500: function(){
                toastr["error"]("Error 500");
            }
        },
        success:function(data){
            var array = $.parseJSON(data);
            console.log(array);
            $('input[name=prioridadr][value='+array.prioridad+']').prop('checked',true);
            $('#solicitudrefaccioncpe').val(array.cot_refaccion);
            $('#cot_contador').val(array.cot_contador);
        }
    });
}
//mismo en calendario
function cl_ser_statusser(serv,serve,tipo,iniciarfinalizar){
    var horaEspecifica =$('.click_ser_'+tipo+'_'+serv).data('horaser');
    var sername =$('.click_ser_'+tipo+'_'+serv).data('sername');
    var sercli =$('.click_ser_'+tipo+'_'+serv).data('sercli');
    var sercliid =$('.click_ser_'+tipo+'_'+serv).data('sercliid');

    var ser_next=0;
    //const ahora = new Date();
    //const [horas, minutos, segundos] = horaEspecifica.split(':');
    //const fechaEspecifica = new Date(ahora.getFullYear(), ahora.getMonth(), ahora.getDate(), horas, minutos, segundos);
    var ser_info='';
    $(".asv_or li").each(function() {
        var ser_array = parseFloat($(this).data('ser'));
        var idcli_array = parseFloat($(this).data('idcli'));
            ser_info += $(this).data('clihrs')+'<br>';
        //if(serv==ser_array){
        if(sercliid==idcli_array){
            ser_next=1;
        }
    });
    if(sercliid==597){//id 597 es de DHL, cuando el servicio sea de este cliente no abra amonestacion
        ser_next=1;
    }

    //console.log(horaEspecifica);
    var serie=$('.disabledinicio').data('serieinicio').toString();
    var serien=serie.substr(-4);
    var serienew = serie.replace(serien, '');
    if(iniciarfinalizar==0){
        if (ser_next==0) {
            console.log("La hora específica es mayor que la hora actual.");
            $.confirm({
                title: 'Amonestación',
                content: 'Recuerda que el itinerario establecido al inicio del dia es la ruta que se tiene que seguir.',
                boxWidth: '30%',  // Ancho personalizado
                useBootstrap: false,  // No usar estilos de Bootstrap
                //theme: 'supervan',  // Un tema de jQuery Confirm, puedes usarlo o definir tu propio tema
                buttons: {
                    confirmar: {
                        text: 'Confirmar',
                        btnClass: 'btn-warning',  // Clase personalizada para el botón
                        action: function () {
                            
                            // Acción al confirmar
                            cl_ser_statusser_pasa(serv,serve,tipo,iniciarfinalizar);
                            $.ajax({
                                type:'POST',
                                url: base_url+'index.php/AsignacionesTecnico/amonestacion',
                                data: {
                                    tec:tec_v,
                                    serv:serv,
                                    tipo:tipo,
                                    hora:horaEspecifica,
                                    sername:sername,
                                    sercli:sercli,
                                    serinfo:ser_info
                                },
                                success:function(data){
                                    
                                }
                            });

                        }
                    }
                },
                onContentReady: function () {
                    // Agregar clases personalizadas a los elementos de la alerta
                    this.$el.addClass('new-alert-person');
                }
            });
        } else {
            console.log("La hora específica no es mayor que la hora actual.");
            cl_ser_statusser_pasa(serv,serve,tipo,iniciarfinalizar);
        }
        
        /*
        $.confirm({
            boxWidth: '60%',
            useBootstrap: false,
            icon: 'fa fa-warning',
            title: 'Atención!',
            content: 'Para inicializar el servicio por favor de completar la serie<br>'+
                     '<input type="text" placeholder="serie" id="serieverifi" class="name form-control" value="'+serienew+'" />',
            type: 'red',
            typeAnimated: true,
            buttons:{
                confirmar: function (){
                    var servf=$('#serieverifi').val();
                    if(servf==serie){
                        cl_ser_statusser_pasa(serv,serve,tipo,iniciarfinalizar);
                    }else{
                        toastr["error"]("La serie no coincide");
                    }
                },
                cancelar: function (){
                    
                }
            }
        });
        */
    }else{
        cl_ser_statusser_pasa(serv,serve,tipo,iniciarfinalizar);
    }
    servicio_siguiente();
}
function cl_ser_statusser_pasa(serv,serve,tipo,iniciarfinalizar){
    var pass=$('.click_ser_'+tipo+'_'+serv).data('pin');
    if(iniciarfinalizar==1){
        var pasaproceso=1;
    }else{
        if($('.servicioinicializado').length>0){
            var pasaproceso=0;
            toastr["info"]("tiene un servicio por finalizar");
        }else{
            var pasaproceso=1;
        }
    }
    if(pasaproceso==1){
        if(pass>0){
            if(iniciarfinalizar==0){
                cl_ser_statusser_pasa_confir(serv,serve,tipo,iniciarfinalizar);
            }else{
                $.confirm({
                    boxWidth: '90%',
                    useBootstrap: false,
                    icon: 'fa fa-warning',
                    title: 'Atención!',
                    content: 'Favor de ingresar ingresar PIN para finalizar<br>'+
                             '<input type="password" placeholder="Contraseña" id="contrasena" class="name form-control" required/>',
                    type: 'red',
                    typeAnimated: true,
                    buttons:{
                        confirmar: function (){
                            var pass_input=$('#contrasena').val();
                            if (pass_input!='') {
                                 if(pass_input==pass){
                                    cl_ser_statusser_pasa_confir(serv,serve,tipo,iniciarfinalizar);
                                 }else{
                                    notificacion(7);
                                 }
                                
                            }else{
                                notificacion(8);
                            }
                        },
                        cancelar: function (){
                            
                        }
                    }
                });
            }
        }else{
            cl_ser_statusser_pasa_confir(serv,serve,tipo,iniciarfinalizar);
        }
    }

}
function cl_ser_statusser_pasa_confir(serv,serve,tipo,iniciarfinalizar){
    $('.equipo_'+serve).removeClass('green');
    $('.equipo_'+serve).addClass('grey');
    var tec = tec_v;
    $.ajax({
        type:'POST',
        url: base_url+'index.php/AsignacionesTecnico/cambiarstatusservicio',
        data: {
            serv:serv,
            serve:serve,
            tipo:tipo,
            iniciarfinalizar:iniciarfinalizar,
            tec:tec
        },
        success:function(data){
            console.log(data);
            var array = $.parseJSON(data);

            //var finalizaco = parseInt(data);
            var finalizaco = parseInt(array.serviciofinales);
            if (finalizaco==1) {
                //$('#firmaclientecoment').val('');
                $('#modalfinservicio').modal({dismissible: false}); 
                $('#modalfinservicio').modal('open');
                $('#fsrid').val(serv);
                $('#fsried').val(serve);
                $('#fsrtipo').val(tipo);
                cl_ser_info(serv,serve,tipo);
                saveubi_ser(serv,tipo,2);
                cl_ser_comentario(serv,serve,tipo,1);//agregar comentario al finalizar por equipo

            }else{
                if(iniciarfinalizar==0){
                    if(array.estatusbloqueoservicio==0){
                        toastr["success"]("Servicio iniciado con éxito. Te recordamos que el servicio finalizará cuando el cliente firme de conformidad");
                    }
                    saveubi_ser(serv,tipo,1);
                    cl_ser_statusser_promedio(serv,serve,tipo);
                }else{
                    toastr["success"]("Equipo finalizado");
                    cl_ser_statusser_promedio(serv,serve,tipo);
                    cl_ser_comentario(serv,serve,tipo,1);//agregar comentario al finalizar por equipo
                }
                verificardesfaces();
            }
            if(iniciarfinalizar==1){
                if(array.not_new_ser==1){
                    //not_new_ser();
                }
            }
            if(array.estatusbloqueoservicio==1){
                //toastr.error("Favor de finalizar el servicio iniciado anteriormente");
                var coms = "Favor de finalizar el servicio iniciado anteriormente";
                $.alert({boxWidth: '90%',useBootstrap: false,title: 'Comentario',theme: 'bootstrap',content: coms});  
            }
            //verificardesfaces();
        }
    });
    if(iniciarfinalizar==0){
        cargaequipos();
        setTimeout(function(){ 
            $('.equipo_'+serve).click().addClass('servicioinicializado');
            $('.disabledinicio').prop( "disabled", true );
            $('.disabledfinalizar').prop( "disabled", false );
        }, 10000);
        setTimeout(function(){ 
            $('.disabledinicio').prop( "disabled", true );
            $('.disabledfinalizar').prop( "disabled", false );
        }, 10000);

    }else{
        cargaequipos();
        $('.equipo_'+serve).removeClass('servicioinicializado');
        setTimeout(function(){ 
            $('.estatusservicio').html('');
            $('.infoser').html('');
            $('.solref').html('');
            $('.infocontadores').html('');
            $('.solventas').html('');
        }, 1000);
    }
}
function not_new_ser(){
    var html='Se ha agregado un nuevo servicio, favor de actualizar su itinerario<br><a class="b-btn b-btn-primary" onclick="not_new_ser2()">Aceptar</a>';
    $('body').loading({theme: 'dark',message: html}); 
}
function not_new_ser2(){
    $('body').loading('stop');
    var tec = tec_v;
    /*
    $.ajax({
        type:'POST',
        url: base_url+'index.php/AsignacionesTecnico/not_new_ser',
        data: {
            tec:tec
        },
        success:function(data){
            
        }
    });
    */
}
function cl_ser_statusser_promedio(serv,serve,tipo){
    $.ajax({
        type:'POST',
        url: base_url+'index.php/AsignacionesTecnico/cambiarstatusservicio_promedio',
        data: {
            serv:serv,
            serve:serve,
            tipo:tipo
        },
        success:function(data){
            var promedio=parseFloat(data);
            console.log('Promedio: '+promedio);
            if(promedio>0){
                toastr["info"]("El Promedio de produccion es de :"+promedio);
                $('.infopromedio').html('Produccion promedio:<b>'+promedio+'</b>');
            }else{
                $('.infopromedio').html('');
            }
        }
    });
}
function cl_ser_statusser2(serv,serve,tipo,iniciarfinalizar){
    
    if(iniciarfinalizar==1){
        var pasaproceso=1;
    }else{
        if($('.servicioinicializado').length>0){
            var pasaproceso=0;
            toastr["info"]("tiene un servicio por finalizar");
        }else{
            var pasaproceso=1;
        }
    }
    if(pasaproceso==1){
        $('.equipo_'+serve).removeClass('green');
        $('.equipo_'+serve).addClass('grey');
        $.ajax({
            type:'POST',
            url: base_url+'index.php/AsignacionesTecnico/cambiarstatusservicio3',
            data: {
                serv:serv,
                serve:serve,
                tipo:tipo,
                iniciarfinalizar:iniciarfinalizar
            },
            success:function(data){
                var finalizaco = parseInt(data);
                if (finalizaco==1) {
                    $('#firmaclientecoment').val('');
                    //$('#modalfinservicio').modal();
                    $('#modalfinservicio').modal({ 
                    dismissible: false}); 
                    $('#modalfinservicio').modal('open');
                    $('#fsrid').val(serv);
                    $('#fsried').val(serve);
                    $('#fsrtipo').val(tipo);
                    cl_ser_info(serv,serve,tipo);
                }else{
                    toastr["success"]("Tiene equipos por concluir");
                }
            }
        });
        if(iniciarfinalizar==0){
            cargaequipos();
            setTimeout(function(){ 
                $('.equipo_'+serve).click().addClass('servicioinicializado');
                $('.disabledinicio').prop( "disabled", true );
                $('.disabledfinalizar').prop( "disabled", false );
            }, 10000);
            setTimeout(function(){ 
                $('.disabledinicio').prop( "disabled", true );
                $('.disabledfinalizar').prop( "disabled", false );
            }, 10000);

        }else{
            cargaequipos();
            $('.equipo_'+serve).removeClass('servicioinicializado');
            setTimeout(function(){ 
                $('.estatusservicio').html('');
                $('.infoser').html('');
                $('.solref').html('');
                $('.infocontadores').html('');
                $('.solventas').html('');
            }, 1000);
        }
    }
}
function cl_ser_info(serv,serve,tipo){
    $.ajax({
        type:'POST',
        url: base_url+'index.php/AsignacionesTecnico/infoser',
        data: {
            serv:serv,
            serve:serve,
            tipo:tipo
        },
        success:function(data){
            var total=0;
            $('.adicionalesrow').html('');
            var array = $.parseJSON(data);
            $('.fmtiposervicio').html(array.fmtiposervicio);
            $('.fmdescripfalla').html(array.fmdescripfalla);
            $('.fmcosto').html(array.fmcosto);
            total=total+parseFloat(array.fmcosto);
            array.consumi.forEach(function(element) {
                    var datos='<div class="col s1 m1 1">'+element.cantidad+'</div>\
                    <div class="col s6 m6 6">'+element.modelo+'</div>\
                    <div class="col s5 m5 5">'+element.costo+'</div>';
                    total=total+parseFloat(element.costo);
                    $('.adicionalesrow').append(datos);
            });
            array.refaccion.forEach(function(element) {
                var datos='<div class="col s1 m1 1">'+element.cantidad+'</div>\
                    <div class="col s6 m6 6">'+element.nombre+'</div>\
                    <div class="col s5 m5 5">'+element.costo+'</div>';
                    total=total+parseFloat(element.costo);
                    $('.adicionalesrow').append(datos);
            });
            var iva=total*0.16;
            $('.fmiva').html(iva.toFixed(2));
            var totalg=total+iva;
            $('.fmtotal').html(totalg.toFixed(2));
            if(array.stock_toner==1){
                $('.stock_toner_comen').show();
            }else{
                $('.stock_toner_comen').hide();
            }
            
        }
    });
}
function firmacliente(){
    var canvas = document.getElementById('patientSignature');
    var dataURL = canvas.toDataURL();
    var fsrid = $('#fsrid').val();
    var fsried = $('#fsried').val();
    var fsrtipo = $('#fsrtipo').val();
    var stock_toner_comen = $('#stock_toner_comen').val();
    $.ajax({
        type:'POST',
        url: base_url+'index.php/AsignacionesTecnico/firmacliente',
        data: {
            fsrid:fsrid,
            fsried:fsried,
            fsrtipo:fsrtipo,
            firma:dataURL,
            stock_toner_comen:stock_toner_comen
        },
        success:function(data){
            console.log(data);
            var array = $.parseJSON(data);
            if(array.comen==0){
                cl_ser_comentario(fsrid,fsried,fsrtipo,1);
            }
            toastr["success"]("Firma registrada!");
            $('#modalfinservicio').modal('close');
        }
    });
    cargaclientes();
}
function cl_ser_ventas(ser,serve,tipo){
    //$('.modal').modal();
    $('#modalventa').modal('open');
    cl_ser_ventas_info(ser,serve,tipo);
}
function cl_ser_ventas_info(ser,serve,tipo){
    $.ajax({
        type:'POST',
        url: base_url+'index.php/AsignacionesTecnico/infoventa',
        data: {
            serv:ser,
            serve:serve,
            tipo:tipo
        },
        success:function(data){
            var total=0;
            $('.tabaddconsul').html('');
            $('.taaddrefaccion').html('');
            $('.taaddaccesorios').html('');

            var array = $.parseJSON(data);
            
            $('.tabaddconsul').html(array.consu);
            $('.taaddrefaccion').html(array.refa);
            $('.taaddaccesorios').html(array.acce);
            $('.totalg').html(array.totalg);
            
        }
    });
}
function delete_ava(id,ser,serve,tipo,table){
    $.confirm({
        boxWidth: '80%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de disminuir accesorio?<br>El cambio será irreversible',
        type: 'red',
        typeAnimated: true,
        buttons: 
        {
            confirmar: function (){
                
                $.ajax({
                    url: base_url+"index.php/AsignacionesTecnico/disminuircantidad/"+id+"/"+table,
                    success: function (response){
                        cl_ser_ventas_info(ser,serve,tipo);
                    },
                    error: function(response){
                        toastr["error"]("Error 500");
                    }
                });
                
            },
            cancelar: function (){
            }
        }
    });
}
function delete_avc(id,ser,serve,tipo,table){
    $.confirm({
        boxWidth: '80%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de disminuir consumible?<br>El cambio será irreversible',
        type: 'red',
        typeAnimated: true,
        buttons: 
        {
            confirmar: function (){
                
                $.ajax({
                    url: base_url+"index.php/AsignacionesTecnico/disminuircantidad/"+id+"/"+table,
                    success: function (response){
                        cl_ser_ventas_info(ser,serve,tipo);
                    },
                    error: function(response){
                        toastr["error"]("Error 500");  
                    }
                });
            },
            cancelar: function () {
            }
        }
    });
}
function delete_avr(id,ser,serve,tipo,table){
    $.confirm({
        boxWidth: '80%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de disminuir refaccion?<br>El cambio será irreversible',
        type: 'red',
        typeAnimated: true,
        buttons: 
        {
            confirmar: function (){
                
                $.ajax({
                    url: base_url+"index.php/AsignacionesTecnico/disminuircantidad/"+id+"/"+table,
                    success: function (response){
                        cl_ser_ventas_info(ser,serve,tipo);
                    },
                    error: function(response){
                        toastr["error"]("Error 500");
                    }
                });
                
            },
            cancelar: function () {
            }
        }
    });
}
function cl_ser_tiempo(serv,serve,tipo){
    $('#fsrid').val(serv);
    $('#fsried').val(serve);
    $('#fsrtipo').val(tipo);
    

    var html_contect='Para continuar se necesita permisos de administrador<br>'+
                    '<input type="text" placeholder="Contraseña" id="contrasena2" name="contrasena2" class="name form-control-bmz" style="width:99%"  onpaste="return false;" required/>';

        $.confirm({
            boxWidth: '30%',
            useBootstrap: false,
            icon: 'fa fa-warning',
            title: 'Atención!',
            content: html_contect,
            type: 'red',
            typeAnimated: true,
            buttons:{
                confirmar: function (){
                    //var pass=$('#contrasena2').val();
                    var pass=$("input[name=contrasena2]").val();
                    if (pass!='') {
                         $.ajax({
                            type:'POST',
                            url: base_url+"index.php/Sistema/solicitarpermiso_l",
                            data: {
                                pass:pass,
                                per:12,
                                per2:54
                            },
                            success: function (response){
                                var respuesta = parseInt(response);
                                if (respuesta==1) {
                                    okcambiartiempos_equipos(serv,tipo);
                                    $('#modalfinserviciotime').modal({dismissible: false}); 
                                    $('#modalfinserviciotime').modal('open');

                                }else{
                                    notificacion(2);
                                }
                            },
                            error: function(response){
                                notificacion(1);
                            }
                        });
                    }else{
                        notificacion(0);
                    }
                },/*
                'Ver Pendientes': function (){
                    obtenerventaspendientesinfo(idcliente);
                },*/
                cancelar: function (){

                }
            }
        });
    
    setTimeout(function(){
        new MaskedPassword(document.getElementById("contrasena2"), '\u25CF');
    },500);
}
function okcambiartiempos(){
    //$( ".tiempo_servicio" ).prop( "disabled", true );
    $('.inp_hi').removeClass('error');
    $('.inp_hf').removeClass('error');
    //=============================================
        var equipos = [];
        var TABLA   = $(".table_tedit_equipos tbody > tr");
        var TABLAlength_equipos=0;
        var TABLA_equipos_valid=1;
        var input_vacios=1;
        TABLA.each(function(){  
            if ($(this).find("input[class*='serivios_tedit']").is(':checked')) {
                item = {};
                item['serve'] = $(this).find("input[id*='asignacion']").val();
                item['tipo'] = $(this).find("input[id*='tipo']").val();
                 
                var v_hi = $(this).find("input[id*='horaserinicioext']").val();
                item['hi'] = v_hi;
                
                var v_hf = $(this).find("input[id*='horaserfinext']").val();
                item['hf'] = v_hf
                
                if (v_hi && v_hf && v_hf < v_hi) {
                    $(this).find("input[id*='horaserfinext']").addClass('error');
                    TABLA_equipos_valid=0;
                }
                if(v_hi==''){
                    input_vacios=0;
                    $(this).find("input[id*='horaserinicioext']").addClass('error');
                }
                if(v_hf==''){
                    input_vacios=0;
                    $(this).find("input[id*='horaserfinext']").addClass('error');
                }
                  
                equipos.push(item);
                TABLAlength_equipos=1;
            }
        });
          aInfoe   = JSON.stringify(equipos);
    //=============================================
    //console.log(aInfoe);
    if(TABLAlength_equipos==1){
        if(TABLA_equipos_valid==1){
            if(input_vacios==1){
                console.log(aInfoe);
                $.ajax({
                    type:'POST',
                    url: base_url+'index.php/AsignacionesTecnico/cambiartiempos',
                    data: {
                        equipos:aInfoe,
                    },
                    async: false,
                    statusCode:{
                        404: function(data){
                            swal("Error", "404", "error");
                        },
                        500: function(){
                            swal("Error", "500", "error"); 
                        }
                    },
                    success:function(data){
                        toastr["success"]("Tiempos actualizados!");
                        //$('#modalfinserviciotime').modal();
                        $('#modalfinserviciotime').modal('close');
                    }
                });
            }else{
                toastr.error('No se acepta campos vacios');
            }
        }else{
            toastr.error('Verifica el horario final de uno de los equipos no puede ser menor al inicio');
        }
    }else{
        toastr.error('Selecciona por lo menos un equipo');
    }
}
function okcambiartiempos_equipos(serv,tipo){
    $.ajax({
        type:'POST',
        url: base_url+'index.php/AsignacionesTecnico/equiposservicios',
        data: {
            serv:serv,
            tipo:tipo,
            tec:tec_v,
            fecha:fecha_hoy
        },
        success:function(data){
            $('.tipe_equipos_info').html(data);
        }
    });
}
function detalle(id){
    window.open(base_url+"PolizasCreadas/view/"+id, "Prefactura", "width=780, height=612");
}
function detallev(id,tipo){
    if (tipo==0) {
        window.open(base_url+"Prefactura/view/"+id, "Prefactura", "width=780, height=612");
    }else{
        window.open(base_url+"Prefactura/viewc/"+id, "Prefactura", "width=780, height=612");
    }
}
toastr.options = {
  "closeButton": false,
  "debug": false,
  "newestOnTop": false,
  "progressBar": true,
  "positionClass": "toast-top-full-width",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "13000",
  "timeOut": "7000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}
function cl_ser_comentario(serv,serve,tipo,view){
    if(view==1){
        var class_btn_cancel='btn_cancel_none';
    }else{
        var class_btn_cancel='';
    }
    var modelo='';
    var serie='';
    if(tipo==1){//contrato
        modelo=$('.equipo_'+serve).data('modelo');
        serie=$('.equipo_'+serve).data('serie');
    }
    if(tipo==2){//poliza
        modelo=$('.equipo_'+serve).data('modelo');
        serie=$('.equipo_'+serve).data('serie');
    }
    if(tipo==3){//evento
        modelo=$('.equipo_'+serve).data('modelo');
        serie=$('.equipo_'+serve).data('serie');
    }
    if(tipo==4){//venta
        modelo=$('.equipo_'+serv).data('modelo');
        serie=$('.equipo_'+serv).data('serie');
    }
    var display='';
    if(tipo==1){
        display='';
    }else{
        display='style="display:none;"';
    }
    if(modelo==undefined){
        modelo='';
        serie='';
    }
    var html='';
        html+='<div class="row">';
            html+='<div class="col s12">';
                html+=modelo+' '+serie;
            html+='</div>';
            html+='<div class="col s12">';
                html+='<label>Comentario</label>';
                html+='<textarea id="comentarioindividual"></textarea>';
                html+='<div class="firm_count_e" style="font-style: italic;font-size: 11px;"></div>';
            html+='</div>';
            html+='<div class="col s6" '+display+'>';
                html+='<label>Contador Final</label>';
                html+='<input type="number" id="info_cont_fin" class="fc-bmz">';
            html+='</div>';
        html+='</div>';
        html+='<div class="row">';
            html+='<div class="col s12" >';
                html+='<label>Nombre de quien validó el servicio</label>';
                html+='<input type="text" id="c_qrecibio" class="fc-bmz">';
            html+='</div>';
            html+='<div class="col s12" >';
                html+='<div class="switch"><label><input type="checkbox" name="paratodoseq" id="paratodoseq"><span class="lever"></span>Mismo nombre para todas la series</label></div>';
            html+='</div>';
        html+='</div>';
    $.confirm({
        boxWidth: '80%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Comentario',
        content: html,
        type: 'red',
        typeAnimated: true,
        buttons: {
            confirmar: {
                text: 'confirmar',
                btnClass: 'b-btn b-btn-success btn_comen_confirm',  // Clase personalizada para el botón
                action: function () {
                    var comen = $('#comentarioindividual').val();
                    var qvalid=$('#c_qrecibio').val();
                    var mismo =$('#paratodoseq').is(':checked')==true?1:0;
                    //var con_i = $('#info_cont_ini').val();
                    var con_f = $('#info_cont_fin').val();
                    if(comen.length>=50){
                        $.ajax({
                            type:'POST',
                            url: base_url+'index.php/AsignacionesTecnico/addcomentarios',
                            data: {
                                serv:serv,
                                serve:serve,
                                tipo:tipo,
                                comentario:comen,
                                contadorfin:con_f,
                                qrecibio:qvalid,
                                mismo:mismo
                            },
                            success:function(data){
                                toastr["success"]("Comentario agregado");
                            }
                        });
                    }else{
                        toastr.error('Favor de ingresar por lo menos  50 caracteres al comentario');
                        cl_ser_comentario(serv,serve,tipo,view);
                        setTimeout(function(){ 
                            $('#comentarioindividual').val(comen);
                            $('.firm_count_e').html(comen.length);
                        }, 900);
                    }
                    

                }
            },
            Cancelar: {
                text: 'Cancelar',
                btnClass: 'b-btn b-btn-secondary '+class_btn_cancel,  // Clase personalizada para el botón
                action: function () {

                }
            }
        }
    });

    $.ajax({
        type:'POST',
        url: base_url+'index.php/AsignacionesTecnico/viewcomentarios',
        data: {
            serv:serv,
            serve:serve,
            tipo:tipo,
        },
        success:function(data){
             var array = $.parseJSON(data);
            $('#comentarioindividual').val(array.comentario);
            //$('#info_cont_ini').val(array.contadorini);
            $('#info_cont_fin').val(array.contadorfin);
            $('.firm_count_e').html(array.comentario.length);
            $('#c_qrecibio').val(array.qrecibio);//implemetar
            if(array.qrecibio.length>5){
                $('#c_qrecibio').prop('readonly',true);
            }
        }
    });

    setTimeout(function(){ 
        $('#comentarioindividual').on('input', function () {
            var count=$(this).val().length;
            $('.firm_count_e').text(count);
            if(count<50){
                $('.firm_count_e').css({
                    color: 'red'
                });
            }else{
                $('.firm_count_e').css({
                    color: 'green'
                });
            }
            var cqr=$('#c_qrecibio').val().length;
            if(cqr>1 && count>=50){
                $('.btn_comen_confirm').prop('disabled',false);
            }else{
                $('.btn_comen_confirm').prop('disabled',true);
            }
        });
        $('#c_qrecibio').on('input', function () {
            var count=$('#comentarioindividual').val().length;
            var cqr=$('#c_qrecibio').val().length;
            if(cqr>1 && count>=50){
                $('.btn_comen_confirm').prop('disabled',false);
            }else{
                $('.btn_comen_confirm').prop('disabled',true);
            }
        });
    }, 500);
    
}

function cl_ser_comentario_error(serv,serve,tipo){
    $.confirm({
        boxWidth: '90%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Notificar de error!',
        content: '<textarea class="form-control-bmz" id="notificarerror"></textarea>',
        type: 'red',
        typeAnimated: true,
        buttons: 
        {
            confirmar: function (){
                var info=$('#notificarerror').val();
                if(info!=''){
                    $.ajax({
                        type:'POST',
                        url: base_url+'index.php/AsignacionesTecnico/viewcomentarios_error',
                        data: {
                            serv:serv,
                            serve:serve,
                            tipo:tipo,
                            info:info
                        },
                        success:function(data){
                            toastr["success"]("Solicitud Agregada");
                        }
                    });
                }else{
                    toastr["error"]("No se permite comentario vació");
                }
            },
            cancelar: function (){
            }
        }
    });
}
function modal_ayuda(){
  //$('#modal_ayuda_color').modal();
  $('#modal_ayuda_color').modal('open');
}
function buscarserie(tr){
    $('body').loader('show');
    var serie=$('.rowtr_'+tr).data('serie');
    setTimeout(function(){ 
        $.ajax({
            type:'POST',
            url: base_url+'index.php/Listaservicios/buscarserie',
            data: {
                serie:serie,
                tipoview:1
            },
            success:function(data){
                $('.seach_serie_tbody').html(data);
                $('#tabla_seach').DataTable();
                $('#tabla_seach0').DataTable();
                $('#tabla_seach1').DataTable();
                $('.collapsible').collapsible();
                  
                setTimeout(function(){ 
                    //$('.modal').modal();
                    $('#modal_seach_serie').modal('open');
                    $('body').loader('hide');
                }, 1000);   
            },
            error: function(response){
                $('body').loader('hide');
            }
        });
    }, 1000);
}
function viewprefacturav(id,tipo){
    if (tipo==0) {
        window.open(base_url+"Prefactura/view/"+id, "Prefactura", "width=780, height=612");
    }else{
        window.open(base_url+"Prefactura/viewc/"+id, "Prefactura", "width=780, height=612");
    }
}
function habilitarconbotonmas(){
    $('.numberidasig').click(function(event) {
        console.log('numberidasig click');
        habilitarbtnsearch()
    });
}
function habilitarbtnsearch(){
    var equipotr = $("#tableservicioequipos tbody > tr");
    var dicserie = $('#tableservicioequipos_filter input').val();
    if(dicserie!=''){
        //toastr["error"]("equipotr.length: "+equipotr.length); 
        if(equipotr.length==1){
                $('.td_button_agregar').hide();
                $('.button_agregar').show();
            setTimeout(function(){ 
                $('.td_button_agregar').hide();
                $('.button_agregar').show();
                //$('.button_agregar').prop('disabled',false);
            }, 1000);
            setTimeout(function(){ 
                //toastr["error"]("dicserie: "+dicserie);
                console.log(dicserie);
                $('.btn_0_'+dicserie).hide();
                $('.btn_1_'+dicserie).show().click();
                setTimeout(function(){ 
                    //$('.disabledinicio').click();
                }, 1000);
            }, 1000);
        }else{
            setTimeout(function(){ 
                //toastr["error"]("dicserie: "+dicserie);
                console.log(dicserie);
                $('.btn_0_'+dicserie).hide();
                $('.btn_1_'+dicserie).show().click();
            }, 1000);
        }
    }
}
function notificaciondisabled(){
   toastr["error"]("Use el buscador con los digitos faltantes de la serie para desbloquear"); 
}
function notificacionrechazo(serv,serve,tipo){
    var v_status=$('.ser_'+tipo+'_'+serv+'_'+serve).data('status');
    if(v_status>0){
        toastr.error('No se puede rechazar, el servicio ya fue iniciado');
    }else{

        var htmlnr='';
        htmlnr+='<label>Tipo</label>';
        htmlnr+='<select id="comtipo" class="form-control-bmz"><option value="0">Otro</option><option value="1">backup</option><option value="2">no se acudio al sitio</option><option value="3">sin acceso</option><option value="4">oficina cerrada</option><option value="5">intercambio de servicio</option></select>';
        htmlnr+='<label>Motivo</label>';
        htmlnr+='<textarea class="form-control-bmz" id="notificarerror"></textarea>';

        htmlnr+='<label>Se necesita permisos de administrador</label><br>';
        htmlnr+='<input type="text" placeholder="Contraseña" id="contrasena3" name="contrasena3" class="name form-control-bmz" required style="width: 99%;"/>';
        $.confirm({
            boxWidth: '90%',
            useBootstrap: false,
            icon: 'fa fa-warning',
            title: 'Motivo Rechazo',
            content: htmlnr,
            type: 'red',
            typeAnimated: true,
            buttons: 
            {
                'Confirmar rechazo': function (){
                    var pass=$("input[name=contrasena3]").val();
                    var info=$('#notificarerror').val();
                    var comtipo=$('#comtipo  option:selected').val();
                    if(pass!=''){
                        if(info!=''){
                            $.ajax({
                                type:'POST',
                                url: base_url+'index.php/AsignacionesTecnico/notificacionrechazo',
                                data: {
                                    serv:serv,
                                    serve:serve,
                                    tipo:tipo,
                                    info:info,
                                    comtipo:comtipo,
                                    pass:pass
                                },
                                success:function(data){
                                    var array = $.parseJSON(data);
                                    if(array.v_pass==0){
                                        toastr.success("Rechazo procesado");    
                                    }else if(array.v_pass==1){
                                        if(array.permiso==1){
                                            toastr.success("Rechazo procesado"); 
                                        }else{
                                            notificacionrechazo(serv,serve,tipo);
                                            //notificacion(2);
                                            toastr.error("No tiene permisos"); 
                                        }
                                    }
                                }
                            });
                        }else{
                            toastr.error('Favor de agregar Motivo del rechazo');
                            notificacionrechazo(serv,serve,tipo);
                        }
                    }else{
                        toastr.error('Ingrese una contraseña');
                        notificacionrechazo(serv,serve,tipo);
                    }
                    
                },
                Salir: function (){
                }
            }
        });
        setTimeout(function(){
                new MaskedPassword(document.getElementById("contrasena3"), '\u25CF');
        },500);
    }
}
function notificacionrechazogeneral(asignacion,tipo){
    var num_ser_s12=0;
    $(".click_ser_"+tipo+"_"+asignacion).each(function() {
        var vstatus = parseFloat($(this).data('status'));
        console.log('status: '+vstatus);
        if(vstatus>0){
            num_ser_s12++;
        }
    });
    if(num_ser_s12>0){
        toastr.error('No todos los equipos se podran rechazar, el servicio ya fue iniciado de unos de los equipos');
    }
    var htmlnr='';
    htmlnr+='<label>Tipo</label>';
    htmlnr+='<select id="comtipo" class="form-control-bmz"><option value="0">Otro</option><option value="1">backup</option><option value="2">no se acudio al sitio</option><option value="3">sin acceso</option><option value="4">oficina cerrada</option><option value="5">intercambio de servicio</option></select>';
    htmlnr+='<label>Motivo</label>';
    htmlnr+='<textarea class="form-control-bmz" id="notificarerror"></textarea>';

    htmlnr+='<label>Se necesita permisos de administrador</label><br>';
    htmlnr+='<input type="text" placeholder="Contraseña" id="contrasena3" name="contrasena3" class="name form-control-bmz" required style="width: 99%;"/>';
    $.confirm({
        boxWidth: '90%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Motivo Rechazo',
        content: htmlnr,
        type: 'red',
        typeAnimated: true,
        buttons: 
        {
            'Confirmar rechazo': function (){
                var info=$('#notificarerror').val();
                var comtipo=$('#comtipo  option:selected').val();
                var pass=$("input[name=contrasena3]").val();
                if(pass!=''){
                    if(info!=''){

                        $.ajax({
                            type:'POST',
                            url: base_url+'index.php/AsignacionesTecnico/notificacionrechazogeneral',
                            data: {
                                asignacion:asignacion,
                                tipo:tipo,
                                info:info,
                                comtipo:comtipo,
                                pass:pass
                            },
                            success:function(data){
                                var array = $.parseJSON(data);
                                if(array.v_pass==0){
                                    toastr.success("Rechazo procesado");    
                                }else if(array.v_pass==1){
                                    if(array.permiso==1){
                                        toastr.success("Rechazo procesado"); 
                                    }else{
                                        //notificacion(2);
                                        notificacionrechazogeneral(asignacion,tipo);
                                        toastr.error("No tiene permisos"); 
                                    }
                                }
                                
                            }
                        });
                    }else{
                        toastr.error('Favor de agregar Motivo del rechazo');
                        notificacionrechazogeneral(asignacion,tipo);
                    }
                }else{
                    toastr.error('Ingrese una contraseña');
                    notificacionrechazogeneral(asignacion,tipo);
                }
                
            },
            Salir: function (){
            }
        }
    });
    setTimeout(function(){
        new MaskedPassword(document.getElementById("contrasena3"), '\u25CF');
    },500);
}
function comment_info(row){
    var com=$('.comment_info_'+row).data('comen');
    $.alert({boxWidth: '90%',useBootstrap: false,title: 'Comentario',theme: 'bootstrap',content: com});  
}
/*
function verificar_obtenernoti(){
    var count = $('.viewstatustec').data('count');
    if(count>0){
        if(statusrow_retrasos==0){
            viewstatustec();
            statusrow_retrasos+=1; 
            console.log(statusrow_retrasos);
        }
        
    }
}*/
function obtenernoti(){
    statusrow_retrasos=0;
    $.ajax({
        type:'POST',
        url: base_url+'index.php/AsignacionesTecnico/obtenernotificacion',
        data: {
            tec:tec_v,
            fechaselect:$('#calendarinput').val()
        },
        success:function(data){
             $('.btn-notificacionstado').html(data);
        },
        error: function(response){
               
        }
    });
}
function div_view_pre(view){
    var tec = tec_v;
    var fecha=$('#calendarinput').val()

    var urlfac=base_url+"index.php/AsignacionesTecnico/prefacturas/"+tec+"/"+fecha+'/'+view;
    var htmliframe="<iframe src='"+urlfac+"' title='description' class='ifrafac'>";
    if(view==0){
        $('.view_info_pre').html(htmliframe);
    }
    if(view==1){
        $('.view_info_pre5').html(htmliframe);
    }
}
function servicio_siguiente(){
    $.ajax({
        type:'POST',
        url: base_url+'index.php/AsignacionesTecnico/obtener_servicio_siguiente',
        data: {
            tec: tec_v,
            fecha:$('#calendarinput').val()
        },
        async: false,
        statusCode:{
            404: function(data){
                toastr["error"]("Error 404");
            },
            500: function(){
                toastr["error"]("Error 500");
            }
        },
        success:function(data){
            console.log(data);
            var tipag=0;
            var array = $.parseJSON(data);
            $('.asv_or').html('');
            console.log(array);
            $.each(array, function(index, item) {
                var cadena =item.ser;
                var horaser =item.hora;
                const numeros = cadena.split(',');
                tipag=item.tipag;
                for (let i = 0; i < numeros.length; i++) {
                    // Eliminar espacios en blanco adicionales
                        const numero = numeros[i].trim();
                    // Aquí puedes realizar la operación que necesites con cada número
                    console.log(numero); // Por ejemplo, imprimir cada número
                    $('.asv_or').append('<li data-ser="'+numero+'" data-clihrs="'+item.clihrs+'" data-idcli="'+item.cli+'">'+numero+' '+horaser+'</li>');
                }
            });
            if(tipag==1){
                not_new_ser();
            }
        }
    });
}
function verificardesfaces(){
    $.ajax({
        type:'POST',
        url: base_url+'index.php/AsignacionesTecnico/verificar_si_desfase_y_ultimo_ser',
        data: {
            tec: tec_v,
            fecha:$('#calendarinput').val()
        },
        async: false,
        statusCode:{
            404: function(data){
                toastr["error"]("Error 404");
            },
            500: function(){
                toastr["error"]("Error 500");
            }
        },
        success:function(data){
            
            var array = $.parseJSON(data);
            console.log(array);
            if(array.ser_desfaces.length>0){
                var html='Se detectaron Desfaces en los siguientes servicios. Por favor reorganiza tu itinerario para cumplir en tiempo';
                    html+='<table class="table bordered table_confirm">';
                    $.each(array.ser_desfaces, function(index, item) {
                       html+='<tr><td>'+item.cli+'</td><td>'+item.ser+'</td><td>'+item.hrs+'</td></tr>'; 
                    });
                    html+='</table>';
                $.confirm({
                    title: 'Notificación de desfase',
                    content: html,
                    boxWidth: '30%',  // Ancho personalizado
                    useBootstrap: false,  // No usar estilos de Bootstrap
                    //theme: 'supervan',  // Un tema de jQuery Confirm, puedes usarlo o definir tu propio tema
                    buttons: {
                        confirmar: {
                            text: 'Confirmar',
                            btnClass: 'btn-warning',  // Clase personalizada para el botón
                            action: function () {

                            }
                        }
                    },
                    onContentReady: function () {
                        // Agregar clases personalizadas a los elementos de la alerta
                        this.$el.addClass('new-alert-person');
                    }
                });
            }
            if(array.noalcanza.length>0){
                var html='Debido a los desfases en el transcurso del día, no podrás alcanzar a llegar con tu cliente. Si no estás en tiempo, favor de rechazar el servicio';
                    html+='<table class="table bordered table_confirm">';
                    var ir_rechazo_tipo=0;
                    var ir_rechazo_idser=0;
                    $.each(array.noalcanza, function(index, item) {
                        ir_rechazo_tipo=item.tiposer;
                        ir_rechazo_idser=item.asignacionId;
                       html+='<tr><td>'+item.cli+'</td><td>'+item.ser+'</td><td>'+item.hrs+'</td></tr>'; 
                    });
                    html+='</table>';
                $.confirm({
                    title: 'Notificación de último servicio',
                    content: html,
                    boxWidth: '30%',  // Ancho personalizado
                    useBootstrap: false,  // No usar estilos de Bootstrap
                    //theme: 'supervan',  // Un tema de jQuery Confirm, puedes usarlo o definir tu propio tema
                    buttons: {
                        confirmar: {
                            text: 'Confirmar',
                            btnClass: 'btn-warning',  // Clase personalizada para el botón
                            action: function () {

                            }
                        },
                        'Ir a Rechazo': function () {
                            if(ir_rechazo_idser>0){
                                notificacionrechazogeneral(ir_rechazo_idser,ir_rechazo_tipo);
                            }
                        }
                    },
                    onContentReady: function () {
                        // Agregar clases personalizadas a los elementos de la alerta
                        this.$el.addClass('new-alert-person');
                    }
                });
            } 
        }
    });
}
function searchserie(){
    console.log('busqueda');
    $('.search_select').removeClass('ser_localizada');
    var serie_loc='';
    var serie_search=$('#searchserie').val();
        serie_search=serie_search.replace(/ /g, "");
    const contenedor = document.querySelector('.addseviciosclientes');

    if (contenedor) {
        // Buscar todos los elementos dentro del contenedor y extraer sus clases
        const clases = Array.from(contenedor.querySelectorAll('[class*="'+serie_search+'"]'))
            .flatMap(elemento => 
                elemento.className.split(' ').filter(clase => clase.endsWith(serie_search))
            );

        // Imprimir las clases encontradas
        console.log(clases);
        if(clases.length>0){
            clases.forEach(function(item) {
                console.log(item);
                serie_loc=item;
                $('.search_select.'+item).addClass('ser_localizada');
            });
        }else{
            console.log('no se encontro relaccion.');
            toastr.error('No se encontro relaccion');
        }
    } else {
        console.log('El contenedor con la clase "addseviciosclientes" no se encontró.');
    }
    if(serie_loc!=''){
        $('.search_select.'+serie_loc).click();
        $('.iconsearch').click();
        setTimeout(function(){ 
            table.search(serie_search).draw();
        }, 500); //1000 1seg 500 0.5seg 
    }
}