<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<title>Accessible markers - Leaflet</title>
	
	<link rel="shortcut icon" type="image/x-icon" href="docs/images/favicon.ico" />

    <!--<link rel="stylesheet" href="https://unpkg.com/leaflet@1.9.4/dist/leaflet.css" integrity="sha256-p4NxAoJBhIIN+hmNHrzRCf9tD/miZyoHS5obTRR9BMY=" crossorigin=""/>
    <script src="https://unpkg.com/leaflet@1.9.4/dist/leaflet.js" integrity="sha256-20nQCchB9co0qIjJZRGuk2/Z9VM+kNiyxNV1lvTlZBo=" crossorigin=""></script>-->

    <link rel="stylesheet" href="<?php echo base_url();?>public/plugins/leaflet/leaflet.css"  crossorigin=""/>
    <script src="<?php echo base_url();?>public/plugins/leaflet/leaflet.js"  crossorigin=""></script>

	<style>
		html, body {
			height: 100%;
			margin: 0;
		}
		.leaflet-container {
			height: 100vh;
			width: 100%;
		}
	</style>

	
</head>
<body>

<div id='map'></div>

<script>

	var map = L.map('map').setView([<?php echo $latitud; ?>,<?php echo $longitud; ?> ], 13);

	var tiles = L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
		maxZoom: 40,
		attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
	}).addTo(map);

	var marker = L.marker([<?php echo $latitud; ?>, <?php echo $longitud; ?>], {alt: 'Kyiv'}).addTo(map)
		.bindPopup('registro');

</script>



</body>
</html>