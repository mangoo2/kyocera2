<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

require 'vendorgoo/autoload.php';
use Google\Auth\Credentials\ServiceAccountCredentials;
use Google\Auth\HttpHandler\HttpHandlerFactory;

class Modelonotify extends CI_Model 
{
    public function __construct() 
    {
        parent::__construct();


    }
    function obtener_token_persona($id){
        $sql = "SELECT * FROM personal WHERE personalId='$id'";
        $query = $this->db->query($sql);
        $token='';
        foreach ($query->result() as $item) {
            $token=$item->token_user;
        }
        return $token;
    }
    function enviopush($idper,$titulo,$mensaje){
        $token_user=$this->obtener_token_persona($idper);

        $credential = new ServiceAccountCredentials(
            "https://www.googleapis.com/auth/firebase.messaging",
            json_decode(file_get_contents('kyocera-58f51.json'), true)
        );

        $token = $credential->fetchAuthToken(HttpHandlerFactory::build());

        $ch = curl_init("https://fcm.googleapis.com/v1/projects/kyocera-58f51/messages:send");

        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
            'Authorization: Bearer '.$token['access_token']
        ]);

        curl_setopt($ch, CURLOPT_POSTFIELDS, '{
            "message": {
              "token": "'.$token_user.'",
              "notification": {
                "title": "'.$titulo.'",
                "body": "'.$mensaje.'",
                "image": "https://altaproductividadapr.com/app-assets/images/favicon/favicon_kyocera.png"
              },
              "webpush": {
                "fcm_options": {
                  "link": "https://altaproductividadapr.com"
                }
              }
            }
          }');

          curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "post");

          $response = curl_exec($ch);

          curl_close($ch);

          //echo $response;
    }
    

}