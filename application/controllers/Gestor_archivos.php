<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Gestor_archivos extends CI_Controller 
{
    public function __construct() 
    {
        parent::__construct();
        $this->load->model('Configuraciones_model', 'model');
        $this->load->model('login_model');
        $this->load->helper('url');
        $this->load->library('menus');
        $this->load->model('ModeloCatalogos');

        date_default_timezone_set('America/Mexico_City');
        
        $this->fechahoyc = date('Y-m-d H:i:s');

        if($this->session->userdata('logeado')==true){
            $this->perfilid =$this->session->userdata('perfilid');
            $this->idpersonal = $this->session->userdata('idpersonal');
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,23);// 23 es el id del submenu
            if ($permiso==0) {
                //redirect('Login');
            }else{
                
            }
        }else{
            redirect('login');
        }
    }
    
    function index() {
        $this->ModeloCatalogos->Insert('bitacora_gestorarchivos',array('personal'=>$this->idpersonal));
        $this->load->view('gestor');
    }


}