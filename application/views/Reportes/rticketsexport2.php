<?php

    require_once('TCPDF4/examples/tcpdf_include.php');
    require_once('TCPDF4/tcpdf.php');
    $this->load->helper('url');

//=======================================================================================
class MYPDF extends TCPDF {
  //Page header
  public function Header() {
      //$img_header = 'header.jpg';
      //$this->Image($img_header, 0, 0, 0, 197, '', '', '', false, 100, '', false, false, 0);
      //$this->Image($img_header, 0, 0, 210, '', 'JPG', '', false, false, 300, '', false, false, 0, false, false, false);
      $imglogo = base_url().'public/img/ops.png';
      $html = '
          
          
          <table width="100%" border="0" >
            <tr>
              <td align="center"><img src="'.$imglogo.'" width="100px" ></td>
              <td></td>
              <td></td>
            </tr>
          </table>';
        $this->writeHTML($html, true, false, true, false, '');
  }
    // Page footer
  public function Footer() {
      $html = ' 
      <table width="100%" border="0">
        <tr>
          <td align="right" class="footerpage">Pagina '.$this->getAliasNumPage().' de '.$this->getAliasNbPages().'</td>
        </tr>
      </table>';
      $this->writeHTML($html, true, false, true, false, '');
  }
} 
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Cuestionario');
$pdf->SetTitle('Cuestionario');
$pdf->SetSubject('Cuestionario');
$pdf->SetKeywords('Cuestionario');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins('10', '30', '10');
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('dejavusans', '', 9);
// add a page
$pdf->AddPage('L', 'A4');
$html='';
$col1='4%';
$col2='10%';
$col3='10%';
$col4='59%';
$col5='10%';
$col6='6%';
$col7='33%';

$html.='<table border="1" cellpadding="2">
  <thead>
    <tr>
      <th width="'.$col1.'">No</th>
      <th width="'.$col2.'">Personal</th>
      <th width="'.$col3.'">Solicitud</th>
      <th width="'.$col4.'">Descripcion</th>
      <th width="'.$col5.'">Fecha</th>
      <th width="'.$col6.'">Estatus</th>
    </tr>
  </thead>
  <tbody>';
    foreach ($resultti->result() as $itemti) { 
      $resulttidll=$this->ModeloCatalogos->getselectwheren('ticket_detalle',array('ticketid'=>$itemti->id,'activo'=>1));
        
$html.='<tr>
        <td width="'.$col1.'">'.$itemti->id.'</td>
        <td width="'.$col2.'">'.$itemti->nombre.' '.$itemti->apellido_paterno.' '.$itemti->apellido_materno.'</td>
        <td width="'.$col3.'">'.$itemti->tcontenido.'</td>
        <td width="'.$col4.'">'.str_replace('data:image/png;base64,', '@', $itemti->contenido).'</td>
        <td width="'.$col5.'">'.$itemti->reg.'</td>
        <td width="'.$col6.'">'; 
            if($itemti->status==0){
              $html.='Solicitud';
            }
            if($itemti->status==1){
              $html.='Revision';
            }
            if($itemti->status==2){
              $html.='Finalizado';
            }
          $html.='</td>
        </tr>
        <tr>
        <td colspan="6">
          <table border="1">';
            foreach ($resulttidll->result() as $itemtidd) {
              $html.='<tr>
                <td width="80%">'.str_replace('data:image/png;base64,', '@', $itemtidd->contenido).'</td>
                <td width="10%">'.$itemtidd->usuario.'</td>
                <td width="10%">'.$itemtidd->reg.'</td>
              </tr>';
            }
  $html.='</table>
        </td>
      </tr>';
     }
  $html.='</tbody>
</table>';

$pdf->writeHTML($html, true, false, true, false, '');

$pdf->Output('Captura.pdf', 'I');

//$pdf->Output('files/'.$GLOBALS["carpeta"].'/facturas/'.$GLOBALS["rrfc"].'_'.$GLOBALS["Folio"].'.pdf', 'F');

?>