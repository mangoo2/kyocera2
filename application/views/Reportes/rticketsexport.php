<?php 
	
	header("Pragma: public");
	header("Expires: 0");
	$filename = "Tickets.xls";
	header("Content-type: application/x-msdownload");
	header("Content-Disposition: attachment; filename=$filename");
	header("Pragma: no-cache");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	
?>
<table border="1">
	<thead>
		<tr>
			<th>No</th>
			<th>Personal</th>
			<th>Solicitud</th>
			<th>Descripcion</th>
			<th>Fecha</th>
			<th>Estatus</th>
			<th>Respuestas</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($resultti->result() as $itemti) { 
			$resulttidll=$this->ModeloCatalogos->getselectwheren('ticket_detalle',array('ticketid'=>$itemti->id,'activo'=>1));
		?>		
			<tr>
				<td><?php echo $itemti->id;?></td>
				<td><?php echo $itemti->nombre.' '.$itemti->apellido_paterno.' '.$itemti->apellido_materno ;?></td>
				<td><?php echo $itemti->tcontenido;?></td>
				<td><?php echo $itemti->contenido;?></td>
				<td><?php echo $itemti->reg;?></td>
				<td><?php 
						if($itemti->status==0){
							echo 'Solicitud';
						}
						if($itemti->status==1){
							echo 'Revision';
						}
						if($itemti->status==2){
							echo 'Finalizado';
						}
					?></td>
				<td>
					<table border="1">
						<?php foreach ($resulttidll->result() as $itemtidd) { ?>						
							<tr>
								<td><?php echo $itemtidd->contenido;?></td>
								<td><?php echo $itemtidd->usuario;?></td>
								<td><?php echo $itemtidd->reg;?></td>
							</tr>
						<?php } ?>
					</table>
				</td>
			</tr>
		<?php } ?>
	</tbody>
</table>