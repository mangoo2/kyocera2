<link href="<?php echo base_url(); ?>public/plugins/select2/select2.min.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/select2/select2.full.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/loading/jquery.loading.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/plugins/loading/demo.css">
<script type="text/javascript" src="<?php echo base_url(); ?>public/js/afacturas.js?v=012<?php echo date('YmdGi');?>" ></script>
<?php if($facturaId>0){ ?>
		<script type="text/javascript">
			$(document).ready(function($) {
				obtenerdatosfactura(<?php echo $facturaId;?>);
				$('.agregarconcepto').hide();
				$('.divrequerimentos').hide();
				$('.divtipoComprobante').hide();
			});
		</script>
<?php } ?>
<script type="text/javascript">
	$(document).ready(function($) {
		<?php if(isset($_GET['uuid'])){ 
			$verifir=$this->ModeloCatalogos->getselectwheren('f_facturas',array('f_r_uuid'=>$_GET['uuid'],'Estado'=>1));
			if($verifir->num_rows()>0){ ?>
				setTimeout(function(){ 
					<?php foreach ($verifir->result() as $item) { ?>
							alertaderelacion(<?php echo $item->FacturasId;?>,<?php echo $item->Folio;?>);
					<?php } ?>
					
				}, 1000);
			<?php } ?>
			setTimeout(function(){ 
				$( "#facturarelacionada" ).prop( "checked", true ); 
				$('.divfacturarelacionada').show('show');
				$('#uuid_r').val('<?php echo $_GET['uuid']; ?>');
				obtenerdatosfactura(<?php echo $_GET['idfac']; ?>);
				addpermisoseditcon();
				$('#TipoRelacion').val('<?php echo $_GET['tr']; ?>').change();
				<?php if($_GET['tr']=='01'){ ?>
					$('#TipoComprobante').val('E')
				<?php } ?>
				<?php if($_GET['tr']=='07' and $_GET['tc']=='E'){ ?>
					$('#TipoComprobante').val('E');
					setTimeout(function(){ 
						$('#MetodoPago').val('AplicacionAnticipos').select2();
						deleteconcepto(0);
						$('#FormaPago').val('PUE');
						$('#infoanticipo').click();
						$('#sdescripcion').val('Aplicación de anticipo');
						
					}, 2000);
				<?php } ?>
				<?php if($_GET['tr']=='07' and $_GET['tc']=='I'){ ?>
					$('#TipoComprobante').val('I');
					setTimeout(function(){ 
						deleteconcepto(0);
						$('#sdescripcion').val('');
					}, 2000);
				<?php } ?>
			}, 1000);
			
		<?php } ?>
		<?php if (isset($_GET['g_tipo'])) { 
				?>
				obtenerrfc(<?php echo $_GET['g_cli'];?>,0)
				<?php
				if($_GET['g_tipo']==0){
					?>
					$('input:radio[name=tiporelacion][value=1]').prop('checked',true).click(); 
					setTimeout(function(){ 
						importarproductos(<?php echo $_GET['g_idvcp'];?>,<?php echo $_GET['g_tipo'];?>,1);
					}, 1000);
					<?php
				}
				if($_GET['g_tipo']==1){
					?>
					$('input:radio[name=tiporelacion][value=2]').prop('checked',true).click();
					setTimeout(function(){ 
					importarproductos(<?php echo $_GET['g_idvcp'];?>,<?php echo $_GET['g_tipo'];?>,2);
					}, 1000);
					<?php
				}
				if($_GET['g_tipo']==2){
					?>
					$('input:radio[name=tiporelacion][value=3]').prop('checked',true).click();
					setTimeout(function(){ 
					importarproductos(<?php echo $_GET['g_idvcp'];?>,<?php echo $_GET['g_tipo'];?>,3);
					}, 1000);
					<?php
				}
		 } ?>
		<?php if($fac_pue_pend->num_rows()>0 and $idpersonal!=63){ ?>
			fac_pen_pago_pue(<?php echo $fac_pue_pend->num_rows();?>);
		<?php } ?>
	});
	function alertaderelacion(id,folio){
		$.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: 'El folio fiscal ya se encuentra relacionada con la factura U'+folio+', ¿Desea continuar?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                
                
            },
            cancelar: function () {
                window.location.href = "<?php echo base_url();?>index.php/Facturaslis";

            }
        }
    });
	}
</script>