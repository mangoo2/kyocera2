<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<title>Prefacturas servicios</title>
		<meta content="width=device-width,initial-scale=1.0" name=viewport>
		<meta name="theme-color" content="#e31a2f" />
		<meta name="description" content="Sistema Kyocera">
		<script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/jquery-3.3.1.min.js"></script>
		<link href="<?php echo base_url(); ?>public/css/fontawesome/css/all.css" type="text/css" rel="stylesheet">
		<!--<script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/fullcalendar-6.1.8/dist/index.global.js"></script>-->
		<!--<link href='<?php echo base_url()?>app-assets/vendors/fullcalendar-3.2.0/fullcalendar.min.css' rel='stylesheet' />
		<link href='<?php echo base_url()?>app-assets/vendors/fullcalendar-3.2.0/fullcalendar.print.min.css' rel='stylesheet' media='print' />
		<script src='<?php echo base_url()?>app-assets/vendors/fullcalendar-3.2.0/lib/moment.min.js'></script>
		<script src='<?php echo base_url()?>app-assets/vendors/fullcalendar-3.2.0/lib/jquery.min.js'></script>
		<script src='<?php echo base_url()?>app-assets/vendors/fullcalendar-3.2.0/fullcalendar.min.js'></script>-->
		<link href="<?php echo base_url()?>public/plugins/note/bootstrap.min.css" rel="stylesheet">
		<script src="<?php echo base_url()?>public/plugins/note/bootstrap.min.js"></script>
		<!--<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/fullcalendar-scheduler@1.10.4/dist/scheduler.min.js"></script>-->
		<!--<script type="text/javascript" src="<?php echo base_url()?>public/plugins/fullcalendar/scheduler.min.js"></script>-->
		<!--<script type="text/javascript" src="https://static.cloudflareinsights.com/beacon.min.js"></script>-->
		<!--<script type="text/javascript" src="<?php echo base_url()?>public/plugins/fullcalendar/beacon.min.js"></script>-->
		<input type="hidden" id="base_url" value="<?php echo base_url();?>">
		<style type="text/css">
			.fc-scroller.fc-time-grid-container{height: inherit !important;}
			.fc-listDay-view .fc-scroller{min-height: 371px !important;}
			.fc-list-table td {padding: 8px 7px;}
			.fc table {font-size: 11px;}
			.btn.btn-primary{
				margin: 5px;
			}
			<?php if($views==1){ ?>
				.class_table_pre_pro tbody,.class_table_pre_pro thead{
					font-size: 11px;
				}
			<?php } ?>
		</style>
		<script>
			var base_url = $('#base_url').val();
			function detallep(id){
			    window.open(base_url+"PolizasCreadas/view/"+id, "Prefactura", "width=780, height=612");
			}
			function detalle(id,tipo){
				//parent.itinerario(5)
			    if (tipo==0) {
			        window.open(base_url+"Prefactura/view/"+id, "Prefactura", "width=780, height=612");
			    }else{
			        window.open(base_url+"Prefactura/viewc/"+id, "Prefactura", "width=780, height=612");
			    }
			}
			function agregarpago(idvp,tipo,monto){
				parent.agregarpago_p(idvp,tipo,monto);//parent es para ejecutar una funcion fuera del iframe digamos a la ventana principal
			}
		</script>
	</head>
	<body>
		<div class="row">
			<div class="col-md-12">
				<table class="table table-striped class_table_pre_pro">
					<thead>
						<tr>
							<?php 
								$tr_html='';
								if($views==0){
									$tr_html='<th>Cliente</th><th style="min-width: 111px;"></th>';
								}
								if($views==1){
									$tr_html='<th>Cant.</th><th>Modelo</th><th>No. de Parte</th><th>Serie/Folio</th><th>Bodega</th>';
								}
								echo $tr_html; 
							?>
							
						</tr>
					</thead>
					<tbody>
						<?php 
							//v_ser_tipo 1 contrato 2 poliza 3 evento 4 venta
							$html='';
							foreach ($result1->result() as $item) {
								$v_ser_tipo=1;
								$v_ser_id=$item->asignacionId;

								//===============================================================================================
									$resultv=$this->ModeloCatalogos->getselectwheren('ventas',array('v_ser_tipo'=>$v_ser_tipo,'v_ser_id'=>$v_ser_id,'activo'=>1));
									foreach ($resultv->result() as $itemv) {
										if($views==0){
										$btn='<a class="btn btn-primary"  onclick="detalle('.$itemv->id.',0)"><i class="fas fa-file-invoice"></i></a>';
										if($itemv->total_general>0){
											$btn.='<a class="btn btn-primary pg_1"  onclick="agregarpago('.$itemv->id.',0,'.$itemv->total_general.')"><i class="fas fa-dollar-sign"></i></a>';
										}
										$html.='<tr>';
											$html.='<td><!--1-->'.$item->empresa.'</td>';
											$html.='<td>'.$btn.'</td>';
										$html.='</tr>';
										}
										if($views==1){
											$html.=$this->ModeloCatalogos->obtener_info_productos($item->empresa,$itemv->combinada,$itemv->id,0);
											/*
											$equipo=$itemv->id;
											$consumibles=$itemv->id;
											$refacciones=$itemv->id;
											if($itemv->combinada==1){
												$resultvc=$this->ModeloCatalogos->getselectwheren('ventacombinada',array('equipo'=>$itemv->id));
												foreach ($resultvc->result() as $itemvc) {
													$equipo=$itemvc->equipo;
													$consumibles=$itemvc->consumibles;
													$refacciones=$itemvc->refacciones;
												}
											}
											$resultadoequipos = $this->ModeloCatalogos->db6_getselectwheren('ventas_has_detallesEquipos',array('idVenta'=>$equipo));
											$resultadoconsumibles = $this->ModeloCatalogos->consumiblesventa($equipo);
											$consumiblesventadetalles = $this->ModeloCatalogos->consumiblesventadetalles($consumibles);
											$ventadetallesrefacion = $this->ModeloCatalogos->ventas_has_detallesRefacciones($refacciones);

											foreach ($resultadoequipos->result() as $item_p_e) {
												$html.='<tr>';
													$html.='<td><!--1-->'.$item->empresa.'<td>';
													$html.='<td>'.$item_p_e->cantidad.'<td>';
													$html.='<td>'.$item_p_e->modelo.'<td>';
													$resuleserie=$this->ModeloCatalogos->getequiposerie($item_p_e->id);
													$series_selecte='';
													foreach ($resuleserie->result() as $itemse) {
														$series_selecte.=$itemse->serie.', ';
													}
													$html.='<td>'.$series_selecte.'<td>';
												$html.='</tr>';
											}
											foreach ($resultadoconsumibles->result() as $item_p_ec) {
												$html.='<tr>';
													$html.='<td><!--1-->'.$item->empresa.'<td>';
													$html.='<td>'.$item_p_ec->cantidad.'<td>';
													$html.='<td>'.$item_p_ec->modelo.'<td>';
													$html.='<td><td>';
												$html.='</tr>';
											}
											foreach ($consumiblesventadetalles->result() as $item_p_c) {
												$html.='<tr>';
													$html.='<td><!--1-->'.$item->empresa.'<td>';
													$html.='<td>'.$item_p_c->cantidad.'<td>';
													$html.='<td>'.$item_p_c->modelo.'<td>';
													$html.='<td>'.$item_p_c->foliotext.'<td>';
												$html.='</tr>';
											}
											foreach ($ventadetallesrefacion->result() as $item_p_r) {
												$html.='<tr>';
													$html.='<td><!--1-->'.$item->empresa.'<td>';
													$html.='<td>'.$item_p_r->cantidad.'<td>';
													$html.='<td>'.$item_p_r->modelo.'<td>';
													$resuleserie=$this->ModeloCatalogos->getrefaccionserie($item_p_r->id);
													$series_selecte='';
													foreach ($resuleserie->result() as $itemse) { 
														$series_selecte.=$itemse->serie.', ';
													}
													$html.='<td>'.$series_selecte.'<td>';
												$html.='</tr>';
											}
											*/
											
										}
									}
								//==============================================================================
									$sql2r="SELECT cf.*,c.modelo,c.parte,p.nombre,r.reg,cli.empresa,vdc.idVentas,ven.total_general,ven.combinada
										  FROM contrato_folio AS cf 
										  INNER JOIN consumibles AS c ON c.id = cf.idconsumible 
										  INNER JOIN rentas AS r ON r.id = cf.idrenta 
										  INNER JOIN personal AS p ON p.personalId = r.id_personal 
										  INNER JOIN clientes AS cli ON cli.id = r.idCliente 
										  INNER JOIN ventas_has_detallesConsumibles as vdc on vdc.foliotext=cf.foliotext
										  INNER JOIN ventas as ven on ven.id=vdc.idVentas
										  where cf.serviciocId='$v_ser_id' and cf.status<=1 and cf.serviciocId_tipo=0
										  group by vdc.idVentas";

										  $query2r = $this->db->query($sql2r);
										  foreach ($query2r->result() as $itemcv) {
										  	if($views==0){
										  		$btn='<a class="btn btn-primary"  onclick="detalle('.$itemcv->idVentas.',0)"><i class="fas fa-file-invoice"></i></a>';
										  		if($itemcv->total_general>0){
										  			if($itemcv->combinada==0){
														$btn.=' <a class="btn btn-primary pg_2"  onclick="agregarpago('.$itemcv->idVentas.',0,'.$itemcv->total_general.')"><i class="fas fa-dollar-sign"></i></a>';
													}
												}
												$html.='<tr>';
													$html.='<td><!--2 '.$v_ser_id.' row-->'.$item->empresa.'</td>';
													$html.='<td>'.$btn.'</td>';
												$html.='</tr>';
										  	}
										  	if($views==1){
										  		$html.=$this->ModeloCatalogos->obtener_info_productos($item->empresa,0,$itemcv->idVentas,0);
										  	}
										  	
										  }


								//==============================================================================
							}
							foreach ($result2->result() as $item) { //poliza
								$v_ser_tipo=2;
								$v_ser_id=$item->asignacionId;
								$tipovpp=4;
								$id_vpp=$item->polizaid;
								if($item->combinada==0){
									$monto = $this->ModeloCatalogos->montopoliza($item->polizaid);
									//log_message('error','monto1 '.$monto);
								}else{
									$rowventa=$this->ModeloCatalogos->db10_getselectwheren('ventacombinada',array('poliza'=>$item->polizaid));
									$idcombinada=0;
									foreach ($rowventa->result() as $itemvp) {
										$idcombinada=$itemvp->combinadaId;
										//log_message('error','idcombinada '.$idcombinada);
									}
									if($idcombinada>0){
										//log_message('error','idcombinada '.$idcombinada);
										//$monto=$this->ModeloCatalogos->montocombinada2($idcombinada);
										//$monto=round($monto*1.16,2);
										$res_m=$this->ModeloCatalogos->getselectwheren('ventacombinada',array('combinadaId'=>$idcombinada));
										foreach ($res_m->result() as $itemm) {
											$monto=$itemm->total_general;
										}
										$tipovpp=1;
										$id_vpp=$idcombinada;
									}
									//log_message('error','monto2 '.$monto);
								}
								if($views==0){
									
									$html.='<tr>';
										$html.='<td><!--3-->'.$item->empresa.'</td>';
											$btn='<a class="btn btn-primary"  onclick="detallep('.$item->polizaid.')"><i class="fas fa-file-invoice"></i></a>';
											if($monto>0){
												$btn.=' <a class="btn btn-primary pg_3"  onclick="agregarpago('.$id_vpp.','.$tipovpp.','.$monto.')"><i class="fas fa-dollar-sign"></i></a>';
											}
										$html.='<td>'.$btn.'</td>';
									$html.='</tr>';
								}
							  	if($views==1){
							  		$html.=$this->ModeloCatalogos->obtener_info_productos($item->empresa,$item->combinada,0,$item->polizaid);
							  	}

								//===============================================================================================
									$resultv=$this->ModeloCatalogos->getselectwheren('ventas',array('v_ser_tipo'=>$v_ser_tipo,'v_ser_id'=>$v_ser_id,'activo'=>1));
									foreach ($resultv->result() as $itemv) {
										if($views==0){

											$btn='<a class="btn btn-primary"  onclick="detalle('.$itemv->id.',0)"><i class="fas fa-file-invoice"></i></a>';
											if($itemv->total_general>0){
												$btn.=' <a class="btn btn-primary pg_4"  onclick="agregarpago('.$itemv->id.',0,'.$itemv->total_general.')"><i class="fas fa-dollar-sign"></i></a>';
											}
											$html.='<tr>';
												$html.='<td><!--4-->'.$item->empresa.'</td>';
												$html.='<td>'.$btn.'</td>';
											$html.='</tr>';
										}
									  	if($views==1){
									  		$html.=$this->ModeloCatalogos->obtener_info_productos($item->empresa,$itemv->combinada,$itemv->id,0);
									  	}
									}
								//==============================================================================
							}
							foreach ($result3->result() as $item) {
								$v_ser_tipo=3;
								$v_ser_id=$item->asignacionId;

								//===============================================================================================
									$resultv=$this->ModeloCatalogos->getselectwheren('ventas',array('v_ser_tipo'=>$v_ser_tipo,'v_ser_id'=>$v_ser_id,'activo'=>1));
									foreach ($resultv->result() as $itemv) {
										if($views==0){
											$btn='<a class="btn btn-primary"  onclick="detalle('.$itemv->id.',0)"><i class="fas fa-file-invoice"></i></a>';
											if($itemv->total_general>0){
												$btn.=' <a class="btn btn-primary pg_5"  onclick="agregarpago('.$itemv->id.',0,'.$itemv->total_general.')"><i class="fas fa-dollar-sign"></i></a>';
											}
											$html.='<tr>';
												$html.='<td><!--5-->'.$item->empresa.'</td>';
												$html.='<td>'.$btn.'</td>';
											$html.='</tr>';
										}
										if($views==1){
											$html.=$this->ModeloCatalogos->obtener_info_productos($item->empresa,$itemv->combinada,$itemv->id,0);
										}
									}
								//==============================================================================

								
							}
							foreach ($result4->result() as $item) {
								$v_ser_tipo=4;
								$v_ser_id=$item->asignacionId;
								if($views==0){
									$btn='<a class="btn btn-primary"  onclick="detalle('.$item->ventaId.','.$item->tipoventa.')"><i class="fas fa-file-invoice"></i></a>';
									$monto=0;
									if($item->tipoventa==0){
										$monto=$this->ModeloCatalogos->montoventas2($item->ventaId);
										$res_m=$this->ModeloCatalogos->getselectwheren('ventas',array('id'=>$item->ventaId));
										foreach ($res_m->result() as $itemm) {
											$monto=$itemm->total_general;
										}
									}
									if($item->tipoventa==1){
										$monto=$this->ModeloCatalogos->montocombinada2($item->ventaId);
										$res_m=$this->ModeloCatalogos->getselectwheren('ventacombinada',array('combinadaId'=>$item->ventaId));
										foreach ($res_m->result() as $itemm) {
											$monto=$itemm->total_general;
										}
									}
									if($monto>0){
										$btn.=' <a class="btn btn-primary pg_6"  onclick="agregarpago('.$item->ventaId.','.$item->tipoventa.','.$monto.')"><i class="fas fa-dollar-sign"></i></a>';
									}
									$html.='<tr>';
										$html.='<td><!--6-->'.$item->empresa.'</td>';
										$html.='<td>'.$btn.'</td>';
									$html.='</tr>';
								}
								if($views==1){
									$html.=$this->ModeloCatalogos->obtener_info_productos($item->empresa,$item->tipoventa,$item->ventaId,0);
								}

								//===============================================================================================
									$resultv=$this->ModeloCatalogos->getselectwheren('ventas',array('v_ser_tipo'=>$v_ser_tipo,'v_ser_id'=>$v_ser_id,'activo'=>1));
									foreach ($resultv->result() as $itemv) {
										if($views==0){
											$btn='<a class="btn btn-primary"  onclick="detalle('.$itemv->id.',0)"><i class="fas fa-file-invoice"></i></a>';
											if($itemv->total_general>0){
												$btn.=' <a class="btn btn-primary pg_7"  onclick="agregarpago('.$itemv->id.',0,'.$itemv->total_general.')"><i class="fas fa-dollar-sign"></i></a>';
											}
											$html.='<tr>';
												$html.='<td><!--7-->'.$item->empresa.'</td>';
												$html.='<td>'.$btn.'</td>';
											$html.='</tr>';
										}
										if($views==1){
											$html.=$this->ModeloCatalogos->obtener_info_productos($item->empresa,$itemv->combinada,$itemv->id,0);
										}
									}
								//==============================================================================
							}

							echo $html;
							
						?>
					</tbody>
				</table>
			</div>
		</div>
	</body>
</html>