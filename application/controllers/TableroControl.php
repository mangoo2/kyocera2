<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class TableroControl extends CI_Controller {
    public function __construct()     {
        parent::__construct();

        $this->load->model('ModeloCatalogos');
        $this->load->model('Configuraciones_model');
        
        $this->load->model('ModeloAsignacion');
        $this->load->model('Modelotablero');
        $this->load->helper('url');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoylarga = date('Y-m-d G:i:s');
        $this->fechahoy = date('Y-m-d');
        $this->horaactual = date('G:i');
        $this->submenu=76;
        $this->perfilid=0;
        if($this->session->userdata('logeado')==true){
            $this->perfilid =$this->session->userdata('perfilid');
            $this->idpersonal = $this->session->userdata('idpersonal');
            /*
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,52);// 52 es el id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
            */
        }else{
            redirect('login'); 
        }
    }

    function index(){
        $data['MenusubId']=$this->submenu;
        //$this->load->view('header');
        //$this->load->view('main',$data);
        //$this->load->view('tablerocontrol/form');
        //$this->load->view('footerl');
        //$this->load->view('tablerocontrol/formjs');
        redirect('TableroControl/viewt'); 
    }
    function viewt($fecha=''){
        $data['MenusubId']=$this->submenu;
        if($fecha==''){
            $fechav=$this->fechahoy;
        }else{
            $fechav=$fecha;
        }
        if($fechav==$this->fechahoy){
            $data['ac_fecha_ac']=1;
        }else{
            $data['ac_fecha_ac']=0;
        }

        $data['fecha']= $fechav;
        $data['resultstec']=$this->Configuraciones_model->view_session_searchTecnico();
        $data['perfilid']=$this->perfilid;


        $this->load->view('header');
        $this->load->view('main');
        $this->load->view('tablerocontrol/form',$data);
        $this->load->view('footer');
        $this->load->view('tablerocontrol/formjs');
        $this->load->view('info_m_servisios');
    }
    function cambiarhora(){
        $params = $this->input->post();
        $tipo = $params['tipo'];
        $idser = $params['idser'];
        $nhora = $params['nhora'];
        $nfecha = $params['nfecha'];
        $nhoraf = $params['nhoraf'];

        $nhorac = $params['nhorac'];
        $nhoracf = $params['nhoracf'];


        $horao=$params['horao'];
        $fechao=$params['fechao'];
        $fecha_hora=' '.$fechao.' '.$horao.'<!--'.$this->idpersonal.'--><br> ';
        if($tipo==1){
            //=====================================================                
                $strq1 = "UPDATE asignacion_ser_contrato_a SET historial_hrs=concat(IFNULL(historial_hrs,''),'$fecha_hora') where asignacionId='$idser'";
                $this->db->query($strq1);
            //====================================================



            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_contrato_a_e',array('hora'=>$nhora,'horafin'=>$nhoraf,'fecha'=>$nfecha,'hora_comida'=>$nhorac,'horafin_comida'=>$nhoracf),array('asignacionId'=>$idser));
        }
        if($tipo==2){
            //=====================================================
                $strq1 = "UPDATE asignacion_ser_poliza_a SET historial_hrs=concat(IFNULL(historial_hrs,''),'$fecha_hora') where asignacionId='$idser'";
                $this->db->query($strq1);
            //====================================================
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_poliza_a_e',array('hora'=>$nhora,'horafin'=>$nhoraf,'fecha'=>$nfecha,'hora_comida'=>$nhorac,'horafin_comida'=>$nhoracf),array('asignacionId'=>$idser));
        }
        if($tipo==3){
            //=====================================================
                $strq1 = "UPDATE asignacion_ser_cliente_a SET historial_hrs=concat(IFNULL(historial_hrs,''),'$fecha_hora') where asignacionId='$idser'";
                $this->db->query($strq1);
            //====================================================
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_cliente_a',array('hora'=>$nhora,'horafin'=>$nhoraf,'fecha'=>$nfecha,'hora_comida'=>$nhorac,'horafin_comida'=>$nhoracf),array('asignacionId'=>$idser));


        }
        if($tipo==4){
            //=====================================================
                $strq1 = "UPDATE asignacion_ser_venta_a SET historial_hrs=concat(IFNULL(historial_hrs,''),'$fecha_hora') where asignacionId='$idser'";
                $this->db->query($strq1);
            //====================================================
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_venta_a',array('hora'=>$nhora,'horafin'=>$nhoraf,'fecha'=>$nfecha,'hora_comida'=>$nhorac,'horafin_comida'=>$nhoracf),array('asignacionId'=>$idser));
        }
        //$this->ModeloCatalogos->updateCatalogo('unidades_bitacora_itinerario',array('hora_inicio'=>$nhora,'hora_fin'=>$nhoraf),array('asignacion'=>$idser,'tipo_asignacion'=>$tipo));
        $asignacion_gen="|$tipo,$idser|";
        $strq = "UPDATE unidades_bitacora_itinerario SET hora_inicio='$nhora' , hora_fin='$nhoraf' where (asignacion='$idser' and tipo_asignacion='$tipo') or asignacion_gen like '%$asignacion_gen%' ";
        $query = $this->db->query($strq);
    }
    function cambiarserviciotec(){
        $params = $this->input->post();
        $tipo = $params['tipo'];
        $idser = $params['idser'];
        $tecd = $params['tecd'];
        $teco = $params['teco'];
        if($tipo==1){

            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_contrato_a_e',array('tecnico'=>$tecd),array('asignacionId'=>$idser,'tecnico'=>$teco,'status'=>0));
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_contrato_a_e',array('tecnico2'=>$tecd),array('asignacionId'=>$idser,'tecnico2'=>$teco,'status'=>0));
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_contrato_a_e',array('tecnico3'=>$tecd),array('asignacionId'=>$idser,'tecnico3'=>$teco,'status'=>0));
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_contrato_a_e',array('tecnico4'=>$tecd),array('asignacionId'=>$idser,'tecnico4'=>$teco,'status'=>0));
        }
        if($tipo==2){
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_poliza_a_e',array('tecnico'=>$tecd),array('asignacionId'=>$idser,'tecnico'=>$teco,'status'=>0));
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_poliza_a_e',array('tecnico2'=>$tecd),array('asignacionId'=>$idser,'tecnico2'=>$teco,'status'=>0));
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_poliza_a_e',array('tecnico3'=>$tecd),array('asignacionId'=>$idser,'tecnico3'=>$teco,'status'=>0));
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_poliza_a_e',array('tecnico4'=>$tecd),array('asignacionId'=>$idser,'tecnico4'=>$teco,'status'=>0));
        }
        if($tipo==3){
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_cliente_a',array('tecnico'=>$tecd),array('asignacionId'=>$idser,'tecnico'=>$teco,'status'=>0));
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_cliente_a',array('tecnico2'=>$tecd),array('asignacionId'=>$idser,'tecnico2'=>$teco,'status'=>0));
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_cliente_a',array('tecnico3'=>$tecd),array('asignacionId'=>$idser,'tecnico3'=>$teco,'status'=>0));
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_cliente_a',array('tecnico4'=>$tecd),array('asignacionId'=>$idser,'tecnico4'=>$teco,'status'=>0));

            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_cliente_a_d',array('tecnico'=>$tecd),array('asignacionId'=>$idser,'tecnico'=>$teco,'status'=>0));
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_cliente_a_d',array('tecnico2'=>$tecd),array('asignacionId'=>$idser,'tecnico2'=>$teco,'status'=>0));
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_cliente_a_d',array('tecnico3'=>$tecd),array('asignacionId'=>$idser,'tecnico3'=>$teco,'status'=>0));
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_cliente_a_d',array('tecnico4'=>$tecd),array('asignacionId'=>$idser,'tecnico4'=>$teco,'status'=>0));
        }
        if($tipo==4){
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_venta_a',array('tecnico'=>$tecd),array('asignacionId'=>$idser,'tecnico'=>$teco,'status'=>0));
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_venta_a',array('tecnico2'=>$tecd),array('asignacionId'=>$idser,'tecnico2'=>$teco,'status'=>0));
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_venta_a',array('tecnico3'=>$tecd),array('asignacionId'=>$idser,'tecnico3'=>$teco,'status'=>0));
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_venta_a',array('tecnico4'=>$tecd),array('asignacionId'=>$idser,'tecnico4'=>$teco,'status'=>0));
        }
        //===================================================
            $result_iti_o = $this->ModeloCatalogos->getselectwheren('unidades_bitacora_itinerario',array('fecha'=>$this->fechahoy,'asignacion_gen'=>"|$tipo,$idser|",'personal'=>$teco));
            if($result_iti_o->num_rows()>0){
                foreach ($result_iti_o->result() as $item_o) {
                    $id_iti_o = $item_o->id;
                        $result_iti_d = $this->ModeloCatalogos->getselectwheren('unidades_bitacora_itinerario',array('fecha'=>$this->fechahoy,'personal'=>$tecd));
                        if($result_iti_d->num_rows()>0){
                            $this->ModeloCatalogos->updateCatalogo('unidades_bitacora_itinerario',array('personal'=>$tecd),array('id'=>$id_iti_o));
                        }else{
                            $this->ModeloCatalogos->getdeletewheren('unidades_bitacora_itinerario',array('id'=>$id_iti_o));
                        }
                }
            }else{
                $result_iti_d = $this->ModeloCatalogos->getselectwheren('unidades_bitacora_itinerario',array('fecha'=>$this->fechahoy,'personal'=>$tecd));
                if($result_iti_d->num_rows()>0){
                    //==================================================
                        $unidadid=0;
                        $res_uni=$this->ModeloCatalogos->getselectwheren('unidades',array('tecnico_asignado'=>$tecd,'activo'=>1));
                        foreach ($res_uni->result() as $item_u) {
                            $unidadid=$item_u->id;
                        }
                        if ($tipo==1) {
                            $resultado=$this->ModeloAsignacion->getdatosrenta($idser,$this->fechahoy,0,0);
                            foreach ($resultado->result() as $item) {
                                $fecha = $item->fecha;
                                $personalId1 = $item->personalId1;
                                $personalId2 = $item->personalId2;
                                $empresa = $item->empresa;
                                $hora = $item->hora;
                                $horafin = $item->horafin;

                                $hora_comida=$item->hora_comida;
                                $horafin_comida=$item->horafin_comida;
                                $prioridad2=$item->prioridadser;
                                $cli=$item->idCliente;
                            }
                        }
                        if ($tipo==2) {
                            $resultado=$this->ModeloAsignacion->getdatospoliza($idser,0,0);
                            foreach ($resultado->result() as $item) {
                                $fecha = $item->fecha;
                                $personalId1 = $item->personalId1;
                                $personalId2 = $item->personalId2;
                                $empresa = $item->empresa;
                                $hora = $item->hora;
                                $horafin = $item->horafin;

                                $hora_comida=$item->hora_comida;
                                $horafin_comida=$item->horafin_comida;
                                $prioridad2=$item->prioridadser;
                                $cli=$item->idCliente;
                            }

                        }
                        if ($tipo==3) {
                            $resultado=$this->ModeloAsignacion->getdatoscliente($idser);
                            foreach ($resultado->result() as $item) {
                                $fecha = $item->fecha;
                                $personalId1 = $item->personalId1;
                                $personalId2 = $item->personalId2;
                                $empresa = $item->empresa;
                                $hora = $item->hora;
                                $horafin = $item->horafin;

                                $hora_comida=$item->hora_comida;
                                $horafin_comida=$item->horafin_comida;
                                $prioridad2=$item->prioridadser;
                                $cli=$item->clienteId;
                            }
                        }
                        if ($tipo==4) {
                            $resultado=$this->ModeloAsignacion->getdatosventas($idser);
                            foreach ($resultado->result() as $item) {
                                $fecha = $item->fecha;
                                $personalId1 = $item->personalId;
                                $personalId2 = 0;
                                $empresa = $item->empresa;
                                $hora = $item->hora;
                                $horafin = $item->horafin;

                                $hora_comida=$item->hora_comida;
                                $horafin_comida=$item->horafin_comida;
                                $prioridad2=$item->prioridad2;
                                $cli=$item->clienteId;
                            }
                        }
                        $paramsai['descripcion']=$empresa;
                        //$paramsai['hora_inicio']=$hora;
                        $paramsai['hora_inicio']=$this->horaactual;
                        
                        $paramsai['hora_fin']=$horafin;
                        $paramsai['hora_limite']=$horafin;

                        $paramsai['fecha']=$fecha;
                        $paramsai['personal']=$tecd;
                        $paramsai['asignacion']=$idser;
                        $paramsai['tipo_asignacion']=$tipo;
                        $asignacion_gen="|$tipo,$idser|";
                        $paramsai['asignacion_gen']=$asignacion_gen;
                        if($prioridad2<8){
                            $paramsai['tipoagregado']=1;
                        }
                        $paramsai['hora_comida']=$hora_comida;
                        $paramsai['horafin_comida']=$horafin_comida;
                        $paramsai['cli']=$cli;
                        if($unidadid>0){
                            $paramsai['unidadid']=$unidadid;
                        }
                        $paramsai['prioridad']=$prioridad2;
                    //==================================================
                    
                    $this->ModeloCatalogos->Insert('unidades_bitacora_itinerario',$paramsai);
                }
            }
        //===================================================
    }
    function confirmacionmv_todo(){
        $params = $this->input->post();
        $fecha = $params['fecha'];
        $tecd = $params['tecd'];
        $teco = $params['teco'];


        $this->ModeloCatalogos->updateCatalogo('asignacion_ser_contrato_a_e',array('tecnico'=>$tecd),array('fecha'=>$fecha,'tecnico'=>$teco,'status'=>0));
        $this->ModeloCatalogos->updateCatalogo('asignacion_ser_contrato_a_e',array('tecnico2'=>$tecd),array('fecha'=>$fecha,'tecnico2'=>$teco,'status'=>0));
        $this->ModeloCatalogos->updateCatalogo('asignacion_ser_contrato_a_e',array('tecnico3'=>$tecd),array('fecha'=>$fecha,'tecnico3'=>$teco,'status'=>0));
        $this->ModeloCatalogos->updateCatalogo('asignacion_ser_contrato_a_e',array('tecnico4'=>$tecd),array('fecha'=>$fecha,'tecnico4'=>$teco,'status'=>0));

        $this->ModeloCatalogos->updateCatalogo('asignacion_ser_poliza_a_e',array('tecnico'=>$tecd),array('fecha'=>$fecha,'tecnico'=>$teco,'status'=>0));
        $this->ModeloCatalogos->updateCatalogo('asignacion_ser_poliza_a_e',array('tecnico2'=>$tecd),array('fecha'=>$fecha,'tecnico2'=>$teco,'status'=>0));
        $this->ModeloCatalogos->updateCatalogo('asignacion_ser_poliza_a_e',array('tecnico3'=>$tecd),array('fecha'=>$fecha,'tecnico3'=>$teco,'status'=>0));
        $this->ModeloCatalogos->updateCatalogo('asignacion_ser_poliza_a_e',array('tecnico4'=>$tecd),array('fecha'=>$fecha,'tecnico4'=>$teco,'status'=>0));

        $this->ModeloCatalogos->updateCatalogo('asignacion_ser_cliente_a',array('tecnico'=>$tecd),array('fecha'=>$fecha,'tecnico'=>$teco,'status'=>0));
        $this->ModeloCatalogos->updateCatalogo('asignacion_ser_cliente_a',array('tecnico2'=>$tecd),array('fecha'=>$fecha,'tecnico2'=>$teco,'status'=>0));
        $this->ModeloCatalogos->updateCatalogo('asignacion_ser_cliente_a',array('tecnico3'=>$tecd),array('fecha'=>$fecha,'tecnico3'=>$teco,'status'=>0));
        $this->ModeloCatalogos->updateCatalogo('asignacion_ser_cliente_a',array('tecnico4'=>$tecd),array('fecha'=>$fecha,'tecnico4'=>$teco,'status'=>0));

        // ajustar esta parte
        //$this->ModeloCatalogos->updateCatalogo('asignacion_ser_cliente_a_d',array('tecnico'=>$tecd),array('asignacionId'=>$idser,'tecnico'=>$teco,'status'=>0));
        //$this->ModeloCatalogos->updateCatalogo('asignacion_ser_cliente_a_d',array('tecnico2'=>$tecd),array('asignacionId'=>$idser,'tecnico2'=>$teco,'status'=>0));
        //$this->ModeloCatalogos->updateCatalogo('asignacion_ser_cliente_a_d',array('tecnico3'=>$tecd),array('asignacionId'=>$idser,'tecnico3'=>$teco,'status'=>0));
        //$this->ModeloCatalogos->updateCatalogo('asignacion_ser_cliente_a_d',array('tecnico4'=>$tecd),array('asignacionId'=>$idser,'tecnico4'=>$teco,'status'=>0));


        $this->ModeloCatalogos->updateCatalogo('asignacion_ser_venta_a',array('tecnico'=>$tecd),array('fecha'=>$fecha,'tecnico'=>$teco,'status'=>0));
        $this->ModeloCatalogos->updateCatalogo('asignacion_ser_venta_a',array('tecnico2'=>$tecd),array('fecha'=>$fecha,'tecnico2'=>$teco,'status'=>0));
        $this->ModeloCatalogos->updateCatalogo('asignacion_ser_venta_a',array('tecnico3'=>$tecd),array('fecha'=>$fecha,'tecnico3'=>$teco,'status'=>0));
        $this->ModeloCatalogos->updateCatalogo('asignacion_ser_venta_a',array('tecnico4'=>$tecd),array('fecha'=>$fecha,'tecnico4'=>$teco,'status'=>0));

    }
    function obtenerdirecciones(){
        $params = $this->input->post();
        $fecha = $params['fecha'];
        $tipo = $params['tecd'];
        $servicio = $params['teco'];

        if($tipo==1){//contrato

        }
        if($tipo==2){//poliza

        }
        if($tipo==3){//cliente

        }
    }
    function tableroinfo(){
        $params = $this->input->post();
        $fecha = $params['fech'];
        $mov_seer_activo=$params['mov'];
        $info=$this->Modelotablero->tablero($mov_seer_activo,$fecha);
        echo $info;
    }

}