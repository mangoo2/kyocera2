<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Listapreciosrefacciones extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->model('refacciones_model');
        $this->load->model('ModeloCatalogos');
        $this->load->model('login_model');
        $this->load->helper('url');
        $this->load->library('menus');
        $this->submenu=19;
        if($this->session->userdata('logeado')==true){
            $this->perfilid =$this->session->userdata('perfilid');
            $this->idpersonal = $this->session->userdata('idpersonal');
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,$this->submenu);// 19 es el id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
        }else{
            redirect('login');
        }
    }

    // Listado de Precios de Refacciones
    function index(){
        $data['MenusubId']=$this->submenu;
            // Obtiene los menús disponibles del sistema dependiendo el perfil, ayudado de la libreria "MENUS"
            $menus = $this->menus->getMenus($this->session->userdata('perfilid'));
            $data['menus'] = $menus['menus'];
            $data['submenusArray'] = $menus['submenusArray'];
            // Cargamos las vistas
            $this->load->view('header');
            $this->load->view('main',$data);
            $this->load->view('preciosrefacciones/listado');
            $this->load->view('preciosrefacciones/listado_js');
            $this->load->view('footer'); 
    }

    // Obtener el JSON del listado de refacciones para enviar a la tabla
    function getListaPreciosRefacciones(){
        $precios = $this->refacciones_model->getListaPreciosRefacciones();
        $json_data = array("data" => $precios);
        echo json_encode($json_data);
    }

    // Actualizar un precio que venga de la tabla
    function actualizaPrecio(){
        $data = $this->input->post();

        // La variable KEY traerá el ID del equipo a actualizar
        $key = key($data["data"]);
        
        // Comparamos si lo que trae es el precio en Dólares, para hacer los cálculos
        if(key($data["data"][$key])=='precioDolares'){
            // Obtenemos el Tipo de Cambio
            $cambio = $this->refacciones_model->getTipoCambioRefacciones();

            // Convertimos a Pesos 
            $tipoCambio = $cambio[0]->cambio;
            $precio = $data["data"][$key]['precioDolares'];
            $precioPesos = $tipoCambio * $precio;

            // Obtenemos los porcentajes de Ganancia para Refacciones
            $porcentajesGanancia = $this->refacciones_model->getPorcentajeGananciaRefacciones();

            // Guardamos cada uno de los porcentajes correspondientes
            // VENTA = GENERAL; RENTA = ESPECIAL; POLIZA = FRECUENTE; REVENDEDOR = POLIZA
            $porcentajeGeneral = $porcentajesGanancia[0]->gananciaVenta;
            //$porcentajeEspecial = $porcentajesGanancia[0]->gananciaRenta;
            //$porcentajeFrecuente = $porcentajesGanancia[0]->gananciaPoliza;
            //$porcentajePoliza = $porcentajesGanancia[0]->gananciaRevendedor;

            // Obtenemos el total de ganancia para cada tarea
            //$gananciaGeneral = ($porcentajeGeneral * $precioPesos) / 100;
            //$gananciaEspecial = ($porcentajeEspecial * $precioPesos) / 100;
            //$gananciaFrecuente = ($porcentajeFrecuente * $precioPesos) / 100;
            //$gananciaPoliza = ($porcentajePoliza * $precioPesos) / 100;

            // Sumamos las ganancias al precio
            //$precioConGananciaGeneral = $precioPesos + $gananciaGeneral;
            //$precioConGananciaEspecial = $precioPesos + $gananciaEspecial;
            //$precioConGananciaFrecuente = $precioPesos + $gananciaFrecuente;
            //$precioConGananciaPoliza = $precioPesos + $gananciaPoliza;

            // Obtenemos los descuentos
            $descuentos = $this->refacciones_model->getDescuentosRefacciones();
            $descuentoRefacciones = $descuentos[0]->descuento;

            // Hacemos los descuentos sobre los precios con Ganancia
            $totalDescuento = ($descuentoRefacciones * $precioPesos) / 100;

            //$precioConDescuentoGeneral = $precioConGananciaGeneral - $totalDescuento;
            //$precioConDescuentoEspecial = $precioConGananciaEspecial - $totalDescuento;
            //$precioConDescuentoFrecuente = $precioConGananciaFrecuente - $totalDescuento;
            //$precioConDescuentoPoliza = $precioConGananciaPoliza - $totalDescuento;
            //============================================================================
            $precioConDescuentoGeneral=0;
            $precioConDescuentoEspecial=0;
            $precioConDescuentoFrecuente=0;
            $precioConDescuentoPoliza=0;
            $tableresult = $this->ModeloCatalogos->getselectwheren('tabla_descuentos_refacciones',array('activo'=>1));
            foreach ($tableresult->result() as $item) {
                if ($item->id<6) {
                    if (floatval($precio)>=floatval($item->pd_inicio) && floatval($precio)<=floatval($item->pd_fin)) {
                        $precioConDescuentoGeneral=$precio*$item->for_gen;
                        $precioConDescuentoEspecial=floatval($item->aaa)*floatval($precio);
                        $precioConDescuentoFrecuente=floatval($item->sem_frec)*floatval($precio);
                        //log_message('error', $item->id.' '.$item->sem_frec.' * '.$precio);
                        $precioConDescuentoPoliza=floatval($item->local_poli)*floatval($precio);
                        
                    }
                }else{
                    if (floatval($precio)>=floatval($item->pd_inicio)) {
                        $precioConDescuentoGeneral=$precio*$item->for_gen;
                        $precioConDescuentoEspecial=floatval($item->aaa)*floatval($precio);
                        $precioConDescuentoFrecuente=floatval($item->sem_frec)*floatval($precio);
                        $precioConDescuentoPoliza=floatval($item->local_poli)*floatval($precio);
                        //log_message('error', $item->id.' '.$item->pd_inicio.'>='.$precio);
                    }
                }
            }

            $data["data"][$key]['general'] = $precioConDescuentoGeneral;
            $data["data"][$key]['especial'] = $precioConDescuentoEspecial;
            $data["data"][$key]['frecuente'] = $precioConDescuentoFrecuente;
            $data["data"][$key]['poliza'] = $precioConDescuentoPoliza;
            $data["data"][$key]['costo_pesos'] = $precioPesos;
            $data["data"][$key]['porcentaje_ganancia'] = $porcentajeGeneral;
            $data["data"][$key]['descuento'] = $totalDescuento;
        } 
        $this->Modelobitacoras->Insert(array('contenido'=>'Se Actualizo Lista de precios Refacciones','nombretabla'=>'refacciones_costos','idtable'=>0,'tipo'=>'Update','personalId'=>$this->idpersonal)); 

        // Se actualiza elregistro        
        $idActualizado = $this->refacciones_model->update_precios_refacciones($data["data"][$key], $key);
        
        // Sí se actualizó bien, se regresan los datos a la tabla
        if($idActualizado==$key)
        {
            // Se obtienene todos los datos del listado
            $preciosActualizados = $this->refacciones_model->getPreciosRefaccionesPorId($idActualizado);
            $preciosActualizadosAux = (array) $preciosActualizados[0];
            
            // Se arma el array
            $dataRetorno = array($preciosActualizadosAux ); 
            
            // Se crea el data y se regresa en JSON
            $datosRetorno = array("data" => $dataRetorno);
            echo json_encode($datosRetorno);
        }
    }

}