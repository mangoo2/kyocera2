<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class Crm_model extends CI_Model 
{
    public function __construct() 
    {
        parent::__construct();
    }

	function getListadoLlamadasPorProspectoAgenda($idProspecto)
    {
        $sql = "SELECT id, CONCAT('Llamada: ',motivo) as title, comentario, 
			CASE
			    WHEN estatus = 2 THEN '#66b4e8'
			    WHEN estatus = 1 AND proxima<CURDATE() THEN '#d60831'
			    ELSE '#058e1c'
			END as 'color', CONCAT(proxima,'T',horaProxima) as start, CONCAT(proxima,'T',horaProxima) as end FROM llamadas WHERE idProspecto=".$idProspecto;
        $query = $this->db->query($sql);
        return $query->result();
    }

    function getListadoCorreosPorProspectoAgenda($idProspecto)
    {
        $sql = "SELECT id, CONCAT('Correo: ',motivo) as title, comentario, 
			CASE
			    WHEN estatus = 2 THEN '#66b4e8'
			    WHEN estatus = 1 AND proxima<CURDATE() THEN '#d60831'
			    ELSE '#058e1c'
			END as 'color', CONCAT(proxima,'T',horaProxima) as start, CONCAT(proxima,'T',horaProxima) as end FROM correos WHERE idProspecto=".$idProspecto;
        $query = $this->db->query($sql);
        return $query->result();
    }

    function getListadoVisitasPorProspectoAgenda($idProspecto)
    {
        $sql = "SELECT id, CONCAT('Visita: ',motivo) as title, comentario, 
            CASE
                WHEN estatus = 2 THEN '#66b4e8'
                WHEN estatus = 1 AND proxima<CURDATE() THEN '#d60831'
                ELSE '#058e1c'
            END as 'color', CONCAT(proxima,'T',horaProxima) as start, CONCAT(proxima,'T',horaProxima) as end FROM visitas WHERE idProspecto=".$idProspecto;
        $query = $this->db->query($sql);
        return $query->result();
    }
}