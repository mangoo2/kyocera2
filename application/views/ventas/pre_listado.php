<style type="text/css">
  table td{
    font-size: 12px;
    padding: 6px 7px;
  }
  .chosen-container{
    margin-left: 0px;
  }
</style>
<!-- START CONTENT -->
    <section id="content">
      <!--start container-->
        <div class="container">
          <div class="section">
            <div class="card-panel">
            <input id="base_url" type="hidden" name="base_url" value="<?php echo base_url(); ?>">
            <div id="table-datatables">
              <br>
                  <h4 class="header">Listado</h4>
                  <div class="row">
                      <div class="col s2">
                        <label>Tipo de listado</label>
                        <select id="tipoventa" class="browser-default chosen-select">
                          <option value="0">Todas</option>
                          <option value="1">Venta</option>
                          <option value="2">Rentas</option>
                          <option value="5">Rentas Historial</option>
                          <option value="3">Pólizas Creadas</option>
                          <option value="4">Combinada</option>
                        </select>
                      </div>
                      <div class="col s2">
                        <label>Clientes</label>
                        <select id="cliente" class="browser-default">
                          <option value="0">Selecione</option>
                        </select>
                      </div>
                      <div class="col s2">
                        <label>Tipo de estatus</label>
                        <select id="tipoestatus" class="browser-default chosen-select">
                          <option value="0">Selecione</option>
                          <option value="1">Pagadas</option>
                          <option value="2">Pendientes</option>
                        </select>
                      </div>
                      <div class="col s2">
                        <label>Fecha Inicio</label>
                        <input type="date" id="fechainicio" class="form-control-bmz">
                      </div>
                      <div class="col s2">
                        <label>Fecha Fin</label>
                        <input type="date" id="fechafin" class="form-control-bmz">
                      </div>
                      <div class="col s2">
                        <label>Empleados</label>
                        <select id="idpersonal" class="browser-default chosen-select">
                          <option value="0">Todos</option>
                          <?php foreach ($get_personal->result() as $p) { ?>
                            <option value="<?php echo $p->personalId ?>"><?php echo $p->nombre ?></option>
                          <?php } ?>
                          <?php ?>
                        </select>
                        <!-- <input type="checkbox" name="checkbox1" id="tipoview" checked> -->
                      </div>
                      <div class="col s2">
                        <br>
                        <button class="btn-bmz cyan waves-effect waves-light" style="width: 100%" type="button" onclick="generarreporte_excel()">Generar
                        </button>
                      </div>
                  </div>
                  <!--
                  <div class="col s2">  
                    <button class="btn waves-effect waves-light gradient-45deg-green-teal gradient-shadow" >Facturar</button>
                  </div>
                  -->
                <!-- <div class="row">
                  <div class="col s12">
                    <table id="tabla_ventas_incompletas" class="responsive-table display" cellspacing="0">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Vendedor</th>
                          <th>Cliente</th>
                          <th>PRE FACTURA</th>
                          <th>Tipo</th>
                          <th>Acciones</th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                  </div>
                </div> -->

            </div>
          </div>
          </div>  
            <!-- Modal finaliza -->
            <div id="modalFinaliza" class="modal"  >
                <div class="modal-content ">
                  <h4>Confirmatión</h4>
                  <div class="col s12 m12 l6">
                        <div class="row" align="center">
                          <h5>Deseas finalizar factura</h5>
                        </div>
                  </div>  
                <input type="hidden" id="id_venta">  
                <div class="modal-footer">
                  <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
                  <a href="#!" class="modal-action modal-close waves-effect waves-red red btn-flat" style="color: white;" onclick="finalizar_factura()">Aceptar</a>
                </div>
              </div>
              <!-- Modal finaliza fin-->
        </div>
        <!-- Modal 1 -->
        <div id="modalDetalles" class="modal"  >
          <div class="modal-content ">
            <h4> Detalles </h4>
            <div class="col s12 m12 l6">
                  <div class="row" align="center">
                    <div class="detalles_ventas"></div>
                  </div>
            </div>  
          <div class="modal-footer">
            <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
          </div>
        </div>
        
        </div>

        <div id="modal_cfdi" class="modal"  >
          <div class="modal-content ">
            <h4 align="center">Captura CFDI</h4>
            <br>
            <div class="row">
              <div class="input-field col s2"></div>
              <div class="input-field col s8">
                <input type="text" id="cdfi_e">
              </div>
              <div class="input-field col s2"></div>
              <input type="hidden" id="id_venta_e">
              <input type="hidden" id="tipo_e">
            </div> 
          </div>    
          <div class="modal-footer">
            <a class="modal-action modal-close waves-effect waves-red gray btn-flat" onclick="update_cfdi()">Guardar</a>
          </div>
        </div>
        <!-- Modal 1 -->
        <div id="modal_pago" class="modal"  >
          <div class="modal-content ">
            <div class="row">
              <div class="col s6">
                <h5>Registro de nuevo pago</h5>
              </div>
              <div class="col s6" align="right">
                <p><h6>Monto de prefactura</h6></p>
                <p><span style="color: red" class="monto_prefactura"></span></p>
              </div>
            </div>  
            <br>
            <h5>Vendedor: <?php echo $nombre.' '.$apellido_paterno.' '.$apellido_materno ?></h5>
            <form class="form" id="form_pago" method="post"> 
              <input type="hidden" id="id_venta_p" name="idcompra"><!--id que se va aguardar dependiendo si es venta, renta, poliza y combinada -->
              <input type="hidden" id="tipo_p" name="tipo"><!-- tipo para verificar en que tabla se va insertar --> 
              <div class="row">
                <div class="input-field col s6">
                  <input type="date" name="fecha" id="fecha_p">
                  <label for="fecha" class="active">Fecha</label>
                </div>
                <div class="input-field col s6">
                  <label for="idmetodo" class="active">Método de pago</label><br>
                  <select name="idmetodo" id="idmetodo" class="browser-default chosen-select">
                     <?php foreach ($get_metodo_pago->result() as $item) { ?>
                      <option value="<?php echo $item->id ?>"><?php echo $item->metodopago_text ?></option>
                    <?php } ?>
                  </select>  
                </div>
              </div>
              <div class="row">
                <div class="input-field col s6">
                  <input type="number" name="pago" id="pago_p">
                  <label for="pago" class="active">Monto</label>
                </div>
              </div> 
              <div class="row">
                <div class="input-field col s12">
                  <textarea rows="2" name="observacion" id="observacion_p"></textarea>
                  <label class="active">Comentario</label>
                </div>
              </div>
            </form>
          </div>    
          <div class="modal-footer">
            <a class="modal-action waves-effect waves-red gray btn-flat" onclick="guardar_pago_compras()">Guardar</a>
          </div>
          <div class="modal-footer">
             <div class="col s12">
              <table id="tabla_pagos" class="responsive-table display" cellspacing="0">
                <thead>
                  <tr>
                    <th>Fecha</th>
                    <th>Método de pago</th>
                    <th>Monto</th>
                    <th>Comprobante</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
          </div>
        </div>
    </section>

