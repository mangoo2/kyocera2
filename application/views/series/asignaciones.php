<style type="text/css">
  table td{
    font-size: 12px;
    padding: 6px 7px;
  }
  .asignarrmultiple{
    display: none;
    text-align: end;
  }
  .div_btn{
    min-width: 171px;
  }
  .errorinput{
    border: 1px solid red !important;
    color: red !important;
  }
  .card-content.card-padding{
    padding-left: 5px;padding-right: 5px;padding-top: 13px;padding-bottom: 13px;
  }
  .ser_mul_g{
    margin: 0;px;height: 24px;
  }
</style>
<link href="<?php echo base_url(); ?>public/css/radiocolor.css?190825" rel="stylesheet">
<!-- START CONTENT -->
<section id="content">
  <!--start container-->
  <div class="container">
    <div class="section">
      <div class="card-panel">
      		<div id="table-datatables">
            <h4 class="header">Listado</h4>
            <div class="row">
              <div class="col s12">
                <div class="col s3">
                  <label>Fecha inicio</label>
                  <input type="date" id="fechainicio" class="form-control-bmz" value="<?php echo $fechainicial_reg;?>" onchange="seleccionconsulta()">
                </div>
                <div class="col s3">
                  <label>Consulta</label>
                  <select id="consulta" name="consulta" class="browser-default form-control-bmz" onchange="seleccionconsulta()">
                    <option value="0">Equipo</option>
                    <option value="1">Accesorios</option>
                    <option value="2">Refacciones</option>
                  </select>
                  
                </div>
                <div class="col s3">
                  <label>Tipo</label>
                  <select id="tipoc" name="tipoc" class="browser-default form-control-bmz" onchange="seleccionconsulta()">
                    <option value="0">Sin asignar</option>
                    <option value="1">Asignadas</option>
                    <option value="2">Todas</option>
                  </select>
                  
                </div>
              </div>
              <div class="col s12">
                <div class="col s3 " >
                  <input type="radio" name="tipoasi" id="tipoasignacion1" value="1" checked onclick="seleccionconsulta()">
                  <label for="tipoasignacion1">Ventas</label>
                  <input type="radio" name="tipoasi" id="tipoasignacion0" class="selectedradio" value="0" onclick="seleccionconsulta()">
                  <label for="tipoasignacion0" class="selectedradio">Rentas</label>
                  <!--<input type="radio" name="tipoasi" id="tipoasignacion2" value="2" onclick="seleccionconsulta()">
                  <label for="tipoasignacion2">Ventas D</label>-->
                </div>
                <div class="col s9 asignarrmultiple">
                  <a class="btn-bmz waves-effect waves-light cyan" onclick="asignarrmultiple()">Asignación múltiple</a>
                </div>
              </div> 
              <div class="col s12">

                <table id="tabla_asignar" class="responsive-table display" cellspacing="0">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th class="cambiotitulo"># Venta</th>
                      <th>Folio</th>
                      <th>Vendedor</th>
                      <th>Bodega</th>
                      <th>Cantidad</th>
                      <th>Modelo</th>
                      <th>No Parte</th>
                      <th>Cliente</th>
                      <th>Fecha Solicitud</th>
                      <td>Acciones</td>
                    </tr>
                  </thead>
                  <tbody>
                    <!------------------------------------->
                    
                    <!------------------------------------->
                  </tbody>
                </table>
              </div>
            </div>
          </div>
      </div>
    </div>  
  </div>
</section>
<div class="modal fade" id="modal_series_e" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="row">
          <div class="col m8 s8 8">
            <h5 class="modal-title" id="exampleModalLabel">Asignacion de series equipos </h5>
          </div>
          <div class="col m4 s4 4"><input type="search" id="search_1" placeholder="Buscar" class="form-control-bmz" onkeyup="search_1()"></div>
        </div>
        
        
      </div>
      <div class="modal-body" style="min-height: 270px;">
        <div class="row addseriesequipos_selected">   
          
        </div>
        <div class="row addseriesequipos">   
          
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="modal-action modal-close waves-effect waves-red gray btn-flat">Cancelar</button>
        <button type="button" class="btn blue aceptar_series_e" onclick="aceptar_series_e()">Aceptar</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal de rentas asignacion -->
<div class="modal fade" id="modal_series_rentas" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="row">
          <div class="col m8 s8 8">
            <h5 class="modal-title" id="exampleModalLabel">Asignacion de series equipos rentas</h5>
          </div>
          <div class="col m4 s4 4"><input type="search" id="search_2" placeholder="Buscar" class="form-control-bmz" onkeyup="search_2()"></div>
        </div>
      </div>
      <div class="modal-body" style="min-height: 270px;">
        <div class="row addseriesequipos_rentas_selected"></div>
        <div class="row addseriesequipos_rentas">   
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="modal-action modal-close waves-effect waves-red gray btn-flat">Cancelar</button>
        <button type="button" class="btn blue aceptar_series_rentas" onclick="aceptar_series_rentas()">Aceptar</button>
      </div>
    </div>
  </div>
</div>
<!-- -->
<div class="modal fade" id="modal_series_a" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="row">
          <div class="col m8 s8 8">
            <h5 class="modal-title" id="exampleModalLabel">Asignacion de series Accesorios</h5>
          </div>
          <div class="col m4 s4 4"><input type="search" id="search_3" placeholder="Buscar" class="form-control-bmz" onkeyup="search_3()"></div>
        </div>
      </div>
      <div class="modal-body" style="min-height: 270px;">
        <div class="row addseriesaccesorios">   
          
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="modal-action modal-close waves-effect waves-red gray btn-flat">Cancelar</button>
        <button type="button" class="btn blue aceptar_series_a" onclick="aceptar_series_a()">Aceptar</button>
      </div>
    </div>
  </div>
</div>
<!-- Accesorios rentas -->
<div class="modal fade" id="modal_series_a_rentas" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="row">
          <div class="col m8 s8 8">
            <h5 class="modal-title" id="exampleModalLabel">Asignacion de series Accesorios de rentas</h5>
          </div>
          <div class="col m4 s4 4"><input type="search" id="search_4" placeholder="Buscar" class="form-control-bmz" onkeyup="search_4()"></div>
        </div>

      </div>
      <div class="modal-body" style="min-height: 270px;">
        <div class="row addseriesaccesorios_rentas">   
          
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="modal-action modal-close waves-effect waves-red gray btn-flat">Cancelar</button>
        <button type="button" class="btn blue aceptar_series_a_rentas" onclick="aceptar_series_a_rentas()">Aceptar</button>
      </div>
    </div>
  </div>
</div>

<!-- --> 
<div class="modal fade" id="modal_series_r" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="row">
          <div class="col m8 s8 8">
            <h5 class="modal-title" id="exampleModalLabel">Asignacion de series Refacciones</h5>
          </div>
          <div class="col m4 s4 4"><input type="search" id="search_5" placeholder="Buscar" class="form-control-bmz" onkeyup="search_5()"></div>
        </div>
      </div>
      <div class="modal-body" style="min-height: 270px;">
        <div class="row addseriesrefacciones">   
          
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="modal-action modal-close waves-effect waves-red gray btn-flat">Cancelar</button>
        <button type="button" class="btn blue aceptar_series_r" onclick="aceptar_series_r()">Aceptar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal_series_editar_e" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Editar de series equipos</h5>
      </div>
      <div class="modal-body" style="min-height: 270px;">
        <div class="row">
          <div class="col s2">
            Agregado
          </div>
          <div class="col s12 newoseriesequipos">
            
          </div>
        </div>
        <div class="row">
          <div class="col s2">
            Nuevo
          </div>
          <div class="col s12 newseriesequipos">
            
          </div>
        </div>
        

      </div>
      <div class="modal-footer">
        <button type="button" class="modal-action modal-close waves-effect waves-red gray btn-flat">Cancelar</button>
        <button type="button" class="btn blue" onclick="editar_series_e()">Aceptar</button>
      </div>
    </div>
  </div>
</div>
<!-- Rentas-->
<div class="modal fade" id="modal_series_editar_e_renta" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Editar de series equipos de rentas</h5>
      </div>
      <div class="modal-body" style="min-height: 270px;">
        <div class="row">
          <div class="col s2">
            Agregado
          </div>
          <div class="col s12 newoseriesequipos_renta">
            
          </div>
        </div>
        <div class="row">
          <div class="col s2">
            Nuevo
          </div>
          <div class="col s12 newseriesequipos_rentas">
            
          </div>
        </div>
        

      </div>
      <div class="modal-footer">
        <button type="button" class="modal-action modal-close waves-effect waves-red gray btn-flat">Cancelar</button>
        <button type="button" class="btn blue" onclick="editar_series_rentas()">Aceptar</button>
      </div>
    </div>
  </div>
</div>
<!-- -->
<div class="modal fade" id="modal_series_editar_a" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Editar de series Accesorios</h5>
      </div>
      <div class="modal-body" >
        <div class="row">
          <div class="col s2">
            Agregado
          </div>
          <div class="col s12 newoseriesaccesorios">
            
          </div>
        </div>
        <div class="row">
          <div class="col s2">
            Nuevo
          </div>
          <div class="col s12 newseriesaccesorios">
            
          </div>
        </div>
        

      </div>
      <div class="modal-footer">
        <button type="button" class="modal-action modal-close waves-effect waves-red gray btn-flat">Cancelar</button>
        <button type="button" class="btn blue" onclick="editar_series_a()">Aceptar</button>
      </div>
    </div>
  </div>
</div>
<!-- accesorios rentas -->
<div class="modal fade" id="modal_series_editar_a_rentas" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Editar de series Accesorios de rentas</h5>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col s2">
            Agregado
          </div>
          <div class="col s12 newoseriesaccesorios_rentas">
            
          </div>
        </div>
        <div class="row">
          <div class="col s2">
            Nuevo
          </div>
          <div class="col s12 newseriesaccesorios_rentas">
            
          </div>
        </div>
        

      </div>
      <div class="modal-footer">
        <button type="button" class="modal-action modal-close waves-effect waves-red gray btn-flat">Cancelar</button>
        <button type="button" class="btn blue" onclick="editar_series_a_rentas()">Aceptar</button>
      </div>
    </div>
  </div>
</div>
<!-- -->
<div class="modal fade" id="modal_series_editar_r" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Editar de series Refacciones</h5>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col s2">
            Agregado
          </div>
          <div class="col s12 newoseriesrefacciones">
            
          </div>
        </div>
        <div class="row">
          <div class="col s2">
            Nuevo
          </div>
          <div class="col s12 newseriesrefacciones">
            
          </div>
        </div>
       

      </div>
      <div class="modal-footer">
        <button type="button" class="modal-action modal-close waves-effect waves-red gray btn-flat">Cancelar</button>
        <button type="button" class="btn blue" onclick="editar_series_r()">Aceptar</button>
      </div>
    </div>
  </div>
</div>
<!-- -->
<div class="modal fade" id="modal_series_multiple_refacciones" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="row">
          <div class="col m8 s8 8">
            <h5 class="modal-title" id="exampleModalLabel">Asignacion de series refacciones</h5>
          </div>
        </div>
      </div>
      <div class="modal-body info_asignacion_body" style="min-height: 270px;">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="modal-action modal-close waves-effect waves-red gray btn-flat">Cancelar</button>
        <button type="button" class="btn blue aceptar_series_multiple_r" onclick="aceptar_series_multiple_r()">Aceptar</button>
      </div>
    </div>
  </div>
</div>