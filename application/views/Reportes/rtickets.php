<!-- START CONTENT -->
    <section id="content">
      <!--start container-->
        <div class="container">
          <div class="section">

          <input id="base_url" type="hidden" name="base_url" value="<?php echo base_url(); ?>">

          <div id="table-datatables">
            <br>
                <h4 class="header">Reporte Tickets</h4>
                <div class="row">
                  <div class="col s12">
                    <div class="card-panel">
                      <div class="row">
                        <div class="col s2">
                          <label>Fecha inicial</label>
                          <input type="date" id="fechainicio" class="form-control-bmz">
                        </div>
                        <div class="col s2">
                          <label>Fecha final</label>
                          <input type="date" id="fechafinal" class="form-control-bmz">
                        </div>
                        <div class="col s2">
                          <button class="btn-bmz cyan waves-effect waves-light" type="button" onclick="generarreporte()">Generar</button>
                        </div>
                      </div>
                    </div>
                  </div> 
                      
                </div>
                   
              </div>
            </div>  
               </div>
        </section>

