<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/plugins/toastr/toastr.css">
<script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/toastr/toastr.min.js" ></script>

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/select2.min.css">
<script src="<?php echo base_url(); ?>/public/js/select2.full.min.js" type="text/javascript"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/tipped/tipped-master/dist/js/tipped.min.js" ></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/plugins/tipped/tipped-master/dist/css/tipped.css">

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/plugins/touchspin/jquery.bootstrap-touchspin.min.css">
<script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/touchspin/jquery.bootstrap-touchspin.min.js" ></script>

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/isotope.css">
<script type="text/javascript" src="<?php echo base_url(); ?>public/js/isotope.pkgd.min.js" ></script>


<script type="text/javascript" src="<?php echo base_url(); ?>public/js/solicitud_refaccion/listadoservicios.js?v=<?php echo date('YmdGis');?>" ></script>

<script type="text/javascript" src="<?php echo base_url(); ?>public/js/form_cotizacion.js?v=<?php echo date('YmdGi'); ?>" ></script>
<script type="text/javascript" src="<?php echo base_url(); ?>public/js/notificacion.js?v=<?php echo date('YmdHGis');?>" ></script>


<?php if(isset($_GET['tiposer'])){ ?>
	<script type="text/javascript">
		$(document).ready(function($) {
			$('#fechainicial').val('');
			setTimeout(function(){
				searchser();
				
			}, 1000);
		});
		function searchser(){
			console.log('searchser');
				var asignacionide=parseInt(<?php echo $_GET['asignacionide']?>);
				var asignacionid=parseInt(<?php echo $_GET['asignacionid']?>);
				var tiposer=parseInt(<?php echo $_GET['tiposer']?>);
				var idrow=0;
				if(tiposer==1){//contrato
                    idrow=asignacionide;
                }
                if(tiposer==2){//poliza
                    idrow=asignacionide;
                }
                if(tiposer==3){//cliente
                    idrow=asignacionid;
                }
                if(tiposer==4){//venta
                    idrow=asignacionid;
                }
                $('#serv').val(tiposer).change().trigger("chosen:updated");
                $('#fechainicial').val('');
                table.search(idrow).draw();
		}
	</script>
<?php } ?>