<?php 

// Validamos si es creación o edición, para así cargar los datos respectivamente
if(isset($consumible))
{
    $idConsumible = $consumible->id;
    $foto = $consumible->foto;
    $modelo = $consumible->modelo;
    $parte = $consumible->parte;
    $stock = $consumible->stock;
    $observaciones = $consumible->observaciones;
    $status = $consumible->status;
    $general = $consumible->general;
    $iva = $consumible->iva;
    $neto = $consumible->neto;
    $frecuente = $consumible->frecuente;
    $iva2 = $consumible->iva2;
    $neto2 = $consumible->neto2;
    $especial = $consumible->especial;
    $iva3 = $consumible->iva3;
    $neto3 = $consumible->neto3;
    $poliza = $consumible->poliza;
    $iva4 = $consumible->iva4;
    $neto4 = $consumible->neto4;
    $idConsumiblesCostos = $consumible->idConsumiblesCostos;
    $rendimiento_unidad_imagen = $consumible->rendimiento_unidad_imagen;
    $rendimiento_toner = $consumible->rendimiento_toner;
    $tipo = $consumible->tipo;
}
else
{
    $observaciones='';
    $idConsumible = 0;
    $foto = '';
    $modelo = '';
    $parte = '';
    $stock = '';
    $status = 0;
    $general = '';
    $iva = ''; 
    $neto = '';
    $frecuente = '';
    $iva2 = '';
    $neto2 = '';
    $especial = 0;
    $iva3 = '';  
    $neto3 = '';
    $poliza = '';
    $iva4 = '';
    $neto4 = '';
    $idConsumiblesCostos = 0;
    $rendimiento_unidad_imagen = '';
    $rendimiento_toner = '';
    $tipo = 0;
} 

if($tipoVista==1)
{
  $label = 'Formulario de Alta de Consumible';
}
else if($tipoVista==2)
{
  $label = 'Formulario de Edición de Consumible';
}
else if($tipoVista==3)
{
  $label = 'Formulario de Visualización de Consumible';
}
?>
<style type="text/css">
    <?php if($this->perfilid!=1){ ?>
    .soloadministradores{
      display: none;
    }
  <?php } ?>
</style>
<!-- START CONTENT --> 
        <section id="content" width="100%">
          

          <!--start container-->
          <div class="container" width="100%">
            <div class="section">
               <h4 class="caption"><?php echo $label; ?></h4>
              <div class="row">
                <input id="base_url" type="hidden" name="base_url" value="<?php echo base_url(); ?>">
                <input id="tipoVista" type="hidden" name="tipoVista" value="<?php echo $tipoVista; ?>">
                <!-- FORMULARIO -->
               

                  <div class="col s12 m12 24">

                    <div class="card-panel">

                      <div class="row">
                         <form class="form" id="consumibles_form" method="post">

                          <?php 
                            if(isset($consumible)) 
                            {
                          ?>
                            <input id="idConsumible" name="idConsumible" type="hidden" value="<?php echo $idConsumible; ?>">
                            <input id="idConsumiblesCostos" name="idConsumiblesCostos" type="hidden" value="<?php echo $idConsumiblesCostos; ?>">

                          <?php
                            }
                          ?>
            
                          <div class="row">
                            <div class="col s3">
                              <div class="input-field ">
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                  <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 230px; height: 210px; border-radius: 12px;">
                                    <?php if ($foto!=''){ ?>
                                      <input type="file" id="foto" name="foto" class="dropify" data-default-file="<?php echo base_url(); ?>uploads/consumibles/<?php echo $foto; ?>">
                                      <a class="btn btn-info" href="<?php echo base_url(); ?>uploads/consumibles/<?php echo $foto; ?>" target="_blank">visualizar</a>
                                    <?php }else{?>  
                                      <input type="file" id="foto" name="foto" class="dropify">
                                    <?php }?>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="col s9">
                             <div class="row">
                               <div class="input-field col s12">
                                <i class="material-icons prefix" title="Modelo">local_library</i>
                                <label>Modelo</label>
                                <input id="modelo" name="modelo" type="text" value="<?php echo $modelo; ?>" >
                               </div>
                             </div> 
                             <div class="row">
                               <div class="input-field col s6">
                                <i class="material-icons prefix" title="No. de parte">format_list_numbered</i>
                                <label>No. de parte</label>
                                <input id="parte" name="parte" type="text" value="<?php echo $parte; ?>" >
                               </div>
                               <div class="input-field col s6" style="display: none">
                                <i class="material-icons prefix" title="Stock">chrome_reader_mode</i>
                                <input id="stock" name="stock" type="number" placeholder="Stock" value="<?php echo $stock; ?>" >
                               </div>
                             </div> 
                             <div class="row">
                               <div class="input-field col s12">
                                <i class="material-icons prefix">question_answer</i>
                                <textarea id="observaciones" name="observaciones" class="materialize-textarea"><?php echo $observaciones; ?></textarea>
                                <label for="message">Observaciones</label>
                              </div>
                             </div> 
                             <div class="row">
                               <div class="input-field col s6">
                                <i class="material-icons prefix" title="rendimiento unidad imagen">assignment</i>
                                <label>Rendimiento unidad imagen</label>
                                <input id="parte" name="rendimiento_unidad_imagen" type="text" value="<?php echo $rendimiento_unidad_imagen; ?>" >
                               </div>
                               <div class="input-field col s6">
                                <i class="material-icons prefix" title="rendimiento toner">assignment</i>
                                <label>Rendimiento toner</label>
                                <input id="rendimiento_toner" name="rendimiento_toner" type="text" value="<?php echo $rendimiento_toner; ?>" >
                               </div>
                             </div>
                             <div class="row">
                                <div class="col s12">
                                  <p>Tipo de consumible</p>
                                </div>
                                
                                <div class="col s3">
                                  <input class="with-gap" name="tipo" type="radio" value="1" id="tipo1" <?php if($tipo==1){ echo'checked';} ?> />
                                  <label for="tipo1">Black</label>
                                </div>
                                <div class="col s3">
                                  <input class="with-gap" name="tipo" type="radio" value="2" id="tipo2" <?php if($tipo==2){ echo'checked';} ?> />
                                  <label for="tipo2">C</label>
                                </div>
                                <div class="col s3">
                                  <input class="with-gap" name="tipo" type="radio" value="3" id="tipo3" <?php if($tipo==3){ echo'checked';} ?> />
                                  <label for="tipo3">M</label>
                                </div>
                                <div class="col s3">
                                  <input class="with-gap" name="tipo" type="radio" value="4" id="tipo4" <?php if($tipo==4){ echo'checked';} ?> />
                                  <label for="tipo4">Y</label>
                                </div>
                                <div class="col s3">
                                  <input class="with-gap" name="tipo" type="radio" value="5" id="tipo5" <?php if($tipo==5){ echo'checked';} ?> />
                                  <label for="tipo5">MK</label>
                                </div>
                                <div class="col s3">
                                  <input class="with-gap" name="tipo" type="radio" value="6" id="tipo6" <?php if($tipo==6){ echo'checked';} ?> />
                                  <label for="tipo6">MK monocromatico</label>
                                </div>
                                <div class="col s3">
                                  <input class="with-gap" name="tipo" type="radio" value="7" id="tipo7" <?php if($tipo==7){ echo'checked';} ?> />
                                  <label for="tipo7">MK color</label>
                                </div>
                                <div class="col s3">
                                  <input class="with-gap" name="tipo" type="radio" value="0" id="tipo0" <?php if($tipo==0){ echo'checked';} ?> />
                                  <label for="tipo0">Ninguno</label>
                                </div>
                                
                                
                               </div>
                             </div> 
                              

                            </div> 
                            
                             
                             <br>
                             
                             <br>
                             

                          </div> 
                          <hr class="divider">
                          <div class="col s4"><h4 class="caption">Compatibilidad de impresoras </h4></div>
                          <div class="col s6"><h4 class="caption">Tabla de Equipos</h4></div>
                          </hr>
                            <div class="row">
                              
                              <div class="col s4">

                                <?php  
                                $i=0;
                                $size=1;
                                do{
                                ?>
                                <div  class="border" id="datos1">
                                  <div class="input-field col s10">
                                  
                                    <select id="equipos" name="equipos" onchange="validarequipo()" class="browser-default chosen-select">
                                       <option value="" disabled selected>Selecciona una opción</option>
                                        <?php foreach($equipo as $item){?> 
                                            <option value="<?php echo $item->id;?>" ><?php echo $item->modelo;?></option>
                                        <?php } ?>
                                    </select>
                                 <!-- 
                                 <input type="text" name=""> 
                                  <label for="equipos">Equipo</label>
                                  -->
                                  </div>
                                  <div class="input-field col s2 div_btn">
                                  <?php if($i==0){ ?>
                                  <a class="btn btn-floating waves-effect waves-light cyan btn_agregar"> <i class="material-icons">add</i></a>
                                  <?php } else {?>
                                  <a class="btn btn-floating waves-effect waves-light red btn_eliminar"><i class="material-icons">delete_forever</i></a>
                                  <?php }?>
                                  </div>

                                </div>
                                <?php 
                                $i++;
                                }while($i<$size);
                                ?>
                                
                              </div> 
                              <div class="col s6">
                                <?php 
                                if($tipoVista!=1)
                                {
                                ?> 
                                
                          
                               
                                          <table class="table table-striped table-condensed table-hover table-responsive" id="tabla_has_consumible" name="tabla_has_consumible">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Equipo</th>
                                                    <th>idConsimibles</th>
                                                    <th></th>
                                                </tr>
                                            </thead>

                                            <tbody >
                                            </tbody>
                                            
                                          </table> 
                               
                                <?php } ?>
                              </div>  

                            </div>


                          <div class="row">
                            <div class="input-field col s12">
                              <button class="btn cyan waves-effect waves-light right" type="submit">Guardar
                                <i class="material-icons right">send</i>
                              </button>
                            </div>
                          </div>
                        </form>
                        
                      </div>

                    </div>

                  </div>
                
              </div>
            </div>
          </div>
        </section>