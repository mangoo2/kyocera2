<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Perfiles extends CI_Controller {
    public function __construct()     {
        parent::__construct();

        $this->load->model('ModeloCatalogos');
        $this->load->model('Modeloaccesorios');
        $this->load->helper('url');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->submenu=65;
        if($this->session->userdata('logeado')==true){
            $this->perfilid =$this->session->userdata('perfilid');
            $this->idpersonal = $this->session->userdata('idpersonal');
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,$this->submenu);// 52 es el id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
        }else{
            redirect('login'); 
        }
    }

    function index(){
        $data['MenusubId']=$this->submenu;
        $data['resultmenu']=$this->ModeloCatalogos->permisosview();
        $this->load->view('header');
        $this->load->view('main');
        $this->load->view('configuraciones/perfiles',$data);
        $this->load->view('footer');
        $this->load->view('configuraciones/perfilesjs');
    }
    function viewperfiles(){
        $result= $this->ModeloCatalogos->getselectwheren('perfiles',array('activo'=>1));

        echo json_encode($result->result());
    }
    function deletep(){
        $idf = $this->input->post('idf');
        $this->ModeloCatalogos->updateCatalogo('perfiles',array('activo'=>0),array('perfilId'=>$idf));
    }
    function inserupdateperfil(){
        $params = $this->input->post();
        if($params['id']>0){
            $this->ModeloCatalogos->updateCatalogo('perfiles',array('nombre'=>$params['nom']),array('perfilId'=>$params['id']));
        }else{
            $this->ModeloCatalogos->Insert('perfiles',array('nombre'=>$params['nom']));
        }
    }
    function viewperfilespermisos(){
        $perfilid = $this->input->post('perfilid');
        $result= $this->ModeloCatalogos->permisosviewperfil($perfilid);

        echo json_encode($result->result());
    }
    function insertpermiso(){
        $params = $this->input->post();
        $perfilid=$params['perfilid'];
        $submenu=$params['submenu'];
        
        $result= $this->ModeloCatalogos->getselectwheren('perfiles_detalles',array('perfilId'=>$perfilid,'MenusubId'=>$submenu));
        $rownum=$result->num_rows();
        if($rownum==0){
            $this->ModeloCatalogos->Insert('perfiles_detalles',array('perfilId'=>$perfilid,'MenusubId'=>$submenu));
        }
        echo $rownum;
    }
    function deletepermiso(){
        $params = $this->input->post();
        $perfilid=$params['perfilid'];
        $submenu=$params['submenu'];
        $this->ModeloCatalogos->Insert('perfiles_detalles',array('perfilId'=>$perfilid,'MenusubId'=>$submenu));
    }
    function deletesubm(){
        $params = $this->input->post();
        $idps=$params['idps'];
        $this->ModeloCatalogos->getdeletewheren('perfiles_detalles',array('Perfil_detalleId'=>$idps));
    }

    


    
}
?>
