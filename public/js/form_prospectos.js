// Saber si es solo Visualización
var tipoVista = $("#tipoVista").val();
var base_url = $('#base_url').val();
var addtr=0;
$(document).ready(function(){
    $('.chosen-select').chosen({width: "95%"});

     // En caso de que sea solo visualizar, deshabilita todo
    /*
        if(tipoVista == 2)
        {   var prospectoID = $('#idProspecto').val();
            loadTabla_Tel_local(prospectoID);
            loadTabla_Tel_celular(prospectoID);
            loadTabla_Tel_persona_contacto(prospectoID);
        }

        if(tipoVista == 3)
        {   var prospectoID = $('#idProspecto').val();
            loadTabla_Tel_local(prospectoID);
            loadTabla_Tel_celular(prospectoID);
            loadTabla_Tel_persona_contacto(prospectoID);
            $("input").prop('disabled', true);
            $("textarea").prop('disabled', true);
            $("select").prop('disabled', true);
            $("checkbox").prop('disabled', true);
            $("button").prop('disabled', true);
            $(".btn_agregar_tel").attr('style', 'pointer-events: none;');
            $(".btn_agregar_cel").attr('style', 'pointer-events: none;');
            $(".btn_agregar_contacto").attr('style', 'pointer-events: none;');
            $(".elimina").attr('style', 'pointer-events: none;');
            
        }
    */
    $('.submitform').click(function(event) {
        funtioncatgando();
        $(".submitform").prop("disabled", true);
        $("#prospectos_from").submit();
        setTimeout(function(){ 
            $(".submitform").prop("disabled", false);
        }, 3000);
    });
   
    var formulario_prospectos = $('#prospectos_from');
     formulario_prospectos.validate({
            rules: 
            {
                empresa: {
                    required: true
                },
                /*
                persona_contacto: {
                    required: true
                },
                */
                puesto_contacto: {
                    required: true
                },
                email: {
                    required: true
                },
               
                municipio: {
                    required: true
                },
                giro_empresa: {
                    required: true
                },
                direccion: {
                    required: true
                }
            },
            // Para mensajes personalizados
            messages: 
            {
                empresa:{
                    required: "Ingrese una empresa"
                },
                persona_contacto:{
                    required: "Ingrese una persona contacto"
                },
                puesto_contacto:{
                    required: "Ingrese un puesto de contacto"
                },
                email:{
                    required: "Ingrese un correo"
                },
                estado:{
                    required: "Seleccione un estado"
                },
                municipio:{
                    required: "Ingrese un municipio"
                },
                giro_empresa:{
                    required: "Ingrese el giro de la empresa"
                },
                direccion:{
                    required: "Ingrese una dirección"
                }
            },
        errorElement : 'div',
            // Si están correctos los campos, procesamos el formulario mediante Ajax
        submitHandler: function (form) {
            var DATA3  = [];
                    var TABLA3   = $("#tabledatoscontacto tbody > tr");
                        TABLA3.each(function(){         
                              items = {};
                              items['datosId'] = $(this).find("input[id*='datosId']").val();
                              items['atencionpara'] = $(this).find("input[id*='atencionpara']").val();
                              
                              var puesto = $(this).find("input[id*='puesto']").val();
                              puesto=puesto.replace('&', '---');
                              items['puesto'] = puesto;

                              var email = $(this).find("input[id*='email']").val();
                                  email=email.replace('&', '---');
                              items['email'] = email;
                              items['telefono'] = $(this).find("input[id*='telefono']").val();
                              items['celular'] = $(this).find("input[id*='celular']").val();
                             DATA3.push(items);
                        });
            persona_contacto = JSON.stringify(DATA3);
            var DATA5  = [];
                    var TABLA5   = $(".tabla_dir tbody > tr");
                        TABLA5.each(function(){         
                             item = {};
                             item ["id"] = $(this).find("input[id*='idclientedirecc']").val();
                             item ["direccion"] = $(this).find("input[id*='direccion']").val();
                             DATA5.push(item);
                        });
            datos_direccion = JSON.stringify(DATA5);
            var aaa_x = 0; 
            if($('#aaa').is(':checked')){
                aaa_x=1;   
            }else{
                aaa_x=0;
            }
            var datos = formulario_prospectos.serialize()+'&aaa='+aaa_x+'&persona_contacto='+persona_contacto+'&datos_direccion='+datos_direccion;
            $.ajax({
                type:'POST',
                url: base_url+'index.php/prospectos/insertaActualizaProspectos',
                data: datos,
                success:function(data)
                {
                    console.log(data);
                    // Si existe el usuario, mostramos el mensaje de entrada y redirigimos
                    if(data>0)
                    {
                        swal({ title: "Éxito",
                            text: "Se insertó/actualizó el prospecto",
                            type: 'success',
                            showCancelButton: false,
                            allowOutsideClick: false,
                        });
                        
                        setTimeout(function(){ 
                                window.location.href = base_url+"index.php/Prospectos"; 
                            }, 2000);
                    
                    }
                    // En caso contrario, se notifica
                    else
                    {
                        swal({ title: "Error",
                            text: "Los datos del prospecto son erroneos",
                            type: 'error',
                            showCancelButton: false,
                            allowOutsideClick: false,
                        });
                        $('body').loadingModal('destroy'); 
                    }
                }
            });
            
            
        }
    
     });
     


     $(".btn_agregar_tel").click(function(){
      var contador = $('.border_tel').length; 
      var contadoraux = contador+1; 
      var $template = $("#tel_dato1"),
      $clone = $template
     .clone()
     .show()
     .removeAttr('id')
     .attr('id',"tel_dato"+contadoraux)
     .insertAfter($("#tel_dato"+contador));

     $clone.find($('#categoria'));
     $clone.find(".div_btn_tel").html('<a class="btn btn-floating waves-effect waves-light red btn_eliminar_tel"><i class="material-icons">delete_forever</i></a>');
     $clone.find("select").val("");
     $clone.find("input").val("");
     eliminar_tel();
     $('select').material_select();

    });

    $(".btn_agregar_cel").click(function(){
      var contador = $('.border_cel').length; 
      var contadoraux = contador+1; 
      var $template = $("#cel_datos1"),
      $clone = $template
     .clone()
     .show()
     .removeAttr('id')
     .attr('id',"cel_datos"+contadoraux)
     .insertAfter($("#cel_datos"+contador));

     $clone.find($('#categoria'));
     $clone.find(".div_btn_cel").html('<a class="btn btn-floating waves-effect waves-light red btn_eliminar_cel"><i class="material-icons">delete_forever</i></a>');
     $clone.find("select").val("");
     $clone.find("input").val("");
     eliminar_cel();
     $('select').material_select();

    });

    $(".btn_agregar_contacto").click(function(){
      var contador = $('.border_contacto').length; 
      var contadoraux = contador+1; 
      var $template = $("#contacto_datos1"),
      $clone = $template
     .clone()
     .show()
     .removeAttr('id')
     .attr('id',"contacto_datos"+contadoraux)
     .insertAfter($("#contacto_datos"+contador));

     $clone.find($('#categoria'));
     $clone.find(".div_btn_contacto").html('<a class="btn btn-floating waves-effect waves-light red btn_eliminar_contacto"><i class="material-icons">delete_forever</i></a>');
     $clone.find("input").val("");
     eliminar_contacto();
    });
          // Listener para Eliminar 
    /*
    $('#tabla_tel_local tbody').on('click', 'a.elimina', function () 
    {
        var tr = $(this).closest('tr');
        var row = table1.row(tr);
        var data = row.data();
        Eliminar_Tabla_Tel_local(data.id);
    });

    $('#tabla_tel_celular tbody').on('click', 'a.elimina', function () 
    {
        var tr = $(this).closest('tr');
        var row = table2.row(tr);
        var data = row.data();
        Eliminar_Tabla_Tel_celular(data.id);
    });
    */
    /*
    $('#tabla_persona_contacto tbody').on('click', 'a.elimina', function () 
    {
        var tr = $(this).closest('tr');
        var row = table3.row(tr);
        var data = row.data();
        
        $.confirm({
            boxWidth: '30%',
            useBootstrap: false,
            icon: 'fa fa-warning',
            title: 'Atención!',
            content: '¿Está seguro de Eliminar este Contacto? El cambio será irreversible',
            type: 'red',
            typeAnimated: true,
            buttons: 
            {
                confirmar: function () 
                {
                    Eliminar_Tabla_Contacto(data.id);
                    tr.remove();
                },
                cancelar: function () 
                {
                    
                }
            }
        });
    });
    */

});
function eliminar_tel(){
    $(".btn_eliminar_tel").click(function(){
                    var $row = $(this).parents('.border_tel');
   
                    // Remove element containing the option
                    $row.remove();

    });
}

function eliminar_cel(){
    $(".btn_eliminar_cel").click(function(){
                    var $row = $(this).parents('.border_cel');
   
                    // Remove element containing the option
                    $row.remove();

    });
}

function eliminar_contacto(){
    $(".btn_eliminar_contacto").click(function(){
                    var $row = $(this).parents('.border_contacto');
   
                    // Remove element containing the option
                    $row.remove();

    });
}

/*
function Eliminar_Tabla_Tel_local(idTel)
{
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de Eliminar este telefono local? El cambio será irreversible',
        type: 'red',
        typeAnimated: true,
        buttons: 
        {
            confirmar: function () 
            {
                $.ajax({
                    url: base_url+"index.php/prospectos/eliminar_tel_local/"+idTel,
                    success: function (response) 
                    {

                        if(response==1)
                        {
                            $.alert({
                                boxWidth: '30%',
                                useBootstrap: false,
                                title: 'Éxito!',
                                content: 'Teléfono Eliminado!'});
                            setTimeout(function(){ 
                            location.reload();
                            }, 2000);
                        }
                        else
                        {
                            $.alert({
                                boxWidth: '30%',
                                useBootstrap: false,
                                title: 'Error!',
                                content: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema'}); 
                            setTimeout(function(){ 
                            location.reload();
                            }, 2000);
                        }
                    },
                    error: function(response)
                    {
                        $.alert({
                                boxWidth: '30%',
                                useBootstrap: false,
                                title: 'Error!',
                                content: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema'});   
                            setTimeout(function(){ 
                            location.reload();
                            }, 2000);
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}

function Eliminar_Tabla_Tel_celular(idTel)
{
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de Eliminar este telefono celular? El cambio será irreversible',
        type: 'red',
        typeAnimated: true,
        buttons: 
        {
            confirmar: function () 
            {
                $.ajax({
                    url: base_url+"index.php/prospectos/eliminar_tel_celular/"+idTel,
                    success: function (response) 
                    {

                        if(response==1)
                        {
                            $.alert({
                                boxWidth: '30%',
                                useBootstrap: false,
                                title: 'Éxito!',
                                content: 'Teléfono Eliminado!'});
                            setTimeout(function(){ 
                            location.reload();
                            }, 2000);
                        }
                        else
                        {
                            $.alert({
                                boxWidth: '30%',
                                useBootstrap: false,
                                title: 'Error!',
                                content: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema'}); 
                            setTimeout(function(){ 
                            location.reload();
                            }, 2000);
                        }
                    },
                    error: function(response)
                    {
                        $.alert({
                                boxWidth: '30%',
                                useBootstrap: false,
                                title: 'Error!',
                                content: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema'});   
                            setTimeout(function(){ 
                            location.reload();
                            }, 2000);
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}


function Eliminar_Tabla_Contacto(idTel)
{
    $.ajax({
        url: base_url+"index.php/prospectos/eliminar_persona_contacto/"+idTel,
        success: function (response) 
        {
            if(response==1)
            {
                $.alert({
                    boxWidth: '30%',
                    useBootstrap: false,
                    title: 'Éxito!',
                    content: 'Contacto Eliminado!'});
                
            }
            else
            {
                $.alert({
                    boxWidth: '30%',
                    useBootstrap: false,
                    title: 'Error!',
                    content: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema'}); 
                
            }
        },
        error: function(response)
        {
            $.alert({
                    boxWidth: '30%',
                    useBootstrap: false,
                    title: 'Error!',
                    content: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema'});   
                setTimeout(function(){ 
                location.reload();
                }, 2000);
        }
    });
}
function loadTabla_Tel_local(id) 
{   
    table1 = $('#tabla_tel_local').DataTable({
            "ajax":{ "url": base_url+"index.php/Prospectos/getListadoTel_local/"+id,
        },
        "columns": [
          {"data": "id"},
          {"data": "tel_local"},
          {"data": "idCliente", visible: false},
          {"data":null,title:"Acciones",orderable:false,class:"centrado_text",
                render:function(data,type,row){
                    $('.tooltipped').tooltip();
                    if(tipoVista == 3){
                        var html='<ul>\
                                    <li>\
                                      <a class="btn-floating red" data-delay="50" >\
                                        <i class="material-icons">delete_forever</i>\
                                      </a>\
                                    </li>\
                                  </ul>';
                        return html;
                        }else{
                        var html='<ul>\
                                    <li>\
                                      <a class="btn-floating red tooltipped elimina" data-position="top" data-delay="50" data-tooltip="Eliminar">\
                                        <i class="material-icons">delete_forever</i>\
                                      </a>\
                                    </li>\
                                  </ul>';
                        return html;  
                        }
                }
            }
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[25, 50, 100], [25, 50, 100]],
        "dom": 'Bfrtpl',
        // Muestra el botón de Excel para importar la tabla
        "buttons": [
            
        ],
        // Cambiamos lo principal a Español
        "language": {
            "lengthMenu": "Desplegando _MENU_ elementos por página",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
              }
        }
    });
}

function loadTabla_Tel_celular(id) 
{   
    table2 = $('#tabla_tel_celular').DataTable({
            "ajax":{ "url": base_url+"index.php/Prospectos/getListadoTel_celular/"+id,
        },
        "columns": [
          {"data": "id"},
          {"data": "celular"},
          {"data": "idCliente", visible: false},
          {"data":null,title:"Acciones",orderable:false,class:"centrado_text",
                render:function(data,type,row){
                    $('.tooltipped').tooltip();
                    if(tipoVista == 3){
                        var html='<ul>\
                                <li>\
                                  <a class="btn-floating red" data-delay="50">\
                                    <i class="material-icons">delete_forever</i>\
                                  </a>\
                                </li>\
                              </ul>';
                        return html;
                        }else{
                        var html='<ul>\
                                    <li>\
                                      <a class="btn-floating red tooltipped elimina" data-position="top" data-delay="50" data-tooltip="Eliminar">\
                                        <i class="material-icons">delete_forever</i>\
                                      </a>\
                                    </li>\
                                  </ul>';
                        return html;   
                        }
                }
            }
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[25, 50, 100], [25, 50, 100]],
        "dom": 'Bfrtpl',
        // Muestra el botón de Excel para importar la tabla
        "buttons": [
           
        ],
        // Cambiamos lo principal a Español
        "language": {
            "lengthMenu": "Desplegando _MENU_ elementos por página",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
              }
        }
    });
}
function loadTabla_Tel_persona_contacto(id) 
{   
    table3 = $('#tabla_persona_contacto').DataTable({
            "ajax":{ "url": base_url+"index.php/Prospectos/getListadoPersona_contacto/"+id,
        },
        "columns": [
          {"data": "id"},
          {"data": "persona_contacto"},
          {"data": "idCliente", visible: false},
          {"data":null,title:"Acciones",orderable:false,class:"centrado_text",
                render:function(data,type,row){
                    $('.tooltipped').tooltip();
                    if(tipoVista == 3){
                        var html='<ul>\
                                <li>\
                                  <a class="btn-floating red" data-delay="50">\
                                    <i class="material-icons">delete_forever</i>\
                                  </a>\
                                </li>\
                              </ul>';
                        return html;
                        }else{
                        var html='<ul>\
                                    <li>\
                                      <a class="btn-floating red tooltipped elimina" data-position="top" data-delay="50" data-tooltip="Eliminar">\
                                        <i class="material-icons">delete_forever</i>\
                                      </a>\
                                    </li>\
                                  </ul>';
                        return html;    
                        }
                }
            }
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[25, 50, 100], [25, 50, 100]],
        "dom": 'Bfrtpl',
        // Muestra el botón de Excel para importar la tabla
        "buttons": [
           
        ],
        // Cambiamos lo principal a Español
        "language": {
            "lengthMenu": "Desplegando _MENU_ elementos por página",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
              }
        }
    });
}
*/
function add_address(){ 
    var dirrecc = $('#direcc').val();
    if(dirrecc!= ""){
      var html_text = '<tr class="addtr_'+addtr+'">\
                          <td><input id="idclientedirecc" type="hidden"  value="0" readonly></td>\
                          <td><input type="text" id="direccion" value="'+dirrecc+'"></td>\
                          <td><a class="btn-floating waves-effect waves-light red accent-2" onclick="deteletr('+addtr+')"><i class="material-icons">clear</i></a></td>\
                          </tr>';
      $('.tbody_direccion').append(html_text);   
      addtr++; 
      $('#direcc').val('');
    }else{
      swal({   
        title: "Atención",
        text: "Tienes que ingresar una dirección",
        timer: 2000,
        showConfirmButton: false
      });
    }
}
function deteletr(id){
  $('.addtr_'+id).remove();
}
function delete_direcc(id)
{
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de eliminar esta dirreción',
        type: 'red',
        typeAnimated: true,
        buttons: 
        {
            confirmar: function () 
            {
                $.ajax({
                    url: base_url+"index.php/clientes/delete_direccion/"+id,
                    success: function (response) 
                    {
                            $.alert({
                                boxWidth: '30%',
                                useBootstrap: false,
                                title: 'Éxito!',
                                content: 'Dirección Eliminada!'});
                            setTimeout(function(){ 
                            location.reload();
                            }, 2000);

                    },
                    error: function(response)
                    {
                        $.alert({
                                boxWidth: '30%',
                                useBootstrap: false,
                                title: 'Error!',
                                content: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema'});   
                            setTimeout(function(){ 
                            location.reload();
                            }, 2000);
                    }
                });
            },
            cancelar: function () 
            {
                
            }
        }
    });
}
function agregar_pc(){
  var atencionpara = $('#atencionpara').val();
  var puesto_contacto = $('#puesto_contacto').val();
  var email = $('#email').val();
  var tel_local = $('#tel_local').val();
  var celular = $('#celular').val();
  datoscontactoadd(0,atencionpara,puesto_contacto,email,tel_local,celular);
  $('.inputdatoscontacto').val('');
}
var rowpc=0;
function datoscontactoadd(id,atencionpara,puesto_contacto,email,tel_local,celular){
  var addtrpc =  '<tr class="rowpc_'+rowpc+'_'+id+'">\
                    <td>\
                      <input type="hidden" id="datosId" value="'+id+'" readonly>\
                      <input type="hidden" id="atencionpara" value="'+atencionpara+'" readonly>\
                      '+atencionpara+'\
                    </td>\
                    <td>\
                      <input type="hidden" id="puesto" value="'+puesto_contacto+'" readonly>\
                      '+puesto_contacto+'\
                    </td>\
                    <td>\
                      <input type="hidden" id="email" value="'+email+'" readonly>\
                      '+email+'\
                    </td>\
                    <td><input type="hidden" id="telefono" value="'+tel_local+'" readonly>\
                      '+tel_local+'\
                    </td>\
                    <td><input type="hidden" id="celular" value="'+celular+'" readonly>\
                      '+celular+'\
                    </td>\
                    <td>\
                      <a class="btn-floating red tooltipped" onclick="Eliminar_pc('+id+','+rowpc+')" \
                        data-position="top" data-delay="50" data-tooltip="Eliminar">\
                        <i class="material-icons">delete_forever</i>\
                      </a>\
                    </td>\
                </tr>';
  $('.tabledatoscontactotb').append(addtrpc);
  rowpc++;
}
function Eliminar_pc(id,row){
  if (id==0) {
    $('.rowpc_'+row+'_'+id).remove();
  }else{
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de Eliminar el contacto? El cambio será irreversible',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    url: base_url+"index.php/clientes/eliminar_persona_contacto/"+id,
                    success: function (response){

                        if(response==1){
                            $.alert({
                                boxWidth: '30%',
                                useBootstrap: false,
                                title: 'Éxito!',
                                content: 'Contacto Eliminado!'});
                                $('.rowpc_'+row+'_'+id).remove();
                        }else{
                            $.alert({
                                boxWidth: '30%',
                                useBootstrap: false,
                                title: 'Error!',
                                content: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema'}); 
                            setTimeout(function(){ 
                            location.reload();
                            }, 2000);
                        }
                    },
                    error: function(response){
                        $.alert({
                                boxWidth: '30%',
                                useBootstrap: false,
                                title: 'Error!',
                                content: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema'});   
                            setTimeout(function(){ 
                            location.reload();
                            }, 2000);
                    }
                });
            },
            cancelar: function (){
                
            }
        }
    });
  }
}
function verificarexis(){
    var idCliente=$('#idCliente').val()==undefined?0:$('#idCliente').val();
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Clientes/existecliente",
        data: {
          nombre: $('#empresa').val(),
          idCliente:idCliente,
        },
        success: function (response){
          var array = $.parseJSON(response);
          if(array.length>0){
            $('#modalclientesexit').modal({
                    dismissible: false
                });
            $('#modalclientesexit').modal('open');
            $.each(array, function(index, item) {
                var html ='<tr>\
                                <td>\
                                    '+item.empresa+'\
                                </td>\
                                <td>\
                                    <a class="btn-floating green tooltipped edit" \
                                    data-position="top" \
                                    data-delay="50" \
                                    data-tooltip="Editar" \
                                    href="'+base_url+'Clientes/edicion/'+item.id+'" \
                                    >\
                                      <i class="material-icons">mode_edit</i> \
                                  </a>\
                                </td>\
                            </tr>';
                $('.listadoclientes').append(html);
            });
          }
          
          
        },
        error: function(response){
            toastr["error"]('Algo salió mal, intente de nuevo o contacte al administrador del sistema', "tención!");
        }
    });
}
function funtioncatgando(){
    $('body').loadingModal({text: 'Procesando...'});
    $('body').loadingModal('animation', 'circle');
        /*
     $('body').loadingModal('animation', 'rotatingPlane').loadingModal('backgroundColor', 'red'); 
                 $('body').loadingModal('animation', 'wave'); 
                 $('body').loadingModal('animation', 'wanderingCubes').loadingModal('backgroundColor', 'green'); 
                 $('body').loadingModal('animation', 'spinner'); 
                 $('body').loadingModal('animation', 'chasingDots').loadingModal('backgroundColor', 'blue'); 
                 $('body').loadingModal('animation', 'threeBounce'); 
                 $('body').loadingModal('animation', 'circle').loadingModal('backgroundColor', 'black'); 
                 $('body').loadingModal('animation', 'cubeGrid'); 
                 $('body').loadingModal('animation', 'fadingCircle').loadingModal('backgroundColor', 'gray'); 
                 $('body').loadingModal('animation', 'foldingCube'); return delay
                 $('body').loadingModal('color', 'black').loadingModal('text', 'Done :-)').loadingModal('backgroundColor', 'yellow');  
                 $('body').loadingModal('hide'); 
                 $('body').loadingModal('destroy');
    */
}