<style type="text/css">
  .chosen-container{
    margin: 0px;
  }
  .datostableinput2{
    border-bottom:0px !important;
  }
</style>
<!-- START CONTENT -->
<section id="content">
  <!--start container-->
  <div class="container">
    <div class="section">   
      <div>
          <h4 class="header">Agregar Consumibles</h4>
          
          <div class="row">
            <div class="col s12">
              <ul class="collapsible collapsible-accordion" data-collapsible="accordion">
                
                <li class="active">
                  <div class="collapsible-header active">
                    <i class="material-icons">color_lens</i> Consumibles</div>
                  <div class="collapsible-body">
                    <div class="row">
                      
                      <div class="input-field col s3">
                        <select id="bodega" name="bodega" class="browser-default chosen-select">
                          <?php foreach ($bodegas as $item) { ?>
                              <option value="<?php echo $item->bodegaId; ?>"><?php echo $item->bodega; ?></option>
                          <?php } ?> 
                        </select>
                        <label for="bodega" class="active">Bodega</label>
                      </div>
                      <div class="input-field col s3">
                        <select id="consumible" name="consumible" class="browser-default chosen-select">
                          <?php foreach ($consumibles->result() as $item) { ?>
                              <option value="<?php echo $item->id; ?>"><?php echo $item->modelo.' ('.$item->parte.')'; ?></option>
                          <?php } ?> 
                        </select>
                        <label for="consumible" class="active">Consumible</label>
                      </div>
                      <div class="input-field col s3">
                        <input placeholder="Cantidad" id="cantidad4" type="number" class="validate">
                        <label for="cantidad4">Cantidad</label>
                      </div>
                      <div class="col s3">
                        <button class="btn waves-effect waves-light addbtnconsumible" name="action">Agregar
                          <i class="material-icons right">system_update_alt</i>
                        </button>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col s12">
                        <table id="tabla_sconsumible" class="striped responsive-table display" cellspacing="0">
                          <thead>
                            <tr>
                              <th>Bodegas</th>
                              <th>Consumible</th>
                              <th>Cantidad</th>
                              <th></th>
                            </tr>
                          </thead>
                          <tbody class="tabla_addsconsumible">
                          </tbody>
                        </table>
                        </div>
                    </div> 
                  </div>
                </li>
                <li  style="display: none;">
                  <div class="collapsible-header active">
                    <i class="material-icons">color_lens</i> Refacciones</div>
                  <div class="collapsible-body">
                    <div class="row">
                      
                      <div class="input-field col s3">
                        <select id="bodegar" name="bodegar" class="browser-default chosen-select">
                          <?php foreach ($bodegas as $item) { ?>
                              <option value="<?php echo $item->bodegaId; ?>"><?php echo $item->bodega; ?></option>
                          <?php } ?> 
                        </select>
                        <label for="bodegar" class="active">Bodega</label>
                      </div>
                      <div class="input-field col s3">
                        <select id="refacciones" name="refacciones" class="browser-default chosen-select">
                          <?php foreach ($refacciones->result() as $item) { ?>
                              <option value="<?php echo $item->id; ?>"><?php echo $item->nombre; ?></option>
                          <?php } ?> 
                        </select>
                        <label for="refacciones" class="active">Refaccion</label>
                      </div>
                      <div class="input-field col s3">
                        <input placeholder="Cantidadr" id="cantidadr" type="number" class="validate">
                        <label for="cantidadr">Cantidad</label>
                      </div>
                      <div class="col s3">
                        <button class="btn waves-effect waves-light addbtnregaccion" name="action">Agregar
                          <i class="material-icons right">system_update_alt</i>
                        </button>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col s12">
                        <table id="tabla_srefaccion" class="striped responsive-table display" cellspacing="0">
                          <thead>
                            <tr>
                              <th>Bodegas</th>
                              <th>Refaccion</th>
                              <th>Cantidad</th>
                              <th></th>
                            </tr>
                          </thead>
                          <tbody class="tabla_addsrefaccion">
                          </tbody>
                        </table>
                        </div>
                    </div> 
                  </div>
                </li>
                <li  style="display: none;">
                  <div class="collapsible-header active">
                    <i class="material-icons">color_lens</i> Accesorios</div>
                  <div class="collapsible-body">
                    <div class="row">
                      
                      <div class="input-field col s3">
                        <select id="bodegaa" name="bodegaa" class="browser-default chosen-select">
                          <?php foreach ($bodegas as $item) { ?>
                              <option value="<?php echo $item->bodegaId; ?>"><?php echo $item->bodega; ?></option>
                          <?php } ?> 
                        </select>
                        <label for="bodega" class="active">Bodega</label>
                      </div>
                      <div class="input-field col s3">
                        <select id="accesorios" name="accesorios" class="browser-default chosen-select">
                          <?php foreach ($accesorios->result() as $item) { ?>
                              <option value="<?php echo $item->id; ?>"><?php echo $item->nombre.' ('.$item->no_parte.')'; ?></option>
                          <?php } ?> 
                        </select>
                        <label for="accesorios" class="active">Accesorio</label>
                      </div>
                      <div class="input-field col s3">
                        <input placeholder="Cantidad" id="cantidada" type="number" class="validate">
                        <label for="cantidada">Cantidad</label>
                      </div>
                      <div class="col s3">
                        <button class="btn waves-effect waves-light addbtnaccesorio" name="action">Agregar
                          <i class="material-icons right">system_update_alt</i>
                        </button>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col s12">
                        <table id="tabla_saccesorio" class="striped responsive-table display" cellspacing="0">
                          <thead>
                            <tr>
                              <th>Bodegas</th>
                              <th>Accesorio</th>
                              <th>Cantidad</th>
                              <th></th>
                            </tr>
                          </thead>
                          <tbody class="tabla_addsaccesorio">
                          </tbody>
                        </table>
                        </div>
                    </div> 
                  </div>
                </li>
              </ul>
            </div>
          </div>
          <div class="row">
            <a class="btn cyan waves-effect waves-light btn right saveallseries">
                    <i class="material-icons right">save</i> Guardar</a>
          </div>
      </div>
    </div>
  </div> 
</section>

              