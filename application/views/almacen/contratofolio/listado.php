<style type="text/css">
  table td{
    font-size: 12px;
    padding: 6px 7px;
  }
</style>
<!-- START CONTENT -->
    <section id="content">
      <!--start container-->
        <div class="container">
          <div class="section">
          <div id="table-datatables">
            <br>
                <h4 class="header">Control de Folios</h4>
                <div class="row">
                  <div class="col s10">
                    <a href="<?php echo base_url(); ?>NewFolio"  class="btn waves-effect waves-light green darken-1">Nuevo Folio</a>
                  </div>
                  <div class="col s2" style="display:none;">
                    <select class="browser-default form-control-bmz" id="tecnico" >
                      <option value="">Todos</option>
                      <?php foreach ($resultstec as $item): ?>
                        <option><?php echo $item->tecnico; ?></option>
                      <?php endforeach ?>
                    </select>
                  </div>
                  <div class="col s2">
                    <button class="btn-bmz waves-effect waves-light facturar" type="button" onclick="modaletiquetas()">Etiquetas</button>
                  </div>
                </div>
                <div class="row">
                  <div class="col s2">
                    <label>Fecha inicial</label>
                    <input type="date" class="form-control-bmz" id="finicial" value="<?php echo $fechainicial_reg;?>">
                  </div>
                  <div class="col s2">
                    <label>Fecha final</label>
                    <input type="date" class="form-control-bmz" id="ffinal" >
                  </div>
                  <div class="col s2">
                    <button class="b-btn b-btn-primary" onclick="load()" style="margin-top: 20px;" title="Realizar consulta"><i class="fas fa-filter"></i> <span class="btn-filtrar"></span></button>
                  </div>
                <div class="row">
                  <input id="base_url" type="hidden" name="base_url" value="<?php echo base_url(); ?>">
                  <div class="col s12">
                    <table id="tabla_con" class="responsive-table display" cellspacing="0">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Modelo</th>
                          <th>Parte</th>
                          <th>Vendedor</th>
                          <th>Fecha</th>
                          <th>Cliente</th>
                          <th>Tecnico</th>
                          <th>Folio</th>
                          <th></th>
                          <th></th>
                          <th width="10%">Acciones</th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
          </div> 
          <div id="modalAcciones" class="modal" >
            <div class="modal-content">
              <div class="col s12 m12 l6">
                <div class="iframe_etiqueta"></div>
              </div>
              <input type="hidden" id="idcontratofolio" oninput="">
            </div>    
            <div class="modal-footer">
              <a class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
              <a class="btn waves-effect waves-light cyan" onclick="statusetiqueta();">Imprimir</a>
            </div>
          </div> 
        </div>
     </div>
    </section>
<div id="modalservicios" class="modal" >
  <div class="modal-content">
    <div class="col s12 m12 l6">
      <h3>Agregar a un servicio</h3>
      <div class="col s12">
        <select class="browser-default form-control-bmz" id="servicioadd">
          
        </select>
      </div>
    </div>
    <input type="hidden" id="idcf">
  </div>    
  <div class="modal-footer">
    <a class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
    <a class="btn waves-effect waves-light cyan" onclick="addserviciotoner();">Agregar</a>
  </div>
</div>


<div id="modalAccionesn" class="modal" >
  <div class="modal-content">
    <div class="col s12 m12 l6">
      <div class="iframe_etiquetan"></div>
    </div>
    <text type="hidden" id="idscontratofolio" ></text>
  </div>    
  <div class="modal-footer">
    <a class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
    <a class="btn waves-effect waves-light cyan" onclick="statusetiquetan();">Imprimir</a>
  </div>
</div>  