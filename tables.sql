-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 06-07-2021 a las 23:40:57
-- Versión del servidor: 10.4.10-MariaDB
-- Versión de PHP: 5.6.40




/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `kyocera_pro7`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rentas_historial`
--

DROP TABLE IF EXISTS `rentas_historial`;
CREATE TABLE IF NOT EXISTS `rentas_historial` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `rentas` int(11) NOT NULL,
  `reg` timestamp NOT NULL DEFAULT current_timestamp(),
  `activo` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rentas_historial_accesorios`
--

DROP TABLE IF EXISTS `rentas_historial_accesorios`;
CREATE TABLE IF NOT EXISTS `rentas_historial_accesorios` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `historialid` bigint(20) NOT NULL,
  `accesorio` int(11) NOT NULL,
  `activo` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rentas_historial_consumibles`
--

DROP TABLE IF EXISTS `rentas_historial_consumibles`;
CREATE TABLE IF NOT EXISTS `rentas_historial_consumibles` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `historialid` bigint(20) NOT NULL,
  `consumible` int(11) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rentas_historial_equipos`
--

DROP TABLE IF EXISTS `rentas_historial_equipos`;
CREATE TABLE IF NOT EXISTS `rentas_historial_equipos` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `historialid` bigint(20) NOT NULL,
  `equipo` int(11) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
