<?php
defined('BASEPATH') OR exit ('No direct script access allowed');

class Ventasd_model extends CI_Model {
    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
        $this->DB3 = $this->load->database('other3_db', TRUE);
    }
    /*
    public function insertarVenta($dato){
	    $this->db->insert('ventas',$dato);   
	    return $this->db->insert_id();
    }  
    public function insertarDetallesVentasEquipos($dato){
        $this->db->insert('ventas_has_detallesEquipos',$dato);   
        return $this->db->insert_id();
    }
    public  function detallesventas($id){
        $sql = "SELECT v.id, vd.modelo, vd.serie FROM ventas AS v
                INNER JOIN ventas_has_detallesEquipos AS vd ON vd.idVenta = v.id
                WHERE v.id =$id";
        $query = $this->DB3->query($sql);
        return $query->result();
    }
    function detallesventasconsumibles($id){
        $sql = "SELECT con.modelo,vdc.* 
                FROM ventas_has_detallesequipos_consumible as vdc 
                inner join consumibles as con on con.id=vdc.id_consumibles
                WHERE vdc.idventa=$id";
        $query = $this->DB3->query($sql);
        return $query;
    }
    function detallesaccesoriosv($id){
        $sql="SELECT acc.nombre,vda.serie 
        FROM `ventas_has_detallesEquipos_accesorios` as vda
        inner JOIN catalogo_accesorios as acc on acc.id=vda.id_accesorio
        WHERE vda.id_venta=".$id;
        $query = $this->DB3->query($sql);
        return $query;
    }
    function updateCatalogo($data,$idname,$id,$catalogo){
        $this->db->set($data);
        $this->db->where($idname, $id);/// Se puede ocupar un array para n condiciones
        $this->db->update($catalogo);
        return $id;
    }
    */
    // Controlador Ventasi
    /*
    public function getListaVentasIncompletasfactura($params){
            $tipo=$params['tipo'];
            $cliente=$params['cliente'];
            $status=$params['status'];
            if ($tipo==1) { //venta
                $columns = array( 
                    0=>'v.estatus',
                    1=>'v.id',
                    2=>'v.prefactura',
                    3=>'v.telefono',
                    4=>'v.correo',
                    5=>'cli.empresa',
                    6=>'per.nombre vendedor',  
                    7=>"'ventas' tabla",
                    8=>'v.fechaentrega',
                    9=>'v.e_entrega'

                );
                $columns2 = array( 
                    0=>'v.estatus',
                    1=>'v.id',
                    2=>'v.prefactura',
                    3=>'v.telefono',
                    4=>'v.correo',
                    5=>'cli.empresa',
                    6=>'per.nombre',
                    7=>'v.fechaentrega'                    
                );
                $select="";
                foreach ($columns as $c) {
                    $select.="$c, ";
                }
                $this->DB3->select($select);
                $this->DB3->from('ventas v ');
                $this->DB3->join('clientes cli', 'cli.id=v.idCliente');
                $this->DB3->join('personal per', 'per.personalId=v.id_personal','left');
                $where = array('v.prefactura'=>1);
                if ($cliente>0) {
                    $where['v.idCliente']=$cliente;
                }
                if ($status==1) {
                    $where['v.statuspago']=1;
                }elseif ($status==2) {
                    $where['v.statuspago']=0;
                }
                    $where['v.activo']=1;
                
                
                $this->DB3->where($where);
            }elseif ($tipo==2) { //renta
                $columns = array( 
                    0=>'v.estatus',
                    1=>'v.id',
                    2=>'v.prefactura',
                    3=>'v.telefono',
                    4=>'v.correo',
                    5=>'cli.empresa',
                    6=>'per.nombre vendedor',
                    7=>"'rentas' tabla",
                    8=>"'' fechaentrega",
                    9=>'0 e_entrega'                   
                );
                $columns2 = array( 
                    0=>'v.estatus',
                    1=>'v.id',
                    2=>'v.prefactura',
                    3=>'v.telefono',
                    4=>'v.correo',
                    5=>'cli.empresa',
                    6=>'per.nombre',                     
                );
                $select="";
                foreach ($columns as $c) {
                    $select.="$c, ";
                }
                $this->DB3->select($select);
                $this->DB3->from('rentas v ');
                $this->DB3->join('clientes cli', 'cli.id=v.idCliente');
                $this->DB3->join('personal per', 'per.personalId=v.id_personal','left');
                $where = array('v.prefactura'=>1);
                 if ($cliente>0) {
                    $where['v.idCliente']=$cliente;
                }
                if ($status==1) {
                    $where['v.statuspago']=1;
                }elseif ($status==2) {
                    $where['v.statuspago']=0;
                }
                $this->DB3->where($where);
            }elseif ($tipo==3) { //polizas
                $columns = array( 
                    0=>'v.estatus',
                    1=>'v.id',
                    2=>'v.prefactura',
                    3=>'v.telefono',
                    4=>'v.correo',
                    5=>'cli.empresa',
                    6=>'per.nombre vendedor',
                    7=>"'polizasCreadas' tabla", 
                    8=>"'' fechaentrega", 
                    9=>'0 e_entrega'                   
                );
                $columns2 = array( 
                    0=>'v.estatus',
                    1=>'v.id',
                    2=>'v.prefactura',
                    3=>'v.telefono',
                    4=>'v.correo',
                    5=>'cli.empresa',
                    6=>'per.nombre',                     
                );
                $select="";
                foreach ($columns as $c) {
                    $select.="$c, ";
                }
                $this->DB3->select($select);
                $this->DB3->from('polizasCreadas v ');
                $this->DB3->join('clientes cli', 'cli.id=v.idCliente');
                $this->DB3->join('personal per', 'per.personalId=v.id_personal','left');
                $where = array('v.prefactura'=>1,'v.activo'=>1);
                 if ($cliente>0) {
                    $where['v.idCliente']=$cliente;
                }
                if ($status==1) {
                    $where['v.statuspago']=1;
                }elseif ($status==2) {
                    $where['v.statuspago']=0;
                }
                $this->DB3->where($where);
            }elseif ($tipo==4) { //conbinado
                $columns = array( 
                    0=>'vc.estatus',
                    1=>'vc.combinadaId id',
                    2=>'vc.prefactura',
                    3=>'vc.telefono',
                    4=>'vc.correo',
                    5=>'cli.empresa',
                    6=>'per.nombre vendedor',
                    7=>"'combinada' tabla",
                    8=>'vc.fechaentrega',
                    9=>'vc.e_entrega'
                );
                $columns2 = array( 
                    0=>'vc.combinadaId',
                    1=>'vc.estatus',
                    2=>'vc.prefactura',
                    3=>'vc.telefono',
                    4=>'vc.correo',
                    5=>'cli.empresa',
                    6=>'per.nombre',                     
                );
                $select="";
                foreach ($columns as $c) {
                    $select.="$c, ";
                }
                $this->DB3->select($select);
                $this->DB3->from('ventacombinada vc ');
                $this->DB3->join('clientes cli', 'cli.id=vc.idCliente');
                $this->DB3->join('personal per', 'per.personalId=vc.id_personal','left');
                $where = array('vc.prefactura'=>1);
                 if ($cliente>0) {
                    $where['vc.idCliente']=$cliente;
                }
                if ($status==1) {
                    $where['vc.statuspago']=1;
                }elseif ($status==2) {
                    $where['vc.statuspago']=0;
                }
                $this->DB3->where($where);
            }           
            if( !empty($params2['search']['value']) ) {
                $search=$params2['search']['value'];
                $this->DB3->group_start();
                foreach($columns as $c){
                    $this->DB3->or_like($c,$search);
                }
                $this->DB3->group_end();
                
            }
            //==========================================
            $this->DB3->order_by($columns2[$params['order'][0]['column']], $params['order'][0]['dir']);
            $this->DB3->limit($params['length'],$params['start']);
            //echo $this->db->get_compiled_select();
            $query=$this->DB3->get();
            // print_r($query); die;
            return $query;
    }
    function getListaVentasIncompletasfacturat($params){
        $tipo=$params['tipo'];
        if ($tipo==1) {
            
            $strq = "SELECT COUNT(*) as total
                FROM ventas as v 
                WHERE v.prefactura = 1";
        }elseif ($tipo==2) {
            $strq = "SELECT COUNT(*) as total
                FROM rentas as v 
                WHERE v.prefactura = 1";
        }
        elseif ($tipo==3) {
            $strq = "SELECT COUNT(*) as total
                FROM polizasCreadas as v 
                WHERE v.activo=1 and v.prefactura = 1";
        }elseif ($tipo==4) {
            $strq = "SELECT COUNT(*) as total
                FROM ventacombinada as vc
                WHERE vc.prefactura = 1";
        }
        $query = $this->DB3->query($strq);
        //$this->db->close();
        $total=0;
        foreach ($query->result() as $row) {
            $total =$row->total;
        } 
        return $total;
    }
    */
    /*
    public function getListaVentasIncompletasfactura($params){
            $tipo=$params['tipo'];
            $cliente=$params['cliente'];
            $status=$params['status'];
            $fecha=$params['fecha'];
                $columns = array( 
                    0=>'v.id',
                    1=>'v.empresa',
                    2=>'v.prefactura',
                    3=>"v.tabla",
                    4=>'v.vendedor',  
                    5=>'v.estatus',
                    6=>'v.reg',
                    7=>'v.fechaentrega',
                    8=>'v.e_entrega',
                    9=>'v.id_rh',
                    10=>'v.renta_rh',
                    11=>'v.tipotabla',
                    12=>'ep.nombre',
                    13=>'ep.apellido_paterno',
                    14=>'v.equipo',
                    15=>'v.consumibles',
                    16=>'v.refacciones',
                    17=>'v.poliza'

                );
                
                $select="";
                foreach ($columns as $c) {
                    $select.="$c, ";
                }
                $this->db->select($select);
                $this->db->from('vista_entregas_dia v ');
                $this->db->join('personal ep', 'ep.personalId = v.e_entrega_p','left');
                $where = array('v.prefactura'=>1);
                if ($cliente>0) {
                    $where['v.idCliente']=$cliente;
                }
                if ($status==1) {
                    $where['v.e_entrega']=1;
                }elseif ($status==2) {
                    $where['v.e_entrega']=0;
                }
                    $where['v.activo']=1;
                if ($tipo>0) {
                    $where['v.tipotabla']=$tipo;
                }
                if($fecha!=''){
                    $where['v.fechaentrega']=$fecha;   
                }
                
                $this->db->where($where);
                     
            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->db->group_start();
                foreach($columns as $c){
                    $this->db->or_like($c,$search);
                }
                $this->db->group_end();
                
            }
            //==========================================
            $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
            $this->db->limit($params['length'],$params['start']);
            //echo $this->db->get_compiled_select();
            $query=$this->db->get();
            // print_r($query); die;
            return $query;
    }
    public function getListaVentasIncompletasfacturat($params){
            $tipo=$params['tipo'];
            $cliente=$params['cliente'];
            $status=$params['status'];
            $fecha=$params['fecha'];
                $columns = array( 
                    0=>'v.id',
                    1=>'v.empresa',
                    2=>'v.prefactura',
                    3=>"v.tabla",
                    4=>'v.vendedor',  
                    5=>'v.estatus',
                    6=>'v.reg',
                    7=>'v.fechaentrega',
                    8=>'v.e_entrega',
                    9=>'v.id_rh',
                    10=>'v.renta_rh',
                    11=>'v.tipotabla',
                    12=>'v.equipo',
                    13=>'v.consumibles',
                    14=>'v.refacciones',
                    15=>'v.poliza'

                );
                
                $select="";
                foreach ($columns as $c) {
                    $select.="$c, ";
                }
                $this->db->select('COUNT(*) as total');
                $this->db->from('vista_entregas_dia v ');
                $where = array('v.prefactura'=>1);
                if ($cliente>0) {
                    $where['v.idCliente']=$cliente;
                }
                if ($status==1) {
                    $where['v.statuspago']=1;
                }elseif ($status==2) {
                    $where['v.statuspago']=0;
                }
                    $where['v.activo']=1;
                if ($tipo>0) {
                    $where['v.tipotabla']=$tipo;
                }
                if($fecha!=''){
                    $where['v.fechaentrega']=$fecha;   
                }
                $this->db->where($where);
                     
            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->db->group_start();
                foreach($columns as $c){
                    $this->db->or_like($c,$search);
                }
                $this->db->group_end();
                
            }
            //==========================================
            
            $query=$this->db->get();

            return $query->row()->total;
    }
    */
    
    /*
    public function getListaVentasIncompletas($data){
        $cliente=$data['cliente'];
        $status=$data['status'];
        if ($cliente>0) {
            $wherecli=' and v.idCliente='.$cliente;
            $wherecli2=' and vc.idCliente='.$cliente;
        }else{
            $wherecli='';
            $wherecli2='';
        }
        if ($status==1) {
            $wherestatatus=' and v.statuspago=1';
            $wherestatatus2=' and vc.statuspago=1';
        }elseif ($status==2) {
            $wherestatatus=' and v.statuspago=0';
            $wherestatatus2=' and vc.statuspago=0';
        }else{
            $wherestatatus='';
            $wherestatatus2='';
        }
        $sql = "SELECT 
                    vc.estatus,
                    vc.combinadaId as id,
                    vc.prefactura,
                    vc.telefono,
                    vc.correo,
                    cli.empresa,
                    per.nombre as vendedor,
                    '1' as combinada
                FROM ventacombinada as vc
                inner JOIN clientes as cli on cli.id=vc.idCliente
                inner join personal as per on per.personalId=vc.id_personal
                where vc.estatus=1 $wherecli2 $wherestatatus2
                UNION
                SELECT
                    v.estatus,
                    v.id,
                    v.prefactura,
                    v.telefono,
                    v.correo,
                    cli.empresa,
                    per.nombre as vendedor,
                    '0' as combinada
                    FROM ventas as v 
                    inner join clientes as cli on cli.id=v.idCliente
                    left join personal as per on per.personalId=v.id_personal
                where v.combinada=0 $wherecli $wherestatatus";
        $query = $this->db->query($sql);
        return $query->result();
    }*/
    //===========================================================================
        /*
            se creo una vista para simplificar el codigo de la cual es la siguiente
            create view vista_ventas as
                SELECT 
                            vc.combinadaId as id,
                            vc.estatus,
                            vc.prefactura,
                            vc.telefono,
                            vc.correo,
                            cli.empresa,
                            per.nombre as vendedor,
                            '1' as combinada,
                            vc.idCliente,
                            vc.statuspago
                            vc.motivo,
                            vc.activo
                        FROM ventacombinada as vc
                        inner JOIN clientes as cli on cli.id=vc.idCliente
                        inner join personal as per on per.personalId=vc.id_personal
                        where vc.estatus=1
                        UNION
                        SELECT
                            v.id,
                            v.estatus,
                            v.prefactura,
                            v.telefono,
                            v.correo,
                            cli.empresa,
                            per.nombre as vendedor,
                            '0' as combinada,
                            v.idCliente,
                            v.statuspago
                            v.motivo,
                            v.activo
                            FROM ventas as v 
                            inner join clientes as cli on cli.id=v.idCliente
                            left join personal as per on per.personalId=v.id_personal
                        where v.combinada=0
        */
    /*
    function getListaVentasIncompletasasi($params){
        $cliente=$params['cliente'];
        $status=$params['status'];
        $tipoventa=$params['tipoventa'];
        $estatusventa=$params['estatusventa'];

        //$tipoview=$params['tipoview'];
        $idpersonal=$params['idpersonal'];
        $columns = array( 
            0=>'id', 
            1=>'empresa',
            2=>'prefactura',
            3=>'vendedor',
            4=>"estatus",
            5=>'reg',
            6=>'vencimiento',
            7=>'fechaentrega',
            8=>'combinada',
            9=>'facturas',
            10=>'idCliente',
            11=>'equipo',
            12=>'consumibles',
            13=>'refacciones',
            14=>'statuspago',
            15=>'totalcon',
            16=>'totalref',
            17=>'totalequ',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->DB3->select($select);
        $this->DB3->from('vista_ventas3');
            
            if ($cliente>0) {
                $this->DB3->where('idCliente',$cliente);
            }
            if($status==1){
                $this->DB3->where('statuspago',1);
            }elseif ($status==2) {
                $this->DB3->where('statuspago',0);
            }
            if ($tipoventa>0) {
                if ($tipoventa==1) {
                    $this->DB3->where('combinada',0);
                }else{
                    $this->DB3->where('combinada',1);
                }
            }
            $this->DB3->where('activo',$estatusventa);
            if($idpersonal>0){
                $this->DB3->where('id_personal',$idpersonal);
            }
            
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->DB3->group_start();
            foreach($columns as $c){
                $this->DB3->or_like($c,$search);
            }
            $this->DB3->group_end();  
        }            
        $this->DB3->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->DB3->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->DB3->get();
        // print_r($query); die;
        return $query;
    }
    */
    /*
    function getListaVentasIncompletasasit($params){
        $cliente=$params['cliente'];
        $status=$params['status'];
        $tipoventa=$params['tipoventa'];
        $estatusventa=$params['estatusventa'];
        //log_message('error', 'cliente '.$cliente);
        //$tipoview=$params['tipoview'];
        $idpersonal=$params['idpersonal'];
        $columns = array( 
            0=>'id', 
            1=>'empresa',
            2=>'prefactura',
            3=>'vendedor',
            4=>"estatus",
            5=>'reg',
            6=>'vencimiento',
            7=>'fechaentrega',
            8=>'combinada',
            9=>'facturas',
            10=>'idCliente',
            11=>'equipo',
            12=>'consumibles',
            13=>'refacciones',
            14=>'statuspago',
        );
        
        $this->DB3->select('COUNT(*) as total');
        $this->DB3->from('vista_ventas3');
            if ($cliente>0) {
                $this->DB3->where('idCliente',$cliente);
            }
            if($status==1){
                $this->DB3->where('statuspago',1);
            }elseif ($status==2) {
                $this->DB3->where('statuspago',0);
            }
            if ($tipoventa>0) {
                if ($tipoventa==1) {
                    $this->DB3->where('combinada',0);
                }else{
                    $this->DB3->where('combinada',1);
                }
            }
            $this->DB3->where('activo',$estatusventa);
            if($idpersonal>0){
                $this->DB3->where('id_personal',$idpersonal);
            }
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->DB3->group_start();
            foreach($columns as $c){
                $this->DB3->or_like($c,$search);
            }
            $this->DB3->group_end();  
        }            
        //$this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        ///$this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->DB3->get();
        // print_r($query); die;
        return $query->row()->total;

    }
    */
    //==============================================================================
        /*
            para las funciones getlistaservicios y getlistaserviciost se creo una vista de la siguiente consulta para simplificarlo
            si se necesita modificar la vista borrar la anterior y cargarla nuevamente DROP VIEW ` ventas_servicios `
            CREATE VIEW  ventas_servicios AS 
                SELECT ac.asignacionId,ac.tipo,cli.empresa, cli.email,cli.id
                FROM asignacion_ser_avc as ac 
                inner join contrato as con on con.idcontrato=ac.asignacionId
                inner join rentas as ren on ren.id=con.idRenta
                inner join clientes as cli on cli.id=ren.idCliente
                WHERE ac.activo=1 and ac.tipo=1
                UNION
                SELECT ac.asignacionId,ac.tipo,cli.empresa, cli.email,cli.id
                FROM asignacion_ser_avc as ac 
                inner join polizasCreadas as pol on pol.id=ac.asignacionId
                inner join clientes as cli on cli.id=pol.idCliente
                WHERE ac.activo=1 and ac.tipo=2
                UNION
                SELECT ac.asignacionId,ac.tipo,cli.empresa, cli.email,cli.id
                FROM asignacion_ser_avc as ac 
                inner join asignacion_ser_cliente_a as asig on asig.asignacionId=ac.asignacionId
                inner join clientes as cli on cli.id=asig.clienteId
                WHERE ac.activo=1 and ac.tipo=3
                UNION
                SELECT ac.asignacionId,ac.tipo,cli.empresa, cli.email,cli.id
                FROM asignacion_ser_avr as ac 
                inner join contrato as con on con.idcontrato=ac.asignacionId
                inner join rentas as ren on ren.id=con.idRenta
                inner join clientes as cli on cli.id=ren.idCliente
                WHERE ac.activo=1 and ac.tipo=1
                UNION
                SELECT ac.asignacionId,ac.tipo,cli.empresa, cli.email,cli.id
                FROM asignacion_ser_avr as ac 
                inner join polizasCreadas as pol on pol.id=ac.asignacionId
                inner join clientes as cli on cli.id=pol.idCliente
                WHERE ac.activo=1 and ac.tipo=2
                UNION
                SELECT ac.asignacionId,ac.tipo,cli.empresa, cli.email,cli.id
                FROM asignacion_ser_avr as ac 
                inner join clientes as cli on cli.id=ac.asignacionId
                WHERE ac.activo=1 and ac.tipo=3
                --------------------------------------------------------------------------
                DROP VIEW ` ventas_servicios_e `
                CREATE VIEW  ventas_servicios_e AS 
                    SELECT asig.asignacionId, 3 as tipo, cli.empresa, cli.email,cli.id,asig.tiposervicio
                    from asignacion_ser_cliente_a as asig
                    inner join clientes as cli on cli.id=asig.clienteId
                    where asig.status!=3
                    UNION
                    SELECT asic.asignacionId,1 as tipo, cli.empresa, cli.email,cli.id,asic.tiposervicio
                    from asignacion_ser_contrato_a as asic
                    inner join contrato as con on con.idcontrato=asic.contratoId
                    inner join rentas as ren on ren.id=con.idRenta
                    inner join clientes as cli on cli.id=ren.idCliente
                    inner join asignacion_ser_contrato_a_e as asice on asice.asignacionId=asic.asignacionId
                    WHERE asice.status!=3
                    UNION
                    SELECT asip.asignacionId,2 as tipo,cli.empresa, cli.email,cli.id,asip.tiposervicio
                    from asignacion_ser_poliza_a as asip
                    inner join polizasCreadas as pol on pol.id=asip.polizaId
                    inner join clientes as cli on cli.id=pol.idCliente
                    inner join asignacion_ser_poliza_a_e as asipe on asipe.asignacionId=asip.asignacionId
                    WHERE asipe.status!=3
        */
        /*
        function getlistaservicios($params){
            $cliente=$params['cliente'];
            $tipo_servicio=$params['tipo_servicio'];
            $activo=$params['activo'];
            $fechainicial_reg=$params['fechainicial_reg'];
            $columns = array(
                0=>'ve.asignacionId',
                1=>'ve.empresa',
                2=>'pre.prefacturaId',
                3=>'ve.tipo',
                4=>'ve.id',
                5=>'ve.activo',
                6=>'ve.motivoeliminado',
                7=>'ve.reg',
            );
            $select="";
            foreach ($columns as $c) {
                $select.="$c, ";
            }
            $this->db->select($select);
            $this->db->from('ventas_servicios_e ve');
            $this->db->join('prefactura_servicio pre', 'pre.asignacionId=ve.asignacionId and pre.tipo=ve.tipo','left');
            if ($cliente>0) {
                $this->db->where(array('id'=>$cliente));
            }
            if($tipo_servicio==1) {
                $this->db->where(array('ve.tipo'=>1));
            }else if($tipo_servicio==2) {
                $this->db->where(array('ve.tipo'=>2));
            }else if($tipo_servicio==3) {
                $this->db->where(array('ve.tipo'=>3));
            }
            if($fechainicial_reg!='') {
                $this->db->where(array('ve.reg >='=>$fechainicial_reg));
            }
            $this->db->where(array('ve.tiposervicio'=>1,'ve.activo'=>$activo));
            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->db->group_start();
                foreach($columns as $c){
                    $this->db->or_like($c,$search);
                }
                $this->db->group_end();  
            }            
            $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
            $this->db->limit($params['length'],$params['start']);
            //echo $this->db->get_compiled_select();
            $query=$this->db->get();
            // print_r($query); die;
            return $query;
        }
        function getlistaserviciost($params){
            $cliente=$params['cliente'];
            $tipo_servicio=$params['tipo_servicio'];
            $activo=$params['activo'];
            $fechainicial_reg=$params['fechainicial_reg'];
            //log_message('error', 'cliente '.$cliente);
            $columns = array(
                0=>'asignacionId',
                1=>'tipo',
                2=>'empresa',
                3=>'id',
                4=>'reg',
            );
            
            $this->db->select('COUNT(*) as total');
            $this->db->from('ventas_servicios_e');
            if ($cliente>0) {
                $this->db->where(array('id'=>$cliente));
            }
            if($tipo_servicio==1) {
                $this->db->where(array('tipo'=>1));
            }else if($tipo_servicio==2) {
                $this->db->where(array('tipo'=>2));
            }else if($tipo_servicio==3) {
                $this->db->where(array('tipo'=>3));
            }
            if($fechainicial_reg!='') {
                $this->db->where(array('reg >='=>$fechainicial_reg));
            }
            $this->db->where(array('tiposervicio'=>1,'activo'=>$activo));
            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->db->group_start();
                foreach($columns as $c){
                    $this->db->or_like($c,$search);
                }
                $this->db->group_end();  
            }            
            //$this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
            ///$this->db->limit($params['length'],$params['start']);
            //echo $this->db->get_compiled_select();
            $query=$this->db->get();
            // print_r($query); die;
            return $query->row()->total;
        }
        */
    //================================================================================
        /*
        function datosserviciosrefacciones($id,$tipo){
            $strq='SELECT ar.asId,ar.cantidad, ref.id,ref.nombre,ref.codigo,ar.costo
                    FROM asignacion_ser_avr as ar
                    inner JOIN refacciones as ref on ref.id=ar.refacciones
                    WHERE ar.activo=1 and ar.asignacionId='.$id.' AND ar.tipo='.$tipo;
            $query = $this->db->query($strq);
            //$this->db->close();
            return $query;
        }
        function datosserviciosconsumibles($id,$tipo){
            $strq='SELECT ac.asId,ac.cantidad,cons.id,cons.modelo,cons.parte,ac.costo
                FROM asignacion_ser_avc as ac
                inner JOIN consumibles as cons on cons.id=ac.consumible
                WHERE ac.activo=1 and ac.asignacionId='.$id.' and ac.tipo='.$tipo;
            $query = $this->db->query($strq);
            //$this->db->close();
            return $query;
        }
        function datosserviciosaccesorios($id,$tipo){
            $strq='SELECT aa.asId,aa.cantidad,acce.id,acce.nombre,acce.no_parte,aa.costo
                FROM asignacion_ser_ava as aa
                inner JOIN catalogo_accesorios as acce on acce.id=aa.accessorio
                WHERE aa.activo=1 and aa.asignacionId='.$id.' and aa.tipo='.$tipo;
            $query = $this->db->query($strq);
            //$this->db->close();
            return $query;
        }
        */
    //================================================================================
        function getlistadosoloventas($params){
            $cliente=$params['cliente'];
            $status=$params['status'];
            $tipoventa=$params['tipoventa'];
            $estatusventa=$params['estatusventa'];

            //$tipoview=$params['tipoview'];
            $idpersonal=$params['idpersonal'];
            $perfilid=$params['perfilid'];
            $fechainicial_reg=$params['fechainicial_reg'];
            $columns = array(
                0=>'v.id',
                1=>'v.idCliente',
                2=>'v.prefactura',
                3=>'v.id_personal',
                4=>'v.statuspago',
                5=>'v.reg',
                6=>'pre.vencimiento',
                7=>'v.fechaentrega',
                8=>'v.estatus',
                9=>'per.nombre as vendedor',
                10=>'0 as combinada',
                11=>'v.motivo',
                12=>'v.activo',
                13=>'cli.empresa',
                14=>'0 as equipo',
                15=>'0 as consumibles',
                16=>'0 as refacciones',
                
                17=>'(SELECT GROUP_CONCAT(fac.Folio)
                        FROM `ventasd_factura` as ven 
                        INNER JOIN f_facturas as fac on fac.FacturasId=ven.facturaId 
                        WHERE ven.ventaId=v.id and fac.Estado=1) as facturas',
                
                
                18=>'(SELECT sum(vhdc.piezas*vhdc.precioGeneral) as total 
                        FROM ventasd_has_detallesconsumibles as vhdc 
                        WHERE vhdc.idVentas=v.id) as totalcon',
                19=>'(SELECT sum(vhdr.piezas*vhdr.precioGeneral) as total 
                        FROM ventasd_has_detallesrefacciones as vhdr 
                        WHERE vhdr.idVentas=v.id) as totalref',
                20=>'(SELECT sum(vhde.cantidad*vhde.precio) AS total 
                        FROM ventasd_has_detallesequipos AS vhde 
                        WHERE vhde.idVenta=v.id ) as totalequ',
                21=>'(SELECT sum(vhdac.cantidad*vhdac.costo) AS total 
                        FROM ventasd_has_detallesequipos_accesorios AS vhdac 
                        WHERE vhdac.id_venta=v.id ) as totalacc',
                22=>'(SELECT sum(vhdc.cantidad*vhdc.costo_toner) as total 
                        FROM ventasd_has_detallesequipos_consumible as vhdc 
                        WHERE vhdc.idVenta=v.id) as totalcon2',
                23=>'v.e_entrega',
                24=>'v.comentario'
            );
            $columnsss = array(
                0=>'v.id',
                1=>'v.estatus',
                2=>'v.prefactura',
                3=>'v.telefono',
                4=>'v.correo',
                5=>'v.reg',
                6=>'per.nombre',
                7=>'v.motivo',
                8=>'cli.empresa',
                /*
                9=>'(SELECT GROUP_CONCAT(fac.Folio)
                        FROM `factura_venta` as ven 
                        INNER JOIN f_facturas as fac on fac.FacturasId=ven.facturaId 
                        WHERE ven.combinada=0 and ven.ventaId=v.id)',
                */
                9=>'pre.vencimiento',
                10=>'v.fechaentrega',
                /*
                11=>'((SELECT IFNULL(sum(vhdc.piezas*vhdc.precioGeneral),0) as total 
                        FROM ventas_has_detallesConsumibles as vhdc 
                        WHERE vhdc.idVentas=v.id)+(SELECT IFNULL(sum(vhdr.piezas*vhdr.precioGeneral),0) as total 
                        FROM ventas_has_detallesRefacciones as vhdr 
                        WHERE vhdr.idVentas=v.id)+(SELECT IFNULL(sum(vhde.cantidad*vhde.precio),0) AS total 
                        FROM ventas_has_detallesEquipos AS vhde 
                        WHERE vhde.idVenta=v.id ))'
                */
            );
            if($perfilid==1){ 
                $columnsss = array(
                    0=>'v.id',
                    1=>'v.reg',
                    2=>'cli.empresa'
                );
            }
            $select="";
            foreach ($columns as $c) {
                $select.="$c, ";
            }
            $this->db->select($select);
            $this->db->from('ventasd v');
            $this->db->join('clientes cli', 'cli.id=v.idCliente');
            $this->db->join('personal per', 'per.personalId=v.id_personal','left');
            $this->db->join('ventasd_prefactura pre', 'pre.ventaId=v.id','left');
                
                if ($cliente>0) {
                    $this->db->where('v.idCliente',$cliente);
                }
                if($status==1){
                    $this->db->where('v.estatus',3);
                }elseif ($status==2) {
                    $this->db->where('v.estatus',1);
                }
                
                $this->db->where('combinada',0);
                    
                $this->db->where('v.activo',$estatusventa);
                if($idpersonal>0){
                    $this->db->where('v.id_personal',$idpersonal);
                }
                if($fechainicial_reg!=''){
                    $this->db->where('v.reg >=',$fechainicial_reg);
                }
                
            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->db->group_start();
                foreach($columnsss as $c){
                    $this->db->or_like($c,$search);
                }
                $this->db->group_end();  
            }            
            $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
            $this->db->limit($params['length'],$params['start']);
            //echo $this->db->get_compiled_select();
            $query=$this->db->get();
            // print_r($query); die;
            return $query;
        }
        function getlistadosoloventast($params){
            $cliente=$params['cliente'];
            $status=$params['status'];
            $tipoventa=$params['tipoventa'];
            $estatusventa=$params['estatusventa'];
            //log_message('error', 'cliente '.$cliente);
            //$tipoview=$params['tipoview'];
            $idpersonal=$params['idpersonal'];
            $perfilid=$params['perfilid'];
            $fechainicial_reg=$params['fechainicial_reg'];
            $columns = array(
                0=>'v.id',
                1=>'v.estatus',
                2=>'v.prefactura',
                3=>'v.telefono',
                4=>'v.correo',
                5=>'v.reg',
                6=>'per.nombre',
                7=>'v.motivo',
                8=>'cli.empresa',
                /*
                9=>'(SELECT GROUP_CONCAT(fac.Folio)
                        FROM `factura_venta` as ven 
                        INNER JOIN f_facturas as fac on fac.FacturasId=ven.facturaId 
                        WHERE ven.combinada=0 and ven.ventaId=v.id)',
                */
                9=>'pre.vencimiento',
                10=>'v.fechaentrega'
                /*
                11=>'((SELECT IFNULL(sum(vhdc.piezas*vhdc.precioGeneral),0) as total 
                        FROM ventas_has_detallesConsumibles as vhdc 
                        WHERE vhdc.idVentas=v.id)+(SELECT IFNULL(sum(vhdr.piezas*vhdr.precioGeneral),0) as total 
                        FROM ventas_has_detallesRefacciones as vhdr 
                        WHERE vhdr.idVentas=v.id)+(SELECT IFNULL(sum(vhde.cantidad*vhde.precio),0) AS total 
                        FROM ventas_has_detallesEquipos AS vhde 
                        WHERE vhde.idVenta=v.id ))'
                */
            );
            $columnsss=$columns;
            if($perfilid==1){
                $columnsss = array(
                    0=>'v.id',
                    1=>'v.reg',
                    2=>'cli.empresa'
                );
            }
            $this->db->select('COUNT(*) as total');
            $this->db->from('ventasd v');
            $this->db->join('clientes cli', 'cli.id=v.idCliente');
            $this->db->join('personal per', 'per.personalId=v.id_personal','left');
            $this->db->join('ventasd_prefactura pre', 'pre.ventaId=v.id','left');

                if ($cliente>0) {
                    $this->db->where('v.idCliente',$cliente);
                }
                if($status==1){
                    $this->db->where('v.estatus',3);
                }elseif ($status==2) {
                    $this->db->where('v.estatus',1);
                }
                
                $this->db->where('v.combinada',0);
                    
                $this->db->where('v.activo',$estatusventa);
                if($idpersonal>0){
                    $this->db->where('v.id_personal',$idpersonal);
                }
                if($fechainicial_reg!=''){
                    $this->db->where('v.reg >=',$fechainicial_reg);
                }
            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->db->group_start();
                foreach($columnsss as $c){
                    $this->db->or_like($c,$search);
                }
                $this->db->group_end();  
            }            
            //$this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
            ///$this->db->limit($params['length'],$params['start']);
            //echo $this->db->get_compiled_select();
            $query=$this->db->get();
            // print_r($query); die;
            return $query->row()->total;
        }
        /*
        function validarfactura($idventa,$tipo){
            $strq="SELECT *
                        FROM `factura_venta` as ven 
                        INNER JOIN f_facturas as fac on fac.FacturasId=ven.facturaId 
                        WHERE ven.combinada=$tipo and ven.ventaId=$idventa and fac.Estado=1";
            $query = $this->db->query($strq);
            //$this->db->close();
            return $query;
        }
        function entregasventasnormal($fechahoy){
            $strq="SELECT 
                    v.estatus,
                    v.id,
                    v.prefactura,
                    v.telefono,
                    v.correo,
                    cli.empresa,
                    per.nombre as vendedor,
                    'ventas' as tabla,
                    1 as tipotabla,
                    v.fechaentrega,
                    v.e_entrega,
                    v.e_entrega_p,
                    v.idCliente,
                    v.statuspago,
                    v.activo,
                    0 as id_rh,
                    0 as renta_rh,
                    pre.reg,
                    v.id_personal
                    FROM ventas as v 
                inner join clientes as cli on cli.id=v.idCliente 
                LEFT join personal as per on per.personalId=v.id_personal 
                left join prefactura as pre on pre.ventaId=v.id and pre.tipo=0
                WHERE v.activo=1 and v.combinada=0 and v.viewed=1 and v.fechaentrega between '$fechahoy 00:00:00' and  '$fechahoy 23:59:59'";
            $query = $this->db->query($strq);
            return $query;
        }
        function entregasventascombinada($fechahoy){
            $strq="SELECT

                vc.estatus,
                vc.combinadaId id,
                vc.prefactura,
                vc.telefono,
                vc.correo,
                cli.empresa,
                per.nombre vendedor,
                'combinada' tabla,
                4 as tipotabla,
                vc.fechaentrega,
                vc.e_entrega,
                vc.e_entrega_p,
                vc.idCliente,
                vc.statuspago,
                vc.activo,
                0 as id_rh,
                0 as renta_rh,
                pre.reg,
                vc.id_personal,
                vc.equipo,
                vc.consumibles,
                vc.refacciones
                FROM ventacombinada vc
                inner join clientes as cli on cli.id=vc.idCliente
                LEFT join personal as per on per.personalId=vc.id_personal
                left join prefactura as pre on pre.ventaId=vc.combinadaId and pre.tipo=3
                WHERE vc.activo=1 and vc.viewed=1 and vc.fechaentrega between '$fechahoy 00:00:00' and  '$fechahoy 23:59:59'";
            $query = $this->db->query($strq);
            return $query;
        }
        function entregasrentas($fechahoy){
            $strq="SELECT 
                    v.estatus,
                    v.id,
                    v.prefactura,
                    v.telefono,
                    v.correo,
                    cli.empresa,
                    per.nombre vendedor,
                    'rentas' tabla,
                    2 as tipotabla,
                    pre.vencimiento as fechaentrega,
                    v.e_entrega,
                    v.e_entrega_p,
                    v.idCliente,
                    v.statuspago,
                    1 as activo,
                    0 as id_rh,
                    0 as renta_rh,
                    pre.reg,
                    v.id_personal,
                    pre.domicilio_entrega
                    from rentas as v
                    inner join clientes as cli on cli.id=v.idCliente
                    LEFT join personal as per on per.personalId=v.id_personal 
                    left join prefactura as pre on pre.ventaId=v.id and pre.tipo=2
                    WHERE v.estatus !=0 and v.viewed=1 and pre.vencimiento between '$fechahoy' and  '$fechahoy'";
            $query = $this->db->query($strq);
            return $query;
        }
        function entregasrentashistorial($fechahoy){
            $strq="SELECT 
                    0 as estatus,
                    pre.prefacturaId as id,
                    1 prefactura,
                    ren.telefono,
                    ren.correo,
                    cli.empresa,
                    per.nombre vendedor,
                    'rentas historial' as tabla,
                    5 as tipotabla,
                    rh.fechaentrega,
                    rh.e_entrega,
                    rh.e_entrega_p,
                    ren.idCliente,
                    ren.statuspago,
                    rh.activo,
                    rh.id as id_rh,
                    rh.rentas as renta_rh,
                    pre.reg,
                    ren.id_personal
                FROM rentas_historial as rh
                INNER JOIN prefactura as pre on pre.ventaId=rh.id AND pre.tipo=4
                INNER JOIN rentas as ren on ren.id=rh.rentas
                INNER JOIN clientes as cli on cli.id=ren.idCliente
                LEFT join personal as per on per.personalId=ren.id_personal
                WHERE rh.activo=1 and rh.viewed=1 and rh.fechaentrega between '$fechahoy 00:00:00' and  '$fechahoy 23:59:59'";
            $query = $this->db->query($strq);
            return $query;
        }
        */
}
?>