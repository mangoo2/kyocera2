<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contratos extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('login_model');
        $this->load->model('Contrato_model');
        $this->load->model('Rentas_model');
        $this->load->model('Clientes_model');
        $this->load->model('ModeloCatalogos');
        $this->load->model('Login_model');
        $this->load->model('ModeloGeneral');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d');
        $this->fechahoyc = date('Y-m-d H:i:s');
        $this->fechasiguiente = date('Y-m-d',strtotime($this->fechahoy."+ 1 days")); 
        $this->submenu=27;
        if($this->session->userdata('logeado')==true){
            $this->perfilid =$this->session->userdata('perfilid');
            $this->idpersonal = $this->session->userdata('idpersonal');
            /*
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,27);// 27 es el id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
            */
        }else{
            redirect('login');
        }
    }
    // Listado de Empleados
    function index(){  
        $data['MenusubId']=$this->submenu;  
        $data['ejecutivos']=$this->ModeloCatalogos->ejecutivos_rentas();
        //$data['personalrows']=$this->ModeloCatalogos->getselectwheren('personal',array('estatus'=>1));
        $data['personalrows']=$this->ModeloGeneral->listadopersonal();
        $data['random_cont']=$this->ModeloGeneral->aleatorio_contrato();
        $data['perfilid']=$this->perfilid;
        $this->load->view('header');
        $this->load->view('main');
        $this->load->view('operaciones/contratos/listado',$data);
        $this->load->view('footer');
        $this->load->view('operaciones/contratos/jslistcontratos');
    }
    function getMenus($idPerfil){
        // Obtenemos los menús del perfil
        $menus = $this->login_model->getMenus($idPerfil);
        $submenusArray = array();
        // Obtenemos y asignamos los submenus 
        foreach ($menus as $item)
        {
            array_push($submenusArray, $this->login_model->submenus($idPerfil,$item->MenuId));
        }
        // Los asignamos al arary que se envía a la vista
        $data['menus'] = $menus;
        $data['submenusArray'] = $submenusArray;
        return $data;
    }
    function getListadocontratos(){
        $datos = $this->Contrato_model->getListadocontratos();
        $json_data = array("data" => $datos);
        echo json_encode($json_data);
    }
    public function getListadocontratosasi() {
        $params = $this->input->post();
        $params['perfilid']=$this->perfilid;
        //$params['idpersonal']=$this->idpersonal;
        $params['idpersonal']=1;
        $getdata = $this->Contrato_model->getListadocontratosasi($params);
        $totaldata= $this->Contrato_model->total_Listadocontratosasi($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    function contrato($id){  
        $data['MenusubId']=$this->submenu;  
        if($this->session->userdata('logeado')==true){
            $personalId_p = $this->idpersonal;
            $data['tipo3']=0;
            // Contrado finalizar
            $data['contratofin'] = 1 ; 
            // = Consultas de datos del cliente
            $existeC = 0; // existe contrato
            $cliente_id = 0; // id del cliente
            $nombre = ''; // nombre
            $id_persona = 0; // id persona 
            $id_cliente = $this->Rentas_model->idcliente_idrenta($id); 
            foreach ($id_cliente as $item) {
                $cliente_id = $item->idCliente;// id del cliente
            }
            $data['cliente_id']=$cliente_id;
            $data['datoscontacto']=$this->ModeloCatalogos->db13_getselectwheren('cliente_datoscontacto',array('clienteId'=>$cliente_id,'activo'=>1));
            //Domicilio fiscal
            $data['fiscalcontacto'] = $this->Clientes_model->getListadoDatosFiscales($cliente_id);
            // Existe un contrato
            $contratoexiste = $this->Rentas_model->existecontrato($id);             
            foreach ($contratoexiste as $item){
                    $existeC = 1;  
            }
            //$data['cliente_rfc']= $this->Rentas_model->cliente_has_datos_fiscales($cliente_id);
            // Se ferifica si existe, si hay se pintan los datos del contrato ya existente 
            $data['speriocidad']=1;
            foreach ($contratoexiste as $item){
                $data['contratoexiste'] = $existeC; // existe contrato
                $data['idcontrato'] = $item->idcontrato;
                $data['tipocontrato'] = $item->tipocontrato;
                $data['tipopersona'] = $item->tipopersona;
                $data['vigencia'] = $item->vigencia;
                $personalId_p = $item->personalId;
                $data['fechasolicitud'] = $item->fechasolicitud;
                $data['fechainicio'] = $item->fechainicio;
                $data['arrendataria'] = $item->arrendataria;
                $data['rfc'] = $item->rfc;
                $data['actaconstitutiva'] = $item->actaconstitutiva;
                $data['fecha'] = $item->fecha;
                $data['notariapublicano'] = $item->notariapublicano;
                $data['titular'] = $item->titular;
                $data['representantelegal'] = $item->representantelegal;
                $data['instrumentonotarial'] = $item->instrumentonotarial;
                $data['instrumentofechasolicitud'] = $item->instrumentofechasolicitud;
                $data['instrumentonotariapublicano'] = $item->instrumentonotariapublicano ;
                $data['domiciliofiscal'] = $item->domiciliofiscal;
                $data['domicilioinstalacion'] = $item->domicilioinstalacion;
                $data['contacto'] = $item->contacto;
                $data['cargoarea'] = $item->cargoarea;
                $data['telefono'] = $item->telefono;
                $data['rentadeposito'] = $item->rentadeposito;
                $data['rentacolor'] = $item->rentacolor;
                $data['rentaadelantada'] = $item->rentaadelantada;
                $data['tiporenta'] = $item->tiporenta;
                $data['tipo'] = $item->tipo;
                $data['cantidad'] = $item->cantidad;
                $data['exendente'] = $item->exendente;
                $data['observaciones'] = $item->observaciones;
                $data['idRenta'] = $item->idRenta;
                $data['estatus'] = $item->estatus;
                $data['folio'] = $item->folio;
                $data['clicks_mono']=$item->clicks_mono;
                $data['precio_c_e_mono']=$item->precio_c_e_mono;
                $data['clicks_color']=$item->clicks_color;
                $data['precio_c_e_color']=$item->precio_c_e_color;
                $data['servicio_doc']=$item->servicio_doc;
                $data['servicio_equipo']=$item->servicio_equipo;
                $data['servicio_horario']=$item->servicio_horario;
                $data['ordencompra']=$item->ordencompra;
                $data['horaentregainicio']=$item->horaentregainicio;
                $data['horaentregafin']=$item->horaentregafin;
                $data['facturacionlibre']=$item->facturacionlibre;
                $data['speriocidad']=$item->speriocidad;
                
                $data['descuento_g_clicks']=$item->descuento_g_clicks;
                $data['descuento_g_scaneo']=$item->descuento_g_scaneo;
                $data['oc_vigencia']=$item->oc_vigencia;
                $data['oc_comentario']=$item->oc_comentario;
                $data['info_factura']=$item->info_factura;
                if($item->tipo3>0){
                    $data['tipo3']=$item->tipo3;
                }else{
                    $datarenta=$this->ModeloCatalogos->db13_getselectwheren('alta_rentas',array('idcontrato'=>$item->idcontrato));
                    foreach ($datarenta->result() as $itemcr) {
                        $data['tipo3']=$itemcr->tipo3;  
                    }
                }

            }
            //$data['direccioncliente']=$this->ModeloCatalogos->getselectwherenall('clientes_direccion',array('idcliente'=>$cliente_id,'status'=>1));
            $data['direccioncliente']=$this->Rentas_model->datosdireccioncontacto($cliente_id);
            $data['equiposrow']=$this->ModeloCatalogos->getselectwherenall('equipos',array('estatus'=>1)); 
            //$data['bodegasrow']=$this->ModeloCatalogos->getselectwherenall('bodegas',array('activo'=>1));
            $data['restbodegas']=$this->ModeloCatalogos->view_session_bodegas();
            $data['existeC'] = $existeC; // existe contrato
            $persona = $this->Rentas_model->getPersona($personalId_p);
            foreach ($persona as $item) {
                 $nombre = $item->nombre;
                 $id_persona = $item->personalId;
            }
            $data['vendedorid'] = $id_persona;
            $data['vendedor'] = $nombre;
            //$data['equipo'] = $this->Rentas_model->getcontratos_id($id);
            $data['equiposerie'] = $this->Rentas_model->equiposrentas2($id);
            $this->load->view('header');
            $this->load->view('main',$data);
            $this->load->view('rentas/contrato');
            $this->load->view('footerl');
            $this->load->view('rentas/jscontrato');
            $this->load->view('info_m_servisios');
        }else{
            redirect('login');
        }
    }
    public function updatefinalizar(){   
        $id = $this->input->post('id'); 
        $folio = $this->input->post('folio');
        $datosAct = array('estatus' => 2, "folio"=>$folio);
        // Actualizamos el contrato a estatus 2, lo cual querrá decir que se finalizo el contrato
        $this->Contrato_model->actualizaContrato($datosAct,$id);
        //$this->Modelobitacoras->Insert(array('contenido'=>'Se Actualizo contrato','nombretabla'=>'contrato','idtable'=>$id,'tipo'=>'Update','personalId'=>$this->idpersonal));

        $result=$this->ModeloCatalogos->db13_getselectwheren('contrato_equipos',array('contrato'=>$id));
        foreach ($result->result() as $item) {
            //$this->ModeloCatalogos->updateCatalogo('series_productos',array('status'=>0,'bodegaId'=>2,'usado'=>1),array('serieId'=>$item->serieId));
            //$this->Configuraciones_model->registrarmovimientosseries($item->serieId,1);
            //1 equipo, 2 acesorio, 3 refaccion,4 consumible    
            //$this->ModeloCatalogos->Insert('bitacora_movimientos',array('id_item'=>0,'cantidad'=>1,'modeloId'=>0,'tipo'=>1,'entrada_salida'=>1,'personal'=>$this->idpersonal,'descripcion_evento'=>'entrada por finalizacionpara contrato '.$id,'serieId'=>$item->serieId));
        }
        $strq="SELECT asigs.* ,con.idRenta
            FROM contrato as con 
            INNER JOIN rentas as ren on ren.id=con.idRenta
            INNER JOIN rentas_has_detallesEquipos as rene on rene.idRenta=ren.id
            INNER join asignacion_series_r_equipos as asigs on asigs.id_equipo=rene.id
            WHERE con.idcontrato='$id' AND asigs.activo=1";
            $query = $this->db->query($strq);
        foreach ($query->result() as $item) {
            $serieId = $item->serieId;
            $id_equipo = $item->id_equipo;
            $idRenta = $item->idRenta;
            //$this->ModeloCatalogos->updateCatalogo('series_productos',array('status'=>0,'bodegaId'=>2,'usado'=>1,'activo'=>1),array('serieId'=>$serieId));
            //$this->Configuraciones_model->registrarmovimientosseries($item->serieId,1);
            //1 equipo, 2 acesorio, 3 refaccion,4 consumible    
            //$this->ModeloCatalogos->Insert('bitacora_movimientos',array('id_item'=>0,'cantidad'=>1,'modeloId'=>0,'tipo'=>1,'entrada_salida'=>1,'personal'=>$this->idpersonal,'descripcion_evento'=>'entrada por finalizacionpara contrato '.$id,'serieId'=>$item->serieId));

            //$this->ModeloCatalogos->updateCatalogo('asignacion_series_r_equipos',array('activo'=>0),array('id_equipo'=>$id_equipo,'serieId'=>$serieId));
            $this->eliminarRentaE($id_equipo);
        }


    }
    public function cargafiles(){
        $idcontrato_f = $this->input->post('idcontrato');
        // estructura para subir un archivo
        $upload_folder ='uploads/contratos';
        $nombre_archivo = $_FILES['foto']['name'];
        $tipo_archivo = $_FILES['foto']['type'];
        $tamano_archivo = $_FILES['foto']['size'];
        $tmp_archivo = $_FILES['foto']['tmp_name'];
        //$archivador = $upload_folder . '/' . $nombre_archivo;
        $fecha=date('ymd-His');
        $newfile='r_-'.$fecha.'_'.$nombre_archivo;        
        $archivador = $upload_folder . '/'.$newfile;
        if (!move_uploaded_file($tmp_archivo, $archivador)) {
            $return = Array('ok' => FALSE, 'msg' => 'Ocurrió un error al subir el archivo. No pudo guardarse.', 'status' => 'error');
        }else{
            $this->ModeloCatalogos->updateCatalogo('contrato',array('file' => $newfile),array('idcontrato' => $idcontrato_f));
            //===================================
            $result_contrato=$this->ModeloCatalogos->getselectwheren('contrato',array('idcontrato'=>$idcontrato_f));
            $idRenta=0;
            foreach ($result_contrato->result() as $itemc) {
                $idRenta=$itemc->idRenta;
                $resultequipo=$this->ModeloCatalogos->getselectwheren('rentas_has_detallesEquipos',array('idRenta'=>$idRenta));
                foreach ($resultequipo->result() as $item) {
                    $idEquipo=$item->id;
                    $idEquipom=$item->idEquipo;
                    $resultequiposeries=$this->ModeloCatalogos->getselectwheren('asignacion_series_r_equipos',array('id_equipo'=>$idEquipo));
                    foreach ($resultequiposeries->result() as $itemse) {
                        $serieId=$itemse->serieId;
                        $this->ModeloCatalogos->updateCatalogo('series_productos',array('status'=>0,'bodegaId'=>2,'usado'=>1),array('serieId'=>$serieId));
                        $this->Configuraciones_model->registrarmovimientosseries($serieId,1);
                        //1 equipo, 2 acesorio, 3 refaccion,4 consumible    
                        $this->ModeloCatalogos->Insert('bitacora_movimientos',array('id_item'=>0,'cantidad'=>1,'modeloId'=>$idEquipom,'tipo'=>1,'entrada_salida'=>1,'personal'=>$this->idpersonal,'descripcion_evento'=>'entrada por finalizacionpara contrato '.$idcontrato_f,'serieId'=>$serieId));
                    }
                }
            }

            //===================================
            $return = Array('ok'=>TRUE);
        }
        echo json_encode($return);
    }
    function imgcontrato($img){   
        //$data['nombre'] = $img;
        $urlfile=base_url().'uploads/contratos/'.$img;
        $data['urlfile']=$urlfile;
        $info = new SplFileInfo($urlfile);
        $extencion=$info->getExtension();
        //echo $extencion;
        if ($extencion=='pdf') {
            redirect($urlfile);
        }else{
            $this->load->view('operaciones/contratos/imgcontrato',$data);
        }
    }
    public function activarRentaE($id){   
        $data['fecha_suspendido']= '';
        $data['fecha_fin_susp']= '';
        $data['estatus_suspendido']=0;
        $this->Contrato_model->actualizaRentaEquipo($data,$id);
    }
    public function eliminarRentaE($idrow_equipo){   
        $idCliente=0;
        $idRenta=0;
        $idEquipo=0;
        //==================
        $resultequipo=$this->ModeloCatalogos->db13_getselectwheren('rentas_has_detallesEquipos',array('id'=>$idrow_equipo));
        foreach ($resultequipo->result() as $item) {
            $idRenta=$item->idRenta;
            $idEquipo=$item->idEquipo;
        }
        $resultrenta=$this->ModeloCatalogos->db13_getselectwheren('rentas',array('id'=>$idRenta));
        foreach ($resultrenta->result() as $item) {
            $idCliente=$item->idCliente;
        }
        $resultseries=$this->ModeloCatalogos->db13_getselectwheren('asignacion_series_r_equipos',array('id_equipo'=>$idrow_equipo));
        foreach ($resultseries->result() as $item) {
            $this->ModeloCatalogos->updateCatalogo('series_productos',array('status'=>0,'bodegaId'=>2,'usado'=>1),array('serieId'=>$item->serieId));$this->Configuraciones_model->registrarmovimientosseries($item->serieId,1);
            $this->ModeloCatalogos->updateCatalogo('asignacion_series_r_equipos',array('activo'=>0),array('id_equipo'=>$idrow_equipo,'serieId'=>$item->serieId));
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_contrato_a_e',array('status'=>3,'motivosuspencion'=>'Eliminacion y/o Cambio'),array('serieId'=>$item->serieId,'idequipo'=>$idrow_equipo,'fecha >='=>$this->fechahoy,'status'=>0));
            //1 equipo, 2 acesorio, 3 refaccion,4 consumible    
            $this->ModeloCatalogos->Insert('bitacora_movimientos',array('id_item'=>0,'cantidad'=>1,'modeloId'=>0,'tipo'=>1,'entrada_salida'=>1,'personal'=>$this->idpersonal,'descripcion_evento'=>'entrada por eliminacion eq renta '.$idRenta,'serieId'=>$item->serieId));
        }

        //=======================================
        /*
        $data['estatus']=0;
        $data['serie_estatus']=0;
        $this->Contrato_model->actualizaRentaEquipo($data,$id);
        unset($data['estatus']);
        $data['serie_bodega']='';
        $this->Contrato_model->actualizaRentaEquipoT($data,$id,'rentas_has_detallesEquipos_accesorios');
        $this->ModeloCatalogos->getdeletewheren('rentas_has_detallesEquipos_accesorios',$where)
        */
        //==============================================================
        $resultseriesa=$this->ModeloCatalogos->getselectwheren('rentas_has_detallesEquipos_accesorios',array('idrentas'=>$idRenta,'id_equipo'=>$idEquipo));
        foreach ($resultseriesa->result() as $itema) {
            $id_accesoriod=$itema->id_accesoriod;
            $resultseriesa0=$this->ModeloCatalogos->getselectwheren('asignacion_series_r_accesorios',array('id_accesorio'=>$id_accesoriod));
            foreach ($resultseriesa0->result() as $item0) {
                $serieaccesorio=$item0->serieId;
                
                $r_serie_acc=$this->ModeloCatalogos->getselectwheren('series_accesorios',array('serieId'=>$serieaccesorio));
                foreach ($r_serie_acc->result() as $item_rsa) {
                    if($item_rsa->con_serie==1){//con serie
                        $this->ModeloCatalogos->updateCatalogo('series_accesorios',array('status'=>0,'bodegaId'=>2),array('serieId'=>$serieaccesorio));
                        $this->Configuraciones_model->registrarmovimientosseries($serieaccesorio,2);
                    }
                    if($item_rsa->con_serie==0){//sin serie
                        //$this->ModeloCatalogos->updateCatalogo('series_accesorios',array('status'=>0,'bodegaId'=>2),array('serieId'=>$serieaccesorio));
                        $this->ModeloCatalogos->updatestock('series_accesorios','cantidad','+',1,'serieId',$serieaccesorio);
                        $this->Configuraciones_model->registrarmovimientosseries($serieaccesorio,2);
                    }
                }

                

                //1 equipo, 2 acesorio, 3 refaccion,4 consumible    
                $this->ModeloCatalogos->Insert('bitacora_movimientos',array('id_item'=>0,'cantidad'=>1,'modeloId'=>0,'tipo'=>2,'entrada_salida'=>1,'personal'=>$this->idpersonal,'descripcion_evento'=>'entrada por eliminacion eq renta '.$idRenta,'serieId'=>$serieaccesorio));
            }
            
            $this->ModeloCatalogos->getdeletewheren('asignacion_series_r_accesorios',array('id_accesorio'=>$id_accesoriod));
        }




        $this->ModeloCatalogos->getdeletewheren('rentas_has_detallesequipos_consumible',array('idrentas'=>$idRenta,'idequipo'=>$idEquipo));
        $this->ModeloCatalogos->getdeletewheren('rentas_has_detallesEquipos_accesorios',array('idrentas'=>$idRenta,'id_equipo'=>$idEquipo));
        //$this->ModeloCatalogos->getdeletewheren('asignacion_series_r_equipos',array('id_equipo'=>$idrow_equipo));


        $this->ModeloCatalogos->getdeletewheren('contrato_equipos',array('equiposrow'=>$idrow_equipo));
        //$this->ModeloCatalogos->getdeletewheren('rentas_has_detallesEquipos',array('id'=>$idrow_equipo));
        $this->ModeloCatalogos->updateCatalogo('rentas_has_detallesEquipos',array('estatus'=>0),array('id'=>$idrow_equipo));

        //$this->eliminarserviciosposteriores($idRenta);
    }
    public function eliminarRentaEparcial($id){
        $result=$this->Contrato_model->RentaEquipodetalles($id);
        $html='Seleccione las series a eliminar';
        $html.='<table id="eliminarseriestable" class="table bordered Highlight">
                <tr>
                    <td>Equipo</td>
                    <td>Serie</td>
                    <td>Seleccione</td>
                </tr>';
        foreach ($result->result() as $item) {
            $html.='
                <tr>
                    <td>'.$item->modelo.'</td>
                    <td>'.$item->serie.'</td>
                    <td>
                        <input 
                            type="checkbox" 
                            class="equipo_series_d nombre" 
                            id="equipo_series_d_'.$item->id.'_'.$item->serieId.'"
                            data-equipod="'.$item->id.'"
                            data-seried="'.$item->serieId.'"
                            value="'.$item->id.'"
                            >
                        <label for="equipo_series_d_'.$item->id.'_'.$item->serieId.'"></label>
                    </td>
                </tr>';
        }
        $html.='</table>';
        echo $html;
    }
    public function equiposasuspender($id){
        //para los suspendidos
        $result=$this->Contrato_model->RentaEquipodetalles($id);
        $html='Seleccione las series a Suspender';
        $html.='<table id="suspenderseriestable" class="table bordered Highlight">
                <thead>
                <tr>
                    <td>Equipo</td>
                    <td>Serie</td>
                    <td>Seleccione</td>
                </tr>
                </thead>
                <tbody>';
        foreach ($result->result() as $item) {
            if($item->estatus_suspendido==0){
                $checked='';
            }else{
                $checked='checked';
            }
            $html.='
                <tr>
                    <td>'.$item->modelo.'</td>
                    <td>'.$item->serie.'</td>
                    <td>
                        <input 
                            type="checkbox" 
                            class="equipo_series_d nombre" 
                            id="equipo_series_d_'.$item->id.'_'.$item->serieId.'"
                            data-equipod="'.$item->id.'"
                            data-seried="'.$item->serieId.'"
                            value="'.$item->id.'"
                            '.$checked.'
                            >
                        <label for="equipo_series_d_'.$item->id.'_'.$item->serieId.'"></label>
                    </td>
                </tr>';
        }
        $html.='</tbody></table>';
        echo $html;
    }
    public function eliminarRentaEparcial2(){
        //se va a modificar este controlador por que ya no sera eliminacion directa si no despues de la fecha especificada
        
        $data = $this->input->post();
        $arrayseries = $data['series'];
        $idRen = $data['idRen'];
        $DATAc = json_decode($arrayseries);
        for ($i=0;$i<count($DATAc);$i++) {
            $equipo=$DATAc[$i]->equipo;
            $serie=$DATAc[$i]->serie;
            //===================================
            $info_serie_eq=$this->ModeloCatalogos->getselectwheren('series_productos',array('serieId'=>$serie));
            $inf_status=0;
            foreach ($info_serie_eq->result() as $itemsqinfo) {
                $inf_status=$itemsqinfo->status;
            }
            if($inf_status==0){
                $this->ModeloCatalogos->updateCatalogo('series_productos',array('status'=>0),array('serieId'=>$serie));
                $this->Configuraciones_model->registrarmovimientosseries($serie,1);
            }else{

                //===========================================
                
                $this->ModeloCatalogos->updateCatalogo('series_productos',array('status'=>0,'bodegaId'=>2,'usado'=>1),array('serieId'=>$serie));$this->Configuraciones_model->registrarmovimientosseries($serie,1);
                //$this->ModeloCatalogos->getdeletewheren('asignacion_series_r_equipos',array('id_equipo'=>$equipo,'serieId'=>$serie));
                $this->ModeloCatalogos->updateCatalogo('asignacion_series_r_equipos',array('activo'=>0),array('id_equipo'=>$equipo,'serieId'=>$serie));
                $this->ModeloCatalogos->updatestock('rentas_has_detallesEquipos','cantidad','-',1,'id',$equipo);
                $this->ModeloCatalogos->updateCatalogo('asignacion_ser_contrato_a_e',array('status'=>3,'motivosuspencion'=>'Eliminacion y/o Cambio'),array('serieId'=>$serie,'idequipo'=>$equipo,'fecha >='=>$this->fechahoy,'status'=>0));
                $this->eliminarserviciosposteriorese($idRen,$equipo,$serie);

                //1 equipo, 2 acesorio, 3 refaccion,4 consumible    
                $this->ModeloCatalogos->Insert('bitacora_movimientos',array('id_item'=>0,'cantidad'=>1,'modeloId'=>0,'tipo'=>1,'entrada_salida'=>1,'personal'=>$this->idpersonal,'descripcion_evento'=>'entrada por eliminacion eq renta '.$idRen,'serieId'=>$serie));
                //==========================================================
                    $resultac=$this->ModeloCatalogos->db13_getselectwheren('rentas_has_detallesEquipos_accesorios',array('rowequipo'=>$equipo));
                    foreach ($resultac->result() as $itemac) {
                        $id_accesoriod=$itemac->id_accesoriod;
                        
                        $resultac2=$this->ModeloCatalogos->db13_getselectwheren('asignacion_series_r_accesorios',array('id_accesorio'=>$id_accesoriod));
                        foreach ($resultac2->result() as $itemara) {
                            //$itemara->serieId;
                            //$this->ModeloCatalogos->updateCatalogo('series_accesorios',array('status'=>0),array('serieId'=>$itemara->serieId));
                            //=================================
                                 $r_serie_acc=$this->ModeloCatalogos->getselectwheren('series_accesorios',array('serieId'=>$itemara->serieId));
                                foreach ($r_serie_acc->result() as $item_rsa) {
                                    if($item_rsa->con_serie==1){//con serie
                                        $this->ModeloCatalogos->updateCatalogo('series_accesorios',array('status'=>0,'bodegaId'=>2),array('serieId'=>$itemara->serieId));$this->Configuraciones_model->registrarmovimientosseries($itemara->serieId,2);
                                    }
                                    if($item_rsa->con_serie==0){//sin serie
                                        //$this->ModeloCatalogos->updateCatalogo('series_accesorios',array('status'=>0,'bodegaId'=>2),array('serieId'=>$serieaccesorio));
                                        $this->ModeloCatalogos->updatestock('series_accesorios','cantidad','+',1,'serieId',$itemara->serieId);
                                        $this->Configuraciones_model->registrarmovimientosseries($itemara->serieId,2);
                                    }
                                }
                            //=================================

                            //1 equipo, 2 acesorio, 3 refaccion,4 consumible    
                            $this->ModeloCatalogos->Insert('bitacora_movimientos',array('id_item'=>0,'cantidad'=>1,'modeloId'=>$itemac->id_accesorio,'tipo'=>2,'entrada_salida'=>1,'personal'=>$this->idpersonal,'descripcion_evento'=>'entrada por eliminacion ac renta '.$idRen,'serieId'=>$itemara->serieId));
                        }
                    }
                    //$this->ModeloCatalogos->getdeletewheren('rentas_has_detallesEquipos_accesorios',array('rowequipo'=>$equipo)); se deja comentado y se vera si se ocupa
                    //$this->ModeloCatalogos->getdeletewheren('rentas_has_detallesequipos_consumible',array('rowequipo'=>$equipo)); se deja comentado y se vera si se ocupa

                //==========================================================
                    $resultve=$this->ModeloCatalogos->db13_getselectwheren('rentas_has_detallesEquipos',array('id'=>$equipo));
                    foreach ($resultve->result() as $itemv) {
                        if($itemv->cantidad<=0){
                            $this->ModeloCatalogos->updateCatalogo('rentas_has_detallesEquipos',array('estatus'=>0),array('id'=>$equipo));
                        }
                    }
                //==========================================================
            }
        }
        if($idRen>0){
            //$this->eliminarserviciosposteriores($idRen);
        }
    }
    public function eliminarRentaEEAC($id){   
        $data['activo']=0;
        $this->Contrato_model->actualizaRentaEquipoT($data,$id,'rentas_has_detallesEquipos_accesorios');
        $this->Contrato_model->actualizaRentaEquipoT($data,$id,'rentas_has_detallesequipos_consumible');
        //$this->Contrato_model->eliminar_renta_equipo_detalle_consu($id,'rentas_has_detallesequipos_consumible');
    }
    /*public function eliminaRentaDetaCambio($id){
        $this->Contrato_model->eliminar_renta_equipo_detalle($id);    
    }*/
    public function datosRentaD(){
        $id =$this->input->post('idrde');
        $detalles = $this->ModeloCatalogos->db13_getselectwheren('rentas_has_detallesEquipos',array('id'=>$id));
        $datos=array(
          'renta_equipos'=>$detalles->result(),
        );
        echo json_encode($datos);
    }
    public function verificaFechaSusp(){   
        $id =$this->input->post('id');
        $dato = $this->Contrato_model->verificarFechaFin($id);
        foreach($dato as $val) {
            $idVer= $val->id;
            //log_message('error', 'idVer: '.$idVer);
            if($idVer>0){
                $this->Contrato_model->cambiarFechaFin($idVer);    
            }
        } 
    }
    function cancelar(){
        $id = $this->input->post('contratoId');
        $motivo = $this->input->post('motivo');
        $this->ModeloCatalogos->updateCatalogo('contrato',array('estatus'=>0,'personalelimino'=>$this->idpersonal,'motivoeliminacion'=>$motivo),array('idcontrato'=>$id));
        $result=$this->ModeloCatalogos->db13_getselectwheren('contrato_equipos',array('contrato'=>$id));
        foreach ($result->result() as $item) {
            $this->ModeloCatalogos->updateCatalogo('series_productos',array('status'=>0,'bodegaId'=>2,'usado'=>1),array('serieId'=>$item->serieId));
            $this->Configuraciones_model->registrarmovimientosseries($item->serieId,1);
            //1 equipo, 2 acesorio, 3 refaccion,4 consumible    
            $this->ModeloCatalogos->Insert('bitacora_movimientos',array('id_item'=>0,'cantidad'=>1,'modeloId'=>0,'tipo'=>1,'entrada_salida'=>1,'personal'=>$this->idpersonal,'descripcion_evento'=>'entrada por Cancelacion equipo Contrato '.$id,'serieId'=>$item->serieId));
        }

        //======================================================
        $resultc=$this->ModeloCatalogos->db13_getselectwheren('contrato',array('idcontrato'=>$id));
        $idRenta=0;
        foreach ($resultc->result() as $item) {
            $idRenta=$item->idRenta;
        }
        //==========================================================
            $this->ModeloCatalogos->updateCatalogo('rentas_has_detallesEquipos_accesorios',array('activo'=>0),array('idrentas'=>$idRenta));
            $resultac=$this->ModeloCatalogos->db13_getselectwheren('rentas_has_detallesEquipos_accesorios',array('idrentas'=>$idRenta));
            foreach ($resultac->result() as $itemac) {
                $id_accesoriod=$itemac->id_accesoriod;
                
                $resultac2=$this->ModeloCatalogos->db13_getselectwheren('asignacion_series_r_accesorios',array('id_accesorio'=>$id_accesoriod));
                foreach ($resultac2->result() as $itemara) {
                    //$itemara->serieId;
                    //$this->ModeloCatalogos->updateCatalogo('series_accesorios',array('status'=>0),array('serieId'=>$itemara->serieId));
                    //=================================
                         $r_serie_acc=$this->ModeloCatalogos->getselectwheren('series_accesorios',array('serieId'=>$itemara->serieId));
                        foreach ($r_serie_acc->result() as $item_rsa) {
                            if($item_rsa->con_serie==1){//con serie
                                $this->ModeloCatalogos->updateCatalogo('series_accesorios',array('status'=>0,'bodegaId'=>2),array('serieId'=>$itemara->serieId));
                                $this->Configuraciones_model->registrarmovimientosseries($itemara->serieId,2);
                            }
                            if($item_rsa->con_serie==0){//sin serie
                                //$this->ModeloCatalogos->updateCatalogo('series_accesorios',array('status'=>0,'bodegaId'=>2),array('serieId'=>$serieaccesorio));
                                $this->ModeloCatalogos->updatestock('series_accesorios','cantidad','+',1,'serieId',$itemara->serieId);
                                $this->Configuraciones_model->registrarmovimientosseries($itemara->serieId,2);
                            }
                        }
                    //=================================

                    //1 equipo, 2 acesorio, 3 refaccion,4 consumible    
                    $this->ModeloCatalogos->Insert('bitacora_movimientos',array('id_item'=>0,'cantidad'=>1,'modeloId'=>0,'tipo'=>2,'entrada_salida'=>1,'personal'=>$this->idpersonal,'descripcion_evento'=>'entrada por Cancelacion accesorio Contrato '.$id,'serieId'=>$itemara->serieId));
                }
            }

        //==========================================================
            $this->ModeloCatalogos->getdeletewheren('contrato_folio',array('idrenta'=>$idRenta,'status'=>0));
        //===========================================================
        $this->ModeloCatalogos->updateCatalogo('rentas',array('estatus'=>0),array('id'=>$idRenta));
        if($idRenta>0){
            $this->eliminarserviciosposteriores($idRenta);
        }
    }
    function addejecutivo(){
        $id = $this->input->post('contratoId');
        $idejecutivo = $this->input->post('ejecutivoId');
        $this->ModeloCatalogos->updateCatalogo('contrato',array('ejecutivo'=>$idejecutivo),array('idcontrato'=>$id));
        $result=$this->ModeloCatalogos->getselectwheren('contrato',array('idcontrato'=>$id));
        $idrenta=0;
        foreach ($result->result() as $item) {
            $idrenta=$item->idRenta ;
        }

        $this->ModeloCatalogos->updateCatalogo('rentas',array('id_personal'=>$idejecutivo),array('id'=>$idrenta));
    }
    function eliminarserviciosposteriores($idrenta){// esta funcion tiene que se la misma a la que se encuentra en el controlador de Rentas
        $rowcontrato=$this->ModeloCatalogos->db13_getselectwheren('contrato',array('idRenta'=>$idrenta));
        $idcontrato=0;
        foreach ($rowcontrato->result() as $item) {
            $idcontrato=$item->idcontrato;
        }
        $rowcontratoservicios=$this->ModeloCatalogos->servicioscontratosactivos($idcontrato);
        foreach ($rowcontratoservicios->result() as $item) {
            //==============================================
                $this->ModeloCatalogos->db4_updateCatalogo('asignacion_ser_contrato_a_e',array('activo'=>0,'infodelete'=>$this->idpersonal),array('asignacionId'=>$item->asignacionId,'status'=>0,'fecha >'=>$this->fechahoy));
            //==============================================
        }
    }
    function eliminarserviciosposteriorese($idrenta,$equipo,$serie){// esta funcion tiene que se la misma a la que se encuentra en el controlador de Rentas
        $rowcontrato=$this->ModeloCatalogos->db13_getselectwheren('contrato',array('idRenta'=>$idrenta));
        $idcontrato=0;
        foreach ($rowcontrato->result() as $item) {
            $idcontrato=$item->idcontrato;
        }
        $rowcontratoservicios=$this->ModeloCatalogos->servicioscontratosactivos($idcontrato);
        foreach ($rowcontratoservicios->result() as $item) {
            //==============================================
                $this->ModeloCatalogos->updateCatalogo('asignacion_ser_contrato_a_e',array('activo'=>0,'infodelete'=>$this->idpersonal),array('serieId'=>$serie,'idequipo'=>$equipo,'asignacionId'=>$item->asignacionId,'status'=>0,'fecha >'=>$this->fechahoy));
            //==============================================
        }
    }
    function eliminarserviciosposteriores2(){
        $idrenta = $this->input->post('idcontrato');
        $rowcontrato=$this->ModeloCatalogos->getselectwheren('contrato',array('idRenta'=>$idrenta));
        $idcontrato=0;
        foreach ($rowcontrato->result() as $item) {
            $idcontrato=$item->idcontrato;
        }
        $rowcontratoservicios=$this->ModeloCatalogos->servicioscontratosactivos($idcontrato);
        foreach ($rowcontratoservicios->result() as $item) {
            //==============================================
                $this->ModeloCatalogos->updateCatalogo('asignacion_ser_contrato_a_e',array('activo'=>0,'infodelete'=>$this->idpersonal),array('asignacionId'=>$item->asignacionId,'status'=>0,'fecha >'=>$this->fechahoy));
            //==============================================
        }
    }
    function eliminarprehistorial(){
        $params = $this->input->post();
        $historialid = $params['id'];
        $this->ModeloCatalogos->updateCatalogo('rentas_historial',array('activo'=>0),array('id'=>$historialid));
        //========== Equipo =====================
            $rowequipo=$this->ModeloCatalogos->getselectwheren('rentas_historial_equipos',array('historialid'=>$historialid));
            foreach ($rowequipo->result() as $iteme) {
                //$iteme->equipo
                $this->ModeloCatalogos->updateCatalogo('asignacion_series_r_equipos',array('activo'=>0),array('id_equipo'=>$iteme->equipo));
                $rowequipose=$this->ModeloCatalogos->getselectwheren('asignacion_series_r_equipos',array('id_equipo'=>$iteme->equipo));
                foreach ($rowequipose->result() as $itemes) {
                    //$itemes->serieId;
                    $this->ModeloCatalogos->updateCatalogo('series_productos',array('status'=>0),array('serieId'=>$itemes->serieId));
                    $this->Configuraciones_model->registrarmovimientosseries($itemes->serieId,1);
                    //1 equipo, 2 acesorio, 3 refaccion,4 consumible    
                    $this->ModeloCatalogos->Insert('bitacora_movimientos',array('id_item'=>0,'cantidad'=>1,'modeloId'=>0,'tipo'=>1,'entrada_salida'=>1,'personal'=>$this->idpersonal,'descripcion_evento'=>'entrada por eliminacion historialid equipo '.$historialid,'serieId'=>$itemes->serieId));

                }
                $this->ModeloCatalogos->updateCatalogo('rentas_has_detallesEquipos',array('estatus'=>0),array('id'=>$iteme->equipo));
            }
        //=======================================
        //========== accesorios =====================
            $rowaccesorio=$this->ModeloCatalogos->getselectwheren('rentas_historial_accesorios',array('historialid'=>$historialid));
            foreach ($rowaccesorio->result() as $itemacc) {
                //$itemacc->id_accesoriod;
                $rowaccesorio2=$this->ModeloCatalogos->getselectwheren('asignacion_series_r_accesorios',array('id_accesorio'=>$itemacc->id_accesoriod));
                foreach ($rowaccesorio2->result() as $itemacc2) {
                    //$itemacc2->serieId;
                    //$this->ModeloCatalogos->updateCatalogo('series_accesorios',array('status'=>0),array('serieId'=>$itemacc2->serieId));
                    //=================================
                         $r_serie_acc=$this->ModeloCatalogos->getselectwheren('series_accesorios',array('serieId'=>$itemacc2->serieId));
                        foreach ($r_serie_acc->result() as $item_rsa) {
                            if($item_rsa->con_serie==1){//con serie
                                $this->ModeloCatalogos->updateCatalogo('series_accesorios',array('status'=>0,'bodegaId'=>2),array('serieId'=>$itemacc2->serieId));$this->Configuraciones_model->registrarmovimientosseries($itemacc2->serieId,2);
                            }
                            if($item_rsa->con_serie==0){//sin serie
                                //$this->ModeloCatalogos->updateCatalogo('series_accesorios',array('status'=>0,'bodegaId'=>2),array('serieId'=>$serieaccesorio));
                                $this->ModeloCatalogos->updatestock('series_accesorios','cantidad','+',1,'serieId',$itemacc2->serieId);$this->Configuraciones_model->registrarmovimientosseries($itemacc2->serieId,2);
                            }
                        }
                    //=================================

                    //1 equipo, 2 acesorio, 3 refaccion,4 consumible    
                    $this->ModeloCatalogos->Insert('bitacora_movimientos',array('id_item'=>0,'cantidad'=>1,'modeloId'=>0,'tipo'=>2,'entrada_salida'=>1,'personal'=>$this->idpersonal,'descripcion_evento'=>'entrada por eliminacion historialid accesorio '.$historialid,'serieId'=>$itemacc2->serieId));
                }
                $this->ModeloCatalogos->updateCatalogo('rentas_has_detallesEquipos_accesorios',array('activo'=>0),array('id_accesoriod'=>$itemacc->id_accesoriod));
            }
        //=======================================
        //========== consumibles =====================
            $rowconsumibles=$this->ModeloCatalogos->getselectwheren('rentas_historial_consumibles',array('historialid'=>$historialid));
            foreach ($rowconsumibles->result() as $itemcon) {
                //$itemcon->consumible;
                //$itemcon->folios;
                $arr2 = explode(' ',$itemcon->folios);
                foreach ($arr2 as $itemcon) {
                    //echo $itemcon.'<br>';
                    $this->ModeloCatalogos->getdeletewheren('contrato_folio',array('foliotext'=>$itemcon));
                }
            }
        //=======================================
    }
    function renovacion($idrenta){
        $datosrenta=$this->ModeloCatalogos->getselectwheren('rentas',array('id'=>$idrenta));
        $datosrenta=$datosrenta->row();
        unset($datosrenta->id);
        //echo json_encode($datosrenta);
        $idrenta_new=$this->ModeloCatalogos->Insert('rentas',$datosrenta);
        

        $datoscontrato=$this->ModeloCatalogos->getselectwheren('contrato',array('idRenta'=>$idrenta));
        $datoscontrato=$datoscontrato->row();
        $idcontrato_ant=$datoscontrato->idcontrato;
        unset($datoscontrato->idcontrato);
        $datoscontrato->idRenta=$idrenta_new;
        $datoscontrato->personalId=$this->idpersonal;
        $datoscontrato->estatus=1;
        $idcontrato_new=$this->ModeloCatalogos->Insert('contrato',$datoscontrato);

        //===========================================================
            $get_ar=$this->ModeloCatalogos->getselectwheren('alta_rentas',array('idcontrato'=>$idcontrato_ant));
            $id_renta_new=0;
            $id_renta_ant=0;
            foreach ($get_ar->result() as $itemar) {
                $itemar->idcontrato=$idcontrato_new;
                $id_renta_ant=$itemar->id_renta;
                unset($itemar->id_renta);
                $id_renta_new=$this->ModeloCatalogos->Insert('alta_rentas',$itemar);
            }
        //===========================================================


        $arrayequipos=array();        
        $rentaventas = $this->ModeloCatalogos->getselectwheren('rentas_has_detallesEquipos',array('idRenta'=>$idrenta,'estatus'=>1));
        foreach ($rentaventas->result() as $item) {
            $datosequipos=$item;
            $renta_id = $item->idRenta;
            $equipo_id = $item->idEquipo;
            $rowequipos = $item->id;


            //===========================================================================
            $tabla_series_equipo=$this->ModeloCatalogos->getselectwheren('asignacion_series_r_equipos',array('id_equipo'=>$item->id,'activo'=>1));
            foreach ($tabla_series_equipo->result() as $itemserie) {
                //===================================
                    $datosequipos->idRenta=$idrenta_new;
                    $datosequipos->cantidad=1;
                    unset($datosequipos->id);
                $rowequipos_new=$this->ModeloCatalogos->Insert('rentas_has_detallesEquipos',$datosequipos);
                //===================================
                    $itemserie->id_equipo=$rowequipos_new;
                    unset($itemserie->id_asig);
                    $this->ModeloCatalogos->Insert('asignacion_series_r_equipos',$itemserie);
                    $this->ModeloCatalogos->updateCatalogo('series_productos',array('status'=>2),array('serieId'=>$itemserie->serieId));
                    $this->Configuraciones_model->registrarmovimientosseries($itemserie->serieId,1);
                    //1 equipo, 2 acesorio, 3 refaccion,4 consumible    
                    $this->ModeloCatalogos->Insert('bitacora_movimientos',array('id_item'=>0,'cantidad'=>1,'modeloId'=>$equipo_id,'tipo'=>1,'entrada_salida'=>0,'personal'=>$this->idpersonal,'descripcion_evento'=>'salida renovacion equipo Renta:'.$idrenta,'serieId'=>$itemserie->serieId));
                //===================================
                    $tabla_consumibles=$this->ModeloCatalogos->getselectwheren('rentas_has_detallesequipos_consumible',array('idrentas'=>$renta_id,'idequipo'=>$equipo_id,'rowequipo'=>$rowequipos));
                    foreach ($tabla_consumibles->result() as $itemdconsu) {
                        $itemdconsu->rowequipo=$rowequipos_new;
                        $idrowconsumible=$itemdconsu->id;
                        unset($itemdconsu->id);
                        $itemdconsu->idrentas=$idrenta_new;
                        $idrowconsumible_new=$this->ModeloCatalogos->Insert('rentas_has_detallesequipos_consumible',$itemdconsu);

                        //===============================================
                            $tabla_cf=$this->ModeloCatalogos->getselectwheren('contrato_folio',array('idrelacion'=>$idrowconsumible,'idrenta'=>$renta_id,'status <='=>2));
                            foreach ($tabla_cf->result() as $itemcf) {
                                $this->ModeloCatalogos->updateCatalogo('contrato_folio',array('idrenta'=>$idrenta_new,'serieId'=>null,'idrelacion'=>$idrowconsumible_new),array('idcontratofolio'=>$itemcf->idcontratofolio));
                            }
                        //===============================================
                    }    
                //===================================
                    $tabla_accesorios=$this->ModeloCatalogos->getselectwheren('rentas_has_detallesEquipos_accesorios',array('idrentas'=>$renta_id,'id_equipo'=>$equipo_id,'rowequipo'=>$rowequipos));//revisar para el tema de los que no tenga
                    foreach ($tabla_accesorios->result() as $item_accesorios) {
                        $id_accesorio_row=$item_accesorios->id_accesoriod;
                        unset($item_accesorios->id_accesoriod);
                        $item_accesorios->idrentas=$idrenta_new;
                        $item_accesorios->rowequipo=$rowequipos_new;
                        $id_accesorio_row_new=$this->ModeloCatalogos->Insert('rentas_has_detallesEquipos_accesorios',$item_accesorios);
                        //=========================================================================================
                            $tabla_acces_ser=$this->ModeloCatalogos->getselectwheren('asignacion_series_r_accesorios',array('id_accesorio'=>$id_accesorio_row));
                            foreach ($tabla_acces_ser->result() as $item_series_acc) {
                                unset($item_series_acc->id_asig);
                                $item_series_acc->id_accesorio=$id_accesorio_row_new;
                                $this->ModeloCatalogos->Insert('asignacion_series_r_accesorios',$item_series_acc);
                                $this->ModeloCatalogos->updateCatalogo('series_accesorios',array('status'=>2),array('serieId'=>$item_series_acc->serieId));
                                $this->Configuraciones_model->registrarmovimientosseries($item_series_acc->serieId,2);
                                //1 equipo, 2 acesorio, 3 refaccion,4 consumible    
                                $this->ModeloCatalogos->Insert('bitacora_movimientos',array('id_item'=>0,'cantidad'=>1,'modeloId'=>0,'tipo'=>2,'entrada_salida'=>0,'personal'=>$this->idpersonal,'descripcion_evento'=>'salida renovacion accesorio Renta:'.$idrenta,'serieId'=>$item_series_acc->serieId));
                            }
                        //========================================================================================
                    }
                //===================================
                    $tabla_contrato_equipos=$this->ModeloCatalogos->getselectwheren('contrato_equipos',array('equiposrow'=>$rowequipos));
                    foreach ($tabla_contrato_equipos->result() as $item_contrato_equipos) {
                        unset($item_contrato_equipos->ceId);
                        $item_contrato_equipos->contrato=$idcontrato_new;
                        $item_contrato_equipos->equiposrow=$rowequipos_new;
                        $this->ModeloCatalogos->Insert('contrato_equipos',$item_contrato_equipos);   
                        // code...
                    }
                //===================================
                    $tabla_contra_equi_dir=$this->ModeloCatalogos->getselectwheren('contrato_equipo_direccion',array('equiposrow'=>$rowequipos));
                    foreach ($tabla_contra_equi_dir->result() as $item_cont_equ_dir) {
                        unset($item_cont_equ_dir->cedId);
                        $item_cont_equ_dir->contrato=$idcontrato_new;
                        $item_cont_equ_dir->equiposrow=$rowequipos_new;                  
                        $this->ModeloCatalogos->Insert('contrato_equipo_direccion',$item_cont_equ_dir);
                    }
                //======================================
                    if($id_renta_ant>0){
                       $get_areq=$this->ModeloCatalogos->getselectwheren('alta_rentas_equipos',array('id_renta'=>$id_renta_ant,'id_equipo'=>$rowequipos,'serieId'=>$itemserie->serieId)); 
                       foreach ($get_areq->result() as $item_areq) {
                            $contador_inicial=0;
                            $contador_iniciale=0;
                            $get_areq_pred=$this->ModeloCatalogos->getselectwheren('alta_rentas_prefactura_d',array('productoId'=>$rowequipos,'serieId'=>$itemserie->serieId,'tipo'=>$item_areq->tipo)); 
                            foreach ($get_areq_pred->result() as $item_areq_pr) {
                                $contador_inicial=$item_areq_pr->c_c_f;
                                $contador_iniciale=$item_areq_pr->e_c_f;
                                // code...
                            }
                            //log_message('error','$rowequipos: '.$rowequipos.' serieId'.$itemserie->serieId.' tipo:'.$item_areq->tipo.' $contador_inicial '.$contador_inicial.' $contador_iniciale '.$contador_iniciale);
                            //====================================================
                            unset($item_areq->areid);
                            $item_areq->id_renta=$id_renta_new;
                            $item_areq->id_equipo=$rowequipos_new;
                            $item_areq->contador_inicial=$contador_inicial+1;
                            $item_areq->contador_iniciale=$contador_iniciale+1;
                            //$item_areq->tipo //1 monocromatico 2 color
                           $this->ModeloCatalogos->Insert('alta_rentas_equipos',$item_areq);
                       }
                    }
                    
                //======================================

            }
            $arrayequipos[]=$item;
        }

        


        redirect('Contratos/contrato/'.$idrenta_new);
    }
    function productosinfo(){
        $idrow = $this->input->post('idrow');
        $result=$this->ModeloCatalogos->rentaequipos_accesorios_rowequipo($idrow);
        if($result->num_rows()>0){
            $html='<table class="table" align="center">
                    <thead>
                        <tr>
                            <th colspan="3">Tambien se quitaran al eliminar el equipo</th>
                        </tr>
                        <tr>
                            <th>Cantidad</th>
                            <th>Modelo</th>
                            <th>Serie/folio</th>
                        </tr>
                    </thead>
                    <tbody>
                    ';
            foreach ($result->result() as $item) {
                $html.='<tr><td>'.$item->cantidad.'</td><td>'.$item->nombre.'</td><td>'.$item->series.'</td></tr>';
            }
            $html.='</tbody></table>';    
        }else{
            $html='';
        }
        echo $html;
    }
    function retiroequipos(){
        $params=$this->input->post();
        $equipos=$params['equipos'];
        $comentario=$params['comentario'];
        $idRen = $params['idRen'];
        $idpoliza=0;// solo para pruebas
        $idcontrato=0;
        //===================
            $atencion='';
            $telefono='';
            $idCliente=0;
        //===================
        $resultr=$this->ModeloCatalogos->getselectwheren('rentas',array('id'=>$idRen));
        foreach ($resultr->result() as $itemr) {
            $idCliente=$itemr->idCliente;
        }

        $resultc=$this->ModeloCatalogos->getselectwheren('contrato',array('idRenta'=>$idRen));
        foreach ($resultc->result() as $itemc) {
            $idcontrato=$itemc->idcontrato;
            $atencion=$itemc->contacto;
            $telefono=$itemc->telefono;
        }
        /*
        $datosCotizacion = array('atencion' => $atencion,
                                            'correo' => '',
                                            'telefono' => $telefono,
                                            'idCliente' => $idCliente,
                                            'tipo' => 4,
                                            'id_personal' => $this->idpersonal,
                                            'estatus' => 1,
                                            'combinada' => 0
                                            );
        $idpoliza = $this->ModeloCatalogos->Insert('polizasCreadas',$datosCotizacion);
        */
        $datosretiro = array('contratoId'=>$idcontrato,
                            'prioridad'=>3,
                            'zonaId'=>2,
                            'tpoliza'=>22,
                            'tservicio_a'=>1067,
                            'ttipo'=>1,
                            'tserviciomotivo'=>'Retiro',
                            'tiposervicio'=>1,
                            'creado_personal'=>$this->idpersonal,
                            'comentariocal'=>$comentario
                            );
        $idpoliza = $this->ModeloCatalogos->Insert('asignacion_ser_contrato_a',$datosretiro);
        
        
        $DATAc = json_encode($equipos);
        //log_message('error','ddd1'.json_encode($equipos));
        //log_message('error','ddd'.$equipos);
        //$DATAc = $equipos;
        $DATAc = json_decode($DATAc);
        $dataeqarrayinsert=array();
        for ($i=0;$i<count($DATAc);$i++) {
            //log_message('error','data:'.json_encode($DATAc[$i]));
            //echo $DATAc[$i]->serie;
            //echo $DATAc[$i]->idEquipo;
            /*
            $detallesCotizacion = array('idPoliza' => $idpoliza,
                                            'idEquipo' => $DATAc[$i]->idEquipo,
                                            'serie'=> $DATAc[$i]->serie,
                                            'idCotizacion' => 0,
                                            'nombre' => 'RETIRO', 
                                            'cubre' => '1 EVENTO',
                                            'vigencia_meses' => 1,
                                            'vigencia_clicks' => 1,
                                            'modelo' => $DATAc[$i]->modelo,
                                            'precio_local' => 0,
                                            'precio_semi' => null,
                                            'precio_foraneo' => null,
                                            'precio_especial' => null,
                                            'comentario' => $comentario,

                                        );
            $dataeqarrayinsert[]=$detallesCotizacion;
            */
            //=========================================================
                $detallesCotizacion=array('asignacionId'=>$idpoliza,
                                          'idequipo'=>$DATAc[$i]->equiposrow,
                                          'serieId'=>$DATAc[$i]->serieId,
                                          'tecnico'=>49,
                                          'fecha'=>$this->fechasiguiente,
                                          'hora'=>'08:00:00',
                                          'horafin'=>'17:00:00',
                                          'hora_comida'=>'00:00:00',
                                          'horafin_comida'=>'00:00:00'
                                        );
                 $dataeqarrayinsert[]=$detallesCotizacion;
            //=========================================================
            $this->ModeloCatalogos->updateCatalogo('asignacion_series_r_equipos',array('poreliminar'=>1),array('id_equipo'=>$DATAc[$i]->equiposrow,'serieId'=>$DATAc[$i]->serieId,'activo'=>1));
            $this->eliminarserviciosposteriorese($idRen,$DATAc[$i]->equiposrow,$DATAc[$i]->serieId);
        }
        if(count($dataeqarrayinsert)>0){
            //$this->ModeloCatalogos->insert_batch('polizasCreadas_has_detallesPoliza',$dataeqarrayinsert);
            $this->ModeloCatalogos->insert_batch('asignacion_ser_contrato_a_e',$dataeqarrayinsert);
        }
    }
    function retiroequipos2(){
        $params=$this->input->post();
        $equipos=$params['equipos'];
        $comentario=$params['comentario'];
        $idRen = $params['idRen'];
        $tretiro = $params['tretiro'];
        //$idEquipo = $params['idEquipo'];
        $idpoliza=0;// solo para pruebas
        $idcontrato=0;
        //===================
            $atencion='';
            $telefono='';
            $idCliente=0;
        //===================
        $resultr=$this->ModeloCatalogos->getselectwheren('rentas',array('id'=>$idRen));
        foreach ($resultr->result() as $itemr) {
            $idCliente=$itemr->idCliente;
        }

        $resultc=$this->ModeloCatalogos->getselectwheren('contrato',array('idRenta'=>$idRen));
        foreach ($resultc->result() as $itemc) {
            $idcontrato=$itemc->idcontrato;
            $atencion=$itemc->contacto;
            $telefono=$itemc->telefono;
        }

        $DATAc = json_encode($equipos);
        $DATAc = json_decode($DATAc);
        $equipos_add_ser=0;
        $producto_add_ser=0;
        for ($j=0;$j<count($DATAc);$j++) {
            if($DATAc[$j]->tipo==1){
                $equipos_add_ser++;
            }
            if($DATAc[$j]->tipo==2){
                $producto_add_ser++;
            }
            if($DATAc[$j]->tipo==3){
                $producto_add_ser++;
            }
        }
        if($equipos_add_ser>0){
            $tservicio_a=1067;//sin modelo
            


           $datosretiro = array('contratoId'=>$idcontrato,
                            'prioridad'=>3,
                            'zonaId'=>2,
                            'tpoliza'=>22,
                            'tservicio_a'=>$tservicio_a,
                            'ttipo'=>1,
                            'tserviciomotivo'=>'Retiro',
                            'tiposervicio'=>1,
                            'creado_personal'=>$this->idpersonal,
                            'comentariocal'=>$comentario
                            );
            //$idpoliza = $this->ModeloCatalogos->Insert('asignacion_ser_contrato_a',$datosretiro); 
            //$dataeqarrayinsert=array();
            for ($i=0;$i<count($DATAc);$i++) {
                if($DATAc[$i]->tipo==1){
                    //=========================================================
                        $detallesCotizacion=array('asignacionId'=>$idpoliza,
                                                  'idequipo'=>$DATAc[$i]->equiposrow,
                                                  'serieId'=>$DATAc[$i]->serieId,
                                                  'tecnico'=>49,
                                                  'fecha'=>$this->fechasiguiente,
                                                  'hora'=>'08:00:00',
                                                  'horafin'=>'17:00:00',
                                                  'hora_comida'=>'00:00:00',
                                                  'horafin_comida'=>'00:00:00'
                                                );
                         //$dataeqarrayinsert[]=$detallesCotizacion;
                    //=========================================================
                        if($tretiro==1){ //retiro por el ultimo folio
                            $this->ModeloCatalogos->updateCatalogo('asignacion_series_r_equipos',array('poreliminar'=>1),array('id_equipo'=>$DATAc[$i]->equiposrow,'serieId'=>$DATAc[$i]->serieId,'activo'=>1));
                        }else{
                            $this->ModeloCatalogos->updateCatalogo('asignacion_series_r_equipos',array('poreliminar_tec'=>1),array('id_equipo'=>$DATAc[$i]->equiposrow,'serieId'=>$DATAc[$i]->serieId,'activo'=>1));
                            //$this->retirarequipo($DATAc[$i]->equiposrow,$DATAc[$i]->serieId);
                        }
                    
                    $this->eliminarserviciosposteriorese($idRen,$DATAc[$i]->equiposrow,$DATAc[$i]->serieId);
                }
            }
            //if(count($dataeqarrayinsert)>0){
                //$this->ModeloCatalogos->insert_batch('polizasCreadas_has_detallesPoliza',$dataeqarrayinsert);
                //$this->ModeloCatalogos->insert_batch('asignacion_ser_contrato_a_e',$dataeqarrayinsert);
            //}
        }
        //if($producto_add_ser>0){
          
            $dataeqarrayinsert=array();
            $datosretiro = array(
                            'clienteId'=>$idCliente, 
                            'fecha'=>$this->fechasiguiente, 
                            'hora'=>'08:00:00', 
                            'horafin'=>'17:00:00', 
                            'hora_comida'=>'00:00:00', 
                            'horafin_comida'=>'00:00:00', 
                            'tecnico'=>49, 
                            'prioridad'=>4, 
                            'prioridad2'=>1, 
                            'zonaId'=>2, 
                            'tserviciomotivo'=>'Retiro', 
                            'tiposervicio'=>1, 
                            'equipostext'=>$comentario, 
                            'documentaccesorio'=>'', 
                            'creado_personal'=>$this->idpersonal
                        );
            $idpoliza = $this->ModeloCatalogos->Insert('asignacion_ser_cliente_a',$datosretiro); 
            for ($i=0;$i<count($DATAc);$i++) {
                //if($DATAc[$i]->tipo>1){
                    $tservicio_a=1067;//sin modelo
                    $resultc_ie=$this->ModeloCatalogos->getselectwheren('polizas_detalles',array('idPoliza'=>22,'modelo'=>$DATAc[$i]->idEquipo));
                    foreach ($resultc_ie->result() as $item_ie) {
                        $tservicio_a=$item_ie->id;
                    }
                    //=========================================================
                        $detallesCotizacion=array('asignacionId'=>$idpoliza, 
                                                  'direccion'=>$DATAc[$i]->direccion, 
                                                  'iddir'=>$DATAc[$i]->iddir, 
                                                  'modelo'=>$DATAc[$i]->infotext_m,
                                                  'serie'=>$DATAc[$i]->infotext_s, 
                                                  'retiro'=>1,
                                                  'tpoliza'=>22, 
                                                  'tservicio_a'=>$tservicio_a, 
                                                  'ttipo'=>1,
                                                  'comentario'=>$DATAc[$i]->comentario,
                                                  'rowequipo'=>$DATAc[$i]->equiposrow,
                                                  'serieId'=>$DATAc[$i]->serieId
                                                );
                         $dataeqarrayinsert[]=$detallesCotizacion;
                    //=========================================================
                //}
            }
            if(count($dataeqarrayinsert)>0){
                $this->ModeloCatalogos->insert_batch('asignacion_ser_cliente_a_d',$dataeqarrayinsert);
            }
        //}
    }
    function movimientoequipos(){
        $params=$this->input->post();
        $equipos=$params['equipos'];
        $idRen = $params['idRen'];
        $contrato_mov=$params['contrato'];
        
        $ttras = $params['ttras']; //0 traslado, con ultimo folio, 1 traslado y eliminacion directa
        $pass = $params['pass'];
        $dir = $params['dir'];
        $idpoliza=0;// solo para pruebas
        $pasaproceso=0;
        if($ttras==1){ //
            $permiso=0;
            $permisos = $this->Login_model->permisoadminarray(array('9','17','18',$this->idpersonal));//1admin 9 conta 17 julio 18 diana
            foreach ($permisos as $item) {
                $permisotemp=0;

                $usuario=$item->Usuario;

                $verificar = password_verify($pass,$item->contrasena);

                if ($verificar==false) {
                    $verificar = password_verify($pass,$item->clavestemp);
                    if ($verificar) {
                        $permisotemp=1;
                    }else{
                        //$result_permiso_cli = $this->ModeloGeneral->permisoscliente($cli,$pass);
                    }
                }
                if ($verificar) {
                    $permiso=1;
                    $pasaproceso=1;
                    if($permisotemp==1){
                        $this->ModeloCatalogos->updateCatalogo('usuarios',array('clavestemp'=>null),array('UsuarioID'=>$item->UsuarioID));
                    }

                    
                }
            }
        }else{
            $pasaproceso=1;
            $permiso=1;
        }
        if($pasaproceso==1){
            $resultr=$this->ModeloCatalogos->getselectwheren('rentas',array('id'=>$idRen));
            foreach ($resultr->result() as $itemr) {
                $idCliente=$itemr->idCliente;
            }
            
            $DATAc = json_encode($equipos);
            $DATAc = json_decode($DATAc);
            $servicios=0;
            //=========================================================
            //            verificar servicios
            for ($i=0;$i<count($DATAc);$i++) {
                //$resultc=$this->ModeloCatalogos->getselectwheren('asignacion_ser_contrato_a_e',array('idequipo'=>$DATAc[$i]->idEquipo,'serieId'=>$DATAc[$i]->serie,'fecha >='=>$this->fechahoy,'status !='=>0,'activo'=>1));
                $s_array_equipo=$DATAc[$i]->idEquipo;
                $s_array_serie=$DATAc[$i]->serieId;
                $strq = "SELECT * from asignacion_ser_contrato_a_e where idequipo='$s_array_equipo' and serieId='$s_array_serie' and fecha>='$this->fechahoy' and (status=0 or status=1) and activo=1 ";
                $serviciose = $this->db->query($strq);
                if($serviciose->num_rows()>0){
                    $servicios=1;
                }
            }
            //=========================================================
            if($servicios==0){
                

                $resultc=$this->ModeloCatalogos->getselectwheren('contrato',array('idcontrato'=>$contrato_mov));
                foreach ($resultc->result() as $itemc) {
                    $idRenta_mov=$itemc->idRenta;
                }
                $resultr2=$this->ModeloCatalogos->getselectwheren('rentas',array('id'=>$idRenta_mov));
                foreach ($resultr2->result() as $itemr2) {
                    $idCliente_d=$itemr2->idCliente;
                }
                for ($j=0;$j<count($DATAc);$j++) {
                    $idEquipo_select=$DATAc[$j]->equiposrow;
                    $serie_select=$DATAc[$j]->serieId;
                    //=============================================================
                        $rentaventas = $this->ModeloCatalogos->getselectwheren('rentas_has_detallesEquipos',array('id'=>$idEquipo_select));
                        foreach ($rentaventas->result() as $item) {
                            $datosequipos=$item;
                            $renta_id = $item->idRenta;
                            $equipo_id = $item->idEquipo;
                            $rowequipos = $item->id;


                            //===========================================================================
                            $tabla_series_equipo=$this->ModeloCatalogos->getselectwheren('asignacion_series_r_equipos',array('id_equipo'=>$item->id,'serieId'=>$serie_select,'activo'=>1));
                            foreach ($tabla_series_equipo->result() as $itemserie) {
                                //===================================
                                    $datosequipos->idRenta=$idRenta_mov;
                                    $datosequipos->cantidad=1;
                                    unset($datosequipos->id);
                                $rowequipos_new=$this->ModeloCatalogos->Insert('rentas_has_detallesEquipos',$datosequipos);
                                //===================================
                                $itemserie->id_equipo=$rowequipos_new;
                                //$itemserie->poreliminar=0;// si tiene una orden de retiro y se traspasa quitar la orden de traspaso
                                    unset($itemserie->id_asig);
                                    unset($itemserie->reg);
                                    $this->ModeloCatalogos->Insert('asignacion_series_r_equipos',$itemserie);
                                    $this->ModeloCatalogos->updateCatalogo('series_productos',array('status'=>2),array('serieId'=>$itemserie->serieId));$this->Configuraciones_model->registrarmovimientosseries($itemserie->serieId,1);
                                //===================================
                                    $tabla_consumibles=$this->ModeloCatalogos->getselectwheren('rentas_has_detallesequipos_consumible',array('idrentas'=>$renta_id,'idequipo'=>$equipo_id,'rowequipo'=>$rowequipos));
                                    foreach ($tabla_consumibles->result() as $itemdconsu) {
                                        $idcontumiblerenta=$itemdconsu->id;
                                        $itemdconsu->rowequipo=$rowequipos_new;
                                        unset($itemdconsu->id);
                                        $itemdconsu->idrentas=$idRenta_mov;
                                        $this->ModeloCatalogos->Insert('rentas_has_detallesequipos_consumible',$itemdconsu);

                                        //==========================================================================================
                                            if($idCliente==$idCliente_d){}else{
                                                    $folio = 0;
                                                    $contr_folio = $this->ModeloCatalogos->contratofolio($idCliente_d);
                                                    foreach ($contr_folio as $itemc) {
                                                        $folio = $itemc->folio;
                                                    }
                                            }
                                            $tabla_consumibles_folios=$this->ModeloCatalogos->getselectwheren('contrato_folio',array('idrelacion'=>$idcontumiblerenta,'status <'=>2));
                                            foreach ($tabla_consumibles_folios->result() as $itemccf) {
                                                if($idCliente==$idCliente_d){
                                                    $this->ModeloCatalogos->updateCatalogo('contrato_folio',array('idrenta'=>$idRenta_mov,'idrelacion'=>$idcontumiblerenta),array('idcontratofolio'=>$itemccf->idcontratofolio));
                                                }else{
                                                    $folio = $folio +1; 
                                                    $foliotext = $idRenta_mov.'T'.str_pad($folio, 3, "0", STR_PAD_LEFT);
                                                    $result_ft=$this->ModeloCatalogos->getselectwheren('contrato_folio',array('foliotext'=>$foliotext));
                                                    if($result_ft->num_rows()>0){
                                                        $folio+1;
                                                        $foliotext = $idRenta_mov.'T'.str_pad($folio, 3, "0", STR_PAD_LEFT);
                                                    }
                                                    unset($itemccf->idcontratofolio);
                                                    unset($itemccf->fechagenera);
                                                    $itemccf->idrelacion=0;
                                                    $itemccf->idrelacion=$idcontumiblerenta;
                                                    $itemccf->idrenta=$idRenta_mov;

                                                    $itemccf->folio=$folio;
                                                    $itemccf->foliotext=$foliotext;

                                                    $this->ModeloCatalogos->Insert('contrato_folio',$itemccf);
                                                }
                                            }
                                        //==========================================================================================
                                    }    
                                //===================================
                                    $tabla_accesorios=$this->ModeloCatalogos->getselectwheren('rentas_has_detallesEquipos_accesorios',array('idrentas'=>$renta_id,'id_equipo'=>$equipo_id,'rowequipo'=>$rowequipos));//revisar para el tema de los que no tenga
                                    foreach ($tabla_accesorios->result() as $item_accesorios) {
                                        $id_accesorio_row=$item_accesorios->id_accesoriod;
                                        unset($item_accesorios->id_accesoriod);
                                        $item_accesorios->idrentas=$idRenta_mov;
                                        $item_accesorios->rowequipo=$rowequipos_new;
                                        $id_accesorio_row_new=$this->ModeloCatalogos->Insert('rentas_has_detallesEquipos_accesorios',$item_accesorios);
                                        //=========================================================================================
                                            $tabla_acces_ser=$this->ModeloCatalogos->getselectwheren('asignacion_series_r_accesorios',array('id_accesorio'=>$id_accesorio_row));
                                            foreach ($tabla_acces_ser->result() as $item_series_acc) {
                                                unset($item_series_acc->id_asig);
                                                unset($item_series_acc->reg);
                                                $item_series_acc->id_accesorio=$id_accesorio_row_new;
                                                $this->ModeloCatalogos->Insert('asignacion_series_r_accesorios',$item_series_acc);
                                                $this->ModeloCatalogos->updateCatalogo('series_accesorios',array('status'=>2),array('serieId'=>$item_series_acc->serieId));
                                                $this->Configuraciones_model->registrarmovimientosseries($item_series_acc->serieId,2);
                                            }
                                        //========================================================================================
                                    }
                                //===================================
                                    $tabla_contrato_equipos=$this->ModeloCatalogos->getselectwheren('contrato_equipos',array('equiposrow'=>$rowequipos));
                                    foreach ($tabla_contrato_equipos->result() as $item_contrato_equipos) {
                                        $item_contrato_equipos->contrato=$contrato_mov;
                                        $item_contrato_equipos->equiposrow=$rowequipos_new;
                                        unset($item_contrato_equipos->ceId);
                                        $this->ModeloCatalogos->Insert('contrato_equipos',$item_contrato_equipos);   
                                        // code...
                                    }
                                //===================================
                                    $tabla_contra_equi_dir=$this->ModeloCatalogos->getselectwheren('contrato_equipo_direccion',array('equiposrow'=>$rowequipos));
                                    foreach ($tabla_contra_equi_dir->result() as $item_cont_equ_dir) {
                                        unset($item_cont_equ_dir->cedId);
                                        $item_cont_equ_dir->contrato=$contrato_mov;
                                        $item_cont_equ_dir->equiposrow=$rowequipos_new;
                                        $item_cont_equ_dir->direccion=$dir;
                                        $this->ModeloCatalogos->Insert('contrato_equipo_direccion',$item_cont_equ_dir);
                                    }
                                //===================================

                            }
                            $arrayequipos[]=$item;
                        }
                    //=============================================================
                        if($ttras==1){
                            $this->ModeloCatalogos->updateCatalogo('asignacion_series_r_equipos',array('activo'=>0),array('id_equipo'=>$idEquipo_select,'serieId'=>$serie_select));
                        }else{
                            $this->ModeloCatalogos->updateCatalogo('asignacion_series_r_equipos',array('poreliminar'=>1),array('id_equipo'=>$idEquipo_select,'serieId'=>$serie_select,'activo'=>1));
                        }
                    //$this->ModeloCatalogos->updateCatalogo('asignacion_series_r_equipos',array('activo'=>0),array('id_equipo'=>$idEquipo_select,'serieId'=>$serie_select));//a peticion de adriana que no se elimine directamente si no que se mantenga por un periodo mas
                    //$this->ModeloCatalogos->updateCatalogo('asignacion_series_r_equipos',array('poreliminar'=>1),array('id_equipo'=>$idEquipo_select,'serieId'=>$serie_select,'activo'=>1));
                    //==========================================================================================================
                    //     verificar si tiene contadores
                        $resultconta=$this->ModeloCatalogos->getselectwheren('alta_rentas_prefactura_d',array('productoId'=>$idEquipo_select,'serieId'=>$serie_select));
                        if($resultconta->num_rows()>0){
                            $contadorfinal1=0;
                            $contadorfinalescaner1=0;
                            $contadorfinal2=0;
                            foreach ($resultconta->result() as $itemc) {
                                if($itemc->tipo==1){
                                    $contadorfinal1=$itemc->c_c_f;
                                    $contadorfinalescaner1=$itemc->e_c_f;
                                }
                                if($itemc->tipo==2){
                                    $contadorfinal2=$itemc->c_c_f;
                                }
                                
                            }
                            //=============================================
                            $resultcontar=$this->ModeloCatalogos->getselectwheren('alta_rentas',array('idcontrato'=>$contrato_mov));
                            $id_renta=0;
                            foreach ($resultcontar->result() as $itemr) {
                                $id_renta=$itemr->id_renta;
                            }
                            if($id_renta>0){
                                if($contadorfinal1>0){
                                    $this->ModeloCatalogos->Insert('alta_rentas_equipos',array('id_renta'=>$id_renta,'id_equipo'=>$rowequipos_new,'serieId'=>$serie_select,'ubicacion'=>'','promedio'=>0,'nuevo'=>0,'arranque'=>0,'stock_mensual'=>0,'contador_inicial'=>$contadorfinal1,'tipo'=>1,'contador_iniciale'=>$contadorfinalescaner1));
                                }
                                if($contadorfinal2>0){
                                    $this->ModeloCatalogos->Insert('alta_rentas_equipos',array('id_renta'=>$id_renta,'id_equipo'=>$rowequipos_new,'serieId'=>$serie_select,'ubicacion'=>'','promedio'=>0,'nuevo'=>0,'arranque'=>0,'stock_mensual'=>0,'contador_inicial'=>$contadorfinal2,'tipo'=>2,'contador_iniciale'=>0));
                                }
                            }

                        }
                    //==========================================================================================================

                }

            }
        }
        //echo $servicios;
        $array=array('servicios'=>$servicios,'permiso'=>$permiso);
        echo json_encode($array);
    }
    function movimientoequipos_contadores(){
        $params=$this->input->post();
        $equipos=$params['equipos'];
        $contador=$params['contador'];
        $DATAc = json_encode($equipos);
        $DATAc = json_decode($DATAc);
        for ($j=0;$j<count($DATAc);$j++) {
            $idEquipo_select=$DATAc[$j]->equiposrow;
            $serie_select=$DATAc[$j]->serieId;
            $this->ModeloCatalogos->updateCatalogo('asignacion_series_r_equipos',array('contadores'=>$contador),array('id_equipo'=>$idEquipo_select,'serieId'=>$serie_select));
        }
    }
    function verificar_periodo(){
        $contratoId=$this->input->post('contratoId');
        $validar=0;
        $validar_num1=0;
        $validar_num2=0;
        $result=$this->ModeloCatalogos->get_validar_periodo($contratoId);
        foreach ($result as $x) {
            if($x->factura_renta==0 || $x->factura_excedente==0){
                $validar=1;
            }
        }
        echo $validar;
    }
    function desvincularat(){
        $params = $this->input->post();
        $idrow = $params['idrow'];
        $tipo = $params['tipo'];
        if($tipo==1){
            $this->ModeloCatalogos->updateCatalogo('rentas_has_detallesEquipos_accesorios',array('rowequipo'=>0),array('id_accesoriod'=>$idrow));
        }
        if($tipo==2){
            $this->ModeloCatalogos->updateCatalogo('rentas_has_detallesequipos_consumible',array('rowequipo'=>null),array('id'=>$idrow));
        }
    }
    function updateubicacion(){
        $params = $this->input->post();
        $id=$params['id'];
        $ubicacion=$params['ubicacion'];
        $orden=$params['orden'];
        $comen=$params['comen'];
        $this->ModeloCatalogos->updateCatalogo('asignacion_series_r_equipos',array('ubicacion'=>$ubicacion,'orden'=>$orden,'comenext'=>$comen),array('id_asig'=>$id));
    }
    public function suspenderRentaE(){   
        $data=$this->input->post();
        $arrayseries = $data['series'];
        $fecha_fin_susp = $data['fecha_fin_susp'];
        date_default_timezone_set('America/Mexico_City');
        $data['fecha_suspendido']= date('Y-m-d H:i:s');
        //$data['estatus_suspendido']=1;
        //$this->Contrato_model->actualizaRentaEquipo($data,$id);
        $DATAc = json_decode($arrayseries);
        for ($i=0;$i<count($DATAc);$i++) {
            $equipo=$DATAc[$i]->equipo;
            $serie=$DATAc[$i]->serie;
            $suspencion=$DATAc[$i]->suspencion;
            $cont=$DATAc[$i]->contador;
            
            if($suspencion==1){
                $arraydata=array(
                    'fecha_suspendido'=>date('Y-m-d H:i:s'),
                    'fecha_fin_susp'=>$fecha_fin_susp,
                    'estatus_suspendido'=>1,
                    'contadorinfo'=>$cont
                    );
            }else{
                $arraydata=array(
                    'fecha_suspendido'=>null,
                    'fecha_fin_susp'=>null,
                    'contadorinfo'=>null,
                    'estatus_suspendido'=>0
                    );
            }

            $this->ModeloCatalogos->updateCatalogo('asignacion_series_r_equipos',$arraydata,array('id_equipo'=>$equipo,'serieId'=>$serie));
        }
    }
    function updatecontadorsre(){
        $params=$this->input->post();
        $id=$params['id'];
        $cont=$params['cont'];

        $this->ModeloCatalogos->updateCatalogo('asignacion_series_r_equipos',array('contadorinfo'=>$cont),array('id_asig'=>$id));
    }
    function instalacionequipos(){
        $params = $this->input->post();
        $equipos =$params['equipos'];
        $contrato =$params['contrato'];
        $idRen =$params['idRen'];
        $comentario =$params['come'];

        $serviciodata= array(
                                        'contratoId'=>$contrato,
                                        'prioridad'=>1,
                                        'zonaId'=>1,//sin zona
                                        'tpoliza'=>17,//Servicios de instalación
                                        'tservicio_a'=>876,//sin modelo
                                        'ttipo'=>1,
                                        'tserviciomotivo'=>'Servicio de instalación',
                                        'servicio_auto_inst'=>1,
                                        'creado_personal'=>$this->idpersonal,
                                        'comentariocal'=>$comentario
                                    );
        $asignacionId=$this->ModeloCatalogos->Insert('asignacion_ser_contrato_a',$serviciodata);

        //=======================================
            $fechaentrega = strtotime ( '+ 1 day' , strtotime ($this->fechahoy));
            $fechaentrega = date ( 'Y-m-d' , $fechaentrega);
        //=======================================
        $DATAc = json_encode($equipos);

        $DATAc = json_decode($DATAc);
        $dataeqarray=array();
        for ($i=0;$i<count($DATAc);$i++) {
            $idequipo=$DATAc[$i]->equiposrow;
            $serieId=$DATAc[$i]->serieId;
            $kfs=$DATAc[$i]->kfs;
            $comen=$DATAc[$i]->comen;
            if($kfs==1){
                $comentario=' Solicitud de Instalación KFS, ';
            }else{
                $comentario='';
            }
            $comentario.=$comen;

            $datainfoeq=array(
                                    'asignacionId'=>$asignacionId,
                                    'idequipo'=>$idequipo,
                                    'serieId'=>$serieId,
                                    'tecnico'=>49,
                                    'fecha'=>$fechaentrega,
                                    'hora'=>'09:00:00',
                                    'horafin'=>'18:00:00',
                                    'hora_comida'=>null,
                                    'horafin_comida'=>null,
                                    'comentario'=>$comentario
                                );
                            $dataeqarray[]=$datainfoeq;  
            $this->ModeloCatalogos->updateCatalogo('asignacion_series_r_equipos',array('fecha_suspendido'=>null,'fecha_fin_susp'=>null,'estatus_suspendido'=>0),array('id_equipo'=>$idequipo,'serieId'=>$serieId));  
        }
        if(count($dataeqarray)>0){
            //$this->ModeloCatalogos->insert_batch('polizasCreadas_has_detallesPoliza',$dataeqarrayinsert);
            $this->ModeloCatalogos->insert_batch('asignacion_ser_contrato_a_e',$dataeqarray);
        } 
    }
    function suspencionequiposall(){
        $params = $this->input->post();
        $equipos =$params['equipos'];
        $contrato =$params['contrato'];
        $idRen =$params['idRen'];
        $comentario =$params['come'];

        $serviciodata= array(
                                        'contratoId'=>$contrato,
                                        'prioridad'=>1,
                                        'zonaId'=>1,//sin zona
                                        'tpoliza'=>22,//Servicios de Retiro
                                        'tservicio_a'=>1067,//sin modelo
                                        'ttipo'=>1,
                                        'tserviciomotivo'=>'Servicio de Retiro',
                                        'servicio_auto_inst'=>1,
                                        'creado_personal'=>$this->idpersonal,
                                        'comentariocal'=>$comentario
                                    );
        $asignacionId=$this->ModeloCatalogos->Insert('asignacion_ser_contrato_a',$serviciodata);

        //=======================================
            $fechaentrega = strtotime ( '+ 1 day' , strtotime ($this->fechahoy));
            $fechaentrega = date ( 'Y-m-d' , $fechaentrega);
        //=======================================
            $fecha_fin_susp = strtotime ( '+ 2 month' , strtotime ($this->fechahoy));
            $fecha_fin_susp = date ( 'Y-m-d' , $fecha_fin_susp);
        //=======================================
        $DATAc = json_encode($equipos);

        $DATAc = json_decode($DATAc);
        $dataeqarray=array();
        for ($i=0;$i<count($DATAc);$i++) {
            $idequipo=$DATAc[$i]->equiposrow;
            $serieId=$DATAc[$i]->serieId;

            $datainfoeq=array(
                                    'asignacionId'=>$asignacionId,
                                    'idequipo'=>$idequipo,
                                    'serieId'=>$serieId,
                                    'tecnico'=>49,
                                    'fecha'=>$fechaentrega,
                                    'hora'=>'09:00:00',
                                    'horafin'=>'18:00:00',
                                    'hora_comida'=>null,
                                    'horafin_comida'=>null
                                );
                            $dataeqarray[]=$datainfoeq;  
            $this->ModeloCatalogos->updateCatalogo('asignacion_series_r_equipos',array('fecha_suspendido'=>date('Y-m-d H:i:s'),'fecha_fin_susp'=>$fecha_fin_susp,'estatus_suspendido'=>1),array('id_equipo'=>$idequipo,'serieId'=>$serieId));  
        }
        if(count($dataeqarray)>0){
            //$this->ModeloCatalogos->insert_batch('polizasCreadas_has_detallesPoliza',$dataeqarrayinsert);
            $this->ModeloCatalogos->insert_batch('asignacion_ser_contrato_a_e',$dataeqarray);
        } 
    }
    function hojasestado(){
        $id_asig=$_POST['id_asig'];

        

        $configUpload['upload_path'] = FCPATH.'uploads/hojasestado/';
        $configUpload['allowed_types'] = '*';
        $configUpload['max_size'] = '9000';
        $configUpload['file_name'] = date('Y-m-d_H_i_s');
        $this->load->library('upload', $configUpload);
        if($this->upload->do_upload('hojasestado')){
            $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
            $file_name = $upload_data['file_name']; //uploded file name
            $extension=$upload_data['file_ext'];    // uploded file extension
        
            $this->ModeloCatalogos->updateCatalogo('asignacion_series_r_equipos',array('hoja_estado'=>$file_name),array('id_asig'=>$id_asig));
            $output = [];
        }else{
            $data = array('error' => $this->upload->display_errors());
            log_message('error', json_encode($data)); 
            $output = $data;
        }
        
        
        

        
        echo json_encode($output);
    }
    function info_servicios_pendientes($serieId,$serie,$idcliente){
        $r_info_cot_cot=$this->ModeloCatalogos->info_get_serie_cotizacion($serie,$idcliente);
        $r_info_ser_cant=$this->ModeloCatalogos->info_get_serie_servicios($serieId,$serie,$idcliente);

        $total=$r_info_cot_cot->num_rows()+$r_info_ser_cant->num_rows();
        $labels='';
        if($r_info_cot_cot->num_rows()>0){
            $htmlc='';
            foreach ($r_info_cot_cot->result() as $itemc) {
                $htmlc.='<a class="b-btn b-btn-primary btn-sm" href="'.base_url().'index.php/Cotizaciones/documento/'.$itemc->id.'" target="_blank"><i class="far fa-file-alt"></i></a>';
            }
            $labels.='<br><span style="color:red">El equipo tiene cotizaciones pendientes </span> '.$htmlc;
        }
        if($r_info_ser_cant->num_rows()>0){
            $htmlc='';
            foreach ($r_info_ser_cant->result() as $itemc) {
                $htmlc.='<a class="b-btn b-btn-primary btn-sm" onclick="obternerdatosservicio('.$itemc->asignacionId.','.$itemc->tipo.')"><i class="far fa-file-alt"></i></a>';
            }
            $labels.='<br><span style="color:red">El equipo tiene servicios pendientes </span> '.$htmlc;
        }
        $array=array(
                        'count'=>$total,
                        'labels'=>$labels
                    );
        echo json_encode($array);
    }

    function retiroequipos_info(){
        $params = $this->input->post();
        $equipos=$params['equipos'];
        $idRen=$params['idRen'];

        $DATAc = json_encode($equipos);
        $DATAc = json_decode($DATAc);
        $html='<table class="table bordered striped" id="table_retiro_info">
                <thead><tr><th></th><th>Equipo</th><th>Serie</th><th></th><th></th></tr></thead><tbody>';
        $row=0;
        $bloqueos=0;
        for ($i=0;$i<count($DATAc);$i++) {
            $serieId=$DATAc[$i]->serieId;
            $serie=$DATAc[$i]->serie;
            $cli=$DATAc[$i]->cli;
            $r_info_cot_cot=$this->ModeloCatalogos->info_get_serie_cotizacion($serie,$cli);
            $r_info_ser_cant=$this->ModeloCatalogos->info_get_serie_servicios($serieId,$serie,$cli);
            $disabled_check='';
            $disabled_label='<br>';
            if($r_info_cot_cot->num_rows()>0){
                $disabled_check=' disabled ';
                $htmlc='';
                foreach ($r_info_cot_cot->result() as $itemc) {
                    $htmlc.='<a class="b-btn b-btn-primary btn-sm" href="'.base_url().'index.php/Cotizaciones/documento/'.$itemc->id.'" target="_blank"><i class="far fa-file-alt"></i></a>';
                }
                $disabled_label.='<br><span style="color:red">El equipo tiene cotizaciones pendientes </span>'.$htmlc;
                $bloqueos++;
            }
            if($r_info_ser_cant->num_rows()>0){
                //$disabled_check=' disabled ';
                //$disabled_label.='<br><span style="color:red">El equipo tiene servicios pendientes </span>';
                //$bloqueos++;
            }
            $html.='<tr>
                        <td>
                            <input type="checkbox" class="filled-in equipos_retiros" id="row_'.$row.'"  '.$disabled_check.'/>
                              <label for="row_'.$row.'" class="floatbtn">.</label>
                              '.$disabled_label.'
                            <input type="hidden" id="s_tipos" value="1">
                            <input type="hidden" id="s_equiposrow" value="'.$DATAc[$i]->equiposrow.'">
                            <input type="hidden" id="s_idEquipo" value="'.$DATAc[$i]->idEquipo.'">
                            <input type="hidden" id="s_modelo" value="'.$DATAc[$i]->modelo.'">
                            <input type="hidden" id="s_serieId" value="'.$DATAc[$i]->serieId.'">
                            <input type="hidden" id="s_serie" value="'.$DATAc[$i]->serie.'">
                            <input type="hidden" id="s_direccion" value="'.$DATAc[$i]->direccion.'">
                            <input type="hidden" id="s_iddir" value="'.$DATAc[$i]->iddir.'">
                            <input type="hidden" id="s_infor_modeloi" value="'.$DATAc[$i]->modelo.'">
                            <input type="hidden" id="s_infor_seriei" value="'.$DATAc[$i]->serie.'">
                            <input type="hidden" id="s_infor_row" value="">
                            <input type="hidden" id="s_infor_modelo" value="">
                            <input type="hidden" id="s_infor_serie" value="">
                        </td>
                        <td>'.$DATAc[$i]->modelo.'</td>
                        <td>'.$DATAc[$i]->serie.'</td>
                        <td></td>
                        <td><textarea id="s_comentario" onchange="actualizarfolioreg('.$DATAc[$i]->equiposrow.','.$DATAc[$i]->serieId.')" class="actualizarfolioreg_'.$DATAc[$i]->equiposrow.'_'.$DATAc[$i]->serieId.' form-control-bmz"></textarea></td>
                        </tr>';
            $row++;
            $rentaequipos_accesorios = $this->ModeloCatalogos->rentaequipos_accesorios($idRen,$DATAc[$i]->idEquipo,$DATAc[$i]->equiposrow);
            foreach ($rentaequipos_accesorios->result() as $itemacc) {
                $html.='<tr class="desvincular_1_'.$itemacc->id_accesoriod.'">
                        <td>
                            <input type="checkbox" class="filled-in equipos_retiros" id="row_'.$row.'" '.$disabled_check.' />
                              <label for="row_'.$row.'" class="floatbtn">.</label>'.$disabled_label.'
                            <input type="hidden" id="s_tipos" value="2">
                            <input type="hidden" id="s_equiposrow" value="'.$DATAc[$i]->equiposrow.'">
                            <input type="hidden" id="s_idEquipo" value="'.$DATAc[$i]->idEquipo.'">
                            <input type="hidden" id="s_modelo" value="'.$DATAc[$i]->modelo.'">
                            <input type="hidden" id="s_serieId" value="'.$DATAc[$i]->serieId.'">
                            <input type="hidden" id="s_serie" value="'.$DATAc[$i]->serie.'">
                            <input type="hidden" id="s_direccion" value="'.$DATAc[$i]->direccion.'">
                            <input type="hidden" id="s_infor_modeloi" value="'.$itemacc->nombre.'">
                            <input type="hidden" id="s_infor_seriei" value="'.$itemacc->series.'">
                            <input type="hidden" id="s_infor_row" value="'.$itemacc->id_accesoriod.'">
                            <input type="hidden" id="s_infor_modelo" value="'.$itemacc->id_accesorio.'">
                            <input type="hidden" id="s_infor_serie" value="'.$itemacc->serieId.'">
                        </td>
                        <td>'.$itemacc->nombre.'</td>
                        <td>'.$itemacc->series.'</td>
                        <td><a class="btn-bmz red desvincular" onclick="desvincular('.$itemacc->id_accesoriod.',1)" title="Desvincular"><i class="fas fa-unlink fa-fw"></i></a></td>
                        <td><textarea id="s_comentario"  class="form-control-bmz"></textarea></td>
                        </tr>';
                $row++;

            }
            $rentaequipos_consumible = $this->ModeloCatalogos->rentaequipos_consumible($idRen,$DATAc[$i]->idEquipo,$DATAc[$i]->equiposrow);
            foreach ($rentaequipos_consumible->result() as $item) {
                      $foliorelacion=0;
                      //============================ cuando ya todos tengan relacion se quitara
                      $contrato_folio = $this->ModeloCatalogos->selectcontrolfoliorenta($idRen,$item->id_consumibles,$serieId);
                      foreach ($contrato_folio->result() as $itemx) {
                        if($itemx->idrelacion>0){
                         
                        }else{
                          //$html.=$itemx->foliotext.',';
                        }
                      }  
                      //============================ cuando ya todos tengan relacion remplazara a la anterior
                      $contrato_folio2 = $this->ModeloCatalogos->obtenerfoliosconsumiblesrenta($idRen,$item->id_consumibles,$item->id); 
                      foreach ($contrato_folio2->result() as $itemx) {
                            if($itemx->idrelacion>0){
                                $foliorelacion=1;
                                if($itemx->status==1){
                              
                              
                                $html.='<tr class="desvincular_2_'.$item->id.'">
                                            <td>
                                                <input type="checkbox" class="filled-in equipos_retiros" id="row_'.$row.'"  '.$disabled_check.'/>
                                                  <label for="row_'.$row.'" class="floatbtn">.</label>'.$disabled_label.'
                                                <input type="hidden" id="s_tipos" value="3">
                                                <input type="hidden" id="s_equiposrow" value="'.$DATAc[$i]->equiposrow.'">
                                                <input type="hidden" id="s_idEquipo" value="'.$DATAc[$i]->idEquipo.'">
                                                <input type="hidden" id="s_modelo" value="'.$DATAc[$i]->modelo.'">
                                                <input type="hidden" id="s_serieId" value="'.$DATAc[$i]->serieId.'">
                                                <input type="hidden" id="s_serie" value="'.$DATAc[$i]->serie.'">
                                                <input type="hidden" id="s_direccion" value="'.$DATAc[$i]->direccion.'">
                                                <input type="hidden" id="s_infor_modeloi" value="'.$item->modelo.'">
                                                <input type="hidden" id="s_infor_seriei" value="'.$itemx->foliotext.'">
                                                <input type="hidden" id="s_infor_row" value="'.$item->id.'">
                                                <input type="hidden" id="s_infor_modelo" value="'.$item->id_consumibles.'">
                                                <input type="hidden" id="s_infor_serie" value="0">
                                            </td>
                                            <td>'.$item->modelo.'</td>
                                            <td>'.$itemx->foliotext.'</td>
                                            <td><a class="btn-bmz red desvincular" onclick="desvincular('.$item->id.',2)" title="Desvincular"><i class="fas fa-unlink fa-fw"></i></a></td>
                                            <td><textarea id="s_comentario"  class="form-control-bmz"></textarea></td>
                                            </tr>';
                                    $row++;
                                }
                            }
                            
                      }
                      if($foliorelacion==0){
                        
                            $html.='<tr class="desvincular_2_'.$item->id.'">
                                        <td>
                                            <input type="checkbox" class="filled-in equipos_retiros" id="row_'.$row.'"  '.$disabled_check.'/>
                                              <label for="row_'.$row.'" class="floatbtn">.</label>'.$disabled_label.'
                                            <input type="hidden" id="s_tipos" value="3">
                                            <input type="hidden" id="s_equiposrow" value="'.$DATAc[$i]->equiposrow.'">
                                            <input type="hidden" id="s_idEquipo" value="'.$DATAc[$i]->idEquipo.'">
                                            <input type="hidden" id="s_modelo" value="'.$DATAc[$i]->modelo.'">
                                            <input type="hidden" id="s_serieId" value="'.$DATAc[$i]->serieId.'">
                                            <input type="hidden" id="s_serie" value="'.$DATAc[$i]->serie.'">
                                            <input type="hidden" id="s_direccion" value="'.$DATAc[$i]->direccion.'">
                                            <input type="hidden" id="s_infor_modeloi" value="'.$item->modelo.'">
                                            <input type="hidden" id="s_infor_seriei" value="">
                                            <input type="hidden" id="s_infor_row" value="'.$item->id.'">
                                            <input type="hidden" id="s_infor_modelo" value="'.$item->id_consumibles.'">
                                            <input type="hidden" id="s_infor_serie" value="0">
                                        </td>
                                        <td>'.$item->modelo.'</td>
                                        <td></td>
                                        <td><a class="btn-bmz red desvincular" onclick="desvincular('.$item->id.',2)" title="Desvincular"><i class="fas fa-unlink fa-fw"></i></a></td>
                                        <td><textarea id="s_comentario"  class="form-control-bmz"></textarea></td>
                                        </tr>';
                                    $row++;
                      }
              
            }
            
            $html.='<tr><td colspan="5"></td></tr>';
        }
        if($bloqueos>0){
            $html.='<tr><td colspan="5"><button type="button" class="btn-bmz  blue" onclick="desbloquear_eq_pass()"><i class="fas fa-key"></i></button></td></tr>';
        }
        $html.='</tbody></table>';
        echo $html;
    }
    //=============================================
    function viewperiodoscontrato(){
        //copia parcial de Configuracionrentas/viewfacturas

        $idrenta = $this->input->post('idrenta');
        $idcontrato = $this->input->post('idcontrato');
        $result=$this->ModeloCatalogos->getselectwheren('alta_rentas',array('idcontrato'=>$idcontrato));
        foreach ($result->result() as $itemr) {
            $idrenta=$itemr->id_renta;
        }

        $results = $this->ModeloCatalogos->viewprefacturas($idrenta);
        $html='<table id="tablefacturasall" class="table"><thead><tr><th></th><th>Periodo</th><th>Periodo Inicial</th><th>Periodo Final</th><th>Subtotal</th><th>Excedentes</th><th>Total</th><th>vendedor</th><th>Folio</th><th>Registro</th><th></th></tr></thead><tbody>';

        $edit=1;
              $view_delete=1;
        foreach ($results->result() as $item) {
          //================================================
            $html_folio='';
            $factotal=0;
            $result_f=$this->ModeloCatalogos->viewprefacturas_folio($item->prefId);
            foreach ($result_f as $f) {
              $html_folio.=$f->Folio.' ';
              $factotal+=$f->total;
              $view_delete=0;
            }
          //================================================
          $totalg=round($item->total+($item->total*0.16), 2);

          
          
          
          if($item->comentario!='' or $item->comentario!= null){
            
            $btn_comen='b-btn-success';
          }else{
            $btn_comen='b-btn-primary';
          }
          
          $html.='<tr>';
                $html.='<td><input type="checkbox" class="filled-in periodos_checked" id="periodos_'.$item->prefId.'" value="'.$item->prefId.'"><label for="periodos_'.$item->prefId.'"></label></td>';
                $html.='<td>'.$item->prefId.'</td>';
                $html.='<td>'.$item->periodo_inicial.'</td>';
                $html.='<td>'.$item->periodo_final.'</td>';
                $html.='<td>'.$item->subtotal.'</td>';
                $html.='<td>'.$item->excedente.'</td>';
                $html.='<td>'.$item->total.'</td>';
                $html.='<td>'.$item->nombre.' '.$item->apellido_paterno.' '.$item->apellido_materno.'</td>';
                $html.='<td>'.$html_folio.'</td>';
                $html.='<td>'.$item->reg.'</td>';
                $html.='<td><a class="b-btn b-btn-primary" href="'.base_url().'Reportes/rentadetalles/'.$item->prefId.'/'.$idcontrato.'" target="_blank"><i class="fas fa-clipboard-list"></i></a> <a class="b-btn '.$btn_comen.'" onclick="comentariop('.$item->prefId.')" title="comentario"><i class="fas fa-comment fa-fw"></i></a></td>';
                  $html.='</tr>';
        }
            $html.='</tbody></table>';
      echo  $html;
    }
    function clonarcontrato(){
        // es igual a $this->renovacion($idrenta)
        $params = $this->input->post();
        $idrenta=$params['idrenta'];
        $idcontrato=$params['idcontrato'];
        $periodos=$params['periodos'];
        $new_folio=$params['new_folio'];

        $DATAp = json_decode($periodos);

        $datosrenta=$this->ModeloCatalogos->getselectwheren('rentas',array('id'=>$idrenta));
        $datosrenta=$datosrenta->row();
        unset($datosrenta->id);
        //echo json_encode($datosrenta);
        $idrenta_new=$this->ModeloCatalogos->Insert('rentas',$datosrenta);
        

        $datoscontrato=$this->ModeloCatalogos->getselectwheren('contrato',array('idRenta'=>$idrenta));
        $datoscontrato=$datoscontrato->row();
        //log_message('error',json_encode($datoscontrato));
        $idcontrato_ant=$datoscontrato->idcontrato;
        unset($datoscontrato->idcontrato);
        $datoscontrato->idRenta=$idrenta_new;
        $datoscontrato->personalId=$this->idpersonal;
        $datoscontrato->estatus=1;
        $datoscontrato->folio=$new_folio;
        $idcontrato_new=$this->ModeloCatalogos->Insert('contrato',$datoscontrato);

        //===========================================================
            $get_ar=$this->ModeloCatalogos->getselectwheren('alta_rentas',array('idcontrato'=>$idcontrato_ant));
            $id_renta_new=0;
            $id_renta_ant=0;
            foreach ($get_ar->result() as $itemar) {
                $itemar->idcontrato=$idcontrato_new;
                $id_renta_ant=$itemar->id_renta;
                unset($itemar->id_renta);
                $id_renta_new=$this->ModeloCatalogos->Insert('alta_rentas',$itemar);
            }
        //===========================================================


        $arrayequipos=array();        
        $rentaventas = $this->ModeloCatalogos->getselectwheren('rentas_has_detallesEquipos',array('idRenta'=>$idrenta,'estatus'=>1));
        foreach ($rentaventas->result() as $item) {
            $datosequipos=$item;
            $renta_id = $item->idRenta;
            $equipo_id = $item->idEquipo;
            $rowequipos = $item->id;


            //===========================================================================
            $tabla_series_equipo=$this->ModeloCatalogos->getselectwheren('asignacion_series_r_equipos',array('id_equipo'=>$item->id,'activo'=>1));
            foreach ($tabla_series_equipo->result() as $itemserie) {
                //===================================
                    $datosequipos->idRenta=$idrenta_new;
                    $datosequipos->cantidad=1;
                    unset($datosequipos->id);
                $rowequipos_new=$this->ModeloCatalogos->Insert('rentas_has_detallesEquipos',$datosequipos);
                //===================================
                    $itemserie->id_equipo=$rowequipos_new;
                    unset($itemserie->id_asig);
                    $this->ModeloCatalogos->Insert('asignacion_series_r_equipos',$itemserie);
                    $this->ModeloCatalogos->updateCatalogo('series_productos',array('status'=>2),array('serieId'=>$itemserie->serieId));
                    $this->Configuraciones_model->registrarmovimientosseries($itemserie->serieId,1);

                    //1 equipo, 2 acesorio, 3 refaccion,4 consumible    
                    $this->ModeloCatalogos->Insert('bitacora_movimientos',array('id_item'=>0,'cantidad'=>1,'modeloId'=>$equipo_id,'tipo'=>1,'entrada_salida'=>0,'personal'=>$this->idpersonal,'descripcion_evento'=>'salida renovacion equipo Renta:'.$idrenta,'serieId'=>$itemserie->serieId));
                //===================================
                    $tabla_consumibles=$this->ModeloCatalogos->getselectwheren('rentas_has_detallesequipos_consumible',array('idrentas'=>$renta_id,'idequipo'=>$equipo_id,'rowequipo'=>$rowequipos));
                    foreach ($tabla_consumibles->result() as $itemdconsu) {
                        $itemdconsu->rowequipo=$rowequipos_new;
                        $idrowconsumible=$itemdconsu->id;
                        unset($itemdconsu->id);
                        $itemdconsu->idrentas=$idrenta_new;
                        $idrowconsumible_new=$this->ModeloCatalogos->Insert('rentas_has_detallesequipos_consumible',$itemdconsu);

                        //===============================================
                            $tabla_cf=$this->ModeloCatalogos->getselectwheren('contrato_folio',array('idrelacion'=>$idrowconsumible,'idrenta'=>$renta_id,'status <='=>2));
                            foreach ($tabla_cf->result() as $itemcf) {
                                $this->ModeloCatalogos->updateCatalogo('contrato_folio',array('idrenta'=>$idrenta_new,'serieId'=>null,'idrelacion'=>$idrowconsumible_new),array('idcontratofolio'=>$itemcf->idcontratofolio));
                            }
                        //===============================================
                    }    
                //===================================
                    $tabla_accesorios=$this->ModeloCatalogos->getselectwheren('rentas_has_detallesEquipos_accesorios',array('idrentas'=>$renta_id,'id_equipo'=>$equipo_id,'rowequipo'=>$rowequipos));//revisar para el tema de los que no tenga
                    foreach ($tabla_accesorios->result() as $item_accesorios) {
                        $id_accesorio_row=$item_accesorios->id_accesoriod;
                        unset($item_accesorios->id_accesoriod);
                        $item_accesorios->idrentas=$idrenta_new;
                        $item_accesorios->rowequipo=$rowequipos_new;
                        $id_accesorio_row_new=$this->ModeloCatalogos->Insert('rentas_has_detallesEquipos_accesorios',$item_accesorios);
                        //=========================================================================================
                            $tabla_acces_ser=$this->ModeloCatalogos->getselectwheren('asignacion_series_r_accesorios',array('id_accesorio'=>$id_accesorio_row));
                            foreach ($tabla_acces_ser->result() as $item_series_acc) {
                                unset($item_series_acc->id_asig);
                                $item_series_acc->id_accesorio=$id_accesorio_row_new;
                                $this->ModeloCatalogos->Insert('asignacion_series_r_accesorios',$item_series_acc);
                                $this->ModeloCatalogos->updateCatalogo('series_accesorios',array('status'=>2),array('serieId'=>$item_series_acc->serieId));
                                $this->Configuraciones_model->registrarmovimientosseries($item_series_acc->serieId,2);
                                //1 equipo, 2 acesorio, 3 refaccion,4 consumible    
                                $this->ModeloCatalogos->Insert('bitacora_movimientos',array('id_item'=>0,'cantidad'=>1,'modeloId'=>0,'tipo'=>2,'entrada_salida'=>0,'personal'=>$this->idpersonal,'descripcion_evento'=>'salida renovacion accesorio Renta:'.$idrenta,'serieId'=>$item_series_acc->serieId));
                            }
                        //========================================================================================
                    }
                //===================================
                    $tabla_contrato_equipos=$this->ModeloCatalogos->getselectwheren('contrato_equipos',array('equiposrow'=>$rowequipos));
                    foreach ($tabla_contrato_equipos->result() as $item_contrato_equipos) {
                        $item_contrato_equipos->contrato=$idcontrato_new;
                        $item_contrato_equipos->equiposrow=$rowequipos_new;
                        $this->ModeloCatalogos->Insert('contrato_equipos',$item_contrato_equipos);   
                        // code...
                    }
                //===================================
                    $tabla_contra_equi_dir=$this->ModeloCatalogos->getselectwheren('contrato_equipo_direccion',array('equiposrow'=>$rowequipos));
                    foreach ($tabla_contra_equi_dir->result() as $item_cont_equ_dir) {
                        unset($item_cont_equ_dir->cedId);
                        $item_cont_equ_dir->contrato=$idcontrato_new;
                        $item_cont_equ_dir->equiposrow=$rowequipos_new;                  
                        $this->ModeloCatalogos->Insert('contrato_equipo_direccion',$item_cont_equ_dir);
                    }
                //======================================
                    if($id_renta_ant>0){
                       $get_areq=$this->ModeloCatalogos->getselectwheren('alta_rentas_equipos',array('id_renta'=>$id_renta_ant,'id_equipo'=>$rowequipos,'serieId'=>$itemserie->serieId)); 
                       foreach ($get_areq->result() as $item_areq) {
                            $contador_inicial=0;
                            $contador_iniciale=0;
                            $get_areq_pred=$this->ModeloCatalogos->getselectwheren('alta_rentas_prefactura_d',array('productoId'=>$rowequipos,'serieId'=>$itemserie->serieId,'tipo'=>$item_areq->tipo)); 
                            foreach ($get_areq_pred->result() as $item_areq_pr) {
                                $contador_inicial=$item_areq_pr->c_c_f;
                                $contador_iniciale=$item_areq_pr->e_c_f;
                                // code...
                            }
                            //log_message('error','$rowequipos: '.$rowequipos.' serieId'.$itemserie->serieId.' tipo:'.$item_areq->tipo.' $contador_inicial '.$contador_inicial.' $contador_iniciale '.$contador_iniciale);
                            //====================================================
                            unset($item_areq->areid);
                            $item_areq->id_renta=$id_renta_new;
                            $item_areq->id_equipo=$rowequipos_new;
                            $item_areq->contador_inicial=$contador_inicial+1;
                            $item_areq->contador_iniciale=$contador_iniciale+1;
                            //$item_areq->tipo //1 monocromatico 2 color
                           $this->ModeloCatalogos->Insert('alta_rentas_equipos',$item_areq);
                       }
                    }
                    
                //======================================

            }
            $arrayequipos[]=$item;
        }

        //===============clonacion de periodos===========================
        $sql_ar_eq="SELECT * FROM `alta_rentas_equipos` WHERE id_renta='$id_renta_new' GROUP BY serieId";
        //$datosequipos=$this->ModeloCatalogos->getselectwheren('alta_rentas_equipos',array('id_renta'=>$id_renta_new));
        $datosequipos = $this->db->query($sql_ar_eq);

        for ($i=0;$i<count($DATAp);$i++) {
            $id_periodo=$DATAp[$i]->per;
            if($id_periodo>0){
                $r_arp=$this->ModeloCatalogos->getselectwheren('alta_rentas_prefactura',array('prefId'=>$id_periodo));
                foreach ($r_arp->result() as $item_r_arp) {
                    unset($item_r_arp->prefId);
                    $item_r_arp->id_renta=$id_renta_new;
                    $prefId_new=$this->ModeloCatalogos->Insert('alta_rentas_prefactura',$item_r_arp);

                    $r_arp_d=$this->ModeloCatalogos->getselectwheren('alta_rentas_prefactura_d',array('prefId'=>$id_periodo));
                    foreach ($r_arp_d->result() as $item_r_arp_d) {
                        unset($item_r_arp_d->prefdId);
                        foreach ($datosequipos->result() as $item_data_eq) {
                            if($item_data_eq->serieId==$item_r_arp_d->serieId){//verificar este paso por que puede que no pase como en el ticket 2189
                                $item_r_arp_d->prefId=$prefId_new;
                                $item_r_arp_d->productoId=$item_data_eq->id_equipo;
                                $this->ModeloCatalogos->Insert('alta_rentas_prefactura_d',$item_r_arp_d);

                            }
                        }
                    }
                    
                }
            }
            

        }

        


        //redirect('Contratos/contrato/'.$idrenta_new);
    }
    function up_doc_multiple(){
        $idcontrato=$_POST['idcontrato'];
      $data = [];
   
      $count = count($_FILES['files']['name']);
      $output = [];
      for($i=0;$i<$count;$i++){
    
        if(!empty($_FILES['files']['name'][$i])){
    
          $_FILES['file']['name'] = $_FILES['files']['name'][$i];
          $file_name_o=$_FILES['files']['name'][$i];//esto solo para guardar el nombre del archivo ya que internamente es otro
          $_FILES['file']['type'] = $_FILES['files']['type'][$i];
          $_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
          $_FILES['file']['error'] = $_FILES['files']['error'][$i];
          $_FILES['file']['size'] = $_FILES['files']['size'][$i];
          $DIR_SUC=FCPATH.'uploads/contratos';

          $config['upload_path'] = $DIR_SUC; 
          //$config['allowed_types'] = 'docx|xlsx|pdf|doc|xls|pptx';
          $config['allowed_types'] = '*';

          //$config['allowed_types'] = 'docx|xlsx|pdf|jpg|jpeg|png|gif';
          $config['max_size'] = 5000;
          $file_names='oc_'.date('Ymd_His').'_'.rand(0, 500);
          //$config['file_name'] = $file_names.'_'.str_replace(' ', '_', $_FILES['files']['name'][$i]);
          $config['file_name'] = $file_names;
          //$config['file_name'] = $file_names;
   
          $this->load->library('upload',$config); 
    
          if($this->upload->do_upload('file')){
            $uploadData = $this->upload->data();
            $filename = $uploadData['file_name'];
            $filenametype = $uploadData['file_ext'];
   
            $data['totalFiles'][] = $filename;
            //$this->ModeloGeneral->tabla_inserta('equipo_imgs',array('idequipo'=>$idequipo,'imagen'=>$filename,'filetype'=>$filenametype,'usuario'=>$this->idpersonal));
            $this->ModeloCatalogos->Insert('contrato_oc_doc',array('idcontrato'=>$idcontrato,'files'=>$filename,'file_name_o'=>$file_name_o,'created_per'=>$this->idpersonal));
          }else{
            $output = array('error' => $this->upload->display_errors());
            log_message('error', json_encode($output)); 
          }
        }
   
      }
      echo json_encode($output);
    }
    function upload_data_view_oc(){
        $params=$this->input->post();
        $idcon = $params['idcon'];

        $datosdoc=$this->ModeloCatalogos->getselectwheren('contrato_oc_doc',array('idcontrato'=>$idcon,'activo'=>1));
        $html='';
        foreach ($datosdoc->result() as $item) {
            $url_file=base_url().'uploads/contratos/'.$item->files;
            $html.='<div class="file-preview-frame krajee-default  kv-preview-thumb"  data-fileindex="-1"  data-template="pdf" title="'.$item->file_name_o.'"><div class="kv-file-content">';
            $html.='<embed class="kv-preview-data file-preview-pdf" src="'.$url_file.'" type="application/pdf" style="width:100%;height:160px;position:relative;">';
            $html.='</div><div class="file-thumbnail-footer">';
                $html.='<div class="file-footer-caption" title="EdoCuenta_01_23.pdf">';
                    $html.='<div class="file-caption-info">'.$item->file_name_o.'</div>';
                $html.='</div>';
                
            
            $html.='<div class="file-actions">';
                $html.='<div class="file-footer-buttons">';
                     $html.='<button class="b-btn b-btn-danger tooltipped deleteocfile" data-position="top" data-delay="50" data-tooltip="Eliminar" onclick="deleteocfile('.$item->id.')" ><i class="fas fa-times"></i></button> ';
             $html.='<a class="b-btn b-btn-primary tooltipped" data-position="top" data-delay="50" data-tooltip="Vizualizar" href="'.$url_file.'"  target="_blank"><i class="fas fa-eye"></i></a>';
             $html.='<a class="b-btn b-btn-primary tooltipped" data-position="top" data-delay="50" data-tooltip="Vizualizar" href="'.$url_file.'"  download><i class="fas fa-download"></i></a>';
             $html.='     </div>';
            $html.='</div>';
            $html.='<div class="clearfix"></div>';
            $html.='</div>';
            $html.='</div>';
        }
        echo $html;
    }
    function deleteocfile(){
        $params=$this->input->post();
        $id = $params['id'];
        $this->ModeloCatalogos->updateCatalogo('contrato_oc_doc',array('activo'=>0,'delete_reg'=>$this->fechahoyc,'delete_per'=>$this->idpersonal),array('id'=>$id));
    }
    function updatecondicionfac(){
        $params=$this->input->post();
        $idcon = $params['idcon'];
        $info_factura = $params['info_factura'];

        $this->ModeloCatalogos->updateCatalogo('contrato',array('info_factura'=>$info_factura),array('idcontrato'=>$idcon));
    }
    function cambiarestatuspe(){
        $params=$this->input->post();
        $ideq = $params['ideq'];
        $idserie = $params['idserie'];

        $this->ModeloCatalogos->updateCatalogo('asignacion_series_r_equipos',array('poreliminar'=>0),array('id_equipo'=>$ideq,'serieId'=>$idserie));
    }
    function retirarequipo($idequipo,$serieid){
        //esta funcion es igual a la de Configuracionrentas/retirarequipo cualquiere modificacion se ara en los dos
        /*
      $resultserie=$this->ModeloCatalogos->getselectwheren('asignacion_series_r_equipos',array('id_equipo'=>$idequipo,'serieId'=>$serieid,'activo'=>1));
      $poreliminar=0;$idmodelo=0;$modeloeq=0;
      foreach ($resultserie->result() as $items) {
        $poreliminar=$items->poreliminar;
      }
      //============================
        $strq="SELECT asca.equipostext,ascae.modelo,ascae.serie,asca.fecha,ascae.asignacionIdd,ascae.status,ascae.rowequipo,ascae.serieId,per.nombre,per.apellido_paterno,per.apellido_materno
            FROM asignacion_ser_cliente_a as asca
            INNER JOIN asignacion_ser_cliente_a_d as ascae ON ascae.asignacionId=asca.asignacionId
            INNER JOIN personal as per on per.personalId=asca.tecnico
            WHERE ascae.tpoliza=22 AND ascae.tservicio_a=1067 AND ascae.activo=1 AND asca.activo=1  AND ascae.nr=0 AND ascae.rowequipo='$idequipo' AND ascae.serieId='$serieid' and ascae.status=2
            ORDER BY `ascae`.`asignacionIdd` DESC LIMIT 1";
            $result_ser = $this->db->query($strq);
            $poreliminar=0;
            foreach ($result_ser->result() as $itemser) {
              if($itemser->status==2){
                $poreliminar=1;
              }else{
                $poreliminar=0;
              }
            }
      //============================
            */
      //if($poreliminar==1){
            $equipo=$idequipo;
            $serie=$serieid;
            $this->ModeloCatalogos->updateCatalogo('series_productos',array('status'=>0,'bodegaId'=>2,'usado'=>1),array('serieId'=>$serie));
            $this->Configuraciones_model->registrarmovimientosseries($serie,1);
            //$this->ModeloCatalogos->getdeletewheren('asignacion_series_r_equipos',array('id_equipo'=>$equipo,'serieId'=>$serie));
            $this->ModeloCatalogos->updateCatalogo('asignacion_series_r_equipos',array('activo'=>0),array('id_equipo'=>$equipo,'serieId'=>$serie));
            $this->ModeloCatalogos->updatestock('rentas_has_detallesEquipos','cantidad','-',1,'id',$equipo);
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_contrato_a_e',array('status'=>3,'motivosuspencion'=>'Eliminacion y/o Cambio'),array('serieId'=>$serie,'idequipo'=>$equipo,'fecha >='=>$this->fechahoy,'status'=>0));
            //==========================================================
                $resultac=$this->ModeloCatalogos->db13_getselectwheren('rentas_has_detallesEquipos_accesorios',array('rowequipo'=>$equipo));
                foreach ($resultac->result() as $itemac) {
                    $id_accesoriod=$itemac->id_accesoriod;//idrow
                    $id_accesorio=$itemac->id_accesorio;//modelo
                    $resultac2=$this->ModeloCatalogos->db13_getselectwheren('asignacion_series_r_accesorios',array('id_accesorio'=>$id_accesoriod));
                    foreach ($resultac2->result() as $itemara) {
                        //$itemara->serieId;
                        $this->ModeloCatalogos->updateCatalogo('series_accesorios',array('status'=>0),array('serieId'=>$itemara->serieId));
                        $this->Configuraciones_model->registrarmovimientosseries($itemara->serieId,2);
                        $this->ModeloCatalogos->Insert('bitacora_movimientos',array('entrada_salida'=>1,'descripcion_evento'=>'Entrada del accesorio por retiro por ultimo contador','personal'=>$this->idpersonal,'tipo'=>2,'id_item'=>$id_accesoriod,'modeloId'=>$id_accesorio,'modelo'=>'','serieId'=>$itemara->serieId,'bodega_d'=>0,'clienteId'=>0));
                    }
                }
                //$this->ModeloCatalogos->getdeletewheren('rentas_has_detallesEquipos_accesorios',array('rowequipo'=>$equipo)); se deja comentado y se vera si se ocupa
                //$this->ModeloCatalogos->getdeletewheren('rentas_has_detallesequipos_consumible',array('rowequipo'=>$equipo)); se deja comentado y se vera si se ocupa

            //==========================================================
                $resultve=$this->ModeloCatalogos->db13_getselectwheren('rentas_has_detallesEquipos',array('id'=>$equipo));
                foreach ($resultve->result() as $itemv) {
                    $idmodelo=$itemv->idEquipo;
                    $modeloeq=$itemv->modelo;
                    if($itemv->cantidad<=0){
                        $this->ModeloCatalogos->updateCatalogo('rentas_has_detallesEquipos',array('estatus'=>0),array('id'=>$equipo));
                    }
                }
            //==========================================================
            $this->ModeloCatalogos->Insert('bitacora_movimientos',array('entrada_salida'=>1,'descripcion_evento'=>'Entrada del equipo por retiro equipo:'.$idequipo.' serie:'.$serieid.' por ultimo contador','personal'=>$this->idpersonal,'tipo'=>1,'id_item'=>$equipo,'modeloId'=>$idmodelo,'modelo'=>$modeloeq,'serieId'=>$serieid,'bodega_d'=>2,'clienteId'=>0));
      //}
    }
}
?>