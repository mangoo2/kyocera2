<style type="text/css">
  table td{
    font-size: 12px;
    padding: 6px 7px;
  }

</style>
<!-- START CONTENT -->
    <section id="content">
      <!--start container-->
        <div class="container">
          <div class="section">
          <div >
            <br>
                <h4 class="header">Listado de Clientes</h4>
                  <div class="col s12">
                    <table id="tabla_clientes" class="table display" cellspacing="0">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Cliente</th>
                          <th>Fecha de suspensión</th>
                          <th>Estado</th>
                          <th>Permitir Servicio</th>
                          <th>Permitir Venta</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php foreach ($result->result() as $item) { ?>
<tr><td><?php echo $item->id;?></td><td><?php echo $item->empresa;?></td>
                            <td><?php
                              if($item->bloqueo==1){
                                echo $item->bloqueo_reg;
                              }elseif($item->bloqueo_fp==1){
                                echo $item->bloqueo_fp_reg;
                              }
                             ?></td>
                            <td><?php
                              if($item->bloqueo==1){
                                echo 'Suspendido';
                              }elseif($item->bloqueo_fp==1){
                                echo 'Suspendido falta de pago';
                              }
                             ?></td>
                             <td><?php 
                                    if($item->bloqueo_perm_ven_ser==1){
                                      $checked=' checked ';
                                    }else{
                                      $checked='';
                                    }
                                    if($idpersonal==1 || $idpersonal==9 || $idpersonal==17 || $idpersonal==18){
                                      $disabled='';
                                    }else{
                                      $disabled=' disabled ';
                                    }
                                  ?><div class="switch"><br><label class="soloadministradores">Permitir<input type="checkbox" name="bloqueo" id="permitir_<?php echo $item->id;?>" onchange="permitir(<?php echo $item->id;?>)" <?php echo $checked.' '.$disabled;?> ><span class="lever"></span></label></div></td>



                              <td><?php 
                                    if($item->bloqueo_perm_ven==1){
                                      $checked=' checked ';
                                    }else{
                                      $checked='';
                                    }
                                    if($idpersonal==1 || $idpersonal==9 || $idpersonal==17 || $idpersonal==18){
                                      $disabled='';
                                    }else{
                                      $disabled=' disabled ';
                                    }
                                  ?><div class="switch"><br><label class="soloadministradores">Permitir<input type="checkbox" name="bloqueo" id="permitirv_<?php echo $item->id;?>" onchange="permitir(<?php echo $item->id;?>)" <?php echo $checked.' '.$disabled;?> ><span class="lever"></span></label></div></td>



                            <td><?php
                              if($item->bloqueo==1){
                              }elseif($item->bloqueo_fp==1){
                                ?><a class="b-btn b-btn-primary" onclick="obtenerventaspendientesinfo2(<?php echo $item->id;?>)" title="comentario"><i class="fas fa-comment fa-fw"></i></a><?php
                              }
                             ?></td>
                          </tr>
                        <?php }?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
          </div>  
        </div>
    </section>