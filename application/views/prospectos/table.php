

<?php 
    if($estatus==5)
    {
        $labelEstatus = "Cliente";
        $checkProspecto = "check_circle";
        $labelProspecto = "Realizado";
        $checkCotizacion = "check_circle";
        $labelCotizacion = "Realizado";
        $checkCliente = "check_circle";
        $labelCliente = "Realizado";
        $checkFactura = "check_circle";
        $labelFactura = "Realizado";
        $checkEntrega = "check_circle";
        $labelEntrega = "Realizado";
    }
    else if($estatus==4)
    {
        $labelEstatus = "Cliente";
        $checkProspecto = "check_circle";
        $labelProspecto = "Realizado";
        $checkCotizacion = "check_circle";
        $labelCotizacion = "Realizado";
        $checkCliente = "check_circle";
        $labelCliente = "Realizado";
        $checkFactura = "check_circle";
        $labelFactura = "Realizado";
        $checkEntrega = "close";
        $labelEntrega = "Pendiente";
    }
    else if($estatus==3)
    {
        $labelEstatus = "Cliente";
        $checkProspecto = "check_circle";
        $labelProspecto = "Realizado";
        $labelCotizacion = "Realizado";
        $checkCotizacion = "check_circle";
        $checkCliente = "check_circle";
        $labelCliente = "Realizado";
        $checkFactura = "close";
        $labelFactura = "Pendiente";
        $checkEntrega = "close";
        $labelEntrega = "Pendiente";
    }
    else if($estatus==2)
    {
        $labelEstatus = "Cotización";
        $checkProspecto = "check_circle";
        $labelProspecto = "Realizado";
        $checkCotizacion = "check_circle";
        $labelCotizacion = "Realizado";
        $checkCliente = "close";
        $labelCliente = "Pendiente";
        $checkFactura = "close";
        $labelFactura = "Pendiente";
        $checkEntrega = "close";
        $labelEntrega = "Pendiente";

    }
    else if($estatus==1)
    {
        $labelEstatus = "Prospecto";
        $checkProspecto = "check_circle";
        $labelProspecto = "Realizado";
        $checkCotizacion = "close";
        $labelCotizacion = "Pendiente";
        $checkCliente = "close";
        $labelCliente = "Pendiente";
        $checkFactura = "close";
        $labelFactura = "Pendiente";
        $checkEntrega = "close";
        $labelEntrega = "Pendiente";
    }

?>


<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body >
        <p>
            Oportunidad: <?php echo $datosProspectoCliente->empresa; ?><br>
            Asesor de ventas: Guillermina<br>
            Estatus: <?php echo $labelEstatus; ?><br>
        </p>

        <div class="row">
                    <div class="col s12 m1">
                      
                    </div>
                    <div class="col s12 m2">
                      <div class="card gradient-shadow gradient-45deg-light-blue-cyan border-radius-3">
                        <div class="card-content center">
                          <i class="material-icons"><?php echo $checkProspecto; ?></i>
                          <h5 class="white-text lighten-4"><?php echo $labelProspecto; ?></h5>
                          <p class="white-text lighten-4">Prospecto</p>
                        </div>
                      </div>
                    </div>
                    <div class="col s12 m2">
                      <div class="card gradient-shadow gradient-45deg-red-pink border-radius-3">
                        <div class="card-content center">
                          <i class="material-icons"><?php echo $checkCotizacion; ?></i>
                          <h5 class="white-text lighten-4"><?php echo $labelCotizacion; ?></h5>
                          <p class="white-text lighten-4">Cotización</p>
                        </div>
                      </div>
                    </div>
                    <div class="col s12 m2">
                      <div class="card gradient-45deg-blue-indigo gradient-shadow min-height-100 white-text border-radius-3">
                        <div class="card-content center">
                          <i class="material-icons"><?php echo $checkCliente; ?></i>
                          <h5 class="white-text lighten-4"><?php echo $labelCliente; ?></h5>
                          <p class="white-text lighten-4">Cliente</p>
                        </div>
                      </div>
                    </div>
                    <div class="col s12 m2">
                      <div class="card gradient-shadow gradient-45deg-amber-amber border-radius-3">
                        <div class="card-content center">
                          <i class="material-icons"><?php echo $checkFactura; ?></i>
                          <h5 class="white-text lighten-4"><?php echo $labelFactura; ?></h5>
                          <p class="white-text lighten-4">Factura</p>
                        </div>
                      </div>
                    </div>
                    
                    <div class="col s12 m2">
                      <div class="card gradient-shadow gradient-45deg-green-teal border-radius-3">
                        <div class="card-content center">
                          <i class="material-icons"><?php echo $checkEntrega; ?></i>
                          <h5 class="white-text lighten-4"><?php echo $labelEntrega; ?></h5>
                          <p class="white-text lighten-4">Entrega / Servicio</p>
                        </div>
                      </div>
                    </div>
                    
                  </div>
    </body>
</html>