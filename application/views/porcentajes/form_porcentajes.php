<section id="content" width="100%">
          

          <!--start container-->
          <div class="container" width="100%">
            <div class="section">
              <div class="row">
                <p class="caption col s6">Configuración de Porcentajes de Ganancia.</p>
                
                
              </div>
              <div class="row" >
                <input id="base_url" type="hidden" name="base_url" value="<?php echo base_url(); ?>">
                
                <!-- FORMULARIO -->
                <form class="col s12 " id="porcentajes-form">

                  <div class="col s12 m12 24" >

                    <div class="card-panel" align="center">

                      <div class="row">
                        <div class="row" >

                          <div class="input-field col s2">
                            <label>Porcentaje de Ganancia Equipos</label>
                          </div>
                          <div class="input-field col s4">
                            <i class="material-icons prefix">attach_money</i>
                            <input id="cambio" name="cambio" type="text" placeholder="Equipos" value="<?php echo $cambio; ?>">
                          </div>                          

                          <div class="input-field col s2">
                            <label>Porcentaje de Ganancia Refacciones</label>
                          </div>
                          <div class="input-field col s4">
                            <i class="material-icons prefix">attach_money</i>
                            <input id="cambio" name="cambio" type="text" placeholder="Refacciones" value="<?php echo $cambio; ?>">
                          </div>  

                        </div>

                        <div class="row" >

                          <div class="input-field col s2">
                            <label>Porcentaje de Ganancia Consumibles</label>
                          </div>
                          <div class="input-field col s4">
                            <i class="material-icons prefix">attach_money</i>
                            <input id="cambio" name="cambio" type="text" placeholder="Consumibles" value="<?php echo $cambio; ?>">
                          </div>                          

                          <div class="input-field col s2">
                            <label>Porcentaje de Ganancia Accesorios</label>
                          </div>
                          <div class="input-field col s4">
                            <i class="material-icons prefix">attach_money</i>
                            <input id="cambio" name="cambio" type="text" placeholder="Accesorios" value="<?php echo $cambio; ?>">
                          </div>

                        </div>

                      </div>

                      <div class="row">
                        <div class="row" >
                          <div class="input-field col s2">
                            <button id="button" name="apellido_paterno" class="btn waves-effect waves-light green darken-1"> Actualizar</button> 
                          </div>
                        </div>
                      </div>

                    </div>

                  </div>

                </form>
              </div>

          </div>
        </div>
      </section>