function loadp(){
  var cliente= $('#cliente option:selected').val()==undefined?0:$('#cliente option:selected').val();
  var estatus= $('#tipoestatus option:selected').val();
  var personal = $('#ejecutivoselect option:selected').val();
  var fec_ini_reg=$('#fechainicial_reg').val();
  var fechafinal_reg = $('#fechafinal_reg').val();
  var emp=$('#empresaselect option:selected').val();
  var activo=$('#tipoventadelete option:selected').val();
  var rfc =$('#cli_rfc option:selected').val()==undefined?'':$('#cli_rfc option:selected').val();
  var pre =$('#s_prefactura option:selected').val();
  table.destroy();
  table = $('#tabla_ventas_incompletas').DataTable({
        //pagingType: 'full_numbers',
           pagingType: 'input',
           //"sPaginationType": "listbox",
        search: {
                return: true
        }, 
        //stateSave: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url+"index.php/PolizasCreadas/getListadocontratosPolizasIncompletasasi",
            type: "post",
            "data": {
                'cliente':cliente,
                'status':estatus,
                'personal':personal,
                'fec_ini_reg':fec_ini_reg,
                'fechafinal_reg':fechafinal_reg,
                'emp':emp,
                'activo':activo,
                'rfc':rfc,
                'pre':pre,
                'viewp':0
            },
        },
        "columns": [
            {"data": "id"},
            {"data": "empresa"},
            {"data": "id",
                render:function(data,type,row){
                    var colorbtton='blue';
                    if (row.prefactura==1) {
                        colorbtton='green';
                    }
                    var html='<a class="btn-floating '+colorbtton+' tooltipped" data-position="top" data-delay="50" data-tooltip="Prefactura" onclick="detallep('+row.id+')"><i class="material-icons">assignment</i></a>';
                    if(row.viewcont==1){
                        if (row.prefactura==1) {
                            html+='<a href="'+base_url+'PolizasCreadas/servicios/'+row.id+'" class="btn-floating tooltipped" data-position="top" data-delay="50" data-tooltip="Servicio" target="_black"><i class="fas fa-file-invoice"></i></a>';
                        }
                    }
                    return html;
                }
            },
            {"data": "nombre"},
            {"data": "estatus",
                render:function(data,type,row){
                    var html='';
                    if(data==1){
                        html='Pendiente';
                    }else if(data==3){
                        html='Pagado';
                    }
                    return html;
                }
            },
            
            {"data": 'reg'},
            {"data": null,
                 render:function(data,type,row){
                    var html='';
                    if (row.prefactura==1) {
                        if(row.vencimiento!='0000-00-00'){
                           var html=row.vencimiento; 
                        }
                    }  
                    return html;
                }
            }, 
            {"data": "tipo", visible: false},
            {"data": null,
                 render:function(data,type,row){
                    var html='';
                    if (row.tipov==1) {
                        html='Alta Productividad';    
                    }
                    if (row.tipov==2) {
                        html='D-Impresión';    
                    }  
                    return html;

                }
            },
            {"data": "facturas",
                render:function(data,type,row){
                    var html='';
                    if(row.siniva==1){
                        var totalventa=(parseFloat(row.totalpoliza)).toFixed(2);
                    }else{
                        var totalventa=(parseFloat(row.totalpoliza)+(parseFloat(row.totalpoliza)*0.16)).toFixed(2);
                    }
                    
                    html+=new Intl.NumberFormat('es-MX',{style: "currency", currency: "MXN"}).format(totalventa);
                    return html;
                }
            },
            {"data": "facturas",
                render:function(data,type,row){
                    var html='';
                    if(row.facturas!='' && row.facturas!='null' && row.facturas!=null){
                        html+=row.facturas;
                        html+=' <a class="b-btn b-btn-primary tooltipped" data-position="top" data-delay="50" data-tooltip="Desvicular Factura" onclick="desvfactura('+row.id+',3)"><i class="fas fa-unlink"></i></a>';
                    }
                        
                    return html;
                }
            }, 

            {"data": null,
                 render:function(data,type,row){
                    var html='';
                    if(row.siniva==1){
                        var totalventa=parseFloat(row.totalpoliza).toFixed(2);
                    }else{
                        var totalventa=(parseFloat(row.totalpoliza)+(parseFloat(row.totalpoliza)*0.16)).toFixed(2);
                    }
                    if(row.facturas!='' && row.facturas!='null' && row.facturas!=null){
                        var fac=1;
                    }else{
                        var fac=0;
                    }
                    $('.tooltipped').tooltip();
                    var btn_cfdi='';
                        var btn_pago='';
                        if (row.prefactura==1) {
                           btn_cfdi='<a class="btn-floating blue tooltipped" data-position="top" data-delay="50" data-tooltip="CFDI" onclick="modal_cdfi('+row.id+')">CFDI</a>';
                            if(totalventa>0){
                                if(row.estatus==3){
                                    var colorbuton='green';
                                }else{
                                    var colorbuton='blue';
                                    //if(row.vencimiento!='0000-00-00'){
                                        console.log('Poliza '+row.id+' '+row.vencimiento+'<'+fechahoy);
                                        if(row.vencimiento<fechahoy){
                                            var colorbuton='red';
                                        }
                                    //}
                                }
                                btn_pago='<a class="btn-floating '+colorbuton+' tooltipped modal_pagosp verificarp_'+row.id+' modal_pago_4_'+row.id+'" data-cliente="'+row.idCliente+'" data-position="top" data-delay="50" data-tooltip="Pago" data-poliza="'+row.id+'" onclick="modal_pago('+row.id+',4)">\
                                        <i class="material-icons">attach_money</i></a>';
                            }
                        }
                    var html='';
                    if(activo==1){
                        html+='<a class="btn-floating red tooltipped facturar" data-position="top" data-delay="50" data-tooltip="Eliminar" onclick="finalizapo('+row.id+','+fac+')">\
                                    <i class="fas fa-trash-alt"></i></a>';
                    }
                                  html+=''+btn_pago+'';
                    if(parseInt(row.activo)==1){
                        if (row.prefactura==1 && totalventa>0) {
                            
                            html+='<a class="b-btn b-btn-primary tooltipped" data-position="top" data-delay="50" \
                                    data-tooltip="Agregar Factura"  onclick="addfactura('+row.id+','+row.idCliente+',2,'+totalventa+','+row.tipov+')"> \
                                            <i class="fas fa-folder-plus"></i></a>';
                        }
                    }
                    if(parseInt(row.activo)==1){
                        html+=' <a class="b-btn b-btn-primary tooltipped" data-position="top" data-delay="50" data-tooltip="Cambiar personal" onclick="cambiar_per('+row.id+',2)"><i class="fas fa-user fa-fw"></i></a>';
                        html+=' <a class="b-btn b-btn-primary tooltipped" data-position="top" data-delay="50" data-tooltip="Cambiar Empresa" onclick="cambiar_emp('+row.id+',2)"><i class="fas fa-hotel"></i></a> ';
                        if(row.facturas!='' && row.facturas!='null' && row.facturas!=null){}else{
                            html+=' <a class="b-btn b-btn-primary tooltipped" data-position="top" data-delay="50" data-tooltip="Cambiar de cliente" onclick="cambiar_cliente('+row.id+',2)"><i class="fas fa-people-arrows"></i></a> ';
                        }
                        if(totalventa>0){
                            if(row.facturas!='' && row.facturas!='null' && row.facturas!=null){}else{
                                html+='<a class="b-btn b-btn-primary tooltipped" data-position="top" data-delay="50" data-tooltip="Ir a Generación de factura" onclick="irfactura('+row.idCliente+',2,'+row.id+','+row.tipov+','+row.tipov+')"><i class="fas fa-file-invoice"></i></a>';
                            }
                        }

                    }
                    if(row.requ_fac==1){
                        var r_f_i='b-btn-primary';
                        var r_f_it='Requiere factura';
                    }else{
                        var r_f_i='b-btn-warning';
                        var r_f_it='No requiere factura';
                    }
                    html+=' <a class="b-btn '+r_f_i+' tooltipped" data-position="top" data-delay="50" data-tooltip="'+r_f_it+'" onclick="requiere_factura('+row.id+',3,'+row.requ_fac+')"><i class="fas fa-file-invoice"></i></a> ';
                    
                    return html;
                }
            }
        ],
        "order": [[ 0, "desc" ]],
        createdRow:function(row,data,dataIndex){
            $(row).find('td:eq(7)')
                .addClass('obtencionfacturas ob_fac_pol_'+data.id)
                .attr('data-poliza',data.id);
        }
        
    }).on('draw',function(){
        $('.dataTables_filter').append('<i class="fa fa-search buttonsearch" onclick="searchtable_v()"></i>');
        vericarpagospoliza();
    });
}
function vericarpagospoliza(){
	console.log('vericarpagospoliza');
    var DATAf  = [];
    $(".modal_pagosp").each(function() {
        item = {};
        item ["poliza"] = $(this).data('poliza');
        DATAf.push(item);
    });
    aInfo   = JSON.stringify(DATAf);
    //console.log(aInfo);
    var datos=aInfo;
    
    $.ajax({
        type:'POST',
        url: base_url+"Generales/vericarpagospoliza",
        data: {
            datos:aInfo
        },
        success: function (data){
            //console.log(data);
            var array = $.parseJSON(data);
            console.log(array);
            $.each(array, function(index, item) {
                $(".verificarp_"+item.idpoliza).addClass( "yellowc" );
                
            });
        }
    });
}
function monto_poliza(id_v){//poliza monto de prefactura
    var result='';
    $.ajax({
      type: "POST",
      url: base_url+"index.php/PolizasCreadas/montopoliza",
      data:{id:id_v},
      async:false,
      success: function(data){
        result=data;
      }
    });   
    return result;
}
function deletepagop(idpago){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: 'Se necesita permisos de administrador<br>'+
                 '<input type="password" placeholder="Contraseña" id="contrasena" class="name form-control" required/>',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var pass=$('#contrasena').val();
                if (pass!='') {
                    $.ajax({
				        type: "POST",
				        url: base_url+"index.php/PolizasCreadas/vericar_pass",
				        data:{
				            pago:idpago,
				            pass:pass
				        },
				        success: function(data){
				            $('#ver_pass').val('');
				            $('#modal_eliminarpago').modal('close');
				            $('#modal_pago').modal('close');
				            if(parseFloat(data)==1){
				                $.alert({boxWidth: '30%',useBootstrap: false,title: 'Atención!',content: 'Pago Eliminado correctamente'});
				            }else{
				                swal({ title: "Error",text: "Verificar la contraseña",type: 'error',showCancelButton: false,allowOutsideClick: false,});
				            }
				        }
				    });

                }else{
                    notificacion(0);
                }
                
            },
            cancelar: function (){
                
            }
        }
    });
}
function finalizapo(id_v,fac){
    if(fac==1){
        alertfunction('Atención!','Cancele la factura antes de continuar');
    }else{
        $.confirm({
            boxWidth: '30%',
            useBootstrap: false,
            icon: 'fa fa-warning',
            title: 'Atención!',
            content: '¿Está seguro de eliminar?<br>Se necesita permisos de administrador<br>'+
                     '<input type="text" placeholder="Contraseña" id="contrasena" name="contrasena" class="name form-control-bmz" style="width:99%" required/>',
            type: 'red',
            typeAnimated: true,
            buttons:{
                confirmar: function (){
                    var pass=$("input[name=contrasena]").val();
                    if (pass!='') {
                        $.ajax({
                            type:'POST',
                            url: base_url+"index.php/Sistema/solicitarpermiso",
                            data: {
                                pass:pass
                            },
                            success: function (response){
                                    var respuesta = parseInt(response);
                                    if (respuesta==1) {
                        //=============================================================================
                                    $.ajax({
                                      type: "POST",
                                      url: base_url+"index.php/PolizasCreadas/finalizar_factura",
                                      data:{id:id_v},
                                      success: function(data){
                                        var status= parseFloat(data);
                                        if (status==1) {
                                            $.alert({boxWidth: '30%',useBootstrap: false,title: 'Atención!',content: 'Eliminado correctamente'});
                                            loadp(); 
                                        }else{
                                            $.alert({boxWidth: '30%',useBootstrap: false,title: 'Atención!',content: 'No es posible eliminar, Prefactura de póliza facturada'});
                                        }
                                      }
                                    });
                        //=============================================================================
                                    }else{
                                        notificacion(2);
                                    }
                            },
                            error: function(response){  
                                 notificacion(1);
                            }
                        });

                    }else{
                        notificacion(0);
                    }
                },
                cancelar: function () {
                }
            }
        });
        setTimeout(function(){
            new MaskedPassword(document.getElementById("contrasena"), '\u25CF');
        },1000);
    }
}
function detallep(id){
    window.open(base_url+"PolizasCreadas/view/"+id, "Prefactura", "width=780, height=612");
}