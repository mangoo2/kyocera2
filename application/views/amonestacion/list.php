<style type="text/css">
  #tabla_amo td{
    font-size: 13px;
  }
  .info_clipro{
    float: inline-end;
  }
  .serinfo{
    min-width: 250px;
  }
</style>
<!-- START CONTENT -->
    <section id="content">
    	<!--start container-->
      	<div class="container">
      		<div class="section">


      		<div>
      			
                <h4 class="header">Reporte de amonestaciones</h4>
                <div class="row">
                  <div class="col s3">
                    <label>Ejecutivo</label>
                    <select class="browser-default form-control-bmz" id="eje" onchange="loadtable()">
                      <option value="0">Todos</option>
                      <?php foreach ($personalrows as $item) { 
                          echo '<option value="'.$item->personalId.'">'.$item->tecnico.'</option>';
                      } ?>
                    </select>
                  </div>
                  <div class="col s3">
                    <label>Fecha</label>
                    <input type="date" class="form-control-bmz" id="fecha" onchange="loadtable()">
                  </div>
                </div>
                <div class="row">
                    <div class="col s12">
                      <ul class="tabs tab-demo z-depth-1">
                        <li class="tab col s3"><a class="active" href="#test1">Servicios</a>
                        </li>
                        <li class="tab col s3"><a href="#test2" class="">Rechazos</a>
                        </li>
                      <li class="indicator" style="right: 1212px; left: 0px;"></li></ul>
                    </div>
                    <div class="col s12">
                      <div id="test1" class="col s12 active" style="">
                        <!------------------------------------------------->
                          <table id="tabla_amo" class="responsive-table display" cellspacing="0">
                            <thead>
                              <tr>
                                <th>Fecha</th>
                                <th>Tecnico</th>
                                <th>Cliente</th>
                                <th>Servicio</th>
                                <th>Fecha y hora<br>agendado</th>
                                <th>Fecha y hora<br>atendido</th>
                                <th></th>
                                <th></th>
                              </tr>
                            </thead>
                            <tbody>
                            </tbody>
                          </table>
                        <!------------------------------------------------->
                      </div>
                      <div id="test2" class="col s12" style="display: none;">
                        <!------------------------------------------------->
                        <div class="info_r_rechazo"></div>
                        <!------------------------------------------------->
                      </div>
                      
                    </div>
                  </div>
              </div>
            </div>  
               </div>
        </section>
<table id="tempTable" >
    <thead>
        <!-- Definir encabezados si es necesario -->
    </thead>
    <tbody>
    </tbody>
</table>