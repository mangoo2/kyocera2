<?php

class Cotizaciones_model extends CI_Model 
{
    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
        $this->DB10 = $this->load->database('other10_db', TRUE);
    }

    /**************** INSERCIONES ****************/
    /*********************************************/
    public function insertarCotizacion($dato){
	    $this->db->insert('cotizaciones',$dato);   
	    return $this->db->insert_id();
    } 

    public function insertarDetallesCotizacionPoliza($dato){
	    $this->db->insert('cotizaciones_has_detallesPoliza',$dato);   
	    $this->db->insert_id();
    } 

    public function insertarDetallesCotizacionEquipos($dato){
        $this->db->insert('cotizaciones_has_detallesEquipos',$dato);   
        return $this->db->insert_id();
    } 
    // ya no se ocupa
    public function insertarDetallesCotizacionConsumibles($dato){
        $this->db->insert('cotizaciones_has_detallesConsumibles',$dato);   
        $this->db->insert_id();
    } 
    public function insertarVentasDetallesCotizacionConsumibles($dato){
        $this->db->insert('ventas_has_detallesConsumibles',$dato);   
        $this->db->insert_id();
    } 

    // ya no se ocupa
    public function insertarDetallesCotizacionRefacciones($dato){
        $this->db->insert('cotizaciones_has_detallesRefacciones',$dato);   
        $this->db->insert_id();
    } 

    /**************** SELECTS ****************/
    /*****************************************/

    public function getDatosCotizacion($idPoliza){
    	$sql = "SELECT * FROM cotizaciones WHERE id=".$idPoliza;
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getDatosCotizacionPorIdProspectoCliente($idCliente){
        $sql = "SELECT * FROM cotizaciones WHERE idCliente=".$idCliente;
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getDetallesCotizacionPoliza($idCotizacion){
    	$sql = "SELECT * FROM cotizaciones_has_detallesPoliza WHERE idCotizacion=".$idCotizacion;
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getDetallesCotizacionPolizaTabla($idCotizacion){
    	$sql = "SELECT id as 'DT_RowId',
				idPoliza,
				idEquipo,
				idCotizacion,
				cubre,
				vigencia_meses,
				vigencia_clicks,
				modelo,
				precio_local,
				precio_semi,
				precio_foraneo,
				nombre FROM cotizaciones_has_detallesPoliza WHERE idCotizacion=".$idCotizacion;
        $query = $this->db->query($sql);
        return $query->result();
    }	

    public function getDetalleCotizacionPolizaPorId($idDetalleCotizacionPoliza){
    	$sql = "SELECT id as 'DT_RowId',
				idPoliza,
				idEquipo,
				idCotizacion,
				cubre,
				vigencia_meses,
				vigencia_clicks,
				modelo,
				precio_local,
				precio_semi,
				precio_foraneo,
				nombre FROM cotizaciones_has_detallesPoliza WHERE id=".$idDetalleCotizacionPoliza;
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getDetallesCotizacionEquiposTabla($idCotizacion){
        $sql = "SELECT
                ce.id as 'DT_RowId',
                ce.idEquipo,
                ce.idCotizacion,
                e.foto,
                e.especificaciones_tecnicas,
                ce.modelo, 
                ce.cantidad,
                ce.excedente,
                ce.precio,

                CONCAT('Toner Black: $ ',ce.costo_toner,'. Rinde: ',ce.rendimiento_toner, 'pag.') as 'toner_black',
                CONCAT('Toner CMY: $ ',ce.costo_toner_c+ce.costo_toner_m+ce.costo_toner_y,'. Rinde: ',ce.rendimiento_toner_cmy, 'pag.') as 'toner_cmy',
                ce.costo_toner_cmy,
                ce.costo_toner_c,
                ce.costo_toner_m,
                ce.costo_toner_y,
                ce.rendimiento_unidad_imagen as 'paginas',
                CONCAT('Garantia y Servicio: $ ',ce.costo_garantia_servicio,'. Rendimiento: ',ce.rendimiento_garantia_servicio, ' pag.') as 'garantia_servicio',
                ce.costo_pagina_monocromatica,
                ce.costo_pagina_color,
                e.categoriaId,
                cate.tipo,
                ce.r_costo_m,
                ce.r_volumen_m,
                ce.r_costo_c,
                ce.r_volumen_c,
                e.excedente as excedentem,
                e.excedentecolor as excedentec
                FROM cotizaciones_has_detallesEquipos AS ce
                INNER JOIN equipos as e ON ce.idEquipo=e.id
                LEFT join categoria_equipos as cate on cate.id=e.categoriaId
                WHERE idCotizacion=".$idCotizacion;
        $query = $this->db->query($sql);
        return $query;
    }
    public function getDetallesCotizacionEquiposaccTabla($idCotizacion){
        $sql = "SELECT 
                    acc.*,
                    cacc.nombre,
                    cacc.observaciones
                    FROM `cotizaciones_has_detallesEquipos_accesorios` as acc 
                    INNER JOIN catalogo_accesorios as cacc on cacc.id=acc.id_accesorio 
                    left join equipos as eq on eq.id=acc.id_equipo
                    WHERE acc.id_cotizacion=$idCotizacion and acc.costo>0";
        $query = $this->db->query($sql);
        return $query;
    }

    public function getDetallesCotizacionEquiposPorIdCotizacion($idCotizacion){
        $sql = "SELECT *
                FROM cotizaciones_has_detallesEquipos AS ce
                WHERE ce.idCotizacion=".$idCotizacion;
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getDetallesCotizacionPolizasPorIdCotizacion($idCotizacion){
        $sql = "SELECT *
                FROM cotizaciones_has_detallesPoliza AS cp
                WHERE cp.idCotizacion=".$idCotizacion;
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getListaCotizacionesPorCliente($idCliente,$denegada){

        $sql = "SELECT *
                FROM cotizaciones as c 
                WHERE c.idCliente='$idCliente' and c.denegada='$denegada'";
        $query = $this->db->query($sql);
        return $query->result();
    }
    function getListaCotizaciones($params){
        $cli=$params['cli'];
        $tipo=$params['tipo'];
        if($tipo==5){
            $where = " and c.rentaVentaPoliza='2' ";
        }else{
            $where = " and c.rentaVentaPoliza='1' and (c.tipo='$tipo' or c.tipog like '%$tipo%' ) ";
        }
        $sql = "SELECT *
                FROM cotizaciones as c 
                WHERE c.idCliente='$cli' and c.denegada='0'";
        $query = $this->db->query($sql);
        return $query;
    }

    public function getDetalleEquiposPorIdCotizacion($idCotizacion){
        $sql = "SELECT * from cotizaciones_has_detallesEquipos WHERE idCotizacion=".$idCotizacion;
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function getDetallePolizasPorIdCotizacion($idCotizacion){
        $sql = "SELECT * from cotizaciones_has_detallesPoliza WHERE idCotizacion=".$idCotizacion;
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function getDetallesCotizacionRefaccionesTabla($idCotizacion){
        $sql = "SELECT
                cr.id as 'DT_RowId',
                cr.piezas,
                cr.modelo,
                cr.serieequipo,
                e.modelo as 'equipo',
                r.rendimiento,
                cr.descripcion,
                cr.precioGeneral,
                cr.totalGeneral,
                cr.idRefaccion,
                r.codigo
                FROM cotizaciones_has_detallesRefacciones as cr
                INNER JOIN equipos as e ON cr.idEquipo=e.id
                INNER JOIN refacciones as r ON cr.idRefaccion=r.id
                WHERE cr.idCotizacion=".$idCotizacion;
        $query = $this->DB10->query($sql);
        return $query;
    }
    public function getDetalleCotizacionEquipoPorId($idDetalleCotizacionEquipo){
        $sql = "SELECT
                ce.id as 'DT_RowId',
                e.foto,
                ce.modelo, 
                ce.precio,
                CONCAT('Toner Black: $ ',ce.costo_toner_black,'. Rinde: ',ce.rendimiento_toner_black, 'pag.') as 'toner_black',
                CONCAT('Toner CMY: $ ',ce.costo_toner_cmy,'. Rinde: ',ce.rendimiento_toner_cmy, 'pag.') as 'toner_cmy',
                ce.rendimiento_unidad_imagen as 'paginas',
                CONCAT('Garantia y Servicio: $ ',ce.costo_garantia_servicio,'. Rendimiento: ',ce.rendimiento_garantia_servicio, ' pag.') as 'garantia_servicio',
                ce.costo_pagina_monocromatica,
                ce.costo_pagina_color
                FROM cotizaciones_has_detallesEquipos AS ce
                LEFT JOIN equipos as e ON ce.idEquipo=e.id
                WHERE ce.id=".$idDetalleCotizacionEquipo;
        $query = $this->DB10->query($sql);
        return $query->result();
    }
    public function getDetallesCotizacionConsumiblesTabla($idCotizacion){
        $sql = "SELECT
                cc.id as 'DT_RowId',
                cc.piezas,
                cc.modelo,
                e.modelo as 'equipo',
                c.rendimiento,
                c.rendimiento_toner,
                c.parte,
                cc.descripcion,
                cc.precioGeneral,
                cc.totalGeneral,
                cc.idConsumible,
                c.rendimiento_unidad_imagen,
                c.tipo
                FROM cotizaciones_has_detallesConsumibles as cc
                left JOIN equipos as e ON cc.idEquipo=e.id
                INNER JOIN consumibles as c ON cc.idConsumible=c.id
                WHERE cc.idCotizacion=".$idCotizacion;
        $query = $this->db->query($sql);
        return $query;
    }
    public function getDetalleCotizacionConsumiblePorId($idDetalleCotizacionConsumible){
        $sql = "SELECT
                cc.id as 'DT_RowId',
                cc.piezas,
                cc.modelo,
                e.modelo as 'equipo',
                c.rendimiento,
                cc.descripcion,
                cc.precioGeneral,
                cc.totalGeneral,
                cc.idConsumible
                FROM cotizaciones_has_detallesConsumibles as cc
                INNER JOIN equipos as e ON cc.idEquipo=e.id
                INNER JOIN consumibles as c ON cc.idConsumible=c.id
                WHERE cc.id=".$idDetalleCotizacionConsumible;
        $query = $this->DB10->query($sql);
        return $query->result();
    }
    public function getDetalleCotizacionRefaccionPorId($idDetalleCotizacionPoliza){
        $sql = "SELECT
                cr.id as 'DT_RowId',
                cr.piezas,
                cr.modelo,
                e.modelo as 'equipo',
                r.rendimiento,
                cr.descripcion,
                cr.precioGeneral,
                cr.totalGeneral,
                cr.idRefaccion
                FROM cotizaciones_has_detallesRefacciones as cr
                INNER JOIN equipos as e ON cr.idEquipo=e.id
                INNER JOIN refacciones as r ON cr.idRefaccion=r.id
                WHERE cr.id=".$idDetalleCotizacionPoliza;
        $query = $this->DB10->query($sql);
        return $query->result();
    }
    public function verificaFamilia($idFamilia){
        $sql = "SELECT COUNT(id) as 'total' FROM equipos WHERE idfamilia=".$idFamilia."  AND estatus=1";
        $query = $this->db->query($sql);
        return $query->row();
    }
    /***************** UPDATES ******************/
    /*********************************************/
    public function actualizaCotizacion($data, $idCotizacion){
        $this->db->set($data);
        $this->db->where('id', $idCotizacion);
        return $this->db->update('cotizaciones');
    }
    public function actualizaDetalleCotizacionPoliza($data, $idDetallePoliza){
        $this->db->set($data);
        $this->db->where('id', $idDetallePoliza);
        return $this->db->update('cotizaciones_has_detallesPoliza');
    }
    public function actualizaDetalleCotizacionEquipo($data, $idDetallePoliza){
        $this->db->set($data);
        $this->db->where('id', $idDetallePoliza);
        return $this->db->update('cotizaciones_has_detallesEquipos');
    }   
    public function actualizaDetalleCotizacionConsumible($data, $idDetallePoliza){
        $this->db->set($data);
        $this->db->where('id', $idDetallePoliza);
        return $this->db->update('cotizaciones_has_detallesConsumibles');
    }
    public function actualizaDetalleCotizacionRefaccion($data, $idDetallePoliza){
        $this->db->set($data);
        $this->db->where('id', $idDetallePoliza);
        return $this->db->update('cotizaciones_has_detallesRefacciones');
    }
    public function getcotizacionaccesorios($idcotizacion,$equipo){
        $sql = "SELECT cequ.id_accesoriod, acc.id,equ.modelo, acc.nombre,acc.costo,cequ.cantidad,cequ.costo as cequ_costo
            FROM cotizaciones_has_detallesEquipos_accesorios as cequ
            inner JOIN equipos as equ on equ.id=cequ.id_equipo
            inner JOIN catalogo_accesorios as acc on acc.id=cequ.id_accesorio
            WHERE cequ.id_cotizacion=$idcotizacion and cequ.id_equipo=$equipo and cequ.costo is null";
        $query = $this->db->query($sql);
        return $query;
    }
    //====================================================================
    function pagmonocromaticas($idequipo){
        $sql = "SELECT pag_monocromo_incluidos
            FROM equipos 
            WHERE id=$idequipo";
        $query = $this->db->query($sql);
        $paginas=0;
        foreach ($query->result() as $item) {
            $paginas=$item->pag_monocromo_incluidos;
        }
        return $paginas;
    }
    function cotizacionequipospoliza($idcotizacion){
        $sql = "SELECT cdp.id,cdp.idCotizacion,cdp.idEquipo,eq.modelo,cdp.serie,cdp.notifi_precio,cdp.precio_local,cdp.precio_semi,cdp.precio_foraneo,cdp.precio_especial,cdp.idPoliza
                FROM cotizaciones_has_detallesPoliza as cdp
                inner join equipos as eq on eq.id=cdp.idEquipo
                WHERE cdp.idCotizacion=$idcotizacion
                GROUP by cdp.idEquipo";
        $query = $this->DB10->query($sql);
        return $query;
    }
    public function getDetallesCotizacionPolizaTablae($idCotizacion,$idEquipo){
        $sql = "SELECT * FROM cotizaciones_has_detallesPoliza 
                WHERE 
                idCotizacion=$idCotizacion and 
                idEquipo=$idEquipo 
                ";
        $query = $this->db->query($sql);
        return $query;
    }
    public function cotizacionequipospoliza2($idCotizacion){
        $sql = "SELECT eq.modelo as modeloeq,p.* FROM cotizaciones_has_detallesPoliza as p 
                inner join equipos as eq on eq.id=p.idEquipo
                WHERE 
                    p.idCotizacion=$idCotizacion

                ";
        $query = $this->db->query($sql);
        return $query;
    }
    public function getalldetallecontizaciones($id){
        $sql = "SELECT e.modelo AS equipo, cd.* 
                FROM cotizaciones_has_detallesConsumibles AS cd
                left JOIN equipos AS e ON e.id = cd.idEquipo
                WHERE cd.idCotizacion=$id";
        $query = $this->db->query($sql);
        return $query;  
    }
    public function getalldetallecontizacionesrefacciones($id){
        $sql = "SELECT e.modelo AS equipo, cd.* 
                FROM cotizaciones_has_detallesRefacciones  AS cd
                INNER JOIN equipos AS e ON e.id = cd.idEquipo
                WHERE cd.idCotizacion=$id";
        $query = $this->db->query($sql);
        return $query;  
    }
    function getcotizacionequipoconsumible($cotizacion,$equipo,$tipo){
        $sql = "SELECT 
                        cdec.id,
                        con.modelo,
                        cdec.costo_toner,
                        cdec.rendimiento_toner,
                        cdec.rendimiento_unidad_imagen 
                from cotizaciones_has_detallesEquipos_consumibles as cdec
                inner join consumibles as con on con.id=cdec.id_consumibles
                WHERE cdec.id_cotizacion=$cotizacion and cdec.id_equipo=$equipo and con.tipo=$tipo";
        $query = $this->DB10->query($sql);
        return $query;
    }
    public function getDatosCotizaciondetalles($id){
        $sql = "SELECT * FROM cotizaciones_has_detallesConsumibles WHERE id=$id";
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function getDatosCotizaciondetallesrefacciones($id){
        $sql = "SELECT * FROM cotizaciones_has_detallesRefacciones WHERE id=$id";
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function insertaventadetallesconsumibles($data) {
        $this->db->insert('ventas_has_detallesConsumibles', $data);
        return $this->db->insert_id();
    }
    public function insertaventadetallesrefacciones($data) {
        $this->db->insert('ventas_has_detallesRefacciones', $data);
        return $this->db->insert_id();
    }


    function getlistcotizaciones($params){
        //log_message('error', json_encode($params));
        $idCliente=$params['idCliente'];
        $denegada=$params['denegada'];
        $fi = $params['fi'];
        $ff = $params['ff'];
        $tipog = $params['tipog'];
        $tipocot = $params['tipocot'];
        $columns = array( 
            0=>'c.id',
            1=>'cli.empresa',
            2=>'c.tipo',
            3=>'c.rentaVentaPoliza',
            4=>'c.prioridad',
            5=>'c.estatus',
            6=>'c.reg',
            7=>'c.info2',
            8=>'c.id',
            9=>'c.id_personal',
            10=>'c.denegada',
            11=>'c.idCliente',
            12=>'c.tipog',
            13=>'c.motivodenegada',
            14=>"CASE
                    WHEN c.rentaVentaPoliza = 1 THEN (SELECT v.id FROM ventas v WHERE v.idCotizacion = c.id limit 1)
                    WHEN c.rentaVentaPoliza = 2 THEN (SELECT r.id FROM rentas r WHERE r.idCotizacion = c.id limit 1)
                    WHEN c.rentaVentaPoliza = 3 THEN (SELECT p.id FROM polizasCreadas p WHERE p.idCotizacion = c.id limit 1)
                    ELSE 'Valor de rentaVentaPoliza no válido'
                END AS resultado",
            15=>"IFNULL((SELECT GROUP_CONCAT(DISTINCT(cdp.serie) separator ', ') from cotizaciones_has_detallesPoliza as cdp where cdp.idCotizacion = c.id),'') as seriep",
            16=>"IFNULL((SELECT GROUP_CONCAT(DISTINCT(cdr.serieequipo) separator ', ') from cotizaciones_has_detallesRefacciones as cdr where cdr.idCotizacion = c.id),'') as serier",
            17=>'cli.sol_orden_com',
            18=>"IFNULL((SELECT GROUP_CONCAT(cdp.vigencia_clicks separator ',') from cotizaciones_has_detallesPoliza as cdp where cdp.idCotizacion = c.id),'') as visitas",
            19=>"IFNULL((SELECT COUNT(*) from cotizaciones_has_detallesPoliza as cdp where cdp.idCotizacion = c.id),'0') as numequipos",
            20=>"c.tipov",
            21=>"cli.bloqueo_perm_ven"
        );
        $columnssss = array( 
            0=>'c.id',
            1=>'cli.empresa',
            2=>'c.tipo',
            3=>'c.rentaVentaPoliza',
            4=>'c.reg',
            5=>'c.info2',
            6=>'c.motivodenegada',
            7=>"IFNULL((SELECT GROUP_CONCAT(cdp.serie separator ', ') from cotizaciones_has_detallesPoliza as cdp where cdp.idCotizacion = c.id),'')",
            8=>"IFNULL((SELECT GROUP_CONCAT(cdr.serieequipo separator ', ') from cotizaciones_has_detallesRefacciones as cdr where cdr.idCotizacion = c.id),'')",
            
        );

        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('cotizaciones c');
        $this->db->join('clientes cli', 'cli.id = c.idCliente');
        if($idCliente>0){
            $this->db->where(array('c.idCliente'=>$idCliente));
        }
        if($fi!=''){
            $this->db->where("c.reg >='$fi 00:00:00' ");
        }
        if($ff!=''){
            $this->db->where("c.reg <='$ff 23:59:59' ");
        }
        $this->db->where(array('c.denegada'=>$denegada));
        if($tipog>0){
            $this->db->like('c.tipog', $tipog, 'both'); 
        }
        if($tipocot>0){
            $this->db->where("c.tipocot like '%$tipocot%' ");
        }
        $this->db->group_by("c.id");
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columnssss as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columnssss[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    public function getlistcotizaciones_total($params){
        $idCliente=$params['idCliente'];
        $denegada=$params['denegada'];
        $fi = $params['fi'];
        $ff = $params['ff'];
        $tipog = $params['tipog'];
        $tipocot = $params['tipocot'];
        $columns = array( 
            0=>'id',
            1=>'atencion',
            2=>'tipo',
            3=>'reg',
            4=>'info2',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(*) as total');
        $this->db->from('cotizaciones');
        if($idCliente>0){
            $this->db->where(array('idCliente'=>$idCliente));
        }
        if($fi!=''){
            $this->db->where("reg >='$fi 00:00:00' ");
        }
        if($ff!=''){
            $this->db->where("reg <='$ff 23:59:59' ");
        }
        $this->db->where(array('denegada'=>$denegada));
        if($tipog>0){
            $this->db->like('tipog', $tipog, 'both'); 
        }
        if($tipocot>0){
            $this->db->where("tipocot like '%$tipocot%' ");
        }
        //$where = ;
        //$this->db->where(array('facturaabierta'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            

        $query=$this->db->get();
        return $query->row()->total;
    }
    
}

?>