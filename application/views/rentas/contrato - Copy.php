<style type="text/css">
  .error{text-align: center;font-weight: bold;}
  #modal_addequipos .chosen-container{margin-left: 0px;}
  #modal_addequipos{top: 5% !important;height: 90%;max-height: 90%;}
  #tabla_equipos_select .btn-delete{display: none;}
  .productosinfo table td,.productosinfo table th{text-align: center;}
  .editarprocontra{margin-top: 15px;text-align: end;}
  .deletee{display: none;}
  .select2-container{z-index: 999999999;}
  .table_direcciones .desvincular{display: none;}
  .ubicacion{font-size: 12px !important;  }
  .modeloadvertencia i{font-size: 14px;color: red;}
  #des_click,#des_escaneo{width: 80px;}
  .floatbtn{float: left;}
  .div-btn{width: 166px;}
  .sequitaelpadig{padding-top: 2px;padding-bottom: 2px;padding-left: 2px;padding-right: 2px;}
  .table #orden, .table #ubicacion, .table #des_click, .table #des_escaneo{margin-bottom: 0px;}
  .docfilehojasestado iframe{width: 100%;border: 0px;height: 400px;}
  .td_docimicilio{max-width: 400px;}
  .td_docimicilio .chosen-container{margin-left: 0px;}
  .table_direcciones td{font-size: 13px;}
  #table_retiro_info p{margin: 0px;}
  #table_retiro_info td{padding: 5px;}
  .error{color: red;}
  input.ordene{width: 40px !important; text-align: center;}
  input.ubicacion{max-width: 200px !important;text-align: center;}
  input.comenext{max-width: 200px !important;text-align: center;}
  .table_moverequipos .ocultar_btn,
  .table_equipos_insta .ocultar_btn{display: none;}
  #tableequipos td{padding-top: 7px;padding-bottom: 7px;}
  #modalinfocalendario{
    z-index: 100000000 !important;
  }
</style>
<input type="hidden" id="idCliente" value="<?php echo $cliente_id;?>">
<!-- START CONTENT -->
<section id="content" width="100%">
  
  <!--start container-->
  <div class="container" width="100%">
    <div class="section">
      <div class="row">
        <p class="caption col s6">Nuevo contrato</p>
      <div class="row">
        <input type="hidden" id="edit_tipo" value="<?php echo isset($_GET['edit']) ?>">
        <input type="hidden" id="estatus_fin" value="<?php echo $estatus ?>">
        <input type="hidden" id="contratofin" value="<?php echo $contratofin ?>">
        <input type="hidden" id="idcontrato" value="<?php echo $idcontrato ?>">
        <input type="hidden" id="idRen" value="<?php echo $idRenta ?>">
        <input id="base_url" type="hidden" name="base_url" value="<?php echo base_url(); ?>">
        <input type="hidden" id="contratoexiste" value="<?php echo $contratoexiste?>">
        <!-- FORMULARIO  <?php ?>-->
        
          <div class="col s12 m12 24">
            <div class="card-panel">
              <div class="row">
              <form id="contrato-form" method="post">
                <input type="hidden" id="idRenta" name="idRenta" value="<?php echo $idRenta ?>">
                <div class="row">
                  <div class="input-field col s5">
                    <i class="material-icons prefix">#</i>
                    <input class="folio_c" type="text" id="folio" name="folio" value="<?php echo $folio ?>">
                    <label for="personalId">Folio</label>
                  </div>
                  <div class="input-field col s2"></div>
                  <div class="col s5">
                    
                    
                    <label for="speriocidad">Periodicidad servicio</label>
                    <select id="speriocidad" name="speriocidad" class="browser-default form-control-bmz descuentos">>
                      <option value="1" <?php if($speriocidad == 1) echo 'selected'?>>cada mes</option>
                      <option value="2" <?php if($speriocidad == 2) echo 'selected'?>>cada 2 meses</option>
                      <option value="3" <?php if($speriocidad == 3) echo 'selected'?>>cada 3 meses</option>
                      <option value="6" <?php if($speriocidad == 6) echo 'selected'?>>cada 6 meses</option>
                    </select>
                  </div>
                </div>
                <div class="row">
                  <div class="input-field col s5">
                    <i class="material-icons prefix">description</i>
                    <span class="tipo_contrato_e" hidden><?php echo $tipocontrato ?></span>
                    <select id="tipocontrato" name="tipocontrato" class="browser-default chosen-select">>
                      <option value="" disabled selected>Selecciona una opción</option>
                      <option value="1" <?php if($tipocontrato == 1) echo 'selected'?>>12 meses</option>
                      <option value="2" <?php if($tipocontrato == 2) echo 'selected'?>>24 meses</option>
                      <option value="3" <?php if($tipocontrato == 3) echo 'selected'?>>36 meses</option>
                    </select>
                    <label class="active">Tipo de contrato</label>
                  </div>
                  <div class="input-field col s2"></div>
                  <div class="input-field col s5">
                    <i class="material-icons prefix">account_box</i>
                    <select id="tipopersona" name="tipopersona" class="browser-default chosen-select">>
                      <option value="" disabled selected>Selecciona una opción</option>
                      <option value="1" <?php if($tipopersona == 1) echo 'selected'?>>Fisica</option>
                      <option value="2" <?php if($tipopersona == 2) echo 'selected'?>>Moral</option>
                    </select>
                    <label  class="active">Tipo de persona</label>
                  </div>
                </div>
                <div class="divider"></div>
                <h5 class="header">Datos de Contrato</h5>
                <div class="row">
                  <div class="input-field col s5">
                    <i class="material-icons prefix"></i>
                    <span class="vigencia_e" hidden><?php echo $vigencia ?></span>
                    <select id="vigencia" name="vigencia" class="browser-default chosen-select">>
                      <option value="" disabled selected>Selecciona una opción</option>
                      <option value="1" <?php if($vigencia == 1) echo 'selected'?>>12 meses</option>
                      <option value="2" <?php if($vigencia == 2) echo 'selected'?>>24 meses</option>
                      <option value="3" <?php if($vigencia == 3) echo 'selected'?>>36 meses</option>
                    </select>
                    <label  class="active">Vigencia</label>
                  </div>
                  <div class="input-field col s2"></div>
                  <div class="input-field col s5">
                    <i class="material-icons prefix">account_circle</i>
                    <input type="text" readonly value="<?php echo $vendedor; ?>">
                    <input id="personalId" name="personalId" type="hidden" value="<?php echo $vendedorid; ?>">
                    <label for="personalId">Vendedor</label>
                  </div>
                </div>
                <div class="row">
                  <div class="input-field col s4">
                    <i class="material-icons prefix">event</i>
                    <input placeholder="" id="fechasolicitud" name="fechasolicitud" type="date" class="validate" value="<?php echo $fechasolicitud; ?>">
                    <label for="fechasolicitud" class="active">Fecha de solicitud</label>
                  </div>
                  
                  <div class="input-field col s4">
                    <i class="material-icons prefix">event</i>
                    <input placeholder="" id="fechainicio" name="fechainicio" type="date" class="validate" value="<?php echo $fechainicio; ?>">
                    <label for="fechainicio" class="active">Fecha de inicio</label>
                  </div>
                  <div class=" col s4">
                    <label>Categoria</label>
                    <select class="form-control-bmz browser-default" id="tipo3" name="tipo3">
                      <option></option>
                      <option value="1" <?php if($tipo3 == 1) echo 'selected'?> >Renta en depósito</option>
                      <option value="2" <?php if($tipo3 == 2) echo 'selected'?>>Combinada</option>
                    </select>
                  </div>
                </div>
                <div class="divider"></div>
                <h5 class="header">Datos de cliente</h5>
                <div class="row">
                  <div class="input-field col s8">
                    <i class="material-icons prefix"></i>
                    <input id="arrendataria" name="arrendataria"  type="text" autocomplete="off" value="<?php echo $arrendataria; ?>">
                    <label for="arrendataria">Arrendataria</label>
                  </div>

                  <div class="input-field col s4">
                    <i class="material-icons prefix"></i>
                    <select id="rfc" name="rfc" class="browser-default chosen-select">
                      <option value="" disabled selected>Selecciona una opción</option>
                      <?php foreach ($fiscalcontacto as $item) { ?>
                        <option data-razonsocial="<?php echo $item->razon_social ?>" value="<?php echo $item->id ?>" <?php if($item->id == $rfc) echo 'selected'?>><?php echo $item->rfc ?></option>
                      <?php } ?>
                    </select>
                    <label for="rfc"  class="active">RFC</label>
                  </div>
                  
                </div>  

                <div class="row">  
                  <div class="input-field col s8">
                    <i class="material-icons prefix">description</i>
                    <input id="actaconstitutiva" name="actaconstitutiva" type="text" value="<?php echo $actaconstitutiva; ?>">
                    <label for="actaconstitutiva">Acta Constitutiva</label>
                  </div>
                  <div class="input-field col s4">
                    <i class="material-icons prefix">event</i>
                    <input id="fecha" name="fecha" type="date" value="<?php echo $fecha; ?>">
                    <label for="fecha" class="active">Fecha</label>
                  </div>
                </div>
                <div class="row">
                  <div class="input-field col s4">
                    <i class="material-icons prefix">#</i>
                    <input id="notariapublicano" name="notariapublicano" type="text" value="<?php echo $notariapublicano; ?>">
                    <label for="notariapublicano">Notaria Publica No.</label>
                  </div>
                  <div class="input-field col s8">
                    <i class="material-icons prefix">person</i>
                    <input id="titular" name="titular"  type="text" autocomplete="off" value="<?php echo $titular; ?>">
                    <label for="titular">Titular</label>
                  </div>
                </div>
                <div class="row">
                  <div class="input-field col s12">
                    <i class="material-icons prefix"></i>
                    <input id="representantelegal" name="representantelegal"  type="text" autocomplete="off" value="<?php echo $representantelegal; ?>">
                    <label for="representantelegal">Representante legal</label>
                  </div>
                </div>  
                <div class="row">
                  <div class="input-field col s4">
                    <i class="material-icons prefix"></i>
                    <input id="instrumentonotarial" name="instrumentonotarial" type="text" value="<?php echo $instrumentonotarial; ?>">
                    <label for="instrumentonotarial">Instrumento notarial</label>
                  </div>
                  <div class="input-field col s4">
                    <i class="material-icons prefix">event</i>
                    <input placeholder="" id="instrumentofechasolicitud" name="instrumentofechasolicitud" type="date" class="validate" value="<?php echo $instrumentofechasolicitud; ?>">
                    <label for="instrumentofechasolicitud" class="active">Fecha de solicitud</label>
                  </div>
                  <div class="input-field col s4">
                    <i class="material-icons prefix">#</i>
                    <input id="instrumentonotariapublicano" name="instrumentonotariapublicano"  type="text" autocomplete="off" value="<?php echo $instrumentonotariapublicano; ?>">
                    <label for="instrumentonotariapublicano">Notaria Pública No.</label>
                  </div>
                </div>
                <div class="row">
                  <div class="input-field col s12">
                    <i class="material-icons prefix">place</i>
                    <select id="domiciliofiscal" name="domiciliofiscal" class="browser-default chosen-select">
                      <option value="" disabled selected>Selecciona una opción</option>
                      <?php foreach ($fiscalcontacto as $item){ ?>
                          <option value="<?php echo $item->id?>" <?php if($item->id == $domiciliofiscal) echo 'selected' ?>><?php echo 'Razon social: '.$item->razon_social.' Colonia:'.$item->colonia.' Calle:'.$item->calle.' Numero:'.$item->num_ext ?></option>
                      <?php } ?>
                    </select>
                    <label for="domiciliofiscal"  class="active">Domicilio fiscal</label>
                  </div>
                </div>
                <div class="row">
                  <div class="col s6"></div>
                  <div class="col s6 editarprocontra"></div>
                </div>
                <div class="row search_serie_div" style="margin-top: 13px;">
                  <div class="col s3 m9 " align="right"><label>Serie</label></div>
                  <div class="col s6 m2 "><input type="text" id="search_serie" class="form-control-bmz" ></div>
                  <div class="col s3 m1 "><button class="btn-bmz cyan waves-effect" type="button" onclick="buscar_serie_con()">Buscar</button></div>
                </div>
                <div class="row">
                  <div class="col s12">
                    <table class=" display table_direcciones table">
                      <thead>
                        <tr>
                          <th>Orden</th>
                          <th>Ubicación</th>
                          <th>Comentario</th>
                          <th>Equipo</th>
                          <th>Serie</th>
                          <th>Info</th>
                          <th></th>
                          <th><button type="button" class="btn-floating waves-effect waves-light cyan btn" onclick="reordenar(<?php echo $idRenta;?>)"><i class="material-icons">swap_vertical_circle</i></button></th>
                          <th class="td_docimicilio">Dirección</th>
                          <th>Descuento<br>clicks</th>
                          <th>Descuento<br>Escaneo</th>
                          <th ></th>  
                        </tr>
                      </thead>
                      <tbody>
                        <?php foreach ($equiposerie as $iteme) {  
                          ?><tr class="serie_<?php echo $iteme->serieId; ?> color_serie accion_equi_<?php echo $iteme->id; ?>_ser_<?php echo $iteme->serieId; ?>" data-equipo="<?php echo $iteme->modelo;?>" data-serie="<?php echo $iteme->serie;?>" data-suspendidofecha="<?php echo $iteme->fecha_fin_susp; ?>" data-suspendido="<?php echo $iteme->estatus_suspendido; ?>" data-contadorinfo="<?php echo $iteme->contadorinfo; ?>" id="select_serie_<?php echo $iteme->serieId; ?>" >
                            <td><input id="orden" type="number" value="<?php echo $iteme->orden; ?>" class="ordene ordene_update_<?php echo $iteme->id_asig;?> form-control-bmz" onchange="updateubicacion(<?php echo $iteme->id_asig;?>)"></td>
                            <td><input id="ubicacion" type="text" value="<?php echo $iteme->ubicacion; ?>" class="ubicacion ubicacion_update_<?php echo $iteme->id_asig;?> form-control-bmz" onchange="updateubicacion(<?php echo $iteme->id_asig;?>)"></td>
                            <td><input type="text" value="<?php echo $iteme->comenext; ?>" class="comenext comenext_update_<?php echo $iteme->id_asig;?> form-control-bmz" onchange="updateubicacion(<?php echo $iteme->id_asig;?>)"></td>
                            <td><span class="modeloadvertencia"><?php if($iteme->cantidad>1){ ?><i class="material-icons">warning</i><?php } ?></span>
                              <?php echo $iteme->modelo; if($iteme->usado==1){echo '-mc';}?><input id="equipo_dir" type="hidden" value="<?php echo $iteme->id; ?>"></td>
                            <td><?php echo $iteme->serie;?><input id="serie_dir" type="hidden" value="<?php echo $iteme->serieId; ?>"></td>
                            <td><?php echo $iteme->contadores;?></td>
                            <td><button type="button" class="btn-floating waves-effect waves-light cyan btn tooltipped" data-position="bottom" data-delay="50" data-tooltip="Agrear Accesorios" onclick="addaccesorio2(<?php echo $iteme->id; ?>,<?php echo $iteme->idEquipo; ?>,'<?php echo $iteme->modelo; ?>')"><i class="material-icons">control_point</i></button></td>
                            <td><?php 
                                $rentaequipos_accesorios = $this->ModeloCatalogos->rentaequipos_accesorios($iteme->idRenta,$iteme->idEquipo,$iteme->id);
                                foreach ($rentaequipos_accesorios->result() as $itemacc) {
                                  $h_acc='<p class="desvincular_1_'.$itemacc->id_accesoriod.'">'.$itemacc->cantidad.' '.$itemacc->nombre.' '.$itemacc->series.'<a class="btn-bmz red desvincular" onclick="desvincular('.$itemacc->id_accesoriod.',1)" title="Desvincular" style="padding-top: 3px;padding-bottom: 3px;padding-left: 8px;padding-right: 8px;font-size: 11px;"><i class="fas fa-unlink fa-fw"></i></a>';
                                    if($itemacc->series==''){
                                    $h_acc.='<button class="b-btn b-btn-danger tooltipped ocultar_btn" data-position="top" data-delay="50" data-tooltip="eliminar accesorio" onclick="delete_accesorio('.$itemacc->id_accesoriod.')" style="padding-top: 3px;padding-bottom: 3px;padding-left: 8px;padding-right: 8px;font-size: 11px;"><i class="fas fa-times"></i></button>';
                                  }
                                  $h_acc.='</p>';
                                  echo $h_acc;
                                  
                                }
                                $arrayconsumibles=array();
                                $rentaequipos_consumible = $this->ModeloCatalogos->rentaequipos_consumible1($iteme->idRenta,$iteme->idEquipo,$iteme->id);
                                foreach ($rentaequipos_consumible->result() as $item0) {
                                    $contrato_folio2 = $this->ModeloCatalogos->obtenerfoliosconsumiblesrenta3($iteme->idRenta,$item0->id_consumibles,$item0->id,$iteme->serieId); 
                                    foreach ($contrato_folio2->result() as $itemx) {
                                        //if($idcontratofolio!=$itemx->idcontratofolio){
                                            $arrayconsumibles[$itemx->idcontratofolio]=array('modelo'=>$item0->modelo,'id'=>$item0->id,'folio'=>$itemx->foliotext);
                                            /*
                                            echo '<p class="desvincular desvincular_2_'.$item0->id.'">'.$item0->modelo.' '.$itemx->foliotext.' 
                                                          <a class="btn-bmz red " onclick="desvincular('.$item0->id.',2)" title="Desvincular"><i class="fas fa-unlink fa-fw"></i></a>
                                                       </p>';
                                            */
                                        //}
                                          
                                          
                                    }
                                }
                                foreach ($arrayconsumibles as $itemu) {
                                  echo '<p class="desvincular desvincular_2_'.$itemu['id'].'">'.$itemu['modelo'].' '.$itemu['folio'].'<a class="btn-bmz red " onclick="desvincular('.$itemu['id'].',2)" title="Desvincular" style="padding-top: 3px;padding-bottom: 3px;padding-left: 8px;padding-right: 8px;font-size: 11px;"><i class="fas fa-unlink fa-fw"></i></a></p>';
                                }
                              ?>
                            </td>
                            <td class="td_docimicilio">
                              <select id="domicilioinstalacion" class="domicilioinstalacion browser-default chosen-select direccion_equipo_<?php echo $iteme->id; ?>_<?php echo $iteme->serieId; ?>">
                                <option value="" disabled selected>Selecciona una opción</option>
                                <?php foreach ($direccioncliente as $item) {
                                    echo '<option value="g_'.$item->idclientedirecc.'" data-iddir="'.$item->idclientedirecc.'" data-atencionpara="'.$item->atencionpara.'">'.$item->direccion.'</option>';
                                    }
                                    foreach ($fiscalcontacto as $item){ 
                                      $direccionff='Razon social: '.$item->razon_social.' Colonia:'.$item->colonia.' Calle:'.$item->calle.' Numero:'.$item->num_ext;
                                ?>
                                    <option value="f_<?php echo $item->id?>" data-iddir="0"><?php echo $direccionff; ?></option>
                                <?php } ?></select>
                              <div class="tag_direccion_equipo_<?php echo $iteme->id; ?>_<?php echo $iteme->serieId; ?>"></div>
                            </td>
                            <td><input id="des_click" type="number" class="form-control-bmz descuentos" value="<?php echo $iteme->des_click; ?>"></td>
                            <td><input id="des_escaneo" type="number" class="form-control-bmz descuentos" value="<?php echo $iteme->des_escaneo; ?>"></td>
                            <td width="94px">
                              <div class="div-btn">
                              <input type="checkbox" class="filled-in delete_selected_serie" id="<?php echo 'select_delete_equi_'.$iteme->id.'_ser_'.$iteme->serieId;?>" data-equiporow="<?php echo $iteme->id;?>" data-serieid="<?php echo $iteme->serieId;?>" data-serie="<?php echo $iteme->serie;?>" data-idEquipo="<?php echo $iteme->idEquipo?>" data-modelo="<?php echo $iteme->modelo;?>" />
                              <label for="<?php echo 'select_delete_equi_'.$iteme->id.'_ser_'.$iteme->serieId;?>" class="floatbtn">.</label>
                              <?php 
                                $estatus_suspendido=$iteme->estatus_suspendido;
                                if($estatus_suspendido==1){
                                  if (date('Y-m-d',strtotime($iteme->fecha_fin_susp)) >= date('Y-m-d')) {
                                    $estatus_suspendido=1;
                                  }else{
                                    $estatus_suspendido=0;
                                  }
                                }
                              ?>
                              <button type="button" class="waves-effect cyan btn-bmz tooltipped suspender floatbtn <?php if($estatus_suspendido==1){ }else{echo 'sequitaelpadig'; }?>" data-position="bottom" data-delay="50" data-tooltip="<?php if($estatus_suspendido==1){echo 'Reactivacion';}else{echo 'Suspender';}?>" onclick="suspenderequiposseries2(<?php echo $iteme->id; ?>,<?php echo $iteme->serieId; ?>,<?php echo $iteme->id_asig;?>)" data-fechafinsusp="<?php echo $iteme->fecha_fin_susp;?>">
                                  <?php if($estatus_suspendido==1){
                                    echo '<i class="fas fa-hourglass"></i>';
                                  }else{
                                    echo '<span class="fa-stack"><i class="fas fa-solid fa-hourglass fa-stack-1x"></i><i class="fas fa-solid fa-check fa-stack-1x" style="color:green"></i></span>';
                                  }?></button>

                              <a class="waves-effect <?php if($iteme->hoja_estado==''){ echo 'cyan';}else{ echo 'green';}?> btn-bmz tooltipped floatbtn hojasestado_<?php echo $iteme->id_asig;?>" data-position="bottom" data-delay="50" data-tooltip="Hoja de estado" data-hojasestado="<?php echo $iteme->hoja_estado;?>" onclick="hojasestado(<?php echo $iteme->id_asig;?>)"><i class="fas fa-file-pdf" style="font-size: 20px;"></i></a>

                              <a class="waves-effect red btn-bmz tooltipped floatbtn" data-position="bottom" data-delay="50" data-tooltip="Eliminar" onclick="eliminarseries2(<?php echo $iteme->id; ?>,<?php echo $iteme->serieId; ?>)" ><i class="fas fa-trash"></i></a>
                              </div>
                            </td> 
                          </tr><?php } ?> 
                      </tbody>
                    </table>
                  </div>
                  
                </div> 
                <div class="row">
                  <div class="input-field col s12">
                    <i class="material-icons prefix">place</i>
                  </div>
                </div> 
                <div class="row">
                  <div class="input-field col s4">
                    <i class="material-icons prefix" onclick="modaldatoscontacto()" title="Dar click para obtener todos los contactos">account_box</i>
                    <input id="contacto" name="contacto" type="text" value="<?php echo $contacto ?>" onclick="modaldatoscontacto()">
                    <label for="contacto">Contacto</label>
                  </div>
                  <div class="input-field col s4">
                    <input id="cargoarea" name="cargoarea" type="text" value="<?php echo $cargoarea?>">
                    <label for="cargoarea">Cargo/Área</label>
                  </div>
                  <div class="input-field col s4">
                    <i class="material-icons prefix">smartphone</i>
                    <input id="telefono" name="telefono"  type="text" autocomplete="off" value="<?php echo $telefono ?>">
                    <label for="telefono">Teléfono</label>
                  </div>
                </div>
                <div class="divider"></div>
                <h5 class="header">Procedimientos</h5>
                <div class="row">
                  <div class="col s2">
                    <h6>Servicio</h6>
                  </div>
                  <div class="input-field col s3">
                    <input id="servicio_horario" name="servicio_horario" type="text" class="readonlys form-control-bmz" value="<?php echo $servicio_horario; ?>">
                    <label for="servicio_horario" class="active">Horario disponible</label>
                  </div>
                  <div class="input-field col s3">
                    <input id="servicio_equipo" name="servicio_equipo" type="text" class="readonlys form-control-bmz" value="<?php echo $servicio_equipo; ?>">
                    <label for="servicio_equipo" class="active">Equipo para acceso</label>
                  </div>
                  <div class="input-field col s4">
                    <input id="servicio_doc" name="servicio_doc" type="text" class="readonlys form-control-bmz" value="<?php echo $servicio_doc; ?>">
                    <label for="servicio_doc" class="active">Documentación de acceso</label>
                  </div>
                </div>
                <div class="divider"></div>
                <h5 class="header">Orden de compra</h5>
                <div class="row">
                  <div class="col s2">
                    <h6></h6>
                  </div>
                  <div class="col s3">
                    <label for="ordencompra" class="active">Orden de entrega</label>
                    <input id="ordencompra" name="ordencompra" type="text" class="readonlys form-control-bmz" value="<?php echo $ordencompra; ?>">
                    
                  </div>
                  <div class=" col s3">
                    <label for="oc_vigencia" class="active">Vigencia</label>
                    <input id="oc_vigencia" name="oc_vigencia" type="date" class="readonlys form-control-bmz" value="<?php echo $oc_vigencia; ?>">
                    
                  </div>
                  <div class=" col s3">
                    <label  class="active">Comentario</label>
                    <textarea id="oc_comentario" name="oc_comentario" class="habilitardeshabilitar form-control-bmz" style="min-height: 58px;"><?php echo $oc_comentario; ?></textarea>
                    
                  </div>
                  
                </div>
                <div class="row files_div_oc_contrato">
                  <div class="col s12">
                    <ul class="collapsible inputequipo" data-collapsible="accordion">
                        <li class="">
                          <div class="collapsible-header">Documentos orden de compra</div>
                          <div class="collapsible-body" style="display: none;">
                            <div class="row">
                              <div class="col s12 files_orden">
                                
                              </div>
                              <div class="col s12">
                                <input type="file" id="files" name="files[]" multiple accept="*"> 
                              </div>
                              
                            </div>
                          </div>
                        </li>
                      </ul>
                  </div>
                </div>
                <div class="divider"></div>
                <h5 class="header">Clausulas</h5>
                <div class="row">
                  <div class="input-field col s4">
                    <i class="material-icons prefix"></i>
                    <select id="rentaadelantada" name="rentaadelantada" class="browser-default chosen-select">
                      <option value="" disabled selected>Selecciona una opción</option>
                      <option value="1" <?php if($rentaadelantada == 1) echo 'selected' ?>>Adelantada</option>
                      <option value="2" <?php if($rentaadelantada == 2) echo 'selected' ?>>Vencida</option>
                      
                    </select>
                    <label for="rentaadelantada"  class="active">Renta tipo</label>
                  </div>
                  <div class="col s4">
                    <label for="tiporenta"  class="active">Renta tipo</label>
                    <select id="tiporenta" name="tiporenta" class="browser-default form-control-bmz" required>
                      <option value="2" <?php if($tiporenta == 2) echo 'selected' ?>>Global</option>
                      <option value="1" <?php if($tiporenta == 1) echo 'selected' ?>>Individual</option>
                      
                      
                    </select>
                    
                  </div>
                  <div class="col s2">
                    <input type="checkbox" name="facturacionlibre" id="facturacionlibre" <?php if($facturacionlibre == 1) echo 'checked' ?>  value="1" >
                    <label class="active" for="facturacionlibre">Producción Libre</label>
                  </div>
                  <div class="col s2">
                    <button class="btn cyan waves-effect waves-light right" onclick="addcondiciones(<?php echo $idRenta ?>)" type="button">condiciones adicionales</button>
                  </div>
                </div>
                <div class="row">
                    <div class="col s12 row_global" style="display: none;">
                      <ul class="collapsible inputequipo" data-collapsible="accordion">
                        <li class="active">
                          <div class="collapsible-header active">Global</div>
                          <div class="collapsible-body" style="display: block;">
                            <div class="inputequipo">
                              <div class="row">
                                <div class="col s2">
                                  <label>Renta Monocromático</label>
                                  <input id="rentadeposito" name="rentadeposito" type="number" value="<?php echo $rentadeposito; ?>" class="form-control-bmz">
                                </div>
                                <div class="col s2">
                                  <label>Clicks incluidos mono</label>
                                  <input name="clicks_mono" id="clicks_mono"  type="number" value="<?php echo $clicks_mono; ?>" class="form-control-bmz">
                                </div>
                                <div class="col s2">
                                  <label class="cambiotitulolibrem">Precio click excedente</label>
                                  <input name="precio_c_e_mono" id="precio_c_e_mono"  type="number" value="<?php echo $precio_c_e_mono; ?>" class="form-control-bmz">
                                </div>
                                <div class="col s2">
                                  <label>Renta Color</label>
                                  <input id="rentacolor" id="rentacolor" name="rentacolor" type="number" value="<?php echo $rentacolor; ?>" class="form-control-bmz">
                                </div>
                                <div class="col s2">
                                  <label>Clicks incluidos color</label>
                                  <input name="clicks_color" id="clicks_color"  type="number" value="<?php echo $clicks_color; ?>" class="form-control-bmz">
                                </div>
                                <div class="col s2">
                                  <label class="cambiotitulolibrec">Precio click color excedente</label>
                                  <input name="precio_c_e_color" id="precio_c_e_color"  type="number" value="<?php echo $precio_c_e_color; ?>" class="form-control-bmz">
                                </div>
                              </div>
                              <div class="row">
                                <div class="col s2">
                                  <label>Descuento clicks</label>
                                  <input id="descuento_g_clicks" name="descuento_g_clicks" type="number" value="<?php echo $descuento_g_clicks; ?>" class="form-control-bmz descuentos">
                                </div>
                                <div class="col s2">
                                  <label>Descuento Escaneo</label>
                                  <input name="descuento_g_scaneo" id="descuento_g_scaneo"  type="number" value="<?php echo $descuento_g_scaneo; ?>" class="form-control-bmz descuentos">
                                </div>
                              </div>
                            </div>
                          </div>
                        </li>
                      </ul>
                    </div>
                </div>
                <div class="row">
                  <div class="col s12">
                    <label>INSTRUCCIONES PARA FACTURACIÓN</label>
                    <textarea class="form-control-bmz descuentos" id="info_factura" name="info_factura" style="height: 76px !important;" onchange="updatecondicionfac(<?php echo $idcontrato;?>)"><?php echo $info_factura;?></textarea>
                  </div>
                </div>
              </form>
              <div class="row">
                <div class="col s12 row_individual tabla_pagos" style="display: none;">
                  <ul class="collapsible inputequipo" data-collapsible="accordion">
                  <?php foreach ($equiposerie as $item) { 
                      ?><li class="active equipo_<?php echo $item->id;?> serie_<?php echo $item->serieId; ?>">
                          <div class="collapsible-header"><?php echo $item->modelo;?> / <?php echo $item->serie;?></div>
                          <div class="collapsible-body">
                            <div class="inputequipo">
                              <?php 
                                $html_r_d_i='';
                                $html_r_d_i.='<div class="row">';
                                $html_r_d_i.='<div class="col s2">';
                                  $html_r_d_i.='<label>Renta Monocromáticos</label>';
                                  $html_r_d_i.='<input id="equipo_row" name="equipo_row" type="hidden" value="'.$item->id.'">';
                                  $html_r_d_i.='<input id="serie_row" name="serie_row" type="hidden" value="'.$item->serieId.'">';
                                  $html_r_d_i.='<input name="rentadeposito_row"';
                                          $html_r_d_i.='id="rentadeposito_row"   ';
                                          $html_r_d_i.='type="text" ';
                                          $html_r_d_i.='value="'.$item->costo_renta.'" ';
                                          $html_r_d_i.='class="rentadeposito_rows rentadeposito_row_'.$item->id.'_'.$item->serieId.' rentadeposito_view_'.$item->id.' habilitardeshabilitar">';
                                $html_r_d_i.='</div>';
                                $html_r_d_i.='<div class="col s2">';
                                  $html_r_d_i.='<label>Clicks incluidos mono</label>';
                                  $html_r_d_i.='<input name="clicks_mono_row" ';
                                          $html_r_d_i.='id="clicks_mono_row"  ';
                                          $html_r_d_i.='type="number" ';
                                          $html_r_d_i.='value="'.$item->pag_monocromo_incluidos.'" ';
                                          $html_r_d_i.='class="clicks_mono_row clicks_mono_row_'.$item->id.'_'.$item->serieId.' clicks_mono_view_'.$item->id.' habilitardeshabilitar">';
                                $html_r_d_i.='</div>';
                                $html_r_d_i.='<div class="col s2">';
                                  $html_r_d_i.='<label class="cambiotitulolibrem">Precio click excedente</label>';
                                  $html_r_d_i.='<input name="precio_c_e_mono_row" ';
                                          $html_r_d_i.='id="precio_c_e_mono_row"  ';
                                          $html_r_d_i.='type="number" ';
                                          $html_r_d_i.='value="'.$item->excedente.'" ';
                                          $html_r_d_i.='class=" precio_c_e_mono_row precio_c_e_mono_row_'.$item->id.'_'.$item->serieId.' habilitardeshabilitar">';
                                $html_r_d_i.='</div>';
                                $html_r_d_i.='<div class="col s2">';
                                  $html_r_d_i.='<label>Renta Color</label>';
                                  $html_r_d_i.='<input name="rentacostocolor_row"';
                                          $html_r_d_i.='id="rentacostocolor_row"   ';
                                          $html_r_d_i.='type="text" ';
                                          $html_r_d_i.='value="" ';
                                          $html_r_d_i.='class="rentacostocolor_row rentacostocolor_row_'.$item->id.'_'.$item->serieId.' rentacostocolor_view_'.$item->id.' habilitardeshabilitar">';
                                $html_r_d_i.='</div>';
                                $html_r_d_i.='<div class="col s2">';
                                  $html_r_d_i.='<label>Clicks incluidos color</label>';
                                  $html_r_d_i.='<input name="clicks_color_row" ';
                                          $html_r_d_i.='id="clicks_color_row"  ';
                                          $html_r_d_i.='type="number" ';
                                          $html_r_d_i.='value="'.$item->pag_color_incluidos.'" ';
                                          $html_r_d_i.='class="clicks_color_row clicks_color_row_'.$item->id.'_'.$item->serieId.' clicks_color_view_'.$item->id.' habilitardeshabilitar">';
                                $html_r_d_i.='</div>';
                                $html_r_d_i.='<div class="col s2">';
                                  $html_r_d_i.='<label class="cambiotitulolibrec">Precio click color excedente</label>';
                                  $html_r_d_i.='<input name="precio_c_e_color_row" ';
                                          $html_r_d_i.='id="precio_c_e_color_row"  ';
                                          $html_r_d_i.='type="number" ';
                                          $html_r_d_i.='value="'.$item->excedentecolor.'" ';
                                          $html_r_d_i.='class="precio_c_e_color_row precio_c_e_color_row_'.$item->id.'_'.$item->serieId.' habilitardeshabilitar">';
                                $html_r_d_i.='</div>';
                              $html_r_d_i.='</div>';
                              echo $html_r_d_i;
                              ?>
                              
                            </div>
                          </div>
                      </li><?php } ?>
                  </ul>
                </div>
                
              </div>
              <div class="divider"></div>
              
              
            <form id="contrato-form" method="post" style="display:none;">
                <div class="divider"></div>
                <h5 class="header">Equipo</h5>
                <div class="row">
                  <div class="col s10"></div>
                  <div class="col s2 editarprocontra">
                    
                  </div>
                </div>  
                <div class="row">
                  <div class="col s12">
                    <table id="tabla_equipos" class="table display" cellspacing="0">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Cliente</th>
                          <th>Modelo</th>
                          <th>Cantidad</th>
                          <!--
                          <th>Área</th>
                          <th>Plazo</th>
                          -->
                          <td></td>
                        </tr>
                      </thead>
                      <tbody id="class_equipos"></tbody>
                    </table>    
                </div>
              </div>
            </form>    
                <div class="row">
                  <div class="botonfinalizar"></div>
                  <div class="input-field col s12 guadarremove">
                    <button class="btn cyan waves-effect waves-light right registro" type="button">Guardar
                      <i class="material-icons right">send</i>
                    </button>
                  </div>
                </div>

              </div>
            </div>
          </div>
        
      </div>
    </div>
  </section>
<!-- Modal inicia -->
<div id="modal_img" class="modal fade" data-keyboard="false" data-backdrop="static">
    <div class="modal-content " >
        <h4>Subir Orden de retiro</h4>
        <div class="col s12 m12 l6">
            <div class="row" align="center">
                <div class="input-field col s3">
                   <div class="fileinput fileinput-new" data-provides="fileinput">
                     <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 530px; height: 250px;"> 
                      <input type="file" id="foto" name="foto" class="dropify foto">
                    </div>
                   </div>
                </div>
            </div>
        </div>  
        <div class="progress cargagif" style="display: none;">
            <div class="indeterminate"></div>
        </div>
        <input type="hidden" id="idcontrato_f">
        
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
        <a href="#!" class="waves-effect waves-red red btn-flat" style="color: white;" onclick="agregarfile()">Aceptar</a>
    </div>  
</div>
<!-- Modal finaliza fin-->
<!-- Modal inicia -->
<div id="modal_addequipos" class="modal fade modal-fixed-footer" data-keyboard="false" data-backdrop="static" style="width: 95%;">
    <div class="modal-content " style="padding-left: 0px; padding-right: 0px;">
        <h4 style="padding-left: 24px">Agregar Equipos</h4>
        <input type="hidden" id="idRentanew">
        <input type="hidden" id="idRentaModel" value="0">
        <ul class="collapsible inputequipo1" data-collapsible="accordion">
          <li class="active">
            <div class="collapsible-header active">Seleccion</div>
            <div class="collapsible-body" style="display: block; padding: 0px;">
              <div class="inputequipo1">
                  <div class="row">
                    <div style="float:left; width: 33%; min-height: 50px; border: 1px solid #e1e1e1;">
                      <div class="row" >
                               <div class="col s2 m2 l2">
                                  <label for="selected_newequipo_cantidad" class="active">Cantidad</label>
                                  <input id="selected_newequipo_cantidad" class="form-control-bmz" type="number" value="1" readonly>
                                  
                                </div>
                                <div class="col s5 m5 l5">
                                  <label for="selected_newequipo" class="active" >Equipo</label>
                                  <select id="selected_newequipo" name="selected_newequipo" class="browser-default disabledquitar chosen-select" onchange="selected_newequipo()">
                                    <option value="" selected disabled></option>
                                  <?php foreach ($equiposrow as $item) { 
                                      echo '<option value="'.$item->id.'">'.$item->modelo.'</option>';
                                    } ?> 
                                  </select>
                                </div>
                                <div class="col s4 m4 l4">
                                  <label for="selected_newequipo_b" class="active" >Bodega</label>
                                  <select id="selected_newequipo_b" name="selected_newequipo_b" class="browser-default disabledquitar chosen-select">
                                   <?php foreach ($restbodegas as $item) { ?>
                                      <option value="<?php echo $item->bodegaId; ?>"><?php echo $item->bodega; ?></option>
                                  <?php } ?> 
                                  </select>
                                </div>
                                
                             </div> 
                             <div class="row" >
                                <div class="col s12 m12 l12">
                                  <select id="domicilioinstalacion_new" class="domicilioinstalacion_new browser-default chosen-select disabledquitar">
                                    <option value="" disabled selected>Selecciona una opción</option>
                                    <?php foreach ($direccioncliente as $item) { 
                                        echo '<option value="g_'.$item->idclientedirecc.'" data-atencionpara="'.$item->atencionpara.'" >'.$item->direccion.'</option>';
                                      }
                                    foreach ($fiscalcontacto as $item){ 
                                      $direccionff='Razon social: '.$item->razon_social.' Colonia:'.$item->colonia.' Calle:'.$item->calle.' Numero:'.$item->num_ext;
                                    ?>
                                    <option value="f_<?php echo $item->id?>"><?php echo $direccionff; ?></option>
                                    <?php } ?>
                                  </select>
                                </div>
                              </div>
                             <div class="row" >
                                <div class="col s6 m6 l6">
                                    <input type="checkbox" id="temp" class="disabledquitar" onchange="temp()" />
                                    <label for="temp">Temporal</label>
                                  </div>
                                  <div class="col s6 m6 l6 divtemp_reg" style="display: none;">
                                    <label for="temp_reg">Fecha Termino</label>
                                    <input type="date" id="temp_reg" class="disabledquitar" />
                                    
                                  </div>
                                  <div class="col s6 m6 l6">
                                    <a class="waves-effect cyan btn-bmz" onclick="addequipocola()">Agregar</a>
                                  </div>

                             </div>
                    </div>
                    <div style="float:left; width: 33%; min-height: 50px; border: 1px solid #e1e1e1;">
                      <div class="row">
                        <div class="col s2 m2 l2">
                          <label for="selected_newconsumible_cantidad" class="active">Cantidad</label>
                          <input id="selected_newconsumible_cantidad" class="form-control-bmz" type="number" value="1" readonly>
                          
                        </div>
                        <div class="col s4 m4 l4">
                          <label for="selected_newconsumible" class="active" >Consumibles</label>
                          <select id="selected_newconsumible" name="selected_newconsumible" class="browser-default disabledquitar chosen-select"></select>
                        </div>
                        <div class="col s4 m4 l4">
                          <label for="selected_newconsumible_b" class="active" >Bodega</label>
                          <select id="selected_newconsumible_b" name="selected_newconsumible_b" class="browser-default disabledquitar chosen-select">
                           <?php foreach ($restbodegas as $item) { ?>
                              <option value="<?php echo $item->bodegaId; ?>"><?php echo $item->bodega; ?></option>
                          <?php } ?> 
                          </select>
                        </div>
                        <div class="col s2 m2 l2" style="text-align: center;">
                          <a class="btn-floating waves-effect waves-light green darken-1" onclick="addconsumible()">
                              <i class="material-icons">vertical_align_bottom</i>
                            </a>
                        </div>
                      </div>
                      <div class="row">
                        <table class="table striped" id="new_table_consumible">
                          <thead>
                            <tr>
                              <th>Cantidad</th>
                              <th>Consumible</th>
                              <th>Bodega</th>
                              <th></th>
                            </tr>
                          </thead>
                          <tbody class="nttbodyc">
                            
                          </tbody>
                        </table>
                      </div> 
                    </div>
                    <div style="float:left; width: 33%; min-height: 50px; border: 1px solid #e1e1e1;">
                      <div class="row">
                        <div class="col s2 m2 l2">
                          <label for="selected_newaccesorio_cantidad" class="active">Cantidad</label>
                          <input id="selected_newaccesorio_cantidad" class="form-control-bmz" type="number" value="1" readonly>
                          
                        </div>
                        <div class="col s4 m4 l4">
                          <label for="selected_newaccesorio" class="active" >Accesorios</label>
                          <select id="selected_newaccesorio" name="selected_newaccesorio" class="browser-default disabledquitar chosen-select"></select>
                        </div>
                        <div class="col s4 m4 l4">
                          <label for="selected_newaccesorio_b" class="active" >Bodega</label>
                          <select id="selected_newaccesorio_b" name="selected_newaccesorio_b" class="browser-default disabledquitar chosen-select">
                           <?php foreach ($restbodegas as $item) { ?>
                              <option value="<?php echo $item->bodegaId; ?>"><?php echo $item->bodega; ?></option>
                          <?php } ?> 
                          </select>
                        </div>
                        <div class="col s2 m2 l2" style="text-align: center;">
                          <a class="btn-floating waves-effect waves-light green darken-1" onclick="addaccesorio();">
                              <i class="material-icons">vertical_align_bottom</i>
                            </a>
                        </div>
                      </div>
                      <div class="row">
                        <table class="table striped" id="new_table_accesorios">
                          <thead>
                            <tr>
                              <th>Cantidad</th>
                              <th>Accesorios</th>
                              <th>Bodega</th>
                              <th></th>
                            </tr>
                          </thead>
                          <tbody class="nttbodya">
                            
                          </tbody>
                        </table>
                      </div> 
                    </div>
                  </div>
              </div>
            </div>
          </li>
        </ul>
        <ul class="collapsible inputequipo2" data-collapsible="accordion">
          <li class="active">
            <div class="collapsible-header active">Por Agregar</div>
            <div class="collapsible-body" style="display: block;">
              <div class="inputequipo2">
                <div class="row">
                  <table id="tabla_equipos_select" class="table striped bordered">
                    <thead>
                      <tr>
                        <th>Cantidad</th>
                        <th>Equipo</th>
                        <th>Bodega</th>
                        <th>Direccion</th>
                        <th>Consumible</th>
                        <th>Accesorios</th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody class="t_e_s">
                      
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </li>
        </ul>

        
        
        
       
        
        
    </div>
    <div class="modal-footer">
        <a class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
        <a class="waves-effect waves-red red btn-flat" style="color: white;" onclick="savenewequipo()">Agregar Equipos</a>
    </div>
    <!-- Modal finaliza fin-->
</div>
<!-- Modal finaliza fin-->
<!-- Modal inicia -->
<div id="modal_susp" class="modal fade text-left" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
  <div class="modal-content">
    <h4 style="padding-left: 24px">Suspender Equipo</h4>
      <div class="modal-body">
          <div class="row">
              <div class="col-md-12">
                  <div class="col-md-3">
                      <div class="form-group">
                          <label class="label-control">Fecha termino de suspencion: </label>
                          <input type="date" class="form-control-bmz" name="fecha_fin_susp" id="fecha_fin_susp" value="<?php echo date('Y-m-d');?>">
                      </div>
                  </div>                  
              </div>
              <div class="col s12 suspencionseriesparcial">
            
            </div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn grey btn-outline-primary" onclick="suspenderequiposseries()">Confirmar</button>
        <a class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
      </div>
  </div>

</div>
<!-- Modal finaliza fin-->
<!-- Modal inicia -->
<div id="modal_deleteseries" class="modal fade modal-fixed-footer" data-keyboard="false" data-backdrop="static">
    <div class="modal-content " style="padding-left: 0px; padding-right: 0px;">
        <h4 style="padding-left: 24px">Eliminar Equipo</h4>
        <div class="row " style="margin-bottom: 0px;">
          <div class="col s12 deleteseriesparcial"></div>
        </div>
    </div>
    <div class="modal-footer">
        <a class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
        <a class="waves-effect modal-close waves-red red btn-flat" style="color: white;" onclick="eliminarseries()">Eliminar</a>
    </div>
    <!-- Modal finaliza fin-->
</div>
<div id="modal_datoscontacto" class="modal fade modal-fixed-footer" data-keyboard="false" data-backdrop="static">
    <div class="modal-content " style="padding-left: 0px; padding-right: 0px;">
        <h4 style="padding-left: 24px">Seleccionar datos</h4>
        <div class="row " style="margin-bottom: 0px;">
          <div class="col s12">
            <table>
              <thead>
                <tr>
                  <th>Contacto</th><th>Cargo</th><th>Telefono</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($datoscontacto->result() as $item) { 
                    echo '<tr onclick="obtenerdatoscontacto($(this))" data-atencionpara="'.$item->atencionpara.'" data-puesto="'.$item->puesto.'" data-telefono="'.$item->telefono.'">';
                    echo '<td>'.$item->atencionpara.'</td><td>'.$item->puesto.'</td><td>'.$item->telefono.'</td>';
                    echo '</tr>';
                   } ?>
              </tbody>
            </table>
          </div>
        </div>
    </div>
    <div class="modal-footer">
        <a class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
    </div>
    <!-- Modal finaliza fin-->
</div>
<!-- Modal finaliza fin-->


<div id="modal_addaccesorios" class="modal fade modal-fixed-footer" data-keyboard="false" data-backdrop="static">
    <div class="modal-content " style="padding-left: 0px; padding-right: 0px;">
        <h4 style="padding-left: 24px">Agregar</h4>
        <div class="row " style="margin-bottom: 0px;">
          <div class="col s2 addmodalinfomodelo" style="text-align:center;">
            
          </div>
          <div class="col s2">
            <label>Cantidad</label>
            <input id="selected_newaccesorio_cantidad_add" class=" form-control-bmz" type="number" value="1">
            <input id="equiporow_extra_add" class=" form-control-bmz" type="hidden" value="">
            <input id="equipo_extra_add" class=" form-control-bmz" type="hidden" value="">
            <input id="renta_extra_add" class=" form-control-bmz" type="hidden" value="<?php echo $idRenta;?>">
          </div>
          <div class="col s3">
            <label>Accesorio</label>
            <select id="selected_newaccesorio_add" name="selected_newaccesorio" class="browser-default  form-control-bmz"></select>
          </div>
          <div class="col s3">
            <label>Bodega</label>
            <select id="selected_newequipo_b_add" name="selected_newequipo_b" class="browser-default form-control-bmz">
                <?php foreach ($restbodegas as $item) { ?>
                  <option value="<?php echo $item->bodegaId; ?>"><?php echo $item->bodega; ?></option>
                <?php } ?> 
            </select>
          </div>
          <div class="col s2">
            <a class="btn-floating waves-effect waves-light green darken-1" onclick="addaccesorioadd();">
                          <i class="material-icons">vertical_align_bottom</i>
                        </a>
          </div>
        </div>
        <div class="row">
          <div class="col s12">
            <table class="table" id="new_table_accesorios_add">
              <thead>
                <tr>
                  <th>Cantidad</th>
                  <th>Accesorios</th>
                  <th>Bodega</th>
                  <th></th>
                </tr>
              </thead>
              <tbody class="nttbodya_add">
                
              </tbody>
            </table>
          </div>
        </div>
    </div>
    <div class="modal-footer">
        <a class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
        <a class="modal-action modal-close waves-effect waves-red gray btn-flat " onclick="agregar_ext_accesorio()">Agregar</a>
    </div>
    <!-- Modal finaliza fin-->
</div>
<style type="text/css">
  #modal_reodenar{
    top: 5% !important;
    height: 90%;
    max-height: 90%;
  }
</style>
<div id="modal_reodenar" class="modal fade modal-fixed-footer" data-keyboard="false" data-backdrop="static">
    <div class="modal-content " style="padding-left: 0px; padding-right: 0px;">
        <h4 style="padding-left: 24px">Reordenar Equipos</h4>
        <input type="hidden" id="id_equipo_row">
        <input type="hidden" id="id_equ_mod_row">
        <input type="hidden" id="id_equ_serie_row">
        <input type="hidden" id="id_tipo_row">
        <input type="hidden" id="id_consu_acce_row">
        <div class="inforeordenar" style="margin-bottom: 0px;">
          
        </div>
        

    </div>
    <div class="modal-footer">
        <a class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
    </div>
    <!-- Modal finaliza fin-->
</div>

<style type="text/css">
  .modal-sm {
      width: 500px;
  }
</style>
<div id="modalasignar" class="modal modal-sm">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <h5>¿Desea especificar de donde obtendra la información de los contadores?</h5>
      <div class="row">
         <div class="col s12">
            <label for="tiporeporte" style="color: #ff000000;">__</label>
            <select class="browser-default form-control-bmz" id="solicitud_info">
              <option value="">Selecciona una opción</option> 
              <option value="PARTNER">PARTNER</option> 
              <option value="KFS">KFS</option> 
              <option value="POR TÉCNICO">POR TÉCNICO</option> 
            </select>
          </div>
      </div>
    </div>
    <div class="modal-footer">
      <a class="modal-action modal-close waves-effect waves-red gray btn-flat " onclick="solicitudinfo_reg()">Aceptar</a>
      <a class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
    </div>
  </div>
</div>




<div id="modal_condiciones" class="modal fade modal-fixed-footer" data-keyboard="false" data-backdrop="static" style="width: 95%;">
    <div class="modal-content " style="padding-left: 0px; padding-right: 0px;">
        <h4 style="padding-left: 24px">Condiciones</h4>
        
        <ul class="collapsible" data-collapsible="accordion">
          <li >
            <div class="collapsible-header ">Historial</div>
            <div class="collapsible-body">
              <div class="row">
                <div class="col s12 tablelist_con">
                  
                </div>
              </div>
            </div>
          </li>
          <li>
            <div class="collapsible-header view_detalles_extras">Agregar Condiciones</div>
            <div class="collapsible-body" >
              <form id="form_condg" >
                <div class="row">
                  <div class="col s2">
                    <input type="hidden" class="input_select_con inc_v" id="con_id" name="con_id" value="0">
                    <input type="hidden" class="input_select_con" id="con_renta" name="con_renta" value="<?php echo $idRenta ?>">
                    <select class="browser-default form-control-bmz input_select_con" id="con_tipo" name="con_tipo" onchange="con_tipochange()">
                      <option value="2">Global</option>
                      <option value="1">Individual</option>
                    </select>
                  </div>
                </div>
                <div class="row">
                  <div class="col s2">
                    <label>Fecha inicial</label>
                    <input type="date" class="form-control-bmz input_select_con inc_v" id="con_fechainicial" name="con_fechainicial">
                  </div>
                  <div class="col s2" style="display:none;">
                    <label>Fecha Final</label>
                    <input type="date" class="form-control-bmz input_select_con inc_v" id="con_fechafinal" name="con_fechafinal">
                  </div>
                  <div class="col s10">
                    <label>Comentario</label>
                    <input type="text" class="form-control-bmz input_select_con inc_v" id="comentario" name="comentario">
                  </div>
                </div>
                <div class="row div_global">
                  <div class="col s2">
                    <label>Renta Monocromatica</label>
                    <input type="number" class="form-control-bmz input_select_con inc_v" id="con_renta_m" name="con_renta_m">
                  </div>
                  <div class="col s2">
                    <label>Click Monocromatica</label>
                    <input type="number" class="form-control-bmz input_select_con inc_v" id="con_click_m" name="con_click_m">
                  </div>
                  <div class="col s2">
                    <label>Excedentes Monocromatica</label>
                    <input type="number" class="form-control-bmz input_select_con inc_v" id="con_excedente_m" name="con_excedente_m">
                  </div>
                  <div class="col s2">
                    <label>Renta Color</label>
                    <input type="number" class="form-control-bmz input_select_con inc_v" id="con_renta_c" name="con_renta_c">
                  </div>
                  <div class="col s2">
                    <label>Click Color</label>
                    <input type="number" class="form-control-bmz input_select_con inc_v" id="con_click_c" name="con_click_c">
                  </div>
                  <div class="col s2">
                    <label>Excedentes Color</label>
                    <input type="number" class="form-control-bmz input_select_con inc_v" id="con_excedente_c" name="con_excedente_c">
                  </div>
                </div>
              </form>
              <div class="row div_individual" style="display:none;">
                <table class="striped" id="table_individual">
                  <thead>
                    <tr>
                      <th>MODELO</th>
                      <th>SERIE</th>
                      <th>Renta Monocromatica</th>
                      <th>Click Monocromatica</th>
                      <th>Excedentes Monocromatica</th>
                      <th>Renta Color</th>
                      <th>Click Color</th>
                      <th>Excedentes Color</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($equiposerie as $item) { 
                        $html_d_i='';
                        $html_d_i.='<tr>';
                        $html_d_i.='<td>';
                          $html_d_i.='<input type="hidden" id="con_id" class="inc con_id_'.$item->id.'_'.$item->serieId.'" value="0">';
                          $html_d_i.='<input type="hidden" id="con_equiporow"  value="'.$item->id.'">';
                          $html_d_i.='<input type="hidden" id="con_serieId"  value="'.$item->serieId.'">';
                          $html_d_i.=$item->modelo;
                        $html_d_i.='</td>';
                        $html_d_i.='<td>'.$item->serie.'</td>';
                        $html_d_i.='<td><input type="number" class="form-control-bmz input_select_con inc cond_renta_m_con_idd_'.$item->id.'_'.$item->serieId.'" id="cond_renta_m"></td>';
                        $html_d_i.='<td><input type="number" class="form-control-bmz input_select_con inc cond_click_m_con_idd_'.$item->id.'_'.$item->serieId.'" id="cond_click_m"></td>';
                        $html_d_i.='<td><input type="number" class="form-control-bmz input_select_con inc cond_excedente_m_con_idd_'.$item->id.'_'.$item->serieId.'" id="cond_excedente_m"></td>';
                        $html_d_i.='<td><input type="number" class="form-control-bmz input_select_con inc cond_renta_c_con_idd_'.$item->id.'_'.$item->serieId.'" id="cond_renta_c"></td>';
                        $html_d_i.='<td><input type="number" class="form-control-bmz input_select_con inc cond_click_c_con_idd_'.$item->id.'_'.$item->serieId.'" id="cond_click_c"></td>';
                        $html_d_i.='<td><input type="number" class="form-control-bmz input_select_con inc cond_excedente_c_con_idd_'.$item->id.'_'.$item->serieId.'" id="cond_excedente_c"></td>';
                      $html_d_i.='</tr>';
                      echo $html_d_i;
                       } ?>
                  </tbody>
                </table>
              </div>

            </div>
          </li>
        </ul>

    </div>
    <div class="modal-footer">
        <a class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
        <a class="waves-effect waves-red red btn-flat" style="color: white;" onclick="savecondiciones()">Save condiciones</a>
    </div>
    <!-- Modal finaliza fin-->
</div>

<div id="modalhojasestado" class="modal" >
  <div class="modal-content" style="padding: 5px">
    <div class="row">
      <div class="col s12">
        <h4>Hoja de estado</h4>
      </div>
    </div>
    <div class="row">
      <div class="col s12 ">
        <input type="file" name="hojasestado" id="hojasestado">
      </div>
      <div class="col s12 docfilehojasestado">
        
      </div>
    </div>
  </div>  
  <div class="modal-footer">
    <a class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
  </div>
</div>

<div id="modalretiroequipos" class="modal" >
  <div class="modal-content" style="padding: 5px">
    <div class="row">
      <div class="col s12">
        <h4>Retiro</h4>
      </div>
    </div>
    <div class="row">
      <div class="col s12 ">
        <p>¿Confirma Eliminación y solicitar su retiro de los equipos seleccionados?</p>
      </div>
      <div class="col s12 doctableretiros">
        
      </div>
      <div class="col s12">
        <label>Comentario</label>
        <textarea class="form-control-bmz" id="comen_retiro"></textarea>
      </div>
    </div>
  </div>  
  <div class="modal-footer">
    <a class="modal-action modal-close waves-effect waves-red gray btn-flat " id="retiromodal">Aceptar</a>
    <a class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
  </div>
</div>

<script type="text/javascript">
  $(document).ready(function($) {
      <?php if($existeC==1){ ?>
          traerdatoscontrato(<?php echo $idcontrato; ?>);
      <?php if($estatus==0){ ?>
              bloquedabtn();

      <?php }
          }else{ ?>
        obtienedatosdecontrato(<?php echo $idRenta; ?>);
        <?php 
            if($estatus==0){ ?>
              bloquedabtn();

      <?php
            }
       } ?>
  });
  function obtenerdatoscontacto(datos){
    var atencionpara = datos.data('atencionpara');
    var puesto = datos.data('puesto');
    var telefono = datos.data('telefono');
    $('#contacto').val(atencionpara);
    $('#cargoarea').val(puesto);
    $('#telefono').val(telefono);

  }
  function modaldatoscontacto(){
    <?php if($existeC==0){ ?>
    $('.modal').modal();
    $('#modal_datoscontacto').modal('open');
  <?php } ?>
  }
</script>
<div id="modalsolicitudinstalacion" class="modal" >
  <div class="modal-content" style="padding: 5px">
    <div class="row">
      <div class="col s12">
        <h4>Solicitud Instalacion</h4>
      </div>
    </div>
    <div class="row">
      <div class="col s12">¿Confirma la solicitud de instalacion para equipos seleccionados?</div>
      <div class="col s12">
        <table class="table bordered striped" id="t_eq_sol_insta">
          <thead><tr><th>Equipo</th><th>Serie</th><th></th><th>KFS</th><th>Comentario</th></tr></thead>
          <tbody class="table_equipos_insta"></tbody>
        </table>
      </div>
      <div class="col s12"><textarea class="form-control-bmz" id="comentariof" placeholder="comentario"></textarea></div>
    </div>
  </div>  
  <div class="modal-footer">
    <a class="modal-action modal-close waves-effect waves-red gray btn-flat " id="oksolinsta">Aceptar</a>
    <a class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
  </div>
</div>



<div id="modalseleccionequipos" class="modal" >
  <div class="modal-content" style="padding: 5px">
    <div class="row">
      <div class="col s12">
        <h4>Seleccion de equipos</h4>
      </div>
    </div>
    <div class="row">
      <div class="col s12 div_table_equipos"></div>
    </div>
  </div>  
  <div class="modal-footer">
    <button class="b-btn b-btn-success " id="okseleccionequipos">Aceptar</button>
    <!--<a class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>-->
  </div>
</div>