var base_url = $('#base_url').val();

$(document).ready(function(){

	table = $('#tabla_polizas').DataTable();
	load();

	// Listener para la acción de editar en la tabla de Pólizas
    $('#tabla_polizas tbody').on('click', 'a.edit', function () 
    {
        var tr = $(this).closest('tr');
        var row = table.row(tr);
        var data = row.data();
        window.location = base_url+'index.php/Polizas/edicion/'+data.id;
    });

    // Listener 
        $('#tabla_polizas tbody').on('click', 'a.visualiza', function () 
    {
        var tr = $(this).closest('tr');
        var row = table.row(tr);
        var data = row.data();
        window.location = base_url+'index.php/Polizas/visualiza/'+data.id;
    });
    
        // Listener para Eliminar 
    $('#tabla_polizas tbody').on('click', 'a.elimina', function () 
    {
        var tr = $(this).closest('tr');
        var row = table.row(tr);
        var data = row.data();
        confirmaEliminar(data.id);
    });

    /*
    // Listener para la acción de editar en la tabla de Pólizas
    $('#tabla_polizas tbody').on('click', 'a.elimina', function () 
    {
        var tr = $(this).closest('tr');
        var row = table.row(tr);
        var data = row.data();
        window.location = base_url+'index.php/polizas/elimina/'+data.id;
    });
    */

});
function confirmaEliminar(id)
{
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de Eliminar esta poliza? El cambio será irreversible',
        type: 'red',
        typeAnimated: true,
        buttons: 
        {
            // Si le da click al botón de "confirmar"
            confirmar: function () 
            {
                $.ajax({
                    url: base_url+"index.php/polizas/eliminarpoliza/"+id,
                    success: function (response) 
                    {
                            $.alert({
                                boxWidth: '30%',
                                useBootstrap: false,
                                title: 'Éxito!',
                                content: 'Poliza Eliminada!'});
                    
                            setTimeout(function(){ 
                                window.location.href = base_url+"index.php/polizas"; 
                            }, 2000);   
                    },
                    error: function(response)
                    {
                        $.alert({
                                boxWidth: '30%',
                                useBootstrap: false,
                                title: 'Error!',
                                content: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema'});   
                        setTimeout(function(){ 
                                window.location.href = base_url+"index.php/polizas";
                        }, 2000);  
                        
                    }
                });
            },
            // Si le da click a "cancelar"
            cancelar: function () 
            {
                
            }
        }
    });
}

function load() {
	// Destruye la tabla y la crea de nuevo con los datos cargados 
	table.destroy();
	table = $('#tabla_polizas').DataTable({
        search: {
                return: true
            }, 
	    "ajax": {
	        "url": base_url+"index.php/Polizas/getListadoPolizas"
	    },
	    "columns": [
	        {"data": "id"},
	        {"data": "nombre"},
	        {"data": "cubre"},
	        {"data": "vigencia_meses"},
	        {"data": "vigencia_clicks"},           
	        {"data": null,
	             render:function(data,type,row){
	                $('.tooltipped').tooltip();
	                var html='    <a class="btn-floating green tooltipped edit soloadministradores" data-position="top" data-delay="50" data-tooltip="Editar">\
	                                <i class="material-icons">mode_edit</i>\
	                              </a>\
	                              <a class="btn-floating blue tooltipped visualiza" data-position="top" data-delay="50" data-tooltip="Visualizar">\
	                                <i class="material-icons">remove_red_eye</i>\
	                              </a>\
	                              <a class="btn-floating red tooltipped elimina soloadministradores" data-position="top" data-delay="50" data-tooltip="Eliminar">\
	                                <i class="material-icons">delete_forever</i>\
	                              </a>';
	                return html;
	            }
	        }
	    ],
	    "order": [[ 0, "desc" ]],
	    "lengthMenu": [[25, 50, 100], [25, 50, 100]],
	    "dom": 'Bfrtpl',
	    // Muestra el botón de Excel para importar la tabla
	    "buttons": [
	        /*
	        {   
	            extend: 'excelHtml5',
	            text: ' Descargar Excel <i class="fa fa-download"></i>',
	            className: 'btn classBotonFratsa'
	        }
	        */
	    ],
	    
	}).on('draw',function(){
        $('.dataTables_filter').append('<i class="fa fa-search buttonsearch" onclick="searchtable_v()"></i>')

    });
}
function searchtable_v(){
    var contenedor = document.getElementById('tabla_polizas_filter');

    // Selecciona el input dentro del contenedor
    var input = contenedor.querySelector('input.form-control-bmz');

    // Obtén el valor del input
    var searchv = input.value;

    // Muestra el valor en la consola
    console.log(searchv);
    //var searchv =$("input[type=search]").val();
    table.search(searchv).draw();
}