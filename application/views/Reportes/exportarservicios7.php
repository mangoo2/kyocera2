<?php 
	date_default_timezone_set('America/Mexico_City');
    $this->fechahoy = date('Y-m-d G:i:s');
    $this->fechaactual = date('Y-m-d');
    $this->horaactual = date('G:i:s');

    /*
	header("Pragma: public");
	header("Expires: 0");
	$filename = "Reporte_AST_".$this->fechaactual.".xls";
	header("Content-type: application/x-msdownload");
	header("Content-Disposition: attachment; filename=$filename");
	header("Pragma: no-cache");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	*/
	
?>
<table border="1">
	<tr>
		<th colspan="7">REPORTE: ANÁLISIS DE ATENCIÓN AL CLIENTE</th>
	</tr>
	<tr>
		<th colspan="7"></th>
	</tr>
	
	<tr>
		<th>Intervalo de Fechas para el análisis</th>
		<th>Cliente</th>
		<th>Falla Reportadas</th>
		<th>Asesor Técnico Asingado</th>
		<th>Observaciones del AT</th>
		<th>Refacciones remplazadas</th>
		<th>Fecha del remplazo</th>
	</tr>
	<tr>
		<th></th>
		<th></th>
		<th></th>
		<th></th>
		<th></th>
		<th></th>
		<th></th>
	</tr>
</table>