var base_url = $('#base_url').val();
var idcontrato_aux = $('#idcontrato').val();
var id_asig;
var cliente_id = $('#cliente_id').val();
$(document).ready(function(){
    $('#files').prop('disabled', false).attr('readonly', false);
    $('.modal').modal();
    $('.chosen-select').chosen({width: "90%"});
    // img
    $('#rfc').change(function(event) {
        var razonsocial=$('#rfc option:selected').data('razonsocial');
        $('#arrendataria').val(razonsocial);
    });
    $('.dropify').dropify({
        messages: {
            default: 'Arrastra y suelta un archivo aquí o haz clic',
            replace: 'Arrastre y suelte un archivo o haga clic para reemplazar',
            remove:  'Eliminar',
            error:  'Lo sentimos, el archivo es demasiado grande.'
        }
    });
    cargar_table_equipos();
    verificarFecha();
    obtenerultimacondicion();
	// Asignamos el formulario de envío
	var formulario = $('#contrato-form');
    var contratoexiste = $('#contratoexiste').val();
    var idRenta = $('#idRenta').val();
    if(contratoexiste == 1){
        $(".folio_c").attr('readonly', true);
        $("input").prop('readonly', true);
        $("textarea").prop('disabled', true);
        $("select").prop('disabled', true);
        $("checkbox").prop('disabled', true);
        $('#foto').prop('disabled', false);
        $('.guadarremove').remove();
        var contratofin = $('#contratofin').val();
        var idcontrato = $('#idcontrato').val();
        var estatus_fin = $('#estatus_fin').val();
        if(contratofin == 1){
            if(estatus_fin ==2){
                $('.botonfinalizar').html('<div class="input-field col s12"><a class="btn red waves-effect waves-light right" type="button" href="'+base_url+'index.php/Contratos">Regresar<i class="material-icons left">keyboard_backspace</i></a></div>');
            }else{
                $("#folio").prop('readonly', false);
                $('.botonfinalizar').html('<div class="input-field col s12"><button class="btn green waves-effect waves-light right finalizar" type="button" onclick="finalizar('+idcontrato+')">Finalizar Contrato<i class="material-icons right">send</i></button></div>');
            }                       
        }else{
            $('.botonfinalizar').html('<div class="input-field col s12"><a class="btn red waves-effect waves-light right" type="button" href="'+base_url+'index.php/Rentas">Regresar<i class="material-icons left">keyboard_backspace</i></a></div>'); 
        }   
                                    
    }else{
        console.log('no existe');
    }
    var c_btn_e='btn-bmz waves-effect waves-light ';
    var editarprocontra='';
        editarprocontra+='<a class="'+c_btn_e+' green darken-1" onclick="editarprocontra('+idRenta+')" title="Agregar Equipo">Agregar</a>';
        editarprocontra+='<a class="'+c_btn_e+' red darken-1" onclick="retiroequipos()" title="Agregar Equipo">Retiro</a>';
        editarprocontra+='<a class="'+c_btn_e+' red darken-1" onclick="moverequipos()" title="Mover Equipo">Mover equipo</a>';
        editarprocontra+='<a class="'+c_btn_e+' red darken-1" onclick="solicitudinfo()" title="Solicitud de información">Solicitud de información</a>';
        editarprocontra+='<a class="'+c_btn_e+' blue darken-1" onclick="instalacionequipos()" title="Solicitud de instalacion">Solicitud de Instalacion</a>';
        editarprocontra+='<a class="'+c_btn_e+' blue darken-1" onclick="suspencionall()" title="Suspensión Temporal">Suspensión Temporal</a>';
        editarprocontra+='<a class="'+c_btn_e+' blue darken-1" onclick="obtenerequipos_de_contratos('+idRenta+')" title="Entrega de equipos parcial">Entrega Parcial</a>';
        editarprocontra+='';
    $('.editarprocontra').html(editarprocontra);
	// Validamos el formulario en base a las reglas mencionadas debajo
	formulario.validate({
            ignore: "",
            rules:{
				rfc: {
                    required: true
                },
                tipocontrato:{
                    required: true
                }
            },
            // Para mensajes personalizados
            messages:{
                rfc:{
                    required: "Ingrese un RFC"
                },
                tipocontrato:{
                    required: "Seleccione una vigencia"
                }
            },
            errorElement : 'div',
            // Si están correctos los campos, procesamos el formulario mediante Ajax
            submitHandler: function (form){
                
            }
        });
    $('#okseleccionequipos').click(function(event) {
        $( "#okseleccionequipos" ).prop( "disabled", true );
        setTimeout(function(){ 
             $("#okseleccionequipos" ).prop( "disabled", false );
        }, 5000);
        okseleccionequipos();
    });
    $('.registro').click(function(event) {
        $( ".registro" ).prop( "disabled", true );
        setTimeout(function(){ 
             $(".registro" ).prop( "disabled", false );
        }, 5000);
        var DATA3  = [];

        var TABLA3 = $(".tabla_pagos ul > li");
        TABLA3.each(function(){         
                item3 = {};
                item3 ["equiposrow"] = $(this).find("input[id*='equipo_row']").val();
                item3 ["serieId"] = $(this).find("input[id*='serie_row']").val();
                item3 ["rentacosto"] = $(this).find("input[id*='rentadeposito_row']").val();
                item3 ["pag_monocromo_incluidos"] = $(this).find("input[id*='clicks_mono_row']").val();
                item3 ["excedente"]  = $(this).find("input[id*='precio_c_e_mono_row']").val();
                
                item3 ["rentacostocolor"] = $(this).find("input[id*='rentacostocolor_row']").val();
                item3 ["pag_color_incluidos"] = $(this).find("input[id*='clicks_color_row']").val();
                item3 ["excedentecolor"] = $(this).find("input[id*='precio_c_e_color_row']").val();
                DATA3.push(item3);
            });
            dataequiposclicks   = JSON.stringify(DATA3);
        var DATAdir  = [];
        var TABLAdir = $(".table_direcciones tbody > tr");
        TABLAdir.each(function(){ 
            itemd = {};
            itemd ["orden"] = $(this).find("input[id*='orden']").val();
            var ubicacion_c = $(this).find("input[id*='ubicacion']").val();
                    ubicacion_c=ubicacion_c.replace('&', ' ');
            itemd ["ubicacion"] = ubicacion_c;
            itemd ["equiposrow"] = $(this).find("input[id*='equipo_dir']").val();
            itemd ["serieId"] = $(this).find("input[id*='serie_dir']").val();
            itemd ["direccion"] = $(this).find("select[id*='domicilioinstalacion'] option:selected").val();
            itemd ["des_click"] = $(this).find("input[id*='des_click']").val();
            itemd ["des_escaneo"] = $(this).find("input[id*='des_escaneo']").val();
            DATAdir.push(itemd);
        });
        dataequiposdirecciones   = JSON.stringify(DATAdir);
       	var validar = $('#contrato-form').valid();
       	if (validar) {
            $.confirm({
                boxWidth: '30%',
                useBootstrap: false,
                icon: 'fa fa-warning',
                title: 'Atención!',
                content: 'Se necesita permisos de administrador<br>'+
                         '<input type="password" placeholder="Contraseña" id="contrasena" class="name form-control" required/>',
                type: 'red',
                typeAnimated: true,
                buttons:{
                    confirmar: function (){
                        var pass=$('#contrasena').val();
                        if (pass!='') {
                            $.ajax({
                                type:'POST',
                                url: base_url+"index.php/Sistema/solicitarpermiso",
                                data: {
                                    pass:pass
                                },
                                success: function (response){
                                        var respuesta = parseInt(response);
                                        if (respuesta==1) {
                            //=============================================================================
                                var datos = formulario.serialize()+'&equipos='+dataequiposclicks+'&direcciones='+dataequiposdirecciones;
                                $.ajax({
                                	type:'POST',
                                	url: base_url+'index.php/Rentas/registrar',
                                	data: datos,
                                	success:function(data){
                                        var idcontra=parseInt(data);
                                		if(idcontra>0){
                                            var idRenta =$('#idRenta').val();
                                            confirmarsolicitudtoner(idRenta);
                                            //confirmarsolicitudinstalacion(idRenta);
                                            confirmartipodeinstalacion(idRenta);
                                		}
                                		// En caso contrario, se notifica
                                		else{
                                			notificacion(1);
                                            $('.registro').attr('disabled',true);
                                		}
                                	}
                                });
                            //=============================================================================
                                        }else{
                                            notificacion(2);
                                        }
                                },
                                error: function(response){
                                    notificacion(1);
                                }
                            });

                        }else{
                            notificacion(0);
                        }
                        
                    },
                    cancelar: function () {
                        
                    }
                }
            });
       	}else{
       		swal({ title: "Error",
                text: "Falta llenar campos",
                type: 'error',
                showCancelButton: false,
                allowOutsideClick: false,
            });
       	}
    });
    $('#tiporenta').change(function(event) {
        if ($('#tiporenta').val()==1) {
            $('.row_global').hide();
            $('.row_individual').show();
            calculartotales();
            //$("#facturacionlibre").attr('disabled', true);
            //$("#facturacionlibre").prop('checked', false);
        }else{
            //$("#facturacionlibre").attr('disabled', false);
            $('.row_individual').hide();
            $('.row_global').show();
        }
    }); 
    $('#facturacionlibre').change(function(event) {
        if($('#facturacionlibre').is(':checked')){
            $('#rentadeposito').val(0).attr('readonly', true);
            $('#clicks_mono').val(0).attr('readonly', true);
            //$('#precio_c_e_mono').val(0).attr('readonly', true);
            $('#rentacolor').val(0).attr('readonly', true);
            $('#clicks_color').val(0).attr('readonly', true);
            //$('#precio_c_e_color').val(0).attr('readonly', true);

            //$('.habilitardeshabilitar').val(0).attr('readonly', true);
            $('.cambiotitulolibrem').html('Precio click');
            $('.cambiotitulolibrec').html('Precio click color');
            $('.rentadeposito_rows').val(0).attr('readonly',true);
            $('.clicks_mono_row').val(0).attr('readonly',true);
            $('.rentacostocolor_row').val(0).attr('readonly',true);
            $('.clicks_color_row').val(0).attr('readonly',true);
        }else{
            $('#rentadeposito').attr('readonly', false);
            $('#clicks_mono').attr('readonly', false);
            //$('#precio_c_e_mono').attr('readonly', false);
            $('#rentacolor').attr('readonly', false);
            $('#clicks_color').attr('readonly', false);
            //$('#precio_c_e_color').attr('readonly', false);

            $('.cambiotitulolibrem').html('Precio click excedente');
            $('.cambiotitulolibrec').html('Precio click color excedente');

            $('.rentadeposito_rows').val(0).attr('readonly',false);
            $('.clicks_mono_row').val(0).attr('readonly',false);
            $('.rentacostocolor_row').val(0).attr('readonly',false);
            $('.clicks_color_row').val(0).attr('readonly',false);

        }
    });
    $('.rentadeposito_rows').change(function(event) {
        calculartotales()
    });
    $('.rentacostocolor_row').change(function(event) {
        calculartotales()
    }); 	

    $('#suspender_equipo').validate({
        rules: {
            fecha_fin_susp: "required",
        },
        messages: {
            fecha_fin_susp: "Campo requerido",
        },
        submitHandler: function (form) {
             $.ajax({
                 type: "POST",
                 url: base_url+'index.php/Contratos/suspenderRentaE/'+$("#id_equipo").val(),
                 data: $(form).serialize(),
                 beforeSend: function(){
                    $("#saveSE").attr("disabled",true);
                 },
                 success: function (result) {
                    //console.log(result);
                    $("#saveSE").attr("disabled",false);
                    swal("Exito!", "Se suspendió correctamente", "success");
                    $('#modal_susp').modal('close'); // $('.modal-backdrop').hide();
                    cargar_table_equipos();
                 }
             });
             return false; // required to block normal submit for ajax
         },
        errorPlacement: function(label, element) {
          label.addClass('mt-2 text-danger');
          label.insertAfter(element);
        },
        highlight: function(element, errorClass) {
          $(element).parent().addClass('has-danger')
          $(element).addClass('form-control-danger')
        }
    });

    /*$('#tabla_equipos').DataTable({
        fixedHeader: true,
        responsive: !0,
        "order": [[ 0, "desc" ]],
        "paging": false,
        "info": false,
        "lengthChange": false,
        "searching": false
    });*/

    /*$('.cambiar').click(function(event) {
        console.log("cambiar");
        $('#modal_addequipos').modal('open');
        var id = $(this).data("id");
        $('#id_pedido').val(id);
 
    });*/
    var edit_c = $('#edit_tipo').val();
    if(edit_c==1){
        setTimeout(function(){ 
            $('#rentaadelantada').removeAttr('disabled', true).trigger("chosen:updated");
            $(".folio_c").attr('readonly', false);
            //$('#tipocontrato').removeAttr('disabled', true).trigger("chosen:updated");
            $('#tipopersona').removeAttr('disabled', true).trigger("chosen:updated");
            $('#vigencia').removeAttr('disabled', true).trigger("chosen:updated");
            $('#fechasolicitud').prop('disabled', false).attr('readonly', false);
            $('#fechainicio').prop('disabled', false).attr('readonly', false);
            $('#arrendataria').prop('disabled', false).attr('readonly', false);
            $('#rfc').removeAttr('disabled', true).trigger("chosen:updated");
            $('#actaconstitutiva').prop('disabled', false).attr('readonly', false);
            $('#fecha').prop('disabled', false).attr('readonly', false);
            $('#notariapublicano').prop('disabled', false).attr('readonly', false);
            $('#titular').prop('disabled', false).attr('readonly', false);
            $('#representantelegal').prop('disabled', false);
            $('#instrumentofechasolicitud').prop('disabled', false);
            $('#instrumentonotariapublicano').prop('disabled', false);
            $('#fechainicio').prop('disabled', false).attr('readonly', false);
            $('#instrumentonotarial').prop('disabled', false).attr('readonly', false);
            $('#domiciliofiscal').removeAttr('disabled', true).trigger("chosen:updated");
            $('#fechainicio').prop('disabled', false).attr('readonly', false);
            $('.domicilioinstalacion').removeAttr('disabled', true).trigger("chosen:updated");
            $('#contacto').prop('disabled', false).attr('readonly', false);
            $('#contacto').prop('readonly', false).attr('readonly', false);
            $('#rentadeposito').prop('readonly', false);
            $('#cargoarea').prop('disabled', false).attr('readonly', false);
            $('#cargoarea').prop('readonly', false).attr('readonly', false);
            $('#telefono').prop('disabled', false).attr('readonly', false);
            $('#telefono').prop('readonly', false).attr('readonly', false);
            $('#servicio_horario').prop('disabled', false).attr('readonly', false);
            $('#servicio_equipo').prop('disabled', false).attr('readonly', false);
            $('#servicio_doc').prop('disabled', false).attr('readonly', false);
            $('#ordencompra').prop('disabled', false).attr('readonly', false);
            $('#horaentregainicio').prop('disabled', false).attr('readonly', false);
            $('#horaentregafin').prop('disabled', false).attr('readonly', false);
            $('.habilitardeshabilitar').prop('disabled',false).attr('readonly', false);
            $('#tiporenta').prop('disabled', false).trigger("chosen:updated");
            $('#facturacionlibre').prop('readonly', false);

            $('#clicks_mono').prop('readonly', false);
            $('#precio_c_e_mono').prop('readonly', false);
            $('#rentacolor').prop('readonly', false);
            $('#rentadeposito').prop('readonly', false);
            $('#clicks_color').prop('readonly', false);
            $('#precio_c_e_color').prop('readonly', false);

            $('#representantelegal').prop('disabled', false).attr('readonly', false);
            $('#instrumentofechasolicitud').prop('disabled', false).attr('readonly', false);
            $('#instrumentonotariapublicano').prop('disabled', false).attr('readonly', false);
            $('.ordene').prop('disabled', false).attr('readonly', false);
            $('.ubicacion').prop('disabled', false).attr('readonly', false);
            $('.descuentos').prop('disabled', false).attr('readonly', false);
            $('.comenext').prop('disabled', false).attr('readonly', false);
            $('#oc_vigencia').prop('disabled', false).attr('readonly', false);
            $('#hojasestado').prop('disabled', false);
            $('#files').prop('disabled', false).attr('readonly', false);
            $('#search_serie').prop('disabled', false).attr('readonly', false);
        },2000);
        $('#tipocontrato').change(function(event) {
            var tip_cotr_e = $('#tipocontrato option:selected').val();
            var tipo_contrato_e= $('.tipo_contrato_e').text();
            if(tip_cotr_e < tipo_contrato_e){
                swal("Atención", "No puedes seleccionar meses hacia abajo", "error"); 
                $("#tipocontrato option[value="+tipo_contrato_e+"]").attr("selected",true);
                $('#tipocontrato').val(tipo_contrato_e);
                $('#tipocontrato').trigger("chosen:updated");
                /*
                setTimeout(function(){
                    $("#tipocontrato option[value="+tipo_contrato_e+"]").attr("selected",true);
                    $('#tipocontrato').trigger('liszt:updated');
                },2000);
                */
            }
        }); 
        $('#vigencia').change(function(event) {
            var vigen_e = $('#vigencia option:selected').val();
            var vigencia_e= $('.vigencia_e').text();
            if(vigen_e < vigencia_e){
                swal("Atención", "No puedes seleccionar meses hacia abajo", "error"); 
                $("#vigencia option[value="+vigencia_e+"]").attr("selected",true);
                $('#vigencia').val(vigencia_e);
                $('#vigencia').trigger("chosen:updated");
            }
        });
        $('.botonfinalizar').html('<div class="input-field col s12">\
            <button class="btn cyan waves-effect waves-light right editar_registro" type="button">Editar\
              <i class="material-icons right">send</i>\
            </button>\
          </div>');
        $('.editar_registro').click(function(event) {
            var validar = $('#contrato-form').valid();
            //========================================================
            var DATAdir  = [];
            var TABLAdir = $(".table_direcciones tbody > tr");
            TABLAdir.each(function(){ 
                itemd = {};
                var ubicacion_c = $(this).find("input[id*='ubicacion']").val();
                    ubicacion_c=ubicacion_c.replace('&', ' ');
                itemd ["ubicacion"] = ubicacion_c;
                itemd ["orden"] = $(this).find("input[id*='orden']").val();
                itemd ["equiposrow"] = $(this).find("input[id*='equipo_dir']").val();
                itemd ["serieId"] = $(this).find("input[id*='serie_dir']").val();
                itemd ["direccion"] = $(this).find("select[id*='domicilioinstalacion'] option:selected").val();
                itemd ["des_click"] = $(this).find("input[id*='des_click']").val();
                itemd ["des_escaneo"] = $(this).find("input[id*='des_escaneo']").val();
                DATAdir.push(itemd);
            });
            dataequiposdirecciones   = JSON.stringify(DATAdir);
            //========================================================
            var DATA3  = [];

            var TABLA3 = $(".tabla_pagos ul > li");
            TABLA3.each(function(){         
                item3 = {};
                item3 ["equiposrow"] = $(this).find("input[id*='equipo_row']").val();
                item3 ["serieId"] = $(this).find("input[id*='serie_row']").val();
                item3 ["rentacosto"] = $(this).find("input[id*='rentadeposito_row']").val();
                item3 ["pag_monocromo_incluidos"] = $(this).find("input[id*='clicks_mono_row']").val();
                item3 ["excedente"]  = $(this).find("input[id*='precio_c_e_mono_row']").val();
                
                item3 ["rentacostocolor"] = $(this).find("input[id*='rentacostocolor_row']").val();
                item3 ["pag_color_incluidos"] = $(this).find("input[id*='clicks_color_row']").val();
                item3 ["excedentecolor"] = $(this).find("input[id*='precio_c_e_color_row']").val();
                DATA3.push(item3);
            });
            dataequiposclicks   = JSON.stringify(DATA3);
            var DATAdir  = [];

            if (validar) {
                var datos = formulario.serialize()+'&idRenta='+$('#idRenta').val()+'&equipos='+dataequiposclicks+'&direcciones='+dataequiposdirecciones;
                if($('.cambiorealizado').length>0){
                    $.confirm({
                        boxWidth: '30%',
                        useBootstrap: false,
                        icon: 'fa fa-warning',
                        title: 'Atención!',
                        content: 'Se necesita permisos de administrador<br>'+
                                 '<input type="password" placeholder="Contraseña" id="contrasena" class="name form-control" required/>',
                        type: 'red',
                        typeAnimated: true,
                        buttons:{
                            confirmar: function (){
                                var pass=$('#contrasena').val();
                                if (pass!='') {
                                    $.ajax({
                                        type:'POST',
                                        url: base_url+"index.php/Sistema/solicitarpermiso",
                                        data: {
                                            pass:pass
                                        },
                                        success: function (response){
                                            var respuesta = parseInt(response);
                                            if (respuesta==1) {
                                                //============================esta opcion debe de ser la misma a la otra dentro del if===================================
                                                    
                                                    $.ajax({
                                                        type:'POST',
                                                        url: base_url+'index.php/Rentas/update_registrar',
                                                        data: datos,
                                                        success:function(data){
                                                            // Si existe el usuario, mostramos el mensaje de entrada y redirigimos
                                                            if(data!=0){
                                                                
                                                                
                                                                swal({ title: "Éxito",
                                                                    text: "Se guardo correctamente",
                                                                    type: 'success',
                                                                    showCancelButton: false,
                                                                    allowOutsideClick: false,
                                                                });
                                                                $('.registro').attr('disabled',true);
                                                                setTimeout(function(){ 
                                                                   window.location.href = base_url+'index.php/Contratos'; 
                                                                }, 2000);
                                                                
                                                            }
                                                            // En caso contrario, se notifica
                                                            else{
                                                                notificacion(1);
                                                                $('.registro').attr('disabled',true);
                                                            }
                                                        }
                                                    });
                                                //===============================================================
                                             }else{
                                                notificacion(2);
                                            }
                                        },
                                        error: function(response){
                                            notificacion(1);
                                        }
                                    });
                                }else{
                                    notificacion(0);
                                }
                            },
                            cancelar: function () {
                            }
                        }
                    });
                }else{
                    //============================esta opcion debe de ser la misma a la otra dentro del if===================================                    
                        $.ajax({
                            type:'POST',
                            url: base_url+'index.php/Rentas/update_registrar',
                            data: datos,
                            success:function(data){
                                // Si existe el usuario, mostramos el mensaje de entrada y redirigimos
                                if(data!=0){
                                    
                                    
                                    swal({ title: "Éxito",
                                        text: "Se guardo correctamente",
                                        type: 'success',
                                        showCancelButton: false,
                                        allowOutsideClick: false,
                                    });
                                    $('.registro').attr('disabled',true);
                                    setTimeout(function(){ 
                                       window.location.href = base_url+'index.php/Contratos'; 
                                    }, 2000);
                                    
                                }
                                // En caso contrario, se notifica
                                else{
                                    notificacion(1);
                                    $('.registro').attr('disabled',true);
                                }
                            }
                        });
                    //===============================================================
                }

            }else{
                swal({ title: "Error",
                    text: "Falta llenar campos",
                    type: 'error',
                    showCancelButton: false,
                    allowOutsideClick: false,
                });
            }
        });
    }
    
    $('#tipocontrato').change(function(event) {
        var tipoc = $('#tipocontrato option:selected').val();
        $('#vigencia').val(tipoc).trigger("chosen:updated");
    });
    $("#hojasestado").fileinput({
            showCaption: false,
            showUpload: true,// quita el boton de upload
            dropZoneEnabled: false,//minimiza el preview y solo se despliea al cargar un archivo
            //rtl: true,
            allowedFileExtensions: ["jpg","png","webp","jpeg","pdf"],
            browseLabel: 'Seleccionar documento',
            uploadUrl: base_url+'Contratos/hojasestado',
            maxFilePreviewSize: 5000,
            previewFileIconSettings: {
                'docx': '<i class="fa fa-file-word-o text-primary"></i>',
                'xlsx': '<i class="fa fa-file-excel-o text-success"></i>',
                'pptx': '<i class="fa fa-file-powerpoint-o text-danger"></i>',
                'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
                'pdf': '<i class="fa fa-file-pdf-o text-danger"></i>',
                'zip': '<i class="fa fa-file-archive-o text-muted"></i>',
                'cer': '<i class="fas fa-file-invoice"></i>',
            },
            uploadExtraData: function (previewId, index) {
            var info = {
                        id_asig:id_asig
                    };
            return info;
          }
        }).on('filebatchuploadcomplete', function(event, files, extra) {
          //location.reload();
            toastr.success('Se cargo Correctamente','Hecho!');
              
        }).on('filebatchuploadsuccess', function(event, files, extra) {
          //location.reload();
          toastr.success('Se cargo Correctamente','Hecho!');
          
    });
    $('#retiromodal').click(function(event) {
        retiromodal();
    });
    obtenerventaspendientes();

    $('#oksolinsta').click(function(event) {
        oksolinsta();
    });
    files_oc();
    if(idcontrato_aux>0){
        $('.files_div_oc_contrato').show();
    }else{
        $('.files_div_oc_contrato').hide();
    }
    
    $('#dir_des').select2();
    $('#search_serie').prop('disabled', false).attr('readonly', false);

    //=====================
        //funcion para cuando llegue a determinada posicion se le agrege la class fixed-button
        /*
        var button = $('.editarprocontra');
        var originalOffsetTop = button.offset().top;
        $(window).scroll(function() {
            var scrollTop = $(window).scrollTop();
            if (scrollTop + 114 >= originalOffsetTop) {
                button.addClass('fixed-button');
            } else {
                button.removeClass('fixed-button');
            }
        });
        */
    //=====================
    $('#collap_equipos').click(function(event) {
        setTimeout(function(){                        
            var button = $('.editarprocontra');
            if ($('#collap_equipos').hasClass('active')) {
                console.log('El elemento tiene la clase "active".');
                 button.addClass('fixed-button');
            } else {
                console.log('El elemento NO tiene la clase "active".');
                button.removeClass('fixed-button');
            }
        }, 500);
    });
    $('.det_cam').on('input change', function () {
        // Agrega la clase .cambiorealizado al input que detectó el cambio
        $(this).addClass('cambiorealizado');
    });
});
function calculartotales(){
    var addtpcm = 0;
    $(".rentadeposito_rows").each(function() {
        var vstotalcm = $(this).val();
        addtpcm += Number(vstotalcm);
    });
    $('#rentadeposito').val(addtpcm);
    var addtpcc = 0;
    $(".rentacostocolor_row").each(function() {
        var vstotalcc = $(this).val();
        addtpcc += Number(vstotalcc);
    });
    $('#rentacolor').val(addtpcc);
}
/// Funcion que cambia de estatus el contrato a ya finalizado cuando ya esta generado
function finalizar(id){  
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Contratos/verificar_periodo",
        data: {
            contratoId:idcontrato_aux},
        success: function (response){
            console.log(response);
           ////
            var v = parseFloat(response);
            if(v==0){
                $('#modal_img').modal('open'); 
                $('#idcontrato_f').val(id);
            }else{
                /*
                $.alert({
                boxWidth: '30%',
                useBootstrap: false,
                title: 'Advertencia!',
                content: 'Se tiene periodos pendientes'}); 
                */
                //====================================================
                    $.confirm({
                        boxWidth: '30%',
                        useBootstrap: false,
                        icon: 'fa fa-warning',
                        title: 'Atención!',
                        content: 'Se tiene periodos pendientes. <br>¿Está seguro de continuar con la finalizacion?<br>Se necesita permisos de administrador<br>'+
                                 '<input type="password" placeholder="Contraseña" id="contrasena" class="name fc-bmz" required/>',
                        type: 'red',
                        typeAnimated: true,
                        buttons:{
                            confirmar: function (){
                                var pass=$('#contrasena').val();
                                if (pass!='') {
                                     $.ajax({
                                        type:'POST',
                                        url: base_url+"index.php/Sistema/solicitarpermiso",
                                        data: {
                                            pass:pass
                                        },
                                        success: function (response){
                                            var respuesta = parseInt(response);
                                            if (respuesta==1) {
                                                $('#modal_img').modal('open'); 
                                                $('#idcontrato_f').val(id);
                                            }else{
                                                notificacion(2); 
                                            }
                                        },
                                        error: function(response){
                                            notificacion(1);
                                        }
                                    });   
                                }else{
                                    notificacion(0);
                                }
                            },
                            cancelar: function () {
                            }
                        }
                    });
                //====================================================
            }
            ////
        },
        error: function(response){
            notificacion(1);
        }
    }); 

}
function agregarfile() {
    if ($('#foto')[0].files.length > 0) {
        var input = document.getElementById('foto');
        var file = input.files[0];
        if(file.size < 15000000){
            $('.cargagif').css('display','block');
            var inputFileImage = document.getElementById('foto');
            var file = inputFileImage.files[0];
            var data = new FormData();
            data.append('foto',file);
            data.append('idcontrato',$('#idcontrato_f').val());
            $.ajax({
                url:base_url+'index.php/Contratos/cargafiles',
                type:'POST',
                contentType:false,
                data:data,
                processData:false,
                cache:false, 
                success: function(data) {
                    var array = $.parseJSON(data);
                        console.log(array.ok);
                        if (array.ok=true) {
                          ///swal("Éxito!", "Se guardo el archivo correctamente", "success");
                          // finalizar
                              $.ajax({
                                    type:'POST',
                                    url: base_url+'index.php/Contratos/updatefinalizar',
                                    data: {id:$('#idcontrato_f').val(),folio:$('#folio').val()},
                                    success:function(data){
                                        /*
                                        swal({ title: "Éxito",text: "Se finalizo correctamente",type: 'success',showCancelButton: false,allowOutsideClick: false,});
                                        setTimeout(function(){ 
                                            window.location.href = base_url+'index.php/Contratos'; 
                                        }, 2000);
                                        */
                                        confirmfinal();
                                    }
                                });
                          //
                          $('#modal_img').modal('close'); 
                        }else{
                          swal("Error", "Error", "error"); 
                        }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                        var data = JSON.parse(jqXHR.responseText);
                    }
            });
        }else{
            swal("Archivo", "El archivo es mayor a 15 megas", "error"); 
        }    
    }else{
       swal("Archivo", "No has seleccionado ningun archivo", "error"); 
    }
}
function traerdatoscontrato(idcontrato){
    
    $.ajax({
        type:'POST',
        url: base_url+'index.php/Rentas/traerdatoscontrato',
        data: {contrato:idcontrato},
        success:function(data){
            var array = $.parseJSON(data);
            console.log(array);
            array.equipos.forEach(function(element) {
                console.log(element);
                $('.rentadeposito_row_'+element.equiposrow+'_'+element.serieId).val(element.rentacosto);
                $('.rentacostocolor_row_'+element.equiposrow+'_'+element.serieId).val(element.rentacostocolor);

                $('.clicks_mono_row_'+element.equiposrow+'_'+element.serieId).val(element.pag_monocromo_incluidos);
                $('.precio_c_e_mono_row_'+element.equiposrow+'_'+element.serieId).val(element.excedente);
                $('.clicks_color_row_'+element.equiposrow+'_'+element.serieId).val(element.pag_color_incluidos);
                $('.precio_c_e_color_row_'+element.equiposrow+'_'+element.serieId).val(element.excedentecolor);
            });/*
            array.eqdirecciones.forEach(function(element) {
                //$('.direccion_equipo_'+element.equiposrow+'_'+element.serieId).val(element.direccion);
                
                setTimeout(function(){ 
                    var atencioninfo=$('.direccion_equipo_'+element.equiposrow+'_'+element.serieId+' option:selected').data('atencionpara');
                    console.log(atencioninfo);
                    $('.tag_direccion_equipo_'+element.equiposrow+'_'+element.serieId).html(atencioninfo);

                }, 1000); 

            });
*/
        }
    });
    $('#folio').prop('readonly', true);
    setTimeout(function(){ 
        $('#tiporenta').change(); 
        //$('#facturacionlibre').change(); 
        $(".chosen-select").attr('disabled', true).trigger("chosen:updated");
    }, 1000);    
}
function obtienedatosdecontrato(idrenta){
    $.ajax({
        type:'POST',
        url: base_url+'index.php/Rentas/traerdatosrenta',
        data: {renta:idrenta},
        success:function(data){
            var array = $.parseJSON(data);
            $('#tiporenta').val(array.tiporenta).trigger("chosen:updated").change();
            if (array.tiporenta==1) {
               array.equipos.forEach(function(element) {
                    $('.rentadeposito_view_'+element.id).val(element.r_costo_m);
                    $('.clicks_mono_view_'+element.id).val(element.r_volumen_m);
                    $('.rentacostocolor_view_'+element.id).val(element.r_costo_c);
                    $('.clicks_color_view_'+element.id).val(element.r_volumen_c);
               });
            }else{
                $('#rentadeposito').val(array.eg_rm);
                $('#clicks_mono').val(array.eg_rvm);
                $('#precio_c_e_mono').val(array.eg_rpem);
                $('#rentacolor').val(array.eg_rc);
                $('#clicks_color').val(array.eg_rvc);
                $('#precio_c_e_color').val(array.eg_rpec);
            }
        }
    });
}
function editarprocontra(idRenta){
    $('#idRentanew').val(idRenta);
    $('#modal_addequipos').modal('open');
    $('.disabledquitar').prop('disabled', false);
    $('.disabledquitar').prop('readonly', false);
    $(".chosen-select").trigger("chosen:updated");
}
function selected_newequipo(){
    var equipo= $('#selected_newequipo').val();
    $.ajax({
        type:'POST',
        url: base_url+'index.php/Rentas/obtenerconsumiblesrefacciones',
        data: {equipo:equipo},
        success:function(data){
            $('.nttbodya').html('');
            $('.nttbodyc').html('');
            console.log(data);
            var array = $.parseJSON(data);
            //accesorios
            //consumibles
            $('#selected_newconsumible').html(array.consumibles);
            $('#selected_newaccesorio').html(array.accesorios);
            $(".chosen-select").trigger("chosen:updated");
        }
    });
}
function temp(){
    if ($('#temp').is(':checked')) {
        $(".divtemp_reg").show( "slow" );
    }else{
        $(".divtemp_reg").hide( "slow" );
    }
}
var btn_acceso=0;
function addaccesorio(){
    var cantidad= $('#selected_newaccesorio_cantidad').val();
    var bodega = $('#selected_newaccesorio_b option:selected').val();
    var bodegatext = $('#selected_newaccesorio_b option:selected').text();
    var acceso = $('#selected_newaccesorio option:selected').val();
    var accesotext = $('#selected_newaccesorio option:selected').text();
    var html ='<tr class="addaccesoriorow addaccesoriorow_'+btn_acceso+'">\
                  <td><input id="cantidad" value="'+cantidad+'" readonly style="display:none">'+cantidad+'</td>\
                  <td><input id="accesorio" value="'+acceso+'" readonly style="display:none">'+accesotext+'</td>\
                  <td><input id="bodega" value="'+bodega+'" readonly style="display:none">'+bodegatext+'</td>\
                  <td><a class="btn-floating waves-effect waves-light red darken-1 btn-delete" onclick="deleteaccesorio('+btn_acceso+')"><i class="material-icons">delete_forever</i></a></td>\
                </tr>';
    $('.nttbodya').append(html);
    btn_acceso++;
}
function deleteaccesorio(row){
    $('.addaccesoriorow_'+row).remove();
}
var btn_consu=0;
function addconsumible(){
    var cantidad= $('#selected_newconsumible_cantidad').val();
    var bodega = $('#selected_newconsumible_b option:selected').val();
    var bodegatext = $('#selected_newconsumible_b option:selected').text();
    var consu = $('#selected_newconsumible option:selected').val();
    var consutext = $('#selected_newconsumible option:selected').text();
    var html ='<tr class="addconsumiblerow addconsumiblerow_'+btn_consu+'">\
                  <td><input id="cantidad" value="'+cantidad+'" readonly style="display:none">'+cantidad+'</td>\
                  <td><input id="consu" value="'+consu+'" readonly style="display:none">'+consutext+'</td>\
                  <td><input id="bodega" value="'+bodega+'" readonly style="display:none">'+bodegatext+'</td>\
                  <td><a class="btn-floating waves-effect waves-light red darken-1 btn-delete" onclick="deleteconsumible('+btn_consu+')"><i class="material-icons">delete_forever</i></a></td>\
                </tr>';
    $('.nttbodyc').append(html);
    btn_consu++;
}
function deleteconsumible(row){
    $('.addconsumiblerow_'+row).remove();
}
function cargar_table_equipos(){
    $.ajax({
        type:'POST',
        url: base_url+'index.php/Rentas/actualizaproductostable',
        data: {id:$('#idRenta').val()},
        success:function(data){
            $('#class_equipos').html(data);
            $('.tooltipped').tooltip({delay: 50});
        }
    });
}
function suspender(id){
    $('#fecha_fin_susp').attr('disabled',false).attr('readonly',false); 
    $.ajax({
        url: base_url+"index.php/Contratos/equiposasuspender/"+id,
        type: 'post',
        success: function (response){
            $('.suspencionseriesparcial').html(response);
            $('#modal_susp').modal('open'); ; 
        },
        error: function(response){
            notificacion(1); 
        }
    });
}
function activar(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de activar el producto?',
        type: 'red',
        typeAnimated: true,
        buttons: {
            confirmar: function (){
                $.ajax({
                    url: base_url+"index.php/Contratos/activarRentaE/"+id,
                    type: 'post',
                    success: function (response){
                        cargar_table_equipos();
                    },
                    error: function(response){
                        notificacion(1); 
                        setTimeout(function(){ 
                            //location.reload();
                        }, 2000);
                    }
                });
            },
            cancelar: function (){
                
            }
        }
    });
}
function eliminar(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de eliminar el producto?',
        type: 'red',
        typeAnimated: true,
        buttons: 
        {   /*
            Aceptar: function () 
            {
                $.ajax({
                    url: base_url+"index.php/Contratos/eliminarRentaE/"+id,
                    type: 'post',
                    success: function (response) 
                    {
                        cargar_table_equipos();
                         setTimeout(function(){ 
                                $('.equipo_'+id).remove();
                            }, 1000);
                    },
                    error: function(response)
                    {
                        $.alert({
                                boxWidth: '30%',
                                useBootstrap: false,
                                title: 'Error!',
                                content: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema'});   
                           
                    }
                });
            },*/
            Aceptar: function () {
                $.ajax({
                    url: base_url+"index.php/Contratos/eliminarRentaEparcial/"+id,
                    type: 'post',
                    success: function (response){
                        $('.deleteseriesparcial').html(response);
                        $('#modal_deleteseries').modal('open'); 
                    },
                    error: function(response){
                        notificacion(1);
                    }
                });
            },
            Cancelar: function (){
                
            }
        }
    });
}
function verificarFecha(){
    $.ajax({
        type:'POST',
        url: base_url+'index.php/Contratos/verificaFechaSusp',
        data: {id:$('#idRen').val()},
        success:function(data){

        }
    });
}
function cambiar(id){ //recibe el id registro de rentas_has_detallesEquipos
    $('#idRentanew').val('');
    $('#idRentaModel').val(id);
    $('#modal_addequipos').modal('open'); 
    $('.disabledquitar').prop('disabled', false);
    $(".chosen-select").trigger("chosen:updated");
    //$("#idRentaModel").val(id);
    /*$.ajax({
        type:'POST',
        url: base_url+"index.php/Contratos/datosRentaD",
        data: {
            idrde:id
        },
        success:function(data){ 
            console.log(data);
            var array = $.parseJSON(data);
            //array.renta_equipos.forEach(function(element) {
            //});
            $("#idRentaModel").val(id);
            $("#selected_newequipo").val(data.idEquipo); $("#selected_newequipo").text(data.modelo);
        }
    });*/
}
function bloquedabtn(){
    setTimeout(function(){ 
        $('.editarprocontra').remove();
        $('.cambiar').remove();
        $('.suspender').remove();
        $('.deletee').remove();
        $('.botonfinalizar').remove();
        $('.registro').remove();
    }, 1000);
}
function eliminarseries(){
    var idRen = $('#idRen').val();
    var eliminacions=0;
    var DATAa  = [];
    var TABLAa   = $("#eliminarseriestable tbody > tr");
            TABLAa.each(function(){    
            if ($(this).find("input[class*='equipo_series_d']").is(':checked')) {
                item = {};
                item ["equipo"]  = $(this).find("input[class*='equipo_series_d']").data('equipod');
                item ["serie"]   = $(this).find("input[class*='equipo_series_d']").data('seried');            
                DATAa.push(item);
                eliminacions++;
            }                     
        });
        INFO  = new FormData();
        arrayseriesdelete   = JSON.stringify(DATAa);
    var datos ='series='+arrayseriesdelete+'&idRen='+idRen;
    if (eliminacions>0) {
        $.ajax({
            type:'POST',
            url: base_url+'index.php/Contratos/eliminarRentaEparcial2',
            data: datos,
            success:function(data){
                TABLAa.each(function(){    
                    if ($(this).find("input[class*='equipo_series_d']").is(':checked')) {
                        
                        var serieremove   = $(this).find("input[class*='equipo_series_d']").data('seried');            
                        $('.serie_'+serieremove).remove();
                    }                     
                });
                cargar_table_equipos();
            }
        });
    }
}
function eliminarseries2(idrow_equipo,idserie){
    var info_modelo=$('.accion_equi_'+idrow_equipo+'_ser_'+idserie).data('equipo');
    var info_serie=$('.accion_equi_'+idrow_equipo+'_ser_'+idserie).data('serie');
    $.confirm({
        boxWidth: '40%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Confirma la Eliminacion del equipo <b>'+info_modelo+'</b> con serie <b>'+info_serie+'</b>?<br><div class="productosinfo"></div><br> Para continuar se necesita permisos de administrador<br>'+
                '<input type="text" placeholder="Contraseña" id="contrasena" name="contrasena" class="name fc-bmz" data-val="" maxlength="10" style="width:99%"/>',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var pass=$("input[name=contrasena]").val();
                if (pass!='') {
                    $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Sistema/solicitarpermiso/",
                        data: {
                            pass:pass
                        },
                        success: function (response){
                                var respuesta = parseInt(response);
                                if (respuesta==1) {
                                    //===================================================
                                        
                                        var idRen = $('#idRen').val();
                                        var DATAa  = [];
                                            
                                            item = {};
                                            item ["equipo"]  = idrow_equipo;
                                            item ["serie"]   = idserie;            
                                            DATAa.push(item);
                                                                     
                                            arrayseriesdelete   = JSON.stringify(DATAa);
                                        var datos ='series='+arrayseriesdelete+'&idRen='+idRen;
                                        $.ajax({
                                            type:'POST',
                                            url: base_url+'index.php/Contratos/eliminarRentaEparcial2',
                                            data: datos,
                                            success:function(data){
                                                toastr["success"]("Equipo Eliminado");
                                                setTimeout(function(){ 
                                                    location.reload();
                                                }, 2000);
                                            }
                                        });
                                    //================================================
                                }else{
                                    notificacion(2);
                                }
                        },
                        error: function(response){
                            notificacion(1);
                        }
                    });
                }else{
                    notificacion(0);
                }
            },
            cancelar: function () {
                
            }
        }
    });
    setTimeout(function(){
            new MaskedPassword(document.getElementById("contrasena"), '\u25CF');
    },1000);
    productosinfo(idrow_equipo);
}
function productosinfo(idrow){
    $.ajax({
        type:'POST',
        url: base_url+'index.php/Contratos/productosinfo',
        data: {
            idrow:idrow
        },
        success:function(data){
            $('.productosinfo').html(data);
        }
    });
}
function suspenderequiposseries(){
    var DATAas  = [];
    var TABLAas   = $("#suspenderseriestable tbody > tr");
        TABLAas.each(function(){    
            item = {};
            item ["equipo"]  = $(this).find("input[class*='equipo_series_d']").data('equipod');
            item ["serie"]   = $(this).find("input[class*='equipo_series_d']").data('seried');
            item ["suspencion"]   = $(this).find("input[class*='equipo_series_d']").is(':checked')==true?1:0;            
            DATAas.push(item);                
        });
    arrayseriessuspencion   = JSON.stringify(DATAas); 
    var datos ='series='+arrayseriessuspencion+'&fecha_fin_susp='+$('#fecha_fin_susp').val(); 
    $.ajax({
         type: "POST",
         url: base_url+'index.php/Contratos/suspenderRentaE',
         data: datos,
         beforeSend: function(){
            $("#saveSE").attr("disabled",true);
         },
         success: function (result) {
            //console.log(result);
            $("#saveSE").attr("disabled",false);
            swal("Exito!", "Se suspendió correctamente", "success");
            $('#modal_susp').modal('close'); // $('.modal-backdrop').hide();
            //cargar_table_equipos();
         }
     });
}
var notificacioninfo=0;
function confirmarsolicitudtoner(idRenta){
    $.confirm({
        boxWidth: '55%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Desea asginar folio al toner?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            Solicitar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+'index.php/Rentas/solicitudentregatoner',
                    data: {
                        idRenta:idRenta,
                        tipo:0
                    },
                    success:function(data){
                        notificacioninfo++;
                        recargar(notificacioninfo);
                        //window.location.href = base_url+'index.php/Rentas'; 
                    }
                });
            },
            'No solicitar': function () {
                notificacioninfo++;
                recargar(notificacioninfo);
                //window.location.href = base_url+'index.php/Rentas'; 
                /*
                $.ajax({
                    type:'POST',
                    url: base_url+'index.php/Rentas/solicitudentregatoner',
                    data: {
                        idRenta:idRenta,
                        tipo:1
                    },
                    success:function(data){
                        window.location.href = base_url+'index.php/Rentas'; 
                    }
                }); 
                */
            }
        }
    });
}
function confirmarsolicitudinstalacion(idRenta){
    $.confirm({
        boxWidth: '55%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Confirma solicitud de instalacion?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            Solicitar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+'index.php/Rentas/solicitudinstalacion',
                    data: {
                        idRenta:idRenta,
                        tipo:0
                    },
                    success:function(data){
                        notificacioninfo++;
                        recargar(notificacioninfo);
                        //window.location.href = base_url+'index.php/Rentas'; 
                    }
                });
            },
            'No solicitar': function () {
                notificacioninfo++;
                recargar(notificacioninfo);
            }
        }
    });
}
function confirmartipodeinstalacion(idRenta){
    var html='';
        html+='Favor de especificar el tipo de Entrega<br>';
    $.confirm({
        boxWidth: '55%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: html,
        type: 'red',
        typeAnimated: true,
        buttons:{
            Total: function (){
                
                confirmarsolicitudinstalacion(idRenta);
            },
            'Parcial': function () {
                obtenerequipos_de_contratos(idRenta);
            }
        }
    });
}
function recargar(not){
    console.log('not '+not);
    if(not>=2){
        window.location.href = base_url+'index.php/Rentas'; 
    }
}
function obtenerequipos_de_contratos(idRenta){
    $.ajax({
        type:'POST',
        url: base_url+'index.php/Rentas/obtenerequipos_de_contratos',
        data: {idRenta:idRenta},
        success:function(data){
            $('.div_table_equipos').html(data);
            $('#modalseleccionequipos').modal({dismissible: false}); 
            $('#modalseleccionequipos').modal('open');
        }
    });
}
function okseleccionequipos(){
    var lis_equipos = $("#tableequipos tbody > tr");
    var num_equipos_selected=0;
    var DATAa  = [];
    lis_equipos.each(function(){  
        if ($(this).find("input[class*='serie_check']").is(':checked')) {
            num_equipos_selected++;
            item = {};                    
            item ["equipoid"]  = $(this).find("input[id*='equipoid']").val();
            item ["serieid"]  = $(this).find("input[id*='serieid']").val();
            //item ["equiporow"]  = $(this).find("input[id*='equiporow']").val();
            
            DATAa.push(item);
        }       
        
    });
    INFOa  = new FormData();
    aInfoa   = JSON.stringify(DATAa);
    console.log(aInfoa);
    if(num_equipos_selected>0){
        $.ajax({
            type:'POST',
            url: base_url+"index.php/Rentas/generacionpreequipos",
            data: {
                equipos:aInfoa,
                idRenta:$('#idRen').val()
            },
            async: false,
            statusCode:{
                404: function(data){
                    swal("Error", "404", "error");
                },
                500: function(){
                    swal("Error", "500", "error"); 
                }
            },
            success:function(response){  
                console.log(response);
                var array = $.parseJSON(response);
                if(array.historialid>0){
                    toastr.success('Equipos seleccionados');
                    
                    notificacioninfo++;
                    recargar(notificacioninfo);

                    $('#modalseleccionequipos').modal('close');
                }
            }
        });
    }else{
        toastr.error('Favor de seleccionar las de un equipo');
    }
}
function addacc2(id,idequipo,modelo){ //addaccesorio2
    $('#equiporow_extra_add').val(id);
    $('#equipo_extra_add').val(idequipo);
    $('#modal_addaccesorios').modal();
    $('#modal_addaccesorios').modal('open');
    $('.addmodalinfomodelo').html(modelo);
    $("#selected_newaccesorio_cantidad_add").prop('readonly', false);
    $("#selected_newaccesorio_add").prop('disabled', false);
    $("#selected_newequipo_b_add").prop('disabled', false);
    $('.nttbodya_add').html('');
    $.ajax({
        type:'POST',
        url: base_url+'index.php/Rentas/obtenerconsumiblesrefacciones',
        data: {equipo:idequipo},
        success:function(data){
            var array = $.parseJSON(data);
            console.log(array.accesorios);
            $('#selected_newaccesorio_add').html(array.accesorios);
        }
    });
}
var btn_acceso=0;
function addaccesorioadd(){
    var equipo= $('#equipo_extra_add').val();
    var equiporow= $('#equiporow_extra_add').val();
    var renta= $('#renta_extra_add').val();
    var cantidad= $('#selected_newaccesorio_cantidad_add').val();
    var bodega = $('#selected_newequipo_b_add option:selected').val();
    var bodegatext = $('#selected_newequipo_b_add option:selected').text();
    var acceso = $('#selected_newaccesorio_add option:selected').val();
    var accesotext = $('#selected_newaccesorio_add option:selected').text();
    var html ='<tr class="addaccesoriorow addaccesoriorow_'+btn_acceso+'">\
                  <td><input id="cantidad" value="'+cantidad+'" readonly style="border-bottom: 0px;width: 50px;"></td>\
                  <td><input id="accesorio" value="'+acceso+'" readonly style="display:none">\
                  <input id="equipo" value="'+equipo+'" readonly style="display:none">\
                  <input id="equiporow" value="'+equiporow+'" readonly style="display:none">\
                  <input id="renta" value="'+renta+'" readonly style="display:none">'+accesotext+'</td>\
                  <td><input id="bodega" value="'+bodega+'" readonly style="display:none">'+bodegatext+'</td>\
                  <td><a class="btn-floating waves-effect waves-light red darken-1" onclick="deleteaccesorioext('+btn_acceso+')"><i class="material-icons">delete_forever</i></a></td>\
                </tr>';
    $('.nttbodya_add').append(html);
    btn_acceso++;
}
function deleteaccesorioext(row){
    $('.addaccesoriorow_'+row).remove();
}
function agregar_ext_accesorio(){
    //===========================
        var DATAa  = [];
        var TABLAa   = $("#new_table_accesorios_add tbody > tr");
                TABLAa.each(function(){         
                item = {};
                item ["piezas"]  = $(this).find("input[id*='cantidad']").val();
                item ["accesorio"]   = $(this).find("input[id*='accesorio']").val();  
                item ["equipo"]   = $(this).find("input[id*='equipo']").val(); 
                item ["equiporow"]   = $(this).find("input[id*='equiporow']").val();            
                item ["bodega"]  = $(this).find("input[id*='bodega']").val();
                item ["renta"]  = $(this).find("input[id*='renta']").val();
                DATAa.push(item);
            });
            INFO  = new FormData();
            arrayaccesorio   = JSON.stringify(DATAa);
    //==========================================================
    var datos ='arrayaccesorios='+arrayaccesorio;
    if (TABLAa.length>0) {
        $.ajax({
            type:'POST',
            url: base_url+'index.php/Rentas/agregarnuevoequipoext',
            data: datos,
            success:function(data){
                var historialid=parseInt(data);
                if(historialid>0){
                    agregarserviciointalacion(historialid,0);
                    agregarfechaentrega(historialid);
                    
                }
                swal({ title: "Éxito",
                    text: "Se registrado correctamente",
                    type: 'success',
                    showCancelButton: false,
                    allowOutsideClick: false,
                });
            }
        });
    }else{
        swal({ title: "Error",
            text: "Seleccione por lo menos un consumible",
            type: 'error',
            showCancelButton: false,
            allowOutsideClick: false,
        });
    }
}
//==========================================================
/*
function savenewequipo(){
    var renta=$('#idRentanew').val();
    var rentamodel=$('#idRentaModel').val();
    var rentaEdita=$('#idRen').val();
    var cant_e = $('#selected_newequipo_cantidad').val();
    var equipo_e = $('#selected_newequipo option:selected').val();
    var bodega_e = $('#selected_newequipo_b option:selected').val();
    if($('#temp').is(':checked')){
        var temp=2;
    }else{
        var temp=0;
    }
    var tempdate=$('#temp_reg').val();
    //===========================
        var DATAc  = [];
        var TABLAc   = $("#new_table_consumible tbody > tr");
                TABLAc.each(function(){         
                item = {};
                item ["piezas"]  = $(this).find("input[id*='cantidad']").val();
                item ["toner"]   = $(this).find("input[id*='consu']").val();            
                item ["bodega"]  = $(this).find("input[id*='bodega']").val();
                DATAc.push(item);
            });
            INFO  = new FormData();
            arrayconsumible   = JSON.stringify(DATAc);
    //==========================================================
    //===========================
        var DATAa  = [];
        var TABLAa   = $("#new_table_accesorios tbody > tr");
                TABLAa.each(function(){         
                item = {};
                item ["piezas"]  = $(this).find("input[id*='cantidad']").val();
                item ["accesorio"]   = $(this).find("input[id*='accesorio']").val();            
                item ["bodega"]  = $(this).find("input[id*='bodega']").val();
                DATAa.push(item);
            });
            INFO  = new FormData();
            arrayaccesorio   = JSON.stringify(DATAa);
    //==========================================================
    var datos ='renta='+renta+'&rentaRenEdita='+rentaEdita+'&rentaedita='+rentamodel+'&cantidad='+cant_e+'&equipo='+equipo_e+'&bodega='+bodega_e+'&temp='+temp+'&fechatemp='+tempdate+'&arrayconsumible='+arrayconsumible+'&arrayaccesorios='+arrayaccesorio;
        $.ajax({
            type:'POST',
            url: base_url+'index.php/Rentas/agregarnuevoequipo',
            data: datos,
            success:function(data){
                $("input").prop('disabled', false);
                $("textarea").prop('disabled', false);
                $("select").prop('disabled', false);
                $("checkbox").prop('disabled', false);
                cargar_table_equipos();
            }
        });
}
*/
var rownewequipo=0;
function addequipocola(){
    var cant_e = $('#selected_newequipo_cantidad').val();
    var equipo_e = $('#selected_newequipo option:selected').val();
    var equipo_e_t = $('#selected_newequipo option:selected').text();
    var bodega_e = $('#selected_newequipo_b option:selected').val();
    var bodega_e_t = $('#selected_newequipo_b option:selected').text();

    var dir_e = $('#domicilioinstalacion_new option:selected').val();
    var dir_e_t = $('#domicilioinstalacion_new option:selected').text();
    //=======================================================
        var htmlcons=$('.nttbodyc').html();
            htmlcons1=htmlcons.replaceAll('<tr','<div');
            htmlcons2=htmlcons1.replaceAll('</tr>','</div>');
            htmlcons3=htmlcons2.replaceAll('<td>','');
            htmlcons=htmlcons3.replaceAll('</td>','');
        var htmlacce=$('.nttbodya').html();
            htmlacce=htmlacce.replaceAll('<tr','<div');
            htmlacce=htmlacce.replaceAll('</tr>','</div>');
            htmlacce=htmlacce.replaceAll('<td>','');
            htmlacce=htmlacce.replaceAll('</td>','');
    //=======================================================
    //=======================================================
    var html='<tr class="rownewequipo_'+rownewequipo+'">\
                <td>'+cant_e+'<input id="cant_e" value="1" style="display:none"></td>\
                <td><input id="equipo_e" value="'+equipo_e+'" style="display:none">'+equipo_e_t+'</td>\
                <td><input id="bodega_e" value="'+bodega_e+'" style="display:none">'+bodega_e_t+'</td>\
                <td><input id="direcc_e" value="'+dir_e+'" style="display:none">'+dir_e_t+'</td>\
                <td>'+htmlcons+'</td>\
                <td>'+htmlacce+'</td>\
                <td><a class="btn-floating waves-effect waves-light red darken-1" onclick="deleteequiponew('+rownewequipo+')"><i class="material-icons">delete_forever</i></a></td>\
              </tr>';
    $('.t_e_s').append(html);
    rownewequipo++;
    $('.nttbodyc').html('');
    $('.nttbodya').html('');
}
function deleteequiponew(row){
    $('.rownewequipo_'+row).remove();
}
function savenewequipo(){
    var renta=$('#idRentanew').val();
    var rentamodel=$('#idRentaModel').val();
    var rentaEdita=$('#idRen').val();
    var DATAe  = [];
    var TABLAe   = $("#tabla_equipos_select tbody > tr");
    TABLAe.each(function(){         
        iteme = {};
        iteme ["equipo"]  = $(this).find("input[id*='equipo_e']").val();
        iteme ["bodega"]   = $(this).find("input[id*='bodega_e']").val();
        iteme ["dic"]   = $(this).find("input[id*='direcc_e']").val();
        //=====================================   
            var DATAc=[];
            var consumible= $(this).find("td > .addconsumiblerow");
            consumible.each(function(){
                itemec = {};
                itemec['cons'] =$(this).find("input[id*='consu']").val();
                itemec['bode_c'] =$(this).find("input[id*='bodega']").val();
                DATAc.push(itemec);
            });  
            iteme ["consumibles"] =DATAc;      
        //===============================
        //=====================================   
            var DATAa=[];
            var accesorio= $(this).find("td > .addaccesoriorow");
            accesorio.each(function(){
                itemea = {};
                itemea['acce'] =$(this).find("input[id*='accesorio']").val();
                itemea['bode_a'] =$(this).find("input[id*='bodega']").val();
                DATAa.push(itemea);
            });  
            iteme ["accesorios"] =DATAa;      
        //===============================
        DATAe.push(iteme);
    });
    console.log(DATAe);
    if(TABLAe.length>0){
        arrayequipos   = JSON.stringify(DATAe);
        var datos='renta='+renta+'&rentaRenEdita='+rentaEdita+'&rentaedita='+rentamodel+'&arrayequipos='+arrayequipos;
        $.ajax({
            type:'POST',
            url: base_url+'index.php/Rentas/agregarnuevoequipo',
            data: datos,
            success:function(data){
                var historialid=parseInt(data);
                if(historialid>0){
                    agregarserviciointalacion(historialid,1);
                    agregarfechaentrega(historialid);
                }
                $("input").prop('disabled', false);
                $("textarea").prop('disabled', false);
                $("select").prop('disabled', false);
                $("checkbox").prop('disabled', false);
                cargar_table_equipos();
                $('#modal_addequipos').modal('close');
            }
        });
    }else{
        swal({ title: "Error",
            text: "Agregue por lo menos un equipo por agregar",
            type: 'error',
            showCancelButton: false,
            allowOutsideClick: false,
        });
    }
}
function reordenar(idrentaw){
    $('#modal_reodenar').modal();
    $('#modal_reodenar').modal('open');
    obtenerdatosrordenarview(idrentaw);
}
function obtenerdatosrordenarview(idrentaw){
    var urlbase=base_url+'index.php/Rentas/datosordenar';
    console.log(urlbase);
    $.ajax({
        type:'POST',
        url: urlbase,
        data: {idRenta:idrentaw},
        success:function(data){
             $('.inforeordenar').html(data);
             $('.collapsible').collapsible();
        }
    });
}
//--------------------------movimientos
function permitirSoltar(ev, n1,modelo,serieid) {
    ev.preventDefault();
    $('#id_equipo_row').val(n1);
    $('#id_equ_mod_row').val(modelo);
    $('#id_equ_serie_row').val(serieid);
}
function soltar(ev, n1) {
    ev.preventDefault();
    var data = ev.dataTransfer.getData(n1);
    //$('#cambiarasi').modal('toggle');
    var tipo=$('#id_tipo_row').val();
    if(tipo==1){
        console.log('funcion de confirmacion de consumible');
        ordenarconsumible();
    }
    if(tipo==2){
        console.log('funcion de confirmacion de refaccion');
        ordenaraccesorio();
    }
}
function arrastrar(ev, idrow,tipo) {
    ev.dataTransfer.setData("Text", ev.target.id);
    $('#id_tipo_row').val(tipo);
    $('#id_consu_acce_row').val(idrow);
}
//------------------------
function ordenaraccesorio(){
    var idrow_a=$('#id_consu_acce_row').val();
    var modelo=$('.accesorio_row_'+idrow_a).data('acce');

    var idrow_e=$('#id_equipo_row').val();
    var equipose=$('.equipo_selected_'+idrow_e).data('equipose');
    var modeloequipo=$('#id_equ_mod_row').val();
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Desea Mover <b>'+modelo+'</b> al equipo <b>'+equipose+'</b>?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Rentas/ordenaraccesorio",
                    data: {
                        idequipo:idrow_e,
                        modeloeq:modeloequipo,
                        idacce:idrow_a
                    },
                    success: function (data){
                        toastr["success"]("Accesorio movido", "Hecho!");
                        var idRen = $('#idRen').val();
                        obtenerdatosrordenarview(idRen);
                    }
                });
            },
            cancelar: function () {
                
            }
        }
    });
}
function ordenarconsumible(){
    var idrow_a=$('#id_consu_acce_row').val();
    var modelo=$('.consumible_row_'+idrow_a).data('consu');
    var idcontrolfolio=$('.consumible_row_'+idrow_a).data('idcontrolfolio');

    var idrow_e=$('#id_equipo_row').val();
    var equipose=$('.equipo_selected_'+idrow_e).data('equipose');
    var modeloequipo=$('#id_equ_mod_row').val();
    var seriee=$('#id_equ_serie_row').val();
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Desea Mover <b>'+modelo+'</b> al equipo <b>'+equipose+'</b>?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Rentas/ordenarconsumible",
                    data: {
                        idequipo:idrow_e,
                        modeloeq:modeloequipo,
                        serieid:seriee,
                        idtoner:idrow_a,
                        idcontrolfolio:idcontrolfolio
                    },
                    success: function (data){
                        toastr["success"]("Accesorio movido", "Hecho!");
                        var idRen = $('#idRen').val();
                        obtenerdatosrordenarview(idRen);
                    }
                });
            },
            cancelar: function () {
                
            }
        }
    });
}
function agregarfechaentrega(idhistorial){
    var fechaentrega = $('#fechaentrega').val();
            $.confirm({
                boxWidth: '40%',
                useBootstrap: false,
                icon: 'fa fa-warning',
                title: 'Atención!',
                content: 'Agregar fecha de entrega <br><input type="date" id="fechaentregarh" class="fc-bmz" style="width:90%">',
                type: 'red',
                typeAnimated: true,
                buttons:{
                    confirmar: function (){
                        var fechaentrega = $('#fechaentregarh').val();
                        //===================================================
                            
                            //console.log(aInfoa);
                            $.ajax({
                                type:'POST',
                                url: base_url+"index.php/Generales/editarfechaentrega",
                                data: {
                                    ventaid:idhistorial,
                                    fentrega:fechaentrega,
                                    tipo:3
                                },
                                success:function(response){  
                                    
                                    swal("Éxito!", "Se ha Modificado", "success");
                                    setTimeout(function(){ 
                                        //location.reload();
                                    }, 1000);
                                }
                            });
                        //================================================
                    },
                    cancelar: function () {
                        
                    }
                }
            });
}
function agregarserviciointalacion(idhistorial,tipo){
    $.confirm({
        boxWidth: '40%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Desea agregar servicio de instalación?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                //===================================================
                    //console.log(aInfoa);
                    $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Generales/agregarserviciointalacion",
                        data: {
                            idhistorial:idhistorial,
                            tipo:tipo
                        },
                        success:function(response){  
                            swal("Éxito!", "Se ha Agregado el servicio", "success");
                        }
                    });
                //================================================
            },
            cancelar: function () { }
        }
    });
}
/*
function retiroequipos(){
    var DATAde  = [];

    var TABLA3 = $(".table_direcciones tbody > tr");
        TABLA3.each(function(){   
            if($(this).find("input[class*='delete_selected_serie']").is(':checked')){
                item3 = {};
                item3 ["equiposrow"] = $(this).find("input[class*='delete_selected_serie']").data('equiporow');
                item3 ["idEquipo"] = $(this).find("input[class*='delete_selected_serie']").data('idequipo');
                item3 ["modelo"] = $(this).find("input[class*='delete_selected_serie']").data('modelo');
                item3 ["serieId"] = $(this).find("input[class*='delete_selected_serie']").data('serieid');
                item3 ["serie"] = $(this).find("input[class*='delete_selected_serie']").data('serie');
                DATAde.push(item3);
            }            
        });
        console.log(DATAde);
        //dataequiposdelete   = JSON.stringify(DATAde);
        //console.log(dataequiposdelete);
        if(DATAde.length>0){
            var html='<table class="table bordered striped"><tr><th>Equipo</th><th>Serie</th><th></th><th></th></tr>';
                    DATAde.forEach(function(element) {
                        var col1=$('.accion_equi_'+element.equiposrow+'_ser_'+element.serieId).find('td:eq(3)').html();
                        var col2=$('.accion_equi_'+element.equiposrow+'_ser_'+element.serieId).find('td:eq(4)').html();
                        var col3=$('.accion_equi_'+element.equiposrow+'_ser_'+element.serieId).find('td:eq(7)').html();
                        var col4='<textarea onchange="actualizarfolioreg('+element.equiposrow+','+element.serieId+')" class="actualizarfolioreg_'+element.equiposrow+'_'+element.serieId+' fc-bmz"></textarea>';
                        html+='<tr>\
                                    <td>'+col1+'</td>\
                                    <td>'+col2+'</td>\
                                    <td>'+col3+'</td>\
                                    <td>'+col4+'</td></tr>';
                   });
                html+='</table>';
            $.confirm({
                boxWidth: '50%',
                useBootstrap: false,
                icon: 'fa fa-warning',
                title: 'Atención!',
                content: '¿Confirma Eliminación y solicitar su retiro de los equipos seleccionados?<br>'+html+'<br><label>Comentario</label><textarea class="fc-bmz" id="comen_retiro"></textarea>',
                type: 'red',
                typeAnimated: true,
                buttons:{
                    confirmar: function (){
                        $( ".btn-default-confirm" ).prop( "disabled", true );
                        setTimeout(function(){ 
                             $(".btn-default-confirm" ).prop( "disabled", false );
                        }, 10000);
                        var comen_retiro= $('#comen_retiro').val();

                        $.ajax({
                            type:'POST',
                            url: base_url+"Contratos/retiroequipos",
                            data: {
                                equipos:DATAde,
                                comentario:comen_retiro,
                                idRen:$('#idRen').val()
                            },
                            success: function (data){
                                console.log(data);
                                toastr["success"]("Se Eliminaron y se realizo la solicitud de retiro");
                            }
                        });
                    },
                    cancelar: function () {
                        
                    }
                }
            });
        }else{
            toastr["error"]("Selecciona uno o mas equipos");
        }
}
*/
function retiroequipos(){
    $('#modalretiroequipos').modal('open');
    retiroequipos_data();
    setTimeout(function(){ 
             $("#comen_retiro" ).prop( "disabled", false );
    }, 1000);
    setTimeout(function(){
            new MaskedPassword(document.getElementById("pass_retiro"), '\u25CF');
    },1000);
}
function retiroequipos_data(){
    var row_select_eq=0;
    var DATAde  = [];

    var TABLA3 = $(".table_direcciones tbody > tr");
        TABLA3.each(function(){   
            if($(this).find("input[class*='delete_selected_serie']").is(':checked')){
                row_select_eq++;
                item3 = {};
                var equiposrow = $(this).find("input[class*='delete_selected_serie']").data('equiporow');
                item3 ["equiposrow"] = equiposrow;
                item3 ["idEquipo"] = $(this).find("input[class*='delete_selected_serie']").data('idequipo');
                item3 ["modelo"] = $(this).find("input[class*='delete_selected_serie']").data('modelo');
                var serieId = $(this).find("input[class*='delete_selected_serie']").data('serieid');
                item3 ["serieId"] = serieId;
                item3 ["serie"] = $(this).find("input[class*='delete_selected_serie']").data('serie');
                item3 ['info'] = $('.accion_equi_'+equiposrow+'_ser_'+serieId).find('td:eq(7)').html();
                item3 ["direccion"] = $(this).find("select[id*='domicilioinstalacion'] option:selected").text();
                item3 ["iddir"] = $(this).find("select[id*='domicilioinstalacion'] option:selected").data('iddir');
                item3 ['cli'] = $('#idCliente').val();
                DATAde.push(item3);
            }            
        });
    if(row_select_eq>0){
        $.ajax({
            type:'POST',
            url: base_url+"Contratos/retiroequipos_info",
            data: {
                equipos:DATAde,
                idRen:$('#idRen').val()
            },
            success: function (data){
                console.log(data);
                $('.doctableretiros').html(data);
            }
        });
    }else{
        toastr.error('Seleccione uno o mas equipos');
    }
}
function retiromodal(){
    var tretiro = $('input[name="t_retiro"]:checked').val();
    if(tretiro>0){
        if(tretiro==2){
            var pass=$("input[name=pass_retiro]").val();
            if (pass!='') {
                $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Sistema/solicitarpermiso/",
                    data: {
                        pass:pass
                    },
                    success: function (response){
                            var respuesta = parseInt(response);
                            if (respuesta==1) {
                                    toastr.success('Contraseña correcta');
                                //===================================================
                                    retiromodal2();
                                //================================================
                            }else{
                                notificacion(2);
                            }
                    },
                    error: function(response){
                        notificacion(1);
                    }
                });
            }else{
                notificacion(0);
            }
        }else{
            retiromodal2();
        }
    }else{
        toastr.error('Favor de seleccionar el tipo de retiro');
    }
}
function retiromodal2(){
    var tretiro = $('input[name="t_retiro"]:checked').val();

    var comen_retiro= $('#comen_retiro').val();
    var DATAde  = [];

    var TABLA3 = $("#table_retiro_info tbody > tr");
        TABLA3.each(function(){   
            if($(this).find("input[class*='filled-in']").is(':checked')){
                item3 = {};
                item3 ["tipo"] = $(this).find("input[id*='s_tipos']").val();
                
                item3 ["equiposrow"] = $(this).find("input[id*='s_equiposrow']").val();
                item3 ["idEquipo"] = $(this).find("input[id*='s_idEquipo']").val();
                item3 ["modelo"] = $(this).find("input[id*='s_modelo']").val();
                item3 ["serieId"] = $(this).find("input[id*='s_serieId']").val();
                item3 ["serie"] = $(this).find("input[id*='s_serie']").val();
                item3 ["direccion"] = $(this).find("input[id*='s_direccion']").val();
                item3 ["iddir"] = $(this).find("input[id*='s_iddir']").val();
                
                item3 ["infotext_m"] = $(this).find("input[id*='s_infor_modeloi']").val();
                item3 ["infotext_s"] = $(this).find("input[id*='s_infor_seriei']").val();
                item3 ["inforow"] = $(this).find("input[id*='s_infor_row']").val();
                item3 ["infomodelo"] = $(this).find("input[id*='s_infor_modelo']").val();
                item3 ["infoserie"] = $(this).find("input[id*='s_infor_serie']").val();
                item3 ["comentario"] = $(this).find("textarea[id*='s_comentario']").val();
                
                DATAde.push(item3);
            }            
        });
        console.log(DATAde);
    if(DATAde.length>0){
        $.ajax({
            type:'POST',
            url: base_url+"Contratos/retiroequipos2",
            data: {
                equipos:DATAde,
                comentario:comen_retiro,
                idRen:$('#idRen').val(),
                tretiro:tretiro
            },
            success: function (data){
                console.log(data);
                $('#modalretiroequipos').modal('close');
                toastr["success"]("Se Eliminaron y se realizo la solicitud de retiro");
                if(tretiro==2){
                    setTimeout(function(){ location.reload(); }, 2000);  
                }
            }
        });
    }else{
        toastr["error"]("Selecciona uno o mas equipos");
    }
}
function moverequipos(){
    var DATAde  = [];

    var TABLA3 = $(".table_direcciones tbody > tr");
        TABLA3.each(function(){   
            if($(this).find("input[class*='delete_selected_serie']").is(':checked')){
                item3 = {};
                item3 ["equiposrow"] = $(this).find("input[class*='delete_selected_serie']").data('equiporow');
                item3 ["idEquipo"] = $(this).find("input[class*='delete_selected_serie']").data('idequipo');
                item3 ["modelo"] = $(this).find("input[class*='delete_selected_serie']").data('modelo');
                item3 ["serieId"] = $(this).find("input[class*='delete_selected_serie']").data('serieid');
                item3 ["serie"] = $(this).find("input[class*='delete_selected_serie']").data('serie');
                DATAde.push(item3);
            }            
        });
        console.log(DATAde);
        //dataequiposdelete   = JSON.stringify(DATAde);
        //console.log(dataequiposdelete);
        if(DATAde.length>0){
            var html='<table class="table bordered striped table_moverequipos"><tr><th>Equipo</th><th>Serie</th><th></th></tr>';
                    DATAde.forEach(function(element) {
                        info_servicios_pendientes(element.serieId,element.serie);
                        var col1=$('.accion_equi_'+element.equiposrow+'_ser_'+element.serieId).find('td:eq(3)').html();
                        var col2=$('.accion_equi_'+element.equiposrow+'_ser_'+element.serieId).find('td:eq(4)').html();
                        var col3=$('.accion_equi_'+element.equiposrow+'_ser_'+element.serieId).find('td:eq(7)').html();
                        html+='<tr><td>'+col1+'<span class="isp_'+element.serieId+'"></span></td><td>'+col2+'</td><td>'+col3+'</td></tr>';
                   });
                html+='</table>';

                var htmlst='<div class="row">';
                        htmlst+='<div class="col s4"><label>Seleccione cliente</label><select class="fc-bmz" id="cliente_mov"></select></div>';
                        htmlst+='<div class="col s4"><label>Seleccione Contrato</label><select class="fc-bmz" id="contrato_mov"></select></div>';
                        htmlst+='<div class="col s4"><label>Seleccione direccion de destino</label><select class="fc-bmz" id="dir_des"></select></div>';
                        htmlst+='<div class="col s12">';
                            htmlst+='<input name="t_traspaso" type="radio" id="t_traspaso_0" value="0" onchange="t_traspaso()" checked><label for="t_traspaso_0"  title="Se traspasara y el equipo del contrato de origen solicitara el ultimo folio para removerlo">Traspaso / solicitud de ultimo folio</label>';
                            htmlst+='<input name="t_traspaso" type="radio" id="t_traspaso_1" value="1" onchange="t_traspaso()" ><label for="t_traspaso_1"   title="Remover directamente el equipo del contrato de origen" >Traspaso / Remover directa</label>';
                        htmlst+='</div>';
                        htmlst+='<div class="col s12 label_pass" style="display:none">';
                        htmlst+='<label>Se necesita contraseña del usuario para confirmar</label>';
                        htmlst+='<input type="text" name="pass_r" id="pass_r" class="fc-bmz">';
                        htmlst+='</div>';
                    htmlst+='</div>';
            $.confirm({
                boxWidth: '50%',
                useBootstrap: false,
                icon: 'fa fa-warning',
                title: 'Atención!',
                content: '¿Confirma el movimiento de los equipos seleccionados?<br>'+html+'<br>'+htmlst+'',
                type: 'red',
                typeAnimated: true,
                buttons:{
                    confirmar: { 
                        text: 'Confirmar',
                        btnClass: 'btn-default btn-default-confirm btn-confirmar-b-ser',
                        action: function (){
                            //==============================
                                var ttras = $('input[name="t_traspaso"]:checked').val();
                                var pass=$("input[name=pass_r]").val();
                                

                                var contrato = $('#contrato_mov option:selected').val();
                                var dir = $('#dir_des option:selected').val();
                                $( ".btn-default-confirm" ).prop( "disabled", true );
                                setTimeout(function(){ 
                                     $(".btn-default-confirm" ).prop( "disabled", false );
                                }, 10000);
                                var comen_retiro= $('#comen_retiro').val();
                                    if(contrato>0){
                                        $.ajax({
                                            type:'POST',
                                            url: base_url+"Contratos/movimientoequipos",
                                            data: {
                                                equipos:DATAde,
                                                contrato:contrato,
                                                idRen:$('#idRen').val(),
                                                ttras:ttras,
                                                pass:pass,
                                                dir:dir
                                            },
                                            statusCode:{
                                                404: function(data){
                                                    toastr["error"]("No se pudo pocesar la solicitud","Error"); 
                                                },
                                                500: function(data){
                                                    toastr["error"]("No se pudo pocesar la solicitud","Error"); 
                                                    console.log(data);
                                                }
                                            },
                                            success: function (data){
                                                var array = $.parseJSON(data);
                                                console.log(data);
                                                if(parseInt(array.servicios)==1){
                                                    toastr["error"]("No se puede realizar el traspaso uno de los equipos tiene un servicio pendiente");
                                                }else{
                                                    toastr.success("Se Realizo el traspaso correctamente"); 
                                                    setTimeout(function(){ location.reload(); }, 2000);   
                                                }
                                                if(array.permiso==0){
                                                    toastr.error('No se tiene permisos y/o contraseña incorrecta');
                                                }
                                                
                                            }
                                        });
                                    }else{
                                        toastr["warning"]("No se a seleccionado contrato donde se moveran los equipos"); 
                                    }
                            //==============================
                        }
                            
                    },
                    cancelar: function () {
                        
                    }
                }
            });
            setTimeout(function(){ cliente_mov(); }, 1000);
            
        }else{
            toastr["error"]("Selecciona uno o mas equipos");
        }
        setTimeout(function(){
            new MaskedPassword(document.getElementById("pass_r"), '\u25CF');
            
        },1000);
}
function desbloquear_eq_pass(){
    $.confirm({
        boxWidth: '40%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Desea desbloquear los equipos con cotizaciones pendientes?<br> Para continuar se necesita permisos de administrador<br>'+
                '<input type="text" placeholder="Contraseña" id="contrasena" name="contrasena" class="name fc-bmz" data-val="" maxlength="10" style="width:99%"/>',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var pass=$("input[name=contrasena]").val();
                if (pass!='') {
                    $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Sistema/solicitarpermiso/",
                        data: {
                            pass:pass
                        },
                        success: function (response){
                                var respuesta = parseInt(response);
                                if (respuesta==1) {
                                        toastr.success('Contraseña correcta');
                                    //===================================================
                                        $('.equipos_retiros').prop('disabled',false);
                                    //================================================
                                }else{
                                    notificacion(2);
                                }
                        },
                        error: function(response){
                            notificacion(1);
                        }
                    });
                }else{
                    notificacion(0);
                }
            },
            cancelar: function () {
                
            }
        }
    });
    setTimeout(function(){
            new MaskedPassword(document.getElementById("contrasena"), '\u25CF');
    },1000);
}
function info_servicios_pendientes(serieid,serie){
    var idcliente = $('#idCliente').val();
    $.ajax({
        type:'POST',
        url: base_url+"Contratos/info_servicios_pendientes/"+serieid+"/"+serie+'/'+idcliente,
        data: {
            cli:0
        },
        success: function (data){
            var array_isp = $.parseJSON(data);
            console.log(array_isp);
            if(array_isp.count>0){
                $('.isp_'+serieid).html(array_isp.labels);
                $('.btn-confirmar-b-ser').prop('disabled',true);
            }
        }
    });
    
}
function t_traspaso(){
    var ttras = $('input[name="t_traspaso"]:checked').val();
    if(ttras==1){
        $('.label_pass').show();
    }else{
        $('.label_pass').hide();
        $("input[name=pass_r]").val('');
        $("#pass_r").val('');
    }
}
function t_retiro(){
    var ttras = $('input[name="t_retiro"]:checked').val();
    if(ttras==2){
        $('.label_pass_r').show();
        $('#pass_retiro').prop('readonly',false);
    }else{
        $('.label_pass_r').hide();
        $("input[name=pass_retiro]").val('');
        $("#pass_retiro").val('');
    }
}
function direccioncliente(idc){
    $('#dir_des').html('');
    $.ajax({
        type:'POST',
        url: base_url+"Generales/pintardireccionesclientes",
        data: {
            clienteid:idc
        },
        success: function (data){
            var array = $.parseJSON(data);
            $.each(array, function(index, item) {
                $(".verificar_"+item.idventa+"_"+item.tipo).addClass( "yellowc" );
                $('#dir_des').append('<option value="g_'+item.idclientedirecc+'">'+item.direccion+'</option>');
            });
            $('#dir_des').val(0);
            $('#dir_des').select2();
        }
    });
}
function cliente_mov(){
    $('#cliente_mov').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una opción',
        allowClear: true,
        ajax: {
            url: base_url+'Ventasi/searchclientes',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.id,
                        text: element.empresa

                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function (e) {
          var data = e.params.data;
          contratoscliente(data.id);
          direccioncliente(data.id);
    });
}
function actualizarfolioreg(rowe,ser){
    $.ajax({
        type:'POST',
        url: base_url+"Configuracionrentas_ext/actualizarfolioreg",
        data: {
            rowe:rowe,
            ser:ser,
            comentario:$('.actualizarfolioreg_'+rowe+'_'+ser).val()
        },
        success: function (data){
            $('#contrato_mov').html(data); 
        }
    });
}
function contratoscliente(id){
    $.ajax({
        type:'POST',
        url: base_url+"NewFolio/contratos",
        data: {
            id:id
        },
        success: function (data){
            $('#contrato_mov').html(data); 
        }
    });
}
function solicitudinfo(){
    $('#solicitud_info').attr('disabled',false);
    $('#modalasignar').modal();
    $('#modalasignar').modal('open');
}
function solicitudinfo_reg(){
    $('#modalasignar').modal();
    $('#modalasignar').modal('close');

    var solicitud_info = $('#solicitud_info').val();
    var DATAde  = [];

    var TABLA3 = $(".table_direcciones tbody > tr");
        TABLA3.each(function(){   
            if($(this).find("input[class*='delete_selected_serie']").is(':checked')){
                item3 = {};
                item3 ["equiposrow"] = $(this).find("input[class*='delete_selected_serie']").data('equiporow');
                item3 ["idEquipo"] = $(this).find("input[class*='delete_selected_serie']").data('idequipo');
                item3 ["modelo"] = $(this).find("input[class*='delete_selected_serie']").data('modelo');
                item3 ["serieId"] = $(this).find("input[class*='delete_selected_serie']").data('serieid');
                item3 ["serie"] = $(this).find("input[class*='delete_selected_serie']").data('serie');
                DATAde.push(item3);
            }            
        });
        if(DATAde.length>0){
            $.ajax({
                type:'POST',
                url: base_url+"Contratos/movimientoequipos_contadores",
                data: {
                    equipos:DATAde,
                    contador:solicitud_info,
                },
                statusCode:{
                    404: function(data){
                        toastr["error"]("No se pudo pocesar la solicitud","Error"); 
                    },
                    500: function(data){
                        toastr["error"]("No se pudo pocesar la solicitud","Error"); 
                        console.log(data);
                    }
                },
                success: function (data){
                    console.log(data);
                    toastr["success"]("Se guarda correctamente"); 
                    setTimeout(function(){ location.reload(); }, 2000);     
                }
            });
        }else{
            toastr["error"]("Selecciona uno o mas equipos");
        }
}
function addcondiciones(idRenta){
    $('#modal_condiciones').modal('open');
    $('.input_select_con').attr('disabled',false).attr('readonly',false);
    tablelistcon(idRenta);
    $('.div_individual .inc').val(0);
    $('.inc_v').val('');
}
function con_tipochange(){
    var con_tipo = $('#con_tipo option:selected').val();
    if(con_tipo==1){

        $('.div_individual').show('show');
        $('.div_global').hide('show');
    }
    if(con_tipo==2){
        $('.div_global').show('show');
        $('.div_individual').hide('show');
        $('.div_individual .inc').val(0);
    }
}
function savecondiciones(){
    var DATA  = [];

        var TABLA = $("#table_individual tbody > tr");
        TABLA.each(function(){         
            item = {};
            item['id'] = $(this).find("input[id*='con_id']").val();
            item['equiporow'] = $(this).find("input[id*='con_equiporow']").val();
            item['serieId'] = $(this).find("input[id*='con_serieId']").val();
            item['renta_m'] = $(this).find("input[id*='cond_renta_m']").val();
            item['click_m'] = $(this).find("input[id*='cond_click_m']").val();
            item['excedente_m'] = $(this).find("input[id*='cond_excedente_m']").val();
            item['renta_c'] = $(this).find("input[id*='cond_renta_c']").val();
            item['click_c'] = $(this).find("input[id*='cond_click_c']").val();
            item['excedente_c'] = $(this).find("input[id*='cond_excedente_c']").val(); 
            if($(this).find("input[id*='con_id']").val()!=undefined){
                DATA.push(item);    
            }       
            
        });
    dataequiposclicks   = JSON.stringify(DATA);
    var form_condg = $('#form_condg');
    var datos = form_condg.serialize()+'&equipos='+dataequiposclicks;
    $.ajax({
        type:'POST',
        url: base_url+'index.php/Rentas/savecondiciones',
        data: datos,
        success:function(data){
            toastr["success"]("Condiciones guardadas");
            var idRen = $('#idRen').val();
            tablelistcon(idRen);
        },
        error: function(response){
            toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema");                             
        }
    });
}
function tablelistcon(idRenta){
    $.ajax({
        type:'POST',
        url: base_url+'index.php/Rentas/listcondiciones',
        data: {
            idRenta:idRenta
        },
        success:function(data){
            $('.tablelist_con').html(data);
        },
        error: function(response){
            toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema");                             
        }
    });
}
function viewcondi(id){
    $('.view_detalles_extras').click();
    $.ajax({
        type:'POST',
        url: base_url+'index.php/Rentas/viewcondi',
        data: {
            id:id
        },
        success:function(data){
            var array = $.parseJSON(data);
            console.log(array);
            $('#con_id').val(array.datos.id);
            $('#con_tipo').val(array.datos.tipo).change();
            $('#con_fechainicial').val(array.datos.fechainicial);
            $('#con_renta_m').val(array.datos.renta_m);
            $('#con_click_m').val(array.datos.click_m);
            $('#con_excedente_m').val(array.datos.excedente_m);
            $('#con_renta_c').val(array.datos.renta_c);
            $('#con_click_c').val(array.datos.click_c);
            $('#con_excedente_c').val(array.datos.excedente_c);
            $('#comentario').val(array.datos.comentario);
            array.datosdll.forEach(function(element) {
                console.log(element);
                $('.con_id_'+element.equiporow+'_'+element.serieId).val(element.id)

                $('.cond_renta_m_con_idd_'+element.equiporow+'_'+element.serieId).val(element.renta_m);
                $('.cond_click_m_con_idd_'+element.equiporow+'_'+element.serieId).val(element.click_m);
                $('.cond_excedente_m_con_idd_'+element.equiporow+'_'+element.serieId).val(element.excedente_m);
                $('.cond_renta_c_con_idd_'+element.equiporow+'_'+element.serieId).val(element.renta_c);
                $('.cond_click_c_con_idd_'+element.equiporow+'_'+element.serieId).val(element.click_c);
                $('.cond_excedente_c_con_idd_'+element.equiporow+'_'+element.serieId).val(element.excedente_c);
            });
        },
        error: function(response){
            notificacion(1);                            
        }
    });
}
function desvincular(idrow,tipo){
    $.confirm({
        boxWidth: '50%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Confirma el desvincular?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                
                $.ajax({
                    type:'POST',
                    url: base_url+"Contratos/desvincularat",
                    data: {
                        idrow:idrow,
                        tipo:tipo
                    },
                    statusCode:{
                        404: function(data){
                            toastr["error"]("No se pudo pocesar la solicitud","Error"); 
                        },
                        500: function(data){
                            toastr["error"]("No se pudo pocesar la solicitud","Error"); 
                            console.log(data);
                        }
                    },
                    success: function (data){
                        $('.desvincular_'+tipo+'_'+idrow).remove();
                    }
                });

            },
            cancelar: function () {
                
            }
        }
    });
}
function obtenerultimacondicion(){
    $.ajax({
        type:'POST',
        url: base_url+'index.php/Configuracionrentas_ext/vercondicionactual',
        data: {
            id:$('#idRen').val()
        },
        success:function(data){
            var array = $.parseJSON(data);
            console.log(array);
            if(array.idcondicionextra>0){
                $('#tiporenta').prop('disabled', false);
                $('#tiporenta').val(array.altarentastipo2).prop('disabled', false).change();
                $('#rentadeposito').val(array.contratorentadeposito);
                $('#clicks_mono').val(array.altarentasclicks_mono);
                $('#precio_c_e_mono').val(array.altarentasprecio_c_e_mono);
                $('#rentacolor').val(array.altarentasmontoc);
                $('#clicks_color').val(array.altarentasclicks_color);
                $('#precio_c_e_color').val(array.contratoprecio_c_e_color);
                
                array.equipos.forEach(function(element) {
                    console.log(element);

                    $('.rentadeposito_row_'+element.equiporow+'_'+element.serieId).val(element.renta_m);
                    $('.clicks_mono_row_'+element.equiporow+'_'+element.serieId).val(element.click_m);
                    $('.precio_c_e_mono_row_'+element.equiporow+'_'+element.serieId).val(element.excedente_m);
                    $('.rentacostocolor_row_'+element.equiporow+'_'+element.serieId).val(element.renta_c);
                    $('.clicks_color_row_'+element.equiporow+'_'+element.serieId).val(element.click_c);
                    $('.precio_c_e_color_row_'+element.equiporow+'_'+element.serieId).val(element.excedente_c);
                });
            }
        },
        error: function(response){
            notificacion(1);                            
        }
    });
}
function up_ubi(id){ //updateubicacion(id)
    $.ajax({
        type:'POST',
        url: base_url+"Contratos/updateubicacion",
        data: {
            ubicacion:$('.ubicacion_update_'+id).val(),
            orden:$('.ordene_update_'+id).val(),
            comen:$('.comenext_update_'+id).val(),
            id:id
        },
        statusCode:{
            404: function(data){
                toastr["error"]("No se pudo pocesar la solicitud","Error"); 
            },
            500: function(data){
                toastr["error"]("No se pudo pocesar la solicitud","Error"); 
                console.log(data);
            }
        },
        success: function (data){
            toastr["success"]("Información Actualizada"); 
            
        }
    });
}

function confirmfinal(){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Exito!',
        content: 'Se finalizo correctamente<br>¿Desea realizar una renovación?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            'Generar renovación': function (){
                var idRen=$('#idRen').val();
                window.location.href = base_url+"index.php/Contratos/renovacion/"+idRen;              
            },
            'Finalizar': function () {
                window.location.href = base_url+'index.php/Contratos';
            }
        }
    });
}
function susp_eq_ser(idrow_equipo,idserie,id_asig){ //suspenderequiposseries2
    var info_modelo=$('.accion_equi_'+idrow_equipo+'_ser_'+idserie).data('equipo');
    var info_serie=$('.accion_equi_'+idrow_equipo+'_ser_'+idserie).data('serie');
    var info_susfecha=$('.accion_equi_'+idrow_equipo+'_ser_'+idserie).data('suspendidofecha');
    var info_sus=$('.accion_equi_'+idrow_equipo+'_ser_'+idserie).data('suspendido');
    var info_cont=$('.accion_equi_'+idrow_equipo+'_ser_'+idserie).data('contadorinfo');

    if(info_sus==1){
        var info_sus_name='Reactivacion';
        var sus=0;
    }else{
        var info_sus_name='Suspensión';
        var sus=1;
    }
    $.confirm({
        boxWidth: '40%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Confirma la '+info_sus_name+' del equipo <b>'+info_modelo+'</b> con serie <b>'+info_serie+'</b>?\
                    <br><div class="form-group"><label class="label-control">Agregar Contador: </label><input type="number" class="fc-bmz" id="contadorrow" value="'+info_cont+'" onchange="updatecontadorsre('+id_asig+')" style="width: 90%;"></div>\
                    <br><div class="form-group"><label class="label-control">Fecha termino de suspensión: </label><input type="date" class="fc-bmz" name="fecha_fin_susp2" id="fecha_fin_susp2" value="'+info_susfecha+'" style="width: 90%;"></div>',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var contador=$('#contadorrow').val();
                //===================================================
                    
                    var DATAas  = [];
                        item = {};
                        item ["equipo"]  = idrow_equipo;
                        item ["serie"]   = idserie;
                        item ["suspencion"]   = sus;  
                        item ['contador']=contador;
                        DATAas.push(item);
                                                
                                            arrayseriessuspencion   = JSON.stringify(DATAas); 
                    var datos ='series='+arrayseriessuspencion+'&fecha_fin_susp='+$('#fecha_fin_susp2').val(); 
                    $.ajax({
                         type: "POST",
                         url: base_url+'index.php/Contratos/suspenderRentaE',
                         data: datos,
                         beforeSend: function(){
                            $("#saveSE").attr("disabled",true);
                         },
                         success: function (result) {
                            //console.log(result);
                            $("#saveSE").attr("disabled",false);
                            swal("Exito!", "Se suspendió correctamente", "success");
                            location.reload();
                            //cargar_table_equipos();
                         }
                     });
                //================================================
            },
            cancelar: function () {
                
            }
        }
    });
    
}
function updatecontadorsre(id){
    $.ajax({
     type: "POST",
     url: base_url+'index.php/Contratos/updatecontadorsre',
     data: {
        id:id,
        cont:$('#contadorrow').val()
     },
     beforeSend: function(){
        //$("#saveSE").attr("disabled",true);
     },
     success: function (result) {
        
     }
 });
}

function suspencionall(){
    var DATAde  = [];

    var TABLA3 = $(".table_direcciones tbody > tr");
        TABLA3.each(function(){   
            if($(this).find("input[class*='delete_selected_serie']").is(':checked')){
                item3 = {};
                item3 ["equiposrow"] = $(this).find("input[class*='delete_selected_serie']").data('equiporow');
                item3 ["idEquipo"] = $(this).find("input[class*='delete_selected_serie']").data('idequipo');
                item3 ["modelo"] = $(this).find("input[class*='delete_selected_serie']").data('modelo');
                item3 ["serieId"] = $(this).find("input[class*='delete_selected_serie']").data('serieid');
                item3 ["serie"] = $(this).find("input[class*='delete_selected_serie']").data('serie');
                DATAde.push(item3);
            }            
        });
        console.log(DATAde);
        //dataequiposdelete   = JSON.stringify(DATAde);
        //console.log(dataequiposdelete);
        if(DATAde.length>0){
            var html='<table class="table bordered striped"><tr><th>Equipo</th><th>Serie</th><th></th></tr>';
                    DATAde.forEach(function(element) {
                        var col1=$('.accion_equi_'+element.equiposrow+'_ser_'+element.serieId).find('td:eq(3)').html();
                        var col2=$('.accion_equi_'+element.equiposrow+'_ser_'+element.serieId).find('td:eq(4)').html();
                        var col3=$('.accion_equi_'+element.equiposrow+'_ser_'+element.serieId).find('td:eq(7)').html();
                        html+='<tr><td>'+col1+'</td><td>'+col2+'</td><td>'+col3+'</td></tr>';
                   });
                html+='</table>';
            $.confirm({
                boxWidth: '50%',
                useBootstrap: false,
                icon: 'fa fa-warning',
                title: 'Atención!',
                content: '¿Confirma la suspensión temporal de los equipos seleccionados?<br>Al realizarse se generara una póliza de retiro<br>'+html+'<textarea class="fc-bmz" id="comentariof" placeholder="comentario"></textarea>',
                type: 'red',
                typeAnimated: true,
                buttons:{
                    confirmar: function (){
                        var comen_retiro= $('#comentariof').val();
                        var contrato = $('#idcontrato').val();
                        $( ".btn-default-confirm" ).prop( "disabled", true );
                        setTimeout(function(){ 
                             $(".btn-default-confirm" ).prop( "disabled", false );
                        }, 10000);
                        //var comen_retiro= $('#comen_retiro').val();
                        if(contrato>0){
                            $.ajax({
                                type:'POST',
                                url: base_url+"Contratos/suspencionequiposall",
                                data: {
                                    equipos:DATAde,
                                    contrato:contrato,
                                    idRen:$('#idRen').val(),
                                    come:comen_retiro
                                },
                                statusCode:{
                                    404: function(data){
                                        toastr["error"]("No se pudo procesar la solicitud","Error"); 
                                    },
                                    500: function(data){
                                        toastr["error"]("No se pudo procesar la solicitud","Error"); 
                                        console.log(data);
                                    }
                                },
                                success: function (data){
                                    console.log(data);
                                    if(parseInt(data)==1){
                                        toastr["error"]("No se puede realizar el traspaso uno de los equipos tiene un servicio pendiente");
                                    }else{
                                        toastr["success"]("Se Realizo la solicitud correctamente"); 
                                        //setTimeout(function(){ location.reload(); }, 2000);
                                           
                                    }
                                    
                                }
                            });
                        }else{
                            toastr["warning"]("No se a seleccionado contrato donde se moverán los equipos"); 
                        }
                        
                    },
                    cancelar: function () {
                        
                    }
                }
            });
            setTimeout(function(){ cliente_mov(); }, 1000);
            
        }else{
            toastr["error"]("Selecciona uno o mas equipos");
        }
}
function hojasestado(idrow){
    id_asig=idrow;
    $('#modalhojasestado').modal('open');

    var doc=$('.hojasestado_'+idrow).data('hojasestado');
    $('.docfilehojasestado').html('');
    if(doc!=null && doc!='null'){
        $('.docfilehojasestado').html('<iframe src="'+base_url+'uploads/hojasestado/'+doc+'" allowfullscreen></iframe>');
    }
}
function obtenerventaspendientes(){
    var idcliente = $('#idCliente').val();
    //restriccion para cuando tenga mas de 3 prefacturas pendientes de agregar pago
    if(idcliente>0){
        if(idcliente!=597){
            $.ajax({
                type:'POST',
                url: base_url+'index.php/Reportes/rventasnum/'+idcliente,
                success:function(data){
                    console.log(data);
                    var numventas=parseInt(data);
                    if(numventas>0){
                        /*
                        $.confirm({
                            boxWidth: '30%',
                            useBootstrap: false,
                            icon: 'fa fa-warning',
                            title: 'Atención!',
                            content: 'No se tiene permitido tiene '+numventas+' pendientes de pago!<br> Para continuar se necesita permisos de administrador<br>'+
                                     '<input type="text" placeholder="Contraseña" id="contrasena" class="name fc-bmz" data-val="" maxlength="10"/>',
                            type: 'red',
                            typeAnimated: true,
                            buttons:{
                                confirmar: function (){
                                    //var pass=$('#contrasena').val();
                                    var pass=$('#contrasena').data('val')
                                    if (pass!='') {
                                         $.ajax({
                                            type:'POST',
                                            url: base_url+"index.php/Sistema/solicitarpermiso",
                                            data: {
                                                pass:pass
                                            },
                                            success: function (response){
                                                    var respuesta = parseInt(response);
                                                    if (respuesta==1) {
                                                    }else{
                                                        notificacion(2);
                                                        setTimeout(function(){
                                                            location.reload();
                                                        },1000);
                                                    }
                                            },
                                            error: function(response){
                                                notificacion(1);
                                                setTimeout(function(){
                                                            location.reload();
                                                },2000);
                                            }
                                        });
                                        
                                    }else{
                                        notificacion(0);
                                        setTimeout(function(){
                                            window.location.href = base_url+"index.php/Clientes";
                                        },2000);
                                    }
                                },
                                cancelar: function (){
                                    setTimeout(function(){
                                        window.location.href = base_url+"index.php/Clientes";
                                    },1000);
                                }
                            }
                        });
                        */
                        
                    }
                }
            });
        }
        /*
        setTimeout(function(){
            $("#contrasena").passwordify({
                maxLength: 10,
                onlyNumbers: true
              }).focus();
        },1000);
        */
    }
}
/*
function instalacionequipos(){
    var DATAde  = [];

    var TABLA3 = $(".table_direcciones tbody > tr");
        TABLA3.each(function(){   
            if($(this).find("input[class*='delete_selected_serie']").is(':checked')){
                item3 = {};
                item3 ["equiposrow"] = $(this).find("input[class*='delete_selected_serie']").data('equiporow');
                item3 ["idEquipo"] = $(this).find("input[class*='delete_selected_serie']").data('idequipo');
                item3 ["modelo"] = $(this).find("input[class*='delete_selected_serie']").data('modelo');
                item3 ["serieId"] = $(this).find("input[class*='delete_selected_serie']").data('serieid');
                item3 ["serie"] = $(this).find("input[class*='delete_selected_serie']").data('serie');
                DATAde.push(item3);
            }            
        });
        console.log(DATAde);
        //dataequiposdelete   = JSON.stringify(DATAde);
        //console.log(dataequiposdelete);
        if(DATAde.length>0){
            var html='<table class="table bordered striped"><tr><th>Equipo</th><th>Serie</th><th></th></tr>';
                    DATAde.forEach(function(element) {
                        var col1=$('.accion_equi_'+element.equiposrow+'_ser_'+element.serieId).find('td:eq(3)').html();
                        var col2=$('.accion_equi_'+element.equiposrow+'_ser_'+element.serieId).find('td:eq(4)').html();
                        var col3=$('.accion_equi_'+element.equiposrow+'_ser_'+element.serieId).find('td:eq(6)').html();
                        html+='<tr><td>'+col1+'</td><td>'+col2+'</td><td>'+col3+'</td></tr>';
                   });
                html+='</table>';
            $.confirm({
                boxWidth: '50%',
                useBootstrap: false,
                icon: 'fa fa-warning',
                title: 'Atención!',
                content: '¿Confirma la solicitud de instalacion para equipos seleccionados?<br>'+html+'<textarea class="fc-bmz" id="comentariof" placeholder="comentario"></textarea>',
                type: 'red',
                typeAnimated: true,
                buttons:{
                    confirmar: function (){
                        var contrato = $('#idcontrato').val();
                        $( ".btn-default-confirm" ).prop( "disabled", true );
                        setTimeout(function(){ 
                             $(".btn-default-confirm" ).prop( "disabled", false );
                        }, 10000);
                        var comen_retiro= $('#comentariof').val();
                        if(contrato>0){
                            $.ajax({
                                type:'POST',
                                url: base_url+"Contratos/instalacionequipos",
                                data: {
                                    equipos:DATAde,
                                    contrato:contrato,
                                    idRen:$('#idRen').val(),
                                    come:comen_retiro
                                },
                                statusCode:{
                                    404: function(data){
                                        toastr["error"]("No se pudo pocesar la solicitud","Error"); 
                                    },
                                    500: function(data){
                                        toastr["error"]("No se pudo pocesar la solicitud","Error"); 
                                        console.log(data);
                                    }
                                },
                                success: function (data){
                                    console.log(data);
                                    if(parseInt(data)==1){
                                        toastr["error"]("No se puede realizar el traspaso uno de los equipos tiene un servicio pendiente");
                                    }else{
                                        toastr["success"]("Se Realizo la solicitud correctamente"); 
                                        //setTimeout(function(){ location.reload(); }, 2000);
                                           
                                    }
                                    
                                }
                            });
                        }else{
                            toastr["warning"]("No se a seleccionado contrato donde se moveran los equipos"); 
                        }
                        
                    },
                    cancelar: function () {
                        
                    }
                }
            });
            setTimeout(function(){ cliente_mov(); }, 1000);
            
        }else{
            toastr["error"]("Selecciona uno o mas equipos");
        }
}
*/
function instalacionequipos(){
    var DATAde  = [];

    var TABLA3 = $(".table_direcciones tbody > tr");
        TABLA3.each(function(){   
            if($(this).find("input[class*='delete_selected_serie']").is(':checked')){
                item3 = {};
                item3 ["equiposrow"] = $(this).find("input[class*='delete_selected_serie']").data('equiporow');
                item3 ["idEquipo"] = $(this).find("input[class*='delete_selected_serie']").data('idequipo');
                item3 ["modelo"] = $(this).find("input[class*='delete_selected_serie']").data('modelo');
                item3 ["serieId"] = $(this).find("input[class*='delete_selected_serie']").data('serieid');
                item3 ["serie"] = $(this).find("input[class*='delete_selected_serie']").data('serie');
                DATAde.push(item3);
            }            
        });
        console.log(DATAde);
        //dataequiposdelete   = JSON.stringify(DATAde);
        //console.log(dataequiposdelete);
        if(DATAde.length>0){
            var html='';
            DATAde.forEach(function(element) {
                var col1=$('.accion_equi_'+element.equiposrow+'_ser_'+element.serieId).find('td:eq(3)').html();
                var col2=$('.accion_equi_'+element.equiposrow+'_ser_'+element.serieId).find('td:eq(4)').html();
                var col3=$('.accion_equi_'+element.equiposrow+'_ser_'+element.serieId).find('td:eq(7)').html();
                html+='<tr><td>';
                    html+='<input type="hidden" id="equiposrow" value="'+element.equiposrow+'">';
                    html+='<input type="hidden" id="idEquipo" value="'+element.idEquipo+'">';
                    html+='<input type="hidden" id="modelo" value="'+element.modelo+'">';
                    html+='<input type="hidden" id="serieId" value="'+element.serieId+'">';
                    html+='<input type="hidden" id="serie" value="'+element.serie+'">';
                    var check_html='<input type="checkbox" class="filled-in select_s_inta_" id="select_s_inta_'+element.serieId+'"/>\
                      <label for="select_s_inta_'+element.serieId+'" class="floatbtn">.</label>';
                html+=col1+'</td><td>'+col2+'</td><td>'+col3+'</td><td>'+check_html+'</td><td><input id="comentindi" class="comentindi fc-bmz"></td></tr>';
           });
               
            $('.table_equipos_insta').html(html);
            $('#modalsolicitudinstalacion').modal('open');
            $("#comentariof" ).prop( "disabled", false );
            $(".comentindi" ).prop( "disabled", false );
        }else{
            toastr["error"]("Selecciona uno o mas equipos");
        }
}
function oksolinsta(){
    var DATAde  = [];

    var TABLA3 = $("#t_eq_sol_insta tbody > tr");
        TABLA3.each(function(){   
            item3 = {};
            item3 ["equiposrow"] = $(this).find("input[id*='equiposrow']").val();
            item3 ["idEquipo"] = $(this).find("input[id*='idEquipo']").val();
            item3 ["modelo"] = $(this).find("input[id*='modelo']").val();
            item3 ["serieId"] = $(this).find("input[id*='serieId']").val();
            item3 ["serie"] = $(this).find("input[id*='serie']").val();
            item3 ["kfs"] = $(this).find("input[class*='select_s_inta_']").is(':checked')==true?1:0;
            item3 ["comen"] = $(this).find("input[id*='comentindi']").val();
            DATAde.push(item3);
                       
        });
    var contrato = $('#idcontrato').val();
        $( ".btn-default-confirm" ).prop( "disabled", true );
        setTimeout(function(){ 
             $(".btn-default-confirm" ).prop( "disabled", false );
        }, 10000);
        var comen_retiro= $('#comentariof').val();
        if(contrato>0){
            if(TABLA3.length>0){
                console.log(DATAde);
                $.ajax({
                    type:'POST',
                    url: base_url+"Contratos/instalacionequipos",
                    data: {
                        equipos:DATAde,
                        contrato:contrato,
                        idRen:$('#idRen').val(),
                        come:comen_retiro
                    },
                    statusCode:{
                        404: function(data){
                            toastr["error"]("No se pudo pocesar la solicitud","Error"); 
                        },
                        500: function(data){
                            toastr["error"]("No se pudo pocesar la solicitud","Error"); 
                            console.log(data);
                        }
                    },
                    success: function (data){
                        console.log(data);
                        if(parseInt(data)==1){
                            toastr["error"]("No se puede realizar el traspaso uno de los equipos tiene un servicio pendiente");
                        }else{
                            toastr["success"]("Se Realizo la solicitud correctamente"); 
                            //setTimeout(function(){ location.reload(); }, 2000);
                               
                        }
                        
                    }
                });
            }
        }else{
            toastr["warning"]("aun no pasa a ser contrato"); 
        }

}
function files_oc(){
    upload_data_view_oc(idcontrato_aux);
    setTimeout(function(){
        $("#files").fileinput({
            showCaption: true,
            showUpload: true,// quita el boton de upload
            //rtl: true,
            allowedFileExtensions: ["pdf"],
            browseLabel: 'Seleccionar documentos',
            uploadUrl: base_url+'Contratos/up_doc_multiple',
            maxFilePreviewSize: 5000,
            previewFileIconSettings: {
                'docx': '<i class="fa fa-file-word-o text-primary"></i>',
                'xlsx': '<i class="fa fa-file-excel-o text-success"></i>',
                'pptx': '<i class="fa fa-file-powerpoint-o text-danger"></i>',
                'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
                'pdf': '<i class="fa fa-file-pdf-o text-danger"></i>',
                'zip': '<i class="fa fa-file-archive-o text-muted"></i>',
                'cer': '<i class="fas fa-file-invoice"></i>',
            },
            uploadExtraData: function (previewId, index) {
            var info = {
                        idcontrato:idcontrato_aux
                    };
            return info;
          }
        }).on('filebatchuploadcomplete', function(event, files, extra) {
          //location.reload();
            upload_data_view_oc(idcontrato_aux);
        }).on('filebatchuploadsuccess', function(event, files, extra) {
          //location.reload();
          //toastr.success('Se cargo el Certificado Correctamente','Hecho!');
            upload_data_view_oc(idcontrato_aux);
            $('#files').fileinput('clear');
        });
    },2000);
}
function upload_data_view_oc(idcon){
    $.ajax({
        type:'POST',
        url: base_url+"Contratos/upload_data_view_oc",
        data: {
            idcon:idcon
        },
        statusCode:{
            404: function(data){
                toastr["error"]("No se pudo pocesar la solicitud","Error"); 
            },
            500: function(data){
                toastr["error"]("No se pudo pocesar la solicitud","Error"); 
                console.log(data);
            }
        },
        success: function (data){
            $('.files_orden').html(data);
            
        }
    });
}    
function deleteocfile(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Confirma la eliminacion del archivo?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+'index.php/Contratos/deleteocfile',
                    data: {
                        id:id
                    },
                    success:function(data){
                        toastr.success('Archivo eliminado');
                        upload_data_view_oc(idcontrato_aux);
                    }
                });
                
            },
            cancelar: function () {
                
            }
        }
    });
}
function updatecondicionfac(idcon){
    if(idcon>0){
        $.ajax({
            type:'POST',
            url: base_url+'index.php/Contratos/updatecondicionfac',
            data: {
                idcon:idcon,
                info_factura:$('#info_factura').val()
            },
            success:function(data){
                toastr.success('Actualizado');
            }
        });
    }
}
function buscar_serie_con(){
    var serie=$('#search_serie').val(); 
    $('.color_serie').css('background','#ff000000');
    if(serie!=''){
        $.ajax({
            type:'POST',
            url: base_url+"index.php/Configuracionrentas/get_serie_color",
            data: {
                serie:$('#search_serie').val()
            },
            success:function(data){  
                var array = $.parseJSON(data);
                console.log(array);
                array.forEach(function(element) {
                    console.log(element.serieId);
                    $('.serie_'+element.serieId).css('background','#00bcd4');
                    if($(".serie_"+element.serieId).length>0){
                        var posicion=$("#select_serie_"+element.serieId).offset().top;
                        //console.log(posicion);
                        posicion=parseFloat(posicion)-parseFloat(100);
                        //console.log(posicion);
                        $('html, body, table').animate({
                            scrollTop: posicion
                        }, 2000);   
                    }
                });
            }
        });
    }
}
function delete_accesorio(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Exito!',
        content: '¿Confirma la eliminacion del accesorio?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            'Confirmar': function (){
                setTimeout(function(){ 
                            //location.reload();
                        }, 2000);
                 $.ajax({
                    type:'POST',
                    url: base_url+'index.php/Generales/accesoriosdevolucion/'+id+'/0/1',
                    data: {
                        idrow:0
                    },
                    statusCode:{
                        404: function(data){
                            toastr["error"]("No se pudo pocesar la solicitud","Error"); 
                        },
                        500: function(data){
                            toastr["error"]("No se pudo pocesar la solicitud","Error"); 
                            console.log(data);
                        }
                    },
                    success: function (data){
                        toastr.success('se elimino correctamente');
                        setTimeout(function(){ 
                            location.reload();
                        }, 2000);
                    }
                });         
            },
            'Cancelar': function () {
            }
        }
    });
}
function cambiarestatuspe(ideq,idserie){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Exito!',
        content: '¿Confirma la habilitación de la serie?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            'Confirmar': function (){
                 $.ajax({
                    type:'POST',
                    url: base_url+'index.php/contratos/cambiarestatuspe',
                    data: {
                        ideq:ideq,
                        idserie:idserie
                    },
                    statusCode:{
                        404: function(data){
                            toastr["error"]("No se pudo pocesar la solicitud","Error"); 
                        },
                        500: function(data){
                            toastr["error"]("No se pudo pocesar la solicitud","Error"); 
                            console.log(data);
                        }
                    },
                    success: function (data){
                        toastr.success('se habilito correctamente');
                        setTimeout(function(){ 
                            location.reload();
                        }, 2000);
                    }
                });         
            },
            'Cancelar': function () {
            }
        }
    });
}