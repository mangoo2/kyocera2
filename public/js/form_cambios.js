var base_url = $('#base_url').val();
$(document).ready(function(){
    comienzacalculo();
    var formulario_tipos_cambio = $('#tipo-cambio-form');
    formulario_tipos_cambio.validate({
            rules:{
                cambio_equipos: {
                    required: true
                },
                cambio_refacciones: {
                    required: true
                },
                cambio_consumibles: {
                    required: true
                },
                cambio_accesorios: {
                    required: true
                }
            },
            // Para mensajes personalizados
            messages:{
                cambio_equipos:{
                    required: "Ingrese un Tipo de Cambio para Equipos"
                },
                cambio_refacciones: {
                    required: "Ingrese un Tipo de Cambio para Refacciones"
                },
                cambio_consumibles: {
                    required: "Ingrese un Tipo de Cambio para Consumibles"
                },
                cambio_accesorios: {
                    required: "Ingrese un Tipo de Cambio para Accesorios"
                }
            },
        errorElement : 'div',
            // Si están correctos los campos, procesamos el formulario mediante Ajax
        submitHandler: function (form){
            var datos = formulario_tipos_cambio.serialize();
            $.ajax({
                type:'POST',
                url: base_url+'index.php/Tiposcambio/actualizarTiposCambio',
                data: datos,
                success:function(data){
                    swal({ title: "Éxito",
                            text: "Se guardaron los datos de manera correcta",
                            type: 'success',
                            showCancelButton: false,
                            allowOutsideClick: false,
                        });
                        tableregacciones();
                        setTimeout(function(){
                            actualizacionprecios();
                            location.reload();
                        },1000);
                }
            }); 
        }
    
    });
    

});
function comienzacalculo(){
    var tcambio = $('#cambio_refacciones').val();
    console.log(tcambio);
    var porgana=$('#porc_gana_refa').val();
    console.log(porgana);
    var valorini = parseFloat(tcambio)/((100-parseFloat(porgana))/100);
    $('.aaa_6').val(valorini.toFixed(2));
    var valor1 =parseFloat($('.aaa_6').val())/0.6; 
    $('.for_gen_6').val(valor1.toFixed(2));

    var valor2 =parseFloat($('.for_gen_6').val())/0.99;
    $('.for_gen_5').val(valor2.toFixed(2));

    var valor3 =parseFloat($('.for_gen_5').val())/0.99;
    $('.for_gen_4').val(valor3.toFixed(2));

    var valor4 =parseFloat($('.for_gen_4').val())/0.99;
    $('.for_gen_3').val(valor4.toFixed(2));

    var valor5 =parseFloat($('.for_gen_3').val())/0.99;
    $('.for_gen_2').val(valor5.toFixed(2));

    var valor6 =parseFloat($('.for_gen_2').val())/0.99;
    $('.for_gen_1').val(valor6.toFixed(2));
    //================================================
    var frec_1 = parseFloat($('.for_gen_1').val())*0.7;
    var frec_2 = parseFloat($('.for_gen_2').val())*0.7;
    var frec_3 = parseFloat($('.for_gen_3').val())*0.7;
    var frec_4 = parseFloat($('.for_gen_4').val())*0.7;
    var frec_5 = parseFloat($('.for_gen_5').val())*0.7;
    var frec_6 = parseFloat($('.for_gen_6').val())*0.7;
    $('.sem_frec_1').val(frec_1.toFixed(2));
    $('.sem_frec_2').val(frec_2.toFixed(2));
    $('.sem_frec_3').val(frec_3.toFixed(2));
    $('.sem_frec_4').val(frec_4.toFixed(2));
    $('.sem_frec_5').val(frec_5.toFixed(2));
    $('.sem_frec_6').val(frec_6.toFixed(2));
    //================================================
    var poli_1 = parseFloat($('.for_gen_1').val())*0.65;
    var poli_2 = parseFloat($('.for_gen_2').val())*0.65;
    var poli_3 = parseFloat($('.for_gen_3').val())*0.65;
    var poli_4 = parseFloat($('.for_gen_4').val())*0.65;
    var poli_5 = parseFloat($('.for_gen_5').val())*0.65;
    var poli_6 = parseFloat($('.for_gen_6').val())*0.65;
    $('.local_poli_1').val(poli_1.toFixed(2));
    $('.local_poli_2').val(poli_2.toFixed(2));
    $('.local_poli_3').val(poli_3.toFixed(2));
    $('.local_poli_4').val(poli_4.toFixed(2));
    $('.local_poli_5').val(poli_5.toFixed(2));
    $('.local_poli_6').val(poli_6.toFixed(2));
    //================================================
    var aaa_1 = parseFloat($('.for_gen_1').val())*0.6;
    var aaa_2 = parseFloat($('.for_gen_2').val())*0.6;
    var aaa_3 = parseFloat($('.for_gen_3').val())*0.6;
    var aaa_4 = parseFloat($('.for_gen_4').val())*0.6;
    var aaa_5 = parseFloat($('.for_gen_5').val())*0.6;
    $('.aaa_1').val(aaa_1.toFixed(2));
    $('.aaa_2').val(aaa_2.toFixed(2));
    $('.aaa_3').val(aaa_3.toFixed(2));
    $('.aaa_4').val(aaa_4.toFixed(2));
    $('.aaa_5').val(aaa_5.toFixed(2));
}
function actualizacionprecios(){
    $.ajax({
        type:'POST',
        url: base_url+'index.php/Listapreciosupdate',
        success:function(data){
        }
    }); 
}
function tableregacciones(){
    var DATAr  = [];
        var TABLA   = $("#tabla_descuentos_refacciones tbody > tr");
            TABLA.each(function(){         
            item = {};
            item ["id"] = $(this).find("input[id*='id']").val();
            item ["pd_inicio"] = $(this).find("input[id*='pd_inicio']").val();
            item ["pd_fin"]   = $(this).find("input[id*='pd_fin']").val();
            item ["for_gen"]  = $(this).find("input[id*='for_gen']").val();
            item ["sem_frec"]  = $(this).find("input[id*='sem_frec']").val();
            item ["local_poli"]  = $(this).find("input[id*='local_poli']").val();
            item ["aaa"]  = $(this).find("input[id*='aaa']").val();
            DATAr.push(item);
        });
        INFO  = new FormData();
        arrayrefaccion   = JSON.stringify(DATAr);
    var datos = 'arrayRefaccion='+arrayrefaccion;
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Tiposcambio/tabladescuentosrefacciones",
        data: datos,
        success: function (response){

        },
        error: function(response){
            alertfunction('Atención!','Algo salió mal, intente de nuevo o contacte al administrador del sistema'); 
        }
    });
}