<style type="text/css">
table td{font-size: 12px;padding: 6px 7px;  }
.div-btn-gropu{min-width: 130px;}
.select2-container--default{  width: 100% !important;}
#calendario{min-height: 550px;}
#expor_fechaini,#expor_fechafin{
  width: 99%;
}
.task-cat{
  min-width: 116px;
  text-align: center;
    font-weight: bold;
    border-radius: 6px;
}
.group-btns{
  min-width: 130px;
}
</style>
<input type="date" id="fechahoy" value="<?php echo date('Y-m-d');?>" style="display: none;">
<input type="number" id="horahoy" value="<?php echo date('G');?>" style="display: none;">
<!-- START CONTENT -->
<section id="content" width="100%">
  
  <!--start container-->
  <div class="container" width="100%">
    <div class="section">
      <div class="row">
        <div class="col s12">
          <ul class="tabs tab-demo z-depth-1">
            <li class="tab col s3"><a class="active" href="#test1">Asistencias</a></li>
            <li class="tab col s3"><a href="#test2" onclick="cargavaca()">Vacaciones/Incapacidad</a></li>
            <li class="tab col s3"><a href="#test3" onclick="loadfhp();">Permisos de accesos</a></li>
          </ul>
        </div>
        <div class="col s12 ">
          <div id="test1" class="col s12 card-panel">
            <!------------->
              <div class="row">
                <div class="col s3"  >
                  <label>Seleccione día</label>
                  <input type="date" class="form-control-bmz" id="sdia" value="<?php echo date('Y-m-d');?>" onchange="load()">
                  
                </div>
                <div class="col s3">
                  <label>Empresa</label>
                  <select name="emp" id="emp" class="browser-default form-control-bmz " onchange="load()" >
                    <option value="0">Todos</option><option value="1">Alta Productividad</option><option value="2">D-Impresión</option>
                  </select>
                </div>
                <div class="col s3"></div>
                <div class="col s3" style="text-align: end;">
                  <button class="b-btn b-btn-success" onclick="generarreporte()" style="margin-top: 22px;" ><i class="fas fa-file-excel"></i> Generar Reporte</button>
                </div>
              </div>
              <div class="row">
                <div class="col s12 m12 24">
                  <div class="card-panel">
                    
                    <div class="row">
                      <div class="col s12">
                        <table class="table" id="tablelist">
                          <thead>
                            <tr>
                              <th>#</th>
                              <th></th>
                              <th>Id Empleado</th>
                              <th>Empleado</th>
                              <th>Día</th>
                              <th>Hora de inicio</th>
                              <th>Hora fin</th>
                              <th>Ubicación</th>
                              <th>Ubicación salida</th>
                              <th></th>
                              <th></th>
                            </tr>
                          </thead>
                          <tbody>
                            
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
                
              </div>
            <!------------->
          </div>
          <div id="test2" class="col s12 card-panel">
            <!------------->
              <div class="row">
                <div class="col s3">
                  <label>Personal</label>
                  <select class="form-control-bmz browser-default" id="empleado">
                    
                  </select>
                </div>
                <div class="col s2">
                  <label>Fecha Inicio</label>
                  <input type="date" class="form-control-bmz" id="fechainicio">
                </div>
                <div class="col s2">
                  <label>Fecha Fin</label>
                  <input type="date" class="form-control-bmz" id="fechafin">
                </div>
                <div class="col s2">
                  <label>Tipo</label>
                  <select class="form-control-bmz browser-default" id="tipo" name="tipo">
                    <option value="1">Vacaciones</option>
                    <option value="2">Incapacidad</option>
                  </select>
                </div>
                <div class="col s3">
                  <button class="b-btn b-btn-success tooltipped" data-position="top" data-delay="50" data-tooltip="Asistencia" id="registrarvacacion" style="margin-top: 22px;">Registrar</button>
                </div>
                
              </div>
              <div class="row">
                <div class="col s7" id="calendario">
                  
                </div>
                <div class="col s5" >
                  <table class="table" id="tableasistencia">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Personal</th>
                        <th>Fecha Inicio</th>
                        <th>Fecha Fin</th>
                        <th title="Fecha en la que no se le bloqueara el acceso">Fecha permitida</th>
                        <th>Tipo</th>
                        <th></th>
                      </tr>
                    </thead>
                  </table>
                </div>
              </div>
              
            <!------------->
          </div>
          <div id="test3" class="col s12 card-panel">
            <!------------------------>
              <div class="row">
                <div class="col s3">
                  <label>Personal</label>
                  <select class="form-control-bmz browser-default" id="pfh_empleado">
                    
                  </select>
                </div>
                <div class="col s2">
                  <label>Fecha</label>
                  <input type="date" class="form-control-bmz" id="pfh_fecha">
                </div>
                
                <div class="col s3">
                  <?php if($idpersonal==1 or $idpersonal==17 or $idpersonal==18){ ?>
                  <button class="b-btn b-btn-success" id="registrarpermiso" style="margin-top: 22px;">Registrar</button>
                  <?php } ?>
                </div>
                
              </div>
              <div class="row">
                <div class="col s6">
                  <table id="table_permisosfuera" class="table">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Personal</th>
                        <th>Fecha</th>
                        <th>Reg</th>
                        <th></th>
                      </tr>
                    </thead>
                    
                  </table>
                </div>
              </div>
            <!------------------------>
          </div>
          
        </div>
      </div>
      
    </div>
  </section>
  