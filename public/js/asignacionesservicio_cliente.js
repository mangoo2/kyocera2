var base_url = $('#base_url').val();
var addtr=0;
$(document).ready(function($) {
  $('.chosen-select').chosen({width: "100%"});
});
function guardar_cliente(){
   var formulario_cliente = $('#clientes_from');
    //===========================================
      var DATA3  = [];
              var TABLA3   = $("#tabledatoscontacto tbody > tr");
                  TABLA3.each(function(){         
                        items = {};
                        items['datosId'] = $(this).find("input[id*='datosId']").val();
                        items['atencionpara'] = $(this).find("input[id*='atencionpara']").val();
                        items['puesto'] = $(this).find("input[id*='puesto']").val();
                        items['email'] = $(this).find("input[id*='email']").val();
                        items['telefono'] = $(this).find("input[id*='telefono']").val();
                        items['celular'] = $(this).find("input[id*='celular']").val();
                       DATA3.push(items);
                  });
      persona_contacto = JSON.stringify(DATA3);
    //===========================================

    var DATA4  = [];
    var TABLA4   = $(".border_datos tbody > tr");
        TABLA4.each(function(){         
             item = {};
             item ["id"]   = $(this).find("input[id*='id_df']").val();
             item ["razon_social"]   = $(this).find("textarea[id*='razon_social']").val().replace('&', '----');
             item ["rfc"]   = $(this).find("input[id*='rfc']").val();
             item ["cp"]   = $(this).find("input[id*='cp']").val();
             item ["calle"]   = $(this).find("textarea[id*='calle']").val();
             item ["num_ext"]   = $(this).find("input[id*='num_ext']").val();
             item ["num_int"]   = $(this).find("input[id*='num_int']").val();
             item ["colonia"]   = $(this).find("textarea[id*='colonia']").val();
             DATA4.push(item);
        });
    datos_fiscales = JSON.stringify(DATA4);
    
    var DATA5  = [];
    var TABLA5   = $(".tabla_dir tbody > tr");
        TABLA5.each(function(){         
             item = {};
             item ["id"] = $(this).find("input[id*='idclientedirecc']").val();
             item ["direccion"] = $(this).find("input[id*='direccion']").val();
             DATA5.push(item);
        });
    datos_direccion = JSON.stringify(DATA5);
    var aaa_x = 0; 
      if($('#aaa').is(':checked')){
          aaa_x=1;   
      }else{
          aaa_x=0;
      }
    var datos = formulario_cliente.serialize()+'&aaa='+aaa_x+'&datos_fiscales='+datos_fiscales+'&datos_direccion='+datos_direccion+'&persona_contacto='+persona_contacto+'&condicion=null';
    $.ajax({
        type:'POST',
        url: base_url+'index.php/clientes/insertaActualizaClientes',
        data: datos,
        success:function(data){  
          console.log(data);
          $('#idclientenew').val(parseFloat(data));
            $('.mostrarsoloclientes').show();
                swal({ title: "Éxito",
                    text: "Se guardaron los datos del cliente",
                    type: 'success',
                    showCancelButton: false,
                    allowOutsideClick: false,
                });
        }
    });
}

function cliente_texto(){
  $('.modal').modal();
  $.ajax({
      type:'POST',
      url: base_url+"Asignaciones/clientes_datos",
      
      success: function (data){
          $('.datos_cliente').html(data);
          $('.chosen-select').chosen({width: "95%"});

          $(".btn_agregar_tel").click(function(){
          var contador = $('.border_tel').length; 
          var contadoraux = contador+1; 
          var $template = $("#tel_dato1"),
          $clone = $template
         .clone()
         .show()
         .removeAttr('id')
         .attr('id',"tel_dato"+contadoraux)
         .insertAfter($("#tel_dato"+contador));

         $clone.find($('#categoria'));
         $clone.find(".div_btn_tel").html('<a class="btn btn-floating waves-effect waves-light red btn_eliminar_tel"><i class="material-icons">delete_forever</i></a>');
         $clone.find("select").val("");
         $clone.find("input").val("");
         eliminar_tel();
         $('select').material_select();

        });

        $(".btn_agregar_cel").click(function(){
          var contador = $('.border_cel').length; 
          var contadoraux = contador+1; 
          var $template = $("#cel_datos1"),
          $clone = $template
         .clone()
         .show()
         .removeAttr('id')
         .attr('id',"cel_datos"+contadoraux)
         .insertAfter($("#cel_datos"+contador));

         $clone.find($('#categoria'));
         $clone.find(".div_btn_cel").html('<a class="btn btn-floating waves-effect waves-light red btn_eliminar_cel"><i class="material-icons">delete_forever</i></a>');
         $clone.find("select").val("");
         $clone.find("input").val("");
         eliminar_cel();
         $('select').material_select();

        });
        
        $(".btn_agregar_contacto").click(function(){
          var contador = $('.border_contacto').length; 
          var contadoraux = contador+1; 
          var $template = $("#contacto_datos1"),
          $clone = $template
         .clone()
         .show()
         .removeAttr('id')
         .attr('id',"contacto_datos"+contadoraux)
         .insertAfter($("#contacto_datos"+contador));

         $clone.find($('#categoria'));
         $clone.find(".div_btn_contacto").html('<a class="btn btn-floating waves-effect waves-light red btn_eliminar_contacto"><i class="material-icons">delete_forever</i></a>');
         $clone.find("input").val("");
         eliminar_contacto();
        });
      }
  });
}
function eliminar_tel(){
    $(".btn_eliminar_tel").click(function(){
        var $row = $(this).parents('.border_tel');
        // Remove element containing the option
        $row.remove();

    });
}

function eliminar_cel(){
    $(".btn_eliminar_cel").click(function(){
        var $row = $(this).parents('.border_cel');
        // Remove element containing the option
        $row.remove();

    });
}

function eliminar_contacto(){
    $(".btn_eliminar_contacto").click(function(){
        var $row = $(this).parents('.border_contacto');
        // Remove element containing the option
        $row.remove();

    });
}
function add_address(){ 
    var dirrecc = $('#direcc').val();
    if(dirrecc!= ""){
      var html_text = '<tr class="addtr_'+addtr+'">\
                          <td><input id="idclientedirecc" type="hidden"  value="0" readonly></td>\
                          <td><input type="text" id="direccion" value="'+dirrecc+'"></td>\
                          <td><a class="btn-floating waves-effect waves-light red accent-2" onclick="deteletr('+addtr+')"><i class="material-icons">clear</i></a></td>\
                          </tr>';
      $('.tbody_direccion').append(html_text);   
      $('#tdireccion2').append('<option>'+dirrecc+'</option>');
      addtr++; 
      $('#direcc').val('');
    }else{
      swal({title: "Atención",text: "Tienes que ingresar una dirección",
        timer: 2000,showConfirmButton: false
      });
    }
}
function deteletr(id){
  $('.addtr_'+id).remove();
}
var datos_df = 1;
function agregar_d_f() {
      var tablerow='<tr class="polizaselecteddd_row_'+datos_df+'">\
                    <td><input type="hidden" id="id_df" value="0" style="max-width: 40px;"></td>\
                    <td><textarea type="text" id="razon_social" value=""></textarea></td>\
                    <td><input type="text" id="rfc" value=""></td>\
                    <td><input type="text" id="cp" value="" style="max-width: 60px;"></td>\
                    <td><textarea type="text" id="calle" value=""></textarea></td>\
                    <td><input type="text" id="num_ext" value="" style="max-width: 60px;"></td>\
                    <td><input type="text" id="num_int" value="" style="max-width: 60px;"></td>\
                    <td><textarea type="text" id="colonia" value=""></textarea></td>\
                    <td>\
                        <a onclick="deletepolisadd_df('+datos_df+')" class="btn-floating red tooltipped elimina" data-position="top" data-delay="50" data-tooltip="Eliminar">\
                          <i class="material-icons">delete_forever</i>\
                        </a>\
                    </td>\
                  </tr>';
    $('#border_datos tbody').append(tablerow);
    datos_df++;
}
function deletepolisadd_df(id) {
  $('.polizaselecteddd_row_'+id).remove();
}
function datoscliente(id){
  $.ajax({
        type:'POST',
        url: base_url+"Asignaciones/datoscliente",
        data: {
          id:id
        },
        success: function (data){
          console.log(data);
          var array = $.parseJSON(data);
          var datos=array.datos;
          if (datos.aaa==1) {
            $('#aaa').prop('checked',true);
          }else{
            $('#aaa').prop('checked',false);
          }
          $('#idclientenew').val(datos.id);
          $('#empresa').val(datos.empresa).change();
          $('#puesto_contacto').val(datos.puesto_contacto).change();
          $('#email').val(datos.email).change();
          $('#estado').val(datos.estado).change();
          $('#municipio').val(datos.municipio).change();
          $('#antiguedad').val(datos.antiguedad).change();
          
          array.direccion.forEach(function(element) {
              var html_text = '<tr class="addtr_'+element.idclientedirecc+'">\
                          <td><input id="idclientedirecc" type="hidden"  value="0" readonly></td>\
                          <td><input type="text" id="direccion" value="'+element.direccion+'"></td>\
                          <td><a class="btn-floating waves-effect waves-light red accent-2" onclick="delete_direcc('+element.idclientedirecc+')" disabled><i class="material-icons">clear</i></a></td>\
                          </tr>';
                $('.tbody_direccion').append(html_text); 
                $('#tdireccion2').append('<option data-iddir="'+element.idclientedirecc+'">'+element.direccion+'</option>');
          });
          $('#observaciones').val(datos.observaciones).change();
          $('#atencionpara').val(datos.antencionpara).change();
          $('#planta_sucursal').val(datos.planta_sucursal).change();
          array.datosfiscales.forEach(function(element) {
            var tablerow='<tr class="polizaselecteddd_row_'+element.id+'">\
                    <td><input type="hidden" id="id_df" value="'+element.id+'" style="max-width: 40px;"></td>\
                    <td><textarea type="text" id="razon_social">'+element.razon_social+'</textarea></td>\
                    <td><input type="text" id="rfc" value="'+element.rfc+'"></td>\
                    <td><input type="text" id="cp" value="'+element.cp+'" style="max-width: 60px;"></td>\
                    <td><textarea type="text" id="calle">'+element.calle+'</textarea></td>\
                    <td><input type="text" id="num_ext" value="'+element.num_ext+'" style="max-width: 60px;"></td>\
                    <td><input type="text" id="num_int" value="'+element.num_int+'" style="max-width: 60px;"></td>\
                    <td><textarea type="text" id="colonia">'+element.colonia+'</textarea></td>\
                    <td>\
                        <a onclick="Eliminar_Tabla_DatosF('+datos_df+')" class="btn-floating red tooltipped elimina" data-position="top" data-delay="50" data-tooltip="Eliminar" disabled>\
                          <i class="material-icons">delete_forever</i>\
                        </a>\
                    </td>\
                  </tr>';
              $('#border_datos tbody').append(tablerow);
          });

          array.contactos.forEach(function(element) {
            datoscontactoadd(element.datosId,element.atencionpara,element.puesto,element.email,element.telefono,element.celular);
          });

          $('#clientes_from input').attr('readonly', true);
          $('#clientes_from textarea').attr('readonly', true);
          $('.chosen-select').trigger("chosen:updated");
          $('.mostrarsoloclientes').show();
          $('.guardar_cliente').hide();
        }
    }); 
}
function agregar_pc(){
  var atencionpara = $('#atencionpara').val();
  var puesto_contacto = $('#puesto_contacto').val();
  var email = $('#email').val();
  var tel_local = $('#tel_local').val();
  var celular = $('#celular').val();
  datoscontactoadd(0,atencionpara,puesto_contacto,email,tel_local,celular);
  $('.inputdatoscontacto').val('');
}
var rowpc=0;
function datoscontactoadd(id,atencionpara,puesto_contacto,email,tel_local,celular){
  var addtrpc =  '<tr class="rowpc_'+rowpc+'_'+id+'">\
                    <td>\
                      <input type="hidden" id="datosId" value="'+id+'" readonly>\
                      <input type="hidden" id="atencionpara" value="'+atencionpara+'" readonly>'+atencionpara+'\
                    </td>\
                    <td><input type="hidden" id="puesto" value="'+puesto_contacto+'" readonly>'+puesto_contacto+'</td>\
                    <td><input type="hidden" id="email" value="'+email+'" readonly>'+email+'</td>\
                    <td><input type="hidden" id="telefono" value="'+tel_local+'" readonly>'+tel_local+'</td>\
                    <td><input type="hidden" id="celular" value="'+celular+'" readonly>'+celular+'</td>\
                    <td>\
                      <a class="btn-floating red tooltipped" onclick="Eliminar_pc('+id+','+rowpc+')" \
                        data-position="top" data-delay="50" data-tooltip="Eliminar">\
                        <i class="material-icons">delete_forever</i>\
                      </a>\
                    </td>\
                </tr>';
  $('.tabledatoscontactotb').append(addtrpc);
  rowpc++;
}
function Eliminar_pc(id,row){
  if (id==0) {
    $('.rowpc_'+row+'_'+id).remove();
  }else{
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de Eliminar el contacto? El cambio será irreversible',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    url: base_url+"index.php/clientes/eliminar_persona_contacto/"+id,
                    success: function (response){

                        if(response==1){
                            $.alert({boxWidth: '30%',useBootstrap: false,
                                title: 'Éxito!',content: 'Contacto Eliminado!'});
                                $('.rowpc_'+row+'_'+id).remove();
                        }else{
                            notificacion(1);
                            setTimeout(function(){ 
                              location.reload();
                            }, 2000);
                        }
                    },
                    error: function(response){
                        notificacion(1);
                        setTimeout(function(){ 
                          location.reload();
                        }, 2000);
                    }
                });
            },
            cancelar: function (){
                
            }
        }
    });
  }
}