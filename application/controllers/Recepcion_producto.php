<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Recepcion_producto extends CI_Controller {
    public function __construct()     {
        parent::__construct();
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloRecepcion_producto');
        $this->load->helper('url');
        if($this->session->userdata('logeado')==true){
            $this->idpersonal = $this->session->userdata('idpersonal');
            $this->perfilid =$this->session->userdata('perfilid');
            /*
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,52);// 52 es el id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
            */
        }else{
            redirect('Sistema'); 
        }
        $this->version = date('YmdHi');
    }

    // Listado de Empleados
    function index()     {
            $data['perfilid'] = $this->perfilid;
            $data['idpersonal'] = $this->idpersonal;
            if($this->perfilid==1){
                $data['display']="block";
            }else{
                $data['display']="none";
            }
            
            $data['version'] = $this->version;
            $data['personalrow'] = $this->ModeloCatalogos->getselectwheren('personal',array('estatus'=>1));
            $this->load->view('header');
            $this->load->view('main',$data);
            $this->load->view('recepcionp/listado');
            $this->load->view('recepcionp/listadojs');
            $this->load->view('footerl');  
    }
    function getlist(){
        $params = $this->input->post();
        $productos = $this->ModeloRecepcion_producto->getListado($params);
        $totalRecords=$this->ModeloRecepcion_producto->getListadototal($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totalRecords),  
            "recordsFiltered" => intval($totalRecords),
            "data"            => $productos->result(),
            "query"           =>$this->db->last_query()   
        );
        
        echo json_encode($json_data);
    }
    
    
}
?>
