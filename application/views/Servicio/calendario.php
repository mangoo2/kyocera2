<style type="text/css">
  .chosen-container{margin-left: 0px;}
  .rowmarginbotom{margin-bottom: 5px;}
  .textcolorred{color:red;}
  #tablerentapoliza td{padding: 0px;}
  .texcolorred{color:red;}
  .texcolorblue{color:blue;}
  .texcolorgrenn{color:green;}
  .texcoloryellow{color:#fdc363;}
  .fc-today{background: #ffffff !important;}
  .colordeiconofinalizado{color:#6fe274;}
  .rowmarginbotom .col{padding: 0 5px;}
  .fa-pencil-alt2:before {content: "\f303";}
  .fc-theme-standard td, .fc-theme-standard th {border: 1px solid #ddd !important;}
  .btn_whatsapp{width: 50px;background: url(<?php echo base_url();?>public/img/whatsapp.png);height: 50px;display: block;float: right;background-size: 100%;cursor: pointer; border: 0px;}
  .btn-envioser{float: right;}
  .error{color: red;}
  #form_add_cont input{margin-bottom: 0px;}
  .datosclienteservicio .gar{font-size: 15px;position: absolute;margin-left: 15px;}
</style>
<input type="hidden" id="idpersonal" value="<?php echo $idpersonal;?>" readonly>
<input type="hidden" id="codigocarga" value="<?php echo date('YmdGis');?>" readonly>
<!-- START CONTENT -->
<section id="content">
  <!--start container-->
  <div class="container">
    <div class="section">
      <div class="card-panel">
          <div id="table-datatables">
            <div class="row">
              <div class="col s6"><h4 class="header">Calendario</h4></div>            
              <div class="col s4" align="right"><button class="btn cyan waves-effect waves-light right savepoliza" type="button" onclick="actualizar_cal()">Actualizar</button></div>  
              <div class="col s1" align="right"><button class="btn-floating right" type="button" onclick="modal_ayuda()">?</button></div>  
            </div>
            <div class="row">
              <div class="col s2">
                <label>Mes</label>
                <select class="browser-default  chosen-select" id="nuevomesview" onchange="cambiarfecha()"><?php 
                  echo '<option value="'.date('Y-m-d').'">Fecha actual</option><option value="'.date('Y').'-01-01">Enero</option><option value="'.date('Y').'-02-01">Febrero</option><option value="'.date('Y').'-03-01">Marzo</option><option value="'.date('Y').'-04-01">Abril</option><option value="'.date('Y').'-05-01">Mayo</option><option value="'.date('Y').'-06-01">Junio</option><option value="'.date('Y').'-07-01">Julio</option><option value="'.date('Y').'-08-01">Agosto</option><option value="'.date('Y').'-09-01">Septiembre</option><option value="'.date('Y').'-10-01">Octubre</option><option value="'.date('Y').'-11-01">Noviembre</option><option value="'.date('Y').'-12-01">Diciembre</option>';
                ?></select>
              </div>
              <div class="col s2">
                <label>Servicio</label><select class="browser-default " id="servicioid" ></select>
              </div>
              <div class="col s2">
                <label>Tecnico</label>
                <select class="browser-default chosen-select" id="tecnicoid" onchange="updatecalendar()">
                  <option value="0">Seleccione una opción</option>
                  <?php foreach ($resultstec as $item) { 
                    echo '<option value="'.$item->personalId.'">'.$item->tecnico.'</option>';
                  }  ?>
                </select>
              </div>
              <div class="col s2">
                <label>Cliente</label><select class="browser-default" id="clienteid" ></select>
              </div>
              <div class="col s2">
                <label>Zona</label>
                <select id="zona" class="browser-default  chosen-select" onchange="updatecalendar()"><option value="0">Todos</option><?php foreach ($resultzonas as $item) {  echo '<option value="'.$item->id.'">'.$item->nombre.'</option>'; } ?></select>
              </div>
              <div class="col s2">
                <label>Solicitante</label>
                <select id="soli" class="browser-default  chosen-select" onchange="updatecalendar()"><option value="0">Todos</option><?php foreach ($personalrows->result() as $item) { echo '<option value="'.$item->personalId.'">'.$item->nombre.' '.$item->apellido_paterno.'</option>'; } ?></select>
              </div>
              <!--
              <div class="col s2">
                <label>Ciudad</label>
                <select class="browser-default  chosen-select" onchange="updatecalendar()">
                  
                </select>
              </div>
              -->
            </div>
            <div class="row" id="calendario">
              <br>
            </div>
            <?php 
              $fila=3;
              while ($fila <= 2){
            ?>
            <div class="row">
              <!--id="calendario"--> 
              <div class="col s12" >
                <?php 
                  $lugar = 15;
                  $dial=1;
                while ($lugar <= 14){
                ?>
                <div class="card cardinfo" style="width: 16.1%;float: left;margin-left: 5px; font-size: 10px">
                  <div class="card-content cardinfo" style="background: #ded5d5b8; padding: 10px; padding-top: 0px;">
                    <div class="row">
                      <div class="col s6" style="border: solid 1px;padding: 4px;font-size: 12px;">
                        <?php   
                            switch ($dial) {
                              case 1:
                                echo 'Lunes';
                                break;
                              case 2:
                                echo 'Martes';
                                break;
                              case 3:
                                echo 'Miercoles';
                                break;
                              case 4:
                                echo 'Jueves';
                                break;
                              case 5:
                                echo 'Viernes';
                                break;
                              case 6:
                                echo 'Sabado';
                                break;
                                    
                              default:
                                # code...
                                break;
                            }
                        ?>
                      </div>
                      <div class="col s2">
                        
                      </div>
                      <div class="col s4">
                        <a class="btn-floating waves-effect waves-light red accent-2" style="text-align: center;">
                          <?php echo $lugar;?>
                        </a>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col s6">10:00 - 12:00</div>
                      <div class="col s6 texcolorred">PUEBLA FC</div>
                    </div>
                    <div class="row">
                      <div class="col s6 ">12:00 - 01:00</div>
                      <div class="col s6 texcolorblue">SISTENSIS</div>
                    </div>
                    <div class="row">
                      <div class="col s6">01:00 - 02:00</div>
                      <div class="col s6 texcoloryellow">ECODYNEUET</div>
                    </div>
                    <div class="row">
                      <div class="col s6">03:00 - 04:00</div>
                      <div class="col s6 texcolorgrenn">SERVIMED</div>
                    </div>
                    <div class="row">
                      <div class="col s6">05:00 - 06:00</div>
                      <div class="col s6 texcolorgrenn">VEROSTAMP</div>
                    </div>
                    <div class="row">
                      <div class="col s6">07:00 - 08:00</div>
                      <div class="col s6 texcoloryellow">SER. FABRICAS</div>
                    </div>

                  </div>
                </div>
                <?php 
                $dial++;
                $lugar++;
                }
                ?>
              </div>
            </div>
            <?php
              $fila++; 
              }
            ?>
          </div>
      </div>
    </div>  
  </div>
</section>
<div id="modalinfocalendario" class="modal"  >
    <div class="modal-content" style="padding-top: 0px; padding-left: 10px; padding-right: 10px;">
      <h5 >Calendario</h5>
      <br>
      <div class="row">
        <div class="col s12 m6 6" style="padding: 0 5px;">
          <div class="row rowmarginbotom">
              <div class="col s12 m5 6 textcolorred">Cliente:</div>
              <div class="col s12 m7 6 datoscliente"></div>
          </div>
          <div class="row rowmarginbotom">
              <div class="col s12 m5 6 textcolorred">Contacto:</div>
              <div class="col s12 m7 6 datosclientecontacto"></div>
          </div>
          <div class="row rowmarginbotom">
              <div class="col s12 m5 6 textcolorred">Tipo de Venta:</div>
              <div class="col s12 m7 6 datosclientetipo"></div>
          </div>
          <div class="row rowmarginbotom">
              <div class="col s12 m5 6 textcolorred">Tipo de Servicio:</div>
              <div class="col s12 m7 6 datosclienteservicio"></div>
          </div>
          <div class="row rowmarginbotom">
              <div class="col s12 m5 6 textcolorred">Zona:</div>
              <div class="col s12 m7 6 datosclientezona"></div>
          </div>
          <div class="row rowmarginbotom">
              <div class="col s12 m5 6 textcolorred">Ciudad:</div>
              <div class="col s12 m7 6">PUEBLA</div>
          </div>
          <div class="row rowmarginbotom">
              <div class="col s12 m5 6 textcolorred">Técnico:</div>
              <div class="col s12 m7 6 datosclientetecnico"></div>
          </div>
          <div class="row rowmarginbotom">
              <div class="col s12 m5 6 textcolorred">Prioridad:</div>
              <div class="col s12 m7 6 datosclienteprioridad"></div>
          </div>
          <div class="row rowmarginbotom equipoacceso"></div>
        </div>
        <div class="col s12 m6 6" style="padding: 0 5px;">
          <div class="row rowmarginbotom textcolorred datosclienteasignaciones">
            Asignaciones Mensuales / Asignaciones diarias
          </div>
          <div class="row rowmarginbotom datosclienteahorario"></div>
          <div class="row rowmarginbotom">
            <div class="col s12">
              <h4 class="datosclienteservicio"></h4>
            </div>
            <table class="table striped" id="tablerentapoliza">
              <thead>
                <tr>
                  <td>Modelo</td>
                  <td>Serie</td>
                  <td>Dirección</td>
                  <td class="infopro"></td>
                </tr>
              </thead>
              <tbody class="tablerentapoliza"></tbody>
            </table>
            <div class="col s12 t_dconsumibles">
            </div>
          </div>
        </div>
      </div>
      <!--- Motivo --->
      <div class="row motivo_texto">
        <div class="col s12 m6 62">
          <h5>Motivo del servicio</h5>
          <p>Motivo del Servicio: <span class="t_motivo_serv"></span></p>
          <p>Documentos y accesorios: <span class="t_doc_acces"></span></p>
        </div>
      </div> 
      <div class="row">
        <div class="col s12 m12 12">
          <div class="btn_confirmacion_servicio" style="text-align: end;"></div>
          <div class="btn_solicitud_consumibles" style="text-align: end;"></div>
        </div>
      </div>
      <span hidden="" class="id_asigna"></span>
      <span hidden="" class="id_tipo"></span>
      <div class="row">
        <div class="col m12">
          <label>Comentario</label>
          <textarea class="form-control-bmz" id="comentariocal" style="height: 100px !important;"></textarea>
        </div>
      </div>
      <div class="row">
        <div class="col m6">
          <div class="row">
            <div class="col m3">
              <button type="button" class="waves-effect cyan btn-bmz modalconfimar" onclick="modalconfimar()">Confirmar</button>
            </div>
            <div class="col m3">
              <button type="button" class="waves-effect cyan btn-bmz btn_reagendar" onclick="re_agendar()">Re Agendar</button>
              <button type="button" class="waves-effect btn-bmz btn_reagendar btn_reagendar_c" style="display: none" onclick="re_agendar_c()">Cancelar</button>
            </div>
            <div class="col m3 btn-clonar"></div>
            <div class="col m3 botondefinalizar"></div>  
          </div>
        </div>
        <div class="col m6">
          <span class="text_reagendar"></span>
        </div>
      </div>
      <div class="modal-footer">
        <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
      </div>
    </div>
</div>

<div id="modal_confimar" class="modal">
    <div class="modal-content" align="center">
      <h5>¿Estas seguro de confirmar esta asignación?</h5>
    </div>
    <div class="row">
      <div class="col m12" align="center">
        <button type="button" class="waves-effect cyan btn-bmz" onclick="confimar_asignacion()">Aceptar</button>
      </div>
    </div>        
</div>      

<div id="modal_ayuda_color" class="modal">
    <div class="modal-content">
      <div class="row">
        <div class="col m1" style="background: #ff0000; color: #ff0000;">
          <b>__</b>
        </div>  
        <div class="col m11">
          <label style="font-size: 20px; color: black;">Servicios Correctivos</label> 
        </div>
      </div>
      <div class="row">
        <div class="col m1" style="background: #ff9100; color: #ff9100;">
          <b>__</b>
        </div>  
        <div class="col m11">
          <label style="font-size: 20px; color: black;">Asesoría - Conexión remota</label> 
        </div>
      </div>
      <div class="row">
        <div class="col m1" style="background: #dbca05; color: #dbca05;">
          <b>__</b>
        </div>  
        <div class="col m11">
          <label style="font-size: 20px; color: black;">Pólizas</label> 
        </div>
      </div>
      <div class="row">
        <div class="col m1" style="background: #00a1ff; color: #00a1ff;">
          <b>__</b>
        </div>  
        <div class="col m11">
          <label style="font-size: 20px; color: black;">Servicio Regular</label> 
        </div>
      </div>
      <div class="row">
        <div class="col m1" style="background: #038f18; color: #038f18;">
          <b>__</b>
        </div>  
        <div class="col m11">
          <label style="font-size: 20px; color: black;">Mensajería</label> 
        </div>
      </div>        
    </div>
    <div class="row">
      <div class="col m12" align="center">
        <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
      </div>
    </div>        
</div>

<div id="modal_consumibles_renta" class="modal">
    <div class="modal-content">
      <div class="row">
        <div class="col s12">
          <label>Firma del ejecutivo</label>
          <select class="browser-default form-control-bmz" id="firma_m">
            <?php foreach ($perfirma->result() as $itemf) {
              echo '<option value="'.$itemf->personalId.'">'.$itemf->nombre.' '.$itemf->apellido_paterno.' '.$itemf->apellido_materno.' / '.$itemf->email.' / '.$itemf->celular.'</option>';
            }?>
          </select>
        </div>
        <div class="col s12">
          <label>Copia oculta (BBC)</label>
          <select class="browser-default form-control-bmz" id="bbc_m"><option value="0"></option>
            <?php foreach ($perfirma->result() as $itemf) {
              echo '<option value="'.$itemf->personalId.'">'.$itemf->nombre.' '.$itemf->apellido_paterno.' '.$itemf->apellido_materno.' / '.$itemf->email.' / '.$itemf->celular.'</option>';
            }?>
          </select>
        </div>
        <div class="col s2" style="color: red;">Cliente</div>
        <div class="col s10 mcr_cliente"></div>
      </div>
      <div class="row">
        <div class="col s2" style="color: red;">Contrato</div>
        <div class="col s10 mcr_contrato"></div>
      </div>
      <div class="row">
        <div class="col s2" style="color: red;">Contratos</div>
        <div class="col s8"><select class="browser-default form-control-bmz" id="mcr_select_cont"></select></div>
        <div class="col s2"><button type="button" class="waves-effect btn-bmz  red" onclick="agregarcontratomail()">Agregar</button></div>
        <div class="col s12">
          <table class="table bordered striped" id="table_mcr_cont"><thead><tr><th>#</th><th>Folio</th><th></th></tr></thead><tbody class="tbody_mcr_cont"></tbody></table>
        </div>
      </div>
      <div class="row">
        <div class="col s2" style="color: red;">Contactos</div>
        <div class="col s8"><select class="browser-default form-control-bmz" id="mcr_select_contac"></select></div>
        <div class="col s2"><button type="button" class="waves-effect btn-bmz  red" onclick="agregarcontactomail()">Agregar</button></div>
        <div class="col s12"><table class="table bordered striped" id="table_mcr_contac"><thead><tr><th>#</th><th>Atencion para</th><th>Correo</th><th>Telefono</th><th>Celular</th><th></th></tr></thead><tbody class="tabbodycr_contac"></tbody></table></div>
      </div>
      <div class="row">
        <div class="col s12">
          <label>Asunto</label>
          <input class="form-control-bmz" id="mcr_asucto" value="Consumibles">
        </div>
        <div class="col s12">
          <label>Comentario</label>
          <textarea class="form-control-bmz" id="mcr_coment" style="min-height: 150px;"></textarea>
        </div>
        <div class="col s12 input_envio_c"></div>
        <div class="col s12 table_mcr_files_c"></div>
      </div>
      <div class="row"><div class="col s12">
        <button onclick="envio(0,1)" class="btn_whatsapp"></button>
        <button type="button" class="waves-effect btn-bmz  red btn-envioser" onclick="envio(0,0)">Enviar individual</button>
        
      </div></div>
      <div class="row"><div class="col s12">
        <button onclick="envio(1,1)" class="btn_whatsapp"></button>
        <button type="button" class="waves-effect btn-bmz  red btn-envioser" onclick="envio(1,0)">Enviar Global</button>
        
      </div> </div>
   
    </div>
    <div class="row">
      <div class="col m12" align="right">
        <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
      </div>
    </div>        
</div>

<div id="modal_servicio" class="modal">
    <div class="modal-content">
      <div class="col s12">
        <label>Firma del ejecutivo</label>
        <select class="browser-default form-control-bmz" id="firma_m_s">
          <?php foreach ($perfirma->result() as $itemf) {
            echo '<option value="'.$itemf->personalId.'">'.$itemf->nombre.' '.$itemf->apellido_paterno.' '.$itemf->apellido_materno.' / '.$itemf->email.' / '.$itemf->celular.'</option>';
          }?>
        </select>
      </div>
      <div class="col s12">
        <label>Copia oculta (BBC)</label>
        <select class="browser-default form-control-bmz" id="bbc_m_s"><option value="0"></option>
          <?php foreach ($perfirma->result() as $itemf) {
            echo '<option value="'.$itemf->personalId.'">'.$itemf->nombre.' '.$itemf->apellido_paterno.' '.$itemf->apellido_materno.' / '.$itemf->email.' / '.$itemf->celular.'</option>';
          }?>
        </select>
      </div>
      <div class="col s12">
          <label>Correo envio</label>
          <div class="m_s_contac"></div>
        </div>
      <div class="row">
        <div class="col s12">
          <label>Asunto</label>
          <input class="form-control-bmz" id="ms_asucto2" value="Servicio">
        </div>
        <div class="col s12">
          <label>Comentario</label>
          <textarea class="form-control-bmz" id="ms_coment2" style="min-height: 150px;"></textarea>
        </div>
        <div class="col s12 input_envio_s">
        </div>
        <div class="col s12 table_mcr_files_s"></div>
      </div>
      <div class="row"><div class="col s12 addbtnservicio"></div></div>
 
    </div>
    <div class="row">
      <div class="col m12" align="right">
        <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
      </div>
    </div>        
</div>


    <div id="modal_add_contacto" class="modal">
        <div class="modal-content">
          <form id="form_add_cont">
            <input type="hidden" class="form-control-bmz" id="dc_cliente" name="dc_cliente" required>
            <div class="row">
              <div class="col s6">
                <label for="dc_atencionpara">Atencion para</label>
                <input type="text" class="form-control-bmz" id="dc_atencionpara" name="dc_atencionpara" required>
              </div>
              <div class="col s6">
                <label for="dc_puesto">Puesto</label>
                <input type="text" class="form-control-bmz" id="dc_puesto" name="dc_puesto" >
              </div>
            </div>
            <div class="row">
              <div class="col s6">
                <label for="dc_email">Email</label>
                <input type="email" class="form-control-bmz" id="dc_email" name="dc_email" >
              </div>
              <div class="col s6">
                <label for="dc_telefono">Telefono</label>
                <input type="tel" class="form-control-bmz" id="dc_telefono" name="dc_telefono" >
              </div>
            </div>
            <div class="row">
              <div class="col s6">
                <label for="dc_celular">Celular</label>
                <input type="tel" class="form-control-bmz" id="dc_celular" name="dc_celular" >
              </div>
            </div>
          </form>
          
     
        </div>
        <div class="row">
          <div class="col m12" align="right"><button class="btn-bmz cyan waves-effect save_contacto" type="button" onclick="save_contacto()">Guardar</button>
            <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
          </div>
        </div>        
    </div>