SELECT * FROM(
    SELECT asig.contratoId,asigd.fecha,altrent.id_renta,arpre.prefId,fpre.facturaId,arpre.factura_renta,arpre.factura_excedente,COUNT(arpred.prefdId) as total, sum(arpred.statusfacturar) as statusfacturar, SUM(arpred.statusfacturae) as statusfacturae
FROM asignacion_ser_contrato_a as asig
INNER JOIN contrato as cont on cont.idcontrato=asig.contratoId
INNER JOIN asignacion_ser_contrato_a_e as asigd on asigd.asignacionId=asig.asignacionId
LEFT JOIN alta_rentas as altrent on altrent.idcontrato=asig.contratoId
LEFT JOIN alta_rentas_prefactura as arpre on arpre.id_renta=altrent.id_renta AND arpre.periodo_inicial<=asigd.fecha and arpre.periodo_final>=asigd.fecha
LEFT JOIN alta_rentas_prefactura_d as arpred on arpre.prefId=arpred.prefId
LEFT JOIN factura_prefactura as fpre on fpre.prefacturaId=arpre.prefId
WHERE cont.estatus!=0 AND asig.activo=1 AND asigd.activo=1 AND asigd.status=2 AND 
(
    arpre.prefId IS null OR (arpre.factura_renta=0 or arpre.factura_excedente=0 ) 
)
GROUP BY asig.contratoId,asigd.fecha
) as datos WHERE prefId IS null or (statusfacturar=0 or statusfacturae=0)  
ORDER BY `datos`.`fecha` DESC





