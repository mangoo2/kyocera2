            <div id="modal_confi_av" class="modal"  >
                <div class="modal-content ">
                    <h4>Confirmación</h4>
                    <div class="col s12 m12 l6">
                        <div class="row" align="center">
                           <h5>¿Desea eliminar la notificación de <span class="tipo_equipo"></span> si le da aceptar se eliminara la notificación?</h5>
                        </div>
                    </div>
                    <input type="hidden" id="asiId">
                    <input type="hidden" id="tipo">
                    <div class="modal-footer">
                        <a href="#!" class="modal-action modal-close waves-effect waves-red cyan btn-flat" style="color: white;" onclick="eliminar_notificacion()">Aceptar</a>
                        <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
                    </div>
                </div>
            </div>
        </div>
      <!-- END WRAPPER -->
    </div>
    <!-- END MAIN -->
    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- START FOOTER -->
    
    <!--materialize js-->
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/js/materialize.min.js"></script>
    <!--scrollbar-->
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <!--plugins.js - Some Specific JS codes for Plugin Settings-->
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/js/plugins.js"></script>
    <!--custom-script.js - Add your own theme custom JS--
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/js/custom-script.js"></script>-->
    <!-- chartist -->
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/jquery-validation/jquery.validate.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/jquery-validation/additional-methods.min.js"></script>
    <!--plugins.js - Some Specific JS codes for Plugin Settings-->
    <!--<script type="text/javascript" src="<?php echo base_url(); ?>app-assets/js/plugins.js"></script>-->
    <!--form-validation.js - Page Specific JS codes-->
    <!--<script type="text/javascript" src="<?php echo base_url(); ?>app-assets/js/scripts/form-validation.js"></script>-->
    <!--sweetalert -->
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/sweetalert/dist/sweetalert.min.js"></script>
    <!--extra-components-sweetalert.js - Some Specific JS-->
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/js/scripts/extra-components-sweetalert.js"></script>
    <!--<script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/data-tables/js/jquery.dataTables.min.js"></script>-->
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/data-tables/js/jquery.dataTables.min.1.11.4.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/data-tables/js/dataTables.responsive.min.2.2.9.js"></script>
    <!-- dropify -->
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/dropify/js/dropify.min.js"></script>
    <!-- jquery confirm -->
    <script src="<?php echo base_url(); ?>app-assets/vendors/confirm/jquery-confirm.min.js"></script>
    <!-- JS Export -->
    <script src="<?php echo base_url(); ?>app-assets/vendors/exportJS/export.js"></script>
    <script src="<?php echo base_url(); ?>app-assets/vendors/exportJS/export.min.js"></script>
    <!-- Full Calendar -->
    <!--<script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/fullcalendar/lib/jquery-ui.custom.min.js"></script>-->
    <!--
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/fullcalendar/lib/moment.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/fullcalendar/js/fullcalendar.min.js"></script>
    -->
   <!--
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/datatable-Editable/js/dataTables.editor.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/datatable-Editable/js/dataTables.editor.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/datatable-Editable/js/editor.bootstrap.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/datatable-Editable/js/editor.bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/datatable-Editable/js/editor.bootstrap4.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/datatable-Editable/js/editor.bootstrap4.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/datatable-Editable/js/editor.foundation.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/datatable-Editable/js/editor.foundation.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/datatable-Editable/js/editor.jqueryui.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/datatable-Editable/js/editor.jqueryui.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/datatable-Editable/js/editor.semanticui.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/datatable-Editable/js/editor.semanticui.min.js"></script>
-->
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/data-tables/js/dataTables.select.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/data-tables/js/dataTables.buttons.min.js"></script>

    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/js/jquery.jeditable.js"></script>

    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/js/chosen.jquery.js" ></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/data-tables/js/input.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/data-tables/js/select.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/toastr/toastr.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/plugins/toastr/toastr.css?v=1">
    <script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/loading/jquery.loading.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/plugins/loading/demo.css">
    <script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/MaskFocus/MaskedPassword.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/mask/jquery.mask.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/mask/jquery.passwordify.js"></script>

    <script type="text/javascript" src="<?php echo base_url(); ?>public/js/servicioscal.js?v=<?php echo date('YmdGis');?>"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>public/js/notificacion.js?v=<?php echo date('Ymd');?>"></script>
    <style type="text/css">
        .paginate_input,.paginate_select{width: 100px !important;}
        .paginate_input{margin: 0px;display: unset;text-align: center;}
        <?php if(isset($MenusubId)){ ?>
            ._su_m_<?php echo $MenusubId;?>{
                background: #e8e2e2db;
            }
        <?php } ?>
    </style>


    <script type="text/javascript">
        var base_url = $('#base_url').val();
        function eliminar_notifi(id,tipo){
            $('#modal_confi_av').modal();
            $('#modal_confi_av').modal('open');
            if(tipo==1){
                $('.tipo_equipo').html('Consumible');
            }else{
                $('.tipo_equipo').html('Refacción');
            }
            $('#asiId').val(id);
            $('#tipo').val(tipo);

        }
        function eliminar_notificacion(){
            var ida = $('#asiId').val();
            var tp = $('#tipo').val();   
            var ax_c = $('.eux_can_asig').text();
            var ax_rest = parseFloat(ax_c);
            var resta_n = parseFloat(ax_rest - 1);
            
            $.ajax({
                type:'POST',
                url: base_url+'index.php/Main/update_cotizacion',
                data: {
                    id:$('#asiId').val(),
                    tipo:$('#tipo').val()
                },
                success:function(data){
                    swal({ title: "Éxito",
                        text: "Se elimino de las notificaciones",
                        type: 'success',
                        showCancelButton: false,
                        allowOutsideClick: false,
                    });
                    setTimeout(function() {
                        $('.eux_can_asig').html(resta_n);
                        $('.eux_can_asig_p').html(resta_n);
                    }, 500);
                    $('.avc_'+ida+'_'+tp).remove();
                }
            });
            
        }
    </script>
    <!--ckeditor js--
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/js/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/js/ckeditor/adapters/jquery.js"></script> -->  
  </body>
</html>
<style type="text/css">
    .menumovil{display: none;}
    @media only screen and (max-width: 600px) {
        .menumovil{background: white;display: block;bottom: 0;position: fixed;width: 100%;box-shadow: 0 2px 2px 0 rgb(0 0 0 / 14%), 0 1px 5px 0 rgb(0 0 0 / 12%), 0 3px 1px -2px rgb(0 0 0 / 20%); }
        .menumovil i{font-size: 40px;}
        .card-panel{margin-bottom: 55px;}
        .divmenucontect{text-align: center;}
    }
</style>
<div class="navbar-fixed menumovil">
    <div class="row">
        <div class="col s4 divmenucontect">
            <i class="fas fa-chevron-left" onclick="javascript:history.back()"></i>
        </div>
        <div class="col s4 divmenucontect">
            <i class="fas fa-home" onclick="$(location).attr('href','<?php echo base_url();?>')"></i>
        </div>
        <div class="col s4 divmenucontect">
            <i class="material-icons" onclick="$('.sidebar-collapse').click()">menu</i>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function($) {
        $(".mode").on("click", function () {
            $('.mode i').toggleClass("fa-moon").toggleClass("fa-lightbulb");
            // $('.mode-sun').toggleClass("show")
            $('body').toggleClass("dark-only");
            var color = $(this).attr("data-attr");
            localStorage.setItem('body', 'dark-only');
            $.ajax({
                type:'POST',
                url: base_url+'index.php/Main/darkonly',
                data: {
                    id:0
                },
                success:function(data){
                }
            });
        });
        <?php 
            // igual se agrego en el footer
            $fecha = date('Y-m-d');
            $idpersonal = $this->session->userdata('idpersonal');
            $res_asi = $this->ModeloSession->verifi_asistencia($idpersonal,$fecha);
            if($res_asi->num_rows()>0){
              $registro='activo';
              
            }else{
                $registro='';
                if($idpersonal==1 || $idpersonal==17 || $idpersonal==18 || $idpersonal==9){
              ?>
                //$('body').loading({theme: 'dark',message: 'Favor de registrar entrada...'});
              <?php
                }else{
                    ?>$('body').loading({theme: 'dark',message: 'Favor de registrar entrada...'});<?php
                }
            }
        ?>
    });
</script>
<script type="text/javascript">
    /*
    var idper = $('#idper').val();
    var base_url = $('#base_url').val();
    var idperf = $('#idperf').val();
        if ('serviceWorker' in navigator) {
          window.addEventListener('load', function() {
            navigator.serviceWorker.register('<?php echo base_url();?>service-worker.js?idper='+idper+'&idperf='+idperf)
            .then(function(registration) {
              // Si es exitoso
              console.log('SW registrado correctamente');
            }, function(err) {
              // Si falla
              console.log('SW fallo', err);
            });
          });
        }
        */
    </script>
<?php include('inchatfot.php'); ?> 