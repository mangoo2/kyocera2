<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sistema extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Login_model');
        $this->load->model('ModeloCatalogos');
        $this->load->model('Configuraciones_model');
        $this->fechahoy = date('Y-m-d');
        if($this->session->userdata('logeado')==true){
            $this->perfilid =$this->session->userdata('perfilid');
            $this->idpersonal = $this->session->userdata('idpersonal');
            $this->usuarioid = $this->session->userdata('usuarioid');
        }else{
            redirect('login'); 
        }
    }
    public function index(){
        $perfilview=$this->perfilid;
        $data['resultzonas']=$this->ModeloCatalogos->view_session_rutas();
        $data['resultstec']=$this->Configuraciones_model->view_session_searchTecnico(); 
        $data['bodegas']=$this->ModeloCatalogos->view_session_bodegas();

        if ($perfilview==1) {
            redirect('/main/inicio');
            //redirect('/Configuracionrentas/servicios');
        }elseif ($perfilview==2) {
            redirect('/main/inicio');
        }elseif ($perfilview==3) {
            redirect('/main/inicio');
        }elseif ($perfilview==4) {
            redirect('/main/inicio');
        }elseif ($perfilview==5) {
            //redirect('/Configuracionrentas/servicios');
            redirect('/TableroControl/viewt');
        }elseif ($perfilview==6) {
            redirect('/main/inicio');
        }elseif ($perfilview==7) {
            redirect('/main/inicio');
        }elseif ($perfilview==8) {
            redirect('/main/inicio');
        }elseif ($perfilview==12) {
            //redirect('/Configuracionrentas/servicios');
            redirect('/TableroControl/viewt');
        }else{
            redirect('/Login');
        }
    }
    function solicitarpermiso($cliente=0){
        $pass = $this->input->post('pass');
        log_message('error',"solicitarpermiso pass: '$pass'");
        $permiso=0;
        
        $permiso = $this->Login_model->permisogeneral($pass);
        

        if($permiso==0 and $cliente==597){ //597 es el id del cliente dhl
            if($pass=='DHL2023' or $pass=='dhl2023'){
                $permiso=1;
                $this->ModeloCatalogos->Insert('bitacora',array('contenido'=>'Se realizo una autorizacion con el con DHL2023','nombretabla'=>'Permisos','idtable'=>0,'tipo'=>'Solicitud permiso','personalId'=>$this->idpersonal));
            }
        }
        if($permiso==0 and $cliente>0){
            $result=$this->ModeloCatalogos->getselectwheren('clientes',array('id'=>$cliente,'pass_estatus'=>1,'pass_vigencia >'=>$this->fechahoy));
            foreach ($result->result() as $item) {
                if($item->pass_value==$pass){
                    $permiso=1;
                }
            }
        }
        echo $permiso;
    }
    function solicitarpermiso_l($cliente=0){
        $pass = $this->input->post('pass');
        $per = $this->input->post('per');
        $per2 = $this->input->post('per2');
        log_message('error',"Pass sl: '$pass' ");
        $permiso=0;
        
        $permiso = $this->Login_model->permisogeneral_l($pass,$per,$per2);
        

        if($permiso==0 and $cliente==597){ //597 es el id del cliente dhl
            if($pass=='DHL2023' or $pass=='dhl2023'){
                $permiso=1;
                $this->ModeloCatalogos->Insert('bitacora',array('contenido'=>'Se realizo una autorizacion con el con DHL2023','nombretabla'=>'Permisos','idtable'=>0,'tipo'=>'Solicitud permiso','personalId'=>$this->idpersonal));
            }
        }
        if($permiso==0 and $cliente>0){
            $result=$this->ModeloCatalogos->getselectwheren('clientes',array('id'=>$cliente,'pass_estatus'=>1,'pass_vigencia >'=>$this->fechahoy));
            foreach ($result->result() as $item) {
                if($item->pass_value==$pass){
                    $permiso=1;
                }
            }
        }
        echo $permiso;
    }
    function permisotempnew(){
        $pass = $this->input->post('pass');
        log_message('error',"permisotempnew pass: '$pass'");
        $datosUsuario = array(
            'clavestemp' => password_hash($pass, PASSWORD_BCRYPT),
            'clavestemp_text'=>$pass
        );
        $this->ModeloCatalogos->updateCatalogo('usuarios',$datosUsuario,array('UsuarioID'=>$this->usuarioid));

    }
    function seleccionsistema(){
        $params = $this->input->post();
        $tipo=$params['tipo'];
        if($tipo==0){
            $_SESSION['opcionweb']=1;
        }
        if($tipo==1){
            $_SESSION['opcionweb']=1;
            $_SESSION['perfilid']=11;
               
        }
    }
}
?>