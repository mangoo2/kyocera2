var base_url = $('#base_url').val();
var table;
var tipovselected;
$(document).ready(function(){
	$('.chosen-select').chosen({width: "100%"});
	$('.modal').modal();
	table = $('#tabla_asignar').DataTable();
	$('.minimodal').click(function(event) {
		$('#modalAsignacion').modal('open');
	}); 
	datos_equipos();
});
function seleccionconsulta(){
    limpiarmodales();
	var consu = $('#consulta option:selected').val();
    var tipo = $('input[name=tipoasi]:checked').val();
    console.log('consu:'+consu+' tipo:'+tipo);
	if (consu==0) {
        $(".selectedradio").show( "slow" );
        if (tipo==1) {
            datos_equipos();
            $('.cambiotitulo').html('# Venta');
        }else if (tipo==2) {
            datos_equipos_vd();
            $('.cambiotitulo').html('# Venta');
        }else{
            datos_equiposr();
            $('.cambiotitulo').html('# Renta');
        }
        $('.asignarrmultiple').hide('show');
		
	}else if (consu==1) {
        $(".selectedradio").show( "slow" );
        if (tipo==1) {
            $('.cambiotitulo').html('# Venta');
            datos_accesorios();
            console.log('datos_accesorios');
        }else if (tipo==2) {
            $('.cambiotitulo').html('# Venta');
            datos_accesorios_vd();
            console.log('datos_accesorios_vd');
        }else{
            $('.cambiotitulo').html('# Renta');
            datos_accesoriosr();
            console.log('datos_accesoriosr');
        }
		$('.asignarrmultiple').hide('show');
	}else if (consu==2) {
        $(".selectedradio").hide( "slow" );
        //$("#tipoasignacion1").prop('checked', true);
		
        $('.cambiotitulo').html('# Venta');
        if (tipo==1) {
            datos_refacciones();
            console.log('datos_refacciones');
        }else{
            datos_refacciones_vd();
            console.log('datos_refacciones_vd');
        }
        $('.asignarrmultiple').show('show');
	}
    table.state.clear();
}
function datos_equipos(){
    var fecha = $('#fechainicio').val();
    var tipoc = $('#tipoc option:selected').val();
    table.destroy();
    table=$("#tabla_asignar").DataTable({
        pagingType: 'input',
        stateSave: true,
        "bProcessing": true,
        "serverSide": true,
        search: {
                return: true
            },
        "ajax": {
           "url": base_url+"index.php/AsignacionesSeries/getData_asignacion_e",
           type: "post",
           "data": {
                'fecha':fecha,
                'tipoc':tipoc
            },
            error: function(){
               $("#tabla_asignar").css("display","none");
            }
        },
        "columns": [
            {"data": "id"},
            {"data": "idVenta",
                render: function(data,type,row){
                    var html='';
                    if(row.combinadaId>0){
                        //html=row.combinadaId+'<!--'+row.idVenta+'-->';
                        if(row.equipo>0){
                            html=row.equipo+' <!--e '+row.idVentas+'-->';
                        }else if(row.consumibles>0){
                            html=row.consumibles+' <!--c'+row.idVentas+'-->';
                        }else if(row.refacciones>0){
                            html=row.refacciones+' <!--r'+row.idVentas+'-->';
                        }
                    }else{
                        html=row.idVenta;
                    }
                    return html;
                }
            },
            {"data": null,"visible": false},
            {"data": "nombre"},
            {"data": "bodega"},
            {"data": "cantidad"},
            {"data": "modelo"},
            {"data": "noparte"},
            {"data": "empresa"},
            {"data": "reg"},
            {"data": null,
                render: function(data,type,row){ 
                    if(row.serie_estatus == 0){
                        var html='<a class="btn-bmz waves-effect waves-light cyan" onclick="asignare('+row.id+','+row.cantidad+','+row.idEquipo+','+row.serie_bodega+','+row.tipov+')" >Asignar</a>';
                            html+='<a class="btn-bmz waves-effect waves-light cyan" onclick="ocultarsolicitud('+row.id+',1)" title="Ocultar Solicitud"><i class="fas fa-eye-slash fa-fw"></i></a>';
                    }else if(row.prefactura == 0){
                        var html='<a class="btn-bmz waves-effect waves-light green darken-1" onclick="editarasignare('+row.id+','+row.cantidad+','+row.idEquipo+','+row.serie_bodega+')">Editar</a>';
                    }else{
                        var html='<a class="btn-bmz waves-effect waves-light green darken-1">Asignadas</a>';
                    }
                    return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[15,20,25, 50, 100], [15,20,25, 50,100]],
    }).on('draw',function(){
        $('.dataTables_filter').append('<i class="fa fa-search buttonsearch" onclick="searchtable_v()"></i>')

    });
}
function datos_accesorios(){
    var fecha = $('#fechainicio').val();
    var tipoc = $('#tipoc option:selected').val();
    table.destroy();
    table=$("#tabla_asignar").DataTable({
        pagingType: 'input',
        "bProcessing": true,
        "serverSide": true,
        stateSave: true,
        search: {
                return: true
            },
        "ajax": {
           "url": base_url+"index.php/AsignacionesSeries/getData_asignacion_a",
           type: "post",
           "data": {
                'fecha':fecha,
                'tipoc':tipoc
            },
            error: function(){
               $("#tabla_asignar").css("display","none");
            }
        },
        "columns": [
            {"data": "id_accesoriod"},
            {"data": "id_venta",
                render: function(data,type,row){
                    var html='';
                    if(row.combinadaId>0){
                        //html=row.combinadaId+'<!--'+row.id_venta+'-->';
                        if(row.equipo>0){
                            html=row.equipo+'<!--e '+row.idVentas+'-->';
                        }else if(row.consumibles>0){
                            html=row.consumibles+'<!--c '+row.idVentas+'-->';
                        }else if(row.refacciones>0){
                            html=row.refacciones+'<!--r '+row.idVentas+'-->';
                        }
                    }else{
                        html=row.id_venta;
                    }
                    return html;
                }
            },
            {"data": null,"visible": false},
            {"data": "vendedor"},
            {"data": "bodega"},
            {"data": "cantidad"},
            {"data": "nombre"},
            {"data": "no_parte"},
            {"data": "empresa"},
            {"data": "reg"},
            {"data": null,
                render: function(data,type,row){ 
                    if(row.serie_estatus == 0){
                        var html='<a class="btn-bmz waves-effect waves-light cyan" onclick="asignara('+row.id_accesoriod+','+row.cantidad+','+row.id_accesorio+','+row.serie_bodega+','+row.tipov+')">Asignar</a>';
                            html+='<a class="btn-bmz waves-effect waves-light cyan" onclick="ocultarsolicitud('+row.id_accesoriod+',2)" title="Ocultar Solicitud"><i class="fas fa-eye-slash fa-fw"></i></a>';
                    }else if(row.prefactura == 0){
                        var html='<a class="btn-bmz waves-effect waves-light green darken-1" onclick="editarasignara('+row.id_accesoriod+','+row.cantidad+','+row.id_accesorio+','+row.serie_bodega+')">Editar</a>';
                    }else{
                        var html='<a class="btn-bmz waves-effect waves-light green darken-1">Asignadas</a>';
                    }
                    return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[15,20,25, 50, 100], [15,20,25, 50,100]],
    }).on('draw',function(){
        $('.dataTables_filter').append('<i class="fa fa-search buttonsearch" onclick="searchtable_v()"></i>')

    });
}
function datos_equiposr(){
    var fecha = $('#fechainicio').val();
    var tipoc = $('#tipoc option:selected').val();
    table.destroy();
    table=$("#tabla_asignar").DataTable({
        pagingType: 'input',
        "bProcessing": true,
        "serverSide": true,
        stateSave: true,
        search: {
                return: true
            },
        "ajax": {
           "url": base_url+"index.php/AsignacionesSeries/getData_asignacion_er",
           type: "post",
           "data": {
                'fecha':'',
                'tipoc':tipoc
            },
            error: function(){
               $("#tabla_asignar").css("display","none");
            }
        },
        "columns": [
            {"data": "id"},
            {"data": "idRenta"},
            {"data": "folio"},
            {"data": "nombre"},
            {"data": "bodega"},
            {"data": "cantidad"},
            {"data": "modelo"},
            {"data": "noparte"},
            {"data": "empresa"},
            {"data": "reg"},
            {"data": null,
                render: function(data,type,row){ 
                    if(row.serie_estatus == 0){
                        var html='<a class="btn-bmz waves-effect waves-light cyan" onclick="asignarer('+row.id+','+row.cantidad+','+row.idEquipo+','+row.serie_bodega+','+row.idCliente+')" >Asignar</a>';
                        html+='<a class="btn-bmz waves-effect waves-light cyan" onclick="ocultarsolicitud('+row.id+',4)" title="Ocultar Solicitud"><i class="fas fa-eye-slash fa-fw"></i></a>';
                    }else if(row.prefactura == 0){
                        var html='<a class="btn-bmz waves-effect waves-light green darken-1" onclick="editarasignarer('+row.id+','+row.cantidad+','+row.idEquipo+','+row.serie_bodega+')">Editar</a>';
                    }else{
                        var html='<a class="btn-bmz waves-effect waves-light green darken-1">Asignadas</a>';
                    }
                    return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
    }).on('draw',function(){
        $('.dataTables_filter').append('<i class="fa fa-search buttonsearch" onclick="searchtable_v()"></i>')

    });
}
function datos_accesoriosr(){
    var fecha = $('#fechainicio').val();
    var tipoc = $('#tipoc option:selected').val();
    table.destroy();
    table=$("#tabla_asignar").DataTable({
        pagingType: 'input',
        "bProcessing": true,
        "serverSide": true,
        stateSave: true,
        search: {
                return: true
            },
        "ajax": {
           "url": base_url+"index.php/AsignacionesSeries/getData_asignacion_ar",
           type: "post",
           "data": {
                'fecha':'',
                'tipoc':tipoc
            },
            error: function(){
               $("#tabla_asignar").css("display","none");
            }
        },
        "columns": [
            {"data": "id_accesoriod"},
            {"data": "idrentas"},
            {"data": "folio"},
            {"data": "vendedor"},
            {"data": "bodega"},
            {"data": "cantidad"},
            {"data": "nombre"},
            {"data": "no_parte"},
            {"data": "empresa"},
            {"data": "reg"},
            {"data": null,
                render: function(data,type,row){ 
                    if(row.serie_estatus == 0){
                        var html='<a class="btn-bmz waves-effect waves-light cyan" onclick="asignara_renta('+row.id_accesoriod+','+row.cantidad+','+row.id_accesorio+','+row.serie_bodega+')">Asignar</a>';
                        html+='<a class="btn-bmz waves-effect waves-light cyan" onclick="ocultarsolicitud('+row.id_accesoriod+',5)" title="Ocultar Solicitud"><i class="fas fa-eye-slash fa-fw"></i></a>';
                    }else if(row.prefactura == 0){
                        var html='<a class="btn-bmz waves-effect waves-light green darken-1" onclick="editarasignararentas('+row.id_accesoriod+','+row.cantidad+','+row.id_accesorio+','+row.serie_bodega+')">Editar</a>';
                    }else{
                        var html='<a class="btn-bmz waves-effect waves-light green darken-1">Asignadas</a>';
                    }
                    return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[15,20,25, 50, 100], [15,20,25, 50,100]],
    }).on('draw',function(){
        $('.dataTables_filter').append('<i class="fa fa-search buttonsearch" onclick="searchtable_v()"></i>')

    });
}
function datos_refacciones(){
    var fecha = $('#fechainicio').val();
    var tipoc = $('#tipoc option:selected').val();
    table.destroy();
    table=$("#tabla_asignar").DataTable({
        pagingType: 'input',
        "bProcessing": true,
        "serverSide": true,
        stateSave: true,
        search: {
                return: true
            },
        "ajax": {
           "url": base_url+"index.php/AsignacionesSeries/getData_asignacion_r",
           type: "post",
           "data": {
                'fecha':fecha,
                'tipoc':tipoc
            },
            error: function(){
               $("#tabla_asignar").css("display","none");
            }
        },
        "columns": [
            {"data": "id"},
            {"data": "idVentas",
                render: function(data,type,row){
                    var html='';
                    if(row.combinadaId>0){
                        //html=row.combinadaId+'<!--'+row.idVentas+'-->';
                        if(row.equipo>0){
                            html=row.equipo+'<!--e '+row.idVentas+'-->';
                        }else if(row.consumibles>0){
                            html=row.consumibles+'<!--c '+row.idVentas+'-->';
                        }else if(row.refacciones>0){
                            html=row.refacciones+'<!--r '+row.idVentas+'-->';
                        }
                    }else{
                        html=row.idVentas;
                    }
                    return html;
                }
            },
            {"data": null,"visible": false},
            {"data": "nombre"},
            {"data": "bodega"},
            {"data": "piezas"},
            {"data": "modelo"},
            {"data": "codigo"},
            {"data": "empresa"},
            {"data": "reg"},
            {"data": null,
                render: function(data,type,row){ 
                    var html='';
                    html+='<div class="div_btn">';
                    if(row.serie_estatus == 0){
                        html+='<a class="btn-bmz waves-effect waves-light cyan" onclick="asignarr('+row.id+','+row.piezas+','+row.idRefaccion+','+row.serie_bodega+','+row.tipov+')">Asignar</a>';
                        html+='<a class="btn-bmz waves-effect waves-light cyan" onclick="ocultarsolicitud('+row.id+',3)" title="Ocultar Solicitud"><i class="fas fa-eye-slash fa-fw"></i></a>';

                        html+='<input type="checkbox" class="filled-in refaccion_checked" id="refaccion_'+row.id+'"';
                            html+=' data-id="'+row.id+'" ';
                            html+=' data-piezas="'+row.piezas+'" ';
                            html+=' data-refaccion="'+row.idRefaccion+'" ';
                            html+=' data-bodega="'+row.serie_bodega+'" data-bodeganame="'+row.bodega+'" ';
                            html+=' data-emp="'+row.tipov+'" data-refname="'+row.modelo+'"';
                            html+='>';
                            html+='<label for="refaccion_'+row.id+'"></label>';

                    }else if(row.prefactura == 0){
                        html+='<a class="btn-bmz waves-effect waves-light green darken-1" onclick="editarasignarr('+row.id+','+row.piezas+','+row.idRefaccion+','+row.serie_bodega+')">Editar</a>';
                    }else{
                        html+='<a class="btn-bmz waves-effect waves-light green darken-1">Asignadas</a>';
                    }
                    html+='</div>';

                    return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[15,20,25, 50, 100], [15,20,25, 50,100]],
    }).on('draw',function(){
        $('.dataTables_filter').append('<i class="fa fa-search buttonsearch" onclick="searchtable_v()"></i>')

    });
}
function searchtable_v(){
    var contenedor = document.getElementById('tabla_asignar_filter');

    // Selecciona el input dentro del contenedor
    var input = contenedor.querySelector('input.form-control-bmz');

    // Obtén el valor del input
    var searchv = input.value;

    // Muestra el valor en la consola
    console.log(searchv);
    //var searchv =$("input[type=search]").val();
    table.search(searchv).draw();
}
function asignarer(row,cantidad,equipo,bo,idcliente){
    tipovselected=1;
    $('.addseriesequipos_rentas_selected').html('');
    limpiarmodales();
	$.ajax({
        type:'POST',
        url: base_url+'AsignacionesSeries/seriese_renta',
        data: {
            equipo: equipo,
            bod: bo,
            cantidad:cantidad,
            idrow:row,
            idcliente:idcliente
        },
        async: false,
        statusCode:{
            404: function(data){
                swal("Error", "404", "error");
            },
            500: function(){
                swal("Error", "500", "error"); 
            }
        },
        success:function(data){
            $('#modal_series_rentas').modal();
            $('#modal_series_rentas').modal('open');
            $('.addseriesequipos_rentas').html(data);
            $('.chosen-select').chosen({width: "100%"});
            bloqueoseries();
        }
    });
}
function asignarer_new(row,cantidad,equipo,idcliente){
    var bo = $('#newbodegaer option:selected').val();
    $.ajax({
        type:'POST',
        url: base_url+'AsignacionesSeries/seriese_renta/'+bo,
        data: {
            equipo: equipo,
            bod: bo,
            cantidad:cantidad,
            idrow:row,
            idcliente:idcliente
        },
        async: false,
        statusCode:{
            404: function(data){
                swal("Error", "404", "error");
            },
            500: function(){
                swal("Error", "500", "error"); 
            }
        },
        success:function(data){
            //$('#modal_series_rentas').modal();
            //$('#modal_series_rentas').modal('open');
            $('.addseriesequipos_rentas').html(data);
            $('.chosen-select').chosen({width: "100%"});
            bloqueoseries();
        }
    });
}
// funcion para rentas 
function asignare(row,cantidad,equipo,bo,tipov){
    tipovselected=tipov;
    $('.addseriesequipos_selected').html('');
    limpiarmodales();
    $('.aceptar_series_e').prop( "disabled", false);
    $.ajax({
        type:'POST',
        url: base_url+'AsignacionesSeries/seriese',
        data: {
            equipo: equipo,
            bod: bo,
            cantidad:cantidad,
            idrow:row,
        },
        async: false,
        statusCode:{
            404: function(data){
                swal("Error", "404", "error");
            },
            500: function(){
                swal("Error", "500", "error"); 
            }
        },
        success:function(data){
            //$('#modal_series_e').modal();
            $('#modal_series_e').modal('open');
            $('.addseriesequipos').html(data);
            $('.chosen-select').chosen({width: "100%"});
        }
    });
    bloqueoseries();
}
function asignare_new(row,cantidad,equipo){
    var bo=$('#newbodegae option:selected').val();
    limpiarmodales();
   $.ajax({
        type:'POST',
        url: base_url+'AsignacionesSeries/seriese/'+bo,
        data: {
            equipo: equipo,
            bod: bo,
            cantidad:cantidad,
            idrow:row,
        },
        async: false,
        statusCode:{
            404: function(data){
                swal("Error", "404", "error");
            },
            500: function(){
                swal("Error", "500", "error"); 
            }
        },
        success:function(data){
            //$('#modal_series_e').modal();
            //$('#modal_series_e').modal('open');
            $('.addseriesequipos').html(data);
            $('.chosen-select').chosen({width: "100%"});
            bloqueoseries();
        }
    }); 
}
// ==
function asignara(row,cantidad,accesorio,bo,tipov){
    tipovselected=tipov;
    limpiarmodales();
	$.ajax({
        type:'POST',
        url: base_url+'AsignacionesSeries/seriesa',
        data: {
            accesorio: accesorio,
            bod: bo,
            cantidad:cantidad,
            idrow:row,
        },
        async: false,
        statusCode:{
            404: function(data){
                swal("Error", "404", "error");
            },
            500: function(){
                swal("Error", "500", "error"); 
            }
        },
        success:function(data){
            $('#modal_series_a').modal();
            $('#modal_series_a').modal('open');
            $('.addseriesaccesorios').html(data);
            $('.chosen-select').chosen({width: "100%"});
            bloqueoseries();
            varidarcantr();
        }
    });
}
function asignara_new(row,cantidad,accesorio){
    var bo = $('#newbodegaac option:selected').val();
    limpiarmodales();
    $.ajax({
        type:'POST',
        url: base_url+'AsignacionesSeries/seriesa/'+bo,
        data: {
            accesorio: accesorio,
            bod: bo,
            cantidad:cantidad,
            idrow:row,
        },
        async: false,
        statusCode:{
            404: function(data){
                swal("Error", "404", "error");
            },
            500: function(){
                swal("Error", "500", "error"); 
            }
        },
        success:function(data){
            //$('#modal_series_a').modal();
            //$('#modal_series_a').modal('open');
            $('.addseriesaccesorios').html(data);
            $('.chosen-select').chosen({width: "100%"});
            bloqueoseries();
        }
    });
}
// accesorios ==
function asignara_renta(row,cantidad,accesorio,bo){
    tipovselected=1;
    limpiarmodales();
    $.ajax({
        type:'POST',
        url: base_url+'AsignacionesSeries/seriesarentas',
        data: {
            accesorio: accesorio,
            bod: bo,
            cantidad:cantidad,
            idrow:row,
        },
        async: false,
        statusCode:{
            404: function(data){
                swal("Error", "404", "error");
            },
            500: function(){
                swal("Error", "500", "error"); 
            }
        },
        success:function(data){
            $('#modal_series_a_rentas').modal();
            $('#modal_series_a_rentas').modal('open');
            $('.addseriesaccesorios_rentas').html(data);
            $('.chosen-select').chosen({width: "100%"});
            bloqueoseries();
        }
    });
}
function asignara_renta_new(row,cantidad,accesorio){
    var bo = $('#newbodegaacr option:selected').val();
    limpiarmodales();
    $.ajax({
        type:'POST',
        url: base_url+'AsignacionesSeries/seriesarentas/'+bo,
        data: {
            accesorio: accesorio,
            bod: bo,
            cantidad:cantidad,
            idrow:row,
        },
        async: false,
        statusCode:{
            404: function(data){
                swal("Error", "404", "error");
            },
            500: function(){
                swal("Error", "500", "error"); 
            }
        },
        success:function(data){
            //$('#modal_series_a_rentas').modal();
            //$('#modal_series_a_rentas').modal('open');
            $('.addseriesaccesorios_rentas').html(data);
            $('.chosen-select').chosen({width: "100%"});
            bloqueoseries();
        }
    });
}
//
function asignarr(row,cantidad,refaccion,bo,tipov){
    tipovselected=tipov;
    limpiarmodales();
	$.ajax({
        type:'POST',
        url: base_url+'AsignacionesSeries/seriesr',
        data: {
            refaccion: refaccion,
            bod: bo,
            cantidad:cantidad,
            idrow:row,
        },
        async: false,
        statusCode:{
            404: function(data){
                swal("Error", "404", "error");
            },
            500: function(){
                swal("Error", "500", "error"); 
            }
        },
        success:function(data){
            $('#modal_series_r').modal();
            $('#modal_series_r').modal('open');
            $('.addseriesrefacciones').html(data);
            $('.chosen-select').chosen({width: "100%"});
            varidarcantr();
            $('#newbodegaarr').val(bo);
            $('#newbodegaarr').trigger("chosen:updated");
            bloqueoseries();
        }
    });
}
function asignarr_new(row,cantidad,refaccion){
    var bo = $('#newbodegaarr option:selected').val();
    $.ajax({
        type:'POST',
        url: base_url+'AsignacionesSeries/seriesr/'+bo,
        data: {
            refaccion: refaccion,
            bod: bo,
            cantidad:cantidad,
            idrow:row,
        },
        async: false,
        statusCode:{
            404: function(data){
                swal("Error", "404", "error");
            },
            500: function(){
                swal("Error", "500", "error"); 
            }
        },
        success:function(data){
            //$('#modal_series_r').modal();
            //$('#modal_series_r').modal('open');
            $('.addseriesrefacciones').html(data);
            $('.chosen-select').chosen({width: "100%"});
            varidarcantr();
            bloqueoseries();
        }
    });
}
function editarasignare(row,cantidad,equipo,bo){
    $.ajax({
        type:'POST',
        url: base_url+'AsignacionesSeries/seriesasignadase',
        data: {
            equipo: row
        },
        async: false,
        statusCode:{
            404: function(data){
                swal("Error", "404", "error");
            },
            500: function(){
                swal("Error", "500", "error"); 
            }
        },
        success:function(data){
            $('.newoseriesequipos').html(data);
        }
    });
    $.ajax({
        type:'POST',
        url: base_url+'AsignacionesSeries/seriese',
        data: {
            equipo: equipo,
            bod: bo,
            cantidad:cantidad,
            idrow:row,
        },
        async: false,
        statusCode:{
            404: function(data){
                swal("Error", "404", "error");
            },
            500: function(){
                swal("Error", "500", "error"); 
            }
        },
        success:function(data){
            $('#modal_series_editar_e').modal();
            $('#modal_series_editar_e').modal('open');
            $('.newseriesequipos').html(data);
            $('.cant_requ_div').remove();
        }
    });
    $(".checkproducto_o").attr("type","radio");
    $(".checkproducto").attr("type","radio");
}
// rentas
function editarasignarer(row,cantidad,equipo,bo){
    $.ajax({
        type:'POST',
        url: base_url+'AsignacionesSeries/seriesasignadaserentas',
        data: {
            equipo: row
        },
        async: false,
        statusCode:{
            404: function(data){
                swal("Error", "404", "error");
            },
            500: function(){
                swal("Error", "500", "error"); 
            }
        },
        success:function(data){
            $('.newoseriesequipos_renta').html(data);
        }
    });
    $.ajax({
        type:'POST',
        url: base_url+'AsignacionesSeries/serieserenta',
        data: {
            equipo: equipo,
            bod: bo,
            cantidad:cantidad,
            idrow:row,
        },
        async: false,
        statusCode:{
            404: function(data){
                swal("Error", "404", "error");
            },
            500: function(){
                swal("Error", "500", "error"); 
            }
        },
        success:function(data){
            $('#modal_series_editar_e_renta').modal();
            $('#modal_series_editar_e_renta').modal('open');
            $('.newseriesequipos_rentas').html(data);
            $('.cant_requ_div_renta').remove();
        }
    });
    $(".checkproducto_o_renta").attr("type","radio");
    $(".checkproductorenta").attr("type","radio");
}
//
function editarasignara(row,cantidad,accesorio,bo){
    $.ajax({
        type:'POST',
        url: base_url+'AsignacionesSeries/seriesasignadasa',
        data: {
            accesorio: row
        },
        async: false,
        statusCode:{
            404: function(data){
                swal("Error", "404", "error");
            },
            500: function(){
                swal("Error", "500", "error"); 
            }
        },
        success:function(data){
            $('.newoseriesaccesorios').html(data);
        }
    });
    $.ajax({
        type:'POST',
        url: base_url+'AsignacionesSeries/seriesa',
        data: {
            accesorio: accesorio,
            bod: bo,
            cantidad:cantidad,
            idrow:row,
        },
        async: false,
        statusCode:{
            404: function(data){
                swal("Error", "404", "error");
            },
            500: function(){
                swal("Error", "500", "error"); 
            }
        },
        success:function(data){
            $('#modal_series_editar_a').modal();
            $('#modal_series_editar_a').modal('open');
            $('.newseriesaccesorios').html(data);
            $('.cant_requ_div').remove();
        }
    });
    $(".checkproducto_o").attr("type","radio");
    $(".checkproducto").attr("type","radio");
}
// accesorios rentas get octener los datos
function editarasignararentas(row,cantidad,accesorio,bo){
    $.ajax({
        type:'POST',
        url: base_url+'AsignacionesSeries/seriesasignadasa_rentas',
        data: {
            accesorio: row
        },
        async: false,
        statusCode:{
            404: function(data){
                swal("Error", "404", "error");
            },
            500: function(){
                swal("Error", "500", "error"); 
            }
        },
        success:function(data){
            $('.newoseriesaccesorios_rentas').html(data);
        }
    });
    $.ajax({
        type:'POST',
        url: base_url+'AsignacionesSeries/seriesarentas',
        data: {
            accesorio: accesorio,
            bod: bo,
            cantidad:cantidad,
            idrow:row,
        },
        async: false,
        statusCode:{
            404: function(data){
                swal("Error", "404", "error");
            },
            500: function(){
                swal("Error", "500", "error"); 
            }
        },
        success:function(data){
            $('#modal_series_editar_a_rentas').modal();
            $('#modal_series_editar_a_rentas').modal('open');
            $('.newseriesaccesorios_rentas').html(data);
            $('.cant_requ_div_rentas').remove();
        }
    });
    $(".checkproducto_o_renta").attr("type","radio");
    $(".checkproducto_renta").attr("type","radio");
}
//
function editarasignarr(row,cantidad,refaccion,bo){
    $.ajax({
        type:'POST',
        url: base_url+'AsignacionesSeries/seriesasignadasr',
        data: {
            refaccion: row
        },
        async: false,
        statusCode:{
            404: function(data){
                swal("Error", "404", "error");
            },
            500: function(){
                swal("Error", "500", "error"); 
            }
        },
        success:function(data){
            $('.newoseriesrefacciones').html(data);
        }
    });
    $.ajax({
        type:'POST',
        url: base_url+'AsignacionesSeries/seriesr',
        data: {
            refaccion: refaccion,
            bod: bo,
            cantidad:cantidad,
            idrow:row,
        },
        async: false,
        statusCode:{
            404: function(data){
                swal("Error", "404", "error");
            },
            500: function(){
                swal("Error", "500", "error"); 
            }
        },
        success:function(data){
            $('#modal_series_editar_r').modal();
            $('#modal_series_editar_r').modal('open');
            $('.newseriesrefacciones').html(data);
            $('.cant_requ_div').remove();
            setTimeout(function(){
                $('.checkproductossc').hide();
            }, 1000);
            
            varidarcantr();
        }
    });
    $(".checkproducto_o").attr("type","radio");
    $(".checkproducto").attr("type","radio");
    $(".checkproductoss").attr("type","radio");
}
function editarasignarr_vd(row,cantidad,refaccion,bo){
    $.ajax({
        type:'POST',
        url: base_url+'AsignacionesSeries/seriesasignadasr_vd',
        data: {
            refaccion: row
        },
        async: false,
        statusCode:{
            404: function(data){
                swal("Error", "404", "error");
            },
            500: function(){
                swal("Error", "500", "error"); 
            }
        },
        success:function(data){
            $('.newoseriesrefacciones').html(data);
        }
    });
    $.ajax({
        type:'POST',
        url: base_url+'AsignacionesSeries/seriesr',
        data: {
            refaccion: refaccion,
            bod: bo,
            cantidad:cantidad,
            idrow:row,
        },
        async: false,
        statusCode:{
            404: function(data){
                swal("Error", "404", "error");
            },
            500: function(){
                swal("Error", "500", "error"); 
            }
        },
        success:function(data){
            $('#modal_series_editar_r').modal();
            $('#modal_series_editar_r').modal('open');
            $('.newseriesrefacciones').html(data);
            $('.cant_requ_div').remove();
            setTimeout(function(){
                $('.checkproductossc').hide();
            }, 1000);
            
            varidarcantr();
        }
    });
    $(".checkproducto_o").attr("type","radio");
    $(".checkproducto").attr("type","radio");
    $(".checkproductoss").attr("type","radio");
}
function aceptar_series_e(){
    var tipo = $('input[name=tipoasi]:checked').val();
    $('.aceptar_series_e').prop( "disabled", true );
    setTimeout(function() { 
        $('.aceptar_series_e').prop( "disabled", false );
    }, 10000);
    var seriesselected=0;
    $(".checkproducto").each(function() {
        if ($(this).is(':checked')) {
            seriesselected++;
        }
    });
    var cantidad=$('#cant_requ_e').val();
    if (seriesselected==cantidad) {
        var DATA  = [];
        var seleccionados   = $(".checkproducto:checked");
        seleccionados.each(function(){
            item = {};
            item ["serie"]  = $(this).val();
            DATA.push(item);
        });
        console.log(DATA);
        arrayreseries  = JSON.stringify(DATA);
        var equipo=$('#equipo').val();
        var newbodega=$('#newbodegae option:selected').val();
        var datos = 'arrayreseries='+arrayreseries+'&equipo='+equipo+'&newbodega='+newbodega;
        if(tipo==1){
            var url_control=base_url+"index.php/AsignacionesSeries/addaseriese";
        }else{
            var url_control=base_url+"index.php/AsignacionesSeries/addaseriese_vd";
        }
        $.ajax({
            type:'POST',
            url: url_control,
            data: datos,
            success: function (response){
                idventatabler=response;
                alertfunction('Éxito!','Series asignadas: '+response);
                $('#modal_series_e').modal('close');
                if(tipo==1){
                    datos_equipos();
                }else{
                    datos_equipos_vd();
                }
                setTimeout(function(){ 
                    $('.addseriesequipos').html('');
                }, 2000);
            
            },
            error: function(response){
                notificacion(1); 
            }
        });
    }else{
        alertfunction('Atención!','Favor de colocar el número exacto de piezas solicitadas');
    }
}
// rentas 
function aceptar_series_rentas(){
    $( ".aceptar_series_rentas" ).prop( "disabled", true );
    setTimeout(function(){ 
         $(".aceptar_series_rentas" ).prop( "disabled", false );
    }, 10000);
    var seriesselected=0;
    $(".checkproducto_renta").each(function() {
        if ($(this).is(':checked')) {
            seriesselected++;
        }
    });
    var cantidad=$('#cant_requ_renta').val();
    if (seriesselected==cantidad) {
        var DATA  = [];
        var seleccionados   = $(".checkproducto_renta:checked");
        seleccionados.each(function(){
            item = {};
            item ["serie"]  = $(this).val();
            DATA.push(item);
        });
        console.log(DATA);
        arrayreseries  = JSON.stringify(DATA);
        var equipo=$('#equipo_renta').val();
        var newbodega=$('#newbodegaer option:selected').val();
        var idcliente=$('#idCliente').val();
        var datos = 'arrayreseries='+arrayreseries+'&equipo='+equipo+'&idcliente='+idcliente+'&newbodega='+newbodega;
            $.ajax({
                type:'POST',
                url: base_url+"index.php/AsignacionesSeries/addaseriesrentas",
                data: datos,
                success: function (response){
                    idventatabler=response;
                    alertfunction('Éxito!','Series asignadas: '+response);
                    $('#modal_series_rentas').modal('close');
                    datos_equiposr();
                    
                    setTimeout(function(){ 
                        $('.addseriesequipos_rentas').html('');
                    }, 2000);
                    
                },
                error: function(response){
                    notificacion(1);  
                }
            });
    }else{
        alertfunction('Atención!','Favor de colocar el número exacto de piezas solicitadas');
    }
}
function aceptar_series_a(){
    var tipo = $('input[name=tipoasi]:checked').val();
    $( ".aceptar_series_a" ).prop( "disabled", true );
    setTimeout(function(){ 
         $(".aceptar_series_a" ).prop( "disabled", false );
    }, 10000);
    var seriesselected=0;
    $(".addseriesaccesorios .checkproductocs").each(function() {
        if ($(this).is(':checked')) {
            seriesselected++;
        }
    });
    //============================
        $(".addseriesaccesorios div > .checkproductossrow ").each(function(){
            //console.log('re');
            if($(this).find("input[class*='checkproducto']").is(':checked')){
                //seriesselected--;
                seriesselected=seriesselected+parseFloat($(this).find("input[class*='checkproductossc']").val());
            }
        });  
    //============================
    var cantidad=$('#cant_requ_a').val();
    if (seriesselected==cantidad) {
        var DATA  = [];
        var seleccionados   = $(".checkproductocs:checked");
        seleccionados.each(function(){
            item = {};
            item ["serie"]  = $(this).val();
            DATA.push(item);
        });
        console.log(DATA);
        arrayreseries  = JSON.stringify(DATA);

        //==============================
            var DATAas  = [];
            $(".addseriesaccesorios div > .checkproductossrow ").each(function(){
    
                if($(this).find("input[class*='checkproducto']").is(':checked')){
                    item = {};
                    item ["serie"]  = $(this).find("input[class*='checkproducto']").val();
                    item ["cantidad"]  = parseFloat($(this).find("input[class*='checkproductossc']").val());
                    DATAas.push(item);
                }
            });
        arrayreseriesss  = JSON.stringify(DATAas);
        //==============================

        var accesorio=$('#accesorio').val();
        var newbodega=$('#newbodegaac option:selected').val();
        var datos = 'arrayreseries='+arrayreseries+'&accesorio='+accesorio+'&newbodega='+newbodega+'&arrayreseriesss='+arrayreseriesss;
        if(tipo==1){
            var url_control=base_url+"index.php/AsignacionesSeries/addaseriesa";
        }else{
            var url_control=base_url+"index.php/AsignacionesSeries/addaseriesa_vd";
        }
            $.ajax({
                type:'POST',
                url: url_control,
                data: datos,
                success: function (response){
                    idventatabler=response;
                    alertfunction('Éxito!','Series asignadas: '+response);
                    $('#modal_series_a').modal('close');
                    if(tipo==1){
                        datos_accesorios();
                    }else{
                        datos_accesorios_vd();
                    }
                    setTimeout(function(){ 
                        $('.addseriesaccesorios').html('');
                    }, 2000);
                    
                },
                error: function(response){
                    notificacion(1);
                }
            });
    }else{
        alertfunction('Atención!','Favor de colocar el número exacto de piezas solicitadas');
    }
}
// renta accesorios
function aceptar_series_a_rentas(){
    $( ".aceptar_series_a_rentas" ).prop( "disabled", true );
    setTimeout(function(){ 
         $(".aceptar_series_a_rentas" ).prop( "disabled", false );
    }, 10000);
    var seriesselected=0;
    var seriesselectedss=0;
    $(".checkproducto_rentacs").each(function() {
        if ($(this).is(':checked')) {
            seriesselected++;
        }
    });
    $(".addseriesaccesorios_rentas div > .checkproductossrow ").each(function(){    
        if($(this).find("input[class*='checkproducto_renta']").is(':checked')){
            //seriesselected--;
            seriesselected=seriesselected+parseFloat($(this).find("input[class*='checkproductossc']").val());
        }
    });
    var cantidad=$('#cant_requ_a_rentas').val();
    if (seriesselected==cantidad) {
        var DATA  = [];
        var seleccionados   = $(".checkproducto_rentacs:checked");
        seleccionados.each(function(){
            item = {};
            item ["serie"]  = $(this).val();
            DATA.push(item);
        });
        console.log(DATA);
        arrayreseries  = JSON.stringify(DATA);
        //==============================
            var DATAas  = [];
            $(".addseriesaccesorios_rentas div > .checkproductossrow ").each(function(){
    
                if($(this).find("input[class*='checkproducto_renta']").is(':checked')){
                    item = {};
                    item ["serie"]  = $(this).find("input[class*='checkproducto_renta']").val();
                    item ["cantidad"]  = parseFloat($(this).find("input[class*='checkproductossc']").val());
                    DATAas.push(item);
                }
            });
        arrayreseriesss  = JSON.stringify(DATAas);
        //==============================
        var accesorio=$('#accesorio_rentas').val();
        var newbodega=$('#newbodegaacr option:selected').val();
        var datos = 'arrayreseries='+arrayreseries+'&accesorio='+accesorio+'&newbodega='+newbodega+'&arrayreseriesss='+arrayreseriesss;
            $.ajax({
                type:'POST',
                url: base_url+"index.php/AsignacionesSeries/addaseriesarentas",
                data: datos,
                success: function (response){
                    idventatabler=response;
                    alertfunction('Éxito!','Series asignadas: '+response);
                    $('#modal_series_a_rentas').modal('close');
                    
                    setTimeout(function(){ 
                        $('.addseriesaccesorios_rentas').html('');
                    }, 2000);
                    
                    datos_accesoriosr();
                },
                error: function(response){
                    notificacion(1);
                }
            });
    }else{
        alertfunction('Atención!','Favor de colocar el número exacto de piezas solicitadas');
    }
}
// ==
function aceptar_series_r(){
    var tipo = $('input[name=tipoasi]:checked').val();
    $( ".aceptar_series_r" ).prop( "disabled", true );
    setTimeout(function(){ 
         $(".aceptar_series_r" ).prop( "disabled", false );
    }, 10000);
    var seriesselected=0;
    $(".addseriesrefacciones div > .checkproductosss").each(function() {
        if ($(this).is(':checked')) {
            seriesselected++;
        }
    });
    $(".addseriesrefacciones div > .checkproductossrow ").each(function(){
        if($(this).find("input[class*='checkproductoss']").is(':checked')){
            //seriesselected--;
            seriesselected=seriesselected+parseFloat($(this).find("input[class*='checkproductossc']").val());
        }
    });
    var cantidad=parseFloat($('#cant_requ_r').val());
    if (seriesselected==cantidad) {
        //====================================
        var DATA  = [];
        var seleccionados   = $(".addseriesrefacciones div > .checkproductosss:checked");
        seleccionados.each(function(){
            item = {};
            item ["serie"]  = $(this).val();
            DATA.push(item);
        });
        console.log(DATA);
        arrayreseries  = JSON.stringify(DATA);
        //====================================
        var refaccion=$('#refaccion').val();
        var newbodega=$('#newbodegaarr option:selected').val();
        //====================================
        var DATAs  = [];
        var seleccionadoss   = $(".addseriesrefacciones div > .checkproductossrow ");
        seleccionadoss.each(function(){
            if($(this).find("input[class*='checkproductoss']").is(':checked')){
                item = {};
                item ["serie"]  = $(this).find("input[class*='checkproductoss']").val()
                item ["cantidad"]  = $(this).find("input[class*='checkproductossc']").val()
                DATAs.push(item);
            }
        });
        arrayreseriess  = JSON.stringify(DATAs);
        var datos = 'arrayreseries='+arrayreseries+'&arrayreseriesss='+arrayreseriess+'&refaccion='+refaccion+'&newbodega='+newbodega;
        console.log(datos);
        if(tipo==1){
            var url_control=base_url+"index.php/AsignacionesSeries/addaseriesr";
        }else{
            var url_control=base_url+"index.php/AsignacionesSeries/addaseriesr_vd";
        }
            $.ajax({
                type:'POST',
                url: url_control,
                data: datos,
                success: function (response){
                    idventatabler=response;
                    alertfunction('Éxito!','Series asignadas: '+response);
                    $('#modal_series_r').modal('close');
                    setTimeout(function(){ 
                        if(tipo==1){
                            datos_refacciones();
                        }else{
                            datos_refacciones_vd();
                        }
                        $('.addseriesrefacciones').html('');
                    }, 2000);
                },
                error: function(response){
                    notificacion(1);
                }
            });
    }else{
        alertfunction('Atención!','Favor de colocar el número exacto de piezas solicitadas');
    }
}
function alertfunction(titlea,contenta){
    $.alert({boxWidth: '30%',useBootstrap: false,title: titlea,theme: 'bootstrap',content: contenta});  
}
function editar_series_e(){
    var tipo = $('input[name=tipoasi]:checked').val();
    var seriesselectedo=0;
    $(".checkproducto_o").each(function() {
        if ($(this).is(':checked')) {
            seriesselectedo++;
        }
    });
    var seriesselected=0;
    $(".checkproducto").each(function() {
        if ($(this).is(':checked')) {
            seriesselected++;
        }
    });
    if (seriesselectedo>0) {
        if (seriesselected>0) {
            var regreso = $(".checkproducto_o:checked").val();
            var nuevo= $(".checkproducto:checked").val();
            var equipotr=$('#equipo').val();
            if(tipo==1){
                var url_control=base_url+'AsignacionesSeries/equipocambio';
            }else{
                var url_control=base_url+'AsignacionesSeries/equipocambio_vd';
            }
            $.ajax({
                type:'POST',
                url: url_control,
                data: {
                    regreso: regreso,
                    nuevo: nuevo,
                    equipotr:equipotr,
                },
                async: false,
                statusCode:{
                    404: function(data){
                        swal("Error", "404", "error");
                    },
                    500: function(){
                        swal("Error", "500", "error"); 
                    }
                },
                success:function(data){
                    alertfunction('Éxito!','Se han modificado las series');
                    $('#modal_series_editar_e').modal('close');
                }
            });
        }else{
            alertfunction('Atención!','Seleccione la serie a agregar');
        }
    }else{
        alertfunction('Atención!','Seleccione la serie a devolución'); 
    }
}
// Rentas
function editar_series_rentas(){
    var seriesselectedo=0;
    $(".checkproducto_o_renta").each(function() {
        if ($(this).is(':checked')) {
            seriesselectedo++;
        }
    });
    var seriesselected=0;
    $(".checkproductorenta").each(function() {
        if ($(this).is(':checked')) {
            seriesselected++;
        }
    });
    if (seriesselectedo>0) {
        if (seriesselected>0) {
            var regreso = $(".checkproducto_o_renta:checked").val();
            var nuevo= $(".checkproductorenta:checked").val();
            var equipotr=$('#equipo_rentas').val();
            $.ajax({
                type:'POST',
                url: base_url+'AsignacionesSeries/equipocambiorentas',
                data: {
                    regreso: regreso,
                    nuevo: nuevo,
                    equipotr:equipotr,
                },
                async: false,
                statusCode:{
                    404: function(data){
                        swal("Error", "404", "error");
                    },
                    500: function(){
                        swal("Error", "500", "error"); 
                    }
                },
                success:function(data){
                    alertfunction('Éxito!','Se han modificado las series');
                    $('#modal_series_editar_e_renta').modal('close');
                    
                }
            });
        }else{
            alertfunction('Atención!','Seleccione la serie a agregar');
        }

    }else{
        alertfunction('Atención!','Seleccione la serie a devolución'); 
    }
}
//
function editar_series_a(){
    var seriesselectedo=0;
    $(".checkproducto_o").each(function() {
        if ($(this).is(':checked')) {
            seriesselectedo++;
        }
    });
    var seriesselected=0;
    $(".checkproducto").each(function() {
        if ($(this).is(':checked')) {
            seriesselected++;
        }
    });
    if (seriesselectedo>0) {
        if (seriesselected>0) {
            var regreso = $(".checkproducto_o:checked").val();
            var nuevo= $(".checkproducto:checked").val();
            var accesoriotr=$('#accesorio').val();
            $.ajax({
                type:'POST',
                url: base_url+'AsignacionesSeries/accesoriocambio',
                data: {
                    regreso: regreso,
                    nuevo: nuevo,
                    accesoriotr:accesoriotr,
                },
                async: false,
                statusCode:{
                    404: function(data){
                        swal("Error", "404", "error");
                    },
                    500: function(){
                        swal("Error", "500", "error"); 
                    }
                },
                success:function(data){
                    alertfunction('Éxito!','Se han modificado las series');
                    $('#modal_series_editar_a').modal('close');
                    
                }
            });
        }else{
            alertfunction('Atención!','Seleccione la serie a agregar');
        }

    }else{
        alertfunction('Atención!','Seleccione la serie a devolución'); 
    }
}
// accesorios rentas
function editar_series_a_rentas(){
    var seriesselectedo=0;
    $(".checkproducto_o_renta").each(function() {
        if ($(this).is(':checked')) {
            seriesselectedo++;
        }
    });
    var seriesselected=0;
    $(".checkproducto_renta").each(function() {
        if ($(this).is(':checked')) {
            seriesselected++;
        }
    });
    if (seriesselectedo>0) {
        if (seriesselected>0) {
            var regreso = $(".checkproducto_o_renta:checked").val();
            var nuevo= $(".checkproducto_renta:checked").val();
            var accesoriotr=$('#accesorio_rentas').val();
            $.ajax({
                type:'POST',
                url: base_url+'AsignacionesSeries/accesoriocambiorentas',
                data: {
                    regreso: regreso,
                    nuevo: nuevo,
                    accesoriotr:accesoriotr,
                },
                async: false,
                statusCode:{
                    404: function(data){
                        swal("Error", "404", "error");
                    },
                    500: function(){
                        swal("Error", "500", "error"); 
                    }
                },
                success:function(data){
                    alertfunction('Éxito!','Se han modificado las series');
                    $('#modal_series_editar_a_rentas').modal('close');
                    
                }
            });
        }else{
            alertfunction('Atención!','Seleccione la serie a agregar');
        }

    }else{
        alertfunction('Atención!','Seleccione la serie a devolución'); 
    }
}
//
function editar_series_r(){
    var tipo = $('input[name=tipoasi]:checked').val();
    var seriesselectedo=0;
    $(".checkproducto_o").each(function() {
        if ($(this).is(':checked')) {
            seriesselectedo++;
        }
    });
    var seriesselected=0;
    $(".checkproductoss").each(function() {
        if ($(this).is(':checked')) {
            seriesselected++;
        }
    });
    if (seriesselectedo>0) {
        if (seriesselected>0) {
            var regreso = $(".checkproducto_o:checked").val();
            var nuevo= $(".checkproductoss:checked").val()==undefined?$(".checkproductoss:checked").val():$(".checkproductoss:checked").val();
            var refacciontr=$('#refaccion').val();

            var regresodata = $(".checkproducto_o:checked").data('con');
            var nuevodata= $(".checkproducto:checked").data('con')==undefined?$(".checkproductoss:checked").data('con'):$(".checkproducto:checked").data('con');
            if (regresodata==nuevodata) {
                if(tipo==1){
                    var url_control=base_url+'AsignacionesSeries/refaccioncambio';
                }else{
                    var url_control=base_url+'AsignacionesSeries/refaccioncambio_vd';
                }
                $.ajax({
                    type:'POST',
                    url: url_control,
                    data: {
                        regreso: regreso,
                        nuevo: nuevo,
                        refacciontr:refacciontr,
                        tipo:regresodata
                    },
                    async: false,
                    statusCode:{
                        404: function(data){
                            swal("Error", "404", "error");
                        },
                        500: function(){
                            swal("Error", "500", "error"); 
                        }
                    },
                    success:function(data){
                        alertfunction('Éxito!','Se han modificado las series');
                        $('#modal_series_editar_r').modal('close');
                    }
                });
            }else{
                alertfunction('Atención!','Solo se puede por el mismo tipo');
            }
        }else{
            alertfunction('Atención!','Seleccione la serie a agregar');
        }
    }else{
        alertfunction('Atención!','Seleccione la serie a devolución'); 
    }
}
function varidarcantr(){
    setTimeout(function(){ 
        $('.checkproductossc').change(function(){
            var dato=parseFloat($(this).val());
            var max=parseFloat($(this).data('max'));
            console.log(dato);  
            console.log(max); 
            if (dato>max) {
                $(this).val(max);
                toastr.error('Se excedio la cantidad disponible de '+max+' Piezas');
            } 
        });
    }, 2000);  
}
function limpiarmodales(){
    $('.addseriesequipos').html('');
    $('.addseriesequipos_rentas').html('');
    $('.addseriesaccesorios').html('');
    $('.addseriesaccesorios_rentas').html('');
    $('.addseriesrefacciones').html('');
    $('.addseriesequipos_rentas_selected').html('');
}
function search_1(){
    var value=$('#search_1').val().toLowerCase();
    $(".addseriesequipos div").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
}
function search_2(){
    var value=$('#search_2').val().toLowerCase();
    $(".addseriesequipos_rentas div").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
}
function search_3(){
    var value=$('#search_3').val().toLowerCase();
    $(".addseriesaccesorios div").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
}
function search_4(){
    var value=$('#search_4').val().toLowerCase();
    $(".addseriesaccesorios_rentas div").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
}
function search_5(){
    var value=$('#search_5').val().toLowerCase();
    $(".addseriesrefacciones div").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
}
function selectedserie(datos) {
    console.log(datos);
    console.log(datos[0].id);
    var inputserie=datos[0].id;
    var inputserievalue=datos[0].value;
    var inputseriechecked=datos[0].checked;
    var inputserielabel=datos[0].labels[0].innerText;
    var html = '<div class="col s6 m3 selectedserieck_'+inputserie+'" >\
                    <div class="card" >\
                        <div class="card-content" style="padding:15px" >\
                          <input type="checkbox" id="'+inputserie+'" class="checkproducto_renta" onclick="selectedserieck($(this))"\
                          name="checkproducto_renta"  value="'+inputserievalue+'" checked="'+inputseriechecked+'">\
                          <label for="'+inputserie+'" style="padding-left:22px">'+inputserielabel+'</label> \
                        </div >\
                      </div>\
                </div>';
    console.log(html);
    $('.addseriesequipos_rentas_selected').append(html);
    $('.serieIdp_renta_ck_'+inputserievalue).remove();
}
function selectedserieck(datos){
    if(datos[0].checked==false){
        $('.selectedserieck_'+datos[0].id).remove();
    }
}
function selectedseriev(datos) {
    console.log(datos);
    console.log(datos[0].id);
    var inputserie=datos[0].id;
    var inputserievalue=datos[0].value;
    var inputseriechecked=datos[0].checked;
    var inputserielabel=datos[0].labels[0].innerText;
    var html = '<div class="col s6 m3 selectedserieck_'+inputserie+'" >\
                    <div class="card" >\
                        <div class="card-content" style="padding:15px" >\
                          <input type="checkbox" id="'+inputserie+'" class="checkproducto" onclick="selectedserieckv($(this))"\
                          name="checkproducto"  value="'+inputserievalue+'" checked="'+inputseriechecked+'">\
                          <label for="'+inputserie+'" style="padding-left:22px">'+inputserielabel+'</label> \
                        </div >\
                      </div>\
                </div>';
    console.log(html);
    $('.addseriesequipos_selected').append(html);
    $('.serieIdp_ck_'+inputserievalue).remove();
}
function selectedserieckv(datos){
    if(datos[0].checked==false){
        $('.selectedserieck_'+datos[0].id).remove();
    }
}
function ocultarsolicitud(row,tipo){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: 'Desea ocultar la solicitud de serie?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                 $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Generales/ocultarsolicitud",
                    data: {
                        rowid:row,
                        tipo:tipo
                    },
                    success: function (response){
                        seleccionconsulta();
                    },
                    error: function(response){
                        notificacion(1); 
                    }
                });
                    
            },
            cancelar: function () {
                
            }
        }
    });
}
//================================================================================================= D impresion
function datos_equipos_vd(){
    table.destroy();
    table=$("#tabla_asignar").DataTable({
        stateSave: true,
        "bProcessing": true,
        "serverSide": true,
        search: {
                return: true
            },
        "ajax": {
           "url": base_url+"index.php/AsignacionesSeries/getData_asignacion_e_vd",
           type: "post",
            error: function(){
               $("#tabla_asignar").css("display","none");
            }
        },
        "columns": [
            {"data": "id"},
            {"data": "idVenta",
                render: function(data,type,row){
                    var html='';
                    
                        html=row.idVenta;
                    
                    return html;
                }
            },
            {"data": null,"visible": false},
            {"data": "nombre"},
            {"data": "bodega"},
            {"data": "cantidad"},
            {"data": "modelo"},
            {"data": "noparte"},
            {"data": "empresa"},
            {"data": "reg"},
            {"data": null,
                render: function(data,type,row){ 
                    if(row.serie_estatus == 0){
                        var html='<a class="btn-bmz waves-effect waves-light cyan" onclick="asignare('+row.id+','+row.cantidad+','+row.idEquipo+','+row.serie_bodega+')" >Asignar</a>';
                            html+='<a class="btn-bmz waves-effect waves-light cyan" onclick="ocultarsolicitud('+row.id+',1)" title="Ocultar Solicitud"><i class="fas fa-eye-slash fa-fw"></i></a>';
                    }else if(row.prefactura == 0){
                        var html='<a class="btn-bmz waves-effect waves-light green darken-1" onclick="editarasignare_vd('+row.id+','+row.cantidad+','+row.idEquipo+','+row.serie_bodega+')">Editar</a>';
                    }else{
                        var html='<a class="btn-bmz waves-effect waves-light green darken-1">Asignadas</a>';
                    }
                    return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
    }).on('draw',function(){
        $('.dataTables_filter').append('<i class="fa fa-search buttonsearch" onclick="searchtable_v()"></i>')

    });
}
function editarasignare_vd(row,cantidad,equipo,bo){
    $.ajax({
        type:'POST',
        url: base_url+'AsignacionesSeries/seriesasignadase_vd',
        data: {
            equipo: row
            },
            async: false,
            statusCode:{
                404: function(data){
                    swal("Error", "404", "error");
                },
                500: function(){
                    swal("Error", "500", "error"); 
                }
            },
            success:function(data){
                $('.newoseriesequipos').html(data);
            }
        });
    $.ajax({
        type:'POST',
        url: base_url+'AsignacionesSeries/seriese',
        data: {
            equipo: equipo,
            bod: bo,
            cantidad:cantidad,
            idrow:row,
            },
            async: false,
            statusCode:{
                404: function(data){
                    swal("Error", "404", "error");
                },
                500: function(){
                    swal("Error", "500", "error"); 
                }
            },
            success:function(data){
                $('#modal_series_editar_e').modal();
                $('#modal_series_editar_e').modal('open');
                $('.newseriesequipos').html(data);
                $('.cant_requ_div').remove();
            }
        });
    $(".checkproducto_o").attr("type","radio");
    $(".checkproducto").attr("type","radio");
}
function datos_accesorios_vd(){
    console.log('datos_accesorios_vd');
    table.destroy();
    table=$("#tabla_asignar").DataTable({
        "bProcessing": true,
        "serverSide": true,
        stateSave: true,
        search: {
                return: true
            },
        "ajax": {
           "url": base_url+"index.php/AsignacionesSeries/getData_asignacion_a_vd",
           type: "post",
            error: function(){
               $("#tabla_asignar").css("display","none");
            }
        },
        "columns": [
            {"data": "id_accesoriod"},
            {"data": "id_venta",
                render: function(data,type,row){
                    var html='';
                    
                        html=row.id_venta;
                    
                    return html;
                }
            },
            {"data": null,"visible": false},
            {"data": "vendedor"},
            {"data": "bodega"},
            {"data": "cantidad"},
            {"data": "nombre"},
            {"data": "no_parte"},
            {"data": "empresa"},
            {"data": "reg"},
            {"data": null,
                render: function(data,type,row){ 
                    if(row.serie_estatus == 0){
                        var html='<a class="btn-bmz waves-effect waves-light cyan" onclick="asignara('+row.id_accesoriod+','+row.cantidad+','+row.id_accesorio+','+row.serie_bodega+')">Asignar</a>';
                            html+='<a class="btn-bmz waves-effect waves-light cyan" onclick="ocultarsolicitud('+row.id_accesoriod+',2)" title="Ocultar Solicitud"><i class="fas fa-eye-slash fa-fw"></i></a>';
                    }else if(row.prefactura == 0){
                        var html='<a class="btn-bmz waves-effect waves-light green darken-1" onclick="editarasignara_vd('+row.id_accesoriod+','+row.cantidad+','+row.id_accesorio+','+row.serie_bodega+')">Editar</a>';
                    }else{
                        var html='<a class="btn-bmz waves-effect waves-light green darken-1">Asignadas</a>';
                    }
                    return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
    }).on('draw',function(){
        $('.dataTables_filter').append('<i class="fa fa-search buttonsearch" onclick="searchtable_v()"></i>')

    });
}
function editarasignara_vd(row,cantidad,accesorio,bo){
    $.ajax({
        type:'POST',
        url: base_url+'AsignacionesSeries/seriesasignadasa_vd',
        data: {
            accesorio: row
        },
        async: false,
        statusCode:{
            404: function(data){
                swal("Error", "404", "error");
            },
            500: function(){
                swal("Error", "500", "error"); 
            }
        },
        success:function(data){
            $('.newoseriesaccesorios').html(data);
        }
    });
    $.ajax({
        type:'POST',
        url: base_url+'AsignacionesSeries/seriesa',
        data: {
            accesorio: accesorio,
            bod: bo,
            cantidad:cantidad,
            idrow:row,
        },
        async: false,
        statusCode:{
            404: function(data){
                swal("Error", "404", "error");
            },
            500: function(){
                swal("Error", "500", "error"); 
            }
        },
        success:function(data){
            $('#modal_series_editar_a').modal();
            $('#modal_series_editar_a').modal('open');
            $('.newseriesaccesorios').html(data);
            $('.cant_requ_div').remove();
        }
    });
    $(".checkproducto_o").attr("type","radio");
    $(".checkproducto").attr("type","radio");
}
function datos_refacciones_vd(){
    table.destroy();
    table=$("#tabla_asignar").DataTable({
        "bProcessing": true,
        "serverSide": true,
        stateSave: true,
        search: {
                return: true
            },
        "ajax": {
           "url": base_url+"index.php/AsignacionesSeries/getData_asignacion_r_vd",
           type: "post",
            error: function(){
               $("#tabla_asignar").css("display","none");
            }
        },
        "columns": [
            {"data": "id"},
            {"data": "idVentas",
                render: function(data,type,row){
                    var html='';
                        html=row.idVentas;
                    
                    return html;
                }
            },
            {"data": null,"visible": false},
            {"data": "nombre"},
            {"data": "bodega"},
            {"data": "piezas"},
            {"data": "modelo"},
            {"data": "codigo"},
            {"data": "empresa"},
            {"data": "reg"},
            {"data": null,
                render: function(data,type,row){ 
                    if(row.serie_estatus == 0){
                        var html='<a class="btn-bmz waves-effect waves-light cyan" onclick="asignarr('+row.id+','+row.piezas+','+row.idRefaccion+','+row.serie_bodega+','+row.tipov+')">Asignar</a>';
                        html+='<a class="btn-bmz waves-effect waves-light cyan" onclick="ocultarsolicitud('+row.id+',3)" title="Ocultar Solicitud"><i class="fas fa-eye-slash fa-fw"></i></a>';
                    }else if(row.prefactura == 0){
                        var html='<a class="btn-bmz waves-effect waves-light green darken-1" onclick="editarasignarr_vd('+row.id+','+row.piezas+','+row.idRefaccion+','+row.serie_bodega+')">Editar</a>';
                    }else{
                        var html='<a class="btn-bmz waves-effect waves-light green darken-1">Asignadas</a>';
                    }
                    return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
    }).on('draw',function(){
        $('.dataTables_filter').append('<i class="fa fa-search buttonsearch" onclick="searchtable_v()"></i>')

    });
}
function bloqueoseries(){
    $.ajax({
        type:'POST',
        url: base_url+'AsignacionesSeries/bodegasview',
        data: {
            accesorio: 0
        },
        async: false,
        statusCode:{
            404: function(data){
                swal("Error", "404", "error");
            },
            500: function(){
                swal("Error", "500", "error"); 
            }
        },
        success:function(data){
            var array = $.parseJSON(data);
            $.each(array, function(index, item) {
                if(parseInt(item.tipov)==parseInt(tipovselected)){

                }else{
                    $('.bodega_'+item.bodegaId).attr('disabled',true);
                }
            });
        }
    });
}
//=========================
function asignarrmultiple(){
    var num_pres_selected=0;
    var rentaslis = $("#tabla_asignar tbody > tr");
    var DATAr  = [];
        
    rentaslis.each(function(){  
        if ($(this).find("input[class*='refaccion_checked']").is(':checked')) {
            num_pres_selected++;
            item = {};                    
            item ["idrow"]  = $(this).find("input[class*='refaccion_checked']").data('id');
            item ["can"]  = $(this).find("input[class*='refaccion_checked']").data('piezas');
            item ["idref"]  = $(this).find("input[class*='refaccion_checked']").data('refaccion');
            item ["refname"]  = $(this).find("input[class*='refaccion_checked']").data('refname');
            item ["bod"]  = $(this).find("input[class*='refaccion_checked']").data('bodega');
            item ["bodname"]  = $(this).find("input[class*='refaccion_checked']").data('bodeganame');
            item ["emp"]  = $(this).find("input[class*='refaccion_checked']").data('emp');
            DATAr.push(item);
        }       
        
    });
    aInfor   = JSON.stringify(DATAr);
    if(num_pres_selected>0){
        $('#modal_series_multiple_refacciones').modal('open');

        datos='refacciones='+aInfor;
        $.ajax({
            type:'POST',
            url: base_url+"index.php/AsignacionesSeries/asignarrmultiple",
            data: datos,
            success: function (response){
                $('.info_asignacion_body').html(response);
                $('.selectedbod').change();
            },
            error: function(response){
                notificacion(1); 
                
            }
        });
    }else{
        toastr["error"]("Seleccione por lo menos una refacción");
    }
}
function m_selec_bod(row){
    var bod = $('.selectedbod_'+row+' option:selected').val();
    var htmlstock=$('.checkproductossrow_'+row+'_'+bod).html()==undefined?'':$('.checkproductossrow_'+row+'_'+bod).html();
    if(htmlstock==''){
        htmlstock='<span style="color:red;">Sin Existencias</span>';
    }
    $('.se_stock_bode_'+row).html(htmlstock);
}

function aceptar_series_multiple_r(){
    $('.ser_mul_g').removeClass('errorinput');
    $('.aceptar_series_multiple_r').prop( "disabled", true );
    setTimeout(function() { 
        $('.aceptar_series_multiple_r').prop( "disabled", false );
    }, 5000);
    var pasa=1;
    var rentaslis = $("#t_selected_ref tbody > tr");
    var DATAr  = [];
        
    rentaslis.each(function(){
        var row = $(this).find("td[class*='info_pro_ref']").data('row');    
        var cansol = $(this).find("td[class*='info_pro_ref']").data('cansol');      
        var cant = $(this).find("input[id*='serieId_mul']").val();
        var max = $(this).find("input[id*='serieId_mul']").data('cantmax');
        if(cansol==cant){
            if(cant>max){
                $('.serieId_mul_'+row).addClass('errorinput');
                pasa=0;
            }
        }else{
            $('.serieId_mul_'+row).addClass('errorinput');
            pasa=0;
        }
        //======================================
        item = {};
        item ["idrow"]  = row;
        item ["idref"]  = $(this).find("td[class*='info_pro_ref']").data('idref');
        item ["bodo"]  = $(this).find("td[class*='info_pro_ref']").data('bodo');
        item ["bodd"]  = $(this).find("select[class*='selectedbod'] option:selected").val();
        item ["serieid"]  = $(this).find("input[id*='serieId_mul']").data('serieid');
        item ["cant"]  = cant;
        if(cant>0){
            DATAr.push(item);
        }
    });
    aInfor   = JSON.stringify(DATAr);

    if(pasa==1){
        datos='refacciones='+aInfor;
        $.ajax({
            type:'POST',
            url: base_url+"index.php/AsignacionesSeries/addaseriesr_multiple",
            data: datos,
            success: function (response){
                alertfunction('Éxito!','Series asignadas: ');
                $('#modal_series_multiple_refacciones').modal('close');
                datos_refacciones();
            },
            error: function(response){
                notificacion(1); 
                
            }
        });
    }else{
        toastr["error"]("Uno de los producto sobrepasa el stock y/o no se agrego la cantidad requerida");
    }
}