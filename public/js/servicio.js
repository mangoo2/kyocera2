var base_url = $('#base_url').val();
var tables;
var idunidad;
var iditinerario=0;
toastr.options = {
  "closeButton": false,
  "debug": false,
  "newestOnTop": false,
  "progressBar": true,
  "positionClass": "toast-top-full-width",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "13000",
  "timeOut": "7000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}
$(document).ready(function($) {
    $('.modal').modal();
    tables = $('#tableunidades').DataTable();
    cargainforma(); 
    $('#infordatac').click(function(event) {
        var tecnico = $('#tecnico option:selected').val();
        var fecha = $('#calendarinput').val();
        window.location.href = base_url+'index.php/AsignacionesTecnico/info/'+tecnico+'/1?fech='+fecha;
    });
    $('#infordatap').click(function(event) {
        var tecnico = $('#tecnico option:selected').val();
        var fecha = $('#calendarinput').val();
        window.location.href = base_url+'index.php/AsignacionesTecnico/info/'+tecnico+'/2?fech='+fecha;
    });
    $('#infordatae').click(function(event) {
        var tecnico = $('#tecnico option:selected').val();
        var fecha = $('#calendarinput').val();
        window.location.href = base_url+'index.php/AsignacionesTecnico/info/'+tecnico+'/3?fech='+fecha;
    });
    $('#infordatav').click(function(event) {
        var tecnico = $('#tecnico option:selected').val();
        var fecha = $('#calendarinput').val();
        window.location.href = base_url+'index.php/AsignacionesTecnico/info/'+tecnico+'/4?fech='+fecha;
    });
    $('#infordatag').click(function(event) {
        var iti = $('.infordatag').data('itinerario');
        if(iti>0){
            var tecnico = $('#tecnico option:selected').val();
            var fecha = $('#calendarinput').val();
            window.location.href = base_url+'index.php/AsignacionesTecnico/info/'+tecnico+'/1?fech='+fecha;
        }else{
            toastr.error('Registra tu itinerario para continuar');
        }
        
    });
    cargaunidades();
    $("#files").fileinput({
        showCaption: true,
        showUpload: false,// quita el boton de upload
        //rtl: true,
        allowedFileExtensions: ["jpg","png","webp","jpeg","pdf"],
        browseLabel: 'Seleccionar documentos',
        uploadUrl: base_url+'Listado_unidades/reg_incidencias',
        maxFilePreviewSize: 5000,
        previewFileIconSettings: {
            'docx': '<i class="fa fa-file-word-o text-primary"></i>',
            'xlsx': '<i class="fa fa-file-excel-o text-success"></i>',
            'pptx': '<i class="fa fa-file-powerpoint-o text-danger"></i>',
            'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
            'pdf': '<i class="fa fa-file-pdf-o text-danger"></i>',
            'zip': '<i class="fa fa-file-archive-o text-muted"></i>',
            'cer': '<i class="fas fa-file-invoice"></i>',
        },
        uploadExtraData: function (previewId, index) {
        var info = {
                    id:idunidad,
                    detalle:$('#detalle').val()
                };
        return info;
      }
    }).on('filebatchuploadcomplete', function(event, files, extra) {
      //location.reload();
        upload_data_view(idunidad);
    }).on('filebatchuploadsuccess', function(event, files, extra) {
      //location.reload();
        //toastr.success('Se cargo el Certificado Correctamente','Hecho!');
        upload_data_view(idunidad);
    });
    $('#registrarincidente').click(function(event) {
        $('#files').fileinput('upload');
    });
  $('#registraritinerario').click(function(event) {
    var iduni = $('#idunidad').val();
    if(iduni>0){}else{iduni=0;}
    var pasaitinerario=1;
    if($('#formser').valid()){
        var clientes = $("#table_clientesininerario tbody > tr");
        //==============================================
            var DATAa  = [];
            clientes.each(function(){         
                item = {};                    
                item ["unidadid"] =iduni;
                item ["descripcion"] = $(this).find("input[id*='in_descrip']").val().replace('&','');
                item ["hora_inicio"] =  $(this).find("input[class*='in_hora_i']").val();
                item ["cli"] =  $(this).find("input[class*='cli']").val();
                item ["hora_fin"] = $(this).find("input[id*='in_hora_f']").val();
                item ["hora_limite"] = $(this).find("input[id*='hora_limite']").val();
                item ["mv_hora"] = $(this).find("input[id*='mv_hora']").val();

                item ["hora_comida"] = $(this).find("input[id*='hora_comida']").val();
                item ["horafin_comida"] = $(this).find("input[id*='horafin_comida']").val();
               
                item ["km_inicio"]=0;
                item ["km_fin"]=$(this).find("input[id*='in_km_f']").val();
                item ["asignacion"]=$(this).find("input[id*='in_asig']").val();
                item ["asignaciong"]=$(this).find("input[id*='in_asig_g']").val();
                item ["vinc"]=$(this).find("input[id*='vinc']").val();
                item ["tipo_asignacion"]=$(this).find("input[id*='in_tipo']").val();
                item ['personal']=$('#tecnico option:selected').val();
                if($(this).find("input[class*='in_hora_i']").val()==''){
                    pasaitinerario=0;
                }
                DATAa.push(item);
            });
            INFOa  = new FormData();
            aInfoa   = JSON.stringify(DATAa);
        //========================================
            var datos='clientes='+aInfoa;
        if(pasaitinerario==1){
            $.ajax({
                type:'POST',
                url: base_url+'index.php/Listado_unidades/registraritinerario',
                data: datos,
                success:function(data){
                    limpiaritinerario()
                    itinerario_data_view();
                    cargainforma();
                }
            });    
        }else{
            toastr.error('Favor de completar todos los horarios');
        }
        
    }else{
        toastr.error('Favor de corregir los horarios');
    }
  });
  obtenernoti_caras();

});
function cargainforma(){
    var fecha = $('#calendarinput').val();
      $.ajax({
        type:'POST',
        url: base_url+'index.php/Configuracionrentas/obtenciondatos',
        data: {
          tecnico:$('#tecnico option:selected').val(),
          fechaselect:fecha
        },
        success:function(data){
            console.log(data);
            var array = $.parseJSON(data);
            $('#infordatac').attr('data-badge',array.contratos);
            $('#infordatap').attr('data-badge',array.poliza);
            $('#infordatae').attr('data-badge',array.evento);
            $('#infordatav').attr('data-badge',array.venta);
            var totalservicios=parseFloat(array.contratos)+parseFloat(array.poliza)+parseFloat(array.evento)+parseFloat(array.venta);
            $('#infordatag').attr('data-badge',totalservicios);
            if(array.itinerario>0){
                $('.infordatag').data('itinerario',array.itinerario);
            }else{
                $('.infordatag').data('itinerario',array.itinerario);
            }
        }
    });
  cargaunidades();
  obtenernoti_caras();
}
function cargaunidades(){
    var tecnico = $('#tecnico').val();
    tables.destroy();
    tables = $('#tableunidades').DataTable({
        stateSave: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "searching": false,
        "ajax": {
            "url": base_url+"index.php/Listado_unidades/getlistasunidades",
            type: "post",
            "data": {
                'tecnico':tecnico
            },
        },
        "columns": [
            
            {"data": "modelo"},
            {"data": "marca"},
            {"data": "placas"}, 
            
            {"data": null,
                render:function(data,type,row){
                    var html='';
                   //var html+='<button class="btn-bmz blue gradient-shadow" onclick="modal_series_file('+row.compraId+')">File</button>';
                    if(row.file_poliza!='null' || row.file_poliza!=''){
                        html+='<a class="btn-bmz blue gradient-shadow btn-accion" href="'+base_url+'uploads/unidades_poliza/'+row.file_poliza+'" target="_black" title="poliza"><i class="fas fa-file-image"></i></a> ';
                    }
                      //html+='<a class="btn-bmz blue gradient-shadow btn-accion" onclick="itinerario('+row.id+')"  title="Itinerario"><i class="fas fa-list-ul"></i></a> ';
                      html+='<a class="btn-bmz blue gradient-shadow btn-accion" onclick="incidencias('+row.id+')"  title="Incidencias"><i class="fas fa-clipboard-list fa-fw"></i></a> ';
                      html+='<input type="numeric" id="idunidad" value="'+row.id+'" style="display:none">';
                       
                    return html;
                }
            }
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[25, 50, 100], [25, 50,100]],
        
    });
}
function incidencias(id){
    idunidad=id;
    $('#modalinfoincidencias').modal('open');
    upload_data_view(id);
}
function upload_data_view(id){
    var fecha = $('#calendarinput').val();
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Listado_unidades/lis_incidencias",
        data: {
          id:id
        },
        success: function (response){
           $('.addtablelistincidencias').html(response);
           $('#table_inci').DataTable({
            responsive: !0,
            "order": [[ 0, "desc" ]]
           });
              
        },
        error: function(response){
            notificacion(1);
        }
    });
}
function itinerario(){
    //idunidad=id;
  $('#modalitinerario').modal('open');
  itinerario_data_view();
}
function itinerario_data_view(){
    var id = $('#idunidad').val();
    if(id>0){
    }else{
        id=0;
    }
    var fecha = $('#calendarinput').val();
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Listado_unidades/lis_itinerario",
        data: {
          id:id,
          fecha:fecha,
          idpersonal:$('#tecnico option:selected').val()
        },
        success: function (response){
           $('.tableitinerario').html(response);
           $('#table_itinerario').DataTable({
            "lengthMenu": [[20,25, 50, 100,200,400,-1], [20,25, 50,100,200,400,'todo']],
            responsive: !0,
            "order": [[ 1, "desc" ]]
           });
           var numberiti=$('.tr_itinerario').length;
           console.log('num iti:'+numberiti);
           if(numberiti==0){
            obtenerclientes(); 

           }else{
            $('#registraritinerario').hide();
            $('.tbcliii').html('');
           } 
            $('.vinc_c').click(function(event) {
                console.log('se hizo click');
                  $(this).addClass('clicked');
                  //$(this).toggleClass('clicked');// esta funcion es para agregar y quitar
            });
        },
        error: function(response){
            notificacion(1);
        }
    });
}
function deleteitiner(id){
    $.confirm({
        boxWidth: '80%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: 'Confirma la eliminacion?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
              //========================================================
              $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Listado_unidades/deleteitiner",
                    data: {
                      id:id
                    },
                    success: function (response){
                       toastr.success('Registro eliminado','Hecho!');
                        itinerario_data_view();
                          
                    },
                    error: function(response){
                        notificacion(1);
                    }
                });
              //===================================================
              },
            cancelar: function () {
                
            }
        }
    });
}
function edititinerario(id){
    iditinerario=id;
    var des = $('.edititinerario_'+id).data('des');
    var hi = $('.edititinerario_'+id).data('hi');
    var hf = $('.edititinerario_'+id).data('hf');
    var kmf = $('.edititinerario_'+id).data('kmf');
    $('#in_descrip').val(des);
    $('#in_hora_i').val(hi);
    $('#in_hora_f').val(hf);
    $('#in_km_f').val(kmf);
}
function limpiaritinerario(){
    iditinerario=0;
    $('#in_descrip').val('');
    $('#in_hora_i').val('');
    $('#in_hora_f').val('');
    $('#in_km_i').val('');
    $('#in_km_f').val('');
    $('#in_km_f').val('');
}
function obtenerclientes(){
    $('.tbcliii').html('');
    $.ajax({
        type:'POST',
        url: base_url+"index.php/AsignacionesTecnico/clientes4",
        data: {
          tecnico:$('#tecnico option:selected').val(),
          tipocpe:1,
          fechaselect:$('#calendarinput').val()
        },
        success: function (response){
           var array = $.parseJSON(response);
           console.log(array);
           var row_ser=0;
            $.each(array, function(index, item) {
                var btn_ll='';
                if(item.idrow!='0'){
                    if(item.longitud>0 || item.latitud>0){
                        btn_ll='<a class="b-btn b-btn-primary" onclick="view_coor('+item.longitud+','+item.latitud+')"><i class="fas fa-map-marked-alt"></i></a>';
                    }else{
                        var idrow="'"+item.idrow+"'";
                        btn_ll='<a class="b-btn b-btn-primary" onclick="savecoor('+idrow+')"><i class="fas fa-map-marker"></i></a>';
                    }
                }else{
                    btn_ll=item.idrow;
                }
                var btn_rechazo=' <a class="b-btn b-btn-danger" onclick="notificacionrechazogeneral('+item.asig+','+item.tipo+')"><i class="fas fa-ban"></i></a>';
                if(item.prioridad2>7){
                    if(item.hora_cri_ser== null){
                        var mv_h=0;
                    }else{
                        if(item.hora_cri_ser!=''){
                            var mv_h=1;
                        }else{
                            var mv_h=0;
                        }
                    }
                    
                }else{
                    var mv_h=0;
                }
                var h_fijo='';
                if(mv_h==1){
                    var readonly = ' readonly ';h_fijo='<span style="color:red">Horario Fijo</span>';
                }else{
                    var readonly = '';h_fijo='';
                }
                if(item.fijo==1){
                    var readonly = ' readonly ';h_fijo='<span style="color:red">Horario Fijo</span> ';
                }else if(item.hora==item.horafin){
                    var readonly = ' readonly ';h_fijo='<span style="color:red">Horario Fijo</span> ';
                }
                if(item.hora_cri_ser=='00:00:00' || item.hora_cri_ser == null){
                    var hora_cri_ser =item.hora;
                }else{
                    var hora_cri_ser =item.hora_cri_ser;
                }
                console.log(item.vinc);
                var vinc_c=sustituirCaracteres(item.vinc);
                var trhtml='<tr>\
                            <td>\
                                <input type="hidden" id="in_asig" value="'+item.asig+'">\
                                <input type="hidden" id="in_asig_g" value="'+item.asigg+'">\
                                <input type="hidden" id="vinc" value="'+item.vinc+'">\
                                <input type="hidden" id="in_tipo" value="'+item.tipo+'">\
                                <input type="hidden" id="hora_limite" value="'+item.horafin+'">\
                                <input type="hidden" id="mv_hora" value="'+mv_h+'">\
                                <input type="hidden" class="cli" value="'+item.cli+'">';
                            trhtml+='<input type="hidden" id="hora_comida" value="'+item.hora_comida+'">';
                            trhtml+='<input type="hidden" id="horafin_comida" value="'+item.horafin_comida+'">';
                            trhtml+='<label for="in_hora_i_'+row_ser+'">Hora Inicial</label>';

                            trhtml+='<input type="time" class="form-control-bmz in_hora_i in_hora_i_'+row_ser+'" id="in_hora_i_'+row_ser+'" name="in_hora_i_'+row_ser+'" ';
                            trhtml+=' min="'+item.hora+'" max="'+item.horafin+'" data-cli="'+item.cli+'" data-row="'+row_ser+'" data-hora="'+item.hora+'"  data-horafin="'+item.horafin+'" data-horacomida="'+item.hora_comida+'" data-horafincomida="'+item.horafin_comida+'" ';
                            trhtml+=' onChange="verhoras('+row_ser+',0)" value="'+hora_cri_ser+'" '+readonly+' >';
                            trhtml+='<p class="hrs_info">'+h_fijo+'horario disponible '+item.hora+' a '+item.horafin+'</p>';
                            if(item.hora_comida!='' && item.hora_comida!='00:00:00'){
                                trhtml+='<p class="hrs_info">Comida<br>'+item.hora_comida+' a '+item.horafin_comida+'</p>';
                            }
                            trhtml+='<label class="in_hora_f">Hora Final</label><input type="time" class="form-control-bmz in_hora_f" id="in_hora_f">\
                                <label>Km Final</label><input type="number" class="form-control-bmz" id="in_km_f">\
                            </td>\
                            <td><label>Cliente</label><input type="text" class="form-control-bmz" id="in_descrip" readonly value="'+item.emp+'">'+item.direccion+btn_ll+btn_rechazo+'<div class="vinc_group">'+vinc_c+'</div></td></tr>';
                $('.tbcliii').append(trhtml);
                row_ser++;
            });
            if(array.length>0){
                $('#registraritinerario').show();
            }
            if(row_ser>0){
                $('#registraritinerario').show();
            }
            $('#formser').validate();
        },
        error: function(response){
            toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema");
        }
    });
}
function editar_hora(id,unidadid,idunidadactual){
    verhoras(id,1);
    var validform =$('#form_t_itinerario').valid();
    if(validform){
        var hora_inicio_v=$('.in_hora_i_'+id).val();
        
        if(hora_inicio_v!=''){
            $.ajax({
                type:'POST',
                url: base_url+"index.php/Listado_unidades/editar_hora",
                data: {
                  id:id,
                  hora_inicio:hora_inicio_v,
                  hora_fin:0,
                  km_fin:$('.in_km_f_'+id).val(),
                  unidadid:unidadid,
                  idunidadactual:idunidadactual
                },
                success: function (response){
                   
                },
                error: function(response){
                    toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema");
                }
            });
        }
    }else{
        toastr.error('Favor de corregir los horarios');
    }
}
function editar_km(id,unidadid,idunidadactual){
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Listado_unidades/editar_km",
        data: {
          id:id,
          km_fin:$('.in_km_f_'+id).val(),
          unidadid:unidadid,
          idunidadactual:idunidadactual
        },
        success: function (response){
           
        },
        error: function(response){
            toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema");
        }
    }); 
}
toastr.options = {
  "closeButton": false,
  "debug": false,
  "newestOnTop": false,
  "progressBar": true,
  "positionClass": "toast-top-full-width",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "13000",
  "timeOut": "7000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}
function notificacionrechazogeneral(asignacion,tipo){
    var htmlnr='';
    htmlnr+='<label>Tipo</label>';
    htmlnr+='<select id="comtipo" class="form-control-bmz"><option value="0">Otro</option><option value="1">backup</option><option value="2">no se acudio al sitio</option><option value="3">sin acceso</option><option value="4">oficina cerrada</option><option value="5">intercambio de servicio</option></select>';
    htmlnr+='<label>Motivo</label>';
    htmlnr+='<textarea class="form-control-bmz" id="notificarerror"></textarea>';

    htmlnr+='<label>Se necesita permisos de administrador</label><br>';
    htmlnr+='<input type="text" placeholder="Contraseña" id="contrasena3" name="contrasena3" class="name form-control-bmz" required style="width: 99%;"/>';
    $.confirm({
        boxWidth: '90%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Motivo Rechazo',
        content: htmlnr,
        type: 'red',
        typeAnimated: true,
        buttons: 
        {
            'Confirmar rechazo': function (){
                var info=$('#notificarerror').val();
                var comtipo=$('#comtipo  option:selected').val();
                var pass=$("input[name=contrasena3]").val();
                if(pass!=''){
                    if(info!=''){
                        $.ajax({
                            type:'POST',
                            url: base_url+'index.php/AsignacionesTecnico/notificacionrechazogeneral',
                            data: {
                                asignacion:asignacion,
                                tipo:tipo,
                                info:info,
                                comtipo:comtipo,
                                pass:pass
                            },
                            success:function(data){
                                var array = $.parseJSON(data);
                                if(array.v_pass==0){
                                    toastr.success("Rechazo procesado");    
                                }else if(array.v_pass==1){
                                    if(array.permiso==1){
                                        toastr.success("Rechazo procesado"); 
                                    }else{
                                        notificacion(2);
                                        notificacionrechazogeneral(asignacion,tipo);
                                    }
                                }
                                itinerario();
                            }
                        });
                    }else{
                        toastr.error('Favor de agregar Motivo del rechazo');
                        notificacionrechazogeneral(asignacion,tipo);
                    }
                }else{
                    toastr.error('Ingrese una contraseña');
                    notificacionrechazogeneral(asignacion,tipo);
                }
                
            },
            Salir: function (){
            }
        }
    });
}
function verhoras(row,tipo){
    var cli =$('.in_hora_i_'+row).data('cli');
    var hora =$('.in_hora_i_'+row).val();

    
    if(tipo==0){
        var serv = $("#table_clientesininerario tbody > tr");
    }else{
        var serv = $("#table_itinerario tbody > tr");
    }
    

    var clirow=cli;
    serv.each(function(){
        var cli_se=$(this).find("input[id*='in_hora_i']").data('cli');
        var hora_se=$(this).find("input[class*='in_hora_i']").val();
        if(hora_se!=''){

            var h_comida =$(this).find("input[id*='in_hora_i']").data('horacomida');
                if(h_comida==''){
                    h_comida='00:00:00';
                }
            var hf_comida =$(this).find("input[id*='in_hora_i']").data('horafincomida');
            var mv_hora = $(this).find("input[id*='mv_hora']").val();
            var hora_se_form=hora_se;
                hora_se_form = hora_se_form.split(':');

                min_se_form =parseInt(hora_se_form[1]);
                hora_se_form =parseInt(hora_se_form[0]);


            var horafin_o=$(this).find("input[id*='in_hora_i']").data('horafin');
            //var horafin =parseInt(horafin_o)-1;
                horasf = horafin_o.split(':');
                console.log(horasf[0]);
                console.log(horasf[1]);
                var horafin = horasf[0];
                    //horafin=parseInt(horafin)-1;
                    horafin=parseInt(horafin);
                var minutofin = parseInt(horasf[1]);
            //var row_se=$(this).find("input[id*='in_hora_i']").data('row');

            if(cli_se!=cli){
                if(hora_se==hora){
                    toastr.error('no se permite tener el mismo horario entre diferentes clientes');
                    $('.in_hora_i_'+row).val('');
                }
            }
            if(hora_se_form > horafin){
                if(min_se_form>=minutofin){
                    toastr['error']('no se permite se excedio el horario asignado como limite de una hora antes del asignado: '+horafin_o);
                    //$('.in_hora_i_'+row).val('');
                    $(this).find("input[id*='in_hora_i']").val('')
                }
            }
            //==============================================
            if(mv_hora==0){
                if(h_comida!='00:00:00'){
                    var h_comida_split=h_comida.split(':');
                    var h_comida_h = h_comida_split[0];
                    var h_comida_m = h_comida_split[1];

                    var hf_comida_split=hf_comida.split(':');
                    var hf_comida_h = hf_comida_split[0];
                    var hf_comida_m = hf_comida_split[1];

                    //===========================
                    var horaVerificar = new Date();
                        horaVerificar.setHours(hora_se_form);
                        horaVerificar.setMinutes(min_se_form);

                        // Rango de horas
                    var horaInicio = new Date();
                        horaInicio.setHours(h_comida_h);
                        horaInicio.setMinutes(h_comida_m);

                    var horaFin = new Date();
                        horaFin.setHours(hf_comida_h);
                        horaFin.setMinutes(hf_comida_m);

                        // Verificar si la hora está fuera del rango
                        if (horaVerificar < horaInicio || horaVerificar >= horaFin) {
                            //console.log("La hora 13:34 no está dentro del rango de 14:00 a 15:00.");
                        } else {
                            //console.log("La hora 13:34 está dentro del rango de 14:00 a 15:00.");
                            toastr.error('La hora '+hora_se+' está dentro del rango de '+h_comida+' a '+hf_comida+' del horario de comida.');
                            $(this).find("input[class*='in_hora_i']").val('')
                        }
                }
            }
            //===============================================
        }
    });
    /*
    var horas = [];
    var clienterow=0;
    serv.each(function(){
        var cli_se=$(this).find("input[id*='in_hora_i']").data('cli');
        var hora_se=$(this).find("input[class*='in_hora_i']").val();
        var tr_row=$(this).find("input[id*='in_hora_i']").data('row');
        if(cli_se!=cli){
            if(hora_se!=''){
                if(clienterow!=cli_se){
                    var item = {};
                    item["id"] = 1;
                    item["hra"] = hora_se; 
                    horas.push(item);
                    clienterow=cli_se;
                }
               
            }
            
        }
        if(tr_row==row){
            if(hora_se!=''){
               var item = {};
                item["id"] = 1;
                item["hra"] = hora_se; 
                horas.push(item);
            }
        }
    });
    if(horas.length>1){
        horas.forEach(function (element) {
            element["minutos"] = convertirAMinutos(element["hra"]);
        });
        horas.sort((a, b) => a.minutos - b.minutos);

        // Verificar la diferencia entre las horas adyacentes
        var diferenciaMaxima = 30; // en minutos
        var notificacion = false;
        var horasNoCumplen = [];

        for (var i = 1; i < horas.length; i++) {
            var diferencia = horas[i]["minutos"] - horas[i - 1]["minutos"];
            if (diferencia <= diferenciaMaxima) {
                console.log('diferencia('+diferencia+') > diferenciaMaxima('+diferenciaMaxima+')');
                notificacion = true;
                horasNoCumplen.push(horas[i - 1]["hra"], horas[i]["hra"]);
                break;
            }
        }

        // Mostrar el resultado
        if (notificacion) {
            console.log("Las horas tienen una separación menor o igual a 30 min.");
            toastr.error('Las horas tienen una separación menor o igual a 30 min.');
            $('.in_hora_i_'+row).val('');
            console.log(horasNoCumplen);
        } else {
            console.log("Las horas tienen una separación de mas de 30 minutos.");
        }
    }
    */
}

function convertirAMinutos(hora) {
    var partesHora = hora.split(':');
    return parseInt(partesHora[0]) * 60 + parseInt(partesHora[1]);
}

function obtenernoti_caras(){
    statusrow_retrasos=0;
    $.ajax({
        type:'POST',
        url: base_url+'index.php/AsignacionesTecnico/obtenernotificacion',
        data: {
            tec:$('#tecnico').val()
        },
        success:function(data){
             $('.btn-notificacionstado').html(data);
        },
        error: function(response){
        }
    });
}
function quitarSegundos(tiempo) {
    // Verificar si la cadena contiene segundos (separados por ':')
    if (tiempo.includes(':')) {
        // Dividir la cadena en partes usando ':' como separador
        var partes = tiempo.split(':');
        
        // Verificar si hay tres partes (horas, minutos y segundos)
        if (partes.length === 3) {
            // Eliminar la última parte (segundos)
            partes.pop();
            // Unir las partes nuevamente con ':' como separador y devolver el resultado
            return partes.join(':');
        } else {
            // Si no hay tres partes, devolver la cadena original
            return tiempo;
        }
    } else {
        // Si no hay segundos en la cadena, devolver la cadena original
        return tiempo;
    }
}
function div_view_pre(view){
    var tec = $('#tecnico').val();
    var fecha=$('#calendarinput').val()

    var urlfac=base_url+"index.php/AsignacionesTecnico/prefacturas/"+tec+"/"+fecha+'/'+view;
    var htmliframe="<iframe src='"+urlfac+"' title='description' class='ifrafac'>";
    if(view==0){
        $('.view_info_pre').html(htmliframe);
    }
    if(view==1){
        $('.view_info_pre5').html(htmliframe);
    }
}
function primiso_desbloqueo(id){
    var html_contect='Para continuar se necesita permisos de administrador<br>'+
                    '<input type="text" placeholder="Contraseña" id="contrasena2" name="contrasena2" class="name form-control-bmz" style="width:99%"  onpaste="return false;" required/>';
    var isReadonly = $('#in_hora_i_'+id).prop('readonly');
    if(isReadonly){
        $.confirm({
            boxWidth: '30%',
            useBootstrap: false,
            icon: 'fa fa-warning',
            title: 'Atención!',
            content: html_contect,
            type: 'red',
            typeAnimated: true,
            buttons:{
                confirmar: function (){
                    //var pass=$('#contrasena2').val();
                    var pass=$("input[name=contrasena2]").val();
                    if (pass!='') {
                         $.ajax({
                            type:'POST',
                            url: base_url+"index.php/Sistema/solicitarpermiso_l",
                            data: {
                                pass:pass,
                                per:12,
                                per2:54
                            },
                            success: function (response){
                                var respuesta = parseInt(response);
                                if (respuesta==1) {
                                    toastr.success('Tiene permisos, ya puede editar el campo');
                                    //$('#in_hora_i_'+id).prop('readonly',false);
                                    $('.in_hora_i').prop({'readonly':false});
                                }else{
                                    notificacion(2);
                                }
                            },
                            error: function(response){
                                notificacion(1);
                            }
                        });
                    }else{
                        notificacion(0);
                    }
                },/*
                'Ver Pendientes': function (){
                    obtenerventaspendientesinfo(idcliente);
                },*/
                cancelar: function (){

                }
            }
        });
    }
    setTimeout(function(){
        new MaskedPassword(document.getElementById("contrasena2"), '\u25CF');
    },500);
}
function sustituirCaracteres(texto) {
  // Reemplazar '|' por 'A'
  let resultado = texto.replace(/\|/g, '<a class="vinc_c b-btn b-btn-primary btn-sm" onclick="obternerdatosservicio(');
  // Reemplazar '°' por 'X'
  resultado = resultado.replace(/°/g, ')"><i class="fas fa-info-circle infoicon"></i></a> ');
  return resultado;
}
/*---------------------*/
function solicitudr(ser,serve,tipo,tipod){
    var modelo='';
    var serie='';
    if(tipo==1){//contrato
        modelo=$('.equipo_'+ser+'_'+serve+'_'+tipo).data('modelo');
        serie=$('.equipo_'+ser+'_'+serve+'_'+tipo).data('serie');
    }
    if(tipo==2){//poliza
        modelo=$('.equipo_'+ser+'_'+serve+'_'+tipo).data('modelo');
        serie=$('.equipo_'+ser+'_'+serve+'_'+tipo).data('serie');
    }
    if(tipo==3){//evento
        modelo=$('.equipo_'+ser+'_'+serve+'_'+tipo).data('modelo');
        serie=$('.equipo_'+ser+'_'+serve+'_'+tipo).data('serie');
    }
    if(tipo==4){//venta
        modelo=$('.equipo_'+ser+'_'+serve+'_'+tipo).data('modelo');
        serie=$('.equipo_'+ser+'_'+serve+'_'+tipo).data('serie');
    }
    
    var title_ser='';
    if(tipod==1){
        title_ser=' por desgaste';
    }
    if(tipod==2){
        title_ser='por daño';
    }
    if(tipod==3){
        title_ser=' por garantia';
    }
    var html='';
        html+='<div class="row">';
            html+='<div class="col s12 laben_prio">';
                html+='<input name="prioridadr" type="radio" id="prioridadr_1" value="1" /><label for="prioridadr_1">Prioridad 1</label>';
                html+='<input name="prioridadr" type="radio" id="prioridadr_2" value="2" /><label for="prioridadr_2">Prioridad 2</label>';
                html+='<input name="prioridadr" type="radio" id="prioridadr_3" value="3" /><label for="prioridadr_3">Prioridad 3</label>';
            html+='</div>';
        html+='</div>';
        html+='<div class="row">';
            html+='<div class="col s12">';
                html+='<label>Descripcion</label>';
                html+='<textarea id="solicitudrefaccioncpe"></textarea>';
            html+='</div>';
        html+='</div>';
        html+='<div class="row">';
            html+='<div class="col s12">';
                html+='<label>Contador Final General</label>';
                html+='<input type="number" class="form-control-bmz" id="cot_contador">';
            html+='</div>';
        html+='</div>';
        html+='Se necesita permisos de administrador para editar<br>'+
                '<input type="text" placeholder="Contraseña" id="contrasena3" name="contrasena3" class="name form-control" required/>';
   
    $.confirm({
        boxWidth: '90%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Solicitud de refaccion '+title_ser+' '+modelo+' '+serie,
        content: html,
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var refa =$('#solicitudrefaccioncpe').val();
                var prioridadr=$('input:radio[name=prioridadr]:checked').val();
                var cot_contador = $('#cot_contador').val();
                var refa =$('#solicitudrefaccioncpe').val();
                //=======================
                var pass=$("input[name=contrasena3]").val();
                if (pass!='') {
                     $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Sistema/solicitarpermiso_l",
                        data: {
                            pass:pass,
                            per:12,
                            per2:54
                        },
                        success: function (response){
                                var respuesta = parseInt(response);
                                if (respuesta==1) {
                //=============================================
                    
                    if(cot_contador==''){
                        $.alert({boxWidth: '30%',useBootstrap: false,
                            title: 'Advertencia!',
                            content: 'Debe de ingresar el contador!'});
                        return false;
                    }
                    
                    if (refa!='') {
                        if(prioridadr>0){
                            $.ajax({
                                type:'POST',
                                url: base_url+'index.php/AsignacionesTecnico/solicitudrefaccion',
                                data: {
                                    ser: ser,
                                    serve: serve,
                                    tipo: tipo,
                                    tipod: tipod,
                                    refaccion: refa,
                                    prioridad: prioridadr,
                                    cot_contador:cot_contador
                                },
                                async: false,
                                statusCode:{
                                    404: function(data){
                                        toastr["error"]("Error 404");
                                    },
                                    500: function(){
                                        toastr["error"]("Error 500");
                                    }
                                },
                                success:function(data){
                                    console.log(data);
                                    var rows = parseInt(data);
                                    if(rows>0){
                                        var class_eq='.equipo_'+ser+'_'+serve+'_'+tipo+'.sol_ref_'+tipod;
                                        console.log(class_eq);
                                        $(class_eq).removeClass('b-btn-primary').addClass('b-btn-success');
                                    }
                                    toastr["success"]("Solicitud se a enviado!");
                                }
                            });
                        }else{
                            solicitudr(ser,serve,tipo,tipod);
                            swal("Debe de seleccionar el tipo de prioridad")
                        }
                    }else{
                        solicitudr(ser,serve,tipo,tipod);
                        $.alert({boxWidth: '30%',useBootstrap: false,
                            title: 'Advertencia!',
                            content: 'No debe de contener campos vacíos!'});
                        return false;
                    }
                //=============================================
                                }else{
                                    notificacion(2);
                                }
                        },
                        error: function(response){
                            notificacion(1);
                        }
                    });
                }else{
                    notificacion(0);
                }
            },
            cancelar: function () {

            }
        }
    });
    
    setTimeout(function(){ 
        obtenerinfosolicitudrefaccion(ser,serve,tipo,tipod);
    }, 900);
    setTimeout(function(){
        new MaskedPassword(document.getElementById("contrasena3"), '\u25CF');
    },500);
}
function obtenerinfosolicitudrefaccion(ser,serve,tipo,tipod){
    $.ajax({
        type:'POST',
        url: base_url+'index.php/AsignacionesTecnico/obtenerinfosolicitudrefaccion',
        data: {
            ser: ser,
            serve: serve,
            tipo: tipo,
            tipod: tipod
        },
        async: false,
        statusCode:{
            404: function(data){
                toastr["error"]("Error 404");
            },
            500: function(){
                toastr["error"]("Error 500");
            }
        },
        success:function(data){
            var array = $.parseJSON(data);
            console.log(array);
            $('input[name=prioridadr][value='+array.prioridad+']').prop('checked',true);
            $('#solicitudrefaccioncpe').val(array.cot_refaccion);
            $('#cot_contador').val(array.cot_contador);
        }
    });
}

function editar_hrs_ser(tipo,serid){
    var html='';
    html+='<div class="row">';
            html+='<div class="col s6">';
                html+='<label >Hora Inicio</label>';
                html+='<input name="h_ini_e" type="time" id="h_ini_e" class="form-control-bmz" />';
            html+='</div>';
            html+='<div class="col s6">';
                html+='<label >Hora Fin</label>';
                html+='<input name="h_fin_e" type="time" id="h_fin_e" class="form-control-bmz" />';
            html+='</div>';
        html+='</div>';
    html +='Para continuar se necesita permisos de administrador<br>';
    html +='<input type="text" placeholder="Contraseña" id="contrasena2" name="contrasena2" class="name form-control-bmz" style="width:99%"  onpaste="return false;" required/>';
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Editar hora de Inicio y Fin del servicio',
        content: html,
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var h_ini_e =$('#h_ini_e').val();
                var h_fin_e =$('#h_fin_e').val();
                //var pass=$('#contrasena2').val();
                //console.log(pass);
                var pass=$("input[name=contrasena2]").val();
                if (pass!='') {
                     $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Sistema/solicitarpermiso",
                        data: {
                            pass:pass
                        },
                        success: function (response){
                            var respuesta = parseInt(response);
                            if (respuesta==1) {
                                //==================================================
                                    $.ajax({
                                        type:'POST',
                                        url: base_url+'index.php/AsignacionesTecnico/editar_hrs_ser',
                                        data: {
                                            ser: serid,
                                            tipo: tipo,
                                            hini: h_ini_e,
                                            hfin: h_fin_e
                                        },
                                        async: false,
                                        statusCode:{
                                            404: function(data){
                                                toastr["error"]("Error 404");
                                            },
                                            500: function(){
                                                toastr["error"]("Error 500");
                                            }
                                        },
                                        success:function(data){
                                            toastr["success"]("Editado Correctamente");
                                            obtenerserviciosreport();
                                        }
                                    });
                                //==================================================
                            }else{
                                editar_hrs_ser(tipo,serid);
                                notificacion(2);
                            }
                        },
                        error: function(response){
                            notificacion(1);
                        }
                    });
                }else{
                    notificacion(0);
                }
            },/*
            'Ver Pendientes': function (){
                obtenerventaspendientesinfo(idcliente);
            },*/
            cancelar: function (){

            }
        }
    });
    setTimeout(function(){
        new MaskedPassword(document.getElementById("contrasena2"), '\u25CF');
    },900);
}