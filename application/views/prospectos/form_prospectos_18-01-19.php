<?php 
// Validamos si es creación o edición, para así cargar los datos respectivamente
if(isset($prospecto))
{
    $idProspecto = $prospecto->id;
    $empresa = $prospecto->empresa;
  //  $persona_contacto = $prospecto->persona_contacto;
    $puesto_contacto = $prospecto->puesto_contacto;
    $email = $prospecto->email;
    $estado = $prospecto->estado;
    $municipio = $prospecto->municipio;
    $giro = $prospecto->giro;
    $direccion = $prospecto->direccion;
    $observaciones = $prospecto->observaciones;
}
else
{
    $idProspecto = 0;
    $empresa = '';
  //  $persona_contacto = '';
    $puesto_contacto = '';
    $email = '';
    $estado = 0;
    $municipio = '';
    $giro = '';
    $direccion = '';
    $observaciones = '';
} 

if($tipoVista==1)
{
  $label = 'Formulario de Alta de Prospecto';
}
else if($tipoVista==2)
{
  $label = 'Formulario de Edición de Prospecto';
}
else if($tipoVista==3)
{
  $label = 'Formulario de Visualización de Prospecto';
}
?>

<!-- START CONTENT --> 
        <section id="content" width="100%">
          

          <!--start container-->
          <div class="container" width="100%">
            <div class="section">
                <h4 class="caption"><?php echo $label; ?></h4>
              <div class="row">
                <input id="base_url" type="hidden" name="base_url" value="<?php echo base_url(); ?>">
                <!-- FORMULARIO -->
                <input id="tipoVista" type="hidden" name="tipoVista" value="<?php echo $tipoVista; ?>">

                  <div class="col s12 m12 24">

                    <div class="card-panel">

                      <div class="row">
                         <form class="form" id="prospectos_from" method="post">

                          <?php 
                            if(isset($prospecto)) 
                            {
                          ?>
                            <input id="idProspecto" name="idProspecto" type="hidden" value="<?php echo $idProspecto; ?>">
                          <?php
                            }
                          ?>

                          <div class="row">
                            <div class="input-field col s4">
                              <i class="material-icons prefix">location_city</i>
                              <input id="empresa" name="empresa" type="text" placeholder="Empresa" value="<?php echo $empresa; ?>">
                            </div>
                            <div class="input-field col s4">
                              <i class="material-icons prefix">person</i>
                              <input id="puesto_contacto" name="puesto_contacto" type="text"  value="<?php echo $puesto_contacto; ?>">
                              <label for="puesto_contacto">Puesto de contacto</label>
                            </div>
                            <div class="input-field col s4">
                              <i class="material-icons prefix">email</i>
                              <input id="email" name="email"  type="email" value="<?php echo $email; ?>">
                              <label for="email">Correo electrónico</label>
                            </div>
                          </div>

                          <div class="row">
                            <div class="input-field col s4">
                              <i class="material-icons prefix">local_library</i>
                             <select id="estado" name="estado">
                                  <option value="" disabled selected>Selecciona una opción</option>
                                    <?php foreach($estados as $estado_elegido)
                                    { 
                                    ?>
                                      <option value="<?php echo $estado_elegido->EstadoId?>" <?php if($estado_elegido->EstadoId==$estado) echo "selected";   ?> ><?php echo $estado_elegido->Nombre; ?></option>
                                    <?php }?>
                              </select>
                              <label for="estado">Estado</label>
                            </div>

                            <div class="input-field col s4">
                              <i class="material-icons prefix">local_library</i>
                              <input id="municipio" name="municipio"  type="text" value="<?php echo $municipio; ?>">
                              <label for="municipio">Municipio</label>
                            </div>

                            <div class="input-field col s4">
                              <i class="material-icons prefix">local_offer</i>
                              <input id="giro" name="giro"  type="text" value="<?php echo $giro; ?>">
                              <label for="giro">Giro de la Empresa</label>
                            </div>
                          </div>

                          <div class="row">
                            <div class="input-field col s12">
                              <i class="material-icons prefix">directions</i>
                              <textarea id="direccion" name="direccion" class="materialize-textarea"><?php echo $direccion; ?></textarea>
                              <label for="message">Dirección</label>
                            </div>
                          </div>

                          <div class="row">
                            <div class="input-field col s12">
                              <i class="material-icons prefix">question_answer</i>
                              <textarea id="observaciones" name="observaciones" class="materialize-textarea"><?php echo $observaciones; ?></textarea>
                              <label for="message">Observaciones</label>
                            </div>
                          </div>

<!-- - --->

                          <div class="row">
                            <div class="input-field col s4">
                            <?php  
                                  $i=0;
                                  $size=1;
                                  do{
                                  ?>
                                  <div  class="border_tel" id="tel_dato1">
                                    <div class="input-field col s10">
                                    <i class="material-icons prefix">call</i>
                                    <input id="tel_local" name="tel_local" type="text" >
                                    <label for="tel_local">Teléfono Local</label>
                                    </div>
                                    <div class="input-field col s2 div_btn_tel">
                                    <?php if($i==0){ ?>
                                    <a class="btn btn-floating waves-effect waves-light cyan btn_agregar_tel"> <i class="material-icons">add</i></a>
                                    <?php } else {?>
                                    <a class="btn btn-floating waves-effect waves-light red btn_eliminar_tel"><i class="material-icons">delete_forever</i></a>
                                    <?php }?>
                                    </div>

                                  </div>
                                  <?php 
                                  $i++;
                                  }while($i<$size);
                                  ?>
                            </div>
                            <div class="input-field col s4">
                                  <?php  
                                  $i=0;
                                  $size=1;
                                  do{
                                  ?>
                                  <div  class="border_cel" id="cel_datos1">
                                    <div class="input-field col s10">
                                    <i class="material-icons prefix">smartphone</i>
                                    <input id="celular" name="celular"  type="text" autocomplete="off" >
                                    <label for="celular">Celular</label>
                                    </div>
                                    <div class="input-field col s2 div_btn_cel">
                                    <?php if($i==0){ ?>
                                    <a class="btn btn-floating waves-effect waves-light cyan btn_agregar_cel"> <i class="material-icons">add</i></a>
                                    <?php } else {?>
                                    <a class="btn btn-floating waves-effect waves-light red btn_eliminar_cel"><i class="material-icons">delete_forever</i></a>
                                    <?php }?>
                                    </div>

                                  </div>
                                  <?php 
                                  $i++;
                                  }while($i<$size);
                                  ?>

                            </div>

                            <div class="input-field col s4">
                                  <?php  
                                  $i=0;
                                  $size=1;
                                  do{
                                  ?>
                                  <div  class="border_contacto" id="contacto_datos1">
                                    <div class="input-field col s10">
                                    <i class="material-icons prefix">account_circle</i>
                                    <input id="persona_contacto" name="persona_contacto" type="text">
                                    <label for="persona_contacto">Persona de contacto</label>
                            
                                    </div>
                                    <div class="input-field col s2 div_btn_contacto">
                                    <?php if($i==0){ ?>
                                    <a class="btn btn-floating waves-effect waves-light cyan btn_agregar_contacto"> <i class="material-icons">add</i></a>
                                    <?php } else {?>
                                    <a class="btn btn-floating waves-effect waves-light red btn_eliminar_contacto"><i class="material-icons">delete_forever</i></a>
                                    <?php }?>
                                    </div>

                                  </div>
                                  <?php 
                                  $i++;
                                  }while($i<$size);
                                  ?>

                            </div>
                            
                          </div>






                        <div class="row">
                            <div class="col s4">
                               <?php 
                                if($tipoVista!=1)
                                {
                                ?> 
                                <h4 class="caption">Tabla de Teléfonos</h4>
                                <div class="row">   
                                   <div class="col s12">
                                        <table class="table table-striped table-condensed table-hover table-responsive" id="tabla_tel_local" name="tabla_tel_local">
                                          <thead>
                                              <tr>
                                                  <th>#</th>
                                                  <th>Teléfono local</th>
                                                  <th>idCliente</th>
                                                  <th></th>
                                              </tr>
                                          </thead>

                                          <tbody >
                                          </tbody>
                                          
                                      </table> 
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                            
                            <div class="col s4">
                                <?php 
                                if($tipoVista!=1)
                                {
                                ?> 
                                <h4 class="caption">Tabla de Teléfonos</h4>
                                <div class="row">   
                                   <div class="col s12">
                                        <table class="table table-striped table-condensed table-hover table-responsive" id="tabla_tel_celular" name="tabla_tel_celular">
                                          <thead>
                                              <tr>
                                                  <th>#</th>
                                                  <th>Teléfono celular</th>
                                                  <th></th>
                                                  <th></th>
                                              </tr>
                                          </thead>

                                          <tbody >
                                          </tbody>
                                          
                                      </table> 
                                    </div>
                                </div>
                                <?php } ?>
                            </div>

                            <div class="col s4">
                                <?php 
                                if($tipoVista!=1)
                                {
                                ?> 
                                <h4 class="caption">Tabla Persona de contacto</h4>
                                <div class="row">   
                                   <div class="col s12">
                                        <table class="table table-striped table-condensed table-hover table-responsive" id="tabla_persona_contacto" name="tabla_persona_contacto">
                                          <thead>
                                              <tr>
                                                  <th>#</th>
                                                  <th>Persona contacto</th>
                                                  <th></th>
                                                  <th></th>
                                              </tr>
                                          </thead>

                                          <tbody >
                                          </tbody>
                                          
                                      </table> 
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                         </div>

                          <div class="row">
                            <div class="input-field col s12">
                              <button class="btn cyan waves-effect waves-light right" type="submit">Guardar
                                <i class="material-icons right">send</i>
                              </button>
                            </div>
                          </div>
                        </form>
                        
                      </div>

                    </div>

                  </div>
                
              </div>
            </div>
          </div>
        </section>