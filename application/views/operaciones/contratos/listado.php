<style type="text/css">
   .redcolor{background: #ee6b0b !important;}
  table td,table th{font-size: 12px;padding: 6px 7px;}
  .espaciobtn{min-width: 180px;}
  .chosen-container{margin-left: 0px !important;}
  .jconfirm .jconfirm-cell{vertical-align:top;}
  .select2-container.select2-container--default.select2-container--open{top:262px !important;}
  .select2.select2-container.select2-container--default.select2-container--below.select2-container--open{top:0px !important;}
</style>
<div style="height: 0px;width: 0px;">
  <input type="password" name="password" style="position: inherit;">
  <input type="text" name="user" style="position: inherit;">
</div>

<!-- START CONTENT -->
<section id="content">
	<!--start container-->
  	<div class="container">
  		<div class="section">
    		<input id="base_url" type="hidden" name="base_url" value="<?php echo base_url(); ?>">
        <input id="perfilid" type="hidden" value="<?php echo $perfilid; ?>">

    		<div id="table-datatables">
          <h4 class="header">Listado de Contratos</h4>
          <div class="row">
            <div class="col s4">
              <select id="tipo" class="browser-default form-control-bmz" onchange="load()"><option value="1">Activo</option><option value="0">Eliminado</option></select>
            </div>
          </div>
          <div class="row">
            <div class="col s12">
              <table id="tabla_contrato" class="responsive-table display" cellspacing="0">
                <thead><tr><th>#</th><th>Cliente</th><th>Folio</th><th>Tipo Persona</th><th>vigencia</th><th>Vendedor</th><th>Fecha Solicitud</th><th>Fecha inicio</th><th>Arrendataria</th><th>RFC</th><th>File</th><th>Ejecutivo</th><th></th></tr></thead>
                <tbody></tbody></table>
            </div>
          </div>
        </div>
      </div>  
    </div>
</section>
<div id="modalcontratoejecutivo" class="modal"  >
  <div class="modal-content " >
    <input type="hidden" name="idcontratoejecu" id="idcontratoejecu">
    <h4>Seleccione la ejecutivo para el contrato.</h4>
    <br>
    <div class="col s12 m12 12" style="min-height:200px;">
        <div class="row"  >
          <div class="col s12">
            <label>Seleccione Ejecutivo</label>
            <select class="browser-default  chosen-select" id="ejecontrato" >
              <?php foreach ($ejecutivos->result() as $item) {
                echo '<option value="'.$item->personalId.'">'.$item->nombre.' '.$item->apellido_paterno.' '.$item->apellido_materno.'</option>';
              } ?>
            </select>
          </div>
        </div>
    </div>
    <div class="modal-footer">
      <a class="btn waves-effect waves-light green " id="agregarejecutivocontrato">Escoger</a>
      <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cancelar</a>
    </div>
  </div>
</div>
<div id="modalprefacturash" class="modal">
    <div class="modal-content ">
      <h4>Prefacturas</h4>
      <div class="row">
        <div class="col s12 m12 l12"><button class="b-btn b-btn-primary" onclick="obtenerequipos_de_contratos()" >Generar prefactura</button></div>
        <div class="col s12 m12 l12 historialprefacturas"></div>
      </div>
        
    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
    </div>
  </div>
</div>
<select class="browser-default  form-control-bmz" id="ejecontratoselect" style="display:none;" >
<?php foreach ($personalrows->result() as $item) { 
  echo '<option value="'.$item->personalId.'">'.$item->nombre.' '.$item->apellido_paterno.' '.$item->apellido_materno.'</option>';
} ?>
</select>


<div id="modalcontratoclonar" class="modal"  >
  <div class="modal-content " >
    <input type="hidden" name="idcontratoejecu" id="idcontratoejecu">
    <h4>Seleccione los Periodos a importar.</h4>
    <br>
    <div class="col s12 m12 12" style="min-height:200px;">
        <div class="row"  >
          <div class="col s12 tablefacturasall"></div>
        </div>
    </div>
    <div class="modal-footer">
      <button class="btn waves-effect waves-light green " id="okclonarcontrato">Escoger</button>
      <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cancelar</a>
    </div>
  </div>
</div>

<div id="modal_comentario" class="modal"  >
    <div class="modal-content ">
        <h4>Comentario</h4>
        <div class="row" > 
          <div class="col s12 comentariopre"></div>   
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
        </div>
    </div>
    <!-- Modal finaliza fin-->
</div>


<div id="modalseleccionequipos" class="modal" >
  <div class="modal-content" style="padding: 5px">
    <div class="row">
      <div class="col s12">
        <h4>Seleccion de equipos</h4>
      </div>
    </div>
    <div class="row">
      <div class="col s12 div_table_equipos"></div>
    </div>
  </div>  
  <div class="modal-footer">
    <button class="b-btn b-btn-success " id="okseleccionequipos">Aceptar</button>
    <a class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
  </div>
</div>
<div class="iframes_generate_cont" style="display:none;">
  <?php foreach ($random_cont->result() as $itemc) { ?>
      <iframe src="<?php echo base_url().'index.php/RentasCompletadas/view/'.$itemc->idRenta;?>"></iframe>
  <?php } ?>
</div>