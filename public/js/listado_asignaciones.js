var base_url = $('#base_url').val();
var table;
$(document).ready(function(){

    /*****  DATOS Y FUNCIONES PARA ASIGNAR DATOS A SERVICIOS ******** */ 
	$('.modal').modal();
	table = $('#tabla_asignar').DataTable();
	$('.asig_tec').click(function(event) {
		$('#modalAsigna').modal('open');
	});
	$('.ruta').click(function(event) {
		$('#modalRuta').modal('open');
	});
	$('.prioridad').click(function(event) {
		$('#modalPrioridad').modal('open');
	});
	$('.datos').click(function(event) {
		$('#modalDatos').modal('open');
		var tr = $(this).closest('tr');
        var row = table.row(tr);
        var data = row.data();
		$("#id_asig").val(data.id);
		datos_servicios(data.id);
	});
	$('.crea_orden').click(function(event) {
		$('#modalOrden').modal('open');
	});
	$('#conf_crea').click(function(event) {
		var number = $("#num_orden").val();
		$.alert({boxWidth: '30%',useBootstrap: false,title: 'Éxito!',content: 'Se ha creado la orden: '+number});
		setTimeout(function(){ 
            window.location.href = base_url+"index.php/Ordenes_asignadas"; 
        }, 2000); 
	});

	$('#ruta').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una opción',
        ajax: {
            url: base_url+'Asignaciones/searchRutas',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.id,
                        text: element.nombre

                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    });

    $('#tecnico').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una opción',
        ajax: {
            url: base_url+'Asignaciones/searchTecnicos',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.UsuarioID,
                        text: element.tecnico

                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    });
});

function detalle(id){
    window.open(base_url+"Asignaciones/view/"+id, "Prefactura", "width=780, height=612");
}

function datos_servicios(id){
    $.ajax({
        type:'POST',
        url: base_url+'index.php/Asignaciones/get_DatosAsigna',
        data: {id:id},
        success:function(data){
            console.log(data);
            var array = $.parseJSON(data);
            $('#direccion').html(array.direccion);
            $('#fecha').html(array.fecha);
            $("#hora").trigger(array.hora);
        }
    });
}