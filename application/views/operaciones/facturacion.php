<!-- START CONTENT -->
<section id="content" width="100%">
  
  <!--start container-->
  <div class="container" width="100%">
    <div class="section">
      <div class="row">
        <p class="caption col s6">Nuevo Facturación</p>
      </div>
      <div class="row">
        <input id="base_url" type="hidden" name="base_url" value="<?php echo base_url(); ?>">
        <!-- FORMULARIO -->
        <form class="col s12 " id="empleados-form">
          <div class="col s12 m12 24">
            <div class="card-panel">
              <div class="row">
                <div class="divider"></div>
                <h5 class="header">Datos fiscales</h5>
                <div class="row">
                  <div class="input-field col s6">
                    <i class="material-icons prefix"></i>
                    <textarea id="" name="" class="materialize-textarea" disabled="">Autopista México Puebla km 116 San Lorenzo Almecatla, Cuautlancingo, Peubla, México C.P. 72700</textarea>
                    <label for=""></label>
                  </div>
                  <div class="input-field col s6">
                    <i class="material-icons prefix"></i>
                    <textarea id="" name="" class="materialize-textarea" disabled="">Autopista México Puebla km 116 San VME640813HF6</textarea>
                    <label for=""></label>
                  </div>
                </div>  

                <div class="divider"></div>
                <h5 class="header">Configuración</h5>

                <div class="row">
                  <div class="col s5">
                    <i class="material-icons prefix"></i>
                    <input name="group1" type="radio" id="test1" checked="">
                    <label for="test1">Mensual</label>
                  </div>
                  <div class="col s5">
                    <i class="material-icons prefix"></i>
                    <input name="group1" type="radio" id="test2">
                    <label for="test2">Anual Acumulativo (excendentes)</label>
                  </div>
                </div>  
                <br>
                <div class="divider"></div>
                <h5 class="header">Equipos</h5>
                <div class="row">
                  <div class="col s12">
                    <table id="tabla_equipos" class="responsive-table display" cellspacing="0">
                      <thead>
                        <tr>
                          <th width="10%"></th>
                          <th width="10%">Modelo</th>
                          <th width="10%">Costo por clic</th>
                          <th>Costo por Excedente</th>
                          <th class="center">Impresión / Copia</th>
                          <th class="center">Escaneo</th>
                          <td></td>
                        </tr>
                      </thead>
                      <tbody id="class_equipos">
                          <tr>
                          <td><img src="<?php echo base_url() ?>app-assets/images/kyo.png" style="height:110px;" class="imgpro"></td>
                          <td>P5021</td>
                          <td>$.5</td>
                          <td>$.20</td>
                          <td>
                            <div class="row">
                              <div class="input-field col s6">
                                <i class="material-icons prefix"></i>
                                <input id="" name=""  type="text" autocomplete="off" value="">
                                <label for="">Contador Inicial</label>
                              </div>
                              <div class="input-field col s6">
                                <i class="material-icons prefix"></i>
                                <input id="" name="" type="text" value="">
                                <label for="">Contador Final</label>
                              </div>
                            </div>  
                          </td>
                          <td>
                            <div class="row">
                              <div class="input-field col s6">
                                <i class="material-icons prefix"></i>
                                <input id="" name=""  type="text" autocomplete="off" value="">
                                <label for="">Contador Inicial</label>
                              </div>
                              <div class="input-field col s6">
                                <i class="material-icons prefix"></i>
                                <input id="" name="" type="text" value="">
                                <label for="">Contador Final</label>
                              </div>
                            </div>  
                          </td>
                        </tr>
                        <tr>
                          <td><img src="<?php echo base_url() ?>app-assets/images/kyo.png" style="height:110px;" class="imgpro"></td>
                          <td>P5021</td>
                          <td>$.5</td>
                          <td>$.20</td>
                          <td>
                            <div class="row">
                              <div class="input-field col s6">
                                <i class="material-icons prefix"></i>
                                <input id="" name=""  type="text" autocomplete="off" value="">
                                <label for="">Contador Inicial</label>
                              </div>
                              <div class="input-field col s6">
                                <i class="material-icons prefix"></i>
                                <input id="" name="" type="text" value="">
                                <label for="">Contador Final</label>
                              </div>
                            </div>  
                          </td>
                          <td>
                            <div class="row">
                              <div class="input-field col s6">
                                <i class="material-icons prefix"></i>
                                <input id="" name=""  type="text" autocomplete="off" value="">
                                <label for="">Contador Inicial</label>
                              </div>
                              <div class="input-field col s6">
                                <i class="material-icons prefix"></i>
                                <input id="" name="" type="text" value="">
                                <label for="">Contador Final</label>
                              </div>
                            </div>  
                          </td>
                        </tr>
                        <tr>
                          <td><img src="<?php echo base_url() ?>app-assets/images/kyo.png" style="height:110px;" class="imgpro"></td>
                          <td>P5021</td>
                          <td>$.5</td>
                          <td>$.20</td>
                          <td>
                            <div class="row">
                              <div class="input-field col s6">
                                <i class="material-icons prefix"></i>
                                <input id="" name=""  type="text" autocomplete="off" value="">
                                <label for="">Contador Inicial</label>
                              </div>
                              <div class="input-field col s6">
                                <i class="material-icons prefix"></i>
                                <input id="" name="" type="text" value="">
                                <label for="">Contador Final</label>
                              </div>
                            </div>  
                          </td>
                          <td>
                            <div class="row">
                              <div class="input-field col s6">
                                <i class="material-icons prefix"></i>
                                <input id="" name=""  type="text" autocomplete="off" value="">
                                <label for="">Contador Inicial</label>
                              </div>
                              <div class="input-field col s6">
                                <i class="material-icons prefix"></i>
                                <input id="" name="" type="text" value="">
                                <label for="">Contador Final</label>
                              </div>
                            </div>  
                          </td>
                        </tr>
                      </tbody>
                    </table>    
                  </div>
                </div>  
                <div class="row">
                  <div class="input-field col s9"></div>
                  <div class="input-field col s3">
                    <i class="material-icons prefix"></i>
                    <input placeholder="" id="" name="" type="text" class="validate">
                    <label for="" class="active">Total</label>
                  </div>
                </div>       
                <div class="row">
                  <div class="col s3">
                    <button class="btn cyan waves-effect waves-light right" type="submit">Incluir tonner
                      <i class="material-icons right">add</i>
                    </button>
                  </div>
                  <div class="input-field col s9">
                    <i class="material-icons prefix"></i>
                    <input id="" name="" type="text" value="">
                    <label for=""></label>
                    <i class="material-icons prefix"></i>
                    <input id="" name="" type="text" value="">
                    <label for=""></label>
                  </div>
                </div>  
                <div class="divider"></div>
                <h5 class="header">Nueva factura</h5>
                <div class="row">
                  <div class="input-field col s6">
                    <i class="material-icons prefix"></i>
                    <input id="" name=""  type="text" autocomplete="off" value="">
                    <label for="">Metodo de pago</label>
                  </div>
                  <div class="input-field col s6">
                    <i class="material-icons prefix"></i>
                    <input id="" name="" type="text" value="">
                    <label for="">Forma de pago</label>
                  </div>
                </div>  
                <div class="row">
                  <div class="input-field col s6">
                    <i class="material-icons prefix"></i>
                    <input id="" name=""  type="text" autocomplete="off" value="">
                    <label for="">Lugar de expedición</label>
                  </div>
                  <div class="input-field col s6">
                    <i class="material-icons prefix"></i>
                    <input id="" name="" type="text" value="">
                    <label for="">Uso de CFDI</label>
                  </div>
                </div>  
                <div class="row">
                  <div class="input-field col s6">
                    <i class="material-icons prefix"></i>
                    <input id="" name=""  type="text" autocomplete="off" value="">
                    <label for="">Tiempo de moneda</label>
                  </div>
                </div>
                <div class="row">
                  <div class="input-field col s12">
                    <button class="btn cyan waves-effect waves-light right" type="submit">Timbrar factura
                      <i class="material-icons right">send</i>
                    </button>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </section>