<?php
require_once dirname(__FILE__) . '/TCPDF3/examples/tcpdf_include.php'; 
require_once dirname(__FILE__) . '/TCPDF3/tcpdf.php';
$GLOBALS['configuracionCotizacion']=$configuracionCotizacion;
$GLOBALS['configuracionCotizacion']=$configuracionCotizacion;
$GLOBALS["fecha_crea"]=$fecha_crea;
$GLOBALS["fecha_asig"]=$fecha_asig;
$GLOBALS["tipo_serv"]=$tipo_serv;
$GLOBALS["responsable_asig"]=$responsable_asig;
$GLOBALS["hora_asig"]=$hora_asig;
$GLOBALS["prioridad"]=$prioridad;
$GLOBALS["descripcion_serv_fall"]=$descripcion_serv_fall;
$GLOBALS["modelo"]=$modelo;
$GLOBALS["serie"]=$serie;
$GLOBALS["nombre_empresa"]=$nombre_empresa;
$GLOBALS["direcc_inst_serv"]=$direcc_inst_serv;
$GLOBALS["tel"]=$tel;
$GLOBALS["cel"]=$cel;
$GLOBALS["correo"]=$correo;
$GLOBALS["docum_acce"]=$docum_acce;
$GLOBALS["observa_edi"]=$observa_edi;
$GLOBALS["asesor_ser"]=$asesor_ser;
$GLOBALS["gerencia"]=$gerencia;
$GLOBALS["firma"]=$firma;
$GLOBALS["comentariocal"]=$comentariocal;
$GLOBALS["comentariotecnico"]=$comentariotecnico;
$GLOBALS["comentarioejecutiva"]=$comentarioejecutiva;

class MYPDF extends TCPDF {
  //Page header
  public function Header() {
      /// datos completos
      $html = '';
   
      $this->writeHTML($html, true, false, true, false, '');
  }

    // Page footer
  public function Footer() {
    $html = '';

      $html .= '<table width="100%" border="0">
                  <tr>
                    <td align="right" class="footerpage">Pagina '.$this->getAliasNumPage().' de '.$this->getAliasNbPages().'</td>
                  </tr>
                </table>';
      $this->writeHTML($html, true, false, true, false, '');
  }
} 
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Cuestionario');
$pdf->SetTitle('Cuestionario');
$pdf->SetSubject('Cuestionario');
$pdf->SetKeywords('Cuestionario');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins('10', '10', '10');
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin("15");

// set auto page breaks
$pdf->SetAutoPageBreak(true, '0');

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//$pdf->SetFont('dejavusans', '', 9);
$pdf->SetFont('dejavusans', '', 7);
// add a page
$html='';
/*
$pdf->AddPage('P', 'A4');
$html='';
// Encabezado
$html.= '<table>
          <tr>
            <td width="25%">
              <img src="'.base_url().'app-assets/images/'.$GLOBALS['configuracionCotizacion']->logo1.'" style="padding: 1px;">
            </td>
            <td width="50%">
              '.$GLOBALS['configuracionCotizacion']->textoHeader.'
            </td>
            <td width="25%"><br><br>
              <img class="" src="'.base_url().'app-assets/images/1024_kyocera_logo_mds.png" alt="materialize logo">
            </td>
          </tr>
        </table>';
        // datos completos        
$html.='<table width="100%">
          <tr><td style="width: 100%;"></td></tr> 
          <tr>
            <td style="width: 100%; font-size: 11px; border-bottom: 1px solid #000000;"><span style="font-weight: bold;"></span></td>
          </tr>
        </table>';
// datos en procesos          
$html.='<table width="100%">
          <tr>
            <td style="width: 100%; font-size: 11px; border-bottom: 1px solid #000000; text-align: center;"><span style="font-weight: bold;">INFORMACIÓN GENERAL DEL SERVICIO</span></td>
          </tr>
        </table>'; 
// datos completos        
$html.='<table width="100%">
          <tr><td style="width: 100%;"></td></tr> 
          <tr>
            <td style="width: 50%; font-size: 11px; border-bottom: 1px solid #000000;"><span style="font-weight: bold;">Fecha de creación: '.date('d-m-Y',strtotime($GLOBALS["fecha_crea"])).'</span></td>
            <td style="width: 50%; font-size: 11px; border-bottom: 1px solid #000000;"><span style="font-weight: bold;">Responsable asignado: '.$GLOBALS["responsable_asig"].'</span></td>
          </tr>
        </table>';
// datos en procesos          
$html.='<table width="100%">
          <tr>
            <td style="width: 50%; font-size: 11px; border-bottom: 1px solid #000000;"><span style="font-weight: bold;">Fecha asignación: '.date('d-m-Y',strtotime($GLOBALS["fecha_asig"])).'</span></td>
            <td style="width: 50%; font-size: 11px; border-bottom: 1px solid #000000;"><span style="font-weight: bold;">Hora de asignación: '.$GLOBALS["hora_asig"].'</span></td>
          </tr>
          <tr>
            <td style="width: 50%; font-size: 11px; border-bottom: 1px solid #000000;"><span style="font-weight: bold;">Prioridad: '.$GLOBALS["prioridad"].'</span></td>
            <td style="width: 50%; font-size: 11px; border-bottom: 1px solid #000000;"><span style="font-weight: bold;"></span></td>
          </tr>
        </table>'; 
// datos en proceso   
$html.='<table width="100%">
          <tr><td style="width: 100%;"></td></tr> 
          <tr>
            <td style="width: 100%; font-size: 11px; border-bottom: 1px solid #000000;"><span style="font-weight: bold;"></span></td>
          </tr>
        </table>';
$html.='<table width="100%">
          <tr>
            <td style="width: 100%; font-size: 11px; border-bottom: 1px solid #000000; text-align: center;"><span style="font-weight: bold;">INFORMACIÓN DEL EQUIPO</span></td>
          </tr>
        </table>'; 

$html.='<table width="100%">
          <tr>
            <td style="width: 100%; font-size: 11px;"><span style="font-weight: bold;">Descripción del servicio ó falla: '.$GLOBALS["descripcion_serv_fall"].'</span></td>
          </tr>
        </table>';
$html.='<table width="100%">
          <tr>
            <td></td>
          </tr>
        </table>';
// datos en procesos          
$html.='<table width="100%">
          <tr>
            <td width="15%" style="font-size: 11px; border-bottom: 1px solid #000000; border-top: 1px solid #000000;">
              <b>Poliza</b>
            </td>
            <td width="15%" style="font-size: 11px; border-bottom: 1px solid #000000; border-top: 1px solid #000000;">
              <b>Equipo/servicio</b>
            </td>
            <td width="10%" style="font-size: 11px; border-bottom: 1px solid #000000; border-top: 1px solid #000000;">
              <b>Serie</b>
            </td>
            <td width="38%"style="font-size: 11px; border-bottom: 1px solid #000000; border-top: 1px solid #000000;">
              <b>Dirección de instalación/servicio</b>
            </td>
            <td width="10%" style="font-size: 11px; border-bottom: 1px solid #000000; border-top: 1px solid #000000;">
              <b>Tipo</b>
            </td>
            <td width="12%" style="font-size: 11px; border-bottom: 1px solid #000000; border-top: 1px solid #000000;">
              <b>Comentario</b>
            </td>
          </tr>';
foreach ($datosdeta->result() as $item) {
  if ($item->ttipo==1) {
    $tipo='Local';
  }elseif ($item->ttipo==2) {
    $tipo='Semi';
  }elseif ($item->ttipo==3) {
    $tipo='Foranea';
  }elseif ($item->ttipo==4) {
    $tipo='Especial';
  }else{
    $tipo='';
  }
  $html.='
          <tr>
            <td style="font-size: 11px; border-bottom: 1px solid #000000; border-top: 1px solid #000000;">
              <b>'.$item->nombre.'</b>
            </td>
            <td style="font-size: 11px; border-bottom: 1px solid #000000; border-top: 1px solid #000000;">
              <b>'.$item->modelo.' '.$item->newmodelo.' / '.$item->servicio.'</b>
            </td>
            <td style="font-size: 11px; border-bottom: 1px solid #000000; border-top: 1px solid #000000;">
              <b>'.$item->serie.'</b>
            </td>
            <td style="font-size: 11px; border-bottom: 1px solid #000000; border-top: 1px solid #000000;">
              <b>'.$item->direccion.'</b>
            </td>
            <td style="font-size: 11px; border-bottom: 1px solid #000000; border-top: 1px solid #000000;">
              <b>'.$tipo.'</b>
            </td>
            <td style="font-size: 11px; border-bottom: 1px solid #000000; border-top: 1px solid #000000;">
              <b>'.$item->comentario_tec.'</b>
            </td>
          </tr>';
}
$html.='</table>'; 
$html.='<table width="100%">
          <tr><td style="width: 100%;"></td></tr> 
          <tr>
            <td style="width: 100%; font-size: 11px; border-bottom: 1px solid #000000;"><span style="font-weight: bold;"></span></td>
          </tr>
        </table>';
// datos en procesos          
$html.='<table width="100%">
          <tr>
            <td style="width: 100%; font-size: 11px; border-bottom: 1px solid #000000; text-align: center;"><span style="font-weight: bold;">INFORMACIÓN DEL CLIENTE</span></td>
          </tr>
        </table>';
$html.='<table width="100%">
          <tr>
            <td style="width: 100%; font-size: 11px;"><span style="font-weight: bold;">Nombre: '.$GLOBALS["nombre_empresa"].'</span></td>
          </tr>
          
        </table>';
$html.='<table width="100%">
          <tr>
            <td></td>
          </tr>
        </table>';
$html.='<table width="100%">
          <tr>
            <td style="width: 30%; font-size: 11px; border-bottom: 1px solid #000000;"></td>
            <td style="width: 30%; font-size: 11px; border-bottom: 1px solid #000000;"></td>
            <td style="width: 40%; font-size: 11px; border-bottom: 1px solid #000000;"></td>
          </tr>
          <tr>
            <td style="width: 30%; font-size: 11px; border-bottom: 1px solid #000000;"><span style="font-weight: bold;">Teléfono: '.$GLOBALS["tel"].'</span></td>
            <td style="width: 30%; font-size: 11px; border-bottom: 1px solid #000000;"><span style="font-weight: bold;">Celular: '.$GLOBALS["cel"].'</span></td>
            <td style="width: 40%; font-size: 11px; border-bottom: 1px solid #000000;"><span style="font-weight: bold;">Correo electrónico: '.$GLOBALS["correo"].'</span></td>
          </tr>
        </table>'; 
$html.='<table width="100%">
          <tr>
            <td style="width: 100%; font-size: 11px;"><span style="font-weight: bold;">Documentos y accesorios para el ingreso: '.$GLOBALS["docum_acce"].'</span></td>
          </tr>
        </table>';
$html.='<table width="100%">
          <tr><td style="width: 100%;"></td></tr> 
          <tr>
            <td style="width: 50%; font-size: 11px;"><span style="font-weight: bold;">Observaciones tecnico: <br>'.$GLOBALS["comentariotecnico"].'</span>
            </td>
            <td style="width: 50%; font-size: 11px;"><span style="font-weight: bold;">Observaciones ejecutiva: <br>'.$GLOBALS["comentarioejecutiva"].'</span>
            </td>
          </tr>
        </table>';
// datos en procesos          

$html.='<table width="100%">
          <tr><td style="width: 100%;"></td></tr> 
          <tr>
            <td style="width: 100%; font-size: 11px; border-bottom: 1px solid #000000;"><span style="font-weight: bold;"></span></td>
          </tr>
        </table>';
// datos completos  
$imgtext_title='x';
$imgtext='x';
//if(isset($GLOBALS["firma"])){
  if($GLOBALS["firma"]==''){
    $imgtext_title='Nombre de quien recibió';
    $imgtext=$qrecibio;
  }else{
    $fh = fopen(base_url()."uploads/firmasservicios/".$GLOBALS["firma"], 'r') or die("Se produjo un error al abrir el archivo");
        $linea = fgets($fh);
        fclose($fh);  
        $linea=preg_replace('#^data:image/[^;]+;base64,#', '', $linea) ;
      $imgtext_title='Firma del cliente';
      $imgtext='<img src="@'.$linea.'" width="355" height="160" style="border:dotted 1px black;">';
  }
//}   
$html.='<table width="100%">
          <tr><td style="width: 100%;"></td></tr> 
          <tr>
            <td rowspan="6" style="width: 40%; font-size: 11px;"><span style="font-weight: bold; text-align: center;">'.$imgtext_title.'</span>
            <div style="text-align:center;margin-top:0.05cm;padding:0.1cm;">
            '.$imgtext.'
            </div>
            </td>
            <td style="width: 60%; font-size: 11px; border-bottom: 1px solid #000000;"><span style="font-weight: bold;">Asesor de servicio: '.$asesor_ser.'</span></td>
          </tr>
          <tr><td style="width: 100%;"></td></tr> 
          
        </table>';
$pdf->writeHTML($html, true, false, true, false, '');
*/

//===================================================================

$pdf->AddPage('P', 'A4');
  $html='<style type="text/css">
          .t_c{text-align:center;}
          .t_b{font-weight: bold;}
          .bor_b{border-bottom: 1px solid black;}
          .bor_l{border-left: 1px solid black;}
          .bor_r{border-right: 1px solid black;}
          .bor_t{border-top: 1px solid black;}
          .tableb th{background-color:rgb(217,217,217);}
        </style>';
  $data['fecha_crea'] =$reg; 
        
        
          
        $text_prioridad=$GLOBALS["prioridad"];
        /*
        $prioridad=$GLOBALS["prioridad"];
        log_message('error','Prioridad:'.$prioridad);
        if ($prioridad==1){
            $text_prioridad='Prioridad 1';
        }else if($prioridad==2){
            $text_prioridad='Prioridad 2';
        }else if($prioridad==3){
            $text_prioridad='Prioridad 3';
        }else if($prioridad==4){
            $text_prioridad='Servicio Regular';
        }else{
            $text_prioridad='';
        } 
        */
        $data['prioridad']=$text_prioridad;

  $html.='<table border="0">
            <tr>
              <td width="20%" class="t_c"><img src="'.base_url().'app-assets/images/'.$GLOBALS['configuracionCotizacion']->logo1.'" width="80px"></td>
              <td width="60%" class="t_c">
                "ALTA PRODUCTIVIDAD, S.A DE C.V."<br>
                RFC: APR980122KZ6<br>
                Impresoras y Multifuncionales, Venta, Renta y Mantenimiento.<br>
                Domicilio Fiscal: Blvd. Norte 1831 Col. Villas San Alejandro. Puebla, Pue. Mexico C.P.72090<br>
                Banco BANAMEX, Cuenta 7347132, Sucursal 826, CLABE 2668082673471320<br>
                Call Center 273 3400, 249 5393. www.kyoceraap.com

              </td>
              <td width="20%" class="t_c"><img src="'.base_url().'app-assets/images/1024_kyocera_logo_mds.png" width="100px"></td>
            </tr>
          </table>
          <p></p>';
    $html.='<table class="tableb" cellpadding="4">
              <tr>
                <th class="t_c t_b bor_b bor_l bor_r bor_t">INFORMACIÓN GENERAL DEL SERVICIO</th>
              </tr>
            </table>
            <table class="tableb" cellpadding="4">
              <tr>
                <th class="bor_l bor_t">FECHA DE CREACION </th>
                <td class="  bor_t">'.date('d-m-Y',strtotime($reg)).'</td>
                <th class="  bor_t">RESPONSABLE ASIGNADO</th>
                <td class=" bor_r bor_t">'.$asesor_ser.'</td>
              </tr>
              <tr>
                <th class="bor_l ">FECHA DE ASIGNACION </th>
                <td class="  ">'.date('d-m-Y',strtotime($GLOBALS["fecha_asig"])).'</td>
                <th class="  ">HORA DE ASIGNACION</th>
                <td class=" bor_r ">'.date('g:i a',strtotime($GLOBALS["hora_asig"])).'</td>
              </tr>
              <tr>
                <th class="bor_l bor_b">TIPO DE SERVICIO</th>
                <td class="  bor_b">'.$poliza.'</td>
                <th class="  bor_b">PRIORIDAD</th>
                <td class=" bor_r bor_b">'.$text_prioridad.'</td>
              </tr>
            </table>
            <p></p>
            <table class="tableb" cellpadding="4">
              <tr>
                <th class="t_c t_b bor_b bor_l bor_r bor_t">INFORMACIÓN DEL EQUIPO</th>
              </tr>
            </table>
            <table class="tableb" cellpadding="4">';
            $direccion ='';
            foreach ($datosdeta->result() as $item) {
              $direccion =$item->direccion;
              $html.='<tr>';
                $html.='<th width="10%" class="bor_b bor_l bor_t">Modelo</th>';
                $html.='<td width="15%" class="bor_b bor_t">'.$item->modelo.' '.$item->newmodelo.' / '.$item->servicio.'</td>';
                $html.='<th width="10%" class="bor_b bor_t">Serie</th>';
                $html.='<td width="10%" class="bor_b bor_t">'.$item->serie.'</td>';
                $html.='<th width="22%" class="bor_b bor_t">Descripción del servicio ó falla : </th>';
                $html.='<td width="33%" class="bor_b bor_r bor_t">'.$tserviciomotivo.'</td>';
              $html.='</tr>';
              //if($comentarioinfo!=''){
              //  $html.='<tr><td colspan="6" class="bor_l bor_r">'.$comentarioinfo.'</td></tr>';
              //}
                
              
              }
            $html.='</table>';
            $html.='<table class="tableb" cellpadding="4">';
              $html.='<tr>';
                $html.='<th class="t_c t_b bor_b bor_l bor_r bor_t">INFORMACION DEL CLIENTE </th>';
              $html.='</tr>';
            $html.='</table>';
            $html.='<table class="tableb" cellpadding="4">';
              $html.='<tr>';
                $html.='<td width="30%" class=" bor_l bor_t">Nombre</td>';
                $html.='<td width="70%" class=" bor_r bor_t"> '.$GLOBALS["nombre_empresa"].'</td>';
              $html.='</tr>';
              $html.='<tr>';
                $html.='<td class=" bor_l ">Dirección de instalación/servicio</td>';
                $html.='<td class=" bor_r ">'.$direccion.'</td>';
              $html.='</tr>';
              $html.='<tr>';
                $html.='<td class=" bor_l ">Contacto</td>';
                $html.='<td class=" bor_r ">'.$atencionpara.'</td>';
              $html.='</tr>';
              $html.='<tr>';
                $html.='<td class=" bor_l ">Puesto</td>';
                $html.='<td class=" bor_r ">'.$puesto.'</td>';
              $html.='</tr>';
              $html.='<tr>';
                $html.='<td class=" bor_l ">Telefono</td>';
                $html.='<td class=" bor_r ">'.$telefono.'</td>';
              $html.='</tr>';
              $html.='<tr>';
                $html.='<td class=" bor_l ">Celular</td>';
                $html.='<td class=" bor_r ">'.$celular.'</td>';
              $html.='</tr>';
              
              //$docum_acce='';
              $docum_acce=$docum_acce;
              

      $html.='<tr>';
                $html.='<td class="bor_b bor_l ">Documentos y accesorios para el ingreso</td>';
                $html.='<td class="bor_b bor_r ">'.$docum_acce.'</td>';
              $html.='</tr>';
            $html.='</table>';
            $html.='<p></p>';
            $html.='<table class="tableb" cellpadding="4">';
              $html.='<tr>';
                $html.='<th class="t_c t_b bor_b bor_l bor_r bor_t">Observaciones Técnico</th>';
                $html.='<th class="t_c t_b bor_b bor_l bor_r bor_t">Observaciones Ejecutiva</th>';
              $html.='</tr>';
              
            if($comentario!=''){
                $comentariotecnico.='Observación General: '.$comentario.'<br>';
            }
            if($comentario_tec!=''){
                $comentariotecnico.='Observación Equipo: '.$comentario_tec.'<br>';
            }
      $html.='<tr>
                <td class="t_c bor_b  bor_l bor_r bor_t">'.$comentariotecnico.'</td>
                <td class="t_c  bor_b bor_l bor_r bor_t">'.$comentariocal.'</td>
              </tr>
              <tr>
                <th class="t_c t_b bor_b bor_l bor_r bor_t">Nombre de quien validó</th>
                <th class="t_c t_b bor_b bor_l bor_r bor_t">Asesor de servicio</th>
              </tr>
              <tr>
                <td class="t_c  bor_b bor_l bor_r ">'.$qrecibio.'</td>
                <td class="t_c  bor_b bor_l bor_r ">'.$asesor_ser.'</td>
              </tr>
            </table>';


$pdf->writeHTML($html, true, false, true, false, '');
$pdf->Output('Captura.pdf', 'I');

?>