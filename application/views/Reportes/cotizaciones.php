<?php
require_once dirname(__FILE__) . '/TCPDF3/examples/tcpdf_include.php'; 
require_once dirname(__FILE__) . '/TCPDF3/tcpdf.php';

$GLOBALS["nombreUsuario"]=$nombreUsuario;
$GLOBALS["emailUsuario"]=$emailUsuario;
$GLOBALS["idCotizacion"]=$idCotizacion;
$GLOBALS["fechareg"]=$fechareg;

$GLOBALS["empresa"]=$datosCliente->empresa;
$GLOBALS["atencion"]=$datosCotizacion[0]->atencion;
$GLOBALS["correo"]=$datosCotizacion[0]->correo;
$GLOBALS["telefono"]=$datosCotizacion[0]->telefono;
$GLOBALS["tipov"]=$tipov;
$GLOBALS["tipovlogo"]=$configuracionCotizacion->logo1;
$GLOBALS["tipovtextoHeader"]=$configuracionCotizacion->textoHeader;
$tipo=$datosCotizacion[0]->tipo; //1 - Equipos, 2 - Consumibles, 3 - Refacciones, 4 - Polizas
$rentaVentaPoliza=$datosCotizacion[0]->rentaVentaPoliza; //1 - venta, 2 - renta, 3 - poliza
$tipoo=$rentaVentaPoliza;
$tiporenta=$datosCotizacion[0]->tiporenta; //1 - venta, 2 - renta, 3 - poliza
$propuesta=$datosCotizacion[0]->propuesta;//0 cotizacion 1 propuesta

//
class MYPDF extends TCPDF {
  //Page header
  public function Header() {

      $img_file1=base_url().'public/img/cotheader1.png';
      $img_file2=base_url().'public/img/cotheader2.png';
      $img_file3=base_url().'public/img/cotheader3.png';
      if($GLOBALS["tipov"]==2){
        $img_file3=base_url().'app-assets/images/'.$GLOBALS["tipovlogo"];
      }                      
      $this->Image($img_file1, 0, 0, 0, 0, '', '', 10, false, 310, '', false, false, 0);
      $this->Image($img_file2, 1, 3, 70, 0, '', '', 10, false, 310, '', false, false, 0);
      $this->Image($img_file3, 160, 3, 40, 0, '', '', 10, false, 310, '', false, false, 0);

      $html = '<style type="text/css" class="print_css">
                  .titulocoti{
                    font-style: italic;
                    font-size: 13px;
                    color:#2e4064;
                  }
                  .numbercoti{
                    font-size: 14px;
                    color:#2e4064;
                    font-weight:bold;

                  }
                  .colorblue{
                    color:#2e4064;
                  }
              </style>';
      $html.= '<table border="0">
                  <tr>
                    <td width="34%"></td>
                    <td width="39%" class="titulocoti" align="center">'.$GLOBALS["tipovtextoHeader"].'<p></p></td>
                    <td width="29%"></td>
                  </tr>
                  <tr>
                    <td></td>
                    <td></td>
                    <td class="numbercoti">
                      <table>
                        <tr>
                          <td></td>
                          <td height="20px;"></td>
                        </tr>
                        <tr>
                          <td colspan="2" align="center">Cotización # '.$GLOBALS["idCotizacion"].'</td>
                        </tr>
                        <tr>
                          <td colspan="2" align="center"> '.$GLOBALS["fechareg"].'</td>
                        </tr>
                      </table>
                      </td>
                  </tr>
                </table>
                <table border="0">
                  <tr>
                    <td></td>
                  </tr>
                  <tr>
                    <td></td>
                  </tr>
                </table>
                
                ';      
      $this->writeHTML($html, true, false, true, false, '');
  }

    // Page footer
  public function Footer() {
    $html = '';
      $img_file=base_url().'public/img/cot_footer.jpg';
                            //x,y    w,h
      $this->Image($img_file, 0, 255, 0, 0, '', '', 10, false, 310, '', false, false, 0);
      $html .= '<table border="0">
                  <tr><td></td><td></td><td ></td></tr>
                  <tr><td></td><td></td><td ></td></tr>
                  <tr><td></td><td></td><td ></td></tr>
                  <tr><td></td><td></td><td ></td></tr>
                  <tr>
                    <td></td>
                    <td></td>
                    <td style="font-weight:bold;color:white;text-align:center;">Le atiende:<br>'.$GLOBALS["nombreUsuario"].'<br>'.$GLOBALS["emailUsuario"].'</td>
                  </tr>
                  <tr>
                    <td></td>
                    <td></td>
                    <td style="font-weight:bold;color:white; text-align:center" align="center">Pagina  '.$this->getAliasNumPage().'/'.$this->getAliasNbPages().'</td>
                  </tr>
                </table>';
      $this->writeHTML($html, true, false, true, false, '');
  }
} 
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('kyocera');
$pdf->SetTitle('Cotización '.$idCotizacion);
$pdf->SetSubject('Cotización');
$pdf->SetKeywords('Cotización');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins('8', '45', '8');
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin("45");

// set auto page breaks
$pdf->SetAutoPageBreak(true, '45');

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//$pdf->SetFont('dejavusans', '', 9);
// add a page
$pdf->AddPage('P', 'A4');
$col1='20';
$col2='10';
$col3='11';
$col4='47';
$col5='12';

$html='<style type="text/css" class="print_css">
          .div{
            border: 1px solid black;
            width: 100px;
          }
          .tamanotext{
            font-size:11px !important;
          }
          .titulositems{
            color:red;
            font-size:14px;
          }
          .titulositems2{
            font-size:12px;
          }
          .trborderbuttom{
            border-bottom:2px sold red;
          }
          .trbordertop{
            border-top:1px sold #eaa2a2;
          }
          .tableitems{
            color:#2e4064;
          }
          .colorblue{
                    color:#2e4064;
                  }
       </style>
                <table border="0">
                  <tr>
                    <td width="25%" class="colorblue titulositems2" style="font-weight:bold">Atención para:</td>
                    <td width="75%" class="colorblue titulositems2">'.$GLOBALS["atencion"].'</td>
                  </tr>
                  <tr>
                    <td class="colorblue titulositems2" style="font-weight:bold">Correo Electrónico:</td>
                    <td class="colorblue titulositems2">'.$GLOBALS["correo"].'</td>
                  </tr>
                  <tr>
                    <td class="colorblue titulositems2" style="font-weight:bold">Teléfono:</td>
                    <td class="colorblue titulositems2">'.$GLOBALS["telefono"].'</td>
                  </tr>
                  <tr>
                    <td class="colorblue titulositems2" style="font-weight:bold">Empresa:</td>
                    <td class="colorblue titulositems2">'.$GLOBALS["empresa"].'</td>
                  </tr>
                </table>
        ';
    //log_message('error','rentaVentaPoliza:'.$rentaVentaPoliza);
$preciosrow=0;
//if($tipo==1){ //equipos
  if($tiporenta==0){//solo venta de equipo
      $preciosrow=0;
      if($detallesCotizacione->num_rows()>0){
        foreach ($detallesCotizacione->result() as $item) {
          if ($item->tipo==1) {
              $tipoimpresora=0;
          }else{
              $tipoimpresora=1;
          }
        }
        //$propuesta=1;
        if($propuesta==1){
          $col1='7';$col2='10';$col3='11';$col4='24';$col5='12';$col6='12';$col7='12';$col8='12';
        }else{
          $col1='13';$col2='10';$col3='11';$col4='28';$col5='12';$col6='14';$col7='12';
        }
        $html.='<div class="titulositems"><b>.Equipos</b></div>
                <table border="0" align="center" class="tamanotext tableitems">
                  <tr>
                    <th width="'.$col1.'%" class="trborderbuttom"></th>
                    <th width="'.$col2.'%" class="trborderbuttom"><b>Cantidad</b></th>
                    <th width="'.$col3.'%" class="trborderbuttom"><b>Modelo</b></th>
                    <th width="'.$col4.'%" class="trborderbuttom"><b>Características</b></th>
                    <th width="'.$col5.'%" class="trborderbuttom"><b>Rendimiento unidad imagen</b></th>
                    <th width="'.$col6.'%" class="trborderbuttom"><b>Costo por pág</b></th>
                    <th width="'.$col7.'%" class="trborderbuttom"><b>Precio</b></th>';
                   if($propuesta==1){
                    $html.='<th width="'.$col8.'%" class="trborderbuttom" ><b>Total</b></th>';
                   } 
        $html.='</tr>';
        foreach ($detallesCotizacione->result() as $item) {
          $costo_pagina_color=$item->costo_pagina_color;
          $preciosrow=$preciosrow+($item->precio*$item->cantidad);
          if ($item->foto==null) {
             $img=base_url().'app-assets/images/1024_kyocera_logo.png';
          }else{
            $img=base_url().'uploads/equipos/'.$item->foto;
          }
          $accesoriosig=$this->Cotizaciones_model->getcotizacionaccesorios($idCotizacion,$item->idEquipo);

          $tconsu=$this->Cotizaciones_model->getcotizacionequipoconsumible($idCotizacion,$item->idEquipo,1);
          $infoc='';
          foreach ($tconsu->result() as $itemc) {
            //$infoc=$itemc->modelo.' $'.number_format($itemc->costo_toner,2,'.',',').' Rinde: '.number_format($itemc->rendimiento_toner,0,'.',',');
            $infoc=$itemc->modelo.' Rinde: '.number_format($itemc->rendimiento_toner,0,'.',',').' págs.<br>';
          }
          //=========================================
            $tconsuc=$this->Cotizaciones_model->getcotizacionequipoconsumible($idCotizacion,$item->idEquipo,2);
            $tconsum=$this->Cotizaciones_model->getcotizacionequipoconsumible($idCotizacion,$item->idEquipo,3);
            $tconsuy=$this->Cotizaciones_model->getcotizacionequipoconsumible($idCotizacion,$item->idEquipo,4);
            $toner=0;
            if ($tconsuc->num_rows()>0) {
               $toner=$toner+1;
            }
            if ($tconsum->num_rows()>0) {
               $toner=$toner+1;
            }
            if ($tconsuy->num_rows()>0) {
               $toner=$toner+1;
            }
            if ($toner==3) {
              $tonescmy=$tconsuc->row()->costo_toner+$tconsum->row()->costo_toner+$tconsuy->row()->costo_toner;
              $infoc.='KIT TONER CMY  Rinde: '.number_format($tconsum->row()->rendimiento_toner,0,'.',',').' págs.<br>';
              $costo_pagina_color=$tonescmy/$tconsum->row()->rendimiento_toner;
            }
          //=========================================
          $infoc.=$item->especificaciones_tecnicas;
          $costosporpagina='';
          if($item->costo_pagina_monocromatica>0){
            $costosporpagina.='<b>B/N:</b> $'.number_format($item->costo_pagina_monocromatica,2,'.',',').'<br>';
          }
          if($costo_pagina_color>0){
            $costosporpagina.='<b>Color :</b> $'.number_format($costo_pagina_color,2,'.',',');
          }
          
          $html.='<tr >
                    <td style="color:white" class="trbordertop" cellpadding="3">-<br><br> <img src="'.$img.'" width="60px" style="margin-top:10px"></td>
                    <td class="trbordertop" >.'.$item->cantidad.'</td>
                    <td class="trbordertop">'.$item->modelo.'</td>
                    <td class="trbordertop">'.$infoc.'</td>
                    <td class="trbordertop">'.number_format($item->paginas,0,'.',',').'</td>
                    <td class="trbordertop">'.$costosporpagina.'</td>
                    <td class="trbordertop">$ '.number_format(($item->precio),2,'.',',').'</td>';
                    if($propuesta==1){
                      $html.='<td class="trbordertop">$'.number_format(($item->precio*$item->cantidad),2,'.',',').'</td>';
                    }
          $html.='</tr>';

          foreach ($accesoriosig->result() as $itemacc) {
            //log_message('error','si entra');
            $preciosrow=$preciosrow+($itemacc->cantidad*$itemacc->costo);
            $html.='<tr>
                    <td></td>
                    <td>'.$itemacc->cantidad.'</td>
                    <td>'.$itemacc->nombre.'</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>$ '.number_format(($itemacc->costo),2,'.',',').'</td></tr>';
          }

          $html.='<tr><td colspan="7"></td></tr>';
        }
        $html.='</table>';
      }
  }else{
    if($tiporenta==1 or $tiporenta==2){
      if($detallesCotizacione->num_rows()>0){
        $datosmono=0;
        $datoscolor=0;
        foreach ($detallesCotizacione->result() as $item) {
          if ($item->r_costo_m>0) {
            $datosmono=1;
          }
          if ($item->r_costo_c>0) {
            $datoscolor=1;
          }
        }
        $html.='<div class="titulositems"><b>Equipos</b></div>
                <table border="0" align="center" class="tamanotext tableitems">
                  <tr>
                    <th class="trborderbuttom"></th>
                    <th class="trborderbuttom"><b>Cantidad</b></th>
                    <th class="trborderbuttom"><b>Modelo</b></th>
                    <th class="trborderbuttom"><b>Características</b></th>
                    ';
                   if($tiporenta==1){
                    if($datosmono==1){
                      $html.='<th class="trborderbuttom" ><b>Renta Mensual Monocomatica</b></th>';
                      $html.='<th class="trborderbuttom" ><b>Páginas monocromáticas incluídas </b></th>';
                      $html.='<th class="trborderbuttom" ><b>Excedente Monocromatica</b></th>';
                    }
                    if($datoscolor==1){
                      $html.='<th class="trborderbuttom" ><b>Renta Mensual Color</b></th>';
                      $html.='<th class="trborderbuttom" ><b>Páginas Color incluídas </b></th>';
                      $html.='<th class="trborderbuttom" ><b>Excedente color</b></th>';
                    }
                   } 
        $html.='</tr>';
        foreach ($detallesCotizacione->result() as $item) {
            $preciosrow=$preciosrow+($item->precio*$item->cantidad);
            if ($item->foto==null) {
               $img=base_url().'app-assets/images/1024_kyocera_logo.png';
            }else{
              $img=base_url().'uploads/equipos/'.$item->foto;
            }
            $accesorios=$this->Cotizaciones_model->getcotizacionaccesorios($idCotizacion,$item->idEquipo);

            $tconsu=$this->Cotizaciones_model->getcotizacionequipoconsumible($idCotizacion,$item->idEquipo,1);
            $infoc='';
            foreach ($tconsu->result() as $itemc) {
              //$infoc=$itemc->modelo.' $'.number_format($itemc->costo_toner,2,'.',',').' Rinde: '.number_format($itemc->rendimiento_toner,0,'.',',');
              $infoc=$itemc->modelo.' Rinde: '.number_format($itemc->rendimiento_toner,0,'.',',').' págs.';
            }
            $infoc.='<br>'.$item->especificaciones_tecnicas;
            $html.='<tr>
                      <td><img src="'.$img.'" width="60px"></td>
                      <td>'.$item->cantidad.'</td>
                      <td>'.$item->modelo.'</td>
                      <td>'.$infoc.'</td>
                      ';
                      if($datosmono==1){
                        $html.='<td>$'.number_format($item->r_costo_m,2,'.',',').' </td>';
                        $html.='<td>'.$item->r_volumen_m.' </td>';
                        $html.='<td>'.$item->excedentem.'</td>';
                      }
                      if($datoscolor==1){
                        $html.='<td>$'.number_format($item->r_costo_c,2,'.',',').' </td>';
                        $html.='<td>'.$item->r_volumen_c.' </td>';
                        $html.='<td>'.$item->excedentec.'</td>';
                      }
            $html.='</tr>';

            foreach ($accesorios->result() as $itemacc) {
              $html.='<tr>
                      <td></td>
                      <td>'.$itemacc->cantidad.'</td>
                      <td>'.$itemacc->nombre.'</td>
                      <td></td>
                      <td></td></tr>';
            }
        }
        $html.='</table>';
        if($tiporenta==2){
        $html.='<table border="0" align="center" class="tamanotext tableitems">';
          $html.='<tr>';
          if($datosCotizacion[0]->eg_rm>0 or $datosCotizacion[0]->eg_rvm>0 or $datosCotizacion[0]->eg_rpem>0){
            $html.='<th><b>Renta Monocromatico</b></th><th><b>Volumen incluido monocromatico</b></th><th><b>Excedente Monocromatico</b></th>';
          }
          if($datosCotizacion[0]->eg_rc>0 or $datosCotizacion[0]->eg_rvc>0 or $datosCotizacion[0]->eg_rpec>0){
            $html.='<th><b>Renta Color</b></th><th><b>Volumen incluido color </b></th><th><b>Excedente color</b></th>';
          }
          $html.='</tr>';
          $html.='<tr>';
          if($datosCotizacion[0]->eg_rm>0 or $datosCotizacion[0]->eg_rvm>0 or $datosCotizacion[0]->eg_rpem>0){
            $html.='<td>$'.number_format($datosCotizacion[0]->eg_rm,2,'.',',').'</td><td>'.$datosCotizacion[0]->eg_rvm.'</td><td>$'.number_format($datosCotizacion[0]->eg_rpem,2,'.',',').'</td>';
          }
          if($datosCotizacion[0]->eg_rc>0 or $datosCotizacion[0]->eg_rvc>0 or $datosCotizacion[0]->eg_rpec>0){
            $html.='<td>$'.number_format($datosCotizacion[0]->eg_rc,2,'.',',').'</td><td>'.$datosCotizacion[0]->eg_rvc.'</td><td>$'.number_format($datosCotizacion[0]->eg_rpec,2,'.',',').'</td>';
          }
          $html.='</tr>';
        $html.='</table>';
        }
      }
    }
  }

//}

//if($tipo==2){ //consumibles
//if($tiporenta==0){//si es 0 no es renta y se despliega pero si es 1 y
if($detallesCotizacioncc->num_rows()>0){
    //$propuesta=1;
    if($propuesta==1){
      $col1='10';$col2='15';$col3='13';$col4='25';$col5='13';$col6='12';$col7='12';
    }else{
      $col1='14';$col2='15';$col3='13';$col4='32';$col5='13';$col6='12';
    }
    $html.='<div class="titulositems"><b>Consumibles</b></div>
                <table border="0" align="center" class="tamanotext tableitems" cellspacing="4">
                  <tr>
                    <th width="'.$col1.'%" class="trborderbuttom"><b>Cantidad</b></th>
                    <th width="'.$col2.'%" class="trborderbuttom"><b>Modelo</b></th>
                    <th width="'.$col3.'%" class="trborderbuttom"><b>N° de parte</b></th>
                    <th width="'.$col4.'%" class="trborderbuttom"><b>Equipo</b></th>
                    <th width="'.$col5.'%" class="trborderbuttom"><b>Rendimiento</b></th>
                    <th width="'.$col6.'%" class="trborderbuttom"><b>Precio</b></th>';
                   if($propuesta==1){
                    $html.='<th width="'.$col7.'%" class="trborderbuttom"><b>>Total</b></th>';
                   } 
        $html.='</tr>';
  foreach ($detallesCotizacioncc->result() as $item) {
    $preciosrow=$preciosrow+($item->piezas*$item->precioGeneral);
    $rendimiento=0;
      if ($item->tipo==5) {
        $rendimiento = $item->rendimiento_unidad_imagen;
      }else{
        $rendimiento = $item->rendimiento_toner;
      }
      $html.='<tr>
                <td>'.$item->piezas.'</td>
                <td>'.$item->modelo.'</td>
                <td>'.$item->parte.'</td>
                <td>'.$item->equipo.'</td>
                <td>'.$rendimiento.'</td>
                <td>$'.number_format($item->precioGeneral,2,'.',',').'</td>';
                if($propuesta==1){
                  $html.='<td>$'.number_format($item->piezas*$item->precioGeneral,2,'.',',').'</td>';
                }
      $html.='</tr>';
  }
    $html.='</table>';
}
//}
//}

//refacciones
if($detallesCotizacionref->num_rows()>0){
    //$propuesta=1;
    if($propuesta==1){
      $col1='10';$col2='15';$col3='12';$col4='27';$col5='12';$col6='12';$col7='12';
    }else{
      $col1='15';$col2='15';$col3='12';$col4='33';$col5='12';$col6='12';
    }
    $html.='<div class="titulositems"><b>Refacciones</b></div>
                <table border="0" align="center" class="tamanotext tableitems" cellspacing="4">
                  <tr>
                    <th width="'.$col1.'%" class="trborderbuttom"><b>Cantidad</b></th>
                    <th width="'.$col2.'%" class="trborderbuttom"><b>Modelo</b></th>
                    <th width="'.$col3.'%" class="trborderbuttom"><b>N° de parte</b></th>
                    <th width="'.$col4.'%" class="trborderbuttom"><b>Equipo</b></th>
                    <th width="'.$col5.'%" class="trborderbuttom"><b>Serie</b></th>
                    <th width="'.$col6.'%" class="trborderbuttom"><b>Precio</b></th>';
                   if($propuesta==1){
                    $html.='<th width="'.$col7.'%" class="trborderbuttom">Total</th>';
                   } 
        $html.='</tr>';

  foreach ($detallesCotizacionref->result() as $item) {
    $preciosrow=$preciosrow+($item->piezas*$item->precioGeneral);
    
      $html.='<tr>
                <td>'.$item->piezas.'</td>
                <td>'.$item->modelo.'</td>
                <td>'.$item->codigo.'</td>
                <td>'.$item->equipo.'</td>
                <td>'.$item->serieequipo.'</td>
                <td>$'.number_format($item->precioGeneral,2,'.',',').'</td>';

                
                if($propuesta==1){
                  $html.='<td>$'.number_format($item->piezas*$item->precioGeneral,2,'.',',').'</td>';
                }
      
      $html.='</tr>';
  }
   $html.='</table>';
}

//if($tipo==4){ //polizas
  if($datosCotizacionpoliza->num_rows()>0) {
      //$propuesta=1;
      if($propuesta==1){
        $col1='6';$col2='12';$col3='10';$col4='18';$col5='14';$col6='9';$col7='8';$col8='11';$col9='12';
      }else{
        $col1='6';$col2='14';$col3='11';$col4='18';$col5='20';$col6='10';$col7='9';$col8='12';
      }
      $html.='<div class="titulositems"><b>Servicios</b></div>
                <table border="0" align="center" class="tamanotext tableitems" cellspacing="4">
                  <tr>
                    <th width="'.$col1.'%" class="trborderbuttom"><b>Cant.</b></th>
                    <th width="'.$col2.'%" class="trborderbuttom"><b>Equipo</b></th>
                    <th width="'.$col3.'%" class="trborderbuttom"><b>Serie</b></th>
                    <th width="'.$col4.'%" class="trborderbuttom"><b>Tipo de Servicio</b></th>
                    <th width="'.$col5.'%" class="trborderbuttom"><b>Cobertura</b></th>
                    <th width="'.$col6.'%" class="trborderbuttom"><b>Vigencia</b></th>
                    <th width="'.$col7.'%" class="trborderbuttom"><b>Visitas</b></th>
                    <th width="'.$col8.'%" class="trborderbuttom"><b>Precio</b></th>';
                   if($propuesta==1){
                    $html.='<th width="'.$col9.'%" class="trborderbuttom"><b>Total</b></th>';
                   } 
        $html.='</tr>';
      foreach ($datosCotizacionpoliza->result() as $itemed) { 
        $preciovalor=0;
        if($itemed->precio_local!=null){
          $preciovalor=$itemed->precio_local;
        }
        if($itemed->precio_semi!=null){
          $preciovalor=$itemed->precio_semi;
        }
        if($itemed->precio_foraneo!=null){
          $preciovalor=$itemed->precio_foraneo;
        }
        if($itemed->precio_especial!=null){
          $preciovalor=$itemed->precio_especial;
        }
        $html.='<tr>
                  <td>'.$itemed->cantidad.'</td>
                  <td>'.$itemed->modeloeq.'</td>
                  <td>'.$itemed->serie.'</td>
                  <td>'.$itemed->nombre.'</td>
                  <td>'.$itemed->cubre.'</td>
                  <td>'.$itemed->vigencia_meses.'</td>
                  <td>'.$itemed->vigencia_clicks.'</td>
                  <td>$'.number_format($preciovalor,2,'.',',').'</td>';
          
                $total=$preciovalor*$itemed->cantidad;
                $preciosrow=$preciosrow+$total;
                if($propuesta==1){
                  $html.='<td>$'.number_format($total,2,'.',',').'</td>';
                }
              $html.='</tr>';
      }
      $html.='</table>';
  }
//}

if($datosCotizacionacc->num_rows()>0){
    $html.='<div class="titulositems"><b>Accesorios</b></div>
                <table border="0" align="center" class="tamanotext tableitems" cellspacing="4">
                  <tr>
                    <th class="trborderbuttom"><b>Cantidad</b></th>
                    <th class="trborderbuttom"><b>Modelo</b></th>
                    <th class="trborderbuttom"><b></b></th>
                    <th class="trborderbuttom"><b>Precio</b></th>';
                   if($propuesta==1){
                    $html.='<th class="trborderbuttom"><b>>Total</b></th>';
                   } 
        $html.='</tr>';
  foreach ($datosCotizacionacc->result() as $item) {
    $preciototal=$item->costo*$item->cantidad;
    $preciosrow=$preciosrow+$preciototal;
    $html.='<tr>
              <td>'.$item->cantidad.'</td>
              <td>'.$item->nombre.'</td>
              <td>'.$item->observaciones.'</td>
              <td>$ '.$item->costo.'</td>';
              if($propuesta==1){
                $html.='<td>'.number_format(($preciototal),2,'.',',').'</td>';
              }
    $html.='</tr>';

  }
    $html.='</table>';
}

    






//$html.=''.$rentaVentaPoliza;
if($rentaVentaPoliza==1 or $rentaVentaPoliza==3){
  if($propuesta==0){
        $html.='<table><tr><td></td></tr></table>
                <table border="0">

                  <tr>
                    <td width="88%" style="font-size:13px;" align="right" ><b>SubTotal:</b></td>
                    <td width="12%" style="font-size:13px;"> $ '.number_format($preciosrow,2,'.',',').'</td>
                  </tr>
                  <tr>
                    <td style="font-size:13px;" align="right" ><b>I.V.A:</b> </td>
                    <td style="font-size:13px;"> $ '.number_format($preciosrow*0.16,2,'.',',').'</td>
                  </tr>
                  <tr>
                    <td style="font-size:13px;" align="right" ><b>TOTAL:</b> </td>
                    <td style="font-size:13px;"> $ '.number_format($preciosrow+($preciosrow*0.16),2,'.',',').'</td>
                  </tr>
                </table>';
      }
}else{
  if($rentaVentaPoliza==2){// renta
    $html.='<table border="0"><tr><td></td></tr></table>';
    if($tiporenta==1){//individual
    }
    
    if($tiporenta==2){//global
      if ($datosCotizacion[0]->eg_rm>0 or $datosCotizacion[0]->eg_rpem>0) {
        /*
        $html.='<table border="1" align="center">
                  <tr><td class="tamanotext">Renta Monocromatico</td ><td class="tamanotext">Volumen incluido monocromatico</td><td class="tamanotext">Excedente Monocromatico</td></tr>
                  <tr>
                    <td class="tamanotext">$ '.number_format($datosCotizacion[0]->eg_rm,2,'.',',').'</td>
                    <td class="tamanotext">'.number_format($datosCotizacion[0]->eg_rvm,0,'.',',').'</td>
                    <td class="tamanotext">$ '.number_format($datosCotizacion[0]->eg_rpem,2,'.',',').'</td>
                  </tr>
                </table>';
        */
      }
      if ($datosCotizacion[0]->eg_rc>0 or $datosCotizacion[0]->eg_rpec>0) {
        /*
        $html.='<table border="1" align="center">
                  <tr><td class="tamanotext">Renta Color</td><td class="tamanotext">Volumen incluido Color</td><td class="tamanotext">Excedente Color</td></tr>
                  <tr>
                    <td class="tamanotext">$'.number_format($datosCotizacion[0]->eg_rc,2,'.',',').'</td>
                    <td class="tamanotext">'.number_format($datosCotizacion[0]->eg_rvc,0,'.',',').'</td>
                    <td class="tamanotext">$'.number_format($datosCotizacion[0]->eg_rpec,2,'.',',').'</td>
                  </tr>
                </table>';
        */
      }

    }
    $html.='<table border="0"><tr><td></td></tr></table>';
  }
}




//========================================================
  if($datosCotizacion[0]->info1!=null){
    $tituloObservaciones= $datosCotizacion[0]->info1;
  }else{
    $tituloObservaciones= $configuracionCotizacion->tituloObservaciones;
  }
  if($datosCotizacion[0]->info2!=null){
    $contenidoObservaciones=$datosCotizacion[0]->info2;
  }else{
    $contenidoObservaciones=$configuracionCotizacion->contenidoObservaciones;
  }
  if($datosCotizacion[0]->info3!=null){
    $titulo2Footer=$datosCotizacion[0]->info3;
  }else{
    $titulo2Footer=$configuracionCotizacion->titulo2Footer;    
  }
  if($datosCotizacion[0]->info4!=null){
    $contenido2Footer=$datosCotizacion[0]->info4;
  }else{
    $contenido2Footer=$configuracionCotizacion->contenido2Footer;  
  }
  $caract   = array(
                    '<br>',
                    '<p><br></p>',
                    '<p>',
                    '</p>',
                    '<p style="margin-bottom:11px">',
                    '&nbsp;',
                    '<table width="79"',
                    'width:60pt;',
                    'width="79"',
                    '​',
                    '<p style="margin-left: 40px;">',
                    '<p  >',
                  );
  $caract2  = array(
                    '','','','<br>','',' ','<table border="0" width="100%" ',' width:100%; ',' width:100%; ','','',''
                  );
  $caract3  = array(
                    '','','','','',' ',''
                  );

  $col1=strval(str_replace($caract, $caract3,$configuracionCotizacion->titulo1Footer));
  $col2=strval(str_replace($caract, $caract2,$configuracionCotizacion->contenido1Footer));
  $col3=strval(str_replace($caract, $caract3,$tituloObservaciones));
  $col4=strval(str_replace($caract, $caract2,$contenidoObservaciones));
  $col5=strval(str_replace($caract, $caract3,$titulo2Footer));
  $col6=strval(str_replace($caract, $caract2,$contenido2Footer));
  $col6=strval(str_replace('<br><br>', '<br>',$col6));
//log_message('error',$col4);
  //var_dump($col2);
  $html.='<table border="0" >
              <tr>
                  <td><b>'.$col1.'</b></td>
              </tr>
              <tr>
                  <td>'.$col2.'</td>
              </tr>
              <tr>
                  <td><b>'.$col3.'</b></td>
              </tr>
              <tr>
                  <td style="font-size:12px;">'.$col4.'</td>
              </tr>
              <tr>
                  <td><b>'.$col5.'</b></td>
              </tr>
              <tr>
                  <td style="font-size:12px;">'.$col6.'</td>
              </tr>
              <tr>
                  <td style="font-size:12px;">'.$configuracionCotizacion->leyendaPrecios.'</td>
              </tr>
          </table>';   
          //log_message('error',$html);     
//================================================
//echo $html;
//var_dump($html);
          //log_message('error',$html);
$pdf->writeHTML($html, true, false, true, false, '');
//$pdf->Output('Captura.pdf', 'I');
$url=FCPATH.'/doccotizacion/';
//$url=$_SERVER['DOCUMENT_ROOT'].'/hulesyg/facturaspdf/';
$pdf->Output($url.'cotizacion_'.$idCotizacion.'.pdf', 'FI');

?>