var base_url = $('#base_url').val();
var table;
var table2
$(document).ready(function() {	
	table = $('#tabla_cotizaciones').DataTable();
	table2 = $('#tabla_ventas_incompletas').DataTable();
	$('#cliente').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una opción',
        allowClear: true,
        ajax: {
            url: base_url+'Ventasi/searchclientes',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.id,
                        text: element.empresa,
                        tipo: element.tipo
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function (e) {
	    var data = e.params.data;
	    $('.datainfolist_ventas').hide();
		$('.datainfolist_cotizaciones').hide();
		$('.datainfo').html('');
	    //console.log(data.id);
	    //console.log(data.tipo);//1 cliente 2 prospecto
	    consultardatos(data.id,data.tipo);
	}).on('select2:clear', function (e) {
	    $('.datainfolist_ventas').hide();
		$('.datainfolist_cotizaciones').hide();
		$('.datainfo').html('');

	});;
});

function consultardatos(clienteid,tipocli){
	$.ajax({
        type:'POST',
        url: base_url+'Atencionclientes/consultardatos',
        data: {
            clienteid: clienteid,
            tipocli: tipocli
            },
            async: false,
            statusCode:{
                404: function(data){
                    swal("Error", "404", "error");
                },
                500: function(){
                    swal("Error", "500", "error"); 
                }
            },
            success:function(data){
                $('.datainfo').html(data);
                $('.dropdown-button').dropdown();
            }
        });

}
function obtenercotizaciones(cliente,tipo){
	$('.datainfolist_cotizaciones').show();
	$('.datainfolist_ventas').hide();
    // Destruye la tabla y la crea de nuevo con los datos cargados 
  /*
    table.destroy();
    table = $('#tabla_cotizaciones').DataTable({
        "ajax": {
            "url": base_url+"index.php/Cotizaciones/getListaCotizacionesPorCliente2",
            type: "post",
            "data": {
                'idCliente':cliente,
                'denegada':0
            },
        },
        "columns": [
            {"data": "id"},
            {"data": "atencion"},
            {"data": "correo"},
            {"data": "telefono"},
            {"data": "tipo",
                 render:function(data,type,row){
                    if(data==1){
                        var html='Equipos';    
                    }else if(data==2){
                        var html='Consumibles';    
                    }else if(data==3){
                        var html='Refacciones';    
                    }else if(data==4){
                        var html='Pólizas';    
                    } 
                    
                    return html;
                }
            },
            {"data": "rentaVentaPoliza",
                 render:function(data,type,row){
                    if(data==1){
                        var html='Venta';    
                    }else if(data==2){
                        var html='Renta';    
                    }else if(data==3){
                        var html='Póliza';    
                    }else{
                        var html='N/A';
                    }
                    
                    return html;
                }
            }, 
            {"data": 'estatus',
                 render:function(data,type,row){
                    if (row.denegada==0) {
                        if(row.estatus==1){
                            var html='Pendiente';    
                        }else if(data==2){
                            var html='Autorizada';    
                        } 
                    }else{
                        var html='Eliminada';
                    }
                    
                    return html;
                }
            }, 
            {"data": 'reg'},
            {"data": "id",
                 render:function(data,type,row)
                 {
                    $('.tooltipped').tooltip();
                    if(row.estatus==1){
                        if (row.denegada==0) {
                            var html='<a class="btn-floating orange  tooltipped" data-position="top" data-delay="50" data-tooltip="Autorizar" onclick="autorizar('+row.id+','+row.tipo+')">\
                                    <i class="material-icons">done</i>\
                                  </a>\
                                  <a class="btn-floating green tooltipped" data-position="top" data-delay="50" data-tooltip="Editar" onclick="editar('+row.id+','+row.tipo+')">\
                                    <i class="material-icons">mode_edit</i>\
                                  </a>\
                                  <a class="btn-floating waves-effect waves-light red accent-2 tooltipped" data-position="top" data-delay="50" data-tooltip="Eliminar" onclick="denegarc('+row.id+')">\
                                    <i class="material-icons">clear</i>\
                                  </a>\
                                ';
                          }else{
                            var html='<a class="btn-floating blue tooltipped" data-position="top" data-delay="50" data-tooltip="Vizualizar" onclick="editar('+row.id+','+row.tipo+')">\
                                    <i class="material-icons">remove_red_eye</i>\
                                  </a>\
                                  <a class="cotizacion_c_'+row.id+' btn-floating waves-effect waves-light red accent-2 tooltipped"\
                                    data-position="top" data-delay="50" data-tooltip="Motivo"\
                                    data-motivo="'+row.motivodenegada+'" onclick="motivod('+row.id+')">\
                                    <i class="material-icons">assignment</i>\
                                  </a>';
                          }
                        
                    }else{
                        
                    }
                    var html='<a class="btn-floating blue tooltipped" data-position="top" data-delay="50" data-tooltip="Vizualizar" onclick="editar('+row.id+','+row.tipo+')">\
                                    <i class="material-icons">remove_red_eye</i>\
                                  </a>';
                    return html;
                }
            }
        ],
        "order": [[ 0, "desc" ]],
    });
*/
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Cotizaciones/obtenercotizaciones",
        data: {
            cli:cliente,
            tipo:tipo
        },
        success: function (data){
            $('.datainfolist_cotizaciones').html(data); 
            $('#tabla_cotizaciones').DataTable();
        }
    });
}
function obtenerventas(cliente){
	$('.datainfolist_ventas').show();
	$('.datainfolist_cotizaciones').hide();
	$('#cliente').val(cliente);
	load();
}
function detallep(id){
    window.open(base_url+"PolizasCreadas/view/"+id, "Prefactura", "width=780, height=612");
}
function load() {
    var cliente= $('#cliente').val();
    var estatus= $('#tipoestatus option:selected').val();
    var tipoventa= $('#tipoventa option:selected').val();
    // Destruye la tabla y la crea de nuevo con los datos cargados 
    table2.destroy();
    table2 = $('#tabla_ventas_incompletas').DataTable({
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url+"index.php/Ventas/getListaVentasIncompletasasi/",
            type: "post",
            "data": {
                'cliente':cliente,
                'status':estatus,
                'tipoventa':tipoventa,
                'estatusventa':1,
                'tipoview':0,
                'idpersonal':0,
                fechainicial_reg:'',
                fechafinal_reg:'',
                emp:1,
                filf:0,
                pagosview:0,
                rfc:'',
                pre:0





            },
        },
        "columns": [
            {"data": "id"},
            {"data": "empresa"},
            
            {"data": "id",
                render:function(data,type,row){
                    if (row.prefactura==1) {
                        var colorbtton='green';
                    }else{
                        var colorbtton='blue';
                    }
                    var html='<a class="btn-floating '+colorbtton+' tooltipped" data-position="top" data-delay="50" data-tooltip="Prefactura" onclick="detallev('+row.id+','+row.combinada+')">\
                                <i class="material-icons">assignment</i>\
                                </a>';
                    return html;
                }
            },
            {"data": "vendedor"},
            {"data": "estatus",
                 render:function(data,type,row){
                    if(data==1){
                        var html='Pendiente';    
                    } 
                    else if(data==2){
                        var html='Facturado';    
                    }
                    else if(data==3){
                        var html='Finalizado';    
                    }  
                        return html;
                }
            },
            {"data": null,
                 render:function(data,type,row){
                    var html='';
                    if (row.prefactura==1) {
                        var html=fecha_vencimiento(row.id);
                    }  
                        return html;

                }
            }, 
            {"data": "combinada",
                 render:function(data,type,row){
                    if(data==1){
                        var html='Venta combinada';    
                    } else{
                        var html='Venta'; 
                    }
                    
                        return html;
                }
            }, 
            //{"data": "idCotizacion"}, 
            {"data": "id",
            "visible": false,
                 render:function(data,type,row){
                    $('.tooltipped').tooltip();
                    if(row.estatus==1){
                        /*
                        var html='<ul>\
                                <li>\
                                  <a class="btn-floating blue tooltipped" data-position="top" data-delay="50" data-tooltip="Cerrar" onclick="completar('+row.id+')">\
                                    <i class="material-icons">done</i>\
                                  </a>\
                                  <a class="btn-floating red tooltipped" data-position="top" data-delay="50" data-tooltip="Finalizar" onclick="finaliza('+row.id+')">\
                                    <i class="material-icons">stop</i>\
                                  </a>\
                                </li>\
                              </ul>';*/
                        var btn_cfdi='';
                        var btn_pago='';
                        if (row.prefactura==1) {
                           btn_cfdi='<a class="btn-floating blue tooltipped" data-position="top" data-delay="50" data-tooltip="CFDI" onclick="modal_cdfi('+row.id+','+row.combinada+')">\
                                        CFDI\
                                      </a>';
                            btn_pago='<a class="btn-floating green tooltipped" data-position="top" data-delay="50" data-tooltip="Pago" onclick="modal_pago('+row.id+','+row.combinada+')">\
                                        <i class="material-icons">attach_money</i>\
                                      </a>';
                        }
                        var html='<a class="btn-floating red tooltipped" data-position="top" data-delay="50" data-tooltip="Finalizar" onclick="finaliza('+row.id+','+row.combinada+')">\
                                    <i class="material-icons">stop</i>\
                                  </a>\
                                  '+btn_cfdi+btn_pago+'';
                    }
                    else{
                        var html='Sin acciones por ejecutar';
                    }
                    return html;
                }
            }
        ],
        "order": [[ 0, "desc" ]],
    });

}
function fecha_vencimiento(id_v){
    var result='';
    $.ajax({
      type: "POST",
      url: base_url+"index.php/Ventas/verificar_fecha_vencimiento",
      data:{id:id_v},
      async:false,
      success: function(data){
        result=data;
      }
    });   
    return result;
}
function detallev(id,tipo){
    if (tipo==0) {
        window.open(base_url+"Prefactura/view/"+id, "Prefactura", "width=780, height=612");
    }else{
        window.open(base_url+"Prefactura/viewc/"+id, "Prefactura", "width=780, height=612");
    }
}
function documento(idasig,tipo,equipo){
    if(tipo==1){
        window.open(base_url+"Listaservicios/contrato_formato/"+idasig+"/"+tipo+"/"+equipo, "Prefactura", "width=780, height=612");
    }else if(tipo==2){
        window.open(base_url+"Listaservicios/tipo_formato/"+idasig+"/"+tipo+"/"+equipo, "Prefactura", "width=780, height=612");
    }else if(tipo==3){
        window.open(base_url+"Listaservicios/tipo_formatoc/"+idasig+"/"+tipo+"/"+equipo, "Prefactura", "width=780, height=612");
    }
} 
function obtenerperiodos(idcontrato,idrenta){
    
    $('.addperiodos').html('');
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Atencionclientes/selectfacturas",
        data: {id:idcontrato},
        success: function (data){
            $('.addperiodos').html(data); 
        }
    });
    setTimeout(function(){ 
        window.open(base_url+'Contratos/contrato/'+idrenta, '_blank');
    }, 1000);
    
}
function contadores(periodo,contrato){
    window.open(base_url+"Reportes/rentadetalles/"+periodo+"/"+contrato, "Contadores", "width=780, height=612");
    var fechainicio = $('.periodoservicio_'+periodo).data('fechainicio');
    var fechafin    = $('.periodoservicio_'+periodo).data('fechafin');

    obtenerservicios(contrato,fechainicio,fechafin);
}
function obtenerservicios(contrato,fechainicio,fechafin){
    $('#modalservicios').modal();
    $('#modalservicios').modal('open');
    $('.addlistservicios').html(''); 
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Atencionclientes/listservicios",
        data: {
            idcontrato:contrato,
            inicio:fechainicio,
            fin:fechafin
        },
        success: function (data){
            $('.addlistservicios').html(data); 
        }
    });
}