var base_url = $('#base_url').val();
$(document).ready(function(){
	upload_data_view();
	$("#files").fileinput({
        showCaption: true,
        showUpload: true,// quita el boton de upload
        //rtl: true,
        allowedFileExtensions: ["jpg","png","webp","jpeg"],
        browseLabel: 'Seleccionar documentos',
        uploadUrl: base_url+'Pagina/baner_imagenes_multiple',
        maxFilePreviewSize: 5000,
        previewFileIconSettings: {
            'docx': '<i class="fa fa-file-word-o text-primary"></i>',
            'xlsx': '<i class="fa fa-file-excel-o text-success"></i>',
            'pptx': '<i class="fa fa-file-powerpoint-o text-danger"></i>',
            'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
            'pdf': '<i class="fa fa-file-pdf-o text-danger"></i>',
            'zip': '<i class="fa fa-file-archive-o text-muted"></i>',
            'cer': '<i class="fas fa-file-invoice"></i>',
        },
        uploadExtraData: function (previewId, index) {
        var info = {
                    idequipo:0
                };
        return info;
      }
    }).on('filebatchuploadcomplete', function(event, files, extra) {
      //location.reload();
        upload_data_view();
    }).on('filebatchuploadsuccess', function(event, files, extra) {
      //location.reload();
      //toastr.success('Se cargo el Certificado Correctamente','Hecho!');
        upload_data_view();
    });
});
function upload_data_view(){
    $.ajax({
        type:'POST',
        url: base_url+'index.php/Pagina/viewimages',
        data: {idequipo:0},
        success:function(data){
            $('.lis_catacter').html(data);
            $('.materialboxed').materialbox();
        }
    });
}
function deleteimg(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de Eliminar la imagen? El cambio será irreversible',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Pagina/deleteimgbaner/",
                    data: {idequipo:id},
                    success: function (response){
                        toastr["success"]("Se ha eliminado con exito", "éxito");
                        upload_data_view();
                    },
                    error: function(response){
                        toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema", "Advertencia");
                    }
                });
            },
            cancelar: function (){
                
            }
        }
    });
}