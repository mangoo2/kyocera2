<?php 
  if(isset($_GET['viewexcel'])){
    $viewexcel=1;
    $tiempo_inicio = microtime(true);
  }else{

    header("Content-Type: text/html;charset=UTF-8");
    header("Pragma: public");
    header("Expires:0");
    header("Cache-Control:must-revalidate,post-check=0, pre-check=0");
    header("Content-Type: application/force-download");
    header("Content-Type: application/octet-stream");
    header("Content-Type: application/download");
    header("Content-Type: application/vnd.ms-excel;");
    header("Content-Disposition: attachment; filename=reporte_".$f1."_".$f2.".xls");
    $viewexcel=0;
  }
?>
<?php 
  
  $t_venta='';
  if($tipoventa==0){  
    $t_venta='Todas';
  }else if($tipoventa==1){
    $t_venta='Venta';
  }else if($tipoventa==2){  
    $t_venta='Rentas';
  }else if($tipoventa==5){  
    $t_venta='Rentas Historial';
  }else if($tipoventa==3){  
    $t_venta='Pólizas Creadas';
  }else if($tipoventa==4){  
    $t_venta='Combinada';
  }
  
  $t_estatus=''; 
  if($tipoventa==1){
    $t_estatus='Pagadas';
  }else if($tipoventa==2){  
    $t_estatus='Pendientes';
  } 
  $row=1;
?>  
<h2>REPORTE DE PREFATURA</h2>
    <table width="100%" border="0"> 
  <thead>
    <tr>
      <td><b>Tipo de listado</b></td>
      <td><b>Cliente</b></td>
      <td><b>Tipo de estatus</b></td>
      <td><b>Fecha inicio</b></td>
      <td><b>Fecha fin</b></td>
      <td><b>Empleado</b></td>
    </tr>
    <tr>
      <td><?php echo utf8_decode($t_venta) ?></td>
      <td><?php echo utf8_decode($cliente) ?></td>
      <td><?php echo $t_estatus ?></td>
      <td><?php echo date('d/m/Y',strtotime($f1)) ?></td>
      <td><?php echo date('d/m/Y',strtotime($f2)) ?></td>
      <td><?php echo utf8_decode($personal) ?></td>
    </tr>
  </thead>
</table>  
<br><br>
<table width="100%" border="1" RULES="rows" style="padding: 5px;" class="table table-striped"> 
  <thead>
    <tr>
      <td class="style_head">#</td>  
      <td class="style_head">Vendedor</td> 
      <td class="style_head">Cliente</td>
      <td class="style_head">Tipo</td>
      <td class="style_head">Estatus</td>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($get_result as $x){ 
      $t_estatusf=''; 
    if($x->estatus==3){
      $t_estatusf='Pagadas';
    }else if($x->estatus==2){  
      $t_estatusf='Pendientes';
    } 
      if($x->tipotabla==4){
        $idpre=$x->equipo;
      }else{
        $idpre=$x->id;
      }
      ?>
      <tr class="row_<?php echo $row;?>">
        <td><?php echo $idpre; ?></td>
        <td><?php echo utf8_decode($x->vendedor) ?></td>
        <td><?php echo utf8_decode($x->empresa) ?></td>
        <td><?php echo utf8_decode($x->tabla) ?></td>
        <td><?php echo $t_estatusf ?></td>       
      </tr>
    <?php $row++;} ?>  
  </tbody>
</table>
<?php 
  if($viewexcel==1){
    // Fin del cronómetro
    $tiempo_fin = microtime(true);
    $tiempo_total = $tiempo_fin - $tiempo_inicio;

    echo "El script tardó " . $tiempo_total . " segundos en ejecutarse.";
  }
?>