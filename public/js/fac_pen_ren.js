var base_url =$('#base_url').val();
var idpersonal_aux = $('#idpersonal').val(); 
var tableload;
$(document).ready(function($) {
	tableload=$('#tabla_accesorios').DataTable();
	loadtable();
    //datepicker();
    $('.modal').modal();
    $('#idcliente').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una opción',
        allowClear: true,
        ajax: {
            url: base_url+'Ventasi/searchclientes',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.id,
                        text: element.empresa

                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function (e) {
        var data = e.params.data;
        
    });
});
function loadtable(){
    var f1=$('#f1').val();
    var f2=$('#f2').val();
    var tipo=$('#tipo option:selected').val();
    var tipov=$('#tipov option:selected').val();
    if(tipo==1){
        $('.tipov_div').show('show');
    }else{
        $('.tipov_div').hide('show');
    }
    var idcliente=$('#idcliente option:selected').val()==undefined?0:$('#idcliente option:selected').val();
    console.log('cliente '+idcliente);
	tableload.destroy();
    if(tipo==2){
        tableload=$("#tabla_accesorios").DataTable({
            search: {
                return: true
            },
            responsive: !0,
            "bProcessing": true,
            "ajax": {
               "url": base_url+"Fac_pen_ren/getlistpendientes",
               type: "post",
               "data": {
                        fechas:f1,
                        fechas2:f2,
                        tipo:tipo,
                        tipov:tipov,
                        cli:idcliente
                },
                error: function(){
                   $("#table").css("display","none");
                }
            },
            "columns": [
                {"data": "idcontrato"},
                {"data": "empresa"},
                {"data": "periodo_inicial"},
                {"data": "periodo_final"},
                {"data": null,
                    "render": function ( data, type, row, meta ){
                        var html=row.nombre+' '+row.apellido_paterno+' '+row.apellido_materno;     
                    return html;
                    }
                },
                {"data": "fechatimbre"},
                {"data": null,
                    "render": function ( data, type, row, meta ){
                        var html=row.nombref+' '+row.apellido_paternof+' '+row.apellido_maternof;     
                    return html;
                    }
                },
                {"data": null,
                    "render": function ( data, type, row, meta ){
                        var html='';
                        if(row.estatus==1){
                            html='<a class="waves-effect cyan btn-bmz" href="'+base_url+'Configuracionrentas/ContratosPagos/'+row.idcontrato+'">Pendiente</a>';
                        }
                    return html;
                    }
                },
                {"data": null,
                    "render": function ( data, type, row, meta ){
                        var html='';
                        if(row.cargadas==1){
                            var num_idper=parseFloat(idpersonal_aux);
                            if(num_idper!=13){//50 marcela 13 susana
                                if(tipo==1){
                                    if(row.asignadas==1){
                                        html='<a class="waves-effect cyan btn-bmz" onclick="modal_asignar('+row.prefId+')">Asignada</a>';     
                                    }else{
                                        html='<a class="waves-effect green btn-bmz" onclick="modal_asignar('+row.prefId+')">Asignar</a>';     
                                    }
                                }
                            }
                            if(row.fechaasignada!=null){
                                html+='<br>'+row.fechaasignada;
                                if(row.prioridad>0){
                                   html+='<br><span class="prioridad_'+row.prioridad+'">Prioridad '+row.prioridad+'</span>'; 
                                }
                                html+='<br>'+row.comen;
                            }
                        }else{
                            html='Pendiente de Generar';
                        }
                    return html;
                    }
                },
                {"data": null,
                    "render": function ( data, type, row, meta ){
                        var html='';
                        if(row.cargadas==1){
                            var num_idper=parseFloat(idpersonal_aux);
                            if(num_idper!=13){//50 marcela 13 susana
                                if(row.estatus==1){
                                    if(tipo==1){
                                    html='<a class="waves-effect red btn-bmz" onclick="delete_reg('+row.prefId+')">Descartar</a>';     
                                    }
                                } 
                            }
                        }
                    return html;
                    }
                },
            ],
            "order": [[ 0, "desc" ]],
            "columnDefs": [
                { "visible": true, "targets": 5 },
                { "visible": true, "targets": 6 },
                { "visible": false, "targets": 7 },
                { "visible": false, "targets": 8 },
                { "visible": false, "targets": 9 },
            ]
        });
    }else{
    	tableload=$("#tabla_accesorios").DataTable({
            responsive: !0,
            "bProcessing": true,
            
            search: {
                        return: true
                    },
            "ajax": {
               "url": base_url+"Fac_pen_ren/getlistpendientes",
               type: "post",
               "data": {
                        'fechas':f1,'fechas2':f2,'tipo':tipo,'tipov':tipov,cli:idcliente
                },
                error: function(){
                   $("#table").css("display","none");
                }
            },
            "columns": [
                {"data": "idcontrato"},
                {"data": "empresa"},
                {"data": "periodo_inicial"},
                {"data": "periodo_final"},
                {"data": null,
                    "render": function ( data, type, row, meta ){
                        var html=row.nombre+' '+row.apellido_paterno+' '+row.apellido_materno;     
                    return html;
                    }
                },
                {"data": "fechatimbre"},
                {"data": null,
                    "render": function ( data, type, row, meta ){
                        var html=row.nombref+' '+row.apellido_paternof+' '+row.apellido_maternof;     
                    return html;
                    }
                },
                {"data": null,
                    "render": function ( data, type, row, meta ){
                        var html='';
                        if(row.estatus==1){
                            if(tipo==1){
                                html='<a class="waves-effect cyan btn-bmz" href="'+base_url+'Configuracionrentas/ContratosPagos/'+row.idcontrato+'">Pendiente</a>';
                            }
                        }
                    return html;
                    }
                },
                {"data": null,
                    "render": function ( data, type, row, meta ){
                        var html='';
                        var num_idper=parseFloat(idpersonal_aux);
                        if(row.cargadas==1){
                        
                            if(num_idper!=13){//50 marcela 13 susana
                                if(tipo==1){
                                    if(row.asignadas==1){
                                        html='<a class="waves-effect cyan btn-bmz" onclick="modal_asignar('+row.prefId+')">Asignada</a>';     
                                    }else{
                                        html='<a class="waves-effect green btn-bmz" onclick="modal_asignar('+row.prefId+')">Asignar</a>';     
                                    }
                                }
                            }
                            if(row.fechaasignada!=null && row.fechaasignada!='0000-00-00'){
                                html+='<br>'+row.fechaasignada;
                                if(row.prioridad>0){
                                   html+='<br><span class="prioridad_'+row.prioridad+'">Prioridad '+row.prioridad+'</span>'; 
                                }
                                html+='<br>'+row.comen;
                            }
                        }else{
                            var fecha1="'"+row.periodo_inicial+"'";
                            var fecha2="'"+row.periodo_final+"'";
                            if(num_idper!=13){//50 marcela 13 susana
                                if(row.asignadas==1){
                                    html+='<a class="waves-effect cyan btn-bmz" onclick="modal_asignar_t('+row.idcontrato+','+fecha1+','+fecha2+')">Asignada</a>'; 
                                      
                                }else{
                                    html+='<a class="waves-effect green btn-bmz" onclick="modal_asignar_t('+row.idcontrato+','+fecha1+','+fecha2+')">Asignar</a>';     
                                }
                            }
                            html+="Pendiente de Generar";
                            if(row.fechaasignada!=null && row.fechaasignada!='0000-00-00'){
                                html+='<br>'+row.fechaasignada;
                                if(row.prioridad>0){
                                   html+='<br><span class="prioridad_'+row.prioridad+'">Prioridad '+row.prioridad+'</span>'; 
                                }
                                html+='<br>'+row.comen;
                            }  
                        }
                    return html;
                    }
                },
                {"data": null,
                    "render": function ( data, type, row, meta ){
                        var fecha1="'"+row.periodo_inicial+"'";
                        var fecha2="'"+row.periodo_final+"'";
                        var html='';
                        if(row.cargadas==1){
                            var num_idper=parseFloat(idpersonal_aux);
                            if(num_idper!=13){//50 marcela 13 susana
                                if(row.estatus==1){
                                    if(tipo==1){
                                        html='<a class="waves-effect red btn-bmz" onclick="delete_reg('+row.prefId+')">Descartar</a>';     
                                    }
                                }else{
                                    html='Motivo: '+row.estatus_motivo+'<br>';
                                    if(row.estatus_confirm==1){
                                        html+='Confirmado <i class="material-icons" style="color:green;">done_all</i>';
                                    }else{
                                        html+='Sin Confirmar <a class="new badge yellow" style="padding: 9px 2px 0px;" onclick="confirmarpagov('+row.prefId+')"><i class="material-icons">warning</i> </a>';
                                    }
                                }  
                            }else{
                                html='<a class="waves-effect red btn-bmz" onclick="delete_reg('+row.prefId+')">Descartar</a>'; 
                            }
                        }else{
                            if(row.estatus==1){
                                html='<a class="waves-effect red btn-bmz" onclick="delete_reg_t('+row.idcontrato+','+fecha1+','+fecha2+')">Descartar</a>';  
                            }else{
                                html='Motivo: '+row.estatus_motivo+'<br>';
                                if(row.estatus_confirm==1){
                                    html+='Confirmado <i class="material-icons" style="color:green;">done_all</i>';
                                }else{
                                    html+='Sin Confirmar <a class="new badge yellow" style="padding: 9px 2px 0px;" onclick="confirmarpagovt('+row.idcontrato+','+fecha1+','+fecha2+')"><i class="material-icons">warning</i> </a>';
                                }
                            } 
                        }
                    return html;
                    }
                },
            ],
            "order": [[ 0, "desc" ]],
            "columnDefs": [
                { "visible": false, "targets": 5 },
                { "visible": false, "targets": 6 },
                { "visible": true, "targets": 7 },
                { "visible": true, "targets": 8 },
                { "visible": true, "targets": 9 }
            ]
        });
    }
}

function datepicker(){
    var urlicon=base_url+'public/img/calendar-color.svg';
    $( "#fechaselected" ).datepicker({
        showOn: "button",
        dateFormat:"yy-mm-dd",
        showOn: 'both',
        
        showButtonPanel: true,
        buttonImage: urlicon,
        closeText: 'Limpiar',
        onSelect: function (date) {
            console.log(date);
            //window.location.href = '<?php echo base_url(); ?>Configuracionrentas/servicios?fech='+date;
            loadtable()
        },
        onClose: function (dateText, inst) {
            var event = arguments.callee.caller.caller.arguments[0];
            // If "Clear" gets clicked, then really clear it
            if ($(event.delegateTarget).hasClass('ui-datepicker-close')) {
                $(this).val('');
                loadtable()
            }
            
        }
        
    });
}

function delete_reg(idreg){
    var html='¿Está seguro de descartar este registro?<br><label>Ingrese Motivo</label><input type="text" id="motivodelete">';
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: html,
        type: 'red',
        typeAnimated: true,
        buttons: 
        {
            confirmar: function (){
                var mot=$('#motivodelete').val();
                $.ajax({
                    type: "post",
                    url: base_url+"index.php/Fac_pen_ren/eliminar_reg/",
                    data:{
                            idreg:idreg,
                            mot:mot
                        },
                    success: function (response){
                        setTimeout(function(){ 
                            loadtable();
                        }, 1000);      
                    },
                    error: function(response){ 
                    }
                });
            },
            cancelar: function () 
            {
            }
        }
    });
}
function delete_reg_t(idcont,fini,ffin){
    var html='¿Está seguro de descartar este registro?<br><label>Ingrese Motivo</label><input type="text" id="motivodelete">';
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: html,
        type: 'red',
        typeAnimated: true,
        buttons: 
        {
            confirmar: function (){
                var mot=$('#motivodelete').val();
                $.ajax({
                    type: "post",
                    url: base_url+"index.php/Fac_pen_ren/eliminar_reg_t",
                    data:{
                            mot:mot,
                            idcont:idcont,
                            fini:fini,
                            ffin:ffin
                        },
                    success: function (response){
                        setTimeout(function(){ 
                            loadtable();
                        }, 1000);      
                    },
                    error: function(response){ 
                    }
                });
            },
            cancelar: function () 
            {
            }
        }
    });
}
var id_reg=0;
function modal_asignar(id){
    id_reg=id;
    $('#modalasignar').modal('open');
}
var idcont_v=0;
var fini_v='';
var ffin_v='';
function modal_asignar_t(idcont,fini,ffin){
    idcont_v=idcont;
    fini_v=fini;
    ffin_v=ffin;
    $('#modal_asignar_t').modal('open');
}
function saveasignar(){
    var check=0;
    if($('#asignarcheck').is(':checked')){
        check=1;
    }else{
        check=0;
    }
    var fecha = $('#fechaasig').val();
    var prioridad = $('#prioridad2').val();
    var comentario = $('#comentario_asig').val();
    $.ajax({
        type: "post",
        url: base_url+"index.php/Fac_pen_ren/asignar_reg",
        data:{
            check:check,
            fecha:fecha,
            idreg:id_reg,
            prio:prioridad,
            comen:comentario
        },
        success: function (response){
            setTimeout(function(){ 
                loadtable();
            }, 1000);      
        },
        error: function(response){ 
        }
    });
}
function saveasignart(){
    var check=0;
    if($('#asignarcheckt').is(':checked')){
        check=1;
    }else{
        check=0;
    }
    var fecha = $('#fechaasigt').val();
    var prioridad = $('#prioridad2t').val();
    var comentario = $('#comentario_asigt').val();
    $.ajax({
        type: "post",
        url: base_url+"index.php/Fac_pen_ren/asignar_reg_t",
        data:{
            check:check,
            fecha:fecha,
            idcont:idcont_v,
            fini:fini_v,
            ffin:ffin_v,
            prio:prioridad,
            comen:comentario
        },
        success: function (response){
            setTimeout(function(){ 
                loadtable();
            }, 1000);      
        },
        error: function(response){ 
        }
    });
}
function confirmarpagov(prefId){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: 'Se necesita permisos de administrador<br>'+
                 '<input type="password" placeholder="Contraseña" id="contrasena" class="name form-control" required/>',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var pass=$('#contrasena').val();
                if (pass!='') {
                    $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Sistema/solicitarpermiso",
                        data: {
                            pass:pass
                        },
                        success: function (response){
                                var respuesta = parseInt(response);
                                if (respuesta==1) {
                    //=============================================================================
                        $.ajax({
                            type:'POST',
                            url: base_url+"index.php/Fac_pen_ren/confirmardes",
                            data: {
                                prefId:prefId
                            },
                            success: function (response){
                                loadtable();
                            },
                            error: function(response){
                                notificacion(1);  
                                 
                            }
                        });
                    //=============================================================================
                                }else{
                                    notificacion(2);
                                }
                        },
                        error: function(response){
                            notificacion(1);   
                             
                        }
                    });

                }else{
                    notificacion(0);
                }
                
            },
            cancelar: function (){
                
            }
        }
    });
}
function confirmarpagovt(con,fini,ffin){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: 'Se necesita permisos de administrador<br>'+
                 '<input type="password" placeholder="Contraseña" id="contrasena" class="name form-control" required/>',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var pass=$('#contrasena').val();
                if (pass!='') {
                    $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Sistema/solicitarpermiso",
                        data: {
                            pass:pass
                        },
                        success: function (response){
                                var respuesta = parseInt(response);
                                if (respuesta==1) {
                    //=============================================================================
                        $.ajax({
                            type:'POST',
                            url: base_url+"index.php/Fac_pen_ren/confirmardes_t",
                            data: {
                                con:con,
                                fini:fini,
                                ffin:ffin
                            },
                            success: function (response){
                                loadtable();
                            },
                            error: function(response){
                                notificacion(1);  
                                 
                            }
                        });
                    //=============================================================================
                                }else{
                                    notificacion(2);
                                }
                        },
                        error: function(response){
                            notificacion(1);   
                        }
                    });

                }else{
                    notificacion(0);
                }
                
            },
            cancelar: function () 
            {
                
            }
        }
    });
}
