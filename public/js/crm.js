var base_url = $('#base_url').val();
var idProspecto = $('#idProspecto').val();

$(document).ready(function(){
    table = $('#tabla_llamadas_agendadas').DataTable();
    table2 = $('#tabla_correos_agendadas').DataTable();
    table3 = $('#tabla_visitas_agendadas').DataTable();
    load();
    loadCorreos();
    loadVisitas();

    /* initialize the calendar
      -----------------------------------------------------------------*/
      $('#calendar').fullCalendar({
      	header: {
          left: 'prev,next today',
          center: 'title',
          right: 'month,basicWeek,basicDay'
        },
        defaultDate: new Date(),
        editable: false,
        droppable: true, // this allows things to be dropped onto the calendar
        eventLimit: true, // allow "more" link when too many events
        events: base_url+'index.php/crm/eventos/'+idProspecto,
        eventClick: function (calEvent) {
           $.confirm({
                boxWidth: '30%',
                useBootstrap: false,
                icon: 'fa fa-info',
                title: 'Comentario del evento: ',
                content: calEvent.comentario,
                type: 'red',
                typeAnimated: true,
                buttons: 
                {
                    realizado: function () 
                    {
                        if(calEvent.estatus==2)
                        {
                            $.alert({
                                boxWidth: '30%',
                                icon: 'fa fa-danger',
                                title: 'Atención',
                                content: 'El registro ya se ha llevado a cabo, no se puede actualizar nuevamente',
                                type: 'red',
                                typeAnimated: true
                            });
                        }
                        else
                        {
                            $.ajax({
                            url: base_url+"index.php/crm/cambiarEstatusRegistroAgenda/",
                            data: {idRegistro : calEvent.id,
                                    tipo: calEvent.tipo},
                                type:'POST',
                                success: function (response) 
                                {
                                    if(response==1)
                                    {
                                        $.alert({
                                            boxWidth: '30%',
                                            icon: 'fa fa-success',
                                            title: 'Éxito',
                                            content: 'Registro actualizado correctamente',
                                            type: 'red',
                                            typeAnimated: true
                                        });
                                        setTimeout(function(){ 
                                            location.reload(); 
                                        }, 2000);
                                    }
                                    
                                },
                                error: function(response)
                                {
                                    $.alert({
                                            boxWidth: '30%',
                                            useBootstrap: false,
                                            title: 'Atención!',
                                            content: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema'});   
                                    
                                }
                            });
                        }


                    },
                    cancelar: function () 
                    {}
                }
            });

        }
      });

    // Asignamos el formulario de envío de agenda de llamadas
    var formulario_llamadas = $('#llamadas_from');

    // Validamos el formulario en base a las reglas mencionadas debajo
    formulario_llamadas.validate({
            ignore: "",
            rules: 
            {
                proxima: {
                    required: true
                },
                horaProxima: {
                    required: true
                },
                comentario: {
                    required: true
                },
                motivo: {
                    required: true
                }
            },
            // Para mensajes personalizados
            messages: 
            {
                proxima:{
                    required: "Ingrese una fecha permitida"
                },
                horaProxima:{
                    required: "Ingrese una hora permitida"
                },
                comentario:{
                    required: "Ingrese un comentario"
                },
                motivo:{
                    required: "Ingrese el motivo de la siguiente llamada"
                }
            },
            errorElement : 'div',
            // Si están correctos los campos, procesamos el formulario mediante Ajax
            submitHandler: function (form) 
            {
                var datos = formulario_llamadas.serialize();
                
                $.ajax({
                    type:'POST',
                    url: base_url+'index.php/crm/insertaLlamada',
                    data: datos,
                    success:function(data)
                    {
                        // Si existe el usuario, mostramos el mensaje de entrada y redirigimos
                        if(data!=0)
                        {
                            swal({ title: "Éxito",
                                text: "La llamada se ha agendado de manera correcta",
                                type: 'success',
                                showCancelButton: false,
                                allowOutsideClick: false,
                            });
                            
                            setTimeout(function(){ 
                                location.reload(); 
                            }, 2000);
                        }
                        // En caso contrario, se notifica
                        else
                        {
                            swal({ title: "Error",
                                text: "La llamada no pudo guardarse. Contacte al administrador del sistema",
                                type: 'error',
                                showCancelButton: false,
                                allowOutsideClick: false,
                            });
                        }
                    }
                });
                
            }
        });

    // Asignamos el formulario de envío de agenda de llamadas
    var formulario_correos = $('#correos_from');

    // Validamos el formulario en base a las reglas mencionadas debajo
    formulario_correos.validate({
            ignore: "",
            rules: 
            {
                proxima: {
                    required: true
                },
                horaProxima: {
                    required: true
                },
                comentario: {
                    required: true
                },
                motivo: {
                    required: true
                }
            },
            // Para mensajes personalizados
            messages: 
            {
                proxima:{
                    required: "Ingrese una fecha permitida"
                },
                horaProxima:{
                    required: "Ingrese una hora permitida"
                },
                comentario:{
                    required: "Ingrese un comentario"
                },
                motivo:{
                    required: "Ingrese el motivo de la siguiente llamada"
                }
            },
            errorElement : 'div',
            // Si están correctos los campos, procesamos el formulario mediante Ajax
            submitHandler: function (form) 
            {
                var datos = formulario_correos.serialize();
                
                $.ajax({
                    type:'POST',
                    url: base_url+'index.php/crm/insertaCorreo',
                    data: datos,
                    success:function(data)
                    {
                        // Si existe el usuario, mostramos el mensaje de entrada y redirigimos
                        if(data!=0)
                        {
                            swal({ title: "Éxito",
                                text: "La llamada se ha agendado de manera correcta",
                                type: 'success',
                                showCancelButton: false,
                                allowOutsideClick: false,
                            });
                            
                            setTimeout(function(){ 
                                location.reload(); 
                            }, 2000);
                        }
                        // En caso contrario, se notifica
                        else
                        {
                            swal({ title: "Error",
                                text: "El correo no pudo guardarse. Contacte al administrador del sistema",
                                type: 'error',
                                showCancelButton: false,
                                allowOutsideClick: false,
                            });
                        }
                    }
                });
                
            }
        });


    // Asignamos el formulario de envío de agenda de llamadas
    var formulario_visitas = $('#visitas_form');

    // Validamos el formulario en base a las reglas mencionadas debajo
    formulario_visitas.validate({
            ignore: "",
            rules: 
            {
                proxima: {
                    required: true
                },
                horaProxima: {
                    required: true
                },
                comentario: {
                    required: true
                },
                motivo: {
                    required: true
                }
            },
            // Para mensajes personalizados
            messages: 
            {
                proxima:{
                    required: "Ingrese una fecha permitida"
                },
                horaProxima:{
                    required: "Ingrese una hora permitida"
                },
                comentario:{
                    required: "Ingrese un comentario"
                },
                motivo:{
                    required: "Ingrese el motivo de la siguiente visita"
                }
            },
            errorElement : 'div',
            // Si están correctos los campos, procesamos el formulario mediante Ajax
            submitHandler: function (form) 
            {
                var datos = formulario_visitas.serialize();
                
                $.ajax({
                    type:'POST',
                    url: base_url+'index.php/crm/insertaVisita',
                    data: datos,
                    success:function(data)
                    {
                        // Si existe el usuario, mostramos el mensaje de entrada y redirigimos
                        if(data!=0)
                        {
                            swal({ title: "Éxito",
                                text: "La visita se ha agendado de manera correcta",
                                type: 'success',
                                showCancelButton: false,
                                allowOutsideClick: false,
                              });
                            
                            setTimeout(function(){ 
                                location.reload(); 
                            }, 2000);
                        }
                        // En caso contrario, se notifica
                        else
                        {
                            swal({ title: "Error",
                                text: "La visita no pudo guardarse. Contacte al administrador del sistema",
                                type: 'error',
                                showCancelButton: false,
                                allowOutsideClick: false,
                            });
                        }
                    }
                });
                
            }
        });

});


function load() 
{
	var idProspecto = $('#idProspecto').val();
    // Destruye la tabla y la crea de nuevo con los datos cargados 
    table.destroy();
    table = $('#tabla_llamadas_agendadas').DataTable({
        "ajax": {
            "url": base_url+"index.php/crm/getListadoLlamadasPorProspecto/"+idProspecto
        },
        "columns": [
            {"data": "id"},
            {"data": "motivo"},
            {"data": "comentario"},
            {"data": "proxima"},
            {"data": "horaProxima"},
            {"data": "idProspecto", visible: false}
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[25, 50, 100], [25, 50, 100]],
        // Cambiamos lo principal a Español
        "language": {
            "lengthMenu": "Desplegando _MENU_ elementos por página",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
              }
        }
    });
}

function loadCorreos() 
{
	var idProspecto = $('#idProspecto').val();
    // Destruye la tabla y la crea de nuevo con los datos cargados 
    table2.destroy();
    table2 = $('#tabla_correos_agendadas').DataTable({
        "ajax": {
            "url": base_url+"index.php/crm/getListadoCorreosPorProspecto/"+idProspecto
        },
        "columns": [
            {"data": "id"},
            {"data": "motivo"},
            {"data": "comentario"},
            {"data": "proxima"},
            {"data": "horaProxima"},
            {"data": "idProspecto", visible: false}
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[25, 50, 100], [25, 50, 100]],
        // Cambiamos lo principal a Español
        "language": {
            "lengthMenu": "Desplegando _MENU_ elementos por página",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
              }
        }
    });
}

function loadVisitas() 
{
    var idProspecto = $('#idProspecto').val();
    // Destruye la tabla y la crea de nuevo con los datos cargados 
    table3.destroy();
    table3 = $('#tabla_visitas_agendadas').DataTable({
        "ajax": {
            "url": base_url+"index.php/crm/getListadoVisitasPorProspecto/"+idProspecto
        },
        "columns": [
            {"data": "id"},
            {"data": "motivo"},
            {"data": "comentario"},
            {"data": "proxima"},
            {"data": "horaProxima"},
            {"data": "idProspecto", visible: false}
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[25, 50, 100], [25, 50, 100]],
        // Cambiamos lo principal a Español
        "language": {
            "lengthMenu": "Desplegando _MENU_ elementos por página",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
              }
        }
    });
}



  
