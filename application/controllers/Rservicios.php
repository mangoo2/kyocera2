<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Rservicios extends CI_Controller {
    public function __construct()     {
        parent::__construct();

        $this->load->model('ModeloCatalogos');
        $this->load->model('Configuraciones_model');
        $this->load->model('ModeloServicio');
        $this->load->helper('url');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->submenu=58;
        if($this->session->userdata('logeado')==true){
            $this->perfilid =$this->session->userdata('perfilid');
            $this->idpersonal = $this->session->userdata('idpersonal');
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,$this->submenu);// 52 es el id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
        }else{
            redirect('login'); 
        }
    }

    function index(){
        $data['MenusubId']=$this->submenu;
        $month_start = strtotime('first day of this month', time());
        $data['primerdiames'] = date('Y-m-d', $month_start);

        $month_end = strtotime('last day of this month', time());
        $data['ultimodiames'] = date('Y-m-d', $month_end);
        //$data['resultstec']=$this->Configuraciones_model->searchTecnico(); 
        $data['resultstec']=$this->Configuraciones_model->view_session_searchTecnico(); 
            $this->load->view('header');
            $this->load->view('main');
            $this->load->view('Servicio/sreportes',$data);
            $this->load->view('footer');
            $this->load->view('Servicio/sreportesjs');
    }
    function reportetecnicos(){
        $fechaini=$_GET['fi'];
        $fechafi=$_GET['ff'];
        $tecnico = 0;
        $resultadoreporte=$this->ModeloServicio->reportetecnicose($fechaini,$fechafi,$tecnico);
        foreach ($resultadoreporte->result() as $item) {
            echo $item->nombre.' '.$item->setotal;
        }
    }
    function tecnicos(){
        $params = $this->input->post();
        $fechaini=$params['fi'];
        $fechafi=$params['ff'];
        $resultstec=$this->Configuraciones_model->searchTecnico();
        $html='';
        foreach ($resultstec->result() as $itemst) {
            $resultadoreporte=$this->ModeloServicio->reportetecnicose($fechaini,$fechafi,$itemst->personalId);
            $resultadoreporte=$resultadoreporte->result();
            $setotal=$resultadoreporte[0]->setotal;
            $tiempohabil=$resultadoreporte[0]->tiempohabil;
            $tiempomuerto=$resultadoreporte[0]->tiempomuerto;
            $cot_status=$resultadoreporte[0]->cot_status;
            $servicioscorrectivos=$this->ModeloServicio->reportetecnicosecorrectivos($fechaini,$fechafi,$itemst->personalId);
            $serviciosgarantias=$this->ModeloServicio->reportetecnicosegarantias($fechaini,$fechafi,$itemst->personalId);
            $confianza=$serviciosgarantias/$setotal;
             $html.='<tr>
                      <td>'.$itemst->tecnico.'</td>
                      <td>'.$setotal.'</td>
                      <td>'.$servicioscorrectivos.'</td>
                      <td>KM Recorrido</td>
                      <td>'.$tiempohabil.'</td>
                      <td>'.$tiempomuerto.'</td>
                      <td>'.$serviciosgarantias.'</td>
                      <td>Eficiencia del recurso</td>
                      <td>'.$confianza.'</td>
                      <td>'.$cot_status.'</td>
                    </tr>';
         } 
         echo $html;
    }
    function reporteatencioncorrectivo(){
        $params = $this->input->post();
        $fechaini=$params['fi'];
        $fechafi=$params['ff'];
        $resultadoreporte=$this->ModeloServicio->reporteatencioncorrectivo($fechaini,$fechafi);
        $html='';
        foreach ($resultadoreporte->result() as $item) {
            $date1 = new DateTime($item->reg);
            $date2 = new DateTime($item->llegada);
            $diff = $date1->diff($date2);
            if ($diff->days>0) {
                $tiempo=$diff->days.' dias';
            }else{
                $tiempo=$diff->h.' horas';
            }
            $html.='<tr>
                      <td>'.$item->empresa.'</td>
                      <td>'.$item->reg.'</td>
                      <td>'.$item->llegada.'</td>
                      <td>'.$tiempo.'</td>
                      <td>'.$item->nombre.'</td>
                      <td>'.$item->tiempoenservicio.'</td>
                    </tr>';
        }
        echo $html;
    }
    function reportefallas(){
        $params = $this->input->post();
        $fechaini=$params['fi'];
        $fechafi=$params['ff'];
        $resultadoreporte=$this->ModeloServicio->reportefallas($fechaini,$fechafi);
        $html='';
        foreach ($resultadoreporte->result() as $item) {
            if ($item->sinmodelo==0) {
                $modelo=$item->modelo;
            }else{
                $modelo=$item->newmodelo;
            }
            $html.='<tr>
                      <td>'.$item->empresa.'</td>
                      <td>'.$item->fecha.'</td>
                      <td>'.$item->hora.'</td>
                      <td>'.$modelo.'</td>
                      <td>'.$item->serie.'</td>
                      <td></td>
                      <td>'.$item->descripcion.'</td>
                      <td>'.$item->nombre.'</td>
                      <td>'.$item->tiempoenservicio.'</td>
                      <td>'.$item->tserviciomotivo.'</td>
                    </tr>';
        }
        echo $html;
    }
    function rservicios(){
        $params = $this->input->post();
        $fechaini=$params['fi'];
        $fechafi=$params['ff'];
            $noservicios=$this->ModeloServicio->reporteservicioscount($fechaini,$fechafi);
            $cancelados=$this->ModeloServicio->reporteservicioscountcancelados($fechaini,$fechafi);
            $cotizados=$this->ModeloServicio->reporteservicioscountcotizaciones($fechaini,$fechafi);

            $rpreventivo=$this->ModeloServicio->reporteservicioscountg($fechaini,$fechafi,1,1);
            $rcorrectivo=$this->ModeloServicio->reporteservicioscountg($fechaini,$fechafi,1,2);
            $rasesoria=$this->ModeloServicio->reporteservicioscountg($fechaini,$fechafi,1,3);
            $rgarantia=$this->ModeloServicio->reporteservicioscountg($fechaini,$fechafi,1,4);

            $ppreventivo=$this->ModeloServicio->reporteservicioscountg($fechaini,$fechafi,2,1);
            $pcorrectivo=$this->ModeloServicio->reporteservicioscountg($fechaini,$fechafi,2,2);
            $pasesoria=$this->ModeloServicio->reporteservicioscountg($fechaini,$fechafi,2,3);
            $pgarantia=$this->ModeloServicio->reporteservicioscountg($fechaini,$fechafi,2,4);

            $epreventivo=$this->ModeloServicio->reporteservicioscountg($fechaini,$fechafi,3,1);
            $ecorrectivo=$this->ModeloServicio->reporteservicioscountg($fechaini,$fechafi,3,2);
            $easesoria=$this->ModeloServicio->reporteservicioscountg($fechaini,$fechafi,3,3);
            $egarantia=$this->ModeloServicio->reporteservicioscountg($fechaini,$fechafi,3,4);


            $garantiasgg=$rgarantia+$pgarantia+$egarantia;
            $datosgeneral=$this->ModeloServicio->reportetecnicosegeneral($fechaini,$fechafi);
            $datosgeneral=$datosgeneral->result();
            //$setotal=$datosgeneral[0]->setotal;
            $tiempohabil=$datosgeneral[0]->tiempohabil;
            $tiempomuerto=$datosgeneral[0]->tiempomuerto;
            //$cot_status=$datosgeneral[0]->cot_status;
            $html='<tr>
                      <td>'.$noservicios.'</td>
                      <td>'.$rpreventivo.'</td>
                      <td>'.$rcorrectivo.'</td>
                      <td>'.$rasesoria.'</td>
                      <td>'.$rgarantia.'</td>
                      <td>'.$ppreventivo.'</td>
                      <td>'.$pcorrectivo.'</td>
                      <td>'.$pasesoria.'</td>
                      <td>'.$pgarantia.'</td>
                      <td>'.$epreventivo.'</td>
                      <td>'.$ecorrectivo.'</td>
                      <td>'.$easesoria.'</td>
                      <td>'.$egarantia.'</td>
                      <td>KMs Recorridos</td>
                      <td>'.$tiempohabil.'</td>
                      <td>'.$tiempomuerto.'</td>
                      <td>'.$garantiasgg.'</td>
                      <td>'.$cotizados.'</td>
                      <td>'.$cancelados.'</td>
                    </tr>';
        echo $html;
    }

    


    
}
?>
