var base_url = $('#base_url').val();
$(document).ready(function() {	
    // Destruye la tabla y la crea de nuevo con los datos cargados 
    table = $('#tabla_solicitudcontrato').DataTable({
        "ajax": {
            "url": base_url+"index.php/RentasCompletadas/getListadocontratosRentasIncompletas"
        },
        "columns": [
            {"data": "id"},
            {"data": "empresa"},
            {"data": "atencion"},
            {"data": "telefono"},
            {"data": "modelo"},
            {"data": "area",
                 render:function(data,type,row)
                 {
                    if(data==null)
                    {
                        return 'Por definir';
                    }
                    else
                    {
                        return data;
                    }
                }
            },
            {"data": "plazo",
                 render:function(data,type,row)
                 {
                    if(data==null)
                    {
                        return 'Por definir';
                    }
                    else
                    {
                        return data;
                    }
                }
            },
            {"data": "estatus"},
            {"data": "idCotizacion"},
            {"data": "idDetalleRenta", visible: false},
            {"data": "idEquipo", visible: false},
            {"data": "costo_pagina_monocromatica"},
            {"data": "excedente"}, 
            {"data": null,
                 render:function(data,type,row)
                 {
                    return '<img src="'+base_url+'app-assets/images/kyorent.png" style="width:100px;" >';
                }
            }, 
            {"data": "id",
                render:function(data,type,row){
                    if (row.prefactura==1) {
                        var colorbtton='green';
                    }else{
                        var colorbtton='blue';
                    }
                        var html='<ul>\
                                <li>\
                                  <a class="btn-floating '+colorbtton+' tooltipped" data-position="top" data-delay="50" data-tooltip="Prefactura" onclick="detalle('+row.id+')">\
                                    <i class="material-icons">assignment</i>\
                                  </a>\
                                </li>\
                              </ul>';
                    return html;
                }
            },
            {"data": "id",
                render:function(data,type,row)
                {
                    if(data==1){
                        var html='<a class="waves-effect green btn" href="'+base_url+'index.php/Rentas/contrato/'+row.id+'"> Completar Venta &nbsp;</a>';    
                    } 
                    else {
                        var html='<a class="waves-effect cyan btn" href="'+base_url+'index.php/Rentas/contrato/'+row.id+'">Generar contrato</a>';    
                    } 

                    return html;
                }
            }
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[25, 50, 100], [25, 50, 100]],
        "dom": 'Bfrtpl',
        // Muestra el botón de Excel para importar la tabla
        "buttons": [
            
        ],
        // Cambiamos lo principal a Español
        "language": {
            "lengthMenu": "Desplegando _MENU_ elementos por página",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
              }
        }
    });
});
function detalle(id){
    window.open(base_url+"RentasCompletadas/view/"+id, "Prefactura", "width=780, height=612");
}