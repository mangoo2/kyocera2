-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 28-07-2023 a las 00:09:14
-- Versión del servidor: 5.7.26
-- Versión de PHP: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `kyocera_pro20`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `unidades`
--

DROP TABLE IF EXISTS `unidades`;
CREATE TABLE IF NOT EXISTS `unidades` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `modelo` varchar(255) NOT NULL,
  `anio` int(11) NOT NULL,
  `marca` varchar(255) NOT NULL,
  `file` varchar(255) NOT NULL,
  `num_serie` varchar(255) NOT NULL,
  `poliza_seguro` varchar(255) NOT NULL,
  `kilometraje` decimal(10,2) NOT NULL,
  `proximo_servicio` date NOT NULL,
  `vigencia_poliza_seguro` date NOT NULL,
  `placas` varchar(50) NOT NULL,
  `tipo_unidad` int(11) NOT NULL,
  `tipo_unidad_texto` varchar(255) NOT NULL,
  `vigencia_contrato` date NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1',
  `reg` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `idpersonal` int(11) NOT NULL,
  `file_poliza` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `unidades`
--

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
ALTER TABLE `unidades` ADD `tecnico_asignado` INT NULL DEFAULT '0' AFTER `file_poliza`;
