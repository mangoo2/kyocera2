<style type="text/css">
  .titleac{
    font-weight: bold;
  }
  .resultadoac{
    color: red;
  }
  .btn-margin-topz{
    margin-top: 7px;
  }
  .dropdown6c li a, .dropdown5c li a{
    font-size: 14px;
    padding: 11px 10px;
  }
  #modalservicios{
    margin-right: 0px !important;
  }
</style>
<!-- START CONTENT -->
    <section id="content">
      <!--start container-->
        <div class="container">
          <div class="section">
            <div class="row">
              <h4 class="header">Atención a Clientes</h4>
            </div>
            <div class="row">
              <div class="col s12">
                <select id="cliente" class="browser-default">
                  
                </select>
              </div>
            </div>
            <div class="row"><br></div>
            <div class="row datainfo"></div>
            <div class="row"><br></div>
            <div class="row datainfolist_cotizaciones" style="display: none;">
              
            </div>
            <div class="row datainfolist_ventas" style="display: none;">
              <div class="row">
                <div class="col s12">
                      <div class="col s2">
                        <label>Tipo de Venta</label>
                        <select id="tipoventa" class="browser-default form-control-bmz" onchange="load();">
                          <option value="1">Ventas</option>
                          <option value="2">Ventas Combinadas</option>
                        </select>
                      </div>
                      <div class="col s2" style="display: none;">
                        <input type="hidden" name="cliente" id="cliente">
                      </div>
                      <div class="col s2">
                        <label>Tipo de estatus</label>
                        <select id="tipoestatus" class="browser-default form-control-bmz" onchange="load();">
                          <option value="0">Selecione</option>
                          <option value="1">Pagadas</option>
                          <option value="2">Pendientes</option>
                        </select>
                      </div>
                      </div>
                </div>
                <div class="row">
                  <div class="col s12">

                  <table id="tabla_ventas_incompletas" class="responsive-table display" cellspacing="0">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>Cliente</th>
                            <th>Correo</th>
                            <th>Teléfono</th>
                            <th>PRE FACTURA</th>
                            <th>Vendedor</th>
                            <th>Estatus</th>
                            <th>Fecha de vencimiento</th>
                            <th></th>
                            <th>Acciones</th>
                          </tr>
                        </thead>
                        <tbody>
                        </tbody>
                      </table>
                  </div>
                </div>
            </div>
          </div>  
        </div>

        
     </div>
     
    </section>

<div id="modalservicios" class="modal modal-fixed-footer" style="margin-right: 0px;">
  <div class="modal-content " style="padding: 5px;">
    <h5>Servicios</h5>
    <div class="row" style="margin-bottom: 0px;">
      <div class="col s12 m12 12">
        <div class="row addlistservicios" style="margin-bottom: 5px;">
          
        </div>      
      </div>
    </div>                
  </div>                   
  <div class="modal-footer">
    <a  class="modal-action modal-close waves-effect waves-red gray btn-flat">Cerrar</a>
  </div>
</div>




