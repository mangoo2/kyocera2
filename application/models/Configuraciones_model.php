<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class Configuraciones_model extends CI_Model 
{
    public function __construct() 
    {
        parent::__construct();
        $this->DB3 = $this->load->database('other3_db', TRUE);
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
    }
    public function insertarCategoriaEquipos($dato){
        $this->db->insert('categoria_equipos',$dato);   
        return $this->db->insert_id();
    } 
    public function insertarCategoriaRefacciones($dato){
        $this->db->insert('categoria_refacciones',$dato);   
        return $this->db->insert_id();
    } 
    public function insertarCatalogoAccesorios($dato){
        $this->db->insert('catalogo_accesorios',$dato);   
        return $this->db->insert_id();
    }
    //Configuraciones familia//
    public function insertarFamilia($dato){
        $this->db->insert('familia',$dato);   
        return $this->db->insert_id();
    }  
    // fin Configuraciones familia//
    function getDataEquipo(){
        $sql = "SELECT * FROM categoria_equipos WHERE status=1";
        $query = $this->db->query($sql);
        return $query->result();
    } 
    function getDataRefacciones(){
        $sql = "SELECT * FROM categoria_refacciones WHERE status=1";
        $query = $this->db->query($sql);
        return $query->result();
    }
    function getDataRutas(){
        $sql = "SELECT * FROM rutas WHERE status=1";
        $query = $this->DB3->query($sql);
        return $query->result();
    }
    function getDataServ(){
        $sql = "SELECT * FROM servicio_evento WHERE status=1";
        $query = $this->DB3->query($sql);
        return $query->result();
    }
    function getDataAccesorios(){
        $sql = "SELECT * FROM catalogo_accesorios WHERE status=1";
        $query = $this->db->query($sql);
        return $query->result();
    }
    //Configuraciones familia//
    function getDataFamilia(){
        $sql = "SELECT * FROM familia WHERE status=1";
        $query = $this->db->query($sql);
        return $query->result();
    }
    // fin Configuraciones familia//
    public function eliminar_equipo($id) {
        $result="UPDATE categoria_equipos SET status=0 WHERE id=$id";
        $datos = $this->db->query($result);
        return $datos;
    }
    public function eliminar_refaccion($id) {
        $result="UPDATE categoria_refacciones SET status=0 WHERE id=$id";
        $datos = $this->db->query($result);
        return $datos;
    }
    public function eliminar_accesorio($id) {
        $result="UPDATE catalogo_accesorios SET status=0 WHERE id=$id";
        $datos = $this->db->query($result);
        return $datos;
    }
    //Configuraciones familia//
    public function editar_familia($id,$nombre) {
        $result="UPDATE familia SET nombre='$nombre' WHERE id=$id";
        $datos = $this->db->query($result);
        return $datos;
    }
    // fin Configuraciones familia//
    public function verificaraccesorio($data){
        $sql = "SELECT * FROM catalogo_accesorios WHERE status=1 AND no_parte='$data'";
        $query = $this->DB3->query($sql);
        return $query->result();
    }
    public function searchRutas(){
        $sql = "SELECT * FROM rutas WHERE status=1";
        $query = $this->db->query($sql);
        return $query;
    }
    public function searchTecnico(){
        $sql = "SELECT usuarios.*, concat(p.nombre,' ',p.apellido_paterno,' ',p.apellido_materno) as tecnico,p.personalId 
                FROM usuarios 
                join personal as p on p.personalId=usuarios.personalId 
                WHERE (usuarios.perfilId=5 or usuarios.perfilId=12 or p.personalId=29 or p.personalId=19 or p.personalId=39 or p.personalId=77)  and p.estatus=1 group by p.personalId  ORDER BY `tecnico` ASC";
        $query = $this->DB3->query($sql);
        return $query;
    }
    public function getAsignaServ($params){ //falta modificar de donde vengan las asignaciones de servicio
        $columns = array( 
            0 => 'gastos.id',
            1 => 'gastos.descrip',
            2 => 'gastos.monto_gasto',
            3 => 'gastos.fecha_gasto'
        );
        $select="gastos.*";
        $this->db->select($select);
        $this->db->from('gastos');
        $this->db->join('usuarios','usuarios.UsuarioID=id_usuario');
        $this->db->join('personal','personal.personalId=usuarios.UsuarioID','left');  
        $this->db->where('personal.bodega',$bodega);
        $this->db->where('gastos.status',1);

        //si hay busqueda con el campo de busqueda
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach ($columns as $c) {
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();
        }
        
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        
        $query=$this->db->get();
        return $query;
    }
    public function TotalAsignaServ($params){ //falta modificar de donde vengan las asignaciones de servicio
        $this->db->select('count(1)');
        $this->db->from('gastos');
        $this->db->join('usuarios','usuarios.UsuarioID=id_usuario');
        $this->db->join('personal','personal.personalId=usuarios.UsuarioID','left');  
        $this->db->where('personal.bodega',$bodega);
        $this->db->where('gastos.status',1);
        $columns = array( 
            0 => 'gastos.id',
            1 => 'gastos.descrip',
            2 => 'gastos.monto_gasto',
            3 => 'gastos.fecha_gasto'
        );
        //si hay busqueda con el campo de busqueda
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach ($columns as $c) {
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();
        }
        return $this->db->count_all_results();
    }
    function serviciosdeventas(){
        $total1=0;
        $total2=0;
        $total3=0;
        $sql1 = "SELECT count(*) as total
                from ventas as v 
                left join asignacion_ser_venta_a as asv on asv.ventaId=v.id and asv.tipov=0
                where v.combinada=0 and v.servicio=1 and v.serviciocount=1 and v.activo=1 and v.prefactura=1 and asv.asignacionId IS NULL";
        $query1 = $this->DB3->query($sql1);
        foreach ($query1->result() as $item) {
            $total1=$item->total;
        }

        $sql2 = "SELECT count(*) as total
                from ventacombinada as v 
                left join asignacion_ser_venta_a as asv on asv.ventaId=v.combinadaId and asv.tipov=1
                where  v.servicio=1 and v.activo=1 and v.serviciocount=1 and v.prefactura=1 and asv.asignacionId IS NULL";
        $query2 = $this->DB3->query($sql2);
        foreach ($query2->result() as $item) {
            $total2=$item->total;
        }

        $sql3 = "SELECT count(*) as total
                from polizasCreadas as v 
                left join asignacion_ser_poliza_a as asp on asp.polizaId=v.id
                where v.activo=1 and v.serviciocount=1 and v.prefactura=1 and asp.asignacionId IS NULL";
        $query3 = $this->DB3->query($sql3);
        foreach ($query3->result() as $item) {
            $total3=$item->total;
        }
        $totalgeneral=$total1+$total2+$total3;
        //log_message('error', 'total1: '.$total1.' total2: '.$total2.'total3: '.$total3);
        return $totalgeneral;
    }
    function serviciosdeventasvvv(){
        $total1=0;
        $total2=0;
        $total3=0;
        $sql1 = "SELECT count(*) as total
                from ventas as v 
                    left join asignacion_ser_venta_a as asv on asv.ventaId=v.id and asv.tipov=0
                    inner join clientes as cli on cli.id=v.idCliente
                    where v.combinada=0 and v.servicio=1 and v.serviciocount=1 and v.activo=1 and v.prefactura=1 and asv.asignacionId IS NULL";
        $query1 = $this->DB3->query($sql1);
        foreach ($query1->result() as $item) {
            $total1=$item->total;
        }

        $totalgeneral=$total1+$total2+$total3;
        //log_message('error', 'total1: '.$total1.' total2: '.$total2.'total3: '.$total3);
        return $totalgeneral;
    }
    function serviciosdeventaspolizasvvv(){
        $total1=0;
        $sql1 = "SELECT 
                    count(*) as total
                    from polizasCreadas as v 
                    left join asignacion_ser_poliza_a as asp on asp.polizaId=v.id 
                    inner join clientes as cli on cli.id=v.idCliente 
                    where v.activo=1 and v.serviciocount=1 and v.prefactura=1 and  asp.asignacionId IS NULL";
        $query1 = $this->DB3->query($sql1);
        foreach ($query1->result() as $item) {
            $total1=$item->total;
        }

        return $total1;
    }
    function serviciosdeventas_info($fecha_ini,$fecha_fin,$personal){
        $where_per='';
        $where_perc='';

        $fecha_actual = date("Y-m-d");
        if($fecha_ini==''){
            $fecha_ini = date("Y-m-d",strtotime($fecha_actual."- 4 days"));
        }
        if($fecha_fin==''){
            $fecha_fin = date("Y-m-d",strtotime($fecha_actual."+ 5 days"));
        }
        if($personal>0){
            $where_per=" and v.id_personal='$personal' ";
            $where_perc=" and vc.id_personal='$personal' ";
        }
        
        $sql2 = "
                SELECT * from (
                    SELECT v.id as idventa,
                    v.id as idventac,
                    0 as tipo,
                    v.idCliente,
                    cli.empresa,
                    4 as prasignaciontipo,
                    v.reg,
                    per.nombre as vendedor,
                    0 as view,
                    v.fechaentrega,
                    0 as viewcont,
                    0 as viewcont_cant, 
                    0 as alert,
                    '' as alert_date,
                    '' as ser_rest,
                    1 as consulta,
                    'Mensajeria' as servicio,
                    0 as idpolizaser,
                    0 as tipopc
                    from ventas as v 
                    left join asignacion_ser_venta_a as asv on asv.ventaId=v.id and asv.tipov=0
                    inner join clientes as cli on cli.id=v.idCliente
                    inner join personal as per on per.personalId=v.id_personal
                    where v.combinada=0 and v.servicio=1 and v.serviciocount=1 and v.activo=1 and v.prefactura=1 and asv.asignacionId IS NULL $where_per and v.fechaentrega>='$fecha_ini' and v.fechaentrega<='$fecha_fin'

                    union 
                    
                    SELECT v.combinadaId as idventa,
                    v.combinadaId as idventac,
                    1 as tipo,
                    v.idCliente,
                    cli.empresa,
                    4 as prasignaciontipo,
                    v.reg,
                    per.nombre as vendedor,
                    0 as view,
                    v.fechaentrega,
                    0 as viewcont,
                    0 as viewcont_cant,
                    0 as alert,
                    '' as alert_date,
                    '' as ser_rest,
                    2 as consulta,
                    '' as servicio,
                    0 as idpolizaser,
                    0 as tipopc
                    from ventacombinada as v 
                    left join asignacion_ser_venta_a as asv on asv.ventaId=v.combinadaId and asv.tipov=1
                    inner JOIN clientes as cli on cli.id=v.idCliente
                    inner join personal as per on per.personalId=v.id_personal
                    where  v.servicio=1 and v.activo=1 and v.serviciocount=1 and v.prefactura=1 and asv.asignacionId IS NULL $where_per

                    union

                    SELECT 
                    v.id as idventa, 
                    v.id as idventac, 
                    0 as tipo, 
                    v.idCliente, 
                    cli.empresa, 
                    2 as prasignaciontipo,
                    v.reg,
                    per.nombre as vendedor,
                    0 as view,
                    v.fechaentrega,
                    v.viewcont,
                    0 as viewcont_cant,
                    0 as alert,
                    '' as alert_date,
                    v.ser_rest,
                    3 as consulta,
                    poldll.nombre as servicio,
                    0 as idpolizaser,
                    0 as tipopc
                    from polizasCreadas as v 
                    left join asignacion_ser_poliza_a as asp on asp.polizaId=v.id and asp.activo=1
                    LEFT JOIN asignacion_ser_poliza_a_e as aspd on aspd.asignacionId=asp.asignacionId AND aspd.activo=1
                    inner join clientes as cli on cli.id=v.idCliente
                    inner join personal as per on per.personalId=v.id_personal 
                    inner join polizasCreadas_has_detallesPoliza as poldll on poldll.idPoliza=v.id
                    where v.viewcont=0 and v.combinada=0 and v.activo=1 and v.serviciocount=1 and v.prefactura=1 and  aspd.asignacionId IS NULL $where_per and v.fechaentrega>='$fecha_ini' and v.fechaentrega<='$fecha_fin'
                    group by v.id
                    union

                    SELECT 
                    v.id as idventa, 
                    v.id as idventac, 
                    0 as tipo, 
                    v.idCliente, 
                    cli.empresa, 
                    2 as prasignaciontipo,
                    v.reg,
                    per.nombre as vendedor,
                    0 as view,
                    v.fechaentrega,
                    v.viewcont,
                    v.viewcont_cant,
                    1 as alert,
                    pol_ser.alert_date,
                    v.ser_rest,
                    4 as consulta,
                    poldll.nombre as servicio,
                    pol_ser.idpolizaser,
                    pol_ser.tipo_pre_corr as tipopc
                    from polizasCreadas as v 
                    /*left join asignacion_ser_poliza_a as asp on asp.polizaId=v.id */
                    inner join clientes as cli on cli.id=v.idCliente
                    inner join personal as per on per.personalId=v.id_personal 
                    inner join polizasCreadas_has_detallesPoliza as poldll on poldll.idPoliza=v.id
                    inner join polizascreadas_sercontr as pol_ser on pol_ser.id=v.id and pol_ser.activo=1
                    where v.viewcont=1 and v.combinada=0 and v.activo=1 and v.serviciocount=1 and v.prefactura=1  and pol_ser.alert_date>= '$fecha_ini' /*and  asp.asignacionId IS NULL*/  $where_per 
                    group by v.id,pol_ser.idpolizaser
                    union

                    SELECT 
                    v.id as idventa, 
                    IF(vc.equipo>0,vc.equipo,IF(vc.consumibles>0,vc.consumibles>0,IF(vc.refacciones>0,vc.refacciones,0))) as idventac,
                    0 as tipo, 
                    v.idCliente, 
                    cli.empresa, 
                    2 as prasignaciontipo,
                    v.reg,
                    per.nombre as vendedor,
                    1 as view,
                    vc.fechaentrega,
                    v.viewcont,
                    v.viewcont_cant,
                    v.alert,
                    v.alert_date,
                    v.ser_rest,
                    5 as consulta,
                    poldll.nombre as servicio,
                    0 as idpolizaser,
                    0 as tipopc
                    from polizasCreadas as v 
                    left join asignacion_ser_poliza_a as asp on asp.polizaId=v.id and asp.activo=1
                    LEFT JOIN asignacion_ser_poliza_a_e as aspd on aspd.asignacionId=asp.asignacionId AND aspd.activo=1
                    inner join clientes as cli on cli.id=v.idCliente
                    inner join personal as per on per.personalId=v.id_personal 
                    inner join ventacombinada as vc on vc.poliza=v.id
                    inner join polizasCreadas_has_detallesPoliza as poldll on poldll.idPoliza=v.id
                    where v.viewcont=0 and v.combinada=1 and v.activo=1 and v.serviciocount=1 and v.prefactura=1 and  aspd.asignacionId IS NULL $where_perc and vc.fechaentrega>='$fecha_ini' and vc.fechaentrega<='$fecha_fin'
                    group by v.id
                ) as datos  ORDER BY datos.fechaentrega DESC
                ";
        $query2 = $this->db->query($sql2);
        return $query2;
    }
    function serviciossintenicos(){
        $sql1 = "SELECT count(*) as total
                from asignacion_ser_contrato_a_e 
                where status=0 and tecnico=0
                ";
        $query1 = $this->DB3->query($sql1);
        $total=0;
        foreach ($query1->result() as $item) {
            $total=$item->total;
        }

        return $total;
    }
    //=================== session ======================================
    function view_session_searchTecnico(){
        if(isset($_SESSION['session_stecnicos'])){
                $session_stecnicos=$_SESSION['session_stecnicos'];
        }else{
            //unset($_SESSION['session_stecnicos']);
            $session_stecnicos=$this->searchTecnico();
            $_SESSION['session_stecnicos']=$session_stecnicos->result();
        }
        return $session_stecnicos;
    }
    function porpagartec(){
        $sql2 = "SELECT 
                    0 as tipo,
                    v.id,
                    0 as equipo,0 as consumibles,0 as refacciones,0 as poliza,
                    per.nombre,
                    per.apellido_paterno,
                    per.apellido_materno,
                    v.reg,
                    cli.empresa,
                    v.idCliente,
                    1 as tv_fil

                    FROM ventas as v 
                    INNER JOIN prefactura as pre on pre.ventaId=v.id and pre.tipo=0 
                    INNER JOIN asignacion_ser_venta_a as asig on asig.ventaId=v.id AND asig.tipov=0 
                    INNER JOIN personal as per on per.personalId=asig.tecnico 
                    INNER JOIN clientes as cli on cli.id=v.idCliente
                    LEFT JOIN pagos_ventas as pv on pv.idventa=v.id
                    WHERE v.combinada=0 AND pre.formadecobro='Tecnico' AND asig.status=2 AND v.estatus!=3 AND pv.idpago is null
                    group by v.id
                union
                SELECT 
                    1 as tipo,
                    v.combinadaId as id,
                    v.equipo,
                    v.consumibles,
                    v.refacciones,
                    v.poliza, 
                    per.nombre,
                    per.apellido_paterno,
                    per.apellido_materno,
                    v.reg,
                    cli.empresa,
                    v.idCliente,
                    2 as tv_fil
                    FROM ventacombinada as v 
                    INNER JOIN prefactura as pre on pre.ventaId=v.combinadaId and pre.tipo=3 
                    INNER JOIN asignacion_ser_venta_a as asig on asig.ventaId=v.combinadaId AND asig.tipov=1 
                    INNER JOIN personal as per on per.personalId=asig.tecnico 
                    INNER JOIN clientes as cli on cli.id=v.idCliente
                    LEFT JOIN pagos_combinada as pv on pv.idventa=v.combinadaId
                WHERE pre.formadecobro='Tecnico' AND asig.status=2 AND v.estatus!=3 AND pv.idpago is null
                group by v.combinadaId
                union
                SELECT 
                    3 as tipo,
                    v.id,
                    0 as equipo,0 as consumibles,0 as refacciones,0 as poliza,
                    per.nombre,
                    per.apellido_paterno,
                    per.apellido_materno,
                    v.reg,
                    cli.empresa,
                    v.idCliente,
                    4 as tv_fil
                    FROM polizasCreadas as v 
                    INNER JOIN prefactura as pre on pre.ventaId=v.id and pre.tipo=1 
                    INNER JOIN asignacion_ser_poliza_a as asig on asig.polizaId=v.id 
                    INNER JOIN asignacion_ser_poliza_a_e as asigd on asigd.asignacionId=asig.asignacionId 
                    INNER JOIN personal as per on per.personalId=asigd.tecnico 
                    INNER JOIN clientes as cli on cli.id=v.idCliente
                    LEFT JOIN pagos_poliza as pv on pv.idpoliza=v.id
                    WHERE pre.formadecobro='Tecnico' AND asigd.status=2 AND v.estatus!=3 AND pv.idpago is null
                    group by v.id
                ORDER BY `reg` DESC
                ";
        $query2 = $this->DB3->query($sql2);
        return $query2;
    }
    function refaccionescontrato($contrato,$fechainicio,$fechafin){
        if($fechainicio!=''){
            $w_fi=" and ven.reg>='$fechainicio' ";
        }else{
            $w_fi="";
        }
        if($fechafin!=''){
            $w_ff=" and ven.reg<='$fechafin' ";
        }else{
            $w_ff="";
        }
        $sql2="SELECT asig.contratoId,asig.asignacionId,asige.asignacionIde,ven.id,vr.piezas,vr.modelo,sr.serie,asv.asignacionId as asignacionId_v,asv.fecha,eq.modelo as modeloe, vr.serie as seriee
               FROM `asignacion_ser_contrato_a` as asig 
               INNER JOIN asignacion_ser_contrato_a_e as asige on asig.asignacionId=asige.asignacionId 
               INNER JOIN ventas as ven on ven.idCotizacion=asige.cot_referencia 
               INNER JOIN ventas_has_detallesRefacciones as vr on vr.idVentas=ven.id 
               INNER JOIN asignacion_series_refacciones as asigs on asigs.id_refaccion=vr.id 
               INNER JOIN series_refacciones as sr on sr.serieId=asigs.serieId 
               INNER JOIN equipos as eq on eq.id=vr.idEquipo 
               LEFT JOIN asignacion_ser_venta_a as asv on asv.ventaId=ven.id 
               WHERE asig.contratoId=$contrato  $w_fi $w_ff";
        $query2 = $this->db->query($sql2);
        return $query2;
    }
    function obtenerpagoscontrastoscomplementos(){
        $sql2="SELECT 
                    pre.facId,
                    pre.contratoId,
                    pre.prefacturaId,
                    com.fechatimbre,
                    comd.ImpPagado,
                    fac.total 
                FROM factura_prefactura as pre 
                INNER JOIN f_complementopago_documento as comd on comd.facturasId=pre.facturaId 
                INNER JOIN f_complementopago as com on com.complementoId=comd.complementoId 
                INNER JOIN f_facturas as fac on fac.FacturasId=pre.facturaId 
                WHERE pre.statuspago=0 AND com.Estado=1 ORDER BY `com`.`fechatimbre` DESC;";
        $query2 = $this->db->query($sql2);
        return $query2;
    }
    function getserviospolizarealizados($idpoliza){
        $sql2="SELECT pd.asignacionId 
                FROM asignacion_ser_poliza_a_e as pdll 
                INNER JOIN asignacion_ser_poliza_a as pd on pd.asignacionId=pdll.asignacionId 
                WHERE pdll.asignacionId='$idpoliza' and pdll.status=2 AND pd.activo=1 
                GROUP BY pd.asignacionId";
        $query2 = $this->db->query($sql2);
        return $query2;
    }
    function refacciones_tecnico(){
        $sql2="SELECT * FROM `refacciones` WHERE status=1 AND rendimiento>0";
        $query2 = $this->db->query($sql2);
        return $query2;
    }
    function notificacionactidadesporacer($personal){
        $sql2="SELECT * FROM actividades as ac 
              inner join actividades_otros_personal as acop on acop.id_asig=ac.id
              WHERE ac.activo=1 AND (ac.asig_personal='$personal' or acop.personal='$personal' )AND ac.status=1 GROUP BY ac.id";
        $query2 = $this->db->query($sql2);
        return $query2;
    }
    function servicios_solicitados_cli_info(){
        $sql2="SELECT ssc.id,ssc.idcliente,cli.empresa, ssc.fecha_sol,ssc.reg,sscd.idequipo,sscd.eq_modelo,sscd.idserie,sscd.serie,sscd.detalle
                FROM solicitud_servicio_cli as ssc 
                INNER JOIN clientes as cli on cli.id=ssc.idcliente
                INNER JOIN solicitud_servicio_cli_dll as sscd on sscd.idsol=ssc.id AND sscd.activo=1
                WHERE ssc.activo=1
                order by  ssc.reg DESC";
        $query2 = $this->db->query($sql2);
        return $query2;
    }
    function notificacion_pagostecnicos(){
        $strq="SELECT 1 as tipovcp,'venta Combinada' as tipovcpname,vc.equipo as idventa,vc.idCliente,cli.empresa,pc.fecha,pc.reg,pc.idpago
                FROM pagos_combinada as pc 
                INNER JOIN ventacombinada as vc on vc.combinadaId=pc.idventa
                INNER JOIN clientes as cli on cli.id=vc.idCliente
                WHERE pagtec=1 AND confirmado=0
                UNION
                SELECT 0 as tipovcp,'Venta' as tipovcpname,pv.idventa,v.idCliente,cli.empresa,pv.fecha,pv.reg,pv.idpago 
                FROM pagos_ventas as pv
                INNER JOIN ventas as v on v.id=pv.idventa
                INNER JOIN clientes as cli on cli.id=v.idCliente
                WHERE pv.pagtec=1 AND pv.confirmado=0
                UNION
                SELECT 2 as tipovcp,'Poliza' as tipovcpname,pp.idpoliza as idventa,pc.idCliente,cli.empresa,pp.fecha,pp.reg,pp.idpago 
                FROM pagos_poliza as pp
                INNER JOIN polizasCreadas as pc on pc.id=pp.idpoliza
                INNER JOIN clientes as cli on cli.id=pc.idCliente
                WHERE pp.confirmado=0 AND pp.pagtec=1  
                ORDER BY `reg`  ASC";
        $query = $this->db->query($strq);
        return $query;
    }
    function registrarmovimientosseries($serieId,$tipo){
        if($tipo==1){
            $strq="UPDATE series_productos SET fechas_mov=concat(IFNULL(fechas_mov,''),'<br>$this->fechahoy') WHERE serieId='$serieId'";
            $query = $this->db->query($strq);
        }
        if($tipo==2){
            $strq="UPDATE series_accesorios SET fechas_mov=concat(IFNULL(fechas_mov,''),'<br>$this->fechahoy') WHERE serieId='$serieId'";
            $query = $this->db->query($strq);
        }
        if($tipo==3){
            $strq="UPDATE series_refacciones SET fechas_mov=concat(IFNULL(fechas_mov,''),'<br>$this->fechahoy') WHERE serieId='$serieId'";
            $query = $this->db->query($strq);
        }
    }
    function actividadesporleer($personal){
        $strq="SELECT * FROM(
                    SELECT actp.id_asig as id_actividad,0 as tipo, actp.personal,per.nombre,per.apellido_paterno, actl.id as id_lectura,actl.reg as reg_lectura,0 as id_ac_dll,act.reg, act.titulo,act.vigencia
                                    FROM actividades_otros_personal as actp
                                    INNER JOIN actividades as act on act.id=actp.id_asig
                                    inner join personal as per on per.personalId=actp.personal
                                    LEFT JOIN actividades_lectura as actl on actl.tipo=0 AND actl.id_act=actp.id_asig AND actl.idpersonal=actp.personal
                                    WHERE act.status>0 AND act.asig_personal!=act.creo_personalId AND act.activo=1 AND per.personalId='$personal' AND actl.id IS null
                    UNION
                    SELECT act.id as id_actividad,1 as tipo,act.asig_personal as personal,per.nombre,per.apellido_paterno,actl.id as id_lectura,actl.reg as reg_lectura,0 as id_ac_dll,act.reg, act.titulo,act.vigencia
                                    FROM actividades as act
                                    inner join personal as per on per.personalId=act.asig_personal
                                    LEFT JOIN actividades_lectura as actl on actl.tipo=0 AND actl.id_act=act.id AND actl.idpersonal=act.asig_personal
                                    WHERE act.status>0 AND act.asig_personal!=act.creo_personalId AND act.activo=1 AND per.personalId='$personal' AND actl.id IS null
                                    UNION
                    SELECT 
                                        dat.id_actividad,2 as tipo,dat.personal,per.nombre,per.apellido_paterno,actl.id as id_lectura,actl.reg as reg_lectura,dat.id_ac_dll,dat.reg, dat.titulo,dat.vigencia
                                    FROM(
                                        SELECT act.asig_personal as personal,act.id as id_actividad,actd.id as id_ac_dll,actd.reg,act.titulo,act.vigencia
                                        FROM actividades as act
                                        INNER JOIN actividades_detalle as actd on actd.id_asig=act.id and actd.creo_personalId!=act.asig_personal
                                        WHERE act.status>0 AND act.activo=1 AND act.asig_personal='$personal'
                                        UNION
                                        SELECT act.creo_personalId as persona,act.id as id_actividad,actd.id as id_ac_dll,actd.reg,act.titulo,act.vigencia
                                        FROM actividades as act
                                        INNER JOIN actividades_detalle as actd on actd.id_asig=act.id and actd.creo_personalId!=act.creo_personalId
                                        WHERE act.status>0 AND act.activo=1 AND act.creo_personalId='$personal'
                                        UNION
                                        SELECT actp.personal,actp.id_asig as id_actividad,actd.id as id_ac_dll,actd.reg,act.titulo,act.vigencia
                                        FROM actividades_otros_personal as actp
                                        INNER JOIN actividades as act on act.id=actp.id_asig
                                        INNER JOIN actividades_detalle as actd on actd.id_asig=actp.id_asig and actd.creo_personalId!=actp.personal
                                        WHERE act.status>0 AND act.activo=1 AND actp.personal='$personal'
                                    ) as dat
                                    INNER JOIN personal as per on per.personalId=dat.personal
                                    LEFT JOIN actividades_lectura as actl on actl.id_act=dat.id_ac_dll and actl.tipo=1 AND actl.idpersonal=dat.personal
                                    WHERE per.personalId>0 AND actl.id IS NULL
                    ) as datos  
                    GROUP BY datos.id_actividad,datos.personal
                    ORDER BY `datos`.`reg`  ASC";

        $query = $this->db->query($strq);
        return $query;
    }
    function ventas_vew_pendientes($act){
        $strq="SELECT v.id,v.id as idventa,1 as tipopre,v.reg,v.idCliente, cli.empresa, DATE_FORMAT(v.reg, '%Y-%m-%d') as fecha,v.activo
            FROM ventas as v
            INNER JOIN clientes as cli on cli.id=v.idCliente
            WHERE v.activo=$act AND v.venta_web=1 AND v.prefactura=0";
        $query = $this->db->query($strq);
        return $query;
    }


}

?>