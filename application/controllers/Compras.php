<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Compras extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        //$this->load->library('excel');
        $this->load->model('Compras_model');
        $this->load->model('ModeloCatalogos');
        $this->load->model('Modeloproprioritarios');
        
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fechahoyc = date('Y-m-d');
        $this->submenu=42;
        if($this->session->userdata('logeado')==true){
            $this->idpersonal=$this->session->userdata('idpersonal');
        }else{
            //redirect('Sistema');
        }
    }

    public function index(){  
        if($this->session->userdata('logeado')==true){
            $this->idpersonal=$this->session->userdata('idpersonal');
        }else{
            redirect('Sistema');
        }
        $data['MenusubId']=$this->submenu; 
        //$data['bodegas']=$this->ModeloCatalogos->getselectwheren('bodegas',array('activo' => 1));
        $data['bodegas']=$this->ModeloCatalogos->view_session_bodegas();
        $this->load->view('header');
        $this->load->view('main',$data);
        $this->load->view('Compras/Compras');
        $this->load->view('footerck');
        $this->load->view('Compras/jscompras');
	}
    function add($idcompra=0){
        if($this->session->userdata('logeado')==true){
            $this->idpersonal=$this->session->userdata('idpersonal');
        }else{
            redirect('Sistema');
        }
        $data['MenusubId']=$this->submenu;
        //$data['bodegas']=$this->ModeloCatalogos->getselectwheren('bodegas',array('activo' => 1));
        $data['bodegas']=$this->ModeloCatalogos->view_session_bodegas();
        $data['consumibles']=$this->ModeloCatalogos->getselectwheren('consumibles',array('status' => 1));
        $data['accesorios']=$this->ModeloCatalogos->getselectwheren('catalogo_accesorios',array('status' => 1));
        $data['equipos']=$this->ModeloCatalogos->getselectwheren('equipos',array('estatus' => 1));
        $data['refacciones']=$this->ModeloCatalogos->getselectwheren('refacciones',array('status' => 1));
        $data['idcompra']=$idcompra;
        $this->load->view('header');
        $this->load->view('main',$data);
        $this->load->view('series/addc');
        $this->load->view('footer');
        $this->load->view('series/addcjs');
    }
    public function getDatacompras() {
        $params = $this->input->post();
        $serie = $this->Compras_model->Getseries_compras($params);
        $totalRecords= $this->Compras_model->TotalSeriecompras($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totalRecords),  
            "recordsFiltered" => intval($totalRecords),
            "data"            => $serie->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    public function seriecompletadas(){
        $id = $this->input->post('id');
        $totalGeneral = 0;
        $compraId = array('compraId' => $id,'status'=>0);
        $totalequipo = $this->Compras_model->getselectwherenall('series_compra_productos',$compraId); 
        foreach ($totalequipo->result() as $item) {
            $totalGeneral = $totalGeneral+1;
        }
        $comprawhere = array('compraId' => $id,'status'=>0,'con_serie'=>1);
        $totalaccesorios = $this->Compras_model->getselectwherenall('series_compra_accesorios',$comprawhere); 
        foreach ($totalaccesorios->result() as $item) {
            $totalGeneral = $totalGeneral+1;
        }
        $comprawhere = array('compraId' => $id,'status'=>0,'con_serie'=>0);
        $totalaccesorios = $this->Compras_model->getselectwherenall('series_compra_accesorios',$comprawhere); 
        foreach ($totalaccesorios->result() as $item) {
            $totalGeneral = $totalGeneral+($item->cantidad-$item->entraron);
        }
        $comprarcs = array('compraId' => $id,'status'=>0,'con_serie'=>1);
        $totalrefacciones = $this->Compras_model->getselectwherenall('series_compra_refacciones',$comprarcs); 
        foreach ($totalrefacciones->result() as $item) {
            $totalGeneral = $totalGeneral+1;
        }
        $comprarcs = array('compraId' => $id,'status'=>0,'con_serie'=>0);
        $totalrefacciones = $this->Compras_model->getselectwherenall('series_compra_refacciones',$comprarcs); 
        foreach ($totalrefacciones->result() as $item) {
            $totalGeneral = $totalGeneral+($item->cantidad-$item->entraron);
        }
        $compraIdc = array('compraId' => $id);
        $totalconsumibles = $this->Compras_model->getselectwherenall('compra_consumibles',$compraIdc); 
        foreach ($totalconsumibles->result() as $item) {
            $totalGeneral = $totalGeneral+($item->cantidad-$item->recibidos);
        }
        if($totalGeneral<=0){
            $this->ModeloCatalogos->updateCatalogo('series_compra',array('completada'=>1),array('compraId'=>$id));
        }
        //var_dump($totalGeneral);       
 
     echo $totalGeneral;
    }
    public function seriecompletadas2($id){
        $totalGeneral = 0;
        $compraId = array('compraId' => $id,'status'=>0);
        $totalequipo = $this->Compras_model->getselectwherenall('series_compra_productos',$compraId); 
        foreach ($totalequipo->result() as $item) {
            $totalGeneral = $totalGeneral+1;
        }
        $comprawhere = array('compraId' => $id,'status'=>0,'con_serie'=>1);
        $totalaccesorios = $this->Compras_model->getselectwherenall('series_compra_accesorios',$comprawhere); 
        foreach ($totalaccesorios->result() as $item) {
            $totalGeneral = $totalGeneral+1;
        }
        $comprawhere = array('compraId' => $id,'status'=>0,'con_serie'=>0);
        $totalaccesorios = $this->Compras_model->getselectwherenall('series_compra_accesorios',$comprawhere); 
        foreach ($totalaccesorios->result() as $item) {
            $totalGeneral = $totalGeneral+($item->cantidad-$item->entraron);
        }
        $comprarcs = array('compraId' => $id,'status'=>0,'con_serie'=>1);
        $totalrefacciones = $this->Compras_model->getselectwherenall('series_compra_refacciones',$comprarcs); 
        foreach ($totalrefacciones->result() as $item) {
            $totalGeneral = $totalGeneral+1;
        }
        $comprarcs = array('compraId' => $id,'status'=>0,'con_serie'=>0);
        $totalrefacciones = $this->Compras_model->getselectwherenall('series_compra_refacciones',$comprarcs); 
        foreach ($totalrefacciones->result() as $item) {
            $totalGeneral = $totalGeneral+($item->cantidad-$item->entraron);
        }
        $compraIdc = array('compraId' => $id);
        $totalconsumibles = $this->Compras_model->getselectwherenall('compra_consumibles',$compraIdc); 
        foreach ($totalconsumibles->result() as $item) {
            $totalGeneral = $totalGeneral+($item->cantidad-$item->recibidos);
        }
        if($totalGeneral<=0){
            $this->ModeloCatalogos->updateCatalogo('series_compra',array('completada'=>1),array('compraId'=>$id));
        }
        //var_dump($totalGeneral);       
 
        return $totalGeneral;
    }
    function verificarmontocompras(){
        $params = $this->input->post();
        $compras = $params['compras'];
        $comprasrow = json_decode($compras);
        $comprasrowi=[];
        for ($i=0;$i<count($comprasrow);$i++) { 
            $idcompra=$comprasrow[$i]->idcompra;
            $monto=$this->seriecompletadas2($idcompra);

            $comprasrowi[]=array('idcompra'=>$idcompra,'monto'=>$monto);
        };
        echo json_encode($comprasrowi);
    }
    public function modal_series_m(){
        $id = $this->input->post('id');
        $idcompra=$id;
        $compraId = array('compraId' => $id,'activo'=>1);
        $html = '';
        $activo = array('activo'=>1);
        $bodegas = $this->Compras_model->getselectwherenall('bodegas',$activo); 
        //====================================== PRODUCTOS ==================================================
            //$totalequipo = $this->Compras_model->getselectwherenall('series_compra_productos',$compraId); // editar esta consulta para colocar el modelo de la serie
            $totalequipo = $this->Compras_model->getseries_compra_productos($id);
            if($totalequipo->num_rows()>0){
                $html.='<div class="row">
                            
                            <div class="col s12 m12 12">
                                <table id="tabla_produto" class="striped">
                                    <thead>
                                        <tr>
                                            <th style="width: 20%"></th>
                                            <th style="width: 20%">Serie Equipo</th>
                                            <th style="width: 30%">Bodega</th>
                                            <th style="width: 10%">Factura</th>
                                            <th style="width: 10%">Fecha</th>
                                            <th style="width: 10%"></th>
                                        </tr>
                                    </thead>
                                    <tbody>';
            }
            foreach ($totalequipo->result() as $item) {
                if($item->status){
                    $disabled ='disabled';
                    $readonly='readonly';
                    $deletetipo=1;
                }else{
                    $disabled ='';
                    $readonly='';
                    $deletetipo=0;
                    $item->bodega=8;
                }
                $html.='<tr class="cserienew">
                            <td >
                                <input type="checkbox" id="serieIdp_'.$item->serieId.'" class="checkproducto" value="'.$item->serieId.'" '.$disabled.' />
                                <label for="serieIdp_'.$item->serieId.'">'.$item->modelo.'</label> 
                            </td>
                            <td >
                                <input type="text" id="serienew" 
                                class="form-control cserienewi cserienewi_1_'.$item->serieId.'" 
                                value="'.$item->serie.'" '.$readonly.' 
                                onchange="verificarserie('.$item->serieId.',1)"
                                ondblclick="editarserie('.$id.','.$item->serieId.',1)" />
                            </td>
                            <td>
                                <select id="idbodega" name="idbodega" class="browser-default chosen" '.$disabled.'>';
                                $html.='<option></option>';
                                foreach ($bodegas->result() as $itemb) {
                                    if ($itemb->bodegaId==$item->bodega) {
                                        $selected='selected';
                                    }else{
                                        $selected='';
                                    }
                                $html.='<option value="'.$itemb->bodegaId.'" '.$selected.'>'.$itemb->bodega.'</option>';
                                 } 
                                $html.='</select>';
                    $html.='</td>
                            <td>
                                <input type="text" id="factura" class="form-control cserienewi" value="'.$item->factura.'" '.$disabled.' />
                            </td>
                            <td>
                                <input type="date" id="factura_fecha" class="form-control cserienewi" value="'.$item->factura_fecha.'" '.$disabled.' />
                            </td>
                            <td>
                                <a onclick="deletecompra('.$item->serieId.','.$deletetipo.',1,'.$item->compraId.')" class="btn-bmz red gradient-shadow" >
                                <i class="fas fa-trash-alt"></i></a>
                            </td>
                        </tr>';
            }
            if($totalequipo->num_rows()>0){
                        $html.='</tbody>
                            </table>
                        </div>
                    </div>';
            }
        //====================================== PRODUCTOS ==================================================
        //====================================== ACCESORIOS ==================================================
            //$totalaccesorios = $this->Compras_model->getselectwherenall('series_compra_accesorios',$compraId); 
            $totalaccesorios = $this->Compras_model->getseries_compra_accesorios($id); 
            if ($totalaccesorios->num_rows()>0) {
                $html.='<div class="row">
                            <div class="col s12 m12 12 ">
                                <table id="tabla_accesorio" class="striped">
                                    <thead>
                                        <tr>
                                            <th style="width: 20%"></th>
                                            <th style="width: 20%">Serie Accesorio</th>
                                            <th style="width: 30%">Bodega</th>
                                            <th style="width: 10%">Factura</th>
                                            <th style="width: 10%">Fecha</th>
                                            <th style="width: 10%"></th>
                                        </tr>
                                    </thead>
                                    <tbody>';
            }
            foreach ($totalaccesorios->result() as $item) {
               if($item->status>0){
                    $disabled ='disabled';
                    $readonly='readonly';
                    $deletetipo=1;
                }else{
                    $disabled ='';
                    $readonly='';
                    $deletetipo=0;
                    $item->bodega=8;
                }
               $html.='<tr class="cserienew">
                            <td >
                                <input type="checkbox" id="serieIda_'.$item->serieId.'" class="checkproducto" value="'.$item->serieId.'" '.$disabled.' />
                                <label for="serieIda_'.$item->serieId.'">'.$item->nombre.'</label> 
                            </td>
                            <td >
                                <input type="text" id="serienew" 
                                        class="form-control cserienewi cserienewi_2_'.$item->serieId.'" 
                                        value="'.$item->serie.'" '.$readonly.' 
                                        onchange="verificarserie('.$item->serieId.',2)"
                                        ondblclick="editarserie('.$id.','.$item->serieId.',2)"/>
                            </td>
                            <td>
                                <select id="idbodega" name="idbodega" class="browser-default chosen" '.$disabled.' >';
                                $html.='<option></option>';
                                foreach ($bodegas->result() as $itemb) {
                                    if ($itemb->bodegaId==$item->bodega) {
                                        $selected='selected';
                                    }else{
                                        $selected='';
                                    }
                                $html.='<option value="'.$itemb->bodegaId.'" '.$selected.'>'.$itemb->bodega.'</option>';
                                 } 
                                $html.='</select>';
                                $html.='</td>
                                <td>
                                    <input type="text" id="factura" class="form-control cserienewi" value="'.$item->factura.'" '.$disabled.' />
                                </td>
                                <td>
                                    <input type="date" id="factura_fecha" class="form-control cserienewi" value="'.$item->factura_fecha.'" '.$disabled.' />
                                </td>
                                <td>
                                    <a onclick="deletecompra('.$item->serieId.','.$deletetipo.',2,'.$item->compraId.')" class="btn-bmz red gradient-shadow">
                                    <i class="fas fa-trash-alt"></i></a>
                                </td>
                        </tr>';               
            }
            if ($totalaccesorios->num_rows()>0) {
                           $html.='</tbody>
                            </table>
                        </div>
                    </div>'; 
            }
            $totalaccesoriosss = $this->Compras_model->getseries_compra_accesoriosss($id); 
            foreach ($totalaccesoriosss->result() as $itemc) {
                $html.='<div class="row">
                            <div class="col s12 m12 12 tablacompras">
                                <table id="tabla_accesoriosss" class="striped">
                                    <thead>
                                        <tr>
                                            <th style="width: 30%">Accesorio</th>
                                            <th style="width: 20%">Cantidad</th>
                                            <th style="width: 20%">Entraron</th>
                                            <th style="width: 20%"></th>
                                            <th ></th>
                                        </tr>
                                        <tr>
                                            <th><input type="text" id="refaccion_con_'.$itemc->serieId.'" 
                                                       class="form-control cserienewi" 
                                                       value="'.$itemc->nombre.'" 
                                                       readonly/>
                                                <input type="hidden" id="refaid_con_'.$itemc->serieId.'" 
                                                       value="'.$itemc->accesoriosid.'" 
                                                       readonly/>
                                            </th>
                                            <th><input type="text" id="refa_cant_con_'.$itemc->serieId.'" 
                                                       class="form-control cserienewi refa_cantidad_'.$itemc->serieId.'" 
                                                       value="'.$itemc->cantidad.'" 
                                                       readonly/>
                                            </th>
                                            <th><input type="text" id="refa_ent_con_'.$itemc->serieId.'" 
                                                       class="form-control cserienewi refa_reci'.$itemc->serieId.'" 
                                                       value="'.$itemc->entraron.'" 
                                                       readonly/>
                                            </th>
                                            <th><button class="btn-bmz waves-effect waves-light" onclick="addaccesorio('.$itemc->serieId.')">Agregar <i class="material-icons right">system_update_alt</i></button></th>
                                            <td>';
                                            if($itemc->entraron>0){
                                                $html.='<a onclick="edit_cant_item('.$itemc->serieId.',1,'.$idcompra.','.$itemc->cantidad.','.$itemc->entraron.')" class="btn-bmz blue"><i class="fas fa-pencil-alt"></i></a>';
                                            }else{
                                                $html.='<a onclick="delete_item('.$itemc->serieId.',1,'.$idcompra.')" class="btn-bmz red"><i class="fas fa-trash-alt"></i></a>';
                                            }
                                    $html.='</td>
                                        </tr>
                                    </thead>
                                    <tbody class="accesorios_trss_'.$itemc->serieId.' accesorioss_add">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        ';
            }
        //====================================== ACCESORIOS ==================================================
        //====================================== REFACCIONES ==================================================
            //$compraId = array('compraId' => $id,'activo'=>1,'con_serie'=>1);
            //$totalrefacciones = $this->Compras_model->getselectwherenall('series_compra_refacciones',$compraId); 
            $totalrefacciones = $this->Compras_model->getseries_compra_refacciones($id); 
            if($totalrefacciones->num_rows()>0){
                $html.='<div class="row">
                            <div class="col s12 m12 12 tablacompras" style="padding:0px;">
                                <table id="tabla_refaccion" class="striped">
                                    <thead>
                                        <tr>
                                            <th style="width: 25%"></th>
                                            <th style="width: 20%">Serie refaccion</th>
                                            <th style="width: 20%">Bodega</th>
                                            <th style="width: 10%">Factura</th>
                                            <th style="width: 10%">Fecha</th>
                                            <th style="width: 20%">Costo</th>
                                            <th style="width: 5%"></th>
                                        </tr>
                                    </thead>
                                    <tbody>';
            }
            foreach ($totalrefacciones->result() as $item) {
                if($item->status>0){
                    $disabled ='disabled';
                    $readonly='readonly';
                    $deletetipo=1;
                }else{
                    $disabled ='';
                    $readonly='';
                    $deletetipo=0;
                    $item->bodega=8;
                }
                $html.='<tr class="cserienew">
                            <td >
                                <input type="checkbox" id="serieIdr_'.$item->serieId.'" class="checkproducto" value="'.$item->serieId.'" '.$disabled.' />
                                <label for="serieIdr_'.$item->serieId.'">'.$item->nombre.'</label> 
                            </td>
                            <td >
                                <input type="text" id="serienew" class="form-control cserienewi cserienewi_3_'.$item->serieId.'" 
                                value="'.$item->serie.'" '.$readonly.' 
                                onchange="verificarserie('.$item->serieId.',3)"
                                ondblclick="editarserie('.$id.','.$item->serieId.',2)"/>
                            </td>
                            <td>
                                <select id="idbodega" name="idbodega" class="browser-default chosen" '.$disabled.'>';
                                $html.='<option></option>';
                                foreach ($bodegas->result() as $itemb) {
                                    if ($itemb->bodegaId==$item->bodega) {
                                        $selected='selected';
                                    }else{
                                        $selected='';
                                    }
                                $html.='<option value="'.$itemb->bodegaId.'" '.$selected.' >'.$itemb->bodega.'</option>';
                                 } 
                                $html.='</select>';
                    $html.='</td>
                            <td>
                                <input type="text" id="factura" class="form-control cserienewi" value="'.$item->factura.'" '.$disabled.' />
                            </td>
                            <td>
                                <input type="date" id="factura_fecha" class="form-control cserienewi" value="'.$item->factura_fecha.'" '.$disabled.' />
                            </td>
                            <td>
                                <a onclick="deletecompra('.$item->serieId.','.$deletetipo.',3,'.$item->compraId.')" class="btn-bmz red gradient-shadow">
                                    <i class="fas fa-trash-alt"></i></a>
                            </td>
                        </tr>'; 
            }
            if($totalrefacciones->num_rows()>0){
                        $html.='</tbody>
                            </table>
                        </div>
                    </div>';
            } 
        //=========================================================================================
            $totalrefaccionesss = $this->Compras_model->getcomprasrefaccionesss($id); 
            foreach ($totalrefaccionesss->result() as $itemc) {
                $html.='<div class="row">
                            <div class="col s12 m12 12 tablacompras">
                                <table id="tabla_refaccionesss" class="striped">
                                    <thead>
                                        <tr>
                                            <th style="width: 30%">Refaccion</th>
                                            <th style="width: 20%">Cantidad</th>
                                            <th style="width: 20%">Entraron</th>
                                            <th style="width: 20%"></th>
                                        </tr>
                                        <tr>
                                            <th>
                                            <input type="text" id="refaccion_con_'.$itemc->serieId.'" 
                                                       class="form-control cserienewi" 
                                                       value="'.$itemc->codigo.' ('.$itemc->nombre.')" 
                                                       readonly/>
                                                <input type="hidden" id="refaid_con_'.$itemc->serieId.'" 
                                                       value="'.$itemc->refaccionid.'" 
                                                       readonly/>
                                            </th>
                                            <th>
                                                <input type="text" id="refa_cant_con_'.$itemc->serieId.'" 
                                                       class="form-control cserienewi refa_cantidad_'.$itemc->serieId.'" 
                                                       value="'.$itemc->cantidad.'" 
                                                       readonly/>
                                            </th>
                                            <th>
                                                <input type="text" id="refa_ent_con_'.$itemc->serieId.'" 
                                                       class="form-control cserienewi refa_reci'.$itemc->serieId.'" 
                                                       value="'.$itemc->entraron.'" 
                                                       readonly/>
                                            </th>
                                            <th><button class="btn-bmz" onclick="addrefaccion('.$itemc->serieId.')"> Agregar <i class="material-icons right">system_update_alt</i></button></th>
                                            <td>';
                                            if($itemc->entraron>0){
                                                $html.='<a onclick="edit_cant_item('.$itemc->serieId.',2,'.$idcompra.','.$itemc->cantidad.','.$itemc->entraron.')" class="btn-bmz blue"><i class="fas fa-pencil-alt"></i></a>';
                                            }else{
                                                $html.='<a onclick="delete_item('.$itemc->serieId.',2,'.$idcompra.')" class="btn-bmz red"><i class="fas fa-trash-alt"></i></a>';
                                            }
                                    $html.='</td>
                                        </tr>
                                    </thead>
                                    <tbody class="refaccion_trss_'.$itemc->serieId.' refaccionss_add">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        ';
            }
        //====================================== REFACCIONES ==================================================
        //====================================== CONSUMIBLES ==================================================
            $totalconsumibles = $this->Compras_model->getcomprasconsumibles($id); 
            foreach ($totalconsumibles->result() as $itemc) {
                $html.='<div class="row">
                            <div class="col s12 m12 12 tablacompras">
                                <table id="tabla_consumibles" class="striped">
                                    <thead>
                                        <tr>
                                            <th style="width: 30%">Consumible</th>
                                            <th style="width: 20%">Cantidad</th>
                                            <th style="width: 20%">Entraron</th>
                                            <th style="width: 20%"></th>
                                        </tr>
                                        <tr>
                                            <th><input type="text" id="consut_con_'.$itemc->cconsumibleId.'" 
                                                       class="form-control cserienewi" 
                                                       value="'.$itemc->modelo.' ('.$itemc->parte.')" 
                                                       readonly/>
                                                <input type="hidden" 
                                                       id="consuid_con_'.$itemc->cconsumibleId.'" 
                                                       value="'.$itemc->consumibleId.'" 
                                                       readonly/>
                                            </th>
                                            <th><input type="text" id="cant_con_'.$itemc->cconsumibleId.'" 
                                                       class="form-control cserienewi consu_cantidad_'.$itemc->cconsumibleId.'" 
                                                       value="'.$itemc->cantidad.'" 
                                                       readonly/>
                                            </th>
                                            <th><input type="text" id="ent_con_'.$itemc->cconsumibleId.'" 
                                                       class="form-control cserienewi consu_reci'.$itemc->cconsumibleId.'" 
                                                       value="'.$itemc->recibidos.'" 
                                                       readonly/>
                                            </th>
                                            <th><button class="btn-bmz" onclick="addconsumible('.$itemc->cconsumibleId.','.$id.')"> Agregar <i class="material-icons right">system_update_alt</i></button></th>
                                            <td>';
                                            if($itemc->recibidos>0){
                                                $html.='<a onclick="edit_cant_item('.$itemc->cconsumibleId.',3,'.$idcompra.','.$itemc->cantidad.','.$itemc->recibidos.')" class="btn-bmz blue"><i class="fas fa-pencil-alt"></i></a>';
                                            }else{
                                                $html.='<a onclick="delete_item('.$itemc->cconsumibleId.',3,'.$idcompra.')" class="btn-bmz red"><i class="fas fa-trash-alt"></i></a>';
                                            }
                                    $html.='</td>
                                        </tr>
                                    </thead>
                                    <tbody class="consumible_'.$itemc->cconsumibleId.' consumibles_add">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        ';
            }

        //====================================== CONSUMIBLES ==================================================
        echo $html;
    }
    function modal_series_m_consul(){
        $id = $this->input->post('id');
        $compraId = array('compraId' => $id,'activo'=>1);
        $html = '';
        $activo = array('activo'=>1);
        $bodegas = $this->Compras_model->getselectwherenall('bodegas',$activo); 
        //====================================== PRODUCTOS ==================================================
            //$totalequipo = $this->Compras_model->getselectwherenall('series_compra_productos',$compraId); // editar esta consulta para colocar el modelo de la serie
            $totalequipo = $this->Compras_model->getseries_compra_productos($id);
            if($totalequipo->num_rows()>0){
                $html.='<div class="row">
                            
                            <div class="col s12 m12 12">
                                <table id="tabla_produto" class="striped">
                                    <thead>
                                        <tr>
                                            <th style="width: 30%"></th>
                                            <th style="width: 25%">Serie Equipo</th>
                                            <th style="width: 25%">Bodega</th>
                                            <th style="width: 10%">Factura</th>
                                            <th style="width: 10%">Fecha</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
            }
            foreach ($totalequipo->result() as $item) {
                            if($item->status){
                                $disabled ='disabled';
                            }else{
                                $disabled ='';
                            }
                            $html.='<tr class="cserienew">
                                        <td >'.$item->modelo.'</td>
                                        <td >'.$item->serie.'</td>
                                        <td>';
                                            $selected='';
                                            foreach ($bodegas->result() as $itemb) {
                                                if ($itemb->bodegaId==$item->bodega) {
                                                    $selected=$itemb->bodega;
                                                }
                                             } 
                                            $html.=$selected;
                                $html.='</td>
                                        <td >'.$item->factura.'</td>
                                        <td >'.$item->factura_fecha.'</td>
                                    </tr>';
            }
            if($totalequipo->num_rows()>0){
                        $html.='</tbody>
                            </table>
                        </div>
                    </div>';
            }
        //====================================== PRODUCTOS ==================================================
        //====================================== ACCESORIOS ==================================================
            //$totalaccesorios = $this->Compras_model->getselectwherenall('series_compra_accesorios',$compraId); 
            $totalaccesorios = $this->Compras_model->getseries_compra_accesorios($id); 
            if ($totalaccesorios->num_rows()>0) {
                $html.='<div class="row">
                            <div class="col s12 m12 12 ">
                                <table id="tabla_accesorio" class="striped">
                                    <thead>
                                        <tr>
                                            <th style="width: 30%"></th>
                                            <th style="width: 25%">Serie Accesorio</th>
                                            <th style="width: 25%">Bodega</th>
                                            <th style="width: 10%">Factura</th>
                                            <th style="width: 10%">Fecha</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
            }
            foreach ($totalaccesorios->result() as $item) {
                           $html.='<tr class="cserienew">
                                        <td >'.$item->nombre.'</td>
                                        <td >'.$item->serie.'</td>
                                        <td>';
                                            $selected='';
                                            foreach ($bodegas->result() as $itemb) {
                                                if ($itemb->bodegaId==$item->bodega) {
                                                    $selected=$itemb->bodega;
                                                }
                                             } 
                                            $html.=$selected;
                                            $html.='</td>
                                            <td >'.$item->factura.'</td>
                                            <td >'.$item->factura_fecha.'</td>
                                    </tr>';               
            }
            if ($totalaccesorios->num_rows()>0) {
                           $html.='</tbody>
                            </table>
                        </div>
                    </div>'; 
            }
            $totalrefaccionesss = $this->Compras_model->getseries_compra_accesoriosss($id); 
            foreach ($totalrefaccionesss->result() as $itemc) {
                $html.='<div class="row">
                            <div class="col s12 m12 12 tablacompras">
                                <table id="tabla_refaccionesss" class="striped">
                                    <thead>
                                        <tr>
                                            <th style="width: 30%">Refaccion</th>
                                            <th style="width: 30%">Cantidad</th>
                                            <th style="width: 20%"></th>
                                        </tr>
                                        <tr>
                                            <th>'.$itemc->nombre.'</th>
                                            <th>'.$itemc->entraron.'</th>
                                            <th>
                                                
                                            </th>
                                        </tr>
                                    </thead>
                                    
                                </table>
                            </div>
                        </div>
                        ';
            }
        //====================================== ACCESORIOS ==================================================
        //====================================== REFACCIONES ==================================================
            //$compraId = array('compraId' => $id,'activo'=>1,'con_serie'=>1);
            //$totalrefacciones = $this->Compras_model->getselectwherenall('series_compra_refacciones',$compraId); 
            $totalrefacciones = $this->Compras_model->getseries_compra_refacciones($id); 
            if($totalrefacciones->num_rows()>0){
                $html.='<div class="row">
                            <div class="col s12 m12 12 tablacompras">
                                <table id="tabla_refaccion" class="striped">
                                    <thead>
                                        <tr>
                                            <th style="width: 30%"></th>
                                            <th style="width: 25%">Serie refaccion</th>
                                            <th style="width: 25%">Bodega</th>
                                            <th style="width: 10%">Factura</th>
                                            <th style="width: 10%">Fecha</th>
                                        </tr>
                                    </thead>
                                    <tbody>';
            }
            foreach ($totalrefacciones->result() as $item) {
                $html.='<tr class="cserienew">
                            <td >'.$item->nombre.'</td>
                            <td >'.$item->serie.'</td>
                            <td>';
                                    $selected='';
                                foreach ($bodegas->result() as $itemb) {
                                    if ($itemb->bodegaId==$item->bodega) {
                                        $selected=$itemb->bodega;
                                    }
                                 } 
                                $html.=$selected;
                    $html.='</td>
                            <td >'.$item->factura.'</td>
                            <td >'.$item->factura_fecha.'</td>
                        </tr>'; 
            }
            if($totalrefacciones->num_rows()>0){
                        $html.='</tbody>
                            </table>
                        </div>
                    </div>';
            } 
            //=========================================================================================
            $totalrefaccionesss = $this->Compras_model->getcomprasrefaccionesss($id); 
            foreach ($totalrefaccionesss->result() as $itemc) {
                $html.='<div class="row">
                            <div class="col s12 m12 12 tablacompras">
                                <table id="tabla_refaccionesss" class="striped">
                                    <thead>
                                        <tr>
                                            <th style="width: 30%">Refaccion</th>
                                            <th style="width: 30%">Cantidad</th>
                                            <th style="width: 20%"></th>
                                        </tr>
                                        <tr>
                                            <th>'.$itemc->codigo.' ('.$itemc->nombre.')</th>
                                            <th>'.$itemc->entraron.'</th>
                                            <th>
                                                
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody class="refaccion_trss_'.$itemc->serieId.' refaccionss_add">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        ';
            }
        //====================================== REFACCIONES ==================================================
        //====================================== CONSUMIBLES ==================================================
            $totalconsumibles = $this->Compras_model->getcomprasconsumibles($id); 
            foreach ($totalconsumibles->result() as $itemc) {
                $html.='<div class="row">
                            <div class="col s12 m12 12 tablacompras">
                                <table id="tabla_consumibles" class="striped">
                                    <thead>
                                        <tr>
                                            <th style="width: 30%">Consumible</th>
                                            <th style="width: 30%">Cantidad</th>
                                        </tr>
                                        <tr>
                                            <th>'.$itemc->modelo.' ('.$itemc->parte.')</th>
                                            <th>'.$itemc->recibidos.'
                                            </th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody class="consumible_'.$itemc->cconsumibleId.' consumibles_add">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        ';
            }

        //====================================== CONSUMIBLES ==================================================
        echo $html;
    }
    public function guardar_series(){
        $data = $this->input->post();
        // Equipo 
            $arrayequipos = $data['arrayequipos'];
            $DATAp = json_decode($arrayequipos); 
            for ($i=0;$i<count($DATAp);$i++) {
                $factura_fecha=$DATAp[$i]->factura_fecha;
                if($factura_fecha==''){
                    $factura_fecha=$this->fechahoyc;
                }

                $serie_id_p = array('serieId' => $DATAp[$i]->checkproducto); 
                $totalequipo = $this->Compras_model->getselectwherenall('series_compra_productos',$serie_id_p);

                foreach ($totalequipo->result() as $item) {
                    $idproducto = $item->productoid;
                    $serie = $item->serie;
                }
                $detallesequipos = array(
                    'bodegaId'=>$DATAp[$i]->idbodega,
                    'productoid'=>$idproducto,
                    'serie'=>$DATAp[$i]->serie,
                    'reg'=>$this->fechahoy,
                    'personalId'=>$this->idpersonal,
                    'reg'=>$factura_fecha
                );
                $serieId=$this->ModeloCatalogos->Insert('series_productos',$detallesequipos);
                
                $this->ModeloCatalogos->updateCatalogo('series_compra_productos',array('status' => 1,'serie'=>$DATAp[$i]->serie,'bodega'=>$DATAp[$i]->idbodega,'factura'=>$DATAp[$i]->factura,'factura_fecha'=>$DATAp[$i]->factura_fecha),$serie_id_p);

                

                $this->ModeloCatalogos->Insert('bitacora_movimientos',array('entrada_salida'=>1,'descripcion_evento'=>'Compra','personal'=>$this->idpersonal,'tipo'=>1,'modeloId'=>$idproducto,'serieId'=>$serieId,'bodega_d'=>$DATAp[$i]->idbodega));
            }
        // Accesorios 
            $arrayaccesorios = $data['arrayaccesorios'];
            $DATAp = json_decode($arrayaccesorios); 
            for ($i=0;$i<count($DATAp);$i++) {
                $factura_fecha=$DATAp[$i]->factura_fecha;
                if($factura_fecha==''){
                    $factura_fecha=$this->fechahoyc;
                }
                $serie_id_p = array('serieId' => $DATAp[$i]->checkproducto); 
                $totaleaccesorio = $this->Compras_model->getselectwherenall('series_compra_accesorios',$serie_id_p);
                foreach ($totaleaccesorio->result() as $item) {
                    $accesoriosid = $item->accesoriosid;
                    $serie = $item->serie;
                }
                $detallesaccesorios = array(
                    'bodegaId'=>$DATAp[$i]->idbodega,
                    'accesoriosid'=>$accesoriosid,
                    'serie'=>$DATAp[$i]->serie,
                    'reg'=>$this->fechahoy,
                    'personalId'=>$this->idpersonal,
                    'reg'=>$factura_fecha
                );
                $serieId=$this->ModeloCatalogos->Insert('series_accesorios',$detallesaccesorios);
                $this->ModeloCatalogos->updateCatalogo('series_compra_accesorios',array('status' => 1,'serie'=>$DATAp[$i]->serie,'bodega'=>$DATAp[$i]->idbodega,'factura'=>$DATAp[$i]->factura,'factura_fecha'=>$DATAp[$i]->factura_fecha),$serie_id_p);

                $this->ModeloCatalogos->Insert('bitacora_movimientos',array('entrada_salida'=>1,'descripcion_evento'=>'Compra','personal'=>$this->idpersonal,'tipo'=>2,'modeloId'=>$accesoriosid,'serieId'=>$serieId,'bodega_d'=>$DATAp[$i]->idbodega));
            }
        // Accesorios sin serie
            $arrayaccesorions = $data['arrayaccesorions'];
            $DATAas = json_decode($arrayaccesorions);
            //log_message('error', 'existe consulta data refaccion: '.$DATAas); 
            for ($i=0;$i<count($DATAas);$i++) {
                $tr=$DATAas[$i]->tr;
                $accesorio=$DATAas[$i]->accesorio;
                $cantidad=$DATAas[$i]->cantidad;
                $bodegaId=$DATAas[$i]->bodegaId;

                $whererefacs = array(
                    'bodegaId' => $bodegaId,
                    'accesoriosid' => $accesorio,
                    'con_serie'=>0
                ); 
                $resultaccess = $this->Compras_model->getselectwherenall('series_accesorios',$whererefacs);
                $existe=0;
                foreach ($resultaccess->result() as $item) {
                    $existe=$item->serieId;
                    $serieId=$existe;
                }
                //log_message('error', 'existe consulta refaccion: '.$existe);
                if ($existe>=1) {
                    //$this->ModeloCatalogos->updateCatalogo('consumibles_bodegas',array('total' =>'total+'.$cantidad),$whereconsu);
                    $this->ModeloCatalogos->updatestock('series_accesorios','cantidad','+',$cantidad,'serieId',$existe);
                    $this->ModeloCatalogos->updateCatalogo('series_accesorios',array('status' => 0),array('serieId'=>$existe));
                }else{
                    $detallesconsumibles = array(
                        'bodegaId'=>$bodegaId,
                        'accesoriosid'=>$accesorio,
                        'serie'=>'Sin Serie',
                        'con_serie'=>0,
                        'cantidad'=>$cantidad,
                        'status'=>0,
                        'personalId'=>$this->idpersonal
                    );
                    $serieId=$this->ModeloCatalogos->Insert('series_accesorios',$detallesconsumibles);
                }
                $this->ModeloCatalogos->updatestock('series_compra_accesorios','entraron','+',$cantidad,'serieId',$tr);
                //$this->ModeloCatalogos->updateCatalogo('compra_consumibles',array('recibidos' =>'recibidos+'.$cantidad),array('cconsumibleId'=>$tr));
                //$this->updateinvetariorefaccion($DATArs[$i]->bodegaId,$DATArs[$i]->refaccion,$DATArs[$i]->cantidad);  
                $this->ModeloCatalogos->Insert('bitacora_movimientos',array('entrada_salida'=>1,'descripcion_evento'=>'Compra','personal'=>$this->idpersonal,'tipo'=>2,'modeloId'=>$accesorio,'serieId'=>$serieId,'bodega_d'=>$bodegaId,'cantidad'=>$cantidad));
            }
        // Refacciones
            $arrayrefaccion = $data['arrayrefaccion'];
            $DATAp = json_decode($arrayrefaccion); 
            for ($i=0;$i<count($DATAp);$i++) {
                $factura_fecha=$DATAp[$i]->factura_fecha;
                if($factura_fecha==''){
                    $factura_fecha=$this->fechahoyc;
                }
                $serie_id_p = array('serieId' => $DATAp[$i]->checkproducto); 
                $totalerefacciones = $this->Compras_model->getselectwherenall('series_compra_refacciones',$serie_id_p);
                foreach ($totalerefacciones->result() as $item) {
                    $refaccionid = $item->refaccionid;
                    $serie = $item->serie;
                }
                $detallesrefacciones = array(
                    'bodegaId'=>$DATAp[$i]->idbodega,
                    'refaccionid'=>$refaccionid,
                    'serie'=>$DATAp[$i]->serie,
                    'reg'=>$this->fechahoy,
                    'personalId'=>$this->idpersonal,
                    'reg'=>$factura_fecha
                );
                $serieId=$this->ModeloCatalogos->Insert('series_refacciones',$detallesrefacciones);
                $this->ModeloCatalogos->updateCatalogo('series_compra_refacciones',array('status' => 1,'serie'=>$DATAp[$i]->serie,'bodega'=>$DATAp[$i]->idbodega,'factura'=>$DATAp[$i]->factura,'factura_fecha'=>$DATAp[$i]->factura_fecha),$serie_id_p);
                $this->updateinvetariorefaccion($DATAp[$i]->idbodega,$refaccionid,1);

                $this->ModeloCatalogos->Insert('bitacora_movimientos',array('entrada_salida'=>1,'descripcion_evento'=>'Compra','personal'=>$this->idpersonal,'tipo'=>3,'modeloId'=>$refaccionid,'serieId'=>$serieId,'bodega_d'=>$DATAp[$i]->idbodega,'cantidad'=>1));
            }
        // consumible
            $arrayconsumible = $data['arrayconsumible'];
            $DATAc = json_decode($arrayconsumible); 
            for ($i=0;$i<count($DATAc);$i++) {
                $compra=$DATAc[$i]->compra;
                $tr=$DATAc[$i]->tr;
                $consumibleId=$DATAc[$i]->consumibleId;
                $cantidad=$DATAc[$i]->cantidad;
                $bodegaId=$DATAc[$i]->bodegaId;
                if($cantidad>0){
                    $whereconsu = array(
                        'bodegaId' => $bodegaId,
                        'consumiblesId' => $consumibleId,
                        'status'=>1
                    ); 
                    $resultconsu = $this->Compras_model->getselectwherenall('consumibles_bodegas',$whereconsu);
                    $existe=0;
                    foreach ($resultconsu->result() as $item) {
                        $existe=1;
                    }
                    //log_message('error', 'existe consulta: '.$existe);
                    if ($existe==1) {
                        //$this->ModeloCatalogos->updateCatalogo('consumibles_bodegas',array('total' =>'total+'.$cantidad),$whereconsu);
                        $this->ModeloCatalogos->updatestock3('consumibles_bodegas','total','+',$cantidad,'bodegaId',$bodegaId,'consumiblesId',$consumibleId,'status',1);
                    }else{
                        $detallesconsumibles = array(
                            'bodegaId'=>$bodegaId,
                            'consumiblesId'=>$consumibleId,
                            'total'=>$cantidad);
                        $this->ModeloCatalogos->Insert('consumibles_bodegas',$detallesconsumibles);

                        
                    }
                    $this->ModeloCatalogos->Insert('compra_consumibles_bitacora',array('compra'=>$compra,'consumible'=>$consumibleId,'bodega'=>$bodegaId,'cantidad'=>$cantidad));
                    $this->ModeloCatalogos->updatestock('compra_consumibles','recibidos','+',$cantidad,'cconsumibleId',$tr);
                    //$this->ModeloCatalogos->updateCatalogo('compra_consumibles',array('recibidos' =>'recibidos+'.$cantidad),array('cconsumibleId'=>$tr));  
                }
                $this->ModeloCatalogos->Insert('bitacora_movimientos',array('entrada_salida'=>1,'descripcion_evento'=>'Compra','personal'=>$this->idpersonal,'tipo'=>4,'modeloId'=>$consumibleId,'serieId'=>0,'bodega_d'=>$bodegaId,'cantidad'=>$cantidad));
            }
        // Refacciones sin serie
            $arrayrefaccions = $data['arrayrefaccions'];
            $DATArs = json_decode($arrayrefaccions);
            //log_message('error', 'existe consulta data refaccion: '.$DATArs); 
            for ($i=0;$i<count($DATArs);$i++) {
                $tr=$DATArs[$i]->tr;
                $refaccion=$DATArs[$i]->refaccion;
                $cantidad=$DATArs[$i]->cantidad;
                $bodegaId=$DATArs[$i]->bodegaId;

                $whererefacs = array(
                    'bodegaId' => $bodegaId,
                    'refaccionid' => $refaccion,
                    'con_serie'=>0
                ); 
                $resultrefaccioness = $this->Compras_model->getselectwherenall('series_refacciones',$whererefacs);
                $existe=0;
                foreach ($resultrefaccioness->result() as $item) {
                    $existe=$item->serieId;
                    $serieId=$existe;
                }
                //log_message('error', 'existe consulta refaccion: '.$existe);
                if ($existe>=1) {
                    //$this->ModeloCatalogos->updateCatalogo('consumibles_bodegas',array('total' =>'total+'.$cantidad),$whereconsu);
                    $this->ModeloCatalogos->updatestock('series_refacciones','cantidad','+',$cantidad,'serieId',$existe);
                }else{
                    $detallesconsumibles = array(
                        'bodegaId'=>$bodegaId,
                        'refaccionid'=>$refaccion,
                        'serie'=>'Sin Serie',
                        'con_serie'=>0,
                        'cantidad'=>$cantidad,
                        'status'=>0,
                        'personalId'=>$this->idpersonal
                    );
                    $serieId=$this->ModeloCatalogos->Insert('series_refacciones',$detallesconsumibles);
                }
                $this->ModeloCatalogos->updatestock('series_compra_refacciones','entraron','+',$cantidad,'serieId',$tr);
                //$this->ModeloCatalogos->updateCatalogo('compra_consumibles',array('recibidos' =>'recibidos+'.$cantidad),array('cconsumibleId'=>$tr));
                $this->updateinvetariorefaccion($DATArs[$i]->bodegaId,$DATArs[$i]->refaccion,$DATArs[$i]->cantidad);  

                $this->ModeloCatalogos->Insert('bitacora_movimientos',array('entrada_salida'=>1,'descripcion_evento'=>'Compra','personal'=>$this->idpersonal,'tipo'=>3,'modeloId'=>$refaccion,'serieId'=>$serieId,'bodega_d'=>$bodegaId,'cantidad'=>$cantidad));
            }
    }
    function cargaexcel(){
        $compra = $_POST['compra'];
        //$tipocarga = $_POST['tipocarga'];
         //log_message('error', 'cargaexcel '.$archivo.' '.$semana.' '.$years);
         $configUpload['upload_path'] = FCPATH.'fileExcel/';
         $configUpload['allowed_types'] = 'xls|xlsx|csv';
         $configUpload['max_size'] = '5000';
         $this->load->library('upload', $configUpload);
         $this->upload->do_upload('inputFile');  
         $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
         $file_name = $upload_data['file_name']; //uploded file name
         $extension=$upload_data['file_ext'];    // uploded file extension
         //===========================================
            $objReader= PHPExcel_IOFactory::createReader('Excel2007'); // For excel 2007     
            $objReader->setReadDataOnly(true);          
            //Load excel file
            $objPHPExcel=$objReader->load(FCPATH.'fileExcel/'.$file_name);      
            $totalrows=$objPHPExcel->setActiveSheetIndex(0)->getHighestRow();   //Count Numbe of rows avalable in excel         
            $objWorksheet=$objPHPExcel->setActiveSheetIndex(0);                
            //loop from first data untill last data

            for($i=2;$i<=$totalrows;$i++){
                $tipocarga = $objWorksheet->getCellByColumnAndRow(0,$i)->getValue(); 
                $factura= $objWorksheet->getCellByColumnAndRow(1,$i)->getValue(); 
                $serie= $objWorksheet->getCellByColumnAndRow(2,$i)->getValue(); 
                $eq_acc_ref= $objWorksheet->getCellByColumnAndRow(3,$i)->getValue(); 
                $bodega= $objWorksheet->getCellByColumnAndRow(4,$i)->getValue();
                $cantidad= $objWorksheet->getCellByColumnAndRow(5,$i)->getValue(); 
                $con_serie= $objWorksheet->getCellByColumnAndRow(6,$i)->getValue(); 

                if ($factura!='') {
                    if ($tipocarga==1) {
                        $id_equipo=$this->obteneridequipos($eq_acc_ref);
                        if ($id_equipo>0) {
                            //===============
                                $resultado=$this->ModeloCatalogos->getselectwheren('series_compra_productos',array('compraId'=>$compra,'productoid'=>$id_equipo,'status'=>0));
                                    if ($resultado->num_rows()>0) {
                                        $resultado=$resultado->result();
                                        $this->ModeloCatalogos->updateCatalogo('series_compra_productos',array('factura'=>$factura,'serie'=>$serie,'bodega'=>$bodega,'status' => 1),array('serieId'=>$resultado[0]->serieId));

                                        $detallesequipos = array(
                                            'bodegaId'=>$bodega,
                                            'productoid'=>$resultado[0]->productoid,
                                            'serie'=>$serie,
                                            'reg'=>$this->fechahoy,
                                            'personalId'=>$this->idpersonal);
                                        $this->ModeloCatalogos->Insert('series_productos',$detallesequipos);
                                    }
                            //===============
                        }
                        
                        
                    }elseif ($tipocarga==2) {
                        $id_accesorio=$this->obteneridaccesorios($eq_acc_ref);
                        if ($id_accesorio>0) {
                            //============================
                                $resultado=$this->ModeloCatalogos->getselectwheren('series_compra_accesorios',array('compraId'=>$compra,'accesoriosid'=>$id_accesorio,'status'=>0));
                                if ($resultado->num_rows()>0) {
                                    $resultado=$resultado->result();

                                    $this->ModeloCatalogos->updateCatalogo('series_compra_accesorios',array('factura'=>$factura,'serie'=>$serie,'bodega'=>$bodega,'status' => 1),array('serieId'=>$resultado[0]->serieId));

                                    $detallesaccesorios = array(
                                        'bodegaId'=>$bodega,
                                        'accesoriosid'=>$id_accesorio,
                                        'serie'=>$serie,
                                        'reg'=>$this->fechahoy,
                                        'personalId'=>$this->idpersonal);
                                    $this->ModeloCatalogos->Insert('series_accesorios',$detallesaccesorios);
                                }
                            //============================
                        }
                        
                        
                    }elseif ($tipocarga==3) {
                        $id_refaccion=$this->obteneridrefacciones($eq_acc_ref);
                        if ($id_refaccion>0) {
                            $resultado=$this->ModeloCatalogos->getselectwheren('series_compra_refacciones',array('compraId'=>$compra,'refaccionid'=>$id_refaccion,'status'=>0,'con_serie'=>0));
                            if ($resultado->num_rows()>0) {
                                $resultado=$resultado->result();
                                if ($con_serie==1) {// refaccion con serie
                                    $this->ModeloCatalogos->updateCatalogo('series_compra_refacciones',array('status' => 1,'serie'=>$serie,'bodega'=>$bodega,'factura'=>$factura),array('serieId'=>$resultado[0]->serieId));

                                    $detallesrefacciones = array(
                                        'bodegaId'=>$bodega,
                                        'refaccionid'=>$id_refaccion,
                                        'serie'=>$serie,
                                        'reg'=>$this->fechahoy,
                                        'personalId'=>$this->idpersonal);
                                    $this->ModeloCatalogos->Insert('series_refacciones',$detallesrefacciones);
                                }else{ // refaccion sin serie
                                    $resultado[0]->cantidad;
                                    $resultado[0]->entraron;
                                    if ($resultado[0]->entraron<$resultado[0]->cantidad) {
                                        $cantidadqueentra=$resultado[0]->cantidad-$resultado[0]->entraron;
                                        if ($cantidadqueentra>=$cantidad) {
                                            $cantidad=$cantidad;
                                        }else{
                                            $cantidad=$cantidadqueentra;
                                        }
                                        $whererefacs = array(
                                                'bodegaId' => $bodega,
                                                'refaccionid' => $id_refaccion,
                                                'con_serie'=>0
                                            ); 
                                            $resultrefaccioness = $this->Compras_model->getselectwherenall('series_refacciones',$whererefacs);
                                            $existe=0;
                                            foreach ($resultrefaccioness->result() as $item) {
                                                $existe=$item->serieId;
                                            }
                                            //log_message('error', 'existe consulta refaccion: '.$existe);
                                            if ($existe>=1) {
                                                //$this->ModeloCatalogos->updateCatalogo('consumibles_bodegas',array('total' =>'total+'.$cantidad),$whereconsu);
                                                $this->ModeloCatalogos->updatestock('series_refacciones','cantidad','+',$cantidad,'serieId',$existe);
                                            }else{
                                                $detallesconsumibles = array(
                                                    'bodegaId'=>$bodegaId,
                                                    'refaccionid'=>$refaccion,
                                                    'serie'=>'Sin Serie',
                                                    'con_serie'=>0,
                                                    'cantidad'=>$cantidad,
                                                    'status'=>0,
                                                    'personalId'=>$this->idpersonal
                                                );
                                                $this->ModeloCatalogos->Insert('series_refacciones',$detallesconsumibles);
                                            }
                                            $this->ModeloCatalogos->updatestock('series_compra_refacciones','entraron','+',$cantidad,'serieId',$resultado[0]->serieId);
                                    }


                                    
                                }

                                
                            }



                            

                        }
                        
                        
                    }elseif ($tipocarga==4) {
                        $consumibleId=$this->obteneridconsumibles($eq_acc_ref);
                        if ($consumibleId>0) {
                            //===============================================================
                                $resultado=$this->ModeloCatalogos->getselectwheren('compra_consumibles',array('compraId'=>$compra,'consumibleId'=>$consumibleId));
                                if ($resultado->num_rows()>0) {
                                     $resultado=$resultado->result();
                                    if ($resultado[0]->recibidos<$resultado[0]->cantidad) {
                                        //========================================================
                                            $cantidadqueentra=$resultado[0]->cantidad-$resultado[0]->recibidos;
                                            if ($cantidadqueentra>=$cantidad) {
                                                $cantidad=$cantidad;
                                            }else{
                                                $cantidad=$cantidadqueentra;
                                            }
                                            $whereconsu = array(
                                                'bodegaId' => $bodega,
                                                'consumiblesId' => $consumibleId,
                                                'status'=>1
                                            ); 
                                            $resultconsu = $this->Compras_model->getselectwherenall('consumibles_bodegas',$whereconsu);
                                            $existe=0;
                                            foreach ($resultconsu->result() as $item) {
                                                $existe=1;
                                            }
                                            //log_message('error', 'existe consulta: '.$existe);
                                            if ($existe==1) {
                                                //$this->ModeloCatalogos->updateCatalogo('consumibles_bodegas',array('total' =>'total+'.$cantidad),$whereconsu);
                                                $this->ModeloCatalogos->updatestock3('consumibles_bodegas','total','+',$cantidad,'bodegaId',$bodega,'consumiblesId',$consumibleId,'status',1);
                                            }else{
                                                $detallesconsumibles = array(
                                                    'bodegaId'=>$bodega,
                                                    'consumiblesId'=>$consumibleId,
                                                    'total'=>$cantidad);
                                                $this->ModeloCatalogos->Insert('consumibles_bodegas',$detallesconsumibles);
                                            }
                                            $this->ModeloCatalogos->updatestock('compra_consumibles','recibidos','+',$cantidad,'cconsumibleId',$resultado[0]->cconsumibleId);
                                            
                                            $this->ModeloCatalogos->Insert('bitacora_movimientos',array('entrada_salida'=>1,'descripcion_evento'=>'Compra','personal'=>$this->idpersonal,'tipo'=>4,'modeloId'=>$consumibleId,'serieId'=>0,'bodega_d'=>$bodega,'cantidad'=>$cantidad));
                                        //========================================================

                                    }




                                    
                                }



                                
                            //===============================================================
                        }
                        
                    }

                    
                }
              
            }
            unlink('fileExcel/'.$file_name); //File Deleted After uploading in database . 

         //===========================================
        $output = [];
        echo json_encode($output);
    }
    function editar_series(){
        $data = $this->input->post();
        $compra =$data['compra'];
        $row =$data['row'];
        $tipo =$data['tipo'];
        $serie =$data['serie'];
        if ($tipo==1) { //Equipos
            $consultar=$this->ModeloCatalogos->getselectwheren('series_compra_productos',array('serieId'=>$row));
            $serieo='';
            foreach ($consultar->result() as $itemc) {
                $serieo=$itemc->serie;
            }
            //=====================================
                $consultaro=$this->ModeloCatalogos->getselectwheren('series_productos',array('serie'=>$serieo));
                $serieIdo=0;
                foreach ($consultaro->result() as $itemco) {
                    $serieIdo=$itemco->serieId;
                }
                $this->ModeloCatalogos->updateCatalogo('series_productos',array('serie'=>$serie),array('serieId'=>$serieIdo));
            //=====================================
            $this->ModeloCatalogos->updateCatalogo('series_compra_productos',array('serie'=>$serie),array('serieId'=>$row));
        }elseif ($tipo==2) { //Accesorios
            $consultar=$this->ModeloCatalogos->getselectwheren('series_compra_accesorios',array('serieId'=>$row));
            $serieo='';
            foreach ($consultar->result() as $itemc) {
                $serieo=$itemc->serie;
            }
            //=====================================
                $consultaro=$this->ModeloCatalogos->getselectwheren('series_accesorios',array('serie'=>$serieo));
                $serieIdo=0;
                foreach ($consultaro->result() as $itemco) {
                    $serieIdo=$itemco->serieId;
                }
                $this->ModeloCatalogos->updateCatalogo('series_accesorios',array('serie'=>$serie),array('serieId'=>$serieIdo));
            //=====================================
            $this->ModeloCatalogos->updateCatalogo('series_compra_accesorios',array('serie'=>$serie),array('serieId'=>$row));
        }elseif ($tipo==3) { //Refacciones
            $consultar=$this->ModeloCatalogos->getselectwheren('series_compra_refacciones',array('serieId'=>$row));
            $serieo='';
            foreach ($consultar->result() as $itemc) {
                $serieo=$itemc->serie;
            }
            //=====================================
                $consultaro=$this->ModeloCatalogos->getselectwheren('series_refacciones',array('serie'=>$serieo));
                $serieIdo=0;
                foreach ($consultaro->result() as $itemco) {
                    $serieIdo=$itemco->serieId;
                }
                $this->ModeloCatalogos->updateCatalogo('series_refacciones',array('serie'=>$serie),array('serieId'=>$serieIdo));
            //=====================================
            $this->ModeloCatalogos->updateCatalogo('series_compra_refacciones',array('serie'=>$serie),array('serieId'=>$row));
        }    
    }
    function reportepdf($id){
        $data['id']=$id;
        $activo = array('activo'=>1);
        $compra = $this->ModeloCatalogos->getselectwheren('series_compra',array('compraId'=>$id)); 
        $data['bodegas'] = $this->Compras_model->getselectwherenall('bodegas',$activo); 
        $data['totalequipo'] = $this->Compras_model->getseries_compra_productos($id);
        $data['totalaccesorios'] = $this->Compras_model->getseries_compra_accesorios($id);
        $data['totalaccesoriosss'] = $this->Compras_model->getseries_compra_accesoriosss($id);
        $data['totalrefacciones'] = $this->Compras_model->getseries_compra_refacciones($id); 
        $data['totalrefaccionesss'] = $this->Compras_model->getcomprasrefaccionesss($id); 
        $data['totalconsumibles'] = $this->Compras_model->getcomprasconsumibles($id); 
        $personalId =0;
        foreach ($compra->result() as $itemc) {
            $data['folio'] = $itemc->folio;
            $data['reg']=$itemc->reg;
            $personalId = $itemc->personalId;
        }
        $personal = $this->ModeloCatalogos->getselectwheren('personal',array('personalId'=>$personalId));
        $data['personalnombre']='';
        foreach ($personal->result() as $itemp) {
            $data['personalnombre']=$itemp->nombre.' '.$itemp->apellido_paterno.' '.$itemp->apellido_materno;
        }

        $this->load->view('Reportes/Compraspdf',$data);
    }
    //====================================================================
    function obteneridequipos($modelo){
        $resultado=$this->ModeloCatalogos->getselectwheren('equipos',array('modelo'=>$modelo,'estatus'=>1));
        $id=0;
        foreach ($resultado->result() as $item) {
            $id=$item->id;
        }
        return $id;
    }
    function obteneridaccesorios($modelo){
        $resultado=$this->ModeloCatalogos->getselectwheren('catalogo_accesorios',array('nombre'=>$modelo,'status'=>1));
        $id=0;
        foreach ($resultado->result() as $item) {
            $id=$item->id;
        }
        return $id;
    }
    function obteneridrefacciones($modelo){
        $resultado=$this->ModeloCatalogos->getselectwheren('refacciones',array('codigo'=>$modelo,'status'=>1));
        $id=0;
        foreach ($resultado->result() as $item) {
            $id=$item->id;
        }
        return $id;
    }
    function obteneridconsumibles($modelo){
        $resultado=$this->ModeloCatalogos->getselectwheren('consumibles',array('modelo'=>$modelo,'status'=>1));
        $id=0;
        foreach ($resultado->result() as $item) {
            $id=$item->id;
        }
        return $id;
    }
    //=============================================================
    function updateinvetariorefaccion($bodega,$refaccion,$cantidad){
            $whereconsu = array( 
                'bodega' => $bodega, 
                'refaccion' => $refaccion, 
                'activo'=>1 
            );  
            $resultconsur = $this->ModeloCatalogos->getselectwherenall('inventario_refacciones',$whereconsu); 
            $consubId=0; 
            foreach ($resultconsur as $item) { 
                $consubId=$item->id; 
            } 
            if ($consubId==0) {
                $detallesconsumibles = array(
                        'bodega'=>$bodega,
                        'refaccion'=>$refaccion,
                        'stock'=>$cantidad
                    );
                $idnew=$this->ModeloCatalogos->Insert('inventario_refacciones',$detallesconsumibles);
            }else{
                $this->ModeloCatalogos->updatestock3('inventario_refacciones','stock','+',$cantidad,'bodega',$bodega,'refaccion',$refaccion,'activo',1);
            }
    }
    function updateinventarioaccesorios($bodega,$accesorios,$cantidad){
        $whereconsu = array( 
            'bodega' => $bodega, 
            'accesoriosid' => $accesorios, 
            'activo'=>1 
        );  
        $resultconsu = $this->ModeloCatalogos->getselectwherenall('inventario_accesorios',$whereconsu); 
        $consubId=0; 
        foreach ($resultconsu as $item) { 
            $consubId=$item->id; 
        } 
        if ($consubId==0) {
            $detallesconsumibles = array(
                    'bodega'=>$bodega,
                    'accesoriosid'=>$accesorios,
                    'stock'=>$cantidad
                );
            $idnew=$this->ModeloCatalogos->Insert('inventario_accesorios',$detallesconsumibles);
        }else{
            $this->ModeloCatalogos->updatestock3('inventario_accesorios','stock','+',$cantidad,'bodega',$bodega,'accesoriosid',$accesorios,'status',1);
        }
    }
    function delete_series(){
        $data = $this->input->post();
        $compra     = $data['compra'];
        $serieid      = $data['serie'];
        $tipo       = $data['tipo'];
        $tipoearc   = $data['tipoearc'];
        if ($tipoearc==1) {
            if ($tipo==1) {
                $result=$this->ModeloCatalogos->getselectwheren('series_compra_productos',array('serieId'=>$serie));
                $productoid=0;
                $serie='';
                $bodega=0;
                foreach ($result->result() as $item) {
                    $productoid=$item->productoid;
                    $serie=$item->serie;
                    $bodega=$item->bodega;
                }
                $this->ModeloCatalogos->getdeletewheren('series_productos',array('bodegaId'=>$bodega,'productoid'=>$productoid,'serie'=>$serie));
                $this->ModeloCatalogos->updateCatalogo('series_compra_productos',array('factura'=>NULL,'serie'=>'','bodega'=>0,'status'=>0),array('serieId'=>$serieid));
            }else{
                $this->ModeloCatalogos->getdeletewheren('series_compra_productos',array('serieId'=>$serieid));
            }
        }
        if ($tipoearc==2) {
            if ($tipo==1) {
                $result=$this->ModeloCatalogos->getselectwheren('series_compra_accesorios',array('serieId'=>$serieid));
                $accesoriosid=0;
                $serie='';
                $bodega=0;
                foreach ($result->result() as $item) {
                    $accesoriosid=$item->accesoriosid;
                    $serie=$item->serie;
                    $bodega=$item->bodega;
                }
                $this->ModeloCatalogos->getdeletewheren('series_accesorios',array('bodegaId'=>$bodega,'accesorioid'=>$accesoriosid,'serie'=>$serie));
                $this->ModeloCatalogos->updateCatalogo('series_compra_accesorios',array('factura'=>NULL,'serie'=>'','bodega'=>0,'status'=>0),array('serieId'=>$serieid));
            }else{
                $this->ModeloCatalogos->getdeletewheren('series_compra_accesorios',array('serieId'=>$serieid));
            }
        }
        if ($tipoearc==3) {
            if ($tipo==1) {
                $result=$this->ModeloCatalogos->getselectwheren('series_compra_refacciones',array('serieId'=>$serieid));
                $refaccionid=0;
                $serie='';
                $bodega=0;
                foreach ($result->result() as $item) {
                    $refaccionid=$item->refaccionid;
                    $serie=$item->serie;
                    $bodega=$item->bodega;
                }
                $this->ModeloCatalogos->getdeletewheren('series_refacciones',array('bodegaId'=>$bodega,'refaccionid'=>$refaccionid,'serie'=>$serie));
                $this->ModeloCatalogos->updateCatalogo('series_compra_refacciones',array('factura'=>NULL,'serie'=>'','bodega'=>0,'status'=>0),array('serieId'=>$serieid));
            }else{
                $this->ModeloCatalogos->getdeletewheren('series_compra_refacciones',array('serieId'=>$serieid));
            }
        }
    }
    function delete_combra(){
        $data = $this->input->post();
        $compra     = $data['compra'];
        //=================================================================
            $result=$this->ModeloCatalogos->getselectwheren('series_compra_productos',array('compraId'=>$compra));
                    
            foreach ($result->result() as $item) {
                $serieid=$item->serieId;

                $productoid=$item->productoid;
                $serie=$item->serie;
                $bodega=$item->bodega;
                
                //$this->ModeloCatalogos->getdeletewheren('series_productos',array('bodegaId'=>$bodega,'productoid'=>$productoid,'serie'=>$serie));
                //$this->ModeloCatalogos->updateCatalogo('series_compra_productos',array('factura'=>NULL,'serie'=>'','bodega'=>0,'status'=>0),array('serieId'=>$serieid));
            }
                
        //========================================================
            $result=$this->ModeloCatalogos->getselectwheren('series_compra_accesorios',array('compraId'=>$compra));
            foreach ($result->result() as $item) {
                $serieid=$item->serieId;
                $accesoriosid=$item->accesoriosid;
                $serie=$item->serie;
                $bodega=$item->bodega;

                //$this->ModeloCatalogos->getdeletewheren('series_accesorios',array('bodegaId'=>$bodega,'accesorioid'=>$accesoriosid,'serie'=>$serie));
                //$this->ModeloCatalogos->updateCatalogo('series_compra_accesorios',array('factura'=>NULL,'serie'=>'','bodega'=>0,'status'=>0),array('serieId'=>$serieid));
            }
        //================================================================
            $result=$this->ModeloCatalogos->getselectwheren('series_compra_refacciones',array('compraId'=>$compra));
            foreach ($result->result() as $item) {
                $serieid=$item->serieId;
                $refaccionid=$item->refaccionid;
                $serie=$item->serie;
                $bodega=$item->bodega;
                $con_serie=$item->con_serie;
                $entraron=$item->entraron;
                if ($con_serie==1) {
                    //$this->ModeloCatalogos->getdeletewheren('series_refacciones',array('bodegaId'=>$bodega,'refaccionid'=>$refaccionid,'serie'=>$serie));
                }else{
                    //$this->ModeloCatalogos->updatestock3('series_refacciones','cantidad','-',$entraron,'bodegaId',$bodega,'refaccionid',$refaccionid,'con_serie',0);
                }
                
                //$this->ModeloCatalogos->updateCatalogo('series_compra_refacciones',array('factura'=>NULL,'serie'=>'','bodega'=>0,'status'=>0),array('serieId'=>$serieid));
            }
            
        //================================================================ 
            $result=$this->ModeloCatalogos->getselectwheren('compra_consumibles_bitacora',array('compra'=>$compra));
            foreach ($result->result() as $item) {

                $consumible=$item->consumible;
                $bodega=$item->bodega;
                $cantidad=$item->cantidad;

                //$this->ModeloCatalogos->updatestock3('consumibles_bodegas','total','-',$cantidad,'bodegaId',$bodega,'consumiblesId',$consumible,'status',1);
                
                
                
            } 
        //================================================================
            $this->ModeloCatalogos->updateCatalogo('series_compra',array('activo'=>0),array('compraId'=>$compra));
    }
    function Solicitudes(){
        $this->load->view('header');
        $this->load->view('main');
        $this->load->view('series/solicitud');
        $this->load->view('footer');
        $this->load->view('series/solicitudjs');
    }
    public function getDatacomprassol() {
        $params = $this->input->post();
        $serie = $this->Modeloproprioritarios->getsolicitudes($params);
        $totalRecords= $this->Modeloproprioritarios->getsolicitudes_total($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totalRecords),  
            "recordsFiltered" => intval($totalRecords),
            "data"            => $serie->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    function verificarstockpro(){
        $datos = $this->input->post('datos');
        $DATA = json_decode($datos);
        $ventas=array();
        for ($i=0;$i<count($DATA);$i++) {
            $tipo=$DATA[$i]->tipo;
            $pro=$DATA[$i]->pro;
            if($tipo==1){
                //html ='Consumible';
                $strq="select sum(cob.total) as total from consumibles_bodegas as cob where cob.consumiblesId=$pro";
                $query = $this->db->query($strq);
                $totalc=0;
                foreach ($query->result() as $item) {
                    $totalc=$item->total;
                }
                $ventas[]=array('tipo'=>$tipo,'pro'=>$pro,'stock'=>$totalc);
            }
            if($tipo==2){
                $strq="SELECT (select count(*) from series_accesorios as sacc where sacc.accesoriosid=$pro and sacc.status<2 and sacc.con_serie=1) as stock, (select sum(sacc.cantidad) from series_accesorios as sacc where sacc.accesoriosid=$pro and sacc.status<2 and sacc.con_serie=0) as stock2";
                $query = $this->db->query($strq);
                $totala=0;
                foreach ($query->result() as $item) {
                    
                    if($item->stock>0){
                        $totala1=$item->stock;    
                    }else{
                        $totala1=0;
                    }
                    if($item->stock2>0){
                        $totala2=$item->stock2;    
                    }else{
                        $totala2=0;
                    }
                    $totala=$totala1+$totala2;
                }
                $ventas[]=array('tipo'=>$tipo,'pro'=>$pro,'stock'=>$totala);
            }
            if($tipo==3){
                //html ='Equipos';
                $strq="SELECT bod.bodega,count(*) as total FROM series_productos as eqs INNER JOIN bodegas as bod on bod.bodegaId=eqs.bodegaId WHERE eqs.productoid=$pro and eqs.status<=1 and eqs.activo=1";
                $query = $this->db->query($strq);
                $totale=0;
                foreach ($query->result() as $item) {
                    $totale=$item->total;
                }
                $ventas[]=array('tipo'=>$tipo,'pro'=>$pro,'stock'=>$totale);
            }
            if($tipo==4){
                //html ='Refacción';
                $strq="SELECT sum(inr.cantidad) as total from series_refacciones as inr where inr.refaccionid=$pro and inr.activo=1 and inr.status=0";
                $query = $this->db->query($strq);
                $totalr=0;
                foreach ($query->result() as $item) {
                    $totalr=$item->total;
                }
                $ventas[]=array('tipo'=>$tipo,'pro'=>$pro,'stock'=>$totalr);
            }

            
            
        }
        echo json_encode($ventas);
    }
    function delete_item(){
        $params = $this->input->post();
        $id = $params['id'];
        $tipo = $params['tipo'];
        if($tipo==1){
            $this->ModeloCatalogos->getdeletewheren('series_compra_accesorios',array('serieId'=>$id));
        }
        if($tipo==2){
            $this->ModeloCatalogos->getdeletewheren('series_compra_refacciones',array('serieId'=>$id));
        }
        if($tipo==3){
            $this->ModeloCatalogos->getdeletewheren('compra_consumibles',array('cconsumibleId'=>$id));
        }
    }
    function edit_cant_item(){
        $params = $this->input->post();
        $id = $params['id'];
        $tipo = $params['tipo'];
        $cant = $params['cant'];
        if($tipo==1){
            $this->ModeloCatalogos->updateCatalogo('series_compra_accesorios',array('cantidad'=>$cant),array('serieId'=>$id));
        }
        if($tipo==2){
            $this->ModeloCatalogos->updateCatalogo('series_compra_refacciones',array('cantidad'=>$cant),array('serieId'=>$id));
        }
        if($tipo==3){
            $this->ModeloCatalogos->updateCatalogo('compra_consumibles',array('cantidad'=>$cant),array('cconsumibleId'=>$id));
        }
    }
}

?>