var base_url = $('#base_url').val();
$(document).ready(function() {
    $('.dropify').dropify({
        messages: {
            default: 'Arrastra y suelta un archivo aquí o haz clic',
            replace: 'Arrastre y suelte un archivo o haga clic para reemplazar',
            remove:  'Eliminar',
            error:  'Lo sentimos, el archivo es demasiado grande.'
        }
    });

    $('.dropifyunidad').dropify({
        allowedFileExtensions: ["pdf"],
        messages: {
            default: 'Arrastra y suelta un archivo aquí o haz clic pdf',
            replace: 'Arrastre y suelte un archivo o haga clic para reemplazar',
            remove:  'Eliminar',
            error:  'Lo sentimos, el archivo es demasiado grande.'
        }
    });
});
function add_registro(){

    var formulario_polizas = $('#form_data');

    // Validamos el formulario en base a las reglas mencionadas debajo
    formulario_polizas.validate({
            ignore: "",
            rules:{
                modelo: {
                    required: true
                },
            },
            // Para mensajes personalizados
            messages:{
                modelo:{
                    required: "Este campo es obligatorio."
                },
            },
            errorElement : 'div',
            // Si están correctos los campos, procesamos el formulario mediante Ajax
            submitHandler: function (form) 
            {
                
                
            }
        });

        var valid =$('#form_data').valid();
        if (valid) {
            var datos = $('#form_data').serialize();
            $.ajax({
                type:'POST',
                url: base_url+'index.php/Listado_unidades/insertaActualiza',
                data: datos,
                success:function(response){
                    var idr=parseFloat(response);
                    cargaimage(idr);
                    cargaimage_poliza(idr);
                    swal({ title: "Éxito",
                        text: "Se ha registrado de manera correcta",
                        type: 'success',
                        showCancelButton: false,
                        allowOutsideClick: false,
                    });
                    
                    setTimeout(function(){ 
                        window.location.href = base_url+'index.php/Listado_unidades'; 
                    }, 5000);
                }
            });
        }
}


function tipo_unidad_slet(){
    var tipo_unidad=$('#tipo_unidad option:selected').val();
    if(tipo_unidad==1){
        $('.leasing_txt').css('display','none');
    }else if(tipo_unidad==2){
        $('.leasing_txt').css('display','block');  
    }
}

function cargaimage(id){
    if ($('#file')[0].files.length > 0) {
        var inputFileImage = document.getElementById('file');
        var file = inputFileImage.files[0];
        var data = new FormData();
        data.append('foto',file);
        data.append('idunidad',id);
        $.ajax({
            url:base_url+'index.php/Listado_unidades/cargafiles',
            type:'POST',
            contentType:false,
            data:data,
            processData:false,
            cache:false,
            success: function(data) {
                var array = $.parseJSON(data);
                    console.log(array.ok);
                    if (array.ok=true) {
                        //$(".fileinput").fileinput("clear");
                       // toastr.success('Se ha cargado el nuevo documento correctamente','Hecho!');
                        //cargafiles();
                    }else{
                       // toastr.error('Error', data.msg);
                    }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                    var data = JSON.parse(jqXHR.responseText);
                    //toastr.error('Error', data.msg);
                }
        });
    }
}

function cargaimage_poliza(id){
    if ($('#file_poliza')[0].files.length > 0) {
        var inputFileImage = document.getElementById('file_poliza');
        var file = inputFileImage.files[0];
        var data = new FormData();
        data.append('foto',file);
        data.append('idunidad',id);
        $.ajax({
            url:base_url+'index.php/Listado_unidades/cargafiles_poliza',
            type:'POST',
            contentType:false,
            data:data,
            processData:false,
            cache:false,
            success: function(data) {
                var array = $.parseJSON(data);
                    console.log(array.ok);
                    if (array.ok=true) {
                        //$(".fileinput").fileinput("clear");
                       // toastr.success('Se ha cargado el nuevo documento correctamente','Hecho!');
                        //cargafiles();
                    }else{
                       // toastr.error('Error', data.msg);
                    }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                    var data = JSON.parse(jqXHR.responseText);
                    //toastr.error('Error', data.msg);
                }
        });
    }
}