<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Personal extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Personal/ModeloPersonal');
        $this->load->model('ModeloCatalogos');
    }
	public function index(){
            //$data['personal']=$this->ModeloPersonal->getpersonal();
            //carga de vistas
            $this->load->view('templates/header');
            $this->load->view('templates/navbar');
            //$this->load->view('Personal/Personal',$data);
            $data['personal']=$this->ModeloPersonal->getpersonal();
            $this->load->view('Personal/Personal',$data);
            $this->load->view('templates/footer');
            $this->load->view('Personal/jspersonal');

	}
    /**
     * Retorna vista para agregar personal 
     */
    public function Personaladd(){
            //carga de vistas
            $this->load->view('templates/header');
            $this->load->view('templates/navbar');
            $this->load->view('Personal/Personaladd');
            $this->load->view('templates/footer');
            $this->load->view('Personal/jspersonal');
    }    
    public function addpersonal(){
        $id = $this->input->post('id');
        $nom = $this->input->post('nom');
        $sexo = $this->input->post('sexo');
        $mail = $this->input->post('mail');
        $sucu = $this->input->post('sucu');
      
        if ($id>0) {
            $this->ModeloPersonal->personalupdate($id,$nom,$sexo,$mail,$sucu); 
        }else{
            $this->ModeloPersonal->personalnuevo($nom,$sexo,$mail,$sucu); 
        }
    }
    public function deletepersonal(){
        $id = $this->input->post('id');
        $this->ModeloPersonal->personaldelete($id); 
    }   
    
}
