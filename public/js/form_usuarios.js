$(document).ready(function(){
    
    var base_url = $('#base_url').val();

    $('#tabla_equipo tbody').on('click', 'a.eliminar', function () {
      var row=$(this).closest("tr");
      var id = row.find("td").eq(1).html();
      $.ajax({
      type: "POST",
      url: base_url+"Usuarios/eliminar_equipo",
      data:{id:id},
      success: function(data){
      row.remove();
      }
      });
    });  

     $('#tabla_refacciones tbody').on('click', 'a.eliminar', function () {
      var row=$(this).closest("tr");
      var id = row.find("td").eq(1).html();
      $.ajax({
      type: "POST",
      url: base_url+"Usuarios/eliminar_refaccion",
      data:{id:id},
      success: function(data){
      row.remove();
      }
      });
    }); 

    $('#tabla_accesorios tbody').on('click', 'a.eliminar', function () {
      var row=$(this).closest("tr");
      var id = row.find("td").eq(1).html();
      $.ajax({
      type: "POST",
      url: base_url+"Usuarios/eliminar_accesorio",
      data:{id:id},
      success: function(data){
      row.remove();
      }
      });
    }); 

    var formulario_categorias_equipo = $('#categorias_equipos_form');
    var formulario_categorias_refacciones = $('#categorias_refacciones_form');
    var formulario_catalogos_accesorios = $('#catalogos_accesorios_form');

    formulario_categorias_equipo.validate({
            rules: 
            {
                
                nombre: {
                    required: true
                }
            },
            // Para mensajes personalizados
            messages: 
            {
                 
                nombre:{
                    required: "Ingrese un nombre"
                }
            },
        errorElement : 'div',
            // Si están correctos los campos, procesamos el formulario mediante Ajax
        submitHandler: function (form) 
        {
            var datos = formulario_categorias_equipo.serialize();
            var texto=$("#nombre1").val();
            $.ajax({
                type:'POST',
                url: base_url+'Usuarios/insertarCategoriaEquipos',
                data: datos,
                success:function(data)
                {
                    console.log(data);
                    // Si existe el usuario, mostramos el mensaje de entrada y redirigimos
                    if(data>0)
                    {
                        swal({ title: "Éxito",
                            text: "Se insertó el equipo",
                            type: 'success',
                            showCancelButton: false,
                            allowOutsideClick: false,
                        });
                        $('#categorias_equipos_form')[0].reset();
                      setTimeout(function(){
                        location.reload();
                      },1000);
                      

                    }
                    else
                    {
                        swal({ title: "Error",
                            text: "Los datos del son erroneos",
                            type: 'error',
                            showCancelButton: false,
                            allowOutsideClick: false,
                        });
                    }
                }
            }); 
        }
    
    });
    

    formulario_categorias_refacciones.validate({
            rules: 
            {
                
                nombre: {
                    required: true
                }
            },
            // Para mensajes personalizados
            messages: 
            {
                 
                nombre:{
                    required: "Ingrese un nombre"
                }
            },
        errorElement : 'div',
            // Si están correctos los campos, procesamos el formulario mediante Ajax
        submitHandler: function (form) 
        {
            var datos = formulario_categorias_refacciones.serialize();
            var texto=$("#nombre3").val();
            $.ajax({
                type:'POST',
                url: base_url+'Usuarios/insertarCategoriaRefacciones',
                data: datos,
                success:function(data)
                {
                    console.log(data);
                    // Si existe el usuario, mostramos el mensaje de entrada y redirigimos
                    if(data>0)
                    {
                        swal({ title: "Éxito",
                            text: "Se insertó la refación",
                            type: 'success',
                            showCancelButton: false,
                            allowOutsideClick: false,
                        });
                       $('#categorias_refacciones_form')[0].reset();
                       setTimeout(function(){
                        location.reload();
                      },1000);
                    }
                   
                    else
                    {
                        swal({ title: "Error",
                            text: "Los datos del son erroneos",
                            type: 'error',
                            showCancelButton: false,
                            allowOutsideClick: false,
                        });
                    }
                }
            });
            
            
        }
    
    });

    formulario_catalogos_accesorios.validate({
            rules: 
            {
                
                equipo: {
                    required: true
                },
                nombre: {
                    required: true
                },
                no_parte: {
                    required: true
                },
                costo: {
                    required: true
                }

            },
            // Para mensajes personalizados
            messages: 
            {
                 
                equipo:{
                    required: "Ingrese un equipo"
                },
                nombre:{
                    required: "Ingrese un nombre"
                },
                no_parte:{
                    required: "Ingrese un número de parte"
                },
                costo:{
                    required: "Ingrese un costo"
                }
            },
        errorElement : 'div',
            // Si están correctos los campos, procesamos el formulario mediante Ajax
        submitHandler: function (form) 
        {
            var datos = formulario_catalogos_accesorios.serialize();
            $.ajax({
                type:'POST',
                url: base_url+'Usuarios/insertarCatalogoAccesorios',
                data: datos,
                success:function(data)
                {
                    console.log(data);
                    // Si existe el usuario, mostramos el mensaje de entrada y redirigimos
                    if(data>0)
                    {
                        swal({ title: "Éxito",
                            text: "Se insertó la refación",
                            type: 'success',
                            showCancelButton: false,
                            allowOutsideClick: false,
                        });
                        $('#catalogos_accesorios_form')[0].reset();
                        setTimeout(function(){
                        location.reload();
                      },1000);
                    }
                    else
                    {
                        swal({ title: "Error",
                            text: "Los datos del son erroneos",
                            type: 'error',
                            showCancelButton: false,
                            allowOutsideClick: false,
                        });
                    }
                }
            });
            
            
        }
    
    });

});