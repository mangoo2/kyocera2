<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Tickes extends CI_Controller {
    public function __construct()     {
        parent::__construct();
        $this->load->model('ModeloTicket');
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloGeneral');
        $this->load->helper('url');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        if($this->session->userdata('logeado')==true){
            $this->perfilid =$this->session->userdata('perfilid');
            $this->idpersonal = $this->session->userdata('idpersonal');
        }else{
            redirect('login'); 
        }
    }
    function index(){
            $data['idpersonal']=$this->idpersonal;
            $data['perfilid']=$this->perfilid;
            //$data['personalrows']=$this->ModeloCatalogos->getselectwheren('personal',array('estatus'=>1));
            $data['personalrows']=$this->ModeloGeneral->listadopersonal();
            $this->load->view('header');
            $this->load->view('main');
            $this->load->view('tickes/listado',$data);
            $this->load->view('footer');
            $this->load->view('tickes/listadojs');
    }
    public function getlisttickets() {
        $params = $this->input->post();
        $getdata = $this->ModeloTicket->gettickets($params);
        $totaldata= $this->ModeloTicket->total_tickets($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    function Tickesiframe(){
        $this->load->view('tickes/tickesiframe');
    }
    function Tickesiframeresponse($id=0){
        $data['idt']=$id;
        $this->load->view('tickes/tickesiframeresponse',$data);
    }

}
?>
