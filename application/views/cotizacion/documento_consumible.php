	
	<div class="row">
                <table class="centered tablaBordesPrincipal letraTablaImpresionConsumibles" id="tablaRefacciones" style="width: 100%">
                  <thead>
                    <tr>
                        
                        <th class="tablaBordesPrincipal" width="5%">Pieza</th>
                        <th class="tablaBordesPrincipal" width="%%">Modelo</th>
                        <th class="tablaBordesPrincipal" width="5%">N° de Parte</th>
                        <th class="tablaBordesPrincipal" width="5%">Equipo</th>
                        <th class="tablaBordesPrincipal" width="5%">Rendimiento</th>
                        <th class="tablaBordesPrincipal" width="9%">Precio</th>
                        <?php if ($datosCotizacion[0]->propuesta==0) { ?>
                          <!--<th class="tablaBordesPrincipal" width="6%">SubTotal</th>-->
                        <?php } ?>
                        <?php if ($datosCotizacion[0]->propuesta==1) { ?>
                          <!--<th class="tablaBordesPrincipal" width="6%">IVA</th>-->
                          <th class="tablaBordesPrincipal" width="9%">Total</th>
                        <?php } ?>
                    </tr>
                  </thead>
                  <tbody >
                    <?php 

                    foreach ($detallesCotizacion_consumibles->result() as $item) { 
                      $GLOBALS["total"]=$GLOBALS["total"]+($item->piezas*$item->precioGeneral);

                      ?>
                      <tr>
                        <td class="tablaBordesPrincipal"  onclick="editarp_consu(<?php echo $item->DT_RowId;?>,<?php echo $item->piezas ?>)">
                          <?php echo $item->piezas ?>   
                        </td>
                        <td class="tablaBordesPrincipal"  ><span class="new badge red badgeaction" onclick="deleteconsumiblev(<?php echo $item->DT_RowId;?>)" >X</span> <?php echo $item->modelo ?></td>
                        <td class="tablaBordesPrincipal"  ><?php echo $item->parte ?></td>
                        <td class="tablaBordesPrincipal"  ><?php echo $item->equipo ?></td>
                        <td class="tablaBordesPrincipal"  ><?php 
                          if ($item->tipo==5) {
                            echo $item->rendimiento_unidad_imagen;
                          }else{
                            echo $item->rendimiento_toner;
                          }

                         ?></td>
                        <td class="tablaBordesPrincipal"  onclick="editar_cons(<?php echo $item->DT_RowId;?>,<?php echo $item->precioGeneral;?>,<?php echo $item->piezas ?>)">$<?php echo number_format($item->precioGeneral,2,'.',','); ?></td>
                        <?php if ($datosCotizacion[0]->propuesta==0) { ?>
                        <!--<td class="tablaBordesPrincipal"  >$<?php echo number_format($item->totalGeneral,2,'.',','); ?></td>-->
                        <?php } ?>
                        <?php if ($datosCotizacion[0]->propuesta==1) { ?>
                          <!--<td class="tablaBordesPrincipal">$<?php echo number_format(($item->totalGeneral*0.16),2,'.',','); ?></td>-->
                          <td class="tablaBordesPrincipal">$<?php echo number_format($item->piezas*$item->precioGeneral,2,'.',','); ?></td>
                        <?php } ?>
                      </tr>
                    <?php } ?>
                    
                  </tbody>
                </table>
        
            </div>
            <script type="text/javascript" src="<?php echo base_url(); ?>public/js/cotizacionconsumibles2.js?v=<?php echo date('YmdGi');?>" ></script>