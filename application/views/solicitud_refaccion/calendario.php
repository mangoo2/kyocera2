<style type="text/css">
  .chosen-container{
    margin-left: 0px;
  }
  .rowmarginbotom{
    margin-bottom: 5px;
  }
  .textcolorred{
    color:red;
  }
  #tablerentapoliza td{
    padding: 0px;
  }
  .texcolorred{
    color:red;
  }
  .texcolorblue{
    color:blue;
  }
  .texcolorgrenn{
    color:green;
  }
  .texcoloryellow{
    color:#fdc363;
  }
</style>
<!-- START CONTENT -->
<section id="content">
  <!--start container-->
  <div class="container">
    <div class="section">
      <div class="card-panel">
          <div id="table-datatables">
            <h4 class="header">Calendario</h4>
            <div class="row">
              <div class="col s2">
              </div>
              <div class="col s2">
                <label>Mes</label>
                <select class="browser-default  chosen-select" id="nuevomesview" onchange="cambiarfecha()">
                  <option value="<?php echo date('Y-m-d');?>">Fecha actual</option>
                  <option value="<?php echo date('Y').'-01-01';?>">Enero</option>
                  <option value="<?php echo date('Y').'-02-01';?>">Febrero</option>
                  <option value="<?php echo date('Y').'-03-01';?>">Marzo</option>
                  <option value="<?php echo date('Y').'-04-01';?>">Abril</option>
                  <option value="<?php echo date('Y').'-05-01';?>">Mayo</option>
                  <option value="<?php echo date('Y').'-06-01';?>">Junio</option>
                  <option value="<?php echo date('Y').'-07-01';?>">Julio</option>
                  <option value="<?php echo date('Y').'-08-01';?>">Agosto</option>
                  <option value="<?php echo date('Y').'-09-01';?>">Septiembre</option>
                  <option value="<?php echo date('Y').'-10-01';?>">Octubre</option>
                  <option value="<?php echo date('Y').'-11-01';?>">Noviembre</option>
                  <option value="<?php echo date('Y').'-12-01';?>">Diciembre</option>
                </select>
              </div>
              <div class="col s2">
                <label>Servicio</label>
                <select class="browser-default " id="servicioid" >
                  
                </select>
              </div>
              <div class="col s2">
                <label>Tecnico</label>
                <select class="browser-default chosen-select" id="tecnicoid" onchange="updatecalendar()">
                  <option value="0">Seleccione una opción</option>
                  <?php 
                    foreach ($resultstec->result() as $item) { ?>
                      <option value="<?php echo $item->personalId;?>"><?php echo $item->tecnico;?></option>
                  <?php }  ?>
                </select>
              </div>
              <div class="col s2">
                <label>Cliente</label>
                <select class="browser-default  " id="clienteid" >
                  
                </select>
              </div>
              <div class="col s2">
                <label>Ciudad</label>
                <select class="browser-default  chosen-select" onchange="updatecalendar()">
                  
                </select>
              </div>
            </div>
            <div class="row" id="calendario">
              <br>
            </div>
            <?php 
              $fila=3;
              while ($fila <= 2){
            ?>
            <div class="row">
              <!--id="calendario"--> 
              <div class="col s12" >
                <?php 
                  $lugar = 15;
                  $dial=1;
                while ($lugar <= 14){
                ?>
                <div class="card cardinfo" style="width: 16.1%;float: left;margin-left: 5px; font-size: 10px">
                  <div class="card-content cardinfo" style="background: #ded5d5b8; padding: 10px; padding-top: 0px;">
                    <div class="row">
                      <div class="col s6" style="border: solid 1px;padding: 4px;font-size: 12px;">
                        <?php   
                            switch ($dial) {
                              case 1:
                                echo 'Lunes';
                                break;
                              case 2:
                                echo 'Martes';
                                break;
                              case 3:
                                echo 'Miercoles';
                                break;
                              case 4:
                                echo 'Jueves';
                                break;
                              case 5:
                                echo 'Viernes';
                                break;
                              case 6:
                                echo 'Sabado';
                                break;
                                    
                              default:
                                # code...
                                break;
                            }
                        ?>
                      </div>
                      <div class="col s2">
                        
                      </div>
                      <div class="col s4">
                        <a class="btn-floating waves-effect waves-light red accent-2" style="text-align: center;">
                          <?php echo $lugar;?>
                        </a>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col s6">
                        10:00 - 12:00
                      </div>
                      <div class="col s6 texcolorred">
                        PUEBLA FC
                      </div>
                    </div>
                    <div class="row">
                      <div class="col s6 ">
                        12:00 - 01:00
                      </div>
                      <div class="col s6 texcolorblue">
                        SISTENSIS
                      </div>
                    </div>
                    <div class="row">
                      <div class="col s6">
                        01:00 - 02:00
                      </div>
                      <div class="col s6 texcoloryellow">
                        ECODYNEUET
                      </div>
                    </div>
                    <div class="row">
                      <div class="col s6">
                        03:00 - 04:00
                      </div>
                      <div class="col s6 texcolorgrenn">
                        SERVIMED
                      </div>
                    </div>
                    <div class="row">
                      <div class="col s6">
                        05:00 - 06:00
                      </div>
                      <div class="col s6 texcolorgrenn">
                        VEROSTAMP
                      </div>
                    </div>
                    <div class="row">
                      <div class="col s6">
                        07:00 - 08:00
                      </div>
                      <div class="col s6 texcoloryellow">
                        SER. FABRICAS
                      </div>
                    </div>

                  </div>
                </div>
                <?php 
                $dial++;
                $lugar++;
                }
                ?>
              </div>
            </div>
            <?php
              $fila++; 
              }
            ?>
          </div>
      </div>
    </div>  
  </div>
</section>
<div id="modalinfocalendario" class="modal"  >
        <div class="modal-content">
          <h5 >Calendario</h5>
          <br>
          <div class="row">
            <div class="col s12 m6 6">
              <div class="row rowmarginbotom">
                  <div class="col s6 textcolorred">
                    Cliente:
                  </div>
                  <div class="col s12 m6 6 datoscliente">
                    
                  </div>
              </div>
              <div class="row rowmarginbotom">
                  <div class="col s6 textcolorred">
                    Tipo de Venta:
                  </div>
                  <div class="col s12 m6 6 datosclientetipo">
                    
                  </div>
              </div>
              <div class="row rowmarginbotom">
                  <div class="col s6 textcolorred">
                    Tipo de Servicio:
                  </div>
                  <div class="col s12 m6 6 datosclienteservicio">
                    
                  </div>
              </div>
              <div class="row rowmarginbotom">
                  <div class="col s6 textcolorred">
                    Zona:
                  </div>
                  <div class="col s12 m6 6 datosclientezona">
                    NORTE/SUR
                  </div>
              </div>
              <div class="row rowmarginbotom">
                  <div class="col s6 textcolorred">
                    Ciudad:
                  </div>
                  <div class="col s12 m6 6">
                    PUEBLA
                  </div>
              </div>
              <div class="row rowmarginbotom">
                  <div class="col s6 textcolorred">
                    Técnico:
                  </div>
                  <div class="col s12 m6 6 datosclientetecnico">
                    
                  </div>
              </div>
              <div class="row rowmarginbotom">
                  <div class="col s6 textcolorred">
                    Prioridad:
                  </div>
                  <div class="col s12 m6 6 datosclienteprioridad">
                    
                  </div>
              </div>
              
            </div>
            <div class="col s12 m6 6">
              <div class="row rowmarginbotom textcolorred datosclienteasignaciones">
                Asignaciones Mensuales / Asignaciones diarias
              </div>
              <div class="row rowmarginbotom datosclienteahorario">
                Dia y hora: Lunes / 10:00-12:00
              </div>
              <div class="row rowmarginbotom">
                <div class="col s12">
                  <h4 class="datosclienteservicio"></h4>
                </div>
                <table class="table striped" id="tablerentapoliza">
                  <thead>
                    <tr>
                      <td>Modelo</td>
                      <td>Serie</td>
                    </tr>
                  </thead>
                  <tbody class="tablerentapoliza">
                    
                  </tbody>
                  
                </table>
              </div>
            </div>
          </div>
          <br>
        <div class="modal-footer">
          <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
        </div>
      </div>
    </div>