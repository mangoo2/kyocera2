<style type="text/css">
.imgpro{
width: 100px;
}
.imgpro:hover {
transform: scale(2.9);
filter: drop-shadow(5px 9px 12px #444);
}
.btn_img{
cursor: pointer;
}
#tabla_consumibles td{
font-size: 12px;
}
<?php if($this->perfilid!=1){ ?>
.soloadministradores{display: none;}
<?php } ?>
.card_img{width: 200px;float: left;border: solid #dddddd;}
.card_img img{width: 100%;/*max-height: 200px;*/  }
.card_img_c{height: 200px;}
.card_img_a{text-align: center;}

.star {visibility:hidden;font-size:30px;cursor:pointer;margin-top: 15px;}
.star:before {content: "\2605";position: absolute;visibility:visible;}
.star:checked:before {content: "\2605";position: absolute;color: #ffeb3b;}
.star_f {visibility:hidden;font-size:30px;cursor:pointer;margin-top: 5px;}
.star_f:before {content: "\2605";position: absolute;visibility:visible;margin-top: -19px;}
.star_f:checked:before {content: "\2605";position: absolute;color: #ffeb3b;margin-top: -19px;}
[type="checkbox"]:not(:checked), [type="checkbox"]:checked {opacity: unset;pointer-events: all;}
</style>
<!-- START CONTENT -->
<section id="content">
  <!--start container-->
  <div class="container">
    <div class="section">
      <input id="base_url" type="hidden" name="base_url" value="<?php echo base_url(); ?>">
      <div id="table-datatables">
        <br>
        <h4 class="header">Listado de Consumibles</h4>
        <div class="row">
          
          <div class="col s12 soloadministradores">
            <a class="btn waves-effect waves-light gradient-45deg-green-teal gradient-shadow col s1" href="<?php echo base_url(); ?>index.php/consumibles/alta">Nuevo</a>
            <div class="col s2"></div>
            <div class="form-group col s6"  align="right">
              <form id="archivo-form" name="archivo-form">
                <!--
                En el input file si pones accept=".xxx" te mostrará predefinidamente los archivos de la extensión seleccionada, sin embargo si el usuario selecciona "Todos los archivos" dentro del explorador de archivos podrá visualizar los demás, es por eso que debe existir una validación de la extensión antes de subirlo, la cual hacemos con Jquery.
                -->
                <input type="file" class="input-field" id="inputFile" name="inputFile" accept=".xlsx">
                <button type="submit" class="btn btn-primary" disabled id="uploadButton" name="uploadButton" >Guardar</button>
              </form>
            </div>
            <div class="col s3" style="text-align: center; visibility: hidden; margin-top: -20px; " name="loading" id="loading" align="right">
              <div class="progress">
                <div class="indeterminate"></div>
              </div>
            </div>
          </div>
          <div class="col s12">
            <table id="tabla_consumibles" class="table display" cellspacing="0">
              <thead>
                <tr>
                  <th>#</th>
                  <!--
                  <th>Foto</th>
                  -->
                  <th>Modelo</th>
                  <th>Parte</th>
                  <!-- <th>Stock</th> -->
                  <th>Observaciones</th>
                  <th>Equipos Compatibles</th>
                  <th>Destacados</th>
                  <th>Acciones</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="modal fade text-left" id="modaldoc" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
  <div class="modal-dialog modal-lx" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel1">Documentos</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-3">
            <img src="<?php echo base_url(); ?>uploads/consumibles/r_-190125-115608-cro1.PNG" style="height:50px;width:50px;">
          </div>
          <div class="col-md-9 filescargados" style="overflow: auto; max-height: 500;">
            
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn  btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn  btn-success" data-dismiss="modal" id="subirdoc">Aceptar</button>
      </div>
    </div>
  </div>
</div>
<div id="modalinfoequipo" class="modal">
  <div class="modal-content ">
    <h4 class="equipo_modelo">equipo_modelo</h4>
    
    <div class="row">
      <div class="col s12">
        <ul class="tabs">
          <li class="tab col s3"><a class="active" href="#test1">Imagenes</a></li>
          <li class="tab col s3"><a  href="#test2">Caracteristicas</a></li>
        </ul>
      </div>
      <div id="test1" class="">
        <div class="col s12">
          <input type="file" id="files" name="files[]" multiple accept="image/*">
        </div>
        <div class="col s12 galeriimg" style="margin-top: 10px;"></div>
      </div>
      <div id="test2">
        <div >
          <form id="form_catacteristicas">
            <input type="hidden" name="id" id="idfc">
            <input type="hidden" name="idconsumible" id="idequipofc">
            <div class="col s2">
              <label>Tipo</label>
              <input type="text" name="name" id="name" class="form-control-bmz">
            </div>
            <div class="col s6">
              <label>Descripcion</label>
              <textarea name="descripcion" id="descripcion" class="form-control-bmz" style="min-height: 70px;"></textarea>
            </div>
            <div class="col s2">
              <input type="checkbox" class="filled-in" id="general" name="general" value="1" style="display: none;">
              <label for="general" style="margin-top: 21px;">General</label>
            </div>
            <div class="col s1">
              <label>Orden</label>
              <input type="number" name="orden" id="orden" class="form-control-bmz">
            </div>
            <div class="col s1">
              <a class="btn-bmz waves-effect waves-light gradient-45deg-green-teal gradient-shadow" id="save_fc" style="margin-top: 21px;">Guardar</a>
              <a class="btn-bmz waves-effect waves-light gradient-45deg-red-teal gradient-shadow" id="cancelar_fc" onclick="fc_limpiar()" style="display: none;">Cancelar</a>
            </div>
          </form>
        </div>
        <div class="lis_catacter">
          
        </div>
      </div>
    </div>
  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
  </div>
</div>