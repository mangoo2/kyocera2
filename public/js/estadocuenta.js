var base_url=$('#base_url').val();
var num_pres_selected=0;
$(document).ready(function($) {
	$('#idcliente').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        
        placeholder: 'Buscar una cliente',
        allowClear: true,
        ajax: {
            url: base_url+'Ventasi/searchclientes',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.id,
                        text: element.empresa

                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function (e) {
        var data = e.params.data;
        //obtenerrfc(data.id,0)
        generar();
    });
});
function generar(){
    $('body').loading({theme: 'dark',message: 'Procesando...'});
    setTimeout(function(){ 
        generar0();
    }, 1000);
    
}
function generar0(){
    num_pres_selected=0;
    $('.preview_iframe').html('');
    var idcliente=$('#idcliente option:selected').val();
    var view=$('#mostrar_todo').is(':checked')==true?1:0;
    var eje = $('#ejecutivoselect option:selected').val();
     $.ajax({
        type:'POST',
        url: base_url+'Estadocuenta/generar',
        data: {
            idcliente: idcliente,
            tipo:view,
            eje:eje
        },
        async: false,
        statusCode:{
            404: function(data){
                swal("Error", "404", "error");
                $('body').loading('stop'); 
            },
            500: function(){
                swal("Error", "500", "error"); 
                $('body').loading('stop'); 
            }
        },
        success:function(data){
            $('body').loading('stop'); 
            var array = $.parseJSON(data);
            console.log(array);
            $('.date-rentas').html(array.renta);
            $('.table_ventas').html(array.ventas);
            $('.table_polizas').html(array.poliza);
            $('.table_tb_fac_sin').html(array.facturas);
            $('#codigoenvio').val(array.codigo);
            /*
            $.each(array, function(index, item) {
                    if(parseInt(item.tipov)==parseInt(tipovselected)){

                    }else{
                        $('.bodega_'+item.bodegaId).attr('disabled',true);
                    }
                
            });
            */
        },
        error: function(response){
            notificacion(1);
            $('body').loading('stop'); 
        }
    });
}
function edit_factura_prefactura(id,tipo){
	$.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Nueva Descripción',
        content: '<textarea class="form-control-bmz" id="descripcion"></textarea>',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var des = $('#descripcion').val();
                     $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Estadocuenta/editardesfactura",
                        data: {
                            id:id,
                            tipo:tipo,
                            des:des
                        },
                        success: function (data){
                        	var array = $.parseJSON(data);

                            $('.des_'+id+'_'+tipo).html(array.des);
                        },
                        error: function(response){
                            notificacion(1);
                        }
                    });
                    
                
            },
            cancelar: function (){
                
            }
        }
    });
}
function all_check_rentas(){
	var checked=$('#factura_0').is(':checked');
    $( ".facturas_r_checked" ).prop( "checked", checked );
}
function all_check_ventas(){
    var checked=$('#factura_v_0').is(':checked');
    $( ".facturas_vc_checked" ).prop( "checked", checked );
    $( ".facturas_v_checked" ).prop( "checked", checked );
}
function all_check_poliza(){
    var checked=$('#factura_v_2').is(':checked');
    $( ".facturas_p_checked" ).prop( "checked", checked );
}
function generarestado(){
    var codi = $('#codigoenvio').val();
    var idcliente=$('#idcliente option:selected').val();
        num_pres_selected=0;
    //==============================================================
        var rentaslis = $("#date-rentas tbody > tr");
        var DATAr  = [];
            
        rentaslis.each(function(){  
            if ($(this).find("input[class*='facturas_r_checked']").is(':checked')) {
                num_pres_selected++;
                item = {};                    
                item ["per"]  = $(this).find("input[class*='facturas_r_checked']").data('value');
                item ["fac"]  = $(this).find("input[class*='facturas_r_checked']").data('factu');
                DATAr.push(item);
            }       
            
        });
        aInfor   = JSON.stringify(DATAr);
    //==============================================================
        var ventasslis = $("#table_ventas tbody > tr");
        var DATAv  = [];
        var DATAvc  = [];
            
        ventasslis.each(function(){  
            if ($(this).find("input[class*='facturas_v_checked']").is(':checked')) {
                //var tipovc=$(this).find("input[class*='facturas_v_checked']").data('tipovc');
                    num_pres_selected++;
                    item = {};                    
                    item ["ve"]  = $(this).find("input[class*='facturas_v_checked']").val();
                    DATAv.push(item);  
            }
            if ($(this).find("input[class*='facturas_vc_checked']").is(':checked')) {
                    num_pres_selected++;
                    item = {};                    
                    item ["ve"]  = $(this).find("input[class*='facturas_vc_checked']").val();
                    DATAvc.push(item);
            }  
        });
        aInfov   = JSON.stringify(DATAv);
        aInfovc   = JSON.stringify(DATAvc);
    //==============================================================
        var polizalis = $("#table_polizas tbody > tr");
        var DATAp  = [];
            
        polizalis.each(function(){  
            if ($(this).find("input[class*='facturas_p_checked']").is(':checked')) {
                    num_pres_selected++;
                    item = {};                    
                    item ["pol"]  = $(this).find("input[class*='facturas_p_checked']").val();
                    DATAp.push(item);
            }
        });
        aInfop   = JSON.stringify(DATAp);
    //==============================================================
        var faclis = $("#table_fac_sin tbody > tr");
        var DATAfsv  = [];
            
        faclis.each(function(){  
            if ($(this).find("input[class*='facturas_fac_sv_checked']").is(':checked')) {
                    num_pres_selected++;
                    item = {};                    
                    item ["fac"]  = $(this).find("input[class*='facturas_fac_sv_checked']").val();
                    DATAfsv.push(item);
            }
        });
        aInfofsv   = JSON.stringify(DATAfsv);
    //==============================================================

        if(num_pres_selected>0){
            datos='rentas='+aInfor+'&ventas='+aInfov+'&ventasc='+aInfovc+'&polizas='+aInfop+'&cliente='+idcliente+'&facsv='+aInfofsv+'&codi='+codi;
            
            //obtenerinfofiscal();
            setTimeout(function(){ 
                var urlfac=base_url+"index.php/Estadocuenta/reporte?"+datos;
                var htmliframe="<iframe src='"+urlfac+"' title='description' class='ifrafac'>";
                $('.preview_iframe').html(htmliframe);
                //window.location.href = base_url+"index.php/Preview/factura?"+datos; 
            }, 1000);
        }else{
            toastr["error"]("Seleccione por lo menos uno");
        }
}
//==========================envio
function enviarestado(){
    if(num_pres_selected>0){
        //$('#firma_m_s').val(54);
        var codi = $('#codigoenvio').val();
        var cliente=$('#idcliente option:selected').val();
        m_asi=codi;
        //m_tipo=tipo;
        m_cli=cliente;
        obtenercontacto(cliente);
        //obtenerbodymail(cliente);
        $('.input_envio_c').html('');
        $('.table_mcr_files_c').html('');
        //$('.input_envio_s').html('<input type="file" id="files" name="files[]" multiple accept="*">');
        //$('.input_envio_s').html('<iframe id="ifracotiza" title="Cotizacion" src="'+base_url+'index.php/Cotizaciones/pdf/'+asi+'"></iframe>');
        $('.table_mcr_files_s').html('<table class="table bordered striped" id="table_mcr_files"><tbody class="tabbodycr_files"></tbody></table>');
        $('.addbtnservicio').html('<button type="button" class="waves-effect btn-bmz  red btn-envioser" onclick="envio_s('+cliente+','+codi+')">Enviar</button>');
        //inputfileadd();
        $('#ms_asucto2').val('Estado de cuenta ');
        $('#modal_servicio').modal();
        $('#modal_servicio').modal('open');
    }else{
        toastr["error"]("Generar el estado de cuenta antes del envió");
    }
}
function obtenercontacto(cliente){
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Calendario/obtenercontactos",
        data: {
            cliente:cliente
        },
        success: function (response){
            $('.m_s_contac').html('<select class="browser-default form-control-bmz" id="ms_contacto">'+response+'</select>');
                
        },
        error: function(response){
            notificacion(1);   
             
        }
    });

    //=====================================================================
    
}
function envio_s(cliente,codi){
    var email =$('#ms_contacto option:selected').data('email');
    $.ajax({
        type:'POST',
        url: base_url+'index.php/Mailenvio/envio_estadocuenta',
        data: {
            cliente:cliente,
            codi:codi,
            email:email,
            per:$('#firma_m_s option:selected').val(),
            perbbc:$('#bbc_m_s option:selected').val(),
            asunto:$('#ms_asucto2').val(),
            comen:$('#ms_coment2').val()
        },
        success:function(data){
            console.log(data);
            var array = $.parseJSON(data);
            if(array.envio==1){
                toastr["success"]("Correo enviado");
                $('#modal_servicio').modal('close');
                $('#ms_coment2').val('');
            }else{
                toastr["error"]("No se pudo enviar el correo");
            }
        },
        error: function(response){
            notificacion(1);   
             
        }
    });
}
function reporte_excedentes(periodo,cli){
    window.open(base_url+'index.php/Estadocuenta/reporte_excedente/'+periodo+'/'+cli, '_blank');
}