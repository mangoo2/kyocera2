<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class NewFolio extends CI_Controller {
    public function __construct()     {
        parent::__construct();
        $this->load->model('ModeloCatalogos');
        $this->load->model('Rentas_model');
        $this->load->helper('url');
        $this->idpersonal = $this->session->userdata('idpersonal');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d');
        $this->fechahoylarga = date('Y-m-d G:i:s');
        if($this->session->userdata('logeado')==true){

        }else{
            redirect('login');
        }
    }

    // Listado de Empleados
    function index()     {
        //$data['get_bodegas']=$this->ModeloCatalogos->getselectwheren('bodegas',array('activo'=>1));
        $data['get_bodegas']=$this->ModeloCatalogos->view_session_bodegas();
        $this->load->view('header');
        $this->load->view('main',$data);
        $this->load->view('almacen/contratofolio/newfolio');
        $this->load->view('almacen/contratofolio/newfoliojs');
        $this->load->view('footer');
        $this->load->view('info_m_servisios');
    }
    function contratos(){
        $id = $this->input->post('id');
        $contratos = $this->Rentas_model->contratorentacliente($id);
        $html='';
            $html.='<option value="0">Seleccione</option>';
        foreach ($contratos as $item) {
                $fechainicio=$item->fechainicio;
            
                if($item->vigencia==1){
                    $vigencia=12;
                }elseif ($item->vigencia==2) {
                    $vigencia=24;
                }elseif($item->vigencia==3){
                    $vigencia=36;
                }else{
                    $vigencia=1;
                }

                $fechainiciof = strtotime ( '+ '.$vigencia.' month' , strtotime ( $fechainicio ) ) ;
                                $fechainiciof = date ( 'Y-m-d' , $fechainiciof );
                if($this->fechahoy>$fechainiciof){
                    $vencido=1;
                    //log_message('error','contrato '.$item->idcontrato.'/ $vencido1 ('.$vencido.')');
                    //log_message('error','contrato '.$item->idcontrato.'/ $this->fechahoy ('.$this->fechahoy.')<$item->conti_cont_fecha ('.$item->conti_cont_fecha.')');
                    if($item->continuarcontadores==1){
                        if(strtotime($this->fechahoy)<strtotime($item->conti_cont_fecha) ){
                            $vencido=0;
                            //log_message('error','contrato '.$item->idcontrato.'/ $vencido2 ('.$vencido.')');
                        }
                    }
                    
                }else{
                    $vencido=0;
                    //log_message('error','contrato '.$item->idcontrato.'/ $vencido3 ('.$vencido.')');
                }

                $rentacolor=$item->rentacolor;
                if($item->precio_c_e_color>0){
                    $rentacolor=$item->precio_c_e_color;
                }
                if($item->permt_continuar==1){
                    $vencido=0;
                }
                //log_message('error','contrato '.$item->idcontrato.'/ $vencido ('.$vencido.')');
            $html.='<option value="'.$item->idcontrato.'" 
                    data-estatus="'.$item->estatus.'" 
                    data-personal="'.$item->personald.' '.$item->apellido_paterno.'" 
                    data-fechainicio="'.$fechainicio.'" 
                    data-fechafinal="'.$fechainiciof.'" 
                    data-vencido="'.$vencido.'"
                    data-rentadeposito="'.$item->rentadeposito.'"
                    data-rentacolor="'.$rentacolor.'"
                    data-fechahoy="'.$this->fechahoy.'"
                    data-continuarcontadores="'.$item->continuarcontadores.'"
                    data-conticont_fecha="'.$item->conti_cont_fecha.'"
                    data-idrenta="'.$item->idRenta.'"
                    >'.$item->idcontrato.' '.date("d-m-Y",strtotime($item->fechainicio)).' '.$item->folio.'</option>';
        }
        echo $html;
    }
    function contratos2(){
        $id = $this->input->post('id');
        $contratos = $this->Rentas_model->contratorentacliente($id);
        $html='';
            $html.='<option value="0">Todos</option>';
        foreach ($contratos as $item) {
                $fechainicio=$item->fechainicio;
            
                if($item->vigencia==1){
                    $vigencia=12;
                }elseif ($item->vigencia==2) {
                    $vigencia=24;
                }elseif($item->vigencia==3){
                    $vigencia=36;
                }else{
                    $vigencia=1;
                }

                $fechainiciof = strtotime ( '+ '.$vigencia.' month' , strtotime ( $fechainicio ) ) ;
                                $fechainiciof = date ( 'Y-m-d' , $fechainiciof );
                if($this->fechahoy>$fechainiciof){
                    $vencido=1;
                }else{
                    $vencido=0;
                }

                $rentacolor=$item->rentacolor;
                if($item->precio_c_e_color>0){
                    $rentacolor=$item->precio_c_e_color;
                }

            $html.='<option value="'.$item->idcontrato.'" 
                    data-estatus="'.$item->estatus.'" 
                    data-personal="'.$item->personald.' '.$item->apellido_paterno.'" 
                    data-fechainicio="'.$fechainicio.'" 
                    data-fechafinal="'.$fechainiciof.'" 
                    data-vencido="'.$vencido.'"
                    data-rentadeposito="'.$item->rentadeposito.'"
                    data-rentacolor="'.$rentacolor.'"
                    data-fechahoy="'.$this->fechahoy.'"
                    >'.$item->idcontrato.' '.$item->folio.'</option>';
        }
        echo $html;
    }
    function cargaequipos(){
        $id = $this->input->post('contrato');
        //$equipos = $this->ModeloCatalogos->equiposcontrato($id);
        $equipos = $this->Rentas_model->equiposrentas($id);
        $html='';
        $html.='<option value="0" data-equipo="0">Seleccione</option>';
        foreach ($equipos as $item) {
            $html.='<option value="'.$item->id.'" data-equipo="'.$item->idEquipo.'" data-serieId="'.$item->serieId.'" data-excedentecolor="'.$item->precio_c_e_color.'" data-serie="'.$item->serie.'">'.$item->modelo.' '.$item->serie.'</option>';
        }

        $lisservicios = $this->Rentas_model->servicioscontrato($id,$this->fechahoy);
        $html2='';
        //if ($lisservicios->num_rows()==0) {
            $html2.='<option value="0">Sin servicio</option>';
            $html2.='<option value="1">Mensajeria</option>';
        //}
        foreach ($lisservicios->result() as $item) {
            $html2.='<option value="'.$item->asignacionId.'">'.$item->fecha.' '.$item->hora.'</option>';
        }
        $datos=array(
                    'equipos'=>$html,
                    'servicio'=>$html2
                    );
        echo json_encode($datos) ;
    }
    function cargaconsumibles(){
        $id = $this->input->post('id');
        $bod = $this->input->post('bod');
        $consumibles = $this->ModeloCatalogos->consumiblesequipos($id,$bod);
        $html='';
        $html.='<option value="0" data-equipois="0">Seleccione</option>';
        foreach ($consumibles->result() as $item) {
            
            if ($item->stock>0) {
                $stock=$item->stock;
            }else{
                $stock=0;
            }
            if ($item->stock_resg>0) {
                $stock_resg=$item->stock_resg;
            }else{
                $stock_resg=0;
            }
            $stock=$stock-$stock_resg;

            $html.='<option value="'.$item->idconsumible.'" data-equipois="'.$id.'" data-tipo="'.$item->tipo.'" class="datatipo_'.$item->tipo.'" data-stock="'.$stock.'">'.$item->modelo.' ('.$stock.')</option>';
        }
        echo $html;

    }
    function addconsumibles(){
        $data = $this->input->post();
        $consumibles = $data['consumibles'];
        $DATAc = json_decode($consumibles);  
        $contratoview=0;     
        $idventa=0;
        for ($i=0;$i<count($DATAc);$i++) {
            $cliente=$DATAc[$i]->cliente;
            $serie=$DATAc[$i]->serie;//idserie
            $contrato=$DATAc[$i]->contrato;
            $servicio=$DATAc[$i]->servicio;
            $consumible=$DATAc[$i]->consumible;
            $consumiblett=$DATAc[$i]->consumiblett;
            
            if(isset($DATAc[$i]->fecha)){
                $fecha=$DATAc[$i]->fecha;
            }else{
                $fecha=$this->fechahoy;
            }
            
            
            $bodega=$DATAc[$i]->bodega;
            $equipo=$DATAc[$i]->equipo;//idrow_equipo
            $equipom=$DATAc[$i]->equipom;//modelo del equipo
            $resultc=$this->ModeloCatalogos->getselectwheren('contrato',array('idcontrato'=>$contrato));
            $idRenta=0;
            foreach ($resultc->result() as $itemc) {
                $idRenta=$itemc->idRenta;
            }
            //================================
                $aux = 0;
                $contr_folio = $this->ModeloCatalogos->contratofolio($cliente);
                foreach ($contr_folio as $itemc) {
                    $aux = $itemc->folio;
                }//log_message('error','aux:'.$aux);
                $ft_result=$this->Rentas_model->generadorfolio($aux,$idRenta);
                $foliotext=$ft_result['foliotext'];
                $folio=$ft_result['folio'];

            //================================
                if($servicio<=1){
                    $servicio_id=0;
                }else{
                    $servicio_id=$servicio;
                }
                $datainserconsu=array(
                                        'idrentas'=>$idRenta,
                                        'idequipo'=>$equipom,
                                        'rowequipo'=>$equipo,
                                        'cantidad'=>1,
                                        'id_consumibles'=>$consumible,
                                        'costo_toner'=>0,
                                        'rendimiento_toner'=>0,
                                        'rendimiento_unidad_imagen'=>0,
                                        'surtir'=>'',
                                        'bodega'=>$bodega

                                    );
                $idrow_consu=$this->ModeloCatalogos->Insert('rentas_has_detallesequipos_consumible',$datainserconsu);





            $datacf = array(
                        'idrenta'=>$idRenta,
                        'idCliente'=>$cliente,
                        'serieId'=>$serie,
                        'idconsumible'=>$consumible,
                        'serviciocId'=>$servicio_id,
                        'folio'=>$folio,
                        'foliotext'=>$foliotext,
                        'status'=>0,
                        'adicional'=>1,
                        'fecha'=>$fecha,
                        'bodegaId'=>$bodega,
                        'creado_personal'=>$this->idpersonal,
                        'idrelacion'=>$idrow_consu
                            );
            $this->ModeloCatalogos->Insert('contrato_folio',$datacf);
            

            if ($contratoview!=$contrato) {
                $contratoview=$contrato;

                $rentasdatos=$this->ModeloCatalogos->getselectwheren('rentas',array('id'=>$idRenta));
                $rentasdatos=$rentasdatos->result();
                if($servicio==1){
                    $servicio_asignacion=1;
                }else{
                    $servicio_asignacion=0;
                }
                $datave=array(
                    'idCliente'=>$cliente,
                    'atencion'=>$rentasdatos[0]->atencion,
                    'correo'=>$rentasdatos[0]->correo,
                    'telefono'=>$rentasdatos[0]->telefono,
                    'estatus'=>1,
                    'idCotizacion'=>$rentasdatos[0]->idCotizacion,
                    'id_personal'=>$this->idpersonal,
                    'servicio'=>$servicio_asignacion
                    );
                $idventa=$this->ModeloCatalogos->Insert('ventas',$datave);

                //==========================================================
                    
                    if(isset($DATAc[$i]->generado)){
                        $generado=$DATAc[$i]->generado;
                        $this->ModeloCatalogos->updateCatalogo('consumibles_solicitud',array('generado'=>$idventa),array('id'=>$generado));
                    }
                //==========================================================
            }
            if ($idventa>0) {
                
                $datavedd = array(
                                'idVentas'=>$idventa,
                                'idEquipo'=>$equipo,//idequiporenta
                                'idserie'=>$serie,//consultar a "alta_rentas_equipos" con el idequipo y la serie
                                'piezas'=>1,
                                'modelo'=>$consumiblett,
                                'rendimiento'=>'',
                                'descripcion'=>'',
                                'precioGeneral'=>0,
                                'totalGeneral'=>0,
                                'idConsumible'=>$consumible,
                                'surtir'=>'',
                                'comentario'=>$foliotext,
                                'foliotext'=>$foliotext,
                                'bodegas'=>$bodega
                            );
                $this->ModeloCatalogos->Insert('ventas_has_detallesConsumibles',$datavedd);
                $this->ModeloCatalogos->updatestock2('consumibles_bodegas','total','-',1,'consumiblesId',$consumible,'bodegaId',$bodega);

                $this->ModeloCatalogos->Insert('bitacora_movimientos',array('entrada_salida'=>0,'descripcion_evento'=>'Solicitud de consumible para rentas: '.$idRenta,'personal'=>$this->idpersonal,'tipo'=>4,'id_item'=>$consumible,'modeloId'=>$consumible,'modelo'=>$consumiblett,'bodega_o'=>$bodega,'clienteId'=>$cliente));
            }
            
        }
    }
    function addserviciotoner(){
        $params = $this->input->post();
        $idfolio=$params['idfolio'];
        $idservicio=$params['idservicio'];
        $this->ModeloCatalogos->updateCatalogo('contrato_folio',array('serviciocId'=>$idservicio),array('idcontratofolio'=>$idfolio));
    }
    function polizas(){
        $id = $this->input->post('id');
        $contratos = $this->Rentas_model->polizascliente($id);
        $html='';
            $html.='<option value="0">Seleccione</option>';
        foreach ($contratos as $item) {
            $html.='<option value="'.$item->id.'" 
                    >'.$item->id.' '.date("d-m-Y",strtotime($item->reg)).'</option>';
        }
        echo $html;
    }
    function cargaequipospoliza(){
        $id = $this->input->post('contrato');
        //$equipos = $this->ModeloCatalogos->equiposcontrato($id);
        //$equipos = $this->Rentas_model->equiposrentas($id);
        $equipos = $this->ModeloCatalogos->polizaventadetalles3($id);
        $html='';
        $html.='<option value="0" data-equipo="0">Seleccione</option>';
        foreach ($equipos->result() as $item) {
            $html.='<option 
                            value="'.$item->id.'" 
                            data-equipo="'.$item->idEquipo.'"
                            data-serieId="'.$item->serie.'"
                            >'.$item->modeloe.' '.$item->serie.'</option>';
        }

        $lisservicios = $this->Rentas_model->serviciospolizas($id,$this->fechahoy);
        $html2='';
        //if ($lisservicios->num_rows()==0) {
            $html2.='<option value="0">Sin servicio</option>';
        //}
        foreach ($lisservicios->result() as $item) {
            $html2.='<option value="'.$item->asignacionId.'">'.$item->fecha.' '.$item->hora.'</option>';
        }
        $datos=array(
                    'equipos'=>$html,
                    'servicio'=>$html2
                    );
        echo json_encode($datos) ;
    }
    function addconsumibles_poliza(){
        $data = $this->input->post();
        $consumibles = $data['consumibles'];
        $DATAc = json_decode($consumibles);  
        $contratoview=0;     
        $idventa=0;
        for ($i=0;$i<count($DATAc);$i++) {
            $cliente=$DATAc[$i]->cliente;
            $serie=$DATAc[$i]->serie;
            $poliza=$DATAc[$i]->contrato;
            $servicio=$DATAc[$i]->servicio;
            $consumible=$DATAc[$i]->consumible;
            $consumiblett=$DATAc[$i]->consumiblett;
            $fecha=$DATAc[$i]->fecha;
            $equipo=$DATAc[$i]->equipo;
            $bodega=$DATAc[$i]->bodega;
            /*
            $resultc=$this->ModeloCatalogos->getselectwheren('contrato',array('idcontrato'=>$contrato));
            $idRenta=0;
            foreach ($resultc->result() as $itemc) {
                $idRenta=$itemc->idRenta;
            }
            */
            //================================

                $aux = 0;
                $contr_folio = $this->ModeloCatalogos->contratofolio($cliente);
                foreach ($contr_folio as $itemc) {
                    $aux = $itemc->folio;
                }
                $folio = $aux +1; 

                $foliotext = '0T'.str_pad($folio, 3, "0", STR_PAD_LEFT); 
                $result_ft=$this->ModeloCatalogos->getselectwheren('contrato_folio',array('foliotext'=>$foliotext));
                if($result_ft->num_rows()>0){
                    $folio+1;
                    $foliotext = '0T'.str_pad($folio, 3, "0", STR_PAD_LEFT); 
                }    
            //================================
            $datacf = array(
                        'idrenta'=>0,
                        'idCliente'=>$cliente,
                        'serieId'=>$serie,
                        'idconsumible'=>$consumible,
                        'serviciocId'=>$servicio,
                        'folio'=>$folio,
                        'foliotext'=>$foliotext,
                        'status'=>0,
                        'adicional'=>1,
                        'fecha'=>$fecha,
                        'serviciocId_tipo'=>1,
                        'bodegaId'=>$bodega,
                        'creado_personal'=>$this->idpersonal
                        );
            $this->ModeloCatalogos->Insert('contrato_folio',$datacf);
            
            
            

                $datave=array(
                    'idCliente'=>$cliente,
                    'atencion'=>'',
                    'correo'=>'',
                    'telefono'=>'',
                    'estatus'=>2,
                    'idCotizacion'=>0,
                    'id_personal'=>$this->idpersonal,
                    'servicio'=>0
                    );
                $idventa=$this->ModeloCatalogos->Insert('ventas',$datave);
            
            
            if ($idventa>0) {
                
                $datavedd = array(
                                'idVentas'=>$idventa,
                                'idEquipo'=>$equipo,//idequiporenta
                                'idserie'=>$serie,//consultar a "alta_rentas_equipos" con el idequipo y la serie
                                'piezas'=>1,
                                'modelo'=>$consumiblett,
                                'rendimiento'=>'',
                                'descripcion'=>'',
                                'precioGeneral'=>0,
                                'totalGeneral'=>0,
                                'idConsumible'=>$consumible,
                                'surtir'=>'',
                                'comentario'=>$foliotext,
                                'foliotext'=>$foliotext
                            );
                $this->ModeloCatalogos->Insert('ventas_has_detallesConsumibles',$datavedd);
            }
            
            
        }
    }
    
}