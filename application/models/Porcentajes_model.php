<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class Porcentajes_model extends CI_Model 
{
    public function __construct() 
    {
        parent::__construct();
    }

    public function actualizaPorcentajes($id, $data)
    {
    	$this->db->set($data);
	    $this->db->where('id', $id);
	    return $this->db->update('porcentajeGanancia');
    }

   	public function getPorcentajesGanancia()
   	{
   		$sql = "SELECT * FROM porcentajeGanancia";
        $query = $this->db->query($sql);
        return $query->result();
   	}
}

?>