var base_url = $('#base_url').val();
//var editor; 
var idPoliza = $('#idPoliza').val();

$(document).ready(function(){  
    /*
    editor = new $.fn.dataTable.Editor( {
            ajax: base_url+"index.php/Cotizaciones/actualizaDetalleCotizacionPoliza",
            table: "#tablaBordesPrincipal",
            fields: [ {
                    label: "Cobertura:",
                    name: "cubre"
                },
                {
                    label: "Vigencia:",
                    name: "vigencia_meses"
                }
            ]
        } );

    table = $('#tablaBordesPrincipal').DataTable({
      "dom": 't',
      "ordering": false
    });

    load(idPoliza);

    // Activa la edición por casilla en cada ROW de la tabla
    $('#tablaBordesPrincipal').on( 'click', 'tbody td:not(:first-child)', function (e){
        editor.inline(this);
    });


    $('.edit').editable();
    */
    $('.editarvalor').click(function(event) {
        var idpoliza = $('#idpoliza').val();
        var polizacol = $('#polizacol').val();
        var pass = $('#password').val();
        var valor = $('#valor').val();
        if (pass!='') {
            //if (valor!='') {
                $.ajax({
                    type:'POST',
                    url: base_url+'Cotizaciones/editarpoliza',
                    data: {
                        idpoliza:idpoliza,
                        polizacol:polizacol,
                        pass:pass,
                        valor:valor
                        },
                        async: false,
                        statusCode:{
                            404: function(data){
                                swal("Error", "404", "error");
                            },
                            500: function(){
                                swal("Error", "500", "error"); 
                            }
                        },
                        success:function(data){
                          if (data==1) {
                            swal("Editado!", "Precio Editado!", "success");
                            setTimeout(function(){ window.location.href=''; }, 2000);
                          }else{
                            swal("Error", "No tiene permiso", "error"); 
                          }

                        }
                    });
            //}else{
              //  swal("Error", "El Campo a editar no debe de quedar vacio", "error"); 
            //}
        }else{
            swal("Error", "Debe de ingresar una contraseña", "error"); 
        }   
    }); 

});

/*
function btn_guardar(){
    $('.print_css').append('.cot{color: red}');
    $('.pantalla').remove();
    $('.usuario').remove();
    $('.menu').remove();
    $('.kyoc').remove();
    $('.log').append("<img class='kyoc' src='"+base_url+"app-assets/images/altakyo.png' alt='materialize logo' width='200px;' height='100px;'>");
    $('.venta_text').append("<h6 align='center' style='font-size:15px; color:black;'>Venta-Renta-Mantenimiento<br>Impresoras y Multifuncionales monocromáticos y color<br> Blvd. Norte No. 18831, San Alejandro, CP 72090, Puebla, Pue.<br> Call Center: 273 3400, 249 5393<br> Kyoceraap.com</h6>");
   // $('.venta_img').append("<img class='kyoc' src='"+base_url+"app-assets/images/1024_kyocera_logo.png' alt='materialize logo' width='200px;' height='80px;'>");
    $('.btn_imp').remove();
    setTimeout(function() {window.print();}, 500);
    setTimeout(function() {location.reload();}, 5000);
}

  
function load(idPoliza) {
    // Destruye la tabla y la crea de nuevo con los datos cargados 
    table.destroy();
    table = $('#tablaBordesPrincipal').DataTable({
        "ajax": {
            "url": base_url+"index.php/Cotizaciones/getTablaDetallesPoliza/"+idPoliza
        },
        "columns": [
            {"data": "DT_RowId", visible: false},
            {"data": "nombre", class: "tablaBordesPrincipal"},
            {"data": "cubre", class: "tablaBordesPrincipal"},
            {"data": "vigencia_meses", class: "tablaBordesPrincipal"},
            {"data": "vigencia_clicks", class: "tablaBordesPrincipal"},
            {"data": "modelo", class: "tablaBordesPrincipal"},
            {"data": "precio_local", class: "tablaBordesPrincipal"},
            {"data": "precio_semi", class: "tablaBordesPrincipal"},
            {"data": "precio_foraneo", class: "tablaBordesPrincipal"}
        ],
        "ordering": false,
        "dom": 't'
    });
}

function selectcotiza(){
    var selection=$('#cotizar option:selected').val(); 
      if(selection==1){
        $('.select_familia').css('display','none');
      }else if(selection==2){
        $('.select_familia').css('display','inline');
      }
}
*/

function editar(id,columna,valor){
    if ($('#estatuscot').val()==1) {
        $('#password').val('');
        $('#modaleditor').modal();
        $('#modaleditor').modal('open');
        $('#valor').val(valor);
        $('#idpoliza').val(id);
        $('#polizacol').val(columna);
    }
    

}
function btn_guardar(){
    //window.open(base_url+'Cotizaciones/pdf/'+idCotizacion, '_blank');
    //$('.print_css').append('.cot{color: red}');
    //$('.pantalla').remove();
    //$('.usuario').remove();
    //$('.menu').remove();
    //$('.kyoc').remove();
    //$('.log').append("<img class='kyoc' src='"+base_url+"app-assets/images/altakyo.png' alt='materialize logo' width='200px;' height='100px;'>");
    //$('.venta_text').append("<h6 align='center' style='font-size:15px; color:black;'>Venta-Renta-Mantenimiento<br>Impresoras y Multifuncionales monocromáticos y color<br> Blvd. Norte No. 18831, San Alejandro, CP 72090, Puebla, Pue.<br> Call Center: 273 3400, 249 5393<br> Kyoceraap.com</h6>");
   // $('.venta_img').append("<img class='kyoc' src='"+base_url+"app-assets/images/1024_kyocera_logo.png' alt='materialize logo' width='200px;' height='80px;'>");
    //$('.btn_imp').remove();
    setTimeout(function() {window.print();}, 500);
    //setTimeout(function() {location.reload();}, 5000);
    saveinfo_o(4);
}
function saveinfo(idcoti,tipo){
    var info=$('.info'+tipo).html();
    console.log(info);
    $.ajax({
        type:'POST',
        url: base_url+'Generales/editarinfocotizacion',
        data: {
            cotizacion: idcoti,
            tipo: tipo,
            info: info
            },
            async: false,
            statusCode:{
                404: function(data){
                    //swal("Error", "404", "error");
                },
                500: function(){
                    //swal("Error", "500", "error"); 
                }
            },
            success:function(data){
              
            }
        });
}