
<style type="text/css">
  .imgpro{
    width: 100px;
  }
  .imgpro:hover {
    transform: scale(2.9); 
    filter: drop-shadow(5px 9px 12px #444);
  }
  .btn_img{
    cursor: pointer;
  }
  td{
    font-size: 12px;
  }
  table.dataTable{
    margin: 0px !important;
  }
  #tabla_facturacion{
    width: 100% !important;
  }
    <?php if($this->perfilid!=1){ ?>
    .soloadministradores{
      display: none;
    }
  <?php } ?>
</style>

<!-- START CONTENT -->
    <section id="content">
      <!--start container-->
        <div class="container">
          <div class="section">

            <input id="base_url" type="hidden" name="base_url" value="<?php echo base_url(); ?>">

            <div id="table-datatables">
              <br>
                  <h4 class="header">Listado de Servicios</h4>
                  <div class="row">
                    
                    <div class="col s12 soloadministradores">
                      <a class="btn waves-effect waves-light gradient-45deg-green-teal gradient-shadow col s1" href="<?php echo base_url(); ?>index.php/polizas/alta">Nuevo</a>

                    </div>

                  <div class="col s12">

                      <table id="tabla_polizas" class="responsive-table display" cellspacing="0">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>Servicio</th>
                            <th>Incluye</th>
                            <th>Vigencia</th>
                            <th>Visitas</th>
                            <th>Acciones</th>
                          </tr>
                        </thead>
                        <tbody>
                        </tbody>
                      </table>
                    </div>
                  </div>
            </div>
          </div>  
        </div>
    </section>


<div class="modal fade text-left" id="modaldoc" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lx" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Documentos</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-md-3">
                    <img src="<?php echo base_url(); ?>uploads/consumibles/r_-190125-115608-cro1.PNG" style="height:50px;width:50px;">
                </div>
                <div class="col-md-9 filescargados" style="overflow: auto; max-height: 500;">
                  
                </div>
              </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn  btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn  btn-success" data-dismiss="modal" id="subirdoc">Aceptar</button>
            </div>
        </div>
    </div>
</div>



