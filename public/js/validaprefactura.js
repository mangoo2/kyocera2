var enviopre=1;
var fechaactual = $('#fechaactual').val();
var fechasiguiente = $('#fechasiguiente').val();
function validpre(){
	var hoy = new Date();
	var hora = hoy.getHours();
	var fechaentrega = $('#fechaentrega').val();
	var horadebloqueo=16;//16 (4 pm)
    var horadebloqueoi=10;//10 (10 am)
	//if(parseInt(hora)<parseInt(horadebloqueoi) && parseInt(hora)>=parseInt(horadebloqueo) ){
    if(parseInt(hora)>=parseInt(horadebloqueo) ){
        /*
        const fechae = new Date(fechaentrega);
        const fechaa = new Date(fechaactual);
        const fechas = new Date(fechasiguiente);
        */
        const fechae = fechaentrega;
        const fechaa = fechaactual;
        const fechas = fechasiguiente;
		if(fechae===fechaa){
            console.log('mismas fechas');
			$(".bodega").each(function() {
		        var bodegaid = parseInt($(this).data('bodega'));
		        if(bodegaid!=1 && bodegaid!=5){
		        	enviopre=0;
		        }
		    });
		}
		if(fechae===fechas){

			$(".bodega").each(function() {
		        var bodegaid = parseInt($(this).data('bodega'));
		        if(bodegaid!=1 && bodegaid!=5){
		        	enviopre=0;
		        }
		    });
		}
	}
	if(enviopre==0){
		envioconpermisos();
	}
	return enviopre;
}

function envioconpermisos(){
    swal({   title: "Advertencia!",
          text: "Se necesita contraseña de administrador:",
          type: "input",   
          showCancelButton: true,
          closeOnConfirm: false,
          showLoaderOnConfirm: true,
          animation: "slide-from-top",
          inputPlaceholder: "********",
          inputType:'password' },
          function(inputValue){
            if (inputValue === false) return false;
            if (inputValue === "") {
                swal.showInputError("No se permite campos vacíos!");
                return false
            }else{
                $.ajax({
                    type:'POST',
                    url: base_url+'Cotizaciones/permisoscostocero',
                    data: {
                        pass: inputValue
                    },
                    async: false,
                    statusCode:{
                        404: function(data){
                                swal("Error", "404", "error");
                        },
                        500: function(){
                            swal("Error", "500", "error"); 
                        }
                    },
                    success:function(data){
                      if (data==1) {
                      	swal({
                            title: "Confirmación",
                            text: "Contraseña correcta.",
                            timer: 2000,
                            showConfirmButton: false
                        });
                        $('#modalconfirmacionenvio').modal();
                        
                      }else{
                        swal.showInputError("No tiene permisos!");
                      }

                    }
                });
            }
            
          });
    setTimeout(function(){ $('.sweet-alert input').val(''); }, 1000);
    
}
function folioventa(tipo){
    $.ajax({
        type:'POST',
        url: base_url+'Prefactura/addfolioventa',
        data: {
            ventaId: $('#ventaId').val(),
            tipo:tipo,
            folio_venta:$('#folio_venta').val()
        },
        async: false,
        statusCode:{
            404: function(data){
                    swal("Error", "404", "error");
            },
            500: function(){
                swal("Error", "500", "error"); 
            }
        },
        success:function(data){
          

        }
    });
}
function descartargarantia(tipo,id){
    $.confirm({
        boxWidth: '40%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Desea indicar que el producto no tiene garantia?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                //===================================================
                    
                    //console.log(aInfoa);
                        $.ajax({
                            type:'POST',
                            url: base_url+"index.php/Generales/descartargarantia",
                            data: {
                                tipo:tipo,
                                id:id
                            },
                            success:function(response){  
                                
                                swal("Éxito!", "Se ha Modificado", "success");
                                setTimeout(function(){ 
                                    location.reload();
                                }, 1000);

                            }
                        });
                    
                //================================================
                 
            },
            cancelar: function () 
            {
                
            }
        }
    });
}
function confir_envio(){
    $.confirm({
        boxWidth: '50%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: 'Faltan series por asignar, ¿Desea Continuar?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                //===================================================
                    if($('#formprefactura').valid()){
                        var validp=validpre();
                        if(validp==1){
                            $('#modalconfirmacionenvio').modal();
                        }
                        
                    }else{
                        swal("Advertencia", "Seleccione los campos requeridos", "error");

                    }
                    
                //================================================
                 
            },
            cancelar: function () 
            {
                
            }
        }
    });
}
function desbloquearcomentarios(){
    $.confirm({
        boxWidth: '50%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Desea editar las direcciones?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                //===================================================
                    desbloquearcomentarios2();
                    
                //================================================
                 
            },
            cancelar: function () 
            {
                
            }
        }
    });
}
function desbloquearcomentarios2(){
    $("#domicilio_entrega").prop('disabled', false).prop('readonly', false);
    $('.comentario').prop('disabled', false).prop('readonly', false);
    $('.buttoresave').html('<button type="button" class="btn btn-success " onclick="buttoresave()">Actualizar</button>');
}
function buttoresave(){
    $('#modalconfirmacionenvio').modal();
}