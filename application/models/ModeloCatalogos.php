<?php
$a=session_id();
if(empty($a)) session_start();
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloCatalogos extends CI_Model {
    public function __construct() {
        parent::__construct();
        $this->DB2 = $this->load->database('other_db', TRUE); 
        $this->DB3 = $this->load->database('other2_db', TRUE);
        $this->DB4 = $this->load->database('other4_db', TRUE);  
        $this->DB5 = $this->load->database('other5_db', TRUE);
        $this->DB6 = $this->load->database('other6_db', TRUE);
        $this->DB7 = $this->load->database('other7_db', TRUE);
        $this->DB8 = $this->load->database('other8_db', TRUE);
        $this->DB9 = $this->load->database('other9_db', TRUE);
        $this->DB10 = $this->load->database('other10_db', TRUE);
        $this->DB11 = $this->load->database('other11_db', TRUE);
        //$this->DB12 = $this->load->database('other12_db', TRUE);
        $this->DB13 = $this->load->database('other13_db', TRUE);
        $this->DB14 = $this->load->database('other14_db', TRUE);
        $this->DB17 = $this->load->database('other17_db', TRUE);

        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fechahoyc = date('Y-m-d');
        $this->diahoyc = date('j');
    }
    function getadminsu(){
        $strq = "CALL SP_GET_PERSONAL_ADMINSU";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function getestados(){
        $strq = "SELECT * FROM estado";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function Insert($Tabla,$data){
        $this->db->insert($Tabla, $data);
        $id=$this->db->insert_id();
        return $id;
    }
    function getselectwherenlimit($table,$where,$limit){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($where);
        $this->db->limit($limit);
        $query=$this->db->get(); 
        return $query;
    }
    function insert_batch($Tabla,$data){
        $this->db->insert_batch($Tabla, $data);
    }
    function db4_insert_batch($Tabla,$data){
        $this->DB4->insert_batch($Tabla, $data);
    }
    function db5_Insert($Tabla,$data){
        $this->DB5->insert($Tabla, $data);
        $id=$this->DB5->insert_id();
        return $id;
    }
    function db5_insert_batch($Tabla,$data){
        $this->DB5->insert_batch($Tabla, $data);
    }
    function db4_Insert($Tabla,$data){
        $this->DB4->insert($Tabla, $data);
        $id=$this->DB4->insert_id();
        return $id;
    }
    function updateCatalogo($Tabla,$data,$where){
        $this->db->set($data);
        $this->db->where($where);
        $this->db->update($Tabla);
        return $this->db->affected_rows();
    }
    function db4_updateCatalogo($Tabla,$data,$where){
        $this->DB4->set($data);
        $this->DB4->where($where);
        $this->DB4->update($Tabla);
        //return $id;
    }
    function db5_updateCatalogo($Tabla,$data,$where){
        $this->DB5->set($data);
        $this->DB5->where($where);
        $this->DB5->update($Tabla);
        //return $id;
    }
    function getselectwheren($table,$where){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($where);
        $query=$this->db->get(); 
        return $query;
    }
    function db2_getselectwheren($table,$where){
        $this->DB2->select('*');
        $this->DB2->from($table);
        $this->DB2->where($where);
        $query=$this->DB2->get(); 
        return $query;
    }
    function db14_getselectwheren($table,$where){
        $this->DB14->select('*');
        $this->DB14->from($table);
        $this->DB14->where($where);
        $query=$this->DB14->get(); 
        return $query;
    }
    function db3_getselectwheren($table,$where){
        $this->DB3->select('*');
        $this->DB3->from($table);
        $this->DB3->where($where);
        $query=$this->DB3->get(); 
        return $query;
    }
    function db6_getselectwheren($table,$where){
        $this->DB6->select('*');
        $this->DB6->from($table);
        $this->DB6->where($where);
        $query=$this->DB6->get(); 
        return $query;
    }
    function db7_getselectwheren($table,$where){
        $this->DB7->select('*');
        $this->DB7->from($table);
        $this->DB7->where($where);
        $query=$this->DB7->get(); 
        return $query;
    }
    function db10_getselectwheren($table,$where){
        $this->DB10->select('*');
        $this->DB10->from($table);
        $this->DB10->where($where);
        $query=$this->DB10->get(); 
        return $query;
    }
    function db13_getselectwheren($table,$where){
        $this->DB13->select('*');
        $this->DB13->from($table);
        $this->DB13->where($where);
        $query=$this->DB13->get(); 
        return $query;
    }

    function getselectwheren_2($select,$table,$where){
        $this->db->select($select);
        $this->db->from($table);
        $this->db->where($where);
        $query=$this->db->get(); 
        return $query->result();
    }
    function db3_getselectwheren_2($select,$table,$where){
        $this->DB3->select($select);
        $this->DB3->from($table);
        $this->DB3->where($where);
        $query=$this->DB3->get(); 
        return $query->result();
    }
    function getdeletewheren($table,$where){
        $this->db->where($where);
        $this->db->delete($table);
    }
    function getdeletewheren3($table,$where){
        $this->db->delete($table,$where);
    }
    function getdeletewheren2($table,$where){
        
        $strq = "DELETE from $table WHERE idpago=$where";
        $query = $this->db->query($strq);
    }
    function getselectwherenall($table,$where){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($where);
        $query=$this->db->get(); 
        return $query->result();
    }
    public function getseleclike($tables,$cols,$values){
        $strq = "SELECT * from $tables WHERE estatus=1 and $cols like '%".$values."%'";
        $query = $this->DB3->query($strq);
        //$this->db->close();
        return $query;
    }
    public function getclienterazonsociallike($search){
        $strq = "SELECT cli.id,cli.empresa,clif.razon_social,clif.rfc,cli.bloqueo,cli.bloqueo_perm_ven_ser,cli.tipo
                FROM clientes as cli 
                INNER JOIN cliente_has_datos_fiscales as clif on clif.idCliente=cli.id and clif.activo=1
                WHERE cli.estatus=1 and (cli.empresa LIKE '%$search%' or clif.razon_social LIKE '%$search%') GROUP BY cli.id order by cli.empresa asc";
        $query = $this->DB3->query($strq);
        //$this->db->close();
        return $query;
    }
    public function getseleclike2($tables,$cols,$values){
        $strq = "SELECT * from $tables WHERE status=1 and $cols like '%".$values."%'";
        $query = $this->DB6->query($strq);
        //$this->db->close();
        return $query;
    }
    
    function updatestock($Tabla,$value,$masmeno,$value2,$idname,$id){
        $strq = "UPDATE $Tabla SET $value = $value $masmeno $value2 where $idname=$id ";
        $query = $this->db->query($strq);
        return $id;
    }
    function updatestock2($Tabla,$value,$masmeno,$value2,$idname1,$id1,$idname2,$id2){
        $strq = "UPDATE $Tabla SET $value = $value $masmeno $value2 where $idname1=$id1 and $idname2=$id2 ";
        $query = $this->db->query($strq);
        //return $id;
    }
    function updatestock3($Tabla,$value,$masmeno,$value2,$idname1,$id1,$idname2,$id2,$idname3,$id3){
        $strq = "UPDATE $Tabla SET $value = $value $masmeno $value2 where $idname1=$id1 and $idname2=$id2 and $idname3=$id3 ";
        $query = $this->db->query($strq);
        //return $id;
    }

    function consumibleshasequipos($equipo){
        $strq = 'SELECT con.id,con.modelo,con.tipo 
                FROM consumibles as con 
                inner join equipos_has_consumibles as econ on econ.idConsumibles=con.id 
                WHERE con.status=1 AND econ.idEquipo='.$equipo;
        $query = $this->DB9->query($strq);
        return $query;
    }
    function consumibleagregado($equipo){
        $strq = 'SELECT con.modelo,cons.poliza,con.rendimiento_unidad_imagen,con.rendimiento_toner,con.tipo,econ.* 
                FROM equipos_consumibles as econ 
                inner join consumibles as con on con.id=econ.idconsumible
                left JOIN consumibles_costos as cons on cons.consumibles_id=econ.idconsumible
                WHERE econ.activo=1 and con.status=1 and econ.idequipo='.$equipo;//agregar este cambio
        $query = $this->DB9->query($strq);
        return $query;
    }
    function consumibleagregado_tipo($equipo,$tipo){
        $strq = "SELECT con.modelo,con.rendimiento_toner,con.tipo
                FROM equipos_consumibles as econ 
                inner join consumibles as con on con.id=econ.idconsumible
                left JOIN consumibles_costos as cons on cons.consumibles_id=econ.idconsumible
                WHERE econ.activo=1 and con.status=1 and econ.idequipo='$equipo'";//agregar este cambio
        $query = $this->DB9->query($strq);
        $modelo='';
        $rendimiento='';
        $info='';
        foreach ($query->result() as $item) {
            if($tipo==1){
                if($item->tipo==1){
                    $modelo=$item->modelo;
                    $rendimiento=$item->rendimiento_toner;
                    $info=$item;
                }
            }
            if($tipo==2){
                if($item->tipo==2 || $item->tipo==3 || $item->tipo==4){
                    $modelo=$item->modelo;
                    $rendimiento=$item->rendimiento_toner;
                    $info=$item;
                }
            }
        }

        $result_array=array('modelo'=>$modelo,'rendimiento'=>$rendimiento,'info'=>$info);
        return $result_array;
    }
    function consumibleagregadoselected($id,$equipo){
        $strq = 'SELECT con.modelo,con.tipo,cons.poliza,con.rendimiento_unidad_imagen,con.rendimiento_toner,econ.* 
                FROM equipos_consumibles as econ 
                inner join consumibles as con on con.id=econ.idconsumible
                left JOIN consumibles_costos as cons on cons.consumibles_id=econ.idconsumible
                WHERE econ.idconsumible='.$id.' and econ.idequipo='.$equipo;
        $query = $this->DB6->query($strq);
        return $query;
    }
    function consumiblesequipos($equipos,$bodega){
        if($bodega>0){
            $wherebodega=" and cob.bodegaId=$bodega ";
        }else{
            $wherebodega="";
        }

        $strq = "SELECT econ.id,econ.idconsumible,con.modelo,econ.idconsumible,con.tipo,
                (select sum(cob.total) from consumibles_bodegas as cob where cob.consumiblesId=con.id $wherebodega) as stock,
                (select sum(cob.resguardar_cant) from consumibles_bodegas as cob where cob.consumiblesId=con.id $wherebodega) as stock_resg

                FROM `equipos_consumibles` as econ
                inner join consumibles as con on con.id=econ.idconsumible
                where econ.activo=1 AND econ.idequipo='$equipos'";
        $query = $this->db->query($strq);
        return $query;
    }
    function consumibleequipocotizacion($coti,$equipo){
        $strq = 'SELECT con.id,con.modelo
                    FROM cotizaciones_has_detallesEquipos_consumibles as codc
                    inner JOIN consumibles as con on con.id=codc.id_consumibles
                    WHERE codc.id_cotizacion='.$coti.'
                    and codc.id_equipo='.$equipo; 
        $query = $this->DB9->query($strq);
        return $query;
    }
    function costoaccesorio($id){
        $strq = 'SELECT costo FROM catalogo_accesorios WHERE id='.$id; 
        $query = $this->DB9->query($strq);
        $costo=0;
        foreach ($query->result() as $item) {
            $costo=$item->costo;
        }
        return $costo;
    }
    function accesoriosventa($id){
        /*
        $strq = "SELECT vdea.id_accesoriod,vdea.cantidad,caa.nombre,caa.no_parte, vdea.serie,vdea.serie_estatus,vdea.costo,vdea.surtir,vdea.serie_bodega,vdea.comentario,
                vdea.entregado_status,vdea.entregado_fecha,per.nombre as personal,per.apellido_paterno,bod.bodega,bod.bodegaId,vdea.id_accesorio,vdea.garantia
                FROM ventas_has_detallesEquipos_accesorios as vdea 
                inner JOIN catalogo_accesorios as caa on caa.id=vdea.id_accesorio
                left join personal as per on per.personalId=vdea.entregado_personal
                left join bodegas as bod on bod.bodegaId=vdea.serie_bodega
                WHERE vdea.id_venta='$id' "; 
                */
        $strq ="SELECT vdea.id_accesoriod,vdea.cantidad,caa.nombre,caa.no_parte, vdea.serie,vdea.serie_estatus,vdea.costo,vdea.surtir,vdea.serie_bodega,vdea.comentario,
                vdea.entregado_status,vdea.entregado_fecha,per.nombre as personal,per.apellido_paterno,bod.bodega,bod.bodegaId,vdea.id_accesorio,vdea.garantia,GROUP_CONCAT(spr.serie) as series
                FROM ventas_has_detallesEquipos_accesorios as vdea 
                inner JOIN catalogo_accesorios as caa on caa.id=vdea.id_accesorio
                left join personal as per on per.personalId=vdea.entregado_personal
                left join bodegas as bod on bod.bodegaId=vdea.serie_bodega
                LEFT JOIN asignacion_series_accesorios as ase ON ase.id_accesorio=vdea.id_accesoriod
                left JOIN series_accesorios as spr on spr.serieId=ase.serieId
                WHERE vdea.id_venta='$id'
                GROUP BY vdea.id_accesoriod";
        $query = $this->DB5->query($strq);
        return $query;
    }
    function accesoriosventa_dev($id){
        $strq = 'SELECT vdea.id_accesoriod,vdea.cantidad,caa.nombre,caa.no_parte, vdea.serie,vdea.serie_estatus,vdea.costo,vdea.surtir,vdea.serie_bodega,vdea.comentario,bod.bodega,per.nombre as personal,per.apellido_paterno
                FROM ventas_has_detallesEquipos_accesorios_dev as vdea
                inner JOIN catalogo_accesorios as caa on caa.id=vdea.id_accesorio
                left join personal as per on per.personalId=vdea.entregado_personal
                left join bodegas as bod on bod.bodegaId=vdea.serie_bodega
                WHERE vdea.id_venta='.$id; 
        $query = $this->DB5->query($strq);
        return $query;
    }
    function consumiblesventa($id){
        $strq = "SELECT vdc.id,vdc.cantidad,vdc.costo_toner,vdc.rendimiento_toner,vdc.rendimiento_unidad_imagen,con.modelo,con.parte,con.tipo,vdc.surtir,vdc.comentario,
                vdc.entregado_status,vdc.entregado_fecha,eq.modelo as modeloeq,bod.bodega,vdc.bodega as serie_bodega,vdc.id_consumibles,bod.bodegaId,vdc.garantia,per.nombre as personal,per.apellido_paterno
                FROM ventas_has_detallesequipos_consumible as vdc
                inner JOIN consumibles as con on con.id=vdc.id_consumibles
                left join personal as per on per.personalId=vdc.entregado_personal
                left join equipos as eq on eq.id=vdc.idequipo
                left join bodegas as bod on bod.bodegaId=vdc.bodega
                WHERE vdc.idventa='$id'"; 
        $query = $this->DB5->query($strq);
        return $query;
    }
    function consumiblesventa_dev($id){
        $strq = "SELECT vdc.id,vdc.cantidad,vdc.costo_toner,vdc.rendimiento_toner,vdc.rendimiento_unidad_imagen,con.modelo,con.parte,con.tipo,vdc.surtir,vdc.comentario,bod.bodega,vdc.motivo_dev,vdc.reg_dev
                FROM ventas_has_detallesequipos_consumible_dev as vdc
                inner JOIN consumibles as con on con.id=vdc.id_consumibles
                left join bodegas as bod on bod.bodegaId=vdc.bodega
                WHERE vdc.idventa='$id'"; 
        $query = $this->DB5->query($strq);
        return $query;
    }
    function consumiblesventadetalles($id){
        $strq = "SELECT vdc.id,
                        vdc.piezas AS cantidad,
                        vdc.precioGeneral AS costo_toner,
                        con.modelo,
                        con.parte,
                        con.tipo,
                        vdc.surtir,
                        vdc.comentario,
                        vdc.idserie,
                        vdc.idEquipo,
                        per.nombre,
                        per.nombre as personal,
                        per.apellido_paterno,
                        vdc.entregado_status,
                        vdc.entregado_fecha,
                        vdc.foliotext,
                        vdc.bodegas,
                        eq.modelo as modeloeq,
                        bod.bodega,
                        vdc.idConsumible,
                        bod.bodegaId,
                        vdc.garantia,
                        vdc.entregado_status,
                        per2.nombre as personal,per2.apellido_paterno,
                        conf.serviciocId_tipo,
                        conf.serviciocId
                FROM ventas_has_detallesConsumibles as vdc
                inner JOIN consumibles as con on con.id=vdc.idConsumible
                left join personal as per on per.personalId=vdc.entregado_personal
                left join equipos as eq on eq.id=vdc.idEquipo
                left join bodegas as bod on bod.bodegaId=vdc.bodegas
                left join personal as per2 on per2.personalId=vdc.entregado_personal
                left join contrato_folio as conf on conf.foliotext=vdc.foliotext
                WHERE vdc.idVentas='$id' GROUP by vdc.id"; 
        $query = $this->DB7->query($strq);
        return $query;
    }
    function productosventa_union($id){
        $strq = "";
        $strq .= "(";
            $strq .= "SELECT 1 as tipo,eq.id,eq.cantidad,eq.modelo,GROUP_CONCAT(spr.serie SEPARATOR ', ') as series
                FROM ventas_has_detallesEquipos as eq 
                left JOIN asignacion_series_equipos as ase on ase.id_equipo=eq.id
                left JOIN series_productos as spr on spr.serieId=ase.serieId
                WHERE eq.idVenta='$id'
                GROUP BY eq.id";
        $strq .= ")";
        $strq .= " UNION ";
        $strq .= "(";
            $strq .= "SELECT 2 as tipo,vdc.id, vdc.cantidad,con.modelo,'' as series FROM ventas_has_detallesequipos_consumible as vdc inner JOIN consumibles as con on con.id=vdc.id_consumibles WHERE vdc.idventa='$id'";
        $strq .= ")";
        $strq .= " UNION ";
        $strq .= "(";
            $strq .= "SELECT 2 as tipo,vdc.id, vdc.piezas AS cantidad,con.modelo,'' as series FROM ventas_has_detallesConsumibles as vdc inner JOIN consumibles as con on con.id=vdc.idConsumible WHERE vdc.idVentas='$id'";
        $strq .= ")";
        $strq .= " UNION ";
        $strq .= "(";
            $strq .= "SELECT 3 as tipo,vdea.id_accesoriod as id,vdea.cantidad,caa.nombre as modelo,GROUP_CONCAT(spr.serie SEPARATOR ', ') as series
                FROM ventas_has_detallesEquipos_accesorios as vdea 
                inner JOIN catalogo_accesorios as caa on caa.id=vdea.id_accesorio
                LEFT JOIN asignacion_series_accesorios as ase ON ase.id_accesorio=vdea.id_accesoriod
                left JOIN series_accesorios as spr on spr.serieId=ase.serieId
                WHERE vdea.id_venta='$id'
                GROUP BY vdea.id_accesoriod";
        $strq .= ")";
        $strq .= " UNION ";
        $strq .= "(";
            $strq .= "SELECT 4 as tipo,v.id,v.piezas AS cantidad,equ.nombre AS modelo,GROUP_CONCAT(DISTINCT spr.serie SEPARATOR ' ') as series
                FROM ventas_has_detallesRefacciones AS v
                INNER JOIN refacciones AS equ on equ.id=v.idRefaccion
                LEFT JOIN asignacion_series_refacciones as ase on ase.id_refaccion=v.id
                inner JOIN series_refacciones as spr on spr.serieId=ase.serieId 
                WHERE v.idVentas='$id' GROUP by v.id";
        $strq .= ")";
        $query = $this->DB7->query($strq);
        return $query;
    }
    function consumiblesventadetalles_dev($id){
        $strq = "SELECT vdc.id,
                        vdc.piezas AS cantidad,
                        vdc.precioGeneral AS costo_toner,
                        con.modelo,con.parte,
                        con.tipo,
                        vdc.surtir,
                        vdc.comentario,
                        vdc.idserie,
                        vdc.idEquipo,
                        vdc.comentario,
                        bod.bodega,
                        vdc.foliotext,
                        vdc.entregado_status,
                        per.nombre as personal,per.apellido_paterno,
                        vdc.motivo_dev,vdc.reg_dev
                FROM ventas_has_detallesConsumibles_dev as vdc
                inner JOIN consumibles as con on con.id=vdc.idConsumible
                left join bodegas as bod on bod.bodegaId=vdc.bodegas
                left join personal as per on per.personalId=vdc.entregado_personal
                WHERE vdc.idVentas='$id' "; 
        $query = $this->DB7->query($strq);
        return $query;
    }
    function polizaventadetalles($id){
        $strq = 'SELECT v.id,
                        v.cantidad,
                        v.serie,
                        v.cubre,v.vigencia_meses,v.vigencia_clicks,
                        v.modelo,
                        v.precio_local,v.precio_semi,v.precio_foraneo,
                        equ.modelo as modeloe,
                        v.surtir,
                        v.comentario,
                        v.idEquipo
                FROM polizasCreadas_has_detallesPoliza AS v
                INNER JOIN polizasCreadas AS p ON p.id = v.idPoliza  
                INNER JOIN equipos AS equ on equ.id=v.idEquipo
                WHERE p.id='.$id; 
        $query = $this->DB7->query($strq);
        return $query;
    }
    function polizaventadetalles_dev($id){
        $strq = 'SELECT v.id,
                        v.cantidad,
                        v.serie,
                        v.cubre,v.vigencia_meses,v.vigencia_clicks,
                        v.modelo,
                        v.precio_local,v.precio_semi,v.precio_foraneo,
                        equ.modelo as modeloe,
                        v.surtir,
                        v.comentario,
                        v.idEquipo
                FROM polizasCreadas_has_detallesPoliza_dev AS v
                INNER JOIN polizasCreadas AS p ON p.id = v.idPoliza  
                INNER JOIN equipos AS equ on equ.id=v.idEquipo
                WHERE p.id='.$id; 
        $query = $this->DB7->query($strq);
        return $query;
    }
    function polizaventadetalles2($id){
        $strq = 'SELECT v.id,
                        v.cantidad,
                        v.serie,
                        v.cubre,v.vigencia_meses,v.vigencia_clicks,
                        v.nombre,v.modelo,
                        v.precio_local,v.precio_semi,v.precio_foraneo,
                        equ.modelo as modeloe,
                        v.surtir,
                        v.direccionservicio,
                        v.comentario,
                        v.idEquipo
                FROM polizasCreadas_has_detallesPoliza AS v
                INNER JOIN equipos AS equ on equ.id=v.idEquipo
                WHERE v.idPoliza ='.$id; 
        $query = $this->db->query($strq);
        return $query;
    }
    function polizaventadetalles3($id){
        $strq = 'SELECT v.id,
                        v.cantidad,
                        v.serie,
                        v.cubre,v.vigencia_meses,v.vigencia_clicks,
                        v.nombre,v.modelo,
                        v.precio_local,v.precio_semi,v.precio_foraneo,v.precio_especial,
                        equ.modelo as modeloe,
                        v.surtir,
                        v.direccionservicio,
                        v.comentario,
                        v.idEquipo,
                        v.suspender
                FROM polizasCreadas_has_detallesPoliza AS v
                INNER JOIN equipos AS equ on equ.id=v.idEquipo
                WHERE v.idPoliza='.$id; 
        $query = $this->db->query($strq);
        return $query;
    }
    function polizaventadetalles0_0($id){
        $strq = 'SELECT v.id,
                        v.cantidad,
                        v.serie,
                        v.cubre,v.vigencia_meses,v.vigencia_clicks,
                        v.nombre,v.modelo,
                        v.precio_local,v.precio_semi,v.precio_foraneo,v.precio_especial,
                        equ.modelo as modeloe,
                        v.surtir,
                        v.direccionservicio,
                        v.comentario,
                        v.idEquipo,
                        v.suspender
                FROM polizasCreadas_has_detallesPoliza AS v
                INNER JOIN equipos AS equ on equ.id=v.idEquipo
                WHERE v.id='.$id; 
        $query = $this->db->query($strq);
        return $query;
    }
    function polizaventadetalles3_dev($id){
        $strq = 'SELECT v.id,
                        v.cantidad,
                        v.serie,
                        v.cubre,
                        v.vigencia_meses,v.vigencia_clicks,
                        v.nombre,v.modelo,
                        v.precio_local,v.precio_semi,v.precio_foraneo,v.precio_especial,
                        equ.modelo as modeloe,
                        v.surtir,
                        v.direccionservicio,
                        v.comentario,
                        v.idEquipo,
                        v.suspender
                FROM polizasCreadas_has_detallesPoliza_dev AS v
                INNER JOIN equipos AS equ on equ.id=v.idEquipo
                WHERE v.idPoliza='.$id; 
        $query = $this->db->query($strq);
        return $query;
    }
    function seriespolizas($id){
        $strq = 'SELECT *
                FROM polizasCreadas_has_detallesPoliza_series AS v
                WHERE v.iddetalle='.$id; 
        $query = $this->DB7->query($strq);
        $series='';
        foreach ($query->result() as $item) {
            $series.='<div>'.$item->serie.'</div>';
        }
        return $series;
    }
    public function ventas_has_detallesRefacciones($idrefaccion){
        /*
        $strq = "SELECT v.id,
                        v.piezas AS cantidad,
                        equ.nombre AS modelo,
                        v.rendimiento,
                        v.descripcion,
                        v.precioGeneral,v.totalGeneral,
                        v.serie_estatus,
                        v.surtir,
                        v.comentario,
                        equ.codigo,equi.modelo as modeloeq,v.serie as serieeq,v.idEquipo,
                        v.entregado_status,
                        v.entregado_fecha,
                        per.nombre as personal,per.apellido_paterno,
                        bod.bodega,v.serie_bodega,
                        v.idRefaccion,
                        v.garantia,
                        v.entregado_status
                FROM ventas_has_detallesRefacciones AS v
                INNER JOIN refacciones AS equ on equ.id=v.idRefaccion
                left JOIN equipos AS equi on equi.id=v.idEquipo
                left join personal as per on per.personalId=v.entregado_personal
                left join bodegas as bod on bod.bodegaId=v.serie_bodega
                WHERE v.idVentas='$idrefaccion' GROUP by v.id"; 
        */
                /*
                $strq="SELECT v.id,v.piezas AS cantidad,equ.nombre AS modelo,
                        v.rendimiento,v.descripcion,v.precioGeneral,v.totalGeneral,
                        v.serie_estatus,v.surtir,v.comentario,equ.codigo,equi.modelo as modeloeq,v.serie as serieeq,v.idEquipo,
                        v.entregado_status,v.entregado_fecha,per.nombre as personal,per.apellido_paterno,
                        bod.bodega,v.serie_bodega,v.idRefaccion,v.garantia,v.entregado_status,
                        GROUP_CONCAT(DISTINCT spr.serie SEPARATOR ' ') as series,ase.usado
                FROM ventas_has_detallesRefacciones AS v
                INNER JOIN refacciones AS equ on equ.id=v.idRefaccion
                left JOIN equipos AS equi on equi.id=v.idEquipo
                left join personal as per on per.personalId=v.entregado_personal
                left join bodegas as bod on bod.bodegaId=v.serie_bodega
                LEFT JOIN asignacion_series_refacciones as ase on ase.id_refaccion=v.id
                LEFT JOIN series_refacciones as spr on spr.serieId=ase.serieId 
                WHERE v.idVentas='$idrefaccion' GROUP by v.id";
                */
                 $strq="SELECT v.id,v.piezas AS cantidad,equ.nombre AS modelo,
                        v.rendimiento,v.descripcion,v.precioGeneral,v.totalGeneral,
                        v.serie_estatus,v.surtir,v.comentario,equ.codigo,equi.modelo as modeloeq,v.serie as serieeq,v.idEquipo,
                        v.entregado_status,v.entregado_fecha,per.nombre as personal,per.apellido_paterno,
                        bod.bodega,v.serie_bodega,v.idRefaccion,v.garantia,v.entregado_status,
                        GROUP_CONCAT(DISTINCT IF(IFNULL(ase.usado, 0) = 1, CONCAT(spr.serie,'(VU) '), spr.serie) SEPARATOR ' ') as series,
                        ase.usado
                FROM ventas_has_detallesRefacciones AS v
                INNER JOIN refacciones AS equ on equ.id=v.idRefaccion
                left JOIN equipos AS equi on equi.id=v.idEquipo
                left join personal as per on per.personalId=v.entregado_personal
                left join bodegas as bod on bod.bodegaId=v.serie_bodega
                LEFT JOIN asignacion_series_refacciones as ase on ase.id_refaccion=v.id
                LEFT JOIN series_refacciones as spr on spr.serieId=ase.serieId 
                WHERE v.idVentas='$idrefaccion' GROUP by v.id";
                //log_message('error',$strq);
        $query = $this->db->query($strq);
        return $query;
    }
    public function ventas_has_detallesRefacciones_mini($idrefaccion){
        /*
        $strq = "SELECT v.id,
                        v.piezas AS cantidad,
                        equ.nombre AS modelo,
                        v.rendimiento,
                        v.descripcion,
                        v.precioGeneral,v.totalGeneral,
                        v.serie_estatus,
                        v.surtir,
                        v.comentario,
                        equ.codigo,equi.modelo as modeloeq,v.serie as serieeq,v.idEquipo,
                        v.entregado_status,
                        v.entregado_fecha,
                        per.nombre as personal,per.apellido_paterno,
                        bod.bodega,v.serie_bodega,
                        v.idRefaccion,
                        v.garantia,
                        v.entregado_status
                FROM ventas_has_detallesRefacciones AS v
                INNER JOIN refacciones AS equ on equ.id=v.idRefaccion
                left JOIN equipos AS equi on equi.id=v.idEquipo
                left join personal as per on per.personalId=v.entregado_personal
                left join bodegas as bod on bod.bodegaId=v.serie_bodega
                WHERE v.idVentas='$idrefaccion' GROUP by v.id"; 
        */
                $strq="SELECT v.id,v.piezas AS cantidad,equ.nombre AS modelo,
                        GROUP_CONCAT(DISTINCT spr.serie SEPARATOR ' ') as series
                FROM ventas_has_detallesRefacciones AS v
                INNER JOIN refacciones AS equ on equ.id=v.idRefaccion
                LEFT JOIN asignacion_series_refacciones as ase on ase.id_refaccion=v.id
                LEFT JOIN series_refacciones as spr on spr.serieId=ase.serieId 
                WHERE v.idVentas='$idrefaccion' GROUP by v.id";
        $query = $this->db->query($strq);
        return $query;
    }
    public function ventas_has_detallesRefacciones_dev($idrefaccion){
        $strq = 'SELECT v.id,
                        v.piezas AS cantidad,
                        equ.nombre AS modelo,
                        v.rendimiento,
                        v.descripcion,
                        v.precioGeneral,v.totalGeneral,
                        v.serie_estatus,
                        v.surtir,
                        v.comentario,
                        equ.codigo,equi.modelo as modeloeq,v.serie as serieeq,v.idEquipo,
                        per.nombre as personal,per.apellido_paterno,
                        bod.bodega,
                        v.entregado_status,
                        v.motivo_dev,v.reg_dev
                FROM ventas_has_detallesRefacciones_dev AS v
                INNER JOIN refacciones AS equ on equ.id=v.idRefaccion
                left JOIN equipos AS equi on equi.id=v.idEquipo
                left join bodegas as bod on bod.bodegaId=v.serie_bodega
                left join personal as per on per.personalId=v.entregado_personal
                WHERE v.idVentas='.$idrefaccion.' GROUP by v.id'; 
        $query = $this->db->query($strq);
        return $query;
    }
    function ventasaccesoriosrentas($id){
        $strq = 'SELECT vdea.id_accesorio,vdea.cantidad,caac.nombre,vdea.serie 
                FROM rentas_has_detallesEquipos_accesorios as vdea
                inner join catalogo_accesorios as caac on caac.id=vdea.id_accesorio
                WHERE vdea.idrentas='.$id.' and vdea.activo=1'; 
        $query = $this->DB7->query($strq);
        return $query;
    }
    function ventasconsumiblesrenta($id){
        $strq = 'SELECT cdc.id,cdc.cantidad,con.modelo,cdc.id_consumibles
                from rentas_has_detallesequipos_consumible as cdc
                inner join consumibles as con on con.id=cdc.id_consumibles
                inner join equipos as eq on eq.id=cdc.idequipo
                WHERE cdc.idrentas='.$id; 
        $query = $this->DB7->query($strq);
        return $query;
    }
    function ventasaccesorios($id){
        $strq = 'SELECT vdea.id_accesorio,vdea.cantidad,caac.nombre,vdea.serie 
                FROM ventas_has_detallesEquipos_accesorios as vdea
                inner join catalogo_accesorios as caac on caac.id=vdea.id_accesorio
                WHERE vdea.id_venta='.$id.' and vdea.activo=1'; 
        $query = $this->db->query($strq);
        return $query;
    }
    function ventasconsumibles($id){
        $strq = 'SELECT cdc.id,cdc.cantidad,con.modelo,cdc.id_consumibles
                from ventas_has_detallesequipos_consumible as cdc
                inner join consumibles as con on con.id=cdc.id_consumibles
                inner join equipos as eq on eq.id=cdc.idequipo
                WHERE cdc.idventa='.$id; 
        $query = $this->DB8->query($strq);
        return $query;
    }
    function rentaventas($id){
        $strq = 'SELECT id, idCliente FROM rentas 
                 WHERE id='.$id; 
        $query = $this->db->query($strq);
        return $query;
    }
    function rentaventasd($id){
        $strq = "SELECT r.*, 
                        b.bodega, 
                        e.noparte,
                        ce.nombre AS categoria, 
                        asies.serieId,
                        per.nombre as personal,per.apellido_paterno,
                        b.bodegaId,
                        concat(per2.nombre,' ',per2.apellido_paterno,' ',per2.apellido_materno) as tecnico
                 FROM rentas_has_detallesEquipos AS r 
                 LEFT JOIN bodegas AS b ON b.bodegaId = r.serie_bodega
                 LEFT JOIN equipos AS e ON e.id =r.idEquipo
                 LEFT JOIN categoria_equipos AS ce ON ce.id = e.categoriaId
                 inner join asignacion_series_r_equipos as asies on asies.id_equipo=r.id and asies.activo=1
                 left join personal as per on per.personalId=r.entregado_personal

                 LEFT JOIN asignacion_ser_contrato_a_e as acd on acd.idequipo=r.id and acd.activo=1
                 LEFT JOIN asignacion_ser_contrato_a as ac on ac.activo=1 and ac.tpoliza=17
                 LEFT JOIN personal as per2 on per2.personalId=acd.tecnico 

                 WHERE idRenta='$id' GROUP by r.id "; 
        $query = $this->db->query($strq);
        return $query;
    }
    function rentaventasd2($id){
        $strq = "SELECT r.*, 
                        b.bodega, 
                        e.noparte,
                        ce.nombre AS categoria, 
                        asies.serieId,
                        per.nombre as personal,per.apellido_paterno,
                        b.bodegaId
                 FROM rentas_has_detallesEquipos AS r 
                 LEFT JOIN bodegas AS b ON b.bodegaId = r.serie_bodega
                 LEFT JOIN equipos AS e ON e.id =r.idEquipo
                 LEFT JOIN categoria_equipos AS ce ON ce.id = e.categoriaId
                 inner join asignacion_series_r_equipos as asies on asies.id_equipo=r.id and asies.activo=1
                 left join personal as per on per.personalId=r.entregado_personal

               

                 WHERE idRenta='$id' GROUP by r.id "; 
        $query = $this->db->query($strq);
        return $query;
    }
    function rentaventasd0($id){
        $strq = 'SELECT r.*, b.bodega, e.noparte,ce.nombre AS categoria, asies.serieId,per.nombre as personal, per.apellido_paterno,b.bodegaId,ser.serie,ser.usado
                 FROM rentas_has_detallesEquipos AS r 
                 LEFT JOIN bodegas AS b ON b.bodegaId = r.serie_bodega
                 LEFT JOIN equipos AS e ON e.id =r.idEquipo
                 LEFT JOIN categoria_equipos AS ce ON ce.id = e.categoriaId
                 inner join asignacion_series_r_equipos as asies on asies.id_equipo=r.id and asies.activo=1
                 INNER JOIN series_productos as ser on ser.serieId=asies.serieId
                 left join personal as per on per.personalId=r.entregado_personal
                 WHERE r.idRenta='.$id.' GROUP by r.id 
                 union
                 SELECT r.*, b.bodega, e.noparte,ce.nombre AS categoria, asies.serieId,per.nombre as personal, per.apellido_paterno,b.bodegaId,ser.serie,ser.usado
                 FROM rentas_has_detallesEquipos AS r 
                 LEFT JOIN bodegas AS b ON b.bodegaId = r.serie_bodega
                 LEFT JOIN equipos AS e ON e.id =r.idEquipo
                 LEFT JOIN categoria_equipos AS ce ON ce.id = e.categoriaId
                 inner join asignacion_series_r_equipos as asies on asies.id_equipo=r.id and asies.activo=0
                 INNER JOIN series_productos as ser on ser.serieId=asies.serieId
                 left join personal as per on per.personalId=r.entregado_personal
                 WHERE r.personal_dev>0 and r.idRenta='.$id.' GROUP by r.id
                 '; 
        $query = $this->DB8->query($strq);
        return $query;
    }
    function rentaventasaccesoriosd($id){
        $strq = 'SELECT r_a.id_accesoriod,r_a.cantidad,bod.bodega,acc.no_parte,acc.nombre,r_a.entregado_status,per.nombre as personal, per.apellido_paterno,sacc.serie
                FROM rentas_has_detallesEquipos_accesorios as r_a 
                LEFT JOIN bodegas as bod on bod.bodegaId=r_a.serie_bodega 
                LEFT JOIN catalogo_accesorios as acc on acc.id=r_a.id_accesorio 
                left join personal as per on per.personalId=r_a.entregado_personal
                left join asignacion_series_r_accesorios as aser on aser.id_accesorio=r_a.id_accesoriod
                left join series_accesorios as sacc on sacc.serieId=aser.serieId

                WHERE r_a.idrentas='.$id; 
        $query = $this->DB8->query($strq);
        return $query;
    }
    function rentaventasaccesoriosd_dev($id){
        $strq = 'SELECT r_a.id_accesoriod,r_a.cantidad,bod.bodega,acc.no_parte,acc.nombre,bod.bodegaId
                FROM rentas_has_detallesEquipos_accesorios_dev as r_a 
                LEFT JOIN bodegas as bod on bod.bodegaId=r_a.serie_bodega 
                LEFT JOIN catalogo_accesorios as acc on acc.id=r_a.id_accesorio 
                WHERE r_a.idrentas='.$id; 
        $query = $this->DB8->query($strq);
        return $query;
    }
    function rentaventasdh($id,$idhistorial){
        $strq = "SELECT r.*, b.bodega, e.noparte,ce.nombre AS categoria,
                        per.nombre as personal, per.apellido_paterno,b.bodegaId,
                        reneqh.historialid as id_rh
                FROM rentas_has_detallesEquipos AS r 
                 LEFT JOIN bodegas AS b ON b.bodegaId = r.serie_bodega
                 LEFT JOIN equipos AS e ON e.id =r.idEquipo
                 LEFT JOIN categoria_equipos AS ce ON ce.id = e.categoriaId
                 inner join rentas_historial_equipos as reneqh on reneqh.equipo=r.id
                 left join personal as per on per.personalId=r.entregado_personal
                 WHERE r.idRenta=$id and reneqh.historialid=$idhistorial"; 
        $query = $this->db->query($strq);
        return $query;
    }
    function rentavconsumiblesh($id,$idhistorial){
        $strq = "SELECT rhc.cantidad,con.modelo,con.parte,rhc.folios,rdec.rowequipo,rdec.id,
                        per.nombre as personal, per.apellido_paterno,rdec.entregado_status,rdec.entregado_fecha,rdec.bodega,rdec.rowequipo,rdec.idequipo,b.bodega as bodegav,b.bodegaId,conf.preren,conf.idcontratofolio
                FROM `rentas_historial_consumibles` as rhc 
                INNER JOIN consumibles as con on con.id=rhc.consumible 

                left join contrato_folio as conf on conf.foliotext=rhc.folios
                inner join rentas_has_detallesequipos_consumible as rdec on rdec.id=conf.idrelacion
                left join personal as per on per.personalId=rdec.entregado_personal
                LEFT JOIN bodegas AS b ON b.bodegaId = rdec.bodega
                WHERE rhc.historialid=$idhistorial"; 
        $query = $this->db->query($strq);
        return $query;
    }
    function rentavconsumiblesh_dev($id,$idhistorial){
        $strq = "SELECT rhc.cantidad,con.modelo,con.parte,rhc.folios,rdec.rowequipo,rdec.id,
                        per.nombre as personal, per.apellido_paterno,rdec.entregado_status,rdec.entregado_fecha,
                        rdec.motivo_dev,rdec.reg_dev
                FROM `rentas_historial_consumibles` as rhc 
                INNER JOIN consumibles as con on con.id=rhc.consumible 

                left join contrato_folio as conf on conf.foliotext=rhc.folios
                inner join rentas_has_detallesequipos_consumible_dev as rdec on rdec.id=conf.idrelacion
                left join personal as per on per.personalId=rdec.entregado_personal
                WHERE rhc.historialid=$idhistorial"; 
        $query = $this->db->query($strq);
        return $query;
    }
    function rentavacessoriosh($id,$idhistorial){
        $strq = "   SELECT rha.id_accesoriod,rha.cantidad,caa.nombre,caa.no_parte,rea.rowequipo,
                    per.nombre as personal,per.apellido_paterno,rea.entregado_status,rea.entregado_fecha,rea.id_equipo,rea.rowequipo,b.bodega,b.bodegaId 
                    FROM `rentas_historial_accesorios` as rha 
                    INNER JOIN catalogo_accesorios as caa on caa.id=rha.accesorio 
                    inner join rentas_has_detallesEquipos_accesorios as rea on rea.id_accesoriod=rha.id_accesoriod
                    left join personal as per on per.personalId=rea.entregado_personal
                    LEFT JOIN bodegas AS b ON b.bodegaId = rea.serie_bodega
                    where rha.historialid=$idhistorial"; 
        $query = $this->DB8->query($strq);
        return $query;
    }
    function rentavacessoriosh_dev($id,$idhistorial){
        $strq = "   SELECT rha.id_accesoriod,rha.cantidad,caa.nombre,caa.no_parte,rea.rowequipo,
                    per.nombre as personal,per.apellido_paterno,rea.entregado_status,rea.entregado_fecha,
                    rea.motivo_dev,rea.reg_dev
                    FROM `rentas_historial_accesorios` as rha 
                    INNER JOIN catalogo_accesorios as caa on caa.id=rha.accesorio 
                    inner join rentas_has_detallesEquipos_accesorios_dev as rea on rea.id_accesoriod=rha.id_accesoriod
                    left join personal as per on per.personalId=rea.entregado_personal

                    where rha.historialid=$idhistorial"; 
        $query = $this->DB8->query($strq);
        return $query;
    }
    function rentaequipos_consumible($idrenta,$idequipo,$idrow){
        $strq = "SELECT rhdc.id,c.modelo, c.parte, rhdc.cantidad,rhdc.id_consumibles, 
                        rhdc.entregado_fecha,rhdc.entregado_status,per.nombre as personal, per.apellido_paterno,bod.bodega,bod.bodegaId
                 FROM rentas_has_detallesequipos_consumible AS rhdc
                 INNER JOIN consumibles AS c ON c.id = rhdc.id_consumibles
                 left join personal as per on per.personalId=rhdc.entregado_personal
                 left JOIN bodegas as bod on bod.bodegaId=rhdc.bodega
                 WHERE rhdc.idrentas=$idrenta AND  rhdc.idequipo=$idequipo and (rhdc.rowequipo=$idrow or rhdc.rowequipo is null ) limit 200"; 
        $query = $this->DB8->query($strq);
        return $query;
    }
    function rentaequipos_consumible_dev($idrenta,$idequipo,$idrow){
        $strq = "SELECT rhdc.id,c.modelo, c.parte, rhdc.cantidad,rhdc.id_consumibles, 
                        rhdc.entregado_fecha,rhdc.entregado_status,per.nombre as personal, per.apellido_paterno,bod.bodega
                 FROM rentas_has_detallesequipos_consumible_dev AS rhdc
                 INNER JOIN consumibles AS c ON c.id = rhdc.id_consumibles
                 left join personal as per on per.personalId=rhdc.entregado_personal
                 left JOIN bodegas as bod on bod.bodegaId=rhdc.bodega
                 WHERE rhdc.idrentas=$idrenta AND  rhdc.idequipo=$idequipo and (rhdc.rowequipo=$idrow or rhdc.rowequipo is null )"; 
        $query = $this->db->query($strq);
        return $query;
    }
    function rentaequipos_consumible0($idrenta,$idequipo,$idrow){
        $strq = "SELECT rhdc.id,c.modelo, c.parte, rhdc.cantidad,rhdc.id_consumibles 
                 FROM rentas_has_detallesequipos_consumible AS rhdc
                 INNER JOIN consumibles AS c ON c.id = rhdc.id_consumibles
                 WHERE rhdc.idrentas=$idrenta AND (rhdc.rowequipo=0 or rhdc.rowequipo is null )"; 
        //log_message('error', 'rentaequipos_consumible0: '.$strq);
        $query = $this->db->query($strq);
        return $query;
    }
    function rentaequipos_consumible1($idrenta,$idequipo,$idrow){
        $strq = "SELECT rhdc.id,c.modelo, c.parte, rhdc.cantidad,rhdc.id_consumibles 
                 FROM rentas_has_detallesequipos_consumible AS rhdc
                 INNER JOIN consumibles AS c ON c.id = rhdc.id_consumibles
                 WHERE rhdc.idrentas=$idrenta AND  rhdc.idequipo=$idequipo and rhdc.rowequipo=$idrow"; 
        //log_message('error', 'rentaequipos_consumible1: '.$strq);
        $query = $this->db->query($strq);
        return $query;
    }
    function obtener_consumibles_con_folios($idrenta,$idequipo,$idrow,$serieId){
        $strq = "SELECT 1 as tipo_c_a,rhdc.id,c.modelo, c.parte, rhdc.cantidad,rhdc.id_consumibles,rhdc.idrentas,cf.idcontratofolio,cf.foliotext,
                '' as id_accesoriod, '' as no_parte, '' as nombre,'' as series, '' as personal,'' as apellido_paterno,'' as entregado_status,'' as entregado_fecha,'' as serie_bodega,'' as bodega,'' as bodegaId,'' as id_accesorio,'' as serieId
                 FROM rentas_has_detallesequipos_consumible AS rhdc
                 INNER JOIN consumibles AS c ON c.id = rhdc.id_consumibles
                 LEFT JOIN contrato_folio as cf on cf.idrenta=rhdc.idrentas AND cf.idconsumible=rhdc.id_consumibles AND (cf.serieId='$serieId' OR cf.idrelacion=rhdc.id) and cf.status!=2
                 WHERE rhdc.idrentas='$idrenta' AND  rhdc.idequipo='$idequipo' and rhdc.rowequipo='$idrow' and cf.idcontratofolio>0 GROUP BY cf.idcontratofolio"; 
        $strq.=" UNION ";
        $strq.="SELECT 2 as tipo_c_a,0 as id,'' as modelo,'' as parte,rhd.cantidad,'' as id_consumibles,rhd.idrentas, 0 as idcontratofolio,'' as foliotext,
                rhd.id_accesoriod, e.no_parte, e.nombre,GROUP_CONCAT(seacc.serie) as series,
                per.nombre as personal,per.apellido_paterno,rhd.entregado_status,rhd.entregado_fecha,rhd.serie_bodega,bod.bodega,bod.bodegaId,rhd.id_accesorio,seacc.serieId
                 FROM rentas_has_detallesEquipos_accesorios AS rhd
                 INNER JOIN catalogo_accesorios AS e ON e.id = rhd.id_accesorio 
                 left join asignacion_series_r_accesorios as sacc on sacc.id_accesorio= rhd.id_accesoriod
                 left join series_accesorios as seacc on seacc.serieId= sacc.serieId
                 left join personal as per on per.personalId=rhd.entregado_personal
                 left JOIN bodegas as bod on bod.bodegaId=rhd.serie_bodega
                 WHERE rhd.idrentas='$idrenta' AND rhd.id_equipo='$idequipo' and (rhd.rowequipo='$idrow' or rhd.rowequipo is null)
                 group by rhd.id_accesoriod";
                 $strq.=" ORDER BY `tipo_c_a` DESC ";

        //log_message('error', 'rentaequipos_consumible1: '.$strq);
        $query = $this->DB7->query($strq);
        return $query;
    }
    function rentaequipos_consumible_total($idrenta,$idequipo){
        $strq = "SELECT COUNT(*) AS totalc FROM rentas_has_detallesequipos_consumible AS rhdc
                 INNER JOIN consumibles AS c ON c.id = rhdc.id_consumibles
                 WHERE rhdc.idrentas=$idrenta AND rhdc.idequipo=$idequipo"; 
        $query = $this->DB8->query($strq);
        return $query;
    }
    function rentaequipos_accesorios($idrenta,$idequipo,$idrow){
        /*
        $strq = "SELECT rhd.cantidad, e.no_parte, e.nombre FROM rentas_has_detallesEquipos_accesorios AS rhd
                 INNER JOIN catalogo_accesorios AS e ON e.id = rhd.id_accesorio 
                 WHERE rhd.idrentas=$idrenta AND rhd.id_equipo=$idequipo
                 group by rhd.id_accesoriod
                 ";
        */
        //log_message('error', 'series: '.$strq);
        
        $strq = "SELECT rhd.id_accesoriod,rhd.cantidad, e.no_parte, e.nombre,GROUP_CONCAT(seacc.serie) as series,
                        per.nombre as personal,per.apellido_paterno,rhd.entregado_status,rhd.entregado_fecha,rhd.serie_bodega,bod.bodega,bod.bodegaId,rhd.id_accesorio,seacc.serieId
                 FROM rentas_has_detallesEquipos_accesorios AS rhd
                 INNER JOIN catalogo_accesorios AS e ON e.id = rhd.id_accesorio 
                 left join asignacion_series_r_accesorios as sacc on sacc.id_accesorio= rhd.id_accesoriod
                 left join series_accesorios as seacc on seacc.serieId= sacc.serieId
                 left join personal as per on per.personalId=rhd.entregado_personal
                 left JOIN bodegas as bod on bod.bodegaId=rhd.serie_bodega
                 WHERE rhd.idrentas=$idrenta AND rhd.id_equipo=$idequipo and (rhd.rowequipo=$idrow or rhd.rowequipo is null)
                 group by rhd.id_accesoriod"; 
        
        $query = $this->DB8->query($strq);
        return $query;
    }
    function rentaequipos_accesorios_rowequipo($idrow){
        
        $strq = "SELECT rhd.id_accesoriod,rhd.cantidad, e.no_parte, e.nombre,GROUP_CONCAT(seacc.serie) as series,
                        per.nombre as personal,per.apellido_paterno,rhd.entregado_status,rhd.entregado_fecha
                 FROM rentas_has_detallesEquipos_accesorios AS rhd
                 INNER JOIN catalogo_accesorios AS e ON e.id = rhd.id_accesorio 
                 left join asignacion_series_r_accesorios as sacc on sacc.id_accesorio= rhd.id_accesoriod
                 left join series_accesorios as seacc on seacc.serieId= sacc.serieId
                 left join personal as per on per.personalId=rhd.entregado_personal
                 WHERE rhd.rowequipo=$idrow 
                 group by rhd.id_accesoriod"; 
        
        $query = $this->DB8->query($strq);
        return $query;
    }
    //accesorios relacionados(estructura igual a rentaequipos_accesorios)
    function rentaequipos_accesorios1($idrenta,$idequipo,$idrow){
        $strq = "SELECT rhd.id_accesoriod,rhd.cantidad, e.no_parte, e.nombre,GROUP_CONCAT(seacc.serie) as series
                 FROM rentas_has_detallesEquipos_accesorios AS rhd
                 INNER JOIN catalogo_accesorios AS e ON e.id = rhd.id_accesorio 
                 left join asignacion_series_r_accesorios as sacc on sacc.id_accesorio= rhd.id_accesoriod
                 left join series_accesorios as seacc on seacc.serieId= sacc.serieId
                 WHERE rhd.idrentas=$idrenta AND rhd.id_equipo=$idequipo and rhd.rowequipo=$idrow
                 group by rhd.id_accesoriod"; 
        
        $query = $this->DB8->query($strq);
        return $query;
    }
    //accesorios sin relacionar(estructura igual a rentaequipos_accesorios)
    function rentaequipos_accesorios0($idrenta,$idequipo,$idrow){
        $strq = "SELECT rhd.id_accesoriod,rhd.cantidad, e.no_parte, e.nombre,GROUP_CONCAT(seacc.serie) as series
                 FROM rentas_has_detallesEquipos_accesorios AS rhd
                 INNER JOIN catalogo_accesorios AS e ON e.id = rhd.id_accesorio 
                 left join asignacion_series_r_accesorios as sacc on sacc.id_accesorio= rhd.id_accesoriod
                 left join series_accesorios as seacc on seacc.serieId= sacc.serieId
                 WHERE rhd.idrentas=$idrenta and (rhd.rowequipo=0 or rhd.rowequipo is null)
                 group by rhd.id_accesoriod"; 
        
        $query = $this->DB8->query($strq);
        return $query;
    }
    function rentaequipos_accesorios_total($idrenta,$idequipo){
        $strq = "SELECT COUNT(*) AS totala FROM rentas_has_detallesEquipos_accesorios AS rhd
                 INNER JOIN accesorios AS a ON a.id = rhd.id_accesorio  
                 INNER JOIN catalogo_accesorios AS e ON e.id = a.idcatalogo_accesorio 
                 WHERE rhd.idrentas=$idrenta AND rhd.id_equipo=$idequipo"; 
        $query = $this->DB8->query($strq);
        return $query;
    }
    function rentaventasdseries($id){
        $strq = 'SELECT sp.*,asre.folio_equipo,asre.bodega_or,bod.bodega FROM rentas_has_detallesEquipos AS rhd
                 INNER JOIN asignacion_series_r_equipos AS asre ON asre.id_equipo = rhd.id
                 INNER JOIN series_productos AS sp ON sp.serieId = asre.serieId
                 LEFT JOIN bodegas as bod on bod.bodegaId=asre.bodega_or
                 WHERE  asre.id_equipo ='.$id.' and asre.activo=1 GROUP BY sp.serieId';
        $query = $this->db->query($strq);
        return $query;
    }
    function rentaventasdseries_renta($idRenta){
        $strq = 'SELECT sp.*,asre.folio_equipo,asre.id_equipo,asre.serieId
                 FROM rentas_has_detallesEquipos AS rhd
                 INNER JOIN asignacion_series_r_equipos AS asre ON asre.id_equipo = rhd.id
                 INNER JOIN series_productos AS sp ON sp.serieId = asre.serieId
                 WHERE  rhd.idRenta ='.$idRenta.' and asre.activo=1 GROUP BY sp.serieId';
        $query = $this->DB8->query($strq);
        return $query;
    }
    function rentaventasaccessoriosseries($id){
        if($id>0){
            $id=$id;
        }else{
            $id=0;
        }
        $strq = "SELECT sacc.serie FROM asignacion_series_r_accesorios as aser 
                INNER JOIN series_accesorios as sacc on sacc.serieId=aser.serieId
                WHERE aser.id_accesorio='$id'";
        $query = $this->db->query($strq);
        return $query;
    }
    function getviewpermiso($perfil,$modulo){
        $strq = "SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=$perfil AND MenusubId=$modulo";
        $query = $this->DB10->query($strq);
        $total=0;
        foreach ($query->result() as $row) {
            $total=$row->total;
        }
        return $total; 
    }
    function getequiposerie($id){
        $strq = 'SELECT ase.id_asig,spr.serie,ase.reg ,ase.serieId,bod.bodega,bod.bodegaId
                from ventas_has_detallesEquipos as eq
                left JOIN asignacion_series_equipos as ase on ase.id_equipo=eq.id
                left JOIN series_productos as spr on spr.serieId=ase.serieId
                left join bodegas as bod on bod.bodegaId=spr.bodegaId
                WHERE eq.id='.$id; 
        $query = $this->db->query($strq);
        return $query;
    }
    function getequiposerie_venta($idventa){
        $strq = "SELECT eq.id,eq.cantidad,eq.modelo,eq.serie_bodega,bod.bodega,GROUP_CONCAT(spr.serie) as series
                FROM ventas_has_detallesEquipos as eq 
                left JOIN asignacion_series_equipos as ase on ase.id_equipo=eq.id
                left JOIN series_productos as spr on spr.serieId=ase.serieId
                left join bodegas as bod on bod.bodegaId=spr.bodegaId
                WHERE eq.idVenta='$idventa'
                GROUP BY eq.id"; 
        $query = $this->db->query($strq);
        return $query;
    }
    function getequiposerie_vd($id){
        $strq = 'SELECT ase.id_asig,spr.serie,ase.reg ,ase.serieId,bod.bodega
                FROM ventasd_asignacion_series_equipos as ase
                inner JOIN series_productos as spr on spr.serieId=ase.serieId
                inner join bodegas as bod on bod.bodegaId=spr.bodegaId
                WHERE ase.id_equipo='.$id; 
        $query = $this->DB8->query($strq);
        return $query;
    }
    function getaccesorioserie($id){
        $strq = 'SELECT ase.id_asig,spr.serie,ase.reg ,ase.serieId,spr.con_serie
                    FROM asignacion_series_accesorios as ase
                    left JOIN series_accesorios as spr on spr.serieId=ase.serieId
                    WHERE ase.id_accesorio='.$id; 
        $query = $this->DB8->query($strq);
        return $query;
    }
    function getaccesorioserie_vd($id){
        $strq = 'SELECT ase.id_asig,spr.serie,ase.reg ,ase.serieId
                    FROM ventasd_asignacion_series_accesorios as ase
                    inner JOIN series_accesorios as spr on spr.serieId=ase.serieId
                    WHERE ase.id_accesorio='.$id; 
        $query = $this->DB8->query($strq);
        return $query;
    }
    // rentas accesorios 
    function getrefaccionserie($id){
        $strq ="SELECT ase.id_asig,ase.cantidad,spr.serie,ase.reg,ase.serieId,bod.bodega,spr.con_serie,spr.bodegaId,ase.usado
        FROM asignacion_series_refacciones as ase 
        INNER JOIN ventas_has_detallesRefacciones as vr on vr.id=ase.id_refaccion
        inner JOIN series_refacciones as spr on spr.serieId=ase.serieId 
        inner join bodegas as bod on bod.bodegaId=vr.serie_bodega
        WHERE ase.id_refaccion='$id' GROUP by ase.id_refaccion,ase.serieId";
        /*$strq = "SELECT ase.id_asig,ase.cantidad,spr.serie,ase.reg,ase.serieId,bod.bodega,spr.con_serie,spr.bodegaId
                FROM asignacion_series_refacciones as ase 
                inner JOIN series_refacciones as spr on spr.serieId=ase.serieId 
                inner join bodegas as bod on bod.bodegaId=spr.bodegaId
                WHERE ase.id_refaccion='$id' GROUP by ase.id_refaccion,ase.serieId";
                */
        $query = $this->db->query($strq);
        return $query;
    }
    function getrefaccionserie_vd($id){
        $strq = 'SELECT ase.id_asig,ase.cantidad,spr.serie,ase.reg,ase.serieId,bod.bodega
                FROM ventasd_asignacion_series_refacciones as ase 
                inner JOIN series_refacciones as spr on spr.serieId=ase.serieId 
                inner join bodegas as bod on bod.bodegaId=spr.bodegaId
                WHERE ase.id_refaccion='.$id.''; 
        $query = $this->db->query($strq);
        return $query;
    }
    function getrefaccionserierentas($id){
        $strq = 'SELECT ase.id_asig,spr.serie,ase.reg ,ase.serieId
                    FROM asignacion_series_r_accesorios as ase
                    inner JOIN series_accesorios as spr on spr.serieId=ase.serieId
                    WHERE ase.id_accesorio='.$id; 
        $query = $this->DB8->query($strq);
        return $query;
    }
    //
    // rentas equipos
    public function getequiposerierentas($id){
        $strq='SELECT ase.id_asig,spr.serie,ase.reg,ase.serieId 
               FROM asignacion_series_r_equipos AS ase
               INNER JOIN series_productos AS spr ON spr.serieId=ase.serieId 
               WHERE ase.id_equipo='.$id;
        $query = $this->DB8->query($strq); 
        return $query;      
    }
    //
    function getalta_rentas_prefactura($id){
        if($id==''){$id=0;}
        $strq = 'SELECT *
                FROM alta_rentas_prefactura WHERE prefId='.$id.' ORDER BY prefId  DESC LIMIT 1'; 
        $query = $this->db->query($strq);
        return $query;
    }
    function getalta_rentas_prefactura0($id){
        if($id==''){$id=0;}
        $strq = 'SELECT *
                FROM alta_rentas_prefactura WHERE id_renta='.$id.' ORDER BY periodo_inicial  DESC LIMIT 1'; 
        $query = $this->db->query($strq);
        return $query;
    }
    function viewprefacturas($id){
        $strq = "SELECT arpf.*,per.nombre,per.apellido_paterno,per.apellido_materno
                FROM alta_rentas_prefactura as arpf
                inner join personal as per on per.personalId=arpf.personalId
                where arpf.id_renta='$id' ORDER BY arpf.prefId DESC"; 
        $query = $this->db->query($strq);
        return $query;
    }
    
    function viewprefacturas_folio($id){
        $strq = "SELECT fp.facId, fac.Folio,fac.total,fac.Estado 
                FROM factura_prefactura as fp 
                INNER JOIN f_facturas as fac on fac.FacturasId=fp.facturaId 
                WHERE fp.prefacturaId='$id' and (fac.Estado=1 or fac.Estado=0) "; 
        $query = $this->db->query($strq);
        return $query->result();
    }

    function viewprefacturaslis($contrato,$tipo){
        if ($tipo>0) {
            if ($tipo==1) {
                $wheretipo=' and facpre.statuspago=0';
            }else{
                $wheretipo=' and facpre.statuspago=1';
            }
            
        }else{
            $wheretipo='';
        }
        /*
        $strq = "SELECT * 
                    FROM factura_prefactura as facpre
                    inner JOIN f_facturas as fac on fac.FacturasId=facpre.facturaId
                    WHERE facpre.contratoId=$contrato $wheretipo"; 
        */
        $strq = "SELECT * from (SELECT 
                    facpre.*,
                    fac.*,
                    apre.periodo_inicial,
                    apre.periodo_final
                FROM factura_prefactura as facpre
                inner JOIN alta_rentas_prefactura as apre on apre.prefId=facpre.prefacturaId
                inner JOIN f_facturas as fac on fac.FacturasId=facpre.facturaId and (fac.Estado=1 or fac.Estado=0) 
                WHERE facpre.contratoId=$contrato $wheretipo

                UNION
                
                SELECT 
                    facpre.*,
                    face.*,
                    apre.periodo_inicial,
                    apre.periodo_final
                FROM factura_prefactura as facpre
                inner JOIN alta_rentas_prefactura as apre on apre.prefId=facpre.prefacturaId
                inner JOIN f_facturas as fac on fac.FacturasId=facpre.facturaId and (fac.Estado=1 or fac.Estado=0) 
                INNER JOIN f_facturas as face on face.f_r_uuid=fac.uuid and (face.Estado=1 or face.Estado=0) 
                WHERE facpre.contratoId=$contrato $wheretipo and face.f_relacion=1 ) as datos
                GROUP BY prefacturaId,facturaId";
        $query = $this->db->query($strq);
        return $query;
    }
    function viewprefacturaslis_general($contrato){
        $strq = "SELECT facpre.*,fac.*,facprep.*,arp.periodo_inicial,arp.periodo_final,sum(facprep.pago) as pagot, max(facprep.fecha) as fechamax
                FROM factura_prefactura as facpre
                INNER JOIN alta_rentas_prefactura as arp on arp.prefId=facpre.prefacturaId
                inner JOIN f_facturas as fac on fac.FacturasId=facpre.facturaId
                left join factura_prefactura_pagos as facprep on facprep.facId=facpre.facId 
                WHERE fac.Estado=1 and facpre.contratoId=$contrato GROUP by fac.FacturasId"; 
        $query = $this->db->query($strq);
        return $query;
    }
    function viewprefacturaslis_general2($contrato){
        $strq = "SELECT arp.prefId,arp.periodo_inicial,arp.periodo_final,facpre.*,fac.*,facprep.*,sum(facprep.pago) as pagot, max(facprep.fecha) as fechamax,arp.total as total_periodo,facpre.facId as fp_facId
                FROM alta_rentas_prefactura as arp
                inner join alta_rentas as ar on ar.id_renta=arp.id_renta
                left join factura_prefactura as facpre on arp.prefId=facpre.prefacturaId
                left JOIN f_facturas as fac on fac.FacturasId=facpre.facturaId and fac.Estado=1
                left join factura_prefactura_pagos as facprep on facprep.facId=facpre.facId 
                WHERE ar.idcontrato=$contrato GROUP by fac.FacturasId,arp.prefId
                ORDER BY arp.prefId  DESC"; 
        $query = $this->db->query($strq);
        return $query;
    }
    function contratofolio($id){
        $sql = "SELECT * FROM contrato_folio WHERE idCliente = $id";
        $query = $this->db->query($sql);
        return $query->result();
    }
    function contrato_renta($id){
        $sql = "SELECT idCliente FROM rentas WHERE id=$id";
        $query = $this->db->query($sql);
        return $query->result();
    }
    function getlistcontratofolio($tecnico){
        if ($tecnico!='') {
            $wheretecnico="where tecnico='$tecnico'";
        }else{
            $wheretecnico="";
        }
        $sql = "SELECT * from ( 
                    SELECT cf.*,c.modelo,c.parte,p.nombre,r.reg,cli.empresa,
                    (SELECT con.idcontrato FROM contrato as con WHERE con.idRenta=cf.idrenta ORDER BY con.idcontrato DESC LIMIT 1) as idcontrato,
                    (
                        select concat(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno)
                        from asignacion_ser_contrato_a_e as asig 
                        inner join personal as per on per.personalId=asig.tecnico
                        where asig.asignacionId= cf.serviciocId limit 1
                    ) as tecnico,r.estatus
                    FROM contrato_folio AS cf
                    INNER JOIN consumibles AS c ON c.id = cf.idconsumible
                    INNER JOIN rentas AS r ON r.id = cf.idrenta
                    INNER JOIN personal AS p ON p.personalId = r.id_personal
                    INNER JOIN clientes AS cli ON cli.id = r.idCliente
                    where cf.status=0 
                ) as datos 
                $wheretecnico ";
        $query = $this->DB8->query($sql);
        return $query->result();
    }
    function getlistcontratofolioid($id){
        $sql = "SELECT cf.*,c.modelo,c.parte,p.nombre,r.reg,cli.empresa FROM contrato_folio AS cf
                INNER JOIN consumibles AS c ON c.id = cf.idconsumible
                INNER JOIN rentas AS r ON r.id = cf.idrenta
                INNER JOIN personal AS p ON p.personalId = r.id_personal
                INNER JOIN clientes AS cli ON cli.id = r.idCliente AND cf.idcontratofolio=$id";
        $query = $this->DB8->query($sql);
        return $query->result();
    }
    
    function getlistfoliostatuscli(){
        $sql = "SELECT cli.id,cli.empresa 
                FROM contrato_folio AS cf
                INNER JOIN rentas AS r ON r.id = cf.idrenta
                INNER JOIN clientes AS cli ON cli.id = r.idCliente
                WHERE cf.status >= 1 GROUP BY cli.id ";
        $query = $this->db->query($sql);
        return $query->result();   
    }
    function getlistfoliocodigo($codigo){
        $sql = "SELECT cf.*,c.modelo,c.parte,p.nombre,r.reg,cli.empresa 
                FROM contrato_folio AS cf
                INNER JOIN consumibles AS c ON c.id = cf.idconsumible
                INNER JOIN rentas AS r ON r.id = cf.idrenta
                INNER JOIN personal AS p ON p.personalId = r.id_personal
                INNER JOIN clientes AS cli ON cli.id = r.idCliente
                WHERE cf.foliotext= '$codigo' AND cf.status=1";
        $query = $this->DB10->query($sql);
        return $query->result();   
    }
    function viewlisfolioscli($contrato){
        $sql = "SELECT 
                    contf.idcontratofolio,
                    contf.idconsumible,
                    cons.modelo,
                    contf.folio,
                    contf.foliotext,
                    contf.fechagenera,
                    contf.fecharetorno,
                    contf.status,
                    contf.comentario,
                    (
                        select concat(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno)
                        from asignacion_ser_contrato_a_e as asig 
                        inner join personal as per on per.personalId=asig.tecnico
                        where asig.asignacionId= contf.serviciocId limit 1
                    ) as tecnico,
                    (
                        select concat(peri.periodo_inicial,'- ',peri.periodo_final)
                        from alta_rentas_prefactura as peri
                        inner join alta_rentas as arent on arent.id_renta=peri.id_renta
                        where arent.idcontrato=cont.idcontrato and 
                                (
                                    (contf.fecha BETWEEN peri.periodo_inicial and peri.periodo_final)                                 )
                    ) as periodo 
                FROM contrato as cont
                inner join contrato_folio as contf on contf.idrenta=cont.idRenta
                inner join consumibles as cons on cons.id=contf.idconsumible
                WHERE cont.idcontrato=".$contrato;
        $query = $this->DB10->query($sql);
        return $query->result();   
    }
    function datosequiporentafactura($renta,$equipo,$serie,$tipo){
        $sql = "SELECT arp.id_renta,are.renta,are.rentac,rde.modelo,sep.serie,arpd.*
                FROM alta_rentas_prefactura_d as arpd
                inner join alta_rentas_prefactura as arp on arp.prefId=arpd.prefId
                inner join alta_rentas_equipos as are on are.id_renta=arp.id_renta AND are.id_equipo=arpd.productoId
                inner join rentas_has_detallesEquipos as rde on rde.id=are.id_equipo
                inner join series_productos as sep on sep.serieId=are.serieId
                WHERE arpd.prefId=$renta AND arpd.prefdId=$equipo and sep.serieId=$serie and arpd.tipo=$tipo LIMIT 1";       
        $query = $this->db->query($sql);
        return $query->result();   
    }

    function detalles_servicios_($id){
        $strq = 'SELECT * from detalles_servicios as ds
                INNER JOIN equipos AS equ on equ.id=ds.idEquipo
                WHERE ds.idCotizacion='.$id; 
        $query = $this->DB10->query($strq);
        return $query;
    }
    function detalle_cotizacion_consumible_equipos($id){
        $strq = 'SELECT cdec.id,cdec.id_cotizacion,cdec.cantidad,eq.modelo,cons.modelo as modeloc,cdec.id_consumibles
                FROM cotizaciones_has_detallesEquipos_consumibles as cdec
                inner JOIN equipos as eq on eq.id=cdec.id_equipo
                inner JOIN consumibles as cons on cons.id=cdec.id_consumibles
                WHERE cdec.id_cotizacion='.$id; 
        $query = $this->db->query($strq);
        return $query;
    }
    function detalle_cotizacion_accesorios_equipos($id){
        $strq = 'SELECT cdea.id_accesoriod,cdea.id_cotizacion,cdea.cantidad,eq.modelo,cacce.nombre,cacce.id
                FROM cotizaciones_has_detallesEquipos_accesorios as cdea
                inner JOIN equipos as eq on eq.id=cdea.id_equipo
                inner JOIN catalogo_accesorios as cacce on cacce.id=cdea.id_accesorio
                WHERE cdea.id_cotizacion='.$id; 
        $query = $this->db->query($strq);
        return $query;
    }
    function obtenerbodega($id){
        if($id>0){

        }else{
            $id=0;            
        }
        $strq = 'SELECT * FROM `bodegas`
                WHERE bodegaId='.$id; 
        $query = $this->db->query($strq);
        $bodega='';
        foreach ($query->result() as $item) {
            $bodega=$item->bodega;
        }
        return $bodega;
    }
    public function pagos_combinada($id){
        $strq = 'SELECT p.idpago,p.fecha,f.formapago_text,p.pago,p.comprobante,p.observacion,p.confirmado,
                per.nombre,per.apellido_paterno,per.apellido_materno,p.reg,per.personalId
                FROM pagos_combinada as p
                inner JOIN f_formapago as f on f.id=p.idmetodo
                left join personal as per on per.personalId=p.idpersonal
                WHERE p.idventa='.$id; 
        $query = $this->db->query($strq);
        return $query->result();
    }
    public function pagos_ventas($id){
        $strq = 'SELECT p.idpago,p.fecha,f.formapago_text,p.pago,p.comprobante,p.observacion,p.confirmado,
                per.nombre,per.apellido_paterno,per.apellido_materno,p.reg,per.personalId
                FROM pagos_ventas as p
                inner JOIN f_formapago as f on f.id=p.idmetodo
                left join personal as per on per.personalId=p.idpersonal
                WHERE p.idventa='.$id; 
        $query = $this->db->query($strq);
        return $query->result();
    }
    public function pagos_v_vc_p_factura($idfac){
        $strq="SELECT fac.FacturasId,ppol.fecha,fp.formapago_text,ROUND(ppol.pago,2) as pago,2 as tipo
                FROM f_facturas as fac
                INNER JOIN factura_poliza as facp on facp.facturaId=fac.FacturasId
                INNER JOIN pagos_poliza as ppol on ppol.idpoliza=facp.polizaId
                INNER JOIN f_formapago as fp on fp.id=ppol.idmetodo
                WHERE fac.FacturasId='$idfac'
                UNION
                SELECT fac.FacturasId,pv.fecha,fp.formapago_text,ROUND(pv.pago,2) as pago,0 as tipo
                FROM f_facturas as fac
                INNER JOIN factura_venta as facv on facv.facturaId=fac.FacturasId AND facv.combinada=0
                INNER JOIN pagos_ventas as pv on pv.idventa=facv.ventaId
                INNER JOIN f_formapago as fp on fp.id=pv.idmetodo
                WHERE fac.FacturasId='$idfac'
                UNION
                SELECT fac.FacturasId,pc.fecha,fp.formapago_text,ROUND(pc.pago,2) as pago,1 as tipo
                FROM f_facturas as fac
                INNER JOIN factura_venta as facv on facv.facturaId=fac.FacturasId AND facv.combinada=1
                INNER JOIN pagos_combinada as pc on pc.idventa=facv.ventaId
                INNER JOIN f_formapago as fp on fp.id=pc.idmetodo
                WHERE fac.FacturasId='$idfac'";
        $query = $this->db->query($strq);
        return $query->result();
    }
    public function pagos_ventas_vd($id){
        $strq = 'SELECT p.idpago,p.fecha,f.formapago_text,p.pago,p.comprobante,p.observacion,p.confirmado,per.personalId
                FROM ventasd_pagos as p
                inner JOIN f_formapago as f on f.id=p.idmetodo
                WHERE p.idventa='.$id; 
        $query = $this->db->query($strq);
        return $query->result();
    }
    public function pagos_factura($id){
        $strq = 'SELECT p.fp_pagosId,p.fecha,f.formapago_text,p.pago,p.comprobante,p.observacion,p.confirmado,
                per.nombre,per.apellido_paterno,per.apellido_materno,p.reg,per.personalId
                FROM factura_prefactura_pagos as p
                inner JOIN f_formapago as f on f.id=p.idmetodo
                left join personal as per on per.personalId=p.idpersonal
                WHERE p.facId='.$id; 
        $query = $this->db->query($strq);
        return $query->result();
    }
    public function pagos_poliza($id){
        $strq = 'SELECT p.idpago,p.fecha,f.formapago_text,p.pago,p.comprobante,p.observacion,p.confirmado,
                per.nombre,per.apellido_paterno,per.apellido_materno,p.reg,per.personalId
                FROM pagos_poliza as p
                inner JOIN f_formapago as f on f.id=p.idmetodo
                left join personal as per on per.personalId=p.idpersonal
                WHERE p.idpoliza='.$id; 
        $query = $this->db->query($strq);
        return $query->result();
    }
    function equiposcontrato($id){
        $strq = 'SELECT coeq.ceId,rdq.modelo,seeq.serie,rdq.idEquipo
                from contrato_equipos as coeq
                inner JOIN rentas_has_detallesEquipos as rdq on rdq.id=coeq.equiposrow
                inner join series_productos as seeq on seeq.serieId=coeq.serieId
                WHERE coeq.contrato='.$id; 
        $query = $this->db->query($strq);
        return $query;
    }
    function ejecutivos(){
        $strq = "SELECT DISTINCT per.personalId,per.nombre,per.apellido_paterno,per.apellido_materno 
            FROM personal as per
            inner join usuarios as usu on usu.personalId=per.personalId
            where per.estatus=1 AND usu.perfilId!=1";
        $query = $this->db->query($strq);
        return $query;
    }
    function ejecutivos_rentas(){
        $strq = "SELECT DISTINCT per.personalId,per.nombre,per.apellido_paterno,per.apellido_materno 
            FROM personal as per
            inner join usuarios as usu on usu.personalId=per.personalId
            where per.estatus=1 AND usu.perfilId=7";
        $query = $this->DB10->query($strq);
        return $query;
    }
    function ejecutivos_rentas_ventas(){
        $strq = "SELECT DISTINCT per.personalId,per.nombre,per.apellido_paterno,per.apellido_materno 
            FROM personal as per
            inner join usuarios as usu on usu.personalId=per.personalId
            where per.estatus=1 group by per.personalId order by per.nombre";
        $query = $this->db->query($strq);
        return $query;
    }
    function descripcionfactura($factura){
        $strq = "SELECT * FROM f_facturas_servicios where FacturasId='$factura'";
        $query = $this->db->query($strq);
        $descripcion='';
        foreach ($query->result() as $item) {
            $descripcion.='<div>'.$item->Descripcion2.'</div>';
        }
        return $descripcion;
    }
    function infocomplementofactura($factura){
        $strq = "SELECT comd.*, com.FechaPago
                FROM f_complementopago_documento as comd 
                INNER JOIN f_complementopago as com on com.complementoId=comd.complementoId 
                WHERE com.Estado=1 AND comd.facturasId='$factura' ";
        $query = $this->db->query($strq);
        $motototal=0;
        $fecha='';
        foreach ($query->result() as $item) {
            $motototal=round($motototal+$item->ImpPagado,2);
            $fecha=$item->FechaPago;
        }
            $array=array($motototal,$fecha);
        return $array;
    }
    public function contratorentacliente($idCliente)    {
        $sql = "SELECT con.idcontrato,per.nombre,con.fechainicio,con.vigencia,con.folio,con.idRenta 
                FROM rentas AS r
                INNER JOIN clientes AS c ON c.id = r.idCliente
                INNER JOIN contrato AS con ON con.idRenta = r.id 
                INNER JOIN personal AS per ON per.personalId = con.personalId
                WHERE con.estatus>0 and r.idCliente = $idCliente";
        $query = $this->DB10->query($sql);
        return $query;
    }
    public function asignaciona_a_e($idasig)    {
        $sql = "SELECT r.modelo,s.serie
                FROM asignacion_ser_contrato_a_e AS a
                INNER JOIN rentas_has_detallesEquipos AS r ON r.id=a.idequipo
                INNER JOIN series_productos AS s ON s.serieId=a.serieId
                WHERE a.asignacionId = $idasig";
        $query = $this->DB8->query($sql);
        return $query;
    }
    public function asignaciona_p_e($idasig)    {
        $sql = "SELECT e.modelo,r.serie
                FROM asignacion_ser_poliza_a_e AS a
                INNER JOIN polizasCreadas_has_detallesPoliza AS r ON r.id=a.idequipo
                INNER JOIN equipos AS e ON e.id=r.idEquipo
                WHERE a.asignacionId = $idasig";
        $query = $this->DB10->query($sql);
        return $query;
    }
    public function doc_contrato($id){  
        $sql="SELECT
                ace.asignacionIde,
                per.personalId,
                CONCAT(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno) as tecnico,
                cli.id,
                cli.empresa,
                cli.email,
                cli.equipo_acceso,
                cli.documentacion_acceso,
                cli.protocolo_acceso,
                ace.fecha,
                ace.hora,
                ac.zonaId,
                ru.nombre,
                rde.modelo,
                serp.serie,
                ace.status,
                ac.tiposervicio,
                ace.horafin,
                ac.asignacionId,
                sereve.nombre as servicio,
                ace.t_fecha,ace.t_horainicio,ace.t_horafin,
                ace.horaserinicio,ace.horaserfin,
                ace.firma,
                ace.qrecibio,
                ace.comentario,
                ac.contratoId,
                ac.prioridad,
                ac.tserviciomotivo,
                ace.idequipo,
                ace.serieId,
                ac.reg,
                ac.tservicio_a,
                pol.nombre as poliza,
                ace.per_fin,
                CONCAT(perf.nombre, ' ', perf.apellido_paterno, ' ', perf.apellido_materno) as tecnicofin,
                ac.id_contacto_new,
                con.contacto,
                con.cargoarea,
                con.telefono
            FROM asignacion_ser_contrato_a_e AS ace 
            INNER JOIN asignacion_ser_contrato_a AS ac ON ac.asignacionId=ace.asignacionId
            LEFT JOIN personal AS per ON per.personalId=ace.tecnico
            INNER JOIN contrato AS con ON con.idcontrato=ac.contratoId
            INNER JOIN rentas AS ren ON ren.id=con.idRenta
            INNER JOIN clientes AS cli ON cli.id=ren.idCliente
            INNER JOIN rutas AS ru ON ru.id=ac.zonaId
            INNER JOIN rentas_has_detallesEquipos AS rde ON rde.id=ace.idequipo
            INNER JOIN series_productos AS serp ON serp.serieId=ace.serieId
            LEFT JOIN servicio_evento AS sereve ON sereve.id=ac.tservicio 
            LEFT JOIN polizas AS pol ON pol.id=ac.tpoliza
            LEFT JOIN personal perf ON perf.personalId=ace.per_fin
            WHERE ace.asignacionId = $id";
        $query=$this->DB10->query($sql);
        return $query->row();
    }
    public function doc_poliza($id,$ide){
        if ($ide>0) {
            $idewhere =" and ape.asignacionIde=$ide";
        }else{
            $idewhere ="";
        }
        $sql="SELECT
            ape.asignacionIde,
            per.personalId,
            CONCAT(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno) as tecnico,
            cli.id,
            cli.empresa,
            cli.email,
            cli.equipo_acceso,
            cli.documentacion_acceso,
            cli.protocolo_acceso,
            ape.fecha,
            ape.hora,
            ap.zonaId,
            ru.nombre,
            equ.modelo,
            ape.serie,
            ape.status,
            ap.tiposervicio,
            ape.horafin,
            ap.asignacionId,
            sereve.nombre as servicio,
            ape.t_fecha,ape.t_horainicio,ape.t_horafin,
            ape.horaserinicio,ape.horaserfin,
            ape.firma,
            ape.qrecibio,
            ape.comentario,
            pocde.comentario as comentariopre,
            ap.reg,
            ap.prioridad,
            ap.tserviciomotivo,
            ap.polizaId,
            pocde.direccionservicio,
            pocde.nombre as polizaser,
            ap.equipo_acceso_info,
            ap.documentacion_acceso_info,
            ap.comentariocal,
            ape.comentario_tec,
            ape.prioridad as prioridadape,
            ape.cot_refaccion,ape.cot_refaccion2,ape.cot_refaccion3,
            ape.per_fin,
            CONCAT(perf.nombre, ' ', perf.apellido_paterno, ' ', perf.apellido_materno) as tecnicofin,
            ap.id_contacto_new
        FROM asignacion_ser_poliza_a_e AS ape
        INNER JOIN asignacion_ser_poliza_a AS ap ON ap.asignacionId=ape.asignacionId
        LEFT JOIN personal AS per ON per.personalId=ape.tecnico
        INNER JOIN polizasCreadas AS poc ON poc.id=ap.polizaId
        INNER JOIN clientes AS cli ON cli.id=poc.idCliente
        INNER JOIN rutas AS ru ON ru.id=ap.zonaId
        INNER JOIN polizasCreadas_has_detallesPoliza AS pocde ON pocde.id=ape.idequipo
        INNER JOIN equipos AS equ ON equ.id=pocde.idEquipo
        LEFT JOIN servicio_evento AS sereve ON sereve.id=ap.tservicio
        LEFT JOIN personal perf ON perf.personalId=ape.per_fin

        WHERE ape.asignacionId = $id $idewhere";
        //log_message('error',$sql);
        $query=$this->DB9->query($sql);
        return $query->row();
    }
    public function doc_servicio($id,$serve=0){
        if($serve>0){
            $w_sere=" and acld.asignacionIdd='$serve' ";
        }else{
            $w_sere="";
        }
        $sql="SELECT
            acl.asignacionId,
            per.personalId,
            CONCAT(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno) as tecnico,
            cli.id,
            cli.empresa,
            cli.email,
            acl.fecha,
            acl.hora,
            acl.horafin,
            acl.zonaId,
            acl.prioridad,
            zo.nombre,
            acl.status,
            acl.tiposervicio,
            sereve.nombre as servicio,
            acl.t_fecha,acl.t_horainicio,acl.t_horafin,
            acl.firma,
            acl.qrecibio,
            acld.qrecibio as qrecibio2,
            acl.comentario,
            acl.reg,
            acl.tserviciomotivo,
            acl.documentaccesorio,
            cli.equipo_acceso,
            cli.documentacion_acceso,
            acl.equipo_acceso_info,
            acl.documentacion_acceso_info,
            acl.comentariocal,
            acld.prioridad as prioridadape,
            acld.cot_refaccion,acld.cot_refaccion2,acld.cot_refaccion3,
            acl.id_contacto_new,
            acld.comentario_tec,
            pol.nombre aS poliza,
            acl.per_fin,
            CONCAT(perf.nombre, ' ', perf.apellido_paterno, ' ', perf.apellido_materno) as tecnicofin
        FROM asignacion_ser_cliente_a AS acl
        INNER JOIN asignacion_ser_cliente_a_d as acld on acld.asignacionId=acl.asignacionId
        INNER JOIN personal AS per ON per.personalId=acl.tecnico
        INNER JOIN clientes AS cli ON cli.id=acl.clienteId
        INNER JOIN rutas AS zo ON zo.id=acl.zonaId
        LEFT JOIN servicio_evento AS sereve ON sereve.id=acl.tservicio
        LEFT JOIN polizas as pol on pol.id=acld.tpoliza
        LEFT JOIN personal perf ON perf.personalId=acl.per_fin
        WHERE acl.asignacionId = '$id' $w_sere
        group by acl.asignacionId
        ";
        $query=$this->DB7->query($sql);
        return $query->row();
    }
    public function doc_cliente_tel_cel($id){
        $sql="SELECT
            ct.tel_local,
            cc.celular,
            cdc.telefono
        FROM clientes AS acl
        left  JOIN cliente_has_telefono AS ct ON ct.idCliente=acl.id
        LEFT JOIN cliente_has_celular AS cc ON cc.idCliente=acl.id
        LEFT JOIN cliente_datoscontacto as cdc on cdc.clienteId=acl.id
        WHERE acl.id = $id
        GROUP BY acl.id
        ";
        $query=$this->DB9->query($sql);
        return $query->row();
    }
    public function doc_cliente_tel_cel2($id){
        $sql="SELECT
            ct.atencionpara,
            ct.puesto,
            ct.telefono,
            ct.celular,
            ct.email
        FROM clientes AS acl
        INNER JOIN cliente_datoscontacto AS ct ON ct.clienteId=acl.id
        WHERE acl.id = $id
        GROUP BY acl.id
        ";
        $query=$this->db->query($sql);
        return $query->row();
    }

    public function contrato_equipo($id,$ideq,$idser){
        $sql="SELECT
              acl.direccion
        FROM contrato_equipo_direccion AS acl
        WHERE acl.contrato = $id AND acl.equiposrow = $ideq AND acl.serieId = $idser
        ";
        $query=$this->DB7->query($sql);
        return $query->row();
    }
    public function clientes_direccion($id){
        $sql="SELECT
              acl.direccion
        FROM clientes_direccion AS acl
        WHERE acl.idclientedirecc = $id";
        $query=$this->DB10->query($sql);
        return $query->row();
    }
    public function clientes_direcc_fiscal($id){
        $sql="SELECT
              CONCAT(acl.colonia,' ',acl.calle,' ',acl.num_int,' ',acl.num_ext) as direccion
        FROM cliente_has_datos_fiscales AS acl
        WHERE acl.id = $id";
        $query=$this->DB10->query($sql);
        return $query->row();
    }
    public function doc_rectas($id){
        $sql="SELECT
              acl.servicio_equipo,acl.servicio_doc
        FROM alta_rentas AS acl
        WHERE acl.idcontrato = $id";
        $query=$this->db->query($sql);
        return $query->row();
    }
    public function doc_polizas_c($id){
        $sql="SELECT
              acl.direccionservicio
        FROM polizasCreadas_has_detallesPoliza AS acl
        WHERE acl.idPoliza = $id";
        $query=$this->DB7->query($sql);
        return $query->row();
    }
    /*
    function getlistfoliostatus($cliente){
        if ($cliente>0) {
           $clientewhere=' and cli.id='.$cliente;
        }else{
            $clientewhere='';
        }

        $sql = "SELECT cf.*,c.modelo,c.parte,p.nombre,r.reg,cli.empresa 
                FROM contrato_folio AS cf
                INNER JOIN consumibles AS c ON c.id = cf.idconsumible
                INNER JOIN rentas AS r ON r.id = cf.idrenta
                INNER JOIN personal AS p ON p.personalId = r.id_personal
                INNER JOIN clientes AS cli ON cli.id = r.idCliente
                WHERE cf.status >= 1 ".$clientewhere;
        $query = $this->db->query($sql);
        return $query->result();   
    }
    */
    //==============================================
    function getlistfoliostatus($params){
        $cliente=$params['cliente'];
        $estatus=$params['estatus'];
        $fi=$params['fi'];
        $ff=$params['ff'];
        $foliotext=$params['foliotext'];
        $toner = $params['toner'];
        $columns = array( 
                0=>'cf.idcontratofolio',
                1=>'c.modelo',
                2=>'c.parte',
                3=>'p.nombre',
                4=>'cf.fechagenera',
                5=>'cli.empresa',
                6=>'cf.foliotext',
                7=>'cf.status',
                8=>'cf.porcentajeretorno',
                9=>'cf.fecharetorno',
                10=>'cf.comentario',
                11=>'vdc.idVentas',
                12=>'cf.preren',
                13=>'cf.proinren',
                14=>'cf.idrenta',
                15=>'vdcd.idVentas as idVentasd',
                
        );
        /*
        10=>"(
                        select concat(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno)
                        from asignacion_ser_contrato_a_e as asig 
                        inner join personal as per on per.personalId=asig.tecnico
                        where asig.asignacionId= cf.serviciocId limit 1
                    ) as tecnico",
        */
        $columnsss = array( 
                0=>'cf.idcontratofolio',
                1=>'c.modelo',
                2=>'c.parte',
                3=>'p.nombre',
                4=>'cf.fechagenera',
                5=>'cli.empresa',
                6=>'cf.foliotext',
                7=>'cf.status',
                8=>'cf.porcentajeretorno',
                9=>'cf.fecharetorno',
                10=>'cf.comentario',
                11=>'vdc.idVentas',
                12=>'cf.preren',
                13=>'cf.proinren',
                14=>'cf.idrenta',
                
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->DB11->select($select);
        $this->DB11->from('contrato_folio AS cf');
        $this->DB11->join('consumibles c', 'c.id = cf.idconsumible');
        $this->DB11->join('rentas r', 'r.id = cf.idrenta');
        $this->DB11->join('personal p', 'p.personalId = cf.creado_personal','left');
        $this->DB11->join('clientes cli', 'cli.id = r.idCliente');
        $this->DB11->join('contrato cont', 'cont.idRenta = r.id');
        $this->DB11->join('ventas_has_detallesConsumibles vdc', 'vdc.foliotext=cf.foliotext','left');
        $this->DB11->join('ventas_has_detallesConsumibles_dev vdcd', 'vdcd.foliotext=cf.foliotext','left');
        if($estatus>0){
            $this->DB11->where(array('cf.status'=>$estatus));
        }else{
            $this->DB11->where('cf.status >=',1);
        }
        
        if ($cliente>0) {
            $this->DB11->where('cli.id',$cliente);
        }
        if($fi!=''){
            $this->DB11->where("cf.fechagenera >='$fi 00:00:00'");
        }
        if($ff!=''){
            $this->DB11->where("cf.fechagenera <='$ff 23:59:59'");
        }
        if($foliotext!=''){
            $this->DB11->where(array('cf.foliotext'=>$foliotext));
        }
        if($toner>0){
             $this->DB11->where(array('cf.idconsumible'=>$toner));
        }
            
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->DB11->group_start();
            foreach($columnsss as $c){
                $this->DB11->or_like($c,$search);
            }
            $this->DB11->group_end();  
        }            
        $this->DB11->order_by($columnsss[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->DB11->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->DB11->get();
        // print_r($query); die;
        return $query;
    }
    function getlistfoliostatust($params){
        $cliente=$params['cliente'];
        $estatus=$params['estatus'];
        $fi=$params['fi'];
        $ff=$params['ff'];
        $foliotext=$params['foliotext'];
        $toner = $params['toner'];
        $columns = array( 
                0=>'cf.idcontratofolio',
                1=>'c.modelo',
                2=>'c.parte',
                3=>'p.nombre',
                4=>'cf.fechagenera',
                5=>'cli.empresa',
                6=>'cf.foliotext',
                7=>'cf.status',
                8=>'cf.porcentajeretorno',
                9=>'cf.fecharetorno',
                10=>'cf.comentario',
                11=>'vdc.idVentas',
                12=>'cf.preren',
                13=>'cf.proinren',
                14=>'cf.idrenta',
        );
        $this->DB11->select('COUNT(*) as total');
        $this->DB11->from('contrato_folio AS cf');
        $this->DB11->join('consumibles c', 'c.id = cf.idconsumible');
        $this->DB11->join('rentas r', 'r.id = cf.idrenta');
        $this->DB11->join('personal p', 'p.personalId = cf.creado_personal','left');
        $this->DB11->join('clientes cli', 'cli.id = r.idCliente');
        $this->DB11->join('ventas_has_detallesConsumibles vdc', 'vdc.foliotext=cf.foliotext','left');

        if($estatus>0){
            $this->DB11->where(array('cf.status'=>$estatus));
        }else{
            $this->DB11->where('cf.status >=',1);
        }
        if ($cliente>0) {
            $this->DB11->where('cli.id',$cliente);
        }
        if($fi!=''){
            $this->DB11->where("cf.fechagenera >='$fi 00:00:00'");
        }
        if($ff!=''){
            $this->DB11->where("cf.fechagenera <='$ff 23:59:59'");
        }
        if($foliotext!=''){
            $this->DB11->where(array('cf.foliotext'=>$foliotext));
        }
        if($toner>0){
             $this->DB11->where(array('cf.idconsumible'=>$toner));
        }
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->DB11->group_start();
            foreach($columns as $c){
                $this->DB11->or_like($c,$search);
            }
            $this->DB11->group_end();  
        }            
        //$this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        ///$this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->DB11->get();
        // print_r($query); die;
        return $query->row()->total;
    }
    function getlistfoliostatus_export($cliente,$estatus){
        $columns = array( 
                0=>'cf.idcontratofolio',
                1=>'c.modelo',
                2=>'c.parte',
                3=>'p.nombre',
                4=>'r.reg',
                5=>'cli.empresa',
                6=>'cf.foliotext',
                7=>'cf.status',
                8=>'cf.porcentajeretorno',
                9=>'cf.fecharetorno',
                10=>'cf.comentario',
                11=>'vdc.idVentas',
                12=>'cf.preren',
                13=>'cf.proinren',
                14=>'cf.idrenta',
                
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->DB11->select($select);
        $this->DB11->from('contrato_folio AS cf');
        $this->DB11->join('consumibles c', 'c.id = cf.idconsumible');
        $this->DB11->join('rentas r', 'r.id = cf.idrenta');
        $this->DB11->join('personal p', 'p.personalId = cf.creado_personal','left');
        $this->DB11->join('clientes cli', 'cli.id = r.idCliente');
        $this->DB11->join('contrato cont', 'cont.idRenta = r.id');
        $this->DB11->join('ventas_has_detallesConsumibles vdc', 'vdc.foliotext=cf.foliotext','left');
        if($estatus>0){
            $this->DB11->where(array('cf.status'=>$estatus));
        }else{
            $this->DB11->where('cf.status >=',1);
        }
        
        if ($cliente>0) {
            $this->DB11->where('cli.id',$cliente);
        }
        $query=$this->DB11->get();
        return $query;
    }
    //==============================================
    public function descripcionpolizadetalle($id,$serve=0){
        if($serve>0){
            $w_serve=" and acd.asignacionIdd='$serve' ";
        }else{
            $w_serve="";
        }
        $sql="SELECT pol.nombre,
                    eq.modelo,
                    seev.sinmodelo,
                    seev.newmodelo,
                    seev.nombre as servicio,
                     acd.serie,acd.direccion,acd.ttipo,acd.comentario_tec
                from asignacion_ser_cliente_a_d as acd
                inner JOIN polizas as pol on pol.id=acd.tpoliza
                left join servicio_evento as seev on seev.id=acd.tservicio
                left JOIN polizas_detalles as pold on pold.id=acd.tservicio_a
                left JOIN equipos as eq on eq.id=pold.modelo
                WHERE acd.asignacionId= $id $w_serve ";
        //log_message('error','descripcionpolizadetalle '.$sql);
        $query=$this->DB7->query($sql);
        return $query;
    }
    public function descripcionpolizaventadetalle($id){
        $sql="SELECT pol.nombre,
                    eq.modelo,
                    seev.sinmodelo,
                    seev.newmodelo,
                    seev.nombre as servicio,
                     acd.serie,acd.direccion,acd.ttipo,acv.comentario_tec
                from asignacion_ser_venta_a_d as acd
                inner JOIN asignacion_ser_venta_a as acv on acv.asignacionId=acd.asignacionId
                inner JOIN polizas as pol on pol.id=acd.tpoliza
                left join servicio_evento as seev on seev.id=acd.tservicio
                left JOIN polizas_detalles as pold on pold.id=acd.tservicio_a
                left JOIN equipos as eq on eq.id=pold.modelo
                WHERE acd.asignacionId= $id";
        $query=$this->DB7->query($sql);
        return $query;
    }
    function nombreunidadfacturacion($id){
        $sql="SELECT nombre
                from f_unidades
                WHERE UnidadId='$id'";
        $query=$this->DB17->query($sql);
        $nombre='';
        foreach ($query->result() as $item) {
            $nombre=$item->nombre;
        }
        return $nombre;
    }
    function traeProductosFactura($FacturasId){
        $sql="SELECT 
                dt.Unidad AS ClaveUnidad, 
                ser.Clave AS ClaveProdServ,
                dt.Descripcion2 as Descripcion, 
                dt.Cu, 
                dt.descuento, 
                dt.Cantidad, 
                dt.Importe, 
                u.nombre,
                u.Clave AS cunidad,
                dt.iva
                FROM f_facturas_servicios AS dt 
                left JOIN f_unidades AS u ON dt.Unidad = u.Clave 
                LEFT JOIN f_servicios AS ser ON ser.Clave = dt.ServicioId 
                WHERE dt.FacturasId =$FacturasId";
        $query=$this->db->query($sql);
        return $query;
    }
    function traeserviciosventas($tipo){
        if ($tipo==0) {
            $sql="SELECT v.id,v.reg, per.nombre,per.apellido_paterno,per.apellido_materno,cli.empresa,v.idCliente
                from ventas as v 
                inner join clientes as cli on cli.id=v.idCliente
                left join personal as per on per.personalId=v.id_personal
                left join asignacion_ser_venta_a as asv on asv.ventaId=v.id and asv.tipov=0
                where v.prefactura=1 and v.combinada=0 and v.servicio=1 and v.activo=1 and asv.asignacionId IS NULL ";
        }else{
            $sql="SELECT v.combinadaId,v.reg, per.nombre,per.apellido_paterno,per.apellido_materno,cli.empresa,v.idCliente
                from ventacombinada as v 
                inner join clientes as cli on cli.id=v.idCliente
                left join personal as per on per.personalId=v.id_personal
                left join asignacion_ser_venta_a as asv on asv.ventaId=v.combinadaId and asv.tipov=1
                where v.prefactura=1 and v.servicio=1 and v.activo=1 and asv.asignacionId IS NULL ";
        }
        
        $query=$this->DB10->query($sql);
        return $query;
    }
    function complementofacturas($facturaId){
        $sql="SELECT comp.complementoId,comp.FechaPago,comp.rutaXml,compd.NumParcialidad,compd.ImpPagado
                FROM f_complementopago AS comp
                inner join f_complementopago_documento as compd on compd.complementoId=comp.complementoId
                where comp.Estado=1 and compd.facturasId=$facturaId ";
        $query=$this->db->query($sql);
        return $query;
    }
    function complementofacturas2($facturaId){
        $sql="SELECT comp.complementoId,comp.FechaPago,comp.rutaXml,compd.NumParcialidad,compd.ImpPagado,fac.total
                FROM f_complementopago AS comp
                inner join f_complementopago_documento as compd on compd.complementoId=comp.complementoId
                inner join f_facturas as fac on fac.FacturasId=compd.facturasId
                where comp.Estado=1 and compd.facturasId=$facturaId ";
        $query=$this->db->query($sql);
        return $query;
    }
    function seriesenventas($idequipo){
        $sql="SELECT sere.serie FROM asignacion_series_equipos as equ 
                inner JOIN series_productos as sere on sere.serieId=equ.serieId 
                WHERE equ.id_equipo=$idequipo ";
        $query=$this->DB10->query($sql);
        $serie='';
        foreach ($query->result() as $item) {
            $serie.=$item->serie.' ';
        }
        return $serie;
    }
    function seriesenventas_vd($idequipo){
        $sql="SELECT sere.serie FROM ventasd_asignacion_series_equipos as equ 
                inner JOIN series_productos as sere on sere.serieId=equ.serieId 
                WHERE equ.id_equipo=$idequipo ";
        $query=$this->DB10->query($sql);
        $serie='';
        foreach ($query->result() as $item) {
            $serie.=$item->serie.' ';
        }
        return $serie;
    }
    function seriesenventasaccesorio($idaccesorio){
        $sql="SELECT seria.serie FROM asignacion_series_accesorios as sera 
              inner JOIN series_accesorios as seria on seria.serieId=sera.serieId 
              WHERE sera.id_accesorio=$idaccesorio ";
        $query=$this->DB7->query($sql);
        $serie='';
        foreach ($query->result() as $item) {
            $serie.=$item->serie.' ';
        }
        return $serie;
    }
    function seriesenventasaccesorio_vd($idaccesorio){
        $sql="SELECT seria.serie FROM ventasd_asignacion_series_accesorios as sera 
              inner JOIN series_accesorios as seria on seria.serieId=sera.serieId 
              WHERE sera.id_accesorio=$idaccesorio ";
        $query=$this->DB7->query($sql);
        $serie='';
        foreach ($query->result() as $item) {
            $serie.=$item->serie.' ';
        }
        return $serie;
    }
    function total_facturas($tipo,$finicio, $ffin){
        if($tipo==1){
            $estado =' (Estado = 1 or  Estado = 0) ';
        }else{
            $estado =' Estado = 0 ';
        }
        $sql="SELECT count(*) as total FROM f_facturas 
                WHERE $estado and activo =1 and fechatimbre between '$finicio 00:00:00' and '$ffin 23:59:59'";

        $query=$this->db->query($sql);
        $total=0;
        foreach ($query->result() as $item) {
            $total=$item->total;
        }
        return $total;
    }
    function totalcomplementos($tipo,$finicio, $ffin){
        if($tipo==1){
            $estado =' (Estado = 1 or  Estado = 0) ';
        }else{
            $estado =' Estado = 0 ';
        }
        $sql="SELECT count(*) as total FROM f_complementopago 
                WHERE $estado and fechatimbre between '$finicio 00:00:00' and '$ffin 23:59:59'";

        $query=$this->db->query($sql);
        $total=0;
        foreach ($query->result() as $item) {
            $total=$item->total;
        }
        return $total;
    }
    function obtenerseviciosdisponibles($fecha,$asignacion){
        $sql="SELECT asignacionId,fecha,hora,horafin 
              FROM `asignacion_ser_contrato_a_e` 
              WHERE fecha>='$fecha' AND status=0 and asignacionId=$asignacion LIMIT 1 ";
        $query=$this->DB7->query($sql);
        return $query;
    }
    function obtenernumeroserieequipo($equipo){
        $sql="SELECT noparte FROM equipos 
                WHERE id=$equipo";

        $query=$this->db->query($sql);
        $noparte='';
        foreach ($query->result() as $item) {
            $noparte=$item->noparte;
        }
        return $noparte;
    }
    function obtenerunidadservicio($id,$tipo){
        $strq = "SELECT * FROM unidades_servicios_default WHERE id=$id and activo=1"; 
        $query = $this->DB10->query($strq);
        $option='';
        foreach ($query->result() as $item) {
            if($tipo==1){
                $option='<option value="'.$item->unidadsat.'">'.$item->unidadsat_text.'</option>';
            }
            if($tipo==2){
                $option='<option value="'.$item->conceptosat.'">'.$item->conceptosat_text.'</option>';
            }
        }
        return $option;
    }
    function obtenerunidadserviciovt($id,$tipo,$vt){
        $strq = "SELECT * FROM unidades_servicios_default WHERE id=$id and activo=1"; 
        $query = $this->DB10->query($strq);
        $option='';
        foreach ($query->result() as $item) {
            if($tipo==1){
                if($vt==1){
                    $option=$item->unidadsat;
                }else{
                    $option=$item->unidadsat_text;
                }
                
            }
            if($tipo==2){
                if($vt==1){
                    $option=$item->conceptosat;
                }else{
                    $option=$item->conceptosat_text;
                }
            }
        }
        return $option;
    }
    function ventaservicio($idventa){
        $strq="SELECT 
                    pol.nombre,
                    eq.modelo,
                    pold.sinmodelo,
                    pold.newmodelo,
                    pold.precio_local,
                    pold.precio_semi,
                    pold.precio_foraneo,
                    pold.precio_especial, 
                    vn.servicio_tipo,
                    vn.servicio_motivo,
                    vn.servicio_direccion 
            FROM ventas as vn 
            INNER JOIN polizas as pol on pol.id=vn.servicio_poliza 
            INNER JOIN polizas_detalles as pold on pold.id=vn.servicio_servicio 
            LEFT JOIN equipos as eq on eq.id=pold.modelo 
            WHERE vn.id=$idventa";
        $query=$this->DB10->query($strq);
        return $query;
    }
    function cliente_has_equipo_historial($clienteId){
        $strq="SELECT ceh.*, eq.modelo as modelot 
                FROM `cliente_has_equipo_historial` as ceh 
                INNER JOIN equipos as eq on eq.id=ceh.modelo 
                WHERE ceh.clienteId=$clienteId AND activo=1";
        $query=$this->DB10->query($strq);
        return $query;
    }
    function cliente_has_equipo_historial_group($clienteId){
        $strq="SELECT equ.modelo as modelot, dat.modelo,dat.serie,dat.tipo,dat.id,dat.idcontrato,dat.equipoid,dat.serieId FROM (
                    SELECT cli.modelo,cli.serie,0 as tipo,0 as id,0 as idcontrato, 0 as equipoid, 0 as serieId
                    FROM cliente_has_equipo_historial as cli 
                    WHERE cli.clienteId=$clienteId AND cli.activo=1
                    union
                    SELECT vd.idEquipo as modelo,sp.serie,1 as tipo, v.id,0 as idcontrato,0 as equipoid,0 as serieId
                    FROM ventas as v 
                    INNER JOIN ventas_has_detallesEquipos as vd on vd.idVenta=v.id 
                    INNER join asignacion_series_equipos as vds on vds.id_equipo=vd.id 
                    INNER JOIN series_productos as sp on sp.serieId=vds.serieId 
                    WHERE v.idCliente=$clienteId
                    union
                    SELECT rene.idEquipo as modelo,sp.serie,2 as tipo, ren.id,cont.idcontrato,rene.id as equipoid,renes.serieId
                    FROM rentas as ren 
                    INNER JOIN rentas_has_detallesEquipos as rene on rene.idRenta=ren.id and rene.estatus=1 
                    inner join asignacion_series_r_equipos as renes on renes.id_equipo=rene.id and renes.activo=1 
                    INNER JOIN series_productos as sp on sp.serieId=renes.serieId 
                    INNER JOIN contrato as cont on cont.idRenta=ren.id
                    WHERE ren.idCliente=$clienteId and cont.estatus=1
                ) AS dat 
                INNER JOIN equipos as equ on equ.id=dat.modelo
                GROUP by dat.modelo,dat.serie
                order by dat.idcontrato DESC
                ";
                //group by dat.modelo,dat.serie
        $query=$this->DB8->query($strq);
        return $query;
    }
    function obtenerequiposcontratos($contratoId,$fecha){
        $strq="SELECT 
                    asigd.idequipo, 
                    asigd.serieId 
                FROM asignacion_ser_contrato_a as asig 
                INNER JOIN asignacion_ser_contrato_a_e as asigd on asigd.asignacionId=asig.asignacionId
                INNER JOIN contrato as con on con.idcontrato=asig.contratoId
                WHERE 
                    con.idRenta=$contratoId and 
                    asigd.activo=1 and 
                    asig.servicio_auto_inst=0 and
                    asigd.fecha>'$fecha'
                GROUP by asigd.idequipo, asigd.serieId
                ";
                //group by dat.modelo,dat.serie
        $query=$this->DB8->query($strq);
        return $query;
    }
    function getdireccionequipos($equipo,$serie){
        $resultdir=$this->getselectwheren('contrato_equipo_direccion',array('equiposrow'=>$equipo,'serieId'=>$serie));
        $iddireccionget='g_0';
        $direccion='';
        foreach ($resultdir->result() as $itemdir) {
            if($itemdir->direccion!=''){
                $iddireccionget=$itemdir->direccion;
            }
        }
        $tipodireccion=explode('_',$iddireccionget);

        $tipodir = $tipodireccion[0];
        $iddir   = $tipodireccion[1];
        if ($tipodir=='g') {
            $resultdirc=$this->getselectwheren('clientes_direccion',array('idclientedirecc'=>$iddir));
            foreach ($resultdirc->result() as $item) {
                $direccion=$item->direccion;
            }
        }
        if ($tipodir=='f') {
            $resultdirc=$this->getselectwheren('cliente_has_datos_fiscales',array('id'=>$iddir));
            foreach ($resultdirc->result() as $item) {
                $direccion='Razon social: '.$item->razon_social.' Colonia:'.$item->colonia.' Calle:'.$item->calle.' Numero:'.$item->num_ext;
            }
        }
        return $direccion;
    }
    function serviciosgenerados($contrato,$fecha){
        $strq="SELECT asigd.fecha 
                FROM asignacion_ser_contrato_a as asig 
                INNER JOIN asignacion_ser_contrato_a_e as asigd on asigd.asignacionId=asig.asignacionId 
                WHERE 
                    asigd.activo=1 and 
                    asig.contratoId=$contrato AND 
                    asigd.fecha<'$fecha' 
                GROUP by asigd.fecha";
        $query=$this->DB8->query($strq);
        return $query;
    }
    function obtencionfacturaspoliza($poliza){
        $strq="SELECT pol.facturaId, fac.Folio 
                FROM `factura_poliza` as pol 
                INNER JOIN f_facturas as fac on fac.FacturasId=pol.facturaId 
                WHERE pol.polizaId=$poliza";
        $query=$this->DB2->query($strq);
        return $query;
    }
    function obtencionfacturasventas($tipo,$venta){
        $strq="SELECT ven.facturaId, fac.Folio 
                FROM `factura_venta` as ven 
                INNER JOIN f_facturas as fac on fac.FacturasId=ven.facturaId 
                WHERE ven.combinada=$tipo and ven.ventaId=$venta";
        $query=$this->DB3->query($strq);
        return $query;
    }
    function cliente_refacciones_cotizacion($cliente,$cotizaciones){
        $strq="SELECT * from   (";
        if($cotizaciones==1){
        $strq.="SELECT 
                    cotd.id,
                    cotd.serieequipo,
                    cotd.idCotizacion,
                    cotd.piezas,
                    cotd.idEquipo,
                    equ.modelo as equipo, 
                    cotd.idRefaccion, 
                    cotd.modelo as refaccion,
                    cotd.precioGeneral,
                    0 tipo
                FROM cotizaciones_has_detallesRefacciones as cotd
                INNER JOIN cotizaciones as cot on cot.id=cotd.idCotizacion
                INNER JOIN equipos as equ on equ.id=cotd.idEquipo
                WHERE (cot.estatus=1 OR cot.estatus=2) AND cot.idCliente=$cliente
                union ";
        }
        $strq.="SELECT 
                    '' as id,    
                    cli.serie as serieequipo,
                    '' as idCotizacion,
                    1 as piezas,
                    cli.modelo as idEquipo,
                    equ.modelo as equipo,
                    0 as idRefaccion,
                    '' as refaccion,
                    0 as precioGeneral,
                    1 tipo
                    FROM cliente_has_equipo_historial as cli 
                    inner join equipos as equ on equ.id=cli.modelo
                    WHERE 
                        cli.clienteId=$cliente AND 
                        cli.activo=1 
                    union ";
        $strq.="SELECT '' as id,sp.serie as serieequipo,'' as cotizacion,1 as piezas,ve.idEquipo,ve.modelo as equipo,0 as idRefaccion,'' as refaccion,0 as precioGeneral,1 as tipo FROM ventas as v INNER JOIN ventas_has_detallesEquipos as ve on ve.idVenta=v.id LEFT JOIN asignacion_series_equipos as ase on ase.id_equipo=ve.id LEFT JOIN series_productos as sp on sp.serieId=ase.serieId WHERE v.idCliente=$cliente";
        $strq.=") as datos group by idEquipo,serieequipo";
        $query=$this->DB8->query($strq);
        return $query;
    }
    function contratossinservicios($fecha){
        $strq="SELECT 
                    cont.idcontrato, 
                    asig.asignacionId,
                    COUNT(*) as total 
                FROM contrato as cont 
                LEFT JOIN asignacion_ser_contrato_a as asig on asig.contratoId=cont.idcontrato 
                LEFT JOIN asignacion_ser_contrato_a_e as asige on asige.asignacionId=asig.asignacionId and asige.activo=1 AND asige.fecha>'$fecha' 
                WHERE cont.estatus=0 and asig.asignacionId IS null 
                GROUP BY cont.idcontrato";
        $query=$this->DB3->query($strq);
        return $query;
    }
    function selectcontrolfoliorenta($idrenta,$idConsumible,$serieId){
        $strq="SELECT *
            FROM `contrato_folio`
            WHERE `idrenta` = '$idrenta'
            AND `idconsumible` = '$idConsumible'
            AND (serieId=$serieId or serieId is null) ";
        $query=$this->db->query($strq);
        return $query;
    }
    function viewagenda($idcontrato,$idequipo,$fechaactual){
        $strq="SELECT 
                    contrd.fecha,contrd.idequipo,contr.contratoId,cli.id,cli.empresa 
                FROM asignacion_ser_contrato_a as contr 
                INNER JOIN asignacion_ser_contrato_a_e as contrd on contrd.asignacionId=contr.asignacionId 
                INNER JOIN contrato as contra on contra.idcontrato=contr.contratoId 
                INNER JOIN rentas as ren on ren.id=contra.idRenta 
                INNER join clientes as cli on cli.id=ren.idCliente 
                WHERE 
                    contr.contratoId=$idcontrato and contrd.idequipo=$idequipo and contrd.fecha>='$fechaactual'
                    GROUP by contrd.fecha";
        $query=$this->db->query($strq);
        return $query;
    }
    //=================== session ======================================
    function view_session_bodegas(){
        if(isset($_SESSION['session_bodegas'])){
                $session_bodegas=$_SESSION['session_bodegas'];
        }else{
            //unset($_SESSION['session_bodegas']);
            $session_bodegas=$this->getselectwheren('bodegas',array('activo' => 1));
            $_SESSION['session_bodegas']=$session_bodegas->result();
        }
        return $session_bodegas;
    }
    function view_session_bodegas0(){
        
            //unset($_SESSION['session_bodegas']);
            $session_bodegas=$this->getselectwheren('bodegas',array('activo' => 1));
            $_SESSION['session_bodegas']=$session_bodegas->result();
        
        return $session_bodegas;
    }
    function view_session_rutas(){
        if(isset($_SESSION['session_rutas'])){
                $session_rutas=$_SESSION['session_rutas'];
        }else{
            //unset($_SESSION['session_rutas']);
            $session_rutas=$this->getselectwheren('rutas',array('status' => 1));
            $_SESSION['session_rutas']=$session_rutas->result();
        }
        return $session_rutas;
    }
    function view_session_f_metodopago(){
        if(isset($_SESSION['session_metodopago'])){
                $session_metodopago=$_SESSION['session_metodopago'];
        }else{
            //unset($_SESSION['session_rutas']);
            $session_metodopago=$this->getselectwheren('f_metodopago',array('activo' => 1));
            $_SESSION['session_metodopago']=$session_metodopago->result();
        }
        return $session_metodopago;
    }
    function ontenerdatosfacturaventa($idventa,$tipo,$limit=0){
        $lim_w='';
        if($limit==1){
            $lim_w= " limit 1 ";
        }
        $strq="SELECT fac.* 
             FROM `factura_venta` as v 
             INNER JOIN f_facturas as fac on fac.FacturasId=v.facturaId 
             WHERE 
                v.ventaId=$idventa and 
                v.combinada=$tipo and 
                fac.Estado=1 $lim_w";
        $query=$this->db->query($strq);
        return $query;
    }
    function ontenerdatosfacturaventa_vd($idventa,$tipo){
        $strq="SELECT fac.* 
             FROM `ventasd_factura` as v 
             INNER JOIN f_facturas as fac on fac.FacturasId=v.facturaId 
             WHERE 
                v.ventaId=$idventa and 
                fac.Estado=1";
        $query=$this->db->query($strq);
        return $query;
    }
    function ontenerdatosfacturapoliza($idventa){
        $strq="SELECT fac.* 
                FROM `factura_poliza` as v 
                INNER JOIN f_facturas as fac on fac.FacturasId=v.facturaId 
                WHERE v.polizaId=$idventa and fac.Estado=1";
        $query=$this->db->query($strq);
        return $query;
    }
    function obtencionventapolizafactura($factura){
        $strq="SELECT v.ventaId,v.combinada 
                FROM 
                `factura_venta` as v
                WHERE v.facturaId=$factura
                UNION 
                SELECT pol.polizaId as ventaId ,2 as combinada
                FROM factura_poliza as pol
                WHERE pol.facturaId=$factura";
        $query=$this->db->query($strq);
        return $query;
    }
    function obtencionventapolizafactura_vd($factura){
        $strq="SELECT v.ventaId
                FROM 
                `ventasd_factura` as v
                WHERE v.facturaId=$factura
               ";
        $query=$this->db->query($strq);
        return $query;
    }
    function obtencionpagosrentasfactura($factura){
        $strq="SELECT fpp.pago,fpp.fecha 
                FROM factura_prefactura as fp 
                INNER JOIN factura_prefactura_pagos as fpp on fpp.facId=fp.facId 
                WHERE fp.facturaId=$factura
                union
                SELECT pf.pago,pf.fecha
                from pagos_factura as pf
                WHERE pf.factura=$factura 

            ";
        $query=$this->db->query($strq);
        return $query;
    }
    function obteciondecomplememtofactura($factura){
        $strq="SELECT 
                    fc.complementoId,
                    fc.FechaPago as Fecha,
                    fcd.ImpPagado 
                FROM f_complementopago as fc 
                INNER JOIN f_complementopago_documento as fcd on fcd.complementoId=fc.complementoId 
                WHERE fc.Estado=1 AND fc.activo=1 AND fcd.facturasId=$factura";
        $query=$this->db->query($strq);
        return $query;
    }
    function obtenerfoliosconsumiblesrenta($renta,$consumible,$idrelacion){
        $strq="SELECT * FROM contrato_folio WHERE idrenta=$renta AND idconsumible = $consumible AND (idrelacion=$idrelacion or idrelacion=0)"; 
        //$strq="SELECT * FROM contrato_folio WHERE idrenta=$renta AND idconsumible = $consumible AND idrelacion=$idrelacion"; verificar si se queda este
        $query=$this->DB7->query($strq);
        return $query;
    }
    function obtenerfoliosconsumiblesrenta2($renta,$consumible,$idrelacion,$serieId){
        $strq="SELECT * FROM contrato_folio WHERE idrenta=$renta AND idconsumible = $consumible AND (serieId=$serieId or serieId is null) and (idrelacion=$idrelacion or idrelacion=0)"; 
        //$strq="SELECT * FROM contrato_folio WHERE idrenta=$renta AND idconsumible = $consumible AND idrelacion=$idrelacion"; verificar si se queda este
        //log_message('error', 'obtenerfoliosconsumiblesrenta2: '.$strq);
        $query=$this->db->query($strq);
        return $query;
    }
    function obtenerfoliosconsumiblesrenta3($renta,$consumible,$idrelacion,$serieId){
        $strq="SELECT * FROM contrato_folio WHERE idrenta=$renta AND idconsumible = $consumible AND (serieId=$serieId or idrelacion=$idrelacion) and status!=2"; 
        //$strq="SELECT * FROM contrato_folio WHERE idrenta=$renta AND idconsumible = $consumible AND idrelacion=$idrelacion"; verificar si se queda este
        //log_message('error', 'obtenerfoliosconsumiblesrenta2: '.$strq);
        $query=$this->db->query($strq);
        return $query;
    }
    function servicioscontratosactivos($contrato){
        $strq = "SELECT 
                        asig.asignacionId,asigd.activo 
                FROM asignacion_ser_contrato_a as asig 
                INNER JOIN asignacion_ser_contrato_a_e asigd on asigd.asignacionId=asig.asignacionId 
                WHERE 
                    asig.contratoId = $contrato and 
                    asigd.activo=1 
                GROUP BY asig.asignacionId"; 
        $query = $this->db->query($strq);
        return $query;
    }
    function permisosview(){
        $strq = "SELECT 
                        ms.MenusubId, 
                        men.Nombre as menu, 
                        ms.Nombre as submenu 
                FROM menu_sub as ms 
                INNER JOIN menu as men on men.MenuId=ms.MenuId"; 
        $query = $this->db->query($strq);
        return $query;
    }
    function permisosviewperfil($perfil){
        $strq = "SELECT 
                        pd.Perfil_detalleId,
                        men.Nombre as menu,
                        mesu.Nombre as submenu
                    FROM perfiles_detalles as pd 
                    INNER JOIN menu_sub as mesu on mesu.MenusubId=pd.MenusubId 
                    INNER JOIN menu as men on men.MenuId=mesu.MenuId 
                    WHERE 
                        pd.perfilId=$perfil 
                    ORDER BY men.Nombre ASC"; 
        $query = $this->db->query($strq);
        return $query;
    }
    function reporteventasnormal($fechaini,$fechafin,$cliente,$ejecutiva,$tipof){
        $fechaini_w='';$fechafin_w='';$cliente_w='';$ejecutiva_w="";
        if($fechaini!=''){
            if($tipof==0){
                $fechaini_w=" and v.reg >= '".$fechaini." 00:00:00'";
            }
            if($tipof==1){
                $fechaini_w=" and pv.fecha >= '".$fechaini."'";
            }
        }
        if($fechafin!=''){
            if($tipof==0){
                $fechafin_w=" and v.reg <= '".$fechafin." 23:59:59'";
            }
            if($tipof==1){
                $fechafin_w=" and pv.fecha <= '".$fechafin."'";
            }
        }
        if($cliente>0){
            $cliente_w=" and v.idCliente=$cliente";
        }
        if($ejecutiva>0){
            $ejecutiva_w=" and v.id_personal=$ejecutiva";
        }

        $strq = "SELECT 
                        v.id,cli.empresa,v.activo,v.reg,v.motivo,v.fechaentrega,
                        per.nombre,per.apellido_paterno,per.apellido_materno,
                        v.estatus,v.activo,v.total_general,cli.credito,fac.serie,fac.Folio,v.subtotal_general,v.iva_general,fac.FacturasId
                from ventas as v
                inner join clientes as cli on cli.id=v.idCliente
                inner join personal as per on v.id_personal=per.personalId
                left join factura_venta as facv on facv.ventaId=v.id and facv.combinada=0
                left JOIN f_facturas as fac on fac.FacturasId= facv.facturaId and fac.Estado=1
                left join pagos_ventas as pv on pv.idventa=v.id
                where v.combinada=0 $fechaini_w $fechafin_w $cliente_w $ejecutiva_w
                group by v.id
                ";
                //log_message('error', $strq);
        $query = $this->db->query($strq);
        return $query;
    }
    function reporteventasconbinada($fechaini,$fechafin,$cliente,$ejecutiva,$tipof){
        $fechaini_w='';$fechafin_w='';$cliente_w='';$ejecutiva_w="";

        if($fechaini!=''){
            if($tipof==0){
                $fechaini_w=" and v.reg >= '".$fechaini." 00:00:00'";
            }
            if($tipof==1){
                $fechaini_w=" and pv.fecha >= '".$fechaini."'";
            }
            
        }
        if($fechafin!=''){
            if($tipof==0){
                $fechafin_w=" and v.reg <= '".$fechafin." 23:59:59'";
            }
            if($tipof==1){
                $fechafin_w=" and pv.fecha <= '".$fechafin."'";
            }
            
        }
        if($cliente>0){
            $cliente_w=" and v.idCliente=$cliente";
        }
        if($ejecutiva>0){
            $ejecutiva_w=" and v.id_personal=$ejecutiva";
        }
        $strq = "SELECT 
                        v.combinadaId,cli.empresa,v.equipo,v.consumibles,v.refacciones,v.poliza,
                        v.activo,v.reg,v.motivo,v.fechaentrega,
                        per.nombre,per.apellido_paterno,per.apellido_materno,
                        v.estatus,v.activo,v.total_general,cli.credito,
                        fac.serie,fac.Folio,fac.FacturasId,v.subtotal_general,v.iva_general
                from ventacombinada as v
                inner join clientes as cli on cli.id=v.idCliente
                inner join personal as per on v.id_personal=per.personalId
                left join factura_venta as facv on facv.ventaId=v.combinadaId and facv.combinada=1
                left JOIN f_facturas as fac on fac.FacturasId= facv.facturaId and fac.Estado=1
                left join pagos_combinada as pv on pv.idventa=v.combinadaId
                WHERE v.combinadaId>0 $fechaini_w $fechafin_w $cliente_w $ejecutiva_w
                group by v.combinadaId
                "; 
        $query = $this->db->query($strq);
        return $query;
    }
    function reporterentaspagoscomp($fecha,$cliente){
        $strq="SELECT con.idcontrato,arpre.prefId,arpre.id_renta,arpre.periodo_inicial,arpre.periodo_final,arpre.subtotal,arpre.subtotalcolor,arpre.excedente,arpre.excedentecolor,arpre.total,sum(fac.total) as totalfactura,sum(compd.ImpPagado) as totalpagoscomp,
                sum(facprep.pago) as facpagodirecto,
                arpre.reg,fac.fechatimbre,cli.credito,count(*) as numreg,
                fac.FacturasId,fac.serie,fac.Folio
                FROM rentas as ren
                INNER JOIN contrato as con on con.idRenta=ren.id
                INNER JOIN alta_rentas as aren on aren.idcontrato=con.idcontrato
                INNER JOIN alta_rentas_prefactura as arpre on arpre.id_renta=aren.id_renta
                inner join clientes as cli on cli.id=ren.idCliente
                LEFT JOIN factura_prefactura as facpre on facpre.prefacturaId=arpre.prefId
                LEFT JOIN f_facturas as fac on fac.FacturasId=facpre.facturaId AND fac.Estado=1
                left join f_complementopago_documento as compd on compd.facturasId=fac.FacturasId and compd.Estado=1
                LEFT JOIN factura_prefactura_pagos as facprep on facprep.facId=facpre.facId
                WHERE arpre.descartar_not=0 and ren.idCliente='$cliente' AND con.estatus=1 AND arpre.reg>='$fecha'
                GROUP BY arpre.prefId";
        $query = $this->db->query($strq);
        return $query;
    }
    function reporteventaspoliza($fechaini,$fechafin,$cliente,$ejecutiva,$tipof){
        $fechaini_w='';$fechafin_w='';$cliente_w='';$ejecutiva_w="";


        if($fechaini!=''){
            if($tipof==0){
                $fechaini_w=" and v.reg >= '".$fechaini." 00:00:00'";
            }
            if($tipof==1){
                $fechaini_w=" and pp.fecha >= '".$fechaini." 00:00:00'";
            }
        }

        if($fechafin!=''){
            if($tipof==0){
                $fechafin_w=" and v.reg <= '".$fechafin." 23:59:59'";
            }
            if($tipof==1){
                $fechafin_w=" and pp.fecha <= '".$fechafin." 23:59:59'";
            }
        }
        if($cliente>0){
            $cliente_w=" and v.idCliente=$cliente";
        }
        if($ejecutiva>0){
            $ejecutiva_w=" and v.id_personal=$ejecutiva";
        }

        $strq = "SELECT 
                        v.id,
                        cli.empresa,
                        v.activo,
                        v.reg,
                        '' as motivo,
                        '' as fechaentrega,
                        per.nombre,per.apellido_paterno,per.apellido_materno,
                        v.estatus,
                        v.activo,
                        cli.credito,
                        fac.serie,
                        fac.Folio,
                        fac.FacturasId,
                        v.subtotal,
                        v.iva,
                        v.total
                from polizasCreadas as v
                inner join clientes as cli on cli.id=v.idCliente
                inner join personal as per on v.id_personal=per.personalId
                left join factura_poliza as facv on facv.polizaId=v.id 
                left JOIN f_facturas as fac on fac.FacturasId=facv.facturaId and fac.Estado=1
                left join pagos_poliza as pp on pp.idpoliza=v.id
                where v.id>0 and v.combinada=0
                $fechaini_w
                $fechafin_w
                $cliente_w
                $ejecutiva_w
                group by v.id
                "; 
        $query = $this->db->query($strq);
        return $query;
    }
    function reportecontratos($fechaini,$fechafin,$cliente){
        if($fechaini!=''){
            $fechaini_w=" and con.fechasolicitud >= '".$fechaini."'";
        }else{
            $fechaini_w='';
        }

        if($fechafin!=''){
            $fechafin_w=" and con.fechasolicitud <= '".$fechafin."'";
        }else{
            $fechafin_w='';
        }
        if($cliente>0){
            $cliente_w=" and ren.idCliente=$cliente";
        }else{
            $cliente_w='';
        }

        $strq = "SELECT 
                        con.idcontrato, 
                        con.idRenta,
                        con.estatus,
                        con.fechasolicitud,
                        cli.empresa,
                        per.nombre,per.apellido_paterno,per.apellido_materno
                FROM contrato as con 
                INNER JOIN rentas as ren on ren.id=con.idRenta 
                INNER JOIN clientes as cli on cli.id=ren.idCliente 
                inner join personal as per on ren.id_personal=per.personalId
                WHERE 
                    con.idcontrato > 0
                    $fechaini_w
                    $fechafin_w
                    $cliente_w
                "; 
        $query = $this->db->query($strq);
        return $query;
    }
    function reporteticket($fechaini,$fechafin){
        if($fechaini!=''){
            $fechaini_w=" and ti.reg >= '".$fechaini." 00:00:00'";
        }else{
            $fechaini_w='';
        }

        if($fechafin!=''){
            $fechafin_w=" and ti.reg <= '".$fechafin." 23:59:59'";
        }else{
            $fechafin_w='';
        }
        $strq="SELECT ti.id,per.nombre,per.apellido_paterno,per.apellido_materno,ti.tcontenido,ti.contenido,ti.status,ti.tipo,ti.reg
                FROM ticket as ti
                INNER JOIN personal as per on per.personalId=ti.personalId
                WHERE ti.activo=1 $fechaini_w $fechafin_w";
       $query = $this->db->query($strq);
        return $query; 
    }
    function montoventas2($id){//ventas 
        $totalgeneral=0; 
        $rowequipos=$this->db10_getselectwheren('ventas_has_detallesEquipos',array('idVenta'=>$id)); 
        foreach ($rowequipos->result() as $item) { 
            $totalgeneral=$totalgeneral+($item->cantidad*$item->precio); 
        } 
        $resultadoaccesorios = $this->accesoriosventa($id); 
        foreach ($resultadoaccesorios->result() as $item) { 
            $totalgeneral=$totalgeneral+($item->cantidad*$item->costo); 
        } 
        $resultadoconsumibles = $this->consumiblesventa($id); 
        foreach ($resultadoconsumibles->result() as $item) {  
            $totalgeneral=$totalgeneral+($item->cantidad*$item->costo_toner); 
        } 
        $consumiblesventadetalles = $this->consumiblesventadetalles($id); 
        foreach ($consumiblesventadetalles->result() as $item) {  
            $totalgeneral=$totalgeneral+($item->cantidad*$item->costo_toner); 
        } 
        $ventadetallesrefacion = $this->ventas_has_detallesRefacciones($id); 
        foreach ($ventadetallesrefacion->result() as $item) {  
            $totalgeneral=$totalgeneral+($item->cantidad*$item->precioGeneral); 
        } 
        $totalgeneral=$totalgeneral+($totalgeneral*0.16); 
 
        return round($totalgeneral, 2);  
    }
    function montoventas3($id){//ventas 
        $totalgeneral=0; 
        $rowequipos=$this->db10_getselectwheren('ventas_has_detallesEquipos',array('idVenta'=>$id)); 
        foreach ($rowequipos->result() as $item) { 
            $totalgeneral=$totalgeneral+($item->cantidad*$item->precio); 
        } 
        //$resultadoaccesorios = $this->accesoriosventa($id); 
        $resultadoaccesorios = $this->db10_getselectwheren('ventas_has_detallesEquipos_accesorios',array('id_venta'=>$id)); 
        foreach ($resultadoaccesorios->result() as $item) { 
            $totalgeneral=$totalgeneral+($item->cantidad*$item->costo); 
        } 
        //$resultadoconsumibles = $this->consumiblesventa($id); 
        $resultadoconsumibles = $this->db10_getselectwheren('ventas_has_detallesequipos_consumible',array('idventa'=>$id)); 
        foreach ($resultadoconsumibles->result() as $item) {  
            $totalgeneral=$totalgeneral+($item->cantidad*$item->costo_toner); 
        } 
        //$consumiblesventadetalles = $this->consumiblesventadetalles($id); 
        $consumiblesventadetalles = $this->db7_getselectwheren('ventas_has_detallesConsumibles',array('idVentas'=>$id)); 
        foreach ($consumiblesventadetalles->result() as $item) {  
            $totalgeneral=$totalgeneral+($item->piezas*$item->precioGeneral); 
        } 
        //$ventadetallesrefacion = $this->ventas_has_detallesRefacciones($id); 
        //$ventadetallesrefacion = $this->ventas_has_detallesRefacciones_mini($id); 
        $ventadetallesrefacion = $this->db7_getselectwheren('ventas_has_detallesRefacciones',array('idVentas'=>$id)); 
        foreach ($ventadetallesrefacion->result() as $item) {  
            $totalgeneral=$totalgeneral+($item->piezas*$item->precioGeneral); 
        } 
        $subtotal=$totalgeneral;
        $iva=$totalgeneral*0.16;
        $totalgeneral=$totalgeneral+($totalgeneral*0.16); 
        $array=array(
            'subtotal'=>$subtotal,
            'iva'=>$iva,
            'total'=>$totalgeneral
        );
        return $array;  
    }
    function montoventas4($id){//ventas 
        $totalgeneral=0; 
        $strq="SELECT SUM(monto) as subtotal FROM(
                SELECT 1 tipo,SUM(veq.cantidad*veq.precio) as monto FROM ventas_has_detallesEquipos as veq WHERE veq.idVenta='$id'
                UNION
                SELECT 2 tipo,SUM(vacc.cantidad*vacc.costo) as monto FROM ventas_has_detallesEquipos_accesorios as vacc WHERE vacc.id_venta='$id'
                UNION
                SELECT 3 tipo,SUM(vcon.cantidad*vcon.costo_toner) as monto FROM ventas_has_detallesequipos_consumible as vcon WHERE vcon.idventa='$id'
                UNION
                SELECT 4 tipo,SUM(vcon2.piezas*vcon2.precioGeneral) as monto FROM ventas_has_detallesConsumibles as vcon2 WHERE vcon2.idVentas='$id'
                UNION
                SELECT 5 tipo,vr.piezas*vr.precioGeneral as monto FROM ventas_has_detallesRefacciones as vr WHERE vr.idVentas='$id'
                ) as datos";
        $query = $this->db->query($strq);
        foreach ($query->result() as $item) {  
            if($item->subtotal>0){
                $totalgeneral=$item->subtotal; 
            }else{
                $totalgeneral=0; 
            }
            
        } 
        $subtotal=$totalgeneral;
        $iva=$totalgeneral*0.16;
        $totalgeneral=$totalgeneral+($totalgeneral*0.16); 
        $array=array(
            'subtotal'=>$subtotal,
            'iva'=>$iva,
            'total'=>$totalgeneral
        );
        return $array;  
    } 
    function montocombinada2($id){// convinadas 

        $resultfactura=$this->ontenerdatosfacturaventa($id,1);
        $totalgeneral=0; 

        if($resultfactura->num_rows()==0){

            $where_ventas=array('combinadaId'=>$id); 
            $resultadov = $this->db10_getselectwheren('ventacombinada',$where_ventas); 
            $equipo=0; 
            $consumibles=0; 
            $refacciones=0; 
            $poliza=0; 
            foreach ($resultadov->result() as $item) { 
                    $equipo=$item->equipo; 
                    $consumibles=$item->consumibles; 
                    $refacciones=$item->refacciones; 
                    $poliza=$item->poliza; 
            } 
            
            $rowequipos=$this->db10_getselectwheren('ventas_has_detallesEquipos',array('idVenta'=>$equipo)); 
            foreach ($rowequipos->result() as $item) { 
                $totalgeneral=$totalgeneral+($item->cantidad*$item->precio); 
                //log_message('error',$item->cantidad.'*'.$item->precio);
            } 
            $resultadoaccesorios = $this->accesoriosventa($equipo); 
            foreach ($resultadoaccesorios->result() as $item) { 
                $totalgeneral=$totalgeneral+($item->cantidad*$item->costo); 
            } 
            $resultadoconsumibles = $this->consumiblesventa($equipo); 
            foreach ($resultadoconsumibles->result() as $item) {  
                $totalgeneral=$totalgeneral+($item->cantidad*$item->costo_toner); 
                //log_message('error','c1 '.$item->cantidad.'*'.$item->costo_toner);
            } 
            $consumiblesventadetalles = $this->consumiblesventadetalles($consumibles); 
            foreach ($consumiblesventadetalles->result() as $item) {  
                $totalgeneral=$totalgeneral+($item->cantidad*$item->costo_toner); 
                //log_message('error','c2 '.$item->cantidad.'*'.$item->costo_toner);
                //log_message('error','c2 totalgeneral '.$totalgeneral);
            } 
            $ventadetallesrefacion = $this->ventas_has_detallesRefacciones($refacciones); 
            foreach ($ventadetallesrefacion->result() as $item) {  
                $totalgeneral=$totalgeneral+($item->cantidad*$item->precioGeneral); 
                //log_message('error','r1 '.$item->cantidad.'*'.$item->precioGeneral);
                //log_message('error','r1 totalgeneral '.$totalgeneral);
            } 
            $polizaventadetalles = $this->polizaventadetalles2($poliza); 
            foreach ($polizaventadetalles->result() as $item) {  
                $totalc=0;
                if($item->precio_local !==NULL) { 
                    $totalc=$item->precio_local; 
                } 
                if($item->precio_semi !==NULL) { 
                    $totalc=$item->precio_semi; 
                } 
                if($item->precio_foraneo !==NULL) { 
                    $totalc=$item->precio_foraneo; 
                } 
                //log_message('error','p1 totalc '.$totalc);
                $totalgeneral=$totalgeneral+($totalc); 
                //$totalgeneral=$totalgeneral+($totalc*item->cantidad); 
                    
            } 
            //$totalgeneral=$totalgeneral+($totalgeneral*0.16); 
        }else{
            foreach ($resultfactura->result() as $item) {
                $totalgeneral=$totalgeneral+$item->total;
            }
        } 
        //log_message('error','r totalgeneral '.$totalgeneral);
        return round($totalgeneral, 2); 
    }
    function montocombinada3($id){// convinadas 

        $resultfactura=$this->ontenerdatosfacturaventa($id,1);
        $totalgeneral=0; 

        if($resultfactura->num_rows()==0){

            $where_ventas=array('combinadaId'=>$id); 
            $resultadov = $this->db10_getselectwheren('ventacombinada',$where_ventas); 
            $equipo=0; 
            $consumibles=0; 
            $refacciones=0; 
            $poliza=0; 
            foreach ($resultadov->result() as $item) { 
                    $equipo=$item->equipo; 
                    $consumibles=$item->consumibles; 
                    $refacciones=$item->refacciones; 
                    $poliza=$item->poliza; 
            } 
            
            $rowequipos=$this->db10_getselectwheren('ventas_has_detallesEquipos',array('idVenta'=>$equipo)); 
            foreach ($rowequipos->result() as $item) { 
                $totalgeneral=$totalgeneral+($item->cantidad*$item->precio); 
                //log_message('error',$item->cantidad.'*'.$item->precio);
            } 
            //$resultadoaccesorios = $this->accesoriosventa($equipo); 
            $resultadoaccesorios = $this->db10_getselectwheren('ventas_has_detallesEquipos_accesorios',array('id_venta'=>$equipo)); 
            foreach ($resultadoaccesorios->result() as $item) { 
                $totalgeneral=$totalgeneral+($item->cantidad*$item->costo); 
            } 
            //$resultadoconsumibles = $this->consumiblesventa($equipo); 
            $resultadoconsumibles = $this->db10_getselectwheren('ventas_has_detallesequipos_consumible',array('idventa'=>$equipo)); 
            foreach ($resultadoconsumibles->result() as $item) {  
                $totalgeneral=$totalgeneral+($item->cantidad*$item->costo_toner); 
                //log_message('error','c1 '.$item->cantidad.'*'.$item->costo_toner);
            } 
            //$consumiblesventadetalles = $this->consumiblesventadetalles($consumibles); 
            $consumiblesventadetalles = $this->db7_getselectwheren('ventas_has_detallesConsumibles',array('idVentas'=>$consumibles)); 
            foreach ($consumiblesventadetalles->result() as $item) {  
                $totalgeneral=$totalgeneral+($item->piezas*$item->precioGeneral); 
                //log_message('error','c2 '.$item->cantidad.'*'.$item->costo_toner);
                //log_message('error','c2 totalgeneral '.$totalgeneral);
            } 
            //$ventadetallesrefacion = $this->ventas_has_detallesRefacciones($refacciones); 
            $ventadetallesrefacion = $this->db7_getselectwheren('ventas_has_detallesRefacciones',array('idVentas'=>$refacciones)); 
            foreach ($ventadetallesrefacion->result() as $item) {  
                $totalgeneral=$totalgeneral+($item->piezas*$item->precioGeneral); 
                //log_message('error','r1 '.$item->cantidad.'*'.$item->precioGeneral);
                //log_message('error','r1 totalgeneral '.$totalgeneral);
            } 
            $polizaventadetalles = $this->polizaventadetalles2($poliza); 
            foreach ($polizaventadetalles->result() as $item) {  
                $totalc=0;
                if($item->precio_local !==NULL) { 
                    $totalc=$item->precio_local; 
                } 
                if($item->precio_semi !==NULL) { 
                    $totalc=$item->precio_semi; 
                } 
                if($item->precio_foraneo !==NULL) { 
                    $totalc=$item->precio_foraneo; 
                } 
                //log_message('error','p1 totalc '.$totalc);
                $totalgeneral=$totalgeneral+($totalc); 
                //$totalgeneral=$totalgeneral+($totalc*item->cantidad); 
                    
            } 
            //$totalgeneral=$totalgeneral+($totalgeneral*0.16); 
        }else{
            foreach ($resultfactura->result() as $item) {
                $totalgeneral=$totalgeneral+$item->total;
            }
        } 
        $subtotal=$totalgeneral;
        $iva=$subtotal*0.16;
        $totalgeneral=$subtotal+$iva;
        //log_message('error','r totalgeneral '.$totalgeneral);
        $array=array(
            'subtotal'=>$subtotal,
            'iva'=>$iva,
            'total'=>$totalgeneral
        );
        return $array; 
    }
    function table_get_tipo_compra($id,$tip){ 
        $totalgeneral=0;
        $resultfactura=$this->ontenerdatosfacturaventa($id,$tip); 
        if($tip==1){ 
            if($resultfactura->num_rows()==0){
                $result = $this->pagos_combinada($id); 
                foreach ($result as $item) { 
                    $totalgeneral=$totalgeneral+$item->pago;
                } 
            }
        }else{ 
            if($resultfactura->num_rows()==0){
                $result = $this->pagos_ventas($id); 
                foreach ($result as $item) { 
                    $totalgeneral=$totalgeneral+$item->pago;
                } 
            }
        }
        foreach($resultfactura->result() as $item){
            $FacturasIdselected=$item->FacturasId;
            $resultvcp = $this->obtencionventapolizafactura($item->FacturasId);

            foreach ($resultvcp->result() as $itemvcp) {
                if($itemvcp->combinada==0){
            
                    //=======================================
                        $result = $this->pagos_ventas($itemvcp->ventaId); 
                        foreach ($result as $item) { 
                            $totalgeneral=$totalgeneral+$item->pago;
                        } 
                    //=======================================
                }elseif ($itemvcp->combinada==1) {
                
                    //========================================
                        $result = $this->pagos_combinada($itemvcp->ventaId); 
                        foreach ($result as $item) { 
                            $totalgeneral=$totalgeneral+$item->pago; 
                        } 
                    //========================================
                }elseif ($itemvcp->combinada==2) {
                        $result = $this->pagos_poliza($itemvcp->ventaId);
                        foreach ($result as $item) {
                            $totalgeneral=$totalgeneral+$item->pago; 
                        }
                    //========================================
                }
            }
            $resultvcp_cp = $this->obteciondecomplememtofactura($FacturasIdselected);
            foreach ($resultvcp_cp->result() as $itemcp) {
                $totalgeneral=$totalgeneral+$itemcp->ImpPagado; 
            }
        }
        return $totalgeneral; 
    }  
    function montopoliza($id){//polizas creadas
        $totalgeneral=0;
        $resultfactura=$this->ontenerdatosfacturapoliza($id);
        if($resultfactura->num_rows()==0){
            $totalc=0;
            $polizaventadetalles = $this->polizaventadetalles3($id);
            foreach ($polizaventadetalles->result() as $item) { 
                if($item->precio_local !==NULL) {
                    $totalc=$item->precio_local;
                }
                if($item->precio_semi !==NULL) {
                    $totalc=$item->precio_semi;
                }
                if($item->precio_foraneo !==NULL) {
                    $totalc=$item->precio_foraneo;
                }
                $totalgeneral=$totalgeneral+($totalc);
                //$totalgeneral=$totalgeneral+($totalc*item->cantidad);
            }
            $totalgeneral=$totalgeneral+($totalgeneral*0.16);
        }else{
            foreach ($resultfactura->result() as $item) {
                $totalgeneral=$totalgeneral+$item->total;
            }
        }

        return round($totalgeneral, 2); 
    }
    function montopoliza2($id){//polizas creadas
        $totalgeneral=0;
        $resultfactura=$this->ontenerdatosfacturapoliza($id);
        if($resultfactura->num_rows()==0){
            $totalc=0;
            $polizaventadetalles = $this->polizaventadetalles3($id);
            foreach ($polizaventadetalles->result() as $item) { 
                if($item->precio_local !==NULL) {
                    $totalc=$item->precio_local;
                }
                if($item->precio_semi !==NULL) {
                    $totalc=$item->precio_semi;
                }
                if($item->precio_foraneo !==NULL) {
                    $totalc=$item->precio_foraneo;
                }
                

                $totalgeneral=$totalgeneral+($totalc);
                //$totalgeneral=$totalgeneral+($totalc*item->cantidad);
            }
            $subtotal=$totalgeneral;
            $iva=$subtotal*0.16;
            $totalgeneral=$subtotal+$iva;
        }else{
            foreach ($resultfactura->result() as $item) {
                $subtotal=$item->subtotal;
                $iva=$item->iva;
                $totalgeneral=$item->total;
            }
        }
        $array=array(
            'subtotal'=>$subtotal,
            'iva'=>$iva,
            'total'=>$totalgeneral
        );
        return $array; 
    }
    public function table_get_tipo_compra_poliza($id){
        $totalgeneral=0;
        $resultfactura=$this->ontenerdatosfacturapoliza($id);
        if($resultfactura->num_rows()==0){
            $result = $this->pagos_poliza($id);
            foreach ($result as $item) {
                $totalgeneral=$totalgeneral+$item->pago;
            }
        }else{
            foreach($resultfactura->result() as $item){
                    $FacturasIdselected=$item->FacturasId;
                    $resultvcp = $this->obtencionventapolizafactura($item->FacturasId);

                    foreach ($resultvcp->result() as $itemvcp) {
                        if($itemvcp->combinada==0){
                            //=======================================
                                 $result = $this->pagos_ventas($itemvcp->ventaId); 
                                    foreach ($result as $item) { 
                                        $totalgeneral=$totalgeneral+$item->pago;
                                    } 
                            //=======================================
                        }elseif ($itemvcp->combinada==1) {
                            //========================================
                                $result = $this->pagos_combinada($itemvcp->ventaId); 
                                foreach ($result as $item) { 
                                    $totalgeneral=$totalgeneral+$item->pago;
                                } 
                            //========================================
                        }elseif ($itemvcp->combinada==2) {
                            //========================================
                                $result = $this->pagos_poliza($itemvcp->ventaId);
                                foreach ($result as $item) {
                                    $totalgeneral=$totalgeneral+$item->pago;
                                }
                            //========================================
                        }
                    }
                    $resultvcp_cp = $this->obteciondecomplememtofactura($FacturasIdselected);
                    foreach ($resultvcp_cp->result() as $itemcp) {
                        $totalgeneral=$totalgeneral+$itemcp->ImpPagado;
                    }
                }
        }
        return $totalgeneral;
    }
    function consultarcomplementosfactura($facturaId){
        $strq="SELECT comd.* FROM f_complementopago_documento as comd INNER JOIN f_complementopago as com on com.complementoId=comd.complementoId WHERE comd.facturasId=$facturaId AND com.Estado=1";
       $query = $this->db->query($strq);
        return $query; 
    }
    function facturaventas($factura){
        $strq="SELECT v.estatus FROM factura_venta as fv INNER JOIN ventas as v on v.id=fv.ventaId WHERE fv.facturaId=$factura AND fv.combinada=0";
        $query = $this->db->query($strq);
        return $query;
    }
    function facturapolizas($factura){
        $strq="SELECT pol.estatus FROM factura_poliza as fpol INNER JOIN polizasCreadas as pol on pol.id=fpol.polizaId WHERE fpol.facturaId=$factura ";
        $query = $this->db->query($strq);
        return $query;
    }
    function facturarenta($factura){
        $strq="SELECT statuspago FROM factura_prefactura WHERE facturaId=$factura ";
        $query = $this->db->query($strq);
        return $query;
    }
    function facturaventascombinada($factura){
        $strq="SELECT v.estatus FROM factura_venta as fv INNER JOIN ventacombinada as v on v.combinadaId=fv.ventaId WHERE fv.facturaId=$factura AND fv.combinada=1";
        $query = $this->db->query($strq);
        return $query;
    }
    function facturasdelperiodo($periodo){
        //$strq="SELECT * FROM `factura_prefactura` WHERE facId=$periodo";
        $strq="SELECT facturaId,1 as tipo
                FROM `factura_prefactura` 
                WHERE facId=$periodo 
                UNION 
                SELECT 
                    fac2.FacturasId as facturaId,2 as tipo
                    FROM factura_prefactura as fp 
                    INNER JOIN f_facturas as fac on fac.FacturasId=fp.facturaId
                    INNER JOIN f_facturas as fac2 on fac2.f_r_uuid=fac.uuid and fac2.f_r_uuid!=''
                WHERE fp.facId=$periodo";
        $query = $this->db->query($strq);
        return $query;
    }
    function montototalpoliza($idventa){
        $total=0;
        $strq="SELECT 
                    SUM(vpd.cantidad*(IF(vpd.precio_local>0, vpd.precio_local, 0)+IF(vpd.precio_semi>0, vpd.precio_semi, 0)+IF(vpd.precio_foraneo>0, vpd.precio_foraneo, 0)+IF(vpd.precio_especial>0, vpd.precio_especial, 0))) as total 
                FROM polizasCreadas_has_detallesPoliza as vpd 
                WHERE vpd.idPoliza=$idventa";
        $query = $this->db->query($strq);
        foreach ($query->result() as $item) {
            $total=$item->total;
        }
        return $total;
    }
    function diferenciaentrehoras($inicio,$fin){
        $horaInicio = new DateTime($inicio);
        $horaTermino = new DateTime($fin);

        $interval = $horaInicio->diff($horaTermino);
        $diferencia = $interval->format('%H-%i-%s');
        
        $horas=$interval->h;
        $min=$interval->i;
        //echo $diferencia.'<br>';

        $totalmin=($horas*60)+$min;
        
        return $totalmin;
    }
    function foliosviculados($id){
        $strq="SELECT fac.Folio
                    FROM `factura_venta` as ven 
                    INNER JOIN f_facturas as fac on fac.FacturasId=ven.facturaId 
                    WHERE ven.ventaId=$id and fac.Estado=1
                UNION
                SELECT fac.Folio
                    FROM `factura_poliza` as pol 
                    INNER JOIN f_facturas as fac on fac.FacturasId=pol.facturaId and fac.Estado=1
                    WHERE pol.polizaId=$id";
        $query = $this->db->query($strq);
        return $query; 
    }

    function acc_equipo_serie($add){
        $strq="SELECT rde.modelo,sp.serie FROM rentas_has_detallesEquipos_accesorios as rda
            INNER JOIN rentas_has_detallesEquipos as rde on rde.id=rda.rowequipo
            LEFT join asignacion_series_r_equipos as asre on asre.id_equipo=rde.id
            left join series_productos as sp on sp.serieId=asre.serieId
            WHERE rda.id_accesoriod=$add";
        $query = $this->db->query($strq);
        return $query; 
    }

    function get_validar_periodo($id){
        $strq="SELECT arp.* FROM alta_rentas as ar INNER JOIN alta_rentas_prefactura as arp on arp.id_renta=ar.id_renta WHERE ar.idcontrato=$id";
        $query = $this->db->query($strq);
        return $query->result(); 
    }
    function obtenerequipo($id){
        if($id>0){

        }else{
            $id=0;            
        }
        $strq = 'SELECT * FROM `equipos`
                WHERE id='.$id; 
        $query = $this->db->query($strq);
        $modelo='';
        foreach ($query->result() as $item) {
            $modelo=$item->modelo;
        }
        return $modelo;
    }
    function subtotal_cotizacion($idcotizacion){
        $strq="SELECT sum(subtotal) as subtotal from(        
                    SELECT sum(cotc.totalGeneral) as subtotal FROM cotizaciones_has_detallesConsumibles as cotc WHERE cotc.idCotizacion=$idcotizacion
                    union
                    SELECT sum(cote.cantidad*cote.precio) as subtotal FROM cotizaciones_has_detallesEquipos as cote WHERE cote.idCotizacion=$idcotizacion
                    union
                    SELECT sum(cotea.cantidad*ca.costo) as subtotal FROM cotizaciones_has_detallesEquipos_accesorios as cotea INNER JOIN catalogo_accesorios as ca on ca.id=cotea.id_accesorio WHERE cotea.id_cotizacion=$idcotizacion
                    union
                    SELECT sum(cotp.cantidad*(IFNULL(cotp.precio_local,0)+IFNULL(cotp.precio_semi,0)+IFNULL(cotp.precio_foraneo,0)+IFNULL(cotp.precio_especial,0) )) as subtotal FROM cotizaciones_has_detallesPoliza as cotp WHERE cotp.idCotizacion=$idcotizacion
                    union
                    SELECT sum(cotr.totalGeneral) as subtotal FROM cotizaciones_has_detallesRefacciones as cotr WHERE cotr.idCotizacion=$idcotizacion
            ) as datos";
        $query = $this->db->query($strq);
        $subtotal=0;
        foreach ($query->result() as $item) {
            $subtotal=$item->subtotal;
        }
        return $subtotal;
    }
    function obtenerpersonal($id){
        if($id>0){

        }else{
            $id=0;            
        }
        $strq = 'SELECT * FROM `personal`
                WHERE personalId='.$id; 
        $query = $this->db->query($strq);
        $personal='';
        foreach ($query->result() as $item) {
            $personal=$item->nombre.''.$item->apellido_materno;
        }
        return $personal;
    }
    //===================================================================================
        // solo consultas Dimpresion
        function accesoriosventasd($id){
            $strq = "SELECT vdea.id_accesoriod,vdea.cantidad,caa.nombre,caa.no_parte, vdea.serie,vdea.serie_estatus,vdea.costo,vdea.surtir,vdea.serie_bodega,vdea.comentario,
                    vdea.entregado_status,vdea.entregado_fecha,per.nombre as personal,per.apellido_paterno,bod.bodega,vdea.id_accesorio
                    FROM ventasd_has_detallesequipos_accesorios as vdea 
                    inner JOIN catalogo_accesorios as caa on caa.id=vdea.id_accesorio
                    left join personal as per on per.personalId=vdea.entregado_personal
                    left join bodegas as bod on bod.bodegaId=vdea.serie_bodega
                    WHERE vdea.id_venta='$id' "; 
            $query = $this->DB5->query($strq);
            return $query;
        }
        function consumiblesventasd($id){
            $strq = "SELECT vdc.id,vdc.cantidad,vdc.costo_toner,vdc.rendimiento_toner,vdc.rendimiento_unidad_imagen,con.modelo,con.parte,con.tipo,vdc.surtir,vdc.comentario,
                    vdc.entregado_status,vdc.entregado_fecha,eq.modelo as modeloeq,bod.bodega,vdc.bodega as serie_bodega,vdc.id_consumibles
                    FROM ventasd_has_detallesequipos_consumible as vdc
                    inner JOIN consumibles as con on con.id=vdc.id_consumibles
                    left join personal as per on per.personalId=vdc.entregado_personal
                    left join equipos as eq on eq.id=vdc.idequipo
                    left join bodegas as bod on bod.bodegaId=vdc.bodega
                    WHERE vdc.idventa='$id'"; 
            $query = $this->DB5->query($strq);
            return $query;
        }
        function consumiblesventasddetalles($id){
            $strq = "SELECT vdc.id,
                            vdc.piezas AS cantidad,
                            vdc.precioGeneral AS costo_toner,
                            con.modelo,
                            con.parte,
                            con.tipo,
                            vdc.surtir,
                            vdc.comentario,
                            vdc.idserie,
                            vdc.idEquipo,
                            per.nombre,per.nombre as personal,per.apellido_paterno,vdc.entregado_status,
                            vdc.entregado_fecha,
                            vdc.foliotext,
                            vdc.bodegas,
                            eq.modelo as modeloeq,
                            bod.bodega,
                            vdc.idConsumible
                    FROM ventasd_has_detallesconsumibles as vdc
                    inner JOIN consumibles as con on con.id=vdc.idConsumible
                    left join personal as per on per.personalId=vdc.entregado_personal
                    left join equipos as eq on eq.id=vdc.idEquipo
                    left join bodegas as bod on bod.bodegaId=vdc.bodegas
                    WHERE vdc.idVentas='$id'"; 
            $query = $this->DB7->query($strq);
            return $query;
        }
        public function ventasd_has_detallesrefacciones($idrefaccion){
            $strq = "SELECT v.id,
                            v.piezas AS cantidad,
                            equ.nombre AS modelo,
                            v.rendimiento,
                            v.descripcion,
                            v.precioGeneral,v.totalGeneral,
                            v.serie_estatus,
                            v.surtir,
                            v.comentario,
                            equ.codigo,
                            equi.modelo as modeloeq,
                            v.serie as serieeq,
                            v.idEquipo,
                            v.entregado_status,
                            v.entregado_fecha,
                            per.nombre as personal,per.apellido_paterno,
                            bod.bodega,
                            v.serie_bodega,
                            v.idRefaccion
                    FROM ventasd_has_detallesrefacciones AS v
                    INNER JOIN refacciones AS equ on equ.id=v.idRefaccion
                    left JOIN equipos AS equi on equi.id=v.idEquipo
                    left join personal as per on per.personalId=v.entregado_personal
                    left join bodegas as bod on bod.bodegaId=v.serie_bodega
                    WHERE v.idVentas='$idrefaccion' GROUP by v.id"; 
            $query = $this->db->query($strq);
            return $query;
        }
        public function ventas_facturas($tipo,$id){
            $strq = "SELECT fac.Folio FROM `factura_venta` as ven INNER JOIN f_facturas as fac on fac.FacturasId=ven.facturaId WHERE ven.combinada=$tipo and ven.ventaId=$id and fac.Estado=1"; 
            $query = $this->db->query($strq);
            return $query;
        }
        public function poliza_facturas($id){
            $strq = "SELECT fac.Folio
                                FROM `factura_poliza` as pol 
                                INNER JOIN f_facturas as fac on fac.FacturasId=pol.facturaId and fac.Estado=1
                                WHERE pol.polizaId=$id"; 
            $query = $this->db->query($strq);
            return $query;
        }
    //===================================================================================
        function vertecnicorh($equipo){
            $strq = "SELECT per.nombre,per.apellido_paterno,per.apellido_materno,per.personalId
                    FROM asignacion_ser_contrato_a as ac
                    INNER JOIN asignacion_ser_contrato_a_e as acd on acd.asignacionId=ac.asignacionId
                    INNER JOIN personal as per on per.personalId=acd.tecnico
                    WHERE ac.activo=1 AND acd.activo=1 AND ac.tpoliza=17 and  acd.idequipo=$equipo "; 
            $query = $this->db->query($strq);
            return $query;
        }
        function actualizarstatusprefactura($contrato){
            /*
            $strq = "SELECT 
                            per.prefId,per.total,fac.Folio,fac.subtotal 
                    FROM alta_rentas_prefactura as per 
                    INNER JOIN alta_rentas_prefactura_d as perd on perd.prefId=per.prefId 
                    INNER JOIN alta_rentas as ar on ar.id_renta=per.id_renta 
                    INNER JOIN factura_prefactura as facp on facp.prefacturaId=per.prefId 
                    INNER JOIN f_facturas as fac on fac.FacturasId=facp.facturaId and fac.Estado=1
                    WHERE 
                        ar.idcontrato=$contrato AND 
                        (per.factura_renta=0 OR per.factura_excedente=0 OR perd.statusfacturar=0 OR perd.statusfacturae=0) 
                        GROUP BY per.prefId;"; 
            */
            $strq="SELECT datos.*,(SELECT SUM(fac.subtotal) FROM factura_prefactura as facp 
                INNER JOIN f_facturas as fac on fac.FacturasId=facp.facturaId and fac.Estado=1
                WHERE facp.prefacturaId=datos.prefId ) as subtotal FROM (
                SELECT 
                            per.prefId,per.total 
                    FROM alta_rentas_prefactura as per 
                    INNER JOIN alta_rentas_prefactura_d as perd on perd.prefId=per.prefId 
                    INNER JOIN alta_rentas as ar on ar.id_renta=per.id_renta 
                    WHERE 
                        ar.idcontrato=$contrato AND 
                        (per.factura_renta=0 OR per.factura_excedente=0 OR perd.statusfacturar=0 OR perd.statusfacturae=0) 
                        GROUP BY per.prefId
            ) as datos";
            $query = $this->db->query($strq);
            return $query;
        }
        function getregistrocontadores(){
            $strq = "SELECT * FROM `carga_contadores` WHERE activo=1 GROUP BY reg ORDER BY `carga_contadores`.`reg` DESC LIMIT 10"; 
            $query = $this->db->query($strq);
            return $query;
        }
        function get_carga_contadores($fecha,$contrato){
            if($contrato>0){
                $contrato_where=" where datos.idcontrato='$contrato' ";
            }else{
                $contrato_where='';
            }
            $strq = "SELECT 
                            datos.*,cont2.folio,cli.empresa,ren.idCliente
                            FROM(
                                SELECT 
                                    cc.*,
                                    sp.serieId,
                                    arpd.prefId as cm,
                                    arpd2.prefId as ccol,
                                    arpd3.prefId as cscan,
                                    (SELECT cont.idcontrato
                                                    FROM asignacion_series_r_equipos as asige
                                                    inner join rentas_has_detallesEquipos as rente on rente.id=asige.id_equipo
                                                    left join contrato as cont on cont.idRenta=rente.idRenta AND cont.estatus=1
                                                    INNER JOIN clientes as cli on cli.id=asige.idCliente
                                                    WHERE asige.serieId=sp.serieId AND asige.activo=1 
                                                    group by cont.idcontrato ORDER BY `cont`.`idcontrato` DESC LIMIT 1) as idcontrato
                                FROM carga_contadores as cc
                                LEFT JOIN series_productos as sp on sp.serie=cc.serie AND sp.activo=1
                                LEFT JOIN alta_rentas_prefactura_d as arpd on arpd.serieId=sp.serieId AND cc.cont_mono=arpd.c_c_f AND cc.cont_mono>0
                                LEFT JOIN alta_rentas_prefactura_d as arpd2 on arpd2.serieId=sp.serieId AND cc.cont_color=arpd2.c_c_f AND cc.cont_color>0
                                LEFT JOIN alta_rentas_prefactura_d as arpd3 on arpd3.serieId=sp.serieId AND cc.cont_escaner=arpd3.e_c_f AND cc.cont_escaner>0
                                WHERE cc.reg='$fecha' and cc.activo=1
                            ) as datos
                            LEFT JOIN contrato as cont2 on cont2.idcontrato=datos.idcontrato
                            LEFT JOIN rentas as ren on ren.id=cont2.idRenta
                            LEFT JOIN clientes as cli on cli.id=ren.idCliente $contrato_where"; 
            $query = $this->db->query($strq);
            return $query;
        }
        function get_info_pendiente_solicitud_refaccion($idequipo,$serieId,$seriename){
            $strq = "SELECT
                        asignacionIde,
                        asignacionId,
                        cot_refaccion,
                        cot_refaccion2,
                        cot_refaccion3,
                        1 as tiposer
                    FROM asignacion_ser_contrato_a_e
                    WHERE cot_status=1 and idequipo=$idequipo and serieId=$serieId

                    union

                    SELECT
                        asignacionIde,
                        asignacionId,
                        cot_refaccion,
                        cot_refaccion2,
                        cot_refaccion3,
                        2 as tiposer
                    FROM asignacion_ser_poliza_a_e
                    WHERE cot_status=1 and serie='$seriename' and serie!=''

                    union

                    SELECT
                        asignacionIdd as asignacionIde,
                        asignacionId,
                        cot_refaccion,
                        cot_refaccion2,
                        cot_refaccion3,
                        3 as tiposer
                    FROM asignacion_ser_cliente_a_d
                    WHERE cot_status=1 and serie='$seriename' and serie!=''

                    union

                    SELECT
                        0 as asignacionIde,
                        ser.asignacionId,
                        ser.cot_refaccion,
                        ser.cot_refaccion2,
                        ser.cot_refaccion3,
                        4 as tiposer
                    FROM asignacion_ser_venta_a_d as serd
                    inner join asignacion_ser_venta_a as ser on ser.asignacionId=serd.asignacionId
                    WHERE ser.cot_status=1 and serd.serie='$seriename' and serd.serie!=''";
            $query = $this->db->query($strq);
            return $query;
        }
        function get_detalles_incidentes($id){
            $strq = "SELECT 
                        ubi.id,
                        uni.modelo,
                        per.nombre,
                        per.apellido_paterno,
                        ubi.detalle,
                        ubi.evidencia,
                        ubi.reg 
                    FROM unidades_bitacora_incidentes as ubi 
                    INNER JOIN unidades as uni on uni.id=ubi.unidadid 
                    INNER JOIN personal as per on per.personalId=ubi.tecnico_responsable 
                    WHERE ubi.activo=1 AND ubi.unidadid=$id "; 
            $query = $this->db->query($strq);
            return $query;
        }
        function get_detalles_itinerario($idpersonal,$fecha){
            $strq = "SELECT * FROM unidades_bitacora_itinerario WHERE activo=1 AND personal=$idpersonal and fecha='$fecha' ORDER BY hora_inicio ASC"; 
            $query = $this->db->query($strq);
            return $query;
        }
        function get_detalles_itinerario_get($id){
            $strq = "SELECT iti.*, per.nombre,per.apellido_paterno,per.apellido_materno
                    FROM unidades_bitacora_itinerario as iti 
                    JOIN personal as per on per.personalId=iti.personal
                    WHERE iti.activo=1 AND iti.unidadid=$id  "; 
            $query = $this->db->query($strq);
            return $query;
        }
        function get_reporte_entradas_salidas($fechaini,$fechafin,$tipo,$producto){
            if($fechaini!=''){
                $fechaini_where=" AND bm.reg >='$fechaini 00:00:00' ";
            }else{
                $fechaini_where="";
            }
            if($fechafin!=''){
                $fechafin_where=" AND bm.reg <='$fechafin 23:59:59' ";
            }else{
                $fechafin_where="";
            }

            if($producto>0){
                $producto_where=" and bm.modeloId=$producto ";
            }else{
                $producto_where="";
            }
            $strq = "";

            $strq .= "SELECT * from (";
                    if($tipo==0 || $tipo==1){

                    $strq .= "(SELECT bm.id,bm.entrada_salida,bm.descripcion_evento, per.nombre,per.apellido_paterno,per.apellido_materno,bm.tipo, eq.modelo,eq.noparte, bm.modelo as modeloext,sp.serie,bo.bodega as bodegao,bd.bodega as bodegad,bm.cantidad,cli.empresa,bm.reg
                        FROM bitacora_movimientos as bm 
                        LEFT JOIN personal as per on per.personalId=bm.personal
                        LEFT JOIN bodegas as bo on bo.bodegaId=bm.bodega_o
                        LEFT JOIN bodegas as bd on bd.bodegaId=bm.bodega_d
                        LEFT JOIN clientes as cli on cli.id=bm.clienteId
                        LEFT JOIN equipos as eq on eq.id=bm.modeloId
                        LEFT JOIN series_productos as sp on sp.serieId=bm.serieId
                        WHERE bm.tipo=1 $fechaini_where $fechafin_where $producto_where)";
                    }
                    if($tipo==0){
                        $strq .= " UNION ";
                    }
                    if($tipo==0 || $tipo==3){
                        
                        $strq .= "(SELECT bm.id,bm.entrada_salida,bm.descripcion_evento, per.nombre,per.apellido_paterno,per.apellido_materno,bm.tipo,acc.nombre as modelo,acc.no_parte as noparte,  bm.modelo as modeloext,sa.serie,bo.bodega as bodegao,bd.bodega as bodegad,bm.cantidad,cli.empresa,bm.reg 
                        FROM bitacora_movimientos as bm 
                        LEFT JOIN personal as per on per.personalId=bm.personal
                        LEFT JOIN bodegas as bo on bo.bodegaId=bm.bodega_o
                        LEFT JOIN bodegas as bd on bd.bodegaId=bm.bodega_d
                        LEFT JOIN clientes as cli on cli.id=bm.clienteId
                        LEFT JOIN catalogo_accesorios as acc on acc.id=bm.modeloId
                        LEFT JOIN series_accesorios as sa on sa.serieId=bm.serieId
                        WHERE bm.tipo=2 $fechaini_where $fechafin_where $producto_where)";
                    }
                    if($tipo==0){
                        $strq .= " UNION ";
                    }
                    if($tipo==0 || $tipo==2){
                        
                        $strq .= "(SELECT bm.id,bm.entrada_salida,bm.descripcion_evento, per.nombre,per.apellido_paterno,per.apellido_materno,bm.tipo,ref.nombre as modelo,ref.codigo as noparte,  bm.modelo as modeloext,sr.serie,bo.bodega as bodegao,bd.bodega as bodegad,bm.cantidad,cli.empresa ,bm.reg
                        FROM bitacora_movimientos as bm 
                        LEFT JOIN personal as per on per.personalId=bm.personal
                        LEFT JOIN bodegas as bo on bo.bodegaId=bm.bodega_o
                        LEFT JOIN bodegas as bd on bd.bodegaId=bm.bodega_d
                        LEFT JOIN clientes as cli on cli.id=bm.clienteId
                        LEFT JOIN refacciones as ref on ref.id=bm.modeloId
                        LEFT JOIN series_refacciones as sr on sr.serieId=bm.serieId
                        WHERE bm.tipo=3 $fechaini_where $fechafin_where $producto_where)";
                    }
                    if($tipo==0){
                        $strq .= " UNION ";
                    }
                    if($tipo==0 || $tipo==4){
                        
                        $strq .= "(SELECT bm.id,bm.entrada_salida,bm.descripcion_evento, per.nombre,per.apellido_paterno,per.apellido_materno,bm.tipo,con.modelo,con.parte as noparte,  bm.modelo as modeloext,'N.A.' as serie,bo.bodega as bodegao,bd.bodega as bodegad,bm.cantidad,cli.empresa,bm.reg 
                        FROM bitacora_movimientos as bm 
                        LEFT JOIN personal as per on per.personalId=bm.personal
                        LEFT JOIN bodegas as bo on bo.bodegaId=bm.bodega_o
                        LEFT JOIN bodegas as bd on bd.bodegaId=bm.bodega_d
                        LEFT JOIN clientes as cli on cli.id=bm.clienteId
                        LEFT JOIN consumibles as con on con.id=bm.modeloId
                        WHERE bm.tipo=4 $fechaini_where $fechafin_where $producto_where)";
                    }
                    $strq .= ") as datos
                        ORDER BY datos.id  DESC"; 
            $query = $this->db->query($strq);
            return $query;
        }
        function getdireccionequipos2($equipo,$serie,$fecha){
            $resultdir=$this->db2_getselectwheren('contrato_equipo_direccion',array('equiposrow'=>$equipo,'serieId'=>$serie));
            $iddireccionget='g_0';
            $direccion='';
            foreach ($resultdir->result() as $itemdir) {
                if($itemdir->direccion!=''){
                    $iddireccionget=$itemdir->direccion;
                }
            }
            //log_message('error','iddireccionget '.$iddireccionget);
            if($iddireccionget!='g_0'){
                $tipodireccion=explode('_',$iddireccionget);

                $tipodir = $tipodireccion[0];
                $iddir   = $tipodireccion[1];
                if ($tipodir=='g') {
                    $resultdirc_where['idclientedirecc']=$iddir;
                    
                    if($fecha>=$this->fechahoy){
                        //log_message('error','entra direccion g');
                        $resultdirc_where['status']=1;
                    }
                    $resultdirc=$this->db2_getselectwheren('clientes_direccion',$resultdirc_where);
                    foreach ($resultdirc->result() as $item) {
                        $direccion=$item->direccion;
                    }
                }
                if ($tipodir=='f') {
                    $resultdirc_where['id']=$iddir;
                    
                    if($fecha>=$this->fechahoy){
                        //log_message('error','entra direccion f');
                        $resultdirc_where['activo']=1;
                    }
                    $resultdirc=$this->db2_getselectwheren('cliente_has_datos_fiscales',$resultdirc_where);
                    foreach ($resultdirc->result() as $item) {
                        $direccion='Razon social: '.$item->razon_social.' Colonia:'.$item->colonia.' Calle:'.$item->calle.' Numero:'.$item->num_ext;
                    }
                }
            }
            return $direccion;
        }
        function lisitinerario($inicio,$fin,$tecnico){
            if($tecnico>0){
                $tecnico_w=" and iti.personal=$tecnico ";
            }else{
                $tecnico_w='';
            }
            $strq="SELECT 
                        iti.id,
                        uni.modelo,
                        uni.marca,
                        iti.descripcion,
                        iti.fecha,
                        iti.hora_inicio,
                        '' as hora_fin,
                        per.nombre,
                        per.apellido_paterno,
                        per.apellido_materno
                FROM `unidades_bitacora_itinerario` as iti
                left JOIN unidades as uni on uni.id=iti.unidadid
                INNER JOIN personal as per on per.personalId=iti.personal
                WHERE iti.activo=1 and iti.fecha >= '$inicio' and iti.fecha <= '$fin' $tecnico_w
                ";
            $query = $this->db->query($strq);
            return $query;
        }
        function save_registro_stock(){
            if($this->diahoyc==1){//tiene que ser 1
                $result_reg = $this->getselectwheren('bitacora_stock_productos',array('fecha'=>$this->fechahoyc));
                if($result_reg->num_rows()==0){
                    $insert_array=array();
                    //===============================================
                        $result_equipo = $this->registro_stock_eq();
                        foreach ($result_equipo->result() as $item) {
                            $insert_array[] = array('tipo'=>1,'item'=>$item->productoid,'stock'=>$item->stock,'fecha'=>$this->fechahoyc);
                        }
                    //===============================================
                        $result_consu = $this->registro_stock_consu();
                        foreach ($result_consu->result() as $item) {
                            $insert_array[] = array('tipo'=>2,'item'=>$item->id,'stock'=>$item->total,'fecha'=>$this->fechahoyc);
                        }
                    //===============================================
                        $result_acces = $this->registro_stock_acce();
                        foreach ($result_acces->result() as $item) {
                            $insert_array[] = array('tipo'=>3,'item'=>$item->id,'stock'=>$item->total,'fecha'=>$this->fechahoyc);
                        }
                    //===============================================
                        $result_ref = $this->registro_stock_ref();
                        foreach ($result_ref->result() as $item) {
                            $insert_array[] = array('tipo'=>4,'item'=>$item->id,'stock'=>$item->cantidad,'fecha'=>$this->fechahoyc);
                        }
                    //===============================================
                        if(count($insert_array)>0){
                            $this->insert_batch('bitacora_stock_productos',$insert_array);
                        }
                    //===============================================


                }    
            }   
        }
        function registro_stock_eq(){
            $strq="SELECT sp.productoid, bod.bodega, eq.modelo,sp.bodegaId,COUNT(*) as stock 
                    FROM series_productos sp 
                    JOIN bodegas bod ON bod.bodegaId=sp.bodegaId 
                    JOIN equipos eq ON eq.id=sp.productoid 
                    WHERE sp.status < 2 AND sp.activo = 1 AND eq.estatus=1
                    GROUP BY eq.id";
            $query = $this->db->query($strq);
            return $query;
        }
        function registro_stock_consu(){
            $strq="SELECT `co`.`id`, `cobo`.`bodegaId`, `co`.`parte`, `co`.`modelo`, IFNULL(SUM(`cobo`.`total`),0) as total, `bo`.`bodega`, `cobo`.`consubId`
                    FROM `consumibles` `co`
                    LEFT JOIN `consumibles_bodegas` `cobo` ON `co`.`id`=`cobo`.`consumiblesId` and `cobo`.`status` = 1
                    LEFT JOIN `bodegas` `bo` ON `bo`.`bodegaId`=`cobo`.`bodegaId`
                    WHERE `co`.`status` = 1 GROUP BY `co`.`id`";
            $query = $this->db->query($strq);
            return $query;
        }
        function registro_stock_acce(){
            $strq="SELECT 
                        id,
                        nombre,
                        SUM(stock2+stock1) as total 
                        FROM( 
                            SELECT 
                                `cata`.`id`, 
                                `via`.`bodega`, 
                                `cata`.`nombre`, 
                                `cata`.`no_parte`, 
                                IFNULL((select sum(sacc.cantidad) from series_accesorios as sacc where sacc.accesoriosid=cata.id and sacc.bodegaId=via.bodegaId and sacc.status<2 and sacc.con_serie=0 and sacc.activo=1),0) as stock2, 
                                IFNULL((select count(*) from series_accesorios as sacc where sacc.accesoriosid=cata.id and sacc.bodegaId=via.bodegaId and sacc.status<2 and sacc.con_serie=1 and sacc.activo=1),0) as stock1 
                            FROM `catalogo_accesorios` `cata` 
                            LEFT JOIN `vista_inventario_accesorios` `via` ON `via`.`id`=`cata`.`id` 
                            WHERE `cata`.`status` = 1 
                        ) as datos GROUP BY id";
            $query = $this->db->query($strq);
            return $query;
        }
        function registro_stock_ref(){
            $strq="SELECT 
                        id,
                        nombre,
                        SUM(cantidad) as cantidad 
                        FROM(
                            SELECT `cata`.`id`, `vire`.`bodega`, `cata`.`codigo`, `cata`.`nombre`, `vire`.`cantidad`, `vire`.`con_serie`, `vire`.`serieId`
                            FROM `refacciones` `cata`
                            LEFT JOIN `vista_inventario_refacciones` `vire` ON `vire`.`id`=`cata`.`id`
                            WHERE `cata`.`status` = 1
                        ) as datos GROUP by id";
            $query = $this->db->query($strq);
            return $query;
        }
        function reg_entrada_salida_g_eq($fechai,$fechas,$producto){
            if($producto>0){
                $producto_w=" and eq.id=$producto ";
            }else{
                $producto_w="";
            }
            $strq="SELECT 
                        eq.id,
                        eq.noparte,
                        eq.modelo,
                        IFNULL(bsp.stock,0) AS stockinicial,
                        IFNULL((SELECT sum(bme.cantidad) FROM bitacora_movimientos as bme WHERE bme.modeloId=eq.id AND bme.tipo=1 AND bme.entrada_salida=1 AND bme.reg BETWEEN '$fechai 00:00:00' AND '$fechas 23:59:59'),0) as cantidadentrada,
                        IFNULL((SELECT sum(bms.cantidad) FROM bitacora_movimientos as bms WHERE bms.modeloId=eq.id AND bms.tipo=1 AND bms.entrada_salida=0 AND bms.reg BETWEEN '$fechai 00:00:00' AND '$fechas 23:59:59'),0) as cantidadsalida
                    FROM equipos as eq 
                    LEFT JOIN bitacora_stock_productos as bsp on bsp.item=eq.id AND bsp.tipo=1 AND bsp.fecha BETWEEN '$fechai' AND '$fechas'
                    WHERE eq.estatus=1  $producto_w
                    GROUP BY eq.id
                    ORDER BY `eq`.`id` ASC";
            $query = $this->db->query($strq);
            return $query;
        }
        function reg_entrada_salida_g_acc($fechai,$fechas,$producto){
            if($producto>0){
                $producto_w=" and cata.id=$producto ";
            }else{
                $producto_w="";
            }
            $strq="SELECT 
                        cata.id,
                        cata.no_parte,
                        cata.nombre,
                        IFNULL(bsp.stock,0) AS stockinicial,
                        IFNULL((SELECT sum(bme.cantidad) FROM bitacora_movimientos as bme WHERE bme.modeloId=cata.id AND bme.tipo=2 AND bme.entrada_salida=1 AND bme.reg BETWEEN '$fechai 00:00:00' AND '$fechas 23:59:59'),0) as cantidadentrada,
                        IFNULL((SELECT sum(bms.cantidad) FROM bitacora_movimientos as bms WHERE bms.modeloId=cata.id AND bms.tipo=2 AND bms.entrada_salida=0 AND bms.reg BETWEEN '$fechai 00:00:00' AND '$fechas 23:59:59'),0) as cantidadsalida
                    FROM catalogo_accesorios as cata
                    LEFT JOIN bitacora_stock_productos as bsp on bsp.item=cata.id AND bsp.tipo=3 AND bsp.fecha BETWEEN '$fechai' AND '$fechas'
                    WHERE cata.status=1 $producto_w
                    GROUP BY cata.id";
            $query = $this->db->query($strq);
            return $query;
        }
        function reg_entrada_salida_g_ref($fechai,$fechas,$producto){
            if($producto>0){
                $producto_w=" and ref.id=$producto ";
            }else{
                $producto_w="";
            }
            $strq="SELECT 
                        ref.id,
                        ref.codigo,
                        ref.nombre,
                        IFNULL(bsp.stock,0) AS stockinicial,
                        IFNULL((SELECT sum(bme.cantidad) FROM bitacora_movimientos as bme WHERE bme.modeloId=ref.id AND bme.tipo=3 AND bme.entrada_salida=1 AND bme.reg BETWEEN '$fechai 00:00:00' AND '$fechas 23:59:59'),0) as cantidadentrada,
                        IFNULL((SELECT sum(bms.cantidad) FROM bitacora_movimientos as bms WHERE bms.modeloId=ref.id AND bms.tipo=3 AND bms.entrada_salida=0 AND bms.reg BETWEEN '$fechai 00:00:00' AND '$fechas 23:59:59'),0) as cantidadsalida
                    FROM refacciones as ref
                    LEFT JOIN bitacora_stock_productos as bsp on bsp.item=ref.id AND bsp.tipo=4 AND bsp.fecha BETWEEN '$fechai' AND '2023-08-31'
                    WHERE ref.status=1 $producto_w
                    GROUP BY ref.id";
            $query = $this->db->query($strq);
            return $query;
        }
        function reg_entrada_salida_g_cons($fechai,$fechas,$producto){
            if($producto>0){
                $producto_w=" and con.id=$producto ";
            }else{
                $producto_w="";
            }
            $strq="SELECT 
                    con.id,
                    con.parte,
                    con.modelo,
                    IFNULL(bsp.stock,0) AS stockinicial,
                    IFNULL((SELECT sum(bme.cantidad) FROM bitacora_movimientos as bme WHERE bme.modeloId=con.id AND bme.tipo=4 AND bme.entrada_salida=1 AND bme.reg BETWEEN '$fechai 00:00:00' AND '$fechas 23:59:59'),0) as cantidadentrada,
                    IFNULL((SELECT sum(bms.cantidad) FROM bitacora_movimientos as bms WHERE bms.modeloId=con.id AND bms.tipo=4 AND bms.entrada_salida=0 AND bms.reg BETWEEN '$fechai 00:00:00' AND '$fechas 23:59:59'),0) as cantidadsalida
                    FROM consumibles as con
                    LEFT JOIN bitacora_stock_productos as bsp on bsp.item=con.id AND bsp.tipo=2 AND bsp.fecha BETWEEN '$fechai' AND '$fechas'
                    WHERE con.status=1 $producto_w ";
            $query = $this->db->query($strq);
            return $query;
        }
        function result_cceq($idcc,$idpre,$idtipo){
            $strq="SELECT cceq.rowequipo,cceq.modeloid,cceq.serieid,eq.modelo,sp.serie,arpd.tipo,arpd.produccion_total,
                            arpd.c_c_i,arpd.c_c_f,arpd.produccion,arpd.e_c_i,arpd.e_c_f
                    FROM centro_costos_equipos as cceq
                    INNER JOIN equipos as eq on eq.id=cceq.modeloid
                    INNER JOIN series_productos as sp on sp.serieId=cceq.serieid
                    INNER JOIN contrato as con on con.idcontrato=cceq.idcontrato
                    INNER JOIN rentas as ren on ren.id=con.idRenta
                    INNER JOIN rentas_has_detallesEquipos as rde on rde.id=cceq.rowequipo
                    INNER JOIN alta_rentas_prefactura_d as arpd on arpd.productoId=cceq.rowequipo AND arpd.serieId=cceq.serieid
                    WHERE cceq.activo=1 AND cceq.idcentroc=$idcc AND arpd.prefId=$idpre AND arpd.tipo=$idtipo";
            $query = $this->db->query($strq);
            return $query;
        }
        function venta_verificarseries($id,$tipo){
            $block_envio=0;
            $equipo=$id;
            $refacciones=$id;
            if($tipo==1){
                $resultadov = $this->ModeloCatalogos->db6_getselectwheren('ventacombinada',array('combinadaId'=>$id));
                foreach ($resultadov->result() as $item) {
                    //$id_personal=$item->id_personal;
                    //$idCliente=$item->idCliente;
                    $equipo=$item->equipo;
                    //$consumibles=$item->consumibles;
                    $refacciones=$item->refacciones;
                    //$poliza=$item->poliza;
                    
                }
            }

            $resultadoequipos = $this->db6_getselectwheren('ventas_has_detallesEquipos',array('idVenta'=>$equipo));
            foreach ($resultadoequipos->result() as $item) { 
                if ($item->serie_estatus==0) {
                    $block_envio=1;
                }
            }
            $resultadoaccesorios = $this->accesoriosventa($equipo);
            foreach ($resultadoaccesorios->result() as $item) { 
                if ($item->serie_estatus==0) {
                    $block_envio=1;
                }
            }
            //$resultadoconsumibles = $this->consumiblesventa($id);
            //$consumiblesventadetalles = $this->consumiblesventadetalles($id);
            //$polizaventadetalles = $this->polizaventadetalles($id);
            $ventadetallesrefacion = $this->ventas_has_detallesRefacciones($refacciones);
            foreach ($ventadetallesrefacion->result() as $item) { 
                if ($item->serie_estatus==0) {
                    $block_envio=1;
                }
            }
            return $block_envio;
        
        }
        function load_pni($per){
            if($per>0){
                $whper=" and (pni.per_asignado='$per' or pni.per_asignado=0) ";
            }else{
                $whper='';
            }
            $strq="SELECT 
                        pni.id,
                        pni.pago,
                        pni.fecha,
                        pni.obser,
                        per.nombre,
                        per.apellido_paterno,
                        mp.formapago_text,
                        cli.empresa,
                        pera.nombre as personala,
                        pni.comen
                    FROM pagos_noidentificados as pni
                    INNER JOIN personal as per on per.personalId=pni.idpersonal_create
                    INNER JOIN f_formapago as mp on mp.id=pni.idmetodo
                    left join clientes as cli on cli.id=pni.clienteId
                    left JOIN personal as pera on pera.personalId=pni.per_asignado
                    WHERE pni.activo=1 $whper";
            $query = $this->db->query($strq);
            return $query;
        }
        function seleccionejecutivofirma(){
            
            $strq="SELECT per.personalId,per.nombre,per.apellido_paterno,per.apellido_materno,per.email,per.celular 
                    FROM personal as per 
                    WHERE per.estatus=1 AND per.email!='' ORDER BY per.nombre ASC";
            $query = $this->db->query($strq);
            return $query;
        }
        function obtencion_polisas_cliente_viewcont($cliente){
            
            $strq="SELECT pol.id,cli.empresa,pol.reg,pol.idCliente FROM polizasCreadas as pol
                    INNER JOIN clientes as cli on cli.id=pol.idCliente
                    WHERE pol.activo=1 and pol.viewcont=0 AND pol.prefactura=1 and pol.idCliente='$cliente'
                    ORDER BY pol.reg DESC";
            $query = $this->db->query($strq);
            return $query;
        }
        function obtencion_polisas_vinculadas($idPoliza){
            
            $strq="SELECT polext.idser,pol.id as idpoliza, cli.empresa,pol.reg,pol.idCliente,polext.id as idpolext
                    FROM polizasCreadas as pol
                    INNER JOIN clientes as cli on cli.id=pol.idCliente
                    INNER JOIN polizascreadas_ext_ser as polext on polext.idpoliza=pol.id 
                    WHERE polext.poliza_principal='$idPoliza' and polext.activo=1";
            $query = $this->db->query($strq);
            return $query;
        }
        function fecha_prox_ser($fechaini,$fechafi){
            $strq="SELECT pxf.id, pxf.idpoliza, pxf.fecha,cli.empresa
                    FROM polizascreadas_ext_fechas as pxf
                    INNER JOIN polizasCreadas as pol on pol.id=pxf.idpoliza
                    INNER JOIN clientes as cli on cli.id=pol.idCliente
                    WHERE pxf.activo=1 AND pol.activo=1 AND pxf.fecha BETWEEN '$fechaini' AND '$fechafi'";
            $query = $this->db->query($strq);
            return $query;
        }
        function verificarhora($hora_inicio){
            $hora_h=date('H', strtotime($hora_inicio));//24 horas con ceros iniciales
            $hora_g=date('G', strtotime($hora_inicio));//24 horas sin ceros iniciales
            $hora_i=date('i', strtotime($hora_inicio));//minutos
            switch ($hora_g) {
                case 0:
                    $fechanew='12:'.$hora_i;
                    break;
                case 1:
                    $fechanew='13:'.$hora_i;
                    break;
                case 2:
                    $fechanew='14:'.$hora_i;
                    break;
                case 3:
                    $fechanew='15:'.$hora_i;
                    break;
                case 4:
                    $fechanew='16:'.$hora_i;
                    break;
                case 5:
                    $fechanew='17:'.$hora_i;
                    break;
                case 6:
                    $fechanew='18:'.$hora_i;
                    break;
                case 20:
                    $fechanew='08:'.$hora_i;
                    break;
                case 21:
                    $fechanew='09:'.$hora_i;
                    break;
                case 22:
                    $fechanew='10:'.$hora_i;
                    break;
                case 23:
                    $fechanew='11:'.$hora_i;
                    break;
                
                default:
                    $fechanew=$hora_h.':'.$hora_i;
                    break;
            }
            return $fechanew;
        }
        function get_asignacion_ser_contrato_a_e($asignacionId){
            $strq="SELECT asce.* FROM asignacion_ser_contrato_a_e as asce 
                INNER JOIN asignacion_series_r_equipos as asre on asre.id_equipo=asce.idequipo AND asre.serieId=asce.serieId
                WHERE asce.asignacionId='$asignacionId' AND asre.activo=1";
            $query = $this->db->query($strq);
            return $query;
        }
        function get_listado_clientes_bloqueados_suspendidos($tipo=0){
            if($tipo==1){
                $opcionrandom=' ORDER BY RAND() LIMIT 1 ';
            }else{
                $opcionrandom='';
            }
            $strq="SELECT * from clientes WHERE estatus=1 AND (bloqueo=1 or bloqueo_fp=1) $opcionrandom";
            $query = $this->db->query($strq);
            return $query;
        }
        function consultarvacainca($idpersonal,$fecha){
            $strq="SELECT * FROM asistencias_vacaciones WHERE idpersonal='$idpersonal' AND fechainicio <= '$fecha' AND fechafin >= '$fecha' and activo=1 and (tipo=1 or tipo=2)";
            $query = $this->db->query($strq);
            return $query;
        }
        function get_obtener_ser($idcli,$fecha,$tipo){
            $fechaante = date ( 'Y-m-d',strtotime ( '-2 day', strtotime ( $fecha ) )) ;
            if($tipo==1){
                $strq="SELECT asserc.asignacionId,asserce.fecha,GROUP_CONCAT(serpro.serie SEPARATOR ' ') as seriesall
                        FROM asignacion_ser_contrato_a as asserc
                        INNER JOIN asignacion_ser_contrato_a_e as asserce on asserce.asignacionId=asserc.asignacionId 
                        INNER JOIN contrato as con on con.idcontrato=asserc.contratoId
                        INNER JOIN rentas as ren on ren.id=con.idRenta
                        INNER JOIN series_productos as serpro on serpro.serieId=asserce.serieId
                        WHERE (asserc.activo=1 AND asserce.nr=0 AND asserce.status<2 and asserce.activo=1 AND asserce.fecha>'$fecha' AND ren.idCliente='$idcli') or
                              (asserc.activo=1 AND asserce.nr=0 and asserce.activo=1 AND asserce.fecha>='$fechaante' and asserce.fecha<='$fecha' AND ren.idCliente='$idcli')
                        GROUP BY asserc.asignacionId";
            }
            if($tipo==2){
                $strq="SELECT assp.asignacionId,assp.fecha,pol.idCliente,GROUP_CONCAT(assp.serie SEPARATOR ' ') as seriesall
                        FROM asignacion_ser_poliza_a_e as assp
                        INNER JOIN asignacion_ser_poliza_a as asspp ON asspp.asignacionId=assp.asignacionId
                        INNER JOIN polizasCreadas as pol on pol.id=asspp.polizaId
                        WHERE (assp.activo=1 AND assp.nr=0 AND assp.status<2 AND assp.fecha>'$fecha' AND pol.idCliente='$idcli') or
                              (assp.activo=1 AND assp.nr=0 AND assp.fecha>='$fechaante' and  assp.fecha<='$fecha' AND pol.idCliente='$idcli')
                        GROUP BY assp.asignacionId";
            }
            if($tipo==3){
                $strq="SELECT acl.asignacionId,cli.empresa, acl.fecha,GROUP_CONCAT(acld.serie SEPARATOR ' ') as seriesall
                        FROM asignacion_ser_cliente_a acl
                        JOIN personal per ON per.personalId=acl.tecnico
                        LEFT JOIN personal per2 ON per2.personalId=acl.tecnico2
                        LEFT JOIN personal per3 ON per3.personalId=acl.tecnico3
                        LEFT JOIN personal per4 ON per4.personalId=acl.tecnico4
                        JOIN clientes cli ON cli.id=acl.clienteId
                        JOIN rutas zo ON zo.id=acl.zonaId
                        JOIN asignacion_ser_cliente_a_d acld ON acld.asignacionId=acl.asignacionId
                        JOIN polizas pol ON pol.id=acld.tpoliza
                        LEFT JOIN servicio_evento sereve ON sereve.id=acld.tservicio
                        LEFT JOIN polizas_detalles pold ON pold.id=acld.tservicio_a
                        WHERE (acl.fecha > '$fecha' AND acl.activo = 1 AND acld.activo = '1' AND acl.clienteId='$idcli') or
                              (acl.fecha >= '$fechaante' and acl.fecha <= '$fecha' AND acl.activo = 1 AND acld.activo = '1' AND acl.clienteId='$idcli')
                        GROUP BY acl.asignacionId";
            }
            if($tipo==4){
                $strq="SELECT sv.asignacionId,sv.fecha,cli.empresa,'' as seriesall
                        FROM asignacion_ser_venta_a as sv
                        INNER JOIN ventas as v on v.id=sv.ventaId
                        INNER JOIN clientes cli ON cli.id=v.idCliente
                        WHERE (sv.tipov=0 and v.idCliente='$idcli' and sv.status<2 AND sv.fecha>'$fecha' ) or 
                              (sv.tipov=0 and v.idCliente='$idcli' AND sv.fecha>='$fechaante' and sv.fecha<='$fecha' )
                        UNION
                        SELECT sv.asignacionId,sv.fecha,cli.empresa,'' as seriesall
                        FROM asignacion_ser_venta_a as sv
                        INNER JOIN ventacombinada as vc on vc.combinadaId=sv.ventaId
                        INNER JOIN ventas as ven on ven.id=vc.equipo
                        INNER JOIN clientes cli ON cli.id=ven.idCliente
                        WHERE (sv.tipov=1 and ven.idCliente='$idcli' and sv.status<2 AND sv.fecha>'$fecha' ) or 
                              (sv.tipov=1 and ven.idCliente='$idcli' AND sv.fecha>='$fechaante' and sv.fecha<='$fecha' ) 
                        ORDER BY fecha DESC";
            }
            
            $query = $this->db->query($strq);
            return $query;
        }
        function get_obtener_ser_cli($idcli,$fecha,$tipo){
            $fechaante = date ( 'Y-m-d',strtotime ( '-12 month', strtotime ( $fecha ) )) ;
            if($tipo==1){
                $strq="SELECT asserc.asignacionId,asserce.fecha,1 as tiposer
                        FROM asignacion_ser_contrato_a as asserc
                        INNER JOIN asignacion_ser_contrato_a_e as asserce on asserce.asignacionId=asserc.asignacionId 
                        INNER JOIN contrato as con on con.idcontrato=asserc.contratoId
                        INNER JOIN rentas as ren on ren.id=con.idRenta
                        WHERE asserc.activo=1 AND asserce.nr=0 AND asserce.status=2 and asserce.activo=1 AND asserce.fecha>'$fechaante' AND ren.idCliente='$idcli' 
                        GROUP BY asserc.asignacionId
                        ORDER BY asserce.fecha DESC ";
            }
            if($tipo==2){
                $strq="SELECT assp.asignacionId,assp.fecha,pol.idCliente,2 as tiposer
                        FROM asignacion_ser_poliza_a_e as assp
                        INNER JOIN asignacion_ser_poliza_a as asspp ON asspp.asignacionId=assp.asignacionId
                        INNER JOIN polizasCreadas as pol on pol.id=asspp.polizaId
                        WHERE assp.activo=1 AND assp.nr=0 AND assp.status=2 AND assp.fecha>'$fechaante' AND pol.idCliente='$idcli'
                        GROUP BY assp.asignacionId
                        ORDER BY assp.fecha DESC";
            }
            if($tipo==3){
                $strq="SELECT `acl`.`asignacionId`,`cli`.`empresa`, `acl`.`fecha`,3 as tiposer
                        FROM `asignacion_ser_cliente_a` `acl`
                        JOIN `personal` `per` ON `per`.`personalId`=`acl`.`tecnico`
                        LEFT JOIN `personal` `per2` ON `per2`.`personalId`=`acl`.`tecnico2`
                        LEFT JOIN `personal` `per3` ON `per3`.`personalId`=`acl`.`tecnico3`
                        LEFT JOIN `personal` `per4` ON `per4`.`personalId`=`acl`.`tecnico4`
                        JOIN `clientes` `cli` ON `cli`.`id`=`acl`.`clienteId`
                        JOIN `rutas` `zo` ON `zo`.`id`=`acl`.`zonaId`
                        JOIN `asignacion_ser_cliente_a_d` `acld` ON `acld`.`asignacionId`=`acl`.`asignacionId`
                        JOIN `polizas` `pol` ON `pol`.`id`=`acld`.`tpoliza`
                        LEFT JOIN `servicio_evento` `sereve` ON `sereve`.`id`=`acld`.`tservicio`
                        LEFT JOIN `polizas_detalles` `pold` ON `pold`.`id`=`acld`.`tservicio_a`
                        WHERE acld.status=2 and acl.fecha > '$fechaante' AND `acl`.`activo` = 1 AND `acld`.`activo` = '1' AND acl.clienteId='$idcli'
                        GROUP BY acl.asignacionId
                        ORDER BY acl.fecha DESC ";
            }
            if($tipo==4){
                $strq="SELECT sv.asignacionId,sv.fecha
                        FROM asignacion_ser_venta_a as sv
                        INNER JOIN ventas as v on v.id=sv.ventaId
                        WHERE sv.tipov=0 AND v.idCliente='$idcli' and sv.status=2 AND sv.fecha>'$fechaante'
                        UNION
                        SELECT sv.asignacionId,sv.fecha
                        FROM asignacion_ser_venta_a as sv
                        INNER JOIN ventacombinada as vc on vc.combinadaId=sv.ventaId
                        INNER JOIN ventas as ven on ven.id=vc.equipo
                        WHERE sv.tipov=1 AND ven.idCliente='$idcli' and sv.status=2 AND sv.fecha>'$fechaante'
                        ORDER BY fecha DESC";
            }
            
            $query = $this->db->query($strq);
            return $query;
        }
        function obtenerproducventavinculada($ser,$tipo){
            $strq="SELECT ve.cantidad,ve.modelo,sp.serie,bod.bodega FROM ventas as v 
                    INNER JOIN ventas_has_detallesEquipos as ve on ve.idVenta=v.id
                    inner join asignacion_series_equipos as ase on ase.id_equipo=ve.id
                    inner join series_productos as sp on sp.serieId=ase.serieId
                    left join bodegas as bod on bod.bodegaId=ve.serie_bodega
                    WHERE v.v_ser_tipo='$tipo' AND v.v_ser_id='$ser' AND v.prefactura=1

                    UNION

                    SELECT ve.cantidad,cata.nombre as modelo,sea.serie,bod.bodega FROM ventas as v 
                    INNER JOIN ventas_has_detallesEquipos_accesorios as ve on ve.id_venta=v.id
                    inner join catalogo_accesorios as cata on cata.id=ve.id_accesorio
                    left JOIN asignacion_series_accesorios as asa on asa.id_accesorio=ve.id_accesoriod
                    left join series_accesorios as sea on sea.serieId=asa.serieId
                    left join bodegas as bod on bod.bodegaId=ve.serie_bodega
                    WHERE v.v_ser_tipo='$tipo' AND v.v_ser_id='$ser' AND v.prefactura=1

                    UNION

                    SELECT ve.piezas as cantidad,ve.modelo,'' as serie,bod.bodega FROM ventas as v 
                    INNER JOIN ventas_has_detallesConsumibles as ve on ve.idVentas=v.id
                    left join bodegas as bod on bod.bodegaId=ve.bodegas
                    WHERE v.v_ser_tipo='$tipo' AND v.v_ser_id='$ser' AND v.prefactura=1

                    UNION

                    SELECT ve.piezas as cantidad, ve.modelo,ser.serie,bod.bodega FROM ventas as v 
                    INNER JOIN ventas_has_detallesRefacciones as ve on ve.idVentas=v.id
                    LEFT JOIN asignacion_series_refacciones as assr on assr.id_refaccion=ve.id
                    LEFT JOIN series_refacciones as ser on ser.serieId=assr.serieId
                    left join bodegas as bod on bod.bodegaId=ve.serie_bodega
                    WHERE v.v_ser_tipo='$tipo' AND v.v_ser_id='$ser' AND v.prefactura=1";
                    //log_message('error', 'view sql obtenerproducventavinculada: '.$strq);
            $query = $this->db->query($strq);
            return $query;
        }
        function obtenerdatoscontactodir($cli){
            $strq="SELECT clidc.*,cdir.direccion
                    FROM cliente_datoscontacto as clidc
                    LEFT JOIN clientes_direccion as cdir on cdir.idclientedirecc=clidc.dir
                    WHERE clidc.clienteId='$cli' AND clidc.activo=1";
            $query = $this->DB6->query($strq);
            return $query;
        }
        function obtenerfacturassinrelacion($cli,$fecha){
            $strq="SELECT fac.FacturasId,fac.Rfc,fac.Nombre,fac.serie,fac.Folio,fac.Estado,fac.fechatimbre,fac.subtotal, fac.iva,fac.total, facv.ventaId,facp.polizaId,facpr.prefacturaId,IFNULL(SUM(pag.pago),0) as pagom,IFNULL(SUM(fcomp.ImpPagado),0) as pagoc,cli.credito
                FROM f_facturas as fac
                INNER JOIN clientes as cli on cli.id=fac.Clientes_ClientesId
                LEFT JOIN factura_venta as facv on facv.facturaId=fac.FacturasId
                LEFT JOIN factura_poliza as facp on facp.facturaId=fac.FacturasId
                LEFT JOIN factura_prefactura as facpr on facpr.facturaId=fac.FacturasId
                LEFT JOIN pagos_factura as pag on pag.factura=fac.FacturasId
                LEFT JOIN f_complementopago_documento as fcomp ON fcomp.facturasId=fac.FacturasId AND fcomp.Estado=1
                WHERE  fac.Estado=1 AND facv.facId IS NULL AND facp.polizaId IS NULL AND facpr.prefacturaId IS NULL AND fac.Clientes_ClientesId='$cli' AND  fac.fechatimbre>'$fecha 00:00:00'
                GROUP BY fac.FacturasId";
            $query = $this->db->query($strq);
            return $query;
        }
        function obtenerproducciontotalcentrocostos($pre,$cce){
            $strq="SELECT rde.id,
                      rde.idEquipo,
                      rde.modelo,
                      sepro.serie,
                      sepro.usado,
                      sepro.serieId,
                      arpd.produccion_total,                      
                      arpd.produccion,
                      arpd.tipo,
                      SUM(arpd.produccion_total) as produccion_total_sum
                FROM `alta_rentas_prefactura_d` as arpd 
                inner JOIN rentas_has_detallesEquipos as rde on rde.id =arpd.productoId
                inner JOIN series_productos as sepro on sepro.serieId=arpd.serieId
                INNER JOIN centro_costos_equipos as cce on cce.serieid=arpd.serieId AND cce.rowequipo=arpd.productoId
                WHERE arpd.prefId='$pre' and cce.idcentroc='$cce' /*AND arpd.tipo=1 */";
            $query = $this->db->query($strq);
            $pro_total=0;
            foreach ($query->result() as $item) {
                $pro_total=$item->produccion_total_sum;
            }
            return $pro_total;
        }
        function getpresrfclike($search){
            $strq="SELECT clif.id,clif.razon_social,clif.rfc FROM prefactura  as pre
                    INNER JOIN cliente_has_datos_fiscales as clif on clif.id=pre.rfc_id
                    WHERE (clif.rfc LIKE '%$search%' or clif.razon_social='%$search%')
                    GROUP BY clif.rfc";
            $query = $this->db->query($strq);
            return $query;
        }
        function viewfacturasstatusbtn($prefactura,$contrato,$view){
              $alta_rentas = $this->db3_getselectwheren('alta_rentas',array('idcontrato'=>$contrato));
              $altarentas=$alta_rentas->row();
              $status_btn=0;
              $total=0;
              $prefacturaresul = $this->db14_getselectwheren('alta_rentas_prefactura',array('prefId'=>$prefactura));
              $factura_renta =0;
              $factura_excedente =0;
              $statusfactura=0;
              $tipo2=$altarentas->tipo2;
              $btn1='';
              $descartar_not=0;
              $factura_excedente_pendiente=0;
              $fac_exc_pen=0;
              foreach ($prefacturaresul->result() as $item) {
                $factura_renta = $item->factura_renta;
                $factura_excedente = $item->factura_excedente;
                $total = ($item->total+round(($item->total*0.16),2));
                $descartar_not=$item->descartar_not;
                $factura_excedente_pendiente=$item->factura_excedente_pendiente;

              }
              if($factura_excedente_pendiente==1){
                $factura_excedente=0;
                $fac_exc_pen=1;
              }
              if ($factura_renta==1 or $factura_excedente==1) {
                $totalf =$factura_renta+$factura_excedente;
                if ($totalf==2) {
                  $statusfactura=2;
                  $btn1='<!--statusfactura:'.$statusfactura.'-->';
                    $prefacturadx = $this->db->query("SELECT * FROM alta_rentas_prefactura_d WHERE prefId = $prefactura AND (statusfacturar=0 or statusfacturae=0)");
                    foreach ($prefacturadx->result() as $item) {
                      $statusfactura=1;
                    }
                    $btn1.='<!--statusfactura:'.$statusfactura.'-->';
                }elseif ($totalf==1) {
                  $statusfactura=1;
                }else{
                  $statusfactura=0;
                }

                


              }else{
                $prefacturad = $this->ModeloCatalogos->db14_getselectwheren('alta_rentas_prefactura_d',array('prefId'=>$prefactura));
                $rowpred=0;
                $statusfacturar=0;
                $statusfacturae=0;
                foreach ($prefacturad->result() as $item) {
                  $statusfacturar=$statusfacturar+$item->statusfacturar;
                  $statusfacturae=$statusfacturae+$item->statusfacturae;
                  if($factura_excedente_pendiente==1){
                    $statusfacturae=0;
                    $fac_exc_pen=1;
                  }
                  $rowpred++;
                  //=====================================
                  //funcion a solicitud de guadalipe para las cuestion de que no se cuente la parte de color si en dado caso este no se cobra
                    if($item->tipo==2){
                      if($tipo2==1){//individual
                        $rs_are=$this->ModeloCatalogos->db14_getselectwheren('alta_rentas_equipos',array('id_equipo'=>$item->productoId,'serieId'=>$item->serieId,'tipo'=>2));
                        $rs_are_result=$rs_are->row();
                        if($rs_are_result->rentac>0 or $rs_are_result->precio_c_e_color>0){

                        }else{
                          $rowpred--;
                        }
                      }
                    }
                  //====================================
                }
                $btn1='<!--$statusfacturar: '.$statusfacturar.'-->';
                $btn1.='<!--$statusfacturae: '.$statusfacturae.'-->';
                //log_message('error', 'prefactura: '.$prefactura.'---'.$statusfacturar.'+'.$statusfacturae);
                $totalfrow=$rowpred*2;
                $totalf=$statusfacturar+$statusfacturae;
                if($totalf>0){
                  if($totalf==$totalfrow){
                    $statusfactura=2;
                  }else{
                    $statusfactura=1;
                  }
                }else{
                  $statusfactura=0;
                }
              }
              $statusfacturaff=1;
              $prefacturaresulff = $this->db14_getselectwheren('factura_prefactura',array('prefacturaId'=>$prefactura));
              $facturaId=0;
              foreach ($prefacturaresulff->result()  as $itempf) {
                  $facturaId=$itempf->facturaId;
                  $prefacturaresulfff = $this->db14_getselectwheren('f_facturas',array('FacturasId'=>$itempf->facturaId));
                  foreach ($prefacturaresulfff->result() as $itempff) {
                    if($itempff->Estado==0){
                      $statusfacturaff=0;
                    }
                  }
              }
              $btn_addfactura='<a class="b-btn b-btn-primary tooltipped" 
                        data-position="top" 
                        data-delay="50" 
                        data-tooltip="Agregar Factura" onclick="addfactura('.$prefactura.','.$total.')">
                                <i class="fas fa-folder-plus"></i></a> ';
              if ($statusfactura==2) {
                $btn='<button class="btn-bmz waves-effect waves-light" type="button" style=" background-color: #56ea36;">Facturada</button>';
                $status_btn=2;
                $btn.=$btn_addfactura;
              }elseif ($statusfactura==1) {
                if($fac_exc_pen==1){
                  $btn_parcial_v='Parcial (Excedentes pendientes)';
                }else{
                  $btn_parcial_v='Parcial';
                }
                $status_btn=1;
                $btn='<button class="btn-bmz waves-effect waves-light btn_parcial_v" onclick_btn_parcial_v type="button" style=" background-color: #f9ad5d;" onclick="Fact('.$prefactura.','.$contrato.')" >'.$btn_parcial_v.'</button>';
                $btn.=$btn_addfactura;
              }else{
                $btn='<button class="btn-bmz waves-effect waves-light" type="button" style=" background-color: #e34740;" onclick="Fact('.$prefactura.','.$contrato.')" >Facturar</button>';
                $status_btn=0;
                $btn.=$btn_addfactura;
                if($descartar_not==1){
                  $desnot=' style="color:red" ';
                  $desnot_title='Periodo Cerrado';
                }else{
                  $desnot='';
                  $desnot_title='Cerrar periodo';
                }
                $btn.='<button class="b-btn b-btn-primary tooltipped" type="button" style=" title="'.$desnot_title.'" data-position="top" 
                        data-delay="50" 
                        data-tooltip="'.$desnot_title.'" onclick="cerrarperiodo('.$prefactura.')" ><i class="far fa-window-close" '.$desnot.'></i></button> ';
              }
              if ($statusfactura==2 or $statusfactura==1) {
                $btn.='<a class="waves-effect btn-bmz blue" onclick="refacturarperiodo('.$prefactura.','.$contrato.','.$facturaId.')" title="Refacturar para cancelacion"><i class="fas fa-file-prescription"></i></a>';
              }
              if($statusfacturaff==0){
                $btn.=$btn_addfactura;
              }
              if($view==0){
                $return_btn=$btn.$btn1;
              }else{
                $return_btn=$status_btn;
              }
              return $return_btn;
        }
        function ver_not_dev_series($idcontrato,$serieId){
            $strq="SELECT * 
                    FROM asignacion_ser_contrato_a as asig
                    INNER JOIN asignacion_ser_contrato_a_e as asigd on asigd.asignacionId=asig.asignacionId
                    WHERE asig.contratoId='$idcontrato' AND
                    asig.activo=1 AND
                    asigd.serieId='$serieId' AND
                    asigd.not_dev=1 AND
                    asigd.fecha>DATE_SUB(NOW(),INTERVAL '1' MONTH)";
            $query = $this->db->query($strq);
            return $query; 
        }
        function info_get_serie_cotizacion($serie,$cli){
            $strq="SELECT cot.id,cot.reg,cot.idCliente
                    FROM cotizaciones_has_detallesPoliza as cotdp
                    INNER JOIN cotizaciones as cot on cot.id=cotdp.idCotizacion
                    WHERE cotdp.serie like '%$serie%' AND cot.estatus=1 and cot.idCliente='$cli' and cot.tipocot like '%2%' and (cotdp.precio_local>0 or cotdp.precio_semi>0 or cotdp.precio_foraneo>0 or cotdp.precio_especial>0)
                    UNION 
                    SELECT cot.id,cot.reg,cot.idCliente
                    FROM cotizaciones_has_detallesRefacciones as cotdp
                    INNER JOIN cotizaciones as cot on cot.id=cotdp.idCotizacion
                    WHERE cotdp.serieequipo like '%$serie%'  AND cot.estatus=1 and cot.idCliente='$cli' and cot.tipocot like '%2%' and cotdp.precioGeneral>0
                    group by id
                    ORDER BY `reg` DESC
                    LIMIT 3";
            $query = $this->db->query($strq);
            return $query; 
        }
        function info_get_serie_servicios($serieId,$serie,$cli){
            $strq="SELECT * FROM(
                        SELECT asig.asignacionId,1 as tipo
                        FROM asignacion_ser_contrato_a_e as asig
                        WHERE asig.activo=1 and asig.serieId='$serieId' AND asig.confirmado=1 AND asig.status<2 and asig.nr=0 and asig.fecha>DATE_SUB(NOW(),INTERVAL '1' MONTH)
                        
                        union
                        
                        SELECT asig.asignacionId, 3 as tipo
                        FROM asignacion_ser_cliente_a_d as asigd
                        INNER JOIN asignacion_ser_cliente_a as asig on asig.asignacionId=asigd.asignacionId
                        WHERE asigd.activo=1 and asigd.serie='$serie' AND asigd.status<2 AND asig.confirmado=1 AND asig.fecha>DATE_SUB(NOW(),INTERVAL '1' MONTH)
                    ) as datoss

                    ";
            $query = $this->db->query($strq);
            return $query; 
        }
        function obtener_info_productos($empresa,$combinada,$idventa,$idpoliza){
            $html='';
            $equipo=0;
            $consumibles=0;
            $refacciones=0;
            if($idventa>0){
                $equipo=$idventa;
                $consumibles=$idventa;
                $refacciones=$idventa;
                if($combinada==1){
                    $resultvc=$this->getselectwheren('ventacombinada',array('equipo'=>$idventa));
                    foreach ($resultvc->result() as $itemvc) {
                        $equipo=$itemvc->equipo;
                        $consumibles=$itemvc->consumibles;
                        $refacciones=$itemvc->refacciones;
                    }
                }
            }
            if($idpoliza>0){
                if($combinada==1){
                    $resultvc=$this->getselectwheren('ventacombinada',array('poliza'=>$idpoliza));
                    foreach ($resultvc->result() as $itemvc) {
                        $equipo=$itemvc->equipo;
                        $consumibles=$itemvc->consumibles;
                        $refacciones=$itemvc->refacciones;
                    }
                }

            }
            $resultadoequipos = $this->db6_getselectwheren('ventas_has_detallesEquipos',array('idVenta'=>$equipo));
            $resultadoconsumibles = $this->consumiblesventa($equipo);
            $resultadoaccesorios = $this->ModeloCatalogos->accesoriosventa($equipo);
            $consumiblesventadetalles = $this->consumiblesventadetalles($consumibles);
            $ventadetallesrefacion = $this->ventas_has_detallesRefacciones($refacciones);
            $total_rows=$resultadoequipos->num_rows()+$resultadoconsumibles->num_rows()+$resultadoaccesorios->num_rows()+$consumiblesventadetalles->num_rows()+$ventadetallesrefacion->num_rows();
            if($total_rows>0){
                $html.='<tr><th colspan="5" style="text-align: center;">'.$empresa.'</th></tr>';
            }
            foreach ($resultadoequipos->result() as $item_p_e) {
                $html.='<tr>';
                    $html.='<td><!--1-->'.$item_p_e->cantidad.'</td>';
                    $html.='<td>'.$item_p_e->modelo.'</td>';
                    $html.='<td>'.$this->ModeloCatalogos->obtenernumeroserieequipo($item_p_e->idEquipo).'</td>';
                    $resuleserie=$this->getequiposerie($item_p_e->id);
                    $series_selecte='';
                    foreach ($resuleserie->result() as $itemse) {
                        $series_selecte.=$itemse->serie.', ';
                    }
                    $html.='<td>'.$series_selecte.'</td>';
                    $bodega=$this->ModeloCatalogos->obtenerbodega($item_p_e->serie_bodega);
                    $html.='<td>'.$bodega.'</td>';
                $html.='</tr>';
            }
            foreach ($resultadoconsumibles->result() as $item_p_ec) {
                $html.='<tr>';
                    $html.='<td><!--2-->'.$item_p_ec->cantidad.'</td>';
                    $html.='<td>'.$item_p_ec->modelo.'</td>';
                    $html.='<td>'.$item_p_ec->parte.'</td>';
                    $html.='<td>'.$item_p_ec->bodega.'</td>';
                    $html.='<td><td>';
                $html.='</tr>';
            }
            foreach ($consumiblesventadetalles->result() as $item_p_c) {
                $html.='<tr>';
                    $html.='<td><!--3-->'.$item_p_c->cantidad.'</td>';
                    $html.='<td>'.$item_p_c->modelo.'</td>';
                    $html.='<td>'.$item_p_c->parte.'</td>';
                    $html.='<td>'.$item_p_c->foliotext.'</td>';
                    $html.='<td>'.$item_p_c->bodega.'</td>';
                $html.='</tr>';
            }
            foreach ($ventadetallesrefacion->result() as $item_p_r) {
                $html.='<tr>';
                    $html.='<td><!--4-->'.$item_p_r->cantidad.'</td>';
                    $html.='<td>'.$item_p_r->modelo.'</td>';
                    $html.='<td>'.$item_p_r->codigo.'</td>';
                    $resuleserie=$this->getrefaccionserie($item_p_r->id);
                    $series_selecte='';
                    foreach ($resuleserie->result() as $itemse) { 
                        $series_selecte.=$itemse->serie.', ';
                    }
                    $html.='<td>'.$series_selecte.'</td>';
                    $html.='<td>'.$item_p_r->bodega.'</td>';
                $html.='</tr>';
            }
            foreach ($resultadoaccesorios->result() as $item_a) {
                $html.='<tr>';
                    $html.='<td><!--5-->'.$item_a->cantidad.'</td>';
                    $html.='<td>'.$item_a->nombre.'</td>';
                    $html.='<td>'.$item_a->no_parte.'</td>';
                    $resuleserie=$this->ModeloCatalogos->getaccesorioserie($item_a->id_accesoriod);

                    $series_selecte='';
                    foreach ($resuleserie->result() as $itemse) {
                        $series_selecte.=$itemse->serie.', ';
                    }
                    $html.='<td>'.$series_selecte.'</td>';
                    $html.='<td>'.$item_a->bodega.'</td>';
                $html.='</tr>';
            }

            //log_message('error',$html);
            return $html;
        }
        function updateuuid($id,$tipo){
            if($tipo==1){//equipos
                $strq="UPDATE equipos SET uuid = UUID() WHERE id = '$id'";
                $this->db->query($strq);
            }
            if($tipo==2){//refacciones
                $strq="UPDATE refacciones SET uuid = UUID() WHERE id = '$id'";
                $this->db->query($strq);
            }
            if($tipo==3){//consumibles
                $strq="UPDATE consumibles SET uuid = UUID() WHERE id = '$id'";
                $this->db->query($strq);
            }
            if($tipo==4){//accesorios
                $strq="UPDATE catalogo_accesorios SET uuid = UUID() WHERE id = '$id'";
                $this->db->query($strq);
            }
            if($tipo==5){//accesorios
                $strq="UPDATE clientes SET uuid = UUID() WHERE id = '$id'";
                $this->db->query($strq);
            }
            if($tipo==6){//contrato
                $strq="UPDATE contrato SET uuid = UUID() WHERE idcontrato = '$id'";
                $this->db->query($strq);
            }
        }
        function verificarpagosalperiodo($idcomplemento){
            $strq="SELECT compd.complementoId,compd.facturasId,fac.Estado,fac.total, facpre.contratoId,facpre.prefacturaId, facpre.facId
                FROM f_complementopago_documento as compd
                INNER JOIN f_facturas as fac on fac.FacturasId=compd.facturasId
                INNER JOIN factura_prefactura as facpre on facpre.facturaId=compd.facturasId
                WHERE compd.complementoId='$idcomplemento'";
            $query = $this->db->query($strq);
            foreach ($query->result() as $item_com) {
                    $facturasId = $item_com->facturasId;
                //===================preriodos===========================
                    $total_factura=$item_com->total;
                    $total_pagos=0;
                    $result = $this->pagos_factura($item_com->facId); 
                    foreach ($result as $item) {
                        $total_pagos=$total_pagos+$item->pago;
                    }
                    $resultf = $this->facturasdelperiodo($item_com->facId); 
                    foreach ($resultf->result() as $itemf) {
                        $resultvcp_cp = $this->obteciondecomplememtofactura($itemf->facturaId);
                        foreach ($resultvcp_cp->result() as $itemcp) {
                            $total_pagos=$total_pagos+$itemcp->ImpPagado;
                        }
                    }
                    if($total_pagos>0){
                        if($total_pagos>=$total_factura){
                            $this->updateCatalogo('factura_prefactura',array('statuspago'=>1),array('facId'=>$item_com->facId));
                        }else{
                            $this->updateCatalogo('factura_prefactura',array('statuspago'=>0),array('facId'=>$item_com->facId));
                        }
                    }else{
                        $this->updateCatalogo('factura_prefactura',array('statuspago'=>0),array('facId'=>$item_com->facId));
                    }
                //================================================
                //===================Ventas===========================
                    $result=$this->getselectwheren('factura_venta',array('facturaId'=>$facturasId));
                    foreach ($result->result() as $item_vent) {
                        $idventa_nc=$item_vent->ventaId;
                        if($item_vent->combinada==0){//normal
                            $this->updateCatalogo('ventas',array('estatus'=>1),array('id'=>$idventa_nc));
                        }else{//combinada
                            $this->updateCatalogo('ventacombinada',array('estatus'=>1),array('combinadaId'=>$idventa_nc));
                        }
                    }
                //===============================================
                //===================polizas===========================
                    $result=$this->getselectwheren('factura_poliza',array('facturaId'=>$facturasId));
                    foreach ($result->result() as $item_vent) {
                        $polizaId=$item_vent->polizaId;
                        $this->updateCatalogo('polizasCreadas',array('estatus'=>1),array('id'=>$polizaId));
                    }
                //==============================
            }
        }
        function optencion_servicios_clinte($idcliente,$limit){
            $strq="";
            $strq.="(";$this->db->join('personal per','per.personalId=ace.tecnico','left');
                        $this->db->join('personal per2','per2.personalId=ace.tecnico2','left');
                    $strq.="SELECT 
                                    `ac`.`asignacionId`,
                                    `ac`.`prioridad`,`ac`.`prioridad2`,
                                    `cli`.`empresa`,
                                    `ace`.`fecha`,
                                    `ace`.`hora`,`ace`.`horafin`,
                                    `ace`.`status`,
                                    group_concat(`ace`.`status` separator ',')  as g_status,
                                    ace.hora_comida,ace.horafin_comida,
                                    ace.firma,
                                    1 as tipo,
                                    ren.idCliente,
                                    group_concat(ace.nr separator ',')  as g_nr,
                                    pol.nombre as servicio,
                                    GROUP_CONCAT(DISTINCT per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno SEPARATOR ', ') as tecnico,
                                    GROUP_CONCAT(DISTINCT per2.nombre,' ',per2.apellido_paterno,' ',per2.apellido_materno SEPARATOR ', ') as tecnico2
                                    FROM `asignacion_ser_contrato_a_e` `ace`
                                    JOIN `asignacion_ser_contrato_a` `ac` ON `ac`.`asignacionId`=`ace`.`asignacionId`
                                    JOIN `personal` `per` ON `per`.`personalId`=`ace`.`tecnico`
                                    JOIN `contrato` `con` ON `con`.`idcontrato`=`ac`.`contratoId`
                                    JOIN `rentas` `ren` ON `ren`.`id`=`con`.`idRenta`
                                    JOIN `clientes` `cli` ON `cli`.`id`=`ren`.`idCliente`
                                    JOIN `rutas` `ru` ON `ru`.`id`=`ac`.`zonaId`
                                    JOIN `rentas_has_detallesEquipos` `rde` ON `rde`.`id`=`ace`.`idequipo`
                                    JOIN `series_productos` `serp` ON `serp`.`serieId`=`ace`.`serieId`
                                    JOIN polizas as pol on pol.id=ac.tpoliza
                                    left JOIN `servicio_evento` `sereve` ON `sereve`.`id`=`ac`.`tservicio` 
                                    left JOIN personal per2 ON per.personalId=ace.tecnico2 
                                    where ace.nr=0 and  ace.activo=1 and ace.confirmado=1 and ace.status<3 and YEAR(ace.fecha) = YEAR(CURDATE()) AND cli.id='$idcliente'
                                    GROUP by `ac`.`asignacionId`,`cli`.`empresa`,`ace`.`fecha`";
                $strq.=")
                UNION
                (";
                    $strq.="SELECT 
                                    `ap`.`asignacionId`,
                                    `ap`.`prioridad`,`ap`.`prioridad2`,
                                    `cli`.`empresa`,
                                    `ape`.`fecha`,
                                    `ape`.`hora`,`ape`.`horafin`,
                                    `ape`.`status`,
                                    group_concat(`ape`.`status` separator ',') as g_status,
                                    ape.hora_comida,ape.horafin_comida,
                                    ape.firma,
                                    2 as tipo,
                                    poc.idCliente,
                                    group_concat(ape.nr separator ',')  as g_nr,
                                    pocde.nombre as servicio,
                                    GROUP_CONCAT(DISTINCT per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno SEPARATOR ', ') as tecnico,
                                    GROUP_CONCAT(DISTINCT per2.nombre,' ',per2.apellido_paterno,' ',per2.apellido_materno SEPARATOR ', ') as tecnico2
                                    FROM `asignacion_ser_poliza_a_e` `ape`
                                    JOIN `asignacion_ser_poliza_a` `ap` ON `ap`.`asignacionId`=`ape`.`asignacionId`
                                    JOIN `personal` `per` ON `per`.`personalId`=`ape`.`tecnico`
                                    JOIN `polizasCreadas` `poc` ON `poc`.`id`=`ap`.`polizaId`
                                    JOIN `clientes` `cli` ON `cli`.`id`=`poc`.`idCliente`
                                    JOIN `rutas` `ru` ON `ru`.`id`=`ap`.`zonaId`
                                    JOIN `polizasCreadas_has_detallesPoliza` `pocde` ON `pocde`.`id`=`ape`.`idequipo`
                                    JOIN `equipos` `equ` ON `equ`.`id`=`pocde`.`idEquipo`
                                    left JOIN personal per2 ON per.personalId=ape.tecnico2 
                                    /*JOIN `servicio_evento` `sereve` ON `sereve`.`id`=`ap`.`tservicio`*/
                                    where ape.nr=0 and ape.activo=1 and ape.status<3 and cli.id='$idcliente' AND YEAR(ape.fecha) = YEAR(CURDATE())
                                    GROUP by `ap`.`asignacionId`,`cli`.`empresa`,`ape`.`hora`,`ape`.`horafin`";
                $strq.=")
                UNION (";
                    $strq.="SELECT 
                                    `acl`.`asignacionId`, 
                                    `acl`.`prioridad`,`acl`.`prioridad2`,
                                    `cli`.`empresa`,
                                    `acl`.`fecha`,`acl`.`hora`,
                                    `acl`.`horafin`,
                                    acl.status,
                                    group_concat(acld.status separator ',') as g_status,
                                    acl.hora_comida,acl.horafin_comida,
                                    acl.firma,
                                    3 as tipo,
                                    acl.clienteId as idCliente,
                                    group_concat(acld.nr separator ',')  as g_nr,
                                    pol.nombre as servicio,
                                    GROUP_CONCAT(DISTINCT per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno SEPARATOR ', ') as tecnico,
                                    GROUP_CONCAT(DISTINCT per2.nombre,' ',per2.apellido_paterno,' ',per2.apellido_materno SEPARATOR ', ') as tecnico2
                                    FROM `asignacion_ser_cliente_a` `acl`
                                    JOIN `personal` `per` ON `per`.`personalId`=`acl`.`tecnico`
                                    JOIN `clientes` `cli` ON `cli`.`id`=`acl`.`clienteId`
                                    JOIN `rutas` `zo` ON `zo`.`id`=`acl`.`zonaId`
                                    JOIN `asignacion_ser_cliente_a_d` `acld` ON `acld`.`asignacionId` = `acl`.`asignacionId`
                                    left JOIN `servicio_evento` `sereve` ON `sereve`.`id` = `acld`.`tservicio`
                                    inner JOIN polizas as pol on pol.id=acld.tpoliza
                                    left JOIN personal per2 ON per.personalId=acl.tecnico2 
                                    where acld.nr=0 and acld.activo=1 and acld.status<3  and cli.id='$idcliente' AND YEAR(acl.fecha) = YEAR(CURDATE())
                                    GROUP by acl.asignacionId,cli.empresa,acl.hora,acl.horafin,acl.status";
                $strq.=")
                UNION(";
                    $strq.="SELECT 
                                    acl.asignacionId, 
                                    acl.prioridad,acl.prioridad2,
                                    cli.empresa,
                                    acl.fecha,
                                    acl.hora,acl.horafin,
                                    acl.status,
                                    group_concat(acl.status separator ',') as g_status,
                                    acl.hora_comida,acl.horafin_comida,
                                    acl.firma,
                                    4 as tipo,
                                    acl.clienteId as idCliente,
                                    group_concat(acl.nr separator ',')  as g_nr,
                                    pol.nombre as servicio,
                                    GROUP_CONCAT(DISTINCT per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno SEPARATOR ', ') as tecnico,
                                    GROUP_CONCAT(DISTINCT per2.nombre,' ',per2.apellido_paterno,' ',per2.apellido_materno SEPARATOR ', ') as tecnico2
                                    FROM `asignacion_ser_venta_a` `acl`
                                    JOIN `personal` `per` ON `per`.`personalId`=`acl`.`tecnico`
                                    JOIN `clientes` `cli` ON `cli`.`id`=`acl`.`clienteId`
                                    JOIN `rutas` `zo` ON `zo`.`id`=`acl`.`zonaId`
                                    JOIN `asignacion_ser_venta_a_d` `acld` ON `acld`.`asignacionId` = `acl`.`asignacionId`
                                    left JOIN `servicio_evento` `sereve` ON `sereve`.`id` = `acld`.`tservicio`
                                    left JOIN polizas as pol on pol.id=acld.tpoliza
                                    left JOIN personal per2 ON per.personalId=acl.tecnico2 
                                    where  acl.nr=0  and  acl.activo=1 and acl.status<3  and cli.id='$idcliente' AND YEAR(acl.fecha) = YEAR(CURDATE())
                                    GROUP by acl.asignacionId,cli.empresa,acl.fecha,acl.hora,acl.horafin,acl.status";
                $strq.=") ORDER BY `fecha` DESC ";
                if($limit>0){
                    $strq.=" LIMIT 3 ";
                }
            $query = $this->db->query($strq);
            return $query; 
        }
        function factura_requerimientos2($idfactura,$tipo,$mon,$col,$equipos,$htmlarray){//$htmlarray 0 html 1 array
                $where = array('prefId' => $idfactura);
              $result = $this->getselectwherenall('alta_rentas_prefactura',$where);
              $id_renta=0;
              $rowcobrar=0;$array_conceptos=array();
              foreach ($result as $item) {
                 $fechai = date("d/m/Y", strtotime($item->periodo_inicial));
                 $fechaf = date("d/m/Y", strtotime($item->periodo_final));

                 $fechaie = date("d/m/Y", strtotime($item->periodo_inicial."- 1 month"));
                 $fechafe = date("d/m/Y", strtotime($item->periodo_final."- 1 month"));
                 if (date('d',strtotime($item->periodo_inicial))==1) {
                    //log_message('error', 'entra al primero del mes');
                    //log_message('error', 'entra al primero del mes'.$item->periodo_inicial);
                    $fechaie1 = date("Y-m-d", strtotime($item->periodo_inicial."- 1 month"));
                    $fechaie = date("d/m/Y", strtotime($fechaie1));
                    $fecha2 = new DateTime($fechaie1);
                    $fecha2->modify('last day of this month');
                    $fechafe = $fecha2->format('d/m/Y');
                    //log_message('error', 'entra al primero del mes'.$fechafe);
                 }

                 $totals = $item->subtotal;
                 $totalcolor = $item->subtotalcolor;
                 $excedente = $item->excedente;
                 $excedentec = $item->excedentecolor; 
                 $id_renta = $item->id_renta;
                 $cant_ext_mono=$item->cant_ext_mono;
                 $prec_ext_mono=$item->prec_ext_mono;
                 $cant_ext_color=$item->cant_ext_color;
                 $prec_ext_color=$item->prec_ext_color;  
              }
              $resultrentas = $this->getselectwherenall('alta_rentas',array('id_renta'=>$id_renta));
                $tipoindiglo=0;
              foreach ($resultrentas as $itemr) {
                $tipoindiglo=$itemr->tipo2;//1 individual 2 global
                $descrip_fac=$itemr->descrip_fac.' ';
                if ($itemr->tipo1==2) {
                  $fechaie=$fechai;
                  $fechafe=$fechaf;
                }
              }
              //=============================================
                $option_unidad_renta = $this->obtenerunidadservicio(1,1);
                $option_concepto_renta = $this->obtenerunidadservicio(1,2);

                $option_unidad_ve_value = $this->ModeloCatalogos->obtenerunidadserviciovt(1,1,1);
                $option_unidad_ve_text = $this->ModeloCatalogos->obtenerunidadserviciovt(1,1,2);
                $option_concepto_ve_value = $this->ModeloCatalogos->obtenerunidadserviciovt(1,2,1);
                $option_concepto_ve_text = $this->ModeloCatalogos->obtenerunidadserviciovt(1,2,2);
              //=============================================


              $html = '';
              if ($tipoindiglo==2) {
                //==================================================================================
                  $prefacturadx = $this->db->query("SELECT tipo,statusfacturar,statusfacturae FROM alta_rentas_prefactura_d WHERE prefId = $idfactura GROUP BY tipo,statusfacturar,statusfacturae");
                    foreach ($prefacturadx->result() as $item) {
                      //log_message('error', 'tipo: '.$item->tipo.' statusfacturar:'.$item->statusfacturar.' statusfacturae:'.$item->statusfacturae);
                      if ($item->tipo==1) {
                        if ($item->statusfacturar==1) {
                          $totals=0;
                          //log_message('error', 'totals');
                        }
                        if ($item->statusfacturae==1) {
                          $excedente=0;
                          //log_message('error', 'excedente');
                        }
                      }
                      if ($item->tipo==2){
                        if ($item->statusfacturar==1) {
                          $totalcolor=0;
                          //log_message('error', 'totalcolor');
                        }
                        if ($item->statusfacturae==1) {
                          $excedentec=0;
                          //log_message('error', 'excedentec');
                        }
                      }



                        
                    }
                //==================================================================================
                if ($mon==0) {
                  $totals=0;
                  $excedente=0;
                }
                if ($col==0) {
                  $totalcolor=0;
                  $excedentec=0;
                }
                if($tipo == 1){// rentas
                  if ($totals>0) {
                        $desc_fr=$descrip_fac.'Renta Monocromaticas del '.$fechai.' al '.$fechaf;
                     $html.='<tr class="datos_principal tr_1">
                              <td>
                                <input id="selectedequipo" name="selectedequipo" type="hidden" value="0" readonly>
                                <input id="selectedprefactura" name="selectedprefactura" type="hidden" value="'.$idfactura.'" readonly>
                                <input id="facturarenta" name="facturarenta" type="hidden" value="1" readonly>
                                <input id="facturaexcedente" name="facturaexcedente" type="hidden" value="0" readonly>
                                <input id="cantidad" name="cantidad" type="text" value="1" readonly></td>
                              <td>
                                  <div style="width:250px">
                                    <select id="unidadsat" class="unidadsat browser-default">
                                    '.$option_unidad_renta.'
                                    </select>
                                  <div>
                              </td>
                              <td>
                                <div style="width:250px">
                                  <select id="conseptosat" class="conseptosat browser-default">
                                  '.$option_concepto_renta.'
                                  </select>
                                <div>
                              </td>
                              <td><input id="descripcion" name="descripcion" type="text" value="'.$desc_fr.'"></td>
                              <td><input id="precio" name="precio" class="precio" type="text" value="'.$totals.'" readonly></td>
                              <td>
                                  <input type="number"
                                    name="descuento"
                                    id="descuento"
                                    class="form-control-bmz cdescuento cdescuento_'.$rowcobrar.'"
                                    onclick="activardescuento('.$rowcobrar.')"
                                    value="0" readonly>
                              </td>
                              <td><a class="btn-bmz red deletetrrow"><i class="fas fa-trash-alt"></i></a></td>
                            </tr>';
                            $ivass=round($totals*0.16, 4);
                            $array_conceptos[]=array('Unidad'=>$option_unidad_ve_value,'servicioId'=>$option_concepto_ve_value,'Descripcion'=>$option_concepto_ve_text,'Descripcion2'=>$desc_fr,'Cu'=>$totals,'Importe'=>$totals,'iva'=>$ivass);
                            $rowcobrar++;
                  }
                  if ($totalcolor>0) {
                    $desc_fr=$descrip_fac.'Renta color del '.$fechai.' al '.$fechaf;
                     $html.='<tr class="datos_principal tr_2">
                              <td>
                                <input id="selectedequipo" name="selectedequipo" type="hidden" value="0" readonly>
                                <input id="selectedprefactura" name="selectedprefactura" type="hidden" value="'.$idfactura.'" readonly>
                                <input id="facturarenta" name="facturarenta" type="hidden" value="1" readonly>
                                <input id="facturaexcedente" name="facturaexcedente" type="hidden" value="0" readonly>
                                <input id="cantidad" name="cantidad" type="text" value="1" readonly></td>
                              <td>
                                  <div style="width:250px">
                                    <select id="unidadsat" class="unidadsat browser-default">
                                    '.$option_unidad_renta.'
                                    </select>
                                  <div>
                              </td>
                              <td>
                                <div style="width:250px">
                                  <select id="conseptosat" class="conseptosat browser-default">
                                  '.$option_concepto_renta.'
                                  </select>
                                <div>
                              </td>
                              <td><input id="descripcion" name="descripcion" type="text" value="'.$desc_fr.'"></td>
                              <td><input id="precio" name="precio" class="precio" type="text" value="'.$totalcolor.'" readonly></td>
                              <td>
                                  <input type="number"
                                    name="descuento"
                                    id="descuento"
                                    class="form-control-bmz cdescuento cdescuento_'.$rowcobrar.'"
                                    onclick="activardescuento('.$rowcobrar.')"
                                    value="0" readonly>
                              </td>
                              <td><a class="btn-bmz red deletetrrow"><i class="fas fa-trash-alt"></i></a></td>
                            </tr>';
                            $ivass=round($totalcolor*0.16, 4);
                            $array_conceptos[]=array('Unidad'=>$option_unidad_ve_value,'servicioId'=>$option_concepto_ve_value,'Descripcion'=>$option_concepto_ve_text,'Descripcion2'=>$desc_fr,'Cu'=>$totalcolor,'Importe'=>$totalcolor,'iva'=>$ivass);
                            $rowcobrar++;
                  } 
                }else if($tipo == 2){ // rentas y excedentes
                  // rentas
                    if ($totals>0) {
                        $desc_fr=$descrip_fac.'Renta Monocromaticas del '.$fechai.' al '.$fechaf;
                       $html.='<tr class="datos_principal tr_3">
                                <td>
                                  <input id="selectedequipo" name="selectedequipo" type="hidden" value="0" readonly>
                                  <input id="selectedprefactura" name="selectedprefactura" type="hidden" value="'.$idfactura.'" readonly>
                                  <input id="facturarenta" name="facturarenta" type="hidden" value="1" readonly>
                                  <input id="facturaexcedente" name="facturaexcedente" type="hidden" value="1" readonly>
                                  <input id="cantidad" name="cantidad" type="text" value="1" readonly></td>
                                <td>
                                    <div style="width:250px">
                                      <select id="unidadsat" class="unidadsat browser-default">
                                      '.$option_unidad_renta.'
                                      </select>
                                    <div>
                                </td>
                                <td>
                                  <div style="width:250px">
                                    <select id="conseptosat" class="conseptosat browser-default">
                                    '.$option_concepto_renta.'
                                    </select>
                                  <div>
                                </td>
                                <td><input id="descripcion" name="descripcion" type="text" value="'.$desc_fr.'"></td>
                                <td><input id="precio" name="precio" class="precio" type="text" value="'.$totals.'" readonly></td>
                                <td>
                                  <input type="number"
                                    name="descuento"
                                    id="descuento"
                                    class="form-control-bmz cdescuento cdescuento_'.$rowcobrar.'"
                                    onclick="activardescuento('.$rowcobrar.')"
                                    value="0" readonly>
                                </td>
                                <td><a class="btn-bmz red deletetrrow"><i class="fas fa-trash-alt"></i></a></td>
                              </tr>';

                            $ivass=round($totals*0.16, 4);
                            $array_conceptos[]=array('Unidad'=>$option_unidad_ve_value,'servicioId'=>$option_concepto_ve_value,'Descripcion'=>$option_concepto_ve_text,'Descripcion2'=>$desc_fr,'Cu'=>$totals,'Importe'=>$totals,'iva'=>$ivass);
                              $rowcobrar++;
                    }
                    if ($totalcolor>0) {
                        $desc_fr=$descrip_fac.'Renta color del '.$fechai.' al '.$fechaf;
                       $html.='<tr class="datos_principal tr_4">
                                <td>
                                  <input id="selectedequipo" name="selectedequipo" type="hidden" value="0" readonly>
                                  <input id="selectedprefactura" name="selectedprefactura" type="hidden" value="'.$idfactura.'" readonly>
                                  <input id="facturarenta" name="facturarenta" type="hidden" value="1" readonly>
                                <input id="facturaexcedente" name="facturaexcedente" type="hidden" value="1" readonly>
                                  <input id="cantidad" name="cantidad" type="text" value="1" readonly></td>
                                <td>
                                    <div style="width:250px">
                                      <select id="unidadsat" class="unidadsat browser-default">
                                      '.$option_unidad_renta.'
                                      </select>
                                    <div>
                                </td>
                                <td>
                                  <div style="width:250px">
                                    <select id="conseptosat" class="conseptosat browser-default">
                                    '.$option_concepto_renta.'
                                    </select>
                                  <div>
                                </td>
                                <td><input id="descripcion" name="descripcion" type="text" value="'.$desc_fr.'"></td>
                                <td><input id="precio" name="precio" class="precio" type="text" value="'.$totalcolor.'" readonly></td>
                                <td>
                                  <input type="number"
                                    name="descuento"
                                    id="descuento"
                                    class="form-control-bmz cdescuento cdescuento_'.$rowcobrar.'"
                                    onclick="activardescuento('.$rowcobrar.')"
                                    value="0" readonly>
                              </td>
                              <td><a class="btn-bmz red deletetrrow"><i class="fas fa-trash-alt"></i></a></td>
                              </tr>';

                            $ivass=round($totalcolor*0.16, 4);
                            $array_conceptos[]=array('Unidad'=>$option_unidad_ve_value,'servicioId'=>$option_concepto_ve_value,'Descripcion'=>$option_concepto_ve_text,'Descripcion2'=>$desc_fr,'Cu'=>$totalcolor,'Importe'=>$totalcolor,'iva'=>$ivass);
                              $rowcobrar++;
                    }

                    if($excedente > 0){ // excedentes monocromatico    
                        $desc_fr =$descrip_fac.''.number_format($cant_ext_mono,0,'.',',').' Clicks Excedente monocromatico del '.$fechaie.' al '.$fechafe;
                     $html.='<tr class="datos_principal tr_5">
                              <td>
                                <input id="selectedequipo" name="selectedequipo" type="hidden" value="0" readonly>
                                <input id="selectedprefactura" name="selectedprefactura" type="hidden" value="'.$idfactura.'" readonly>
                                <input id="facturarenta" name="facturarenta" type="hidden" value="1" readonly>
                                <input id="facturaexcedente" name="facturaexcedente" type="hidden" value="1" readonly>
                                <input id="cantidad" name="cantidad" type="text" value="1" class="excedente info cant_'.$rowcobrar.'" data-cantidad="'.$cant_ext_mono.'" data-precioexc="'.$prec_ext_mono.'" data-preid="'.$rowcobrar.'" readonly></td>
                              <td>
                                <div style="width:250px">
                                  <select id="unidadsat" class="unidadsat browser-default">
                                  '.$option_unidad_renta.'
                                  </select>
                                <div>
                              </td>
                              <td>
                                <div style="width:250px">
                                  <select id="conseptosat" class="conseptosat browser-default">
                                  '.$option_concepto_renta.'
                                  </select>
                                <div>
                              </td>
                              <td><input id="descripcion" name="descripcion" type="text" value="'.$desc_fr.'"></td>
                              <td><input id="precio" name="precio" class="precio excedente precio_'.$rowcobrar.'" type="text" value="'.$excedente.'" readonly></td>
                              <td>
                                  <input type="number"
                                    name="descuento"
                                    id="descuento"
                                    class="form-control-bmz cdescuento cdescuento_'.$rowcobrar.'"
                                    onclick="activardescuento('.$rowcobrar.')"
                                    value="0" readonly>
                              </td>
                              <td><a class="btn-bmz red deletetrrow"><i class="fas fa-trash-alt"></i></a></td>
                            </tr>';
                            $ivass=round($excedente*0.16, 4);
                            $array_conceptos[]=array('Unidad'=>$option_unidad_ve_value,'servicioId'=>$option_concepto_ve_value,'Descripcion'=>$option_concepto_ve_text,'Descripcion2'=>$desc_fr,'Cu'=>$excedente,'Importe'=>$excedente,'iva'=>$ivass);
                            $rowcobrar++;
                    }
                    if($excedentec > 0){// excedentes a color    
                        $desc_fr =$descrip_fac.''.number_format($cant_ext_color,0,'.',',').' Clicks Excedente color del '.$fechaie.' al '.$fechafe;
                     $html.='<tr class="datos_principal tr_6">
                              <td>
                                <input id="selectedequipo" name="selectedequipo" type="hidden" value="0" readonly>
                                <input id="selectedprefactura" name="selectedprefactura" type="hidden" value="'.$idfactura.'" readonly>
                                <input id="facturarenta" name="facturarenta" type="hidden" value="1" readonly>
                                <input id="facturaexcedente" name="facturaexcedente" type="hidden" value="1" readonly>
                                <input id="cantidad" name="cantidad" type="text" value="1" class="excedente info cant_'.$rowcobrar.'" data-cantidad="'.$cant_ext_color.'" data-precioexc="'.$prec_ext_color.'" data-preid="'.$rowcobrar.'" readonly></td>
                              <td><div style="width:250px">
                                  <select id="unidadsat" class="unidadsat browser-default">
                                  '.$option_unidad_renta.'
                                  </select>
                                <div>
                              </td>
                              <td>
                                  <div style="width:250px">
                                    <select id="conseptosat" class="conseptosat browser-default">
                                    '.$option_concepto_renta.'
                                    </select>
                                  <div>
                              </td>
                              <td><input id="descripcion" name="descripcion" type="text" value="'.$desc_fr.'"></td>
                              <td><input id="precio" name="precio" class="precio excedente precio_'.$rowcobrar.'" type="text" value="'.$excedentec.'" readonly></td>
                              <td>
                                  <input type="number"
                                    name="descuento"
                                    id="descuento"
                                    class="form-control-bmz cdescuento cdescuento_'.$rowcobrar.'"
                                    onclick="activardescuento('.$rowcobrar.')"
                                    value="0" readonly>
                              </td>
                              <td><a class="btn-bmz red deletetrrow"><i class="fas fa-trash-alt"></i></a></td>
                            </tr>'; 
                            $ivass=round($excedentec*0.16, 4);
                            $array_conceptos[]=array('Unidad'=>$option_unidad_ve_value,'servicioId'=>$option_concepto_ve_value,'Descripcion'=>$option_concepto_ve_text,'Descripcion2'=>$desc_fr,'Cu'=>$excedentec,'Importe'=>$excedentec,'iva'=>$ivass);
                            $rowcobrar++;   
                    }            
                }else if($tipo == 3){
                    if($excedente > 0){ // excedentes a monocromatico  
                        $desc_fr=$descrip_fac.''.number_format($cant_ext_mono,0,'.',',').' Clicks Excedente monocromatico del '.$fechaie.' al '.$fechafe;
                      $html.='<tr class="datos_principal tr_7">
                              <td>
                                <input id="selectedequipo" name="selectedequipo" type="hidden" value="0" readonly>
                                <input id="selectedprefactura" name="selectedprefactura" type="hidden" value="'.$idfactura.'" readonly>
                                <input id="facturarenta" name="facturarenta" type="hidden" value="0" readonly>
                                <input id="facturaexcedente" name="facturaexcedente" type="hidden" value="1" readonly>
                                <input id="cantidad" name="cantidad" type="text" value="1" class="excedente info cant_'.$rowcobrar.'" data-cantidad="'.$cant_ext_mono.'" data-precioexc="'.$prec_ext_mono.'" data-preid="'.$rowcobrar.'" readonly></td>
                              <td>
                                <div style="width:250px">
                                  <select id="unidadsat" class="unidadsat browser-default">
                                  '.$option_unidad_renta.'
                                  </select>
                                <div>
                              </td>
                              <td>
                                  <div style="width:250px">
                                    <select id="conseptosat" class="conseptosat browser-default">
                                    '.$option_concepto_renta.'
                                    </select>
                                  <div>
                              </td>
                              <td><input id="descripcion" name="descripcion" type="text" value="'.$desc_fr.'"></td>
                              <td><input id="precio" name="precio" class="precio  excedente precio_'.$rowcobrar.'" type="text" value="'.$excedente.'" readonly></td>
                              <td>
                                  <input type="number"
                                    name="descuento"
                                    id="descuento"
                                    class="form-control-bmz cdescuento cdescuento_'.$rowcobrar.'"
                                    onclick="activardescuento('.$rowcobrar.')"
                                    value="0" readonly>
                              </td>
                              <td><a class="btn-bmz red deletetrrow"><i class="fas fa-trash-alt"></i></a></td>
                            </tr>';

                            $ivass=round($excedente*0.16, 4);
                            $array_conceptos[]=array('Unidad'=>$option_unidad_ve_value,'servicioId'=>$option_concepto_ve_value,'Descripcion'=>$option_concepto_ve_text,'Descripcion2'=>$desc_fr,'Cu'=>$excedente,'Importe'=>$excedente,'iva'=>$ivass);

                            $rowcobrar++;
                    }
                    if($excedentec > 0){ // excedentes a monocromatico  
                        $desc_fr = $descrip_fac.'Excedente color del '.$fechaie.' al '.$fechafe;
                      $html.='<tr class="datos_principal tr_8">
                              <td>
                                <input id="selectedequipo" name="selectedequipo" type="hidden" value="0" readonly>
                                <input id="selectedprefactura" name="selectedprefactura" type="hidden" value="'.$idfactura.'" readonly>
                                <input id="facturarenta" name="facturarenta" type="hidden" value="0" readonly>
                                <input id="facturaexcedente" name="facturaexcedente" type="hidden" value="1" readonly>
                                <input id="cantidad" name="cantidad" type="text" value="1" class="excedente info cant_'.$rowcobrar.'" data-cantidad="'.$cant_ext_color.'" data-precioexc="'.$prec_ext_color.'" data-preid="'.$rowcobrar.'" readonly></td>
                              <td>
                                <div style="width:250px">
                                  <select id="unidadsat" class="unidadsat browser-default">
                                  '.$option_unidad_renta.'
                                  </select>
                                <div>
                              </td>
                              <td>
                                  <div style="width:250px">
                                    <select id="conseptosat" class="conseptosat browser-default">
                                    '.$option_concepto_renta.'
                                    </select>
                                  <div>
                              </td>
                              <td><input id="descripcion" name="descripcion" type="text" value="'.$desc_fr.'"></td>
                              <td><input id="precio" name="precio" class="precio  excedente precio_'.$rowcobrar.'" type="text" value="'.$excedentec.'" readonly></td>
                              <td>
                                  <input type="number"
                                    name="descuento"
                                    id="descuento"
                                    class="form-control-bmz cdescuento cdescuento_'.$rowcobrar.'"
                                    onclick="activardescuento('.$rowcobrar.')"
                                    value="0" readonly>
                              </td>
                              <td><a class="btn-bmz red deletetrrow"><i class="fas fa-trash-alt"></i></a></td>
                            </tr>';

                            $ivass=round($excedentec*0.16, 4);
                            $array_conceptos[]=array('Unidad'=>$option_unidad_ve_value,'servicioId'=>$option_concepto_ve_value,'Descripcion'=>$option_concepto_ve_text,'Descripcion2'=>$desc_fr,'Cu'=>$excedentec,'Importe'=>$excedentec,'iva'=>$ivass);

                            $rowcobrar++;
                    }
                }
              }else{
                //$equipos = $this->input->post('equipos');
                $DATAeq = json_decode($equipos);
                for ($i=0;$i<count($DATAeq);$i++) {
                  $equiposfacturar=$this->datosequiporentafactura($idfactura,$DATAeq[$i]->equipos,$DATAeq[$i]->serie,$DATAeq[$i]->tipo);
                  foreach ($equiposfacturar as $item) {
                    if($tipo == 1){//renta
                      if ($item->statusfacturar==0) {
                        if ($item->renta>0) {
                          if($item->tipo==1 and $mon==1){
                            $desc_fr=$descrip_fac.'Renta '.$item->modelo.' '.$item->serie.' Monocromaticas del '.$fechai.' al '.$fechaf;
                          $html.='<tr class="datos_principal tr_9">
                              <td>
                                <input id="selectedequipo" name="selectedequipo" type="hidden" value="'.$item->prefdId.'" readonly>
                                <input id="selectedprefactura" name="selectedprefactura" type="hidden" value="0" readonly>
                                <input id="facturarenta" name="facturarenta" type="hidden" value="1" readonly>
                                <input id="facturaexcedente" name="facturaexcedente" type="hidden" value="0" readonly>
                                <input id="cantidad" name="cantidad" type="text" value="1" readonly></td>
                              <td>
                                  <div style="width:250px">
                                    <select id="unidadsat" class="unidadsat browser-default">
                                    '.$option_unidad_renta.'
                                    </select>
                                  <div>
                              </td>
                              <td>
                                <div style="width:250px">
                                  <select id="conseptosat" class="conseptosat browser-default">
                                  '.$option_concepto_renta.'
                                  </select>
                                <div>
                              </td>
                              <td><input id="descripcion" name="descripcion" type="text" value="'.$desc_fr.'"></td>
                              <td><input id="precio" name="precio" class="precio" type="text" value="'.$item->renta.'" readonly></td>
                              <td>
                                  <input type="number"
                                    name="descuento"
                                    id="descuento"
                                    class="form-control-bmz cdescuento cdescuento_'.$rowcobrar.'"
                                    onclick="activardescuento('.$rowcobrar.')"
                                    value="0" readonly>
                              </td>
                              <td><a class="btn-bmz red deletetrrow"><i class="fas fa-trash-alt"></i></a></td>
                            </tr>';

                            $ivass=round($item->renta*0.16, 4);
                            $array_conceptos[]=array('Unidad'=>$option_unidad_ve_value,'servicioId'=>$option_concepto_ve_value,'Descripcion'=>$option_concepto_ve_text,'Descripcion2'=>$desc_fr,'Cu'=>$item->renta,'Importe'=>$item->renta,'iva'=>$ivass);

                            $rowcobrar++;
                          }
                        }
                        if ($item->rentac>0) {
                          if($item->tipo==2 and $col==2){
                            $desc_fr=$descrip_fac.'Renta '.$item->modelo.' '.$item->serie.' Color del '.$fechai.' al '.$fechaf;
                          $html.='<tr class="datos_principal tr_10">
                              <td>
                                <input id="selectedequipo" name="selectedequipo" type="hidden" value="'.$item->prefdId.'" readonly>
                                <input id="selectedprefactura" name="selectedprefactura" type="hidden" value="0" readonly>
                                <input id="facturarenta" name="facturarenta" type="hidden" value="1" readonly>
                                <input id="facturaexcedente" name="facturaexcedente" type="hidden" value="0" readonly>
                                <input id="cantidad" name="cantidad" type="text" value="1" readonly></td>
                              <td>
                                  <div style="width:250px">
                                    <select id="unidadsat" class="unidadsat browser-default">
                                    '.$option_unidad_renta.'
                                    </select>
                                  <div>
                              </td>
                              <td>
                                <div style="width:250px">
                                  <select id="conseptosat" class="conseptosat browser-default">
                                  '.$option_concepto_renta.'
                                  </select>
                                <div>
                              </td>
                              <td><input id="descripcion" name="descripcion" type="text" value="'.$desc_fr.'"></td>
                              <td><input id="precio" name="precio" class="precio" type="text" value="'.$item->rentac.'" readonly></td>
                              <td>
                                  <input type="number"
                                    name="descuento"
                                    id="descuento"
                                    class="form-control-bmz cdescuento cdescuento_'.$rowcobrar.'"
                                    onclick="activardescuento('.$rowcobrar.')"
                                    value="0" readonly>
                              </td>
                              <td><a class="btn-bmz red deletetrrow"><i class="fas fa-trash-alt"></i></a></td>
                            </tr>';
                            
                            $ivass=round($item->rentac*0.16, 4);
                            $array_conceptos[]=array('Unidad'=>$option_unidad_ve_value,'servicioId'=>$option_concepto_ve_value,'Descripcion'=>$option_concepto_ve_text,'Descripcion2'=>$desc_fr,'Cu'=>$item->rentac,'Importe'=>$item->rentac,'iva'=>$ivass);

                            $rowcobrar++;
                          }
                        }
                        
                      }
                    }
                    if($tipo == 3){//excedentes
                      if ($item->statusfacturae==0) {
                        if ($item->totalexcedente>0 and $mon==1) {
                            $desc_fr=$descrip_fac.''.number_format($item->excedentes,0,'.',',').' Clicks Excedente '.$item->modelo.' '.$item->serie.' Monocromaticas del '.$fechaie.' al '.$fechafe;
                          $html.='<tr class="datos_principal tr_11">
                              <td>
                                <input id="selectedequipo" name="selectedequipo" type="hidden" value="'.$item->prefdId.'" readonly>
                                <input id="selectedprefactura" name="selectedprefactura" type="hidden" value="0" readonly>
                                <input id="facturarenta" name="facturarenta" type="hidden" value="0" readonly>
                                <input id="facturaexcedente" name="facturaexcedente" type="hidden" value="1" readonly>
                                <input id="cantidad" name="cantidad" type="text" value="1" class="excedente info cant_'.$item->prefdId.'" data-cantidad="'.$item->excedentes.'" data-precioexc="'.$item->costoexcedente.'" data-preid="'.$item->prefdId.'" readonly></td>
                              <td>
                                  <div style="width:250px">
                                    <select id="unidadsat" class="unidadsat browser-default">
                                    '.$option_unidad_renta.'
                                    </select>
                                  <div>
                              </td>
                              <td>
                                <div style="width:250px">
                                  <select id="conseptosat" class="conseptosat browser-default">
                                  '.$option_concepto_renta.'
                                  </select>
                                <div>
                              </td>
                              <td><input id="descripcion" name="descripcion" type="text" value="'.$desc_fr.'"></td>
                              <td><input id="precio" name="precio" class="precio excedente precio_'.$item->prefdId.'" type="text" value="'.$item->totalexcedente.'" readonly></td>
                              <td>
                                  <input type="number"
                                    name="descuento"
                                    id="descuento"
                                    class="form-control-bmz cdescuento cdescuento_'.$rowcobrar.'"
                                    onclick="activardescuento('.$rowcobrar.')"
                                    value="0" readonly>
                              </td>
                              <td><a class="btn-bmz red deletetrrow"><i class="fas fa-trash-alt"></i></a></td>
                            </tr>';

                            $ivass=round($item->totalexcedente*0.16, 4);
                            $array_conceptos[]=array('Unidad'=>$option_unidad_ve_value,'servicioId'=>$option_concepto_ve_value,'Descripcion'=>$option_concepto_ve_text,'Descripcion2'=>$desc_fr,'Cu'=>$item->totalexcedente,'Importe'=>$item->totalexcedente,'iva'=>$ivass);

                            $rowcobrar++;
                        }
                      }
                    }
                    if($tipo == 2){//renta y excendentes
                      if ($item->statusfacturar==0) {
                        if ($item->renta>0) {
                          if($item->tipo==1 and $mon==1){
                            $desc_fr=$descrip_fac.'Renta '.$item->modelo.' '.$item->serie.' Monocromaticas del '.$fechai.' al '.$fechaf;
                          $html.='<tr class="datos_principal tr_12">
                              <td>
                                <input id="selectedequipo" name="selectedequipo" type="hidden" value="'.$item->prefdId.'" readonly>
                                <input id="selectedprefactura" name="selectedprefactura" type="hidden" value="0" readonly>
                                <input id="facturarenta" name="facturarenta" type="hidden" value="1" readonly>
                                <input id="facturaexcedente" name="facturaexcedente" type="hidden" value="1" readonly>
                                <input id="cantidad" name="cantidad" type="text" value="1" readonly></td>
                              <td>
                                  <div style="width:250px">
                                    <select id="unidadsat" class="unidadsat browser-default">
                                    '.$option_unidad_renta.'
                                    </select>
                                  <div>
                              </td>
                              <td>
                                <div style="width:250px">
                                  <select id="conseptosat" class="conseptosat browser-default">
                                  '.$option_concepto_renta.'
                                  </select>
                                <div>
                              </td>
                              <td><input id="descripcion" name="descripcion" type="text" value="'.$desc_fr.'"></td>
                              <td><input id="precio" name="precio" class="precio" type="text" value="'.$item->renta.'" readonly></td>
                              <td>
                                  <input type="number"
                                    name="descuento"
                                    id="descuento"
                                    class="form-control-bmz cdescuento cdescuento_'.$rowcobrar.'"
                                    onclick="activardescuento('.$rowcobrar.')"
                                    value="0" readonly>
                              </td>
                              <td><a class="btn-bmz red deletetrrow"><i class="fas fa-trash-alt"></i></a></td>
                            </tr>';
                                $ivass=round($item->renta*0.16, 4);
                            $array_conceptos[]=array('Unidad'=>$option_unidad_ve_value,'servicioId'=>$option_concepto_ve_value,'Descripcion'=>$option_concepto_ve_text,'Descripcion2'=>$desc_fr,'Cu'=>$item->renta,'Importe'=>$item->renta,'iva'=>$ivass);

                            $rowcobrar++;
                            }
                          }
                          if ($item->rentac>0) {
                            if($item->tipo==2 and $col==2){
                                $desc_fr=$descrip_fac.'Renta '.$item->modelo.' '.$item->serie.' Color del '.$fechai.' al '.$fechaf;
                            $html.='<tr class="datos_principal tr_13">
                                <td>
                                  <input id="selectedequipo" name="selectedequipo" type="hidden" value="'.$item->prefdId.'" readonly>
                                  <input id="selectedprefactura" name="selectedprefactura" type="hidden" value="0" readonly>
                                  <input id="facturarenta" name="facturarenta" type="hidden" value="1" readonly>
                                  <input id="facturaexcedente" name="facturaexcedente" type="hidden" value="1" readonly>
                                  <input id="cantidad" name="cantidad" type="text" value="1" readonly></td>
                                <td>
                                    <div style="width:250px">
                                      <select id="unidadsat" class="unidadsat browser-default">
                                      '.$option_unidad_renta.'
                                      </select>
                                    <div>
                                </td>
                                <td>
                                  <div style="width:250px">
                                    <select id="conseptosat" class="conseptosat browser-default">
                                    '.$option_concepto_renta.'
                                    </select>
                                  <div>
                                </td>
                                <td><input id="descripcion" name="descripcion" type="text" value="'.$desc_fr.'"></td>
                                <td><input id="precio" name="precio" class="precio" type="text" value="'.$item->rentac.'" readonly></td>
                                <td>
                                  <input type="number"
                                    name="descuento"
                                    id="descuento"
                                    class="form-control-bmz cdescuento cdescuento_'.$rowcobrar.'"
                                    onclick="activardescuento('.$rowcobrar.')"
                                    value="0" readonly>
                              </td>
                              <td><a class="btn-bmz red deletetrrow"><i class="fas fa-trash-alt"></i></a></td>
                              </tr>';

                                $ivass=round($item->rentac*0.16, 4);
                                $array_conceptos[]=array('Unidad'=>$option_unidad_ve_value,'servicioId'=>$option_concepto_ve_value,'Descripcion'=>$option_concepto_ve_text,'Descripcion2'=>$desc_fr,'Cu'=>$item->rentac,'Importe'=>$item->rentac,'iva'=>$ivass);


                              $rowcobrar++;
                            }
                          }
                      }
                      if ($item->statusfacturae==0) {
                        if ($item->totalexcedente>0 and $mon==1) {
                            if ($item->tipo==2) {
                              $tipocequipo=' Color ';
                            }else{
                              $tipocequipo=' Monocromaticas ';
                            }
                            $desc_fr=$descrip_fac.''.number_format($item->excedentes,0,'.',',').' Clicks Excedente '.$item->modelo.' '.$item->serie.' '.$tipocequipo.' del '.$fechaie.' al '.$fechafe;
                          $html.='<tr class="datos_principal tr_14">
                              <td>
                                <input id="selectedequipo" name="selectedequipo" type="hidden" value="'.$item->prefdId.'" readonly>
                                <input id="selectedprefactura" name="selectedprefactura" type="hidden" value="0" readonly>
                                <input id="facturarenta" name="facturarenta" type="hidden" value="1" readonly>
                                <input id="facturaexcedente" name="facturaexcedente" type="hidden" value="1" readonly>
                                <input id="cantidad" name="cantidad" type="text" value="1" class="excedente info cant_'.$item->prefdId.'" data-cantidad="'.$item->excedentes.'" data-precioexc="'.$item->costoexcedente.'" data-preid="'.$item->prefdId.'" readonly></td>
                              <td>
                                  <div style="width:250px">
                                    <select id="unidadsat" class="unidadsat browser-default">
                                    '.$option_unidad_renta.'
                                    </select>
                                  <div>
                              </td>
                              <td>
                                <div style="width:250px">
                                  <select id="conseptosat" class="conseptosat browser-default">
                                  '.$option_concepto_renta.'
                                  </select>
                                <div>
                              </td>
                              <td><input id="descripcion" name="descripcion" type="text" value="'.$desc_fr.'"></td>
                              <td><input id="precio" name="precio" class="precio excedente precio_'.$item->prefdId.'" type="text" value="'.$item->totalexcedente.'" readonly></td>
                              <td>
                                  <input type="number"
                                    name="descuento"
                                    id="descuento"
                                    class="form-control-bmz cdescuento cdescuento_'.$rowcobrar.'"
                                    onclick="activardescuento('.$rowcobrar.')"
                                    value="0" readonly>
                              </td>
                              <td><a class="btn-bmz red deletetrrow"><i class="fas fa-trash-alt"></i></a></td>
                            </tr>';

                            $ivass=round($item->totalexcedente*0.16, 4);
                                $array_conceptos[]=array('Unidad'=>$option_unidad_ve_value,'servicioId'=>$option_concepto_ve_value,'Descripcion'=>$option_concepto_ve_text,'Descripcion2'=>$desc_fr,'Cu'=>$item->totalexcedente,'Importe'=>$item->totalexcedente,'iva'=>$ivass);

                            $rowcobrar++;
                        }
                      }
                    }
                    
                  }
                }
              }
              if($htmlarray==0){
                return $html;
              }elseif($htmlarray==1){
                return $array_conceptos;
              }

        }
        function info_equipo_dev($idventa){
            $strq="SELECT vde.id_dev,vde.id,vde.idVenta, vde.cantidad,vde.modelo,vde.motivo_dev,vde.reg_dev
                    FROM `ventas_has_detallesEquipos_dev` as vde
                    WHERE vde.idVenta='$idventa'";
            $query = $this->db->query($strq);
            return $query; 
        }
}
