// Obtener la base_url 
var base_url = $('#base_url').val();
var equipo_infog;
$(document).ready(function(){
    $('.modal').modal();
    // Inicializar la tabla
    table = $('#tabla_equipos').DataTable();
    load();

    // Listener para editar
    $('#tabla_equipos tbody').on('click', 'a.edit', function () {
        var tr = $(this).closest('tr');
        var row = table.row(tr);
        var data = row.data();
        window.location = 'equipos/edicion/'+data.id;
    });

    // Listener para Visualizar 
    $('#tabla_equipos tbody').on('click', 'a.visualiza', function () {
        var tr = $(this).closest('tr');
        var row = table.row(tr);
        var data = row.data();
        window.location = 'equipos/visualizar/'+data.id;
    });

    // Listener para Eliminar 
    $('#tabla_equipos tbody').on('click', 'a.elimina', function () {
        var tr = $(this).closest('tr');
        var row = table.row(tr);
        var data = row.data();
        confirmaEliminarEquipo(data.id);
    });

    // Validar en la carga masiva, que el tipo del archivo sea XLSX
    // Si no lo es, no lo dejará avanzar
    $('#inputFile').change(function () {
        var ext = this.value.match(/\.(.+)$/)[1];
        switch (ext) {
            case 'xlsx':
                $('#uploadButton').attr('disabled', false);
            break;
            default:
                $.alert({boxWidth: '30%',useBootstrap: false,
                        title: 'Atención!',content: 'No es el tipo de archivo permitido.'});
                $('#uploadButton').attr('disabled', true);
            break;
        }
    });

    // Enviar el archivo para carga masiva vía AJAX
    $("#archivo-form").on("submit", function(e){
        e.preventDefault();
        var formData = new FormData(document.getElementById("archivo-form"));
        $.ajax({
                url: base_url+"index.php/equipos/cargaArchivo",
                type: "post",
                data: formData,
                dataType: "html",
                cache: false,
                contentType: false,
                processData: false,
                beforeSend: function() {
                    document.getElementById('loading').style.visibility = "visible";
                },
                success: function (response) {
                    alert("Archivo cargado correctamente");
                    $.alert({boxWidth: '30%',useBootstrap: false,
                            title: 'Atención!',content: 'Archivo cargado correctamente'});
                    setTimeout(function(){ location.reload(); }, 3000);
                },
                error:function() {
                    toastr.error("Error Inesperado, intente de nuevo");
                }
        });
    });

    // Descargar tabla
    $('#descargarExcel').on('click', function () {
        var datos = document.getElementById('tabla_equipos');
        Exporter.export(datos, 'reportes.xls', 'reportes');         
    });
    $("#files").fileinput({
        showCaption: true,
        showUpload: true,// quita el boton de upload
        //dropZoneEnabled:false,//false lo compacta, true deja el espacio
        //rtl: true,
        allowedFileExtensions: ["jpg","png","webp","jpeg"],
        browseLabel: 'Seleccionar documentos',
        uploadUrl: base_url+'Equipos/imagenes_multiple',
        maxFilePreviewSize: 5000,
        previewFileIconSettings: {
            'docx': '<i class="fa fa-file-word-o text-primary"></i>',
            'xlsx': '<i class="fa fa-file-excel-o text-success"></i>',
            'pptx': '<i class="fa fa-file-powerpoint-o text-danger"></i>',
            'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
            'pdf': '<i class="fa fa-file-pdf-o text-danger"></i>',
            'zip': '<i class="fa fa-file-archive-o text-muted"></i>',
            'cer': '<i class="fas fa-file-invoice"></i>',
        },
        uploadExtraData: function (previewId, index) {
        var info = {
                    idequipo:equipo_infog
                };
        return info;
      }
    }).on('filebatchuploadcomplete', function(event, files, extra) {
      //location.reload();
        upload_data_view(equipo_infog);
        $('#files').fileinput('clear');
    }).on('filebatchuploadsuccess', function(event, files, extra) {
      //location.reload();
      //toastr.success('Se cargo el Certificado Correctamente','Hecho!');
        upload_data_view(equipo_infog);
        $('#files').fileinput('clear');
    });
    $('#save_fc').click(function(event) {
        var datos = $('#form_catacteristicas').serialize();
                
        $.ajax({
            type:'POST',
            url: base_url+"index.php/Equipos/insertaActualizacaracteristicas",
            data: datos,
            success:function(data){  
                catacte_data_view(equipo_infog);
                toastr["success"]("Se ha Procesado con exito", "éxito");
                fc_limpiar();
                
            },
            error: function(response){
                toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema", "Advertencia");
            }
        });
    });
});

function confirmaEliminarEquipo(idCliente){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de Eliminar este Equipo? El cambio será irreversible',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    url: base_url+"index.php/Equipos/eliminarEquipo/"+idCliente,
                    success: function (response){
                            $.alert({
                                boxWidth: '30%',
                                useBootstrap: false,
                                title: 'Éxito!',
                                content: 'Equipo Eliminado!'});
                            setTimeout(function(){ 
                                window.location.href = base_url+"index.php/equipos"; 
                            }, 2000);
                    },
                    error: function(response){
                        $.alert({boxWidth: '30%',useBootstrap: false,
                                title: 'Error!',content: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema'});   
                        setTimeout(function(){ 
                                window.location.href = base_url+"index.php/equipos";
                            }, 2000);  
                    }
                });
            },
            cancelar: function (){
                
            }
        }
    });
}

function load() {
    // Destruye la tabla y la crea de nuevo con los datos cargados 
    table.destroy();
    table = $('#tabla_equipos').DataTable({
        search: {
                return: true
            }, 
        //stateSave: true,
        "ajax": {
            "url": base_url+"index.php/equipos/getListadoEquipos"
        },
        "columns": [
            {"data": "id"},
            {"data": "foto",
                render:function(data,type,row){ 
                    if(data!=""){
                        return '<a class="btn_img" target="_blank" href="'+base_url+'uploads/equipos/'+data+'"><img src="'+base_url+'uploads/equipos/'+data+'" style="width:50px;" class="imgpro"></a>';
                    }else{
                        return '<img src="'+base_url+'app-assets/images/kyocera_mini.png" style="width:50px;" class="imgpro">';
                    }
                }
            },
            {"data": "modelo"},
            {"data": "especificaciones"},
            {"data": "especificaciones_tecnicas"},
            {"data": "nombre"},
            //{"data": "stock"},
            {"data": "destacado",
                render:function(data,type,row){ 
                    var html='';
                    if(row.destacado==1){
                        var startchecked='checked';
                    }else{
                        var startchecked='';
                    }
                    html='<input class="star_f" type="checkbox" title="Favorito" id="star_factura_'+row.id+'" onclick="favorito('+row.id+')" '+startchecked+'>';
                    return html;
                }
            },
            {"data": null,
                render:function(data,type,row){
                    // Se muestran todos los botones correspondientes
                    $('.tooltipped').tooltip();
                    var html='<a class="btn-floating green tooltipped edit soloadministradores" data-position="top" data-delay="50" data-tooltip="Editar"><i class="material-icons">mode_edit</i></a>\
                            <a class="btn-floating blue tooltipped visualiza" data-position="top" data-delay="50" data-tooltip="Visualizar"><i class="material-icons">remove_red_eye</i></a>\
                            <a class="btn-floating red tooltipped elimina soloadministradores" data-position="top" data-delay="50" data-tooltip="Eliminar"><i class="material-icons">delete_forever</i></a>\
                            <a class="btn-floating blue tooltipped infogen_'+row.id+'" data-modelo="'+row.modelo+'" data-position="top" data-delay="50" data-tooltip="Informacion General" onclick="infogen('+row.id+')"><i class="fa fa-laptop fa-fw"></i></a>';
                            if(row.paginaweb==1){
                                var viewweb='checked';
                            }else{
                                var viewweb='';
                            }
                        html+='<div class="switch"><br>\
                                  <label class="soloadministradores">Mostrar\
                                    <input type="checkbox" name="bloqueo" id="mostrarweb_'+row.id+'" wtx-context="7579CA6E-0857-4E6C-B2D3-9042AE775C00" onclick="mostrarweb('+row.id+')" '+viewweb+'>\
                                    <span class="lever"></span>\
                                  </label>\
                            </div>';    
                    return html;
                }
            }
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[25, 50, 100], [25, 50, 100]],
        "dom": 'Bfrtpl',
        // Muestra el botón de Excel para importar la tabla
        "buttons": [
            
        ]
    }).on('draw',function(){
        $('.dataTables_filter').append('<i class="fa fa-search buttonsearch" onclick="searchtable_v()"></i>')

    });
}
function searchtable_v(){
    var contenedor = document.getElementById('tabla_equipos_filter');

    // Selecciona el input dentro del contenedor
    var input = contenedor.querySelector('input.form-control-bmz');

    // Obtén el valor del input
    var searchv = input.value;

    // Muestra el valor en la consola
    console.log(searchv);
    //var searchv =$("input[type=search]").val();
    table.search(searchv).draw();
}
function infogen(id){
    $('#modalinfoequipo').modal('open');
    var modelo=$('.infogen_'+id).data('modelo');
    $('#idequipofc').val(id);
    $('.equipo_modelo').html(modelo);
    equipo_infog=id;
    upload_data_view(equipo_infog);
    catacte_data_view(equipo_infog);
    $('#files').fileinput('clear');
}
function upload_data_view(id){
    console.log(id);
    $.ajax({
        type:'POST',
        url: base_url+'index.php/Equipos/viewimages',
        data: {idequipo:id},
        success:function(data){
            $('.galeriimg').html(data);
            $('.materialboxed').materialbox();
        }
    });
}
function deleteimg(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de Eliminar la imagen? El cambio será irreversible',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Equipos/deleteimg/",
                    data: {idequipo:id},
                    success: function (response){
                        toastr["success"]("Se ha eliminado con exito", "éxito");
                        upload_data_view(equipo_infog);
                    },
                    error: function(response){
                        toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema", "Advertencia");
                    }
                });
            },
            cancelar: function (){
                
            }
        }
    });
}
function catacte_data_view(id){
    $.ajax({
        type:'POST',
        url: base_url+'index.php/Equipos/viewcaracte',
        data: {idequipo:id},
        success:function(data){
            $('.lis_catacter').html(data);
        }
    });
}
function fc_limpiar(){
    $('#idfc').val(0);
    $('#descripcion').val('');
    $('#name').val('');
    $('#general').prop('checked',false);
    $('#orden').val('');
    $('#cancelar_fc').hide('slow');
}
function editar_ec(id){
    $('#cancelar_fc').show('slow');
    var name = $('.eq_cact_'+id).data('name');
    var descripcion = $('.eq_cact_'+id).data('descripcion');
    var general = $('.eq_cact_'+id).data('general');
    var orden = $('.eq_cact_'+id).data('orden');

    $('#idfc').val(id);
    $('#name').val(name);
    $('#descripcion').val(descripcion);
    if(general==1){
        $('#general').prop('checked',true);    
    }else{
        $('#general').prop('checked',false);
    }
    $('#orden').val(orden);
}
function delete_ec(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de Eliminar la caracteristica? El cambio será irreversible',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Equipos/deleteec",
                    data: {idequipo:id},
                    success: function (response){
                        toastr["success"]("Se ha eliminado con exito", "éxito");
                        catacte_data_view(equipo_infog);
                    },
                    error: function(response){
                        toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema", "Advertencia");
                    }
                });
            },
            cancelar: function (){
                
            }
        }
    });
}
function mostrarweb(id){
    setTimeout(function(){ 
        var paginaweb = $('#mostrarweb_'+id).is(':checked')==true?1:0;
        $.ajax({
            type:'POST',
            url: base_url+"index.php/Equipos/mostrarweb",
            data: {
                    idequipo:id,
                    mostrar:paginaweb
                },
            success: function (response){
                
            },
            error: function(response){
                toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema", "Advertencia");
            }
        });
    }, 1000);
}
function favorito(id){
    setTimeout(function(){ 
        var statusfav=$('#star_factura_'+id).is(':checked')==true?1:0;
        $.ajax({
            type:'POST',
            url: base_url+'Equipos/favorito',
            data: {
                equipo:id,
                status:statusfav
            },
            statusCode:{
                404: function(data){
                    toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema", "Advertencia");
                },
                500: function(){
                    toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema", "Advertencia");
                }
            },
            success:function(data){
                
               
            }
        });
    }, 1000);
}