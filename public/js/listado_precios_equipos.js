var base_url = $('#base_url').val();
var perfilid = $('#perfilid').val();
var editor; 

$(document).ready(function(){
    // Si tiene el perfil de Administrador, se habilita la edición de la lista de precios
	if(perfilid==1){
        editor = new $.fn.dataTable.Editor( {
        ajax: base_url+"index.php/Listapreciosequipos/actualizaPrecio",
        table: "#tabla_precios",
            fields: [ {
                    label: "Costo en Dólares:",
                    name: "precioDolares"
                }, {
                    label: "Costo Local:",
                    name: "costo_venta"
                }, {
                    label: "Costo General:",
                    name: "costo_renta"
                }, {
                    label: "Costo Foraneo:",
                    name: "costo_poliza"
                }, {
                    label: "Costo Revendendor:",
                    name: "costo_revendedor"
                }
            ]
        } );
    }
    
    // Activa la edición por casilla en cada ROW de la tabla
    $('#tabla_precios').on( 'click', 'tbody td:not(:first-child)', function (e){
        editor.inline(this);
    });

    table = $('#tabla_precios').DataTable();
    load();
});


function load() {
    // Destruye la tabla y la crea de nuevo con los datos cargados 
    table.destroy();
    table = $('#tabla_precios').DataTable({
        dom: 'Blfrtip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'pdfHtml5',
        ],
        "ajax": {
            "url": base_url+"index.php/Listapreciosequipos/getListaPreciosEquipo"
        },
        "columns": [
            {"data": "DT_RowId"},
            {"data": "modelo"},
            {"data": "precioDolares"},
            {"data": "costo_pesos"},
            {"data": "costo_renta"},
            {"data": "costo_venta"},
            
            {"data": "costo_poliza"},
            //{"data": "costo_revendedor"},
            //{"data": "porcentaje_ganancia"},
            //{"data": "descuento"},
            {"data": "categoria"}
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[25, 50, 100], [25, 50, 100]],
        
        // Cambiamos lo principal a Español
        "language": {
            "lengthMenu": "Desplegando _MENU_ elementos por página",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
              }
        }
    });
}

