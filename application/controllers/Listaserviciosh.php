<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Listaserviciosh extends CI_Controller {
    public function __construct()     {
        parent::__construct();

        $this->load->model('ModeloCatalogos');
        $this->load->model('Configuraciones_model');
        $this->load->model('ConfiguracionDocumentos_Model');
        $this->load->model('ModeloServicioh');
        $this->load->model('Ventas_model');
        $this->load->helper('url');
        $this->version = date('YmdHi');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fechaactual=date('Y-m-d H:i:s');
        $this->submenu=61;
        if($this->session->userdata('logeado')==true){
            $this->perfilid =$this->session->userdata('perfilid');
            $this->idpersonal = $this->session->userdata('idpersonal');
            /*
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,52);// 52 es el id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
            */
        }else{
            redirect('login'); 
        }
    }

    function index(){
        $data['MenusubId']=$this->submenu; 
        //$data['resultstec']=$this->Configuraciones_model->searchTecnico();  
        //$data['resultzonas']=$this->ModeloCatalogos->getselectwheren('rutas',array('status'=>1));
        $data['resultstec']=$this->Configuraciones_model->searchTecnico(); 
        //$data['consumiblesresult']=$this->ModeloCatalogos->getselectwheren('consumibles',array('status'=>1)); 
        //$data['refaccionesresult']=$this->ModeloCatalogos->getselectwheren('refacciones',array('status'=>1)); 
        $data['version']=$this->version;
        if ($this->perfilid==1 or $this->perfilid=10) {
            $data['idpersonal']=0;
            $data['idpersonaldisplay']='';
        }else{
            $data['idpersonal']=$this->idpersonal;
            $data['idpersonaldisplay']='style="display: none;"';
        }
            $data['perfilid']=$this->perfilid;
            $data['fechainicial_reg'] = date("Y-m-d",strtotime($this->fechaactual."- 12 month")); 
        $this->load->view('header');
        $this->load->view('main');
        $this->load->view('Servicio/listaservicioshi',$data);
        $this->load->view('footer');
        $this->load->view('Servicio/listaservicioshijs');
    }
    
    public function getlistaservicioc() {
        $params = $this->input->post();
        $params['perfil']=$this->perfilid;
        $getdata = $this->ModeloServicioh->getlistaservicioc($params);
        $totaldata= $this->ModeloServicioh->getlistaservicioct($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    public function getlistaserviciop() {
        $params = $this->input->post();
        $params['perfil']=$this->perfilid;
        $getdata = $this->ModeloServicioh->getlistaserviciop($params);
        $totaldata= $this->ModeloServicioh->getlistaserviciopt($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    public function getlistaserviciocl() {
        $params = $this->input->post();
        $params['perfil']=$this->perfilid;
        $getdata = $this->ModeloServicioh->getlistaserviciocl($params);
        $totaldata= $this->ModeloServicioh->getlistaservicioclt($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    public function getlistaserviciovent() {
        $params = $this->input->post();
        $params['perfil']=$this->perfilid;
        $getdata = $this->ModeloServicioh->getlistaserviciovent($params);
        $totaldata= $this->ModeloServicioh->getlistaservicioventt($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    function editcomen(){
        $params = $this->input->post();
        $asig   = $params['asig'];
        $tipo   = $params['tipo'];
        $asigd  = $params['asigd'];
        $coment = $params['coment'];
        if($tipo==1){//contrato
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_contrato_a_e',array('comentario'=>$coment),array('asignacionId'=>$asig));
        }
        if($tipo==2){//poliza
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_poliza_a_e',array('comentario'=>$coment),array('asignacionId'=>$asig));
        }
        if($tipo==3){//evento
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_cliente_a',array('comentario'=>$coment),array('asignacionId'=>$asig));
        }
        if($tipo==4){//venta
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_venta_a',array('comentario'=>$coment),array('asignacionId'=>$asig));
        }
    }
    /*
    function editarfecha(){
        $params = $this->input->post();
        $idrow =$params['idrow'];
        $table =$params['table'];
        $fecha =$params['fecha'];
        $hora =$params['hora'];
        $horafin =$params['horafin'];
        if ($table==1) {
            $tables='asignacion_ser_contrato_a_e';
            $idname='asignacionIde';
        }elseif ($table==2) {
            $tables='asignacion_ser_poliza_a_e';
            $idname='asignacionIde';
        }elseif ($table==3) {
            $tables='asignacion_ser_cliente_a';
            $idname='asignacionId';
        }elseif ($table==4) {
            $tables='asignacion_ser_venta_a';
            $idname='asignacionId';
        }
        $datas=array(
            'fecha'=>$fecha,
            'hora'=>$hora,
            'horafin'=>$horafin,

        );
        $re_datas=array(
            't_fecha'=>null,
            't_horainicio'=>null,
            't_horafin'=>null,

        );
        $this->ModeloCatalogos->updateCatalogo($tables,$datas,array($idname=>$idrow));
        $this->ModeloCatalogos->updateCatalogo($tables,$re_datas,array($idname=>$idrow));
    }
    function edittecnico(){
        $params = $this->input->post();
        $idrow = $params['idrow'];
        $idrowt = $params['idrowt'];
        $table = $params['table']; //1 contrato 2 poliza 3 cliente
        $tipo = $params['tipo'];//0 total 1 individual
        $tecnicoo = $params['tecnicoo'];
        $tecnico = $params['tecnico'];
        if ($table==1) {
            $tables='asignacion_ser_contrato_a_e';
            $idname='asignacionIde';
        }elseif ($table==2) {
            $tables='asignacion_ser_poliza_a_e';
            $idname='asignacionIde';
        }elseif ($table==3) {
            $tables='asignacion_ser_cliente_a';
            $idname='asignacionId';
        }elseif ($table==3) {
            $tables='asignacion_ser_venta_a';
            $idname='asignacionId';
        }
        if ($tipo==1) {
            $this->ModeloCatalogos->updateCatalogo($tables,array('tecnico'=>$tecnico),array($idname=>$idrow));
            $this->ModeloCatalogos->Insert('asignacion_ser_historial_t',array('asignacionId'=>$idrow,'tecnicoId'=>$tecnicoo,'tabless'=>$table));
        }else{
            $resultrow=$this->ModeloCatalogos->getselectwheren($tables,array('asignacionId'=>$idrowt));
            foreach ($resultrow->result() as $item) {
                if ($table==1) {
                    $idrowrow=$item->asignacionIde;
                }elseif ($table==2) {
                    $idrowrow=$item->asignacionIde;
                }elseif ($table==3) {
                    $idrowrow=$item->asignacionId;
                }
                $tecnico=$item->tecnico;
                $this->ModeloCatalogos->updateCatalogo($tables,array('tecnico'=>$tecnico),array($idname=>$idrowrow));
                $this->ModeloCatalogos->Insert('asignacion_ser_historial_t',array('asignacionId'=>$idrowrow,'tecnicoId'=>$tecnicoo,'tabless'=>$tecnico));
            }
        }
    }
    function viewhistorial(){
        $params = $this->input->post();
        $asi =$params['idrow'];
        $table =$params['table'];
        $resultrow = $this->ModeloServicio->historiatecnicos($asi,$table);
        $html='<table class="table">';
        foreach ($resultrow->result() as $item) {
            $html.='<tr>
                        <td>'.$item->nombre.' '.$item->apellido_paterno.' '.$item->apellido_materno.'</td>
                        <td>'.$item->reg.'</td>
                    </tr>
                    ';
        }
        $html.='</table>';
        echo $html;
    }
    function suspender(){
        $params = $this->input->post();
        $idrow =$params['idrow'];
        $table =$params['table'];
        $tserviciomotivo =$params['tserviciomotivo'];
        if ($table==1) {
            $tables='asignacion_ser_contrato_a_e';
            $idname='asignacionIde';
        }elseif ($table==2) {
            $tables='asignacion_ser_poliza_a_e';
            $idname='asignacionIde';
        }elseif ($table==3) {
            $tables='asignacion_ser_cliente_a';
            $idname='asignacionId';
        }elseif ($table==4) {
            $tables='asignacion_ser_venta_a';
            $idname='asignacionId';
        }
        $datas=array(
            'motivosuspencion'=>$tserviciomotivo,
            'status'=>3
        );
        $this->ModeloCatalogos->updateCatalogo($tables,$datas,array($idname=>$idrow));
    }
    function datosventaservicio(){
        $params = $this->input->post();
        $id =$params['id'];
        $tipo =$params['tipo'];
        $resultadoconsumibles=$this->Ventas_model->datosserviciosconsumibles($id,$tipo);
        $resultadorefacciones=$this->Ventas_model->datosserviciosrefacciones($id,$tipo);
        $array = array(
                    'consumibles'=>$resultadoconsumibles->result(),
                    'refacciones'=>$resultadorefacciones->result()
        );
        echo json_encode($array);
    }
    function editarventaservicio(){
        $params = $this->input->post();
        $tasign=$params['tasign'];
        $tipo=$params['tipo'];
        $vconsumible=$params['vconsumible'];
        $vrefaccion  =$params['vrefaccion'];

        $DATAvc = json_decode($vconsumible);
        for ($s=0;$s<count($DATAvc);$s++){
            if ($DATAvc[$s]->id > 0) {

                $this->ModeloCatalogos->updateCatalogo('asignacion_ser_avc',array('cantidad'=>$DATAvc[$s]->cantidad),array('asId'=>$DATAvc[$s]->id));
            }else{
                $datavca =array(
                    'asignacionId'=>$tasign,
                    'cantidad'=>$DATAvc[$s]->cantidad,
                    'consumible'=>$DATAvc[$s]->consumible,
                    'costo'=>$DATAvc[$s]->precio,
                    'tipo'=>$tipo
                );
                $this->ModeloCatalogos->Insert('asignacion_ser_avc',$datavca);
            }
            
        }
        $DATAvr = json_decode($vrefaccion);
        for ($c=0;$c<count($DATAvr);$c++){
            if ($DATAvr[$c]->id > 0) {

                $this->ModeloCatalogos->updateCatalogo('asignacion_ser_avr',array('cantidad'=>$DATAvr[$c]->cantidad),array('asId'=>$DATAvr[$c]->id));
            }else{
                $datavcra =array(
                    'asignacionId'=>$tasign,
                    'cantidad'=>$DATAvr[$c]->cantidad,
                    'refacciones'=>$DATAvr[$c]->refaccion,
                    'costo'=>$DATAvr[$c]->precio,
                    'tipo'=>$tipo
                );
                $this->ModeloCatalogos->Insert('asignacion_ser_avr',$datavcra);
            }
            
        }
    }
    function deleteacre(){
        $params = $this->input->post();
        $rowid = $params['rowid'];
        $tipo = $params['tipo'];
        if ($tipo==1) {//consumible
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_avc',array('activo'=>0),array('asId'=>$rowid));
        }
        if ($tipo==2) {//refaccion
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_avr',array('activo'=>0),array('asId'=>$rowid));
        }
        
    }
    function cancelartotalvs(){
        $params = $this->input->post();
        $tasign = $params['tasign'];
        $this->ModeloCatalogos->updateCatalogo('asignacion_ser_avc',array('activo'=>0),array('asignacionId'=>$tasign));
        $this->ModeloCatalogos->updateCatalogo('asignacion_ser_avr',array('activo'=>0),array('asignacionId'=>$tasign));
    }
    public function contrato_formato($id,$tipo,$equipo){
        $data['configuracionCotizacion'] = $this->ConfiguracionDocumentos_Model->getConfiguracion(1);
        $datosequiporow = $this->ModeloCatalogos->getselectwheren('asignacion_ser_contrato_a_e',array('asignacionIde'=>$equipo));
        foreach ($datosequiporow->result() as $item) {
            $idequipo = $item->idequipo;
            $serieId = $item->serieId;
        }
        $rowequipos = $this->ModeloCatalogos->getselectwheren('rentas_has_detallesEquipos',array('id'=>$idequipo));
        foreach ($rowequipos->result() as $item) {
            $modelo = $item->modelo;
        }
        $rowseries = $this->ModeloCatalogos->getselectwheren('series_productos',array('serieId'=>$serieId));
        foreach ($rowseries->result() as $item) {
            $serie = $item->serie;
        }
        $datos = $this->ModeloCatalogos->doc_contrato($id);
        //$te_cel = $this->ModeloCatalogos->doc_cliente_tel_cel($datos->id);
        $te_cel = $this->ModeloCatalogos->doc_cliente_tel_cel2($datos->id);
        $doc_ren = $this->ModeloCatalogos->doc_rectas($datos->contratoId);
        $get_contrato=$this->ModeloCatalogos->contrato_equipo($datos->contratoId,$idequipo,$serieId);
        $text_prioridad='';
        if ($datos->prioridad==1){
            $text_prioridad='Mismo día';
        }else if($datos->prioridad==2){
            $text_prioridad='Día siguiente';
        }else if($datos->prioridad==3){
            $text_prioridad='15 días';
        }else if($datos->prioridad==4){
            $text_prioridad='Servicio Regular';
        }else{
            $text_prioridad='';
        }
        $getdire = '';
        $porciones = explode("_", $get_contrato->direccion);
        if($porciones[0]=='g'){
            $getdire = $this->ModeloCatalogos->clientes_direccion($porciones[1]);
        }else if($porciones[0]=='f'){
            $getdire = $this->ModeloCatalogos->clientes_direcc_fiscal($porciones[1]);
        }
        $get_reg = explode(" ", $datos->reg);
        if (isset($te_cel->telefono)) {
            $tel_local=$te_cel->telefono;
        }else{
            $tel_local='';
        }
        if (isset($te_cel->celular)) {
            $celular=$te_cel->celular;
        }else{
            $celular='';
        }
         if (isset($te_cel->email)) {
            $email=$te_cel->email;
        }else{
            $email='';
        }
        if (isset($doc_ren->servicio_equipo)) {
            $servicio_equipo=$doc_ren->servicio_equipo;
        }else{
            $servicio_equipo='';
        }
        if (isset($doc_ren->servicio_doc)) {
            $servicio_doc=$doc_ren->servicio_doc;
        }else{
            $servicio_doc='';
        }
        //==============================================================================
            $lectu_ini='';
            $lectur_fin='';
            $sqlr="SELECT ard.c_c_i,ard.c_c_f 
                    FROM alta_rentas_prefactura_d as ard
                    inner JOIN alta_rentas_prefactura as ar on ar.prefId=ard.prefId
                    WHERE ard.productoId=$idequipo and '".$datos->fecha."' BETWEEN ar.periodo_inicial AND ar.periodo_final";
            $query = $this->db->query($sqlr);
            foreach ($query->result() as $item) {
                $lectu_ini=$item->c_c_i;
                $lectur_fin=$item->c_c_f;
            }

        //==============================================================================
        $data['idpdf']=$id;
        $data['tipopdf']=$tipo;
        $data['equipopdf']=$equipo;

        $data['fecha_crea']=$get_reg[0];
        $data['fecha_asig']=$datos->fecha;
        $data['tipo_serv']=$datos->servicio;
        $data['responsable_asig']=$datos->tecnico;
        $data['hora_asig']=$datos->hora;
        $data['prioridad']=$text_prioridad;
        $data['descripcion_serv_fall']=$datos->tserviciomotivo;
        $data['modelo']=$modelo;
        $data['serie']=$serie;
        $data['nombre_empresa']=$datos->empresa;
        $data['direcc_inst_serv']=$getdire->direccion;
        $data['tel']=$tel_local;
        $data['cel']=$celular;
        $data['correo']=$email;
        $data['docum_acce']=$servicio_equipo.' '.$servicio_doc;
        $data['observa_edi']='';
        $data['lectu_ini']=$lectu_ini;
        $data['km_veh_i']='';
        $data['lectur_fin']=$lectur_fin;
        $data['km_veh_f']='';
        $data['asesor_ser']='';
        $data['gerencia']='';
        $data['firma']=$datos->firma;
        $this->load->view('Reportes/contrato',$data);
    }    
    public function tipo_formato($id,$tipo){
        $data['configuracionCotizacion'] = $this->ConfiguracionDocumentos_Model->getConfiguracion(1);
        if($tipo==2){//Poliza
            $datos = $this->ModeloCatalogos->doc_poliza($id);
            $doc_polizas_cr = $this->ModeloCatalogos->doc_polizas_c($datos->polizaId);
            //$te_cel = $this->ModeloCatalogos->doc_cliente_tel_cel($datos->id);
            $te_cel = $this->ModeloCatalogos->doc_cliente_tel_cel2($datos->id);
            $text_prioridad='';
            if ($datos->prioridad==1){
                $text_prioridad='Mismo día';
            }else if($datos->prioridad==2){
                $text_prioridad='Día siguiente';
            }else if($datos->prioridad==3){
                $text_prioridad='15 días';
            }else if($datos->prioridad==4){
                $text_prioridad='Servicio Regular';
            }else{
                $text_prioridad='';
            }
            if (isset($te_cel->telefono)) {
                $tel_local=$te_cel->telefono;
            }else{
                $tel_local='';
            }
            if (isset($te_cel->celular)) {
                $celular=$te_cel->celular;
            }else{
                $celular='';
            }
             if (isset($te_cel->email)) {
                $email=$te_cel->email;
            }else{
                $email='';
            }
            
            $get_reg = explode(" ", $datos->reg);
            $data['fecha_crea']=$get_reg[0];
            $data['fecha_asig']=$datos->fecha;
            $data['tipo_serv']=$datos->servicio;
            $data['responsable_asig']=$datos->tecnico;
            $data['hora_asig']=$datos->hora;
            $data['prioridad']=$text_prioridad;
            $data['descripcion_serv_fall']=$datos->tserviciomotivo;
            $data['modelo']=$datos->modelo;
            $data['serie']=$datos->serie;
            $data['nombre_empresa']=$datos->empresa;
            $data['direcc_inst_serv']=$doc_polizas_cr->direccionservicio;
            $data['tel']=$tel_local;
            $data['cel']=$celular;
            $data['correo']=$email;
            $data['docum_acce']='';
            $data['observa_edi']='';
            $data['lectu_ini']='';
            $data['km_veh_i']='';
            $data['lectur_fin']='';
            $data['km_veh_f']='';
            $data['asesor_ser']='';
            $data['gerencia']='';
            $data['firma']=$datos->firma;
        }else{
            $datos = $this->ModeloCatalogos->doc_servicio($id);
            $te_cel = $this->ModeloCatalogos->doc_cliente_tel_cel($datos->id);
            $text_prioridad='';
            if ($datos->prioridad==1){
                $text_prioridad='Mismo día';
            }else if($datos->prioridad==1){
                $text_prioridad='Día siguiente';
            }else if($datos->prioridad==1){
                $text_prioridad='15 días';
            }else if($datos->prioridad==1){
                $text_prioridad='Servicio Regular';
            }else{
                $text_prioridad='';
            }
            $get_reg = explode(" ", $datos->reg);
            $data['fecha_crea']=$get_reg[0];
            $data['fecha_asig']=$datos->fecha;
            $data['tipo_serv']=$datos->servicio;
            $data['responsable_asig']=$datos->tecnico;
            $data['hora_asig']=$datos->hora;
            $data['prioridad']=$text_prioridad;
            $data['descripcion_serv_fall']=$datos->tserviciomotivo;
            $data['modelo']='';
            $data['serie']='';
            $data['nombre_empresa']=$datos->empresa;
            $data['direcc_inst_serv']='';
            $data['tel']=$te_cel->tel_local;
            $data['cel']=$te_cel->celular;
            $data['correo']=$datos->email;
            $data['docum_acce']='';
            $data['observa_edi']='';
            $data['lectu_ini']='';
            $data['km_veh_i']='';
            $data['lectur_fin']='';
            $data['km_veh_f']='';
            $data['asesor_ser']='';
            $data['gerencia']='';
            $data['firma']=$datos->firma;
        }
        
        $this->load->view('Reportes/porevento',$data); 
    } 
    public function tipo_formatoc($id,$tipo){
        $data['configuracionCotizacion'] = $this->ConfiguracionDocumentos_Model->getConfiguracion(1);
        $data['datosdeta'] = $this->ModeloCatalogos->descripcionpolizadetalle($id);
        if($tipo==2){//Poliza
            $datos = $this->ModeloCatalogos->doc_poliza($id);

            $doc_polizas_cr = $this->ModeloCatalogos->doc_polizas_c($datos->polizaId);
            $te_cel = $this->ModeloCatalogos->doc_cliente_tel_cel($datos->id);
            $text_prioridad='';
            if ($datos->prioridad==1){
                $text_prioridad='Mismo día';
            }else if($datos->prioridad==2){
                $text_prioridad='Día siguiente';
            }else if($datos->prioridad==3){
                $text_prioridad='15 días';
            }else if($datos->prioridad==4){
                $text_prioridad='Servicio Regular';
            }else{
                $text_prioridad='';
            }
            if (isset($te_cel->tel_local)) {
                $tel_local=$te_cel->tel_local;
            }else{
                $tel_local='';
            }
            if (isset($te_cel->celular)) {
                $celular=$te_cel->celular;
            }else{
                $celular='';
            }
            
            $get_reg = explode(" ", $datos->reg);
            $data['fecha_crea']=$get_reg[0];
            $data['fecha_asig']=$datos->fecha;
            $data['tipo_serv']=$datos->servicio;
            $data['responsable_asig']=$datos->tecnico;
            $data['hora_asig']=$datos->hora;
            $data['prioridad']=$text_prioridad;
            $data['descripcion_serv_fall']=$datos->tserviciomotivo;
            $data['modelo']=$datos->modelo;
            $data['serie']=$datos->serie;
            $data['nombre_empresa']=$datos->empresa;
            $data['direcc_inst_serv']=$doc_polizas_cr->direccionservicio;
            $data['tel']=$tel_local;
            $data['cel']=$celular;
            $data['correo']=$datos->email;
            $data['docum_acce']='';
            $data['observa_edi']='';
            $data['lectu_ini']='';
            $data['km_veh_i']='';
            $data['lectur_fin']='';
            $data['km_veh_f']='';
            $data['asesor_ser']='';
            $data['gerencia']='';
            $data['firma']=$datos->firma;
        }else{
            $datos = $this->ModeloCatalogos->doc_servicio($id);
            $te_cel = $this->ModeloCatalogos->doc_cliente_tel_cel2($datos->id);
            $text_prioridad='';
            if ($datos->prioridad==1){
                $text_prioridad='Mismo día';
            }else if($datos->prioridad==1){
                $text_prioridad='Día siguiente';
            }else if($datos->prioridad==1){
                $text_prioridad='15 días';
            }else if($datos->prioridad==1){
                $text_prioridad='Servicio Regular';
            }else{
                $text_prioridad='';
            }
            $get_reg = explode(" ", $datos->reg);
            $data['fecha_crea']=$get_reg[0];
            $data['fecha_asig']=$datos->fecha;
            $data['tipo_serv']=$datos->servicio;
            $data['responsable_asig']=$datos->tecnico;
            $data['hora_asig']=$datos->hora;
            $data['prioridad']=$text_prioridad;
            $data['descripcion_serv_fall']=$datos->tserviciomotivo;
            $data['modelo']='';
            $data['serie']='';
            $data['nombre_empresa']=$datos->empresa;
            $data['direcc_inst_serv']='';
            if (isset($te_cel->telefono)) {
                $telefono=$te_cel->telefono;
            }else{
                $telefono='';
            }
            if (isset($te_cel->celular)) {
                $celular=$te_cel->celular;
            }else{
                $celular='';
            }
            if (isset($te_cel->email)) {
                $email=$te_cel->email;
            }else{
                $email='';
            }


            $data['tel']=$telefono;
            $data['cel']=$celular;
            //$data['tel']='';//componer
            //$data['cel']='';//componer
            $data['correo']=$email;
            $data['docum_acce']=$datos->documentaccesorio;
            $data['observa_edi']='';
            $data['lectu_ini']='';
            $data['km_veh_i']='';
            $data['lectur_fin']='';
            $data['km_veh_f']='';
            $data['asesor_ser']=$datos->tecnico;
            $data['gerencia']='';
            $data['firma']=$datos->firma;
        }
        
        $this->load->view('Reportes/poreventoc',$data); 
    } 
    */   
}
?>
