$(document).ready(function(){
    
    var base_url = $('#base_url').val();

    var formulario_descuentos = $('#descuentos-form');

    formulario_descuentos.validate({
            rules: 
            {
                porcentaje_descuentos_equipos: {
                    required: true
                },
                porcentaje_descuentos_refacciones: {
                    required: true
                },
                porcentaje_descuentos_consumibles: {
                    required: true
                },
                porcentaje_descuentos_accesorios: {
                    required: true
                }
            },
            // Para mensajes personalizados
            messages: 
            {
                porcentaje_descuentos_equipos:{
                    required: "Ingrese un porcentaje de descuento para Equipos"
                },
                porcentaje_descuentos_refacciones: {
                    required: "Ingrese un porcentaje de descuento para Refacciones"
                },
                porcentaje_descuentos_consumibles: {
                    required: "Ingrese un porcentaje de descuento para Consumibles"
                },
                porcentaje_descuentos_accesorios: {
                    required: "Ingrese un porcentaje de descuento para Accesorios"
                }
            },
        errorElement : 'div',
            // Si están correctos los campos, procesamos el formulario mediante Ajax
        submitHandler: function (form) 
        {
            var datos = formulario_descuentos.serialize();
            
            $.ajax({
                type:'POST',
                url: base_url+'index.php/Descuentos/actualizarPorcentajesDescuento',
                data: datos,
                success:function(data)
                {
                    swal({ title: "Éxito",
                            text: "Se guardaron los datos de manera correcta",
                            type: 'success',
                            showCancelButton: false,
                            allowOutsideClick: false,
                        });
                        
                        setTimeout(function()
                        {
                            location.reload();
                        },1000);
                        
                }
            }); 
        }
    
    });
    

});