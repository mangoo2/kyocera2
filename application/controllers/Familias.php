<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Familias extends CI_Controller 
{
    public function __construct() 
    {
        parent::__construct();
        $this->load->model('Configuraciones_model', 'model');
        $this->load->model('login_model');
        $this->load->helper('url');
        $this->load->library('menus');
        $this->load->model('ModeloCatalogos');
        $this->submenu=23;
        if($this->session->userdata('logeado')==true){
            $this->perfilid =$this->session->userdata('perfilid');
            $this->idpersonal = $this->session->userdata('idpersonal');
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,$this->submenu);// 23 es el id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
        }else{
            redirect('login');
        }
    }
    
    function index()     {
        $data['MenusubId']=$this->submenu;
            // Obtiene los menús disponibles del sistema dependiendo el perfil, ayudado de la libreria "MENUS"
            $menus = $this->menus->getMenus($this->session->userdata('perfilid'));
            $data['menus'] = $menus['menus'];
            $data['submenusArray'] = $menus['submenusArray'];
            $familia = $this->model->getDataFamilia();
            $data['familia'] = $familia;
            $this->load->view('header');
            $this->load->view('main',$data);
            $this->load->view('configuraciones/familia/familia');
            $this->load->view('configuraciones/familia/jsfamilia');
            $this->load->view('footer');
    }
    public function insertarFamilia()
    {   $result = 0; 
        $data = $this->input->post();
               $datosFamilia = array('nombre' => $data["nombre"],);
               $result = $this->model->insertarFamilia($datosFamilia);
        echo $result;  
        $this->Modelobitacoras->Insert(array('contenido'=>'Se inserto un familia','nombretabla'=>'familia','idtable'=>$result,'tipo'=>'Insert','personalId'=>$this->idpersonal));
    }
    public function editar_familia()
    {   $id = $this->input->post('id');
        $nombre = $this->input->post('nombre');
        $result = $this->model->editar_familia($id,$nombre);
        echo $result; 
        $this->Modelobitacoras->Insert(array('contenido'=>'Se Modifico familia '.$nombre,'nombretabla'=>'familia','idtable'=>$id,'tipo'=>'update','personalId'=>$this->idpersonal)); 
    } 

}