<style type="text/css">
  table td{
    font-size: 12px;
    padding: 6px 7px;
  }
  .chosen-container{
    margin-left: 0px;
  }
  td{
    font-size: 12px !important;
  }
</style>
<!-- START CONTENT -->
    <section id="content">
      <!--start container-->
        <div class="container">
          <div class="row">
            <div class="col s10 m6 l6">
              <h5 class="breadcrumbs-title">Listado Devoluciones</h5>
            </div>
          </div>
        </div>
        <div class="container">
          <div class="section">
            <div class="card-panel">
            <input id="base_url" type="hidden" name="base_url" value="<?php echo base_url(); ?>">
            <div id="table-datatables">    
                  <!--
                  <div class="col s2">  
                    <button class="btn waves-effect waves-light gradient-45deg-green-teal gradient-shadow" >Facturar</button>
                  </div>
                  -->
                  <div class="row">
                   <div class="col s2">
                     <label>Tipo producto</label>
                     <select class="browser-default form-control-bmz" id="tipovs" onchange="loadtable()">
                       <option class="0">Todos</option>
                       <option value="1">Consumibles</option>
                       <option value="2">Accesorios</option>
                       <option value="3">Equipo</option>
                       <option value="4">Refacciones</option>
                     </select>
                   </div>
                   <div class="col s6 m2  l2">
                        <label>Clientes</label>
                        <select id="cliente" class="browser-default" onchange="loadtable();">
                          <option value="0">Seleccione</option>
                        </select>
                      </div> 
                  </div>
                <div class="row">
                  <div class="col s12">
                      <table id="tabla_ventas" class="responsive-table display" cellspacing="0">
                        <thead>
                          <tr>
                            <th># venta</th>
                            <th></th>
                            <th></th>
                            <th>Cantidad</th>
                            <th>Producto</th>
                            <th></th>
                            <th>Personal</th>
                            <th>Motivo</th>
                            <th>Fecha</th>
                            <th></th>
                            
                          </tr>
                        </thead>
                        <tbody>
                        </tbody>
                      </table>
                    </div>
                  </div>
            </div>
          </div>
          </div>  
            

    </section>
