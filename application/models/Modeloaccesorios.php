<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class Modeloaccesorios extends CI_Model 
{
    public function __construct() 
    {
        parent::__construct();
    }
//==============Personal==========
    function getaccesorios($params){
        $columns = array( 
            0=>'ca.id',
            1=>'ca.equipo',
            2=>'ca.nombre',
            3=>'ca.no_parte',
            4=>'ca.stock',
            5=>'ca.costo',
            6=>'ca.observaciones',
            7=>'ca.paginaweb',
            8=>'ca.destacado'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('catalogo_accesorios ca');
        
        $where = array('ca.status'=>1);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    public function total_accesorios($params){
        $columns = array( 
            0=>'ca.id',
            1=>'ca.equipo',
            2=>'ca.nombre',
            3=>'ca.no_parte',
            4=>'ca.stock',
            5=>'ca.costo',
            6=>'ca.observaciones',
            7=>'ca.paginaweb',
            8=>'ca.destacado'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(*) as total');
        $this->db->from('catalogo_accesorios ca');
        
        $where = array('ca.status'=>1);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        //$this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        //$this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query->row()->total;
    }







}