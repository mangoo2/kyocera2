<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="icon" href="<?php echo base_url(); ?>app-assets/images/favicon/favicon_kyocera.png" sizes="32x32">
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<title>Orden de Servicio</title>
		<!-- jQuery Library -->
    	<script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/jquery-3.3.1.min.js"></script>
    	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <link href="<?php echo base_url(); ?>app-assets/vendors/sweetalert/dist/sweetalert.css" type="text/css" rel="stylesheet">

		<style type="text/css">
			.tdstitle{
				background: #c1bcbc;
				font-weight: bold;
				color: white;
			}
			html{
			        -webkit-print-color-adjust: exact;
			}
			input{
				
				padding: 0px !important;
				height: 28px !important;
			}
			select{
				
				padding: 0px !important;
				height: 28px !important;
				
				 font-size: 11px !important;
			}
			@media print{
				input{
					background: transparent;
					border: 0 !important;
					
				}
				.buttonenvio{
					display: none;
				}
				html{
			        -webkit-print-color-adjust: exact;
				}
				.tdstitle{
					background: #c1bcbc;
					font-weight: bold;
					color: white;
				}
				.buttoimprimir{
					display: none;
				}
				select{
					background: transparent;
					border: 0 !important;
					-moz-appearance: none;
					 -webkit-appearance: none;
					 appearance: none;
				}
				.form-control:disabled, .form-control[readonly] {
				    background-color: transparent;
				    opacity: 1;
				}
			}
		</style>
	</head>
	<body>
		<input type="hidden" id="base_url" name="base_url" value="<?php echo base_url(); ?>">
		<form method="post" id="form_orden">
			<input type="hidden" id="ventaId" name="ventaId" value="0">
			<table border="1" width="100%">
				<tr>
					<td>
						<img src="<?php echo base_url(); ?>app-assets/images/<?php echo $configuracionCotizacion->logo1; ?>">
					</td>
					<td>
						<?php echo $configuracionCotizacion->textoHeader; ?>
					</td>
					<td>
						<img class="" src="<?php echo base_url(); ?>app-assets/images/1024_kyocera_logo_mds.png" alt="materialize logo"  style="max-width:200px; width: 100%;">
					</td>
				</tr>
			</table>
			<table border="1" width="100%">
				<tr>
					<td class="tdstitle" style="width: 10%; text-align: center;">FECHA</td>
					<td style="width: 5%; text-align: center;"><?php echo date('d');?></td>
					<td style="width: 5%; text-align: center;"><?php echo date('m');?></td>
					<td style="width: 5%; text-align: center;"><?php echo date('Y');?></td>
					<td class="tdstitle" style="width: 5%; text-align: center;">EV</td>
					<td style="width: 10%;"></td>
					<td class="tdstitle" style="width: 10%; text-align: center;">PROIN</td>
					<td style="width: 10%;"></td>
					<td class="tdstitle" style="width: 10%; text-align: center;">OS</td>
					<td style="width: 10%;"></td>
					<td class="tdstitle" style="width: 10%; text-align: center;">CDF</td>
					<td style="width: 20%"></td>
				</tr>
			</table>
			<table border="1" width="100%">
				<tr>
					<td style="width: 5%;">Cia.</td>
					<td style="width: 80%;" colspan="5"><output id="razon_social"></output></td>
					<td style="width: 5%;">RFC</td>
					<td style="width: 10%;">
						<select class="form-control" id="rfc_id" name="rfc_id" onchange="selecrfcp()">>
						    <option value="">Seleccione</option>
						<?php foreach ($rfc_datos as $item) { ?>
								<option value="<?php echo $item->id; ?>" ><?php echo $item->rfc; ?></option>
						<?php } ?>
					    </select>	
					</td>
				</tr>
				<tr>
					<td style="width: 5%; font-size: 9px">VENCE</td>
					<td style="width: 12%;">
						<input type="date" id="vencimiento" name="vencimiento" class="form-control" value="<?php echo date('Y-m-d');?>" <?php echo date('Y-m-d');?> style="max-width: 138px;"></td>
					<td style="width: 10%; font-size: 11px; text-align: center;">METODO DE PAGO</td>
					<td style="width: 20%; font-size: 11px; text-align: center;">
					<select class="form-control" id="metodopagoId" name="metodopagoId" >
						<?php foreach ($metodopagorow->result() as $item) { ?>
								<option value="<?php echo $item->id; ?>"><?php echo $item->metodopago_text; ?></option>
						<?php } ?>
					</select>
					</td>
					<td style="width: 12%; font-size: 11px; text-align: center;">FORMA DE PAGO</td>
					<td style="width: 14%; font-size: 11px; text-align: center;">
						<select class="form-control" id="formapagoId" name="formapagoId">
						<?php foreach ($formapagorow->result() as $item) { ?>
								<option value="<?php echo $item->id; ?>" ><?php echo $item->formapago_text; ?></option>
						<?php } ?>
					</select>
					</td>
					<td style="width: 10%; font-size: 11px;">USO DE CFDI</td>
					<td style="width: 17%; font-size: 11px;">
						<select class="form-control" id="usocfdiId" name="usocfdiId">
							<?php foreach ($cfdirow->result() as $item) { ?>
									<option value="<?php echo $item->id; ?>"><?php echo $item->uso_cfdi_text; ?></option>
							<?php } ?>
						</select>
					</td>	
					
				</tr>
			</table>
			<table border="1" width="100%">
				<tr>
					<td rowspan="2" style="font-size: 11px; width: 15%;">DOMICILIO Y REFERENCIAS</td>
					<td rowspan="2" style="font-size: 11px; width: 60%;"><textarea id="domiciio" class="form-control"></textarea></td>
					<td style="font-size: 11px; width: 10%; text-align: center;">FORMA DE COBRO</td>
					<td style="font-size: 11px; width: 15%;">
						<input type="tex" id="formadecobro" name="formadecobro" class="form-control">
					</td>
				</tr>
				<tr>
					<td style="font-size: 11px; width: 10%; text-align: center;">email:</td>
					<td style="font-size: 11px; width: 15%;">
						<input type="tex" id="email" name="email" class="form-control">
					</td>
				</tr>
				<tr>
					<td style="font-size: 11px; width: 15%;">DOCUMENTOS ACCESO</td>
					<td style="font-size: 11px; width: 35%;"><input id="docs_acce" class="form-control"></td>
					<td style="font-size: 11px; width: 10%;">CONTACTO GRAL SERV.</td>
					<td style="font-size: 11px; width: 25%;"><select class="form-control" id="contactoId" name="contactoId">
						<?php foreach ($resultadoclipcontacto->result() as $item) { ?>
							<option value="<?php echo $item->id;?>" >"<?php echo $item->persona_contacto;?>"</option>		
					    <?php } ?>
						</select></td>
				</tr>
				<tr>
					<td rowspan="2" style="font-size: 11px; width: 15%;">INSTRUCCIÓN ESPECIAL AL TÉCNICO</td>
					<td rowspan="2" style="font-size: 11px; width: 60%;"><textarea id="instruccion" class="form-control"></textarea></td>
					<td style="font-size: 11px; width: 10%; text-align: center;">CONTACTO LOCAL EQ</td>
					<td style="font-size: 11px; width: 15%;">
						<select class="form-control" id="contactoId" name="contactoId">
						<?php foreach ($resultadoclipcontacto->result() as $item) { ?>
							<option value="<?php echo $item->id;?>" >"<?php echo $item->persona_contacto;?>"</option>		
					    <?php } ?>
						</select>
					</td>
				</tr>
				<tr>
					<td style="font-size: 11px; width: 10%; text-align: center;">CONTACTO COMPRAS:</td>
					<td style="font-size: 11px; width: 15%;">
						<select class="form-control" id="contactoId" name="contactoId">
						<?php foreach ($resultadoclipcontacto->result() as $item) { ?>
							<option value="<?php echo $item->id;?>" >"<?php echo $item->persona_contacto;?>"</option>		
					    <?php } ?>
						</select>
					</td>
				</tr>
			</table>
			<table border="1" width="100%">	
				<tr>
					<td colspan="1" style="font-size: 11px; width: 8%;">Póliza No.</td>
					<td colspan="1" style="font-size: 11px; width: 12%;"><input id="docs_acce" class="form-control"></input></td>
					<td align="center" style="font-size: 11px; width: 3%;">Tipo</td>
					<td style="font-size: 11px; width: 12%;"><input id="contacto_gral" class="form-control"></input></td>
					<td align="center" style="font-size: 11px; width: 10%;">Vence</td>
					<td style="font-size: 11px; width: 10%;"><input id="contacto_gral" class="form-control"></input></td>
					<td align="center" style="font-size: 11px; width: 3%;">OC</td>
					<td style="font-size: 11px; width: 10%;"><input id="contacto_gral" class="form-control"></input></td>
					<td style="font-size: 11px; width: 10%;">CONFIRMADO CON</td>
					<td style="font-size: 11px; width: 22%;"><select class="form-control" id="contactoId" name="contactoId">
						<?php foreach ($resultadoclipcontacto->result() as $item) { ?>
							<option value="<?php echo $item->id;?>" >"<?php echo $item->persona_contacto;?>"</option>		
					    <?php } ?>
						</select>
					</td>
				</tr>
				<tr>
					<td colspan="1" style="font-size: 11px; width: 8%;">Garantía de "O S"</td>
					<td  colspan="1" style="font-size: 11px; width: 12%;"><input id="docs_acce" class="form-control"></input></td>
					<td align="center" style="font-size: 11px; width: 3%;">Fecha</td>
					<td style="font-size: 11px; width: 12%;"><input id="contacto_gral" class="form-control"></input></td>
					<td align="center" style="font-size: 11px; width: 10%;">Contador</td>
					<td style="font-size: 11px; width: 10%;"><input id="contacto_gral" class="form-control"></input></td>
					<td align="center" style="font-size: 11px; width: 3%;">Asesor</td>
					<td style="font-size: 11px; width: 10%;"><input id="contacto_gral" class="form-control"></input></td>
					<td style="font-size: 11px; width: 10%;">Horario de atención:</td>
					<td style="font-size: 11px; width: 22%;"><input id="contacto_gral" class="form-control"></input></td>
				</tr>
			</table>
			<table border="1" width="100%">
				<thead>
				<tr>
					<th style="font-size: 11px;">Cantidad</th>
					<th style="font-size: 11px;">Surtir en</th>
					<th style="font-size: 11px;">No. de Parte</th>
					<th style="font-size: 11px;" colspan="3">Descripcion</th>
					<th style="font-size: 11px;">Precio Unitario</th>
					<th style="font-size: 11px;" >Total</th>
				</tr>
				</thead>
				<tbody>
					<?php 
						$totalgeneral=0;
                        // Tabla de polizas creadas detalles  		
                        if(isset($serviciodetalles)){
							foreach ($serviciodetalles->result() as $item) { 
							$totalc = 0;
							if($item->precio_local !==NULL) {
								$totalc=$item->precio_local;
							}
							if($item->precio_semi !==NULL) {
								$totalc=$item->precio_semi;
							}
							if($item->precio_foraneo !==NULL) {
								$totalc=$item->precio_foraneo;
							}
							$totalgeneral=$totalgeneral+$totalc;
							?>
							<tr>
								<td style="font-size: 11px; text-align: center;"><?php echo $item->cantidad;?></td>
								<td style="font-size: 11px; text-align: center;">OF</td>
								<td style="font-size: 11px; ">N/A</td>
								<td style="font-size: 11px; " colspan="3">
									<table border="1" width="100%">
										<tr>
											<td width="30%"></td>
											<td width="70%">Poliza</td>		
										</tr>
										<tr>
											<td>TIPO</td>
											<td>IMPRESORA</td>		
										</tr>
										<tr>
											<td>MODELO</td>
											<td><?php echo $item->modeloe;?></td>		
										</tr>
										<tr>
											<td>SERIE</td>
											<td><?php echo $item->serie;?></td>		
										</tr>
										<tr>
											<td></td>
											<td><?php echo $item->modelo;?></td>		
										</tr>
										<tr>
											<td>Cubre</td>
											<td><?php echo $item->cubre;?></td>		
										</tr>
										<tr>
											<td>Vigencia de meses</td>
											<td><?php echo $item->vigencia_meses;?></td>		
										</tr>
										<tr>
											<td>Vigencia de clicks</td>
											<td><?php echo $item->vigencia_clicks;?></td>		
										</tr>
										<?php if ($item->precio_local!=NULL) { ?>
											<tr>
												<td>Precio local</td>
												<td><?php echo number_format($item->precio_local,2,'.',',');?></td>		
											</tr>
										<?php	}?>
										<?php if ($item->precio_semi!=NULL) { ?>
											<tr>
												<td>Precio semi</td>
												<td><?php echo number_format($item->precio_semi,2,'.',',');?></td>		
											</tr>
										<?php	}?>
										<?php if ($item->precio_foraneo!=NULL) { ?>
											<tr>
												<td>Precio foraneo</td>
												<td><?php echo number_format($item->precio_foraneo,2,'.',',');?></td>		
											</tr>
										<?php	}?>
									</table>
								</td>
								<td style="font-size: 11px; text-align: center;"  >$<?php echo number_format($totalc,2,'.',',');?></td>
								<td style="font-size: 11px; text-align: center;" >$<?php echo number_format($totalc,2,'.',',');?></td>
							</tr>
						<?php	}
						}else{
							for ($i=0; $i <8 ; $i++) { 
								echo '<tr><td style="font-size: 11px; text-align: center;"></td>
								<td style="font-size: 11px; text-align: center;"></td>
								<td style="font-size: 11px; "></td>
								<td style="font-size: 11px; " colspan="3"></td>
								<td style="font-size: 11px; text-align: center;"></td>
								<td style="font-size: 11px; text-align: center;">$ -</td></tr>';
							}
						}
					?>	
				</tbody>
				<tfoot>
					<tr>
						<td style="font-size: 11px; " colspan="2">Subtotal</td>
						<td style="font-size: 11px; " colspan="2">$<?php echo number_format($totalgeneral,2,'.',',');?></td>
						<td style="font-size: 11px; ">Iva</td>
						<td style="font-size: 11px; " >$<?php echo number_format($totalgeneral*0.16,2,'.',',');?></td>
						<td style="font-size: 11px; ">Total</td>
						<td style="font-size: 11px; " >$<?php echo number_format(($totalgeneral*1.16),2,'.',',');?></td>
					</tr>
				</tfoot>
			</table>
		</form>
		<table border="1" width="100%">
			<tr>
				<td rowspan="10" style="font-size: 11px; width: 10%; text-align: center;">SERVICIO</td>
				<td rowspan="3" style="font-size: 11px; width: 10%; text-align: center;">EQUIPO</td>
				<td style="font-size: 11px; width: 10%; text-align: center;">PROD. PROM.</td>
				<td style="font-size: 11px; width: 10%;">
					<input type="tex" id="formadecobro" name="formadecobro" class="form-control">
				</td>
				<td style="font-size: 11px; width: 10%; text-align: center;">Modelo</td>
				<td style="font-size: 11px; width: 10%;">
					<input type="tex" id="formadecobro" name="formadecobro" class="form-control">
				</td>
				<td style="font-size: 11px; width: 10%; text-align: center;">Serie</td>
				<td style="font-size: 11px; width: 10%;">
					<input type="tex" id="formadecobro" name="formadecobro" class="form-control">
				</td>
				<td style="font-size: 11px; width: 10%; text-align: center;">tipo servicio</td>
				<td style="font-size: 11px; width: 10%;">
					<input type="tex" id="formadecobro" name="formadecobro" class="form-control">
				</td>
			</tr>
			<tr>
				<td rowspan="2" style="font-size: 11px; width: 10%; text-align: center;">descripción de la falla:</td>
				<td rowspan="2" colspan="4" style="font-size: 11px; width: 10%;">
					<textarea type="tex" id="email" name="email" class="form-control"></textarea>
				</td>
				<td style="font-size: 11px; width: 10%; text-align: center;">código</td>
				<td colspan="2" style="font-size: 11px; width: 10%;">
					<input type="tex" id="formadecobro" name="formadecobro" class="form-control">
				</td>
			</tr>
			<tr>
				<td style="font-size: 11px; width: 10%; text-align: center;">Lectura inicial</td>
				<td colspan="2" style="font-size: 11px; width: 10%;">
					<input type="tex" id="formadecobro" name="formadecobro" class="form-control">
				</td>
			</tr>
			<tr>
				<td rowspan="4" style="font-size: 11px; width: 10%; text-align: center; background-color: #D5DADB">Trabajo realizado (si/no):</td>
			</tr>
			<tr>
				<td style="font-size: 11px; width: 10%; text-align: center;">1-investigacion</td>
				<td style="font-size: 11px; width: 10%;">
					<input type="tex" id="formadecobro" name="formadecobro" class="form-control">
				</td>
				<td style="font-size: 11px; width: 10%; text-align: center;">2-revisión</td>
				<td style="font-size: 11px; width: 10%;">
					<input type="tex" id="formadecobro" name="formadecobro" class="form-control">
				</td>
				<td style="font-size: 11px; width: 10%; text-align: center;">3-muestra inicia</td>
				<td style="font-size: 11px; width: 10%;">
					<input type="tex" id="formadecobro" name="formadecobro" class="form-control">
				</td>
				<td style="font-size: 11px; width: 10%; text-align: center; background-color: #D5DADB">tiempo ocupado</td>
				<td style="font-size: 11px; width: 10%;">
					<input type="tex" id="formadecobro" name="formadecobro" class="form-control">
				</td>
			</tr>
			<tr>
				<td style="font-size: 11px; width: 10%; text-align: center;">4-limpieza</td>
				<td style="font-size: 11px; width: 10%;">
					<input type="tex" id="formadecobro" name="formadecobro" class="form-control">
				</td>
				<td style="font-size: 11px; width: 10%; text-align: center;">5-lubricación</td>
				<td style="font-size: 11px; width: 10%;">
					<input type="tex" id="formadecobro" name="formadecobro" class="form-control">
				</td>
				<td style="font-size: 11px; width: 10%; text-align: center;">6-ajustes</td>
				<td style="font-size: 11px; width: 10%;">
					<input type="tex" id="formadecobro" name="formadecobro" class="form-control">
				</td>
				<td style="font-size: 11px; width: 10%; text-align: center; background-color: #D5DADB">inicio</td>
				<td style="font-size: 11px; width: 10%;">
					<input type="tex" id="formadecobro" name="formadecobro" class="form-control">
				</td>
			</tr>
			<tr>
				<td style="font-size: 11px; width: 10%; text-align: center;">7-reemplazo</td>
				<td style="font-size: 11px; width: 10%;">
					<input type="tex" id="formadecobro" name="formadecobro" class="form-control">
				</td>
				<td style="font-size: 11px; width: 10%; text-align: center;">8-muestra final</td>
				<td style="font-size: 11px; width: 10%;">
					<input type="tex" id="formadecobro" name="formadecobro" class="form-control">
				</td>
				<td style="font-size: 11px; width: 10%; text-align: center;">9-entrega</td>
				<td style="font-size: 11px; width: 10%;">
					<input type="tex" id="formadecobro" name="formadecobro" class="form-control">
				</td>
				<td style="font-size: 11px; width: 10%; text-align: center; background-color: #D5DADB">fin</td>
				<td style="font-size: 11px; width: 10%;">
					<input type="tex" id="formadecobro" name="formadecobro" class="form-control">
				</td>
			</tr>
			<tr>
				<td rowspan="3" style="font-size: 11px; width: 10%; text-align: center; background-color: #D5DADB">Edo Final (excelente, bien,regular):</td>
			</tr>
			<tr>
				<td style="font-size: 11px; width: 10%; text-align: center;">corriente Volts</td>
				<td style="font-size: 11px; width: 10%;">
					<input type="tex" id="formadecobro" name="formadecobro" class="form-control">
				</td>
				<td style="font-size: 11px; width: 10%; text-align: center;">regulador</td>
				<td style="font-size: 11px; width: 10%;">
					<input type="tex" id="formadecobro" name="formadecobro" class="form-control">
				</td>
				<td style="font-size: 11px; width: 10%; text-align: center;">ubicación</td>
				<td style="font-size: 11px; width: 10%;">
					<input type="tex" id="formadecobro" name="formadecobro" class="form-control">
				</td>
				<td style="font-size: 11px; width: 10%; text-align: center; background-color: #D5DADB">hrs</td>
				<td style="font-size: 11px; width: 10%;">
					<input type="tex" id="formadecobro" name="formadecobro" class="form-control">
				</td>
			</tr>
			<tr>
				<td style="font-size: 11px; width: 10%; text-align: center;">operación</td>
				<td style="font-size: 11px; width: 10%;">
					<input type="tex" id="formadecobro" name="formadecobro" class="form-control">
				</td>
				<td style="font-size: 11px; width: 10%; text-align: center;">apariencia</td>
				<td style="font-size: 11px; width: 10%;">
					<input type="tex" id="formadecobro" name="formadecobro" class="form-control">
				</td>
				<td style="font-size: 11px; width: 10%; text-align: center;">calidad</td>
				<td style="font-size: 11px; width: 10%;">
					<input type="tex" id="formadecobro" name="formadecobro" class="form-control">
				</td>
				<td style="font-size: 11px; width: 10%; text-align: center; background-color: #D5DADB">lectura final</td>
				<td style="font-size: 11px; width: 10%;">
					<input type="tex" id="formadecobro" name="formadecobro" class="form-control">
				</td>
			</tr>
		</table>
		<table border="1" width="100%">
			<tr>
				<td rowspan="3" style="font-size: 11px; width: 10%; background-color: #D5DADB">Observaciones/recomendaciones</td>
				<td style="font-size: 11px; width: 60%;">
					<input type="tex" id="formadecobro" name="formadecobro" class="form-control">
				</td>
				<td colspan="3" style="font-size: 11px; width: 10%; text-align: center; background-color: #D5DADB">fecha de realizado</td>
				<td colspan="3" style="font-size: 11px; width: 20%;">
					<input type="tex" id="formadecobro" name="formadecobro" class="form-control">
				</td>
			</tr>
			<tr>
				<td style="font-size: 11px; width: 60%;">
					<input type="tex" id="formadecobro" name="formadecobro" class="form-control">
				</td>
				<td style="font-size: 11px; width: 5%; text-align: center; background-color: #D5DADB">d</td>
				<td style="font-size: 11px; width: 5%; text-align: center; background-color: #D5DADB">m</td>
				<td style="font-size: 11px; width: 5%; text-align: center; background-color: #D5DADB">a</td>
				<td style="font-size: 11px; width: 5%; text-align: center; background-color: #D5DADB">Kilometraje</td>
			</tr>	
			<tr>
				<td style="font-size: 11px; width: 60%;">
					<input type="tex" id="formadecobro" name="formadecobro" class="form-control">
				</td>
				<td style="font-size: 11px; width: 5%; text-align: center;">
					<input type="tex" id="formadecobro" name="formadecobro" class="form-control">
				</td>
				<td style="font-size: 11px; width: 5%; text-align: center;">
					<input type="tex" id="formadecobro" name="formadecobro" class="form-control">
				</td>
				<td style="font-size: 11px; width: 5%; text-align: center;">
					<input type="tex" id="formadecobro" name="formadecobro" class="form-control">
				</td>
				<td style="font-size: 11px; width: 5%; text-align: center;">
					<input type="tex" id="formadecobro" name="formadecobro" class="form-control">
				</td>
			</tr>	
		</table>
		<table border="1" width="100%">
			<tr>
				<td style="font-size: 11px; width: 35%; text-align: center;">Asesor de Servicio
				</td>
				<td  style="font-size: 11px; width: 30%; text-align: center;">
					Gerencia de Operaciones
				</td>
				<td  style="font-size: 11px; width: 35%; text-align: center;">
					 Nombre y firma de quien aprueba el servicio realizado
				</td>
			</tr>
			<tr>
				<td style="font-size: 11px; width: 35%; text-align: center;">
					<input type="tex" id="formadecobro" name="formadecobro" class="form-control">
				</td>
				<td style="font-size: 11px; width: 30%; text-align: center;">
					<input type="tex" id="formadecobro" name="formadecobro" class="form-control">
				</td>
				<td style="font-size: 11px; width: 35%; text-align: center;">
					<input type="tex" id="formadecobro" name="formadecobro" class="form-control">
				</td>
			</tr>

			<tr>
				<td rowspan="2" style="font-size: 11px; width: 35%; text-align: center;">
				</td>
				<td rowspan="2" style="font-size: 11px; width: 30%; text-align: center;">

				</td>
				<td  rowspan="2" style="font-size: 11px; width: 35%; text-align: center;">
					Firma de confirmidad sobre el servicio recibido
				</td>
			</tr>
		</table>

		<div class="row">
		    <div class="col-md-12">
		        <div class="col-md-12 text-right">
		        	<?php 
		        		if($block_button==0){ ?>
		        			<button type="button" class="btn btn-success buttonenvio">Enviar</button>
		        	<?php  }else{  	?>	
		        		<button type="button" class="btn btn-success buttoimprimir">Imprimir</button>
		        	<?php  }  	?>	
		        </div>
		    </div>
		</div>
		<div class="modal fade" id="modalconfirmacionenvio" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h5 class="modal-title" id="exampleModalLabel">Enviar</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      <div class="modal-body">
		        <div class="row">
		        	<div class="col-md-12">
		        		<h4>¿Desea enviar la prefactura?</h4>
		        	</div>
		        </div>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
		        <button type="button" class="btn btn-primary buttonenvioconfirm">Enviar</button>
		      </div>
		    </div>
		  </div>
		</div>
		
	</body>
	<script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/sweetalert/dist/sweetalert.min.js"></script>
	<script type="text/javascript">
		var base_url = $('#base_url').val();
		$(document).ready(function(){ 
			$('.buttonenvio').click(function(event) {
				$('#modalconfirmacionenvio').modal();
			});
			$('.buttonenvioconfirm').click(function(event) {
				var datos = $('#form_orden').serialize();
				$.ajax({
                    type:'POST',
                    url: base_url+'Ordenes_asignadas/save',
                    data: datos,
                    async: false,
                    statusCode:{
                            404: function(data){
                                swal("Error", "404", "error");
                            },
                            500: function(){
                                swal("Error", "500", "error"); 
                            }
                        },
                        success:function(data){
                            swal("Éxito", "Orden de servicio creada", "success");
                            setTimeout(function(){ window.location.href='Ordenes_asignadas'; }, 3000);
                         

                        }
                    });
			});
			$('.buttoimprimir').click(function(event) {
				window.print();
			});
			<?php if ($rfc_id>0) { ?>
				selectrcfv(<?php echo $rfc_id;?>);
			<?php } ?>

		});
		function selecrfcp() {
			var rfc_id = $('#rfc_id option:selected').val();
			selectrcfv(rfc_id);
			
		}
		function selectrcfv(rfc_id){
			$.ajax({
                url: base_url+'Prefactura/selecciontrcf',
                dataType: 'json',
                data:{id:rfc_id},
                statusCode:{
                        404: function(data){
                            swal("Error", "404", "error");
                        },
                        500: function(){
                            swal("Error", "500", "error"); 
                        }
                    },
                    success:function(data){
                    	var datos=data;
                        datos.forEach(function(r) {
                        $('#razon_social').val(r.razon_social);
                        $('#num_ext').val(r.num_ext);
                        $('#colonia').val(r.colonia);
                        $('#calle').val(r.calle);
                        });
                    }
            });
		}
	</script>
</html>
