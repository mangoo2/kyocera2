<?php 
    require_once('TCPDF3/examples/tcpdf_include.php');
    require_once('TCPDF3/tcpdf.php');
    $this->load->helper('url');
    $htmlf='';
    $GLOBALS['facturasvencidas']=0;
    $GLOBALS['montovencido']=0;
    $GLOBALS['ultimafactura']=0;
    //=======================================================================================
    //$facturas = $this->ModeloCatalogos->viewprefacturaslis_general($idc);
    $datoscontrato = $this->Rentas_model->getdatosdeestadodecuenta($idc);
    //$GLOBALS['folio']='';
    $GLOBALS['empresa']='';
    $GLOBALS['Direccion']='';
    foreach ($datoscontrato->result() as $item) {
        //$GLOBALS['folio']=$item->folio;
        $GLOBALS['empresa']=$item->empresa;
        $GLOBALS['Direccion']=$item->Direccion;
        $GLOBALS['credito']=$item->credito;
    }
    $array_contratos=array();
    if($cliente==0){
        $array_contratos[]=array('con'=>$idc,'fol'=>'');
    }else{
        $strq="SELECT con.* FROM rentas as ren INNER JOIN contrato as con on con.idRenta=ren.id WHERE con.estatus>0 AND ren.idCliente='$cliente' ";
        $query = $this->db->query($strq);
        foreach ($query->result() as $itemc) {
            //log_message('error','idcontrato: '.$itemc->idcontrato);
            $array_contratos[]=array('con'=>$itemc->idcontrato,'fol'=>$itemc->folio);
        }
    }
    //log_message('error',json_encode($array_contratos));
    
    foreach ($array_contratos as $itemc) {
        $idcontrato = $itemc['con'];
        
        $facturas = $this->ModeloCatalogos->viewprefacturaslis_general($idcontrato);
        //==========================================================================
            
            foreach ($facturas->result() as $item) {
                if ($item->pagot>0) {
                    # code...
                }else{
                    $GLOBALS['montovencido']=$GLOBALS['montovencido']+$item->total;
                    //$GLOBALS['facturasvencidas']=$GLOBALS['facturasvencidas']+1; 
                }
                $GLOBALS['ultimafactura']=$item->fechatimbre;
            }
            

            $montovencido=0;
            foreach ($facturas->result() as $item) {
                $descripcion=$this->ModeloCatalogos->descripcionfactura($item->FacturasId);
                if ($item->fechamax==null) {
                    $fechapago='Pendiente';
                }else{
                    $fechapago=$item->fechamax;
                }
                if( floatval($item->pagot)>0){
                    $pagot='$ '.number_format($item->pagot,2,'.',',');
                    $pagot_num=round($item->pagot,2);
                }else{
                    $pagot='';
                    
                    $pagot_num=0;
                }
                $totalf=$item->total-$pagot_num;

                if($totalf>0){
                    $infocomplemento=$this->ModeloCatalogos->infocomplementofactura($item->FacturasId);
                    $pagot_num=$infocomplemento[0];
                    if($infocomplemento[0]>0){
                        $fechapago=date('Y-m-d',strtotime($infocomplemento[1]));
                        $pagot='$ '.number_format($pagot_num,2,'.',',');
                        $totalf=$item->total-$pagot_num;
                    }    
                }
                
                
                $montovencido=$montovencido+$totalf;
                if($totalf>0){
                    $GLOBALS['facturasvencidas']=$GLOBALS['facturasvencidas']+1;
                }
                
            }


            
        //==========================================================================
    }
    
            
            









class MYPDF extends TCPDF {
  //Page header
  public function Header() {
      //$img_header = 'header.jpg';
      //$this->Image($img_header, 0, 0, 0, 197, '', '', '', false, 100, '', false, false, 0);
      //$this->Image($img_header, 0, 0, 210, '', 'JPG', '', false, false, 300, '', false, false, 0, false, false, false);
      $imglogo = base_url().'public/img/ops.png';
      $imglogo0 = base_url().'public/img/MDS.png';
        if($GLOBALS['facturasvencidas']>0){
            $statusgeneral='FACTURAS PENDIENTES DE PAGO';
        }else{
            $statusgeneral='VIGENTE';
        }
      
      
          
$html = '<style type="text/css"> .titulotex{font-size:11px;} .titulotex_grey{color:grey;} .titulotex_red{color:red;} .boderb{border-bottom:1px solid black;} </style>
<table border="0" align="center"><tr><td width="25%"><img src="'.$imglogo.'"></td><td width="50%">Impresoras y Multifuncionales, Venta, Renta, Mantenimiento<br>Blvd. Norte 1831 V. San Alejandro Puebla, Pue.<br>Call. Center 273 3400, 249 5393<br>www.Kyoceraap.com</td><td width="25%"><img src="'.$imglogo0.'"></td></tr></table>
<table align="center" cellpadding="5"><tr><td colspan="2" class="boderb">ESTADO DE CUENTA AL '.date('d/m/Y').'</td></tr><tr><td class="boderb">Estatus General:<b>'.$statusgeneral.'</b></td><td class="boderb">Condiciones de Crédito: '.$GLOBALS['credito'].' días</td></tr><tr><td colspan="2" class="tableb">'.$GLOBALS['empresa'].'</td></tr></table>';
          /*
        $html0 = '<table width="100%" border="0" >
            <tr>
                <td width="19%" class="titulotex titulotex_grey">FACTURAS VENCIDAS</td>
                <td width="17%" class="titulotex titulotex_red">'.$GLOBALS['facturasvencidas'].'</td>
                <td width="28%" align="center" rowspan="3"></td>
                <td width="19%"  class="titulotex titulotex_grey">FECHA DE ULTIMA FACTURA</td>
                <td width="17%" class="titulotex titulotex_red">'.$GLOBALS['ultimafactura'].'</td>
            </tr>
            <tr>
                <td class="titulotex titulotex_grey">MONTO VENCIDO</td>
                <td class="titulotex titulotex_red">'.$GLOBALS['montovencido'].'</td>
                <td class="titulotex titulotex_grey">FECHA DE VENCIMIENTO</td>
                <td class="titulotex titulotex_red"></td>
            </tr>
            <tr>
                <td class="titulotex titulotex_grey">NUMERO DE EQUIPOS</td>
                <td class="titulotex titulotex_red"></td>
                <td class="titulotex"></td>
                <td class="titulotex titulotex_red"></td>
            </tr>
        </table>
          <table width="100%" border="0" >
            <tr>
                <td align="center" style="color:#0078d4; font-weight:bold;">
                    
                </td>
            </tr>
            <tr>
                <td align="center" style="color:#0078d4; font-weight:bold; font-size:14px;">
                    '.$GLOBALS['Direccion'].'
                </td>
            </tr>
            <tr>
                <td align="center" >
                    CONTRATO '.$GLOBALS['folio'].'
                </td>
            </tr>
          </table>
          ';
          */
        $this->writeHTML($html, true, false, true, false, '');
  }
    // Page footer
  public function Footer() {
      $html = '<table width="100%" border="0"><tr><td align="right" class="footerpage">Pagina '.$this->getAliasNumPage().' de '.$this->getAliasNbPages().'</td></tr></table>';
      $this->writeHTML($html, true, false, true, false, '');
  }
} 
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Kyocera');
$pdf->SetTitle('Estado Cuenta');
$pdf->SetSubject('Estado Cuenta');
$pdf->SetKeywords('Estado Cuenta');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins('10', '50', '10');
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
$pdf->SetFont('dejavusans', '', 9);
// add a page



//======================================================
foreach ($array_contratos as $itemc) {
    
    $htmlf='';
        $idcontrato = $itemc['con'];
        $fol = $itemc['fol'];
        $facturas = $this->ModeloCatalogos->viewprefacturaslis_general($idcontrato);
        //==========================================================================
            $GLOBALS['facturasvencidas']=0;
            $GLOBALS['montovencido']=0;
            $GLOBALS['ultimafactura']=0;
            foreach ($facturas->result() as $item) {
                if ($item->pagot>0) {
                    # code...
                }else{
                    $GLOBALS['montovencido']=$GLOBALS['montovencido']+$item->total;
                    //$GLOBALS['facturasvencidas']=$GLOBALS['facturasvencidas']+1; 
                }
                $GLOBALS['ultimafactura']=$item->fechatimbre;
            }
            
$htmlf .= '<style type="text/css"> .stileback{background-color:grey;color: white;font-weight: bold;} .stileback2{background-color:#e0e0e0;font-weight: bold;} .bodertable{border: #0078d4 5px solid;} .table td{font-size:10px;} </style>';
            if($cliente>0){
                $htmlf .='<table><tr><td><b>Contrato:</b> '.$fol.'</td></tr></table>';
            }
        $htmlf .= '<table cellpadding="5" border="1" class="table bodertable" width="100%" align="center"><thead><tr><td width="9%" class="stileback bodertable">Folio Factura</td><td width="9%" class="stileback">Fecha Factura</td><td width="40%" class="stileback">Concepto</td><td width="10%" class="stileback">Aplicación de pago</td><td width="10%" class="stileback">Monto Factura</td><td width="11%" class="stileback">Monto Pagado</td><td width="11%" class="stileback">Saldo</td></tr></thead>';
            $montovencido=0;
            $row=0;
            foreach ($facturas->result() as $item) {
                $totalpagado=0;
                $descripcion=$this->ModeloCatalogos->descripcionfactura($item->FacturasId);
                if ($item->fechamax==null) {
                    $fechapago='Pendiente';
                }else{
                    $fechapago=$item->fechamax;
                }
                if( floatval($item->pagot)>0){
                    $pagot_pm=$item->pagot;//pago manual
                    $pagot='$ '.number_format($item->pagot,2,'.',',');
                    $pagot_num=round($item->pagot,2);
                }else{
                    $pagot='';
                    $pagot_pm=0;
                    $pagot_num=0;
                }
                $totalf=$item->total-$pagot_num;

                if($totalf>0){
                    $infocomplemento=$this->ModeloCatalogos->infocomplementofactura($item->FacturasId);
                    $pagot_num=$infocomplemento[0];
                    if($infocomplemento[0]>0){
                        $fechapago=date('Y-m-d',strtotime($infocomplemento[1]));
                        $pagot='$ '.number_format($pagot_num+$pagot_pm,2,'.',',');
                        $totalf=$item->total-$pagot_num-$pagot_pm;
                    }    
                }
                
                
                $montovencido=$montovencido+$totalf;
                if($totalf>0){
                    $GLOBALS['facturasvencidas']=$GLOBALS['facturasvencidas']+1;
                }
                if($row==0){
                    $td1=' width="9%" ';
                    $td2=' width="9%" ';
                    $td3=' width="40%" ';
                    $td4=' width="10%" ';
                    $td5=' width="10%" ';
                    $td6=' width="11%" ';
                    $td7=' width="11%" ';
                }else{
                    $td1='';$td2='';$td3='';$td4='';$td5='';$td6='';$td7='';
                }
                $htmlf .= '<tr nobr="true"><td'.$td1.'>U'.$item->Folio.'</td><td'.$td2.'>'.$item->fechatimbre.'</td><td'.$td3.'>'.$descripcion.'</td><td'.$td4.'>'.$fechapago.'</td><td'.$td5.'>$ '.number_format($item->total,2,'.',',').'</td><td'.$td6.'>'.$pagot.'</td><td'.$td7.'>$ '.number_format($totalf,2,'.',',').'</td></tr>';
                $row++;
            }


            $htmlf .= '<tr><td colspan="6"align="right" class="stileback2"><b>SALDO PENDIENTE</b></td><td class="stileback2">$ '.number_format($montovencido,2,'.',',').'</td></tr></table>';
            $htmlf .='<table><tr><td></td></tr></table><pagebreak style="page-break-after: always;">';
        //==========================================================================
        $pdf->AddPage('L', 'A4');
            $html=$htmlf;
            //log_message('error',$html);
        $pdf->writeHTML($html, true, false, true, false, '');
    }
//======================================================
$pdf->Output('Documentos.pdf', 'I');