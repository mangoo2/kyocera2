<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class R_contratosv extends CI_Controller {
    public function __construct()     {
        parent::__construct();

        $this->load->model('ModeloCatalogos');
        $this->load->model('Modeloreportes');
        $this->load->helper('url');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->submenu=84;
        if($this->session->userdata('logeado')==true){
            $this->perfilid =$this->session->userdata('perfilid');
            $this->idpersonal = $this->session->userdata('idpersonal');
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,$this->submenu);// 52 es el id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
        }else{
            redirect('login'); 
        }
    }

    function index(){
        $data['MenusubId']=$this->submenu;
        $data['perfilid']=$this->perfilid;
            $this->load->view('header');
            $this->load->view('main');
            $this->load->view('rentas/contratosvencidos',$data);
            $this->load->view('footer');
            $this->load->view('rentas/contratosvencidosjs');
    }
    public function getlistconven() {
        $params = $this->input->post();
        $getdata = $this->Modeloreportes->getlistadocontratosvencidos($params);
        $totaldata= $this->Modeloreportes->getlistadocontratosvencidost($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    function permitircontinuar(){
        $params = $this->input->post();
        $idcon = $params['idcon'];
        $tipo = $params['tipo'];
        if($tipo==1){
            $tipo=0;
        }else{
            $tipo=1;
        }
        $this->ModeloCatalogos->updateCatalogo('contrato',array('permt_continuar'=>$tipo),array('idcontrato'=>$idcon));
    }
    

    
}
?>
