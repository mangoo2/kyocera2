var base_url = $('#base_url').val();
var addtr=0;
$(document).ready(function(){
    $('.chosen-select').chosen({width: "90%"});
});
function clientecontrato() {
	var idcliente = $('#idcliente option:selected').val();
	$.ajax({
        type:'POST',
        url: base_url+"index.php/Configuracionrentas/contratorentaclientec",
        data: {id:idcliente},
        success: function (data){
        	$('.mostrar_tabla').html(data); 
        }
    });
}
function  vercontrato(idcontrato) {
     window.location.href = base_url+"index.php/Configuracionrentas/ContratosPagos/"+idcontrato;
}
function agregarcontacto() {
        var tipo=$('#tipo').val();
        var nombre=$('#nombre').val();
        var telefono=$('#telefono').val();
        var correo=$('#correo').val();
        var ubicacion=$('#ubicacion_c').val();
        var ultima=$('#ultima').val();
        var comentario=$('#comentario').val();
        if (nombre!='') {
                //==========================================
                var addtre='<tr class="addtr_'+addtr+'">\
                                <td>\
                                    <input id="bodegasadd" type="hidden"  value="'+tipo+'" readonly>\
                                    '+tipo+'\
                                </td>\
                                <td>\
                                    <input id="consumible" type="hidden"  value="'+nombre+'" readonly>\
                                    '+nombre+'\
                                </td>\
                                <td>\
                                    <input id="consumible" type="hidden"  value="'+telefono+'" readonly>\
                                    '+telefono+'\
                                </td>\
                                <td>\
                                    <input id="consumible" type="hidden"  value="'+correo+'" readonly>\
                                    '+correo+'\
                                </td>\
                                <td>\
                                    <input id="consumible" type="hidden"  value="'+ubicacion+'" readonly>\
                                    '+ubicacion+'\
                                </td>\
                                <td>\
                                    <input id="consumible" type="hidden"  value="'+ultima+'" readonly>\
                                    '+ultima+'\
                                </td>\
                                <td>\
                                    <input id="consumible" type="hidden"  value="'+comentario+'" readonly>\
                                    '+comentario+'\
                                </td>\
                                <td><a class="btn-floating waves-effect waves-light red accent-2" onclick="deteletr('+addtr+')"><i class="material-icons">clear</i></a></td>\
                          </tr>';
                $('.tabla_equiposrenta').append(addtre);
                addtr++;
                $('#tipo').val('');
                $('#nombre').val('');
                $('#telefono').val('');
                $('#correo').val('');
                $('#ubicacion_c').val('');
                $('#ultima').val('');
                $('#comentario').val('');
                //====================================
        }else{
            swal({ title: "Atención!",
                            text: "Tienes que agregar un nombre",
                            type: 'error',
                            showCancelButton: false,
                            allowOutsideClick: false,
                        });
        }
}
function deteletr(id){
    $('.addtr_'+id).remove();
}
function pagos_procedimientos() {
    
    var pagos = $('#form_pagos').serialize();
    var procediminetos = $('#form_procedimientos').serialize();
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Configuracionrentas/guardarcontratopago",
        data: {pagos:pagos,procediminetos:procediminetos},
        success: function (data){
            $('.mostrar_tabla').html(data); 
        }
    });
}