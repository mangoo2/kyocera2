<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class Modelotablero extends CI_Model {
    public function __construct() 
    {
        parent::__construct();
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fechaactual = date('Y-m-d');
        $this->horaactual = date('G:i:s');
        if($this->session->userdata('logeado')==true){
            $this->perfilid =$this->session->userdata('perfilid');
            $this->idpersonal = $this->session->userdata('idpersonal');
            $this->usuario2 = $this->session->userdata('usuario2');
            
        }else{
            $this->perfilid =0;
            $this->idpersonal=0;
        }
    }

    function Insert($Tabla,$data){
        $this->db->insert($Tabla, $data);
        $id=$this->db->insert_id();
        return $id;
    }
    function updateCatalogo($Tabla,$data,$where){
        $this->db->set($data);
        $this->db->where($where);
        $this->db->update($Tabla);
        //return $id;
    }
    function getselectwheren($table,$where){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($where);
        $query=$this->db->get(); 
        return $query;
    }
    public function searchTecnico(){
        $sql = "SELECT usuarios.*, concat(p.nombre,' ',p.apellido_paterno,' ',p.apellido_materno) as tecnico,p.personalId 
                FROM usuarios 
                join personal as p on p.personalId=usuarios.personalId 
                WHERE (usuarios.perfilId=5 or usuarios.perfilId=12 or p.personalId=29 or p.personalId=19 or p.personalId=39)  and p.estatus=1 group by p.personalId";
        $query = $this->db->query($sql);
        return $query;
    }
    public function searchTecnico_per($id){
        $sql = "SELECT usuarios.*, concat(p.nombre,' ',p.apellido_paterno,' ',p.apellido_materno) as tecnico,p.personalId 
                FROM usuarios 
                join personal as p on p.personalId=usuarios.personalId 
                WHERE p.personalId='$id'   and p.estatus=1 group by p.personalId";
        $query = $this->db->query($sql);
        return $query;
    }
    function view_session_searchTecnico(){
        if(isset($_SESSION['session_stecnicos'])){
                $session_stecnicos=$_SESSION['session_stecnicos'];
        }else{
            //unset($_SESSION['session_stecnicos']);
            $session_stecnicos=$this->searchTecnico();
            $_SESSION['session_stecnicos']=$session_stecnicos->result();
        }
        return $session_stecnicos;
    }
    function tablero($mov_seer_activo,$fecha){
        $html_tablero_t='';
        $resultstec=$this->view_session_searchTecnico();
        if($this->perfilid==5 || $this->perfilid==12){
          $resultstec=$this->searchTecnico_per($this->idpersonal);
          $resultstec=$resultstec->result();
        }
        $arrays_tr_tec=array();
        $arrays_tr_tec[]=array('servicios'=>0,'html'=>'');
        foreach ($resultstec as $item_tec) {
            $opcion_mover_tec=' ondrop="soltar(event)" ondragover="permitirSoltar(event,'.$item_tec->personalId.')" ';
            $aux=array();

            $opcion_mover_todos=' ondragstart="arrastrar_todo(event,'.$item_tec->personalId.')" ';
            if($mov_seer_activo==0){
              $opcion_mover_todos='';
              $opcion_mover_tec='';
            }

          
            $html_tablero='';
            $html_tablero.='<div class="col s12 m6 l3 xl3 tecnico_info_'.$item_tec->personalId.'"  data-nametec="'.$item_tec->tecnico.'">';
              $html_tablero.='<div class="coloborder">';
                $html_tablero.='<div style="padding: 10px;">';
                  $html_tablero.='<p class="title_tec txt_c" '.$opcion_mover_todos.' ><b>'.$item_tec->tecnico.' </b><img style="width: 30px;" src="'.base_url().'public/img/control/monitor.svg"></p>';
                  $html_tablero.='<div class="div_table" '.$opcion_mover_tec.' >';
                    
                  
                    $html_tablero.='<table class="table table-datatables table-servicios" >';
                      $html_tablero.='<thead>';
                        $html_tablero.='<tr>';
                          $html_tablero.='<td></td>';
                        $html_tablero.='</tr>';
                      $html_tablero.='</thead>';
                      $html_tablero.='<tbody>';
                        
                          $url_img_pin=base_url().'public/img/control/pin.svg';
                          $url_img_info=base_url().'public/img/control/pin.svg';
                          $url_img_cancel=base_url().'public/img/control/cancel.svg';


                          $arrays_tr=array();
                          
                          $resultser_con=$this->ModeloAsignacion->getlistadocalentariotipogroup($fecha,$item_tec->personalId,1);
                          $num_con=$resultser_con->num_rows();
                          foreach ($resultser_con->result() as $item) {
                            $colortr='';
                            //=====================
                                $resultser_con=$this->ModeloAsignacion->get_info_status($item->g_status);
                                $resultser_con_nr=$this->ModeloAsignacion->get_info_status_nr($item->g_nr);
                                
                                
                                
                            //=====================
                            $hora=date("G.i", strtotime($item->hora));
                            $horaincial=$item->hora;
                            $horal=date("h:i A", strtotime($item->hora));
                            
                            $horafin_res=$this->restarhoras($item->horafin,60);//resta una hora a la hora fin
                            
                            $onclick_h="cambiarhora(1,$item->asignacionId,'$item->hora','$item->horafin','$item->hora_comida','$item->horafin_comida','$horafin_res')";
                            $onclick_r="notificacionrechazogeneral(1,$item->asignacionId)";
                            //=======================
                              $rest_iti1=$this->ModeloAsignacion->get_info_itinerario_asignacion($item->asignacionId,1,$item_tec->personalId);
                              foreach ($rest_iti1->result() as $itemasig) {
                                $hora=date("G.i", strtotime($itemasig->hora_inicio));
                                $horal=date("h:i A", strtotime($itemasig->hora_inicio));
                                $horaincial=$itemasig->hora_inicio;
                                $onclick_h="cambiarhora(1,$item->asignacionId,'$item->hora','$item->horafin','$item->hora_comida','$item->horafin_comida','$horafin_res')";//esto solo es por si en dado caso tiene horario por parte del tecnico pero el anterio y esta deben de ser casi iguales
                                if($this->perfilid==5 || $this->perfilid==12){
                                  $onclick_h='';
                                }
                              }
                            //=======================
                            //========================================================
                                  //log_message('error', " ______________{$item->asignacionId}____________");
                                  $hora1 = date("H:i:s",strtotime($horaincial.' -50 minutes'));
                                  $hora2 = date("H:i:s");
                                  //log_message('error', " ______________{$hora1}_<_{$hora2}__________");

                                  $hora1 = new DateTime($hora1);
                                  $hora2 = new DateTime($hora2);

                                  if ($hora1 < $hora2) {
                                      $colortr = ' tr_poriniciar ';
                                  }
                            //========================================================
                            //========================================================
                                  //log_message('error', " ______________{$item->asignacionId}____________");
                                  $hora1 = date("H:i:s",strtotime($horaincial.' -1 minutes'));// va a ser con la hora que el tecnico establecio
                                  $hora2 = date("H:i:s");
                                  //log_message('error', " ______________{$hora1}_<_{$hora2}__________");

                                  $hora1 = new DateTime($hora1);
                                  $hora2 = new DateTime($hora2);

                                  if ($hora1 < $hora2) {
                                      $colortr = ' tr_porvencer ';
                                      if($resultser_con==0){
                                        if($this->fechaactual==$fecha){
                                            $result_bit = $this->getselectwheren('bitacora_servicios_retraso',array('tipo'=>1,'idservicio'=>$item->asignacionId,'fecha'=>$fecha,'tecnico'=>$item_tec->personalId));  
                                            if($result_bit->num_rows()==0){
                                              $this->Insert('bitacora_servicios_retraso',array('tipo'=>1,'idservicio'=>$item->asignacionId,'fecha'=>$fecha,'tecnico'=>$item_tec->personalId));
                                            }
                                        }
                                      }
                                      
                                  }
                              //========================================================
                                if($resultser_con==1){
                                  $colortr=' tr_orage ';
                                }
                                if($resultser_con==2){
                                  $colortr=' tr_green ';
                                  $this->updateCatalogo('bitacora_servicios_retraso',array('activo'=>0),array('tipo'=>1,'idservicio'=>$item->asignacionId,'fecha'=>$fecha,'tecnico'=>$item_tec->personalId));
                                }
                                if($resultser_con_nr==1){
                                  $colortr=' tr_red ';
                                }
                                $icon_proceso='';
                                if (strpos($item->g_status, '1') !== false) {
                                    $icon_proceso='<i class="fas fa-sync fa-spin"></i>';
                                    $colortr=' tr_blue ';
                                    $result_ser_status = $this->getselectwheren('asignacion_ser_contrato_a_e',array('asignacionId'=>$item->asignacionId,'status'=>1,'fecha'=>$fecha,'tecnico'=>$item_tec->personalId)); 
                                    foreach ($result_ser_status->result() as $itemserd) {
                                      
                                      $icon_proceso.=$this->tiempotrascurrido($itemserd->horaserinicio);
                                    }


                                    
                                }

                            //$opcion_mover=' ondrop="soltar(event)" ondragover="permitirSoltar(event,1,'.$item->asignacionId.')" ondragstart="arrastrar(event,1,'.$item->asignacionId.')" ';
                            $opcion_mover=' ondragstart="arrastrar(event,1,'.$item->asignacionId.','.$item_tec->personalId.')" ';
                            if($mov_seer_activo==0){
                              $opcion_mover='';
                            }
                            $html='<tr class=" '.$item->g_status.' -'.$resultser_con.'- contrato '.$colortr.' status_'.$item->status.'"  '.$opcion_mover.' draggable="true">';
                              $html.='<td><span>'.$item->empresa.'</span><br><span class="span_hora" style="color: red" ondblclick="'.$onclick_h.'">'.$horal.' </span> '.$icon_proceso.'</td>';
                              $html.='<td ondblclick="obternerdatosservicio('.$item->asignacionId.',1)"><i class="fas fa-info-circle infoicon"></i></td>';
                              $html.='<td ondblclick="obtenerdirecciones(1,'.$item->asignacionId.')" class="pinsvg"></td>';
                              $html.='<td ondblclick="'.$onclick_r.'" class="cansvg rechag"></td>';
                            $html.='</tr>';

                            $arrays_tr[]=array('hora'=>$hora,'html'=>$html);
                          }
                          $resultser_pol=$this->ModeloAsignacion->getlistadocalentariotipogroup($fecha,$item_tec->personalId,2);
                          $num_pol=$resultser_pol->num_rows();
                          foreach ($resultser_pol->result() as $item) {
                            $colortr='';
                            if($item->status==2){
                              $colortr=' tr_green ';
                            }
                             
                            $resultser_con=$this->ModeloAsignacion->get_info_status($item->g_status);
                            $resultser_con_nr=$this->ModeloAsignacion->get_info_status_nr($item->g_nr);
                            $hora=date("G.i", strtotime($item->hora));
                            $horal=date("h:i A", strtotime($item->hora));
                            $horaincial=$item->hora;
                            
                            $horafin_res=$this->restarhoras($item->horafin,60);//resta una hora a la hora fin

                            $onclick_h="cambiarhora(2,$item->asignacionId,'$item->hora','$item->horafin','$item->hora_comida','$item->horafin_comida','$horafin_res')";
                            $onclick_r="notificacionrechazogeneral(2,$item->asignacionId)";
                            //$opcion_mover=' ondrop="soltar(event)" ondragover="permitirSoltar(event,1,'.$item->asignacionId.')" ondragstart="arrastrar(event,1,'.$item->asignacionId.')" ';
                            $opcion_mover=' ondragstart="arrastrar(event,2,'.$item->asignacionId.','.$item_tec->personalId.')" ';
                            //=======================
                              $rest_iti2=$this->ModeloAsignacion->get_info_itinerario_asignacion($item->asignacionId,2,$item_tec->personalId);
                              foreach ($rest_iti2->result() as $itemasig) {
                                $hora=date("G.i", strtotime($itemasig->hora_inicio));
                                $horal=date("h:i A", strtotime($itemasig->hora_inicio));
                                $horaincial=$itemasig->hora_inicio;
                                $onclick_h="cambiarhora(2,$item->asignacionId,'$item->hora','$item->horafin','$item->hora_comida','$item->horafin_comida','$horafin_res')";//esto solo es por si en dado caso tiene horario por parte del tecnico pero el anterio y esta deben de ser casi iguales
                                if($this->perfilid==5 || $this->perfilid==12){
                                  $onclick_h='';
                                }
                              }
                            //=======================
                            //========================================================
                                  //log_message('error', " ______________{$item->asignacionId}____________");
                                  $hora1 = date("H:i:s",strtotime($horaincial.' -50 minutes'));
                                  $hora2 = date("H:i:s");
                                  //log_message('error', " ______________{$hora1}_<_{$hora2}__________");

                                  $hora1 = new DateTime($hora1);
                                  $hora2 = new DateTime($hora2);

                                  if ($hora1 < $hora2) {
                                      $colortr = ' tr_poriniciar ';
                                  }
                            //========================================================
                            //========================================================
                                  //log_message('error', " ______________{$item->asignacionId}____________");
                                  $hora1 = date("H:i:s",strtotime($horaincial.' -1 minutes'));// va a ser con la hora que el tecnico establecio
                                  $hora2 = date("H:i:s");
                                  $colortr_h =$hora1 .' < '. $hora2;
                                  $hora1 = new DateTime($hora1);
                                  $hora2 = new DateTime($hora2);

                                  if ($hora1 < $hora2) {
                                      $colortr = ' tr_porvencer ';
                                      $colortr .=$colortr_h;
                                      if($resultser_con==0){
                                        if($this->fechaactual==$fecha){
                                            $result_bit = $this->getselectwheren('bitacora_servicios_retraso',array('tipo'=>2,'idservicio'=>$item->asignacionId,'fecha'=>$fecha,'tecnico'=>$item_tec->personalId));  
                                            if($result_bit->num_rows()==0){
                                              $this->Insert('bitacora_servicios_retraso',array('tipo'=>2,'idservicio'=>$item->asignacionId,'fecha'=>$fecha,'tecnico'=>$item_tec->personalId));
                                            }
                                        }
                                      }
                                  }
                                //========================================================
                            //=====================
                                
                                if($resultser_con==1){
                                  $colortr=' tr_orage ';
                                }
                                if($resultser_con==2){
                                  $colortr=' tr_green ';
                                  $this->updateCatalogo('bitacora_servicios_retraso',array('activo'=>0),array('tipo'=>2,'idservicio'=>$item->asignacionId,'fecha'=>$fecha,'tecnico'=>$item_tec->personalId));
                                }
                                if($resultser_con_nr==1){
                                  $colortr=' tr_red ';
                                }
                                $icon_proceso='';
                                if (strpos($item->g_status, '1') !== false) {
                                    $icon_proceso='<i class="fas fa-sync fa-spin"></i>';
                                    $colortr=' tr_blue ';
                                    $icon_proceso.=$this->tiempotrascurrido($item->hora);
                                }
                            //=====================
                            if($mov_seer_activo==0){
                              $opcion_mover='';
                            }

                            $html='<tr class="poliza '.$colortr.'" '.$opcion_mover.' draggable="true">';
                              $html.='<td>'.$item->empresa.'<br><span class="span_hora" style="color: red" ondblclick="'.$onclick_h.'">'.$horal.'</span> '.$icon_proceso.'</td>';
                              $html.='<td ondblclick="obternerdatosservicio('.$item->asignacionId.',2)"><i class="fas fa-info-circle infoicon"></i></td>';
                              $html.='<td ondblclick="obtenerdirecciones(2,'.$item->asignacionId.')" class="pinsvg"></td>';
                              $html.='<td ondblclick="'.$onclick_r.'" class="cansvg rechag"></td>';
                            $html.='</tr>';

                            $arrays_tr[]=array('hora'=>$hora,'html'=>$html);
                          }
                          $resultser_cli=$this->ModeloAsignacion->getlistadocalentariotipogroup($fecha,$item_tec->personalId,3);
                          $num_cli=$resultser_cli->num_rows();
                          foreach ($resultser_cli->result() as $item) {
                            $colortr='';
                            if($item->estatus1==2){
                              $colortr=' tr_green ';
                            }
                            
                            
                            $hora=date("G.i", strtotime($item->hora));
                            $horal=date("h:i A", strtotime($item->hora));
                            $horaincial=$item->hora;
                            
                            $horafin_res=$this->restarhoras($item->horafin,60);//resta una hora a la hora fin

                            $onclick_h="cambiarhora(3,$item->asignacionId,'$item->hora','$item->horafin','$item->hora_comida','$item->horafin_comida','$horafin_res')";
                            $onclick_r="notificacionrechazogeneral(3,$item->asignacionId)";

                            //$opcion_mover=' ondrop="soltar(event)" ondragover="permitirSoltar(event,1,'.$item->asignacionId.')" ondragstart="arrastrar(event,1,'.$item->asignacionId.')" ';
                            $opcion_mover=' ondragstart="arrastrar(event,3,'.$item->asignacionId.','.$item_tec->personalId.')" ';
                            //=======================
                              $rest_iti3=$this->ModeloAsignacion->get_info_itinerario_asignacion($item->asignacionId,3,$item_tec->personalId);
                              foreach ($rest_iti3->result() as $itemasig) {
                                $hora=date("G.i", strtotime($itemasig->hora_inicio));
                                $horal=date("h:i A", strtotime($itemasig->hora_inicio));
                                $horaincial=$itemasig->hora_inicio;
                                $onclick_h="cambiarhora(3,$item->asignacionId,'$item->hora','$item->horafin','$item->hora_comida','$item->horafin_comida','$horafin_res')";//esto solo es por si en dado caso tiene horario por parte del tecnico pero el anterio y esta deben de ser casi iguales
                                if($this->perfilid==5 || $this->perfilid==12){
                                  $onclick_h='';
                                }
                              }
                            //=======================
                            //========================================================
                                  //log_message('error', " ______________{$item->asignacionId}____________");
                                  $hora1 = date("H:i:s",strtotime($horaincial.' -50 minutes'));
                                  $hora2 = date("H:i:s");
                                  //log_message('error', " ______________{$hora1}_<_{$hora2}__________");

                                  $hora1 = new DateTime($hora1);
                                  $hora2 = new DateTime($hora2);

                                  if ($hora1 < $hora2) {
                                      $colortr = ' tr_poriniciar ';
                                  }
                            //========================================================
                                  $resultser_con=$this->ModeloAsignacion->get_info_status($item->g_status1);
                                  $resultser_con_nr=$this->ModeloAsignacion->get_info_status_nr($item->g_nr);
                            //========================================================
                                  //log_message('error', " ______________{$item->asignacionId}____________");
                                  $hora1 = date("H:i:s",strtotime($horaincial.' -1 minutes'));// va a ser con la hora que el tecnico establecio
                                  $hora2 = date("H:i:s");

                                  $hora1 = new DateTime($hora1);
                                  $hora2 = new DateTime($hora2);

                                  if ($hora1 < $hora2) {
                                      $colortr = ' tr_porvencer ';
                                      if($resultser_con==0){
                                        if($this->fechaactual==$fecha){
                                            $result_bit = $this->getselectwheren('bitacora_servicios_retraso',array('tipo'=>3,'idservicio'=>$item->asignacionId,'fecha'=>$fecha,'tecnico'=>$item_tec->personalId));  
                                            if($result_bit->num_rows()==0){
                                              $this->Insert('bitacora_servicios_retraso',array('tipo'=>3,'idservicio'=>$item->asignacionId,'fecha'=>$fecha,'tecnico'=>$item_tec->personalId));
                                            }
                                        }
                                      }
                                  }
                                //========================================================
                                //=====================
                              
                                if($resultser_con==1){
                                  $colortr=' tr_orage ';
                                }
                                if($resultser_con==2){
                                  $colortr=' tr_green ';
                                  $this->updateCatalogo('bitacora_servicios_retraso',array('activo'=>0),array('tipo'=>3,'idservicio'=>$item->asignacionId,'fecha'=>$fecha,'tecnico'=>$item_tec->personalId));
                                }
                                if($resultser_con_nr==1){
                                  $colortr=' tr_red ';
                                }
                                 $icon_proceso='';
                                if (strpos($item->g_status1, '1') !== false) {
                                    $icon_proceso='<i class="fas fa-sync fa-spin"></i>';
                                    $colortr=' tr_blue ';
                                    $icon_proceso.=$this->tiempotrascurrido($item->g_horaserinicio);
                                    $icon_proceso.=$item->g_horaserinicio;
                                }
                            //=====================
                            if($mov_seer_activo==0){
                              $opcion_mover='';
                            }

                            $html='<tr class=" eventocli '.$colortr.' '.$item->estatus1.' '.$item->estatus2.' " '.$opcion_mover.' draggable="true">';
                              $html.='<td>'.$item->empresa.'<br><span class="span_hora" style="color: red" ondblclick="'.$onclick_h.'">'.$horal.'</span> '.$icon_proceso.'</td>';
                              $html.='<td ondblclick="obternerdatosservicio('.$item->asignacionId.',3)"><i class="fas fa-info-circle infoicon"></i></td>';
                              $html.='<td ondblclick="obtenerdirecciones(3,'.$item->asignacionId.')" class="pinsvg"></td>';
                              $html.='<td ondblclick="'.$onclick_r.'" class="cansvg rechag"></td>';
                            $html.='</tr>';

                            $arrays_tr[]=array('hora'=>$hora,'html'=>$html);
                          }
                          $resultser_ve=$this->ModeloAsignacion->getlistadocalentariotipogroup($fecha,$item_tec->personalId,4);
                          $num_ve=$resultser_ve->num_rows();
                          foreach ($resultser_ve->result() as $item) {
                            $colortr='';
                            if($item->status==2){
                              $colortr=' tr_green ';
                            }
                            //========================================================
                                  //log_message('error', " ______________{$item->asignacionId}____________");
                                  $hora1 = date("H:i:s",strtotime($item->horafin.' -30 minutes'));
                                  $hora2 = date("H:i:s");

                                  $hora1 = new DateTime($hora1);
                                  $hora2 = new DateTime($hora2);

                                  if ($hora1 < $hora2) {
                                      $colortr = ' tr_porvencer ';
                                      if($this->fechaactual==$fecha){
                                          $result_bit = $this->getselectwheren('bitacora_servicios_retraso',array('tipo'=>4,'idservicio'=>$item->asignacionId,'fecha'=>$fecha,'tecnico'=>$item_tec->personalId));  
                                          if($result_bit->num_rows()==0){
                                            $this->Insert('bitacora_servicios_retraso',array('tipo'=>4,'idservicio'=>$item->asignacionId,'fecha'=>$fecha,'tecnico'=>$item_tec->personalId));
                                          }
                                      }
                                  }
                                //========================================================
                            //=====================
                                $resultser_con=$this->ModeloAsignacion->get_info_status($item->g_status);
                                $resultser_con_nr=$this->ModeloAsignacion->get_info_status_nr($item->g_nr);
                                
                            $hora=date("G.i", strtotime($item->hora));
                            $horal=date("h:i A", strtotime($item->hora));
                            $horaincial=$item->hora;
                            
                            $horafin_res=$this->restarhoras($item->horafin,60);//resta una hora a la hora fin

                            $onclick_h="cambiarhora(4,$item->asignacionId,'$item->hora','$item->horafin','$item->hora_comida','$item->horafin_comida','$horafin_res')";
                            $onclick_r="notificacionrechazogeneral(4,$item->asignacionId)";

                            //$opcion_mover=' ondrop="soltar(event)" ondragover="permitirSoltar(event,1,'.$item->asignacionId.')" ondragstart="arrastrar(event,1,'.$item->asignacionId.')" ';
                            $opcion_mover=' ondragstart="arrastrar(event,4,'.$item->asignacionId.','.$item_tec->personalId.')" ';
                            //=======================
                              $rest_iti3=$this->ModeloAsignacion->get_info_itinerario_asignacion($item->asignacionId,4,$item_tec->personalId);
                              foreach ($rest_iti3->result() as $itemasig) {
                                $hora=date("G.i", strtotime($itemasig->hora_inicio));
                                $horal=date("h:i A", strtotime($itemasig->hora_inicio));
                                $horaincial=$itemasig->hora_inicio;
                                $onclick_h="cambiarhora(4,$item->asignacionId,'$item->hora','$item->horafin','$item->hora_comida','$item->horafin_comida','$horafin_res')";
                                if($this->perfilid==5 || $this->perfilid==12){
                                  $onclick_h='';
                                }
                              }
                            //=======================
                            //========================================================
                                  //log_message('error', " ______________{$item->asignacionId}____________");
                                  $hora1 = date("H:i:s",strtotime($horaincial.' -50 minutes'));
                                  $hora2 = date("H:i:s");
                                  //log_message('error', " ______________{$hora1}_<_{$hora2}__________");

                                  $hora1 = new DateTime($hora1);
                                  $hora2 = new DateTime($hora2);

                                  if ($hora1 < $hora2) {
                                      $colortr = ' tr_poriniciar ';
                                  }
                            //========================================================
                                if($resultser_con==1){
                                  $colortr=' tr_orage ';
                                }
                                if($resultser_con==2){
                                  $colortr=' tr_green ';
                                  $this->updateCatalogo('bitacora_servicios_retraso',array('activo'=>0),array('tipo'=>4,'idservicio'=>$item->asignacionId,'fecha'=>$fecha,'tecnico'=>$item_tec->personalId));
                                }
                                if($resultser_con_nr==1){
                                  $colortr=' tr_red ';
                                }
                                $icon_proceso='';
                                if (strpos($item->g_status, '1') !== false) {
                                    $icon_proceso='<i class="fas fa-sync fa-spin"></i>';
                                    $colortr=' tr_blue ';
                                    $icon_proceso.=$this->tiempotrascurrido($item->hora);
                                }
                            //=====================
                            if($mov_seer_activo==0){
                              $opcion_mover='';
                            }

                            $html='<tr class=" eventocli '.$colortr.' '.$item->status.'" '.$opcion_mover.' draggable="true">';
                              $html.='<td>'.$item->empresa.'<br><span class="span_hora" style="color: red" ondblclick="'.$onclick_h.'">'.$horal.'</span> '.$icon_proceso.'</td>';
                              $html.='<td ondblclick="obternerdatosservicio('.$item->asignacionId.',4)"><i class="fas fa-info-circle infoicon"></i></td>';
                              $html.='<td ondblclick="obtenerdirecciones(4,'.$item->asignacionId.')" class="pinsvg"></td>';
                              $html.='<td ondblclick="'.$onclick_r.'" class="cansvg rechag"></td>';
                            $html.='</tr>';

                            $arrays_tr[]=array('hora'=>$hora,'html'=>$html);
                          }
                          //var_dump($arrays_tr);
                          
                          


                          //==============================================================================
                          //==============================================================================
                          if(count($arrays_tr)>0){
                            foreach ($arrays_tr as $key => $row) {
                              if(isset($row['html'])){
                                $aux[$key] = $row['hora'];
                              }
                            }
                            //var_dump($aux);
                            array_multisort($aux, SORT_ASC, $arrays_tr);
                            foreach ($arrays_tr as $key => $row) {
                                  $html_tablero.= $row['html'];
                            }
                          }
                          

                         
                        




                        
                      $html_tablero.='</tbody>';
                    $html_tablero.='</table>';
                  $html_tablero.='</div>';
                $html_tablero.='</div>  ';
              $html_tablero.='</div>';
            $html_tablero.='</div> ';

            
            
            
            
            $num_servicios=$num_con+$num_pol+$num_cli+$num_ve;
            $arrays_tr_tec[]=array('servicios'=>$num_servicios,'html'=>$html_tablero);

        }
        if($this->perfilid==5 || $this->perfilid==12){
          $html_tablero=str_replace('ondblclick', 'onclick', $html_tablero);
          $html_tablero_t=$html_tablero;
        }else{
          foreach ($arrays_tr_tec as $key => $row) {
                $aux[$key] = $row['servicios'];
            }
            array_multisort($aux, SORT_DESC, $arrays_tr_tec);
            foreach ($arrays_tr_tec as $key => $row) {
                  $html_tablero_t.=$row['html'];
            }  
        }
        
        return $html_tablero_t;
    }


    function ajustarhora($horafin){
      $horamaxima = '17:30:00';

      $horafin = new DateTime($horafin);
      $horamaxima = new DateTime($horamaxima);
      if ($horamaxima < $horafin) {
        $horafin='17:00:00';
      }
    }
    function restarhoras($horafin,$min){
      $horafin = new DateTime($horafin);
      $horafin->modify('-'.$min.' minutes');
      return $horafin->format('H:i:s');
    }
    function tiempotrascurrido($hora){

      $hora_proporcionada = new DateTime($hora);

      // Hora actual
      $hora_actual = new DateTime();

      // Calcular la diferencia
      $diferencia = $hora_proporcionada->diff($hora_actual);

      // Mostrar diferencia en formato de horas, minutos y segundos
      $dif_hrs= $diferencia->format('%H horas, %I minutos');
      $d_h=$diferencia->format('%h');
      if(intval($d_h)>0){
        $d_h_mayor=' mayor ';
      }else{
        $d_h_mayor='';
      }
      return '<div class="dif_hrs '.$d_h_mayor.'" data-horai="'.$hora.'">'.$dif_hrs.'</div>';
    }
   
}