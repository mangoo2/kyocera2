<?php
defined('BASEPATH') OR exit ('No direct script access allowed');

class Ventaspre_model extends CI_Model {
    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
        //$this->DB3 = $this->load->database('other3_db', TRUE);
    }
    
    
    public function getListaVentasIncompletasfactura($params){
            $tipo=$params['tipo'];
            $cliente=$params['cliente'];
            $status=$params['status'];
            $tipoview = $params['tipoview'];
            $fechainicio = $params['fechainicio'];
            $fechafin = $params['fechafin'];
            $idpersonal = $params['idpersonal'];
                $columns = array( 
                    0=>'v.id',
                    1=>'v.vendedor',
                    2=>'v.empresa',
                    3=>'v.estatus',
                    4=>'v.prefactura',
                    5=>"v.tabla",
                    6=>'v.fechaentrega',
                    7=>'v.e_entrega',
                    8=>'v.id_rh',
                    9=>'v.renta_rh'

                );
                
                $select="";
                foreach ($columns as $c) {
                    $select.="$c, ";
                }
                $this->db->select($select);
                $this->db->from('vista_entregas_dia v ');
                $where = array('v.prefactura'=>1);
                if ($cliente>0) {
                    $where['v.idCliente']=$cliente;
                }
                if ($status==1) {
                    $where['v.statuspago']=1;
                }elseif ($status==2) {
                    $where['v.statuspago']=0;
                }
                    $where['v.activo']=1;
                if ($tipo>0) {
                    $where['v.tipotabla']=$tipo;
                }
                if($tipoview==1){
                    $where['v.id_personal']=$idpersonal;
                }
                if ($fechainicio!='') {
                    $where['v.reg >=']=$fechainicio;
                }
                if ($fechafin!='') {
                    $where['v.reg <=']=$fechafin;
                }
                
                $this->db->where($where);
                     
            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->db->group_start();
                foreach($columns as $c){
                    $this->db->or_like($c,$search);
                }
                $this->db->group_end();
                
            }
            //==========================================
            $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
            $this->db->limit($params['length'],$params['start']);
            //echo $this->db->get_compiled_select();
            $query=$this->db->get();
            // print_r($query); die;
            return $query;
    }
    public function getListaVentasIncompletasfacturat($params){
            $tipo=$params['tipo'];
            $cliente=$params['cliente'];
            $status=$params['status'];

            $tipoview = $params['tipoview'];
            $fechainicio = $params['fechainicio'];
            $fechafin = $params['fechafin'];
            $idpersonal = $params['idpersonal'];

                $columns = array( 
                     0=>'v.id',
                    1=>'v.vendedor',
                    2=>'v.empresa',
                    3=>'v.estatus',
                    4=>'v.prefactura',
                    5=>"v.tabla",
                    6=>'v.fechaentrega',
                    7=>'v.e_entrega',
                    8=>'v.id_rh',
                    9=>'v.renta_rh'

                );
                
                $select="";
                foreach ($columns as $c) {
                    $select.="$c, ";
                }
                $this->db->select('COUNT(*) as total');
                $this->db->from('vista_entregas_dia v ');
                $where = array('v.prefactura'=>1);
                if ($cliente>0) {
                    $where['v.idCliente']=$cliente;
                }
                if ($status==1) {
                    $where['v.statuspago']=1;
                }elseif ($status==2) {
                    $where['v.statuspago']=0;
                }
                    $where['v.activo']=1;
                if ($tipo>0) {
                    $where['v.tipotabla']=$tipo;
                }
                if($tipoview==1){
                    $where['v.id_personal']=$idpersonal;
                }
                if ($fechainicio!='') {
                    $where['v.reg >=']=$fechainicio;
                }
                if ($fechafin!='') {
                    $where['v.reg <=']=$fechafin;
                }
                $this->db->where($where);
                     
            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->db->group_start();
                foreach($columns as $c){
                    $this->db->or_like($c,$search);
                }
                $this->db->group_end();
                
            }
            //==========================================
            
            $query=$this->db->get();

            return $query->row()->total;
    }

    public function get_prefactura($tipoventa,$cliente,$tipoestatus,$f1,$f2,$idpersonal)
    {   
        $whare_c='';
        if($cliente>0){
            $whare_c=' and cli.id='.$cliente;
        }

        $whare_p='';
        if($idpersonal>0){
            $whare_p=' and per.personalId='.$idpersonal;
        }
        
        $whare_v='';$whare_pol='';
        if($tipoestatus>0){
            $whare_v=' and v.estatus='.$tipoestatus;
            $whare_pol=' and pol.estatus='.$tipoestatus;
        }

        $whare_vc='';
        if($tipoestatus>0){
            $whare_vc=' and vc.estatus='.$tipoestatus;
        }

        $whare_ren='';
        if($tipoestatus>0){
            $whare_ren=' and ren.estatus='.$tipoestatus;
        }
      
        
        if($tipoventa==0){
            $sql = "
            SELECT 
                v.estatus,
                v.id,
                v.prefactura,
                v.telefono,
                v.correo,
                cli.empresa,
                per.nombre as vendedor,
                'ventas' as tabla,
                1 as tipotabla,
                v.fechaentrega,
                v.fechaentregah,
                v.e_entrega,
                v.e_entrega_p,
                v.idCliente,
                v.statuspago,
                v.activo,
                0 as id_rh,
                0 as renta_rh,
                pre.reg,
                v.id_personal,
                '' as equipo,
                '' as consumibles,
                '' as refacciones,
                '' as poliza
                FROM ventas as v 
            inner join clientes as cli on cli.id=v.idCliente 
            LEFT join personal as per on per.personalId=v.id_personal 
            left join prefactura as pre on pre.ventaId=v.id and pre.tipo=0
            WHERE v.activo=1 and v.combinada=0 and v.viewed=1 $whare_v $whare_c $whare_p and pre.reg BETWEEN '$f1 00:00:00' AND '$f2 23:59:59'

            union

            SELECT 
                v.estatus,
                v.id,
                v.prefactura,
                v.telefono,
                v.correo,
                cli.empresa,
                per.nombre vendedor,
                'rentas' tabla,
                2 as tipotabla,
                pre.vencimiento as fechaentrega,
                v.fechaentregah,
                v.e_entrega,
                v.e_entrega_p,
                v.idCliente,
                v.statuspago,
                1 as activo,
                0 as id_rh,
                0 as renta_rh,
                pre.reg,
                v.id_personal,
                pre.prefacturaId as equipo,
                '' as consumibles,
                '' as refacciones,
                '' as poliza
                from rentas as v
                inner join clientes as cli on cli.id=v.idCliente
                LEFT join personal as per on per.personalId=v.id_personal 
                left join prefactura as pre on pre.ventaId=v.id and pre.tipo=2
                WHERE v.estatus !=0 and v.viewed=1 $whare_v $whare_c $whare_p and pre.reg BETWEEN '$f1 00:00:00' AND '$f2 23:59:59'

            
            union

            SELECT

                vc.estatus,
                vc.combinadaId id,
                vc.prefactura,
                vc.telefono,
                vc.correo,
                cli.empresa,
                per.nombre vendedor,
                'combinada' tabla,
                4 as tipotabla,
                vc.fechaentrega,
                vc.fechaentregah,
                vc.e_entrega,
                vc.e_entrega_p,
                vc.idCliente,
                vc.statuspago,
                vc.activo,
                0 as id_rh,
                0 as renta_rh,
                pre.reg,
                vc.id_personal,
                vc.equipo,
                vc.consumibles,
                vc.refacciones,
                vc.poliza
                FROM ventacombinada vc
                inner join clientes as cli on cli.id=vc.idCliente
                LEFT join personal as per on per.personalId=vc.id_personal
                left join prefactura as pre on pre.ventaId=vc.combinadaId and pre.tipo=3
                WHERE vc.activo=1 and vc.viewed=1 $whare_vc $whare_c $whare_p and pre.reg BETWEEN '$f1 00:00:00' AND '$f2 23:59:59'

            union

            SELECT 
                0 as estatus,
                pre.prefacturaId as id,
                1 prefactura,
                ren.telefono,
                ren.correo,
                cli.empresa,
                per.nombre vendedor,
                'rentas historial' as tabla,
                5 as tipotabla,
                rh.fechaentrega,
                rh.fechaentregah,
                rh.e_entrega,
                rh.e_entrega_p,
                ren.idCliente,
                ren.statuspago,
                rh.activo,
                rh.id as id_rh,
                rh.rentas as renta_rh,
                pre.reg,
                ren.id_personal,
                '' as equipo,
                '' as consumibles,
                '' as refacciones,
                '' as poliza
            FROM rentas_historial as rh
            INNER JOIN prefactura as pre on pre.ventaId=rh.id AND pre.tipo=4
            INNER JOIN rentas as ren on ren.id=rh.rentas
            INNER JOIN clientes as cli on cli.id=ren.idCliente
            LEFT join personal as per on per.personalId=ren.id_personal
            WHERE rh.activo=1 and rh.viewed=1 $whare_ren $whare_c $whare_p and pre.reg BETWEEN '$f1 00:00:00' AND '$f2 23:59:59'

            union

            SELECT 
                        pol.estatus,
                        pol.id,
                        pol.prefactura,
                        pol.telefono,
                        pol.correo,
                        cli.empresa,
                        per.nombre as vendedor,
                        'Servicio' as tabla,
                        3 as tipotabla,
                        pol.fechaentrega,
                        pol.fechaentregah,
                        pol.e_entrega,
                        pol.e_entrega_p,
                        pol.idCliente,
                        pol.statuspago,
                        pol.activo,
                        0 as id_rh,
                        0 as renta_rh,
                        pre.reg,
                        pol.id_personal,
                        '' as equipo,
                        '' as consumibles,
                        '' as refacciones,
                        '' as poliza
                        FROM polizasCreadas as pol
                        INNER JOIN clientes as cli on cli.id=pol.idCliente
                        INNER JOIN personal as per on per.personalId=pol.id_personal
                        INNER JOIN prefactura as pre on pre.ventaId=pol.id AND pre.tipo=1
                        WHERE pol.activo=1 AND pol.combinada=0 $whare_pol $whare_c $whare_p AND pol.reg BETWEEN '$f1 00:00:00' AND '$f2 00:00:00'

            ";
        }else{
            if($tipoventa==1){
                $sql = "SELECT 
                    v.estatus,
                    v.id,
                    v.prefactura,
                    v.telefono,
                    v.correo,
                    cli.empresa,
                    per.nombre as vendedor,
                    'ventas' as tabla,
                    1 as tipotabla,
                    v.fechaentrega,
                    v.fechaentregah,
                    v.e_entrega,
                    v.e_entrega_p,
                    v.idCliente,
                    v.statuspago,
                    v.activo,
                    0 as id_rh,
                    0 as renta_rh,
                    pre.reg,
                    v.id_personal,
                    '' as equipo,
                    '' as consumibles,
                    '' as refacciones,
                    '' as poliza
                    FROM ventas as v 
                inner join clientes as cli on cli.id=v.idCliente 
                LEFT join personal as per on per.personalId=v.id_personal 
                left join prefactura as pre on pre.ventaId=v.id and pre.tipo=0
                WHERE v.activo=1 and v.combinada=0 and v.viewed=1 $whare_v $whare_c $whare_p and pre.reg BETWEEN '$f1 00:00:00' AND '$f2 23:59:59'";

            }else if($tipoventa==2){
                $sql = "SELECT 
                    v.estatus,
                    v.id,
                    v.prefactura,
                    v.telefono,
                    v.correo,
                    cli.empresa,
                    per.nombre vendedor,
                    'rentas' tabla,
                    2 as tipotabla,
                    pre.vencimiento as fechaentrega,
                    v.fechaentregah,
                    v.e_entrega,
                    v.e_entrega_p,
                    v.idCliente,
                    v.statuspago,
                    1 as activo,
                    0 as id_rh,
                    0 as renta_rh,
                    pre.reg,
                    v.id_personal,
                    pre.prefacturaId as equipo,
                    '' as consumibles,
                    '' as refacciones,
                    '' as poliza
                    from rentas as v
                    inner join clientes as cli on cli.id=v.idCliente
                    LEFT join personal as per on per.personalId=v.id_personal 
                    left join prefactura as pre on pre.ventaId=v.id and pre.tipo=2
                    WHERE v.estatus !=0 and v.viewed=1 $whare_v $whare_c $whare_p and pre.reg BETWEEN '$f1 00:00:00' AND '$f2 23:59:59'";

                
            }else if($tipoventa==4){

                $sql = "SELECT

                    vc.estatus,
                    vc.combinadaId id,
                    vc.prefactura,
                    vc.telefono,
                    vc.correo,
                    cli.empresa,
                    per.nombre vendedor,
                    'combinada' tabla,
                    4 as tipotabla,
                    vc.fechaentrega,
                    vc.fechaentregah,
                    vc.e_entrega,
                    vc.e_entrega_p,
                    vc.idCliente,
                    vc.statuspago,
                    vc.activo,
                    0 as id_rh,
                    0 as renta_rh,
                    pre.reg,
                    vc.id_personal,
                    vc.equipo,
                    vc.consumibles,
                    vc.refacciones,
                    vc.poliza
                    FROM ventacombinada vc
                    inner join clientes as cli on cli.id=vc.idCliente
                    LEFT join personal as per on per.personalId=vc.id_personal
                    left join prefactura as pre on pre.ventaId=vc.combinadaId and pre.tipo=3
                    WHERE vc.activo=1 and vc.viewed=1 $whare_vc $whare_c $whare_p and pre.reg BETWEEN '$f1 00:00:00' AND '$f2 23:59:59'";

            }else if($tipoventa==5){

                $sql = "SELECT 
                    0 as estatus,
                    pre.prefacturaId as id,
                    1 prefactura,
                    ren.telefono,
                    ren.correo,
                    cli.empresa,
                    per.nombre vendedor,
                    'rentas historial' as tabla,
                    5 as tipotabla,
                    rh.fechaentrega,
                    rh.fechaentregah,
                    rh.e_entrega,
                    rh.e_entrega_p,
                    ren.idCliente,
                    ren.statuspago,
                    rh.activo,
                    rh.id as id_rh,
                    rh.rentas as renta_rh,
                    pre.reg,
                    ren.id_personal,
                    '' as equipo,
                    '' as consumibles,
                    '' as refacciones,
                    '' as poliza
                FROM rentas_historial as rh
                INNER JOIN prefactura as pre on pre.ventaId=rh.id AND pre.tipo=4
                INNER JOIN rentas as ren on ren.id=rh.rentas
                INNER JOIN clientes as cli on cli.id=ren.idCliente
                LEFT join personal as per on per.personalId=ren.id_personal
                WHERE rh.activo=1 and rh.viewed=1 $whare_ren $whare_c $whare_p and pre.reg BETWEEN '$f1 00:00:00' AND '$f2 23:59:59'";
            }else if($tipoventa==3){

                $sql = "SELECT 
                        pol.estatus,
                        pol.id,
                        pol.prefactura,
                        pol.telefono,
                        pol.correo,
                        cli.empresa,
                        per.nombre as vendedor,
                        'Servicio' as tabla,
                        pol.fechaentrega,
                        pol.fechaentregah,
                        pol.e_entrega,
                        pol.e_entrega_p,
                        pol.idCliente,
                        pol.statuspago,
                        pol.activo,
                        0 as id_rh,
                        0 as renta_rh,
                        pre.reg,
                        pol.id_personal,
                        '' as equipo,
                        '' as consumibles,
                        '' as refacciones,
                        '' as poliza
                        FROM polizasCreadas as pol
                        INNER JOIN clientes as cli on cli.id=pol.idCliente
                        INNER JOIN personal as per on per.personalId=pol.id_personal
                        INNER JOIN prefactura as pre on pre.ventaId=pol.id AND pre.tipo=1
                        WHERE pol.activo=1 AND pol.combinada=0 $whare_pol $whare_c $whare_p AND pol.reg BETWEEN '$f1 00:00:00' AND '$f2 00:00:00'";
            }
        }
        $query = $this->db->query($sql);
        return $query->result();

    }    
}
?>