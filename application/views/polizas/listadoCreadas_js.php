<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/plugins/toastr/toastr.css">
<script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/toastr/toastr.min.js" ></script>

<link href="<?php echo base_url(); ?>public/plugins/switcher/switcher.css" rel="stylesheet">
<script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/switcher/jquery.switcher.js?v=<?php echo date('Ymd');?>" ></script>

<link href="<?php echo base_url(); ?>public/plugins/swich/on-off-switch.css" type="text/css" rel="stylesheet">

<link href="<?php echo base_url(); ?>public/plugins/select2/select2.min.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/select2/select2.full.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/plugins/touchspin/jquery.bootstrap-touchspin.min.css">
<script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/touchspin/jquery.bootstrap-touchspin.min.js" ></script>

<script type="text/javascript" src="<?php echo base_url(); ?>public/js/listado_polizasCreadas.js?v=<?php echo date('YmdGis');?>" ></script>
<script type="text/javascript" src="<?php echo base_url(); ?>public/js/addfacturaexterna.js?v=<?php echo date('YmdGis')?>" ></script>

<?php if(isset($_GET['pag'])){ 
	$tipo=$_GET['tipo'];
?>
<script type="text/javascript">
	$(document).ready(function($) {
		$('#ejecutivoselect').val(0).trigger("chosen:updated");
		setTimeout(function(){ 
			table.search(<?php echo $_GET['id'];?>).draw();
		}, 1000);
		setTimeout(function(){ 
			load();
		}, 2000);
		
	

	});
</script>
<?php } ?>
<script type="text/javascript">
	$(document).ready(function($) {
		setTimeout(function(){ 
			<?php foreach ($lispagosvp->result() as $item) { ?>
				$('#id_venta_p').val(<?php echo $item->polizaId;?>);
			    $('#fecha_p').val('');
			    $('#pago_p').val('');
			    $('#observacion_p').val('');
			    
			    $('.monto_prefactura').html('$ <span class="totalpoliza">'+monto_poliza(<?php echo $item->polizaId;?>)+'</span>');
			    table_tipo_compra(<?php echo $item->polizaId;?>);
			    setTimeout(function(){ 
			        calcularrestante();
			    }, 1000);




			<?php } ?>

		}, 1000);
	});
</script>