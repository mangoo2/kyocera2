<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Servicios extends CI_Controller 
{
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Servicios_model');
        $this->load->model('ModeloCatalogos');
        if($this->session->userdata('logeado')==true){
            $this->idpersonal=$this->session->userdata('idpersonal');
        }else{
            redirect('login');
        }
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
    }

    public function index(){   
        $this->load->view('header');
        $this->load->view('main');
        $this->load->view('Servicio/ListaAsigna');
        $this->load->view('Servicio/listado_asignaciones_js');
	}

    function add(){
        $data['bodegas']=$this->ModeloCatalogos->getselectwheren('bodegas',array('activo' => 1));
        $data['consumibles']=$this->ModeloCatalogos->getselectwheren('consumibles',array('status' => 1));
        $data['accesorios']=$this->ModeloCatalogos->getselectwheren('catalogo_accesorios',array('status' => 1));
        $data['equipos']=$this->ModeloCatalogos->getselectwheren('equipos',array('estatus' => 1));
        $data['refacciones']=$this->ModeloCatalogos->getselectwheren('refacciones',array('status' => 1));
        $this->load->view('header');
        $this->load->view('main',$data);
        $this->load->view('series/addc');
        $this->load->view('footer');
        $this->load->view('series/addcjs');
    }
    public function getDatacompras() {
        $params = $this->input->post();
        $serie = $this->Compras_model->Getseries_compras($params);
        $totalRecords= $this->Compras_model->TotalSeriecompras($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totalRecords),  
            "recordsFiltered" => intval($totalRecords),
            "data"            => $serie->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }

}

?>