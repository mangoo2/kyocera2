var addfactura_idv=0;
var addfactura_cliente=0;
var addfactura_tipo=0;
var tablef;
$(document).ready(function($) {
    tablef = $('#tabla_facturacion').DataTable();
});
function addfactura(id,idCliente,tipo,total,tipov){
    
    var html_contect='Se necesita permisos de administrador<br>'+
        '<input type="text" placeholder="Contraseña" id="contrasena3" name="contrasena3" class="name form-control-bmz" style="width:99%"  onpaste="return false;" required/>';
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: html_contect,
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                
                    //var pass=$('#contrasena').val();
                    var pass=$("input[name=contrasena3]").val();
                    if (pass!='') {
                         $.ajax({
                            type:'POST',
                            url: base_url+"index.php/Sistema/solicitarpermiso",
                            data: {
                                pass:pass
                            },
                            success: function (response){
                                    var respuesta = parseInt(response);
                                    if (respuesta==1) {
                                        addfactura2(id,idCliente,tipo,total,tipov);
                                    }else{
                                        notificacion(2);
                                        
                                    }
                            },
                            error: function(response){
                                notificacion(1);
                                
                            }
                        });
                        
                    }else{
                        notificacion(0);
                        
                    }
                
                
            },
            cancelar: function (){
                
            }
        }
    });
    setTimeout(function(){
        new MaskedPassword(document.getElementById("contrasena3"), '\u25CF');
        
    },1000);
}
function addfactura2(id,idCliente,tipo,total,tipov){
    var seriesselected='U';
    $('#totalgeneralvalor').val(total);
    $('#modal_addfactura').modal();
    $('#modal_addfactura').modal('open');
    addfactura_idv=id;
    addfactura_cliente=idCliente;
    addfactura_tipo=tipo;
    if(tipov==2){
        series='D';
    }else{
        series='U';
    }
    loadtablef(series);
}
function loadtablef(seriesselected){
    console.log(seriesselected);
    tablef.destroy();
    tablef = $('#tabla_facturacion').DataTable({
        //stateSave: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url+"index.php/Facturaslis/getlistfacturas",
            type: "post",
            "data": {
                'personal':0,
                'cliente':addfactura_cliente,
                'finicio':'',
                'ffin':'',
                'idclienterfc':0,
                'rs_folios':seriesselected,
                'metodopago':0,
                'tcfdi':0
            },
        },
        "columns": [
            {"data": "FacturasId",
                render:function(data,type,row){
                    var html='';
                    if(row.Estado==1){
                        html='<input type="checkbox" class="filled-in facturas_checked" id="factura_'+row.FacturasId+'" value="'+row.FacturasId+'" data-total="'+row.total+'"><label for="factura_'+row.FacturasId+'"></label>';
                    }else{
                        html='<input type="checkbox" class="filled-in facturas_checked" id="factura_'+row.FacturasId+'" value="'+row.FacturasId+'" disabled><label for="factura_'+row.FacturasId+'"></label>';
                    }
                    return html;
                }
            },
            {"data": "Folio",
                render:function(data,type,row){
                    var html ='';
                        var serie=row.cadenaoriginal;
                        serie=serie.split('|');
                        if (serie[3]==undefined) {
                            var seriev='';
                        }else{
                            var seriev=serie[3];
                        }
                        html=seriev+''+row.Folio.padStart(4, 0);
                    return html;
                }
            },
            {"data": "Nombre"},
            {"data": "Rfc",},
            {"data": "total",
                render:function(data,type,row){
                    var html='';
                        
                    html=new Intl.NumberFormat('es-MX',{style: "currency", currency: "MXN"}).format(row.total);
                    return html;
                }
            },
            {"data": "Estado",
                render:function(data,type,row){
                    var html='';
                    if(row.Estado==0){
                        html='Cancelada';
                    }else if(row.Estado==1){
                        html='Facturado';
                    }else if(row.Estado==2){
                        html='Sin Facturar';
                    }else{
                        html='';
                    }
                    return html;
                }
            },
            {"data": "fechatimbre"},
            {"data": null,
                 render:function(data,type,row){
                    $('.tooltipped').tooltip();
                    var html='<div class="btns-factura">';
                    if(row.Estado==0){ // cancelado
                        html+='<a class="b-btn b-btn-primary tooltipped" \
                                href="'+base_url+'Configuracionrentas/generarfacturas/'+row.FacturasId+'/1" \
                                target="_blank"\
                                data-position="top" data-delay="50" data-tooltip="Factura"\
                              ><i class="fas fa-file-alt fa-2x"></i>\
                            </a> ';
                        html+='<a class="b-btn b-btn-primary tooltipped" \
                                    href="'+base_url+row.rutaXml+'" \
                                    target="_blank"\
                                    data-position="top" data-delay="50" data-tooltip="XML"\
                                  download ><i class="far fa-file-code fa-2x"></i>\
                                    </a> ';
                        html+='<a class="b-btn b-btn-primary tooltipped" \
                                    href="'+base_url+'kyocera/facturas/'+row.rutaAcuseCancelacion+'" \
                                    target="_blank"\
                                    data-position="top" data-delay="50" data-tooltip="Acuse de cancelaciÃƒÂ³n"\
                                  download ><i class="fas fa-file-invoice fa-2x" style="color: #ef7e4f;"></i>\
                                    </a> ';
                    }else if(row.Estado==1){
                        html+='<a class="b-btn b-btn-primary tooltipped" \
                                href="'+base_url+'Configuracionrentas/generarfacturas/'+row.FacturasId+'/1" \
                                target="_blank"\
                                data-position="top" data-delay="50" data-tooltip="Factura"\
                              ><i class="fas fa-file-alt fa-2x"></i>\
                            </a> ';
                        /*
                        html+='<a\
                                    class="b-btn b-btn-primary tooltipped" \
                                    href="'+base_url+row.rutaXml+'" \
                                    target="_blank"\
                                    data-position="top" data-delay="50" data-tooltip="XML"\
                                  download ><i class="far fa-file-code fa-2x"></i>\
                                </a> ';
                        */
                    }else if(row.Estado==2){
                        
                    }else{
                        html+='';
                    }
                    html+='</div>';
                    return html;
                }
            }
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[25, 50, 100], [25, 50,100]]
        
    });
}


function agregarfacturas(){
    var totalgvalor=$('#totalgeneralvalor').val();
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Esta seguro de agregar la factura al venta?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                //===================================================
                var facturaslis = $("#tabla_facturacion tbody > tr");
                var DATAa  = [];
                var num_facturas_selected=0;
                var totalfacturas=0;
                facturaslis.each(function(){  
                    if ($(this).find("input[class*='facturas_checked']").is(':checked')) {
                        num_facturas_selected++;
                        item = {};                    
                        item ["FacturasIds"]  = $(this).find("input[class*='facturas_checked']").val();
                        var totalfac  = $(this).find("input[class*='facturas_checked']").data('total');
                        totalfacturas+=Number(totalfac);
                        DATAa.push(item);
                    }       
                    
                });
                INFOa  = new FormData();
                aInfoa   = JSON.stringify(DATAa);
                //console.log(aInfoa);
                if (num_facturas_selected==1) {
                    //if(totalfacturas<=totalgvalor){
                    if(true){ 
                        $.ajax({
                            type:'POST',
                            url: base_url+"index.php/Generales/addfacturavcp",
                            data: {
                                facturas:aInfoa,
                                idvcp:addfactura_idv,
                                tipo:addfactura_tipo
                            },
                            success:function(response){ 
                                var numfac =parseInt(response);
                                if(numfac==0){
                                    toastr["info"]("Factura ya se encuentra viculada");
                                } 
                                if(numfac==1){
                                    toastr["success"]("Se ha Agregado la factura", "éxito");
                                } 
                                
                                loadtable();

                            }
                        });
                    }else{
                        toastr["warning"]("el total de la factura debe de ser igual o menor a la prefactura", "");
                    }
                }else{
                    toastr["warning"]("Seleccione solo una factura", "Advertencia");
                }
                //================================================
            },
            cancelar: function (){
                
            }
        }
    });
}
function cambiarestatuspago(monto){
    console.log('montos:'+monto);
    var DATAa  = [];
    var TABLA   = $("#tablepagosg tbody > tr");
        TABLA.each(function(){         
        item = {};
        item ["idventapoliza"] = $(this).find("input[id*='idventapoliza']").val();
        item ["tipo"] = $(this).find("input[id*='idventapoliza']").data('tipovp');
        DATAa.push(item);
    });
    INFO  = new FormData();
    cvcp   = JSON.stringify(DATAa);
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Generales/cambiarestatuspago",
        data: {
            cvcp:cvcp,
            montos:monto
        },
        success:function(response){  
         
        }
    });
}