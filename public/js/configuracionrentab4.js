var base_url = $('#base_url').val();
var table;
var tablepre=0;
$(document).ready(function($) {
	$('#pcliente').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una opción',
        allowClear: true,
        ajax: {
            url: base_url+'Ventasi/searchclientes',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.id,
                        text: element.empresa

                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    	}).on('select2:select', function (e) {
          clientecontrato();
    });
    $('.exportar').click(function(event) {
    	var presepcto=$('#idfactura_f option:selected').val();
    	if (presepcto>0) {
    		$.ajax({
			    type:'POST',
			    url: base_url+"index.php/Configuracionrentas/exportarcontsobraresibe",
			    data: {
			    	periodoini:$('#periodoexport').val(),
			    	cotamono:$('#contadorexportmono').val(),
			    	contacolor:$('#contadorexportcolor').val(),
			    	periodoreseptor:presepcto
			    },
			    success:function(data){  
			    	$('#test_tab_5').click();

			    }
		    });
    	}else{
    	}
    });
    $('.modal').modal();
    table = $('#tabla_facturacion').DataTable();
    cargaequiposs();

  	$('.descuento_readonly').click(function(event) {
  		var isrealy=$(this).is('[readonly]');
  		var isr_equipo=$(this).data('equipo');
  		var isr_serie=$(this).data('serie');
  		var isr_moco=$(this).data('moco');
  		//console.log(isrealy);
  		if(isrealy==true){
  			$.confirm({
                boxWidth: '30%',
                useBootstrap: false,
                icon: 'fa fa-warning',
                title: 'Atención!',
                content: 'Se necesita permisos de administrador<br>'+
                         '<input type="password" placeholder="Contraseña" id="contrasena" class="name form-control" required/>',
                type: 'red',
                typeAnimated: true,
                buttons:{
                    confirmar: function (){
                        var pass=$('#contrasena').val();
                        if (pass!='') {
                            $.ajax({
                                type:'POST',
                                url: base_url+"index.php/Sistema/solicitarpermiso",
                                data: {
                                    pass:pass
                                },
                                success: function (response){
                                        var respuesta = parseInt(response);
                                        if (respuesta==1) {
                            //=============================================================================
                                $('.descuento_'+isr_equipo+'_'+isr_serie+'_'+isr_moco).attr('readonly', false);
                                $('.descuentoe_'+isr_equipo+'_'+isr_serie+'_'+isr_moco).attr('readonly', false);
                            //=============================================================================
                                        }else{
                                            notificacion(2);
                                        }
                                },
                                error: function(response){
                                    notificacion(1);
                                }
                            });

                        }else{
                            notificacion(0);
                        }
                        
                    },
                    cancelar: function () {
                        
                    }
                }
            });
  		}
  	});
});
function  dataprefactura() {	
	$('.addprefactura').prop( "disabled", false);
	var id_renta = $('#id_renta').val();
	$.ajax({
	    type:'POST',
	    url: base_url+"index.php/Configuracionrentas/datosprefactura",
	    data: {
	    	idrenta:id_renta,
	    },
	    success:function(data){  
	    	//console.log('infor factura: '+data);
	    	var array = $.parseJSON(data);
	    	$('#periodo_inicial').val(array.periodoinicial);
	    	$('#periodo_final').val(array.periodofinal);
	    	$('.periodon').val(array.periodon);
	    	$('#periodoex').val(array.periodone);
	    	$('#periodoex_e').val(array.periodone);
	    	//$('.total_toner').val(array.toner_consumido/100);
	    	var costoperiodo=array.rentadeposito;
	    	var rentacolor=array.rentacolor;
	    	
	    	array.equipos.forEach(function(element) {

	    		if (array.statusnuevo==0) {
	    			$('.f_c_cont_ini_'+element.id_equipo+'_'+element.serieId+'_'+element.tipo).val(element.contador_inicial);
	    			$('.f_e_cont_ini_'+element.id_equipo+'_'+element.serieId+'_'+element.tipo).val(element.contador_iniciale);
	    			totaltoner(element.id_equipo,element.serieId,element.tipo);
	    			var suspendido=$('.f_c_cont_fin_'+element.id_equipo+'_'+element.serieId+'_'+element.tipo).data('suspendido');
	    				suspendido=parseInt(suspendido);
	    			if (suspendido==1) {
	    				calculatdatosproductos();
	    				$('.f_c_cont_fin_'+element.id_equipo+'_'+element.serieId+'_'+element.tipo).val(element.contador_inicial).prop('readonly', true).change();
	    				$('.trcsss_'+element.id_equipo+'_'+element.serieId+'_'+element.tipo).addClass('equiposuspendido');
	    			}else{
	    				$('.f_c_cont_fin_'+element.id_equipo+'_'+element.serieId+'_'+element.tipo).prop('readonly', false);
	    				$('.trcsss_'+element.id_equipo+'_'+element.serieId+'_'+element.tipo).removeClass('equiposuspendido');
	    			}
	    			if (suspendido==1) {
	    				calculatdatosproductos();
	    				$('.f_e_cont_fin_'+element.id_equipo+'_'+element.serieId+'_'+element.tipo).val(element.contador_iniciale).prop('readonly', true).change();
	    				$('.trcsss_'+element.id_equipo+'_'+element.serieId+'_'+element.tipo).addClass('equiposuspendido');
	    			}else{
	    				$('.f_e_cont_fin_'+element.id_equipo+'_'+element.serieId+'_'+element.tipo).prop('readonly', false);
	    				$('.trcsss_'+element.id_equipo+'_'+element.serieId+'_'+element.tipo).removeClass('equiposuspendido');
	    			}
	    		}else{
	    			totaltoner(element.productoId,element.serieId,element.tipo);
	    			var suspendido=$('.f_c_cont_fin_'+element.productoId+'_'+element.serieId+'_'+element.tipo).data('suspendido');
	    				suspendido=parseInt(suspendido);
	    			console.log('suspendido:'+suspendido);
	    			if (suspendido==1) {
	    				calculatdatosproductos();
	    				$('.f_c_cont_ini_'+element.productoId+'_'+element.serieId+'_'+element.tipo).val(parseFloat(element.c_c_f));
	    				$('.f_c_cont_fin_'+element.productoId+'_'+element.serieId+'_'+element.tipo).val(parseFloat(element.c_c_f)).prop('readonly', true).change();
	    				$('.trcsss_'+element.productoId+'_'+element.serieId+'_'+element.tipo).addClass('equiposuspendido');
	    			}else{
	    				$('.f_c_cont_ini_'+element.productoId+'_'+element.serieId+'_'+element.tipo).val(parseFloat(element.c_c_f)+1);
	    				$('.f_c_cont_fin_'+element.productoId+'_'+element.serieId+'_'+element.tipo).prop('readonly', false);
	    				$('.trcsss_'+element.productoId+'_'+element.serieId+'_'+element.tipo).removeClass('equiposuspendido');
	    			}
	    			if (suspendido==1) {
	    				calculatdatosproductos();
	    				$('.f_e_cont_ini_'+element.productoId+'_'+element.serieId+'_'+element.tipo).val(parseFloat(element.e_c_f));
	    				$('.f_e_cont_fin_'+element.productoId+'_'+element.serieId+'_'+element.tipo).val(parseFloat(element.e_c_f)).prop('readonly', true).change();
	    				$('.trcsss_'+element.productoId+'_'+element.serieId+'_'+element.tipo).addClass('equiposuspendido');
	    			}else{
	    				if (element.e_c_f==0) {
	    					var e_c_f_mas=0;
		    			}else{
		    				var e_c_f_mas=1;
		    			}
	    				$('.f_e_cont_ini_'+element.productoId+'_'+element.serieId+'_'+element.tipo).val(parseFloat(element.e_c_f)+e_c_f_mas);
	    				$('.f_e_cont_fin_'+element.productoId+'_'+element.serieId+'_'+element.tipo).prop('readonly', false);
	    				$('.trcsss_'+element.id_equipo+'_'+element.serieId+'_'+element.tipo).removeClass('equiposuspendido');
	    			}
	    			//$('.td_orden_'+element.productoId+'_'+element.serieId+'_'+element.tipo).append('<span class="span_orden">'+element.orden+'</span>');
	    		}
	    		
	    	});
	    	array.equiposanteriores.forEach(function(elemento) {
	    				if ($('.f_c_cont_ini_'+elemento.id_equipo+'_'+elemento.serieId+'_'+elemento.tipo).val()==='') {
	    					$('.f_c_cont_ini_'+elemento.id_equipo+'_'+elemento.serieId+'_'+elemento.tipo).val(elemento.contador_inicial);
	    					$('.f_e_cont_ini_'+elemento.id_equipo+'_'+elemento.serieId+'_'+elemento.tipo).val(elemento.contador_iniciale);
	    				}

	    			});
	    	/*array.toners_cant.forEach(function(element) {
	    		console.log("toner_cons: "+'total_toner_'+element.id_equipo+'_'+element.serieId+'_'+element.tipo)
	    		$('.total_toner_'+element.id_equipo+'_'+element.serieId+'_'+element.tipo).val(array.toner_consumido/100);
	    	});*/
	    	if(array.statusnuevo==0){
	    		$('.excedentesmono').hide();
		    	$('.excedentescolor').hide();
	    	}else{
	    		if (array.tipo4==1) {
		    		$('.excedentesmono').show();
		    		$('.excedentescolor').hide();
		    	}else if (array.tipo4==2){
		    		$('.excedentesmono').show();
		    		$('.excedentescolor').show();
		    	}else if (array.tipo4==3){
		    		$('.excedentesmono').show();
		    		$('.excedentescolor').show();
		    	}else if (array.tipo4==4){
		    		$('.excedentesmono').hide();
		    		$('.excedentescolor').show();
		    	}
	    	}
	    	
	    	if (array.tipo2==1) {
	    		$('.removetipo2').show();
	    	}else{
	    		$('.removetipo2').hide();
	    	}
	    	$('#costoperiodoexcedente').val(0);
			$('#costoperiodoexcedente_c').val(0);
	    	$('#costoperiodo').val(costoperiodo);
	    	$('#costoperiodocolor').val(rentacolor);
	    	if (rentacolor<=0) {
	    		$('.removecostoperiodocolor').hide();
	    	}
	    	calculartotalperiodo();
	    	$('.removeproduccion').hide();
	    }
    });
    setTimeout(function(){
    	calculatdatosproductos();
    	//verificaimg();
    }, 1000);
    
	if(tablepre==0){
		setTimeout(function(){ 
			var tprofactura = $('#profactura');
	
			tprofactura.DataTable({
				/*fixedHeader: true,*/
                "lengthMenu": [[-1], ["All"]],
                "searching": false,
                "paging": false,
                "info": false
            });

            tprofactura.floatThead();
			tprofactura.trigger('reflow');
            
		}, 2000);
		
	}
	tablepre++;
	
}
function  dataprefactura0() {	
	var descuento_g_clicks=$('#descuento_g_clicks').val()==undefined?0:$('#descuento_g_clicks').val();
	$('#descuento_g_clicks2').val(descuento_g_clicks);
	var descuento_g_scaneo=$('#descuento_g_scaneo').val()==undefined?0:$('#descuento_g_scaneo').val();
	$('#descuento_g_scaneo2').val(descuento_g_scaneo);
	$('.addprefactura').prop( "disabled", false);
	var id_renta = $('#id_renta').val();
	var indiglob=$('#tipo2 option:selected').val();
	if(indiglob==1){
		$('.indiglob_des').hide();
	}
	$.ajax({
	    type:'POST',
	    url: base_url+"index.php/Configuracionrentas/datosprefactura0",
	    data: {
	    	idrenta:id_renta,
	    },
	    success:function(data){  
	    	//console.log('infor factura: '+data);
	    	var array = $.parseJSON(data);
	    	$('#periodo_inicial').val(array.periodoinicial);
	    	$('#periodo_final').val(array.periodofinal);
	    	$('.periodon').val(array.periodon);
	    	$('#periodoex').val(array.periodone);
	    	$('#periodoex_e').val(array.periodone);
	    	//$('.total_toner').val(array.toner_consumido/100);
	    	var costoperiodo=array.rentadeposito;
	    	var rentacolor=array.rentacolor;
	    	
	    	array.equipos.forEach(function(element) {

	    		if (array.statusnuevo==0) {
	    			$('.f_c_cont_ini_'+element.id_equipo+'_'+element.serieId+'_'+element.tipo).val(element.contador_inicial);
	    			$('.f_e_cont_ini_'+element.id_equipo+'_'+element.serieId+'_'+element.tipo).val(element.contador_iniciale);
	    			totaltoner(element.id_equipo,element.serieId,element.tipo);
	    			var suspendido=$('.f_c_cont_fin_'+element.id_equipo+'_'+element.serieId+'_'+element.tipo).data('suspendido');
	    				suspendido=parseInt(suspendido);
	    			if (suspendido==1) {
	    				calculatdatosproductos();
	    				$('.f_c_cont_fin_'+element.id_equipo+'_'+element.serieId+'_'+element.tipo).val(element.contador_inicial).prop('readonly', true).change();
	    				$('.trcsss_'+element.id_equipo+'_'+element.serieId+'_'+element.tipo).addClass('equiposuspendido');
	    			}else{
	    				$('.f_c_cont_fin_'+element.id_equipo+'_'+element.serieId+'_'+element.tipo).prop('readonly', false);
	    				$('.trcsss_'+element.id_equipo+'_'+element.serieId+'_'+element.tipo).removeClass('equiposuspendido');
	    			}
	    			if (suspendido==1) {
	    				calculatdatosproductos();
	    				$('.f_e_cont_fin_'+element.id_equipo+'_'+element.serieId+'_'+element.tipo).val(element.contador_iniciale).prop('readonly', true).change();
	    				$('.trcsss_'+element.id_equipo+'_'+element.serieId+'_'+element.tipo).addClass('equiposuspendido');
	    			}else{
	    				$('.f_e_cont_fin_'+element.id_equipo+'_'+element.serieId+'_'+element.tipo).prop('readonly', false);
	    				$('.trcsss_'+element.id_equipo+'_'+element.serieId+'_'+element.tipo).removeClass('equiposuspendido');
	    			}
	    			
	    		}else{

	    			if(element.produccion_total==0){
	    				var sin_pro=' sin_pro ';
	    				$('.trcsss_'+element.productoId+'_'+element.serieId+'_'+element.tipo).addClass('sin_pro');
	    				setTimeout(function(){ 
		    				var poreliminar = $('.img_captura_'+element.productoId+'_'+element.serieId).data('poreliminar');
		    				console.log('poreliminar '+'.img_captura_'+element.productoId+'_'+element.serieId+' poreliminar'+poreliminar);
		    				if(poreliminar==1){
		    					var poreli='<a class="infoequiporetiro" onclick="infoequiporetiro('+element.productoId+','+element.serieId+')"><i class="fas fa-info-circle"></i></a>';
		    					$('.img_captura_'+element.productoId+'_'+element.serieId).html(poreli);
		    					
		    				}
	    				}, 1600);
	    			}
	    			totaltoner(element.productoId,element.serieId,element.tipo);
	    			var suspendido=$('.f_c_cont_fin_'+element.productoId+'_'+element.serieId+'_'+element.tipo).data('suspendido');
	    				suspendido=parseInt(suspendido);
	    			console.log('suspendido:'+suspendido);
	    			if (suspendido==1) {
	    				calculatdatosproductos();
	    				$('.f_c_cont_ini_'+element.productoId+'_'+element.serieId+'_'+element.tipo).val(parseFloat(element.c_c_f));
	    				$('.f_c_cont_fin_'+element.productoId+'_'+element.serieId+'_'+element.tipo).val(parseFloat(element.c_c_f)).prop('readonly', true).change();
	    				$('.trcsss_'+element.productoId+'_'+element.serieId+'_'+element.tipo).addClass('equiposuspendido');
	    			}else{
	    				$('.f_c_cont_ini_'+element.productoId+'_'+element.serieId+'_'+element.tipo).val(parseFloat(element.c_c_f)+1);
	    				$('.f_c_cont_fin_'+element.productoId+'_'+element.serieId+'_'+element.tipo).prop('readonly', false);
	    				$('.trcsss_'+element.productoId+'_'+element.serieId+'_'+element.tipo).removeClass('equiposuspendido');
	    			}
	    			if (suspendido==1) {
	    				calculatdatosproductos();
	    				$('.f_e_cont_ini_'+element.productoId+'_'+element.serieId+'_'+element.tipo).val(parseFloat(element.e_c_f));
	    				$('.f_e_cont_fin_'+element.productoId+'_'+element.serieId+'_'+element.tipo).val(parseFloat(element.e_c_f)).prop('readonly', true).change();
	    				$('.trcsss_'+element.productoId+'_'+element.serieId+'_'+element.tipo).addClass('equiposuspendido');
	    			}else{
	    				if (element.e_c_f==0) {
	    					var e_c_f_mas=0;
		    			}else{
		    				var e_c_f_mas=1;
		    			}
	    				$('.f_e_cont_ini_'+element.productoId+'_'+element.serieId+'_'+element.tipo).val(parseFloat(element.e_c_f)+e_c_f_mas);
	    				$('.f_e_cont_fin_'+element.productoId+'_'+element.serieId+'_'+element.tipo).prop('readonly', false);
	    				$('.trcsss_'+element.id_equipo+'_'+element.serieId+'_'+element.tipo).removeClass('equiposuspendido');
	    			}
	    			//$('.td_orden_'+element.productoId+'_'+element.serieId+'_'+element.tipo).append('<span class="span_orden">'+element.orden+'</span>');
	    		}
	    		
	    	});
	    	array.equiposanteriores.forEach(function(elemento) {
	    				if ($('.f_c_cont_ini_'+elemento.id_equipo+'_'+elemento.serieId+'_'+elemento.tipo).val()==='') {
	    					$('.f_c_cont_ini_'+elemento.id_equipo+'_'+elemento.serieId+'_'+elemento.tipo).val(elemento.contador_inicial);
	    					$('.f_e_cont_ini_'+elemento.id_equipo+'_'+elemento.serieId+'_'+elemento.tipo).val(elemento.contador_iniciale);
	    				}

	    			});
	    	/*array.toners_cant.forEach(function(element) {
	    		console.log("toner_cons: "+'total_toner_'+element.id_equipo+'_'+element.serieId+'_'+element.tipo)
	    		$('.total_toner_'+element.id_equipo+'_'+element.serieId+'_'+element.tipo).val(array.toner_consumido/100);
	    	});*/
	    	if(array.statusnuevo==0){
	    		$('.excedentesmono').hide();
		    	$('.excedentescolor').hide();
	    	}else{
	    		if (array.tipo4==1) {
		    		$('.excedentesmono').show();
		    		$('.excedentescolor').hide();
		    	}else if (array.tipo4==2){
		    		$('.excedentesmono').show();
		    		$('.excedentescolor').show();
		    	}else if (array.tipo4==3){
		    		$('.excedentesmono').show();
		    		$('.excedentescolor').show();
		    	}else if (array.tipo4==4){
		    		$('.excedentesmono').hide();
		    		$('.excedentescolor').show();
		    	}
	    	}
	    	
	    	if (array.tipo2==1) {
	    		$('.removetipo2').show();
	    	}else{
	    		$('.removetipo2').hide();
	    	}
	    	$('#costoperiodoexcedente').val(0);
			$('#costoperiodoexcedente_c').val(0);
	    	$('#costoperiodo').val(costoperiodo);
	    	$('#costoperiodocolor').val(rentacolor);
	    	if (rentacolor<=0) {
	    		$('.removecostoperiodocolor').hide();
	    	}
	    	calculartotalperiodo();
	    	$('.removeproduccion').hide();

	    }
    });
    setTimeout(function(){
    	calculatdatosproductos();
    	//verificaimg();
    	verificar_serie_carga();
    	verificar_contadorinicial();
    }, 1000);
    
	if(tablepre==0){
		setTimeout(function(){ 
			var tprofactura = $('#profactura');
	
			tprofactura.DataTable({
				/*fixedHeader: true,*/
                "lengthMenu": [[-1], ["All"]],
                "searching": false,
                "paging": false,
                "info": false
            });

            tprofactura.floatThead();
			tprofactura.trigger('reflow');
            
		}, 2000);
		
	}
	tablepre++;
	
}
function totaltoner(id_equipo,serieId,tipo){
	console.log('id_equipo: '+id_equipo);
	$.ajax({
	    type:'POST',
	    url: base_url+"index.php/Configuracionrentas/datosToner",
	    data: {
	    	idequipo:id_equipo,serie:serieId,tipoT:tipo
	    },
	    success:function(data){ 
	    	//var array = $.parseJSON(data);
	    	//console.log(data);
	    	//array.toners_cant.forEach(function(element) {
	    		//console.log("toner_cons: "+array.toner_consumido);
	    		$('.total_toner_'+id_equipo+'_'+serieId+'_'+tipo).val(parseFloat(data)/100);
	    	//});
	    }
    });

}
function calculartotalperiodo(){

	setTimeout(function(){ 
		var periodo =$('#costoperiodo').val();
		var periodoc =$('#costoperiodocolor').val();
		var periodoext =$('#costoperiodoexcedente').val();
		var periodoextc =$('#costoperiodoexcedente_c').val();
		var total= parseFloat(periodo)+parseFloat(periodoc)+parseFloat(periodoext)+parseFloat(periodoextc);
		$('#totalperiodo').val(total);
	}, 1000);	
}
function calculatdatosproductos(){
	
	$('.prfprocontador').change(function(event) {
		
		var equipo=$(this).data('equipo');
		var serie=$(this).data('serie');
		var moco=$(this).data('moco');
		var tipo=$(this).data('tipo');
		var tomarescaner=$(this).data('tomarescaner');
		var cci=$('.f_c_cont_ini_'+equipo+'_'+serie+'_'+moco).val()==''?0:$('.f_c_cont_ini_'+equipo+'_'+serie+'_'+moco).val();
		var ccf=$('.f_c_cont_fin_'+equipo+'_'+serie+'_'+moco).val()==''?0:$('.f_c_cont_fin_'+equipo+'_'+serie+'_'+moco).val();
		if (tomarescaner==1) {
			var eci=$('.f_e_cont_ini_'+equipo+'_'+serie+'_'+moco).val()==''?0:$('.f_e_cont_ini_'+equipo+'_'+serie+'_'+moco).val();
			var ecf=$('.f_e_cont_fin_'+equipo+'_'+serie+'_'+moco).val()==''?0:$('.f_e_cont_fin_'+equipo+'_'+serie+'_'+moco).val();
		}else{
			var eci=0;
			var ecf=0;
		}
		
		var producccion_copia = parseFloat(ccf)-parseFloat(cci);
		$('.f_c_cont_pro_'+equipo+'_'+serie+'_'+moco).val(producccion_copia);
		var producccion_escaner = parseFloat(ecf)-parseFloat(eci);
		$('.f_e_cont_pro_'+equipo+'_'+serie+'_'+moco).val(producccion_escaner);

		var desc=$('.descuento_'+equipo+'_'+serie+'_'+moco).val()==''?0:$('.descuento_'+equipo+'_'+serie+'_'+moco).val();
		var desce=$('.descuentoe_'+equipo+'_'+serie+'_'+moco).val()==''?0:$('.descuentoe_'+equipo+'_'+serie+'_'+moco).val();

		var produccion = parseFloat(producccion_copia)+parseFloat(producccion_escaner)-parseFloat(desc)-parseFloat(desce);
		$('.f_produccion_'+equipo+'_'+serie+'_'+moco).val(produccion);
		var rendimientox=$('.nuevo_'+equipo+'_'+serie+'_'+tipo).val();
		//var tonerconsumido=(parseFloat(ccf)-parseFloat(cci))/$('.nuevo_'+equipo+'_'+serie+'_'+tipo).val();
		var tonerconsumido=(parseFloat(ccf)-parseFloat(cci))/rendimientox;
		tonerconsumido=parseFloat(tonerconsumido)*100;
		$('.f_toner_consumido_'+equipo+'_'+serie+'_'+moco).val(Math.round(tonerconsumido));
		$('.produccion_total_'+equipo+'_'+serie+'_'+moco).val(produccion);//tengo dudas con este
		//$('.total_toner'+equipo+'_'+serie+'_'+moco).val(produccion); //aqui estoy con el calculo de toner consumido
		
		if ($('#tipo2 option:selected').val()==1) {
			var cantidad = $('.f_produccion_'+equipo+'_'+serie+'_'+moco).val();
			var clicks_mono = $('.clicks_mono_'+equipo+'_'+serie).val();
			var precio_c_e_mono = $('.precio_c_e_mono_'+equipo+'_'+serie).val();

			var clicks_color = $('.clicks_color_'+equipo+'_'+serie).val();
			var precio_c_e_color = $('.precio_c_e_color_'+equipo+'_'+serie).val();

			if (moco==1) {
				var excedente =parseFloat(cantidad)-parseFloat(clicks_mono);
				var excedentescosto= precio_c_e_mono;
			}else{
				var excedente =parseFloat(cantidad)-parseFloat(clicks_color);
				var excedentescosto= precio_c_e_color;
			}
			var excedentetotal=parseFloat(excedente)*parseFloat(excedentescosto);
			if (excedente<0) {
				excedente=0;
				excedentetotal=0;
			}

			$('.excedentetotal_'+equipo+'_'+serie+'_'+moco).val(excedentetotal.toFixed(2));
			$('.excedentescosto_'+equipo+'_'+serie+'_'+moco).val(excedentescosto);
			$('.excedentes_'+equipo+'_'+serie+'_'+moco).val(excedente);
		}
		calculartotalp();
		caldesglobal();

	});
}
function calculartotalp(){
    var descuento_g_clicks=$('#descuento_g_clicks').val();
    var descuento_g_scaneo=$('#descuento_g_scaneo').val();
    //var descuento_g=parseInt(descuento_g_clicks)+parseInt(descuento_g_scaneo);
    var produccion_c=0;
    var produccion_m=0;

    var addtpc = 0;
    $(".produccion_color").each(function() {
        var vstotalc = $(this).val();
        addtpc += Number(vstotalc);
        produccion_c=1;
    });
    var addtpm = 0;
    $(".produccion_mono").each(function() {
        var vstotalm = $(this).val();
        addtpm += Number(vstotalm);
        produccion_m=1;
    });
    //===========================================
    	addtpm=parseInt(addtpm)-parseInt(descuento_g_scaneo);
    	if(addtpm<0){
    		addtpm=0;
    	}
    	addtpm=parseInt(addtpm)-parseInt(descuento_g_clicks);
    	//var produccion_t=parseInt(produccion_c)+parseInt(produccion_m);
    //===========================================

    if (parseInt($('#tipo2 option:selected').val())==2) {
    	var clicks_mono = parseFloat($('input[name=clicks_mono]').val());
    	var precio_monoex = parseFloat($('input[name=precio_c_e_mono]').val());
    	var monto_exc_mono=parseFloat(addtpm)-parseFloat(clicks_mono);
    		if(monto_exc_mono<0){
    			monto_exc_mono=0;
    		}
    	var excedente = (monto_exc_mono)*parseFloat(precio_monoex);
		if (excedente<0) {
			excedente=0;
		}
		var clicks_color = parseFloat($('input[name=clicks_color]').val());
    	var precio_colorex = parseFloat($('input[name=precio_c_e_color]').val());

    	var monto_exc_color=parseFloat(addtpc)-parseFloat(clicks_color);
    		if(monto_exc_color<0){
    			monto_exc_color=0;
    		}

    	var excedentec = (monto_exc_color)*parseFloat(precio_colorex);
		if (excedentec<0) {
			excedentec=0;
		}
    }else{
    		var monto_exc_mono=0;
    		var monto_exc_color=0;
    		$(".exc_mono").each(function() {
	        	var exc_mono = $(this).val();
		        monto_exc_mono += Number(exc_mono);
		    });
		    $(".exc_color").each(function() {
	        	var exc_color = $(this).val();
		        monto_exc_color += Number(exc_color);
		    });
    		//===============================
	    	var excedente = 0;
	    	$(".excedente_mono").each(function() {
	        var vstotalm = $(this).val();
		        excedente += Number(vstotalm);
		    });
		    var excedentec = 0;
		    $(".excedente_color").each(function() {
		        var vstotalc = $(this).val();
		        excedentec += Number(vstotalc);
		    });
    }
    $('#costoperiodoexcedente').val(excedente);
    $('#costoperiodoexcedente').val(excedente);
    $('.exc_mono').val(monto_exc_mono);
    $('.exc_color').val(monto_exc_color);
    $('.exc_clicks_mono').val(addtpm);
	$('.exc_clicks_color').val(addtpc);
    calculartotalperiodo();
}

function buscar_serie(){
	var serie=$('#search_serie').val(); 
	$('.color_serie').css('background','#ff000000');
	if(serie!=''){
		$.ajax({
		    type:'POST',
		    url: base_url+"index.php/Configuracionrentas/get_serie_color",
		    data: {
		    	serie:$('#search_serie').val()
		    },
		    success:function(data){  
		    	var array = $.parseJSON(data);
		    	console.log(array);
		    	array.forEach(function(element) {
		    		console.log(element.serieId);
		    		$('.serie_'+element.serieId).css('background','#00bcd4');
		    		if($(".serie_"+element.serieId).length>0){
		    			var posicion=$("#select_serie_"+element.serieId).offset().top;
		    			if(posicion<1100){
		    				posicion=600;
		    			}
		    			$('html, body, table').animate({
				            scrollTop: posicion
				        }, 2000);	
		    		}
		    	});
		    }
	    });
	}
}

function addprefactura(){

	var permitido=1;
	var fechalimite=$('#ordenc_vigencia').val();
	var periodo_inicial=$('#periodo_inicial').val();
	var permt_continuar=$('#permt_continuar').val();
	if(permt_continuar==1){
		$('.addprefactura').prop( "disabled", true );
	    agregarcontadores();
	    setTimeout(function(){ 
	    	$('.addprefactura').prop( "disabled", false);
	    }, 4000);
	}else{
		if (periodo_inicial<fechalimite) {
			$('.addprefactura').prop( "disabled", true );
		    agregarcontadores();
		    setTimeout(function(){ 
		    	$('.addprefactura').prop( "disabled", false);
		    }, 4000);
		}else{
			/*
			swal({   
	    		title: "No permitido!",
			    text: "Vigencia excedida",
			    timer: 2000,
			    showConfirmButton: false
			  });*/
			swal({   
	    		title: "No permitido!",
			    text: "Vigencia excedida,¿Desea Continuar?",
			    //timer: 2000,
			    showCancelButton: true,
			    confirmButtonClass: "btn-danger",
				confirmButtonText: "Si,continuar!",
				closeOnConfirm: true
			  },function(){
			  	if(parseInt($('#continuarcontadores').val())==1){
			  		agregarcontadores();
			  	}else{
			  		$('#modalcontinuarcontadores').modal();
	            	$('#modalcontinuarcontadores').modal('open');
	            	$('#password').val('');	
			  	}
	            
			  });
		}
	}

	
}
function viewfacturas(idcontrato){
	var id_renta = $('#id_renta').val();
	$.ajax({
	    type:'POST',
	    url: base_url+"index.php/Configuracionrentas/viewfacturas",
	    data: {
	    	idrenta:id_renta,idcontrato:idcontrato
	    },
	    success:function(data){  
	    	$('.viewfacturas').html(data);
	    	if ($('#tipo2').val()!=2) {
	    		$('.sologlobales').remove();
	    	}
	    	$('#tablefacturasall').DataTable({"order": [[ 2, "desc" ]]});
	    	$('.tooltipped').tooltip({delay: 50});
	    	//actualizarestatus();
	    }
    });
}
function Fact(idfac,idcont) {
	window.location= base_url+"index.php/Configuracionrentas/Facturar/"+idfac+"/"+idcont;
}
function listfolios(idcontrato){
	cargaequiposs();
	$.ajax({
	    type:'POST',
	    url: base_url+"index.php/Configuracionrentas/lisfolios",
	    data: {
	    	idcontrato:$('#idcontrato').val()
	    },
	    success:function(data){  
	    	$('.listfolios').html(data);
	    	$('#tablelisfolios').DataTable();
	    }
    });
}
function addpagosmodal(idrow,statusp,total){
	$('#modal_pagos').modal('open');
}
function agregarcontadores(){
	var contaclick=0;
	$.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Desea confirmar la carga de contadores?',
        type: 'red',
        typeAnimated: true,
        buttons: {
        	confirmar:{ 
        		btnClass: 'confirm-addcontadores',
        		action: function (){
        			$(".confirm-addcontadores").prop("disabled", true);
	        		//====================================================
						var permitido=1;
						var tipo2=$('#tipo2 option:selected').val();
						var DATA4  = [];
					    var TABLA4   = $("#profactura tbody > tr");
					        TABLA4.each(function(){         
					            item2 = {};
					            item2 ["productoId"] = $(this).find("input[class*='fd_equipo']").val();
					            item2 ["serieId"] = $(this).find("input[class*='fd_serie']").val();
					            item2 ["c_c_i"] = $(this).find("input[class*='f_c_c_i']").val();
					            item2 ["c_c_f"]  = $(this).find("input[class*='f_c_c_f']").val();
					            item2 ["e_c_i"] = $(this).find("input[class*='f_e_c_i']").val();
					            item2 ["e_c_f"] = $(this).find("input[class*='f_e_c_f']").val();
					            item2 ["produccion"] = $(this).find("input[class*='f_produccion']").val();
					            item2 ["toner_consumido"] = $(this).find("input[class*='f_toner_consumido']").val();
					            item2 ["descuento"] = $(this).find("input[class*='descuento']").val();
					            item2 ["descuentoe"] = $(this).find("input[class*='descuentoe']").val();
					            item2 ["produccion_total"] = $(this).find("input[class*='produccion_total']").val();
					            item2 ["tipo"] = $(this).find("input[class*='fd_tipo']").val();
					            item2 ["tipo2"]=tipo2;
					            	item2 ["excedentes"] = $(this).find("input[class*='excedentes']").val();
					            	item2 ["costoexcedente"] = $(this).find("input[class*='excedentescosto']").val();
					            	item2 ["totalexcedente"] = $(this).find("input[class*='excedentestotal']").val();
					            if (parseFloat($(this).find("input[class*='f_c_c_f']").val()) < parseFloat($(this).find("input[class*='f_c_c_i']").val())) {
					            	permitido=0;
					            }
					            
					            if ($(this).find("input[class*='f_c_c_f']").data('tomarescaner')==1) {
					            	if (parseFloat($(this).find("input[class*='f_e_c_f']").val()) < parseFloat($(this).find("input[class*='f_e_c_i']").val())) {
					                	permitido=0;
					                }
					            }
					            
					            DATA4.push(item2);
					        });
					        dataequipos   = JSON.stringify(DATA4);
					        if (permitido==1) {
					        	var descuento_g_clicks=$('#descuento_g_clicks2').val()==undefined?0:$('#descuento_g_clicks2').val();
					        	var descuento_g_scaneo=$('#descuento_g_scaneo2').val()==undefined?0:$('#descuento_g_scaneo2').val();
					        	$.ajax({
								    type:'POST',
								    url: base_url+"index.php/Configuracionrentas/addprefactura",
								    data: {
								    	id_renta:$('#id_renta').val(),
								    	periodo_inicial:$('#periodo_inicial').val(),
								    	periodo_final:$('#periodo_final').val(),
								    	subtotal:$('#costoperiodo').val(),
								    	subtotalcolor:$('#costoperiodocolor').val(),
								    	excedente:$('#costoperiodoexcedente').val(),
								    	excedentecolor:$('#costoperiodoexcedente_c').val(),
								    	total:$('#totalperiodo').val(),
								    	personalId:$('#personalId').val(),
								    	equipos:dataequipos,
								    	idcondicionextra:$('#idcondicionextra').val(),
								    	descuentogclisck:descuento_g_clicks,
								    	descuentogscaneo:descuento_g_scaneo
								    },
								    success:function(data){  
								    	$('.f_c_c_f').val(0);
										$('.f_e_c_f').val(0);
										$('.f_produccion').val(0);
										$('.f_toner_consumido').val(0);
										$('.f_produccion_mono').val(0);
								    	//dataprefactura();
								    	dataprefactura0()
								    	var idpre=parseInt(data);
								    		if(idpre>0){
								    			//generarfacturaauto(idpre);
								    		}
								    	
								    	swal({   
								    		title: "Contadores agregados!",
										    text: "",
										    timer: 2000,
										    showConfirmButton: false
										  });
								    }
							    });
					        }else{
					        	swal({title: "No permitido",text: "verifique sus contadores",timer: 2000,showConfirmButton: false});
					        }
	        		//=========================================================
	        		contaclick++;
	        	}
	        },
            cancelar: function () 
            {
            }
        }
    });
}
function autorizaragregar(){
	$.ajax({
        type:'POST',
        url: base_url+'Sistema/solicitarpermiso',
        data: {
            pass: $('#password').val()
        },
        async: false,
        statusCode:{
            404: function(data){
                swal("Error", "404", "error");
            },
            500: function(){
                swal("Error", "500", "error"); 
            }
        },
        success:function(data){
          if (data==1) {
            permisocontinuar();
            agregarcontadores();
            $('.addprefactura').prop( "disabled", false );
          }else{
            swal("Error", "No tiene permiso", "error"); 
          }
        }
    });
}
function exportarcontsobra(periodo,contrato){
	$.ajax({
	    type:'POST',
	    url: base_url+"index.php/Configuracionrentas/exportarcontsobra",
	    data: {
	    	periodo:periodo,
	    	contrato:contrato
	    },
	    success:function(data){  
	    	$('#modal_exportarcontadores').modal('open');
	    	$('.viewcontadoresesportar').html(data);
	    }
    });
}
function permisocontinuar(){
	$.ajax({
	    type:'POST',
	    url: base_url+"index.php/Generales/permisocontinuar",
	    data: {
	    	contrato:$('#idcontrato').val()
	    },
	    success:function(data){
	    }
    });	
}
function clientecontrato() {
	var idcliente = $('#pcliente option:selected').val();
	$.ajax({
        type:'POST',
        url: base_url+"index.php/Configuracionrentas/selectcontratos2",
        data: {id:idcliente},
        success: function (data){
        	$('#idcontrato_f').html(data); 
        }
    });
}
function facturaselect() {
    var idcontrato = $('#idcontrato_f option:selected').val();
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Configuracionrentas/selectfacturas2",
        data: {id:idcontrato},
        success: function (data){
            $('#idfactura_f').html(data); 
        }
    });
}
function addfactura(periodo,valortotal){
	var html_contect='Se necesita permisos de administrador<br>'+
        '<input type="text" placeholder="Contraseña" id="contrasena3" name="contrasena3" class="name form-control-bmz" style="width:99%"  onpaste="return false;" required/>';
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: html_contect,
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                
                    //var pass=$('#contrasena').val();
                    var pass=$("input[name=contrasena3]").val();
                    if (pass!='') {
                         $.ajax({
                            type:'POST',
                            url: base_url+"index.php/Sistema/solicitarpermiso",
                            data: {
                                pass:pass
                            },
                            success: function (response){
                                    var respuesta = parseInt(response);
                                    if (respuesta==1) {
                                    	//=======================================
                                    		$('#totalgeneralvalor').val(valortotal);
											$('#modal_addfactura').modal('open');
											$('#addfactperiodo').val(periodo);
											$('#addfacttipo').val(0);
											loadtable();
                                    	//=======================================
                                    }else{
                                        notificacion(2);
                                        
                                    }
                            },
                            error: function(response){
                                notificacion(1);
                                
                            }
                        });
                        
                    }else{
                        notificacion(0);
                        
                    }
                
                
            },
            cancelar: function (){
                
            }
        }
    });
    setTimeout(function(){
        new MaskedPassword(document.getElementById("contrasena3"), '\u25CF');
        
    },1000);
	
}
function addfactura2(periodo,valortotal){
	/*
	var html_contect='Se necesita permisos de administrador<br>'+
        '<input type="text" placeholder="Contraseña" id="contrasena3" name="contrasena3" class="name form-control-bmz" style="width:99%"  onpaste="return false;" required/>';
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: html_contect,
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                
                    //var pass=$('#contrasena').val();
                    var pass=$("input[name=contrasena3]").val();
                    if (pass!='') {
                         $.ajax({
                            type:'POST',
                            url: base_url+"index.php/Sistema/solicitarpermiso",
                            data: {
                                pass:pass
                            },
                            success: function (response){
                                    var respuesta = parseInt(response);
                                    if (respuesta==1) {
                                    	//=======================
                                    	*/
                                    		$('#totalgeneralvalor').val(valortotal);
											$('#modal_addfactura').modal('open');
											$('#addfactperiodo').val(periodo);
											$('#addfacttipo').val(1);
											loadtable();
											/*
                                    	//=======================
                                    }else{
                                        notificacion(2);
                                        
                                    }
                            },
                            error: function(response){
                                notificacion(1);
                                
                            }
                        });
                        
                    }else{
                        notificacion(0);
                        
                    }
                
                
            },
            cancelar: function (){
                
            }
        }
    });
    setTimeout(function(){
        new MaskedPassword(document.getElementById("contrasena3"), '\u25CF');
        
    },1000);
	*/
}
function loadtable(){
	table.destroy();
	table = $('#tabla_facturacion').DataTable({
        stateSave: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url+"index.php/Facturaslis/getlistfacturas",
            type: "post",
            "data": {
                'personal':0,
                'cliente':$('#addfactcliente').val(),
                'finicio':'',
                'ffin':'',
                'idclienterfc':0,
                'rs_folios':'U',
                'metodopago':0,
                'tcfdi':0
            },
        },
        "columns": [
            {"data": "FacturasId",
            	render:function(data,type,row){
            		var html='';
            		if(row.Estado==1){
                        html='<input type="checkbox" class="filled-in facturas_checked" id="factura_'+row.FacturasId+'" value="'+row.FacturasId+'" data-valortotal="'+row.total+'"><label for="factura_'+row.FacturasId+'"></label>';
                    }else{
                    	html='<input type="checkbox" class="filled-in facturas_checked" id="factura_'+row.FacturasId+'" value="'+row.FacturasId+'" disabled><label for="factura_'+row.FacturasId+'"></label>';
                    }
                    return html;
            	}
        	},
            {"data": "Folio",
                render:function(data,type,row){
                    var html ='';
                        var serie=row.cadenaoriginal;
                        serie=serie.split('|');
                        if (serie[3]==undefined) {
                            var seriev='';
                        }else{
                            var seriev=serie[3];
                        }
                        html=seriev+''+row.Folio.padStart(4, 0);
                    return html;
                }
            },
            {"data": "Nombre"},
            {"data": "Rfc",},
            {"data": "total"},
            {"data": "Estado",
                render:function(data,type,row){
                    var html='';
                    if(row.Estado==0){
                        html='Cancelada';
                    }else if(row.Estado==1){
                        html='Facturado';
                    }else if(row.Estado==2){
                        html='Sin Facturar';
                    }else{
                    	html='';
                    }
                    return html;
                }
            },
            {"data": "fechatimbre"},
            {"data": null,
                 render:function(data,type,row){
                    $('.tooltipped').tooltip();
                    var html='<div class="btns-factura">';
                    if(row.Estado==0){ // cancelado
                        html+='<a class="b-btn b-btn-primary tooltipped" href="'+base_url+'Configuracionrentas/generarfacturas/'+row.FacturasId+'/1" \
		                        target="_blank" data-position="top" data-delay="50" data-tooltip="Factura"><i class="fas fa-file-alt fa-2x"></i></a> ';
		                html+='<a class="b-btn b-btn-primary tooltipped" href="'+base_url+row.rutaXml+'" target="_blank" data-position="top" data-delay="50" data-tooltip="XML" download ><i class="far fa-file-code fa-2x"></i></a> ';
                        html+='<a class="b-btn b-btn-primary tooltipped" href="'+base_url+'kyocera/facturas/'+row.rutaAcuseCancelacion+'" target="_blank" data-position="top" data-delay="50" data-tooltip="Acuse de cancelación" download ><i class="fas fa-file-invoice fa-2x" style="color: #ef7e4f;"></i></a> ';
                    }else if(row.Estado==1){
                        html+='<a class="b-btn b-btn-primary tooltipped" href="'+base_url+'Configuracionrentas/generarfacturas/'+row.FacturasId+'/1" target="_blank" data-position="top" data-delay="50" data-tooltip="Factura"><i class="fas fa-file-alt fa-2x"></i></a> ';
		                /*
		                html+='<a\
			                        class="b-btn b-btn-primary tooltipped" \
			                        href="'+base_url+row.rutaXml+'" \
			                        target="_blank"\
			                        data-position="top" data-delay="50" data-tooltip="XML"\
			                      download ><i class="far fa-file-code fa-2x"></i>\
                      			</a> ';
                        */
                    }else if(row.Estado==2){
                        html+='<button type="button"\
                          class="b-btn b-btn-primary tooltipped" \
                          onclick="retimbrar('+row.FacturasId+',0)"\
                          data-position="top" data-delay="50" data-tooltip="Retimbrar" id="refacturar">\
                          <span class="fa-stack">\
                          <i class="fas fa-file fa-stack-2x"></i>\
                          <i class="fas fa-sync fa-stack-1x" style="color: red;"></i>\
                          </span>\
                        </button> ';
                    }else{
                    	html+='';
                    }
                    html+='</div>';
                    return html;
                }
            }
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[25, 50, 100], [25, 50,100]]
        
    });
}
function agregarfacturas(){
	var totalgvalor=parseFloat($('#totalgeneralvalor').val());
		totalgvalor=totalgvalor.toFixed(2);
	var facttipo=$('#addfacttipo').val();
	if(facttipo==0){
		var titlefac='¿Está seguro de agregar la factura al periodo?';
	}else{
		var titlefac='¿Está seguro de agregar la factura como deposito?';
	}
	$.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: titlefac,
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
            	//===================================================
            	var facturaslis = $("#tabla_facturacion tbody > tr");
            	var DATAa  = [];
            	var num_facturas_selected=0;
            	var totalfacturas=0;
		        facturaslis.each(function(){  
		        	if ($(this).find("input[class*='facturas_checked']").is(':checked')) {
		        		num_facturas_selected++;
		        		item = {};                    
		            	item ["FacturasIds"]  = $(this).find("input[class*='facturas_checked']").val();
		            	var totalfac  = $(this).find("input[class*='facturas_checked']").data('valortotal');
                        totalfacturas+=Number(totalfac);
		            	DATAa.push(item);
		        	}       
		            
		        });
		        INFOa  = new FormData();
		        aInfoa   = JSON.stringify(DATAa);
		        //console.log(aInfoa);
		        if (num_facturas_selected==1) {
		        	if(facttipo==0){
		        		//========================
		        			//if(totalfacturas<=totalgvalor){ se quito momentaneamente esta restriccion a solicitud de la lic
			                    $.ajax({
			                        type:'POST',
			                        url: base_url+"index.php/Generales/addfacturaperiodo",
			                        data: {
			                            facturas:aInfoa,
			                            contrato:$('#addfactcontrato').val(),
			                            periodo:$('#addfactperiodo').val(),
			                            facttipo:facttipo
			                        },
			                        success:function(response){  
			                            swal("Éxito!", "Se ha Agregado la factura", "success");
			                            $('#test_tab_5').click();
			                        }
			                    });
			                    /*
		                	}else{
		                		alertfunction('Atención!','el total de la factura debe de ser igual o menor a la prefactura');
		                	}
		                	*/
		        		//========================
		        	}
		        	if(facttipo==1){
		        		//======================================
		        			$.ajax({
		                        type:'POST',
		                        url: base_url+"index.php/Generales/addfacturaperiodo",
		                        data: {
		                            facturas:aInfoa,
		                            contrato:$('#addfactcontrato').val(),
		                            periodo:$('#addfactperiodo').val(),
		                            facttipo:facttipo
		                        },
		                        success:function(response){  
		                            
		                                swal("Éxito!", "Se ha Agregado la factura", "success");
		                                $('#test_tab_5').click();
		                                contratosview($('#idcontrato').val());

		                        }
		                    });
		        		//======================================
		        	}
		        	
		        }else{
		        	alertfunction('Atención!','Seleccione solo una factura');
		        }
                //================================================
            },
            cancelar: function (){
                
            }
        }
    });
}
function cargaequiposs(){
    $.ajax({
        type:'POST',
        url: base_url+"NewFolio/cargaequipos",
        data: {
            contrato:$('#idcontrato').val()
        },
        success: function (data){
            console.log('xxxxx');
            console.log(data);
            var array = $.parseJSON(data);
            $('#nfequipos').html(array.equipos); 
            //$('#nfequipos').select2();
            $('#nfservicios').html(array.servicio);
        }
    });
}
function cargaconsumibles(){
	//var color = $('#montoc').val();
	var color =$('#nfcontratos option:selected').data('rentacolor');
	var idequipo = $('#nfequipos option:selected').data('equipo');
	var idequipov = $('#nfequipos option:selected').val();
	var tipo2 = $('#tipo2 option:selected').val();
	if(color==0){
		if(tipo2==1){
			color=$('.precio_c_equi_color_'+idequipov).val()
		}	
	}
	
	$.ajax({
        type:'POST',
        url: base_url+"NewFolio/cargaconsumibles",
        data: {
        	id:idequipo
        },
        success: function (data){
        	$('#nfconsumibles').html(data); 
        	if(color==0){
        		$('.datatipo_2').prop( "disabled", true );
                $('.datatipo_3').prop( "disabled", true );
                $('.datatipo_4').prop( "disabled", true );
        	}
            //$('#nfconsumibles').select2();
        }
    });
}
var rowconsumible=0;
function addconsumibles(){
	var vcontrato = $('#nfcontratos option:selected').val();
	var vequipo = $('#nfequipos option:selected').val();
	var vequipo_modelo = $('#nfequipos option:selected').data('equipo');
    var vserieid = $('#nfequipos option:selected').data('serieid');
	var vconsumible= $('#nfconsumibles option:selected').val();
	var vclientes= $('#nfclientes option:selected').val();
    var vfservicios= $('#nfservicios option:selected').val();

	var tcontrato = $('#nfcontratos option:selected').text();
	var tequipo = $('#nfequipos option:selected').text();
	var tconsumible= $('#nfconsumibles option:selected').text();
	var tclientes= $('#nfclientes option:selected').text();
    var tfservicios= $('#nfservicios option:selected').text();
    var nfecha= $('#nfecha').val();
    var nbodega= $('#nbodega option:selected').val();
    var nbodega_text= $('#nbodega option:selected').text();

	if(vconsumible>0){
		var html='<tr class="add_new_consumible_'+rowconsumible+'">\
	                  <td><input type="hidden" id="addnf_cliente" value="'+vclientes+'">'+tclientes+'</td>\
	                  <td><input type="hidden" id="addnf_contrato" value="'+vcontrato+'">'+tcontrato+'</td>\
	                  <td><input type="hidden" id="addnf_servicio" value="'+vfservicios+'">'+tfservicios+'</td>\
	                  <td><input type="hidden" id="addnf_equipo_modelo" value="'+vequipo_modelo+'">\
	                  	<input type="hidden" id="addnf_equipo" value="'+vequipo+'">\
	                    <input type="hidden" id="addnf_serie" value="'+vserieid+'">'+tequipo+'</td>\
	                  <td><input type="hidden" id="addnf_consumible" value="'+vconsumible+'">\
	                  	<input type="hidden" id="addnf_consumiblett" value="'+tconsumible+'">'+tconsumible+'</td>\
	                  <td><input type="hidden" id="addnf_fecha" value="'+nfecha+'">'+nfecha+'</td>\
	                  <td><input type="hidden" id="addnf_bodega" value="'+nbodega+'">'+nbodega_text+'</td>\
	                  <td><a class="btn-floating waves-effect waves-light red accent-2" onclick="deleteconsu('+rowconsumible+')"><i class="material-icons">clear</i></a></td>\
	                </tr>';
	    $('.addconsumiblesbody').append(html);
	}else{
        alertfunction('Atención!','Seleccione un consumible');
    }
    rowconsumible++;
}
function deleteconsu(id){
	$('.add_new_consumible_'+id).remove();
}
function agregarconsumibles(){
    var TABLAc   = $("#tabla_cons tbody > tr");
    var DATAc  = [];
        TABLAc.each(function(){         
            item = {};
            item ["cliente"]   = $(this).find("input[id*='addnf_cliente']").val();
            item ["equipo"]  = $(this).find("input[id*='addnf_equipo']").val();
            item ["equipom"]  = $(this).find("input[id*='addnf_equipo_modelo']").val();
            item ["serie"]  = $(this).find("input[id*='addnf_serie']").val();
            item ["contrato"]  = $(this).find("input[id*='addnf_contrato']").val();
            item ["servicio"]  = $(this).find("input[id*='addnf_servicio']").val();
            item ["consumible"]  = $(this).find("input[id*='addnf_consumible']").val();
            item ["consumiblett"]  = $(this).find("input[id*='addnf_consumiblett']").val();
            item ["fecha"]  = $(this).find("input[id*='addnf_fecha']").val();
            item ["bodega"]  = $(this).find("input[id*='addnf_bodega']").val();
            DATAc.push(item);
        });
        INFOc  = new FormData();
        aInfoc   = JSON.stringify(DATAc);
    //===========================================
    datos = 'consumibles='+aInfoc;
        $.ajax({
            type:'POST',
            url: base_url+"index.php/NewFolio/addconsumibles",
            data: datos,
            success: function (response){
                $.alert({boxWidth: '30%',useBootstrap: false,title: 'Exito',theme: 'bootstrap',content: 'Se han agregados los consumibles'});   
                setTimeout(function(){ 
                    $('.addconsumiblesbody').html('');
                    $('#listfolios').click();
                }, 2000);
                    
            },
            error: function(response){
            	notificacion(1);
            }
        });

}
function refacturarperiodo(periodoid,contrato,facturaid){
	var contaclick=0;
	$.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Desea refacturar con relacion para la cancelacion previa?',
        type: 'red',
        typeAnimated: true,
        buttons: {
        	confirmar:function (){ 	
	        	window.location.replace(base_url+"Facturas/refacturarperiodo/"+periodoid+"/"+contrato+"/"+facturaid);
	        },
            cancelar: function (){
            }
        }
    });
}
function cerrarperiodo(periodoid){
	$.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Desea cerrar el periodo?<br> Para continuar se necesita permisos de administrador<br>'+
                 '<input type="text" placeholder="Contraseña" id="contrasena" name="contrasena" class="name form-control" data-val="" onpaste="return false;" maxlength="10"/>',
        type: 'red',
        typeAnimated: true,
        buttons:{

            confirmar: function (){
                //var pass=$('#contrasena').val();
                var pass=$("input[name=contrasena]").val();
                if (pass!='') {
                     $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Sistema/solicitarpermiso",
                        data: {
                            pass:pass
                        },
                        success: function (response){
                                var respuesta = parseInt(response);
                                if (respuesta==1) {
                                	cerrarperiodo2(periodoid);
                                }else{
                                    toastr["error"]("No se tiene permisos");
                                }
                        },
                        error: function(response){
                            notificacion(1);
                            
                        }
                    });
                    
                }else{
                    notificacion(0);
                    
                }
            },
            
            cancelar: function (){
                
            }
        }
    });
    setTimeout(function(){
        new MaskedPassword(document.getElementById("contrasena"), '\u25CF');
    },1000);
}
function cerrarperiodo2(periodoid){
	$.ajax({
        type:'POST',
        url: base_url+"index.php/Generales/cerrarperiodo",
        data: {
        	periodoid:periodoid
        },
        success: function (response){
            $('#test_tab_5').click(); 
            toastr["success"]("Periodo cerrado");  
        },
        error: function(response){
            
        }
    });
}
var prefId_row;
function comentariop(prefId){
	prefId_row=prefId;
	$('#modal_comentario').modal();
	$('#modal_comentario').modal('open');
	$.ajax({
        type:'POST',
        url: base_url+"index.php/Generales/getcomentarioperiodo",
        data: {
        	prfId:prefId
        },
        success: function (response){
            $('#comentariopre').val(response.replace(/(\r\n|\n|\r)/gm, " "));    
        },
        error: function(response){
            
        }
    });
}
function updatecomentario(){
	$.ajax({
        type:'POST',
        url: base_url+"index.php/Generales/inserupdatecomentario",
        data: {
        	prfId:prefId_row,
        	comen:$('#comentariopre').val()
        },
        success: function (response){
        },
        error: function(response){  
        }
    });
}
function deleteprefacturax(pre){
	$.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Desea eliminar el periodo?<br>Necesita permisos de administrador<br><input type="password" id="passworddetele" class="form-control-bmz" style="width:90%">',
        type: 'red',
        typeAnimated: true,
        buttons: {
        	confirmar:function (){ 	
        		var pass=$('#passworddetele').val();
	        	$.ajax({
			        type:'POST',
			        url: base_url+"Generales/deleteprefactura",
			        data: {
			        	pre:pre,
			        	pass:pass
			        },
			        success: function (data){
			        	var su=parseInt(data);
			        	if(su==1){
			        		toastr["success"]("periodo eliminado");
			        		$('#test_tab_5').click();
			        	}else{
			        		toastr["warning"]("No tiene permisos", "Advertencia");
			        	}
			        }
			    });
	        },
            cancelar: function (){
            }
        }
    });
}
function caldesglobal(){

	var tipo2=$('#tipo2 option:selected').val();
	var opcion=1;//1 se le agrega al mas alto,2 se divide ente todos
	var des_clicks=$('#descuento_g_clicks2').val();
	var des_escaneo=$('#descuento_g_scaneo2').val();
	//=======================================================================
		var DATA4  = [];
		var TABLA4   = $("#profactura tbody > tr");
	        TABLA4.each(function(){         
	            item2 = {};
	            item2 ["productoId"] = $(this).find("input[class*='fd_equipo']").val();
	            item2 ["serieId"] = $(this).find("input[class*='fd_serie']").val();
	            item2 ["tipo"] = $(this).find("input[class*='fd_tipo']").val();
	            item2 ["produccion_clicks"] = $(this).find("input[class*='f_c_c_p']").val();
                item2 ["produccion_scaneo"] = $(this).find("input[class*='f_e_c_p']").val();
                item2 ["produccion_total"] = $(this).find("input[class*='produccion_total']").val();

	           // item2 ["accesorios"]  = $(this).find("input[id*='accesorios']").val();
	            DATA4.push(item2);
	        });
		console.log(DATA4);
		//====================================================================
			// Inicializar la variable para almacenar el máximo valor
			let maxProduccionclicks = -Infinity;
			let objetoConMayorProduccionclicks = null;

			// Iterar sobre cada objeto en el array
			DATA4.forEach(objeto => {
			    const produccionclicks = parseInt(objeto.produccion_clicks); // Convertir a número entero

			    // Comprobar si la producción total de este objeto es mayor que la actual máxima
			    if (produccionclicks > maxProduccionclicks) {
			        maxProduccionclicks = produccionclicks; // Actualizar el máximo valor
			        objetoConMayorProduccionclicks = objeto; // Guardar referencia al objeto
			    }
			});

			// Imprimir el objeto con la producción total más alta
			console.log("El objeto con producción cliks mayor es:", objetoConMayorProduccionclicks);
		//=====================================================================================
		//====================================================================
			// Inicializar la variable para almacenar el máximo valor
			let maxProduccionscaneo = -Infinity;
			let objetoConMayorProduccionscaneo = null;

			// Iterar sobre cada objeto en el array
			DATA4.forEach(objeto => {
			    const produccionscaneo = parseInt(objeto.produccion_scaneo); // Convertir a número entero

			    // Comprobar si la producción total de este objeto es mayor que la actual máxima
			    if (produccionscaneo > maxProduccionscaneo) {
			        maxProduccionscaneo = produccionscaneo; // Actualizar el máximo valor
			        objetoConMayorProduccionscaneo = objeto; // Guardar referencia al objeto
			    }
			});

			// Imprimir el objeto con la producción total más alta
			console.log("El objeto con producción scaneo mayor es:", objetoConMayorProduccionscaneo);
		//=====================================================================================
	//=======================================================================
	if(tipo2==1){
		if(des_clicks>0){
			$('.descuento_mono').val(0);
			$('.descuento_color').val(0);


		}
		if(des_escaneo>0){
			$('.descuentoe_mono').val(0);
			$('.descuentoe_color').val(0);
		}
		if(des_clicks>0){
			if(opcion==1){
				if(objetoConMayorProduccionclicks!=null){
					$('.descuento_'+objetoConMayorProduccionclicks.productoId+'_'+objetoConMayorProduccionclicks.serieId+'_'+objetoConMayorProduccionclicks.tipo).val(des_clicks);
				}
			}
			if(opcion==2){
				
			}
		}
		if(des_escaneo>0){
			if(opcion==1){
				if(objetoConMayorProduccionscaneo!=null){
					$('.descuentoe_'+objetoConMayorProduccionscaneo.productoId+'_'+objetoConMayorProduccionscaneo.serieId+'_'+objetoConMayorProduccionscaneo.tipo).val(des_escaneo);
				}
			}
			if(opcion==2){
				
			}
		}
		if(objetoConMayorProduccionclicks!=null || objetoConMayorProduccionscaneo!=null){
			//$('.descuento_'+objetoConMayorProduccionclicks.productoId+'_'+objetoConMayorProduccionclicks.serieId+'_'+objetoConMayorProduccionclicks.tipo).change();
		}
	}
}
function view_pre_renta(pre,con,cc,c_fac){
	/*
	if(cc==0){
		var url=base_url+'Reportes/rentadetalles/'+pre+'/'+con;
		window.open(url, '_blank');
	}else{
		*/
		
		var htmlif='¿selecciona el documento a visualizar?<br>';
		htmlif+='<input name="formt" type="radio" class="formt" id="formt0" value="0" checked>\
                <label for="formt0">PDF</label>\
                <input name="formt" type="radio" class="formt" id="formt1" value="1">\
                <label for="formt1">XLS</label><br>';

		htmlif+='<div class="col s6">\
                <input name="tasign" type="radio" class="tasign" id="asign0" value="0" checked>\
                <label for="asign0">Normal</label><br>';
                if(cc==1){
        htmlif+='<input name="tasign" type="radio" class="tasign" id="asign1" value="1">\
                <label for="asign1">Por centro de costos Detallado</label>\
                <input name="tasign" type="radio" class="tasign" id="asign2" value="2">\
                <label for="asign2">Por centro de costos Global</label><br>';
            	}
        htmlif+='<input name="view_fol" type="radio" id="view_fol_0" value="0" >\
                <label for="view_fol_0">Sin Folios</label>\
                <input name="view_fol" type="radio" id="view_fol_1" value="1" checked>\
                <label for="view_fol_1">Con folios</label><br>';
      htmlif+='</div>';
		$.confirm({
	        boxWidth: '30%',
	        useBootstrap: false,
	        icon: 'fa fa-warning',
	        title: 'Documento!',
	        content: htmlif,
	        type: 'red',
	        typeAnimated: true,
	        buttons: {
	        	Ver:function (){ 	
	        		var file =$('input:radio[name=formt]:checked').val();
	        		var fol =$('input:radio[name=view_fol]:checked').val();
	        		var tasign=$('input:radio[name=tasign]:checked').val();
	        		if(tasign==0){
	        			var url=base_url+'Reportes/rentadetalles/'+pre+'/'+con+'?fol='+fol+'&file='+file;
		
	        		}else{
	        			var url=base_url+'Reportes/rentadetalles/'+pre+'/'+con+'?cc='+tasign+'&fol='+fol+'&file='+file;
	        		}
	        			window.open(url, '_blank');
		        },
	            Timbrar: {
		            text: 'Timbrar',
		            btnClass: 'btn-timbrarpre', // Agregar la clase personalizada
		            action: function () {
		                procederatimbrar(pre);
		            }
		        },
	            cancelar: function (){
	            }
	        }
	    });
	    var chek = $('#fac_auto').is(':checked');
	    if(c_fac>0){
	    	chek=false;
	    }
	    if(chek){
				$('.btn-timbrarpre').show('show');
			}else{
				$('.btn-timbrarpre').hide('show');
			}
	    setTimeout(function(){
            
			if(chek){
				$('.btn-timbrarpre').show('show');
			}else{
				$('.btn-timbrarpre').hide('show');
			}
        },500);
	//}

}
function procederatimbrar(idpre){
	$.confirm({
	        boxWidth: '30%',
	        useBootstrap: false,
	        icon: 'fa fa-warning',
	        title: 'Timbrado!',
	        content: 'Confirma que desea realzar el timbrado?',
	        type: 'red',
	        typeAnimated: true,
	        buttons: {
	        	Confirmar:function (){ 	
	        		generarfacturaauto(idpre);
		        },

	            cancelar: function (){
	            }
	        }
	    });
}
function obtenercontratosoption(idCliente){
	var idcontrato = $('#idcontrato').val();
	$.ajax({
        type:'POST',
        url: base_url+"index.php/Configuracionrentas_ext/contratorentaclienteoption",
        data: {
            id:idCliente,
            con:idcontrato
        },
        success: function (data){
        	console.log(data);
        	$('#idcontra').html(data);
        }
    });
    load_contratos_rel();
}
function agregarcontratoexte(){
	var idcontrato = $('#idcontrato').val();
	var select_con = $('#idcontra option:selected').val();
	var select_con_ext = $('#idcontra option:selected').data('idarpex');
	if(select_con_ext>0){
		alertfunction('Atención!','el contrato ya se ecuentra agregado');
	}else{
		$.ajax({
        type:'POST',
        url: base_url+"index.php/Configuracionrentas_ext/agregarcontratoexte",
        data: {
            cono:idcontrato,
            cond:select_con
        },
        success: function (data){
        	toastr["success"]("Contrato agregado");
        }
    });
	}
}
function load_contratos_rel(){

}
function infoequiporetiro(rowequipo,serieId){
	$.ajax({
        type:'POST',
        url: base_url+"index.php/Configuracionrentas_ext/infoequiporetiro",
        data: {
            rowequipo:rowequipo,
            serieId:serieId
        },
        success: function (data){
        	var array = $.parseJSON(data);
        	var text_status='';
        	if(array.status==0){
        		text_status='En espera';
        	}
        	if(array.status==1){
        		text_status='En proceso';
        	}
        	if(array.status==2){
        		text_status='Finalizado';
        	}
        	if(array.status==3){
        		text_status='Suspensión';
        	}
        	if(array.comt==null){
        		var comt='';
        	}else{
        		var comt=array.comt;
        	}

        	var html='<div class="row"><div class="col s6">Comentario</div><div class="col s6">'+array.comentario+'</div></div>';
        		html+='<div class="row"><div class="col s6">Servicio</div><div class="col s6">Retiro</div></div>';
        		html+='<div class="row"><div class="col s6">Fecha de servicio</div><div class="col s6">'+array.fecha+'</div></div>';
        		html+='<div class="row"><div class="col s6">Hora de servicio</div><div class="col s6">'+array.hora+'</div></div>';
        		html+='<div class="row"><div class="col s6">Estatus</div><div class="col s6">'+text_status+'</div></div>';
        		html+='<div class="row"><div class="col s6">Tecnico</div><div class="col s6">'+array.tecnico+'</div></div>';
        		html+='<div class="row"><div class="col s12">'+comt+'</div></div>';
        	$.alert({boxWidth: '30%',useBootstrap: false,title: 'Información',theme: 'bootstrap',content: html});  
        }
    });
}
function deletefactura(prefid,facId,folio){
	$.confirm({
            boxWidth: '30%',
            useBootstrap: false,
            icon: 'fa fa-warning',
            title: 'Atención!',
            content: '¿Confirma la desviculacion del folio '+folio+' del periodo '+prefid+'?<br>Se necesita permisos de administrador'+
                     '<input type="text" placeholder="Contraseña" id="contrasena" name="contrasena" class="name form-control" data-val="" onpaste="return false;" maxlength="10"/>',
            type: 'red',
            typeAnimated: true,
            buttons:{

                confirmar: function (){
                    
                    //var pass=$('#contrasena').val();
                    var pass=$("input[name=contrasena]").val();
                    if (pass!='') {
                         $.ajax({
                            type:'POST',
                            url: base_url+"index.php/Sistema/solicitarpermiso/",
                            data: {
                                pass:pass
                            },
                            success: function (response){
                                    var respuesta = parseInt(response);
                                    if (respuesta==1) {
                                    	deletefactura2(prefid,facId,folio);
                                    }else{
                                        notificacion(2);
                                    }
                            },
                            error: function(response){
                                notificacion(1);
                                
                            }
                        });
                        
                    }else{
                        notificacion(0);
                    }
                    
                },
                
                cancelar: function (){
                    
                }
            }
        });
        setTimeout(function(){
            new MaskedPassword(document.getElementById("contrasena"), '\u25CF');
        },1000);
}
function deletefactura2(prefid,facId,folio){
	var idcon = $('#idcontrato').val();
	$.ajax({
        type:'POST',
        url: base_url+"index.php/Configuracionrentas_ext/deletefacturaperiodo",
        data: {
            prefid:prefid,
            facId:facId,
            folio:folio
        },
        success: function (data){
        	viewfacturas(idcon);
        	toastr["success"]("Factura desvinculada");
        },
        error: function(response){
            notificacion(1);
        }
    });
}


function verificar_serie_carga(){
	//====================================================
		var DATA4  = [];
	    var TABLA4   = $("#profactura tbody > tr");
	        TABLA4.each(function(){         
	            item2 = {};	            
	            item2 ["serie"] = $(this).find("input[class*='fd_serie']").data('serie');

	            DATA4.push(item2);
	        });

	        // Eliminar duplicados basados en la propiedad 'serie'
			let uniqueSeries = {};
			let result = [];

			// Recorrer el array y filtrar elementos únicos
			DATA4.forEach(item => {
			    if (!uniqueSeries[item.serie]) {
			        uniqueSeries[item.serie] = true; // Almacenar serie única
			        result.push(item); // Agregar solo si no está repetida
			    }
			});
	        dataequipos   = JSON.stringify(result);
        	$.ajax({
			    type:'POST',
			    url: base_url+"index.php/Configuracionrentas_ext/verificar_serie_carga",
			    data: {
			    	idCliente:$('#idCliente').val(),
			    	id_renta:$('#id_renta').val(),
			    	periodo_inicial:$('#periodo_inicial').val(),
			    	periodo_final:$('#periodo_final').val(),
			    	equipos:dataequipos
			    },
			    success:function(data){  
			    	console.log(data);
			    	var array = $.parseJSON(data);
			    	console.log(array);
			    	if(array.length>0){
			    		$('.serie_input').addClass('cont_not_exit');
			    	}
			    	$.each(array, function(index, item) {
		                $('.serie_input_'+item.serie).removeClass('cont_not_exit');
		                $('.date-ser.ser_'+item.serie).html(item.fecha);
		            });
			    	
			    }
		    });
	//=========================================================       		
}
function verificar_contadorinicial(){
	var permitido=1;
	var series_lab='';
	//var DATA4  = [];
    var TABLA4   = $("#profactura tbody > tr");
        TABLA4.each(function(){         
            item2 = {};

            var serid = $(this).find("input[class*='fd_serie']").val();
            var sername = $(this).find("input[class*='fd_serie']").data('serie');

            var cont_ini = $(this).find("input[class*='f_c_c_i']").val();
            var cont_ini_bo = $(this).find("input[class*='f_c_c_i']").data('bodegaor');
            if(cont_ini>0){

            }else if(cont_ini_bo==2){
            	permitido=0;
            	series_lab+=sername+', ';
            }
        });
    if(permitido==0){
    	$.confirm({
	        boxWidth: '30%',
	        useBootstrap: false,
	        icon: 'fa fa-warning',
	        title: 'Atención!',
	        content: 'Las siguientes series les hace falta el contador inicial :'+series_lab,
	        type: 'red',
	        typeAnimated: true,
	        buttons:{
	            confirmar: function (){
	                location.reload();
	                
	            }
	        }
	    });
    }
}
function generarfacturaauto(idpre){
	var idcontrato = $('#idcontrato').val();
	var chek = $('#fac_auto').is(':checked');
	if(chek){
		$.ajax({
	        type:'POST',
	        url: base_url+"index.php/Configuracionrentas_ext/generarfacturaauto",
	        data: {
	            con:idcontrato,
	            idpre:idpre
	        },
	        success: function (data){
	        	console.log(data);
	        	var array = $.parseJSON(data);
	        	if(array.FacturasId>0){
	        		retimbrar0(array.FacturasId);
	        	}
	        }
	    });
	}
}
function retimbrar0(idfactura){//esta se genera internamente
    var idcontrato = $('#idcontrato').val();
    $('body').loading({theme: 'dark',message: 'Timbrando factura...'});
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Configuracionrentas/retimbrar",
        data: {
            factura:idfactura
        },
        success:function(response){  
        	$('body').loading('stop'); 
            console.log(response);
            var array = $.parseJSON(response);
            if (array.resultado=='error') {
                
                swal({    title: "Error "+array.CodigoRespuesta+"!",
                              text: array.MensajeError+' '+array.info.TimbrarCFDIResult.MensajeErrorDetallado,
                              type: "warning",
                              showCancelButton: false
                 });
                 
                notificardeerror(idfactura);
            }else{
                swal("Éxito!", "Se ha creado la factura", "success");
                //loadtable();
                viewfacturas(idcontrato);
            }

        }
    });
            
}
function retimbrar2(idfactura){//esta se genera internamente
    
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Configuracionrentas/retimbrar",
        data: {
            factura:idfactura
        },
        success:function(response){  
            console.log(response);
            var array = $.parseJSON(response);
            if (array.resultado=='error') {
                /*
                swal({    title: "Error "+array.CodigoRespuesta+"!",
                              text: array.MensajeError+' '+array.info.TimbrarCFDIResult.MensajeErrorDetallado,
                              type: "warning",
                              showCancelButton: false
                 });
                 */
                notificardeerror(idfactura);
            }else{
                //swal("Éxito!", "Se ha creado la factura", "success");
                //loadtable();
            }

        }
    });
            
}
function notificardeerror(idfactura){
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Mailenvio/envio_fac_error",
        data: {
            idfac:idfactura
        },
        success:function(data){ 
            console.log(data);
        }
    });
}