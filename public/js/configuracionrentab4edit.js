var base_url = $('#base_url').val();

function totaltoneredit(id_equipo,serieId,tipo){
	//console.log('id_equipo: '+id_equipo);
	$.ajax({
	    type:'POST',
	    url: base_url+"index.php/Configuracionrentas/datosToner",
	    data: {
	    	idequipo:id_equipo,serie:serieId,tipoT:tipo
	    },
	    success:function(data){ 
	    	//var array = $.parseJSON(data);
	    	//console.log(data);
	    	//array.toners_cant.forEach(function(element) {
	    		//console.log("toner_cons: "+array.toner_consumido);
	    		$('.total_toner_'+id_equipo+'_'+serieId+'_'+tipo+'_edit').val(parseFloat(data)/100);
	    	//});
	    }
    });

}

function calculartotalperiodo_edit(){
	setTimeout(function(){ 
		var periodo =$('#costoperiodo_edit').val();
		var periodoc =$('#costoperiodocolor_edit').val();
		var periodoext =$('#costoperiodoexcedente_edit').val();
		var periodoextc =$('#costoperiodoexcedente_c_edit').val();
		var total= parseFloat(periodo)+parseFloat(periodoc)+parseFloat(periodoext)+parseFloat(periodoextc);
		$('#totalperiodo_edit').val(total);
	}, 1000);	
}
function calculartotalpedit(){
    var addtpm = 0;
    $(".produccion_mono_edit").each(function() {
        var vstotalm = $(this).val();
        addtpm += Number(vstotalm);
    });
    var addtpc = 0;
    $(".produccion_color_edit").each(function() {
        var vstotalc = $(this).val();
        addtpc += Number(vstotalc);
    });
    if ($('#tipo2').val()==2) {
    	var clicks_mono = parseFloat($('input[name=clicks_mono]').val());
    	var precio_monoex = parseFloat($('input[name=precio_c_e_mono]').val());
    	var excedente = (parseFloat(addtpm)-parseFloat(clicks_mono))*parseFloat(precio_monoex);
		if (excedente<0) {
			excedente=0;
		}
		var clicks_color = parseFloat($('input[name=clicks_color]').val());
    	var precio_colorex = parseFloat($('input[name=precio_c_e_color]').val());
    	var excedentec = (parseFloat(addtpc)-parseFloat(clicks_color))*parseFloat(precio_colorex);
		if (excedentec<0) {
			excedentec=0;
		}
    }else{
    	var excedente = 0;
    	$(".excedente_mono_edit").each(function() {
        var vstotalm = $(this).val();
	        excedente += Number(vstotalm);
	    });
	    var excedentec = 0;
	    $(".excedente_color_edit").each(function() {
	        var vstotalc = $(this).val();
	        excedentec += Number(vstotalc);
	    });
    }
    $('#costoperiodoexcedente_edit').val(excedente);
    $('#costoperiodoexcedente_c_edit').val(excedentec);
    calculartotalperiodo_edit();
}


function Fact(idfac,idcont) {
	window.location= base_url+"index.php/Configuracionrentas/Facturar/"+idfac+"/"+idcont;
}
function listfolios(idcontrato){
	$.ajax({
	    type:'POST',
	    url: base_url+"index.php/Configuracionrentas/lisfolios",
	    data: {
	    	idcontrato:$('#idcontrato').val()
	    },
	    success:function(data){  
	    	$('.listfolios').html(data);
	    	$('#tablelisfolios').DataTable();
	    }
    });
}
function addpagosmodal(idrow,statusp,total){
	$('.modal').modal();
	$('#modal_pagos').modal('open');
}
/*
function agregarcontadores(){
	var permitido=1;
	var tipo2=$('#tipo2 option:selected').val();
	var DATA4  = [];
    var TABLA4   = $("#profactura tbody > tr");
        TABLA4.each(function(){         
            item2 = {};
            item2 ["productoId"] = $(this).find("input[class*='fd_equipo']").val();
            item2 ["serieId"] = $(this).find("input[class*='fd_serie']").val();
            item2 ["c_c_i"] = $(this).find("input[class*='f_c_c_i']").val();
            item2 ["c_c_f"]  = $(this).find("input[class*='f_c_c_f']").val();
            item2 ["e_c_i"] = $(this).find("input[class*='f_e_c_i']").val();
            item2 ["e_c_f"] = $(this).find("input[class*='f_e_c_f']").val();
            item2 ["produccion"] = $(this).find("input[class*='f_produccion']").val();
            item2 ["toner_consumido"] = $(this).find("input[class*='f_toner_consumido']").val();
            item2 ["produccion_total"] = $(this).find("input[class*='produccion_total']").val();
            item2 ["tipo"] = $(this).find("input[class*='fd_tipo']").val();
            item2 ["tipo2"]=tipo2;
            	item2 ["excedentes"] = $(this).find("input[class*='excedentes']").val();
            	item2 ["costoexcedente"] = $(this).find("input[class*='excedentescosto']").val();
            	item2 ["totalexcedente"] = $(this).find("input[class*='excedentestotal']").val();
            if (parseFloat($(this).find("input[class*='f_c_c_f']").val()) < parseFloat($(this).find("input[class*='f_c_c_i']").val())) {
            	permitido=0;
            }
            
            if ($(this).find("input[class*='f_c_c_f']").data('tomarescaner')==1) {
            	if (parseFloat($(this).find("input[class*='f_e_c_f']").val()) < parseFloat($(this).find("input[class*='f_e_c_i']").val())) {
                	permitido=0;
                }
            }
            
            
           // item2 ["accesorios"]  = $(this).find("input[id*='accesorios']").val();
            DATA4.push(item2);
        });
        dataequipos   = JSON.stringify(DATA4);
        if (permitido==1) {
        	$.ajax({
			    type:'POST',
			    url: base_url+"index.php/Configuracionrentas/addprefactura",
			    data: {
			    	id_renta:$('#id_renta').val(),
			    	periodo_inicial:$('#periodo_inicial').val(),
			    	periodo_final:$('#periodo_final').val(),
			    	subtotal:$('#costoperiodo').val(),
			    	subtotalcolor:$('#costoperiodocolor').val(),
			    	excedente:$('#costoperiodoexcedente').val(),
			    	excedentecolor:$('#costoperiodoexcedente_c').val(),
			    	total:$('#totalperiodo').val(),
			    	personalId:$('#personalId').val(),
			    	equipos:dataequipos
			    },
			    success:function(data){  
			    	$('.f_c_c_f').val(0);
					$('.f_e_c_f').val(0);
					$('.f_produccion').val(0);
					$('.f_toner_consumido').val(0);
					$('.f_produccion_mono').val(0);
			    	dataprefactura();
			    	swal({   
			    		title: "Contadores agregados!",
					    text: "",
					    timer: 2000,
					    showConfirmButton: false
					  });
			    }
		    });
        }else{
        	swal({   
	    		title: "No permitido",
			    text: "verifique sus contadores",
			    timer: 2000,
			    showConfirmButton: false
			  });
        }
}
*/
function editarcontadores_permiso(idperiodo){
	$.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: 'Se necesita permisos de administrador<br>'+
                 '<input type="password" placeholder="Contraseña" id="contrasena" class="name form-control" required/>',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var pass=$('#contrasena').val();
                if (pass!='') {
                    $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Sistema/solicitarpermiso",
                        data: {
                            pass:pass
                        },
                        success: function (response){
                                var respuesta = parseInt(response);
                                if (respuesta==1) {
                    //=============================================================================
                    	
                        editarcontadores(idperiodo,1);
                    //=============================================================================
                                }else{
                                    notificacion(2);
                                }
                        },
                        error: function(response){
                            notificacion(1); 
                             
                        }
                    });

                }else{
                    notificacion(0);
                }
                
            },
            cancelar: function (){
                
            }
        }
    });
}

function editarcontadores(id,tipo){
	setTimeout(function(){ 
		$('.f_c_c_i_edit').prop('readonly',false);
		$('.f_e_c_i_edit').prop('readonly',false);
	}, 2000);
	setTimeout(function(){ 
		$('.f_c_c_i_edit').prop('readonly',false);
		$('.f_e_c_i_edit').prop('readonly',false);
	}, 3000);
	$('#modal_modi_contadores').modal({
      dismissible: false});
	$('#modal_modi_contadores').modal('open');
	
		var urltipo=base_url+"index.php/Contadores/consultarcontadores";

	$.ajax({
	    type:'POST',
	    url: urltipo,
	    data: {
	    	idpoliza:id,
	    	idcontrato:$('#idcontrato').val(),
	    	tipo:tipo
	    },
	    success:function(data){  
	    	var array = $.parseJSON(data);
	    	$('.viewcontadorescargados').html(array.contadoreshtml);
	    	dataprefacturaedit(id);
	    }
    });
}
function buscar_serie_edit(){
	var serie=$('#search_serie_edit').val(); 
	$('.color_serie').css('background','#ff000000');
	if(serie!=''){
		$.ajax({
		    type:'POST',
		    url: base_url+"index.php/Configuracionrentas/get_serie_color",
		    data: {
		    	serie:serie
		    },
		    success:function(data){  
		    	var array = $.parseJSON(data);
		    	console.log(array);
		    	array.forEach(function(element) {
		    		console.log(element.serieId);
		    		$('.serie_edit_'+element.serieId).css('background','#00bcd4');
		    		if($(".serie_edit_"+element.serieId).length>0){
		    			var posicion=$("#select__edit_serie_"+element.serieId).offset().top;
		    			if(posicion<1100){
		    				posicion=600;
		    			}
		    			$('html, body, table').animate({
				            scrollTop: posicion
				        }, 2000);	
		    		}
		    			
		    	});
		    }
	    });
		
	}
}
function  dataprefacturaedit(id) {	
	var id_renta = $('#id_renta').val();
	$.ajax({
	    type:'POST',
	    url: base_url+"index.php/Configuracionrentas/datosprefactura",
	    data: {
	    	idrenta:id_renta,
	    	prefId:id,
	    },
	    success:function(data){  
	    	//console.log('infor factura: '+data);
	    	var array = $.parseJSON(data);
	    	if(array.idcondicionextra>0){
	    		viewcondiciones(array.idcondicionextra);
	    	}
	    	//$('#periodo_inicial').val(array.periodoinicial);
	    	//$('#periodo_final').val(array.periodofinal);
	    	//$('.periodon').val(array.periodon);
	    	//$('#periodoex').val(array.periodone);
	    	//$('#periodoex_e').val(array.periodone);
	    	//$('.total_toner').val(array.toner_consumido/100);
	    	var costoperiodo=array.rentadeposito;
	    	var rentacolor=array.rentacolor;
	    	
	    	array.equipos.forEach(function(element) {

	    		if (array.statusnuevo==0) {
	    			//$('.f_c_cont_ini_'+element.id_equipo+'_'+element.serieId+'_'+element.tipo).val(element.contador_inicial);
	    			//$('.f_e_cont_ini_'+element.id_equipo+'_'+element.serieId+'_'+element.tipo).val(element.contador_iniciale);
	    			//$('.f_c_cont_ini_'+element.id_equipo+'_'+element.serieId+'_2').val(element.contador_inicial);
	    			//$('.f_e_cont_ini_'+element.id_equipo+'_'+element.serieId+'_2').val(element.contador_iniciale);
	    			//$('.total_toner_'+element.id_equipo+'_'+element.serieId+'_'+element.tipo).val(array.toner_consumido/100);
	    			//totaltoner(element.id_equipo,element.serieId,element.tipo);
	    		}else{
	    			$('.prefdId_'+element.productoId+'_'+element.serieId+'_'+element.tipo+'_edit').val(parseFloat(element.prefdId));
	    			$('.f_c_cont_ini_'+element.productoId+'_'+element.serieId+'_'+element.tipo+'_edit').val(parseFloat(element.c_c_i));
	    			$('.f_c_cont_fin_'+element.productoId+'_'+element.serieId+'_'+element.tipo+'_edit').val(parseFloat(element.c_c_f));
	    			$('.f_e_cont_ini_'+element.productoId+'_'+element.serieId+'_'+element.tipo+'_edit').val(parseFloat(element.e_c_i));
	    			$('.f_e_cont_fin_'+element.productoId+'_'+element.serieId+'_'+element.tipo+'_edit').val(parseFloat(element.e_c_f));
	    			$('.descuento_'+element.productoId+'_'+element.serieId+'_'+element.tipo+'_edit').val(parseFloat(element.descuento));
	    			$('.descuentoe_'+element.productoId+'_'+element.serieId+'_'+element.tipo+'_edit').val(parseFloat(element.descuentoe));
	    			//$('.total_toner_'+element.id_equipo+'_'+element.serieId+'_'+element.tipo).val(array.toner_consumido/100);
	    			//totaltoner(element.productoId,element.serieId,element.tipo);
	    		}
	    		
	    	});
	    	/*array.toners_cant.forEach(function(element) {
	    		console.log("toner_cons: "+'total_toner_'+element.id_equipo+'_'+element.serieId+'_'+element.tipo)
	    		$('.total_toner_'+element.id_equipo+'_'+element.serieId+'_'+element.tipo).val(array.toner_consumido/100);
	    	});*/
	    	if(array.statusnuevo==0){
	    		//$('.excedentesmono').hide();
		    	//$('.excedentescolor').hide();
	    	}else{
	    		if (array.tipo4==1) {
		    		$('.excedentesmono_edit').show();
		    		$('.excedentescolor_edit').hide();
		    	}else if (array.tipo4==2){
		    		$('.excedentesmono_edit').show();
		    		$('.excedentescolor_edit').show();
		    	}else if (array.tipo4==3){
		    		$('.excedentesmono_edit').show();
		    		$('.excedentescolor_edit').show();
		    	}else if (array.tipo4==4){
		    		$('.excedentesmono_edit').hide();
		    		$('.excedentescolor_edit').show();
		    	}
	    	}
	    	
	    	if (array.tipo2==1) {
	    		$('.S').show();
	    	}else{
	    		$('.removetipo2').hide();
	    	}
	    	$('#costoperiodoexcedente').val(0);
			$('#costoperiodoexcedente_c').val(0);
	    	$('#costoperiodo').val(costoperiodo);
	    	$('#costoperiodocolor').val(rentacolor);
	    	if (rentacolor<=0) {
	    		$('.removecostoperiodocolor').hide();
	    	}

	    	calculartotalperiodo_edit();
	    	$('.removeproduccion').hide();
	    	$('.f_c_c_f_edit').change();
	    }
    });
    setTimeout(function(){
    	calculatdatosproductosedit();
    	//verificaimg();
    	$('.prfprocontador_edit').change();
    }, 1000);
    var tprofactura = $('#profactura');
	tprofactura.floatThead();
	tprofactura.trigger('reflow');
}
function calculatdatosproductosedit(){
	$('.prfprocontador_edit').change(function(event) {
		var equipo=$(this).data('equipo');
		var serie=$(this).data('serie');
		var moco=$(this).data('moco');
		var tipo=$(this).data('tipo');
		var tomarescaner=$(this).data('tomarescaner');
		var cci=$('.f_c_cont_ini_'+equipo+'_'+serie+'_'+moco+'_edit').val()==''?0:$('.f_c_cont_ini_'+equipo+'_'+serie+'_'+moco+'_edit').val();
		var ccf=$('.f_c_cont_fin_'+equipo+'_'+serie+'_'+moco+'_edit').val()==''?0:$('.f_c_cont_fin_'+equipo+'_'+serie+'_'+moco+'_edit').val();
		var desc=$('.descuento_'+equipo+'_'+serie+'_'+moco+'_edit').val()==''?0:$('.descuento_'+equipo+'_'+serie+'_'+moco+'_edit').val();
		var desce=$('.descuentoe_'+equipo+'_'+serie+'_'+moco+'_edit').val()==''?0:$('.descuentoe_'+equipo+'_'+serie+'_'+moco+'_edit').val();
		if (tomarescaner==1) {
			var eci=$('.f_e_cont_ini_'+equipo+'_'+serie+'_'+moco+'_edit').val()==''?0:$('.f_e_cont_ini_'+equipo+'_'+serie+'_'+moco+'_edit').val();
			var ecf=$('.f_e_cont_fin_'+equipo+'_'+serie+'_'+moco+'_edit').val()==''?0:$('.f_e_cont_fin_'+equipo+'_'+serie+'_'+moco+'_edit').val();
		}else{
			var eci=0;
			var ecf=0;
		}
		
		var producccion_copia = parseFloat(ccf)-parseFloat(cci);
		$('.f_c_cont_pro_'+equipo+'_'+serie+'_'+moco+'_edit').val(producccion_copia);
		var producccion_escaner = parseFloat(ecf)-parseFloat(eci);
		$('.f_e_cont_pro_'+equipo+'_'+serie+'_'+moco+'_edit').val(producccion_escaner);


		var produccion = parseFloat(producccion_copia+'_edit')+parseFloat(producccion_escaner)-parseFloat(desc)-parseFloat(desce);
		$('.f_produccion_'+equipo+'_'+serie+'_'+moco+'_edit').val(produccion);
		var tonerconsumido=(parseFloat(ccf)-parseFloat(cci))/$('.nuevo_'+equipo+'_'+serie+'_'+tipo).val();
		tonerconsumido=parseFloat(tonerconsumido)*100;
		$('.f_toner_consumido_'+equipo+'_'+serie+'_'+moco+'_edit').val(Math.round(tonerconsumido));
		$('.produccion_total_'+equipo+'_'+serie+'_'+moco+'_edit').val(produccion);//tengo dudas con este
		//$('.total_toner'+equipo+'_'+serie+'_'+moco).val(produccion); //aqui estoy con el calculo de toner consumido
		
		if ($('#tipo2 option:selected').val()==1) {
			var cantidad = $('.f_produccion_'+equipo+'_'+serie+'_'+moco+'_edit').val();
			var clicks_mono = $('.clicks_mono_'+equipo+'_'+serie).val();
			var precio_c_e_mono = $('.precio_c_e_mono_'+equipo+'_'+serie).val();

			var clicks_color = $('.clicks_color_'+equipo+'_'+serie).val();
			var precio_c_e_color = $('.precio_c_e_color_'+equipo+'_'+serie).val();

			if (moco==1) {
				var excedente =parseFloat(cantidad)-parseFloat(clicks_mono);
				var excedentescosto= precio_c_e_mono;
			}else{
				var excedente =parseFloat(cantidad)-parseFloat(clicks_color);
				var excedentescosto= precio_c_e_color;
			}
			var excedentetotal=parseFloat(excedente)*parseFloat(excedentescosto);
			if (excedente<0) {
				excedente=0;
				excedentetotal=0;
			}

			$('.excedentetotal_'+equipo+'_'+serie+'_'+moco+'_edit').val(excedentetotal.toFixed(2));
			$('.excedentescosto_'+equipo+'_'+serie+'_'+moco+'_edit').val(excedentescosto);
			$('.excedentes_'+equipo+'_'+serie+'_'+moco+'_edit').val(excedente);
		}
		calculartotalpedit();
	});
}
function editarcontadoress(){
	var permitido=1;
	var DATA4  = [];
    var TABLA4   = $("#profactura_edit tbody > tr");
        TABLA4.each(function(){         
            item2 = {};
            item2 ["prefdId"] = $(this).find("input[class*='prefdId_edit']").val();
            item2 ["c_c_i"]  = $(this).find("input[class*='f_c_c_i_edit']").val();
            item2 ["c_c_f"]  = $(this).find("input[class*='f_c_c_f_edit']").val();
            item2 ["e_c_i"] = $(this).find("input[class*='f_e_c_i_edit']").val();
            item2 ["e_c_f"] = $(this).find("input[class*='f_e_c_f_edit']").val();
            item2 ["descuento"] = $(this).find("input[class*='descuento_edit']").val();
            item2 ["descuentoe"] = $(this).find("input[class*='descuentoe_edit']").val();
            item2 ["produccion"] = $(this).find("input[class*='f_produccion_edit']").val();
            item2 ["toner_consumido"] = $(this).find("input[class*='f_toner_consumido_edit']").val();
            item2 ["produccion_total"] = $(this).find("input[class*='produccion_total_edit']").val();
            
            	item2 ["excedentes"] = $(this).find("input[class*='excedentes_edit']").val();
            	item2 ["costoexcedente"] = $(this).find("input[class*='excedentescosto_edit']").val();
            	item2 ["totalexcedente"] = $(this).find("input[class*='excedentestotal_edit']").val();
            if (parseFloat($(this).find("input[class*='f_c_c_f_edit']").val()) < parseFloat($(this).find("input[class*='f_c_c_i_edit']").val())) {
            	permitido=0;
            }
            
            if ($(this).find("input[class*='f_c_c_f_edit']").data('tomarescaner')==1) {
            	if (parseFloat($(this).find("input[class*='f_e_c_f_edit']").val()) < parseFloat($(this).find("input[class*='f_e_c_i_edit']").val())) {
                	permitido=0;
                }
            }
            
            
           // item2 ["accesorios"]  = $(this).find("input[id*='accesorios']").val();
            DATA4.push(item2);
        });
        dataequipos   = JSON.stringify(DATA4);
        if (permitido==1) {
        	$.ajax({
			    type:'POST',
			    url: base_url+"index.php/Configuracionrentas_ext/editcontadores",
			    data: {
			    	id_polizar:$('#idpolizaedit').val(),
			    	subtotal:$('#costoperiodo_edit').val(),
			    	subtotalcolor:$('#costoperiodocolor_edit').val(),
			    	excedente:$('#costoperiodoexcedente_edit').val(),
			    	excedentecolor:$('#costoperiodoexcedente_c_edit').val(),
			    	total:$('#totalperiodo_edit').val(),
			    	fechainicial:$('#periodo_inicial_edit').val(),
			    	fechafin:$('#periodo_final_edit').val(),
			    	equipos:dataequipos
			    },
			    success:function(data){  
			    	var facturado = parseInt(data);
			    	//$('#modalcontinuarcontadores').modal();
			    	$('#modalcontinuarcontadores').modal('close');
			    	swal({   
			    		title: "Contadores editados!",
					    text: "",
					    timer: 2000,
					    showConfirmButton: false
					  });
			    	if(facturado>0){
			    		liberarperiodofac(facturado);
			    	}else{
			    		setTimeout(function(){
				    		location.reload();
				    	}, 1000);
			    	}
			    	
			    }
		    });
        }else{
        	swal({   
	    		title: "No permitido",
			    text: "verifique sus contadores",
			    timer: 2000,
			    showConfirmButton: false
			  });
        }
}
function viewcondiciones(id){
	//console.log('viewcondiciones: '+id);
	$.ajax({
	    type:'POST',
	    url: base_url+"index.php/Generales/viewcondiciones",
	    data: {
	    	id:id
	    },
	    success:function(data){  
	    	var array = $.parseJSON(data);
	    	$('#tipo2').val(array.datos.tipo).attr('disabled', false).trigger("chosen:updated").change();
	    	if(array.datos.tipo==2){//global
	    		$('#monto').val(array.datos.renta_m);
	    		$('#montoc').val(array.datos.renta_c);
	    		$('input[name=clicks_mono]').val(array.datos.click_m);
                $('input[name=precio_c_e_mono]').val(array.datos.excedente_m);
                $('input[name=clicks_color]').val(array.datos.click_c);
                $('input[name=precio_c_e_color]').val(array.datos.excedente_c);
	    	}
	    	if(array.datos.tipo==1){//individual
	    		array.datosd.forEach(function(element) {
                    //console.log(element)
                    
                    if (element.renta_m>0 || element.renta_c>0) {
                        /*
                       $('.renta_'+element.id_equipo+'_'+element.serieId+'_'+element.tipo).val(element.renta);
                       $('.clicks_mono_'+element.id_equipo+'_'+element.serieId+'_'+element.tipo).val(element.clicks_mono);
                       $('.precio_c_e_mono_'+element.id_equipo+'_'+element.serieId+'_'+element.tipo).val(element.precio_c_e_mono);
                       $('.clicks_color_'+element.id_equipo+'_'+element.serieId+'_'+element.tipo).val(element.clicks_color);
                       $('.precio_c_e_color_'+element.id_equipo+'_'+element.serieId+'_'+element.tipo).val(element.precio_c_e_color); 
                       */
                        
                       $('.renta_'+element.equiporow+'_'+element.serieId).val(element.renta_m);
                       $('.rentac_'+element.equiporow+'_'+element.serieId).val(element.renta_c);
                       $('.clicks_mono_'+element.equiporow+'_'+element.serieId).val(element.click_m);
                       $('.precio_c_e_mono_'+element.equiporow+'_'+element.serieId).val(element.excedente_m);
                       $('.clicks_color_'+element.equiporow+'_'+element.serieId).val(element.click_c);
                       $('.precio_c_e_color_'+element.equiporow+'_'+element.serieId).val(element.excedente_c); 
                    }
                });
	    	}
	    }
    });
}
function liberarperiodofac(prefId){
	$.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: 'Periodo ya fue facturado<br>'+
                 '¿Desea liberarlo para una nueva facturación?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                
                    $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Configuracionrentas_ext/liberarperiodofac",
                        data: {
                            prefId:prefId
                        },
                        success: function (response){
                                
                                    $.alert({
                                    boxWidth: '30%',
                                    useBootstrap: false,
                                    title: 'Hecho!',
                                    content: 'Periodo Liberado'}); 
                                $('#test_tab_5').click();
                        },
                        error: function(response){
                            notificacion(1);  
                        }
                    });
            },
            cancelar: function (){
                $('#test_tab_5').click();
            }
        }
    });
}
function editarcondicion(prefId,idcondicion){
	$.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Desea editar condiciones de periodo?<br> Para continuar se necesita permisos de administrador<br>'+
                 '<input type="text" placeholder="Contraseña" id="contrasena" name="contrasena" class="name form-control" data-val="" onpaste="return false;" maxlength="10"/>',
        type: 'red',
        typeAnimated: true,
        buttons:{

            confirmar: function (){
                //var pass=$('#contrasena').val();
                var pass=$("input[name=contrasena]").val();
                if (pass!='') {
                     $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Sistema/solicitarpermiso",
                        data: {
                            pass:pass
                        },
                        success: function (response){
                                var respuesta = parseInt(response);
                                if (respuesta==1) {
                                	editarcondicion2(prefId,idcondicion);
                                }else{
                                    notificacion(2);
                                }
                        },
                        error: function(response){
                            notificacion(1);
                    
                        }
                    });
                    
                }else{
                    notificacion(0);
                }
            },

            cancelar: function (){
               
            }
        }
    });
    setTimeout(function(){
        new MaskedPassword(document.getElementById("contrasena"), '\u25CF');
    },1000);
}
function editarcondicion2(prefId,idcondicion){
	$('#modal_condiciones').modal('open');
	obtenerdatoscondiciones(prefId,idcondicion);
}
function con_tipochange(){
    var con_tipo = $('#con_tipo option:selected').val();
    if(con_tipo==1){

        $('.div_individual').show('show');
        $('.div_global').hide('show');
    }
    if(con_tipo==2){
        $('.div_global').show('show');
        $('.div_individual').hide('show');
        $('.div_individual .inc').val(0);
    }
}
function obtenerdatoscondiciones(prefId,idcondicion){
	$('#con_preid').val(prefId);
	$('.tbody_table_individual').html('');
	$.ajax({
        type:'POST',
        url: base_url+'index.php/Rentas/viewcondi2',
        data: {
            id:idcondicion,
            prefId:prefId
        },
        success:function(data){
            var array = $.parseJSON(data);
            //console.log(array);
            $('#con_id').val(0);
            $('#con_tipo').val(array.datos.tipo).change();
            $('#con_fechainicial').val('');
            $('#con_renta_m').val(array.datos.renta_m);
            $('#con_click_m').val(array.datos.click_m);
            $('#con_excedente_m').val(array.datos.excedente_m);
            $('#con_renta_c').val(array.datos.renta_c);
            $('#con_click_c').val(array.datos.click_c);
            $('#con_excedente_c').val(array.datos.excedente_c);
            $('#comentario').val(array.datos.comentario);
            array.datosdll.forEach(function(element) {
                //console.log(element);
                /*
                $('.con_id_'+element.equiporow+'_'+element.serieId).val(element.id)

                $('.cond_renta_m_con_idd_'+element.equiporow+'_'+element.serieId).val(element.renta_m);
                $('.cond_click_m_con_idd_'+element.equiporow+'_'+element.serieId).val(element.click_m);
                $('.cond_excedente_m_con_idd_'+element.equiporow+'_'+element.serieId).val(element.excedente_m);
                $('.cond_renta_c_con_idd_'+element.equiporow+'_'+element.serieId).val(element.renta_c);
                $('.cond_click_c_con_idd_'+element.equiporow+'_'+element.serieId).val(element.click_c);
                $('.cond_excedente_c_con_idd_'+element.equiporow+'_'+element.serieId).val(element.excedente_c);
                */

                var html='<tr>\
                        <td>\
                          <input type="hidden" id="con_id" class="inc con_id_'+element.equiporow+'_'+element.serieId+'" value="0">\
                          <input type="hidden" id="con_equiporow"  value="'+element.equiporow+'">\
                          <input type="hidden" id="con_serieId"  value="'+element.serieId+'">\
                          '+element.modelo+'\
                        </td>\
                        <td>'+element.serie+'</td>\
                        <td><input type="number" class="form-control-bmz input_select_con inc cond_renta_m_con_idd_'+element.equiporow+'_'+element.serieId+'" id="cond_renta_m" value="'+element.renta_m+'" ></td>\
                        <td><input type="number" class="form-control-bmz input_select_con inc cond_click_m_con_idd_'+element.equiporow+'_'+element.serieId+'" id="cond_click_m" value="'+element.click_m+'" ></td>\
                        <td><input type="number" class="form-control-bmz input_select_con inc cond_excedente_m_con_idd_'+element.equiporow+'_'+element.serieId+'" id="cond_excedente_m" value="'+element.excedente_m+'" ></td>\
                        <td><input type="number" class="form-control-bmz input_select_con inc cond_renta_c_con_idd_'+element.equiporow+'_'+element.serieId+'" id="cond_renta_c" value="'+element.renta_c+'" ></td>\
                        <td><input type="number" class="form-control-bmz input_select_con inc cond_click_c_con_idd_'+element.equiporow+'_'+element.serieId+'" id="cond_click_c" value="'+element.click_c+'" ></td>\
                        <td><input type="number" class="form-control-bmz input_select_con inc cond_excedente_c_con_idd_'+element.equiporow+'_'+element.serieId+'" id="cond_excedente_c" value="'+element.excedente_c+'" ></td>\
                      </tr>';
                $('.tbody_table_individual').append(html);
            });


        },
        error: function(response){
            toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema");                             
        }
    });

}
function savecondiciones(){
    var DATA  = [];

        var TABLA = $("#table_individual tbody > tr");
        TABLA.each(function(){         
            item = {};
            item['id'] = $(this).find("input[id*='con_id']").val();
            item['equiporow'] = $(this).find("input[id*='con_equiporow']").val();
            item['serieId'] = $(this).find("input[id*='con_serieId']").val();
            item['renta_m'] = $(this).find("input[id*='cond_renta_m']").val();
            item['click_m'] = $(this).find("input[id*='cond_click_m']").val();
            item['excedente_m'] = $(this).find("input[id*='cond_excedente_m']").val();
            item['renta_c'] = $(this).find("input[id*='cond_renta_c']").val();
            item['click_c'] = $(this).find("input[id*='cond_click_c']").val();
            item['excedente_c'] = $(this).find("input[id*='cond_excedente_c']").val(); 
            if($(this).find("input[id*='con_id']").val()!=undefined){
                DATA.push(item);    
            }       
            
        });
    dataequiposclicks   = JSON.stringify(DATA);
    var form_condg = $('#form_condg');
    var datos = form_condg.serialize()+'&equipos='+dataequiposclicks;
    $.ajax({
        type:'POST',
        url: base_url+'index.php/Rentas/savecondiciones',
        data: datos,
        success:function(data){
            toastr["success"]("Condiciones guardadas");
            //var idRen = $('#idRen').val();
            //tablelistcon(idRen);
            $('#modal_condiciones').modal('close');
        },
        error: function(response){
            toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema");                             
        }
    });
}