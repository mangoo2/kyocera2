<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require 'vendor/autoload.php';//php 7.1 como minimo
//use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Asistencias extends CI_Controller {
    public function __construct()     {
        parent::__construct();

        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloGeneral');
        $this->load->model('ModeloAsistencia');
        $this->load->model('Login_model');
        $this->load->helper('url');
        date_default_timezone_set('America/Mexico_City');
        $this->fechacompleta = date('Y-m-d H:i:s');
        $this->fecha = date('Y-m-d');
        $this->horac = date('H:i:s');
        $this->hora = date('G');//hora sin ceros iniciales
        $this->minuto = date('i');//hora sin ceros iniciales
        $this->submenu=73;
        if($this->session->userdata('logeado')==true){
            $this->perfilid =$this->session->userdata('perfilid');
            $this->idpersonal = $this->session->userdata('idpersonal');
            $this->usuario = $this->session->userdata('usuario');
            
        }else{
            redirect('login'); 
        }
    }
    function index(){
        $data['MenusubId']=$this->submenu;
        $data['idpersonal']=$this->idpersonal;
        $this->load->view('header');
        $this->load->view('main');
        $this->load->view('asistencias/list',$data);
        
        $this->load->view('footerc2');  
        $this->load->view('asistencias/listjs');
    }
    public function getlists() {
        $params = $this->input->post();
        $serie = $this->ModeloAsistencia->getlists($params);
        $totalRecords= $this->ModeloAsistencia->getliststotal($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totalRecords),  
            "recordsFiltered" => intval($totalRecords),
            "data"            => $serie->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    function registro(){
        $params_e = $this->input->post();

        $params_e['personal']=$this->idpersonal;
        $params_e['fecha']=$this->fecha;
        $params_e['hora_entrada']=$this->horac;
        $params_s['hora_salida']=$this->horac;
        $params_e['ubicacion']=$_SERVER['REMOTE_ADDR'];
        $entrada=0;
        //====================================================
            $permitido=0;
            if($this->hora >= 7  && $this->hora <= 19){
                $permitido=1;
            }
        //====================================================
        if($permitido==1){
            $result = $this->ModeloCatalogos->getselectwheren('asistencias',array('fecha'=>$this->fecha,'personal'=>$this->idpersonal));
            if($result->num_rows()>0){
                $rowresult=$result->row();
                if($this->hora >=13  ){
                    
                    $params_s['ubicacion_salida']=$_SERVER['REMOTE_ADDR'];
                    $params_s['longitud_salida']=$params_e['longitud'];
                    $params_s['latitud_salida']=$params_e['latitud'];
                    $params_s['entrada_salida']=2;
                    $this->ModeloCatalogos->updateCatalogo('asistencias',$params_s,array('id'=>$rowresult->id));
                    $entrada=0;
                }else{
                    $entrada=1;
                }
                
            }else{
                $entrada=1;
                $this->ModeloCatalogos->Insert('asistencias',$params_e);   
            }
        }
        echo $entrada;
    }
    function ubicacion($longitud,$latitud){
        $data['longitud']=$longitud;
        $data['latitud']=$latitud;
        $this->load->view('asistencias/ubicacion',$data);
    }
    function obtenerubicacion(){
        $this->load->view('asistencias/obtenerubicacion');
    }
    function espefalta(){
        $params = $this->input->post();
        $asi=$params['asi'];
        $per=$params['per'];
        $fecha=$params['fecha'];
        $efa = $params['efa'];
        if($asi>0){
            $params_s['estatus']=$efa;
            $this->ModeloCatalogos->updateCatalogo('asistencias',$params_s,array('id'=>$asi));
        }else{
            if($efa>0){
                $params_e['personal']=$per;
                $params_e['fecha']=$fecha;
                $params_e['estatus']=$efa;
                $this->ModeloCatalogos->Insert('asistencias',$params_e);  
            }
        }
    }
    function savecoor(){
        $params = $this->input->post();

        $longitud = $params['longitud'];
        $latitud = $params['latitud'];
        $idrow = $params['idrow'];

        $tipodireccion=explode('_',$idrow);

        $tipodir = $tipodireccion[0];
        $iddir   = $tipodireccion[1];

        if ($tipodir=='g') {
            $resultdirc_where['idclientedirecc']=$iddir;

            $this->ModeloCatalogos->updateCatalogo('clientes_direccion',array('longitud'=>$longitud,'latitud'=>$latitud),$resultdirc_where);
            
        }
        if ($tipodir=='f') {
            $resultdirc_where['id']=$iddir;
            
            $this->ModeloCatalogos->updateCatalogo('cliente_has_datos_fiscales',array('longitud'=>$longitud,'latitud'=>$latitud),$resultdirc_where);
            
            
        }
    }
    function darentrada(){
        $params  = $this->input->post();
        $idp     = $params['idp'];
        $horae   = $params['horae'];
        $fechahoy= $params['fechahoy'];
        $pass    = $params['pass'];

        $permiso=0;
        $permisos = $this->Login_model->permisoadminarray(array('1','17','18'));//1admin 17 julio 18 diana

        foreach ($permisos as $item) {
            $permisotemp=0;

            $usuario=$item->Usuario;

            $verificar = password_verify($pass,$item->contrasena);

            if ($verificar==false) {
                $verificar = password_verify($pass,$item->clavestemp);
                if ($verificar) {
                    $permisotemp=1;
                }
            }
            if ($verificar) {
                $permiso=1;

                $this->ModeloCatalogos->Insert('asistencias',array('personal'=>$idp,'fecha'=>$fechahoy,'entrada_salida'=>1,'hora_entrada'=>$horae)); 

                if($permisotemp==1){
                    $this->ModeloCatalogos->updateCatalogo('usuarios',array('clavestemp'=>null),array('UsuarioID'=>$item->UsuarioID));
                }

                
            }
        }
        echo $permiso;
    }
    function editarhorario(){
        $params  = $this->input->post();
        $id     = $params['id'];
        $horae   = $params['horae'];
        $horas   = $params['horas'];
        $pass    = $params['pass'];

        $permiso=0;
        $permisos = $this->Login_model->permisoadminarray(array('1','17','18'));//1admin 17 julio 18 diana

        foreach ($permisos as $item) {
            $permisotemp=0;

            $usuario=$item->Usuario;

            $verificar = password_verify($pass,$item->contrasena);

            if ($verificar==false) {
                $verificar = password_verify($pass,$item->clavestemp);
                if ($verificar) {
                    $permisotemp=1;
                }
            }
            if ($verificar) {
                $permiso=1;
                $params_edit['hora_entrada']=$horae;
                if($horas!=''){
                    $params_edit['hora_salida']=$horas;
                    $params_edit['entrada_salida']=2;
                }
                
                $this->ModeloCatalogos->updateCatalogo('asistencias',$params_edit,array('id'=>$id));

                if($permisotemp==1){
                    $this->ModeloCatalogos->updateCatalogo('usuarios',array('clavestemp'=>null),array('UsuarioID'=>$item->UsuarioID));
                }

                
            }
        }
        echo $permiso;
    }
    function searchempleados(){
      $search = $this->input->get('search');
      $results=$this->ModeloAsistencia->searchempleados($search);
        echo json_encode($results->result());
    }
    function vacacionesadd(){
        $params = $this->input->post();
        $personalId = $params['personalId'];
        $fi = $params['fi'];
        $ff = $params['ff'];
        $tipo = $params['tipo'];
        $this->ModeloCatalogos->Insert('asistencias_vacaciones',array('idpersonal'=>$personalId,'fechainicio'=>$fi,'fechafin'=>$ff,'tipo'=>$tipo)); 
    }
    function liscalendario(){
        $data = $this->input->post();
       $inicio = date('Y-m-d',$data['start']);
       $fin = date('Y-m-d',$data['end']);
       //$inicio = $data['start'];
       //$fin = $data['end'];

        $result = $this->ModeloAsistencia->liscalendar($inicio,$fin);
       echo json_encode($result->result());
    }
    public function getlistvacaciones() {
        $params = $this->input->post();
        $getdata = $this->ModeloAsistencia->getlistsvc($params);
        $totaldata= $this->ModeloAsistencia->getlistsvctotal($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    function editarfechavaca(){
        $params = $this->input->post();
        $id = $params['id'];
        $nfi = $params['nfi'];
        $nff = $params['nff'];
        $nfp = $params['nfp'];
        if($nfp==''){
            $nfp=null;
        }
        $this->ModeloCatalogos->updateCatalogo('asistencias_vacaciones',array('fechainicio'=>$nfi,'fechafin'=>$nff,'fechapermitida'=>$nfp),array('id'=>$id));
    }
    function deletevaca(){
        $params = $this->input->post();
        $id = $params['id'];
        $this->ModeloCatalogos->updateCatalogo('asistencias_vacaciones',array('activo'=>0),array('id'=>$id));
    }
    function deleteentrada(){
        $params = $this->input->post();
        $pass=$params['pass'];
        $id = $params['id'];

        $permisos = $this->Login_model->permisogeneral($pass);
        if($permisos==1){
            //===========================
                $this->ModeloCatalogos->getdeletewheren('asistencias',array('id'=>$id));
            //===========================
        }

        echo $permisos;
    }
    function exportar(){
        // esta funcion tiene que tener la misma estructura que el listado de asistencias asistencia.js load()
        $fechainicio    =$_GET['finicio'];
        $fechafin       =$_GET['ffin'];

        //$this->load->view('Reportes/exportarservicios',$data);
        //$this->load->view('Reportes/exportarservicios_xlsx',$data);
        //=====================================================================
            $spreadsheet = new Spreadsheet();
            $fecha_inicial = new DateTime($fechainicio);
            $fecha_final = new DateTime($fechafin);
            // Necesitamos modificar la fecha final en 1 día para que aparezca en el bucle
            $fecha_final = $fecha_final ->modify('+1 day');

            $intervalo = DateInterval::createFromDateString('1 day');
            $periodo = new DatePeriod($fecha_inicial , $intervalo, $fecha_final);
            $row_h=0;
            foreach ($periodo as $dt) {
                $fecha_de_consulta=$dt->format("Y-m-d");
                
                $params['fechainicio']=$fecha_de_consulta;
                $params['fechafin']=$fecha_de_consulta;

                if($row_h==0){
                    $sheet = $spreadsheet->getActiveSheet();
                }else{
                    $spreadsheet->createSheet();
                    $sheet = $spreadsheet->setActiveSheetIndex($row_h);
                }
                $sheet->setTitle($fecha_de_consulta);
                //echo $fecha_de_consulta;
                //===========================================
                    $sheet->setCellValue('A1', '#');
                    $sheet->setCellValue('B1', 'Empresa');
                    $sheet->setCellValue('C1', 'Id Empleado');
                    $sheet->setCellValue('D1', 'Empleado');
                    $sheet->setCellValue('E1', 'Día');
                    $sheet->setCellValue('F1', 'Hora inicio');
                    $sheet->setCellValue('G1', 'Hora fin');
                    $sheet->setCellValue('H1', '');
                //===========================================
                    $rc=2;
                    //===============================================================
                    $numeroDia=date('w', strtotime($fecha_de_consulta));
                    $horahoy=date('G');
                        $strq="SELECT 
                                    asi.id, per.emp, per.personalId, per.nombre, asi.fecha, asi.hora_entrada, asi.hora_salida, per.apellido_paterno, per.apellido_materno, asi.longitud, asi.latitud, asi.ubicacion, asi.ubicacion_salida, asi.longitud_salida, asi.latitud_salida, asi.entrada_salida, asi.estatus, 
                                    (SELECT av.tipo FROM asistencias_vacaciones as av WHERE av.idpersonal=per.personalId AND av.fechainicio <= '$fecha_de_consulta' AND av.fechafin >= '$fecha_de_consulta') as tipo_av
                                    FROM personal per
                                    LEFT JOIN asistencias asi ON asi.personal=per.personalId and asi.fecha='$fecha_de_consulta' 
                                    WHERE (per.estatus = 1 or asi.activo = 1)
                                    ORDER BY asi.id DESC";
                            $query = $this->db->query($strq);

                            foreach ($query->result() as $item) {
                                $colorcelda='ffffff';
                                //=====================================
                                    if($item->emp==1){
                                        $emp='Alta Productividad';
                                    }else{
                                        $emp='D-Impresión';
                                    }
                                //========================================
                                    $hora_salida='';
                                    if($item->id>0){
                                        if($item->hora_salida=='' || $item->hora_salida==null){
                                            if($item->fecha<$fecha_de_consulta){
                                                //log_message('error','Personal:'.$item->personalId.' $item->fecha('.$item->fecha.')<$fecha_de_consulta('.$fecha_de_consulta.')');
                                                $hora_salida ='17:00:00 a';
                                                if($item->emp==2){
                                                    $hora_salida ='18:00:00 b';    
                                                }
                                            }
                                            if($item->fecha==$fecha_de_consulta){
                                                if($horahoy>=17){
                                                    $hora_salida ='17:00:00 c';
                                                    if($item->emp==2){
                                                        $hora_salida ='18:00:00 d';    
                                                    }
                                                }
                                            }
                                            
                                        }else{
                                            $hora_salida = $item->hora_salida;
                                        }
                                    }
                                //============================================
                                    $status_asis ='';
                                    //$btn_falta='<a class="b-btn b-btn-danger tooltipped" data-position="top" data-delay="50" data-tooltip="Falta">F</a>';
                                    $btn_falta='F';
                                    $colorcelda='FF0000';//rojo
                                    if($item->entrada_salida>0){
                                        //$status_asis = '<a class="b-btn b-btn-success tooltipped" data-position="top" data-delay="50" data-tooltip="Asistencia">A</a>';
                                        $status_asis = 'A';
                                        $colorcelda='00FF00';//verde
                                    }
                                    if($numeroDia>0){
                                        if($item->fecha==null){
                                            $status_asis = $btn_falta;
                                            //console.log('f1');
                                        }else{
                                            if($item->fecha==$fecha_de_consulta){
                                                if($horahoy>=17){
                                                    if($item->entrada_salida>0){
                                                        //html = btn_falta;
                                                    }else{
                                                        $status_asis = $btn_falta;
                                                        //console.log('f2');
                                                    }
                                                }
                                            }else{
                                                if($item->latitud_salida>0){
                                                        //html = btn_falta;
                                                }else{
                                                    //html = btn_falta;
                                                    //console.log('f3');
                                                }
                                            }
                                        }
                                    }
                                    if ($item->estatus==1) {
                                        //$status_asis = '<a class="b-btn b-btn-success tooltipped" data-position="top" data-delay="50" data-tooltip="Permiso">P</a>';
                                        $status_asis = 'P';
                                        $colorcelda='00FF00';//verde
                                    }
                                    if ($item->estatus==2) {
                                        //$status_asis = '<a class="b-btn b-btn-success tooltipped" data-position="top" data-delay="50" data-tooltip="Vacaciones">V</a>';
                                        $status_asis = 'V';
                                        $colorcelda='00FF00';//verde
                                    }
                                    if ($item->estatus==3) {
                                        //$status_asis = '<a class="b-btn b-btn-success tooltipped" data-position="top" data-delay="50" data-tooltip="Incapacidad">V</a>';
                                        $status_asis = 'V';
                                        $colorcelda='00FF00';//verde
                                    }
                                    if ($item->estatus==4) {
                                        //$status_asis = '<a class="b-btn b-btn-success tooltipped" data-position="top" data-delay="50" data-tooltip="Retardo">V</a>';
                                        $status_asis = 'V';
                                        $colorcelda='00FF00';//verde
                                    }
                                    if ($item->tipo_av==1) {
                                        //$status_asis = '<a class="b-btn b-btn-success tooltipped" data-position="top" data-delay="50" data-tooltip="Vacaciones">V</a>';
                                        $status_asis = 'V';
                                        $colorcelda='00FF00';//verde
                                    }
                                    if ($item->tipo_av==2) {
                                        //$status_asis = '<a class="b-btn b-btn-success tooltipped" data-position="top" data-delay="50" data-tooltip="Incapacidad">V</a>';
                                        $status_asis = 'V';
                                        $colorcelda='00FF00';//verde
                                    }
                                //=============================================
                                    $sheet->setCellValue('A'.$rc, $item->id);
                                    $sheet->setCellValue('B'.$rc, $emp);
                                    $sheet->setCellValue('C'.$rc, $item->personalId);
                                    $sheet->setCellValue('D'.$rc, $item->nombre.' '.$item->apellido_paterno.' '.$item->apellido_materno);
                                    $sheet->setCellValue('E'.$rc, $item->fecha);
                                    $sheet->setCellValue('F'.$rc, $item->hora_entrada);
                                    $sheet->setCellValue('G'.$rc, $hora_salida);
                                    $sheet->setCellValue('H'.$rc, $status_asis);
                                    $cell='H'.$rc;
                                    $style = $sheet->getStyle($cell);
                                    $style->applyFromArray([
                                        'fill' => [
                                            'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                                            'startColor' => ['rgb' => $colorcelda],
                                        ],
                                    ]);

                                $rc++;
                            }
                    //===============================================================
                   
                    
                    
                    
                $row_h++;
            }
        
        //=====================================================================
            $tiposave=0;//0 guarde directamente 1 descargue
            if($tiposave==1){
                /*
                $writer = new Xlsx($spreadsheet);

                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header('Content-Disposition: attachment;filename="archivo.xlsx"');
                header('Cache-Control: max-age=0');

                // Guardar el archivo en la salida del búfer de salida (output buffer)
                $writer->save('php://output');
                */
            }else{
                // Guardar el archivo XLSX
                $writer = new Xlsx($spreadsheet);
                $urlarchivo0='fichero_reporte/archivoasi.xlsx';
                $urlarchivo=FCPATH.$urlarchivo0;
                $writer->save($urlarchivo);

                redirect(base_url().$urlarchivo0); 

            }
            // Configurar encabezados para la descarga
    }
    public function getlistpermisosfuera() {
        $params = $this->input->post();
        $getdata = $this->ModeloAsistencia->getlistfhp($params);
        $totaldata= $this->ModeloAsistencia->getlistfhptotal($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    function registrarpermiso(){
        $params =$this->input->post();
        $personalid=$params['personalId'];
        $fecha=$params['fecha'];
        $this->ModeloCatalogos->Insert('asistencias_per_fhorario',array('personalid'=>$personalid,'fecha'=>$fecha,'personalpermiso'=>$this->idpersonal)); 
    }
    function deletefhp(){
        $params =$this->input->post();
        $id=$params['id'];

        $this->ModeloCatalogos->updateCatalogo('asistencias_per_fhorario',array('activo'=>0),array('id'=>$id));
    }
    function rutas(){
        $params = $this->input->post();

        $params['idpersonal']=$this->idpersonal;
        $params['ubicacion']=$_SERVER['REMOTE_ADDR'];
        $this->ModeloCatalogos->Insert('personal_ubicaciones',$params); 
    }
    function view_rutas($idper,$fecha=0){
        if($fecha==0){
            $fecha=$this->fecha;
        }
        $strq="SELECT * FROM personal_ubicaciones where idpersonal='$idper' and activo=1 AND reg BETWEEN '$fecha 00:00:00' AND '$fecha 23:59:59'";
        $data['query'] = $this->db->query($strq);
        $this->load->view('asistencias/view_rutas',$data);
    }
    function confirmassolicitud(){
        $params = $this->input->post();
        $id = $params['id'];
        $autrech = $params['autrech'];
        if($autrech==1){//Autorizar
            $this->ModeloCatalogos->updateCatalogo('asistencias_vacaciones',array('aceptada_rechzada'=>1,'tipo'=>1),array('id'=>$id));
        }
        if($autrech==2){//Rechazar solicitud
            $this->ModeloCatalogos->updateCatalogo('asistencias_vacaciones',array('aceptada_rechzada'=>0),array('id'=>$id));
        }
    }

}
?>
