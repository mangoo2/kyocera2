var base_url = $('#base_url').val();
$(document).ready(function($) {
	
});
function generar(){
	var form = $('#form_reporte');
	if(form.valid()){
		loadtable();
	}
}
function loadtable(){
	$('body').loading({theme: 'dark',message: 'Procesando...'});
	var eje = $('#eje option:selected').val();
	var fecha = $('#fecha').val();
	var fechaf = $('#fechaf').val();
	if(eje>0 && fecha!=''){
		setTimeout(function(){ 
			$.ajax({
		        type:'POST',
		        url: base_url+'R_desfase/cargainfo',
		        data: {
		            eje: eje,
		            fecha: fecha,
		            fechaf:fechaf
		        },
		        async: false,
		        statusCode:{
		            404: function(data){
		                swal("Error", "404", "error");
		                $('body').loading('stop'); 
		            },
		            500: function(){
		            	$('body').loading('stop'); 
		                swal("Error", "500", "error"); 
		            }
		        },
		        success:function(data){
		        	$('body').loading('stop'); 
		            $('.carga_info').html(data);
		            $('#t_iti').DataTable({
				        //pagingType: 'input',
				        dom: 'Blfrtip',
				        buttons: [
				            'copyHtml5',
				            'excelHtml5',
				            {
			                    extend: 'pdfHtml5',
			                    orientation: 'landscape',
			                    pageSize: 'LEGAL'
			                }
				        ]});
		        }
		    });
	    }, 800);
	}
}
function viem_map(row){
	var ubiinicial = $('.row_tr_'+row).data('ubiinicial');
	var ubifinal = $('.row_tr_'+row).data('ubifinal');
	window.open(base_url+"index.php/Rutas_tecnicos/mapaview2?puntoa="+ubiinicial+"&puntob="+ubifinal, "ubicacion", "width=780, height=612");
}
function calcu_mes(){
	var menosmeses=1;//un mes
	var fini = $('#fechaf').val();

	var fechaFinal = moment(fini, "YYYY-MM-DD").subtract(menosmeses, 'months');

    // Formatear la fecha en formato YYYY-MM-DD
    var fechaInicialFormateada = fechaFinal.format("YYYY-MM-DD");

    // Actualizar el elemento con la fecha inicial
    console.log(fechaInicialFormateada);
    $('#fecha').prop('min',fechaInicialFormateada);
    $('#fecha').prop('max',fini);
}