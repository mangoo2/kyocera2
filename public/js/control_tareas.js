var base_url = $('#base_url').val();
var mv_idact;
var mv_estado;
var viewtipo = $('#viewtipo').val();//0 control de tareas 1 acuerdos
var idactividad=0;
$(document).ready(function($) {
	$('.modal').modal();
	recargardatos();
});
function addtarea(){
	//$('#modaladdtarea').modal('open');
	var nuevaVentana = window.open(base_url+"Control_tareas/iframeaddact/"+viewtipo, "Crear Actividad", "width=400, height=612");
}
function funcionPrincipal(){
	console.log("Función ejecutada en la ventana principal.");
	recargardatos();
}
function recargardatos(){
	datos_actividades(0);
	if(viewtipo==0){
		datos_actividades(1);
		datos_actividades(2);
		datos_actividades(3);
	}
	if(viewtipo==1){
		datos_actividades(4);
        datos_actividades(5);
	}
	
}
function datos_actividades(tipo){
	console.log('datos_actividades');
	var visor = $('#visor').val();
	var per = $('#personal option:selected').val();
	$.ajax({
	    type:'POST',
	    url: base_url+'Control_tareas/datos_actividades',
	    data: {
	        visor:visor,
	        tipo:tipo,
	        per:per,
	        viewtipo:viewtipo
	    },
	    async: false,
	    statusCode:{
	        404: function(data){
	            swal("Error", "404", "error");
	        },
	        500: function(){
	            swal("Error", "500", "error"); 
	        }
	    },
	    success:function(data){
	      $('.class_card_'+tipo).html(data);
	    }
	});
}
function arrastrar(ev,idact) {
    ev.dataTransfer.setData("Text", ev.target.id);
    console.log('idact: '+idact);
    mv_idact=idact;   
}
function permitirSoltar(ev, estado) {
    ev.preventDefault();
    mv_estado=parseInt(estado);
    console.log('mv_estado: '+mv_estado);
}
function soltar(ev, n1) {
    //ev.preventDefault();
    //var data = ev.dataTransfer.getData(n1);
    //$('#cambiarasi').modal('toggle');
    if(mv_idact>0 && mv_estado>=0){
    	confirmacionmv();
    }   
}
function confirmacionmv(){
	console.log('confirmacionmv');
	$.ajax({
	    type:'POST',
	    url: base_url+'Control_tareas/confirmacionmv',
	    data: {
	        idact:mv_idact,
	        estado:mv_estado
	    },
	    async: false,
	    statusCode:{
	        404: function(data){
	            swal("Error", "404", "error");
	        },
	        500: function(){
	            swal("Error", "500", "error"); 
	        }
	    },
	    success:function(data){
	      recargardatos();
	    }
	});
}
function info_ac(id){
	window.open(base_url+"Control_tareas/info_ac/"+id, "Prefactura", "width=700, height=660");
}
function deleteact(id){
	$.confirm({
        boxWidth: '35%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Confirma la eliminación de la actividad?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                
                     $.ajax({
                        type:'POST',
                        url: base_url+'Control_tareas/deleteact',
                        data: {
                            id:id
                        },
                        success: function (response){
                                recargardatos();
                                toastr["success"]("Eliminada con existo");
                        },
                        error: function(response){
                            
                            toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema");
                        }
                    });
                    
                
            },
            cancelar: function (){
                
            }
        }
    });
}
function finaliza(id){
	$.confirm({
        boxWidth: '35%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Confirma la finalizacion de la actividad?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                
                     $.ajax({
                        type:'POST',
                        url: base_url+'Control_tareas/finaliza',
                        data: {
                            id:id
                        },
                        success: function (response){
                                recargardatos();
                                toastr["success"]("Se finalizo correctamente");
                        },
                        error: function(response){
                            
                            toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema");
                        }
                    });
                    
                
            },
            cancelar: function (){
                
            }
        }
    });
}
function editarlimite(id){
	idactividad=id;
	var html_contect='Para continuar se necesita permisos de administrador<br>'+
                    '<input type="text" placeholder="Contraseña" id="contrasena2" name="contrasena2" class="name form-control-bmz" style="width:99%"  onpaste="return false;"/>';
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: html_contect,
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                //var pass=$('#contrasena').val();
                var pass=$("input[name=contrasena2]").val();
                if (pass!='') {
                     $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Sistema/solicitarpermiso",
                        data: {
                            pass:pass
                        },
                        success: function (response){
                                var respuesta = parseInt(response);
                                if (respuesta==1) {
                                	editarlimiteok();
                                }else{
                                    notificacion(2);
                                }
                        },
                        error: function(response){
                            notificacion(1);
                        }
                    });
                }else{
                    notificacion(0);
                }
                
            },
            cancelar: function (){

            }
        }
    });
    setTimeout(function(){
        new MaskedPassword(document.getElementById("contrasena2"), '\u25CF');
        
    },1000);
}
function editarlimiteok(){
	var date_o =$('actividad_'+idactividad).data('fecha');
	var html_contect='¿Desea editar la fecha?<br>'+
                    '<input type="date" placeholder="Fecha" id="new_fecha" class="form-control-bmz" value="'+date_o+'" style="width:99%"/>';
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: html_contect,
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var fecha=$("#new_fecha").val();
                if (fecha!='') {
                     $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Control_tareas/editarlimiteok",
                        data: {
                            fecha:fecha,
                            id:idactividad
                        },
                        success: function (response){
                            toastr.success('Fecha actualizada');
                            recargardatos();
                        },
                        error: function(response){
                            notificacion(1);
                        }
                    });
                }else{
                    toastr.error('No se permiten campos vacios');
                    editarlimiteok();
                }
               
            },
            cancelar: function (){

            }
        }
    });
}