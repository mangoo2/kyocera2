var base_url=$('#base_url').val();
$(document).ready(function($) {
    $('#metodopagoId').change(function(event) {
        if($('#metodopagoId').val()==2){//PPD
            $('#formapagoId option').prop('disabled',true);
            $('#formapagoId').val(21).trigger('change');
            $('#formapagoId option[value=21]').prop('disabled',false);
            //$('#MetodoPago').select2();
        }
        if($('#metodopagoId').val()==1){//PUE
            $('#formapagoId option').prop('disabled',false);
            $('#formapagoId option[value=21]').prop('disabled',true);
            //$('#MetodoPago').select2();
        }
    });
});
function editarseriepol(id,tipo){
	var html='<label>Se necesita permisos de administrador</label><br>\
				<input id="password" type="password" class="validate form-control" autocomplete="new-password" placeholder="Contraseña"><br>\
				<label>Nueva serie</label><br>\
				<input id="new_serie" type="text" class="form-control">';
	$.confirm({
        boxWidth: '41%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Editar serie',
        content: html,
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var pass=$('#password').val();
                var serie=$('#new_serie').val();
                if (pass!='') {
                    //if (precio>0) {
                        $.ajax({
                            type:'POST',
                            url: base_url+'Generales/editarserie',
                            data: {
		                        id: id,
		                        pass: pass,
		                        tipo: tipo,
		                        serie:serie
                                },
                                async: false,
                                statusCode:{
                                    404: function(data){
                                        swal("Error", "404", "error");
                                    },
                                    500: function(){
                                        swal("Error", "500", "error"); 
                                    }
                                },
                                success:function(data){
                                  if (data==1) {
                                    swal("Hecho!", "Realizado", "success");
                                    setTimeout(function(){ window.location.href=''; }, 3000);
                                  }else{
                                    swal("Error", "No tiene permiso", "error"); 
                                  }

                                }
                            });
                    //}else{
                    //    swal("Error", "El precio no debe de ser menor o igual a cero", "error"); 
                    //}
                }else{
                    swal("Error", "Debe de ingresar una contraseña", "error"); 
                }  
            },
            cancelar: function (){
            }
        }
    });
}
function deletepro(tipo,id){
    $.confirm({
        boxWidth: '41%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Eliminar',
        content: '¿Confirma la eliminación del producto?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                
                    //if (precio>0) {
                        $.ajax({
                            type:'POST',
                            url: base_url+'Generales/deletepro',
                            data: {
                                id: id,
                                tipo: tipo,
                                },
                                async: false,
                                statusCode:{
                                    404: function(data){
                                        swal("Error", "404", "error");
                                    },
                                    500: function(){
                                        swal("Error", "500", "error"); 
                                    }
                                },
                                success:function(data){
                                    swal("Hecho!", "Realizado", "success");
                                    setTimeout(function(){ window.location.href=''; }, 3000);
                                  

                                }
                            });
                    //}else{
                    //    swal("Error", "El precio no debe de ser menor o igual a cero", "error"); 
                    //}
                 
            },
            cancelar: function (){
            }
        }
    });
}
function ref_usado(idrow,tipo){
    var usa =$('#ref_usado_'+idrow).is(':checked')==true?1:0;
    console.log('usado '.usa);
    $.ajax({
        type:'POST',
        url: base_url+'Generales/refusado',
        data: {
            id: idrow,
            tipo: tipo,
            usa:usa
            },
            async: false,
            statusCode:{
                404: function(data){
                    swal("Error", "404", "error");
                },
                500: function(){
                    swal("Error", "500", "error"); 
                }
            },
            success:function(data){
            }
    });
}
function notserentrega(){
    var entrega = $('#fechaentrega').data('entrega');
    var iser = $('#fechaentrega').data('iser');
    var html='Esta prefactura ya fue';
    if(entrega==1){
        html+=' entregada';
    }
    if(entrega==1 && iser==1){
        html+=' y';
    }
    if(iser==1){
        html+=' Agendada';
    }
    html+=', ya no puedes mover la fecha.';

    $.confirm({
        boxWidth: '41%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Bloqueado',
        content: html,
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
            }
        }
    });
}