<style type="text/css">
  .chosen-container{
    margin: 0px;
  }
  .datostableinput2{
    border-bottom:0px !important;
  }
</style>
<!-- START CONTENT -->
<section id="content">
  <!--start container-->
  <div class="container">
    <div class="section">   
      <div>
          <h4 class="header header_compra">Agregar Compra</h4>
          <div class="row div_folio">
            <div class="input-field col s6">
              <input type="hidden" id="idcompra" value="<?php echo $idcompra;?>">
              <input placeholder="folio" id="folio" type="text" class="validate">
              <label for="folio">Folio</label>
            </div>
          </div>
          <div class="row">
            <div class="col s12">
              <ul class="collapsible collapsible-accordion" data-collapsible="accordion">
                <li class="">
                  <div class="collapsible-header">
                    <i class="material-icons">print</i> Equipos</div>
                  <div class="collapsible-body">
                    <div class="row">
        
                      <div class="input-field col s3">
                        <select id="equipos" name="equipos" class="browser-default chosen-select">
                          <?php foreach ($equipos->result() as $item) { ?>
                              <option value="<?php echo $item->id; ?>"><?php echo $item->modelo; ?></option>
                          <?php } ?> 
                        </select>
                        <label for="equipos" class="active">Equipo</label>
                      </div>
                      <div class="input-field col s3">
                        <input placeholder="Cantidad" id="serie1" type="number" class="validate">
                        <label for="serie1">Cantidad</label>
                      </div>
                      <div class="input-field col s3">
                        <input placeholder="Precio" id="Precio1" type="number" class="validate">
                        <label for="serie1">Precio Unitario</label>
                      </div>
                      <div class="col s3">
                        <button class="btn waves-effect waves-light  addbtnequipo" name="action">Agregar
                          <i class="material-icons right">system_update_alt</i>
                        </button>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col s12">
                        <table id="tabla_sequipos" class="striped responsive-table display" cellspacing="0">
                        <thead>
                          <tr>
                            <th>Equipo</th>
                            <th>Cantidad</th>
                            <th>Precio</th>
                            <th></th>
                          </tr>
                        </thead>
                        <tbody class="tabla_addsequipos">
                        </tbody>
                      </table>
                      </div>
                    </div> 
                    

                  </div>
                </li>
                <li>
                  <div class="collapsible-header">
                    <i class="material-icons">developer_board</i> Accesorios</div>
                  <div class="collapsible-body">
                    <div class="row">
                      
                      <div class="input-field col s3">
                        <select id="accesorio" name="accesorio" class="browser-default chosen-select">
                          <?php foreach ($accesorios->result() as $item) { ?>
                              <option value="<?php echo $item->id; ?>"><?php echo $item->nombre; ?></option>
                          <?php } ?> 
                        </select>
                        <label for="accesorio" class="active">Accesorio</label>
                      </div>
                      <div class="input-field col s3">
                        <input placeholder="cantidad" id="serie2" type="number" class="validate">
                        <label for="serie2">Cantidad</label>
                      </div>
                      <div class="input-field col s3">
                        <input placeholder="Precio" id="Precio2" type="number" class="validate">
                        <label for="serie1">Precio Unitario</label>
                      </div>
                      <div class="col s3">
                        <button class="btn waves-effect waves-light addbtnaccesorio" name="action">Agregar
                          <i class="material-icons right">system_update_alt</i>
                        </button>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col s12">
                        <table id="tabla_saccesorios" class="striped responsive-table display" cellspacing="0">
                        <thead>
                          <tr>
                            <th>Accesorio</th>
                            <th>Cantidad</th>
                            <th>Precio</th>
                            <th></th>
                          </tr>
                        </thead>
                        <tbody class="tabla_addsaccesorios">
                        </tbody>
                      </table>
                      </div>
                    </div> 
                  </div>
                </li>
                <li>
                  <div class="collapsible-header">
                    <i class="material-icons">build</i> Refacciones</div>
                  <div class="collapsible-body">
                    <div class="row">
                      
                      <div class="input-field col s3">
                        <select id="refaccion" name="refaccion" class="browser-default chosen-select">
                          <?php foreach ($refacciones->result() as $item) { ?>
                              <option value="<?php echo $item->id; ?>"><?php echo $item->nombre.' ('.$item->codigo.')'; ?></option>
                          <?php } ?> 
                        </select>
                        <label for="refaccion" class="active">Refaccion</label>
                      </div>
                      <div class="input-field col s3">
                        <input placeholder="Cantidad" id="serie3" type="number" class="validate">
                        <label for="serie3">Cantidad</label>
                      </div>
                      <div class="input-field col s3">
                        <input placeholder="Precio" id="Precio3" type="number" class="validate">
                        <label for="serie1">Precio Unitario</label>
                      </div>
                      <div class="col s3">
                        <button class="btn waves-effect waves-light addbtnrefaccion" name="action">Agregar
                          <i class="material-icons right">system_update_alt</i>
                        </button>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col s12">
                        <table id="tabla_srefacciones" class="striped responsive-table display" cellspacing="0">
                          <thead>
                            <tr>
                              <th>Refaccion</th>
                              <th>Cantidad</th>
                              <th></th>
                              <th>Precio</th>
                              <th></th>
                            </tr>
                          </thead>
                          <tbody class="tabla_addsrefacciones">
                          </tbody>
                        </table>
                        </div>
                    </div> 
                  </div>
                </li>
                <li>
                  <div class="collapsible-header">
                    <i class="material-icons">color_lens</i> Consumibles</div>
                  <div class="collapsible-body">
                    <div class="row">
                      
                      <div class="input-field col s3">
                        <select id="consumible" name="consumible" class="browser-default chosen-select">
                          <?php foreach ($consumibles->result() as $item) { ?>
                              <option value="<?php echo $item->id; ?>"><?php echo $item->modelo.' ('.$item->parte.')'; ?></option>
                          <?php } ?> 
                        </select>
                        <label for="consumible" class="active">Consumible</label>
                      </div>
                      <div class="input-field col s3">
                        <input placeholder="Cantidad" id="cantidad4" type="number" class="validate">
                        <label for="cantidad4">Cantidad</label>
                      </div>
                      <div class="input-field col s3">
                        <input placeholder="Precio" id="Precio4" type="number" class="validate">
                        <label for="serie1">Precio Unitario</label>
                      </div>
                      <div class="col s3">
                        <button class="btn waves-effect waves-light addbtnconsumible" name="action">Agregar
                          <i class="material-icons right">system_update_alt</i>
                        </button>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col s12">
                        <table id="tabla_sconsumible" class="striped responsive-table display" cellspacing="0">
                          <thead>
                            <tr>
                              <th>Consumible</th>
                              <th>Cantidad</th>
                              <th>Precio</th>
                              <th></th>
                            </tr>
                          </thead>
                          <tbody class="tabla_addsconsumible">
                          </tbody>
                        </table>
                        </div>
                    </div> 
                  </div>
                </li>
              </ul>
            </div>
          </div>
          <div class="row">
            <a class="btn cyan waves-effect waves-light btn right saveallseries">
                    <i class="material-icons right">save</i><span>Guardar</span></a>
          </div>
      </div>
    </div>
  </div> 
</section>

              