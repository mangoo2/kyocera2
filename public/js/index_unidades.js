
var base_url = $('#base_url').val();
var tables;
var idunidad;
$(document).ready(function() {  
    $('.modal').modal();
    tables = $('#tabla_registros').DataTable();
    loadtables();
    $("#files").fileinput({
        showCaption: true,
        showUpload: false,// quita el boton de upload
        //rtl: true,
        allowedFileExtensions: ["jpg","png","webp","jpeg","pdf"],
        browseLabel: 'Seleccionar documentos',
        uploadUrl: base_url+'Listado_unidades/reg_incidencias',
        maxFilePreviewSize: 5000,
        previewFileIconSettings: {
            'docx': '<i class="fa fa-file-word-o text-primary"></i>',
            'xlsx': '<i class="fa fa-file-excel-o text-success"></i>',
            'pptx': '<i class="fa fa-file-powerpoint-o text-danger"></i>',
            'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
            'pdf': '<i class="fa fa-file-pdf-o text-danger"></i>',
            'zip': '<i class="fa fa-file-archive-o text-muted"></i>',
            'cer': '<i class="fas fa-file-invoice"></i>',
        },
        uploadExtraData: function (previewId, index) {
        var info = {
                    id:idunidad,
                    detalle:$('#detalle').val()
                };
        return info;
      }
    }).on('filebatchuploadcomplete', function(event, files, extra) {
      //location.reload();
        upload_data_view(idunidad);
    }).on('filebatchuploadsuccess', function(event, files, extra) {
      //location.reload();
        //toastr.success('Se cargo el Certificado Correctamente','Hecho!');
        upload_data_view(idunidad);
    });
    $('#registrarincidente').click(function(event) {
        $('#files').fileinput('upload');
    });
});
function loadtables(){
    var perfilid = $('#perfilid').val();
    tables.destroy();
    tables = $('#tabla_registros').DataTable({
        stateSave: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url+"index.php/Listado_unidades/getlistasunidades",
            type: "post",
            "data": {
                'activo':0
            },
        },
        "columns": [
            {"data": "id"},
            {"data": "file",
                render:function(data,type,row){ 
                    if(data!=""){
                        return '<a class="btn_img" target="_blank" href="'+base_url+'uploads/unidades/'+data+'"><img src="'+base_url+'uploads/unidades/'+data+'" style="width:50px;" class="imgpro"></a>';
                    }else{
                        return '<img src="'+base_url+'app-assets/images/kyocera_mini.png" style="width:50px;" class="imgpro">';
                    }
                }
            },
            {"data": "modelo"},
            {"data": "marca"},
            {"data": "placas"}, 
            {"data": "kilometraje",
                render:function(data,type,row){ 
                    return row.kilometraje;
                }
            },
            {"data": "tipo_unidad",
                render:function(data,type,row){ 
                    if(row.tipo_unidad==1){
                        return 'Unidad propia';
                    }else{
                        return 'Leasin';
                    }
                }
            },
            {"data": "vigencia_poliza_seguro"},
            {"data": "proximo_servicio"},
            {"data": "nombre",
                render:function(data,type,row){ 
                    var html=row.nombre+' '+row.apellido_paterno+' '+row.apellido_materno;
                    return html;
                }
            },
            {"data": null,
                render:function(data,type,row){
                    var html='';
                   //var html+='<button class="btn-bmz blue gradient-shadow" onclick="modal_series_file('+row.compraId+')">File</button>';
                       html+='<a href="'+base_url+'Listado_unidades/registro/'+row.id+'" class="btn-bmz blue gradient-shadow" ><i class="fas fa-edit"></i></a> ';
                       html+='<a onclick="incidencias('+row.id+')" class="btn-bmz blue gradient-shadow" ><i class="fas fa-clipboard-list fa-fw"></i></a> ';
                       html+=' <a class="btn-bmz blue gradient-shadow btn-accion" onclick="itinerario('+row.id+')" title="Itinerario"><i class="fas fa-list-ul"></i></a> ';
                       if(perfilid==1){
                            html+='<a onclick="deletereg('+row.id+')" class="btn-bmz red gradient-shadow deletereg">\
                                    <i class="fas fa-trash-alt"></i></a> ';
                        }
                    return html;
                }
            }
        ],
        "order": [[ 2, "desc" ]],
        "lengthMenu": [[25, 50, 100], [25, 50,100]],
        
    });
}



function deletereg(id){
  $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: 'Confirma la eliminacion?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
              //========================================================
              $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Listado_unidades/eliminarregistro",
                    data: {
                      id:id
                    },
                    success: function (response){
                       $.alert({
                            boxWidth: '30%',
                            useBootstrap: false,
                            title: 'Éxito!',
                            content: 'Registro eliminado'});
                        loadtables();
                          
                    },
                    error: function(response){
                        alertfunction('Atención!','Algo salió mal, intente de nuevo o contacte al administrador del sistema'); 
                    }
                });
              //===================================================
              },
            cancelar: function ()             {
                
            }
        }
    });
}
function incidencias(id){
    idunidad=id;
    $('#modalinfoincidencias').modal('open');
    upload_data_view(id);
}
function upload_data_view(id){
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Listado_unidades/lis_incidencias",
        data: {
          id:id
        },
        success: function (response){
           $('.addtablelistincidencias').html(response);
           $('#table_inci').DataTable();
              
        },
        error: function(response){
            alertfunction('Atención!','Algo salió mal, intente de nuevo o contacte al administrador del sistema'); 
        }
    });
}
function itinerario(id){
  $('#modalitinerario').modal('open');
  itinerario_data_view(id);
}
function itinerario_data_view(id){
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Listado_unidades/lis_itinerario_gen",
        data: {
          id:id
        },
        success: function (response){
           $('.tableitinerario').html(response);
           $('#table_itinerario').DataTable({
            responsive: !0,
            "order": [[ 0, "desc" ]]
           });
              
        },
        error: function(response){
            alertfunction('Atención!','Algo salió mal, intente de nuevo o contacte al administrador del sistema'); 
        }
    });
}
function reporte(){
    var primerdia=$('.infogen').data('primerdia');
    var ultimodia=$('.infogen').data('ultimodia');
    var se_eje = $('#se_eje').html();
    var se_uni = $('#se_uni').html();
    var html='';
        html+='<div class="row">';
            html+='<div class="col s6">';
                html+='<label>Tecnico</label>';
                html+='<select class="form-control-bmz browser-default" id="se_eje_selected">'+se_eje+'</select>';
            html+='</div>';
            html+='<div class="col s6">';
                html+='<label>Unidad</label>';
                html+='<select class="form-control-bmz browser-default" id="se_uni_selected">'+se_uni+'</select>';
            html+='</div>';
            html+='<div class="col s6">';
                html+='<label>Fecha Inicial</label>';
                html+='<input type="date" class="form-control-bmz" id="r_fecha_ini" value="'+primerdia+'">';
            html+='</div>';
            html+='<div class="col s6">';
                html+='<label>Fecha Fin</label>';
                html+='<input type="date" class="form-control-bmz" id="r_fecha_fin" value="'+ultimodia+'">';
            html+='</div>';
        html+='</div>';
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Generar Reporte',
        content: html,
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var eje = $('#se_eje_selected option:selected').val();
                var uni = $('#se_uni_selected option:selected').val();
                var fini = $('#r_fecha_ini').val();
                var ffin = $('#r_fecha_fin').val();
              //========================================================
                window.open(base_url+'Listado_unidades/reporte/'+eje+'/'+uni+'/'+fini+'/'+ffin, '_blank');
              //===================================================
              },
            cancelar: function ()             {
                
            }
        }
    });
}