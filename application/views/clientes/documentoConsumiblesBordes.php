<?php
if(isset($cliente))
{
$idCliente = $cliente->id;
$empresa = $cliente->empresa;
}
?>
<style type="text/css" class="print_css">
  
</style>
<!-- START CONTENT -->
<section id="content" width="100%">
  <!--start container-->
  <div class="container" width="100%">
    <div class="section" style="margin-top: -53px;">
     <br>
      <div class="row">
        <input id="base_url" type="hidden" name="base_url" value="<?php echo base_url(); ?>">
        <!-- FORMULARIO  card-panel-->
        <div class="col s12">
          <div class="">

            <!-- ENCABEZADO PRINCIPAL PDF -->
            <div class="row"> 
              <div class="col s3">
                <img src="<?php echo base_url(); ?>app-assets/images/alta.png">
              </div>
              <div class="col s6">
                <p class="headerPDF"><b>Impresoras y Multifuncionales, Venta, Renta y Mantenimiento.<br>Blvd. Norte 1831 V. San Alejandro. Puebla, Pue. Call Center 273 3400, 249 5393<br>www.kyoceraapp.com</b></p>
              </div> 
              <div class="col s3">
                <p align="center"><img class="" src="<?php echo base_url(); ?>app-assets/images/1024_kyocera_logo_mds.png" alt="materialize logo" width="200px;" height="50px;"></p>
              </div>
            </div>

            <!-- APARTADO DE INFORMACIÓN DE RECEPTOR DE COTIZACIÓN-->
            
            
            <div class="row">
              <table class="centered datosClienteCotizacion tablaBordesHeader">
                  <thead>
                    
                  </thead>
                  <tbody>
                    <tr >
                      <td class="tablaBordesHeader" >
                        Cotización Especial Para: <br>
                        Mangoo Software
                      </td>
                      <td class="tablaBordesHeader">
                        Atención para: <br>
                        Raúl Trujeque Martínez
                      </td>
                      <td class="tablaBordesHeader">
                        Correo raul@mangoo.com.mx <br>
                        Teléfono: 2224578961
                      </td>
                      <td class="tablaBordesHeader">
                        Cotización No. <?php echo rand ( 1000, 5000 );?> 
                      </td>
                    </tr>
                    <tr>
                      <td colspan="4" style="font-size: 12px;">
                        <b>Suministros / Refacciones para su equipo de la marca "KYOCERA"</b>
                      </td>
                    </tr>
                  </tbody>
                </table>
            </div>
            <br><br>
            <!------------ TABLA DE INFORMACIÓN DE EQUIPOS ------------>
            <div class="row">
                <table class="centered tablaBordesPrincipal letraTablaImpresionConsumibles">
                  <thead>
                    <tr>
                        <th class="tablaBordesPrincipal" width="5%">Pieza</th>
                        <th class="tablaBordesPrincipal" width="%%">Producto</th>
                        <th class="tablaBordesPrincipal" width="5%">Equipo</th>
                        <th class="tablaBordesPrincipal" width="5%">Modelo</th>
                        <th class="tablaBordesPrincipal" width="5%">Rendimiento</th>
                        <th class="tablaBordesPrincipal" width="30%">Descripción</th>
                        <th class="tablaBordesPrincipal" width="5%">Precio General</th>
                        <th class="tablaBordesPrincipal" width="5%">I.V.A. General</th>
                        <th class="tablaBordesPrincipal" width="5%">Total General</th>
                        <th class="tablaBordesPrincipal" width="5%">Precio Frecuente</th>
                        <th class="tablaBordesPrincipal" width="5%">I.V.A. Frecuente</th>
                        <th class="tablaBordesPrincipal" width="5%">Total Frecuente</th>
                        <th class="tablaBordesPrincipal" width="5%">Precio Especial</th>
                        <th class="tablaBordesPrincipal" width="5%">I.V.A. Especial</th>
                        <th class="tablaBordesPrincipal" width="5%">Total Especial</th>
                    </tr>
                  </thead>
                  <tbody >
                    <tr>
                      <td class="tablaBordesPrincipal edit">1</td>
                      <td class="tablaBordesPrincipal">Toner</td>
                      <td class="tablaBordesPrincipal">M-3540</td>
                      <td class="tablaBordesPrincipal">TK-3102</td>
                      <td class="tablaBordesPrincipal">12,500 Páginas</td>
                      <td class="tablaBordesPrincipal">Toner negro para impresoras</td>
                      <td class="tablaBordesPrincipal edit">$1,745.00</td>
                      <td class="tablaBordesPrincipal edit">$279.20</td>
                      <td class="tablaBordesPrincipal edit">$2024.20</td>
                      <td class="tablaBordesPrincipal edit">$1,274.00</td>
                      <td class="tablaBordesPrincipal edit">$203.91</td>
                      <td class="tablaBordesPrincipal edit">$1,478.35</td>
                      <td class="tablaBordesPrincipal edit">$1,174.00</td>
                      <td class="tablaBordesPrincipal edit">$187.84</td>
                      <td class="tablaBordesPrincipal edit">$1,361.84</td>
                    </tr>
                  </tbody>
                </table>
            </div>

            <br><br><br><br><br><br><br><br><br><br><br><br><br><br>
            <!-- Condiciones de Equipos -->
            <div class="row" style="margin-bottom: -10px">
              <table class="centered letraTablaFooterConsumibles">
                  <thead>
                      <tr>
                        <th>Condiciones</th>
                        <th>Observaciones</th>
                        <th>Responsable</th>
                      </tr>
                  </thead>
                  <tbody >
                    <tr>
                        <td width="33%"><b>Precios: </b>Sujetos a cambios sin previo aviso.  <b>Pago: </b>Con transferencia bancaria. <b>Consumibles: </b>Originales de "KYOCERA.  <b>Rendimiento: </b>Estimado en A4 al 5% ISO 19752. </td>
                        <td width="34%">Cotización Solicitada via correo. No se hacen cambios ni devoluciones y no aplica garantía si la srefacciones no son instaladas por nuestros asesores técnicos.</td>
                        <td width="33%"><b>Enero de 2018</b><br><b><b  class="edit">Karla Castañeda. karlac.a@kyoceraap.com </b></td>
                    </tr>
                    <tr>
                        <td colspan="3">Depositar a nombre de <b>"Alta Productividad, S.A. de C.V."</b> Banco Banamex, Suc 0826, Cuenta 7347132, CLABE 002668082673471327 (para transferencia).<br><b>Mandar comprobante a este correo por favor: kyorent@kyoceraapp.com</b></td>
                    </tr>
                  </tbody>
                </table>
            </div>

           

<!--columna 1---------------------------------------------------------------->

              <form class="form" id="" method="post">
                <?php
                if(isset($cliente))
                {
                ?>
                <input id="idCliente" name="idCliente" type="hidden" value="<?php echo $idCliente; ?>">
                <?php
                }
                ?>
                <div class="row">
                  <div class="input-field col s12">
                    <button class="btn cyan waves-effect waves-light right btn_imp" type="button" onclick="btn_guardar()">Imprimir
                    <i class="material-icons right">send</i>
                    </button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>