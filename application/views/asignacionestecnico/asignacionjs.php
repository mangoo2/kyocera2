<script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/toastr/toastr.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/plugins/toastr/toastr.css">

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/radioservicios.css?v=<?php echo date('YmdGis')?>">

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/responsive.dataTables/responsive.dataTables.min.css">
<script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/responsive.dataTables/dataTables.responsive.min.js"></script>

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/plugins/loader2/jquery.loader.css">
<script src="<?php echo base_url(); ?>public/plugins/loader2/jquery.loader.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>public/js/asignacionestecnico.js?v=<?php echo date('YmdGis'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>public/js/canvas.js?v=<?php echo date('Ym'); ?>"></script>
<script type="text/javascript">
	$(document).ready(function($) {
    	<?php if($idpersonal ==1 || $perfilid==5 || $perfilid==10 || $perfilid==12){ ?>
    		setInterval(saveubi, 600000);//10 min (600000)
    	<?php } ?>
	});
</script>