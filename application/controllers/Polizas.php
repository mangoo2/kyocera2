<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/****************************************************************/
class Polizas extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->model('Polizas_model', 'model');
        $this->load->model('ModeloCatalogos');
        $this->load->model('Equipos_model');
        $this->load->helper('url');
        $this->idpersonal = $this->session->userdata('idpersonal');
        $this->perfilid =$this->session->userdata('perfilid');
        $this->submenu=24;
    }

    // Listado de Consumibles
    function index() {
        $data['perfilid']=$this->perfilid;
        $data['MenusubId']=$this->submenu;
        $this->load->view('header');
        $this->load->view('main');
        $this->load->view('polizas/listado',$data);
        $this->load->view('polizas/listado_js');
        $this->load->view('footer');        
    }

    // Listado de Consumibles
    function alta() {
        $data['perfilid']=$this->perfilid;
        $data['equipos'] = $this->Equipos_model->getListadoEquipos();
        // Obtenemos los menus por medio de la libreria
        $data['tipoVista'] = 1;
        // Cargamos las vistas correspondientes
        $data['MenusubId']=$this->submenu;
        $this->load->view('header');
        $this->load->view('main',$data);
        $this->load->view('polizas/catalogo_polizas');
        $this->load->view('polizas/catalogo_polizas_js');
        $this->load->view('footer');        
    }

    // Vista de formulario para Edición de Empleado
    public function edicion($idPoliza) {
        $data['perfilid']=$this->perfilid;
        $data['equipos'] = $this->Equipos_model->getListadoEquipos();
        // Obtenemos los menus por medio de la libreria
        $data['tipoVista'] = 2;
        // Consultamos los datos de la Póliza para poder mandarlos a la vista de Edición
        $data['poliza'] = $this->model->getPoliza($idPoliza);
        //$data['detallesPoliza'] = $this->model->getDetallesPoliza($idPoliza);
        $data['MenusubId']=$this->submenu;
        // Cargamos las vistas correspondientes
        $this->load->view('header');
        $this->load->view('main',$data);
        $this->load->view('polizas/catalogo_polizas');
        $this->load->view('polizas/catalogo_polizas_js');
        $this->load->view('footer');
    }
        // Visualización
    public function visualiza($idPoliza) {
        $data['perfilid']=$this->perfilid;
        // Obtenemos los menus por medio de la libreria
        $data['tipoVista'] = 3;
        // Consultamos los datos de la Póliza para poder mandarlos a la vista de Edición
        $data['poliza'] = $this->model->getPoliza($idPoliza);
        $data['detallesPoliza'] = $this->model->getDetallesPoliza($idPoliza);
        $data['equipos'] = $this->Equipos_model->getListadoEquipos();
        // Cargamos las vistas correspondientes
        $data['MenusubId']=$this->submenu;
        $this->load->view('header');
        $this->load->view('main',$data);
        $this->load->view('polizas/catalogo_polizas');
        $this->load->view('polizas/catalogo_polizas_js');
        $this->load->view('footer');
    }

    public function getListadoPolizas(){
        $polizas = $this->model->getListadoPolizas();
        $json_data = array("data" => $polizas);
        echo json_encode($json_data);
    }

    public function getListadocontratosPolizasIncompletas()    {
        $data = $this->input->post();
        $polizas = $this->model->getListadocontratosPolizasIncompletas($data);
        $json_data = array("data" => $polizas);
        echo json_encode($json_data);
    }
    
    public function eliminarpoliza($id){

        $datos = array('estatus' => 0);
        $eliminado = $this->model->actualizaPoliza($datos, $id);
        echo $eliminado;
        //$this->Modelobitacoras->Insert(array('contenido'=>'Se Elimino Poliza','nombretabla'=>'polizas','idtable'=>$id,'tipo'=>'Delete','personalId'=>$this->idpersonal));
    }

    function insertaActualizaPoliza(){
        // Recupera los datos que vienen del formukario
        $data = $this->input->post();

        $idPoliza = $data['idPoliza'];
        unset($data['idPoliza']);

        //$arraypolizasdetalles = $data['arraypolizasdetalles'];
        $DATApd = json_decode($data['arraypolizasdetalles']);
        unset($data['arraypolizasdetalles']);
        
        if ($idPoliza==0) {
            $idPoliza = $this->model->insertaPoliza($data);
            //$this->Modelobitacoras->Insert(array('contenido'=>'Se Inserto Poliza','nombretabla'=>'polizas','idtable'=>$idPoliza,'tipo'=>'Insert','personalId'=>$this->idpersonal));
        }else{
            $this->model->actualizaPoliza($data, $idPoliza);
            //$this->Modelobitacoras->Insert(array('contenido'=>'Se Actualizo Poliza','nombretabla'=>'polizas','idtable'=>$idPoliza,'tipo'=>'Update','personalId'=>$this->idpersonal));
        }
        //=============================================================
        
        for ($i=0;$i<count($DATApd);$i++) {
            if ($DATApd[$i]->id>0) {
                $datarpd=array(
                            'modelo'=>$DATApd[$i]->modelo,
                            'sinmodelo'=>$DATApd[$i]->sinmodelo,
                            'newmodelo'=>$DATApd[$i]->newmodelo,
                            'precio_local'=>$DATApd[$i]->precio_local,
                            'precio_semi'=>$DATApd[$i]->precio_semi,
                            'precio_foraneo'=>$DATApd[$i]->precio_foraneo,
                            'precio_especial'=>$DATApd[$i]->precio_especial,
                        );
                $this->ModeloCatalogos->db4_updateCatalogo('polizas_detalles',$datarpd,array('id'=>$DATApd[$i]->id));
            }else{
                $datarpd=array(
                            'modelo'=>$DATApd[$i]->modelo,
                            'sinmodelo'=>$DATApd[$i]->sinmodelo,
                            'newmodelo'=>$DATApd[$i]->newmodelo,
                            'precio_local'=>$DATApd[$i]->precio_local,
                            'precio_semi'=>$DATApd[$i]->precio_semi,
                            'precio_foraneo'=>$DATApd[$i]->precio_foraneo,
                            'precio_especial'=>$DATApd[$i]->precio_especial,
                            'idPoliza'=>$idPoliza
                        );
                $this->ModeloCatalogos->Insert('polizas_detalles',$datarpd);
            }

        }



        echo $idPoliza;
    }
    function detallespolizas(){
        $idPoliza = $this->input->post('idPoliza');
        $detallesPoliza = $this->model->getDetallesPoliza2($idPoliza);
        echo json_encode($detallesPoliza);
    }
    function eliminarpolizad(){
        $idPolizad = $this->input->post('idPolizad');
        $this->ModeloCatalogos->updateCatalogo('polizas_detalles',array('activo'=>0),array('id'=>$idPolizad));
    }
}

?>