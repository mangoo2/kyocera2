$(document).ready(function(){
    
    var base_url = $('#base_url').val();

    var formulario_porcentajes = $('#porcentajes-form');

    formulario_porcentajes.validate({
            rules: 
            {
                porcentaje_equipos_venta: {
                    required: true
                },
                porcentaje_refacciones_venta: {
                    required: true
                },
                porcentaje_consumibles_venta: {
                    required: true
                },
                porcentaje_accesorios_venta: {
                    required: true
                },
                porcentaje_equipos_renta: {
                    required: true
                },
                porcentaje_refacciones_renta: {
                    required: true
                },
                porcentaje_consumibles_renta: {
                    required: true
                },
                porcentaje_accesorios_renta: {
                    required: true
                },
                porcentaje_equipos_poliza: {
                    required: true
                },
                porcentaje_refacciones_poliza: {
                    required: true
                },
                porcentaje_consumibles_poliza: {
                    required: true
                },
                porcentaje_accesorios_poliza: {
                    required: true
                },
                porcentaje_equipos_revendedor: {
                    required: true
                },
                porcentaje_refacciones_revendedor: {
                    required: true
                },
                porcentaje_consumibles_revendedor: {
                    required: true
                },
                porcentaje_accesorios_revendedor: {
                    required: true
                }
            },
            // Para mensajes personalizados
            messages: 
            {
                porcentaje_equipos_venta:{
                    required: "Ingrese un porcentaje de ganancia para Equipos, en venta"
                },
                porcentaje_refacciones_venta: {
                    required: "Ingrese un porcentaje de ganancia para Refacciones, en venta"
                },
                porcentaje_consumibles_venta: {
                    required: "Ingrese un porcentaje de ganancia para Consumibles, en venta"
                },
                porcentaje_accesorios_venta: {
                    required: "Ingrese un porcentaje de ganancia para Accesorios, en venta"
                },
                porcentaje_equipos_renta:{
                    required: "Ingrese un porcentaje de ganancia para Equipos, en renta"
                },
                porcentaje_refacciones_renta: {
                    required: "Ingrese un porcentaje de ganancia para Refacciones, en renta"
                },
                porcentaje_consumibles_renta: {
                    required: "Ingrese un porcentaje de ganancia para Consumibles, en renta"
                },
                porcentaje_accesorios_renta: {
                    required: "Ingrese un porcentaje de ganancia para Accesorios, en renta"
                },
                porcentaje_equipos_poliza: {
                    required: "Ingrese un porcentaje de ganancia para Equipos, con Póliza"
                },
                porcentaje_refacciones_poliza: {
                    required: "Ingrese un porcentaje de ganancia para Refacciones, con Póliza"
                },
                porcentaje_consumibles_poliza: {
                    required: "Ingrese un porcentaje de ganancia para Consumibles, con Póliza"
                },
                porcentaje_accesorios_poliza: {
                    required: "Ingrese un porcentaje de ganancia para Accesorios, con Póliza"
                },
                porcentaje_equipos_revendedor: {
                    required: "Ingrese un porcentaje de ganancia para Equipos, para Revendedor"
                },
                porcentaje_refacciones_revendedor: {
                    required: "Ingrese un porcentaje de ganancia para Refacciones, para Revendedor"
                },
                porcentaje_consumibles_revendedor: {
                    required: "Ingrese un porcentaje de ganancia para Consumibles, para Revendedor"
                },
                porcentaje_accesorios_revendedor: {
                    required: "Ingrese un porcentaje de ganancia para Accesorios, para Revendedor"
                }
            },
        errorElement : 'div',
            // Si están correctos los campos, procesamos el formulario mediante Ajax
        submitHandler: function (form){
            var datos = formulario_porcentajes.serialize();
            
            $.ajax({
                type:'POST',
                url: base_url+'index.php/Porcentajesganancia/actualizarPorcentajesGanancia',
                data: datos,
                success:function(data){
                    swal({ title: "Éxito",
                            text: "Se guardaron los datos de manera correcta",
                            type: 'success',
                            showCancelButton: false,
                            allowOutsideClick: false,
                        });
                        actualizacionprecios();
                        setTimeout(function(){
                            location.reload();
                        },1000);
                }
            }); 
        }
    
    });
    

});
function actualizacionprecios(){
    $.ajax({
        type:'POST',
        url: base_url+'index.php/Listapreciosupdate',
        success:function(data){
        }
    }); 
}