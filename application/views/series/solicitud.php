<style type="text/css">
  #tabla_compras td{
    font-size: 12px;
  }
  .botes_group{
    width: 215px;
  }
  .botes_group a{
    font-size: 12px;
    text-align: center;
  }
</style>
<!-- START CONTENT -->
    <section id="content">
      <!--start container-->
        <div class="container">
          <div class="section">
          <div id="table-datatables">
            <br>
                <h4 class="header">Listado de Solicitudes</h4>
                <div class="row">
                  <div class="col s2">
                    <label>Tipo</label>
                    <select class="browser-default form-control-bmz" id="tipo" onchange="datos_compras();">
                      <option value="0">Todos</option>
                      <option value="1">Consumibles</option>
                      <option value="2">Accesorio</option>
                      <option value="3">Equipos</option>
                      <option value="4">Refacciones</option>
                    </select>
                  </div>
                  <div class="col s2">
                    <label>Prioridad</label>
                    <select class="browser-default form-control-bmz" id="prioridad" onchange="datos_compras();">
                      <option value="0">Todos</option>
                      <option value="1">Prioridad 1</option>
                      <option value="2">Prioridad 2</option>
                      <option value="3">Prioridad 3</option>
                    </select>
                  </div>                  
                </div>
                <div class="row">

                  <div class="col s12">
                    <table id="tabla_compras" class="table tabla_compras" cellspacing="0">
                      <thead>
                        <tr>
                          <th >#</th>
                          <th  >Cantidad</th>
                          <th >Modelo</th>
                          <th>Prioridad</th>
                          <th>Cotizaciones</th>
                          <th>Stock</th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
          </div>  
        </div>
     </div>
    </section>       