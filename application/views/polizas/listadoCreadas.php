<style type="text/css">
  #tabla_polizasCreadas td,#tabla_polizasCreadas th,#tabla_facturacion td{
    font-size:12px; 
    padding: 6px 7px;
  }
  .chosen-container{
    margin-left: 0px;
  }
   table.dataTable{
    margin: 0px !important;
  }
  #tabla_facturacion{
    width: 100% !important;
  }
  .yellowc {
    background-color: #ffeb3b !important;
  }
  .fa_plus_div{

    font-size: 12px;
    right: 10px;
    position: relative;
    z-index: 1;
    color: white;
  }
  .div_btn_pol_eq p{
    font-size: 12px;
  }
  .div_btn_pol_eq{
    width: 100px;
    text-align: center;
    float: left;
  }
</style>
<input  type="password" name="password" style="display: none;">
<input  type="text" name="usuario" style="display: none;">
<input  type="date" id="fechahoy" value="<?php echo date('Y-m-d')?>" style="display: none;">
<!-- START CONTENT -->
    <section id="content">
    	<!--start container-->
        <div class="container">
          <div class="row">
            <div class="col s10 m6 l6">
              <h5 class="breadcrumbs-title">Listado de Pólizas</h5>
            </div>
          </div>
        </div>
      	<div class="container">
      		<div class="section">
<div class="card-panel">
      		<input id="base_url" type="hidden" name="base_url" value="<?php echo base_url(); ?>">

      		<div id="table-datatables">
                <div class="row">
                    <div class="col s6 m2 l2">
                      <label>Fecha inicio</label>
                      <input type="date" id="fechainicial_reg" class="form-control-bmz" value="<?php echo $fechainicial_reg;?>" >
                    </div>
                    <div class="col s6 m2 l2">
                      <label>Fecha final</label>
                      <input type="date" id="fechafinal_reg" class="form-control-bmz" value="" >
                    </div>
                  </div>
                <div class="row">
                      <div class="col s6 m2 l2">
                        <label>Estatus</label>
                        <select id="tipoventadelete" class="browser-default form-control-bmz" >
                          <option value="1">Activas</option>
                          <option value="0">Eliminadas</option>
                        </select>
                      </div>
                      <div class="col s6 m2  l2" style="display:none;">
                        <label>Tipo de Venta</label>
                        <select id="tipoventa" class="browser-default form-control-bmz" onchange="loadtable();" wtx-context="5B99A416-635C-43F8-812F-177A04D64D35">
                          <!--<option value="0">Todo</option>-->
                          <!--<option value="1" <?php if(isset($_GET['tv'])){ if($_GET['tv']==1){echo 'selected';} }?> >Ventas</option>
                          <option value="2" <?php if(isset($_GET['tv'])){ if($_GET['tv']==2){echo 'selected';} }?> >Ventas Combinadas</option>
                          <option value="3" <?php if(isset($_GET['tv'])){ if($_GET['tv']==3){echo 'selected';} }?> >Servicios </option>-->
                          <option value="4" selected>Polizas</option>
                        </select>
                      </div>
                      <div class="col s6 m2 l2" style="min-height:54.5px">
                        <label>Clientes</label>
                        <select id="cliente" class="browser-default" >
                          <option value="0">Selecione</option>
                        </select>
                      </div>
                      <div class="col s6 m2 l2">
                        <label>Tipo de estatus</label>
                        <select id="tipoestatus" class="browser-default form-control-bmz" >
                          <option value="0">Selecione</option>
                          <option value="1">Pagadas</option>
                          <option value="2">Pendientes</option>
                        </select>
                      </div>
                      <div class="col s6 m2  l2">
                        <label>Ejecutivo</label>
                        <select id="ejecutivoselect" class="browser-default form-control-bmz chosen-select" >
                          <option value="0">Todos</option>
                          <?php
                            if($idpersonal==29){
                              $idpersonal=0;
                            } 
                            foreach ($personalrows->result() as $item) { 

                            ?>
                            <option 
                            value="<?php echo $item->personalId;?>"
                            <?php if($item->personalId==$idpersonal){ echo 'selected';}?>
                            ><?php echo $item->nombre.' '.$item->apellido_paterno.' '.$item->apellido_paterno;?></option>
                          <?php } ?>
                        </select>
                      </div>
                      <div class="col s6 m2  l2">
                        <label>Empresa</label>
                        <select id="empresaselect" class="browser-default form-control-bmz" >
                          <option value="0">Todos</option>
                          <option value="1">Kyocera</option>
                          <option value="2">D-Impresión</option>
                        </select>
                      </div>
                      <div class="col s4 m1  l1">
                        <button class="b-btn b-btn-primary" onclick="load()" title="Realizar consulta" style="margin-top: 20px;"><i class="fas fa-filter"></i> <span class="btn-filtrar"></span></button>
                      </div>
                  </div>
                <div class="row" style="overflow: auto;">
                  <div class="col s12">
                    <table id="tabla_polizasCreadas" class="table display" cellspacing="0">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Cliente</th>
                          <th>Pre factura</th>
                          <th>Vendedor</th>
                          <th>Estatus</th>
                          <th>Tipo</th>
                          <th>Fecha de<br>Creación</th>
                          <th>Fecha de<br>vencimiento</th>
                          <th>Empresa</th>
                          <th>Monto</th>
                          <th>Factura</th>
                          <th>Acciones</th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            </div>  
               </div>
        </section>
<!-- Modal finaliza -->
<div id="modalFinaliza" class="modal"  >
    <div class="modal-content ">
        <h4>Confirmación</h4>
        <div class="col s12 m12 l6">
            <div class="row" align="center">
                <h5>Deseas finalizar</h5>
            </div>
        </div>  
        <input type="hidden" id="id_venta">  
        <div class="modal-footer">
            <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
            <a href="#!" class="modal-action modal-close waves-effect waves-red red btn-flat" style="color: white;" onclick="finalizar_factura()">Aceptar</a>
        </div>
    </div>
    <!-- Modal finaliza fin-->
</div>
<!-- Modal 2 -->
            <div id="modal_cfdi" class="modal"  >
              <div class="modal-content ">
                <h4 align="center">Captura CFDI</h4>
                <br>
                <div class="row">
                  <div class="input-field col s2"></div>
                  <div class="input-field col s8">
                    <input type="text" id="cdfi_e">
                  </div>
                  <div class="input-field col s2"></div>
                  <input type="hidden" id="id_venta_e">
                </div> 
              </div>    
              <div class="modal-footer">
                <a class="modal-action modal-close waves-effect waves-red gray btn-flat" onclick="update_cfdi()">Guardar</a>
              </div>
            </div>
            
            <!-- Modal 2 -->
            <div id="modal_pago" class="modal"  >
              <div class="modal-content ">
                <div class="row">
                  <div class="col s6">
                    <h5>Registro de nuevo pago</h5>
                  </div>
                  <div class="col s6" align="right"> 
                    <div class="row" style="margin-bottom: 0px;">
                      <div class="col s7">
                        Monto de prefactura
                      </div>
                      <div style="color: red" class="col s5 monto_prefactura"></div>
                    </div>
                    <div class="row" style="margin-bottom: 0px;">
                      <div class="col s7">
                        Restante
                      </div>
                      <div class="col s5 restante_prefactura" style="color: red"></div>
                    </div>
                  </div> 
                </div>  
                <h6>Vendedor: <?php echo $nombre.' '.$apellido_paterno.' '.$apellido_materno ?></h6>
                <form class="form" id="form_pago" method="post"> 
                  <input type="hidden" id="id_venta_p" name="idcompra"><!--id que se va aguardar dependiendo si es venta, renta, poliza y combinada -->
                  <div class="row">
                    <div class="input-field col s4">
                      <input type="date" name="fecha" id="fecha_p" class="form-control-bmz">
                      <label for="fecha" class="active">Fecha</label>
                    </div>
                    <div class="input-field col s4">
                      <label for="idmetodo" class="active">Método de pago</label><br>
                      <select name="idmetodo" id="idmetodo" class="browser-default chosen-select" onchange="idmetodop()">
                        <option></option>
                         <?php 

                         foreach ($get_f_formapago->result() as $item) { 
                            if($item->id==1){
                              if($pfectivo==1){
                                echo '<option value="'.$item->id.'">'.$item->formapago_text.'</option>';
                              }
                            }else{
                              echo '<option value="'.$item->id.'">'.$item->formapago_text.'</option>';
                            }
                        } ?>
                      </select>  
                    </div>
                    <div class="input-field col s4">
                      <input type="number" name="pago" id="pago_p">
                      <label for="pago" class="active">Monto</label>
                    </div>
                  </div>
                  <div class="row">
                    <div class="input-field col s9">
                      <textarea name="observacion" id="observacion_p"></textarea>
                      <label for="observacion_p" class="active">Comentario</label>
                    </div>
                    <div class="col s3">
                    </div> 
                  </div>
                </form>
                <div class="row">
                    <div class="input-field col s9">
                      
                    </div>
                    <div class="col s3">
                      <button class="waves-effect waves-light btn modal-triggert guardar_pago_compras" onclick="guardar_pago_compras()">Guardar</button> 
                    </div> 
                  </div>
                <div class="row">
                  <div class="col s12">
                    <span class="text_tabla_pago"></span>
                  </div>
                </div>
              </div>    
              
            </div>

            <div id="modal_eliminarpago" class="modal"  >
                <div class="modal-content ">
                    <h4>Eliminar pago</h4> 
                    <p>Ingresa contraseña</p>
                    <div class="row">  
                      <div class="input-field col s12">
                        <input id="ver_pass" type="password">
                        <label for="ver_pass" class="active">Contraseña</label>
                      </div>
                    </div>  
                </div>
                <div class="modal-footer">
                    <button class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</button>
                    <button class="waves-effect waves-red red btn-flat" style="color: white;" onclick="eliminarpago()">Aceptar</button>
                </div>
                <!-- Modal finaliza fin-->
            </div>


<div id="modal_addfactura" class="modal"  style="width: 90%">
    <div class="modal-content ">
        <h4>Agregar una factura</h4>
        <input type="hidden" id="addfactcontrato" value="160">
        <input type="hidden" id="addfactperiodo">
        <input type="hidden" id="addfactpersonal" value="1">
        <input type="hidden" id="totalgeneralvalor" value="0">
        <div class="row">
          <div class="col s12">
            <table id="tabla_facturacion" name="tabla_facturacion_cliente" class="responsive-table display" cellspacing="0">
                            <thead>
                              <tr>
                                <th>#</th>
                                <th>Folio</th>
                                <th>Cliente</th>
                                <th>RFC</th>
                                <th>Monto</th>
                                <th>Estatus</th><!--sin timbrar timbrada cancelada-->
                                <th>Fecha</th>
                                <th></th>
                                
                              </tr>
                            </thead>
                            <tbody>
                            </tbody>
                          </table>
          </div>
        </div>
        
        
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
        <a href="#!" class="modal-action modal-close waves-effect waves-red red btn-flat" onclick="agregarfacturas()" style="color: white;">Aceptar</a>
    </div>
    <!-- Modal finaliza fin-->
</div>   



<div class="modal fade text-left" id="modalpolizadetalle" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lx" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Equipos</h4>
                
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-md-12 add_info_pol_dll">
                </div>
                  
                </div>
              </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="modal-action modal-close btn  btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>