<script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/loading/jquery.loading.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/plugins/loading/demo.css">

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/plugins/toastr/toastr.css">
<script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/toastr/toastr.min.js" ></script>

<script type="text/javascript" src="<?php echo base_url(); ?>public/js/complemeto.js?v=<?php echo date('YmdGis');?>" ></script>
<script type="text/javascript">
	$(document).ready(function($) {
		<?php  if (isset($_GET['idcomp'])) { ?>
			refacturacion_comp(<?php echo $_GET['idcomp'];?>);
		<?php } ?>
		<?php  if (isset($_GET['tr'])) {  ?>
			$('#facturarelacionada').click();
			$('#TipoRelacion').val('<?php echo $_GET['tr'];?>');
			$('#uuid_r').val('<?php echo $_GET['uuid'];?>');
		<?php } ?>
	});
</script>