
  <body>
    <!-- Start Page Loading -->
    <div id="loader-wrapper">
      <div id="loader"></div>
      <div class="loader-section section-left"></div>
      <div class="loader-section section-right"></div>
    </div>
    <!-- End Page Loading -->
    <div id="error-page">
      <div class="row">
        <div class="col s12">
          <div class="browser-window">
            <div class="top-bar">
              <div class="circles">
                <div id="close-circle" class="circle"></div>
                <div id="minimize-circle" class="circle"></div>
                <div id="maximize-circle" class="circle"></div>
              </div>
            </div>
            <div class="content " align="center">
              <div class="row ">
                <div id="site-layout-example-top" class="col s12 gradient-45deg-light-red-orange">
                  <p class="flat-text-logo center white-text caption-uppercase ">Esta página aún se encuentra en construcción</p>
                </div>
                <div id="site-layout-example-right" class="col s12 m12 l12 gradient-45deg-light-red-orange">
                  <div class="row center">
                    <img src="<?php echo base_url(); ?>app-assets/images/1024_kyocera_logo.png" alt="">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    