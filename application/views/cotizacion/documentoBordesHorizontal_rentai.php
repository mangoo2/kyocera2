<style type="text/css" class="print_css">
  .datosClienteCotizacion{
    background: <?php echo $configuracionCotizacion->color ?> !important;
    -webkit-print-color-adjust:exact;
  }
  html{
        -webkit-print-color-adjust: exact;
  }
  .tablaBordesPrincipal p{
    margin: 0px
  }
  .badgeaction {
    border-radius: 13px !important;
    min-width: 24px !important;
    cursor: pointer;
}
  @media print {
    html{
        -webkit-print-color-adjust: exact;
    }
    .btn_imp{
      display: none;
    }
    td,th{
      font-size: 12px;
      padding: 1px;
    }
    h1{
      margin-top: 10px;
      margin-bottom: 10px;
    }
    .contenido4 h1{
      font-size: 10px;
    }
  }
</style>
<input type="hidden" id="estatuscot" value="<?php echo $datosCotizacion[0]->estatus;?>">
<!-- START CONTENT -->
<section id="content" width="100%">
  <!--start container-->
  <div class="container" width="100%">
    <div class="section" style="margin-top: -53px;">
     <br>
      <div class="row">
        <input id="base_url" type="hidden" name="base_url" value="<?php echo base_url(); ?>">
        <input id="idCotizacion" type="hidden" name="idCotizacion" value="<?php echo $datosCotizacion[0]->id; ?>">
        
        <!-- FORMULARIO  card-panel-->
        <div class="col s12">
          <div class="">

            <!-- ENCABEZADO PRINCIPAL PDF -->
            <div class="row"> 
              <div class="col s3">
                <img src="<?php echo base_url(); ?>app-assets/images/<?php echo $configuracionCotizacion->logo1; ?>">
              </div>
              <div class="col s6">
                <p class="headerPDF"><?php echo $configuracionCotizacion->textoHeader; ?></p>
              </div> 
              <div class="col s3">
                <p align="center"><img class="" src="<?php echo base_url(); ?>app-assets/images/1024_kyocera_logo_mds.png" alt="materialize logo"  style="max-width:200px; width: 100%;"></p>
              </div>
            </div>

            <!-- APARTADO DE INFORMACIÓN DE RECEPTOR DE COTIZACIÓN-->
            
            
            <div class="row">
              <table class="centered datosClienteCotizacion tablaBordesHeader" style="width:100%">
                  <thead>
                    
                  </thead>
                  <tbody class="tablaBordes">
                    <tr >
                      <td class="tablaBordesHeader" >
                        Cotización Especial Para: <br>
                        <?php echo $datosCliente->empresa ?>
                      </td>
                      <td class="tablaBordesHeader">
                        Atención para: <br>
                        <?php echo $datosCotizacion[0]->atencion ?>
                      </td>
                      <td class="tablaBordesHeader">
                        Correo: <?php echo $datosCotizacion[0]->correo ?> <br>
                        Teléfono: <?php echo $datosCotizacion[0]->telefono ?>
                      </td>
                      <td class="tablaBordesHeader">
                        Cotización No. <?php echo $datosCotizacion[0]->id?> 
                      </td>
                    </tr>
                  </tbody>
                </table>
            </div>
            <div align="center"><div class="" id="textoHeader2" contenteditable="true"><?php echo $configuracionCotizacion->textoHeader2; ?></div></div>
            <div class="row button_add_cotizacion">
              <div class="col s3">
                <a class="btn-bmz btn cyan waves-effect waves-light obj_remove_x" type="button" href="<?php echo base_url(); ?>Cotizaciones/altaCotizacion/<?php echo $button_add_cotizacion;?>&eleccionCotizar=1&rentaventapoliza=2" >Agregar nuevo</a>
              </div>
            </div>
            <!------------ TABLA DE INFORMACIÓN DE EQUIPOS ------------>
            <div class="row">
                <table class="centered bordered letraTablaImpresionEquipos tablaBordesPrincipal" id="tablaHorizontal" style="width:100%">
                  <thead>
                    <tr class="tablaBordesPrincipal">
                        <th style="font-size:10px;" class="tablaBordesPrincipal"><?php echo $tipoimg; ?></th>
                        <th class="tablaBordesPrincipal">Modelo</th>
                        <th class="tablaBordesPrincipal">Cantidad</th>
                        <th class="tablaBordesPrincipal thdatosm">Renta Mensual Monocomatica</th>
                        <th class="tablaBordesPrincipal thdatosm">Páginas monocromáticas incluídas</th>
                        <th class="tablaBordesPrincipal thdatosm">Excedente Monocromatica</th>
                        <th class="tablaBordesPrincipal thdatosc">Renta Mensual Color</th>
                        <th class="tablaBordesPrincipal thdatosc">Páginas Color incluídas</th>
                        <th class="tablaBordesPrincipal thdatosc">Excedente Color</th>
                    </tr>
                  </thead>
                  <tbody >
                   <?php 
                      $totalm=0;
                      $totalc=0;
                      $datosmono=0;
                      $datoscolor=0;
                      foreach ($detallesCotizacion->result() as $item) { 
                          if ($item->r_costo_m==0) {
                            $datosmono=1;
                          }
                          if ($item->r_costo_c==0) {
                            $datoscolor=1;
                          }


                          $totalm=$totalm+($item->r_costo_m*$item->cantidad);
                          $totalc=$totalc+($item->r_costo_c*$item->cantidad);
                          
                          if ($item->foto==null) {
                             $img=base_url().'app-assets/images/1024_kyocera_logo.png';
                          }else{
                            $img=base_url().'uploads/equipos/'.$item->foto;
                          }
                          $accesorios=$this->Cotizaciones_model->getcotizacionaccesorios($idCotizacion,$item->idEquipo);
                          $accerow=$accesorios->num_rows();
                          //$accerow=0;
                    ?>
                     <tr class="tablaBordesPrincipal">
                        <td style="font-size:10px;" class="tablaBordesPrincipal">
                          <img src="<?php echo $img; ?>" class="onMouseOverDisabled" style="width: 75px;" >
                          <span class="new badge red badgeaction" onclick="deleteequipov(<?php echo $item->DT_RowId;?>)" >X</span>
                        </td>
                        <td class="tablaBordesPrincipal"><?php echo $item->modelo;?></td>
                        <td class="tablaBordesPrincipal" onclick="editarp_eq(<?php echo $item->DT_RowId;?>,<?php echo $item->cantidad ?>)"><?php echo $item->cantidad;?></td>
                        <td class="tablaBordesPrincipal thdatosm" style="padding: 0px;" 
                            ondblclick="editarconficionesi(<?php echo $item->DT_RowId;?>,<?php echo $item->r_costo_m; ?>,<?php echo $item->r_volumen_m; ?>,<?php echo $item->excedentem; ?>,<?php echo $item->r_costo_c; ?>,<?php echo $item->r_volumen_c; ?>,<?php echo $item->excedentec; ?>)">$ <?php echo number_format($item->r_costo_m,2,'.',',');?></td>
                        <td class="tablaBordesPrincipal thdatosm " ondblclick="editarconficionesi(<?php echo $item->DT_RowId;?>,<?php echo $item->r_costo_m; ?>,<?php echo $item->r_volumen_m; ?>,<?php echo $item->excedentem; ?>,<?php echo $item->r_costo_c; ?>,<?php echo $item->r_volumen_c; ?>,<?php echo $item->excedentec; ?>)" ><?php echo $item->r_volumen_m;?></td>
                        <td class="tablaBordesPrincipal thdatosm " ondblclick="editarconficionesi(<?php echo $item->DT_RowId;?>,<?php echo $item->r_costo_m; ?>,<?php echo $item->r_volumen_m; ?>,<?php echo $item->excedentem; ?>,<?php echo $item->r_costo_c; ?>,<?php echo $item->r_volumen_c; ?>,<?php echo $item->excedentec; ?>)"><?php echo $item->excedentem; ?></td>
                        <td class="tablaBordesPrincipal thdatosc " ondblclick="editarconficionesi(<?php echo $item->DT_RowId;?>,<?php echo $item->r_costo_m; ?>,<?php echo $item->r_volumen_m; ?>,<?php echo $item->excedentem; ?>,<?php echo $item->r_costo_c; ?>,<?php echo $item->r_volumen_c; ?>,<?php echo $item->excedentec; ?>)">$ <?php echo number_format($item->r_costo_c,2,'.',',');?></td>
                        <td class="tablaBordesPrincipal thdatosc " ondblclick="editarconficionesi(<?php echo $item->DT_RowId;?>,<?php echo $item->r_costo_m; ?>,<?php echo $item->r_volumen_m; ?>,<?php echo $item->excedentem; ?>,<?php echo $item->r_costo_c; ?>,<?php echo $item->r_volumen_c; ?>,<?php echo $item->excedentec; ?>)"><?php echo $item->r_volumen_c;?></td>
                        <td class="tablaBordesPrincipal thdatosc " ondblclick="editarconficionesi(<?php echo $item->DT_RowId;?>,<?php echo $item->r_costo_m; ?>,<?php echo $item->r_volumen_m; ?>,<?php echo $item->excedentem; ?>,<?php echo $item->r_costo_c; ?>,<?php echo $item->r_volumen_c; ?>,<?php echo $item->excedentec; ?>)"><?php echo $item->excedentec;?></td>
                        
                    </tr>
                   <?php if($accerow>0){ 
                            foreach ($accesorios->result() as $item2) {
                            ?>
                            <tr class="tablaBordesPrincipal">
                                <td class="tablaBordesPrincipal"></td>
                                <td class="tablaBordesPrincipal" style="font-size: 10px;"><?php echo $item2->nombre;?></td>
                            </tr>
                      <?php } ?>
                            
                    <?php }?> 
                          <tr class="tablaBordesPrincipal">
                                <td class="tablaBordesPrincipal" colspan="9" ><?php echo $item->especificaciones_tecnicas;?></td>
                                   
                            </tr>


                    <?php } ?> 
                    
                  </tbody>
                </table>
                <table class="centered bordered letraTablaImpresionEquipos tablaBordesPrincipal" style="width:100%">
                  <!--<tr class="tablaBordesPrincipal">
                      <td class="tablaBordesPrincipal" colspan="2" align="right" style="text-align: right;"><b>SubTotal:</b></td>
                      <td class="tablaBordesPrincipal "><b>$ <?php echo number_format(($totalc+$totalm),2,'.',',');?></b></td> 
                  </tr>-->
                  <!--<tr class="tablaBordesPrincipal">
                      <td class="tablaBordesPrincipal" colspan="2" align="right" style="text-align: right;"><b>I.V.A:</b></td>
                      <td class="tablaBordesPrincipal "><b>$ <?php echo number_format(($totalc+$totalm)*0.16,2,'.',',');?></b></td> 
                  </tr>-->
                  <tr class="tablaBordesPrincipal">
                      <td class="tablaBordesPrincipal" colspan="2" align="right" style="text-align: right;"><b>TOTAL:</b></td>
                      <td class="tablaBordesPrincipal "><b>$ <?php echo number_format(($totalc+$totalm),2,'.',',');?></b></td> 
                  </tr>
                </table>
                
            </div>
           


            <div align="center"><?php echo $configuracionCotizacion->leyendaPrecios; ?></div>
            
            <!-- Observaciones -->
            <div class="row" style="margin-bottom: -12px">
              <table class="centered letraTablaFooterEquipos" style="width:100%">
                  <thead>
                      <tr>
                        <th class="tablaBordesPrincipal letraTablaFooterEquiposHeader">
                          <div id="contenido1" contenteditable="true" class="info1" oninput="saveinfo_o(1)"><?php 
                            if($datosCotizacion[0]->info1!=null){
                              echo $datosCotizacion[0]->info1;
                            }else{
                              echo $configuracionCotizacion->tituloObservaciones;
                            }
                          //$configuracionCotizacion->tituloObservaciones; ?></div>
                        </th>
                        <th class="tablaBordesPrincipal letraTablaFooterEquiposHeader"><?php echo $configuracionCotizacion->titulo1Footer; ?></th>
                      </tr>
                  </thead>
                  <tbody >
                    <tr>
                        <td class="tablaBordesPrincipal contenido1" width="50%">
                          <div  id="contenido2" contenteditable="true" class="info2" oninput="saveinfo_o(2)"><?php 
                            if($datosCotizacion[0]->info2!=null){
                              echo $datosCotizacion[0]->info2;
                            }else{
                              echo $configuracionCotizacion->contenidoObservaciones;
                            }
                          //$configuracionCotizacion->contenidoObservaciones; ?></div>
                        </td>
                        <td class="tablaBordesPrincipal contenido2" width="50%"><?php echo $configuracionCotizacion->contenido1Footer; ?></td>
                    </tr>
                  </tbody>
                </table>
            </div>
            <br>

            
            <div class="row" style="margin-bottom: -12px">
              <table class="centered letraTablaFooterEquipos" style="width:100%">
                  <thead>
                      <tr>
                        <th class="tablaBordesPrincipal">
                          <div id="contenido3" contenteditable="true" class="info3" oninput="saveinfo_o(3)"><?php 
                            if($datosCotizacion[0]->info3!=null){
                              echo $datosCotizacion[0]->info3;
                            }else{
                              echo $configuracionCotizacion->titulo2Footer;    
                            }
                          //$configuracionCotizacion->titulo2Footer; ?></div>
                        </th>
                        <th class="tablaBordesPrincipal"><?php echo $fechareg; ?></th>
                      </tr>
                  </thead>
                  <tbody >
                    <tr>
                        <td class="tablaBordesPrincipal " width="50%" >
                          
                          <div id="contenido4" contenteditable="true" class="info4" onchange="saveinfo_o(4)" ><?php 
                            if($datosCotizacion[0]->info4!=null){
                              echo $datosCotizacion[0]->info4;
                            }else{
                              echo $configuracionCotizacion->contenido2Footer;  
                            }
                          //$configuracionCotizacion->contenido2Footer; ?></div>  
                        </td>
                        <td class="tablaBordesPrincipal contenido4" width="50%">
                          <?php echo $configuracionCotizacion->firma;?><b><?php echo $nombreUsuario ?></b><br><?php echo $emailUsuario ?>
                          </td>
                    </tr>
                  </tbody>
                </table>
            </div>

           

<!--columna 1---------------------------------------------------------------->

              <form class="form" id="" method="post">
                <?php
                if(isset($cliente))
                {
                ?>
                <input id="idCliente" name="idCliente" type="hidden" value="<?php echo $idCliente; ?>">
                <?php
                }
                ?>
                <div class="row">
                  <div class="input-field col s12">
                    <button class="btn cyan waves-effect waves-light right btn_imp" type="button" onclick="btn_guardar()">Imprimir
                    <i class="material-icons right">send</i>
                    </button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<div id="modaleditor" class="modal">
  <div class="modal-content ">
    <h4>Editar Precio</h4>
    <div class="border_datos" id="fiscales_dato1">
      <div class="col s24 m24 l12">
        <div class="row ">
            <div class="row">
              <input id="idEquipon" type="hidden" value="">
              <div class="input-field col s12">
                <input id="password" type="password" class="validate" autocomplete="new-password" required>
                <label for="password">Password</label>
              </div>
            </div>
            <div class="row">
              <div class="input-field col s12">
                <input id="newprecio" type="number" class="validate" placeholder="Precio" required>
                <label for="newprecio">Nuevo Precio</label>
              </div>
            </div>
                                        

        </div>                          
      </div>
    </div>                    
  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
    <a href="#!" class="modal-action modal-close waves-effect waves-green green btn-flat editarcosto">Aceptar</a>
  </div>
</div>
<?php foreach ($detallesCotizacion->result() as $item) { 
          if ($item->tipo==1) {
            $tipoimpresora=0;
          }else{
            $tipoimpresora=1;
          }
         
                               
          if($tipoimpresora==0){ ?>
            <script type="text/javascript">
            // <?php echo $tipoimpresora.','.$item->tipo;?>
            </script>     
            <script type="text/javascript">
                   $('.ocultarsiesbn').hide();
            </script>
    <?php }
    }      
?>
<script type="text/javascript">
  $(document).ready(function($) {
    <?php if($datosmono==1){ ?>
      $('.thdatosm').remove();
    <?php }?>
    <?php if($datoscolor==1){ ?>
      $('.thdatosc').remove();
    <?php }?>
  });
</script>
<script type="text/javascript">
  function saveinfo_o(tipo){
    saveinfo(<?php echo $idCotizacion;?>,tipo);
  }
  function btn_guardar(){
    saveinfo_o(1);
    saveinfo_o(2);
    saveinfo_o(3);
    saveinfo_o(4);
    saveinfo_o(5);
    
    window.open(base_url+'index.php/Cotizaciones/pdf/'+idCotizacion, '_blank');
  }
  function saveinfo(idcoti,tipo){
    var info=$('.info'+tipo).html();
    console.log(info);
    $.ajax({
        type:'POST',
        url: base_url+'Generales/editarinfocotizacion',
        data: {
            cotizacion: idcoti,
            tipo: tipo,
            info: info
            },
            async: false,
            statusCode:{
                404: function(data){
                    //swal("Error", "404", "error");
                },
                500: function(){
                    //swal("Error", "500", "error"); 
                }
            },
            success:function(data){
              
            }
        });
  }
</script>
</script>