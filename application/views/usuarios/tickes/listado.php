<style type="text/css">
  table td{
    font-size: 12px;
    padding: 6px 7px;
  }
  .btns-factura{
    min-width: 147px;
  }
  .btn-retimbrar{
    /*display: none; */
  }
</style>
<div  clas="row" style="display: none;">
 <input type="password" name="" >
  <input type="search" name="" > 
</div>

<!-- START CONTENT -->
    <section id="content">
      <!--start container-->
        <div class="container">
          <div class="section">
            <div class="">
                    <h4 class="header">Tickets</h4>
                    
                    <div class="row">
                      <div class="col s12">
                        <ul class="tabs tab-demo z-depth-1">
                          <li class="tab col s3"><a class="active" href="#test1">Tickets</a>
                          </li>
                          
                        </ul>
                      </div>
                      <div class="col s12 ">
                        <div id="test1" class="col s12 card-panel">
                          <!------------->
                          <div class="row">
                            
                          <div class="row">
                            <div class="col s3" >
                              <label>Vendedor</label>
                              <select id="personals" class="browser-default form-control-bmz" onchange="loadtable()">
                                <option value="0">Todos</option>
                                <?php foreach ($personalrows->result() as $item) { ?>
                                  <option value="<?php echo $item->personalId; ?>" <?php if($item->personalId==$idpersonal){ echo 'selected';}?> ><?php echo $item->nombre; ?> <?php echo $item->apellido_paterno; ?> <?php echo $item->apellido_materno; ?></option>
                                <?php } ?>
                              </select>
                            </div>
                          </div>
                          <table id="tabla_facturacion" name="tabla_facturacion_cliente" class="responsive-table display" cellspacing="0">
                            <thead>
                              <tr>
                                <th>#</th>
                                <th>Personal</th>
                                <th>Solicitud</th>
                                <th>Estatus</th>
                                <th>Fecha</th>
                                <th></th>
                                
                              </tr>
                            </thead>
                            <tbody>
                            </tbody>
                          </table>
                          <!------------->
                        </div>
                        
                      </div>
                    </div>
                  

                  
              </div> 

            </div> 
          </div>
</section>
<div id="modaldescripcion" class="modal" >
  <div class="modal-content" style="padding: 5px">
    <div class="row descripciongeneral" style="max-height: 400px; overflow: auto;">
      
    </div>
    <div class="row">
      
      <div class="col s12">
        <textarea class="form-control-bmz" id="respuesta"></textarea>
      </div>
      <div class="col s12">
        <button class="btn cyan waves-effect waves-light right responderticket"  name="action">Responder 
          <i class="material-icons right">send</i>
        </button>
      </div>
    </div>
  </div>  
  <div class="modal-footer">
    <a class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
  </div>
</div>