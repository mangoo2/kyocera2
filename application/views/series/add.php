<style type="text/css">
  .chosen-container{
    margin: 0px;
  }
  .datostableinput2{
    border-bottom:0px !important;
  }
  td{
    padding: 0px;
  }
</style>
<!-- START CONTENT -->
<section id="content">
  <!--start container-->
  <div class="container">
    <div class="section">   
      <div>
          <h4 class="header">Agregar Series</h4>
          <div class="row">
            <div class="col s12">
              <ul class="collapsible collapsible-accordion" data-collapsible="accordion">
                <li class="">
                  <div class="collapsible-header">
                    <i class="material-icons">print</i> Equipos</div>
                  <div class="collapsible-body">
                    <div class="row">
                      <div class="input-field col s3">
                        <select id="bodega1" name="bodega1" class="browser-default chosen-select">
                          <?php foreach ($bodegas as $item) { ?>
                              <option value="<?php echo $item->bodegaId; ?>"><?php echo $item->bodega; ?></option>
                          <?php } ?> 
                        </select>
                        <label for="bodega1" class="active">Bodega</label>
                      </div>
                      <div class="input-field col s3">
                        <select id="equipos" name="equipos" class="browser-default chosen-select">
                          <?php foreach ($equipos->result() as $item) { ?>
                              <option value="<?php echo $item->id; ?>"><?php echo $item->modelo; ?></option>
                          <?php } ?> 
                        </select>
                        <label for="equipos" class="active">Equipo</label>
                      </div>
                      <div class="input-field col s3">
                        <input placeholder="Serie" id="serie1" type="text" class="validate">
                        <label for="serie1">Serie</label>
                      </div>
                      <div class="col s3">
                        <button class="btn waves-effect waves-light  addbtnequipo" name="action">Agregar
                          <i class="material-icons right">system_update_alt</i>
                        </button>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col s12">
                        <table id="tabla_sequipos" class="striped responsive-table display" cellspacing="0">
                        <thead>
                          <tr>
                            <th>Bodega</th>
                            <th>Equipo</th>
                            <th>Serie</th>
                            <th></th>
                          </tr>
                        </thead>
                        <tbody class="tabla_addsequipos">
                        </tbody>
                      </table>
                      </div>
                    </div> 
                    

                  </div>
                </li>
                <li>
                  <div class="collapsible-header">
                    <i class="material-icons">developer_board</i> Accesorios</div>
                  <div class="collapsible-body">
                    <div class="row">
                      <div class="input-field col s2">
                        <select id="bodega2" name="bodega2" class="browser-default chosen-select">
                          <?php foreach ($bodegas as $item) { ?>
                              <option value="<?php echo $item->bodegaId; ?>"><?php echo $item->bodega; ?></option>
                          <?php } ?> 
                        </select>
                        <label for="bodega2" class="active">Bodega</label>
                      </div>
                      <div class="input-field col s2">
                        <select id="accesorio" name="accesorio" class="browser-default chosen-select">
                          <?php foreach ($accesorios->result() as $item) { ?>
                              <option value="<?php echo $item->id; ?>"><?php echo $item->nombre; ?></option>
                          <?php } ?> 
                        </select>
                        <label for="accesorio" class="active">Accesorio</label>
                      </div>
                      <div class="col s2">
                        <div class="input-field col s12 con_serie_a">
                          <input placeholder="Serie" id="serie2" type="text" class="validate">
                          <label for="serie2">Serie</label>
                        </div>
                        <div class="input-field col s12 sin_serie_a" style="display: none;">
                          <input placeholder="Cantidad" id="Cantidad_a" type="text" class="validate" min="1" value="1">
                          <label for="Cantidad">Cantidad</label>
                        </div>
                      </div>
                      <div class=" col s2">
                        <input type="checkbox" id="sinserie_a" class="sinserie_a" />
                        <label for="sinserie_a">Sin serie</label>
                      </div>
                      <div class="col s3">
                        <button class="btn waves-effect waves-light addbtnaccesorio" name="action">Agregar
                          <i class="material-icons right">system_update_alt</i>
                        </button>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col s12">
                        <table id="tabla_saccesorios" class="striped responsive-table display" cellspacing="0">
                        <thead>
                          <tr>
                            <th>Bodega</th>
                            <th>Accesorio</th>
                            <th>Cantidad</th>
                            <th>Serie</th>
                            <th></th>
                          </tr>
                        </thead>
                        <tbody class="tabla_addsaccesorios">
                        </tbody>
                      </table>
                      </div>
                    </div> 
                  </div>
                </li>
                <li>
                  <div class="collapsible-header">
                    <i class="material-icons">build</i> Refacciones</div>
                  <div class="collapsible-body">
                    <div class="row">
                      <div class="input-field col s2">
                        <select id="bodega3" name="bodega3" class="browser-default chosen-select">
                          <?php foreach ($bodegas as $item) { ?>
                              <option value="<?php echo $item->bodegaId; ?>"><?php echo $item->bodega; ?></option>
                          <?php } ?> 
                        </select>
                        <label for="bodega3" class="active">Bodega</label>
                      </div>
                      <div class="input-field col s2">
                        <select id="refaccion" name="refaccion" class="browser-default chosen-select">
                          <?php foreach ($refacciones->result() as $item) { ?>
                              <option value="<?php echo $item->id; ?>"><?php echo $item->nombre.' ('.$item->codigo.')'; ?></option>
                          <?php } ?> 
                        </select>
                        <label for="refaccion" class="active">Refaccion</label>
                      </div>
                      <div class=" col s3">
                        <div class="input-field col s12 con_serie">
                          <input placeholder="Serie" id="serie3" type="text" class="validate">
                          <label for="serie3">Serie</label>
                        </div>
                        <div class="input-field col s12 sin_serie" style="display: none;">
                          <input placeholder="Cantidad" id="Cantidad" type="text" class="validate" min="1" value="1">
                          <label for="Cantidad">Cantidad</label>
                        </div>
                        
                      </div>
                      <div class=" col s2">
                        <input type="checkbox" id="sinserie" class="sinserie" />
                        <label for="sinserie">Sin serie</label>
                      </div>
                      <div class="col s3">
                        <button class="btn waves-effect waves-light addbtnrefaccion" name="action">Agregar
                          <i class="material-icons right">system_update_alt</i>
                        </button>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col s12">
                        <table id="tabla_srefacciones" class="striped responsive-table display" cellspacing="0">
                          <thead>
                            <tr>
                              <th>Bodega</th>
                              <th>Refaccion</th>
                              <th>Cantidad</th>
                              <th>Serie</th>
                              <th></th>
                            </tr>
                          </thead>
                          <tbody class="tabla_addsrefacciones">
                          </tbody>
                        </table>
                        </div>
                    </div> 
                  </div>
                </li>
              </ul>
            </div>
          </div>
          <div class="row">

            <button class="btn cyan waves-effect waves-light btn right saveallseries">
                    <i class="material-icons right">save</i> Guardar</button>
          </div>
      </div>
    </div>
  </div> 
</section>

              