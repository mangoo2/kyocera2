<style type="text/css">
  .chosen-container{margin-left: 0px;}
  table.dataTable tbody th, table.dataTable tbody td {padding: 5px 10px;}
  .burbuja_agb{position: absolute;margin-top: -8px !important;border-radius: 14px !important; padding: 0px !important;}
  .btn_agb_w{min-width: 134px;}
  .bloqueo,.bloqueorow{
    border: 1px solid red; 

    -webkit-box-shadow: 10px 10px 5px -10px rgb(255 8 8); 
    -moz-box-shadow: 10px 10px 5px -10px rgba(255,8,8,1); box-shadow: 10px 10px 5px -10px rgb(255 8 8);
  }
  .bloqueorow td{
    border-top: 1px solid red !important;
    border-bottom: 1px solid red !important;
  }
  .bloqueorow input{
    border: 1px solid #f00 !important;
  }
  .costocero{display: none;}
  <?php
    //27 35 42 antonio 9 israel 56 adriana 62 crstina 50 marcela 17 julio 18 diana
    if($idpersonal==27 or $idpersonal==35 or $idpersonal==42 or $idpersonal==9 or $idpersonal==56 or $idpersonal==62 or $idpersonal==50 or $idpersonal==18 or $idpersonal==17){ ?>
       .costocero{display: block !important;}
  <?php } ?>
  option:disabled{background: #bbb7b7;}
    .intermitente_red{
  
  box-shadow: 0px 0px 20px rgba(250,10,10,1);
  animation: infinite resplandorAnimation 2s;
  
}
@keyframes resplandorAnimation {
  0%,100%{
    box-shadow: 0px 0px 20px rgba(250,10,10,1);
  }
  50%{
  box-shadow: 0px 0px 0px;
  
  }

}
.input-ag {
  padding-left: 40px !important;
}
.input-ag i{
  position: absolute;
  margin-left: -30px;
  margin-top: 26px;
}
</style>
<input id="sol_orden_com"   type="hidden" value="<?php echo $sol_orden_com;?>">
<input id="clientename"   type="hidden" value="<?php echo $nombreCliente;?>">
<input id="blq_perm_ven_ser"   type="hidden" value="<?php echo $blq_perm_ven_ser;?>">
<input id="perfilid"   type="hidden" value="<?php echo $perfilid;?>">
<input id="fechaactual" type="hidden" value="<?php echo date('Y-m-d');?>">
<div class="newstyles"></div>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/bulma-badge.min.css">
<!-- START CONTENT --> 
  <section id="content" width="100%">
    <!--start container-->
    <div class="container" width="100%">

          <input id="base_url" type="hidden" name="base_url" value="<?php echo base_url(); ?>">
          <input id="contadorPolizas" type="hidden" name="contadorPolizas" value="1">
          <input id="contadorConsumibles" type="hidden" name="contadorConsumibles" value="1">
          <input id="contadorRefacciones" type="hidden" name="contadorRefacciones" value="1">
          <input id="aaa" type="hidden" value="<?php echo $aaa ?>">
          <input id="idpersonal" type="hidden" value="<?php echo $idpersonal ?>">
          <div class="section">

              <div class="content-header">Ventas</div>
              <div class="row">
                <div class="col s2 m2 l2 desbloqueocostosceros" style="float:right; text-align: right;" onclick="desbloqueocostosceros()">
                  Costos Cero <i class="fa fa-lock fa-fw"></i>
                </div>
              </div>
              <?php if ($condicion==null) { }else{?>
                <div class="row">
                  <div class="col s12 m12 l12">
                    <div class="card gradient-45deg-deep-orange-orange darken-2">
                      <div class="card-content " style="padding-top: 5px;padding-bottom: 5px;">
                        <div class="row">
                          <div class="card col s12 m12 l12">
                            <?php echo $condicion;?>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              <?php  } ?>

              <div class="row">

                <div class="col s12 m12 24">

                    <div class="card-panel">
                        <div class="row">
                          <div class="col s6"></div>
                            <div class="col s6">
                              <select id="atencionselect" class="browser-default form-control-bmz" style="display: none;" onchange="obtenercontactosss()">
                                      <option 
                                      data-atencionpara=""
                                      data-email=""
                                      data-telefono=""
                                      data-celular=""
                                      disabled selected
                                      >Seleccione</option>
                                  <?php foreach ($datoscontacto->result() as $item) { ?>
                                      <option 
                                        data-atencionpara="<?php echo $item->atencionpara;?>"
                                        data-email="<?php echo $item->email;?>"
                                        data-telefono="<?php echo $item->telefono;?>"
                                        data-celular="<?php echo $item->celular;?>"

                                        ><?php echo $item->atencionpara;?></option>
                                  <?php } ?>
                                </select>
                            </div>
                        </div>
                      <form id="formulario_cotizacion">
                        <input type="hidden" name="idventaaddnew" id="idventaaddnew" value="0">
                        <div class="row paraagregarnuevos">
                          <div class="col s6 selectweb">
                            <?php
                              if(isset($_SESSION['selectweb'])){
                                if($this->session->userdata('selectweb')==1){
                                  $empresa=0;
                                  if($this->session->userdata('idpersonal')==64){//64 es de amazon
                                    $empresa=0;
                                  }
                                  if($this->session->userdata('idpersonal')==63){//63 es de mercadolibre
                                    $empresa=2;
                                  }
                                }else{
                                  $empresa=1;
                                }
                              }else{
                                $empresa=1;
                              }
                            ?>
                            <select name="tipov" id="tipov" class="browser-default form-control-bmz " onchange="validarpermisosbodega()">
                              <option value="0" <?php if($empresa==2){ echo 'style="display: none;" disabled';} ?>> Seleccione</option>
                              <option value="1" <?php if($empresa==1){ echo 'selected';} if($empresa==2){ echo 'style="display: none;" disabled';}?>>Alta Productividad</option>
                              <option value="2" <?php if($empresa==2){ echo 'selected';}?>>D-Impresión</option>
                            </select>
                          </div>
                        </div>
                        <div class="row paraagregarnuevos">
                           <div class="input-field col s6">
                              <i class="material-icons prefix">account_box</i>
                              <input id="cliente" name="cliente"  type="text" value="<?php echo $nombreCliente;?>" disabled="">
                              <label for="cliente">Cliente / Prospecto</label>
                            </div>

                            <div class="input-ag col s6">
                              <i class="material-icons prefix">account_box</i>

                              <label for="atencion" class="active">Atención para:</label>

                              <input id="atencion" name="atencion"  type="text" placeholder=" " value="<?php echo $atencionpara;?>" style="width: 95%; float: left;" class="form-control-bmz" required>
                              <span class="fas fa-list-alt material-icons prefix" onclick="obtenercontactos()" style="margin-left: 5px;float: left;"></span>
                              
                              
                            </div>
                        </div> 

                        <div class="row paraagregarnuevos">
                            <?php 
                              if($sol_orden_com==1){
                                $row_col="col s4";
                              }else{
                                $row_col="col s6";
                              }
                            ?>
                           <div class="input-ag <?php echo $row_col;?>">
                              <i class="material-icons prefix">mail_outline</i>
                              <label for="cliente" class="active">Correo</label>
                              <input id="correo" name="correo"  type="text" placeholder=" " value="<?php echo $emailCliente;?>" class="form-control-bmz" required>
                              
                            </div>

                            <div class="input-ag <?php echo $row_col;?>">
                              <i class="material-icons prefix">call</i>
                              <label for="telefono" class="active">Teléfono:</label>
                              <input id="telefono" name="telefono"  type="text" placeholder=" " value="<?php echo $telefonoCliente;?>" class="form-control-bmz" required>
                              
                            </div>
                            <?php if($sol_orden_com==1){ ?>
                              
                              <div class="form-cot <?php echo $row_col;?>">
                                <label>Num. Orden de compra</label>
                                <div class="b-input-group mb-3">
                                  <input type="text" class="form-control-bmz" id="orden_comp" name="orden_comp" required>
                                  <div class="b-input-group-append">
                                    <button class="b-btn b-btn-primary"  type="button" onclick="retirar_soleq()"><i class="fas fa-lock"></i></button>
                                  </div>
                                </div>
                              </div>
                            <?php } ?>
                        </div>  
                        
                        <!-- ELECCION DE COTIZACION -->
                        <div class="row">
                          <div class=" col s6">
                            <div class="input-field col s5" style="margin-left: -12px;">
                              <i class="material-icons prefix">computer</i>
                              <label for="estado">¿Que desea vender?</label>
                            </div>
                            <div class="col s7 " >
                                <p>
                                  <input type="checkbox" class="filled-in" id="ventae" value="1" />
                                  <label for="ventae">Equipos</label>
                                </p>
                                <p>
                                  <input type="checkbox" class="filled-in" id="ventac" value="2" />
                                  <label for="ventac">Consumibles</label>
                                </p>
                                <p >
                                  <input type="checkbox" class="filled-in" id="ventar" value="3" />
                                  <label for="ventar">Refacciones</label>
                                </p>
                                <p class="paraagregarnuevosp">
                                  <input type="checkbox" class="filled-in" id="ventap" value="4" />
                                  <label for="ventap">Servicio</label>
                                </p>
                                <p>
                                  <input type="checkbox" class="filled-in" id="ventaa" value="5" />
                                  <label for="ventaa">Accesorios</label>
                                </p>
                            </div>
                          </div>

                          <div class="input-field col s6" style="display: none" id="opcionesConsumiblesRefacciones">
                            <div class="col s5" style="margin-left: -12px;">
                              <i class="material-icons prefix">monetization_on</i>
                              <label for="estado">Tipo de Venta</label>
                            </div>
                            <div class="col s7 margenTopDivChosen" >
                              <select id="tipo" name="tipo" class="browser-default chosen-select tipoConsumibles">
                                <option value="" disabled selected>Selecciona tipo de Venta</option>
                                <option value="1" >Venta</option>
                                <option value="0" >Venta Mostrador</option>
                              </select>
                            </div>

                          </div>
                          <div class="col s6">
                            <?php if($horario_disponible!= null or $horario_disponible!=''){ ?>
                            <div class="col s6">
                              Horario disponible
                            </div>
                            <div class="col s6">
                              <?php echo $horario_disponible;?>
                            </div>
                            <?php } ?>
                            <?php if($equipo_acceso!= null or $equipo_acceso!=''){ ?>
                            <div class="col s6">
                              Equipo de acceso
                            </div>
                            <div class="col s6">
                              <?php echo $equipo_acceso;?>
                            </div>
                            <?php } ?>
                            <?php if($documentacion_acceso!= null or $documentacion_acceso!=''){ ?>
                            <div class="col s6">
                              Documentación de acceso
                            </div>
                            <div class="col s6">
                              <?php echo $documentacion_acceso;?>
                            </div>
                            <?php } ?>
                          </div>
                          <div class="col s6 soloventas">
                            <div style="text-align: end;"><a class="btn-bmz blue" onclick="servicioaddcliente()" style="margin-bottom: 10px;">Agregar un servicio existente</a></div>
                            <div>
                              <input type="number" name="v_ser_tipo" id="v_ser_tipo" style="display:none">
                              <input type="number" name="v_ser_id" id="v_ser_id" style="display:none">
                            </div>
                            <div class="addinfoser"></div>
                          </div>  
                        </div>
                        <div class="row">
                          <ul class="collapsible" data-collapsible="accordion">
                            <li class="collapsible_venta_e" style="display: none;">
                                <div class="collapsible-header">Equipos</div>
                                <div class="collapsible-body">
                                    <div class="row">
                                      <div class="col s6">
                                        <div class="col s5" style="margin-left: -12px;">
                                          <label>Seleccionar precio</label>
                                        </div>
                                        <div class="col s7">
                                          <select  id="tipoprecioequipo" class="browser-default form-control-bmz">
                                            <option value="0">General</option>
                                            <?php if($perfilid==1 or $perfilid==8){ ?>
                                              <option value="1">Reventa local</option>
                                              <option value="2">Reventa foráneo</option>
                                            <?php } ?>
                                            <?php if($perfilid==1){ ?>
                                            <option value="3">Sin costo</option>
                                            <?php } ?>
                                          </select>
                                        </div>
                                        
                                        
                                      </div>
                                      <div class=" col s6" id="opcionesEquipos">
                                        <div class="col s5" style="margin-left: -12px;">
                                          <i class="material-icons prefix">monetization_on</i>
                                          <label for="estado">Tipo de Venta Equipos</label>
                                        </div>
                                        <div class="col s7 margenTopDivChosen" >
                                          <select id="tipo" name="tipo" class="browser-default form-control-bmz tipoventaequipo">
                                              <option value="" disabled selected>Selecciona tipo de Venta</option>
                                              <option value="1">Venta</option>
                                              <option value="2">Renta</option>
                                              <!--<option value="3">Póliza</option>-->
                                          </select>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s6" id="tipoCotizacion" style="display: none;">
                                          <div class="col s5" style="margin-left: -12px;">
                                            <i class="material-icons prefix">format_list_bulleted</i>
                                            <label for="estado">Venta</label>
                                          </div>
                                          <div class="col s7 margenTopDivChosen" >
                                            <select id="cotizar" name="cotizar" onchange="selectcotiza()" class="browser-default chosen-select">
                                                <option value="" disabled selected>Forma de Venta</option>
                                                <option value="1" >Manual</option>
                                                <!--<option value="2">Familia</option>-->
                                            </select>
                                          </div>
                                        </div>
                                        <div class="input-field col s6 select_familia" style="display: none;" id="select_familia">
                                          <div class="col s5" style="margin-left: -12px;">   
                                            <i class="material-icons prefix">person</i>
                                            <label for="estado">Familiar</label>
                                          </div>
                                          <div class="col s7 margenTopDivChosen" >
                                            <select id="idfamilia" name="idfamilia" class="browser-default chosen-select" onchange="verificaFamilia(this)">
                                               <option value="" disabled selected>Selecciona una opción</option>
                                                 
                                            </select>
                                          </div>   
                                        </div> 
                                           
                                    </div>
                                    <div class="row equipos_select" style="display: none;">
                                      <table id="tables_equipos_selected" class="display">
                                        <thead>
                                          <tr>
                                            <th></th>
                                            <th></th>
                                            <th>Equipo</th>
                                            <th>Consumibles</th>
                                            <th>Accesorios</th>
                                            <th></th>
                                            <th></th>
                                          </tr>
                                        </thead>
                                        <tbody>
                                          <?php foreach ($equiposimg as $item){ 
                                            //$stockequipo=$this->ModeloGeneral->stockequipo($item->id);
                                            $stockequipo=$item->stockequipo;
                                            ?>
                                            <tr class="equipo_row_<?php echo $item->id ?>">
                                              <td style="width: 5%;">
                                                
                                                <input type="checkbox" class="equipo_check nombre" id="equipo_check_<?php echo $item->id ?>" value="<?php echo $item->id ?>"/>
                                                <label for="equipo_check_<?php echo $item->id ?>" <?php //if($stockequipo==0){ ?> onclick="alertastockequipo(<?php echo $item->id ?>,<?php echo $stockequipo;?>)" <?php //} ?>></label>
                                                
                                              </td>
                                              <td style="width: 15%;" class="input_cant">
                                                <input type="text" id="equipo_cantidad" class="equipo_cantidad equ_cant_<?php echo $item->id ?>" value="1"  oninput="alertastockequipo(<?php echo $item->id ?>,<?php echo $stockequipo;?>)" onchange="alertastockequipo(<?php echo $item->id ?>,<?php echo $stockequipo;?>)" />
                                              </td>
                                              <td style="width: 15%;">
                                                <label class="btn btn-info btn_agb_w has-badge-rounded has-badge-danger notificacion_tipped notificacion_tipped_<?php echo $item->id ?>" for="equipo_check_<?php echo $item->id ?>" data-badge="<?php echo $stockequipo;?>" <?php if($stockequipo==0){ ?> onclick="alertastockequipo(<?php echo $item->id ?>,<?php echo $stockequipo;?>)" <?php }else{ ?> 
                                                    onmouseover="bodegasequipo(<?php echo $item->id ?>)" 
                                                    data-equipo="<?php echo $item->id ?>"
                                                  <?php } ?> >
                                                  <?php echo $item->modelo ?>
                                                </label>
                                              </td>
                                              <td class="equipo_consumibleselected equipo_c_<?php echo $item->id ?>"></td>
                                              <td class="equipo_accessoriosselected equipo_s_<?php echo $item->id ?>"></td>
                                              <td class="equipo_rendimientoselected equipo_r_<?php echo $item->id ?>"></td> 
                                              <td style="width: 5%;"><a class="btn btn-info"  style="padding: 0 5px;" onclick="addaccesorios(<?php echo $item->id ?>)"><i class="material-icons">list</i></a></td>
                                            </tr>
                                          <?php } ?>
                                        </tbody>
                                      </table>
                                    </div> 
                                </div>
                                
                            </li>
                            <li class="collapsible_venta_c" style="display: none;">
                                <div class="collapsible-header">Consumibles</div>
                                <div class="collapsible-body">
                                    <div class="row">
                                      <div class="col s9"></div>
                                      <div class="col s3">
                                            <a class="waves-effect waves-light purple lightrn-1 btn-bmz tooltipped" data-position="top" data-tooltip="Nuevo" id="botonAgregarConsumible" name="botonAgregarConsumible" style="float: right;">
                                              agregar otro consumible
                                            </a>
                                          
                                      </div>
                                    </div>
                                    <div class="row rowConsumibles" id="rowConsumibles1" name="rowConsumibles1">
                                        <div class="input-field col s1">
                                          <a class="btn-bmz material-icons prefix" id="addh_consumible_equipo" onClick="addh_consumible_equipo(1)" title="Equipos registrados" style="margin-top: -6px;padding: 0px;">format_list_bulleted</a>
                                        </div>
                                        <div class="col s2" id="selectEquiposConsumibles">
                                                <label>Equipos</label>
                                                <select id="equipoListaConsumibles" name="equipoListaConsumibles" class="browser-default chosen-select equipoListaConsumibles" onchange="funcionConsumibles($(this))">
                                                  <option value="" disabled selected></option>
                                                  <option value="0">General</option>
                                                  <?php foreach($equipos as $equipo) { ?>
                                                      <option value="<?php echo $equipo->id;?>" >
                                                          <?php echo $equipo->modelo;?>
                                                      </option>
                                                  <?php } ?>
                                              </select>
                                            
                                        </div>
                                        <div class="col s2">
                                          
                                              <label>Bodega</label> 
                                              <select id="bodegacon" name="bodegacon" class="browser-default  selectbodegacon form-control-bmz" onchange="funcionConsumibles($(this))">
                                                <option value="0">Seleccione</option>
                                                <?php foreach($restbodegas as $bodegas){?>
                                                    <option value="<?php echo $bodegas->bodegaId;?>"  class="tipo_v_selected_<?php echo $bodegas->tipov;?>">
                                                        <?php echo $bodegas->bodega;?>
                                                    </option>
                                                <?php } ?>
                                              </select>
                                        </div>
                                        <div class="col s2" id="selectToner">
                                            <label>Disponibles</label>
                                            <select id="toner" name="toner" class="browser-default chosen-select toner" onchange="funcionConsumiblesprecios($(this))">
                                              <option value="" disabled selected>Selecciona una opción</option>
                                            </select>
                                          
                                        </div> 
                                        <div class="col s3" id="selectTonerp">
                                          <div class="col s7 cprecio0" >  
                                            <label>Precios</label>
                                            <div class="div_tonerp">
                                            <select id="tonerp" name="tonerp" class="browser-default form-control-bmz tonerp" onchange="funcionConsumiblespreciosv($(this))">
                                              <option value="" disabled selected>Selecciona una opción</option>
                                            </select>
                                            </div>
                                          </div>
                                          <div class="col s5 cprecio1" >
                                            <label class="cprecio1_label" style="display:none;">Precio</label>
                                            <label style="color:transparent;">Precios</label>
                                            <input type="number" name="" min="0" class="precionew form-control-bmz"  style="text-align: center;" onclick="precionewreadonly($(this))" value="0" readonly>
                                          </div> 
                                        </div>
                                        <div class="input-field col s1" id="inputCantidad" style="display: none;">
                                            <input type="number" name="" min="1" max="5" class="piezas form-control-bmz piezas_0 " value="1" onchange="funcionConsumiblesprecioscantidad($(this))">
                                            <label for="estado">Cantidad</label>
                                        </div>

                                        
                                        <div class="input-field col s1">
                                          
                                            <a class="btn-floating waves-effect waves-light red lightrn-1 btn tooltipped botonEliminarConsumible" data-position="top" data-tooltip="Eliminar" id="botonEliminarConsumible" name="botonEliminarConsumible">
                                              <i class="material-icons">delete</i>
                                            </a>
                                          
                                        </div>
                                      </div>
                                </div>
                            </li>
                            <li class="collapsible_venta_r" style="display: none;">
                                <div class="collapsible-header">Refacciones</div>
                                <div class="collapsible-body">
                                    <!--
                                    <div class="row rowRefacciones" id="rowRefacciones1" name="rowRefacciones1">
                                      <div class="input-field col s4" style="display: none;" id="selectEquiposRefacciones">
                                          <div class="col s5" style="margin-left: -12px;">   
                                              <i class="material-icons prefix">format_list_bulleted</i>
                                              <label for="estado">Equipos</label>
                                          </div>
                                          <div class="col s7 margenTopDivChosen" > 
                                            <select id="equipoListaRefacciones" name="equipoListaRefacciones" class="browser-default chosen-select equipoListaRefacciones" onchange="funcionRefacciones($(this))">
                                                <option value="" disabled selected></option>
                                                <?php foreach($equipos as $equipo) { ?>
                                                    <option value="<?php echo $equipo->id;?>" >
                                                        <?php echo $equipo->modelo;?>
                                                    </option>
                                                <?php } ?>
                                            </select>
                                          </div>
                                      </div>
                                      
                                      <div class="input-field col s4" style="display: none;" id="selectRefaccion">

                                        <div class="col s5" style="margin-left: -12px;">  
                                          <i class="material-icons prefix">person</i>
                                          <label for="estado">Disponibles</label>
                                        </div>

                                        <div class="col s7 margenTopDivChosen" >
                                          <select id="refaccion" name="refaccion" class="browser-default chosen-select refaccion">
                                            <option value="" disabled selected>Selecciona una opción</option>
                                          </select>
                                        </div> 

                                      </div> 

                                      <div class="input-field col s4" id="inputCantidadRefaccion" style="display: none;">
                                        <div class="col s10" style="margin-left: -12px;">  

                                         <i class="material-icons prefix">format_list_bulleted</i>
                                            <input type="number" name="piezasRefacion" min="1" max="5" class="piezasRefacion" id="piezasRefacion">
                                          <label for="estado">Cantidad</label>
                                        </div>
                                      
                                      <div class="col s1" >
                                          <a class="btn-floating waves-effect waves-light purple lightrn-1 btn tooltipped" data-position="top" data-tooltip="Nuevo" id="botonAgregarRefaccion" name="botonAgregarRefaccion">
                                            <i class="material-icons">add</i>
                                          </a>
                                        </div>  
                                        <div class="col s1" >
                                          <a class="btn-floating waves-effect waves-light red lightrn-1 btn tooltipped botonEliminarRefaccion" data-position="top" data-tooltip="Eliminar" id="botonEliminarRefaccion" name="botonEliminarRefaccion">
                                            <i class="material-icons">delete</i>
                                          </a>
                                        </div>

                                        </div>
                                    </div>-->
                                    <div class="row">
                                      <div class="col s2">  
                                          <a class="btn-bmz material-icons prefix" onClick="addh_refacciones_cotizacion()" title="Equipos registrados">format_list_bulleted</a>
                                          <label for="estado">Equipos</label> 
                                          <br>
                                          <select id="equipoListarefacciones" name="equipoListarefacciones" class="browser-default chosen-select equipoListarefacciones" onchange="selectequipor()">
                                              <option value="" disabled selected></option>
                                              <?php foreach($equipos as $equipo){?>
                                                  <option value="<?php echo $equipo->id;?>" >
                                                      <?php echo $equipo->modelo;?>
                                                  </option>
                                              <?php } ?>
                                          </select>
                                      </div>
                                      <div class="col s2">  
                                          <label for="estado" style="transform: scale(1) !important;">Serie</label>
                                          <input type="text" id="seriee" name="seriee" class="form-control-bmz" >
                                      </div>
                                      <div class="col s2">  
                                          <label for="bodegar">Bodega</label> 
                                          <br>
                                          <select id="bodegar" name="bodegar" class="browser-default equipoListarefacciones form-control-bmz" onchange="selectequipor()">
                                              <option value="0">Seleccione</option>
                                              <?php foreach($restbodegas as $bodegas){?>
                                                  <option value="<?php echo $bodegas->bodegaId;?>" class="tipo_v_selected_<?php echo $bodegas->tipov;?>">
                                                      <?php echo $bodegas->bodega;?>
                                                  </option>
                                              <?php } ?>
                                          </select>
                                      </div>
                                      <div class="col s2">  
                                          <label for="estado">Disponibles</label> 
                                          <br>
                                          <div class="refacciones_mostrar"></div>
                                      </div>
                                      <div class="col s2">  
                                          <label for="estado">Precios</label> 
                                          <br>
                                          <div class="refaccionesp_mostrar"></div>
                                      </div>
                                      <div class="col s1">  
                                          <label for="estado" style="transform: scale(1) !important;">Cantidad</label>
                                          <br>
                                          <input type="number" id="piezasr" name="piezasr" class="form-control-bmz" min="1" max="5" onchange="validarcantidadstockr($(this))">
                                      </div>
                                      
                                      <div class="input-field col s1">
                                        
                                        <a class="btn-bmz waves-effect waves-light purple lightrn-1 btn tooltipped" data-position="top" data-tooltip="Agregar" id="botonAgregarConsumible" name="botonAgregarConsumible" onclick="addrefaccion()">Agregar<i class="material-icons">system_update_alt</i></a>
                                      </div>
                                  </div>  
                                  <div class="row">
                                     <div class="col s12 m12 12 equipotableconsumicle">
                                          <table class="col s12 m12 12 tablerefacciones Highlight striped"><tbody></tbody></table>
                                      </div>
                                  </div> 

                                </div>
                            </li>
                            <li class="collapsible_venta_p" style="display: none;">
                                <div class="collapsible-header">Servicios</div>
                                <div class="collapsible-body">
                                  <div class="row rowPolizas" id="rowPolizas1" name="rowPolizas1">
                                    <div class="input-field col s6" id="selectPolizas" style="display: none;">
                                        <div class="col s5" style="margin-left: -12px;">   
                                          <i class="material-icons prefix">format_list_bulleted</i>
                                          <label for="estado">Pólizas Existentes</label>
                                        </div>
                                        <div class="col s7 margenTopDivChosen" >
                                          <select id="polizasLista" name="polizasLista" class="browser-default chosen-select polizasLista" onchange="funcionDetalles($(this))">
                                              <option value="" disabled selected></option>
                                              <?php foreach($polizas as $poliza) { 
                                                echo '<option value="'.$poliza->id.'" data-visitas="'.$poliza->vigencia_clicks.'">'.$poliza->nombre.'</option>';
                                                 } ?>
                                          </select>
                                        </div>
                                    </div>        
                                  </div>
                                  <div class="row addpolizass" style="display: none;">
                                    <a class="btn waves-effect waves-light cyan" id="btn_add_equipo_poliza">Agregar equipo</a>
                                  </div> 
                                  <div class="row addpolizas"></div>
                                </div>
                            </li>
                            <li class="collapsible_venta_a" style="display: none;">
                                <div class="collapsible-header">Accesorios</div>
                                <div class="collapsible-body">
                                  <div class="row">
                                    <div class="col s1">
                                      <a class="btn-bmz material-icons prefix" id="addh_acc_equipo" onClick="addh_accesorios_equipo()" title="Equipos registrados" style="margin-top: -6px;padding: 0px;font-size: 33px;">format_list_bulleted</a>
                                    </div>
                                    <div class="col s11">
                                      <table class="table" id="table_venta_accesorios">
                                        <thead>
                                          <tr>
                                            <th>Equipo</th>
                                            <th>Bodega</th>
                                            <th>Accesorio</th>
                                            <th>Precio</th>
                                            <th>Cantidad</th>
                                            
                                            <th></th>
                                          </tr>
                                          <tr>
                                            <th>
                                              <select id="equipo_acce" class="browser-default chosen-select" onchange="obtenerabsesorios()">
                                                  <option value="" disabled selected></option>
                                                  <?php foreach($equipos as $equipo) { ?>
                                                      <option value="<?php echo $equipo->id;?>" >
                                                          <?php echo $equipo->modelo;?>
                                                      </option>
                                                  <?php } ?>
                                              </select>
                                            </th>
                                            <th>
                                              <select id="bodega_acce" class="browser-default form-control-bmz" onchange="obtenerabsesorios()">
                                                  <option value="0">Seleccione</option>
                                                  <?php foreach($restbodegas as $bodegas){?>
                                                      <option value="<?php echo $bodegas->bodegaId;?>" class="tipo_v_selected_<?php echo $bodegas->tipov;?>">
                                                          <?php echo $bodegas->bodega;?>
                                                      </option>
                                                  <?php } ?>
                                              </select>
                                            </th>
                                            <th>
                                              <select id="accesorio_acce" class="browser-default chosen-select" onchange="obtenerabsesorioscosto()">
                                                  
                                              </select>
                                            </th>
                                            <th>
                                              <input type="number" id="precio_acce" class="form-control-bmz" readonly>
                                            </th>
                                            <th>
                                              <input type="number" id="cantidad_acc" class="form-control-bmz">
                                            </th>
                                            
                                            <th>
                                              <a class="btn-floating waves-effect waves-light purple lightrn-1 btn tooltipped" data-position="top" data-tooltip="Agregar" id="botonAgregarConsumible" name="botonAgregarConsumible" onclick="addaccesoriov()">
                                                <i class="material-icons">system_update_alt</i>
                                              </a>
                                            </th>
                                          </tr>
                                        </thead>
                                        <tbody class="tbody_acce"></tbody>
                                      </table>
                                    </div>
                                  </div>
                                </div>
                            </li>
                          </ul>
                        </div>
                        <!---------------------->
                        
                      <div class="row">
                        <div class="input-field col s6" id="selectEquipos" style="display: none;">
                          <div class="col s5" style="margin-left: -12px;">   
                            <i class="material-icons prefix">format_list_bulleted</i>
                            <label for="estado">Equipos</label>
                          </div>
                          <div class="col s7 margenTopDivChosen" >
                            <select id="equipoLista" name="equipoLista" class="browser-default chosen-select">
                                <option value="" disabled selected></option>
                                <?php foreach($equipos as $equipo) { ?>
                                    <option value="<?php echo $equipo->id;?>" >
                                        <?php echo $equipo->modelo;?>
                                    </option>
                                <?php } ?>
                            </select>
                          </div>
                        </div>
                              
                      </div>
                      <br>
                      
                      <input type="hidden" name="idCliente" id="idCliente" value="<?php echo $idCliente; ?>">
                    </form>

                    <br><br>
                    <div class="row center fixed-action-btn" style="bottom: 50px; right: 19px;">
                      <button class="btn waves-effect waves-light cyan" id="btn_venta">Aceptar</button>
                    </div>
                    <br><br>

                  </div>
                
                </div>

              </div>

          </div>
        
      </div>
  </section>
<div id="modalaccesorios" class="modal">
  <div class="modal-content ">
    
    <div class="row"> 
      <div class="col s12">
        <ul class="tabs">
          <li class="tab col s3"><a class="active" href="#test1">Accesorios</a></li>
          <li class="tab col s3"><a  href="#test2">Consumibles</a></li>
          <li class="tab col s3 solopararentas" style="display: none;"><a  href="#test3">Condiciones</a></li>
        </ul>
      </div>
      <div id="test1" class="col s12 modaltableaccesorios"></div>
      <div id="test2" class="col s12 modaltableconsu"></div>
      <div id="test3" class="col s12 modaltablerendimientos">
        <div class="row">
          <div class="col s4">
            <label>Volumen monocromático</label>
            <input type="text" id="volumen_monocromatico" value="0">
          </div>
          <div class="col s1" style="margin-top: 25px;">
            X
          </div>
          <div class="col s3">
            <label>Costo</label>
            <input type="text" id="costo_monocromatico" class="form-control-b" value="0.25" readonly>
          </div>
          <div class="col s1" style="margin-top: 25px;">
            =
          </div>
          <div class="col s2">
            <label>Total</label>
            
            <div class="input-group-b">
             <span class="input-group-btn-b">
               <button class="btn-b btn-primary-b bootstrap-touchspin-down" type="button">$</button>
             </span>
             <input type="text" id="total_monocromatico" class="form-control-b" value="0.00" readonly> 
            </div> 
          </div>
        </div>
        <div class="row">
          <div class="col s4">
            <label>Volumen monocromático</label>
            <input type="text" id="volumen_color" value="0">
          </div>
          <div class="col s1" style="margin-top: 25px;">
            X
          </div>
          <div class="col s3">
            <label>Costo</label>
            <input type="text" id="costo_color" class="form-control-b" value="2.5" readonly>
          </div>
          <div class="col s1" style="margin-top: 25px;">
            =
          </div>
          <div class="col s2">
            <label>Total</label>
            <div class="input-group-b">
             <span class="input-group-btn-b">
               <button class="btn-b btn-primary-b bootstrap-touchspin-down" type="button">$</button>
             </span>
             <input type="text" id="total_color" class="form-control-b" value="0.00" readonly> 
            </div> 
            
          </div>
        </div>
        <div class="row">
          <input type="hidden" id="tiporendimientoselectede">
          <input type="hidden" id="tiporendimientoselectedm">
          <input type="hidden" id="tiporendimientoselectedc">
          <button  class="waves-effect waves-green green btn-flat addtabuladorrendimiento">Aceptar</button>
        </div>
      </div>
                    
    </div>                   
  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>    
  </div>
</div>
<div id="modalupdateventa" class="modal" data-backdrop="static" data-keyboard="false">
  <div class="modal-content ">
    <div class="row">
      <div class="col s12 m12 12 venta_detalles_view"></div>                
    </div>
    <div class="row ventaeservicio_fe">
      <div class="col s3 m3 3">
        <label>Fecha Entrega</label>
        <input type="date" id="ventaeservicio_fe" class="form-control-bmz" value="<?php echo date("Y-m-d",strtotime(date("Y-m-d")."+ 1 days"))?>">
      </div>
    </div>
    <div class="row ventaeservicio" style="display:none;">
        <div class="col s3 m3 l3">
          <label for="tpoliza2e">Póliza</label>
          <select id="tpoliza2e" class="browser-default chosen-select form-control-bmz" onchange="obtenerservicios2e();">
            <?php foreach ($resultpolizas->result() as $item) { ?>
                <option value="<?php echo $item->id;?>"><?php echo $item->nombre;?></option>
            <?php } ?>
          </select>
        </div>
        <div class="col s3 m3 l3">
          <label for="tservicio2e">Equipo / Servicio</label>
            <select id="tservicio2e" class="browser-default chosen-select form-control-bmz">
              
            </select>
        </div>
        <div class="col s4 m4 l4">
          <label for="tdireccion2e">Dirección</label>
          <select id="tdireccion2e" name="tdireccion2" class="browser-default  form-control-bmz">
            
          </select>
        </div>
        <div class="col s2 m2 l2">
          <label class="ttipo2e">Tipo Servicio</label>
          <select class="browser-default  form-control-bmz" name="ttipo22e" id="ttipo22e">
            <option value="1">Local</option>
            <option value="2">Semi</option>
            <option value="3">Foráneo</option>
            <option value="4">Especial</option>
          </select>
        </div>
    </div>
    <div class="row ventaeservicio" style="display:none;">
          <label for="tserviciomotivo2e">Motivo del Servicio</label>
          <textarea id="tserviciomotivo2e"></textarea>
        
    </div>                    
  </div>
  <div class="modal-footer">
    <!--<a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>-->
    <a href="#!" class="modal-action  waves-effect waves-green green btn-flat aceptareditarventa ">Aceptar</a>
  </div>
</div>
<div id="modalupdateventarentas" class="modal">
  <div class="modal-content ">
    <div class="row">
      <div class="col s12 m12 12 venta_detalles_view_renta"></div>                     
    </div>                   
  </div>
  <div class="modal-footer">
    <!--<a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>-->
    <a href="#!" class="modal-action  waves-effect waves-green green btn-flat aceptareditarventarenta ">Aceptar</a>
  </div>
</div>
<div id="modaleditor" class="modal" style="z-index: 1003; display: none; opacity: 0; transform: scaleX(0.7); top: 4%;">
  <div class="modal-content ">
    <h4>Editar Precio</h4>
    <div class="border_datos" id="fiscales_dato1">
      <div class="col s24 m24 l12">
        <div class="row ">
            <div class="row">
              <input id="idEquipon" type="hidden" value="185">
              <div class="input-field col s12">
                <input id="password" type="password" class="validate" autocomplete="new-password" required="">
                <label for="password">Password</label>
              </div>
            </div>                                        
        </div>                          
      </div>
    </div>                    
  </div>
  <div class="modal-footer">
    <button href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</button>
    <button href="#!" class="modal-action modal-close waves-effect waves-green green btn-flat editarcosto">Aceptar</button>
  </div>
</div>
<div id="modalconsumibleservicio" class="modal">
  <div class="modal-content ">
    <div class="row ocultarsolicitudservicio">
      <div class="col s12 m12 12">
        <input type="hidden" id="ventaservicioc">
        <div class="switch">
          <label style="font-size: 20px;display: none;">
          ¿Desea incluir instalación y/o servicio o póliza de cortesía?
          <input type="checkbox" id="aceptarcservicio" onchange="aceptarcservicio()">
          <span class="lever"></span>
          </label>
        </div>
      </div>                
    </div>
    <div class="row ventaservicio_fe">
      <div class="col s3 m3 l3">
        <label >Fecha de recolección</label>
        <input type="date" id="ventaservicio_fe" class="form-control-bmz">
      </div>
    </div>
    <div class="row ventaservicio" style="display:none;">
        <div class="col s3 m3 l3">
          <label for="tpoliza2">Póliza</label>
          <select id="tpoliza2" class="browser-default chosen-select form-control-bmz" onchange="obtenerservicios2();">
            <?php foreach ($resultpolizas->result() as $item) { ?>
                <option value="<?php echo $item->id;?>"><?php echo $item->nombre;?></option>
            <?php } ?>
          </select>
        </div>
        <div class="col s3 m3 l3">
          <label for="tservicio2">Equipo / Servicio</label>
            <select id="tservicio2" class="browser-default chosen-select form-control-bmz">
              
            </select>
        </div>
        <div class="col s4 m4 l4">
          <label for="tdireccion2">Dirección</label>
          <select id="tdireccion2" name="tdireccion2" class="browser-default  form-control-bmz">
            
          </select>
        </div>
        <div class="col s2 m2 l2">
          <label class="ttipo2">Tipo Servicio</label>
          <select class="browser-default  form-control-bmz" name="ttipo22" id="ttipo22">
            <option value="1">Local</option>
            <option value="2">Semi</option>
            <option value="3">Foráneo</option>
            <option value="4">Especial</option>
          </select>
        </div>
    </div>
    <div class="row ventaservicio" style="display:none;">
          <label for="tserviciomotivo2">Motivo del Servicio</label>
          <textarea id="tserviciomotivo2"></textarea>
        
    </div>                 
  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat redirigirventas">Cerrar</a>
    <a href="#!" class="modal-action  waves-effect waves-green green btn-flat ventaserviciook ">Aceptar</a>
  </div>
</div>
<div id="modalprefacturaspendiestes" class="modal">
  <div class="modal-content ">
    <div class="row">
      <div class="col s12 m12 12">
        <h5>Prefacturas pendientes por completar</h5>
      </div>
      <div class="col s12 m12 12">
        <table>
          <thead>
            <tr>
              <th>Fecha de creación</th>
              <th></th>
            </tr>
          </thead>
          <tbody class="prefacturaslis"></tbody>
        </table>
      </div>                
    </div>                   
  </div>
</div>

<div id="modalserieshistorial" class="modal" style="min-height: 497px;">
  <div class="modal-content " style="padding-top: 10px;">
    <div class="row">
      <div class="col s12 m12 12 table-h-e-s"></div>                
    </div>                 
  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat">Cerrar</a>
  </div>
</div>

<div id="modalrefaccionescotizacion" class="modal" style="min-height: 497px;">
  <div class="modal-content " style="padding-top: 10px;">
    <div class="row">
      <div class="col s12 m12 12 table-h-r-c"></div>                
    </div>                 
  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat">Cerrar</a>
  </div>
</div>


<div id="modalservicioadd" class="modal" style="z-index: 1003; display: none; opacity: 0; transform: scaleX(0.7); top: 4%;">
  <div class="modal-content ">
    <h5>Seleccione servicio existente</h5>
    <div class="row">
      <div class="col s12 servicioaddcliente"></div>
    </div>
  </div>
  <div class="modal-footer">
    <button class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</button>
  </div>
</div>
