<style type="text/css">
  td,th{
    font-size: 12px;
  }
</style>
<!-- START CONTENT -->
    <section id="content">
      <!--start container-->
        <div class="container">
          <div class="section">

          <input id="base_url" type="hidden" name="base_url" value="<?php echo base_url(); ?>">
          <input id="perfilid" type="hidden" name="perfilid" value="<?php echo $_SESSION["perfilid"]; ?>">
          
          <div id="table-datatables">
            
                <h4 class="header">Listado de Precios de Refacciones</h4>
                <div class="row">
                  
                  <div class="col s12">
                    <!--<a class="btn waves-effect waves-light gradient-45deg-green-teal gradient-shadow col s1" href="<?php echo base_url(); ?>index.php/Clientes/alta">Nuevo</a>-->
                
                    <div class="col s2">
                      
                      </div>
                      <div class="form-group col s6"  align="right">
                        
                      </div>

                      <div class="col s3" align="right" style="display:none;">
                        <button type="submit" class="btn waves-effect waves-light purple lightrn-1" id="descargarExcel" name="descargarExcel">Descargar Excel <i class="material-icons">file_download</i></button>
                      </div>
                  </div>

                  <div class="col s12">
                    <br>
                    <table id="tabla_precios" class="responsive-table display" cellspacing="0">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Código</th>
                          <th>Nombre</th>
                          <th>$ Dólares</th>
                          <th>Foráneo / General</th>
                          <th>Iva G</th>
                          <th>Neto G</th>
                          <th>Semi/ Frecuente</th>
                          <th>Iva F</th>
                          <th>Neto F</th>
                          <th>Local/Póliza</th>
                          <th>Iva E</th>
                          <th>Neto E</th>
                          
                          <th>Especial/Clientes"AAA"</th>
                          <th>Iva P</th>
                          <th>Neto P</th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
          </div>  
        </div>

        <div id="modalAcciones" class="modal"  >
          <div class="modal-content ">

            <h4> <i class="material-icons">mode_edit</i> Acciones </h4>

            <div class="col s12 m12 l6">
                  <div class="row" align="center">
                      <ul>
                        <li>
                          <a class="btn-floating green tooltipped edit" data-position="top" data-delay="50" data-tooltip="Editar" id="lista_editar">
                              <i class="material-icons">mode_edit</i>
                          </a>
                          <a class="btn-floating blue tooltipped visualiza" data-position="top" data-delay="50" data-tooltip="Visualizar" id="lista_visualizar">
                              <i class="material-icons">remove_red_eye</i>
                          </a>
                          <a class="btn-floating red tooltipped elimina" data-position="top" data-delay="50" data-tooltip="Eliminar" id="lista_eliminar">
                              <i class="material-icons">delete_forever</i>
                          </a>
                          <a class="btn-floating orange tooltipped llamadas" data-position="top" data-delay="50" data-tooltip="Agenda Llamada" id="lista_llamadas">
                              <i class="material-icons">phone_in_talk</i>
                          </a>
                          <a class="btn-floating pink tooltipped correos" data-position="top" data-delay="50" data-tooltip="Agendar Correo" id="lista_correos">
                              <i class="material-icons">email</i>
                          </a>
                          <a class="btn-floating amber tooltipped visitas" data-position="top" data-delay="50" data-tooltip="Agendar Visita" id="lista_visitas">
                              <i class="material-icons">add_alert</i>
                          </a>
                          <a class="btn-floating cyan tooltipped agenda" data-position="top" data-delay="50" data-tooltip="Revisar Agenda" id="lista_agenda">
                              <i class="material-icons">date_range</i>
                          </a>
                        </li>
                      </ul>
                      <input type="hidden" name="idClienteModal" id="idClienteModal">
                  </div>
              </div>
              
          <div class="modal-footer">
            <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
          </div>
        </div>
     </div>
     
    </section>