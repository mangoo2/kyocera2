<?php 
if (isset($_GET['id'])) {
    $id=$_GET['id'];
    $personalv=$this->ModeloPersonal->personalview($id);
    foreach ($personalv->result() as $item){
      $id=$item->personalId;
      $nom=$item->nombre;
      $sex=$item->sexo;
      $email=$item->Email;
      $sucu=$item->sucursalId;
      /*
      $apell=$item->Apellidos;
      
      $cel=$item->Celular;
      $tel=$item->TelefonoCasa;
      $calle=$item->Calle;
      $NoInterior=$item->NoInterior;
      $NoExterior=$item->NoExterior;
      $Estado=$item->Estado;
      $Municipio=$item->Municipio;
      $Colonia=$item->Colonia;
      $CP=$item->CP;
      */
    }
}else{
  $id='';
  $nom='';
  $sex=1; 
  $email='';       
  $sucu='';     
} 
?>
<div class="row">
                <div class="col-md-12">
                  <h2>Personal</h2>
                </div>
              </div>
              <!--Statistics cards Ends-->
              <!--Line with Area Chart 1 Starts-->
              <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Personal</h4>
                        </div>
                        <div class="card-body">
                            <div class="card-block form-horizontal">
                                <!--------//////////////-------->
                                <form method="post" id="frmPersonal" role="form" action="../Personal">
              <!------------------------------------------------------>
              <div class="col-md-12">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Nombre</label>
                    <div class="col-sm-7 controls">
                        <div class="input-group"> <span class="input-group-addon  vd_bg-blue vd_white vd_bdt-blue"><i class="fa fa-user"></i></span>
                          <input type="text" placeholder="Username" id="nombre" name="nombre" value="<?php echo $nom; ?>" class="form-control">
                          <input type="text" placeholder="Username" id="persolnalid" value="<?php echo $id; ?>" hidden>

                        </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Sexo</label>
                    <div class="col-sm-7 controls">
                        <div class="input-group"> <span class="input-group-addon  vd_bg-blue vd_white vd_bdt-blue"><i class="fa fa-user"></i></span>
                          <select id="sexo" name="sexo" class="form-control">
                            <option value="1" <?php if($sex==1){echo "selected";} ?> >Masculino</option>
                            <option value="2" <?php if($sex==2){echo "selected";} ?>>Femenino</option>
                          </select>

                        </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Correo</label>
                    <div class="col-sm-7 controls">
                        <div class="input-group"> <span class="input-group-addon  vd_bg-blue vd_white vd_bdt-blue">@</span>
                          <input type="email" placeholder="ejemplo@ejemplo.com" id="email" name="email" value="<?php echo $email; ?>" class="form-control">
                        </div>
                    </div>
                  </div>
                  
                  
                </div>
              </div>
              
                     
              <div class="col-md-12">
                <hr/>
              </div>
            	<div class="col-md-12">
            		<div class="col-md-9">
            			
            		</div>
            		<div class="col-md-3">
            			<button class="btn vd_btn vd_bg-blue"  type="submit"  id="savep">Guardar</button>
            		</div>
            	</div>		
              <!------------------------------------------------------>
            </form>






   					</div>
   				</div>
   			</div>
   		</div>
   	</div>
	