<style type="text/css">
#tabla_accesorios td{font-size: 12px;}
<?php if($this->perfilid!=1){ ?>
.soloadministradores{display: none;}
<?php } ?>
.card_img{width: 200px;float: left;border: solid #dddddd;}
.card_img img{width: 100%;/*max-height: 200px;*/  }
.card_img_c{height: 200px;}
.card_img_a{text-align: center;}

.star {visibility:hidden;font-size:30px;cursor:pointer;margin-top: 15px;}
.star:before {content: "\2605";position: absolute;visibility:visible;}
.star:checked:before {content: "\2605";position: absolute;color: #ffeb3b;}
.star_f {visibility:hidden;font-size:30px;cursor:pointer;margin-top: 5px;}
.star_f:before {content: "\2605";position: absolute;visibility:visible;margin-top: -19px;}
.star_f:checked:before {content: "\2605";position: absolute;color: #ffeb3b;margin-top: -19px;}
[type="checkbox"]:not(:checked), [type="checkbox"]:checked {opacity: unset;pointer-events: all;}
</style>
<!-- START CONTENT -->
<section id="content">
  <!--start container-->
  <div class="container">
    <div class="section">
      <input id="base_url" type="hidden" name="base_url" value="<?php echo base_url(); ?>">
      <div id="table-datatables">
        <br>
        <h4 class="header">Listado de Accesorios</h4>
        <div class="row">
          <div class="col s12 m12 l12">
            <div class="card-panel">
              <div class="row">
                <form class="form soloadministradores" id="catalogos_accesorios_form" method="post">
                  
                  <div class="row">
                    <div class="input-field col s3">
                      <i class="material-icons prefix">build</i>
                      <input id="id" name="id" type="hidden" value="0">
                      <input id="nombre" name="nombre" type="text" placeholder=" ">
                      <label for="nombre">Nombre</label>
                    </div>
                    <div class="input-field col s2">
                      <i class="material-icons prefix">format_list_numbered</i>
                      <input id="no_parte" name="no_parte" type="text" onchange="validar_accesorio()" placeholder=" ">
                      <label for="no_parte" class="active">No Parte</label>
                    </div>
                    <div class="input-field col s2" style="display: none;">
                      <i class="material-icons prefix">format_list_numbered</i>
                      <input id="stock" name="stock" type="text" >
                      <label for="stock" class="active">Stock</label>
                    </div>
                    <div class="input-field col s2">
                      <i class="material-icons prefix">attach_money</i>
                      <input id="costo" name="costo" type="number" step="any" placeholder=" ">
                      <label for="costo">Costo</label>
                    </div>
                    <div class="input-field col s3">
                      <i class="material-icons prefix">format_list_numbered</i>
                      <input id="observaciones" name="observaciones" type="text" placeholder=" ">
                      <label for="costo">Observaciones/Comentarios</label>
                    </div>
                    <div class=" col s1 ">
                      <button type="submit" class="btn-bmz waves-effect waves-light cyan addaccesorios">Guardar</button>
                    </div>
                    <div class=" col s1 divaddaccesorios">
                    </div>
                  </div>
                </form>
                <div class="row">
                  <div class="col s12" >
                    
                    <table id="tabla_accesorios">
                      <thead>
                        <tr>
                          <th> </th>
                          <th>Nombre</th>
                          <th>No Parte</th>
                          <th>Stock</th>
                          <th>Costo</th>
                          <th>Observaciones/Comentarios</th>
                          <th>Destacados</th>
                          <th>Acciones</th>
                        </tr>
                      </thead>
                      <tbody>
                        
                      </tbody>
                    </table>
                  </div>
                </div>
                
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<div id="modalinfoequipo" class="modal">
  <div class="modal-content ">
    <h4 class="equipo_modelo">equipo_modelo</h4>
    
    <div class="row"> 
      <div class="col s12">
        <ul class="tabs">
          <li class="tab col s3"><a class="active" href="#test1">Imágenes</a></li>
          <li class="tab col s3"><a  href="#test2">Características</a></li>
        </ul>
      </div>
      <div id="test1" class="">
        <div class="col s12">
          <input type="file" id="files" name="files[]" multiple accept="image/*"> 
        </div>
        <div class="col s12 galeriimg" style="margin-top: 10px;"></div>
      </div>
      <div id="test2">
        <div >
          <form id="form_catacteristicas">
            <input type="hidden" name="id" id="idfc">
            <input type="hidden" name="idaccesorios" id="idequipofc">
            <div class="col s2">
              <label>Tipo</label>
              <input type="text" name="name" id="name" class="form-control-bmz">
            </div>
            <div class="col s6">
              <label>Descripción</label>
              <textarea name="descripcion" id="descripcion" class="form-control-bmz" style="min-height: 70px;"></textarea>
            </div>
            <div class="col s2">
              <input type="checkbox" class="filled-in" id="general" name="general" value="1" style="display: none;">
              <label for="general" style="margin-top: 21px;">General</label>
            </div>
            <div class="col s1">
              <label>Orden</label>
              <input type="number" name="orden" id="orden" class="form-control-bmz">
            </div>
            <div class="col s1">
              <a class="btn-bmz waves-effect waves-light gradient-45deg-green-teal gradient-shadow" id="save_fc" style="margin-top: 21px;">Guardar</a>
              <a class="btn-bmz waves-effect waves-light gradient-45deg-red-teal gradient-shadow" id="cancelar_fc" onclick="fc_limpiar()" style="display: none;">Cancelar</a>
            </div>
          </form>
        </div>
        <div class="lis_catacter">
          
        </div>
      </div>       
    </div>                   
  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>    
  </div>
</div>