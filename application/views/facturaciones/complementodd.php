<style type="text/css">
  input{
    background-color: white;
  }
  .error{
    color: red;
  }
  .form-control-bmz .error{
    border: 1px solid #ea1e09 !important;
  }
  .IdDocumentos,.facturasuuid{
    font-size: 13px !important;
  }
  .intermitente{
    border: 1px solid black;
    padding: 20px 20px;
    box-shadow: 0px 0px 20px;
    animation: infinite resplandorAnimation 2s;
    
  }
@keyframes resplandorAnimation {
  0%,100%{
    box-shadow: 0px 0px 20px red;
  }
  50%{
  box-shadow: 0px 0px 0px red;
  
  }

}
</style>
<input type="hidden" id="idclienteregresa" value="<?php echo $cliente;?>">
<section id="content" width="100%">
  
  <!--start container-->
  <div class="container" width="100%">
    <div class="section">
      <div class="row">
        <p class="caption col s6">Capturar complemento</p>
      </div>
      <div class="row">
        <!-- FORMULARIO -->
        <form id="validateSubmitForm" method="post" autocomplete="off"> 
          <input type="hidden" name="clienteId" id="clienteId" class="form-control-bmz" value="<?php echo $cliente;?>" readonly required>
          <ul class="collapsible" data-collapsible="accordion" style="margin: 0px;">
            <li >
              <div class="collapsible-header ">Datos del emisor</div>
              <div class="collapsible-body" >
                <div class="row">
                  <div class="col s6">
                   <label>Nombre o razón social:</label>
                   <input type="text" name="razonsocial" id="razonsocial" class="form-control-bmz" value="<?php echo $Nombre;?>" readonly required>
                  </div>
                  <div class="col s6">
                   <label>RFC:</label>
                   <input type="text" name="rfcemisor" id="rfcemisor" class="form-control-bmz" value="<?php echo $rFCEmisor;?>" readonly required>
                  </div>
                  <div class="col s6">
                    <label>Régimen fiscal: <?php echo $Regimen;?></label>
                    <select id="Regimen" name="Regimen" class="form-control-bmz browser-default">
                    <?php if($Regimen==601){ ?>
                      <option value="601">601 General de Ley Personas Morales</option>
                    <?php } ?>
                    <?php if($Regimen==603){ ?>
                      <option value="603">603 Personas Morales con Fines no Lucrativos</option>
                    <?php } ?>
                    <?php if($Regimen==605){ ?>
                      <option value="605">605 Sueldos y Salarios e Ingresos Asimilados a Salarios</option>
                    <?php } ?>
                    <?php if($Regimen==606){ ?>
                      <option value="606">606 Arrendamiento</option>
                    <?php } ?>
                    <?php if($Regimen==607){ ?>
                      <option value="607">607 Régimen de Enajenación o Adquisición de Bienes</option>
                    <?php } ?>
                    <?php if($Regimen==608){ ?>
                      <option value="608">608 Demás ingresos</option>
                    <?php } ?>
                    <?php if($Regimen==609){ ?>
                      <option value="609">609 Consolidación</option>
                    <?php } ?>
                    <?php if($Regimen==610){ ?>
                      <option value="610">610 Residentes en el Extranjero sin Establecimiento Permanente en México</option>
                    <?php } ?>
                    <?php if($Regimen==611){ ?>
                      <option value="611">611 Ingresos por Dividendos (socios y accionistas)</option>
                    <?php } ?>
                    <?php if($Regimen==612){ ?>
                      <option value="612">612 Personas Físicas con Actividades Empresariales y Profesionales</option>
                    <?php } ?>
                    <?php if($Regimen==614){ ?>
                      <option value="614">614 Ingresos por intereses</option>
                    <?php } ?>
                    <?php if($Regimen==615){ ?><option value="615">615 Régimen de los ingresos por obtención de premios</option>
                    <?php } ?>
                    <?php if($Regimen==616){ ?>
                      <option value="616">616 Sin obligaciones fiscales</option>
                    <?php } ?>
                    <?php if($Regimen==620){ ?>
                      <option value="620">620 Sociedades Cooperativas de Producción que optan por diferir sus ingresos</option>
                    <?php } ?>
                    <?php if($Regimen==621){ ?>
                      <option value="621">621 Incorporación Fiscal</option>
                    <?php } ?>
                    <?php if($Regimen==622){ ?>
                      <option value="622">622 Actividades Agrícolas, Ganaderas, Silvícolas y Pesqueras</option>
                    <?php } ?>
                    <?php if($Regimen==623){ ?>
                      <option value="623">623 Opcional para Grupos de Sociedades</option>
                    <?php } ?>
                    <?php if($Regimen==624){ ?>
                      <option value="624">624 Coordinados</option>
                    <?php } ?>
                    <?php if($Regimen==625){ ?>
                      <option value="625">625 Régimen de las Actividades Empresariales con ingresos a través de Plataformas Tecnológicas</option>
                    <?php } ?>
                    <?php if($Regimen==628){ ?>
                      <option value="628">628 Hidrocarburos</option>
                    <?php } ?>
                    <?php if($Regimen==629){ ?>
                      <option value="629">629 De los Regímenes Fiscales Preferentes y de las Empresas Multinacionales</option>
                      <?php } ?>
                    <?php if($Regimen==630){ ?>
                      <option value="630">630 Enajenación de acciones en bolsa de valores</option>
                    <?php } ?>
                                 
                    </select> 
                  </div>
                  <div class="col s6">
                     <label>Tipo:</label>
                     <select name="tipof" id="tipof" class="form-control-bmz browser-default">
                       <option value="P">P Pago</option>
                     </select>
                  </div>
                </div>
              </div>
            </li>
          </ul>
          <ul class="collapsible" data-collapsible="accordion" style="margin: 0px;">
            <li >
              <div class="collapsible-header ">Datos del Receptor</div>
              <div class="collapsible-body">
                <div class="row">
                  <div class="col s6">
                   <label>Nombre o razón social:</label>
                   <input type="text" name="razonsocialreceptor" id="razonsocialreceptor" class="form-control-bmz" value='<?php echo $razonsocialreceptor;?>' readonly required>
                  </div>
                  <div class="col s6">
                   <label>RFC:</label>
                   <input type="text" name="rfcreceptor" id="rfcreceptor" class="form-control-bmz" value="<?php echo $rfcreceptor;?>" readonly required>
                  </div>
                  <div class="col s6">
                    <label>Uso:</label>
                    <select id="UsoCFDI" name="UsoCFDI" class="form-control-bmz browser-default">
                      <option value="P01">P01 Por definir</option>
                    </select>
                  </div>
                </div>
              </div>
            </li>
          </ul>
          <ul class="collapsible" data-collapsible="accordion" style="margin: 0px;">
            <li class="active">
              <div class="collapsible-header active">Comprobante</div>
              <div class="collapsible-body" style="display: block;">
                <div class="row">
                  <div class="col s4">
                   <label>Fecha y hora de expedición:</label>
                   <input type="datetime-local" name="Fecha" id="Fecha" class="form-control-bmz" value="<?php echo $Fecha;?>" style="background-color: white;"  required readonly>
                  </div>
                  <div class="col s4">
                   <label>Codigo postal:</label>
                   <input type="text" name="LugarExpedicion" id="LugarExpedicion" class="form-control-bmz" value="<?php echo $LugarExpedicion;?>" style="background-color: white;" required>
                  </div>
                  <div class="col s4">
                    <label>Moneda:</label>
                    <select id="Moneda" name="Moneda" class="form-control-bmz browser-default">
                      <option value="XXX">XXX los códigos asignados para las transacciones en que intervenga ninguna moneda</option>
                    </select>
                  </div>
                </div>
                <div class="row">
                  <div class="col s4">
                   <label>Folio:</label>
                   <input type="text" name="Folio" id="Folio" class="form-control-bmz" value="<?php echo $Folio;?>" style="background-color: white;" required>
                  </div>
                  <div class="col s4">
                   <label>Serie:</label>
                   <input type="text" name="Serie" id="Serie" class="form-control-bmz" value="<?php echo $serie;?>" style="background-color: white;" required readonly>
                  </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <input type="checkbox" class="filled-in" id="facturarelacionada" >
                      <label for="facturarelacionada">Factura Relacionada</label>
                    </div>
                </div>
                <div class="form-group row divfacturarelacionada" style="display:none;">
                    <div class="col s4">
                        <label>Tipo Relacion</label>
                        <select class="form-control-bmz browser-default" id="TipoRelacion">
                            <!--
                            <option value="01">01 Nota de crédito de los documentos relacionados</option>
                            <option value="02">02 Nota de débito de los documentos relacionados</option>
                            <option value="03">03 Devolución de mercancía sobre facturas o traslados previos</option>
                            -->
                            <option value="04">04 Sustitución de los CFDI previos</option>
                            <!--
                            <option value="05">05 Traslados de mercancias facturados previamente</option>
                            <option value="06">06 Factura generada por los traslados previos</option>
                            <option value="07">07 CFDI por aplicación de anticipo</option>
                            -->
                        </select>
                    </div>
                    <div class="col s4">
                        <label>Folio Fiscal</label>
                        <input type="text" id="uuid_r" class="form-control-bmz" placeholder="111AAA1A-1AA1-1A11-11A1-11A1AA111A11">
                    </div>
                </div>
              </div>
            </li>
          </ul>
          <ul class="collapsible" data-collapsible="accordion" style="margin: 0px;">
            <li class="active">
              <div class="collapsible-header active">Recepción de pagos</div>
              <div class="collapsible-body" style="display: block;">
                <div class="row">
                  <div class="col s4">
                   <label>Fecha y hora de pago *:</label>
                   <input type="datetime-local" name="Fechatimbre" id="Fechatimbre" class="form-control-bmz" value="" style="background-color: white;" required>
                  </div>
                  <div class="col s4">
                    <label>Forma de pago *:</label>
                    <select id="FormaDePagoP" name="FormaDePagoP" class="form-control-bmz browser-default" required>
                      <option></option>
                      <?php foreach ($forma as $key) { ?>
                        <option value="<?php echo str_pad($key->id, 2, "0", STR_PAD_LEFT); ?>" <?php if($key->id==$formapago){echo 'selected';} ?> ><?php echo $key->formapago_text; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                  <div class="col s4">
                   <label>Moneda *:</label>
                   <select id="ModedaP" name="ModedaP" class="form-control-bmz browser-default">
                     <option value="MXN">MXN Peso Mexicano</option>
                   </select>
                  </div>
                </div>
                <div class="row">
                  <div class="col s4">
                   <label>Monto *:</label>
                   <input type="number" name="Monto" id="Monto" class="form-control-bmz" value="" style="background-color: white;" required>
                  </div>
                  <div class="col s4">
                   <label>Total de Importes *:</label>
                   <div class="totalimport"></div>
                  </div>
                </div>
                <div class="row">
                  <div class="col s4">
                   <label>Número de operación:</label>
                   <input type="text" name="NumOperacion" id="NumOperacion" class="form-control-bmz" onpaste="return false;" style="background-color: white;" >
                  </div>
                  <div class="col s4">
                   <label>Cuenta Beneficiario:</label>
                   <input type="text" name="CtaBeneficiario" id="CtaBeneficiario" class="form-control-bmz" value="" style="background-color: white;" >
                  </div>
                </div>

                
              </div>
            </li>
          </ul>
          <ul class="collapsible" data-collapsible="accordion" style="margin: 0px;">
            <li class="active">
              <div class="collapsible-header active">Documentos relacionados</div>
              <div class="collapsible-body" style="display: block;">
                <div class="row">
                  <div class="col s12">
                    <a class="btn-bmz waves-effect waves-light cyan" onclick="adddocumento(<?php echo $cliente.','.$Folio ?>)">Agregar nuevo</a>
                  </div>
                  <div class="col s12">
                    <table id="tabledocumentosrelacionados">
                      <tbody class="tabletbodydr">
                        <tr class="documento_add_<?php echo $FacturasId;?>">
                          <td>
                            <div class="col s3">
                              <input type="hidden" id="idfactura" class="form-control-bmz" value="<?php echo $FacturasId;?>"  readonly required>
                              <input type="hidden" id="MetodoDePagoDR" class="form-control-bmz" value="<?php echo $MetodoDePagoDR;?>"  readonly required>
                             <label>Id del documento:</label>
                             <input type="text" id="IdDocumento" class="form-control-bmz IdDocumentos" value="<?php echo $uuid;?>" readonly required>
                            </div>
                            <div class="col s2">
                             <label>Número de parcialidad:</label>
                             <input type="text" id="NumParcialidad" class="form-control-bmz" value="<?php echo $copnum;?>"  required>
                            </div>
                            <div class="col s2">
                             <label>Importe de saldo anterior:</label>
                             <input type="text" id="ImpSaldoAnt" class="form-control-bmz ImpSaldoAnt_<?php echo $Folio;?>" value="<?php echo $saldoanterior;?>" readonly required>
                            </div>
                            <div class="col s2">
                             <label>Importe de Pagado:</label>
                             <input type="text" id="ImpPagado" class="form-control-bmz ImpPagado ImpPagado_<?php echo $Folio;?>" value="0" oninput="calcularsinsoluto(<?php echo $Folio;?>)">
                            </div>
                            <div class="col s2">
                             <label>Importe de Pagado:</label>
                             <input type="text" id="ImpSaldoInsoluto" class="form-control-bmz ImpSaldoInsoluto_<?php echo $Folio;?>" value="<?php echo $saldoanterior;?>" readonly>
                            </div>
                            <div class="col s1">
                            </div>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  
                  
                </div>
                

                
              </div>
            </li>
          </ul>
        </form>
        <input type="hidden" name="totalimportes" id="totalimportes" value="">
        <div class="row center fixed-action-btn" style="bottom: 100px; right: 19px;">
          <a href="<?php echo base_url();?>Facturaslis?clienteid=<?php echo $cliente;?>&cliente=<?php echo $razonsocialreceptor;?>" class="btn-bmz waves-effect waves-light cyan" style="background-color: grey !important;">Regresar</a>
        </div>
        <div class="row center fixed-action-btn" style="bottom: 50px; right: 19px;">
          <a class="btn-bmz waves-effect waves-light cyan" id="btn_savecomplemento_previe">Agregar complemento</a>
        </div>
        
      </div>
    </div>
  </section>
<div id="modaldocumentos" class="modal" >
  <div class="modal-content" style="padding: 5px">
    <div class="row">
      <div class="col s12">
        <h3>Documentos del cliente</h3>
      </div>
    </div>
    <div class="row">
      <div class="col s12 listadodocumentos">
      </div>
    </div>
  </div>  
  <div class="modal-footer">
    <a class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
  </div>
</div>

<style type="text/css">
  .ifrafac{
    width: 100%;
    height: 412px;
    border: 0;
  }
</style>
<div id="modal_previefactura" class="modal modal-fixed-footer" style="width: 80%;">
    <div class="modal-content preview_iframe" style="padding: 0px;">
      <h4>Preview Complemento</h4>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-close waves-effect waves-green btn-flat">Cancelar</a>
      <button href="#!" class="modal-close waves-effect waves-green btn-flat registrofac" id="btn_savecomplemento">Aceptar</button>
    </div>
  </div>