var base_url = $('#base_url').val();
var table_tipo;
var numsuma = 0;
$(document).ready(function() {
    //$('.chosen-select').chosen({width: "100%"});
    table_tipo=$(".tabla_compras").DataTable();
    setTimeout(function(){ 
      datos_compras();
    }, 1000);
    
    $("#inputFile").fileinput({
            showCaption: false,
            showZoom: false,
            //showUpload: false,// quita el boton de upload
            //rtl: true,
            allowedFileExtensions: ["xlsx"],
            browseLabel: 'Seleccionar Excel',
            uploadUrl: base_url+'Compras/cargaexcel',
            maxFilePreviewSize: 5000,
            previewFileIconSettings: {
                'docx': '<i class="fa fa-file-word-o text-primary"></i>',
                'xlsx': '<i class="fas fa-file-excel"></i>',
                'pptx': '<i class="fa fa-file-powerpoint-o text-danger"></i>',
                'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
                'pdf': '<i class="fa fa-file-pdf-o text-danger"></i>',
                'zip': '<i class="fa fa-file-archive-o text-muted"></i>',
            },
            uploadExtraData: function (previewId, index) {
            var info = {
                        compra:$('#id_compra_c_f').val()
                      };
            return info;
          }
        }).on('filebatchuploadcomplete', function(event, files, extra) {
          setTimeout(function(){ 
            $('#modal_series_e_file').modal('close');
                datos_compras();
            }, 2000);
        }).on('filebatchuploadsuccess', function(event, files, extra) {
          setTimeout(function(){ 
            $('#modal_series_e_file').modal('close');
                datos_compras();
            }, 2000);

        }).on('fileuploaded', function(event, files, extra) {
          setTimeout(function(){ 
            $('#modal_series_e_file').modal('close');
                datos_compras();
            }, 2000);
          
        });
    
});
function datos_compras(){
  var fecha = $('#fecha').val();
  var tipoc = $('#tipoc option:selected').val();
  table_tipo.destroy();
  table_tipo=$(".tabla_compras").DataTable({
    pagingType: 'input',
    "bProcessing": true,
    "serverSide": true,
    search: {
                return: true
            }, 
    "ajax": {
       "url": base_url+"index.php/Compras/getDatacompras",
       type: "post",
       "data": {
                "fecha": fecha,
                tipoc:tipoc
        },
        error: function(){
           $("#tabla_compras").css("display","none");
        }
    },
    "columns": [
        {"data": "compraId"},
        {"data": "folio"},
        {"data": "nombre"},
        {"data": null,
            render: function(data,type,row){ 
                var html = "";
                
                return html;
            }
        },
        {"data": "total",
          render: function(data,type,row){ 
            if (row.total>0) {
              var total=Number(row.total).toFixed(2);
            }else{
              var total=0;
            }
            return "$ "+total;
          }
        },
        {"data": "reg"},
        {"data": null,
          render: function(data,type,row){ 
                    var html='';
                   //var html+='<button class="btn-bmz blue gradient-shadow" onclick="modal_series_file('+row.compraId+')">File</button>';
                       html+='<a href="'+base_url+'Compras/reportepdf/'+row.compraId+'" class="btn-bmz blue gradient-shadow" target="_blank"><i class="fas fa-file-pdf"></i></a>';
                       html+='<a onclick="deletecompraall('+row.compraId+')" class="btn-bmz red gradient-shadow">\
                                    <i class="fas fa-trash-alt"></i></a>';
                return html;
            }
        },
    ],
    "order": [[ 0, "desc" ]],
    createdRow:function(row,data,dataIndex){
            $(row).find('td:eq(3)')
                .addClass('seriecompletadas vericaseries_'+data.compraId)
                .attr('data-compra',data.compraId);
    }
  }).on('draw',function(){
        $('.dataTables_filter').append('<i class="fa fa-search buttonsearch" onclick="searchtable_v()"></i>')
        verificarmontosvc();

    });
}
function searchtable_v(){
    var contenedor = document.getElementById('tabla_compras_filter');

    // Selecciona el input dentro del contenedor
    var input = contenedor.querySelector('input.form-control-bmz');

    // Obtén el valor del input
    var searchv = input.value;

    // Muestra el valor en la consola
    console.log(searchv);
    //var searchv =$("input[type=search]").val();
    table_tipo.search(searchv).draw();
}
function verificarmontosvc(){
    var TABLA   = $(".seriecompletadas");
    var DATA  = [];
    TABLA.each(function(){
        item = {};
        item ["idcompra"] = $(this).data('compra');
        DATA.push(item);
    });
    datacompras   = JSON.stringify(DATA);
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Compras/verificarmontocompras",
        data: {
            compras:datacompras
        },
        success:function(data){  
            console.log(data);
            var array = $.parseJSON(data);
            console.log(array);
            array.forEach(function(element) {
                if(element.monto>0){
                  $('.vericaseries_'+element.idcompra).html('<button class="btn red gradient-shadow" onclick="modal_series('+element.idcompra+')" style="font-size: 12px;width: 121px;">En proceso: '+element.monto+'</button>');
                }else{
                  $('.vericaseries_'+element.idcompra).html('<button class="btn blue gradient-shadow" onclick="modal_series_consultar('+element.idcompra+')">Completadas</button>');
                }
            });
        }
    });
}
function modal_series(id) {
    $('.modal').modal({
        dismissible: true
    });
    $('#modal_series_e').modal('open');
    $('#id_compra_c').val(id);
    $.ajax({
      type: "POST",
      url: base_url+"index.php/Compras/modal_series_m",
      data:{id:id},
      async: false,
      success: function(datosucces){

            $('.text_label').html(datosucces);
            $('.chosen').chosen({width: "100%"});
      }
    });
}
function modal_series_file(id){
  $('.modal').modal({
        dismissible: true
    });
    $('#modal_series_e_file').modal('open');
    $('#id_compra_c_f').val(id);
}
function modal_series_consultar(id) {
    $('.modal').modal({
        dismissible: true
    });
    $('#modal_series_c').modal('open');
    $.ajax({
      type: "POST",
      url: base_url+"index.php/Compras/modal_series_m_consul",
      data:{id:id},
      async: false,
      success: function(datosucces){
            $('.class_consultacompra').html(datosucces);
      }
    });
}
// guardado de series de compras
function aceptar_series() {
  $(".aceptar_series").prop("disabled", true);
  setTimeout(function(){ 
      $(".aceptar_series").prop("disabled", false);
  }, 5000);
  var checked=0;
  $("#tabla_produto tbody > tr").each(function(){       
      if($(this).find("input[class*='checkproducto']").is(':checked')){
          var slectp =$(this).find("select[id*='idbodega']").val();
         if(slectp==0){
            checked++;  
         }
      } 
  });
  $("#tabla_accesorio tbody > tr").each(function(){       
      if($(this).find("input[class*='checkproducto']").is(':checked')){
          var slectp =$(this).find("select[id*='idbodega']").val();
         if(slectp==0){
            checked++;  
         }
      } 
  });
  $("#tabla_refaccion tbody > tr").each(function(){       
      if($(this).find("input[class*='checkproducto']").is(':checked')){
          var slectp =$(this).find("select[id*='idbodega']").val();
         if(slectp==0){
            checked++;  
         }
      } 
  });
  console.log(checked);
  if (checked==0) {
    //guardado
    //alert('Guardar');
    var DATAp = [];
    var TABLA = $("#tabla_produto tbody > tr");
        TABLA.each(function(){         
        if($(this).find("input[class*='checkproducto']").is(':checked')){
          item = {};
          item ["checkproducto"] = $(this).find("input[class*='checkproducto']").val();
          item ["serie"] = $(this).find("input[id*='serienew']").val();
          item ["idbodega"] = $(this).find("select[id*='idbodega']").val();
          item ["factura"] = $(this).find("input[id*='factura']").val();
          item ["factura_fecha"] = $(this).find("input[id*='factura_fecha']").val();
          DATAp.push(item);
        }
    });
    arrayequipos = JSON.stringify(DATAp);
    var DATAa = [];
    var TABLA = $("#tabla_accesorio tbody > tr");
        TABLA.each(function(){        
        if($(this).find("input[class*='checkproducto']").is(':checked')){ 
          item = {};
          item ["checkproducto"] = $(this).find("input[class*='checkproducto']").val();
          item ["serie"] = $(this).find("input[id*='serienew']").val();
          item ["idbodega"] = $(this).find("select[id*='idbodega']").val();
          item ["factura"] = $(this).find("input[id*='factura']").val();
          item ["factura_fecha"] = $(this).find("input[id*='factura_fecha']").val();
          DATAa.push(item);
        }
    });
    arrayaccesorios = JSON.stringify(DATAa);    
    var DATAr = [];
    var TABLA = $("#tabla_refaccion tbody > tr");
        TABLA.each(function(){      
        if($(this).find("input[class*='checkproducto']").is(':checked')){    
          item = {};
          item ["checkproducto"] = $(this).find("input[class*='checkproducto']").val();
          item ["serie"] = $(this).find("input[id*='serienew']").val();
          item ["idbodega"] = $(this).find("select[id*='idbodega']").val();
          item ["factura"] = $(this).find("input[id*='factura']").val();
          item ["factura_fecha"] = $(this).find("input[id*='factura_fecha']").val();
          DATAr.push(item);
        }
    });
    arrayrefaccion = JSON.stringify(DATAr);   

    var DATAc = [];
    var TABLA = $(".consumibles_add  > tr");
        TABLA.each(function(){      
          item = {};
          item ["tr"] = $(this).find("input[id*='consumibleaddtr']").val();
          item ["compra"] = $(this).find("input[id*='compraaddtr']").val();
          item ["consumibleId"] = $(this).find("input[id*='consumibleadd']").val();
          if($(this).find("input[id*='consumibleaddcantidad']").val()>0){
            item ["cantidad"] = $(this).find("input[id*='consumibleaddcantidad']").val();
          }else{
            item ["cantidad"] = 0;
          }
          
          item ["cantidad"] = $(this).find("input[id*='consumibleaddcantidad']").val();
          item ["bodegaId"] = $(this).find("select[id*='idbodega'] option:selected").val();
          DATAc.push(item);

    });
    arrayconsumible = JSON.stringify(DATAc); 

    var DATArs = [];
    var TABLA = $(".refaccionss_add  > tr");
        TABLA.each(function(){      
          item = {};
          item ["tr"] = $(this).find("input[id*='refaccionaddtr']").val();
          item ["refaccion"] = $(this).find("input[id*='refaccionadd']").val();
          item ["cantidad"] = $(this).find("input[id*='refaccionaddcantidad']").val();
          item ["bodegaId"] = $(this).find("select[id*='idbodega'] option:selected").val();
          DATArs.push(item);

    });
    arrayrefaccions = JSON.stringify(DATArs); 

    var DATAas = [];
    var TABLA = $(".accesorioss_add  > tr");
        TABLA.each(function(){      
          item = {};
          item ["tr"] = $(this).find("input[id*='refaccionaddtr']").val();
          item ["accesorio"] = $(this).find("input[id*='refaccionadd']").val();
          item ["cantidad"] = $(this).find("input[id*='refaccionaddcantidad']").val();
          item ["bodegaId"] = $(this).find("select[id*='idbodega'] option:selected").val();
          DATAas.push(item);

    });
    arrayaccesorions = JSON.stringify(DATAas);   

    var datos = 'arrayequipos='+arrayequipos+'&arrayaccesorios='+arrayaccesorios+'&arrayrefaccion='+arrayrefaccion+'&arrayconsumible='+arrayconsumible+'&arrayrefaccions='+arrayrefaccions+'&arrayaccesorions='+arrayaccesorions;
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Compras/guardar_series",
        data: datos,
        success: function (response){
                alertfunction('Éxito!','Se guardado correctamente');
                setTimeout(function(){ 
                  datos_compras();
                  $('#modal_series_e').modal('close');
                }, 2000);  
        },
        error: function(response){
            alertfunction('Atención!','Algo salió mal, intente de nuevo o contacte al administrador del sistema'); 
        }
    });
  }else{
    //alerta de que hay un error
    alert('Falta seleccionar bodega');
  }

}
function alertfunction(titlea,contenta){
    $.alert({boxWidth: '30%',useBootstrap: false,title: titlea,theme: 'bootstrap',content: contenta});  
}
var addcantconsu=0;
function addconsumible(id,compra){
  var cantidad=$('#cant_con_'+id).val();
  var entro=$('#ent_con_'+id).val();
  var total=parseFloat(cantidad)-parseFloat(entro);
  var sucursales = $('#idbodeganone').html();
  var addtpv = 0;
    $(".consumibleaddcantidad_"+id).each(function() {
        var vstotal = $(this).val();
        addtpv += Number(vstotal);
    });
  if (total>0) {
    if (total!=addtpv) {
      var consumibleid=$('#consuid_con_'+id).val();
      var consumibletxt=$('#consut_con_'+id).val();
      var addhtml='<tr class="tr_consumible_'+addcantconsu+'">\
                      <td>'+consumibletxt+'\
                        <input type="hidden"\
                            id="consumibleadd"\
                            value="'+consumibleid+'"\
                            readonly/>\
                        <input type="hidden"\
                            id="consumibleaddtr"\
                            value="'+id+'"\
                            readonly/>\
                        <input type="hidden"\
                            id="compraaddtr"\
                            value="'+compra+'"\
                            readonly/>\
                      </td>\
                      <td>\
                          <input type="number"\
                            class="form-control cserienewi consumibleaddcantidad_'+id+' " data-tr="'+addcantconsu+'"" \
                            id="consumibleaddcantidad" onchange="verificarcant(this,'+addcantconsu+','+id+');" />\
                      </td>\
                      <td><select id="idbodega" name="idbodega" class="browser-default chosen">'+sucursales+'</select></td>\
                      <td>\
                      <a onclick="deleteconsuadd('+addcantconsu+')"style="cursor: pointer;"><i class="material-icons" style="color: red;">delete_forever</i></a>\
                      </td>\
                  </tr>';
      $('.consumible_'+id).append(addhtml);
      addcantconsu++;
      //$('.chosen-select').chosen('destroy');
      $('.chosen').chosen({width: "100%"});
      //$('.chosen').trigger("chosen:updated"); 
    }
  }
}
function verificarcant(data,tr,row){
  var cantidad=$('#cant_con_'+row).val();
  var entro=$('#ent_con_'+row).val();
  var total=parseFloat(cantidad)-parseFloat(entro);
  console.log(data);
  var addtp = 0;
    $(".consumibleaddcantidad_"+row).each(function() {
        var vstotal = $(this).val();
        addtp += Number(vstotal);
    });
  if (total==0) {
    $('.tr_consumible_'+tr).remove();
  }else{
    if (addtp>total) {
      var total2=parseFloat(addtp)-parseFloat(total);
      var cantidadinput=$(data).val();
      var newcat=parseFloat(cantidadinput)-parseFloat(total2);
      if (newcat>0) {
        $(data).val(newcat);
      }else{
        $('.tr_consumible_'+tr).remove();
      }
      
    }
  }
  console.log(addtp);
}
function deleteconsuadd(id){
  $('.tr_consumible_'+id).remove();
}
//========================================================
function addrefaccion(id){
  var cantidad=$('#refa_cant_con_'+id).val();
  var entro=$('#refa_ent_con_'+id).val();
  var total=parseFloat(cantidad)-parseFloat(entro);
  var sucursales = $('#idbodeganone').html();
  var addtpv = 0;
    $(".refaccionaddcantidad_"+id).each(function() {
        var vstotal = $(this).val();
        addtpv += Number(vstotal);
    });
  if (total>0) {
    if (total!=addtpv) {
      var refaccionid=$('#refaid_con_'+id).val();
      var refacciontxt=$('#refaccion_con_'+id).val();
      var addhtml='<tr class="tr_refaccion_'+addcantconsu+'">\
                      <td>'+refacciontxt+'\
                        <input type="hidden"\
                            id="refaccionadd"\
                            value="'+refaccionid+'"\
                            readonly/>\
                        <input type="hidden"\
                            id="refaccionaddtr"\
                            value="'+id+'"\
                            readonly/>\
                      </td>\
                      <td>\
                          <input type="number"\
                            class="form-control cserienewi refaccionaddcantidad_'+id+' " data-tr="'+addcantconsu+'"" \
                            id="refaccionaddcantidad" onchange="verificarrcant(this,'+addcantconsu+','+id+');" />\
                      </td>\
                      <td><select id="idbodega" name="idbodega" class="browser-default chosen">'+sucursales+'</select></td>\
                      <td>\
                      <a onclick="deleterefaccionadd('+addcantconsu+')"style="cursor: pointer;"><i class="material-icons" style="color: red;">delete_forever</i></a>\
                      </td>\
                  </tr>';
      $('.refaccion_trss_'+id).append(addhtml);
      addcantconsu++;
      //$('.chosen-select').chosen('destroy');
      $('.chosen').chosen({width: "100%"});
      //$('.chosen').trigger("chosen:updated"); 
    }
  }
}
function addaccesorio(id){
  var cantidad=$('#refa_cant_con_'+id).val();
  var entro=$('#refa_ent_con_'+id).val();
  var total=parseFloat(cantidad)-parseFloat(entro);
  var sucursales = $('#idbodeganone').html();
  var addtpv = 0;
    $(".refaccionaddcantidad_"+id).each(function() {
        var vstotal = $(this).val();
        addtpv += Number(vstotal);
    });
  if (total>0) {
    if (total!=addtpv) {
      var refaccionid=$('#refaid_con_'+id).val();
      var refacciontxt=$('#refaccion_con_'+id).val();
      var addhtml='<tr class="tr_refaccion_'+addcantconsu+'">\
                      <td>'+refacciontxt+'\
                        <input type="hidden"\
                            id="refaccionadd"\
                            value="'+refaccionid+'"\
                            readonly/>\
                        <input type="hidden"\
                            id="refaccionaddtr"\
                            value="'+id+'"\
                            readonly/>\
                      </td>\
                      <td>\
                          <input type="number"\
                            class="form-control cserienewi refaccionaddcantidad_'+id+' " data-tr="'+addcantconsu+'"" \
                            id="refaccionaddcantidad" onchange="verificarrcant(this,'+addcantconsu+','+id+');" />\
                      </td>\
                      <td><select id="idbodega" name="idbodega" class="browser-default chosen">'+sucursales+'</select></td>\
                      <td>\
                      <a onclick="deleterefaccionadd('+addcantconsu+')"style="cursor: pointer;"><i class="material-icons" style="color: red;">delete_forever</i></a>\
                      </td>\
                  </tr>';
      $('.accesorios_trss_'+id).append(addhtml);
      addcantconsu++;
      //$('.chosen-select').chosen('destroy');
      $('.chosen').chosen({width: "100%"});
      //$('.chosen').trigger("chosen:updated"); 
    }
  }
}
function verificarrcant(data,tr,row){
  var cantidad=$('#refa_cant_con_'+row).val();
  var entro=$('#refa_ent_con_'+row).val();
  var total=parseFloat(cantidad)-parseFloat(entro);
  console.log(data);
  var addtp = 0;
    $(".refaccionaddcantidad_"+row).each(function() {
        var vstotal = $(this).val();
        addtp += Number(vstotal);
    });
  if (total==0) {
    $('.tr_refaccion_'+tr).remove();
  }else{
    if (addtp>total) {
      var total2=parseFloat(addtp)-parseFloat(total);
      var cantidadinput=$(data).val();
      var newcat=parseFloat(cantidadinput)-parseFloat(total2);
      if (newcat>0) {
        $(data).val(newcat);
      }else{
        $('.tr_refaccion_'+tr).remove();
      }
      
    }
  }
  console.log(addtp);
}
function deleterefaccionadd(id){
    $('.tr_refaccion_'+id).remove();
}
function editarserie(compra,row,tipo){
  $('#editserie').val('');
  $('.modal').modal({
        dismissible: true
  });
  $('#modal_series_editar').modal('open');
  $('#id_compra_edit').val(compra);
  $('#id_row_edit').val(row);
  $('#id_tipo_edit').val(tipo);
}
function aceptar_editar_series(){
  var compra = $('#id_compra_edit').val();
  var row = $('#id_row_edit').val();
  var tipo = $('#id_tipo_edit').val();
  var serie =$('#editserie').val();
  if (serie!='') {
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Compras/editar_series",
        data: {
          compra:compra,
          row:row,
          tipo:tipo,
          serie:serie
        },
        success: function (response){
                alertfunction('Éxito!','Se guardado correctamente');
                $('#modal_series_editar').modal('close');
                $('#modal_series_e').modal('close');
                modal_series(compra);
        },
        error: function(response){
            alertfunction('Atención!','Algo salió mal, intente de nuevo o contacte al administrador del sistema'); 
        }
    });
  }else{
    alertfunction('Atención!','El campo de serie no debe de ir vacio'); 
  }
}
function deletecompra(serie,tipo,tipoearc,compra){
  $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: 'Confirma la eliminacion?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
              //========================================================
              $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Compras/delete_series",
                    data: {
                      compra:compra,
                      serie:serie,
                      tipo:tipo,
                      tipoearc:tipoearc
                    },
                    success: function (response){
                            alertfunction('Éxito!','Se Elimino correctamente');
                            $('#modal_series_editar').modal('close');
                            $('#modal_series_e').modal('close');
                            setTimeout(function(){ 
                              modal_series(compra);
                            }, 1000);
                            
                    },
                    error: function(response){
                        alertfunction('Atención!','Algo salió mal, intente de nuevo o contacte al administrador del sistema'); 
                    }
                });
              //===================================================
              },
            cancelar: function ()             {
                
            }
        }
    });
}
function deletecompraall(compra){
  $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: 'Confirma la eliminacion?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
              //========================================================
              $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Compras/delete_combra",
                    data: {
                      compra:compra
                    },
                    success: function (response){
                      alertfunction('Éxito!','Se Elimino correctamente');
                      datos_compras();
                            
                    },
                    error: function(response){
                        alertfunction('Atención!','Algo salió mal, intente de nuevo o contacte al administrador del sistema'); 
                    }
                });
              //===================================================
              },
            cancelar: function ()             {
                
            }
        }
    });
}
function verificarserie(id,tipo){
    //tipo 1 : equipos
    //tipo 2 : accesorios
    var serie=$('.cserienewi_'+tipo+'_'+id).val();
    console.log(serie);
    if(tipo==1){
      $.ajax({
            type:'POST',
            url: base_url+"index.php/Series/verificar_serie",
            data: {ser:serie},
            success: function (response){
                  if(response==1){
                    alertfunction('Atención!','El numero de serie '+serie+' ya existe');
                    $('.cserienewi_'+tipo+'_'+id).val('');
                  }
            },
            error: function(response){
                alertfunction('Atención!','Algo salió mal, intente de nuevo o contacte al administrador del sistema'); 
            }
        });
    }
    if(tipo==2){
       console.log(serie);
      $.ajax({
            type:'POST',
            url: base_url+"index.php/Series/verificar_serie2",
            data: {ser:serie},
            success: function (response){
                  if(response==1){
                    alertfunction('Atención!','El numero de serie '+serie+' ya existe');
                    $('.cserienewi_'+tipo+'_'+id).val('');
                  }
            },
            error: function(response){
                alertfunction('Atención!','Algo salió mal, intente de nuevo o contacte al administrador del sistema'); 
            }
        });
    }
    if(tipo==3){
       console.log(serie);
      $.ajax({
            type:'POST',
            url: base_url+"index.php/Series/verificar_serie3",
            data: {ser:serie},
            success: function (response){
                  if(response==1){
                    alertfunction('Atención!','El numero de serie '+serie+' ya existe');
                    $('.cserienewi_'+tipo+'_'+id).val('');
                  }
            },
            error: function(response){
                alertfunction('Atención!','Algo salió mal, intente de nuevo o contacte al administrador del sistema'); 
            }
        });
    }
}
function agregaritems(){
  var id=$('#id_compra_c').val();
  location.href =base_url+'index.php/Compras/add/'+id;
}
//=================
function delete_item(id,tipo,compra){
  $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Confirma la eliminacion?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
              //========================================================
              $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Compras/delete_item",
                    data: {
                      id:id,
                      tipo:tipo
                    },
                    success: function (response){
                            alertfunction('Éxito!','Se Elimino correctamente');
                            $('#modal_series_editar').modal('close');
                            $('#modal_series_e').modal('close');
                            setTimeout(function(){ 
                              modal_series(compra);
                            }, 1000);
                            
                    },
                    error: function(response){
                        alertfunction('Atención!','Algo salió mal, intente de nuevo o contacte al administrador del sistema'); 
                    }
                });
              //===================================================
              },
            cancelar: function ()             {
                
            }
        }
    });
}
function edit_cant_item(id,tipo,compra,cant,entr){
  var html='<br><div class="row"><div class="col s6">\
                <label>Cantidad</label>\
                <input type="number" class="form-control-bmz" id="edit_cant" value="'+cant+'">\
                </div><div class="col s6">\
                <label>Entraron</label>\
                <input type="number" class="form-control-bmz" id="edit_entr" value="'+entr+'" readonly>\
                </div></div>';
  $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Confirma la Edicion?'+html,
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
              var edit_cant = $('#edit_cant').val();
              var edit_entr = $('#edit_entr').val();
              if(parseInt(edit_cant)>=parseInt(edit_entr)){
                //========================================================
                $.ajax({
                      type:'POST',
                      url: base_url+"index.php/Compras/edit_cant_item",
                      data: {
                        id:id,
                        tipo:tipo,
                        cant:edit_cant
                      },
                      success: function (response){
                              alertfunction('Éxito!','Se Edito correctamente');
                              $('#modal_series_editar').modal('close');
                              $('#modal_series_e').modal('close');
                              setTimeout(function(){ 
                                modal_series(compra);
                              }, 1000);
                              
                      },
                      error: function(response){
                          alertfunction('Atención!','Algo salió mal, intente de nuevo o contacte al administrador del sistema'); 
                      }
                  });
                //===================================================
              }else{
                alertfunction('Atención!','La cantidad debe de ser mayor o igual a la entrada'); 
              }
              
            },
            cancelar: function () {
                
            }
        }
    });
}