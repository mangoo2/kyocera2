<?php 
   $addassesorios=1;
?>
<!DOCTYPE html>
<html>
  <head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
<!--
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/formv/formValidation.min3f0d.css?v2.2.0">
-->
		<link rel="icon" href="<?php echo base_url(); ?>app-assets/images/favicon/favicon_kyocera.png" sizes="32x32">
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<title>Prefactura</title>
		<!-- jQuery Library -->
    	<script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/jquery-3.3.1.min.js"></script>
    	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <link href="<?php echo base_url(); ?>app-assets/vendors/sweetalert/dist/sweetalert.css" type="text/css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>public/css/fontawesome/css/all.css" type="text/css" rel="stylesheet">
		<style type="text/css">
			.tdstitle{background: #c1bcbc;font-weight: bold;color: white;}
			html{-webkit-print-color-adjust: exact;}
			input{padding: 0px !important;height: 28px !important;}
			select{padding: 0px !important;height: 28px !important;font-size: 11px !important;}
			@media print{
				input{background: transparent;border: 0 !important;}
				.buttonenvio{display: none;}
				html{-webkit-print-color-adjust: exact;}
				.tdstitle{background: #c1bcbc;font-weight: bold;color: white;}
				.buttoimprimir{display: none;}
				select{background: transparent;border: 0 !important; -moz-appearance: none; -webkit-appearance: none; appearance: none; }
				.form-control:disabled, .form-control[readonly] {background-color: transparent;opacity: 1;}
			}
			<?php if(isset($_GET['devolucion'])){ ?>
				.table_dev{display: none;}
			<?php }else{ ?>
				.btndevolucion{display: none;}
			<?php } ?>
			.devolucion{background-color: #ff02026b;}
				.btndevolucion2{display: none;}
				.btndevolucion3{display: none;}
			.font11{font-size: 11px;}
		</style>
	</head>
	<body>
		<?php if($cargo_block==''){ ?>
			<style type="text/css">
				.nosolicitado {background: url(<?php echo base_url().'public/img/nosolicitado.png'?>);background-size: contain;background-repeat: no-repeat;background-position: center;}
		        
			</style>
		<?php } ?>
		<input type="hidden" id="base_url" name="base_url" value="<?php echo base_url(); ?>">
		<form method="post" id="formprefactura">
			<input type="hidden" id="ventaId" name="ventaId" value="<?php echo $ventaId;?>">
			<table border="1" width="100%" class="table_dev">
				<tr>
					<td><img src="<?php echo base_url(); ?>app-assets/images/<?php echo $configuracionCotizacion->logo1; ?>"></td>
					<td><?php echo $configuracionCotizacion->textoHeader; ?></td>
					<td><img class="" src="<?php echo base_url(); ?>app-assets/images/1024_kyocera_logo_mds.png" alt="materialize logo"  style="max-width:200px; width: 100%;"></td>
				</tr>
			</table>
			<table border="1" width="100%" class="table_dev">
				<tr>
					<td class="tdstitle" style="width: 10%;">FECHA</td>
					<td style="width: 15%;"><input type="date" name="reg" id="reg" value="<?php if(isset($dia_reg)){ echo $ano_reg.'-'.$mes_reg.'-'.$dia_reg; }else{ echo date('Y-m-d');}?>" style="border: 0px"></td>
					<td class="tdstitle" style="width: 5%">AC</td>
					<td style="width: 10%;"><?php echo $ini_personal;?></td>
					<td class="tdstitle" style="width: 20%">PROINREN No.</td>
					<td style="width: 10%;"><?php echo $proinven;?></td>
					<td class="tdstitle" style="width: 10%">ENTREGA</td>
					<td style="width: 20%"><input type="date" id="fechaentrega" value="<?php echo $rentaventashg->fechaentrega;?>" class="form-control" onchange="actualizarfechae(<?php echo $ventaId;?>)" required></td>
				</tr>
			</table>
			<table border="1" width="100%" class="table_dev">
				<tr>
					<td style="width: 5%;">Cia.</td>
					<td style="width: 80%;" colspan="5"><output id="razon_social"></td>
					<td style="width: 5%;">RFC</td>
					<td style="width: 10%;">
					    <select class="form-control" id="rfc_id" name="rfc_id" onchange="selectrcf()" <?php echo $rfc_id_selectetd;?>>
						    <option value="">Seleccione</option>
							<?php foreach ($rfc_datos as $item) { ?>
								<option value="<?php echo $item->id; ?>" <?php if($item->id==$rfc_id){ echo 'selected';}?> ><?php echo $item->rfc; ?></option>
							<?php } ?>
					    </select><span style="color: red;" class="dato1"></span>	
					</td>
				</tr>
				<tr>
					<td style="width: 5%; font-size: 9px">VENCE</td>
					<td style="width: 12%;">
						<input type="date" id="vencimiento" name="vencimiento" class="form-control" value="<?php echo $vence;?>" <?php echo $vence_block;?> style="max-width: 138px;"><span style="color: red;" class="dato2"></span></td>
					<td style="width: 10%; font-size: 11px; text-align: center;">METODO DE PAGO</td>
					<td style="width: 20%; font-size: 11px; text-align: center;">
					<select class="form-control" id="metodopagoId" name="metodopagoId" <?php echo $metodopagoId_block;?> >
						<?php foreach ($metodopagorow->result() as $item) { ?>
								<option value="<?php echo $item->id; ?>" <?php if($item->id==$metodopagoId){ echo 'selected';} ?>    ><?php echo $item->metodopago_text; ?></option>
						<?php } ?>
					</select><span style="color: red;" class="dato3"></span>
					</td>
					<td style="width: 12%; font-size: 11px; text-align: center;">FORMA DE PAGO</td>
					<td style="width: 14%; font-size: 11px; text-align: center;">
						<select class="form-control" id="formapagoId" name="formapagoId" <?php echo $formapago_block;?> >
						<?php foreach ($formapagorow->result() as $item) { ?>
								<option value="<?php echo $item->id; ?>" <?php if($item->id==$formapago){ echo 'selected';} ?>    ><?php echo $item->formapago_text; ?></option>
						<?php } ?>
					</select><span style="color: red;" class="dato4"></span>
					</td>
					<td style="width: 10%; font-size: 11px;">USO DE CFDI</td>
					<td style="width: 17%; font-size: 11px;">
						<select class="form-control" id="usocfdiId" name="usocfdiId" <?php echo $cfdi_block;?>>
							<?php foreach ($cfdirow->result() as $item) { ?>
									<option value="<?php echo $item->id; ?>" <?php if($item->id==$cfdi){ echo 'selected';} ?>    ><?php echo $item->uso_cfdi_text; ?></option>
							<?php } ?>
						</select><span style="color: red;" class="dato5"></span>
					</td>	
					
				</tr>
			</table>
			<table border="1" width="100%" class="table_dev">
				<tr>
					<td style="font-size: 11px; width: 5%;">tel.</td>
					<td style="font-size: 11px;" colspan="2">
						<select class="form-control" id="telId" name="telId" <?php echo $telId_block; ?>>
						<?php foreach ($resultadoclitel->result() as $item) { ?>
							<option value="<?php echo $item->id;?>"   <?php if($telId==$item->id){echo 'selected'; }?> ><?php echo $item->tel_local;?></option>		
					    <?php } 
					    		foreach ($datoscontacto->result() as $item) { ?>
					    	<option data-contacto="1"  value="<?php echo $item->datosId;?>" <?php if($telId==$item->datosId){echo 'selected'; }?>><?php echo $item->telefono;?></option>	
					    <?php }
					    ?>
					    
						</select><span style="color: red;" class="dato7"></span>
					</td>
					<td style="font-size: 11px; width: 9%; text-align: center;">FORMA DE COBRO</td>
					<td style="font-size: 11px; width: 20%;">
						<?php if($formacobro!=''){
							echo $formacobro;
						}else{ ?> 
							<select class="form-control" name="formadecobro" id="formadecobro" required>
								<option></option><option>Mostrador</option><option>Transferencia</option><option>Deposito directo</option><option>Tecnico</option>
							</select>
						<?php } ?>
						<span style="color: red;" class="dato6"></span>
					</td>
				</tr>
			</table>
			<table border="1" width="100%" class="table_dev">
				<tr>
					<td class="tdstitle" colspan="8">Dirección Fiscal</td>
				</tr>
				<tr class="font11">
					<td style="width: 5%;">Calle</td>
					<td style="width: 26%;">
						<input type="text" value="<?php echo $calle;?>" id="calle" name="d_calle" style="width: 100%; border: 0px;" readonly>
					</td>
					<td style="width: 5%;">Cd</td>
					<td style="width: 20%;"><input type="text" value="<?php echo $municipio;?>" id="d_ciudad" name="d_ciudad"  style="width: 100%; border: 0px;" readonly></td>
					<td style="width: 5%;">Edo.</td>
					<td style="width: 15%;" >
							<?php
								$estadoval=''; 
								foreach ($estadorow->result() as $item) { 
									if($item->EstadoId==$estado){ 
									 	$estadoval= $item->Nombre;
									}
								} ?>
						<input type="text" value="<?php if($estadovals!=''){echo $estadovals;}else{echo $estadoval.' C.P.'.$cp;}?>" id="d_estado" name="d_estado" style="width: 100%; border: 0px;" readonly>
					</td>
				</tr>
			</table>
			<table border="1" width="100%" class="table_dev">
				<tr>
					<td class="tdstitle" colspan="8">Domicilio de entrega/Instalación</td>
				</tr>
				<tr>
					<td style="width: 15%">Domicilio</td>
					<td style="width: 85%">
						<input type="text" name="domicilio_entrega" class="form-control" id="domicilio_entrega" value="<?php echo $domicilio_entrega;?>" readonly style="background-color: transparent; border:0px;" onclick="editardireccion(<?php echo $idCliente;?>)"><span style="color: red;" class="dato10"></span>
					</td>
				</tr>
			</table>
			<div class="nosolicitado">
			<table border="1" width="100%" class="table_dev">
				<tr>
					<td style="font-size: 11px; width: 10%;" colspan="2">Contacto</td>
					<td class="font11" colspan="2">
						<select class="form-control" id="contactoId" name="contactoId" <?php echo $contactoId_block; ?> onchange="obtenercargo()">
						<?php foreach ($resultadoclipcontacto->result() as $item) { ?>
							<option 
							value="<?php echo $item->id;?>" 
							<?php if($contactoId==$item->id){echo 'selected'; }?>
							data-contacto="0"
							><?php echo $item->persona_contacto;?></option>		
					    	<?php } ?>
					    	<?php foreach ($datoscontacto->result() as $item) { ?>
							<option 
								value="<?php echo $item->datosId;?>" 
								<?php if($contactoId==$item->datosId){echo 'selected'; }?>
									data-contacto="1"
									data-email="<?php echo $item->email;?>"
									data-puesto="<?php echo $item->puesto;?>"
									data-telefono="<?php echo $item->telefono;?>"
								><?php echo $item->atencionpara;?></option>		
					    	<?php } ?>
						</select><span style="color: red;" class="dato8"></span>
					</td>
					<td style="font-size: 11px; width: 5%;">Cargo</td>
					<td style="font-size: 11px; width: 5%;" colspan="3">
						<input type="tex" id="cargo" name="cargo" class="form-control" value="<?php echo $cargo;?>" <?php echo $cargo_block;?> ><span style="color: red;" class="dato9"></span>
					</td>
					<td style="font-size: 11px; width: 5%;">email:</td>
					<td style="font-size: 11px;" colspan="2" class="addemail"><?php echo $email;?></td>
				</tr>
				<tr class="font11">
					<td style=" width: 10%;" colspan="2">Fecha de entrega</td>
					<td >Orden de compra</td>
					<td style=" width: 5%;" colspan="3">Horario de entrega</td>
					<td style=" width: 5%;" rowspan="2">REFERENCIA DOMICILIO</td>
					<th style=" width: 5%; text-align: center;" rowspan="2" colspan="4">LUGAR DE INSTALACIÓN: <?php echo $direccion_c ?>. <?php echo $equipo_acceso ?>. <?php echo $doc_acceso ?></th>
				</tr>
				<tr class="font11">
					<td style=" text-align: center; width: 10%;" colspan="2"><?php echo $fechainicio ?></td>
					<td style=" text-align: center;" ><?php echo $ordencompra ?></td>
					<td style=" text-align: center; width: 5%;" colspan="3"><?php echo $horaentregainicio ?> a <?php echo $horaentregafin ?></td>
				</tr>
				
			</table>
			<table border="1" width="100%" id="table_productos" >
				<thead>
					<tr class="font11">
						<th>Cantidad</th>
						<th>No. de Parte</th>
						<th colspan="3">Descripción</th>
						<th>Precio Unitario</th>
						<th >Total</th>
					</tr>
				</thead>
				<tbody>
					<?php 
						$arrays_tr=array();
						$arrays_tr[]=array('equipo'=>0,'html'=>'');
						$totalgeneral=0;
						foreach ($rentaventash->result() as $item) { 
							if($item->personal_dev>0){
								$cssdev='class="table_dev devolucion"';
							}else{
								$cssdev='';
							}			
							//$totalgeneral=$totalgeneral+($item->precio*$item->cantidad);
							$totalgeneral=$totalgeneral+0;
							$datosseries = $this->ModeloCatalogos->rentaventasdseries($item->id);
							$usado=0;
							foreach($datosseries->result() as $items) {
								$usado=$items->usado;
							}
							$vusado='';

							if($usado==1){
								$vusado='-mc';
							}
							if($item->serie_bodega==2){//seminuevos
								$vusado='-mc';
							}

							$html='';	
							$html.='<tr '.$cssdev.'>';
								$html.='<td style="font-size: 11px; text-align: center;">'.$item->cantidad.'<!--'.$item->id.'-->';
								$html.='<a class="btn btn-danger btn-sm btndevolucion" onclick="devolucion(1,'.$item->id.','.$item->cantidad.')" ><i class="fas fa-trash"></i></a>	</td>';
								$html.='<td style="font-size: 11px; ">'.$item->noparte.'</td>';
								$html.='<td style="font-size: 11px; " colspan="3">';
									$html.='<table border="1" width="100%">';
										$html.='<tr>';
											$html.='<td width="30%"></td>';
											$html.='<td width="70%">Renta</td>';
										$html.='</tr>';
										$html.='<tr>';
											$html.='<td>MODELO</td>';
											$html.='<td>'.$item->modelo.$vusado.'</td>';
										$html.='</tr>';
											if($datosseries->num_rows()==0){
		            							$addassesorios=0;
		            						}
		                        			$bodega_or = '';
											$bodega_or_name	  = '';
											foreach($datosseries->result() as $items) {
												$bodega_or =$items->bodega_or;
												$bodega_or_name =$items->bodega;
												$html.='<tr><td>Serie</td><td>'.$items->serie.'<a class="btn btn-danger btn-sm btndevolucion2" onclick="devolucion2(1,'.$item->id.','.$items->serieId.'" ><i class="fas fa-trash"></i></a></td></tr>';
											}
											if($item->bodegaId>0){
												if($bodega_or>0){
													$html.='<tr><td>Bodega</td><td>'.$bodega_or_name.'</td></tr>';//se comenta por si lo quieren tambien aqui se descomenta
													//$html.='<tr><td>Bodega</td><td>'.$item->bodega.'</td></tr>';
												}else{
													$html.='<tr><td>Bodega</td><td>'.$item->bodega.'</td></tr>';
												}
												
											}
										$html.='</table>';
								$html.='</td>';
								$html.='<td style="font-size: 11px; text-align: center;"  >$'.number_format(0,2,'.',',').'</td>';
								$html.='<td style="font-size: 11px; text-align: center;" >$'.number_format(0,2,'.',',').'</td>';
							$html.='</tr>';
							$arrays_tr[]=array('equipo'=>$item->id,'html'=>$html);
						}
					?>
					<?php 
						$totalgeneral=0;
						foreach ($rentavacessoriosh->result() as $item) { 
									
						//$totalgeneral=$totalgeneral+($item->precio*$item->cantidad);
						$totalgeneral=$totalgeneral+0;
						$html='';
					
						$html.='<tr>
							<td style="font-size: 11px; text-align: center;">'.$item->cantidad.'<!--'.$item->rowequipo.'-->
								<a class="btn btn-danger btn-sm btndevolucion" onclick="devolucion(3,'.$item->id_accesoriod.','.$item->cantidad.')" ><i class="fas fa-trash"></i></a>
							</td>
							<td style="font-size: 11px; ">'.$item->no_parte.'</td>
							<td style="font-size: 11px; " colspan="3">
								<table border="1" width="100%">
									<tr>
										<td width="30%"></td>
										<td width="70%">Accesorios</td>		
									</tr>
									<tr>
										<td>MODELO</td>
										<td>'.$item->nombre.'</td>		
									</tr>';
                        						$datosseries = $this->ModeloCatalogos->rentaventasaccessoriosseries($item->id_accesoriod); 
                        						if($datosseries->num_rows()==0){
                        							$addassesorios=0;
                        						}
									foreach($datosseries->result() as $items) { 
										$bloquearsaveserie=1;
										
										$html.='<tr><td>Serie</td><td>'.$items->serie.'</td></tr>';
									}
									if($item->bodegaId>0){
										$html.='<tr><td>Bodega</td><td>'.$item->bodega.'</td></tr>';
									}
									$datosseriesequipos = $this->ModeloCatalogos->acc_equipo_serie($item->id_accesoriod); 
									foreach ($datosseriesequipos->result() as $itemae) {
										if($proinven==21871){// este filtro solo es para esta pre para que no salgan otras series por el tema de la configuracion anterior
											if($itemae->serie=='VRA1420581'){// este filtro solo es para esta pre para que no salgan otras series por el tema de la configuracion anterior
												$html.='<tr><td>Equipo</td><td>'.$itemae->modelo.' '.$itemae->serie.'</td></tr>';	
											}
										}else{
											$html.='<tr><td>Equipo</td><td>'.$itemae->modelo.' '.$itemae->serie.'</td></tr>';	
										}
									}
									
								$html.='</table>
							</td>
							<td style="font-size: 11px; text-align: center;"  >$'.number_format(0,2,'.',',').'</td>
							<td style="font-size: 11px; text-align: center;" >$'.number_format(0,2,'.',',').'</td>
						</tr>';
						$arrays_tr[]=array('equipo'=>$item->rowequipo,'html'=>$html);
						}


						$totalgeneral=0;
						foreach ($rentavacessoriosh_dev->result() as $item) { 
									
						//$totalgeneral=$totalgeneral+($item->precio*$item->cantidad);
						$totalgeneral=$totalgeneral+0;
						$html='';
					
						$html.='<tr class="table_dev devolucion">
							<td style="font-size: 11px; text-align: center;">'.$item->cantidad.'<!--'.$item->rowequipo.'--></td>
							<td style="font-size: 11px; ">'.$item->no_parte.'</td>
							<td style="font-size: 11px; " colspan="3">
								<table border="1" width="100%">
									<tr>
										<td width="30%"></td>
										<td width="70%">Accesorios</td>		
									</tr>
									<tr>
										<td>MODELO</td>
										<td>'.$item->nombre.'</td>		
									</tr>';
                        						$datosseries = $this->ModeloCatalogos->rentaventasaccessoriosseries($item->id_accesoriod); 
                        						if($datosseries->num_rows()==0){
                        							$addassesorios=0;
                        						}
									foreach($datosseries->result() as $items) { 
										$bloquearsaveserie=1;
										
										$html.='<tr><td>Serie</td><td>'.$items->serie.'</td></tr>';
									}
									
								$html.='</table>
							</td>
							<td style="font-size: 11px; text-align: center;"  >$'.number_format(0,2,'.',',').'</td>
							<td style="font-size: 11px; text-align: center;" >$'.number_format(0,2,'.',',').'</td>
						</tr>';
						$arrays_tr[]=array('equipo'=>$item->rowequipo,'html'=>$html);
						}
					 
						$totalgeneral=0;
						foreach ($rentavconsumiblesh->result() as $item) { 
							if($item->preren==0){
								$this->ModeloCatalogos->updateCatalogo('contrato_folio',array('preren'=>$ventaId,'proinren'=>$proinven),array('idcontratofolio'=>$item->idcontratofolio));
							}
						//$totalgeneral=$totalgeneral+($item->precio*$item->cantidad);
						$totalgeneral=$totalgeneral+0;
						$html='';
						$html.='<tr>';
							$html.='<td style="font-size: 11px; text-align: center;">'.$item->cantidad.'<!--'.$item->rowequipo.'-->';
								//$html.='<!--<a class="btn btn-danger btn-sm btndevolucion" onclick="devolucion(2,'.$item->id.','.$item->cantidad.')" ><i class="fas fa-trash"></i></a>-->';
								$html.='<a class="btn btn-danger btn-sm btndevolucion" href="'.base_url().'index.php/ListadoFolio?foliotext='.$item->folios.'" target="_blank"><i class="fas fa-trash"></i></a>';
							$html.='</td>';
							$html.='<td style="font-size: 11px; ">'.$item->parte.'</td>';
							$html.='<td style="font-size: 11px; " colspan="3">';
								$html.='<table border="1" width="100%">';
									$html.='<tr>';
										$html.='<td width="30%"></td>';
										$html.='<td width="70%">Consumible</td>';
									$html.='</tr>';
									$html.='<tr>';
										$html.='<td>MODELO</td>';
										$html.='<td>'.$item->modelo.'</td';
									$html.='</tr>';
									$html.='<tr>';
										$html.='<td>Folio</td>';
										$html.='<td>'.$item->folios.'</td>';
									$html.='</tr>';
									if($item->bodegaId>0){
										$html.='<tr><td>Bodega</td><td>'.$item->bodegav.'</td></tr>';
									}
									
								$html.='</table>';
							$html.='</td>';
							$html.='<td style="font-size: 11px; text-align: center;"  >$'.number_format(0,2,'.',',').'</td>';
							$html.='<td style="font-size: 11px; text-align: center;" >$'.number_format(0,2,'.',',').'</td>';
						$html.='</tr>';
						$arrays_tr[]=array('equipo'=>$item->rowequipo,'html'=>$html);
						}
						$totalgeneral=0;
						foreach ($rentavconsumiblesh_dev->result() as $item) { 
									
						//$totalgeneral=$totalgeneral+($item->precio*$item->cantidad);
						$totalgeneral=$totalgeneral+0;
						$html='';
						$html.='<tr class="table_dev devolucion">
							<td style="font-size: 11px; text-align: center;">'.$item->cantidad.'<!--'.$item->rowequipo.'--></td>
							<td style="font-size: 11px; ">'.$item->parte.'</td>
							<td style="font-size: 11px; " colspan="3">
								<table border="1" width="100%">
									<tr>
										<td width="30%"></td>
										<td width="70%">Consumible</td>		
									</tr>
									<tr>
										<td>MODELO</td>
										<td>'.$item->modelo.'</td>		
									</tr>
									<tr>
										<td>Folio</td>
										<td>'.$item->folios.'</td>		
									</tr>
									
								</table>
							</td>
							<td style="font-size: 11px; text-align: center;"  >$'.number_format(0,2,'.',',').'</td>
							<td style="font-size: 11px; text-align: center;" >$'.number_format(0,2,'.',',').'</td>
						</tr>';
						$arrays_tr[]=array('equipo'=>$item->rowequipo,'html'=>$html);
						}

						foreach ($arrays_tr as $key => $row) {
						    $aux[$key] = $row['equipo'];
						}
						array_multisort($aux, SORT_ASC, $arrays_tr);
						foreach ($arrays_tr as $key => $row) {
    							echo $row['html'];
						}

					?>	
					
				</tbody>
				<tfoot>
					<tr class="font11">
						<td colspan="2">Subtotal</td>
						<td colspan="1">$<?php echo number_format($totalgeneral,2,'.',',');?></td>
						<td>Iva</td>
						<td >$<?php echo number_format($totalgeneral*0.16,2,'.',',');?></td>
						<td>Total</td>
						<td >$<?php echo number_format(($totalgeneral*1.16),2,'.',',');?></td>
					</tr>
				</tfoot>

			</table>
			</div>
			<!-- -->
			<table border="1" width="100%" class="table_dev">
				<tbody>
					<tr>
						<th class="font11" style="text-align: center;">Deposito a nombre de "ALTA PRODUCTIVIDAD, S.A DE C.V.", BANAMEX, Cuenta 7347132 sucursal 826, CLABET/Transferencia Electrónica 002668082673471327</th>
					</tr>
				</tbody>				
			</table>
			<!-- -->
			<!--firmas -->
			<table border="1" width="100%" class="table_dev">
				<thead>
				<tr>
					<td style="font-size: 10px; text-align: center;" width="32%">ACEPTA QUE SE SURTA ESTA ORDEN DE COMPRA, EN LOS TÉRMINOS ESTABLECIDOS</td>
					<td style="font-size: 11px; text-align: center;" wdth="18%">ALMACEN</td>
					<td style="font-size: 11px; text-align: center;" wdth="25%">ASESOR COMERCIAL</td>
					<td style="font-size: 11px; text-align: center;" wdth="25%">GERENCIA GENERAL</td>
				</tr>
				</thead>
				<tbody >
					<tr class="font11">
						<td style="text-align: center;"><br><br></td>
						<td style="text-align: center;"></td>
						<td style="text-align: center;"></td>
						<td style="text-align: center;"></td>
					</tr>
				</tbody>
				<tfoot>
					<tr class="font11">
						<td style="text-align: center;">NOMBRE Y FIRMA DEL CLIENTE</td>
						<td style="text-align: center;">FIRMA</td>
						<td style="text-align: center;">FIRMA</td>
						<td style="text-align: center;">FIRMA</td>
					</tr>
					<tr>
						<td colspan="4">
							<label for="observaciones" class="table_dev">Observaciones:</label>
							<textarea name="observaciones" id="observaciones" class="form-control table_dev" style="min-height: 150px;" <?php echo $cargo_block;?>><?php echo $observaciones;?></textarea>
						</td>
					</tr>
				</tfoot>				
			</table>
			<!--firmas fin -->
		</form>
		<div class="row">
		    <div class="col-md-12">
		        <div class="col-md-12 text-right">
		           <?php if($addassesorios==1){
		        		if($block_button==0){ ?>
		        			<button type="button" class="btn btn-success buttonenvio">Enviar</button>
		        	
		        	<?php  }else{  	?>	
		        		<button type="button" class="btn btn-success buttoimprimir table_dev">Imprimir</button>
		        	<?php  } 
		        	} 	?>	
		        </div>
		    </div>
		</div>
		<div class="modal fade" id="modalconfirmacionenvio" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h5 class="modal-title" id="exampleModalLabel">Enviar</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      <div class="modal-body">
		        <div class="row">
		        	<div class="col-md-12">
		        		<h4>¿Desea enviar la prefactura?</h4>
		        	</div>
		        </div>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
		        <button type="button" class="btn btn-primary buttonenvioconfirm">Enviar</button>
		      </div>
		    </div>
		  </div>
		</div>
		<div class="modal fade" id="modaldirecciones" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"> 
		  <div class="modal-dialog modal-xl" role="document"> 
		    <div class="modal-content"> 
		      <div class="modal-header"> 
		        <h5 class="modal-title" id="exampleModalLabel">Direcciones</h5> 
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> 
		          <span aria-hidden="true">&times;</span> 
		        </button> 
		      </div> 
		      <div class="modal-body"> 
		        <div class="row"> 
		        	<div class="col-md-12 viewdirecciones" style="font-size: 12px;"> 
		        		 
		        	</div> 
		        </div> 
		         
		      </div> 
		      <div class="modal-footer"> 
		        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button> 
		      </div> 
		    </div> 
		  </div> 
		</div> 
		
	</body>
	<script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/sweetalert/dist/sweetalert.min.js"></script>
	<link href="<?php echo base_url(); ?>app-assets/vendors/confirm/jquery-confirm.min.css" type="text/css" rel="stylesheet">
	<script src="<?php echo base_url(); ?>app-assets/vendors/confirm/jquery-confirm.min.js"></script>
	<link href="<?php echo base_url(); ?>app-assets/vendors/data-tables/css/jquery.dataTables.min.1.11.4.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/data-tables/js/jquery.dataTables.min.1.11.4.js"></script>
	<!--
<script src="<?php echo base_url(); ?>app-assets/vendors/js/formvalidation/formValidation.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>app-assets/vendors/js/formvalidation/framework/bootstrap.min.js"></script>
-->
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.28.14/js/jquery.tablesorter.min.js"></script>
	<script type="text/javascript">
		var base_url = $('#base_url').val();
		$(document).ready(function(){ 
			$('.buttonenvio').click(function(event) {
				$('#modalconfirmacionenvio').modal();
			});
			$('.buttonenvioconfirm').click(function(event) {
				var datos = $('#formprefactura').serialize();
		                var rfc_id_v = $('#rfc_id option:selected').val(); 
		                var vencimiento_v = $('#vencimiento').val(); 
		                var metodopagoId_v = $('#metodopagoId option:selected').val();
		                var formapagoId_v = $('#formapagoId option:selected').val();
		                var usocfdiId_v = $('#usocfdiId option:selected').val();
		                var formadecobro_v = $('#formadecobro').val();
		                var telId_v = $('#telId option:selected').val();
		                var contactoId_v = $('#contactoId option:selected').val();
		                var cargo_v = $('#cargo').val();
		                var domicilio_entrega = $('#domicilio_entrega').val();
				if(rfc_id_v != "" && vencimiento_v != "" && metodopagoId_v != "" && formapagoId_v != "" && usocfdiId_v !="" && formadecobro_v != "" && telId_v !="" && contactoId_v !="" && cargo_v !="" && domicilio_entrega !=""){
					$.ajax({
	                    type:'POST',
	                    url: base_url+'RentasCompletadas/saveh',
	                    data: datos,
	                    async: false,
	                    statusCode:{
	                            404: function(data){
	                                swal("Error", "404", "error");
	                            },
	                            500: function(){
	                                swal("Error", "500", "error"); 
	                            }
	                        },
	                        success:function(data){
	                            swal("Éxito", "Pre factura enviada. Por favor recargue la vista de listado de rentas", "success");
	                            setTimeout(function(){ location.reload(); }, 3000);
	                         

	                        }
	                    });
				}else{
					swal("Registro", "Verifica que todos los campos esten llenos", "error");
					if(rfc_id_v == ""){$('.dato1').html("Campo requerido");}
					if(vencimiento_v == ""){$('.dato2').html("Campo requerido");} 
				    if(metodopagoId_v == ""){$('.dato3').html("Campo requerido");} 
				    if(formapagoId_v == ""){$('.dato4').html("Campo requerido");} 
				    if(usocfdiId_v ==""){$('.dato5').html("Campo requerido");} 
				    if(formadecobro_v == ""){$('.dato6').html("Campo requerido");} 
				    if(telId_v ==""){$('.dato7').html("Campo requerido");} 
				    if(contactoId_v ==""){$('.dato8').html("Campo requerido");} 
				    if(cargo_v ==""){$('.dato9').html("Campo requerido");}
				    if(domicilio_entrega ==""){$('.dato10').html("Campo requerido");}
				}
			    
			});
			$('.buttoimprimir').click(function(event) {
				window.print();
			});
			
			<?php if ($rfc_id>0) { ?>
				selectrcfv(<?php echo $rfc_id;?>);
			<?php }else{ ?>
				
			<?php } ?>
			obtenercargo();
		});
		function selectrcf() {
			var rfc_id = $('#rfc_id option:selected').val();
			selectrcfv(rfc_id);
			
		}
		function selectrcfv(rfc_id){
			$.ajax({
                url: base_url+'Prefactura/selecciontrcf',
                dataType: 'json',
                data:{id:rfc_id},
                statusCode:{
                        404: function(data){
                            swal("Error", "404", "error");
                        },
                        500: function(){
                            swal("Error", "500", "error"); 
                        }
                    },
                    success:function(data){
                    	var datos=data;
                        datos.forEach(function(r) {
                        $('#razon_social').val(r.razon_social);
                         <?php if ($rfc_id==0) { ?> 
                        $('#num_ext').val(r.num_ext);
                        $('#colonia').val(r.colonia);
                        $('#calle').val(r.calle);
                        $('#cp').val(r.cp);
                        <?php } ?>
                        });
                    }
            });
		}
		function editardireccion(cliente){ 
			<?php if ($rfc_id==0) { ?> 
			$('#modaldirecciones').modal(); 
			$.ajax({ 
				type:'POST', 
                url: base_url+'PolizasCreadas/pintardireccionesclientes', 
                data:{clienteid:cliente}, 
                statusCode:{ 
                        404: function(data){ 
                            swal("Error", "404", "error"); 
                        }, 
                        500: function(){ 
                            swal("Error", "500", "error");  
                        } 
                    }, 
                    success:function(data){ 
                    	$('.viewdirecciones').html(data); 
                    	$('.adddirecciones').click(function(event) {
					                    		var direccion = $(this).html();
					                    		//console.log(direccion);
					                    		$('#domicilio_entrega').val(direccion);
					                    	});
                    	$('#table_pinta_dir').dataTable();
                    	 
 
                    } 
            }); 
           	<?php } ?>  
		} 
		function obtenercargo(){
			var contacto=$('#contactoId option:selected').data('contacto');
			if (contacto==1) {
				var contactoid=$('#contactoId option:selected').val();
				//$('#telId option:selected').val(contactoid);
				$("#telId option").removeAttr("selected");
				$("#telId option[value="+contactoid+"]").attr('selected', 'selected');

				var puesto=$('#contactoId option:selected').data('puesto');

				$('#cargo').val(puesto);

				var email=$('#contactoId option:selected').data('email');
				$('.addemail').html(email);
			}
		}
		function actualizarfechae(idventa){
			var fechaentrega = $('#fechaentrega').val();
			var fecha=fechaentrega;
			var fecha=fecha.split("-");
			if(fecha[0]>2021){
				$.confirm({
			        boxWidth: '40%',
			        useBootstrap: false,
			        icon: 'fa fa-warning',
			        title: 'Atención!',
			        content: '¿Confirma la edición de la fecha de entrega a <b>'+fechaentrega+'</b>?',
			        type: 'red',
			        typeAnimated: true,
			        buttons:{
			            confirmar: function (){
			                //===================================================
			                    
			                    //console.log(aInfoa);
			                        $.ajax({
			                            type:'POST',
			                            url: base_url+"index.php/Generales/editarfechaentrega",
			                            data: {
			                                ventaid:idventa,
			                                fentrega:fechaentrega,
			                                tipo:3
			                            },
			                            success:function(response){  
			                                
			                                swal("Éxito!", "Se ha Modificado", "success");
			                                setTimeout(function(){ 
												location.reload();
											}, 1000);

			                            }
			                        });
			                    
			                //================================================
			                 
			            },
			            cancelar: function () 
			            {
			                
			            }
			        }
			    });
			}
		}
	</script>
	<?php if(isset($_GET['devolucion'])){ ?>
		<script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/mask/jquery.mask.min.js"></script>
    	<script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/mask/jquery.passwordify.js"></script>
    	
		<script type="text/javascript" src="<?php echo base_url(); ?>public/js/devolucion_r.js?v=<?php echo date('YmdGis');?>" ></script>

	<?php } ?>
</html>
