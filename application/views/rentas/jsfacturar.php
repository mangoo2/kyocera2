<link href="<?php echo base_url(); ?>public/plugins/select2/select2.min.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/select2/select2.full.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/loading/jquery.loading.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/plugins/loading/demo.css">
<script src="<?php echo base_url(); ?>app-assets/js/jquery-ui.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>public/js/operaciones/form_factura.js?v=021<?php echo $version;?>"></script>
<script type="text/javascript">
	$(document).ready(function($) {
		obtenerdatosrentas();
		verficartiporfc();
		<?php if(isset($_GET['uuid'])){ ?>
			setTimeout(function(){ 
				$( "#facturarelacionada" ).prop( "checked", true ); 
				$('.divfacturarelacionada').show('show');
				$('#uuid_r').val('<?php echo $_GET['uuid']; ?>');
			}, 1000);
			
		<?php } ?>	
		<?php if($fac_pue_pend->num_rows()>0){ ?>
			fac_pen_pago_pue(<?php echo $fac_pue_pend->num_rows();?>,<?php echo $idcontrato;?>);
		<?php } ?>
	});
</script>