var base_url = $('#base_url').val();
var table;
$(document).ready(function() {	
	table = $('#tabla_ventas').DataTable();
	loadtable();
    $('#cliente').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una opción',
        allowClear: true,
        ajax: {
            url: base_url+'Ventasi/searchclientes',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.id,
                        text: element.empresa

                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    });
});
function loadtable(){
    var tipovs=$('#tipovs option:selected').val();
    var clientes=$('#cliente option:selected').val()==undefined?0:$('#cliente option:selected').val();
	table.destroy();
	table = $('#tabla_ventas').DataTable({
        //stateSave: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url+"index.php/Devoluciones/getlistaccesorios",
            type: "post",
            "data": {
                tipov:tipovs,
                cliente:clientes
            },
        },
        "columns": [
            {"data": "idVentas",
                render:function(data,type,row){
                    var html='';
                        html=row.idVentas;
                        html+='<div class="vr_'+row.vr+'_'+row.tipo+'_'+row.row+'_p"></div>';
                    return html;
                }
            },
            {"data": "idVentas",
                render:function(data,type,row){
                    var html='';
                        var comillas = "'";
                        html+='<a class="btn-floating green tooltipped vr_'+row.vr+' vr_'+row.vr+'_'+row.tipo+'_'+row.row+'" data-tipo="'+row.tipo+'" data-row="'+row.row+'" data-position="top" data-delay="50" data-tooltip="Detalle" onclick="detalle('+row.idVentas+')">\
                                    <i class="material-icons">assignment</i>\
                                  </a>';
                    return html;
                }
            },
            {"data": null,
            	render:function(data,type,row){
            		var html='';
            		if(row.tipo==1){
            			html='Consumibles';
            		}
            		if(row.tipo==2){
            			html='Accesorios';
            		}
            		if(row.tipo==3){
            			html='Equipo';
            		}
            		if(row.tipo==4){
            			html='Refacciones';
            		}
            		
                    return html;
            	}
        	},
            {"data": "piezas"},
            {"data": "modelo"},
            {"data": "serie"},
            {"data": null,
            	render:function(data,type,row){
            		var html='';
            		html=row.nombre+' '+row.apellido_paterno+' '+row.apellido_materno;
            		
                    return html;
            	}
        	},
            {"data": "motivo_dev"},
            {"data": "reg_dev"},
            {"data": null,
            	render:function(data,type,row){
            		var html='';
            		
                    return html;
            	}
        	},
        ],
        "order": [[ 8, "desc" ]],
        "lengthMenu": [[25, 50, 100], [25, 50,100]],
        "searching": false
        
    }).on('draw',function(){
        vericarprerentas();
    });
}
function detalle(id){
    
    window.open(base_url+"Prefactura/view/"+id, "Prefactura", "width=780, height=612");
    
    //window.open(base_url+"Prefactura/view/"+id, "Prefactura", "width=780, height=612");
}
function vericarprerentas(){
    var DATAf  = [];
    $(".vr_0").each(function() {
        item = {};
        item ["tipo"] = $(this).data('tipo');
        item ["row"] = $(this).data('row');
        DATAf.push(item);
    });
    aInfo   = JSON.stringify(DATAf);
    //console.log(aInfo);
    var datos=aInfo;
    
    $.ajax({
        type:'POST',
        url: base_url+"Generales/vericarprerentas",
        data: {
            datos:aInfo
        },
        success: function (data){
            //console.log(data);
            var array = $.parseJSON(data);
            console.log(array);
            $.each(array, function(index, item) {
                    //$(".verificar_"+item.idpre).addClass( "yellowc" );
                if(item.historialid>0){
                    $('.vr_0_'+item.tipo+'_'+item.row).attr('onClick','prefacturah('+item.rentas+','+item.historialid+')');
                    
                }else{
                    $('.vr_0_'+item.tipo+'_'+item.row).attr('onClick','detaller('+item.rentas+')');
                }
                if(item.prefacturaId>0){
                    $('.vr_0_'+item.tipo+'_'+item.row+'_p').html(item.prefacturaId);    
                }
                
                
            });
        }
    });
}
function prefacturah(idrenta,idhistorial){
    window.open(base_url+"RentasCompletadas/viewh/"+idrenta+'/'+idhistorial, "Prefactura", "width=780, height=612");
}
function detaller(id){
    window.open(base_url+"RentasCompletadas/view/"+id+'/1', "Prefactura", "width=780, height=612");
}