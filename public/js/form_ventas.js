// Obtener la BASE URL
var base_url = $('#base_url').val();
var formulario_cotizacion;
var idventatablee=0;
var idventatablec=0;
var idventatablep=0;
var idventatabler=0;
var idventaeaaccc;
var tipocosto=0;
var sol_orden_com = $('#sol_orden_com').val();
var continuar_sin_ser=0;
$(document).ready(function() {
    $('.modal').modal();
    $('.chosen-select').chosen({width: "90%"});
    $('#btn_venta').on('click', function() {
        $("#btn_venta").prop("disabled", true);
        setTimeout(function(){ 
            $("#btn_venta").prop("disabled", false);
        }, 5000);
        var validaciong = validaciongeneral();
        if($('#formulario_cotizacion').valid()==false){
            validaciong=0;
            toastr.error('Debe llenar los campos de requeridos. De lo contrario no podrá seguir creando una venta.');
        }
        if(validaciong>0){
            if(validaciong==1){
                saveventageneral();
            }
            if(validaciong==2){
                $.confirm({
                    boxWidth: '55%',
                    useBootstrap: false,
                    icon: 'fa fa-warning',
                    title: 'Atención!',
                    content: '¿Estas segura de que agregaste consumibles y accesorios?',
                    type: 'red',
                    typeAnimated: true,
                    buttons:{
                        Confirmar: function (){
                            creaventaEquiposr(0);
                        },
                        'Cancelar': function () { }
                    }
                });
            }
        }
    });
    // Dependiendo de lo que vaya a cotizar, es lo que mostraremos como opciones
    $('#ventae').click(function(event) {
        if($('#ventae').is(':checked')){

            //$('#tipoCotizacion').attr('style', 'display: block');
            $('#cotizar').val(1).change();
            //$('#cotizar').trigger("chosen:updated");

            $('.collapsible_venta_e').attr('style', 'display: block');
            $('.collapsible').collapsible('open', 0);
            //setTimeout(function(){ buscador(); }, 1000);
            
        }else{
            //$('#tipoCotizacion').attr('style', 'display: none');

            $('.collapsible_venta_e').attr('style', 'display: none');
        }
        verificartiposventacant();
    });
    $('#ventac').click(function(event) {
        if($('#ventac').is(':checked')){
            
            $('#selectEquiposConsumibles').attr('style', 'display: block');
            $('#opcionesConsumiblesRefacciones').attr('style', 'display: block');
            $('.tipoConsumibles').val(1).change();
            $('.tipoConsumibles').trigger("chosen:updated"); 
            $('#selectToner').attr('style', 'display: block');
             $('#selectTonerp').attr('style','display: block');
             $('#inputCantidad').attr('style', 'display: block');
            $('.collapsible_venta_c').attr('style', 'display: block');
            $('.collapsible').collapsible('open', 1);
        }else{
            $('#selectEquiposConsumibles').attr('style', 'display: none');
            $('#opcionesConsumiblesRefacciones').attr('style', 'display: none');
            $('.collapsible_venta_c').attr('style', 'display: none');
        }
        verificartiposventacant();
        validarpermisosbodega();
    });
    $('#ventar').click(function(event) {
        if($('#ventar').is(':checked')){
            
            $('#opcionesConsumiblesRefacciones').attr('style', 'display: block');
            $('#selectEquiposRefacciones').attr('style', 'display: block');
            $('.tipoConsumibles').val(1).change();
            $('.tipoConsumibles').trigger("chosen:updated"); 
            $('#selectRefaccion').attr('style', 'display: block');
            $('#inputCantidadRefaccion').attr('style', 'display: block');
            $('.collapsible_venta_r').attr('style', 'display: block');
            $('.collapsible').collapsible('open', 2);
        }else{
            $('#opcionesConsumiblesRefacciones').attr('style', 'display: none');

            $('.collapsible_venta_r').attr('style', 'display: none');
        }
        verificartiposventacant();
        validarpermisosbodega();
    });
    $('#ventap').click(function(event) {
        if($('#ventap').is(':checked')){
            
            $('#selectDetallesPolizas').attr('style', 'display: block');
            $('.addpolizass').attr('style','display: block');
            seleccionpoliza();

            $('.collapsible_venta_p').attr('style', 'display: block');
            $('.collapsible').collapsible('open', 3);
        }else{
            $('#selectDetallesPolizas').attr('style', 'display: none');
            $('.addpolizass').attr('style','display: none');
            $('.addpolizas').html('');

            $('.collapsible_venta_p').attr('style', 'display: none');
        }
        verificartiposventacant();
        validarpermisosbodega();
    });
    $('#ventaa').click(function(event) {
        if($('#ventaa').is(':checked')){
            
            $('.collapsible_venta_a').attr('style', 'display: block');
            $('.collapsible').collapsible('open', 4);
            $('#opcionesConsumiblesRefacciones').show('show');
        }else{
            $('#opcionesConsumiblesRefacciones').hide('show');
            $('.collapsible_venta_a').attr('style', 'display: none');
        }
        verificartiposventacant();
        validarpermisosbodega();
    });
    
     // Agrega mas equipos para poliza
    $('#btn_add_equipo_poliza').click(function(event) {
        seleccionpoliza();
    });
     
     // Botón para agregar un nuevo consumible a la cotizacion
     $('#botonAgregarConsumible').on('click', function() {
         agregarConsumible();
         precionewreadonly();
     });

     // Botón para eliminar un consumible de la cotizacion
     $('#botonEliminarConsumible').on('click', function() {
         //eliminarConsumible();
     });

     // Botón para agregar una nueva refaccion a la cotizacion
     $('#botonAgregarRefaccion').on('click', function() {
         agregarRefaccion();
     });

     // Botón para eliminar una refaccion de la cotizacion
     $('#botonEliminarRefaccion').on('click', function() {
         eliminarRefaccion();
     });

     // Basic
     $('.dropify').dropify({
         messages: {
             default: 'Arrastra y suelta un archivo aquí o haz clic',
             replace: 'Arrastre y suelte un archivo o haga clic para reemplazar',
             remove: 'Eliminar',
             error: 'Lo sentimos, el archivo es demasiado grande.'
         }
     });
     // Used events
     var drEvent = $('.dropify-event').dropify();
     drEvent.on('dropify.beforeClear', function(event, element) {
         return confirm("Do you really want to delete \"" + element.filename + "\" ?");
     });
     drEvent.on('dropify.afterClear', function(event, element) {
         alert('File deleted');
     });
     $('.aceptareditarventa').click(function(event) {
            if ($('.bloqueo').length==0) {
                aceptareditarventa();
            }else{
                toastr["error"]("No esta permitido", "Error");
            }
     });
     $('.aceptareditarventarenta').click(function(event) {
         aceptareditarventarenta();
     });
     $('#tables_equipos_selected').dataTable( {
            "pageLength": 170,
            "lengthChange": false,
            "columns": [
                { "searchable": false },
                { "searchable": false },
                null,
                { "searchable": false },
                { "searchable": false },
                { "searchable": false },
                { "searchable": false }
                ] 
            });

     $('#costo_monocromatico').click(function(event) {
        if ($('#costo_monocromatico').is('[readonly]')) {
            tipocosto=1;
            $('#password').val('');
            $('#modaleditor').modal();
            $('#modaleditor').modal('open');
        }
     });
     $('#costo_color').click(function(event) {
        if ($('#costo_color').is('[readonly]')) {
            tipocosto=2;
            $('#password').val('');
            $('#modaleditor').modal();
            $('#modaleditor').modal('open');
        }
     });
     $('.editarcosto').click(function(event) {
        $.ajax({
            type:'POST',
            url: base_url+'Sistema/solicitarpermiso',
            data: {
                pass: $('#password').val()
                },
                async: false,
                statusCode:{
                    404: function(data){
                        swal("Error", "404", "error");
                    },
                    500: function(){
                        swal("Error", "500", "error"); 
                    }
                },
                success:function(data){
                  if (data==1) {
                    //swal("Editado!", "Precio Editado!", "success");
                    if (tipocosto==1) {
                        $("#costo_monocromatico").attr("readonly", false);
                        $("#costo_monocromatico").trigger("touchspin.updatesettings", {max: 999999999999});
                        $("#costo_monocromatico").trigger("touchspin.updatesettings", {min: 0.01});
                    }else{
                        $("#costo_color").attr("readonly", false);
                        $("#costo_color").trigger("touchspin.updatesettings", {max: 999999999999});
                        $("#costo_color").trigger("touchspin.updatesettings", {min: 0.01});
                    }

                  }else{
                    swal("Error", "No tiene permiso", "error"); 
                  }

                }
            });
        $('#password').val('');
     });
    $('.addtabuladorrendimiento').click(function(event) {
        var equipo = $('#tiporendimientoselectede').val();
        var htmlval='';
        if ($('#tiporendimientoselectedm').val()==1) {
            var vm =$('#volumen_monocromatico').val();
            var tm =$('#total_monocromatico').val();
            htmlval+='<div class="row" style="width: 220px">\
                        <div class="col s6">\
                            <label>Volumen Monocromático</label>\
                            <input type="number" value="'+vm+'" id="equipo_r_volm" class="form-control-b" style="width: 100px" readonly>\
                        </div>\
                        <div class="col s6">\
                            <label>Costo Monocromático</label>\
                            <input type="number" value="'+tm+'" id="equipo_r_costm" class="form-control-b" style="width: 100px" readonly>\
                        </div>\
                        </div>';
        }
        if ($('#tiporendimientoselectedc').val()==1) {
            var vc= $('#volumen_color').val();
            var tc= $('#total_color').val();
            htmlval+='<div class="row" style="width: 220px">\
                        <div class="col s6">\
                            <label>Volumen Color</label>\
                            <input type="number" value="'+vc+'" id="equipo_r_volc" class="form-control-b" style="width: 100px" readonly>\
                        </div>\
                        <div class="col s6">\
                            <label>Costo Color</label>\
                            <input type="number" value="'+tc+'" id="equipo_r_costc" class="form-control-b" style="width: 100px" readonly>\
                        </div>\
                        </div>';
        }
        
        $('.equipo_r_'+equipo).html(htmlval);
    });
    $('.tipoventaequipo').change(function(event) {
        if($('.tipoventaequipo option:selected').val()==2){
            //$('.solopararentas').show();
            $('.soloventas').hide();
            deleteser();
        }else{
            $('.solopararentas').hide();
            $('.soloventas').show('show');
        }
    });
    $(".equipo_cantidad").TouchSpin({
                min: 1
            });
    $('.redirigirventas').click(function(event) {
        setTimeout(function(){ 
                    window.location.href = base_url+"index.php/Ventas";
        }, 2000);
    });
    $('.ventaserviciook').click(function(event) {
        if ($('#aceptarcservicio').is(':checked')) {
            $.ajax({
            type:'POST',
            url: base_url+'Ventas/addseriviciovd',
            data: {
                idventa: $('#ventaservicioc').val(),
                poliza: $('#tpoliza2 option:selected').val(),
                servicio: $('#tservicio2 option:selected').val(),
                direccion: $('#tdireccion2 option:selected').val(),
                tipo: $('#ttipo22 option:selected').val(),
                motivo: $('#tserviciomotivo2').val()
                },
                async: false,
                statusCode:{
                    404: function(data){
                        swal("Error", "404", "error");
                    },
                    500: function(){
                        swal("Error", "500", "error"); 
                    }
                },
                success:function(data){
                    setTimeout(function(){ 
                        window.location.href = base_url+"index.php/Ventas";
                    }, 2000);

                }
            });
        }else{
            $.ajax({
                type:'POST',
                url: base_url+'Ventas/addseriviciovd2',
                data: {
                        idventa: $('#ventaservicioc').val(),
                        fecharecoleccion: $('#ventaservicio_fe').val()
                    },
                    async: false,
                    statusCode:{
                        404: function(data){
                            //swal("Error", "404", "error");
                        },
                        500: function(){
                            //swal("Error", "500", "error"); 
                        }
                    },
                    success:function(data){
                       

                    }
                });
            setTimeout(function(){ 
                window.location.href = base_url+"index.php/Ventas";
            }, 2000);
        }
        
    });
    $('.input_cant').click(function(event) {
        console.log('click_check');  
        console.log($(this));
        $(this).find('input[type="text"]').change();
    });
    //validarprefacturaspendientes();
    obtenerpres_pendientes_por_completar();
 });
function verificartiposventacant(){
    var optpcant=0;
    if($('#ventap').is(':checked')){
        if($('#ventap').is(':checked')){ optpcant++;}
        if($('#ventaa').is(':checked')){ optpcant++;}
        if($('#ventar').is(':checked')){ optpcant++;}
        if($('#ventac').is(':checked')){ optpcant++;}
        if($('#ventae').is(':checked')){ optpcant++;}
    }else{
        optpcant=0;
    }
    
}
 // Llamar a la función para cargar datos de los consumibles del equipo seleccionado
 function funcionConsumibles(selectConsumibleDetalle) {
     var aux = selectConsumibleDetalle.closest('.rowConsumibles');
     cargaConsumiblesPorEquipo(aux);
 }
 // Llamar a la función para cargar datos de los consumibles del equipo seleccionado
 function funcionRefacciones(selectRefaccionDetalle) {
     var aux = selectRefaccionDetalle.closest('.rowRefacciones');
     cargaRefaccionesPorEquipo(aux);
 }
 function selectcotiza() {
     var selection = $('#cotizar option:selected').val();
     if (selection == 1) {
         $('.select_familia').css('display', 'none');
         $('.equipos_select').css('display', 'block');
         //buscador();
     } else if (selection == 2) {
         $('.select_familia').css('display', 'block');
         $('.equipos_select').css('display', 'none');
     }
 }
 // Saber si una Familia tiene los 3 integrantes correspondientes o no
 function verificaFamilia(selectFamilias) {
     $.ajax({
         url: base_url + "index.php/Cotizaciones/verificaFamilia/" + selectFamilias.value,
         success: function(response) {
             if (response == 0) {
                 var mensaje = 'La familia seleccionada se encuentra vaciá, seleccione otra con los 3 integrantes exactos.';
                 $('#btn_venta').attr('style', 'display: none');
             } else if (response == 1) {
                 $('#btn_venta').attr('style', 'display: inline-block');
             } else if (response == 2) {
                 var mensaje = 'La familia seleccionada tiene registrados mas de 3 integrantes, seleccione otra con los 3 integrantes exactos.';
                 $('#btn_venta').attr('style', 'display: none');
             } else if (response == 3) {
                 var mensaje = 'La familia seleccionada tiene registrados menos de 3 integrantes, seleccione otra con los 3 integrantes exactos.';
                 $('#btn_venta').attr('style', 'display: none');
             }
             if (response != 1) {
                 alertfunction('Atención!',mensaje);
             }
         },
         error: function(response) {
            notificacion(1);
         }
     });
 }
 // Clonar el DIV de la selección de un Consumible
 function agregarConsumible() {
     var contadorConsumibles = $('#contadorConsumibles').val();
     var contadorConsumiblesAux = contadorConsumibles;
     contadorConsumibles++;

     $('#contadorConsumibles').val(contadorConsumibles);
     $('.chosen-select').chosen('destroy');

     var $template = $('#rowConsumibles1'),
         $clone = $template
         .clone()
         .show()
         .attr('data-conrow', contadorConsumibles)
         .removeAttr('id')
         .attr('id', "rowConsumibles" + contadorConsumibles)
         .insertAfter("#rowConsumibles" + contadorConsumiblesAux)
         
         //.append('')
     $clone.find("select").val("");

     $('.rowConsumibles .precionew').prop('readonly', true);
     $('.chosen-select').chosen({
         width: "90%"
     });
     $('#rowConsumibles'+contadorConsumibles+' .piezas').val(1);
     //=============================================
     var rowcom = $('.collapsible_venta_c .collapsible-body > .rowConsumibles');;
     rowcom.each(function(){  
        var conrow = $(this).data('conrow');
        $(this).find("a[id*='botonEliminarConsumible']").attr("onclick","deletevconsu("+conrow+")");
        $(this).find("a[id*='addh_consumible_equipo']").attr("onclick","addh_consumible_equipo("+conrow+")");
     });
 }
 function deletevconsu(id){
    $('#rowConsumibles'+id).remove();
 }
 // Elimiinar un registro clonado de un consumible
 function eliminarConsumible() {
     var contadorConsumibles = $('#contadorConsumibles').val();

     if (contadorConsumibles > 1) {
         $('#rowConsumibles' + contadorConsumibles).remove();
         contadorConsumibles--;
         $('#contadorConsumibles').val(contadorConsumibles);
     } else if (contadorConsumibles == 1) {
        alertfunction('Atención!','Debe tener por lo menos un consumible seleccionado');
     }
 }
 // Clonar el DIV de la selección de una refaccion
 function agregarRefaccion() {
     var contadorRefacciones = $('#contadorRefacciones').val();
     var contadorRefaccionesAux = contadorRefacciones;
     contadorRefacciones++;

     $('#contadorRefacciones').val(contadorRefacciones);
     $('.chosen-select').chosen('destroy');

     var $template = $('#rowRefacciones1'),
         $clone = $template
         .clone()
         .show()
         .removeAttr('id')
         .attr('id', "rowRefacciones" + contadorRefacciones)
         .insertAfter("#rowRefacciones" + contadorRefaccionesAux)
         //.append('')
     $clone.find("select").val("");
     $('.chosen-select').chosen({
         width: "100%"
     });
 }
 // Elimiinar un registro clonado de una refaccion
 function eliminarRefaccion() {
     var contadorRefacciones = $('#contadorRefacciones').val();

     if (contadorRefacciones > 1) {
         $('#rowRefacciones' + contadorRefacciones).remove();
         contadorRefacciones--;
         $('#contadorRefacciones').val(contadorRefacciones);
     } else if (contadorRefacciones == 1) {
         alertfunction('Atención!','Debe tener por lo menos una refacción seleccionado');
     }
 }
 // Cargar por AJAX los consumibles que pertenencen a un equipo
 function cargaConsumiblesPorEquipo(aux) {
     var idDiv = aux.attr("id");
     var idEquipo = aux.find('.equipoListaConsumibles').val();
     if(idEquipo>0){}else{idEquipo=0;}
     var idbodegac = aux.find('.selectbodegacon').val();
     console.log(idbodegac);

     $.ajax({
         url: base_url + "index.php/Cotizaciones/getDatosConsumiblesPorEquipo/" + idEquipo+'/'+idbodegac,
         success: function(response) {
             aux.find('.toner').html('');
             aux.find('.toner').html(response);
             $('.toner').trigger("chosen:updated");
         },
         error: function(response) {
             notificacion(1);
         }
     });
 }
 // Cargar por AJAX los consumibles que pertenencen a un equipo
 function cargaRefaccionesPorEquipo(aux) {
     var idDiv = aux.attr("id");
     var idEquipo = aux.find('.equipoListaRefacciones').val();

     $.ajax({
         url: base_url + "index.php/Cotizaciones/getDatosRefaccionesPorEquipo/" + idEquipo,
         success: function(response) {
             aux.find('.refaccion').html('');
             aux.find('.refaccion').html(response);
             $('.refaccion').trigger("chosen:updated");
         },
         error: function(response) {
             notificacion(1);
         }
     });
 }
 function validaciones(eleccionCotizar) {
     var retorno = 1;
     if ($('#atencion').val().trim() == '' || $('#correo').val().trim() == '' || $('#telefono').val().trim() == '') {
         alertfunction('Atención!','Debe llenar los campos de "Atención", "Correo" y "Teléfono". De lo contrario no podrá seguir creando una venta.');
         retorno = 0;
     } else {
         switch (eleccionCotizar) {
             case 1:
                 console.log('Validaciones de equipos');
                 if ($('.tipoventaequipo').val() == '' || $('.tipoventaequipo').val() == null) {
                     alertfunction('Atención!','Seleccione que tipo de venta de equipos se realizará.');
                     retorno = 0;
                 } else if ($('#cotizar').val() == '' || $('#cotizar').val() == null) {
                     alertfunction('Atención!','Seleccione que forma de venta de equipos se realizará.');
                     retorno = 0;
                 } else if ($('#cotizar').val() == 2 && $('#idfamilia').val() == null) {
                     alertfunction('Atención!','Seleccione la familia.');
                     retorno = 0;
                 } else if ($('#cotizar').val() == 1) {
                     var addtp = 0;
                     $(".nombre:checked").each(function() {
                         addtp++;
                     });
                     if (addtp == 0) {
                         alertfunction('Atención!','Favor de selecionar por lo menos un equipos');
                         retorno = 0;
                     }
                     /*
                     else if (addtp > 3) {
                         alertfunction('Atención!','Favor de selecionar tres equipos');
                         retorno = 0;
                     } else if (addtp == 3) {
                         console.log('Éxito');
                     }
                     */
                 }
                 break;
             case 2:
                 console.log('Validaciones de consumibles');
                 if ($('#opcionesConsumiblesRefacciones').find('.tipoConsumibles').val() == '' || $('#opcionesConsumiblesRefacciones').find('.tipoConsumibles').val() == null) {
                     alertfunction('Atención!','Seleccione que tipo de venta de consumibles se realizará.');
                     retorno = 0;
                 } /*else if ($('.equipoListaConsumibles').val() == '' || $('.equipoListaConsumibles').val() == null) {
                     alertfunction('Atención!','Seleccione para que equipo se busca el consumible.');
                     retorno = 0;
                 } */else if ($('.toner').val() == '' || $('.toner').val() == null) {
                     alertfunction('Atención!','Seleccione el consumible deseado.');
                     retorno = 0;
                 } else if ($('.piezas').val() == '' || $('.piezas').val() == null || $('.piezas').val() == 0) {
                     alertfunction('Atención!','Ingrese la cantidad de piezas del consumible.');
                     retorno = 0;
                 }

                 break;
             case 3:
                 console.log('Validaciones de refacciones');
                 if ($('#opcionesConsumiblesRefacciones').find('.tipoConsumibles').val() == '' || $('#opcionesConsumiblesRefacciones').find('.tipoConsumibles').val() == null) {
                     alertfunction('Atención!','Seleccione que tipo de venta de Refacciones se realizará.');
                     retorno = 0;
                 } /*else if ($('.equipoListaRefacciones').val() == '' || $('.equipoListaRefacciones').val() == null) {
                     alertfunction('Atención!','Seleccione para que equipo se busca la refaccion.');
                     retorno = 0;
                 } else if ($('.refaccion').val() == '' || $('.refaccion').val() == null) {
                     alertfunction('Atención!','Seleccione la refacción deseada.');
                     retorno = 0;
                 } else if ($('.piezasRefacion').val() == '' || $('.piezasRefacion').val() == null || $('.piezasRefacion').val() == 0) {
                     alertfunction('Atención!','Ingrese la cantidad de piezas de la refacción.');
                     retorno = 0;
                 }*/

                 break;
             case 4:
                 console.log('Validaciones de polizas');
                 break;
             case 5:
                 if($("#table_venta_accesorios tbody > tr").length==0){
                    retorno = 0;
                 }
                 break;
             default:
                 break;
         }
     }
     return retorno;
 }
function addaccesorios(id){
    
    $('#modalaccesorios').modal('open');
    $("#costo_monocromatico").TouchSpin({
                min: 0.25,
                max: 0.25,
                initval: 0.25,
                step: 0.01,
                decimals: 2,
            });
    $("#costo_color").TouchSpin({
                min: 2.5,
                max:2.5,
                initval: 2.5,
                step: 0.01,
                decimals: 2,
            });
    $.ajax({
        type:'POST',
        url: $('#base_url').val()+'Equipos/accesorios',
        data: {
            id: id,  
        },
        async: false,
        statusCode:{
            404: function(data){
                alertfunction('Atención!','Error 404');
            },
            500: function(data){
                    alertfunction('Atención!','Error 500');
                }
        },
        success:function(data){
            console.log(data);
            var array = $.parseJSON(data);
            $('.modaltableaccesorios').html(array.accesorios);
            $('.modaltableconsu').html(array.consumibles);
            $('.tablemaccesorios').DataTable();
            $('.tableconsu').DataTable();
            console.log(array.rendimiento.r_mono)
            $("#volumen_monocromatico").TouchSpin({
                min: array.rendimiento.r_mono,
                max:99999999999999999999999999,
                initval: array.rendimiento.r_mono,
                //step: 0.1,
                decimals: 2,
                boostat: 5,
                maxboostedstep: 10
            }).change(function(event) {
                var vol_mon = $('#volumen_monocromatico').val();
                var costo_mon = $('#costo_monocromatico').val();
                var total_mon = parseFloat(vol_mon)*parseFloat(costo_mon);
                $('#total_monocromatico').val(total_mon);
            });
            $("#volumen_monocromatico").trigger("touchspin.updatesettings", {min: array.rendimiento.r_mono});
            $("#volumen_color").TouchSpin({
                min: array.rendimiento.r_color,
                max:99999999999999999999999999,
                initval: array.rendimiento.r_color,
                //step: 0.1,
                decimals: 2,
                boostat: 5,
                maxboostedstep: 10
            }).change(function(event) {
                var vol_mon = $('#volumen_color').val();
                var costo_mon = $('#costo_color').val();
                var total_mon = parseFloat(vol_mon)*parseFloat(costo_mon);
                $('#total_color').val(total_mon);
            });
            $("#volumen_color").trigger("touchspin.updatesettings", {min: array.rendimiento.r_color});
            $('#tiporendimientoselectedm').val(array.rendimiento.mono);
            $('#tiporendimientoselectedc').val(array.rendimiento.color);
             $('#tiporendimientoselectede').val(id);
        }
    });
}
var addaccesoriorow=0;
function addaccesorio(ide,ida,name,bloqueo){
    if (bloqueo==1) {
        var sbloqueo='bloqueo';
        toastr["error"]("No hay suficiente stock", "Alerta!");
    }else{
        var sbloqueo='';
    }
    var tipoventar = $('.tipoventaequipo option:selected').val();
    if(tipoventar==2){
        var solorentas="";
    }else{
        var solorentas="style='display:none'";
    }
    var accesorio = '<div class="row accesorioadd_'+ide+'_'+ida+'_'+addaccesoriorow+' '+sbloqueo+'">\
                        <div class="col s3 solorentas " '+solorentas+'>\
                            <input type="number" id="accesoriocantidadn" value="1" class="form-control-bmz">\
                        </div>\
                        <div class="col s7">\
                            <input type="hidden" id="equiposelected" value="'+ide+'">\
                            <input type="hidden" id="accesorioselected" value="'+ida+'">'+name+'\
                        </div>\
                        <div class="col s2">\
                        <a onclick="deleteaccesorio('+ide+','+ida+','+addaccesoriorow+')"><i class="material-icons">delete_forever</i></a>\
                        </div>\
                    </div>';
    $('.equipo_s_'+ide).append(accesorio);
    addaccesoriorow++;
}
var addconsurow=0;
function addconsu(ide,id,name,bloqueo){
    if (bloqueo==1) {
        var sbloqueo='bloqueo';
        toastr["error"]("No hay suficiente stock", "Alerta!");
    }else{
        var sbloqueo='';
    }
    var tipoventar = $('.tipoventaequipo option:selected').val();
    if(tipoventar==2){
        var solorentas="";
    }else{
        var solorentas="style='display:none'";
    }
    var accesorio = '<div class="row consumibleadd_'+ide+'_'+id+'_'+addconsurow+' '+sbloqueo+'">\
                        <div class="col s3 solorentas " '+solorentas+'>\
                            <input type="number" id="consumiblescantidadn" value="1" class="form-control-bmz">\
                        </div>\
                        <div class="col s8">\
                            <input type="hidden" id="equiposelectedc" value="'+ide+'">\
                            <input type="hidden" id="consumibleselected" value="'+id+'">'+name+'\
                        </div>\
                        <div class="col s2">\
                        <a onclick="deleteconsu('+ide+','+id+','+addconsurow+')"><i class="material-icons">delete_forever</i></a>\
                        </div>\
                    </div>';
    $('.equipo_c_'+ide).append(accesorio);
    addconsurow++;
}
function deleteaccesorio(ide,ida,row){
    $('.accesorioadd_'+ide+'_'+ida+'_'+row).remove();
}
function deleteconsu(ide,ida,row){
    $('.consumibleadd_'+ide+'_'+ida+'_'+row).remove();
}

function funcionConsumiblesprecios(selected){
    var aux = selected.closest('.rowConsumibles');
    cargaConsumiblesPorEquipop(aux);
}
function cargaConsumiblesPorEquipop(aux){
    var idDiv = aux.attr("id");
    var toner = aux.find('.toner').val();
    var bode = aux.find('.selectbodegacon option:selected').val();
    var aaa = $('#aaa').val();
    if(bode>0){
        $.ajax({
            url: base_url+"index.php/Cotizaciones/getDatosConsumiblesPorEquipop/"+toner,
            success: function (response){
                console.log('cargaConsumiblesPorEquipop1');
                var array = $.parseJSON(response); 
                aux.find('.tonerp').html('');
                aux.find('.tonerp').html(array.select_option);
                console.log(array.select_option);
                if(aaa==1){
                    aux.find('.tonerp').val(array.espe).trigger('chosen:updated');
                }else{
                    aux.find('.tonerp').trigger("chosen:updated");    
                }
            },
            error: function(response){
                notificacion(1);
            }
        }); 
    }else{
        notificacion(5);
    }
    
}
function funcionConsumiblespreciosv(selected){

    var aux = selected.closest('.rowConsumibles');
    console.log(aux);
    var idDiv = aux.attr("id");
    console.log(idDiv);
    var toner = aux.find('.tonerp').val();
    console.log(toner);
    aux.find('.precionew').val('');
    aux.find('.precionew').val(toner);
}
// ================================= nuevas funciones ===============================================================================================
var spoliza=1;
function seleccionpoliza(){
    var idpersonal = $('#idpersonal').val();
    $('#selectEquipos #equipoLista').html();
        var equipospoliza='<div class="col s12 m12 12 polizaselected_row_'+spoliza+'">\
                                <div class="col s10" style="margin-top:10px;">\
                                    <div class="col s2" style="margin-left: -12px;">\
                                        <label style="position:absolute;left: 43px;">Equipos registrados</label>\
                                        <a class="btn-bmz material-icons prefix" onClick="addserieshisto('+spoliza+')" title="Equipos registrados" style="margin-top: 18px;">format_list_bulleted</a>\
                                        <label for="estado">Equipos</label>\
                                    </div>\
                                    <div class="col s3 margenTopDivChosen" >\
                                        <select id="equipoListas_'+spoliza+'" class="browser-default equipoListaselected chosen-select" onchange="polizaseleccionada('+spoliza+')">\
                                            '+$('#selectEquipos #equipoLista').html()+'\
                                        </select>\
                                    </div>\
                                     <div class="col s2" style="height: 40px;">\
                                        <p><input type="checkbox" id="sinmodelo_'+spoliza+'"  value="1" onclick="sinmodelo('+spoliza+')">\
                                        <label for="sinmodelo_'+spoliza+'">Sin modelo</label></p>\
                                    </div>\
                                    <div class="input-field col s3">\
                                        <input id="polizaserie_'+spoliza+'" type="text" class="validate">\
                                        <label for="polizaserie_'+spoliza+'">Serie</label>\
                                    </div>\
                                    <div class="col s2">\
                                        <a class="btn-floating waves-effect waves-light red lightrn-1 btn tooltipped" data-position="top" data-tooltip="Eliminar"  data-tooltip-id="4f233d2f-8677-72fd-0179-9a198bed22f6" onclick="deleteequipo('+spoliza+')">\
                                            <i class="material-icons">delete</i>\
                                        </a>\
                                    </div>\
                                </div>\
                                <div class="col s12 m12 12">\
                                    <div class="col s4">\
                                        <label for="estado">Pólizas Existentes</label>\
                                        <div class="col s12 margenTopDivChosen">\
                                            <select id="polizasLista_'+spoliza+'" class="browser-default chosen-select " onchange="polizaseleccionada('+spoliza+')" >\
                                                '+$('#selectPolizas #polizasLista').html()+'\
                                            </select>\
                                        </div>\
                                    </div>\
                                    <div class="input-field col s4" id="selectDetallesPolizas" style="display:none">\
                                        <i class="material-icons prefix">person</i>\
                                        <label for="estado">Modelos de Póliza</label><br><br>\
                                        <div class="col s12 margenTopDivChosen">\
                                            <select id="detallesPoliza_'+spoliza+'" name="detallesPoliza" class="browser-default chosen-select" onchange="polizalfm('+spoliza+')"></select>\
                                        </div>\
                                    </div>\
                                    <div class="col s3" id="selectDetallesPolizas2">\
                                        <label for="estado">Costo</label>\
                                        <div class="col s12 margenTopDivChosen">';
                                            //if(idpersonal==63 || idpersonal==1){
                                            if(idpersonal==63){
                                            
                                                equipospoliza+='<input type="number" id="detallesPolizacosto_'+spoliza+'" name="detallesPolizacosto" class="form-control-bmz detallesPolizacosto">';
                                            }else{
                                                equipospoliza+='<select id="detallesPolizacosto_'+spoliza+'" name="detallesPolizacosto" class="browser-default form-control-bmz detallesPolizacosto" >\
                                                            <option value="local">Local</option>\
                                                            <option value="semi">Semi</option>\
                                                            <option value="foraneo">Foraneo</option>\
                                                            <option value="especial">Especial</option>\
                                                            <option value="0" class="costocero">0</option>\
                                                        </select>';

                                            }
                        equipospoliza+='</div>\
                                    </div>\
                                    <div class="col s3">\
                                        <label for="estado">Problema</label>\
                                        <div class="col s12 margenTopDivChosen">\
                                            <textarea class="form-control-bmz" id="motivopoliza_'+spoliza+'"></textarea>\
                                        </div>\
                                    </div>\
                                    <div class="col s1"><br><br>\
                                        <a class="btn-bmz waves-effect waves-light purple lightrn-1 tooltipped addpolizatable" data-position="top" data-tooltip="Agregar"  onclick="addpolizatable('+spoliza+')">Agregar <i class="far fa-arrow-alt-circle-down"></i></a>\
                                    </div>\
                                </div>\
                                <div class="col s12 m12 12 equipotablepoliza__'+spoliza+'">\
                                    <table class="col s12 m12 12 tablepolizas Highlight striped"><tbody></tbody></table>\
                                </div>\
                            </div>';
    $('.addpolizas').append(equipospoliza);
    $('.chosen-select').chosen({width: "100%"});
    spoliza++;
}
function polizalfm(idrow){
    var local = $('#detallesPoliza_'+idrow+' option:selected').data('local');
    var semi = $('#detallesPoliza_'+idrow+' option:selected').data('semi');
    var foraneo  = $('#detallesPoliza_'+idrow+' option:selected').data('foraneo');
    var especial  = $('#detallesPoliza_'+idrow+' option:selected').data('especial');

    $("#detallesPolizacosto_"+idrow+" [value='local']").text('Local ('+local+')');
    $("#detallesPolizacosto_"+idrow+" [value='semi']").text('Semi ('+semi+')');
    $("#detallesPolizacosto_"+idrow+" [value='foraneo']").text('Foraneo ('+foraneo+')');
    $("#detallesPolizacosto_"+idrow+" [value='especial']").text('Especial ('+especial+')');
    $("#detallesPolizacosto_"+idrow).trigger("chosen:updated");
}
function deleteequipo(id){
    $('.polizaselected_row_'+id).remove();
}
function deletepolisadd(id){
    $('.polizaselecteddd_row_'+id).remove();
    $('.addpolizatable').show();
    $('#btn_add_equipo_poliza').show();
    
}
var polizaseleccionadapasa=1;
function polizaseleccionada(id){
    var polizaid=$('#polizasLista_'+id+' option:selected').val();
    if(polizaid>0){
        $.ajax({
            url: base_url+"index.php/Cotizaciones/getDatosPolizaporId/"+polizaid,
            success: function (response){
                console.log(response);
                $('#detallesPoliza_'+id).html('');
                $('#detallesPoliza_'+id).html(response);
                $('#detallesPoliza_'+id).trigger("chosen:updated");
                var equipo = $('#equipoListas_'+id+' option:selected').val();
                setTimeout(function(){ 
                    $('#detallesPoliza_'+id+' [data-modelo='+equipo+']').attr('selected',true); 
                    $('#detallesPoliza_'+id).trigger("chosen:updated");
                    polizalfm(id);
                    setTimeout(function(){
                        var polizaselected=$('#detallesPoliza_'+id+' option:selected').text(); 
                        if (parseInt(equipo)!=parseInt(polizaselected)) {
                            alertfunction('Atención!','No existe poliza para el equipo seleccionado'); 
                            polizaseleccionadapasa=0;
                        }else{
                            polizaseleccionadapasa=1;
                        }
                    }, 1100);
                    verificarseriederentas(id);
                }, 1000);
            },
            error: function(response){
                notificacion(1); 
            }
        });
    }
}
var spolizadd=1;
function addpolizatable(id){
    polizaseleccionada(id);
    var equipo = $('#equipoListas_'+id+' option:selected').val();
    var equipo_text = $('#equipoListas_'+id+' option:selected').text();
    var serie = $('#polizaserie_'+id).val();
    var polizaval = $('#polizasLista_'+id+' option:selected').val();
    var polizatext = $('#polizasLista_'+id+' option:selected').text();
    var poliza_visitas = $('#polizasLista_'+id+' option:selected').data('visitas');
    var polizadval = $('#detallesPoliza_'+id+' option:selected').val();
    var polizadtext = $('#detallesPoliza_'+id+' option:selected').text();
    var polizacval = $('#detallesPolizacosto_'+id+' option:selected').val();
    var polizactext = $('#detallesPolizacosto_'+id+' option:selected').text();
    if(polizacval==undefined){
        var polizacval = $('#detallesPolizacosto_'+id).val();
        var polizactext =polizacval;
    }
    var polizamotivo = $('#motivopoliza_'+id+'').val();
    console.log(polizaseleccionadapasa);
    if(parseInt(polizaseleccionadapasa)==1){
        if(polizaval>0){
            if(equipo>0){
                var tablerow='<tr class="polizaselecteddd_row_'+spolizadd+'">\
                                <td>\
                                    <input type="hidden" id="equipoposelected" value="'+equipo+'">\
                                    <input type="hidden" id="polizaval" value="'+polizaval+'">'+polizatext+' \
                                    <input type="hidden" id="polizavalserie" value="'+serie+'">\
                                </td>\
                                <td><input type="hidden" id="polizadval" value="'+polizadval+'">'+equipo_text+' '+serie+'\
                                </td>\
                                <td><input type="hidden" id="polizacval" value="'+polizacval+'">'+polizactext+'\
                                </td>\
                                <td><input type="hidden" id="polizamotivo" value="'+polizamotivo+'">'+polizamotivo+'\
                                </td>\
                                <td><a onclick="deletepolisadd('+spolizadd+')"style="cursor: pointer;"><i class="material-icons" style="color: red;">delete_forever</i></a>\
                                </td>\
                              </tr>';
                $('.equipotablepoliza__'+id+' .tablepolizas tbody').append(tablerow);
                spolizadd++;
                if(poliza_visitas>1){
                    $('.addpolizatable').hide();
                    $('#btn_add_equipo_poliza').hide();
                }
            }else{
                alertfunction('Atención!','Seleccione un equipo ó sin modelo'); 
            }
        }else{
            /*
            $('#polizasLista_1_chosen a').css({"border": "1px solid #c53333"});
            setTimeout(function(){ 
            $('#polizasLista_1_chosen a').css({"border": "1px solid #cccccc"}); }, 1000);
            */
            alertfunction('Atención!','Seleccione una Póliza'); 
        }
    }
}
function modalventasdetalle(idventa,vtipo){
    console.log('modalventasdetalle('+idventa+','+vtipo+')');
    $('#modalupdateventa').modal({
        dismissible: false
    });
    $('#modalupdateventa').modal('open');
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Ventas/viewventa",
        data: {
            idventa:idventa
        },
        success: function (response){
           $('.venta_detalles_view').html(response);
            /*
             setTimeout(function(){ 
                    //window.open(base_url+"index.php/Ventas"); 
                    //window.location.href = base_url+"index.php/Ventas";
                }, 2000);
            */     
           validarpermisosbodega();
        },
        error: function(response){
            notificacion(1); 
        }
    });
} 
function aceptareditarventa(){
    var idCliente = $('#idCliente').val();
    //================================
        var DATAe  = [];
        var TABLA   = $("#venta_equipos tbody > tr");
            TABLA.each(function(){         
            item = {};
            item ["id"] = $(this).find("input[id*='id']").val();
            item ["cantidad"]   = $(this).find("input[id*='cantidad']").val();
            item ["serie"]  = $(this).find("select[id*='serie_bodega']").val();
            DATAe.push(item);
        });
        INFO  = new FormData();
        arrayequipos   = JSON.stringify(DATAe);
    //================================
    //================================
        var DATAa  = [];
        var TABLA   = $("#venta_accesorios tbody > tr");
            TABLA.each(function(){         
            item = {};
            item ["id_accesorio"] = $(this).find("input[id*='id_accesorio']").val();
            item ["cantidad"]   = $(this).find("input[id*='cantidad']").val();
            item ["serie"]  = $(this).find("select[id*='serie_bodega']").val();
            DATAa.push(item);
        });
        INFO  = new FormData();
        arrayaccesorios   = JSON.stringify(DATAa);
    //================================
    //================================
        var DATAc  = [];
        var TABLA   = $("#venta_consumibles tbody > tr");
            TABLA.each(function(){         
            item = {};
            item ["id"] = $(this).find("input[id*='id_accesorio']").val();
            item ["id_consu"] = $(this).find("input[id*='id_consu']").val();
            item ["cantidad"]   = $(this).find("input[id*='cantidad']").val();
            item ["bodega"]  = $(this).find("select[id*='serie_bodega']").val();
            DATAc.push(item);
        });
        INFO  = new FormData();
        arrayconsumibles   = JSON.stringify(DATAc);


    //================================
        if($('#aceptareservicio').is(':checked')){
            var servicio = $('#aceptareservicio').val();
        }else{
            var servicio =0;
        }
        var sidventa = $('#aceptareservicio').val();
    //====================================
    var polizae=$('#tpoliza2e option:selected').val();
    var servicioe=$('#tservicio2e option:selected').val();
    var direccione=$('#tdireccion2e option:selected').val();
    var tipoe=$('#ttipo2e option:selectede').val();
    var motivoe=$('#tserviciomotivo2e').val();
    var fechae=$('#ventaeservicio_fe').val();
    var datos = 'arrayequipos='+arrayequipos+'&arrayaccesorios='+arrayaccesorios+'&arrayconsumibles='+arrayconsumibles+'&servicio='+servicio+'&polizae='+polizae+'&servicioe='+servicioe+'&direccione='+direccione+'&tipoe='+tipoe+'&motivoe='+motivoe+'&fechae='+fechae+'&sidventa='+sidventa;
    if(fechae!=''){
        $.ajax({
            type:'POST',
            url: base_url+"index.php/Ventas/editarventacs",
            data: datos,
            success: function (response){
                alertfunction('Éxito!','Se ha modificado la venta');         
                setTimeout(function(){ 
                    window.location.href =base_url+"index.php/Ventas"; 
                }, 2000);/*
                setTimeout(function(){ 
                    window.location.href = base_url+"index.php/Cotizaciones/listaCotizacionesPorCliente/"+idCliente; 
                }, 2000);
                */
            },
            error: function(response){
                notificacion(1); 
            }
        });
    }else{
        alertfunction('Atención!','Favor de agregar una fecha de entrega'); 
    }
}
function aceptareditarventarenta(){
    var idCliente = $('#idCliente').val();
    var entraupdate=1;
    //================================
        var DATAe  = [];
        var TABLA   = $("#venta_equiposrenta tbody > tr");
            TABLA.each(function(){         
            item = {};
            item ["id"] = $(this).find("input[id*='id']").val();
            item ["cantidad"]   = $(this).find("input[id*='cantidad']").val();
            item ["serie"]  = $(this).find("select[id*='serie_bodega'] option:selected").val();
            if($(this).find("select[id*='serie_bodega'] option:selected").data('stock')==0){
                entraupdate=0;
            }
            DATAe.push(item);
        });
        INFO  = new FormData();
        arrayequipos   = JSON.stringify(DATAe);
            //===============================
        var DATAa  = [];
        var TABLA   = $("#venta_accesorios tbody > tr");
            TABLA.each(function(){         
            item = {};
            item ["id_accesorio"] = $(this).find("input[id*='id_accesorio']").val();
            item ["cantidad"]   = $(this).find("input[id*='cantidad']").val();
            item ["serie"]  = $(this).find("select[id*='serie_bodega'] option:selected").val();
            if($(this).find("select[id*='serie_bodega'] option:selected").data('stock')==0){
                entraupdate=0;
            }
            DATAa.push(item);
        });
        INFO  = new FormData();
        arrayaccesorios   = JSON.stringify(DATAa);
    //================================
    //================================
        var DATAc  = [];
        var TABLA   = $("#venta_consumibles tbody > tr");
            TABLA.each(function(){         
            item = {};
            item ["id"] = $(this).find("input[id*='id_accesorio']").val();
            item ["cantidad"]   = $(this).find("input[id*='cantidad']").val();
            item ["bodega"]  = $(this).find("select[id*='serie_bodega'] option:selected").val();
            item ["id_consu"] = $(this).find("input[id*='id_consu']").val();
            if($(this).find("select[id*='serie_bodega'] option:selected").data('stock')==0){
                entraupdate=0;
            }
            DATAc.push(item);
        });
        INFO  = new FormData();
        arrayconsumibles   = JSON.stringify(DATAc);
    //================================
    var datos = 'arrayequipos='+arrayequipos+'&arrayaccesorios='+arrayaccesorios+'&arrayconsumibles='+arrayconsumibles;
    //================================
    if(entraupdate==1){
        $.ajax({
            type:'POST',
            url: base_url+"index.php/Ventas/editarventacsrenta",
            data: datos,
            success: function (response){
                alertfunction('Éxito!','Se han creado solicitudes para asignación de números de serie');          
                setTimeout(function(){ 
                    window.location.href =base_url+"index.php/Rentas"; 
                }, 2000);/*
                setTimeout(function(){ 
                    window.location.href = base_url+"index.php/Cotizaciones/listaCotizacionesPorCliente/"+idCliente; 
                }, 2000);
                */
                
            },
            error: function(response){
                notificacion(1);
            }
        });
    }else{
        alertfunction('Atención!','Una de las bodegas no tiene stock');
    }
}
/*
function funcionConsumiblesprecios(selected){
    var aux = selected.closest('.rowConsumibles');
    cargaConsumiblesPorEquipop(aux);
}
*/
function cargaConsumiblesPorEquipop(aux){
    var idDiv = aux.attr("id");
    var toner = aux.find('.toner').val();
    var aaa = $('#aaa').val();
    var bode = aux.find('.selectbodegacon option:selected').val();
    if(bode>0){
        $.ajax({
            url: base_url+"index.php/Cotizaciones/getDatosConsumiblesPorEquipop/"+toner,
            success: function (response){
                console.log('cargaConsumiblesPorEquipop2');
                var array = $.parseJSON(response); 
                aux.find('.tonerp').html('');
                aux.find('.tonerp').html(array.select_option).change();
                if(aaa==1){
                    aux.find('.tonerp').val(array.espe);
                    aux.find('.precionew').val('');
                    aux.find('.precionew').val(array.espe);
                }else{
                    //aux.find('.tonerp').trigger("chosen:updated");    
                }
            },
            error: function(response){
                notificacion(1);
            }
        }); 
        alertastocktoner(toner,aux);
    }else{
        notificacion(5);
    }
}
function funcionConsumiblesprecioscantidad(selected){
    var aux = selected.closest('.rowConsumibles');
    cargaConsumiblesPorEquipopcantidad(aux);
}
function cargaConsumiblesPorEquipopcantidad(aux){
    var idDiv = aux.attr("id");
    var toner = aux.find('.toner').val();
    var aaa = $('#aaa').val();
    var bode = aux.find('.selectbodegacon option:selected').val();
    if(bode>0){
       
        alertastocktoner(toner,aux);
    }else{
        notificacion(5);
    }
}
/*
function creaventaEquipos(vtipo){
    var datos = formulario_cotizacion.serialize();
    var rentas_detalle_modal = $('.tipoventaequipo option:selected').val();
    var tipoprecioequipo = $('#tipoprecioequipo option:selected').val();
    var pasa=1;
    var nombre = [];
    var TABLA   = $("#tables_equipos_selected tbody > tr");
    TABLA.each(function(){  
        if ($(this).find("input[class*='equipo_check']").is(':checked')) {
            item = {};
            item['equipos'] = $(this).find("input[class*='equipo_check']").val();
            item['cantidad'] = $(this).find("input[id*='equipo_cantidad']").val();
            
            var equiposs = $(this).find("input[class*='equipo_check']").val();
            var equiposconsumiblelength=$('.equipo_c_'+equiposs+' > div').length;
            if (equiposconsumiblelength==0) {
              //  pasa=0;// se quito esta validacion por que puede que el equipo no tenga consumibles
            }
            nombre.push(item);
        }else{
            var equiposs = $(this).find("input[class*='equipo_check']").val();
            $('.equipo_c_'+equiposs).html('');
            $('.equipo_s_'+equiposs).html('');
        }
    });
    aInfoe   = JSON.stringify(nombre);
    //===========================================
        var TABLAa   = $(".equipo_accessoriosselected > div");
        var DATAa  = [];
            TABLAa.each(function(){         
                item = {};
                item ["equiposelected"]   = $(this).find("input[id*='equiposelected']").val();
                item ["cantidad"]   = $(this).find("input[id*='accesoriocantidadn']").val();
                item ["accesorioselected"]  = $(this).find("input[id*='accesorioselected']").val();
                DATAa.push(item);
            });
            INFOa  = new FormData();
            aInfoa   = JSON.stringify(DATAa);

        var TABLAc   = $(".equipo_consumibleselected > div");
        var DATAc  = [];
            TABLAc.each(function(){         
                item = {};
                item ["equiposelected"]   = $(this).find("input[id*='equiposelectedc']").val();
                item ["consumibleselected"]  = $(this).find("input[id*='consumibleselected']").val();
                DATAc.push(item);
            });
            INFOc  = new FormData();
            aInfoc   = JSON.stringify(DATAc);
    //===========================================

    datos = datos+'&equiposIndividuales='+aInfoe+'&accesorios='+aInfoa+'&consumibles='+aInfoc+'&combinada='+vtipo+'&tipoprecioequipo='+tipoprecioequipo;
    var tipoDocumento = '';
    var idCliente = $('#idCliente').val();
    console.log('idCliente: '+idCliente);
    if(pasa == 1){
        $.ajax({
            type:'POST',
            url: base_url+"index.php/Ventas/nuevaventaequipos",
            data: datos,
            success: function (response){
                var idresponse=parseInt(response);
                idventatablee=parseInt(response);
                idventaeaaccc=parseInt(idventatablee);
                console.log(idventatablee);
                if(vtipo==0){
                    alertfunction('Éxito!','Se ha creado la venta: '+response);  
                } 
                if(rentas_detalle_modal == 2){
                    modalventasdetalleventas(idresponse,vtipo);
                }else{
                    modalventasdetalle(idresponse,vtipo);
                    setTimeout(function(){ 
                        modalventasdetalle(idresponse,vtipo);
                    }, 1000);
                }
                
            },
            error: function(response){
                notificacion(1);
            }
        });

    }else{
       alertfunction('Atención!','Falta seleccionar al menos un consumible de un equipo.');
    }
}
*/
function creaventaEquiposr(vtipo){
    var datos = $('#formulario_cotizacion').serialize();
    var rentas_detalle_modal = $('.tipoventaequipo option:selected').val();
    var tipoprecioequipo = $('#tipoprecioequipo option:selected').val();
    var pasa=1;
    var nombre = [];
    var TABLA   = $("#tables_equipos_selected tbody > tr");
    TABLA.each(function(){  
        if ($(this).find("input[class*='equipo_check']").is(':checked')) {
            item = {};
            item['equipos'] = $(this).find("input[class*='equipo_check']").val();
            item['cantidad'] = $(this).find("input[id*='equipo_cantidad']").val();
            
            var equiposs = $(this).find("input[class*='equipo_check']").val();
            var equiposconsumiblelength=$('.equipo_c_'+equiposs+' > div').length;
            if (equiposconsumiblelength==0) {
              //  pasa=0;// se quito esta validacion por que puede que el equipo no tenga consumibles
            }
            nombre.push(item);
        }else{
            var equiposs = $(this).find("input[class*='equipo_check']").val();
            $('.equipo_c_'+equiposs).html('');
            $('.equipo_s_'+equiposs).html('');
        }
    });
    aInfoe   = JSON.stringify(nombre);
    //===========================================
        var TABLAa   = $(".equipo_accessoriosselected > div");
        var DATAa  = [];
            TABLAa.each(function(){         
                item = {};
                item ["equiposelected"]   = $(this).find("input[id*='equiposelected']").val();
                item ["cantidad"]   = $(this).find("input[id*='accesoriocantidadn']").val();
                item ["accesorioselected"]  = $(this).find("input[id*='accesorioselected']").val();
                DATAa.push(item);
            });
            INFOa  = new FormData();
            aInfoa   = JSON.stringify(DATAa);

        var TABLAc   = $(".equipo_consumibleselected > div");
        var DATAc  = [];
            TABLAc.each(function(){         
                item = {};
                item ["equiposelected"]   = $(this).find("input[id*='equiposelectedc']").val();
                item ["cantidad"]   = $(this).find("input[id*='consumiblescantidadn']").val();
                item ["consumibleselected"]  = $(this).find("input[id*='consumibleselected']").val();
                DATAc.push(item);
            });
            INFOc  = new FormData();
            aInfoc   = JSON.stringify(DATAc);
    //===========================================

    datos = datos+'&equiposIndividuales='+aInfoe+'&accesorios='+aInfoa+'&consumibles='+aInfoc+'&combinada='+vtipo+'&tipoprecioequipo='+tipoprecioequipo;
    var tipoDocumento = '';
    var idCliente = $('#idCliente').val();
    console.log('idCliente: '+idCliente);
    if(pasa == 1){
        $.ajax({
            type:'POST',
            url: base_url+"index.php/Ventas/nuevaventaequiposr",
            data: datos,
            success: function (response){
                var responseid=parseInt(response);
                idventatablee=response;
                console.log(idventatablee);
                if(vtipo==0){
                    alertfunction('Éxito!','Se ha creado la venta: '+response);  
                } 
                if(rentas_detalle_modal == 2){
                    modalventasdetalleventas(responseid,vtipo);
                }else{
                    modalventasdetalle(responseid,vtipo);
                }
                
            },
            error: function(response){
                notificacion(1);
            }
        });

    }else{
       alertfunction('Atención!','Falta seleccionar al menos un consumible de un equipo.');
    }
}
/*
function creaventaconsumibles(vtipo){
    var equipoListaConsumibles = [];
     $(".equipoListaConsumibles").each(function() {
         equipoListaConsumibles.push($(this).val());
     });
     var toner = [];
     $(".toner").each(function() {
         toner.push($(this).val());
     });
     var bodegasc = [];
     $(".selectbodegacon").each(function() {
         bodegasc.push($(this).val());
     });
     var precio = [];
        // se cambio de tonerp a precionew para su edicion
        $(".precionew").each(function() {
            precio.push($(this).val()); 
        });
     var piezas = [];
     $(".piezas").each(function() {
         piezas.push($(this).val());
     });
     var datos = formulario_cotizacion.serialize() + '&arrayEquipos=' + equipoListaConsumibles + '&arrayToner=' + toner + '&arrayPiezas=' + piezas+'&arrayPrecio='+precio+'&combinada='+vtipo+'&bodegasc='+bodegasc;
     //creaVentas(datos);
     $.ajax({
        type:'POST',
        url: base_url+"index.php/Ventas/nuevaventaconsumibles",
        data: datos,
        success: function (response){
            idventatablec=response;
            console.log(idventatablec);
            if (vtipo==0) {
                alertfunction('Éxito!','Se ha creado la venta: '+response);
                $('#modalconsumibleservicio').modal({
                    dismissible: false
                });
                $('#modalconsumibleservicio').modal('open');
                $('#ventaservicioc').val(idventatablec);
                if($('.tipoConsumibles option:selected').val()==0){
                    $('.ocultarsolicitudservicio').hide();
                }else{
                    $('.ocultarsolicitudservicio').show();
                }
            }   
        },
        error: function(response){
            notificacion(1);
        }
    });
}
*/
/*
function creacotizacionpoliza(vtipo){
    var idCliente = $('#idCliente').val();
    //================================
        var DATAp  = [];
        var TABLA   = $(".tablepolizas tbody > tr");
            TABLA.each(function(){         
            item = {};
            item ["equipo"] = $(this).find("input[id*='equipoposelected']").val();
            item ["serie"]   = $(this).find("input[id*='polizavalserie']").val();
            item ["poliza"]   = $(this).find("input[id*='polizaval']").val();
            item ["polizad"]  = $(this).find("input[id*='polizadval']").val();
            item ["costo"]  = $(this).find("input[id*='polizacval']").val();
            item ["motivo"]  = $(this).find("input[id*='polizamotivo']").val();
            DATAp.push(item);
        });
        INFO  = new FormData();
        arrayPolizas   = JSON.stringify(DATAp);
        INFO.append('data', arrayPolizas);
    //================================
    var datos = $('#formulario_cotizacion').serialize()+'&arrayPolizas='+arrayPolizas+'&combinada='+vtipo;
    if(TABLA.length>0){
        $.ajax({
            type:'POST',
            url: base_url+"index.php/Ventas/nuevaCotizacionpoliza",
            data: datos,
            success: function (response){
                idventatablep=response;
                console.log(idventatablep);
                if(vtipo==0){
                    alertfunction('Éxito!','Se ha creado la venta: '+response);
                    setTimeout(function(){ 
                        window.location.href = base_url+"index.php/PolizasCreadas";
                    }, 2000);
                }          
            },
            error: function(response){
                notificacion(1);
            }
        });
    }else{
        alertfunction('Atención!','Agregue un concepto');
    }
}
*/
/*
function creacotizacionaccesorio(vtipo){
    var idCliente = $('#idCliente').val();
    //================================
        var DATAac  = [];
        var TABLAac   = $("#table_venta_accesorios  tbody > tr");
            TABLAac.each(function(){         
            item = {};
            item ["equipo"] = $(this).find("input[id*='equi_acce_selected']").val();
            item ["accesorio"]   = $(this).find("input[id*='acce_acce_selected']").val();
            item ["costo"]   = $(this).find("input[id*='costo_acce_selected']").val();
            item ["cantidad"]  = $(this).find("input[id*='cantidad_acce_selected']").val();
            item ["bodega"]  = $(this).find("input[id*='bodega_acce_selected']").val();
            DATAac.push(item);
        });
        INFO  = new FormData();
        arrayac   = JSON.stringify(DATAac);
        
    //================================
    var datos = $('#formulario_cotizacion').serialize()+'&arrayac='+arrayac+'&combinada='+vtipo+'&idventatablee='+idventaeaaccc;
    if(TABLAac.length>0){
        $.ajax({
            type:'POST',
            url: base_url+"index.php/Ventas/nuevaCotizacionaccesorios",
            data: datos,
            success: function (response){
                idventatablee=response;
                console.log(idventatablee);
                if(vtipo==0){
                    alertfunction('Éxito!','Se ha creado la venta: '+response);
                    $('#modalconsumibleservicio').modal({
                        dismissible: false
                    });
                    $('#modalconsumibleservicio').modal('open');
                    $('#ventaservicioc').val(idventatablee);
                    if($('.tipoConsumibles option:selected').val()==0){
                        $('.ocultarsolicitudservicio').hide();
                    }else{
                        $('.ocultarsolicitudservicio').show();
                    }
                    
                }          
            },
            error: function(response){
                notificacion(1);
            }
        });
    }else{
        alertfunction('Atención!','Agregue un concepto');
    }
}
*/
/*
function creaventarefacciones(vtipo){
    var DATAr  = [];
        var TABLA   = $(".tablerefacciones tbody > tr");
            TABLA.each(function(){         
            item = {};
            item ["equipo"] = $(this).find("input[id*='equipotx']").val();
            item ["serie"] = $(this).find("input[id*='seriees']").val();
            item ["toner"]   = $(this).find("input[id*='tonertx']").val();
            item ["tonerp"]  = $(this).find("input[id*='tonerptx']").val();
            item ["piezas"]  = $(this).find("input[id*='piezastx']").val();
            item ["bodega"]  = $(this).find("input[id*='bodegartx']").val();
            DATAr.push(item);
        });
        INFO  = new FormData();
        arrayrefaccion   = JSON.stringify(DATAr);
    var datos = formulario_cotizacion.serialize()+'&arrayRefaccion='+arrayrefaccion+'&combinada='+vtipo;
    if(TABLA.length>0){
        $.ajax({
            type:'POST',
            url: base_url+"index.php/Ventas/nuevaventarefacciones",
            data: datos,
            success: function (response){
                idventatabler=parseInt(response);
                //modalventasdetalle(response);
                if(vtipo==0){
                    alertfunction('Éxito!','Se ha creado la venta: '+response);
                    $('#modalconsumibleservicio').modal({
                        dismissible: false
                    });
                    $('#modalconsumibleservicio').modal('open');
                    $('#ventaservicioc').val(idventatabler);
                    if($('.tipoConsumibles option:selected').val()==0){
                        $('.ocultarsolicitudservicio').hide();
                    }else{
                        $('.ocultarsolicitudservicio').show();
                    }
                    //setTimeout(function(){ 
                    //    window.location.href = base_url+"index.php/Ventas";
                    //}, 2000);
                } 
                
                
                setTimeout(function(){ 
                    //window.location.href = base_url+"index.php/Cotizaciones/listaCotizacionesPorCliente/"+idCliente; 
                }, 2000);
                
            },
            error: function(response){
                notificacion(1);
            }
        });
    }else{
        alertfunction('Atención!','Agregue una refacción');
    }
}
*/
function creaventacombinada(){
    if ($('#ventae').is(':checked')) {
        creaventaEquipos(1);
    }
    setTimeout(function(){  
        if ($('#ventac').is(':checked')) {
            creaventaconsumibles(1);
        }
        if ($('#ventar').is(':checked')) {
            creaventarefacciones(1);
        }
        if ($('#ventap').is(':checked')) {
            creacotizacionpoliza(1);
        }
        if ($('#ventaa').is(':checked')) {
            creacotizacionaccesorio(1);
        }
    }, 1000);
    setTimeout(function(){  
        
        idventatablee = parseInt(idventatablee);
        idventatablec = parseInt(idventatablec);
        idventatabler = parseInt(idventatabler);
        idventatablep = parseInt(idventatablep);
        console.log(idventatablee+''+idventatablec+''+idventatabler+''+idventatablep);
        $.ajax({
            type:'POST',
            url: base_url+"index.php/Ventas/nuevaventacombinada",
            data: {
                equipo:idventatablee,
                consumibles:idventatablec,
                refacciones:idventatabler,
                poliza:idventatablep,
                telefono:$('#telefono').val(),
                correo:$('#correo').val(),
                idCliente:$('#idCliente').val(),
                tipov:$('#tipov option:selected').val(),
            },
            success: function (response){
                alertfunction('Éxito!!','Se ha creado la Venta combinada: '+response);
                //modalventasdetalle(response);
                if (idventatablee>0) {
                    setTimeout(function(){ 
                        window.location.href = base_url+"index.php/Ventas"; 
                    }, 25000);
                }else{
                    setTimeout(function(){ 
                        window.location.href = base_url+"index.php/Ventas"; 
                    }, 25000);  
                }
                        
            },
            error: function(response){
                notificacion(1); 
            }
        });
    }, 3100);
}
// rentas cuando se guardan los datos 
function modalventasdetalleventas(idventa,vtipo){
    //call the specific div (modal)108|0
    console.log(idventa+'|'+vtipo);    
    $('#modalupdateventarentas').modal({
        dismissible: false
    });
    $('#modalupdateventarentas').modal('open');
    //$('#modalupdateventarentas').modal();
    ///$('#modalupdateventarentas').modal('open');
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Ventas/viewventarenta",
        data: {
            idventa:idventa
        },
        success: function (response){
           $('.venta_detalles_view_renta').html(response);
           validarpermisosbodega();
        },
        error: function(response){
            notificacion(1);
        }
    });
} 
function precionewreadonly(selected){
    var aux = selected.closest('.rowConsumibles');
    if (aux.find('.precionew').is('[readonly]')) {
        swal({   title: "Advertencia!",
          text: "Se necesita contraseña de administrador:",
          type: "input",   showCancelButton: true,
          closeOnConfirm: false,
          showLoaderOnConfirm: true,
          animation: "slide-from-top",
          inputPlaceholder: "********",
          inputType:'password' },
          function(inputValue){
            if (inputValue === false) return false;
            if (inputValue === "") {
                swal.showInputError("No se permite campos vacíos!");
                return false
            }else{
                $.ajax({
                    type:'POST',
                    url: base_url+'Cotizaciones/permisos',
                    data: {
                        pass: inputValue
                    },
                    async: false,
                    statusCode:{
                        404: function(data){
                                swal("Error", "404", "error");
                        },
                        500: function(){
                            swal("Error", "500", "error"); 
                        }
                    },
                    success:function(data){
                      if (data==1) {
                        swal({
                            title: "Confirmación",
                            text: "Contraseña correcta.",
                            timer: 3000,
                            showConfirmButton: false
                        });
                        aux.find('.precionew').attr("readonly", false);
                      }else{
                        swal.showInputError("No tiene permisos!");
                      }

                    }
                });
            }
            
          });
    }
    
}
//=====================================
function selectequipor(){
    var id = $('#equipoListarefacciones option:selected').val();
    var bodega = $('#bodegar option:selected').val();
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Cotizaciones/mostrarselecrefaccionesr",
        data: {
            id:id,
            bodega:bodega
        },
        success: function (data){
            $('.refacciones_mostrar').html('');
            $('.refacciones_mostrar').html(data);   
            $('.chosen-select').chosen({width: "100%"}); 
        },
        error: function(data){
            notificacion(1);
        }
    });
}
function selectprecior(aux){
    var id = $('#srefacciones option:selected').val();
    var aaa = $('#aaa').val();
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Cotizaciones/mostrarselectpreciosr",
        data: {id:id},
        success: function (data){
            var array = $.parseJSON(data); 
            $('.refaccionesp_mostrar').html('');
            $('.refaccionesp_mostrar').html(array.select_option);   
            $('.chosen-select').chosen({width: "100%"}); 

            if(aaa==1){
                $('.refaccionesp').val(array.espe).trigger('chosen:updated');
            }else{
                $('.refaccionesp').trigger("chosen:updated");    
            }
            preciorefaccion();
            //verificarseriederentas();
        },
        error: function(data){ 
            notificacion(1);
        }
    });
    alertastockrefaccion(id,aux);
}
var srefaadd=1;
function addrefaccion(){
    var equipoval = $('#equipoListarefacciones option:selected').val();
    var seriee = $('#seriee').val();
    var equipotext = $('#equipoListarefacciones option:selected').text();
    var tornerval = $('#srefacciones option:selected').val();
    var tornerstock = $('#srefacciones option:selected').data('stock');
    var tornertext = $('#srefacciones option:selected').text();
    var tornerpval = $('#refaccionesp option:selected').val();
    var tornerptext = $('#refaccionesp option:selected').text();
    if(tornerpval==undefined){
        var tornerpval = $('#refaccionesp').val();
        var tornerptext = tornerpval;
    }
    var bodegarval = $('#bodegar option:selected').val();
    var bodegartext = $('#bodegar option:selected').text();
    var piezastext = $('#piezasr').val();
    if(bodegarval>0){
        if(tornerstock>=piezastext){
            var tablerow='<tr class="refacddd_row_'+srefaadd+'">\
                            <td>\
                                <input type="hidden" id="equipotx" class="equipotx" value="'+equipoval+'">\
                                <input type="hidden" id="seriees" class="seriees" value="'+seriee+'">'+equipotext+' '+seriee+'</td>\
                            <td><input type="hidden" id="tonertx" class="tonertx" value="'+tornerval+'">'+tornertext+'</td>\
                            <td><input type="hidden" id="tonerptx" class="tonerptx" value="'+tornerpval+'">'+tornerptext+'</td>\
                            <td><input type="hidden" id="piezastx" class="piezastx" value="'+piezastext+'">'+piezastext+'</td>\
                            <td><input type="hidden" id="bodegartx" class="bodegartx" value="'+bodegarval+'">'+bodegartext+'</td>\
                            <td><a onclick="deleterefaadd('+srefaadd+')"style="cursor: pointer;"><i class="material-icons" style="color: red;">delete_forever</i></a></td>\
                          </tr>';
            $('.equipotableconsumicle .tablerefacciones tbody').append(tablerow);
            srefaadd++;
            $('#piezasr').val(1);
        }else{
            notificacion(6);
        } 
    }else{
        notificacion(5);
    }
}
function deleterefaadd(id){
    $('.refacddd_row_'+id).remove();
}
function alertfunction(titlea,contenta){
    $.alert({boxWidth: '30%',useBootstrap: false,title: titlea,theme: 'bootstrap',content: contenta});  
}
function obtenercontactos(){
    //$('#atencion').hide(1000);
    $('#atencionselect').show("slow");
    //$('#atencionselect').chosen({width: "100%"});
}
function obtenercontactosss(){
    var atencionpara = $('#atencionselect option:selected').data('atencionpara');
    var email = $('#atencionselect option:selected').data('email');
    var telefono = $('#atencionselect option:selected').data('telefono');
    var celular = $('#atencionselect option:selected').data('celular');
    if(telefono.length==0){
        telefono=celular;
    }
    $('#atencionselect').hide(1000);
    //$('#atencion').show("slow");
    $('#atencion').val(atencionpara);
    $('#correo').val(email);
    $('#telefono').val(telefono);
}
function alertastockequipo(id,stock){
    console.log('alertastockequipo');
    var cantidad = parseInt($('.equ_cant_'+id).val());
    setTimeout(function(){ 
        if ($('#equipo_check_'+id).is(':checked')) {
            if (stock<=0) {
                toastr["error"]("No hay suficiente stock", "Alerta!");
                $('#equipo_check_'+id).addClass('bloqueo');
                $('.equipo_row_'+id).addClass('bloqueorow');
            }else if(stock<cantidad){
                toastr["error"]("No hay suficiente stock", "Alerta!");
                $('#equipo_check_'+id).addClass('bloqueo');
                $('.equipo_row_'+id).addClass('bloqueorow');
            }else{
                $('#equipo_check_'+id).removeClass('bloqueo');
                $('.equipo_row_'+id).removeClass('bloqueorow');
            }
            
        }else{
            $('#equipo_check_'+id).removeClass('bloqueo');
            $('.equipo_row_'+id).removeClass('bloqueorow');
        }
    }, 1000);
    
    console.log('alertastockequipo');
}
function alertastocktoner(toner,aux){
    //var toner = toner.value();
    var cantidad=aux.find("input[class*='piezas']").val();
    var bodega=aux.find("#bodegacon option:selected").val();
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Aconsumibles/stockconsumiblesid2",
        data: {
            id:toner,
            bodega:bodega
        },
        success: function (data){
            if (parseInt(data)<=0) {
                toastr["error"]("No hay suficiente stock", "Alerta!");
                console.log(aux.find("input[class*='piezas']").val());
                aux.addClass('bloqueo');
            }else if(parseInt(data)>=cantidad){
                console.log(aux);
                aux.removeClass('bloqueo');
            }else{
                toastr["error"]("No hay suficiente stock", "Alerta!");
                aux.addClass('bloqueo');
            }
            
        },
        error: function(data){ 
            notificacion(1);
        }
    });
}
function alertastockrefaccion(id,aux){
    console.log(aux);
    var bodega=$("#bodegar option:selected").val();
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Aconsumibles/stockrefaccionid2",
        data: {
            id:id,
            bodega:bodega
        },
        success: function (data){
            var stock =parseInt(data);
            var psolicitud=$('#piezasr').val();
            if (stock<=0) {
                toastr["error"]("No hay suficiente stock", "Alerta!");
                aux.addClass('bloqueo');
            }else if(stock>=psolicitud){
                aux.removeClass('bloqueo');
            }else{
                toastr["error"]("No hay suficiente stock", "Alerta!");
                aux.addClass('bloqueo');
            }
            
        },
        error: function(data){ 
            notificacion(1);
        }
    });
}
function validarcantidadstockr(aux){
    //$('#srefacciones').change();
    var id = $('#srefacciones option:selected').val();
    var aaa = $('#aaa').val();
    
    alertastockrefaccion(id,aux);
}
function validstockequedit(datos,stock){
    var cantidad = datos.val();
    if (parseInt(cantidad)<=parseInt(stock)) {
        datos.removeClass('bloqueo');
    }else{
        datos.addClass('bloqueo');
        toastr["error"]("No hay suficiente stock", "Alerta!");
    }
}
function validarprefacturaspendientes(){
    $('.prefacturaslis').html('');
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Ventas/prefacturaspedientes",
        data: {
            idcliente:$('#idCliente').val()
        },
        success: function (data){
            console.log(data);
            var array = $.parseJSON(data);

            if (array.length>0) {
                $('#modalprefacturaspendiestes').modal({
                    dismissible: false
                });
                $('#modalprefacturaspendiestes').modal('open');
                array.forEach(function(element) {
                    var html='<tr><td>'+element.reg+'</td><td><a class="btn-floating blue tooltipped" href="'+base_url+'Ventas?cli='+element.empresa+'"><i class="material-icons">assignment</i></a>/td></tr>';
                    $('.prefacturaslis').append(html);
                });
                
            }
            
        },
        error: function(data){ 
        }
    });
}
function direccionesclientes(id){
  $.ajax({
        type:'POST',
        url: base_url+"Asignaciones/datoscliente",
        data: {
          id:id
        },
        success: function (data){
          console.log(data);
          var array = $.parseJSON(data);
          array.direccion.forEach(function(element) {
                $('#tdireccion2').append('<option>'+element.direccion+'</option>');
                $('#tdireccion2e').append('<option>'+element.direccion+'</option>');
          });
        }
    }); 
}
function obtenerservicios2(){
  var tpoliza = $('#tpoliza2').val();
  $.ajax({
        type:'POST',
        url: base_url+'index.php/Asignaciones/obtenerservicios',
        data: {
          tpoliza:tpoliza
          },
        success:function(data){
            $('#tservicio2').html(data);
            $('.chosen-select').trigger("chosen:updated");

        }
    });
}
function obtenerservicios2e(){
  var tpoliza = $('#tpoliza2e').val();
  $.ajax({
        type:'POST',
        url: base_url+'index.php/Asignaciones/obtenerservicios',
        data: {
          tpoliza:tpoliza
          },
        success:function(data){
            $('#tservicio2e').html(data);
            $('.chosen-select').trigger("chosen:updated");

        }
    });
}
function aceptarcservicio(){
    var serv = $('#aceptarcservicio').is(':checked');
    if (serv==true) {
        $('.ventaservicio').show('show');
        $('.ventaservicio_fe').hide('show');
        $('#ventaservicio_fe').val('');
    }else{
        $('.ventaservicio').hide('show');
        $('.ventaservicio_fe').show('show');
    }
}
function aceptareservicio(){
    var serv = $('#aceptareservicio').is(':checked');
    if (serv==true) {
        $('.ventaeservicio').show('show');
        //$('.ventaeservicio_fe').hide('show');
        //$('#ventaeservicio_fe').val('');
    }else{
        $('.ventaeservicio').hide('show');
        //$('.ventaeservicio_fe').show('show');
    }
}
toastr.options = {
  "closeButton": false,
  "debug": false,
  "newestOnTop": false,
  "progressBar": true,
  "positionClass": "toast-top-right",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "2000",
  "timeOut": "5000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}
function sinmodelo(row) {
    setTimeout(function(){
        var sinmodelov = $('#sinmodelo_'+row).is(':checked')==true?1:0; 
        if(sinmodelov==1){
            $('#equipoListas_'+row).html('<option value="113" selected>Sin modelo</option>');
            //$('.chosen-select').select2({width: 'resolve'});

            $('.chosen-select').chosen({width: '100%'});
            setTimeout(function(){
                $('#equipoListas_'+row).val(113);
                $('#equipoListas_'+row).trigger("chosen:updated");
            }, 500);
        }else{
            //$('.select_newmodelo_'+row).show('show');
            //$('.newmodelo_'+row).hide('show');

            var select =$('#selectEquipos #equipoLista').html();
            $('#equipoListas_'+row).html(select);
            //$('.chosen-select').select2({width: 'resolve'});
            $('3equipoListas_'+row).chosen({width: '100%'});
            $('#equipoListas_'+row).trigger("chosen:updated");
        }
        polizaseleccionada(row);
    }, 500);
}
function addserieshisto(row){
    $('#modalserieshistorial').modal();
    $('#modalserieshistorial').modal('open');
    $.ajax({
        type:'POST',
        url: base_url+'index.php/Ventas/cliente_equipo_historial_group',
        data: {
            idcliente:$('#idCliente').val()
        },
        success:function(data){
            var array = $.parseJSON(data);
            var html_table='';
            var infocliente=$('#cliente').val();
            html_table+='<table class="table" id="table-h-e-s">\
                  <thead>\
                    <tr>\
                      <th>Modelo</th>\
                      <th>Serie</th>\
                      <th></th>\
                      <th></th>\
                      <th></th>\
                      <th></th>\
                      <th></th>\
                    </tr>\
                  </thead>\
                <tbody>';
                var seriehiqrow=0;
                array.forEach(function(element) {

                    var contrato='';
                    if(element.tipo==2){
                        //obtenerdireccionescontrato(element.equipoid,element.serieId);
                        var contrato='Contrato: '+element.idcontrato;

                        var classdireccion='contratosdireccion equipo_'+element.equipoid+'_serie_'+element.serieId;
                        var classdirecciondata=' data-equipo="'+element.equipoid+'" data-serie="'+element.serieId+'" ' ;
                        var classubicacion='u_equipo_'+element.equipoid+'_serie_'+element.serieId;
                    }else{
                        var classdireccion='';
                        var classubicacion='';
                        var classdirecciondata='';
                    }
                    html_table+='<tr>\
                                  <td style="font-size:12px;">'+element.modelot+'</td>\
                                  <td style="font-size:12px;">'+element.serie+'</td>\
                                  <td style="font-size:12px;">'+contrato+'</td>\
                                  <td style="font-size:12px;" class="'+classdireccion+'" '+classdirecciondata+'></td>\
                                  <td style="font-size:12px;" class="'+classubicacion+'"></td>\
                                  <td>\
                                    <a class="btn-bmz dataseries_historico_'+seriehiqrow+'" onclick="m_s_historico('+seriehiqrow+','+row+')" \
                                        data-modelo="'+element.modelot+'" \
                                        data-modeloid="'+element.modelo+'" \
                                        data-serie="'+element.serie+'" \
                                        data-tipo="'+element.tipo+'" \
                                        data-id="'+element.id+'" \
                                    >Agregar</a>\
                                  </td>\
                                  <td>';
                                    if(element.tipo>0){
                                        html_table+='<a class="btn-floating blue tooltipped " onclick="m_s_historico_pre('+seriehiqrow+')" ><i class="material-icons">assignment</i></a>';
                                    }
                        html_table+='</td>\
                                </tr>';
                    seriehiqrow++;
                });
                html_table+='</tbody>\
                </table>';  

            $('.table-h-e-s').html(html_table);
            $('#table-h-e-s').DataTable({
                order: [
                        [2, "desc"]
                        ]
            }).on('draw',function(){
                 cargardireccionesubicaciones();
            });;
            cargardireccionesubicaciones();

        }
    });
    
}
function cargardireccionesubicaciones(){
    $(".contratosdireccion").each(function() {
        var equipo = $(this).data('equipo');
        var serie = $(this).data('serie');
        obtenerdireccionescontrato(equipo,serie);
        obtenerdireccionescontratoubicacion(equipo,serie);
    });
}
function obtenerdireccionescontrato(equipo,serie){
    $.ajax({
      type:'POST',
      url: base_url+'index.php/Calendario/obtenerdireccionescontrato',
      data: {
        equipo:equipo,
        serie:serie
        },
      success:function(data){
          $('.equipo_'+equipo+'_serie_'+serie).html(data);
      }
  });
}
function obtenerdireccionescontratoubicacion(equipo,serie){
    $.ajax({
      type:'POST',
      url: base_url+'index.php/Calendario/obtenerdireccionescontratoubicacion',
      data: {
        equipo:equipo,
        serie:serie
        },
      success:function(data){
          $('.u_equipo_'+equipo+'_serie_'+serie).html(data);
      }
  });
}
function m_s_historico(seriehiqrow,row) {
    var modelo  = $('.dataseries_historico_'+seriehiqrow).data('modelo');
    var modeloid= $('.dataseries_historico_'+seriehiqrow).data('modeloid');
    var serie   = $('.dataseries_historico_'+seriehiqrow).data('serie');
    if(modeloid==113){
        $('#sinmodelo_'+row).prop('checked',true);
    }else{
        $('#sinmodelo_'+row).prop('checked',false);
    }
    $('#equipoListas_'+row).html('<option value="'+modeloid+'">'+modelo+'</option>').trigger("chosen:updated");
    $('#polizaserie_'+row).val(serie).change();
    $('#modalserieshistorial').modal('close');
}
function m_s_historico_pre(seriehiqrow) {
    var tipo   = $('.dataseries_historico_'+seriehiqrow).data('tipo');
    var id     = $('.dataseries_historico_'+seriehiqrow).data('id');
    if(tipo==1){
        window.open(base_url+"Prefactura/view/"+id, "Prefactura", "width=780, height=612");
        //window.open(base_url+"Prefactura/viewc/"+id, "Prefactura", "width=780, height=612");
    }
    if(tipo==2){
        window.open(base_url+"RentasCompletadas/view/"+id, "Prefactura", "width=780, height=612");
    }
}
function addh_refacciones_cotizacion() {
    $('#modalrefaccionescotizacion').modal();
    $('#modalrefaccionescotizacion').modal('open');
    $.ajax({
        type:'POST',
        url: base_url+'index.php/Ventas/cliente_refacciones_cotizacion',
        data: {
            idcliente:$('#idCliente').val(),
            cotizaciones:1
        },
        success:function(data){
            var array = $.parseJSON(data);
            var html_table='';
            var infocliente=$('#cliente').val();
            html_table+='<table class="table" id="table-h-r-c">\
                  <thead>\
                    <tr>\
                      <th>Cotizacion</th><th>Equipo</th><th>Cantidad</th><th>Refaccion</th><th></th><th></th></tr>\
                  </thead>\
                <tbody>';
                var seriehiqrow=0;
                array.forEach(function(element) {
                    html_table+='<tr>\
                                  <td style="font-size:12px;">'+element.idCotizacion+'</td>\
                                  <td style="font-size:12px;">'+element.equipo+'</td>\
                                  <td style="font-size:12px;">'+element.piezas+'</td>\
                                  <td style="font-size:12px;">'+element.refaccion+'</td>\
                                  <td>\
                                    <a class="btn-bmz datasrefaccion_historico_'+seriehiqrow+'" onclick="m_r_historico('+seriehiqrow+')" \
                                        data-idequipo="'+element.idEquipo+'" \
                                        data-idrefaccion="'+element.idRefaccion+'" \
                                        data-piezas="'+element.piezas+'" \
                                        data-precio="'+element.precioGeneral+'" \
                                        data-serie="'+element.serieequipo+'" \
                                        data-tipo="'+element.tipo+'" \
                                    >Agregar</a>\
                                  </td>\
                                  <td>';
                                  if(tipo==0){
                                    html_table+='<a href="'+base_url+'Cotizaciones/documentoRefaccionesBordes/'+element.idCotizacion+'" class="btn-floating blue tooltipped" target="_blank"><i class="material-icons">assignment</i></a>';
                                  }
                        html_table+='</td>\
                                </tr>';
                    seriehiqrow++;
                });
                html_table+='</tbody>\
                </table>';  

            $('.table-h-r-c').html(html_table);
            $('#table-h-r-c').DataTable();

        }
    });
}
function m_r_historico(row){
    var idequipo    = $('.datasrefaccion_historico_'+row).data('idequipo');
    var idrefaccion = $('.datasrefaccion_historico_'+row).data('idrefaccion');
    var piezas      = $('.datasrefaccion_historico_'+row).data('piezas');
    var precio      = $('.datasrefaccion_historico_'+row).data('precio');
    var serie      = $('.datasrefaccion_historico_'+row).data('serie');
    var tipo      = $('.datasrefaccion_historico_'+row).data('tipo');

    $('#equipoListarefacciones').val(idequipo).trigger("chosen:updated").change();
    $('#seriee').val(serie);

    setTimeout(function(){
        console.log(idrefaccion);
        if(tipo==0){
            $('#srefacciones').val(idrefaccion).trigger("chosen:updated").change(); 
        }
        $('#piezasr').val(piezas);  
    }, 1000);
    setTimeout(function(){
            $('#refaccionesp').html('<option value="'+precio+'">'+precio+'</option>').trigger("chosen:updated").change(); 
    }, 1250);
    setTimeout(function(){
            if(tipo==0){
                addrefaccion();
            }
    }, 1300);
    $('#modalrefaccionescotizacion').modal('close');
}
function obtenerabsesorios(){
    var equipo = $('#equipo_acce option:selected').val();
    var bodega = $('#bodega_acce option:selected').val();
    $('#accesorio_acce').html('');
    $.ajax({
        type:'POST',
        url: base_url+'index.php/equipos/getListadoAcesoriosEquipo/'+equipo+'/'+bodega,
        success:function(data){
            var array = $.parseJSON(data);
            console.log(array);
            var accesorioselected='<option></option>';
            array.data.forEach(function(element) {
                if(element.stock>0){
                    var stock1=element.stock;
                }else{
                    var stock1=0;
                }
                if(element.stock2>0){
                    var stock2=element.stock2;
                }else{
                    var stock2=0;
                }

                var stock=parseFloat(stock1)+parseFloat(stock2);

                accesorioselected+='<option value="'+element.idcatalogo_accesorio+'" data-costo="'+element.costo+'" data-stock="'+stock+'">'+element.nombre+' ('+stock+')</option>';
            });
            $('#accesorio_acce').html(accesorioselected).trigger("chosen:updated");
        }
    });
}
function obtenerabsesorioscosto(){
    var costo = $('#accesorio_acce option:selected').data('costo');
    $('#precio_acce').val(costo);
}
var acc_row=0;
function addaccesoriov(){
    var equipo_acce=$('#equipo_acce option:selected').val();
    var equipo_acce_text=$('#equipo_acce option:selected').text();
    var accesorio_acce=$('#accesorio_acce option:selected').val();
    var accesorio_acce_text=$('#accesorio_acce option:selected').text();
    var accesorio_acce_stock=$('#accesorio_acce option:selected').data('stock');
    var precio_acce = $('#precio_acce').val();
    var cantidad_acc = $('#cantidad_acc').val();
    var bodega_acce=$('#bodega_acce option:selected').val();
    var bodega_acce_text=$('#bodega_acce option:selected').text();
    if(equipo_acce>0 && accesorio_acce>0 && cantidad_acc>0 && bodega_acce>0){
        if(cantidad_acc<=accesorio_acce_stock){
            var html='<tr class="add_venta_acc_'+acc_row+'">\
                <td><input type="hidden" id="equi_acce_selected" value="'+equipo_acce+'" class="form-control-bmz" readonly>'+equipo_acce_text+'</td>\
                <td><input type="hidden" id="acce_acce_selected" value="'+accesorio_acce+'" class="form-control-bmz" readonly>'+accesorio_acce_text+'</td>\
                <td><input type="hidden" id="costo_acce_selected" value="'+precio_acce+'" class="form-control-bmz" readonly>'+precio_acce+'</td>\
                <td><input type="hidden" id="cantidad_acce_selected" value="'+cantidad_acc+'" class="form-control-bmz">'+cantidad_acc+'</td>\
                <td><input type="hidden" id="bodega_acce_selected" value="'+bodega_acce+'" class="form-control-bmz">'+bodega_acce_text+'</td>\
                <td><a class="btn-floating waves-effect waves-light red accent-2"><i class="material-icons" onclick="deleteventaacce('+acc_row+')">clear</i></a></td>\
              </tr>';
            $('.tbody_acce').append(html);
        }else{
            toastr["error"]("Stock insuficiente", "Advertencia");
        }
    
    }
    acc_row++;
}
function deleteventaacce(row){
    $('.add_venta_acc_'+row).remove();
}
function addh_consumible_equipo(rowtr){
    $('#modalrefaccionescotizacion').modal();
    $('#modalrefaccionescotizacion').modal('open');
    $.ajax({
        type:'POST',
        url: base_url+'index.php/Ventas/cliente_refacciones_cotizacion',
        data: {
            idcliente:$('#idCliente').val(),
            cotizaciones:1
        },
        success:function(data){
            var array = $.parseJSON(data);
            var html_table='';
            var infocliente=$('#cliente').val();
            html_table+='<table class="table" id="table-h-r-c">\
                  <thead>\
                    <tr>\
                      <th>Cotizacion</th><th>Equipo</th><th>Cantidad</th><th>Refaccion</th><th></th><th></th></tr>\
                  </thead>\
                <tbody>';
                var seriehiqrow=0;
                array.forEach(function(element) {
                    html_table+='<tr>\
                                  <td style="font-size:12px;">'+element.idCotizacion+'</td>\
                                  <td style="font-size:12px;">'+element.equipo+'</td>\
                                  <td style="font-size:12px;">'+element.piezas+'</td>\
                                  <td style="font-size:12px;">'+element.refaccion+'</td>\
                                  <td>\
                                    <a class="btn-bmz datasce_historico_'+seriehiqrow+'" onclick="m_ce_historico('+seriehiqrow+','+rowtr+')" \
                                        data-idequipo="'+element.idEquipo+'" \
                                        data-idrefaccion="'+element.idRefaccion+'" \
                                        data-piezas="'+element.piezas+'" \
                                        data-precio="'+element.precioGeneral+'" \
                                        data-serie="'+element.serieequipo+'" \
                                        data-tipo="'+element.tipo+'" \
                                    >Agregar</a>\
                                  </td>\
                                  <td>';
                                  if(tipo==0){
                                    html_table+='<a href="'+base_url+'Cotizaciones/documentoRefaccionesBordes/'+element.idCotizacion+'" class="btn-floating blue tooltipped" target="_blank"><i class="material-icons">assignment</i></a>';
                                  }
                        html_table+='</td>\
                                </tr>';
                    seriehiqrow++;
                });
                html_table+='</tbody>\
                </table>';  

            $('.table-h-r-c').html(html_table);
            $('#table-h-r-c').DataTable();

        }
    });
}
function m_ce_historico(seriehiqrow,rowtr){
    var idequipo    = $('.datasce_historico_'+seriehiqrow).data('idequipo');
    $('#rowConsumibles'+rowtr+' #equipoListaConsumibles').val(idequipo).trigger("chosen:updated").change();
    $('#modalrefaccionescotizacion').modal('close');
}
function addh_accesorios_equipo(){
    $('#modalrefaccionescotizacion').modal();
    $('#modalrefaccionescotizacion').modal('open');
    $.ajax({
        type:'POST',
        url: base_url+'index.php/Ventas/cliente_refacciones_cotizacion',
        data: {
            idcliente:$('#idCliente').val(),
            cotizaciones:1
        },
        success:function(data){
            var array = $.parseJSON(data);
            var html_table='';
            var infocliente=$('#cliente').val();
            html_table+='<table class="table" id="table-h-r-c">\
                  <thead>\
                    <tr>\
                      <th>Cotizacion</th><th>Equipo</th><th>Cantidad</th><th>Refaccion</th><th></th><th></th>\
                    </tr>\
                  </thead>\
                <tbody>';
                var seriehiqrow=0;
                array.forEach(function(element) {
                    html_table+='<tr>\
                                  <td style="font-size:12px;">'+element.idCotizacion+'</td>\
                                  <td style="font-size:12px;">'+element.equipo+'</td>\
                                  <td style="font-size:12px;">'+element.piezas+'</td>\
                                  <td style="font-size:12px;">'+element.refaccion+'</td>\
                                  <td>\
                                    <a class="btn-bmz datasce_historico_'+seriehiqrow+'" onclick="m_acc_historico('+seriehiqrow+')" \
                                        data-idequipo="'+element.idEquipo+'" \
                                        data-idrefaccion="'+element.idRefaccion+'" \
                                        data-piezas="'+element.piezas+'" \
                                        data-precio="'+element.precioGeneral+'" \
                                        data-serie="'+element.serieequipo+'" \
                                        data-tipo="'+element.tipo+'" \
                                    >Agregar</a>\
                                  </td>\
                                  <td>';
                                  if(tipo==0){
                                    html_table+='<a href="'+base_url+'Cotizaciones/documentoRefaccionesBordes/'+element.idCotizacion+'" class="btn-floating blue tooltipped" target="_blank"><i class="material-icons">assignment</i></a>';
                                  }
                        html_table+='</td>\
                                </tr>';
                    seriehiqrow++;
                });
                html_table+='</tbody>\
                </table>';  

            $('.table-h-r-c').html(html_table);
            $('#table-h-r-c').DataTable();

        }
    });
}
function m_acc_historico(seriehiqrow){
    var idequipo    = $('.datasce_historico_'+seriehiqrow).data('idequipo');
    $('#equipo_acce').val(idequipo).trigger("chosen:updated").change();
    $('#modalrefaccionescotizacion').modal('close');
}

function passinput(){
    $("#contrasena").change();
    var pass=$('#contrasena').val();
    if(pass==''){
        $('#contrasena').data('val','');
    }
}
function desbloqueocostosceros(){
    swal({   title: "Advertencia!",
          text: "Se necesita contraseña de administrador:",
          type: "input",   
          showCancelButton: true,
          closeOnConfirm: false,
          showLoaderOnConfirm: true,
          animation: "slide-from-top",
          inputPlaceholder: "********",
          inputType:'password' },
          function(inputValue){
            if (inputValue === false) return false;
            if (inputValue === "") {
                swal.showInputError("No se permite campos vacíos!");
                return false
            }else{
                $.ajax({
                    type:'POST',
                    url: base_url+'Cotizaciones/permisoscostocero',
                    data: {
                        pass: inputValue
                    },
                    async: false,
                    statusCode:{
                        404: function(data){
                                swal("Error", "404", "error");
                        },
                        500: function(){
                            swal("Error", "500", "error"); 
                        }
                    },
                    success:function(data){
                      if (data==1) {
                        swal({
                            title: "Confirmación",
                            text: "Contraseña correcta.",
                            timer: 3000,
                            showConfirmButton: false
                        });
                        $('.newstyles').html('<style type="text/css">.costocero{display: block !important;}</style>');
                        $('.desbloqueocostosceros').html('Costos Cero <i class="fa fa-lock-open fa-fw" style="color: green;"></i>');
                      }else{
                        swal.showInputError("No tiene permisos!");
                      }

                    }
                });
            }
            
          });
}
function validarpermisosbodega(){
    var perfilid = $('#perfilid').val();
    var tipo_v_selected =$('#tipov option:selected').val();
    if(perfilid!=1){
        $('.selectbodegacon option[value=2]').prop('disabled',true);//seminuevos
        $('.equipoListarefacciones option[value=2]').prop('disabled',true);//seminuevos
        $('#bodega_acce option[value=2]').prop('disabled',true);//seminuevos
        $('.serie_bodega option[value=2]').prop('disabled',true);//seminuevos    
    }
    
    if(tipo_v_selected==1){
        $('.tipo_v_selected_1').prop('disabled',false);
        $('.tipo_v_selected_2').prop('disabled',true);
    }
    if(tipo_v_selected==2){
        $('.tipo_v_selected_1').prop('disabled',true);
        $('.tipo_v_selected_2').prop('disabled',false);
    }
}
function validaciongeneral(){
    var validado=1;
    var tipov = $('#tipov option:selected').val();
    if(tipov>0){
        if ($('.bloqueo').length==0) {
            var checked=0;
            $(".filled-in").each(function() {
                if($(this).is(':checked')){
                    checked++;
                }
            });
            if (checked==0) {
                alertfunction('Atención!','Debe seleccionar que desea Vender');
                validado=0;
            }
            //Equipos========================================================
                if ($('#ventae').is(':checked')) {
                    if ($('.tipoventaequipo').val() == '' || $('.tipoventaequipo').val() == null) {
                         alertfunction('Atención!','Seleccione que tipo de venta de equipos se realizará.');
                         validado = 0;
                    } else if ($('#cotizar').val() == '' || $('#cotizar').val() == null) {
                         alertfunction('Atención!','Seleccione que forma de venta de equipos se realizará.');
                         validado = 0;
                    } else if ($('#cotizar').val() == 2 && $('#idfamilia').val() == null) {
                         alertfunction('Atención!','Seleccione la familia.');
                         validado = 0;
                    } else if ($('#cotizar').val() == 1) {
                         var addtp = 0;
                         $(".nombre:checked").each(function() {
                             addtp++;
                         });
                         if (addtp == 0) {
                             alertfunction('Atención!','Favor de selecionar por lo menos un equipos');
                             validado = 0;
                         }
                    }
                    if(validado==1){
                        if($('.tipoventaequipo option:selected').val()==2){
                            validado=2;
                        }  
                    }
                    
                }
                if ($('#ventac').is(':checked')) {
                    console.log('Validaciones de consumibles');
                    if ($('#opcionesConsumiblesRefacciones').find('.tipoConsumibles').val() == '' || $('#opcionesConsumiblesRefacciones').find('.tipoConsumibles').val() == null) {
                         alertfunction('Atención!','Seleccione que tipo de venta de consumibles se realizará.');
                         validado = 0;
                    } /*else if ($('.equipoListaConsumibles').val() == '' || $('.equipoListaConsumibles').val() == null) {
                         alertfunction('Atención!','Seleccione para que equipo se busca el consumible.');
                         retorno = 0;
                    } */else if ($('.toner').val() == '' || $('.toner').val() == null) {
                         alertfunction('Atención!','Seleccione el consumible deseado.');
                         validado = 0;
                    } else if ($('.piezas').val() == '' || $('.piezas').val() == null || $('.piezas').val() == 0) {
                         alertfunction('Atención!','Ingrese la cantidad de piezas del consumible.');
                         validado = 0;
                    }
                }
                if ($('#ventar').is(':checked')) {
                    console.log('Validaciones de refacciones');
                     if ($('#opcionesConsumiblesRefacciones').find('.tipoConsumibles').val() == '' || $('#opcionesConsumiblesRefacciones').find('.tipoConsumibles').val() == null) {
                         alertfunction('Atención!','Seleccione que tipo de venta de Refacciones se realizará.');
                         validado = 0;
                     }
                }
                if ($('#ventaa').is(':checked')) {
                    if($("#table_venta_accesorios tbody > tr").length==0){
                        validado = 0;
                    }
                }
            //========================================================

        }else{
            validado=0;  
            toastr["error"]("Stock insuficiente", "Advertencia");  
        }
    }else{
        validado=0;
        toastr["error"]("Seleccione la Empresa", "Advertencia");
    }

    return validado;
}
var incluirpoliza=0;
function saveventageneral(){
    var idCliente = $('#idCliente').val();
    var clientename = $('#clientename').val();
    formulario_cotizacion = $('#formulario_cotizacion');
    var datos = formulario_cotizacion.serialize();
    var rentas_detalle_modal = $('.tipoventaequipo option:selected').val();
    var tipoprecioequipo = $('#tipoprecioequipo option:selected').val();
    var pasa=1;
    //Equipos===========================================
        var nombre = [];
        var TABLA   = $("#tables_equipos_selected tbody > tr");
        TABLA.each(function(){  
            if ($(this).find("input[class*='equipo_check']").is(':checked')) {
                item = {};
                item['equipos'] = $(this).find("input[class*='equipo_check']").val();
                item['cantidad'] = $(this).find("input[id*='equipo_cantidad']").val();
                
                var equiposs = $(this).find("input[class*='equipo_check']").val();
                var equiposconsumiblelength=$('.equipo_c_'+equiposs+' > div').length;
                if (equiposconsumiblelength==0) {
                  //  pasa=0;// se quito esta validacion por que puede que el equipo no tenga consumibles
                }
                nombre.push(item);
            }else{
                var equiposs = $(this).find("input[class*='equipo_check']").val();
                $('.equipo_c_'+equiposs).html('');
                $('.equipo_s_'+equiposs).html('');
            }
        });
        aInfoe   = JSON.stringify(nombre);
    //Equipos accesorios consumibles===========================================
        var TABLAa   = $(".equipo_accessoriosselected > div");
        var DATAa  = [];
            TABLAa.each(function(){         
                item = {};
                item ["equiposelected"]   = $(this).find("input[id*='equiposelected']").val();
                item ["cantidad"]   = $(this).find("input[id*='accesoriocantidadn']").val();
                item ["accesorioselected"]  = $(this).find("input[id*='accesorioselected']").val();
                DATAa.push(item);
            });
            INFOa  = new FormData();
            aInfoa   = JSON.stringify(DATAa);

        var TABLAc   = $(".equipo_consumibleselected > div");
        var DATAc  = [];
            TABLAc.each(function(){         
                item = {};
                item ["equiposelected"]   = $(this).find("input[id*='equiposelectedc']").val();
                item ["consumibleselected"]  = $(this).find("input[id*='consumibleselected']").val();
                DATAc.push(item);
            });
            INFOc  = new FormData();
            aInfoc   = JSON.stringify(DATAc);
    //Consumibles===========================================
        var equipoListaConsumibles = [];
         $(".equipoListaConsumibles").each(function() {
             equipoListaConsumibles.push($(this).val());
         });
         var toner = [];
         $(".toner").each(function() {
             toner.push($(this).val());
         });
         var bodegasc = [];
         $(".selectbodegacon").each(function() {
             bodegasc.push($(this).val());
         });
         var precio = [];
            // se cambio de tonerp a precionew para su edicion
            $(".precionew").each(function() {
                precio.push($(this).val()); 
            });
         var piezas = [];
         $(".piezas").each(function() {
             piezas.push($(this).val());
         });
    //Refacciones===========================================
        var DATAr  = [];
        var TABLA   = $(".tablerefacciones tbody > tr");
            TABLA.each(function(){         
            item = {};
            item ["equipo"] = $(this).find("input[id*='equipotx']").val();
            item ["serie"] = $(this).find("input[id*='seriees']").val();
            item ["toner"]   = $(this).find("input[id*='tonertx']").val();
            item ["tonerp"]  = $(this).find("input[id*='tonerptx']").val();
            item ["piezas"]  = $(this).find("input[id*='piezastx']").val();
            item ["bodega"]  = $(this).find("input[id*='bodegartx']").val();
            DATAr.push(item);
        });
        INFO  = new FormData();
        arrayrefaccion   = JSON.stringify(DATAr);
    //accesorios===========================================
        var DATAac  = [];
        var TABLAac   = $("#table_venta_accesorios  tbody > tr");
            TABLAac.each(function(){         
            item = {};
            item ["equipo"] = $(this).find("input[id*='equi_acce_selected']").val();
            item ["accesorio"]   = $(this).find("input[id*='acce_acce_selected']").val();
            item ["costo"]   = $(this).find("input[id*='costo_acce_selected']").val();
            item ["cantidad"]  = $(this).find("input[id*='cantidad_acce_selected']").val();
            item ["bodega"]  = $(this).find("input[id*='bodega_acce_selected']").val();
            DATAac.push(item);
        });
        INFO  = new FormData();
        arrayac   = JSON.stringify(DATAac);
    //poliza===========================================
        var DATAp  = [];
        var TABLAp   = $(".tablepolizas tbody > tr");
            TABLAp.each(function(){
            incluirpoliza=1;      
            item = {};
            item ["equipo"] = $(this).find("input[id*='equipoposelected']").val();
            item ["serie"]   = $(this).find("input[id*='polizavalserie']").val();
            item ["poliza"]   = $(this).find("input[id*='polizaval']").val();
            item ["polizad"]  = $(this).find("input[id*='polizadval']").val();
            item ["costo"]  = $(this).find("input[id*='polizacval']").val();
            item ["motivo"]  = $(this).find("input[id*='polizamotivo']").val();
            DATAp.push(item);
        });
        INFO  = new FormData();
        arrayPolizas   = JSON.stringify(DATAp);
        INFO.append('data', arrayPolizas);
    //===========================================

    datos = datos;
    if ($('#ventae').is(':checked')) {
        datos = datos+'&equiposIndividuales='+aInfoe +'&tipoprecioequipo='+tipoprecioequipo+'&accesorios='+aInfoa+'&consumibles='+aInfoc;
    }
    if ($('#ventac').is(':checked')) {
        datos = datos + '&arrayEquipos=' + equipoListaConsumibles + '&arrayToner=' + toner + '&arrayPiezas=' + piezas+'&arrayPrecio='+precio+'&bodegasc='+bodegasc;
    }
    if ($('#ventar').is(':checked')) {
        datos = datos +'&arrayRefaccion='+arrayrefaccion;
    }
    if ($('#ventaa').is(':checked')) {
        datos = datos +'&arrayac='+arrayac;
    }
    var pasapoliza=1;
    if ($('#ventap').is(':checked')) {
        datos = datos+'&arrayPolizas='+arrayPolizas;
        TABLAp.length
        if(TABLAp.length==0){
            pasapoliza=0;
        }
    }

    var tipoDocumento = '';
    var idCliente = $('#idCliente').val();
    console.log('idCliente: '+idCliente);
    if(validar_refaccion_servicio()==1){
        if(incluirpoliza ==1){
            if(pasapoliza ==1 ){
                if(pasa == 1){
                    $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Ventas/nuevaventageneral",
                        data: datos,
                        success: function (response){
                            var array = $.parseJSON(response);
                            if(array.combinada>0){
                                alertfunction('Éxito!','Se ha creado la venta combinada: '+array.combinada);  
                            }else{
                                
                                if(array.idventa>0){
                                    alertfunction('Éxito!','Se ha creado la venta: '+array.idventa); 
                                }
                                if(array.idpoliza){
                                    alertfunction('Éxito!','Se ha creado la poliza: '+array.idpoliza); 
                                }
                            }
                            if(array.idventa>0){
                                if ($('#ventae').is(':checked')) {
                                    modalventasdetalle(array.idventa,0);
                                    setTimeout(function(){ 
                                        modalventasdetalle(array.idventa,0);
                                    }, 1000); 
                                }else{
                                    setTimeout(function(){ 
                                        window.location.href = base_url+"index.php/Ventas?idcli="+idCliente+"&emp="+clientename; 
                                    }, 2000); 
                                }
                            }else{
                                setTimeout(function(){ 
                                    window.location.href = base_url+"index.php/Ventas?idcli="+idCliente+"&emp="+clientename;  
                                }, 5000);   
                            }
                                
                            
                            setTimeout(function(){ 
                                window.location.href = base_url+"index.php/Ventas?idcli="+idCliente+"&emp="+clientename;  
                            }, 25000);  
                            
                            
                        },
                        error: function(response){
                            notificacion(1);
                        }
                    });

                }else{
                   alertfunction('Atención!','Falta seleccionar al menos un consumible de un equipo.');
                }
            }else{
                alertfunction('Atención!','Se selecciono Servicio pero no se a agregado');
                $('.addpolizatable').addClass('intermitente_red');
            }
        }else{
            $.confirm({
                boxWidth: '55%',
                useBootstrap: false,
                icon: 'fa fa-warning',
                title: 'Atención!',
                content: '¿Desea incluir un servicio a la venta?',
                type: 'red',
                typeAnimated: true,
                buttons:{
                    'Agregar': function (){
                        $('#ventap').click();
                        
                    },
                    'Sin Servicio': function () { 
                        incluirpoliza=1;
                        saveventageneral();
                    }
                }
            });
        }
    }else{
        var html='Favor de seleccionar un Servicio para la Refacción<br>';
            html+='Para continuar sin servicio se necesita permisos de administrador';
            html+='<input type="text" placeholder="Contraseña" id="contrasena" name="contrasena" class="name form-control"  onpaste="return false;" required/>';
        $.confirm({
                boxWidth: '55%',
                useBootstrap: false,
                icon: 'fa fa-warning',
                title: 'Atención!',
                content: html,
                type: 'red',
                typeAnimated: true,
                buttons:{
                    'Aceptar': function (){
                        var pass=$("input[name=contrasena]").val();
                        if (pass!='') {
                             $.ajax({
                                type:'POST',
                                url: base_url+"index.php/Sistema/solicitarpermiso/",
                                data: {
                                    pass:pass
                                },
                                success: function (response){
                                        var respuesta = parseInt(response);
                                        if (respuesta==1) {
                                            continuar_sin_ser=1;
                                            saveventageneral();
                                        }else{
                                            notificacion(2);
                                        }
                                },
                                error: function(response){
                                    notificacion(1);
                                }
                            });
                        }else{
                            notificacion(0);
                        }
                    },
                    'Cancelar': function (){
                    }
                }
            });
        setTimeout(function(){
            new MaskedPassword(document.getElementById("contrasena"), '\u25CF');
        },900);
    }
}

function confirmarfmcredito(){
    var html='Favor de confirmar el tipo de persona.';
        html+='<div class="row">\
                    <div class="col s3">\
                         <input type="radio" name="fisicamoral" id="fisicamoral_1" value="1" onchange="fismo()">\
                         <label for="fisicamoral_1">Física</label>\
                         <input type="radio" name="fisicamoral" id="fisicamoral_2" value="2" onchange="fismo()">\
                         <label for="fisicamoral_2">Moral</label>\
                    </div>\
                    <div class="col s3">\
                      <label>Credito</label>\
                      <select class="browser-default form-control-bmz" name="credito" id="credito" onchange="creditofun()">\
                        <option value="1"  class="credito_1">Sin crédito</option>\
                        <option value="15"  class="credito_15">15 dias</option>\
                        <option value="30"  class="credito_30">30 dias</option>\
                        <option value="45"  class="credito_45">45 dias</option>\
                        <option value="60"  class="credito_60">60 dias</option>\
                      </select>\
                    </div>\
                </div>';
    $.confirm({
        boxWidth: '55%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: html,
        type: 'red',
        typeAnimated: true,
        buttons:{
            Confirmar: function (){
                var fm=$('input:radio[name=fisicamoral]:checked').val();
                var cre=$('#credito option:selected').val();
                var cli=$('#idCliente').val();
                    $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Clientes/updateclifm",
                        data: {
                            fm:fm,
                            cre:cre,
                            cli:cli
                        },
                        success: function (response){
                             toastr["success"]("Datos actualizados", "Hecho");   
                        },
                        error: function(response){
                            notificacion(1);
                        }
                    });
                
            },
            'Cancelar': function () { }
        }
    });
}
function fismo(){
    var fm=$('input:radio[name=fisicamoral]:checked').val();
    if(fm==1){
        $('#credito').val(1);
    }else{
        $('#credito').val(15);
    }
}
function creditofun(){
    var idCliente = $('#idCliente').val();
    var cre =$('#credito').val();
    var fm=$('input:radio[name=fisicamoral]:checked').val();
    if(fm==1){
        if(cre>1){
            //===================================
                $.confirm({
                    boxWidth: '30%',
                    useBootstrap: false,
                    icon: 'fa fa-warning',
                    title: 'Atención!',
                    content: 'Se necesita permisos de administrador<br>'+
                             '<input type="text" placeholder="Contraseña" id="contrasena" name="contrasena" class="name form-control" required/>',
                    type: 'red',
                    typeAnimated: true,
                    buttons:{
                        confirmar: function (){
                            //var pass=$('#contrasena').val();
                            var pass=$("input[name=contrasena]").val();
                            if (pass!='') {
                                 $.ajax({
                                    type:'POST',
                                    url: base_url+"index.php/Sistema/solicitarpermiso/"+idCliente,
                                    data: {
                                        pass:pass
                                    },
                                    success: function (response){
                                            var respuesta = parseInt(response);
                                            if (respuesta==1) {
                                            }else{
                                                $('#credito').val(1);
                                            }
                                    },
                                    error: function(response){
                                        notificacion(1);
                                    }
                                });
                                
                            }else{
                                notificacion(0);
                                $('#credito').val(1);
                            }
                        },
                        cancelar: function (){
                            $('#credito').val(1);
                        }
                    }
                });
                setTimeout(function(){
                    new MaskedPassword(document.getElementById("contrasena"), '\u25CF'); 
                },1000);
            //====================================
        }
    }
    if(fm==2){
        if(cre>15){
            //===================================
                $.confirm({
                    boxWidth: '30%',
                    useBootstrap: false,
                    icon: 'fa fa-warning',
                    title: 'Atención!',
                    content: 'Se necesita permisos de administrador<br>'+
                             '<input type="text" placeholder="Contraseña" id="contrasena" name="contrasena" class="name form-control" required/>',
                    type: 'red',
                    typeAnimated: true,
                    buttons:{
                        confirmar: function (){
                            var pass=$('#contrasena').val();
                            if (pass!='') {
                                 $.ajax({
                                    type:'POST',
                                    url: base_url+"index.php/Sistema/solicitarpermiso/"+idCliente,
                                    data: {
                                        pass:pass
                                    },
                                    success: function (response){
                                            var respuesta = parseInt(response);
                                            if (respuesta==1) {
                                            }else{
                                                $('#credito').val(15);
                                            }
                                    },
                                    error: function(response){
                                        notificacion(1);
                                    }
                                });
                                
                            }else{
                                notificacion(0);
                                $('#credito').val(15);
                            }
                        },
                        cancelar: function (){
                            $('#credito').val(15);
                        }
                    }
                });
                setTimeout(function(){
                    new MaskedPassword(document.getElementById("contrasena"), '\u25CF'); 
                },1000);
            //====================================
        }
    }
    if(fm==undefined){
        $('#credito').val(1);
        notificacion(4);
    }
}
//==================================================================
function tipo2selected(){
    var tipo2=$('.tipo2selected option:selected').val();
    $('#v_ser_tipo').val(0);
    $('#v_ser_id').val(0);
    $('.addinfoser').html('');
    if(tipo2==1){
        $('.soloventas').show('show');
    }else{
        $('.soloventas').hide('show');
    }
}
function servicioaddcliente(){
    $('#modalservicioadd').modal('open');
    var idCliente = $('#idCliente').val();
    $.ajax({
        type:'POST',
        url: base_url+'Generales/servicioaddcliente',
        data: {
            cli: idCliente
        },
        async: false,
        statusCode:{
            404: function(data){
                    swal("Error", "404", "error");
            },
            500: function(){
                swal("Error", "500", "error"); 
            }
        },
        success:function(data){
          $('.servicioaddcliente').html(data);

        }
    });
}
function selectservent(ser,tipo){
    var ser_fecha=$('.seri_'+tipo+'_'+ser).data('fecha');
    $('#v_ser_tipo').val(tipo);
    $('#v_ser_id').val(ser);
    var htmlser='Servicio: '+ser+' Fecha:'+ser_fecha+' ';
        htmlser+='<button class="b-btn b-btn-danger" onclick="deleteser()"><i class="fas fa-trash"></i></button>';
    $('.addinfoser').html(htmlser);
    $('#modalservicioadd').modal('close');
}
function deleteser(){
    $('#v_ser_tipo').val(0);
    $('#v_ser_id').val(0);
    $('.addinfoser').html('');
}
//====================================================================
/*
function obtenerventaspendientes(idcliente){
    //restriccion para cuando tenga mas de 3 prefacturas pendientes de agregar pago
    $.ajax({
        type:'POST',
        url: base_url+'index.php/Reportes/rventasnum/'+idcliente,
        success:function(data){
            console.log(data);
            var numventas=parseInt(data);
            if(numventas>0){
                $.confirm({
                    boxWidth: '30%',
                    useBootstrap: false,
                    icon: 'fa fa-warning',
                    title: 'Atención!',
                    content: 'No se tiene permitido tiene '+numventas+' pendientes de pago!<br> Para continuar se necesita permisos de administrador<br>'+
                             '<input type="text" placeholder="Contraseña" id="contrasena" name="contrasena" class="name form-control"  onpaste="return false;" required/>',
                    type: 'red',
                    typeAnimated: true,
                    buttons:{
                        confirmar: function (){
                            //var pass=$('#contrasena').val();
                            var pass=$("input[name=contrasena]").val();
                            if (pass!='') {
                                 $.ajax({
                                    type:'POST',
                                    url: base_url+"index.php/Sistema/solicitarpermiso/"+idcliente,
                                    data: {
                                        pass:pass
                                    },
                                    success: function (response){
                                            var respuesta = parseInt(response);
                                            if (respuesta==1) {
                                            }else{
                                                notificacion(2);
                                                setTimeout(function(){
                                                    location.reload();
                                                },1000);
                                            }
                                    },
                                    error: function(response){
                                        notificacion(1);
                                        setTimeout(function(){
                                                    location.reload();
                                        },2000);
                                    }
                                });
                                
                            }else{
                                notificacion(0);
                                setTimeout(function(){
                                                    location.reload();
                                },2000);
                            }
                        },
                        'Ver Pendientes': function (){
                            obtenerventaspendientesinfo(idcliente);
                        },
                        cancelar: function (){
                            window.location.href = base_url+"index.php/Clientes";
                        }
                    }
                });
                setTimeout(function(){
                    new MaskedPassword(document.getElementById("contrasena"), '\u25CF');
                    
                },1000);
            }
        }
    });
}
*/
function obtenerventaspendientes(idcliente){
    //restriccion para cuando tenga mas de 3 prefacturas pendientes de agregar pago
    var blq_perm_ven_ser = $('#blq_perm_ven_ser').val();
    
    $.ajax({
        type:'POST',
        url: base_url+'index.php/Reportes/rventasnum/'+idcliente,
        success:function(data){
            console.log(data);
            var numventas=parseInt(data);
            //=======================
                if(blq_perm_ven_ser==1){
                    var html_contect='El cliente tiene '+numventas+' pendientes de pago!';
                }else{
                    var html_contect='El cliente tiene '+numventas+' pendientes de pago!<br> Para continuar se necesita permisos de administrador<br>'+
                                         '<input type="text" placeholder="Contraseña" id="contrasena" name="contrasena" class="name form-control"  onpaste="return false;" required/>';
                }
            //=======================
            if(numventas>0){
                $.confirm({
                    boxWidth: '30%',
                    useBootstrap: false,
                    icon: 'fa fa-warning',
                    title: 'Atención!',
                    content: html_contect,
                    type: 'red',
                    typeAnimated: true,
                    buttons:{
                        confirmar: function (){
                            if(blq_perm_ven_ser==1){

                            }else{
                                //var pass=$('#contrasena').val();
                                var pass=$("input[name=contrasena]").val();
                                if (pass!='') {
                                     $.ajax({
                                        type:'POST',
                                        url: base_url+"index.php/Sistema/solicitarpermiso/"+idcliente,
                                        data: {
                                            pass:pass
                                        },
                                        success: function (response){
                                                var respuesta = parseInt(response);
                                                if (respuesta==1) {
                                                }else{
                                                    notificacion(2);
                                                    setTimeout(function(){
                                                        location.reload();
                                                    },1000);
                                                }
                                        },
                                        error: function(response){
                                            notificacion(1);
                                            setTimeout(function(){
                                                        location.reload();
                                            },2000);
                                        }
                                    });
                                    
                                }else{
                                    notificacion(0);
                                    setTimeout(function(){
                                        location.reload();
                                    },2000);
                                }
                            }
                            
                        },
                        'Ver Pendientes': function (){
                            obtenerventaspendientesinfo(idcliente);
                        },
                        cancelar: function (){
                            if(blq_perm_ven_ser==1){}else{
                            window.location.href = base_url+"index.php/Clientes";
                            }
                        }
                    }
                });
                setTimeout(function(){
                    new MaskedPassword(document.getElementById("contrasena"), '\u25CF');
                    
                },1000);
            }
        }
    });
}
//==============================================================
function obtenerpres_pendientes_por_completar(){
    var idpersonal= $('#idpersonal').val();
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Ventas/pres_pendientes_por_completar",
        data: {
            per:idpersonal
        },
        success: function (response){
            var array = $.parseJSON(response);
            if(array.totalg>0){
                //===========================
                    var html_contect='Tiene '+array.totalg+' prefacturas por completar<br> Para continuar se necesita permisos de administrador y/o completar las prefacturas<br>'+
                                         '<input type="text" placeholder="Contraseña" id="contrasena2" name="contrasena2" class="name form-control-bmz" style="width:99%"  onpaste="return false;" required/>';
                    $.confirm({
                        boxWidth: '30%',
                        useBootstrap: false,
                        icon: 'fa fa-warning',
                        title: 'Atención!',
                        content: html_contect,
                        type: 'red',
                        typeAnimated: true,
                        buttons:{
                            confirmar: function (){
                                if(blq_perm_ven_ser==1){

                                }else{
                                    //var pass=$('#contrasena').val();
                                    var pass=$("input[name=contrasena2]").val();
                                    if (pass!='') {
                                         $.ajax({
                                            type:'POST',
                                            url: base_url+"index.php/Sistema/solicitarpermiso",
                                            data: {
                                                pass:pass
                                            },
                                            success: function (response){
                                                    var respuesta = parseInt(response);
                                                    if (respuesta==1) {
                                                    }else{
                                                        notificacion(2);
                                                        setTimeout(function(){
                                                            location.reload();
                                                        },1000);
                                                    }
                                            },
                                            error: function(response){
                                                notificacion(1);
                                                setTimeout(function(){
                                                            location.reload();
                                                },2000);
                                            }
                                        });
                                        
                                    }else{
                                        notificacion(0);
                                        setTimeout(function(){
                                                            location.reload();
                                        },2000);
                                    }
                                }
                                
                            },/*
                            'Ver Pendientes': function (){
                                obtenerventaspendientesinfo(idcliente);
                            },*/
                            cancelar: function (){
                                if(blq_perm_ven_ser==1){}else{
                                window.location.href = base_url+"index.php/Clientes";
                                }
                            }
                        }
                    });
                    setTimeout(function(){
                        new MaskedPassword(document.getElementById("contrasena2"), '\u25CF');
                        
                    },1000);
                //===========================
            }
        },
        error: function(response){
            notificacion(1);
            
        }
    });
}
function validar_refaccion_servicio(){
    var solo_ref=0;
    var pasa=0;
    if($('#ventar').is(':checked')){
        solo_ref=1;
    }
    if($('#ventae').is(':checked')){
        solo_ref=2;
    }
    if($('ventac').is(':checked')){
        solo_ref=2;
    }
    if($('ventap').is(':checked')){
        //solo_ref=2;
    }
    if($('ventaa').is(':checked')){
        solo_ref=2;
    }
    if(solo_ref==1){
        var TABLAp   = $(".tablepolizas tbody > tr");
            
        if(TABLAp.length>0){
            pasa=1;
        }else{
            pasa=0;
        }
    }else{
        pasa=1;  
    }
    if(continuar_sin_ser==1){
        pasa=1;
    }
    return pasa;
}
function verificarseriederentas(aux){
    var ser =$('#seriee').val();
    var solo_ref=0;
    if($('#ventar').is(':checked')){
        solo_ref=1;
    }
    if($('#ventae').is(':checked')){
        solo_ref=0;
    }
    if($('ventac').is(':checked')){
        solo_ref=0;
    }
    if($('ventap').is(':checked')){
        //solo_ref=2;
    }
    if($('ventaa').is(':checked')){
        solo_ref=0;
    }
    if(solo_ref==1){
        $.ajax({
            type:'POST',
            url: base_url+"index.php/Generales/verificarseriederentas",
            data: {
                ser:ser
            },
            success: function (response){
                var array = $.parseJSON(response);
                console.log(array);
                if(array.length>0){
                    //$("#refaccionesp option.esp").val(0).text("Especial(0)").prop("selected", true);
                    $('#detallesPolizacosto_'+aux+' .costocero').show();
                    $('#detallesPolizacosto_'+aux+' option.costocero').prop("selected", true);
                }
            },
            error: function(response){
                
            }
        });
                                        
    }
}