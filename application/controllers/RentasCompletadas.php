<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class RentasCompletadas extends CI_Controller {
    public function __construct()     {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('login_model');
        $this->load->model('Rentas_model');
        $this->load->model('ModeloCatalogos');
        $this->load->model('ConfiguracionDocumentos_Model');
        $this->load->model('Clientes_model');
        $this->load->library('menus');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d');
        $this->fechahoyc = date('Y-m-d H:i:s');
        $this->fechahoyL = date('Y-m-d_H_i_s');
    }
    // Listado de Empleados
    function index() {
        if($this->session->userdata('logeado')==true){
            // Obtiene los menús disponibles del sistema dependiendo el perfil, ayudado de la libreria "MENUS"
            $menus = $this->menus->getMenus($this->session->userdata('perfilid'));
            $data['menus'] = $menus['menus'];
            $data['submenusArray'] = $menus['submenusArray'];
        
            $this->load->view('header');
            $this->load->view('main',$data);
            $this->load->view('almacen/rentascmpl/listado');
            $this->load->view('footer');
            $this->load->view('almacen/rentascmpl/jslistado');
        }else{
            redirect('login');
        }
    }

    function getListadocontratosRentasIncompletas(){
        $datos = $this->Rentas_model->getListadocontratosRentasIncompletas();
        $json_data = array("data" => $datos);
        echo json_encode($json_data);
    }
    
    public function view($id=0,$view=0){
        $data['ventaId'] = $id;
        $data['dia_reg']=date('d');
        $data['mes_reg']=date('m');
        $data['ano_reg']=date('y');
        $data['proinven']=''; // es el numero de folio de la factura aparecera cuando se guarden los campos restantes o se finalise la edicion
        $data['cfd']='';
        $data['vence']='';// sera la fecha de vencimiento sera remplazada una ves guardad
        $data['vence_block']='';//el campo de vencimiento sera bloqueado una ves guardado
        $data['formapago']=0;
        $data['formapago_block']='';
        $data['cfdi']=0;
        $data['cfdi_block']='';
        $data['formacobro']='';
        $data['formacobro_block']='';
        $data['estado']=0;
        $data['cargo']='';
        $data['cargo_block']='';
        $data['metodopagoId']=0;
        $data['metodopagoId_block']='';

        $data['telId']=0;
        $data['telId_block']='';        
        $data['contactoId']=0;
        $data['contactoId_block']='';            
 
        $data['rfc_id']=0;
        $data['block_button']=0;
        $data['rfc_id_selectetd']='';

        $data['ordencompra']='';
        $data['horaentregainicio']='';
        $data['horaentregafin']='';
        $data['direccion_c']='';
        $data['equipo_acceso']='';
        $data['doc_acceso']='';
        $data['colonia']=''; 
        $data['calle']=''; 
        $data['num_ext']=''; 
        $data['municipio']=''; 
        $data['estadovals']='';
        $data['cfdi']='';
        $data['domicilio_entrega']='';
        $iddomicilioinstalacion=0;
        //=================================================================
        $where_c=array('idRenta'=>$id);
        $contrato = $this->ModeloCatalogos->getselectwheren('contrato',$where_c);
        foreach ($contrato->result() as $item) {
            $data['fechainicio']=date('d-m-y',strtotime($item->horaentregainicio));
            $data['ordencompra']=$item->ordencompra;
            $data['horaentregainicio']=date('h:i:A',strtotime($item->horaentregainicio));
            $data['horaentregafin']=date('h:i:A',strtotime($item->horaentregafin));
            $data['equipo_acceso']=$item->servicio_equipo;
            $data['doc_acceso']=$item->servicio_doc;
            $iddomicilioinstalacion=$item->domicilioinstalacion;
        }
        $where_d=array('idclientedirecc'=>$iddomicilioinstalacion);
        $clientes_direccion = $this->ModeloCatalogos->getselectwheren('clientes_direccion',$where_d);
        foreach ($clientes_direccion->result() as $item) {
            $data['direccion_c']=$item->direccion;
        }
        //$resultadov = $this->ModeloCatalogos->rentaventas($id);
        $where_ventas=array('id'=>$id);
        $resultadov = $this->ModeloCatalogos->getselectwheren('rentas',$where_ventas);
        $id_personal=0;
        foreach ($resultadov->result() as $item) {
            $id_personal=$item->id_personal;
            $idCliente=$item->idCliente;
        }
        $data['idCliente']=$idCliente;
        //==========================================================================
        $where_personal=array('personalId'=>$id_personal);
        $data['datoscontacto']=$this->ModeloCatalogos->getselectwheren('cliente_datoscontacto',array('clienteId'=>$idCliente,'activo'=>1));
        $resultadop = $this->ModeloCatalogos->getselectwheren('personal',$where_personal);
        $personal_nombre='';
        $personal_apellidop='';
        $personal_apellidom='';
        
        foreach ($resultadop->result() as $item) {
            $personal_nombre= $item->nombre;
            $personal_apellidop= $item->apellido_paterno;
            $personal_apellidom= $item->apellido_materno;
        }
        
        $data['rfc_datos']=$this->Clientes_model->getListadoDatosFiscales($idCliente); 
        $data['ini_personal']=substr($personal_nombre, 0, 1).substr($personal_apellidop, 0, 1).substr($personal_apellidom, 0, 1);
        //============================================
            $resultadocli = $this->ModeloCatalogos->db6_getselectwheren('clientes',array('id'=>$idCliente));
            foreach ($resultadocli->result() as $item) {
                $data['empresa']=$item->empresa;
                $data['razon_social']=$item->razon_social;
                $data['rfc']=$item->rfc;
                $data['calle']=$item->calle;
                $data['num_ext']=$item->num_ext;
                $data['colonia']=$item->colonia;
                $data['estado']=$item->estado;
                $data['cp']=$item->cp;
                $data['municipio']=$item->municipio;
                $data['email']=$item->email;
                $data['cargo']=$item->puesto_contacto;
            }
            /*
            $resultadoclidf = $this->ModeloCatalogos->getselectwheren('cliente_has_datos_fiscales',array('idCliente'=>$idCliente));
            foreach ($resultadoclidf->result() as $item) {
                $data['razon_social']=$item->razon_social;
                $data['rfc']=$item->rfc;
                $data['calle']=$item->calle;
                $data['num_ext']=$item->num_ext;
                $data['colonia']=$item->colonia;
                $data['cp']=$item->cp;
            }
            */

            $data['resultadoclitel'] = $this->ModeloCatalogos->db6_getselectwheren('cliente_has_telefono',array('idCliente'=>$idCliente));
            $data['resultadoclipcontacto'] = $this->ModeloCatalogos->db6_getselectwheren('cliente_has_persona_contacto',array('idCliente'=>$idCliente,'status'=>1));
            $data['datoscontacto'] = $this->ModeloCatalogos->db6_getselectwheren('cliente_datoscontacto',array('clienteId'=>$idCliente));
        //================================================================================================================================ 
            
            //$data['rentaventas'] = $this->ModeloCatalogos->rentaventasd($id);
            $data['rentaventasacc'] = $this->ModeloCatalogos->rentaventasaccesoriosd($id);
            $data['rentaventasacc_dev'] = $this->ModeloCatalogos->rentaventasaccesoriosd_dev($id);
        //===========================================
            //$data['rentaventasseries'] = $this->ModeloCatalogos->rentaventasdseries($id);
            $data['metodopagorow']=$this->ModeloCatalogos->db6_getselectwheren('f_metodopago',array('activo'=>1));
            $data['formapagorow']=$this->ModeloCatalogos->db6_getselectwheren('f_formapago',array('activo'=>1));
            $data['cfdirow']=$this->ModeloCatalogos->db6_getselectwheren('f_uso_cfdi',array('activo'=>1));
            $data['estadorow']=$this->ModeloCatalogos->db6_getselectwheren('estado',array('activo'=>1));
            $data['configuracionCotizacion'] = $this->ConfiguracionDocumentos_Model->getConfiguracion(1);
        //============================================================================================
            $redirect=0;
        $resultprefactura = $this->ModeloCatalogos->db6_getselectwheren('prefactura',array('ventaId'=>$id,'tipo'=>2));
        foreach ($resultprefactura->result() as $item) {
            $redirect=1;
            $data['proinven']=$item->prefacturaId;
            $data['vence']=$item->vencimiento;
            $data['vence_block']='disabled';
            $data['metodopagoId']=$item->metodopagoId;
            $data['metodopagoId_block']='disabled';
            $data['formapago']=$item->formapagoId;
            $data['formapago_block']='disabled';
            $data['cfdi']=$item->usocfdiId;
            $data['cfdi_block']='disabled';
            $data['formacobro']=$item->formadecobro;
            $data['formacobro_block']='disabled';

        
            $data['telId']=$item->telId;
            $data['telId_block']='disabled';
        
            $data['contactoId']=$item->contactoId;
            $data['contactoId_block']='disabled';
            
            $data['cargo']=$item->cargo;
            $data['cargo_block']='disabled';
            $data['block_button']=1;
            $fechareg=$item->reg;
            $data['dia_reg']=date('d',strtotime($fechareg));
            $data['mes_reg']=date('m',strtotime($fechareg));
            $data['ano_reg']=date('y',strtotime($fechareg));
            $data['rfc_id']=$item->rfc_id;
            $data['rfc_id_selectetd']='disabled';

            $data['colonia']=$item->d_colonia; 
            $data['calle']=$item->d_calle; 
            $data['num_ext']=$item->d_numero; 
            $data['municipio']=$item->d_ciudad; 
            $data['estadovals']=$item->d_estado;
            $data['cfdi']='';
            $data['domicilio_entrega']=$item->domicilio_entrega; 
                
        }
        if($view==0){
            if($redirect==1){

            }
        }
        $data['rentaventas'] = $this->ModeloCatalogos->rentaventasd0($id);
        if($view==0){
            if($redirect==1){
                $data['rentaventas'] = $this->ModeloCatalogos->rentaventasd2($id);
                $url='RentasCompletadas/viewpdf/'.$id;
                 redirect($url);
            }
        }
        //============================================================================================
        $this->load->view('almacen/rentascmpl/prefactura',$data);
    }
    public function view_info($id=0,$view=0){
        $data['ventaId'] = $id;
        $data['resultbodegas'] = $this->ModeloCatalogos->db6_getselectwheren('bodegas',array('activo'=>1));
        $data['rentaventas'] = $this->ModeloCatalogos->rentaventasd2($id);
        $data['rentaventasacc'] = $this->ModeloCatalogos->rentaventasaccesoriosd($id);
        $this->load->view('almacen/rentascmpl/prefactura_info',$data);
    }
    public function save(){
        $data = $this->input->post();
        $id=$data['ventaId'];
        $data['tipo']=2;
        $this->ModeloCatalogos->Insert('prefactura',$data);
        $dataup['prefactura']=1;
        $this->ModeloCatalogos->updateCatalogo('rentas',$dataup,array('id' => $id));
    }
    public function saveh(){
        $data = $this->input->post();
        $id=$data['ventaId'];
        $data['tipo']=4;
        $data['reg']=$this->fechahoyc;
        $this->ModeloCatalogos->Insert('prefactura',$data);
        //$dataup['prefactura']=1;
        //$this->ModeloCatalogos->updateCatalogo('rentas',$dataup,array('id' => $id));
    }
    public function viewpdf($id=0,$tipov=0){
        $data['idRenta']=$id;
        $data['domicilio_entrega']='';
        $data['tel_local']='';
        $data['persona_contacto']='';
        //$this->load->library('Pdf');
        //===
        $data['dia_reg']=date('d');
        $data['mes_reg']=date('m');
        $data['ano_reg']=date('y');
        //==
        $data['cfd']=''; 
        //== 
        $data['configuracionCotizacion'] = $this->ConfiguracionDocumentos_Model->getConfiguracion(1);
        $where_ventas=array('id'=>$id);
        $resultadov = $this->ModeloCatalogos->db6_getselectwheren('rentas',$where_ventas);
        $id_personal=0;
        foreach ($resultadov->result() as $item) {
            $id_personal=$item->id_personal;
            $idCliente=$item->idCliente;
        }
        //==========================================================================
        $where_personal=array('personalId'=>$id_personal);
        $resultadop = $this->ModeloCatalogos->db6_getselectwheren('personal',$where_personal);
        $personal_nombre='';
        $personal_apellidop='';
        $personal_apellidom='';
        
        foreach ($resultadop->result() as $item) {
            $personal_nombre= $item->nombre;
            $personal_apellidop= $item->apellido_paterno;
            $personal_apellidom= $item->apellido_materno;
        }

       $data['rfc_datos']=$this->Clientes_model->getListadoDatosFiscales($idCliente); 
       $data['ini_personal']=substr($personal_nombre, 0, 1).substr($personal_apellidop, 0, 1).substr($personal_apellidom, 0, 1);
       //============
       $resultprefactura = $this->ModeloCatalogos->db6_getselectwheren('prefactura',array('ventaId'=>$id,'tipo'=>2));
        foreach ($resultprefactura->result() as $item) {
            $data['proinven']=$item->prefacturaId;
            $data['vence']=date('d-m-y',strtotime($item->vencimiento));
            $metodopagoId=$item->metodopagoId;
            $formapago=$item->formapagoId;
            $cfdi=$item->usocfdiId;
            $data['formacobro']=$item->formadecobro;
            $telId=$item->telId;
            $contactoId=$item->contactoId;
            $data['cargo']=$item->cargo;
            $fechareg=$item->reg;
            $data['dia_reg']=date('d',strtotime($fechareg));
            $data['mes_reg']=date('m',strtotime($fechareg));
            $data['ano_reg']=date('y',strtotime($fechareg));
            $rfc_id=$item->rfc_id; 
            $data['d_calle']=$item->d_calle;
            $data['d_numero']=$item->d_numero;
            $data['d_colonia']=$item->d_colonia;
            $data['d_ciudad']=$item->d_ciudad;
            $data['d_estado']=$item->d_estado;
            $data['domicilio_entrega']=$item->domicilio_entrega;
        }
        //datos del rfc
          $rfc_c=$this->Clientes_model->getListadoDatosFiscalesprefactura($rfc_id);
          foreach ($rfc_c as $item) {
            $data['razon_social']=$item->razon_social;
            $data['rfc']=$item->rfc;
            $data['num_ext']=$item->num_ext;
            $data['colonia']=$item->colonia;
            $data['calle']=$item->calle;
            $data['cp']=$item->cp;
            $data['rs_estado']=$item->estado;
          }
        //
          $metodopagorow=$this->ModeloCatalogos->db6_getselectwheren('f_metodopago',array('id'=>$metodopagoId));
          foreach ($metodopagorow->result() as $item) {
            $data['metodopago_text']=$item->metodopago_text;
          }
        //
          $formapagorow=$this->ModeloCatalogos->db6_getselectwheren('f_formapago',array('id'=>$formapago));
          foreach ($formapagorow->result() as $item) {
            $data['formapago_text']=$item->formapago_text;
          }
        // 
          $cfdirow=$this->ModeloCatalogos->db6_getselectwheren('f_uso_cfdi',array('id'=>$cfdi));
          foreach ($cfdirow->result() as $item) {
            $data['uso_cfdi_text']=$item->uso_cfdi_text;
          } 
        //  
        $resultadocli = $this->ModeloCatalogos->db6_getselectwheren('clientes',array('id'=>$idCliente));
            foreach ($resultadocli->result() as $item) {
                $data['empresa']=$item->empresa;
                $estado=$item->estado;
                $data['municipio']=$item->municipio;
                $data['email']=$item->email;
               // $data['cargo']=$item->puesto_contacto;
            }
        //
        $estadorow=$this->ModeloCatalogos->db6_getselectwheren('estado',array('EstadoId'=>$estado));    
            foreach ($estadorow->result() as $item) {
            $data['estado']= $item->Nombre;
            } 

        //
        $resultadoclitel = $this->ModeloCatalogos->db6_getselectwheren('cliente_has_telefono',array('id'=>$telId));
            foreach ($resultadoclitel->result() as $item) {
            $data['tel_local']=$item->tel_local;
            } 
        //    
        $resultadoclipcontacto = $this->ModeloCatalogos->db6_getselectwheren('cliente_has_persona_contacto',array('id'=>$contactoId));
            foreach ($resultadoclipcontacto->result() as $item) {
            //$data['persona_contacto']=$item->persona_contacto;
            }
        $resultadoclipcontactos = $this->ModeloCatalogos->db6_getselectwheren('cliente_datoscontacto',array('datosId'=>$contactoId));
            foreach ($resultadoclipcontactos->result() as $item) {
                $data['persona_contacto']=$item->atencionpara;
                $data['email']=$item->email;
                $data['cargo']=$item->puesto;
            }
        $data['rentaventas'] = $this->ModeloCatalogos->rentaventasd2($id);       
        //
        $where_constrato=array('idRenta'=>$id);
        $resultadoc = $this->ModeloCatalogos->db6_getselectwheren('contrato',$where_constrato);
        foreach ($resultadoc->result() as $item) {
            $data['idcontrato']=$item->idcontrato;
            $data['videncia']=$item->vigencia;
            $data['clicks_mono']=$item->clicks_mono;       
            $data['precio_c_e_mono']=$item->precio_c_e_mono;   
            $data['clicks_color']=$item->clicks_color;  
            $data['precio_c_e_color']=$item->precio_c_e_color;  
            $data['rentaadelantada']=$item->rentaadelantada;   
            $data['rentadeposito']=$item->rentadeposito;
            $data['rentacolor']=$item->rentacolor;
            $data['tipocontrato']=$item->tipocontrato;
            $data['tiporenta']=$item->tiporenta;
            $data['fechainicio']=date('d-m-y',strtotime($item->horaentregainicio));
            $data['ordencompra']=$item->ordencompra;
            $data['horaentregainicio']=date('h:i:A',strtotime($item->horaentregainicio));
            $data['horaentregafin']=date('h:i:A',strtotime($item->horaentregafin));
            $data['equipo_acceso']=$item->servicio_equipo;
            $data['doc_acceso']=$item->servicio_doc;
            $iddomicilioinstalacion=$item->domicilioinstalacion;
            $data['folio']=$item->folio;
            //$data['persona_contacto']=$item->contacto;
            $data['uuid']=$item->uuid;

        }   
            $where_d=array('idclientedirecc'=>$iddomicilioinstalacion);
            $clientes_direccion = $this->ModeloCatalogos->db6_getselectwheren('clientes_direccion',$where_d);
            $data['direccion_c']='';
            foreach ($clientes_direccion->result() as $item) {
                $data['direccion_c']=$item->direccion;
            }
        ///
        ///===========
            if($tipov==1){
                $this->load->view('Reportes/rentasprefactura',$data);
            }else{
                $this->load->view('Reportes/prefactura_c',$data);
            }
        
    }
    public function viewh($id=0,$idhostorial=0,$tipov=0){
        $idhostorial=$idhostorial;
        $data['ventaId'] = $idhostorial;
        $data['dia_reg']=date('d');
        $data['mes_reg']=date('m');
        $data['ano_reg']=date('y');
        $data['proinven']=''; // es el numero de folio de la factura aparecera cuando se guarden los campos restantes o se finalise la edicion
        $data['cfd']='';
        $data['vence']='';// sera la fecha de vencimiento sera remplazada una ves guardad
        $data['vence_block']='';//el campo de vencimiento sera bloqueado una ves guardado
        $data['formapago']=0;
        $data['formapago_block']='';
        $data['cfdi']=0;
        $data['cfdi_block']='';
        $data['formacobro']='';
        $data['formacobro_block']='';
        $data['estado']=0;
        $data['cargo']='';
        $data['cargo_block']='';
        $data['metodopagoId']=0;
        $data['metodopagoId_block']='';

        $data['telId']=0;
        $data['telId_block']='';        
        $data['contactoId']=0;
        $data['contactoId_block']='';            
 
        $data['rfc_id']=0;
        $data['block_button']=0;
        $data['rfc_id_selectetd']='';

        $data['ordencompra']='';
        $data['horaentregainicio']='';
        $data['horaentregafin']='';
        $data['direccion_c']='';
        $data['equipo_acceso']='';
        $data['doc_acceso']='';
        $data['colonia']=''; 
        $data['calle']=''; 
        $data['num_ext']=''; 
        $data['municipio']=''; 
        $data['estadovals']='';
        $data['cfdi']='';
        $data['domicilio_entrega']='';
        $data['observaciones']='';
        $iddomicilioinstalacion=0;
        //=================================================================
        $where_c=array('idRenta'=>$id);
        $contrato = $this->ModeloCatalogos->db6_getselectwheren('contrato',$where_c);
        foreach ($contrato->result() as $item) {
            $data['fechainicio']=date('d-m-y',strtotime($item->horaentregainicio));
            $data['ordencompra']=$item->ordencompra;
            $data['horaentregainicio']=date('h:i:A',strtotime($item->horaentregainicio));
            $data['horaentregafin']=date('h:i:A',strtotime($item->horaentregafin));
            $data['equipo_acceso']=$item->servicio_equipo;
            $data['doc_acceso']=$item->servicio_doc;
            $data['folio']=$item->folio;
            $iddomicilioinstalacion=$item->domicilioinstalacion;
        }
        $where_d=array('idclientedirecc'=>$iddomicilioinstalacion);
        $clientes_direccion = $this->ModeloCatalogos->db6_getselectwheren('clientes_direccion',$where_d);
        foreach ($clientes_direccion->result() as $item) {
            $data['direccion_c']=$item->direccion;
        }
        //$resultadov = $this->ModeloCatalogos->rentaventas($id);
        $where_ventas=array('id'=>$id);
        $resultadov = $this->ModeloCatalogos->db6_getselectwheren('rentas',$where_ventas);
        $id_personal=0;
        foreach ($resultadov->result() as $item) {
            $id_personal=$item->id_personal;
            $idCliente=$item->idCliente;
        }
        $data['idCliente']=$idCliente;
        //==========================================================================
        $where_personal=array('personalId'=>$id_personal);
        $data['datoscontacto']=$this->ModeloCatalogos->db6_getselectwheren('cliente_datoscontacto',array('clienteId'=>$idCliente,'activo'=>1));
        $resultadop = $this->ModeloCatalogos->db6_getselectwheren('personal',$where_personal);
        $personal_nombre='';
        $personal_apellidop='';
        $personal_apellidom='';
        
        foreach ($resultadop->result() as $item) {
            $personal_nombre= $item->nombre;
            $personal_apellidop= $item->apellido_paterno;
            $personal_apellidom= $item->apellido_materno;
        }
        
        $data['rfc_datos']=$this->Clientes_model->getListadoDatosFiscales($idCliente); 
        $data['ini_personal']=substr($personal_nombre, 0, 1).substr($personal_apellidop, 0, 1).substr($personal_apellidom, 0, 1);
        //============================================
            $resultadocli = $this->ModeloCatalogos->db6_getselectwheren('clientes',array('id'=>$idCliente));
            foreach ($resultadocli->result() as $item) {
                $data['empresa']=$item->empresa;
                $data['razon_social']=$item->razon_social;
                $data['rfc']=$item->rfc;
                $data['calle']=$item->calle;
                $data['num_ext']=$item->num_ext;
                $data['colonia']=$item->colonia;
                $data['estado']=$item->estado;
                $data['cp']=$item->cp;
                $data['municipio']=$item->municipio;
                $data['email']=$item->email;
                $data['cargo']=$item->puesto_contacto;
            }
            /*
            $resultadoclidf = $this->ModeloCatalogos->getselectwheren('cliente_has_datos_fiscales',array('idCliente'=>$idCliente));
            foreach ($resultadoclidf->result() as $item) {
                $data['razon_social']=$item->razon_social;
                $data['rfc']=$item->rfc;
                $data['calle']=$item->calle;
                $data['num_ext']=$item->num_ext;
                $data['colonia']=$item->colonia;
                $data['cp']=$item->cp;
            }
            */

            $data['resultadoclitel'] = $this->ModeloCatalogos->db6_getselectwheren('cliente_has_telefono',array('idCliente'=>$idCliente));
            $data['resultadoclipcontacto'] = $this->ModeloCatalogos->db6_getselectwheren('cliente_has_persona_contacto',array('idCliente'=>$idCliente));
            $data['datoscontacto'] = $this->ModeloCatalogos->db6_getselectwheren('cliente_datoscontacto',array('clienteId'=>$idCliente));
        //================================================================================================================================
            $rentaventashg = $this->ModeloCatalogos->db6_getselectwheren('rentas_historial',array('id'=>$idhostorial));
            $data['rentaventashg'] = $rentaventashg->row();
            $data['rentaventas'] = $this->ModeloCatalogos->rentaventasd2($id);
            $data['rentaventash'] = $this->ModeloCatalogos->rentaventasdh($id,$idhostorial);
            $data['rentavacessoriosh'] = $this->ModeloCatalogos->rentavacessoriosh($id,$idhostorial);
            $data['rentavconsumiblesh'] = $this->ModeloCatalogos->rentavconsumiblesh($id,$idhostorial);

            $data['rentavacessoriosh_dev'] = $this->ModeloCatalogos->rentavacessoriosh_dev($id,$idhostorial);
            $data['rentavconsumiblesh_dev'] = $this->ModeloCatalogos->rentavconsumiblesh_dev($id,$idhostorial);
        //===========================================
            //$data['rentaventasseries'] = $this->ModeloCatalogos->rentaventasdseries($id);
            $data['metodopagorow']=$this->ModeloCatalogos->db6_getselectwheren('f_metodopago',array('activo'=>1));
            $data['formapagorow']=$this->ModeloCatalogos->db6_getselectwheren('f_formapago',array('activo'=>1));
            $data['cfdirow']=$this->ModeloCatalogos->db6_getselectwheren('f_uso_cfdi',array('activo'=>1));
            $data['estadorow']=$this->ModeloCatalogos->db6_getselectwheren('estado',array('activo'=>1));
            $data['configuracionCotizacion'] = $this->ConfiguracionDocumentos_Model->getConfiguracion(1);
        //============================================================================================
            $redirect=0;
        $resultprefactura = $this->ModeloCatalogos->db6_getselectwheren('prefactura',array('ventaId'=>$idhostorial,'tipo'=>4));
        foreach ($resultprefactura->result() as $item) {
            $redirect=1;
            $data['proinven']=$item->prefacturaId;
            $data['vence']=$item->vencimiento;
            $data['vence_block']='disabled';
            $data['metodopagoId']=$item->metodopagoId;
            $data['metodopagoId_block']='disabled';
            $data['formapago']=$item->formapagoId;
            $data['formapago_block']='disabled';
            $data['cfdi']=$item->usocfdiId;
            $data['cfdi_block']='disabled';
            $data['formacobro']=$item->formadecobro;
            $data['formacobro_block']='disabled';

        
            $data['telId']=$item->telId;
            $data['telId_block']='disabled';
        
            $data['contactoId']=$item->contactoId;
            $data['contactoId_block']='disabled';
            
            $data['cargo']=$item->cargo;
            $data['cargo_block']='disabled';
            $data['block_button']=1;
            $fechareg=$item->reg;
            $data['dia_reg']=date('d',strtotime($fechareg));
            $data['mes_reg']=date('m',strtotime($fechareg));
            $data['ano_reg']=date('Y',strtotime($fechareg));
            $data['rfc_id']=$item->rfc_id;
            $data['rfc_id_selectetd']='disabled';

            $data['colonia']=$item->d_colonia; 
            $data['calle']=$item->d_calle; 
            $data['num_ext']=$item->d_numero; 
            $data['municipio']=$item->d_ciudad; 
            $data['estadovals']=$item->d_estado;
            $data['cfdi']='';
            $data['domicilio_entrega']=$item->domicilio_entrega; 
            $data['observaciones']=$item->observaciones; 
                
        }
        //============================================================================================
        if($tipov==0){
            $this->load->view('almacen/rentascmpl/prefacturah',$data);
        }else{
            $this->load->view('Reportes/prefactura_rh',$data);
        }
        
    }
    public function viewh_info($id=0,$idhostorial=0){
        $idhostorial=$idhostorial;
        $data['ventaId'] = $idhostorial;
            $data['resultbodegas'] = $this->ModeloCatalogos->db6_getselectwheren('bodegas',array('activo'=>1));
        //============================================
        //================================================================================================================================
            $rentaventashg = $this->ModeloCatalogos->db6_getselectwheren('rentas_historial',array('id'=>$idhostorial));
            $data['rentaventashg'] = $rentaventashg->row();
            $data['rentaventas'] = $this->ModeloCatalogos->rentaventasd2($id);
            $data['rentaventash'] = $this->ModeloCatalogos->rentaventasdh($id,$idhostorial);
            $data['rentavacessoriosh'] = $this->ModeloCatalogos->rentavacessoriosh($id,$idhostorial);
            $data['rentavconsumiblesh'] = $this->ModeloCatalogos->rentavconsumiblesh($id,$idhostorial);
        
        //============================================================================================
        $this->load->view('almacen/rentascmpl/prefacturah_info',$data);
    }
}    