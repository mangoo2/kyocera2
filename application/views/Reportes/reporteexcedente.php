<?php
require_once dirname(__FILE__) . '/TCPDF3/examples/tcpdf_include.php'; 
require_once dirname(__FILE__) . '/TCPDF3/tcpdf.php';

$GLOBALS["nombreUsuario"]=$nombreUsuario;
$GLOBALS["emailUsuario"]=$emailUsuario;
$GLOBALS["idCotizacion"]=$idCotizacion;
$GLOBALS["fechareg"]=$fechareg;
$GLOBALS["fechareg2"]=$fechareg2;

$GLOBALS["empresa"]=$datosCliente->empresa;

$GLOBALS["tipov"]=$tipov;
$GLOBALS["tipovlogo"]=$configuracionCotizacion->logo1;
$GLOBALS["tipovtextoHeader"]=$configuracionCotizacion->textoHeader;

//
class MYPDF extends TCPDF {
  //Page header
  public function Header() {

      $img_file1=base_url().'public/img/cotheader1.png';
      $img_file2=base_url().'public/img/cotheader2.png';
      $img_file3=base_url().'public/img/cotheader3.png';
      if($GLOBALS["tipov"]==2){
        $img_file3=base_url().'app-assets/images/'.$GLOBALS["tipovlogo"];
      }                      
      $this->Image($img_file1, 0, 0, 0, 0, '', '', 10, false, 310, '', false, false, 0);
      $this->Image($img_file2, 1, 3, 70, 0, '', '', 10, false, 310, '', false, false, 0);
      $this->Image($img_file3, 160, 3, 40, 0, '', '', 10, false, 310, '', false, false, 0);

      $html = '<style type="text/css" class="print_css">
                  .titulocoti{
                    font-style: italic;
                    font-size: 13px;
                    color:#2e4064;
                  }
                  .numbercoti{
                    font-size: 14px;
                    color:#2e4064;
                    font-weight:bold;

                  }
                  .colorblue{
                    color:#2e4064;
                  }
              </style>';
      $html.= '<table border="0">
                  <tr>
                    <td width="34%"></td>
                    <td width="39%" class="titulocoti" align="center">'.$GLOBALS["tipovtextoHeader"].'<p></p></td>
                    <td width="29%"></td>
                  </tr>
                  <tr>
                    <td></td>
                    <td></td>
                    <td class="numbercoti">
                      <table>
                        <tr>
                          <td></td>
                          <td height="20px;"></td>
                        </tr>
                        <tr>
                          <td colspan="2" align="center"> # '.$GLOBALS["idCotizacion"].'</td>
                        </tr>
                        <tr>
                          <td colspan="2" align="center">'.$GLOBALS["fechareg"].'<br>'.$GLOBALS["fechareg2"].'</td>
                        </tr>
                      </table>
                      </td>
                  </tr>
                </table>
                <table border="0">
                  <tr>
                    <td></td>
                  </tr>
                  <tr>
                    <td></td>
                  </tr>
                </table>
                
                ';      
      $this->writeHTML($html, true, false, true, false, '');
  }

    // Page footer
  public function Footer() {
    $html = '';
      $img_file=base_url().'public/img/cot_footer.jpg';
                            //x,y    w,h
      $this->Image($img_file, 0, 255, 0, 0, '', '', 10, false, 310, '', false, false, 0);
      $html .= '<table border="0">
                  <tr><td></td><td></td><td ></td></tr>
                  <tr><td></td><td></td><td ></td></tr>
                  <tr><td></td><td></td><td ></td></tr>
                  <tr><td></td><td></td><td ></td></tr>
                  <tr>
                    <td></td>
                    <td></td>
                    <td style="font-weight:bold;color:white;text-align:center;">Le atiende:<br>'.$GLOBALS["nombreUsuario"].'<br>'.$GLOBALS["emailUsuario"].'</td>
                  </tr>
                  <tr>
                    <td></td>
                    <td></td>
                    <td style="font-weight:bold;color:white; text-align:center" align="center">Pagina  '.$this->getAliasNumPage().'/'.$this->getAliasNbPages().'</td>
                  </tr>
                </table>';
      $this->writeHTML($html, true, false, true, false, '');
  }
} 
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('kyocera');
$pdf->SetTitle('Cotización '.$idCotizacion);
$pdf->SetSubject('Cotización');
$pdf->SetKeywords('Cotización');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins('8', '45', '8');
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin("45");

// set auto page breaks
$pdf->SetAutoPageBreak(true, '45');

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//$pdf->SetFont('dejavusans', '', 9);
// add a page
$pdf->AddPage('P', 'A4');
$col1='20';
$col2='10';
$col3='11';
$col4='47';
$col5='12';

$html='<style type="text/css" class="print_css">
          .div{
            border: 1px solid black;
            width: 100px;
          }
          .tamanotext{
            font-size:11px !important;
          }
          .titulositems{
            color:red;
            font-size:14px;
          }
          .titulositems2{font-size:12px;}
          .titulositems4{font-size:14px;}
          .trborderbuttom{
            border-bottom:2px sold red;
          }
          .trbordertop{
            border-top:1px sold #eaa2a2;
          }
          .tableitems{
            color:#2e4064;
          }
          .colorblue{color:#2e4064;}

          .t_c{text-align:center;}
          .httable{
              font-size: 10px;
            }
       </style>';
                $html.='<table border="0">';
                  $html.='<tr>';
                    $html.='<td width="25%" class="colorblue titulositems2" style="font-weight:bold">Empresa:</td>';
                    $html.='<td width="75%" class="colorblue titulositems2">'.$GLOBALS["empresa"].'</td>';
                  $html.='</tr>';
                  $html.='<tr>';
                    $html.='<td colspan="2"></td>';
                  $html.='</tr>';
                  $html.='<tr>';
                    $html.='<td colspan="2" class="colorblue titulositems4 t_c" style="font-weight:bold">Cotización de excedentes</td>';
                  $html.='</tr>';
                  $html.='<tr><td colspan="2"></td></tr>';
                $html.='</table>';

                $html.='<table border="1" cellpadding="2">';
                  $html.='<thead>';
                    $html.='<tr>';
                      $html.='<th style="background-color: #d8d4d4; color: black" align="center" class="httable" width="20%">CANTIDAD</th>';
                      $html.='<th style="background-color: #d8d4d4; color: black" align="center" class="httable" width="40%">DESCRIPCIÓN</th>';
                      $html.='<th style="background-color: #d8d4d4; color: black" align="center" class="httable" width="20%">PRECIO UNITARIO</th>';
                      $html.='<th style="background-color: #d8d4d4; color: black" align="center" class="httable" width="20%">TOTAL</th>';
                    $html.='</tr>';
                  $html.='</thead>';
                  $html.='<tbody>';
                  if($indi_global==2){//1 individual 2 global
                    $subtotalgeneral=0;
                    foreach ($result_pre->result() as $item) {
                      if($item->cant_ext_mono>0){
                        $total_gi=$item->cant_ext_mono*$precio_c_e_mono;
                        $subtotalgeneral+=$total_gi;
                        $html.='<tr>';
                          $html.='<td width="20%" class="httable" align="center">'.number_format(($item->cant_ext_mono),0,'.',',').'</td>';
                          $html.='<td width="40%" class="httable" align="center">CLICKS EXCEDENTES DEL '.$item->periodo_inicial.' al '.$item->periodo_final.'</td>';
                          $html.='<td width="20%" class="httable" align="center">$ '.number_format(($precio_c_e_mono),2,'.',',').'</td>';
                          $html.='<td width="20%" class="httable" align="center">$ '.number_format(($total_gi),2,'.',',').'</td>';
                        $html.='</tr>';
                      }
                      if($item->cant_ext_color>0){
                        $total_gi=$item->cant_ext_color*$precio_c_e_color;
                        $subtotalgeneral+=$total_gi;
                        $html.='<tr>';
                          $html.='<td width="20%" class="httable" align="center">'.number_format(($item->cant_ext_color),0,'.',',').'</td>';
                          $html.='<td width="40%" class="httable" align="center">CLICKS EXCEDENTES COLOR DEL '.$item->periodo_inicial.' al '.$item->periodo_final.'</td>';
                          $html.='<td width="20%" class="httable" align="center">$ '.number_format(($precio_c_e_color),2,'.',',').'</td>';
                          $html.='<td width="20%" class="httable" align="center">$ '.number_format(($total_gi),2,'.',',').'</td>';
                        $html.='</tr>';
                      }
                      
                    }

                        $html.='<tr><td></td><td></td><td></td><td></td></tr>';
                        $html.='<tr><td></td><td></td><td></td><td></td></tr>';
                        $iva=round($subtotalgeneral*0.16,2);
                        $totalgeneral=$subtotalgeneral+$iva;

                        $html.='<tr>';
                          $html.='<td class="httable" align="center" rowspan="3" colspan="2">CLICKS EXCEDENTES PERIODO DE PRODUCCION DEL<BR>'.$p_ini.' al '.$p_fin.'</td>';
                          $html.='<td class="httable" align="center">Subtotal</td>';
                          $html.='<td class="httable" align="center">$ '.number_format(($subtotalgeneral),2,'.',',').'</td>';
                        $html.='</tr>';
                        $html.='<tr>';
                          $html.='<td class="httable" align="center">IVA</td>';
                          $html.='<td class="httable" align="center">$ '.number_format(($iva),2,'.',',').'</td>';
                        $html.='</tr>';
                        $html.='<tr>';
                          $html.='<td class="httable" align="center">TOTAL</td>';
                          $html.='<td class="httable" align="center">$ '.number_format(($totalgeneral),2,'.',',').'</td>';
                        $html.='</tr>';
                  }else{
                    //===============
                      $valor_g_subtotal=0;
                      $valor_g_subtotalcolor=0;
                      $valor_g_excedente_m=0;
                      $valor_g_excedente_c=0;

                      $subtotalgeneral=0;
                    //===============

                    //========================================================= esta es la misma estructura del archivo facturad2.php
                        foreach ($detalleperido->result() as $item) {

                          if($item->tipo==2){
                            $title='COLOR';
                            $volumenincluido =  $item->clicks_color;
                            $costorenta=round($item->precio_rc,2);
                            $costoexcedente  =  round($item->precio_c_e_color,2);
                            if($idcondicionextra>0){
                              $condicionesdll = $this->ModeloCatalogos->getselectwheren('rentas_condiciones_detalle',array('idc'=>$idcondicionextra,'equiporow'=>$item->id,'serieId'=>$item->serieId));
                              foreach ($condicionesdll->result() as $itemcdll) {
                                $volumenincluido = $itemcdll->click_c;
                                $costorenta = $itemcdll->renta_c;
                                $costoexcedente = $itemcdll->excedente_c;
                              }
                            }
                            $valor_g_subtotalcolor=$valor_g_subtotalcolor+$costorenta;
                          }else{
                            $title='MONO';
                            $volumenincluido =  $item->clicks_mono;
                            $costorenta=round($item->precio_r,2);
                            $costoexcedente  =  round($item->precio_c_e_mono,3);
                            if($idcondicionextra>0){
                              $condicionesdll = $this->ModeloCatalogos->getselectwheren('rentas_condiciones_detalle',array('idc'=>$idcondicionextra,'equiporow'=>$item->id,'serieId'=>$item->serieId));
                              foreach ($condicionesdll->result() as $itemcdll) {
                                $volumenincluido = $itemcdll->click_m;
                                $costorenta = $itemcdll->renta_m;
                                $costoexcedente = $itemcdll->excedente_m;
                              }
                            }
                            $valor_g_subtotal=$valor_g_subtotal+$costorenta;
                          }
                          
                            $excedente=$item->produccion-$volumenincluido;
                            if ($excedente<0) {
                              $excedente=0;
                            }
                            if($item->tipo==2){
                              //$valor_g_excedente_c=$valor_g_excedente_c+($excedente*$costoexcedente);
                              $valor_g_excedente_c=($excedente*$costoexcedente);
                            }else{
                              //$valor_g_excedente_m=$valor_g_excedente_m+($excedente*$costoexcedente);
                              $valor_g_excedente_m=($excedente*$costoexcedente);
                            }
                            $totalexcedente=($excedente*$costoexcedente);
                            //$subtotal=$costorenta+$totalexcedente;
                            $subtotal=$totalexcedente;
                            $subtotaliva=$subtotal*0.16;
                            $totalgeneral=round($subtotal+$subtotaliva,2);
                            $subtotalgeneral+=$subtotal;
                              $html.='<tr>';
                                $html.='<td width="20%" class="httable" align="center">'.$excedente.'</td>';
                                $html.='<td width="40%" class="httable" align="center">CLICKS EXCEDENTES '.$title.' '.$item->modelo.' '.$item->serie.' DEL '.$p_ini.' al '.$p_fin.'</td>';
                                $html.='<td width="20%" class="httable" align="center">$'.$costoexcedente.'</td>';
                                $html.='<td width="20%" class="httable" align="center">$'.$subtotal.'</td>';
                              $html.='</tr>';
                          
                        }

                        $iva=round($subtotalgeneral*0.16,2);
                        $totalgeneral=$subtotalgeneral+$iva;
                        $html.='<tr>';
                          $html.='<td class="httable" align="center" rowspan="3" colspan="2">CLICKS EXCEDENTES PERIODO DE PRODUCCION DEL<BR>'.$p_ini.' al '.$p_fin.'</td>';
                          $html.='<td class="httable" align="center">Subtotal</td>';
                          $html.='<td class="httable" align="center">$ '.number_format(($subtotalgeneral),2,'.',',').'</td>';
                        $html.='</tr>';
                        $html.='<tr>';
                          $html.='<td class="httable" align="center">IVA</td>';
                          $html.='<td class="httable" align="center">$ '.number_format(($iva),2,'.',',').'</td>';
                        $html.='</tr>';
                        $html.='<tr>';
                          $html.='<td class="httable" align="center">TOTAL</td>';
                          $html.='<td class="httable" align="center">$ '.number_format(($totalgeneral),2,'.',',').'</td>';
                        $html.='</tr>';
                    //=========================================================
                  }
                    
                  $html.='</tbody>';
                  
                $html.='</table>';

          //log_message('error',$html);     
//================================================
//echo $html;
//var_dump($html);
          //log_message('error',$html);
$pdf->writeHTML($html, true, false, true, false, '');
//$pdf->Output('Captura.pdf', 'I');
$url=FCPATH.'/doccotizacion/';
//$url=$_SERVER['DOCUMENT_ROOT'].'/hulesyg/facturaspdf/';
$pdf->Output($url.'cotizacion_'.$idCotizacion.'.pdf', 'FI');

?>