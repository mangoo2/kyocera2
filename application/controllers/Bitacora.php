<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Bitacora extends CI_Controller {
    public function __construct()     {
        parent::__construct();
        //$this->load->model('Clientes_model', 'model');
        $this->load->model('ModeloCatalogos');
        $this->load->helper('url');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->submenu=45;
        if($this->session->userdata('logeado')==true){
            $this->perfilid =$this->session->userdata('perfilid');
            $this->idpersonal = $this->session->userdata('idpersonal');
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,$this->submenu);// 45 es el id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
        }else{
            redirect('login'); 
        }
    }

    // Listado de series
    function index(){
        $data['MenusubId']=$this->submenu;
        $this->load->view('header');
        $this->load->view('main');
        $this->load->view('bitacora/listado');
        $this->load->view('footerl');
        $this->load->view('bitacora/listadojs');
    }
    public function getdata() {
        $params = $this->input->post();
        $bitacora = $this->Modelobitacoras->Getbitacora($params);
        $totalRecords=$this->Modelobitacoras->Totalbitacora(); 

        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totalRecords),  
            "recordsFiltered" => intval($totalRecords),
            "data"            => $bitacora->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    public function getinfopruebas() {
        $tiempo_vida_minutos = session_cache_expire();
        echo "El tiempo de vida de la sesión es de $tiempo_vida_minutos minutos.<br>";
        echo $_SESSION['sess_expiration'].'<br>';
        $cookie_name = $this->config->item('sess_cookie_name');

        if (isset($_SESSION)) {
            // Verificar si la sesión contiene la fecha de vencimiento
            if (isset($_SESSION['session_expire'])) {
                // Obtener la fecha de vencimiento de la sesión
                $session_expire = $_SESSION['session_expire'];

                // Convertir la fecha de vencimiento a un formato legible
                $expiration_date = date('Y-m-d H:i:s', $session_expire);

                // Imprimir la fecha de vencimiento de la sesión
                echo "La sesión expirará el: $expiration_date";
            } else {
                echo "La sesión no contiene información sobre la fecha de vencimiento.";
            }
        } else {
            echo "No hay sesión activa.";
        }
    }
}
?>
