<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require 'vendor/autoload.php';//php 7.1 como minimo
use PhpOffice\PhpSpreadsheet\IOFactory;

class Configuracionrentas_ext extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->model('ModeloCatalogos');
        $this->load->model('Rentas_model');
        $this->load->model('ModeloAsignacion');
        $this->load->model('ModeloGeneral');
        $this->load->model('Modelofacturas');
        $this->load->model('Configuraciones_model');
        $this->load->model('ModeloConsultas');
        $this->load->helper('url');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d');
        $this->fechahoyc = date('Y-m-d H:i:s');
        $this->fechahoyL = date('Y-m-d_H_i_s');

        $this->version = date('YmdHi');
        
        $this->idpersonal = $this->session->userdata('idpersonal');
        $fecha = new DateTime();
        $fecha->modify('first day of this month');
        $this->fechainicio = $fecha->format('Y-m-d');
        $fecha2 = new DateTime();
        $fecha2->modify('last day of this month');
        $this->fechafin = $fecha2->format('Y-m-d');
        if($this->session->userdata('logeado')==true){
            $this->perfilid =$this->session->userdata('perfilid');
            $this->idpersonal = $this->session->userdata('idpersonal');
        }else{
            redirect('login');
        }
        $this->trunquearredondear=0;//0 redondeo 1 trunquear (dos dijitos)

    }

    //no subir todo el proyecto, si hay cambios solo subir la linea modificada

    function index(){
        
    }
    function listfacturas(){
        //copia de Configuracionrentas/listfacturas para ya no mover ese controlador
      $idcontrato = $this->input->post('idcontrato');
      $tipo = $this->input->post('tipo');
      $datoscli = $this->ModeloConsultas->get_datosclientecontrato($idcontrato);
      foreach ($datoscli->result() as $itemc) {
          $credito=$itemc->credito;
      }
      $results = $this->ModeloCatalogos->viewprefacturaslis($idcontrato,$tipo);
      $html='<table id="tablefacturaslis">
              <thead>
                <tr>
                  <th></th>
                  <th>Periodo</th>
                  <th>Fecha periodo</th>
                  <th>Folio</th>
                  <th>Cliente</th>
                  <th>RFC</th>
                  <th>Monto</th>
                  <th>Estatus</th>
                  <th>Fecha</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>';
        foreach ($results->result() as $item) {
          if($item->Estado==2){
            $st='<b>ST</b>';
          }else{
            $st='';
          }
          if ($item->statuspago==0) {
           $statuspagoc='';
           if($item->fechatimbre!='0000-00-00 00:00:00'){
            $fechatimbrecre = strtotime ( '+'.$credito.' day', strtotime ( $item->fechatimbre) ) ;
            $fechatimbrecre = date ( 'Y-m-d' , $fechatimbrecre );
            if($fechatimbrecre<$this->fechahoy){
                $statuspagoc='red';
            }

           }
          }else{
            $statuspagoc='green';
          }
          $html.='<tr>
                    <td>'.$item->FacturasId.'</td>
                    <td>'.$item->prefacturaId.'</td>
                    <td>'.$item->periodo_inicial.' '.$item->periodo_final.'</td>
                    <td>'.$st.$item->Folio.'</td>
                    <td>'.$item->Nombre.'</td>
                    <td>'.$item->Rfc.'</td>
                    <td>'.number_format($item->total, 2, '.', ' ').'</td>';
                    if($item->Estado==2){
                      $html.= "<td>Sin Timbrar</td>";
                    }elseif($item->Estado==1){
                      $html.= "<td>Timbrada</td>";
                    }elseif($item->Estado==0){
                      $html.= "<td>Cancelada</td>";
                    }
                    $html.='
                    <td>'.$item->fechatimbre.'</td>
                    <td>';
                if($item->Estado==1 or $item->Estado==0){
                $html.='<a 
                        class="b-btn b-btn-primary tooltipped btn_rcp_fac" 
                        href="'.base_url().'Configuracionrentas/generarfacturas/'.$item->FacturasId.'/1" 
                        target="_blank" data-position="top" data-delay="50" data-tooltip="Factura">
                        <i class="fas fa-file-alt fa-2x"></i></a>
                      <a 
                        class="b-btn b-btn-primary tooltipped btn_rcp_xml" 
                        href="'.base_url().$item->rutaXml.'" 
                        target="_blank" data-position="top" data-delay="50" data-tooltip="XML">
                        <i class="far fa-file-code fa-2x"></i></a>
                      <a 
                        class="b-btn b-btn-primary tooltipped btn_rcp_facc" 
                        href="'.base_url().'Configuracionrentas/generarfacturas/'.$item->FacturasId.'/3" 
                        target="_blank" data-position="top" data-delay="50" data-tooltip="Completo"><i class="fas fa-file-pdf fa-2x"></i></a>
                      <a 
                        class="b-btn b-btn-primary tooltipped btn_rcp_facd" 
                        href="'.base_url().'Reportes/rentadetalles/'.$item->prefacturaId.'/'.$idcontrato.'" 
                        target="_blank" data-position="top" data-delay="50" data-tooltip="Detalles"><i class="material-icons">assignment</i></a>';
                    }
                      if($item->Estado==1){
                      $html.='
                        <button type="button"
                          class="b-btn b-btn-primary tooltipped btn_rcp_faccan" style="background-color: red; text-color: white"
                          onclick="cancelar('.$item->FacturasId.','.$idcontrato.')" data-position="top" data-delay="50" data-tooltip="Cancelar" id="cancelaF"><i class="fas fa-ban fa-2x"></i></button> ';
                      }else{
                         $html.='<button type="button" class="b-btn b-btn-primary tooltipped btn_rcp_facref" 
                          onclick="refacturar('.$item->FacturasId.','.$idcontrato.')" data-position="top" data-delay="50" data-tooltip="Refacturar" id="refacturar"> <i class="material-icons">restore_page</i> </button> ';
                      }
                      if ($item->Estado==2) {
                        $html.='<button type="button"
                          class="b-btn b-btn-primary tooltipped btn_rcp_facref" 
                          onclick="retimbrar('.$item->FacturasId.','.$idcontrato.')";
                          data-position="top" data-delay="50" data-tooltip="Retimbrar" id="refacturar">
                          <span class="fa-stack">
                          <i class="fas fa-file fa-stack-2x"></i>
                          <i class="fas fa-sync fa-stack-1x" style="color: red;"></i>
                          </span>
                        </button> ';
                      }

                        if ($item->Estado==2) {
                          $displayp='style="display:none"';
                        }else{
                          $displayp='';
                        }


                        $html.='<button type="button"
                          class="b-btn b-btn-primary tooltipped '.$statuspagoc.' modal_pagos verificar_'.$item->facId.'" 
                          onclick="facturapago('.$item->facId.','.$item->total.')";
                          data-position="top" data-delay="50" data-tooltip="Pagos" '.$displayp.' data-idpre="'.$item->facId.'">
                          <i class="fas fa-file-invoice-dollar fa-2x"></i>
                        </button>';
                        if($item->Estado==1){
                          $html.=' <button type="button" class="b-btn b-btn-primary" 
                            onclick="enviarmailfac('.$item->FacturasId.')" data-position="top" data-delay="50" data-tooltip="Pagos"><i class="fas fa-mail-bulk fa-2x"></i></button>';
                        }

                    $html.='
                    </td>
                  </tr>';
        }
      $html.='</tbody>
            </table>';
      echo  $html;
    }
    function vercondicionactual(){
      $idRentac = $this->input->post('id');
      $strq = "SELECT * from rentas_condiciones WHERE renta='$idRentac' and activo=1 and fechainicial<='$this->fechahoy' ORDER BY fechainicial DESC LIMIT 1 ";
        $condiciones_g = $this->db->query($strq);

        $idcondicionextra=0;
        $altarentastipo2=0;
        $altarentasclicks_mono=0;
        $altarentasclicks_color=0;
        $altarentasprecio_c_e_mono=0;
        $contratoprecio_c_e_color=0;
        $contratorentadeposito=0;
        $altarentasmontoc=0;
        foreach ($condiciones_g->result() as $itemcg) {
            $idcondicionextra=$itemcg->id;
            $altarentastipo2=$itemcg->tipo;
            $altarentasclicks_mono=$itemcg->click_m;
            $altarentasclicks_color=$itemcg->click_c;
            $altarentasprecio_c_e_mono=$itemcg->excedente_m;
            $contratoprecio_c_e_color=$itemcg->excedente_c;
            $contratorentadeposito=$itemcg->renta_m;
            $altarentasmontoc=$itemcg->renta_c;
        }
        $condiciones_e = $this->ModeloCatalogos->getselectwheren('rentas_condiciones_detalle',array('idc'=>$idcondicionextra));
        $array = array(
                        'idcondicionextra'=>$idcondicionextra,
                        'altarentastipo2'=>$altarentastipo2,
                        'altarentasclicks_mono'=>$altarentasclicks_mono,
                        'altarentasclicks_color'=>$altarentasclicks_color,
                        'altarentasprecio_c_e_mono'=>$altarentasprecio_c_e_mono,
                        'contratoprecio_c_e_color'=>$contratoprecio_c_e_color,
                        'contratorentadeposito'=>$contratorentadeposito,
                        'altarentasmontoc'=>$altarentasmontoc,
                        'equipos'=>$condiciones_e->result()
                      );
        echo json_encode($array);
    }
    function actualizarfolioreg(){
      $params = $this->input->post();
      $rowe = $params['rowe'];
      $ser = $params['ser'];
      $comentario = $params['comentario'];
      $this->ModeloCatalogos->updateCatalogo('asignacion_series_r_equipos',array('comentario'=>$comentario),array('id_equipo'=>$rowe,'serieId'=>$ser));
    }
    function listrefacciones($idcontrato,$fechainicio='',$fechafin=''){
      header("Pragma: public");
      header("Expires: 0");
      $filename = "refacciones.xls";
      header("Content-type: application/x-msdownload");
      header("Content-Disposition: attachment; filename=$filename");
      header("Pragma: no-cache");
      header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
      $result=$this->Configuraciones_model->refaccionescontrato($idcontrato,$fechainicio,$fechafin);
      $html='<table id="tabla_estadorefaccion" border="1"><thead>
      <tr><th></th><th>Equipo</th><th>Serie Equipo</th><th>Cantidad</th><th>Modelo</th><th>Serie</th><th></th></tr></thead><tbody>';
      foreach ($result->result() as $item) {
        if($item->asignacionId_v>0){
          $btn_estado='<a class="waves-effect cyan btn-bmz" onclick="documento('.$item->asignacionId_v.',4,0)"><i class="fas fa-file-alt" style="font-size: 20px;"></i></a>';
        }else{
          $btn_estado='';
        }
        $html.='<tr><td>'.$item->fecha.'</td><td>'.$item->modeloe.'</td><td>'.$item->seriee.'</td><td>'.$item->piezas.'</td><td>'.$item->modelo.'</td><td>'.$item->serie.'</td><td>'.$btn_estado.'</td></tr>';
      }
      $html.='</tbody></table>';
      echo $html;
    }
    function editcontadores(){
      $data = $this->input->post();
      $equiposrow=$data['equipos'];
      unset($data['equipos']);
      //$prefId = $this->ModeloCatalogos->Insert('alta_rentas_prefactura',$data);
      $subtotal = $data['subtotal'];
      $subtotalcolor = $data['subtotalcolor'];
      $excedente = $data['excedente'];
      $excedentecolor = $data['excedentecolor'];
      $fechainicial = $data['fechainicial'];
      $fechafin = $data['fechafin'];
      $total = $data['total'];
      $prefId=$data['id_polizar'];
      /*
      $datap  = array(
                  'subtotal'=> $subtotal,
                  'subtotalcolor'=> $subtotalcolor,
                  'excedente'=> $excedente,
                  'excedentecolor'=> $excedentecolor,
                  'total'=> $total,
                  'factura_renta'=>0,
                  'factura_excedente'=>0,
                  'periodo_inicial'=>$fechainicial,
                  'periodo_final'=>$fechafin
                );
      */
      $datap  = array(
                  'subtotal'=> $subtotal,
                  'subtotalcolor'=> $subtotalcolor,
                  'excedente'=> $excedente,
                  'excedentecolor'=> $excedentecolor,
                  'total'=> $total,
                  'periodo_inicial'=>$fechainicial,
                  'periodo_final'=>$fechafin
                );

      $this->ModeloCatalogos->updateCatalogo('alta_rentas_prefactura',$datap,array('prefId'=>$data['id_polizar']));
      $this->ModeloCatalogos->Insert('bitacora',array('contenido'=>'Se realizo una edicion de contadores del periodo'.$data['id_polizar'],'nombretabla'=>'alta_rentas_prefactura','idtable'=>$data['id_polizar'],'tipo'=>'edicion','personalId'=>$this->idpersonal));
      $equiposrow = json_decode($equiposrow);
      for ($i=0;$i<count($equiposrow);$i++) { 
        $prefdId=$equiposrow[$i]->prefdId;
        $dataeq['c_c_i']=$equiposrow[$i]->c_c_i;
        $dataeq['c_c_f']=$equiposrow[$i]->c_c_f;
        $dataeq['e_c_i']=$equiposrow[$i]->e_c_i;
        $dataeq['e_c_f']=$equiposrow[$i]->e_c_f;
        $dataeq['descuento']=$equiposrow[$i]->descuento;
        $dataeq['descuentoe']=$equiposrow[$i]->descuentoe;

        $dataeq['produccion']=$equiposrow[$i]->produccion;
        $dataeq['toner_consumido']=$equiposrow[$i]->toner_consumido;
        $dataeq['produccion_total']=$equiposrow[$i]->produccion_total;
      
        $dataeq['excedentes']=$equiposrow[$i]->excedentes;
        $dataeq['costoexcedente']=$equiposrow[$i]->costoexcedente;
        $dataeq['totalexcedente']=$equiposrow[$i]->totalexcedente;

        //$dataeq['statusfacturar']=0;
        //$dataeq['statusfacturae']=0;
        
        $this->ModeloCatalogos->updateCatalogo('alta_rentas_prefactura_d',$dataeq,array('prefdId'=>$prefdId));
      }

      $facturado=0;
      //===================================================
      $result_pref=$this->ModeloCatalogos->getselectwheren('alta_rentas_prefactura',array('prefId'=>$prefId));
      $result_prefd=$this->ModeloCatalogos->getselectwheren('alta_rentas_prefactura_d',array('prefId'=>$prefId));
      foreach ($result_pref->result() as $item) {
        if($item->factura_renta>0){
          $facturado=$prefId;
        }
        if($item->factura_excedente>0){
          $facturado=$prefId;
        }
      }
      foreach ($result_prefd->result() as $item) {
        if($item->statusfacturar>0){
          $facturado=$prefId;
        }
        if($item->statusfacturae>0){
          $facturado=$prefId;
        }
      }
      echo $facturado;
    }
    function liberarperiodofac(){
      $params = $this->input->post();
      $prefId = $params['prefId'];
      $this->ModeloCatalogos->updateCatalogo('alta_rentas_prefactura',array('factura_renta'=>0,'factura_excedente'=>0),array('prefId'=>$prefId));
      $this->ModeloCatalogos->updateCatalogo('alta_rentas_prefactura_d',array('statusfacturar'=>0,'statusfacturae'=>0),array('prefId'=>$prefId));
    }
    function verificarnotascredito(){
        $uuid=$this->input->post('uuid');

        $resultado=$this->ModeloCatalogos->getselectwheren('f_facturas',array('f_r_uuid'=>$uuid,'f_r_tipo'=>'01','Estado'=>1));
        $html='';
        foreach ($resultado->result() as $item) {
          $html.='<tr><td>'.$item->serie.''.$item->Folio.'</td><td>'.$item->fechatimbre.'</td><td>'.$item->total.'</td><td>
                            <a class="b-btn b-btn-primary tooltipped" href="'.base_url().$item->rutaXml.'" target="_blank" data-position="top" data-delay="50" data-tooltip="XML" download="" data-tooltip-id="54ee1647-5011-f5dd-45f2-7aa729169609"><i class="far fa-file-code fa-2x"></i></a>
                            <a class="b-btn b-btn-primary tooltipped" href="'.base_url().'Configuracionrentas/generarfacturas/'.$item->FacturasId.'/1" target="_blank" data-position="top" data-delay="50" data-tooltip="XML" data-tooltip-id="54ee1647-5011-f5dd-45f2-7aa729169609">
                                <i class="fas fa-file-pdf fa-2x"></i>
                                </a>
                        </td></tr>';
        }
        echo $html;
        
    }
    function cargacontadores(){
        $file_name=$this->fechahoyL;
        $file_o=$_FILES['filecontadores']['name'];

        $configUpload['upload_path'] = FCPATH.'fileExcel/';
        $configUpload['allowed_types'] = '*';
        $configUpload['max_size'] = '5000';
        $configUpload['file_name'] = 'cont_'.$file_name;
        $this->load->library('upload', $configUpload);
        if (!$this->upload->do_upload('filecontadores')) {
            $output=array('error'=>$this->upload->display_errors());
        }else{
            $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
            $archivocer = $upload_data['file_name']; //uploded file name
            $this->procesarfilecont($archivocer,$file_o);

            $output = [];
            ///echo 'correcto';
        }

        echo json_encode($output);
    }
    function procesarfilecont($filecsv,$file_o){
        $fechahoyc=$this->fechahoyc;

        $file=FCPATH.'fileExcel/'.$filecsv;
        $documento = IOFactory::load($file);

        $totalHojas = $documento->getSheetCount();

        //for($indiceHoja=0; $indiceHoja<$totalHojas; $indiceHoja++){
          //  $hojaActual = $documento->getsheet($indiceHoja);
              $hojaActual = $documento->getsheet(0);
              $numeroFilas = $hojaActual->getHighestDataRow();
              $letra = $hojaActual->getHighestColumn();
              //$html='<table border="1">';
              $dataeqarrayinsert=array();
              for($indiceFila = 2; $indiceFila<=$numeroFilas; $indiceFila++){
                $valor1=$hojaActual->GetCellByColumnAndRow('1',$indiceFila);
                $valor2=$hojaActual->GetCellByColumnAndRow('2',$indiceFila);
                $valor3=$hojaActual->GetCellByColumnAndRow('3',$indiceFila);
                $valor4=$hojaActual->GetCellByColumnAndRow('4',$indiceFila);
                $valor5=$hojaActual->GetCellByColumnAndRow('5',$indiceFila);
                $valor6=$hojaActual->GetCellByColumnAndRow('6',$indiceFila);
                $valor7=$hojaActual->GetCellByColumnAndRow('7',$indiceFila);
                /*
                $valor5=$hojaActual->getCell('E'.$indiceFila)->getFormattedValue();
                if(strlen($valor5)<=5){
                    $valor5=$valor5.':00';
                }
                $valor6=$hojaActual->getCell('F'.$indiceFila)->getFormattedValue();
                if(strlen($valor6)<=5){
                    $valor6=$valor6.':00';
                }


                $valor7=$hojaActual->getCell('G'.$indiceFila)->getValue();
                $valor7 = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToTimestamp($valor7);
                $valor7=date('Y-m-d',strtotime("+1 day",$valor7));
                */
                $dataeqarrayinsert[]=array(
                                          'modelo'=>$valor1,
                                          'serie'=>$valor2,
                                          'ubicacion'=>$valor3,
                                          'fecha_obtencion'=>$valor4,
                                          'cont_mono'=>$valor5,
                                          'cont_color'=>$valor6,
                                          'cont_escaner'=>$valor7,
                                          'reg'=>$fechahoyc,
                                          'file_name'=>$file_o
                                        );


                //$html.='<tr><td>'.$valor1.'</td><td>'.$valor2.'</td><td>'.$valor3.'</td><td>'.$valor4.'</td><td>'.$valor5.'</td><td>'.$valor6.'</td><td>'.$valor7.'</td></tr>';
              }
              if(count($dataeqarrayinsert)>0){
                $this->ModeloCatalogos->insert_batch('carga_contadores',$dataeqarrayinsert);
              }
              //$html.='</table>';
              //echo $html;
              //log_message('error',$html);
        //}
    }
    function contreg(){
      $result=$this->ModeloCatalogos->getregistrocontadores();
      $html='';
      $row=0;
      foreach ($result->result() as $item) {
        $html.='<tr><td>'.$item->reg.'</td><td>'.$item->file_name.'</td><td>
                  <a class="btn-bmz waves-effect waves-light gradient-45deg-green-teal gradient-shadow import_cont import_cont_'.$row.'" data-reg="'.$item->reg.'" onclick="importarcontadores('.$row.')">Exportar</a> 
                  <a class="btn-bmz waves-effect waves-light gradient-45deg-green-teal gradient-shadow import_cont_dell import_cont"  onclick="importarcontadoresall('.$row.')">Detalle</a>
                  <a class="waves-effect red btn-bmz import_cont_delete"  onclick="deletecontadoresall('.$row.')"><i class="fas fa-trash-alt"></i></a></td></tr>';
        $row++;
      }
      echo $html;
    }
    function contregall(){
      $reg = $this->input->post('reg');
      $idcontrato = $this->input->post('idcontrato');
      //$result=$this->ModeloCatalogos->getselectwheren('carga_contadores',array('reg'=>$reg,'activo'=>1));
      $result=$this->ModeloCatalogos->get_carga_contadores($reg,$idcontrato);
      foreach ($result->result() as $item) {
        $id_row = $item->id;
        $idcliente_cc = $item->idcliente_cc;
        $idCliente = $item->idCliente;
        if($item->idcliente_cc>0){

        }else{
          $this->ModeloCatalogos->updateCatalogo('carga_contadores',array('idcliente_cc'=>$idCliente,'serieId_cc'=>$item->serieId),array('id'=>$id_row));
        }
      }
      echo json_encode($result->result());
    }
    function deletecontadoresall(){
      $reg = $this->input->post('reg');
      $this->ModeloCatalogos->updateCatalogo('carga_contadores',array('activo'=>0),array('reg'=>$reg));
    }
    function addcentrocostos(){
      $params = $this->input->post();
      $des=$params['ccdes'];
      $idcontrado = $params['idcontrado'];
      $this->ModeloCatalogos->Insert('centro_costos',array('idcontrato'=>$idcontrado,'descripcion'=>$des));
    }
    function view_centroscostos(){
      $params = $this->input->post();
      $idcontrado = $params['idcontrado'];
      $result=$this->ModeloCatalogos->getselectwheren('centro_costos',array('idcontrato'=>$idcontrado,'activo'=>1));
      $resultcceq=$this->ModeloCatalogos->getselectwheren('centro_costos_equipos',array('idcontrato'=>$idcontrado,'activo'=>1));
      /*
      $html='';
      foreach ($result->result() as $item) {
        $html.='<tr><td>'.$item->descripcion.'</td><td><a onclick="deletecc('.$item->id.')" class="btn-bmz red"><i class="fas fa-trash-alt"></i></a></td></tr>';
      }
      */
      //echo $html;
      $array=array(
                    'css'=>$result->result(),
                    'css_quipo'=>$resultcceq->result()
                  );
      echo json_encode($array);
    }
    function centroscostos_delete(){
      $params = $this->input->post();
      $id = $params['id'];
      $this->ModeloCatalogos->updateCatalogo('centro_costos',array('activo'=>0),array('id'=>$id));
    }
    function addcentrocostosequiposinsertupdate(){
      $params = $this->input->post();
      $idcontrato = $params['idcontrato'];
      $idcentroc = $params['idcentroc'];
      $rowequipo = $params['rowequipo'];
      $modeloid = $params['modeloid'];
      $serieid = $params['serieid'];
      $result=$this->ModeloCatalogos->getselectwheren('centro_costos_equipos',array('idcontrato'=>$idcontrato,'rowequipo'=>$rowequipo,'modeloid'=>$modeloid,'serieid'=>$serieid,'activo'=>1));
      if($result->num_rows()>0){
        $row=$result->row();
        $this->ModeloCatalogos->updateCatalogo('centro_costos_equipos',array('idcentroc'=>$idcentroc),array('id'=>$row->id));
      }else{
        $this->ModeloCatalogos->Insert('centro_costos_equipos',array('idcontrato'=>$idcontrato,'rowequipo'=>$rowequipo,'modeloid'=>$modeloid,'serieid'=>$serieid,'idcentroc'=>$idcentroc));
      }

    }
    function iucentrocostosfac(){
      $params = $this->input->post();
      $idcentroc = $params['idcentroc'];
      $prefid = $params['prefid'];
      $montoc = $params['montoc'];
      $result=$this->ModeloCatalogos->getselectwheren('centro_costos_prefa',array('idcentroc'=>$idcentroc,'prefId'=>$prefid,'activo'=>1));
      if($result->num_rows()>0){
        $row=$result->row();
        $this->ModeloCatalogos->updateCatalogo('centro_costos_prefa',array('monto'=>$montoc),array('id'=>$row->id));
      }else{
        $this->ModeloCatalogos->Insert('centro_costos_prefa',array('idcentroc'=>$idcentroc,'prefId'=>$prefid,'monto'=>$montoc));
      }
    }
    function view_ccs_montos(){
      $params = $this->input->post();
      $prefid = $params['prefid'];
      $result=$this->ModeloCatalogos->getselectwheren('centro_costos_prefa',array('prefId'=>$prefid,'activo'=>1));
      
      $array=array(
                    'ccsmontos'=>$result->result()
                  );
      echo json_encode($array);
    }
    function agregarequiposccs(){
      $params = $this->input->post();
      $idccs = $params['idccs'];//id del centro de costo
      $idpre = $params['idpre'];//id de la prefactura
      $idcontrato=$params['idcontrato'];//id del contrato
      $fechaie = $params['fechai'];
      $fechafe = $params['fechaf'];
      $tipo = $params['tipo'];
      $costoclick_m=0;
      $costoclick_c=0;
      $rowcobrar=0;
      $html='';
      $precio_excedente_mono=0;
      $precio_excedente_color=0;
      $clicks_mono=0;
      $clicks_color=0;
      //=============================================
        $result_ar=$this->ModeloCatalogos->getselectwheren('alta_rentas',array('idcontrato'=>$idcontrato,'activo'=>1));
        foreach ($result_ar->result() as $item) {
          if($item->monto>0){
            $costoclick_m=$item->monto/$item->clicks_mono;
          }
          if($item->montoc>0){
            $costoclick_c=$item->montoc/$item->clicks_color;
          }
          if($item->precio_c_e_mono>0){
            $precio_excedente_mono=$item->precio_c_e_mono;
          }
          if($item->precio_c_e_color>0){
            $precio_excedente_color=$item->precio_c_e_mono;
          }
          //$clicks_mono=$item->clicks_mono;
          //$clicks_color=$item->clicks_color;
        }
        //======================================================================
        $result_arp=$this->ModeloCatalogos->getselectwheren('alta_rentas_prefactura',array('prefId'=>$idpre,'activo'=>1));
        $idcondicionextra=0;
        foreach ($result_arp->result() as $item) {
          $idcondicionextra=$item->idcondicionextra;
        }
        if($idcondicionextra>0){
          $condiciones = $this->ModeloCatalogos->getselectwheren('rentas_condiciones',array('id'=>$idcondicionextra));
          foreach ($condiciones->result() as $itemrc) {
            if($itemrc->renta_m>0){
              $costoclick_m=$itemrc->renta_m/$itemrc->click_m;
            }
            if($itemrc->renta_c>0){
              $costoclick_c=$itemrc->renta_c/$itemrc->click_c;
            }
            if($itemrc->excedente_m>0){
              $precio_excedente_mono=$itemrc->excedente_m;
            }
            if($itemrc->excedente_c>0){
              $precio_excedente_color=$itemrc->excedente_c;
            }
            //$clicks_mono=$itemrc->click_m;
            //$clicks_color=$itemrc->click_c;
          }
        }
      //=============================================
        $option_unidad_renta = $this->ModeloCatalogos->obtenerunidadservicio(1,1);
        $option_concepto_renta = $this->ModeloCatalogos->obtenerunidadservicio(1,2);
      //=============================================
        $result_cc=$this->ModeloCatalogos->getselectwheren('centro_costos',array('id'=>$idccs));
        $clicks_mono=0;
        $clicks_color=0;
        $mono_renta_cc=0;
        $color_renta_cc=0;
        foreach ($result_cc->result() as $item) {
          $descripcion=$item->descripcion;
          $clicks_mono=$item->clicks;
          $clicks_color=$item->clicks_c;
          if($clicks_mono>0){
            $mono_renta_cc=$clicks_mono*$costoclick_m;
          }
          if($clicks_color>0){
            $color_renta_cc=$clicks_color*$costoclick_c;
          }
        }
      //=============================================
        if($tipo==0){//0 detallado 1 monto global
          if($clicks_mono>0){
            $result_cceq=$this->ModeloCatalogos->result_cceq($idccs,$idpre,1);
            $num_equipos=$result_cceq->num_rows();
            if($num_equipos>0){
              $monto_x_equipo=round($mono_renta_cc/$num_equipos,3);
              /* 
                echo round( 1.55, 1, PHP_ROUND_HALF_UP);   //  1.6
                echo round( 1.54, 1, PHP_ROUND_HALF_UP);   //  1.5
              */
              foreach ($result_cceq->result() as $item) {
                $html.='<tr class="datos_principal tr_8">
                        <td>
                          <input id="selectedequipo" name="selectedequipo" type="hidden" value="1" readonly>
                          <input id="selectedprefactura" name="selectedprefactura" type="hidden" value="0" readonly>
                          <input id="facturarenta" name="facturarenta" type="hidden" value="1" readonly>
                          <input id="facturaexcedente" name="facturaexcedente" type="hidden" value="1" readonly>
                          <input id="cantidad" name="cantidad" type="text" value="1" class="excedente info cant_'.$rowcobrar.'" data-cantidad="'.$monto_x_equipo.'" data-precioexc="'.$monto_x_equipo.'" data-preid="'.$rowcobrar.'" readonly></td>
                        <td>
                          <div style="width:250px">
                            <select id="unidadsat" class="unidadsat browser-default">
                            '.$option_unidad_renta.'
                            </select>
                          <div>
                        </td>
                        <td>
                            <div style="width:250px">
                              <select id="conseptosat" class="conseptosat browser-default">
                              '.$option_concepto_renta.'
                              </select>
                            <div>
                        </td>
                        <td><input id="descripcion" name="descripcion" type="text" value="Renta '.$item->modelo.' '.$item->serie.' del '.date("d/m/Y", strtotime($fechaie)).' al '.date("d/m/Y", strtotime($fechafe)).'"></td>
                        <td><input id="precio" name="precio" class="precio  excedente precio_'.$rowcobrar.'" type="text" value="'.$monto_x_equipo.'" readonly></td>
                        <td>
                            <input type="number"
                              name="descuento"
                              id="descuento"
                              class="form-control-bmz cdescuento cdescuento_'.$rowcobrar.'"
                              onclick="activardescuento('.$rowcobrar.')"
                              value="0" readonly>
                        </td>
                        <td><a class="btn-bmz red deletetrrow"><i class="fas fa-trash-alt"></i></a></td>
                      </tr>';
                      $rowcobrar++;
              }
            }
          }else{
            $result_cceq=$this->ModeloCatalogos->result_cceq($idccs,$idpre,1);
            $num_equipos=$result_cceq->num_rows();
            //log_message('error','$num_equipos:'.$num_equipos);
            if($num_equipos>0){
              //$monto_x_equipo=round($mono_renta_cc/$num_equipos,3);
              /* 
                echo round( 1.55, 1, PHP_ROUND_HALF_UP);   //  1.6
                echo round( 1.54, 1, PHP_ROUND_HALF_UP);   //  1.5
              */
              foreach ($result_cceq->result() as $item) {
                $monto_x_equipo=round($item->produccion_total*$precio_excedente_mono,3);
                if($monto_x_equipo>0 and $clicks_mono==0){
                $html.='<tr class="datos_principal tr_8">
                        <td>
                          <input id="selectedequipo" name="selectedequipo" type="hidden" value="1" readonly>
                          <input id="selectedprefactura" name="selectedprefactura" type="hidden" value="0" readonly>
                          <input id="facturarenta" name="facturarenta" type="hidden" value="1" readonly>
                          <input id="facturaexcedente" name="facturaexcedente" type="hidden" value="1" readonly>
                          <input id="cantidad" name="cantidad" type="text" value="1" class="excedente info cant_'.$rowcobrar.'" data-cantidad="'.$monto_x_equipo.'" data-precioexc="'.$monto_x_equipo.'" data-preid="'.$rowcobrar.'" readonly></td>
                        <td>
                          <div style="width:250px">
                            <select id="unidadsat" class="unidadsat browser-default">
                            '.$option_unidad_renta.'
                            </select>
                          <div>
                        </td>
                        <td>
                            <div style="width:250px">
                              <select id="conseptosat" class="conseptosat browser-default">
                              '.$option_concepto_renta.'
                              </select>
                            <div>
                        </td>
                        <td><input id="descripcion" name="descripcion" type="text" value="'.$item->modelo.' '.$item->serie.' del '.date("d/m/Y", strtotime($fechaie)).' al '.date("d/m/Y", strtotime($fechafe)).'"></td>
                        <td><input id="precio" name="precio" class="precio  excedente precio_'.$rowcobrar.'" type="text" value="'.$monto_x_equipo.'" readonly data-prod='.$item->produccion_total.' data-costoe='.$precio_excedente_mono.'></td>
                        <td>
                            <input type="number"
                              name="descuento"
                              id="descuento"
                              class="form-control-bmz cdescuento cdescuento_'.$rowcobrar.'"
                              onclick="activardescuento('.$rowcobrar.')"
                              value="0" readonly>
                        </td>
                        <td><a class="btn-bmz red deletetrrow"><i class="fas fa-trash-alt"></i></a></td>
                      </tr>';
                      $rowcobrar++;
                  }
              }
            }
          }

          if($clicks_color>0){
            $result_cceq=$this->ModeloCatalogos->result_cceq($idccs,$idpre,2);
            $num_equipos=$result_cceq->num_rows();
            if($num_equipos>0){
              $monto_x_equipo=round($color_renta_cc/$num_equipos,3);
              /* 
                echo round( 1.55, 1, PHP_ROUND_HALF_UP);   //  1.6
                echo round( 1.54, 1, PHP_ROUND_HALF_UP);   //  1.5
              */
              foreach ($result_cceq->result() as $item) {
                $html.='<tr class="datos_principal tr_8">
                        <td>
                          <input id="selectedequipo" name="selectedequipo" type="hidden" value="1" readonly>
                          <input id="selectedprefactura" name="selectedprefactura" type="hidden" value="0" readonly>
                          <input id="facturarenta" name="facturarenta" type="hidden" value="1" readonly>
                          <input id="facturaexcedente" name="facturaexcedente" type="hidden" value="1" readonly>
                          <input id="cantidad" name="cantidad" type="text" value="1" class="excedente info cant_'.$rowcobrar.'" data-cantidad="'.$monto_x_equipo.'" data-precioexc="'.$monto_x_equipo.'" data-preid="'.$rowcobrar.'" readonly></td>
                        <td>
                          <div style="width:250px">
                            <select id="unidadsat" class="unidadsat browser-default">
                            '.$option_unidad_renta.'
                            </select>
                          <div>
                        </td>
                        <td>
                            <div style="width:250px">
                              <select id="conseptosat" class="conseptosat browser-default">
                              '.$option_concepto_renta.'
                              </select>
                            <div>
                        </td>
                        <td><input id="descripcion" name="descripcion" type="text" value="Renta '.$item->modelo.' '.$item->serie.' del '.date("d/m/Y", strtotime($fechaie)).' al '.date("d/m/Y", strtotime($fechafe)).'"></td>
                        <td><input id="precio" name="precio" class="precio  excedente precio_'.$rowcobrar.'" type="text" value="'.$monto_x_equipo.'" readonly></td>
                        <td>
                            <input type="number"
                              name="descuento"
                              id="descuento"
                              class="form-control-bmz cdescuento cdescuento_'.$rowcobrar.'"
                              onclick="activardescuento('.$rowcobrar.')"
                              value="0" readonly>
                        </td>
                        <td><a class="btn-bmz red deletetrrow"><i class="fas fa-trash-alt"></i></a></td>
                      </tr>';
                      $rowcobrar++;
              }
            }
          }else{
            $result_cceq=$this->ModeloCatalogos->result_cceq($idccs,$idpre,2);
            $num_equipos=$result_cceq->num_rows();
            if($num_equipos>0 and $clicks_color==0){
              //$monto_x_equipo=round($color_renta_cc/$num_equipos,3);
              /* 
                echo round( 1.55, 1, PHP_ROUND_HALF_UP);   //  1.6
                echo round( 1.54, 1, PHP_ROUND_HALF_UP);   //  1.5
              */
              foreach ($result_cceq->result() as $item) {
                $monto_x_equipo=round($item->produccion_total*$precio_excedente_color,3);
                if($monto_x_equipo>0){
                $html.='<tr class="datos_principal tr_8">
                        <td>
                          <input id="selectedequipo" name="selectedequipo" type="hidden" value="1" readonly>
                          <input id="selectedprefactura" name="selectedprefactura" type="hidden" value="0" readonly>
                          <input id="facturarenta" name="facturarenta" type="hidden" value="1" readonly>
                          <input id="facturaexcedente" name="facturaexcedente" type="hidden" value="1" readonly>
                          <input id="cantidad" name="cantidad" type="text" value="1" class="excedente info cant_'.$rowcobrar.'" data-cantidad="'.$monto_x_equipo.'" data-precioexc="'.$monto_x_equipo.'" data-preid="'.$rowcobrar.'" readonly></td>
                        <td>
                          <div style="width:250px">
                            <select id="unidadsat" class="unidadsat browser-default">
                            '.$option_unidad_renta.'
                            </select>
                          <div>
                        </td>
                        <td>
                            <div style="width:250px">
                              <select id="conseptosat" class="conseptosat browser-default">
                              '.$option_concepto_renta.'
                              </select>
                            <div>
                        </td>
                        <td><input id="descripcion" name="descripcion" type="text" value="'.$item->modelo.' '.$item->serie.' del '.date("d/m/Y", strtotime($fechaie)).' al '.date("d/m/Y", strtotime($fechafe)).'"></td>
                        <td><input id="precio" name="precio" class="precio  excedente precio_'.$rowcobrar.'" type="text" value="'.$monto_x_equipo.'" readonly data-prod='.$item->produccion_total.' data-costoe='.$precio_excedente_color.' ></td>
                        <td>
                            <input type="number"
                              name="descuento"
                              id="descuento"
                              class="form-control-bmz cdescuento cdescuento_'.$rowcobrar.'"
                              onclick="activardescuento('.$rowcobrar.')"
                              value="0" readonly>
                        </td>
                        <td><a class="btn-bmz red deletetrrow"><i class="fas fa-trash-alt"></i></a></td>
                      </tr>';
                      $rowcobrar++;
                }
              }
            }
          }
        }
        if($tipo==1){//0 detallado 1 monto global
          if($mono_renta_cc>0){
            $html.='<tr class="datos_principal tr_8">
                        <td>
                          <input id="selectedequipo" name="selectedequipo" type="hidden" value="1" readonly>
                          <input id="selectedprefactura" name="selectedprefactura" type="hidden" value="0" readonly>
                          <input id="facturarenta" name="facturarenta" type="hidden" value="1" readonly>
                          <input id="facturaexcedente" name="facturaexcedente" type="hidden" value="1" readonly>
                          <input id="cantidad" name="cantidad" type="text" value="1" class="excedente info cant_'.$rowcobrar.'" data-cantidad="'.$mono_renta_cc.'" data-precioexc="'.$mono_renta_cc.'" data-preid="'.$rowcobrar.'" readonly></td>
                        <td>
                          <div style="width:250px">
                            <select id="unidadsat" class="unidadsat browser-default">
                            '.$option_unidad_renta.'
                            </select>
                          <div>
                        </td>
                        <td>
                            <div style="width:250px">
                              <select id="conseptosat" class="conseptosat browser-default">
                              '.$option_concepto_renta.'
                              </select>
                            <div>
                        </td>
                        <td><input id="descripcion" name="descripcion" type="text" value="Renta '.$descripcion.' Monocromatica del '.date("d/m/Y", strtotime($fechaie)).' al '.date("d/m/Y", strtotime($fechafe)).'"></td>
                        <td><input id="precio" name="precio" class="precio  excedente precio_'.$rowcobrar.'" type="text" value="'.$mono_renta_cc.'" readonly></td>
                        <td>
                            <input type="number"
                              name="descuento"
                              id="descuento"
                              class="form-control-bmz cdescuento cdescuento_'.$rowcobrar.'"
                              onclick="activardescuento('.$rowcobrar.')"
                              value="0" readonly>
                        </td>
                        <td><a class="btn-bmz red deletetrrow"><i class="fas fa-trash-alt"></i></a></td>
                      </tr>';
                      $rowcobrar++;
          }
          if($color_renta_cc>0){
             $html.='<tr class="datos_principal tr_8">
                        <td>
                          <input id="selectedequipo" name="selectedequipo" type="hidden" value="1" readonly>
                          <input id="selectedprefactura" name="selectedprefactura" type="hidden" value="0" readonly>
                          <input id="facturarenta" name="facturarenta" type="hidden" value="1" readonly>
                          <input id="facturaexcedente" name="facturaexcedente" type="hidden" value="1" readonly>
                          <input id="cantidad" name="cantidad" type="text" value="1" class="excedente info cant_'.$rowcobrar.'" data-cantidad="'.$color_renta_cc.'" data-precioexc="'.$color_renta_cc.'" data-preid="'.$rowcobrar.'" readonly></td>
                        <td>
                          <div style="width:250px">
                            <select id="unidadsat" class="unidadsat browser-default">
                            '.$option_unidad_renta.'
                            </select>
                          <div>
                        </td>
                        <td>
                            <div style="width:250px">
                              <select id="conseptosat" class="conseptosat browser-default">
                              '.$option_concepto_renta.'
                              </select>
                            <div>
                        </td>
                        <td><input id="descripcion" name="descripcion" type="text" value="Renta '.$descripcion.' Color del '.date("d/m/Y", strtotime($fechaie)).' al '.date("d/m/Y", strtotime($fechafe)).'"></td>
                        <td><input id="precio" name="precio" class="precio  excedente precio_'.$rowcobrar.'" type="text" value="'.$color_renta_cc.'" readonly></td>
                        <td>
                            <input type="number"
                              name="descuento"
                              id="descuento"
                              class="form-control-bmz cdescuento cdescuento_'.$rowcobrar.'"
                              onclick="activardescuento('.$rowcobrar.')"
                              value="0" readonly>
                        </td>
                        <td><a class="btn-bmz red deletetrrow"><i class="fas fa-trash-alt"></i></a></td>
                      </tr>';
                      $rowcobrar++;
          }
          if($precio_excedente_mono>0 and $clicks_mono==0){
            $result_cceq=$this->ModeloCatalogos->result_cceq($idccs,$idpre,1);
            $num_equipos=$result_cceq->num_rows();
            
            $produccion_total=0;
            if($num_equipos>0){
              //$monto_x_equipo=round($mono_renta_cc/$num_equipos,3);
              /* 
                echo round( 1.55, 1, PHP_ROUND_HALF_UP);   //  1.6
                echo round( 1.54, 1, PHP_ROUND_HALF_UP);   //  1.5
              */
                
              foreach ($result_cceq->result() as $item) {
                $produccion_total=$produccion_total+$item->produccion_total;
              }
            }
            if($produccion_total>0){
              $mono_renta_cc=$produccion_total*$precio_excedente_mono;
              $html.='<tr class="datos_principal tr_8">
                        <td>
                          <input id="selectedequipo" name="selectedequipo" type="hidden" value="1" readonly>
                          <input id="selectedprefactura" name="selectedprefactura" type="hidden" value="0" readonly>
                          <input id="facturarenta" name="facturarenta" type="hidden" value="1" readonly>
                          <input id="facturaexcedente" name="facturaexcedente" type="hidden" value="1" readonly>
                          <input id="cantidad" name="cantidad" type="text" value="1" class="excedente info cant_'.$rowcobrar.'" data-cantidad="'.$mono_renta_cc.'" data-precioexc="'.$mono_renta_cc.'" data-preid="'.$rowcobrar.'" readonly></td>
                        <td>
                          <div style="width:250px">
                            <select id="unidadsat" class="unidadsat browser-default">
                            '.$option_unidad_renta.'
                            </select>
                          <div>
                        </td>
                        <td>
                            <div style="width:250px">
                              <select id="conseptosat" class="conseptosat browser-default">
                              '.$option_concepto_renta.'
                              </select>
                            <div>
                        </td>
                        <td><input id="descripcion" name="descripcion" type="text" value="'.$descripcion.' Monocromatica del '.date("d/m/Y", strtotime($fechaie)).' al '.date("d/m/Y", strtotime($fechafe)).'"></td>
                        <td><input id="precio" name="precio" class="precio  excedente precio_'.$rowcobrar.'" type="text" value="'.$mono_renta_cc.'" readonly></td>
                        <td>
                            <input type="number"
                              name="descuento"
                              id="descuento"
                              class="form-control-bmz cdescuento cdescuento_'.$rowcobrar.'"
                              onclick="activardescuento('.$rowcobrar.')"
                              value="0" readonly>
                        </td>
                        <td><a class="btn-bmz red deletetrrow"><i class="fas fa-trash-alt"></i></a></td>
                      </tr>';
                      $rowcobrar++;
            }
          }
          if($precio_excedente_color>0 and $clicks_color==0){
            $result_cceq=$this->ModeloCatalogos->result_cceq($idccs,$idpre,2);
            $num_equipos=$result_cceq->num_rows();
            $produccion_total=0;
            if($num_equipos>0){
              //$monto_x_equipo=round($color_renta_cc/$num_equipos,3);
              /* 
                echo round( 1.55, 1, PHP_ROUND_HALF_UP);   //  1.6
                echo round( 1.54, 1, PHP_ROUND_HALF_UP);   //  1.5
              */
              foreach ($result_cceq->result() as $item) {
                $produccion_total=$produccion_total+$item->produccion_total;
              }
              if($produccion_total>0){
                $color_renta_cc=$produccion_total*$precio_excedente_color;
                $html.='<tr class="datos_principal tr_8">
                        <td>
                          <input id="selectedequipo" name="selectedequipo" type="hidden" value="1" readonly>
                          <input id="selectedprefactura" name="selectedprefactura" type="hidden" value="0" readonly>
                          <input id="facturarenta" name="facturarenta" type="hidden" value="1" readonly>
                          <input id="facturaexcedente" name="facturaexcedente" type="hidden" value="1" readonly>
                          <input id="cantidad" name="cantidad" type="text" value="1" class="excedente info cant_'.$rowcobrar.'" data-cantidad="'.$color_renta_cc.'" data-precioexc="'.$color_renta_cc.'" data-preid="'.$rowcobrar.'" readonly></td>
                        <td>
                          <div style="width:250px">
                            <select id="unidadsat" class="unidadsat browser-default">
                            '.$option_unidad_renta.'
                            </select>
                          <div>
                        </td>
                        <td>
                            <div style="width:250px">
                              <select id="conseptosat" class="conseptosat browser-default">
                              '.$option_concepto_renta.'
                              </select>
                            <div>
                        </td>
                        <td><input id="descripcion" name="descripcion" type="text" value="'.$descripcion.' Color del '.date("d/m/Y", strtotime($fechaie)).' al '.date("d/m/Y", strtotime($fechafe)).'"></td>
                        <td><input id="precio" name="precio" class="precio  excedente precio_'.$rowcobrar.'" type="text" value="'.$color_renta_cc.'" readonly></td>
                        <td>
                            <input type="number"
                              name="descuento"
                              id="descuento"
                              class="form-control-bmz cdescuento cdescuento_'.$rowcobrar.'"
                              onclick="activardescuento('.$rowcobrar.')"
                              value="0" readonly>
                        </td>
                        <td><a class="btn-bmz red deletetrrow"><i class="fas fa-trash-alt"></i></a></td>
                      </tr>';
                      $rowcobrar++;
              }
            }
          }
        }
























     
      echo $html;
    }
    function updateccv(){
      $params = $this->input->post();
      $id = $params['id'];
      $cm = $params['cm'];
      $cc = $params['cc'];
      $this->ModeloCatalogos->updateCatalogo('centro_costos',array('clicks'=>$cm,'clicks_c'=>$cc),array('id'=>$id));
    }
    public function contratorentacliente(){   
        $idcliente = $this->input->post('id');
        $ideje = $this->input->post('ideje');
        $estatus = $this->input->post('estatus');
        $html = '';
        $html.='<div class="col s12">
                    <table id="tabla_contratos" class="display" cellspacing="0">
                      <thead>
                        <tr>
                          <th></th>
                          <th>#</th>
                          <th>vendedor</th>
                          <th>Fecha de solicitud</th>
                          <th>Vigencia</th>
                          <th>Folio contrato</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>';
                $contrato = $this->Rentas_model->contratorentaclienteeje($idcliente,$ideje,$estatus,0);
                foreach ($contrato as $item) {
                    $color_reg='';
                    if($item->estatus==2){
                      $color_reg='style="background: green;"';
                      $finalizado='Finalizado';
                    }else{
                      $color_reg='';
                      $finalizado='';
                    }  
                    $base_url=base_url();
                    if($item->vigencia==1){
                        $vigenciasuma = "+12 month";
                    }else if($item->vigencia==2){
                        $vigenciasuma = "+24 month";
                    }else if($item->vigencia==3){
                        $vigenciasuma = "+36 month";
                    } else{
                      $vigenciasuma = "+0 month";
                    }
                    $messuma = date("d-m-Y",strtotime($item->fechainicio.$vigenciasuma)); 
                    $html.='<tr '.$color_reg.'>
                              <td>'.$finalizado.'</td>
                              <td class="contratossearch contratossearch_'.$item->idcontrato.'">'.$item->idcontrato.'</td> 
                              <td>'.$item->nombre.'</td>
                              <td>'.date("d-m-Y",strtotime($item->fechainicio)).'</td>
                              <td>'.$messuma.'</td>
                              <td>'.$item->folio.'</td>
                              <td><div class="form-group">
                                  
                                  <a  class="btn btn-raised round btn-min-width mr-1 mb-1" style="background-color: green" onclick="vercontrato('.$item->idcontrato.')">Agregar</a>
                                  </div>
                              </td>
                            </tr>';
                }
              $html.='</tbody>
                    </table>
                  </div>';
        echo $html;
    }
    public function contratorentaclienteoption(){   
        $idcliente = $this->input->post('id');
        $con = $this->input->post('con');
        $html = '';
                $contrato = $this->Rentas_model->contratorentaclienteeje($idcliente,0,0,0);
                foreach ($contrato as $item) {
                    $color_reg='';
                    if($item->estatus==2){
                      $color_reg='style="background: green;"';
                      $finalizado='Finalizado';
                    }else{
                      $color_reg='';
                      $finalizado='';
                    }  
                    if($con!=$item->idcontrato){
                      $html .= '<option value="'.$item->idcontrato.'" '.$color_reg.' data-idarpex="'.$item->idarpex.'">('.$item->idcontrato.') '.$item->folio.'</option>'; 
                    }
                     
                }
        echo $html;
    }
    function agregarcontratoexte(){
      $params = $this->input->post();
      $cono = $params['cono'];
      $cond = $params['cond'];
      $this->ModeloCatalogos->Insert('alta_rentas_prefactura_cex',array('idcontrato_p'=>$cono,'idcontrato'=>$cond));
    }
    function infoequiporetiro(){
      $params = $this->input->post();
      $rowequipo = $params['rowequipo'];
      $serieId = $params['serieId'];

      $strq="SELECT asca.equipostext,ascae.modelo,ascae.serie,asca.fecha,asca.hora,asca.horafin,ascae.asignacionIdd,ascae.status,ascae.rowequipo,ascae.serieId,per.nombre,per.apellido_paterno,per.apellido_materno,asca.comentario
            FROM asignacion_ser_cliente_a as asca
            INNER JOIN asignacion_ser_cliente_a_d as ascae ON ascae.asignacionId=asca.asignacionId
            INNER JOIN personal as per on per.personalId=asca.tecnico
            WHERE ascae.tpoliza=22 AND ascae.tservicio_a=1067 AND ascae.activo=1 AND ascae.nr=0 AND ascae.rowequipo='$rowequipo' AND ascae.serieId='$serieId'
            ORDER BY `ascae`.`asignacionIdd` DESC;";
        $result = $this->db->query($strq);
        
        $comentario='';
        $fecha='';
        $hora='';
        $status='';
        $tecnico='';
        $come_t='';
        foreach ($result->result() as $item) {
          $comentario=$item->equipostext;
          $fecha = $item->fecha;
          $status = $item->status;
          $tecnico = $item->nombre.' '.$item->apellido_paterno.' '.$item->apellido_materno;
          $come_t=$item->comentario;
          $hora=$item->hora.' a '.$item->horafin;
        }

        $array = array(
              'comentario'=>$comentario,
              'fecha'=>$fecha,
              'status'=>$status,
              'tecnico'=>$tecnico,
              'comt'=>$come_t,
              'hora'=>$hora
        );
        echo json_encode($array);
    }
    function deletefacturaperiodo(){
      $params = $this->input->post();
      $prefid = $params['prefid'];
      $facId  = $params['facId'];
      $folio  = $params['folio'];
      $this->ModeloCatalogos->getdeletewheren('factura_prefactura_pagos',array('facId'=>$facId));
      $this->ModeloCatalogos->getdeletewheren('factura_prefactura',array('facId'=>$facId));
      $this->ModeloCatalogos->updateCatalogo('alta_rentas_prefactura',array('factura_renta'=>0,'factura_excedente'=>0),array('prefId'=>$prefid));
      $this->ModeloCatalogos->updateCatalogo('alta_rentas_prefactura_d',array('statusfacturar'=>0,'statusfacturae'=>0),array('prefId'=>$prefid));
      $this->ModeloCatalogos->Insert('bitacora',array('contenido'=>'Se elimino la factura '.$folio.' del periodo: '.$prefid.' liberando el periodo', 'nombretabla'=>'alta_rentas_prefactura','idtable'=>$prefid,'tipo'=>'delete','personalId'=>$this->idpersonal));
    }
    function aplasarnoti(){
      $params = $this->input->post();
      $idcon  = $params['idcon'];
      $vig    = $params['vig'];
      //=================================

        $fecha = new DateTime($this->fechahoy); // Crear un objeto DateTime con la fecha inicial
        $dias_a_sumar = 7; // Número de días que quieres sumar

        $fecha->modify("+" . $dias_a_sumar . " days"); // Sumar los días utilizando el método modify

        $vig_new = $fecha->format("Y-m-d");
        if($vig_new>$vig){
          $vig_new=$vig;
        }
      //=================================
        $this->ModeloCatalogos->updateCatalogo('contrato',array('oc_vigencia_new'=>$vig_new),array('idcontrato'=>$idcon));
    }
    function reportecontadores($idcontrato){
      if(isset($_GET['ser'])){
        $ser = $_GET['ser'];
      }else{
        $ser=1;
      }
      if(isset($_GET['fi'])){
        $fi = $_GET['fi'];
      }else{
        $fi='';
      }
      if(isset($_GET['ff'])){
        $ff = $_GET['ff'];
      }else{
        $ff='';
      }
      $alta_rentas = $this->ModeloCatalogos->db3_getselectwheren('alta_rentas',array('idcontrato'=>$idcontrato));
      $contrato = $this->ModeloCatalogos->db3_getselectwheren('contrato',array('idcontrato'=>$idcontrato));
      $contrato=$contrato->row();
      $filas=$alta_rentas->num_rows();
      
      $i_g=2;
      $monto=0;      $clicks_mono=0;      $precio_c_e_mono=0;      $montoc=0;      $clicks_color=0;      $precio_c_e_color=0; 

      foreach ($alta_rentas->result() as $item_ar) {
        $id_renta =$item_ar->id_renta;
        $i_g=$item_ar->tipo2;

        $monto=$item_ar->monto;
        if($item_ar->clicks_mono>0){
          $clicks_mono=$item_ar->clicks_mono;  
        }
        
        $precio_c_e_mono=$item_ar->precio_c_e_mono;

        $montoc=$item_ar->montoc;
        if($item_ar->clicks_color>0){
          $clicks_color=$item_ar->clicks_color;
        }
        
        $precio_c_e_color=$item_ar->precio_c_e_color;
      }
      //==============================================
        $idRentac=$contrato->idRenta;
        $datosrentas = $this->ModeloCatalogos->db3_getselectwheren('rentas',array('id'=>$idRentac));
        $datosrentas=$datosrentas->row();
        $datosrentas->idCliente;

        $datoscliente= $this->ModeloCatalogos->db3_getselectwheren('clientes',array('id'=>$datosrentas->idCliente));
        $nombreclienter='';
        foreach ($datoscliente->result() as $itemc) {
          $nombreclienter=$itemc->empresa;
        }
        
      //==============================================
        $result_equipos=$this->Rentas_model->get_equipos_contrato($idcontrato,$ser);
        $result_pref_info=$this->Rentas_model->get_info_prefactura_2($idcontrato,$fi,$ff);
        $result_prefd_info=$this->Rentas_model->get_info_prefactura_d($idcontrato,$fi,$ff);
        foreach ($result_prefd_info->result() as $item_p_i_d) {
          ${'contador_'.$item_p_i_d->fecha_form.'_'.$item_p_i_d->productoId.'_'.$item_p_i_d->serieId.'_'.$item_p_i_d->tipo.'_i'}=$item_p_i_d->c_c_f-$item_p_i_d->c_c_i;
          ${'contador_'.$item_p_i_d->fecha_form.'_'.$item_p_i_d->productoId.'_'.$item_p_i_d->serieId.'_'.$item_p_i_d->tipo.'_e'}=$item_p_i_d->e_c_f-$item_p_i_d->e_c_i;
        }
        //===============================================
          $viewr=0;
          if($viewr==0){
            header("Pragma: public");
            header("Expires: 0");
            $filename = $nombreclienter."_Reporte.xls";
            header("Content-type: application/x-msdownload");
            header("Content-Disposition: attachment; filename=$filename");
            header("Pragma: no-cache");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
          }
        //==================================================
      $html='<style type="text/css">
            .color_title{background-color:#8ea9db;}
            .color_title_i{background-color:#acb9ca;}
            .color_title_e{background-color:#c6e0b4;}
            .color_title_c{background-color:#ffe699;}
      </style>';
      $html.='<table align="center"><tr><th colspan="15" >'.$nombreclienter.'<th><tr></table>';
      $html.='<table><tr><td><td><tr></table>';
      if($i_g==2){
        $html.='<table><tr>
                      <th>RENTA<th>
                      <td>$ '.number_format(($monto),0,'.',',').'<td>';
              $html.='<th>VOLUMEN<th>
                      <td>'.$clicks_mono.'<td>
                      <th>EXCEDENTES<th>
                      <td>$ '.number_format(($precio_c_e_mono),2,'.',',').'<td>';
              if($montoc>0 || $precio_c_e_color>0){
              $html.='<th>RENTA<th>
                      <td>$ '.number_format(($montoc),0,'.',',').'<td>
                      <th>VOLUMEN COLOR<th>
                      <td>'.$clicks_color.'<td>
                      <th>EXCEDENTES COLOR<th>
                      <td>$ '.number_format(($precio_c_e_color),2,'.',',').'<td>';
              }
              $html.='<tr></table>';
        $html.='<table><tr><td><td><tr></table>';
      }
      $html.='<table border="1" align="center">';
        $html.='<thead>';
        $html.='<tr>';
          $html.='<th rowspan="2" class="color_title">Modelo</th>';
          $html.='<th rowspan="2" class="color_title">Serie</th>';
            foreach ($result_pref_info->result() as $item_pi) {
              $fecha_letra = $this->reformateofecha($item_pi->periodo_inicial); 
              $html.='<th colspan="3" class="color_title">'.$fecha_letra.'</th>';

              $fecha_f = date('m_Y',strtotime($item_pi->periodo_inicial)); 
              ${'total_i_c_e_'.$fecha_f}=0;
              ${'total_c_'.$fecha_f}=0;
            }
        $html.='</tr>';
        $html.='<tr>';
          foreach ($result_pref_info->result() as $item_pi) { 
              $html.='<th class="color_title_i">Impresion</th>';
              $html.='<th class="color_title_e">Escaneo</th>';
              $html.='<th class="color_title_c">Color</th>';
          }
        $html.='</tr>';
        $html.='</thead><tbody>';
              foreach ($result_equipos->result() as $item_eq) {
                  $productoId = $item_eq->productoId;
                  $serieId = $item_eq->serieId;
                  $html.='<tr>';
                    $html.='<td>'.$item_eq->modelo.'</td>';
                    $html.='<td>'.$item_eq->serie.'</td>';
                    foreach ($result_pref_info->result() as $item_pi) {
                      $fecha_f = date('m_Y',strtotime($item_pi->periodo_inicial)); 
                      $imprecion=0;
                      $escaneo=0;
                      $color=0; 
                      if (isset(${'contador_'.$fecha_f.'_'.$productoId.'_'.$serieId.'_1_i'})) {
                        $imprecion=${'contador_'.$fecha_f.'_'.$productoId.'_'.$serieId.'_1_i'};
                      }
                      if (isset(${'contador_'.$fecha_f.'_'.$productoId.'_'.$serieId.'_1_e'})) {
                        $escaneo=${'contador_'.$fecha_f.'_'.$productoId.'_'.$serieId.'_1_e'};
                      }
                      if (isset(${'contador_'.$fecha_f.'_'.$productoId.'_'.$serieId.'_2_i'})) {
                        $color=${'contador_'.$fecha_f.'_'.$productoId.'_'.$serieId.'_2_i'};
                      }
                      ${'total_i_c_e_'.$fecha_f}+=$imprecion+$escaneo;
                      ${'total_c_'.$fecha_f}+=$color;

                      $html.='<td>'.number_format(($imprecion),0,'.',',').'</td>';
                      $html.='<td>'.number_format(($escaneo),0,'.',',').'</td>';
                      $html.='<td>'.number_format(($color),0,'.',',').'</td>';
                    }
                  $html.='</tr>';
                  if($i_g==1){
                    $datosresntas_eq = $this->ModeloCatalogos->db3_getselectwheren('alta_rentas_equipos',array('id_renta'=>$id_renta,'id_equipo'=>$productoId,'serieId'=>$serieId));
                    foreach ($datosresntas_eq->result() as $item_dr_eq) {
                      $clicks_mono+=$item_dr_eq->clicks_mono;
                      $clicks_color+=$item_dr_eq->clicks_color;
                    }
                  }
              }
              $html.='<tr>';
                        $html.='<th colspan="2">Impresion / Copia / Escaneo</th>';
                        foreach ($result_pref_info->result() as $item_pi) {
                          $fecha_f = date('m_Y',strtotime($item_pi->periodo_inicial)); 
                          if(${'total_i_c_e_'.$fecha_f}>$clicks_mono){
                            $style=' style="background-color:#f4b084;" ';
                          }else{
                            $style=' style="background-color:#c6e0b4;" ';
                          }
                          $html.='<td colspan="3" '.$style.'>'.number_format((${'total_i_c_e_'.$fecha_f}),0,'.',',').'</td>';
                        }
              $html.='</tr>';
              $html.='<tr>';
                        $html.='<th colspan="2">Color</th>';
                        foreach ($result_pref_info->result() as $item_pi) {
                          $fecha_f = date('m_Y',strtotime($item_pi->periodo_inicial)); 
                          $html.='<td colspan="3" style="background-color:#c6e0b4;">'.number_format((${'total_c_'.$fecha_f}),0,'.',',').'</td>';
                        }
              $html.='</tr>';
              $html.='<tr>';
                        $html.='<th colspan="2">Excedentes</th>';
                        foreach ($result_pref_info->result() as $item_pi) {
                          $fecha_f = date('m_Y',strtotime($item_pi->periodo_inicial)); 
                          if(${'total_i_c_e_'.$fecha_f}>$clicks_mono){
                            $style=' style="background-color:#ffd966;" ';
                            $exce_cant=${'total_i_c_e_'.$fecha_f}-$clicks_mono;
                            $exce_cant=number_format(($exce_cant),0,'.',',');
                          }else{
                            $style='';
                            $exce_cant='';
                          }
                          $html.='<td colspan="3" '.$style.'>'.$exce_cant.'</td>';
                        }
              $html.='</tr>';
        $html.='</tbody>';
      $html.='</table>';

      echo $html;
    }
    function reformateofecha($fecha){
        $dia=date("d",strtotime(date($fecha))); 
        $mes = date("n",strtotime(date($fecha)));
        $yyyy = date("Y",strtotime(date($fecha)));
        if ($mes=='1') { $mesl ="Enero"; }
        if ($mes=='2') { $mesl ="Febrero"; }
        if ($mes=='3') { $mesl ="Marzo"; }
        if ($mes=='4') { $mesl ="Abril"; }
        if ($mes=='5') { $mesl ="Mayo"; }
        if ($mes=='6') { $mesl ="Junio"; }
        if ($mes=='7') { $mesl ="Julio"; }
        if ($mes=='8') { $mesl ="Agosto"; }
        if ($mes=='9') { $mesl ="Septiembre"; }
        if ($mes=='10') { $mesl ="Octubre"; }
        if ($mes=='11') { $mesl ="Noviembre"; }
        if ($mes=='12') { $mesl ="Diciembre"; }
        $fecha=$mesl.' de '.$yyyy;
        return $fecha; 
    }
    /*
    function verificar_serie_carga(){
      $data = $this->input->post();
      
      $periodo_inicial = $data['periodo_inicial'];
      $periodo_final = $data['periodo_final'];

      $equiposrow=$data['equipos'];
      $equiposrow = json_decode($equiposrow);
      $equipos_array=array();

      $series_row=array();
      $series_encon=array();
      //===========================================
        for ($i=0;$i<count($equiposrow);$i++) { 
          //$this->verificar_serie_carga2($equiposrow[$i]->serie,$periodo_inicial,$periodo_final);
          //$equipos_array[]=array('serie'=>$equiposrow[$i]->serie,'confirm'=>0);
          $series_row[]=$equiposrow[$i]->serie;
        }
      //===========================================
        $result =  $this->verificar_serie_carga2($series_row,$periodo_inicial,$periodo_final);
        foreach ($result->result() as $item) {
          $series_encon[]=$item->serie;
          //$equipos_array[]=array('serie'=>$item->serie);
        }
      //===========================================
        $equipos_array = array();
        foreach ($series_row as $serie) {
            $encontrado = in_array($serie, $series_encon) ? 1 : 0;
            $equipos_array[] = array('ser' => $serie, 'enc' => $encontrado);
        }
      //===========================================

      echo json_encode($equipos_array);
    }
    */
    function verificar_serie_carga(){
      $params = $this->input->post();
      $idCliente = $params['idCliente'];
      //$equiposrow=$data['equipos'];
  
        $strq="SELECT con2.* 
              FROM(
                        SELECT con.*
                        FROM carga_contadores as con 
                        WHERE con.idcliente_cc='$idCliente' AND con.pintado=0 
                        GROUP BY con.reg  
                        ORDER BY `con`.`reg` DESC LIMIT 1
              ) as da
              INNER JOIN carga_contadores as con2 on con2.reg=da.reg AND con2.pintado=0";
        $result = $this->db->query($strq);
        $series=array();
        foreach ($result->result() as $item) {
          $series[]=array('serie'=>$item->serie,'serieId'=>$item->serieId_cc,'fecha'=>$item->fecha_obtencion);
        }

      echo json_encode($series);
    }
    function verificar_serie_carga2($serie,$f_inicial,$f_final){
        $this->db->select('*');
        $this->db->from('carga_contadores');
        $this->db->where(array('activo'=>1));
        if($f_inicial!=''){
          $this->db->where("reg >= '$f_inicial 00:00:00' ");
        }
        if($f_final!=''){
          $this->db->where("reg <= '$f_final 23:59:59' ");
        }
        
        $this->db->group_start();
        
        foreach($serie as $p){
          //$this->db->or_like('per.personalId',$p);
            $this->db->or_where('serie',$p);
        }
        $this->db->group_end();  
        $this->db->group_by("serie");
        
        $query=$this->db->get();

        return $query;
    }
    function obtenerdateinstalacionretiro(){
      /*
        22 retiro
        17 instalacion

        SELECT asig.tpoliza,asig.contratoId, asigd.*
        FROM asignacion_ser_contrato_a as asig 
        INNER JOIN asignacion_ser_contrato_a_e as asigd on asigd.asignacionId=asig.asignacionId
        WHERE 
        (asig.tpoliza=22 OR asig.tpoliza=17) AND
        asig.contratoId='579' AND asigd.serieId='1010';



        SELECT * FROM asignacion_ser_cliente_a_d as asigd WHERE (asigd.tpoliza=22 or asigd.tpoliza=17) AND asigd.activo=1 AND asigd.status=2 AND asigd.rowequipo='1149';
      */
      $params = $this->input->post();
      $idcontrato=$params['idcontrato'];
      $equiposrow=$params['equiposrow'];
      unset($params['equiposrow']);

      $array=array();

      $this->db->select('asig.tpoliza,asig.contratoId, asigd.*');
      $this->db->from('asignacion_ser_contrato_a as asig');
      $this->db->join('asignacion_ser_contrato_a_e asigd', 'asigd.asignacionId=asig.asignacionId');
      $this->db->where(array('asig.contratoId'=>$idcontrato,'asigd.status'=>2));
      $this->db->where("(asig.tpoliza=22 OR asig.tpoliza=17)");
      $equiposrow = json_decode($equiposrow);
      if(count($equiposrow)>0){
        $this->db->group_start();
          //==========================
            for ($i=0;$i<count($equiposrow);$i++) { 
              $this->db->or_where('asigd.serieId',$equiposrow[$i]->serieid);
            }
          //==========================
        $this->db->group_end(); 
      }
      $query=$this->db->get();
      foreach ($query->result() as $item) {
        $array[]=array('tip'=>1,'pol'=>$item->tpoliza,'roweq'=>$item->idequipo,'serieId'=>$item->serieId,'fecha'=>$item->fecha);
      }
      echo json_encode($array);
    }
    function parametrosfactura(){
      $params = $this->input->post();
      $idcon = $params['idcon'];
      $id_fac=0;
      $id_renta=0;
      $formapagoId=0;
      $metodopagoId=0;
      $usocfdiId=0;$condicionpago='';
      $html='';
      $clientes = $this->Rentas_model->clientefactura($idcon);
      foreach ($clientes as $item) {
        $cliente = $item->empresa;
        $idcliente = $item->id;
        $rfc = $item->rfc;
        $tiporenta=$item->tiporenta;
        $idRenta = $item->idRenta;
      }
      $res_renta = $this->ModeloCatalogos->getselectwheren('alta_rentas',array('idcontrato'=>$idcon));
      
      foreach ($res_renta->result() as $item_r) {
        $id_renta=$item_r->id_renta;
      }
      $r_cfdi=$this->ModeloGeneral->genSelect('f_uso_cfdi'); 
      $r_metodo=$this->ModeloGeneral->genSelect('f_metodopago'); 
      $r_forma=$this->ModeloGeneral->genSelect('f_formapago');

      $res_df = $this->ModeloCatalogos->getselectwheren('alta_rentas_fac',array('id_renta'=>$id_renta,'activo'=>1));
      foreach ($res_df->result() as $item) {
        $id_fac=$item->id;
        $metodopagoId=$item->metodo_pago;
        $formapagoId=$item->formapago;
        $usocfdiId=$item->uso_cfdi;
        $condicionpago=$item->condicionpago;
      }
      if($id_renta>0){
        $html.='<form id="validateSubmitForm_df" method="post" autocomplete="off">'; 
                  $html.='<input id="id_renta" name="id_renta" type="hidden" value="'.$id_renta.'" >';
                  $html.='<input id="id_fac" name="id_fac" type="hidden" value="'.$id_fac.'" >';
                  //$html.='<input id="idcliente" type="hidden" value="'.$idcliente.'" >';

                    $html.='<div class="row">';
                     $html.='<div class=" col s6">';
                      $html.='<label for="" class="active">Cliente</label> <!--Llenar -->';
                      $html.='<input id="" name="cliente" type="text" value="'.$cliente.'"readonly>';
                      
                     $html.='</div> ';
                     $html.='<div class=" col s6">';
                      $html.='<label for="" class="active">RFC</label> <!--Llenar -->';
                      $html.='<input id="rfc" name="rfc" type="text" value="'.$rfc.'" readonly onchange="verficartiporfc()">';
                      
                     $html.='</div>';
                     $html.='<div class="col s6"></div><div class="col s6 infodatosfiscales"></div> ';
                    $html.='</div>';
                   
                   
                  $html.='<div class="row">';
                    $html.='<div class="col s6">';
                        $html.='<label class="active">Método de pago</label>';
                        $html.='<select id="FormaPago" name="FormaPago" class="browser-default form-control-bmz" required>';
                          $html.='<option value="" disabled selected>Selecciona una opción</option>';
                            foreach ($r_metodo as $key) {
                              if($key->metodopago==$metodopagoId){
                                $s_met='selected';
                              }else{
                                $s_met='';
                              }
                              $html.='<option value="'.$key->metodopago.'" '.$s_met.' >'.$key->metodopago_text.'</option>';
                            }
                        $html.='</select>';
                        
                      $html.='</div> ';
                      $html.='<div class="col s6">';
                        $html.='<label class="active">Forma de pago</label>';
                        $html.='<select  id="MetodoPago" name="MetodoPago" class="browser-default form-control-bmz" required>';
                          $html.='<option value="" disabled selected>Selecciona una opción</option>';
                            foreach ($r_forma as $key) { 
                              if($key->clave==$formapagoId){
                                $s_fop='selected';
                              }else{
                                $s_fop='';
                              }
                              $html.='<option value="'.$key->clave.'" '.$s_fop.'>'.$key->formapago_text.'</option>';
                            } 
                        $html.='</select>';
                        
                      $html.='</div>';
                  $html.='</div>';
                  
                  $html.='<div class="row">';
                    
                    $html.='<div class=" col s6">';
                        $html.='<label class="active">Uso CFDI</label>';
                        $html.='<select id="uso_cfdi" name="uso_cfdi" class="browser-default form-control-bmz " required>';
                          $html.='<option value="" disabled selected>Selecciona una opción</option>';
                          foreach ($r_cfdi as $key) {
                             if($key->id==$usocfdiId){
                                $s_uso='selected';
                              }else{
                                $s_uso='';
                              }
                            $html.='<option value="'.$key->uso_cfdi.'" '.$s_uso.' class="pararf '.str_replace(',',' ',$key->pararf).'" disabled >'.$key->uso_cfdi_text.'</option>';
                          }
                        $html.='</select>';
                        
                      $html.='</div>';
                    $html.='<div class="col s6">';
                      $html.='<label for="CondicionesDePago">Condicion de Pago</label>';
                      $html.='<input type="text" name="CondicionesDePago" id="CondicionesDePago" class="browser-default form-control-bmz" value="'.$condicionpago.'" onpaste="return false;">';
                    $html.='</div>';
                  $html.='</div>';
                  
                  
                   $html.='</form>';
                   $html.='<div class="row">';
                     $html.='<div class="col s12">';
                      $html.='<button class="b-btn b-btn-success" onclick="savedf()" style="margin-top: 20px;" title="Guardar datos fiscales">Guardar</button>';
                     $html.='</div>';
                   $html.='</div>';
                   
      }
      echo $html;
    }
    function savedf(){
      $params=$this->input->post();
      $id_fac = $params['id_fac'];
      unset($params['id_fac']);

      $arraydata=array(
            'metodo_pago'=>$params['FormaPago'],
            'formapago'=>$params['MetodoPago'],
            'uso_cfdi'=>$params['uso_cfdi'],
            'condicionpago'=>$params['CondicionesDePago']
          );
      if($id_fac>0){
        $this->ModeloCatalogos->updateCatalogo('alta_rentas_fac',$arraydata,array('id'=>$id_fac));
      }else{
        $arraydata['id_renta']=$params['id_renta'];
        $this->ModeloCatalogos->Insert('alta_rentas_fac',$arraydata);
      }
    }
    function save_program(){
      $params = $this->input->post();
      $fecha = $params['fecha'];
      $idfac = $params['idfac'];
      $this->ModeloCatalogos->updateCatalogo('f_facturas',array('program'=>1,'program_date'=>$fecha),array('FacturasId'=>$idfac));
    }
    public function factura_requerimientos() { //originamente estaba en el contrador Configuracionrentas
      $idfactura = $this->input->post('id');
      $tipo = $this->input->post('tip');//tipo de facturas
      $mon = $this->input->post('mon');//tipo de monocromaticas
      $col = $this->input->post('col');//tipo de color
      $equipos = $this->input->post('equipos');
      //log_message('error','$idfactura: '.$idfactura.' $tipo: '.$tipo.' $mon: '.$mon.' $col: '.$col.' $equipos: '.$equipos);
      $html = $this->ModeloCatalogos->factura_requerimientos2($idfactura,$tipo,$mon,$col,$equipos,0);

      echo $html;
    }
    function generarfacturaauto(){
      $params = $this->input->post();
      $idcontrato = $params['con'];
      $idpre = $params['idpre'];

      $info_subtotal=0;$FacturasId=0;

      $clientes = $this->Rentas_model->clientefactura($idcontrato);
      foreach ($clientes as $item) {
        $cliente = $item->empresa;
        $idcliente = $item->id;
        $rfc = $item->rfc;
        $tiporenta=$item->tiporenta;
        $idRenta = $item->idRenta;
        $ordencompra = $item->ordencompra;
      }

      //$this->ModeloCatalogos->updateCatalogo('alta_rentas_prefactura',array('factura_excedente_pendiente'=>0),array('prefId'=>$idpre));
      $dcliente=$this->ModeloCatalogos->db14_getselectwheren('clientes',array('id'=>$idcliente));
      foreach ($dcliente->result() as $item) {
        $municipio=$item->municipio;
        $idestado=$item->estado;
      }
      $destado=$this->ModeloCatalogos->db14_getselectwheren('estado',array('EstadoId'=>$idestado));
      foreach ($destado->result() as $item) {
        $estado=$item->Nombre;
      }
      $direccion=$this->ModeloCatalogos->db14_getselectwheren('cliente_has_datos_fiscales',array('rfc'=>$rfc,'idCliente'=>$idcliente,'activo'=>1));
      foreach ($direccion->result() as $item) {
        $razon_social = $item->razon_social;
        $rfc = $item->rfc;
        $cp = $item->cp;
        $num_ext = $item->num_ext;
        $num_int = $item->num_int;
        $colonia = $item->colonia;
        $calle = $item->calle;
        if ($item->estado!=null) {
          $estado=$item->estado;
        }
        if ($item->municipio!=null) {
          $municipio=$item->municipio;
        }
        $localidad=$item->localidad;
        $RegimenFiscalReceptor=$item->RegimenFiscalReceptor;
      }
      $pais = 'MEXICO';
      $TipoComprobante='I';
      //====================================================
        
            $equipos_res = $this->Rentas_model->equiposfactura($idpre);
            $equipos=array();
            foreach ($equipos_res as $item) {
                //{"equipos":"24610","renta":0,"serie":5145,"tipo":1,"excedente":0}
                $equipos[]=array(
                            'equipos'=>$item->prefdId,
                            'renta'=>$item->statusfacturar,
                            'serie'=>$item->serieId,
                            'tipo'=>$item->tipo,
                            'excedente'=>$item->statusfacturae
                            );

            }
            $equipos =json_encode($equipos);
            //echo $equipos;
        
        
        $conceptos = $this->ModeloCatalogos->factura_requerimientos2($idpre,$tiporenta,1,2,$equipos,1);
        foreach ($conceptos as $itemfac) {
          $info_subtotal+=$itemfac['Importe'];
          //log_message('error','Importe1: '.$info_subtotal);
        }
      //====================================================
        $info_iva=round($info_subtotal*0.16,2);
        $info_total=$info_subtotal+$info_iva;
        $folio = $this->Rentas_model->ultimoFolio('U') + 1;

        $strq="SELECT arenf.* FROM alta_rentas as aren INNER JOIN alta_rentas_fac as arenf on arenf.id_renta=aren.id_renta WHERE aren.idcontrato='$idcontrato'";
        $query_df = $this->db->query($strq);
        foreach ($query_df->result() as $item_df) {
          $metodo_pago=$item_df->metodo_pago;
          $formapago = $item_df->formapago;
          $uso_cfdi = $item_df->uso_cfdi;
          $condicionpago = $item_df->condicionpago;
        }


        //log_message('error','num_rows: '.$query_df->num_rows());
        if($info_subtotal>0 && $query_df->num_rows()>0){
          //============================================
          $data = array(
                "serie"         => 'U',
                "nombre"        => $razon_social,
                "direccion"     => $calle . ' No ' . $num_ext . ' ' . $num_int . ' Col: ' . 
                                   $colonia . ' , ' . 
                                   $localidad . ' , '.$municipio . ' , ' . $estado . ' . ', 
                "cp"            => $cp,
                "rfc"           => $rfc,
                "folio"         => $folio,
                "PaisReceptor"  =>$pais,
                "Clientes_ClientesId"       => $idcliente,
                //"status"        => 1,
                "TipoComprobante"=>$TipoComprobante,
                "usuario_id"       => $this->idpersonal,
                "creada_sesionId"=> $this->idpersonal,
                "rutaXml"           => '',
                "FormaPago"         => $metodo_pago,
                "tarjeta"       => '',
                "MetodoPago"        => $formapago,
                "ordenCompra"   => '',//no se ocupa
                "moneda"        => 'pesos',
                "observaciones" => '',
                "numproveedor"  => '',
                "numordencompra"=> $ordencompra,//$contrato->ordencompra
                "Lote"          => '',//no se ocupa
                "Paciente"      => '',//no se ocupa
                "Caducidad"     => '',//no se ocupa
                "uso_cfdi"      => $uso_cfdi,
                "subtotal"      => $info_subtotal,
                "iva"           => $info_iva,
                "total"         => $info_total,
                "honorario"     => $info_subtotal,
                "ivaretenido"   => 0,
                "isr"           => 0,
                "cincoalmillarval"  => 0,
                "CondicionesDePago" => $condicionpago,
                "outsourcing" => 0,
                "facturaabierta"=>0,

                "f_relacion"    => 0,
                "f_r_tipo"      => 0,
                "f_r_uuid"      => '',
                "r_regimenfiscal"=>$RegimenFiscalReceptor,
                "save"=>0,
                "program"=>0,//no va a ser programacion se timbrara directo
                "program_date"=>$this->fechahoy,
                "Estado"=>2//solo se agrega para que no se coloque por defecto el 1
            );
          $FacturasId=$this->ModeloCatalogos->Insert('f_facturas',$data);
          foreach ($equipos_res as $item) {

          }
          $this->ModeloCatalogos->updateCatalogo('alta_rentas_prefactura',array('factura_renta'=>1,'factura_excedente'=>1),array('prefId'=>$idpre));
          $this->ModeloCatalogos->updateCatalogo('alta_rentas_prefactura_d',array('statusfacturar'=>1,'statusfacturae'=>1),array('prefId'=>$idpre));
          foreach ($conceptos as $itemfac) {
            $dataco['FacturasId']=$FacturasId;
            $dataco['Cantidad'] =1;
            $dataco['Unidad'] =$itemfac['Unidad'];
            $dataco['servicioId'] =$itemfac['servicioId'];
            $dataco['Descripcion'] =$itemfac['Descripcion'];
            $dataco['Descripcion2'] =$itemfac['Descripcion2'];
            $dataco['Cu'] =$itemfac['Cu'];
            $dataco['descuento'] =0;
            $dataco['Importe'] =$itemfac['Importe'];
            $dataco['iva']=$itemfac['iva'];
            $this->ModeloCatalogos->Insert('f_facturas_servicios',$dataco);
          }
          $this->ModeloCatalogos->Insert('factura_prefactura',array('contratoId'=>$idcontrato,'prefacturaId'=>$idpre,'facturaId'=>$FacturasId));

          //============================================================
        }

      $array=array('FacturasId'=>$FacturasId);
      echo json_encode($array);
    }


}