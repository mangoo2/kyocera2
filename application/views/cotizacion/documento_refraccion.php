
            <div class="row">
                <table class="centered tablaBordesPrincipal letraTablaImpresionConsumibles" id="tablaRefacciones">
                  <thead>
                    <tr>
                        <!--<th class="tablaBordesPrincipal" width="5%">#</th>-->
                        <th class="tablaBordesPrincipal" width="10%">Pieza</th>
                        <th class="tablaBordesPrincipal" width="%%">Descripción</th>
                        <th class="tablaBordesPrincipal" width="5%">Modelo</th>
                        <th class="tablaBordesPrincipal" width="5%">Serie</th>
                        <!--<th class="tablaBordesPrincipal" width="5%">Rendimiento</th>-->
                        <th class="tablaBordesPrincipal" width="5%">No Parte</th>
                        <th class="tablaBordesPrincipal" width="11%">Precio</th>
                        <?php if ($datosCotizacion[0]->propuesta==0) { ?>
                        <!--<th class="tablaBordesPrincipal" width="7%">SubTotal</th>-->
                        <?php } ?>
                        <?php if ($datosCotizacion[0]->propuesta==1) { ?>
                          <!--<th class="tablaBordesPrincipal" width="5%">IVA</th>-->
                          <th class="tablaBordesPrincipal" width="11%">Total</th>
                        <?php } ?>
                    </tr>
                  </thead>
                  <tbody >
                    <?php 
                    foreach ($detallesCotizacion_refaccion->result() as $itemed) { 
                      $GLOBALS["total"]=$GLOBALS["total"]+($itemed->piezas*$itemed->precioGeneral);
                      ?>
                      <tr>
                        <!--<td class="tablaBordesPrincipal"><?php echo $itemed->DT_RowId;?></td>-->
                        <td class="tablaBordesPrincipal" onclick="editarp_ref(<?php echo $itemed->DT_RowId;?>,<?php echo $itemed->piezas ?>)"><?php echo $itemed->piezas ;?></td>
                        <td class="tablaBordesPrincipal"><span class="new badge red badgeaction" onclick="deleterefaccionv(<?php echo $itemed->DT_RowId;?>)" >X</span><?php echo $itemed->modelo ;?></td>
                        <td class="tablaBordesPrincipal"><?php echo $itemed->equipo ;?></td>
                        <td class="tablaBordesPrincipal">
                          <?php echo $itemed->serieequipo ;?><span class="new badge blue badgeaction" onclick="editarseriepol(<?php echo $itemed->DT_RowId;?>,1)" ><i class="fas fa-edit"></i></span>    
                        </td>
                        <!--<td class="tablaBordesPrincipal"><?php echo $itemed->rendimiento ;?></td>-->
                        <td class="tablaBordesPrincipal"><?php echo $itemed->codigo ;?></td>
                        <td class="tablaBordesPrincipal"onclick="editar_ref(<?php echo $itemed->DT_RowId;?>,<?php echo $itemed->precioGeneral;?>,<?php echo $itemed->piezas ?>)">$<?php echo number_format($itemed->precioGeneral,2,'.',','); ?></td>
                        <?php if ($datosCotizacion[0]->propuesta==0) { ?>
                        <!--<td class="tablaBordesPrincipal">$<?php echo number_format($itemed->totalGeneral,2,'.',','); ?></td>-->
                        <?php } ?>
                        <?php if ($datosCotizacion[0]->propuesta==1) { ?>
                          <!--<td class="tablaBordesPrincipal">$<?php echo number_format(($itemed->totalGeneral*0.16),2,'.',','); ?></td>-->
                          <td class="tablaBordesPrincipal">$<?php echo number_format($itemed->piezas*$itemed->precioGeneral,2,'.',','); ?></td>
                        <?php } ?>
                    </tr>
                    <?php } ?>
                    
                  </tbody>
                </table>
            </div>
<script type="text/javascript" src="<?php echo base_url(); ?>public/js/cotizacionrefacciones2.js?v=<?php echo date('YmdHi');?>" ></script>