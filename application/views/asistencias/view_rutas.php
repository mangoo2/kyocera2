<html>
<head>
  <title>Mapa con Leaflet</title>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="<?php echo base_url();?>public/plugins/leaflet/leaflet.css" />
  <style>
    #map {
      height: 99vh;
      width: 100%;
    }
  </style>
</head>
<body>
  <div id="map"></div>
  <script src="<?php echo base_url();?>public/plugins/leaflet/leaflet.js"></script>
  <!--<script src="script.js?v=1"></script>-->
  <script type="text/javascript">
    // Inicializa el mapa
    var map = L.map('map').setView([19.037778, -98.213703], 13);

    // Añade una capa de mapa (puedes usar cualquier otra capa)
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map);

    // Define el icono personalizado
    var customIcon = L.icon({
        iconUrl: '<?php echo base_url();?>public/img/pngegg.png',
        iconSize: [38, 38], // tamaño del icono
        iconAnchor: [15, 35], // punto del icono que corresponde a la posición del marcador
        popupAnchor: [0, -35] // punto desde el cual se abrirá el popup en relación con el icono
    });
    // Array de ubicaciones (latitud, longitud)
    var locations  = [];
    locations.push([19.006563, -98.149418,'Mensaje 1']);
    locations.push([19.014711, -98.137462,'Mensaje 2']);
    locations.push([19.024810, -98.140679,'Mensaje 3']);
    /*
    var locations = [
      [51.505, -0.09],
      [51.51, -0.1],
      [51.51, -0.12]
    ];
    */


    // Añade marcadores al mapa
    locations.forEach(function(location) {
        L.marker(location, { icon: customIcon }).addTo(map).bindPopup(location[2]);
    });

    // Dibuja una polilínea para conectar las ubicaciones
    var polyline = L.polyline(locations, {color: 'red'}).addTo(map);

    // Centra el mapa para que se ajuste a la polilínea
    map.fitBounds(polyline.getBounds());
  </script>
</body>
</html>