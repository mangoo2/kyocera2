<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class Modelosolicitudcons extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    /*
SELECT cons.id,cons.contratoid, cons.equipoid, cons.serieid,cons.consumibleid,cons.cantidad,cons.reg,
cli.empresa,con.folio,eq.modelo,spro.serie,ccons.modelo
FROM consumibles_solicitud as cons
INNER JOIN contrato as con on con.idcontrato=cons.contratoid
INNER JOIN rentas as ren on ren.id=con.idRenta
INNER JOIN clientes as cli on cli.id=ren.idCliente
INNER JOIN equipos as eq on eq.id=cons.equipoid
INNER JOIN series_productos as spro on spro.serieId=cons.serieid
INNER JOIN consumibles as ccons on ccons.id=cons.consumibleid
where cons.activo
*/
    function getlist($params){
        $activo=$params['activo'];
        $generado=$params['generado'];
        $columns = array( 
            0=>'cons.id',
            1=>'cons.contratoid',
            2=>'cons.equipoid',
            3=>'cons.serieid',
            4=>'cons.consumibleid',
            5=>'cons.cantidad',
            6=>'cons.reg',
            7=>'cli.empresa',
            8=>'con.folio',
            9=>'eq.modelo as modeloeq',
            10=>'spro.serie',
            11=>'ccons.modelo as modelocon',
            12=>'cons.tipo',
            13=>'cons.comen',
            14=>'cons.idenvio',
            15=>'ren.idCliente',
            16=>'con.idcontrato',
            17=>'cons.serieid',
            18=>'cons.equiporow',
            19=>'cons.generado'
        );
        $columnsss = array( 
            0=>'cons.id',
            1=>'cons.contratoid',
            2=>'cons.equipoid',
            3=>'cons.serieid',
            4=>'cons.consumibleid',
            5=>'cons.cantidad',
            6=>'cons.reg',
            7=>'cli.empresa',
            8=>'con.folio',
            9=>'eq.modelo',
            10=>'spro.serie',
            11=>'ccons.modelo',
            
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('consumibles_solicitud as cons');
        $this->db->join('contrato con', 'con.idcontrato=cons.contratoid');
        $this->db->join('rentas ren', 'ren.id=con.idRenta');
        $this->db->join('clientes cli', 'cli.id=ren.idCliente');
        $this->db->join('equipos eq', 'eq.id=cons.equipoid','left');
        $this->db->join('series_productos spro', 'spro.serieId=cons.serieid','left');
        $this->db->join('consumibles ccons', 'ccons.id=cons.consumibleid');

        $this->db->where(array('cons.activo'=>$activo));
        if($generado==0){
            $this->db->where(array('cons.generado'=>0));
        }
        if($generado==1){
            $this->db->where(array('cons.generado > '=>0));
        }

        //$this->db->where(array('facturaabierta'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columnsss as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columnsss[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    public function getlisttotal($params){
        $activo=$params['activo'];
        $generado=$params['generado'];
        $columns = array( 
            0=>'cons.id',
            1=>'cons.contratoid',
            2=>'cons.equipoid',
            3=>'cons.serieid',
            4=>'cons.consumibleid',
            5=>'cons.cantidad',
            6=>'cons.reg',
            7=>'cli.empresa',
            8=>'con.folio',
            9=>'eq.modelo',
            10=>'spro.serie',
            11=>'ccons.modelo',
            12=>'cons.generado'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('*');
        $this->db->from('consumibles_solicitud as cons');
        $this->db->join('contrato con', 'con.idcontrato=cons.contratoid');
        $this->db->join('rentas ren', 'ren.id=con.idRenta');
        $this->db->join('clientes cli', 'cli.id=ren.idCliente');
        $this->db->join('equipos eq', 'eq.id=cons.equipoid');
        $this->db->join('series_productos spro', 'spro.serieId=cons.serieid');
        $this->db->join('consumibles ccons', 'ccons.id=cons.consumibleid');
        
        $this->db->where(array('cons.activo'=>$activo));

        if($generado==0){
            $this->db->where(array('cons.generado'=>0));
        }
        if($generado==1){
            $this->db->where(array('cons.generado > '=>0));
        }

        //$where = ;
        //$this->db->where(array('facturaabierta'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        //$this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        //$this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query->num_rows();
    }
}