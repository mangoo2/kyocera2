<style type="text/css">
  .npaddingtopbuttom{
    padding-top: 0px;
    padding-bottom: 0px;
  }
  #modal_produccionlibre{
    height: 214px;
  }
  .error{
    color: red;
  }
</style>
<input type="hidden" value="<?php echo $tiporenta;?>" id="tiporenta" readonly>
<section id="content" width="100%">
  
  <!--start container-->
  <div class="container" width="100%">
    <div class="section">
      <div class="row">
        <p class="caption col s6">Facturar</p>
      </div>
      <div class="row">
        <input type="hidden" id="idfactura" value="<?php echo $idfactura ?>">
        <input type="hidden" id="idcontrato" value="<?php echo $idcontrato ?>">
        <!-- FORMULARIO  <?php ?>-->
          <div class="col s12 m12 24">
            <div class="card-panel">
              <div class="row"> 
                <form id="validateSubmitForm" method="post" autocomplete="off"> 
                  <div class="row">
                   <div class="input-field col s6">
                    <i class="material-icons prefix">face</i>
                    <input id="idcliente" name="idcliente" type="hidden" value="<?php echo $idcliente ?>" >
                    <input id="" name="cliente" type="text" value="<?php echo $cliente ?>"readonly>
                    <label for="">Cliente</label> <!--Llenar -->
                   </div> 
                   <div class="input-field col s6">
                    <i class="material-icons prefix">receipt</i>
                    <input id="rfc" name="rfc" type="text" value="<?php echo $rfc ?>" readonly onchange="verficartiporfc()">
                    <label for="">RFC</label> <!--Llenar -->
                   </div>
                   <div class="col s6"></div><div class="col s6 infodatosfiscales"></div> 
                  </div>
                  <div class="row">
                    <div class="input-field col s6">
                        <i class="material-icons prefix">monetization_on</i>
                        <select id="FormaPago" name="FormaPago" class="browser-default chosen-select" required>
                          <option value="" disabled selected>Selecciona una opción</option>
                          <?php foreach ($metodo as $key) { ?>
                            <option value="<?php echo $key->metodopago; ?>" <?php if($key->id==$metodopagoId) echo 'selected'?>><?php echo $key->metodopago_text; ?></option>
                          <?php } ?>
                        </select>
                        <label class="active">Método de pago</label>
                      </div> 
                      <div class="col s6">
                        <label class="active">Forma de pago</label>
                        <select  id="MetodoPago" name="MetodoPago" class="browser-default" required>
                          <option value="" disabled selected>Selecciona una opción</option>
                          <?php foreach ($forma as $key) { ?>
                            <option value="<?php echo $key->formapago; ?>" <?php if($key->id==$formapagoId) echo 'selected'?>><?php echo $key->formapago_text; ?></option>
                          <?php } ?>
                        </select>
                        
                      </div>
                  </div>
                  <div class="row">
                   <div class="input-field col s6">
                    <i class="material-icons prefix">date_range</i>
                    <input id="" disabled="" name="" type="text" value="">
                    <label for="">Fecha Aplicación</label> <!--Llenar -->
                   </div> 
                   <div class="input-field col s6">
                    <i class="material-icons prefix">add_location</i>
                    <input id="" disabled="" name="" type="text" value="">
                    <label for="">Lugar Expedición</label> <!--Llenar -->
                   </div> 
                  </div>
                  <div class="row">
                    <div class="input-field col s6">
                      <i class="material-icons prefix">euro_symbol</i>
                      <select id="moneda" name="moneda" class="span12 form-control browser-default chosen-select" required>
                          <option value="pesos">MXN Peso Mexicano</option>
                          <option value="USD">Dólares Norteamericanos</option>
                          <option value="EUR">Euro</option>
                      </select>
                      <label for="" class="active">Tipo Moneda</label> <!--Llenar -->
                    </div> 
                    <div class="input-field col s6">
                        <i class="material-icons prefix">receipt</i>
                        <select id="uso_cfdi" name="uso_cfdi" class="browser-default chosen-select" required>
                          <option value="" disabled selected>Selecciona una opción</option>
                          <?php foreach ($cfdi as $key) { ?>
                            <option 
                                  value="<?php echo $key->uso_cfdi; ?>" 
                                  <?php if($key->id==$usocfdiId) echo 'selected'?>
                                  class="pararf <?php echo str_replace(',',' ',$key->pararf) ?>" disabled ><?php echo $key->uso_cfdi_text; ?></option>
                          <?php } ?>
                        </select>
                        <label class="active">Uso CFDI</label>
                      </div>
                    <div class="col s6">
                      <label for="CondicionesDePago">Condicion de Pago</label>
                      <input type="text" name="CondicionesDePago" id="CondicionesDePago" class="browser-default form-control-bmz" onpaste="return false;">
                    </div>
                  </div>
                  <div class="row">
                    <div class="col s7">
                      
                      
                      <input type="checkbox" class="filled-in" id="facturarelacionada" >
                      <label for="facturarelacionada">Factura Relacionada</label>
                    </div>
                    
                    <div class="col s12">    
                      <div class="form-group divfacturarelacionada" style="display:none;">
                        <div class="col s4">
                          <div class="form-group">
                            <label>Tipo Relacion</label>
                            <select class="browser-default form-control-bmz" id="TipoRelacion">
                              <!--
                              <option value="01">01 Nota de crédito de los documentos relacionados</option>
                              <option value="02">02 Nota de débito de los documentos relacionados</option>
                              <option value="03">03 Devolución de mercancía sobre facturas o traslados previos</option>
                              -->
                              <option value="04">04 Sustitución de los CFDI previos</option>
                              <!--
                              <option value="05">05 Traslados de mercancias facturados previamente</option>
                              <option value="06">06 Factura generada por los traslados previos</option>
                              <option value="07">07 CFDI por aplicación de anticipo</option>
                              -->
                            </select>
                          </div>
                        </div>
                        <div class="col s4">
                          <div class="form-group">
                            <label>Folio Fiscal</label>
                            <input type="text" id="uuid_r" class="form-control-bmz" placeholder="111AAA1A-1AA1-1A11-11A1-11A1AA111A11">
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row tarjeta_datos" style="display: none;">
                    <div class="input-field col s6">
                      <i class="material-icons prefix">credit_card</i>
                      <input id="tarjeta" name="tarjeta" type="number" value="">
                      <label for="">Últimos Dígitos:</label> <!--Llenar -->
                    </div>  
                  </div>
                   <div class="row">
                     <div class="col s12">
                       <hr>
                     </div>
                     <div>
                       <ul class="collapsible" data-collapsible="accordion">
                        <li>
                         <div class="collapsible-header li_pedimentos">Pedimento</div>
                         <div class="collapsible-body row">
                           <div class="input-field col s6">
                            <i class="material-icons prefix"></i>
                            <input id="numproveedor" name="numproveedor" type="text" value="">
                            <label for="">Numero de proveedor</label> <!--Llenar -->
                           </div>
                           <div class="input-field col s6">
                            <i class="material-icons prefix"></i>
                            <input id="numordencompra" name="numordencompra" type="text" value="">
                            <label for="">numero de orden de compra</label> <!--Llenar -->
                           </div>
                           <div class="input-field col s6">
                            <textarea id="observaciones" name="observaciones" class="materialize-textarea"></textarea>
                            <label for="">Observaciones</label>
                            </div>
                         </div>
                        </li>
                       </ul>
                     </div>

                   </div>
                   </form>
                   <div class="row">
                     <div class="col s12">
                       <hr>
                     </div>
                     <div>
                       <ul class="collapsible" data-collapsible="accordion">
                        <li>
                         <div class="collapsible-header">Requerimentos</div>
                         <div class="collapsible-body row">
                          <div class="col s6">
                           <p>
                            <table id="equipos_facturar">
                              <thead>
                                <tr>
                                  <th></th>
                                  <th>Modelo</th>
                                  <th>Serie</th>
                                  <th>Renta</th>
                                  <th>Excedente</th>
                                </tr>
                              </thead>
                              <tbody>
                               <?php foreach ($equipos as $item) { ?>
                                <tr>
                                  <td class="npaddingtopbuttom">
                                    <input 
                                      type="checkbox" 
                                      id="testadd_<?php echo $item->prefdId ?>" 
                                      class="testadd" 
                                      value="<?php echo $item->prefdId ?>"
                                      data-renta='<?php echo $item->statusfacturar ?>'
                                      data-excedente='<?php echo $item->statusfacturae ?>'
                                      data-serie='<?php echo $item->serieId ?>'
                                      data-tipo='<?php echo $item->tipo ?>'
                                      checked 
                                      <?php if($tiporenta==2){ echo 'disabled'; } ?> 
                                    >
                                    <label for="testadd_<?php echo $item->prefdId ?>"></label></td>
                                  <td class="npaddingtopbuttom"><?php echo $item->modelo ?></td>
                                  <td class="npaddingtopbuttom"><?php echo $item->serie ?></td>
                                  <td>
                                    <?php if($item->statusfacturar==1){
                                      echo '<b>R</b>';
                                    }?>
                                  </td>
                                  <td>
                                    <?php if($item->statusfacturae==1){
                                      echo '<b>E</b>';
                                    }?>
                                  </td>
                                </tr> 
                               <?php }?>                        
                              </tbody>
                            </table>
                           </p> 
                           <div class="row">
                             <div class="col s6">
                                 <p>
                                  <input type="checkbox" id="tipomc_m" checked onchange="checkfactura()">
                                  <label for="tipomc_m">Monocromatico</label>
                                </p>
                             </div>
                             <div class="col s6">
                                
                                <p>
                                  <input type="checkbox" id="tipomc_c" checked onchange="checkfactura()">
                                  <label for="tipomc_c">Color</label>
                                </p>
                             </div>
                           </div>
                          
                           <p>
                            <input class="with-gap" name="radio0" type="radio" id="checkfact2" value="2" onchange="checkfactura()" />
                            <label for="checkfact2">Facturar renta con excedentes</label>
                            <input class="with-gap" name="radio0" type="radio" id="checkfact1" value="1" onchange="checkfactura()" />
                            <label for="checkfact1">Facturar renta</label>
                            <input class="with-gap" name="radio0" type="radio" id="checkfact3" value="3" onchange="checkfactura()" />
                            <label for="checkfact3">Facturar excedentes</label>
                            <button class="btn-bmz cyan waves-effect green" title="Cambiar Excedente de global a unitario" onclick="cameu()" type="button">E/U</button>
                            </p>
                          </div>
                          <div class="col s6">
                            <p>
                              <input type="checkbox" id="testadd" onchange="view_factura()">
                              <label for="testadd">Agregar prefactura</label>
                            </p>
                            <p>
                              <div class="view_factura" style="display: none">
                                <div class="row">
                                  <div class="col">
                                    <h6>Seleccionar cliente</h6>
                                  </div>  
                                  <div class="col s8">
                                    <select id="idclientec" name="idclientec" class="browser-default chosen-select" onchange="clientecontrato()">
                                      <option value="" disabled selected>Selecciona una opción</option>
                                      <<?php foreach ($clientes as $item){ ?>
                                        <option value="<?php echo $item->id?>" ><?php echo $item->empresa?></option>
                                      <?php } ?>
                                    </select>
                                  </div>
                                </div>
                                <br>
                                <div class="mostrar_tabla"></div><br>
                                <div class="mostrar_factura"></div>
                              </div>  
                            </p>
                            <p class="check_renta" style="display: none">
                            <input class="with-gap" name="radio1" type="radio" id="checkfact21" value="1" onchange="checkfactura2()" />
                            <label for="checkfact21">Facturar renta</label>
                            <input class="with-gap" name="radio1" type="radio" id="checkfact22" value="2" onchange="checkfactura2()" />
                            <label for="checkfact22">Facturar renta con excedentes</label>
                            <input class="with-gap" name="radio1" type="radio" id="checkfact23" value="3" onchange="checkfactura2()" />
                            <label for="checkfact23">Capturar excedentes</label>
                            </p>
                          </div>

                          <div class="row">
                            <div class="col s6">
                              
                            </div>
                            <div class="col s6">
                              <table class="table striped" id="tablepediodosextras">
                                <thead>
                                  <tr>
                                    <th>Contrato</th>
                                    <th>Periodo</th>
                                    <th></th>
                                  </tr>
                                </thead>
                                <tbody class="tablepediodosextrastbody">
                                  
                                </tbody>
                              </table>
                            </div>
                          </div>
                          <div class="col s5 div_fac_pen">
                      <input type="checkbox" class="filled-in" id="fac_pen" >
                      <label for="fac_pen" title="Especificar si los excedentes quedaran pendientes de facturar">Es parcial</label>
                    </div>  
                         </div>
                        </li>
                       </ul>
                       <?php if($tiporenta==2){ ?>
                          <style type="text/css">
                            .btn-agrega[disabled]{
                              background-color: #8a928b;
                              border-color: #8a928b;
                            }
                          </style>
                          <ul class="collapsible" data-collapsible="accordion">
                        <li>
                         <div class="collapsible-header">Centro de costos</div>
                         <div class="collapsible-body">
                          <div class="row">
                            <div class="col s3">
                              <label>Renta Monocromatico</label>
                              <input type="number" class="form-control-bmz info_ren_exce" id="info_renta_mono" value="<?php echo $result_pre->subtotal; ?>" readonly>
                            </div>
                            <div class="col s3">
                              <label>Renta Color</label>
                              <input type="number" class="form-control-bmz info_ren_exce" id="info_renta_color" value="<?php echo $result_pre->subtotalcolor; ?>" readonly>
                            </div>
                            <div class="col s3">
                              <label>Excedente Monocromatico</label>
                              <input type="number" class="form-control-bmz info_ren_exce" id="info_excede_mono" value="<?php echo $result_pre->excedente; ?>" readonly>
                            </div>
                            <div class="col s3">
                              <label>Excedente Color</label>
                              <input type="number" class="form-control-bmz info_ren_exce" id="info_excede_color" value="<?php echo $result_pre->excedentecolor; ?>" readonly>
                            </div>
                            <div class="col s4">
                              <style type="text/css">
                                .info_ren_exce_value{
                                  margin-bottom: 0px;
                                }
                                .table_ccs td{
                                  padding: 5px;
                                }
                              </style>
                              <table class="table table_ccs">
                                <thead>
                                  <tr>
                                    <th>Centro de costos</th>
                                    
                                    <th></th>
                                    <th></th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <?php foreach ($result_cc->result() as $itemcc) { ?>
                                    <tr>
                                      <td><?php echo $itemcc->descripcion;?></td>
                                      
                                      <td><button class="b-btn b-btn-success btn-agrega btn-agrega_ccs" onclick="agregarequiposccs(<?php echo $itemcc->id;?>,<?php echo $idfactura;?>,<?php echo $idcontrato;?>,'<?php echo $result_pre->periodo_inicial; ?>','<?php echo $result_pre->periodo_final; ?>',0)">Agregar Detallado</button></td>
                                      <td><button class="b-btn b-btn-success btn-agrega btn-agrega_ccs" onclick="agregarequiposccs(<?php echo $itemcc->id;?>,<?php echo $idfactura;?>,<?php echo $idcontrato;?>,'<?php echo $result_pre->periodo_inicial; ?>','<?php echo $result_pre->periodo_final; ?>',1)">Agregar Global</button></td>
                                    </tr>
                                  <?php } ?>
                                  <?php if($result_pre->excedente>0){ ?>
                                    <tr>
                                      <td>Excedente Monogromatico</td>
                                      
                                      <td><button class="b-btn b-btn-success btn-agrega btn-agrega_0" onclick="agregarex(0)"  
                                        data-agregado="Excedente monocromatico del <?php echo date("d/m/Y", strtotime($result_pre->periodo_inicial)); ?> al <?php echo date("d/m/Y", strtotime($result_pre->periodo_final)); ?>"
                                        data-monto="<?php echo $result_pre->excedente; ?>">Agregar</button></td>
                                      <td></td>
                                    </tr>
                                  <?php } ?>
                                  <?php if($result_pre->excedentecolor>0){ ?>
                                    <tr>
                                      <td>Excedente Color</td>
                                      
                                      <td><button class="b-btn b-btn-success btn-agrega btn-agrega_1" onclick="agregarex(1)" 
                                        data-agregado="Excedente Color del <?php echo date("d/m/Y", strtotime($result_pre->periodo_inicial)); ?> al <?php echo date("d/m/Y", strtotime($result_pre->periodo_final)); ?>"
                                        data-monto="<?php echo $result_pre->excedentecolor; ?>">Agregar</button></td>
                                      <td></td>
                                    </tr>
                                  <?php } ?>
                                </tbody>
                              </table>
                              
                            </div>
                          </div>

                            
                         </div>
                        </li>
                       </ul>
                       <?php } ?>
                     </div>

                   </div>
                
                <!-- inicio -->
                <div class="row">
                  <div class="col s12">
                    <table id="table_conceptos" class="striped">
                      <thead>
                        <tr>
                          <th width="8%">Cantidad</th>
                          <th width="20%">Unidad SAT</th>
                          <th width="20%">Concepto SAT</th>
                          <th width="30%">Descripción</th>
                          <th width="9%">Precio</th>
                          <th width="8%">Descuento</th>
                          <th width="5%"></th>
                        </tr>
                      </thead>
                      <tbody class="addcobrar">
                        
                      </tbody>
                    </table>
                    
                  </div>
                </div>
                <div class="row">
                  <div class="col s8">
                    
                  </div>
                  <div class="col s4">
                    <div class="row">
                      <div class="col s7" style="text-align: right;">
                        Subtotal
                      </div>
                      <div class="col s5">
                        <span style="float: left;">$</span>
                        <input type="" name="Subtotalinfo" id="Subtotalinfo" readonly style="background: transparent; border:0; width: 90%;" value="0.00">
                        <input type="hidden" name="Subtotal" id="Subtotal" readonly style="background: transparent; border:0; width: 90%;" value="0.00">
                      </div>
                    </div> 
                    <div class="row">
                      <div class="col s7" style="text-align: right;">
                        Descuento
                      </div>
                      <div class="col s5">
                        <span style="float: left;">$</span>
                        <input type="" name="descuentof" id="descuentof" readonly style="background: transparent; border:0; width: 90%;" value="0.00">
                      </div>
                    </div> 
                    <div class="row">
                      <div class="col s7" style="text-align: right;">
                        I.V.A
                      </div>
                      <div class="col s5">
                        <span style="float: left;">$</span>
                        <input type="" name="iva" id="iva" readonly style="background: transparent; border:0; width: 90%;" value="0.00">
                      </div>
                    </div> 
                    <div class="row">
                      <div class="col s7" style="text-align: right;">
                        <input type="checkbox" id="risr" />
                        <label for="risr" onclick="calculartotales_set(1000)">10 % Retencion I.S.R.</label>
                      </div>
                      <div class="col s5">
                        <span style="float: left;">$</span>
                        <input type="" name="isr" id="isr" readonly style="background: transparent; border:0; width: 90%;" value="0.00">
                      </div>
                    </div> 
                    <div class="row">
                      <div class="col s7" style="text-align: right;">
                        <input type="checkbox" id="riva" />
                        <label for="riva" onclick="calculartotales_set(1000)">10.67 % Retencion I.V.A.</label>
                      </div>
                      <div class="col s5">
                        <span style="float: left;">$</span>
                        <input type="" name="ivaretenido" id="ivaretenido" readonly style="background: transparent; border:0; width: 90%;" value="0.00">
                      </div>
                    </div>
                    <div class="row">
                      <div class="col s7" style="text-align: right;">
                        <input type="checkbox" id="5almillar" />
                        <label for="5almillar" onclick="calculartotales_set(1000)">5 al millar.</label>
                      </div>
                      <div class="col s5">
                        <span style="float: left;">$</span>
                        <input type="" name="5almillarval" id="5almillarval" readonly style="background: transparent; border:0; width: 90%;" value="0.00">
                      </div>
                    </div>
                    <div class="row">
                      <div class="col s7" style="text-align: right;">
                        <input type="checkbox" id="aplicaout" />
                        <label for="aplicaout" onclick="calculartotales_set(1000)">6% Retencion servicios de personal.</label>
                      </div>
                      <div class="col s5">
                        <span style="float: left;">$</span>
                        <input type="" name="outsourcing" id="outsourcing" readonly style="background: transparent; border:0; width: 90%;" value="0.00">
                      </div>
                    </div>   
                    <div class="row">
                      <div class="col s7" style="text-align: right;">
                        Total
                      </div>
                      <div class="col s5">
                        <span style="float: left;">$</span>
                        <input type="" name="total" id="total" readonly style="background: transparent; border:0; width: 90%;" value="0.00">
                      </div>
                    </div> 
                    
                  </div>
                </div>
                <!-- fin --> 
                <div class="row">
                  <div class="botonfinalizar"></div>
                  <div class="input-field col s9">
                    <button class="btn-bmz cyan waves-effect green  produccionlibe" type="button">Producción Libre</button>
                  </div>
                  <div class="input-field col s3 guadarremove">
                    <button class="btn cyan waves-effect waves-light right registrofac_preview" type="button">Facturar<i class="material-icons right">send</i>
                    </button>
                  </div>
                </div>

              </div>
            </div>
          </div>
        
      </div>
    </div>
  </section>
  <div id="modal_produccionlibre" class="modal fade modal-fixed-footer" data-keyboard="false" data-backdrop="static">
    <div class="modal-content " style="padding-left: 0px; padding-right: 0px;">
        <h4 style="padding-left: 24px">Agregar valor por producción</h4>
        <div class="row " style="margin-bottom: 0px;">
          <div class="col s4">
            <label for="valorpl">Precio por pagina:</label>
          </div>
          <div class="col s8">
            <input type="number" class="form-control-bmz" name="valorpl" id="valorpl">
          </div>
        </div>
    </div>
    <div class="modal-footer">
        <a class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
        <a class="waves-effect modal-close waves-red red btn-flat" style="color: white;" onclick="plgenerar()">Generar</a>
    </div>
    <!-- Modal finaliza fin-->
</div>
<style type="text/css">
  .ifrafac{
    width: 100%;
    height: 412px;
    border: 0;
  }
</style>
<div id="modal_previefactura" class="modal modal-fixed-footer" style="width: 80%;">
    <div class="modal-content" style="padding: 0px;">
      <div class="row">
        <!--<div class="col s12">
          Favor de validar los siguientes datos del receptor:
        </div>
        <div class="col s4">
          <b>Nombre Fiscal: </b><br><span class="v_nomfical"></span>
        </div>
        <div class="col s4">
          <b>Direccion Fiscal(C.P.): </b><br><span class="v_direccionfiscal"></span>
        </div>
        <div class="col s4">
          <b>Regimen Fiscal Receptor: </b><br><span class="v_regimenfiscal"></span>
        </div>-->
        <div class="col-md-12 preview_iframe">
          
        </div>
      </div>
    </div>
    <div class="modal-footer">
      <div class="row">
        <div class="col s6" style="text-align: left;">
          <button  class="modal-close b-btn b-btn-primary registrofac" onclick="registrofac(0)">Guardar</button>
        </div>
        <div class="col s6">
          <a href="#!" class="modal-close b-btn b-btn-secondary btn-flat">Cancelar</a>
          <button  class="modal-close b-btn b-btn-success registrofac" onclick="registrofac(1)">Aceptar</button>
        </div>
      </div>
      
    </div>
  </div>
  <div class="addinsercionprefacturas" style="display:none;">
    <iframe src="<?php echo base_url();?>index.php/Reportes/rentadetalles/<?php echo $idfactura;?>/<?php echo $idcontrato;?>"></iframe>
  </div>