<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/plugins/toastr/toastr.css">
<script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/toastr/toastr.min.js" ></script>

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/plugins/touchspin/jquery.bootstrap-touchspin.min.css">
<script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/touchspin/jquery.bootstrap-touchspin.min.js" ></script>

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/isotope.css">
<script type="text/javascript" src="<?php echo base_url(); ?>public/js/isotope.pkgd.min.js" ></script>

<script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/tipped/tipped-master/dist/js/tipped.min.js" ></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/plugins/tipped/tipped-master/dist/css/tipped.css">

<script type="text/javascript" src="<?php echo base_url(); ?>public/js/form_ventasd.js?v=<?php echo date('YmdGis');?>" ></script>
<script type="text/javascript">
	direccionesclientes(<?php echo $idCliente;?>);
	obtenerservicios2();
	obtenerventaspendientes(<?php echo $idCliente;?>);
	bodegasequipo(0);
	function bodegasequipo(id){
		if(id>0){
			var etiqueta='.notificacion_tipped_'+id;
		}else{
			var etiqueta='.notificacion_tipped';
		}
		Tipped.create(etiqueta, {
	      ajax: {
	        url: "<?php echo base_url();?>Ventas/stockporbodegas",
	        type: 'post',
  			data: { 
  				equipo: id 
  			},
	        success: function (data, textStatus, jqXHR) {
	          return jqXHR.responseText;
	        },
	      },
	    });
	}
	/*
	$(document).ready(function($) {
		Tipped.create(".notificacion_tipped", {
	      ajax: {
	        url: "<?php echo base_url();?>Ventas/stockporbodegas",
	        type: 'post',
  			data: { 
  				equipo: $(this).data('equipo') 
  			},
	        success: function (data, textStatus, jqXHR) {
	          return jqXHR.responseText + ", this is a modified response";
	        },
	      },
	    });

	});
	*/
	<?php if($bloqueo==1){ ?>
		$(document).ready(function($) {
			var base_url = $('#base_url').val();
			swal({
			  title: "Bloqueado",
			  text: "Cliente bloqueado, contacte a administración.",
			  type: "warning",
			  showCancelButton: false,
			  confirmButtonColor: "#DD6B55",
			  confirmButtonText: "OK",
			  closeOnConfirm: false
			},
			function(){
			  location.href =base_url+'index.php/Clientes';

			});
		});
	<?php } ?>
</script>