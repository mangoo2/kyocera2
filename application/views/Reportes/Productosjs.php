<link href="<?php echo base_url(); ?>public/plugins/select2/select2.min.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/select2/select2.full.min.js"></script>

<script type="text/javascript">
  function generarreporte(){
    var fechainicio = $('#fechainicio').val();
    var fechafinal = $('#fechafinal').val();
    var tiporeporte = $('#tiporeporte').val();
    var idcliente = $('#idcliente').val();
    if(idcliente>0){
        idcliente=idcliente;
    }else{
        idcliente=0;
    }
    var tipopgg = $('#tipopgg option:selected').val();
    var tipo=$('#tipop option:selected').val();
    if(tipo==0){
        var producto=0;
    }
    if(tipo==1){
        var producto=$('#selecte option:selected').val()==undefined?0:$('#selecte option:selected').val();
    }
    if(tipo==2){
        var producto=$('#selectr option:selected').val()==undefined?0:$('#selectr option:selected').val();
    }
    if(tipo==3){
        var producto=$('#selecta option:selected').val()==undefined?0:$('#selecta option:selected').val();
    }
    if(tipo==4){
        var producto=$('#selectc option:selected').val()==undefined?0:$('#selectc option:selected').val();
    }
    //window.open('<?php base_url();?>Productos/exportar/'+fechainicio+'/'+fechafinal+'/'+tiporeporte+'/'+idcliente, '_blank');
    window.open('<?php base_url();?>Productos/entradassalidas?fechainicio='+fechainicio+'&fechafinal='+fechafinal+'&tipo='+tipo+'&tipopgg='+tipopgg+'&producto='+producto, '_blank');
  }
  $(document).ready(function($) {
    var base_url=$('#base_url').val();
   $('#idcliente').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una opción',
        allowClear: true,
        ajax: {
            url: '<?php base_url();?>Ventasi/searchclientes',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.id,
                        text: element.empresa

                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }); 
    $('#selecte').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un equipo',
          ajax: {
            url: base_url+'Inventario_productos/selecte',
            dataType: "json",
            data: function (params) {
              var query = {
                search: params.term,
                type: 'public'
              }
              return query;
          },
          processResults: function(data){
            var clientes=data;
            var itemscli = [];
            data.forEach(function(element) {
                    itemscli.push({
                        id: element.id,
                        text: element.modelo
                    });
                });
                return {
                    results: itemscli
                };        
          },  
        }
    });
    $('#selectr').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una refaccion',
          ajax: {
            url: base_url+'Inventario_productos/selectr',
            dataType: "json",
            data: function (params) {
              var query = {
                search: params.term,
                type: 'public'
              }
              return query;
          },
          processResults: function(data){
            var clientes=data;
            var itemscli = [];
            data.forEach(function(element) {
                    itemscli.push({
                        id: element.id,
                        text: element.nombre
                    });
                });
                return {
                    results: itemscli
                };        
          },  
        }
    }); 
    $('#selecta').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un accesorio',
          ajax: {
            url: base_url+'Inventario_productos/selecta',
            dataType: "json",
            data: function (params) {
              var query = {
                search: params.term,
                type: 'public'
              }
              return query;
          },
          processResults: function(data){
            var clientes=data;
            var itemscli = [];
            data.forEach(function(element) {
                    itemscli.push({
                        id: element.id,
                        text: element.nombre
                    });
                });
                return {
                    results: itemscli
                };        
          },  
        }
    }); 
    $('#selectc').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un consumible',
          ajax: {
            url: base_url+'Inventario_productos/selectc',
            dataType: "json",
            data: function (params) {
              var query = {
                search: params.term,
                type: 'public'
              }
              return query;
          },
          processResults: function(data){
            var clientes=data;
            var itemscli = [];
            data.forEach(function(element) {
                    itemscli.push({
                        id: element.id,
                        text: element.modelo
                    });
                });
                return {
                    results: itemscli
                };        
          },  
        }
    }); 
  });
function tipopselec(){
  var tipo=$('#tipop option:selected').val();
  if(tipo==1){
    $('.selecte').show('show');
    $('.selectr').hide();
    $('.selecta').hide();
    $('.selectc').hide();
  }
  if(tipo==2){
    $('.selecte').hide();
    $('.selectr').show('show');
    $('.selecta').hide();
    $('.selectc').hide();
  }
  if(tipo==3){
    $('.selecte').hide();
    $('.selectr').hide();
    $('.selecta').show('show');
    $('.selectc').hide();
  }
  if(tipo==4){
    $('.selecte').hide();
    $('.selectr').hide();
    $('.selecta').hide();
    $('.selectc').show('show');
  }
  if(tipo==0){
    $('.selecte').hide();
    $('.selectr').hide();
    $('.selecta').hide();
    $('.selectc').hide();
  }
}

</script> 