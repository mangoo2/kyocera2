<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class Estados_model extends CI_Model 
{
    public function __construct() 
    {
        parent::__construct();
    }

	public function getEstados()
    {
        $sql = "SELECT * FROM estados";
        $query = $this->db->query($sql);
        return $query->result();
    }
   
}