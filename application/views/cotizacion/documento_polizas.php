
<div class="row">
              <table class="centered bordered letraTablaImpresionPoliza tablaBordesPrincipal" style="width:100%">
                <?php 

                  foreach ($datosCotizacionequipospoliza->result() as $iteme) { 
                    $detallesCotizacion = $this->Cotizaciones_model->getDetallesCotizacionPolizaTablae($iteme->idCotizacion,$iteme->idEquipo);
                  ?>
                    <tr>
                      <th style="float: inline-start;"><?php echo $iteme->modelo.' '.$iteme->serie; ?><span class="new badge red badgeaction"  onclick="deletepoliza(<?php echo $iteme->id;?>)" >X</span>
                          <span class="new badge blue badgeaction"  onclick="editarseriepol(<?php echo $iteme->id;?>,0)" title="Editar serie"><i class="fas fa-edit"></i></span> 
                      </th>
                    </tr>
                    <tr>
                      <td>
                        <table class="centered bordered letraTablaImpresionPoliza"style="width:100%">
                          <thead >
                            <tr class="tablaBordesPrincipal">
                                <th class="tablaBordesPrincipal" >Tipo de Servicio</th>
                                <th class="tablaBordesPrincipal" >Cobertura</th>
                                <th class="tablaBordesPrincipal" >Vigencia</th>
                                <th class="tablaBordesPrincipal" >Visitas</th>
                                <th class="tablaBordesPrincipal" >Cantidad</th>
                                <th class="tablaBordesPrincipal" >Precio</th>
                                <?php if ($datosCotizacion[0]->propuesta==1) { ?>
                                  <!--<th class="tablaBordesPrincipal" >I.V.A</th>-->
                                  <th class="tablaBordesPrincipal" >Total</th>
                                <?php }  ?>
                            </tr>
                          </thead>
                          <tbody >
                            <?php 
                              
                            foreach ($detallesCotizacion->result() as $itemed) { 
                              if($itemed->precio_local!=null){
                                $preciovalor=$itemed->precio_local;
                                $columna='precio_local';
                              }
                              if($itemed->precio_semi!=null){
                                $preciovalor=$itemed->precio_semi;
                                $columna='precio_semi';
                              }
                              if($itemed->precio_foraneo!=null){
                                $preciovalor=$itemed->precio_foraneo;
                                $columna='precio_foraneo';
                              }
                              if($itemed->precio_especial!=null){
                                $preciovalor=$itemed->precio_especial;
                                $columna='precio_especial';
                              }

                              ?>
                              <tr class="tablaBordesPrincipal">
                                  <td class="tablaBordesPrincipal"><?php echo $itemed->nombre;?></td>
                                  <td class="tablaBordesPrincipal" 
                                      onclick="editar(<?php echo $itemed->id;?>,'cubre','<?php echo $itemed->cubre;?>')"
                                  ><?php echo $itemed->cubre;?></td>
                                  <td class="tablaBordesPrincipal" 
                                      onclick="editar(<?php echo $itemed->id;?>,'vigencia_meses','<?php echo $itemed->vigencia_meses;?>')"
                                  ><?php echo $itemed->vigencia_meses;?></td>
                                  <td class="tablaBordesPrincipal"><?php echo $itemed->vigencia_clicks;?></td>
                                  <!--<td class="tablaBordesPrincipal"><?php echo $itemed->modelo;?></td>-->
                                  <td class="tablaBordesPrincipal"><?php echo $itemed->cantidad;?></td>
                                  <td class="tablaBordesPrincipal" onclick="editarp(<?php echo $itemed->id;?>,'<?php echo $columna; ?>','<?php echo $preciovalor;?>')">
                                    <?php 
                                      if($itemed->precio_local!=null){
                                        echo '$ '.number_format($itemed->precio_local,2,'.',',').'';
                                        $total=$itemed->precio_local*$itemed->cantidad;
                                      }
                                      if($itemed->precio_semi!=null){
                                        echo '$ '.number_format($itemed->precio_semi,2,'.',',').'';
                                        $total=$itemed->precio_semi*$itemed->cantidad;
                                      }
                                      if($itemed->precio_foraneo!=null){
                                        echo '$ '.number_format($itemed->precio_foraneo,2,'.',',').'';
                                        $total=$itemed->precio_foraneo*$itemed->cantidad;
                                      }
                                      if($itemed->precio_especial!=null){
                                        echo '$ '.number_format($itemed->precio_especial,2,'.',',').'';
                                        $total=$itemed->precio_especial*$itemed->cantidad;
                                      }
                                      $GLOBALS["total"]=$GLOBALS["total"]+$total;
                                    ?></td>
                                    <?php if ($datosCotizacion[0]->propuesta==1) { ?>
                                    <!--<td class="tablaBordesPrincipal">
                                      $ <?php echo number_format($total*0.16,2,'.',',');?>
                                    </td>-->
                                    <td class="tablaBordesPrincipal">
                                      $ <?php echo number_format($itemed->cantidad*$total,2,'.',',');?> 
                                    </td>
                                    <?php }  ?>

                              </tr>
                            <?php }  ?>
                            
                          </tbody>
                        </table>
                        
                      </td>
                    </tr>
                <?php }  ?>
                    
              </table> 
</div>
<div id="modaleditor" class="modal">
  <div class="modal-content ">
    <h4>Editar</h4>
    <div class="border_datos" id="fiscales_dato1">
      <div class="col s24 m24 l12">
        <div class="row ">
            <div class="row">
              <input id="idpoliza" type="hidden" value="">
              <input id="polizacol" type="hidden" value="">
              <div class="input-field col s12">
                <input id="password" type="password" class="validate" autocomplete="new-password" required>
                <label for="password">Password</label>
              </div>
            </div>
            <div class="row">
              <div class="input-field col s12">
                <textarea class="materialize-textarea" id="valor"></textarea>
              </div>
            </div>
                                        

        </div>                          
      </div>
    </div>                    
  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
    <a href="#!" class="modal-action modal-close waves-effect waves-green green btn-flat editarvalor">Aceptar</a>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function($) {
    $('.editarvalor').click(function(event) {
        var idCliente=$('#idCliente').val();
        var idpoliza = $('#idpoliza').val();
        var polizacol = $('#polizacol').val();
        var pass = $('#password').val();
        var valor = $('#valor').val();
        if (pass!='') {
            //if (valor!='') {
                $.ajax({
                    type:'POST',
                    url: base_url+'Cotizaciones/editarpoliza',
                    data: {
                        idpoliza:idpoliza,
                        polizacol:polizacol,
                        pass:pass,
                        valor:valor,
                        idCliente:idCliente
                        },
                        async: false,
                        statusCode:{
                            404: function(data){
                                swal("Error", "404", "error");
                            },
                            500: function(){
                                swal("Error", "500", "error"); 
                            }
                        },
                        success:function(data){
                          if (data==1) {
                            swal("Editado!", "Precio Editado!", "success");
                            setTimeout(function(){ window.location.href=''; }, 2000);
                          }else{
                            swal("Error", "No tiene permiso", "error"); 
                          }

                        }
                    });
            //}else{
              //  swal("Error", "El Campo a editar no debe de quedar vacio", "error"); 
            //}
        }else{
            swal("Error", "Debe de ingresar una contraseña", "error"); 
        }   
    }); 
  });
  function editarp(id,columna,valor){
    if ($('#estatuscot').val()==1) {
        $('#password').val('');
        $('#modaleditor').modal();
        $('#modaleditor').modal('open');
        $('#valor').val(valor);
        $('#idpoliza').val(id);
        $('#polizacol').val(columna);
    }
    

}
  function deletepoliza(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: '¿Confirma la eliminación de la póliza de la cotización?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                
                    //if (precio>0) {
                        $.ajax({
                        type:'POST',
                        url: base_url+'Cotizaciones/deletepoliza',
                        data: {
                            id: id
                            },
                            async: false,
                            statusCode:{
                                404: function(data){
                                    swal("Error", "404", "error");
                                },
                                500: function(){
                                    swal("Error", "500", "error"); 
                                }
                            },
                            success:function(data){
                              toastr.success('Registro eliminado');
                              setTimeout(function(){ location.reload(); }, 3000);

                            }
                        });
                    
            },
            cancelar: function (){
            }
        }
    });
  }
</script>