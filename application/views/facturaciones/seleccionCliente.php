<style type="text/css">
  table td{
    font-size: 12px;
    padding: 6px 7px;
  }
</style>
<section id="content">
      <!--start container-->
        <div class="container">
          <div class="section">
            <div class="card-panel">
              <input id="base_url" type="hidden" name="base_url" value="<?php echo base_url(); ?>">
              
                <div class="row">
                    <p class="caption col s6">Seleccione al cliente al que desea facturar:</p>
                </div>

                <div class="row " >
                   <select id="idCliente" name="idCliente" class="browser-default chosen-select col s6" >
                      <option value="" disabled selected>Clientes</option>
                      <?php foreach($clientes as $cliente)
                        { 
                        ?>
                        <option value="<?php echo $cliente->id?>"><?php echo $cliente->empresa; ?></option>
                      <?php }?>
                    </select>
                </div>

                <div class="row " >
                  <div class="input-field col s12">
                    <button class="btn cyan waves-effect waves-light right" id="botonGuardar" name="botonGuardar" type="button">Siguiente
                      <i class="material-icons right">send</i>
                    </button>
                  </div>
                </div>
              
            </div> 

          </div> 
        </div>



     
    </section>