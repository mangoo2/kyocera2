            
            <!------------ TABLA DE INFORMACIÓN DE EQUIPOS ------------>
            <div class="row">
                <table class="centered bordered letraTablaImpresionEquipos tablaBordesPrincipal" id="tablaHorizontal" style="width:100%">
                  <thead>
                    <tr class="tablaBordesPrincipal">
                        <th style="font-size:10px;" class="tablaBordesPrincipal"><?php echo $tipoimg; ?></th>
                        <th class="tablaBordesPrincipal">Cantidad</th>
                        <th class="tablaBordesPrincipal">Modelo</th>
                        
                        <?php if($tipo==1){ ?>
                        <th class="tablaBordesPrincipal">Toner Black</th>
                        <?php } ?>
                        
                        <?php if($tipo==1){ ?>
                        <th class="tablaBordesPrincipal">Rendimiento unidad imagen</th>
                        <?php } ?>
                        <!--<th class="tablaBordesPrincipal">Garantía y Servicio</th>-->
                        <?php if($tipo==2){ ?>
                        <th class="tablaBordesPrincipal">Páginas monocromáticas incluídas</th>
                        
                        <?php } ?>
                        <?php if($tipo==1){ ?>
                        <th class="tablaBordesPrincipal">Costo x paginas monocromaticas</th>
                        <?php } ?>
                        <?php if($tipo==2){ ?>
                        <th class="tablaBordesPrincipal">Excedente</th>
                        <?php } ?>
                        <th class="tablaBordesPrincipal ocultarsiesbn">Toner CMY</th>
                        <th class="tablaBordesPrincipal ocultarsiesbn">COSTOS x página a color</th>
                        <th class="tablaBordesPrincipal" width="82px"><?php if($tipo==1){echo 'Precio';}elseif ($tipo==2){echo 'Renta Mensual';} ?></th>
                        <?php if ($datosCotizacion[0]->propuesta==0) { ?>
                          <!--<th class="tablaBordesPrincipal" width="82px">SubTotal</th>-->
                        <?php } ?>
                        <?php if ($datosCotizacion[0]->propuesta==1) { ?>
                          <!--<th class="tablaBordesPrincipal" width="82px">SubTotal</th>-->
                          <!--<th class="tablaBordesPrincipal" width="82px">Iva</th>-->
                          <th class="tablaBordesPrincipal" width="82px">Total</th>
                        <?php } ?>
                    </tr>
                  </thead>
                  <tbody >
                   <?php 
                          
                          foreach ($detallesCotizacion_equipo->result() as $item) {
                          $costo_pagina_color=$item->costo_pagina_color;
                          $GLOBALS["total"]=$GLOBALS["total"]+($item->precio*$item->cantidad);
                          if ($item->foto==null) {
                             $img=base_url().'app-assets/images/1024_kyocera_logo.png';
                          }else{
                            $img=base_url().'uploads/equipos/'.$item->foto;
                          }
                          $accesorios=$this->Cotizaciones_model->getcotizacionaccesorios($idCotizacion,$item->idEquipo);
                          $accerow=$accesorios->num_rows();
                          //$accerow=0;
                    ?>
                     <tr class="tablaBordesPrincipal" >
                        <td style="font-size:10px;" class="tablaBordesPrincipal">
                          <img src="<?php echo $img; ?>" class="onMouseOverDisabled img_equipos" style="width: 120px;" >
                          <span class="new badge red badgeaction" onclick="deleteequipov(<?php echo $item->DT_RowId;?>)" >X</span>
                        </td>
                        <td class="tablaBordesPrincipal" onclick="editarp_eq(<?php echo $item->DT_RowId;?>,<?php echo $item->cantidad ?>)"><?php echo $item->cantidad;?></td>
                        <td class="tablaBordesPrincipal"><?php echo $item->modelo;?></td>
                        <?php if($tipo==1){ ?>
                        <td class="tablaBordesPrincipal">
                          <?php $tconsu=$this->Cotizaciones_model->getcotizacionequipoconsumible($idCotizacion,$item->idEquipo,1);
                          if ($tconsu->num_rows()>0) {
                            echo '<span onclick="editarprecioconsumible('.$tconsu->row()->id.','.$tconsu->row()->costo_toner.')">'.$tconsu->row()->modelo.' $'.$tconsu->row()->costo_toner.' Rinde: '.number_format($tconsu->row()->rendimiento_toner,0,'.',',').'</span>';
                          }
                          ?>
                            
                        </td>
                        <?php } ?>
                        
                        <?php if($tipo==1){ ?>
                        <td class="tablaBordesPrincipal"><?php echo number_format($item->paginas,0,'.',',');?></td>
                        <?php } ?>
                        <!--<td class="tablaBordesPrincipal"><?php echo $item->garantia_servicio;?></td>-->
                        <?php if($tipo==2){ ?>
                        <td class="tablaBordesPrincipal"><?php echo $this->Cotizaciones_model->pagmonocromaticas($item->idEquipo);?></td>
                        
                        

                        <?php } ?>
                        <?php if($tipo==1){ ?>
                        <td class="tablaBordesPrincipal">$<?php echo number_format($item->costo_pagina_monocromatica,2,'.',',');?></td>
                        <?php } ?>
                        <?php if($tipo==2){ ?>
                        <td class="tablaBordesPrincipal"><?php echo $item->excedente;?></td>
                        <?php } ?>
                        <td class="tablaBordesPrincipal ocultarsiesbn"><?php 
                              $tconsuc=$this->Cotizaciones_model->getcotizacionequipoconsumible($idCotizacion,$item->idEquipo,2);
                              $tconsum=$this->Cotizaciones_model->getcotizacionequipoconsumible($idCotizacion,$item->idEquipo,3);
                              $tconsuy=$this->Cotizaciones_model->getcotizacionequipoconsumible($idCotizacion,$item->idEquipo,4);
                              $toner=0;
                              if ($tconsuc->num_rows()>0) {
                                 $toner=$toner+1;
                              }
                              if ($tconsum->num_rows()>0) {
                                 $toner=$toner+1;
                              }
                              if ($tconsuy->num_rows()>0) {
                                 $toner=$toner+1;
                              }
                              if ($toner==3) {
                                $tonescmy=$tconsuc->row()->costo_toner+$tconsum->row()->costo_toner+$tconsuy->row()->costo_toner;
                                echo 'KIT TONER CMY $'.number_format($tonescmy,2,'.',',').' Rinde: '.number_format($tconsum->row()->rendimiento_toner,0,'.',',');
                                $costo_pagina_color=$tonescmy/$tconsum->row()->rendimiento_toner;
                              }


                              if ($tconsuc->num_rows()>0) {
                                 //echo '<p>Toner C $'.$tconsuc->row()->costo_toner.'<p>' ;
                              }
                              if ($tconsum->num_rows()>0) {
                                 //echo '<p>Toner M $'.$tconsum->row()->costo_toner.'<p>' ;
                              }
                              if ($tconsuy->num_rows()>0) {
                                 //echo '<p>Toner Y $'.$tconsuy->row()->costo_toner.'<p>' ;
                              }
                        ?>
                        </td>
                        <td class="tablaBordesPrincipal ocultarsiesbn">$<?php echo number_format($costo_pagina_color,2,'.',',');?></td>
                        <td class="tablaBordesPrincipal contardato" onclick="editar_p_eq(<?php echo $item->DT_RowId;?>,<?php echo $item->precio;?>)">$ <?php echo number_format($item->precio,2,'.',',');?></td>
                        <?php if ($datosCotizacion[0]->propuesta==0) { ?>
                          <!--<td class="tablaBordesPrincipal">$ <?php echo number_format($item->precio*$item->cantidad,2,'.',',');?></td>-->
                        <?php } ?>
                        <?php if ($datosCotizacion[0]->propuesta==1) { ?>
                          <!--<td class="tablaBordesPrincipal">$ <?php echo number_format(($item->precio*$item->cantidad)*0.16,2,'.',',');?></td>-->
                          <td class="tablaBordesPrincipal">$ <?php echo number_format(($item->precio*$item->cantidad),2,'.',',');?></td>
                        <?php } ?>
                    </tr>
                   <?php if($accerow>0){ 
                            foreach ($accesorios->result() as $item2) {
                                $costo_acc=0;
                                if($item2->cequ_costo>0){
                                    $costo_acc=$item2->cequ_costo;
                                }else{
                                    $costo_acc=$item2->costo;
                                }
                              $GLOBALS["total"]=$GLOBALS["total"]+($item2->cantidad*$item2->costo);
                            ?>
                            <tr >
                                <td></td>
                                <td onclick="editarp_acc(<?php echo $item2->id_accesoriod;?>,<?php echo $item2->cantidad;?>)"><?php echo $item2->cantidad;?></td>
                                <td  colspan="8" style="padding: 0px;">
                                  <table>
                                    <tr>
                                      <td class="tablaBordesPrincipal"></td>
                                      <td class="tablaBordesPrincipal" width="10%"style="font-size: 10px;"><?php echo $item2->nombre;?></td>
                                      <td class="tablaBordesPrincipal" width="82px" onclick="editar_p_eq_acc(<?php echo $item2->id_accesoriod;?>,<?php echo $costo_acc;?>)" >$ <?php echo number_format($costo_acc,2,'.',',');?></td>
                                      <?php if ($datosCotizacion[0]->propuesta==1) { ?>
                                      <td class="tablaBordesPrincipal" width="82px">$ <?php echo number_format($costo_acc,2,'.',',');?></td>
                                      <td class="tablaBordesPrincipal" width="82px">$ <?php echo number_format(($costo_acc)*0.16,2,'.',',');?></td>
                                      <td class="tablaBordesPrincipal" width="82px">$ <?php echo number_format($costo_acc+($costo_acc*0.16),2,'.',',');?></td>
                                    <?php } ?>
                                    </tr>
                                  </table>
                                </td>
                                 
                            </tr>
                      <?php } ?>
                            
                    <?php }?> 
                          <tr class="tablaBordesPrincipal">
                                <td class="tablaBordesPrincipal" colspan="10" ><?php echo $item->especificaciones_tecnicas;?></td>
                                   
                            </tr>


                    <?php } ?> 
                    
                    
                  </tbody>
                </table>
                
                
                
            </div>
           



<?php foreach ($detallesCotizacion_equipo->result() as $item) { 
          if ($item->tipo==1) {
            $tipoimpresora=0;
          }else{
            $tipoimpresora=1;
          }
         
                               
          if($tipoimpresora==0){ ?>
            <script type="text/javascript">
            // <?php echo $tipoimpresora.','.$item->tipo;?>
            </script>     
            <script type="text/javascript">
                   $('.ocultarsiesbn').hide();
            </script>
    <?php }
    }      
?>
<script type="text/javascript">
  function editarprecioconsumible(idrow,precio){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Editar',
        content: 'Editar costo toner<br>'+
                 '<input type="number" placeholder="monto" id="costo_toner" class="name form-control-bmz" value="'+precio+'" required/>',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var costo_toner_new=$('#costo_toner').val();
                
                     $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Cotizaciones/editarcostotoner",
                        data: {
                            id:idrow,
                            costo_toner:costo_toner_new
                        },
                        success: function (response){
                                location.reload();
                                
                        },
                        error: function(response){
                            $.alert({
                                    boxWidth: '30%',
                                    useBootstrap: false,
                                    title: 'Error!',
                                    content: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema'});   
                             
                        }
                    });
                    
                
                
            },
            cancelar: function () 
            {
                
            }
        }
    });
  }
</script>