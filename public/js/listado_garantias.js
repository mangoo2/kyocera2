var base_url=$('#base_url').val();
var table;
$(document).ready(function($) {
	table = $('#tableg').DataTable();
	load();
});
function load() {
	var tipog = $('#tipog option:selected').val();
	//if(tipog==1){
		load_equipos();
	//}
}
function load_equipos(){
	var tipog = $('#tipog option:selected').val();
    var estatus = $('#estatus option:selected').val();
	table.destroy();
	table = $('#tableg').DataTable({
        
        search: {
                return: true
            }, 
        //stateSave: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url+"index.php/Garantias/getListadoequipos",
            type: "post",
            "data": {
                'tipog':tipog,
                'estatus':estatus
            },
        },
        "columns": [
            {"data": "idVenta"},
            {"data": "empresa"},
            {"data": "serieId",
                render:function(data,type,row){
                    var html='';
                        html=row.serie;
                    return html;
                }
            },
            {"data": "productoid",
                render:function(data,type,row){
                    var html='';
                        html=row.modelo;
                    return html;
                }
            },
            
            {"data": null,
                render:function(data,type,row){
                    var html='';
                    	html='<input type="type" class="form-control-bmz serie_instalada" id="serie_instalada" onchange="saveedit_garantia('+row.tipop+','+row.idVenta+','+row.productoid+','+row.serieId+')">';
                    return html;
                }
            },
            {"data": null,
                render:function(data,type,row){
                    var html='';
                    	html='<input type="type" class="form-control-bmz serie_retirada" id="serie_retirada" onchange="saveedit_garantia('+row.tipop+','+row.idVenta+','+row.productoid+','+row.serieId+')">';
                    return html;
                }
            },
            {"data": null,
                render:function(data,type,row){
                    var html='';
                    	html='<div class="switch">\
							    <label>\
							    \
							    <input type="checkbox" id="folio_rma" onchange="saveedit_garantia('+row.tipop+','+row.idVenta+','+row.productoid+','+row.serieId+')" checked>\
							    <span class="lever"></span>\
							    \
							    </label>';
                    return html;
                }
            },
            {"data": null,
                render:function(data,type,row){
                    var html='';
                    html+='<input type="text" class="form-control-bmz folio_rma_text" id="folio_rma_text" placeholder="Rechazado" onchange="saveedit_garantia('+row.tipop+','+row.idVenta+','+row.productoid+','+row.serieId+')">';
                    html+='<textarea  class="form-control-bmz folio_rma_rechazo" id="folio_rma_rechazo" placeholder="Motivo" onchange="saveedit_garantia('+row.tipop+','+row.idVenta+','+row.productoid+','+row.serieId+')" style="min-width:100px"></textarea>';
                    return html;
                }
            },
            {"data": null,
                render:function(data,type,row){
                    var html='';
                    html='<input type="date" class="form-control-bmz fecha_tramite" id="fecha_tramite" onchange="saveedit_garantia('+row.tipop+','+row.idVenta+','+row.productoid+','+row.serieId+')">';
                    return html;
                }
            },
            {"data": null,
                render:function(data,type,row){
                    var html='';
                    html='<input type="date" class="form-control-bmz fecha_envio" id="fecha_envio" onchange="saveedit_garantia('+row.tipop+','+row.idVenta+','+row.productoid+','+row.serieId+')">';
                    return html;
                }
            },
            {"data": null,
                render:function(data,type,row){
                    var html='';
                    html='<input type="text" class="form-control-bmz" id="notacredito" onchange="saveedit_garantia('+row.tipop+','+row.idVenta+','+row.productoid+','+row.serieId+')">';
                    return html;
                }
            }
            
        ],
        "order": [[ 0, "desc" ]],
        //"lengthMenu": [[25, 50, 100,200,400], [25, 50,100,200,400]],
        //"searching": false
        createdRow: function (row, data, index) {
        	$(row).addClass('garantiarow garantia_'+data.tipop+'_'+data.idVenta+'_'+data.productoid+'_'+data.serieId).data( "tipop", data.tipop).data( "idVenta", data.idVenta).data( "productoid",data.productoid).data( "serieId", data.serieId);
        }
        
     }).on('draw',function(){
        verificarregistrosgarantias();
    });
}
function saveedit_garantia(tipo,idventa,modelo,serie){
	var folio_rma = $('.garantia_'+tipo+'_'+idventa+'_'+modelo+'_'+serie+' #folio_rma').is(':checked')==true?1:0;
	if(folio_rma==0){
		$('.garantia_'+tipo+'_'+idventa+'_'+modelo+'_'+serie+' #fecha_tramite').prop('readonly',true).val('');
		$('.garantia_'+tipo+'_'+idventa+'_'+modelo+'_'+serie+' #fecha_envio').prop('readonly',true).val('');
		$('.garantia_'+tipo+'_'+idventa+'_'+modelo+'_'+serie+' #notacredito').prop('readonly',true).val('');
		$('.garantia_'+tipo+'_'+idventa+'_'+modelo+'_'+serie+' #folio_rma_rechazo').show('show');

		$('.garantia_'+tipo+'_'+idventa+'_'+modelo+'_'+serie+' #folio_rma_text').prop('readonly',true).val('');
	}else{
		$('.garantia_'+tipo+'_'+idventa+'_'+modelo+'_'+serie+' #folio_rma_text').prop('readonly',false);
		$('.garantia_'+tipo+'_'+idventa+'_'+modelo+'_'+serie+' #fecha_tramite').prop('readonly',false);
		$('.garantia_'+tipo+'_'+idventa+'_'+modelo+'_'+serie+' #fecha_envio').prop('readonly',false);
		$('.garantia_'+tipo+'_'+idventa+'_'+modelo+'_'+serie+' #notacredito').prop('readonly',false);
		$('.garantia_'+tipo+'_'+idventa+'_'+modelo+'_'+serie+' #folio_rma_rechazo').hide('show').val('');
	}
	var serie_instalada = $('.garantia_'+tipo+'_'+idventa+'_'+modelo+'_'+serie+' #serie_instalada').val();
	var serie_retirada = $('.garantia_'+tipo+'_'+idventa+'_'+modelo+'_'+serie+' #serie_retirada').val();
	var fecha_tramite = $('.garantia_'+tipo+'_'+idventa+'_'+modelo+'_'+serie+' #fecha_tramite').val();
	var fecha_envio = $('.garantia_'+tipo+'_'+idventa+'_'+modelo+'_'+serie+' #fecha_envio').val();
	var notacredito = $('.garantia_'+tipo+'_'+idventa+'_'+modelo+'_'+serie+' #notacredito').val();
	var folio_rma_rechazo = $('.garantia_'+tipo+'_'+idventa+'_'+modelo+'_'+serie+' #folio_rma_rechazo').val();
	var folio_rma_text = $('.garantia_'+tipo+'_'+idventa+'_'+modelo+'_'+serie+' #folio_rma_text').val();
	$.ajax({
        type:'POST',
        url: base_url+"index.php/Garantias/saveeditgarantia",
        data: {
            tipo:tipo,
            idventa:idventa,
            modeloid:modelo,
            serieid:serie,
            folio_rma:folio_rma,
			serie_instalada:serie_instalada,
			serie_retirada:serie_retirada,
			fecha_tramite:fecha_tramite,
			fecha_envio:fecha_envio,
			notacredito:notacredito,
			folio_rma_rechazo:folio_rma_rechazo,
			folio_rma_text:folio_rma_text
        },
        success:function(response){  
            

        }
    });
}
function verificarregistrosgarantias(){
	var DATAa  = [];
    $("#tableg tbody > tr").each(function(){  
    	
    		item = {};                    
        	//item ["tipop"]  = $(this).find("input[class*='facturas_checked']").data('tipop');
        	item ["tipop"]  = $(this).data('tipop');
        	item ["idVenta"]  = $(this).data('idVenta');
        	item ["productoid"]  = $(this).data('productoid');
        	item ["serieId"]  = $(this).data('serieId');

        	DATAa.push(item);
    	       
        
    });
    INFOa  = new FormData();
    aInfoa   = JSON.stringify(DATAa);
    //console.log(aInfoa);
    
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Garantias/verificarregistrosgarantias",
        data: {
            productos:aInfoa
        },
        success:function(response){  
            console.log(response);
            var array = $.parseJSON(response);
            $.each(array, function(index, item) {
            	var tipo = item.tipo;
				var idventa = item.idventa;
				var modelo = item.modeloid;
				var serie = item.serieid;
            		if(item.folio_rma==1){
            			$('.garantia_'+item.tipo+'_'+item.idventa+'_'+item.modeloid+'_'+item.serieid+' #folio_rma').prop('checked',true);
            			$('.garantia_'+tipo+'_'+idventa+'_'+modelo+'_'+serie+' #folio_rma_rechazo').hide('show');
            			$('.garantia_'+tipo+'_'+idventa+'_'+modelo+'_'+serie+' #folio_rma_text').prop('readonly',false);
            		}else{
            			$('.garantia_'+tipo+'_'+idventa+'_'+modelo+'_'+serie+' #folio_rma_text').prop('readonly',true).val('');
            			$('.garantia_'+item.tipo+'_'+item.idventa+'_'+item.modeloid+'_'+item.serieid+' #folio_rma').prop('checked',false);
            			$('.garantia_'+tipo+'_'+idventa+'_'+modelo+'_'+serie+' #fecha_tramite').prop('readonly',true);
						$('.garantia_'+tipo+'_'+idventa+'_'+modelo+'_'+serie+' #fecha_envio').prop('readonly',true);
						$('.garantia_'+tipo+'_'+idventa+'_'+modelo+'_'+serie+' #notacredito').prop('readonly',true);
						$('.garantia_'+tipo+'_'+idventa+'_'+modelo+'_'+serie+' #folio_rma_rechazo').show('show');
            		}
                	$('.garantia_'+tipo+'_'+idventa+'_'+modelo+'_'+serie+' #folio_rma_text').val(item.folio_rma_text);

                    $('.garantia_'+tipo+'_'+idventa+'_'+modelo+'_'+serie+' #serie_instalada').val(item.serie_instalada);
					$('.garantia_'+tipo+'_'+idventa+'_'+modelo+'_'+serie+' #serie_retirada').val(item.serie_retirada);
					$('.garantia_'+tipo+'_'+idventa+'_'+modelo+'_'+serie+' #fecha_tramite').val(item.fecha_tramite);
					$('.garantia_'+tipo+'_'+idventa+'_'+modelo+'_'+serie+' #fecha_envio').val(item.fecha_envio);
					$('.garantia_'+tipo+'_'+idventa+'_'+modelo+'_'+serie+' #notacredito').val(item.notacredito);
					$('.garantia_'+tipo+'_'+idventa+'_'+modelo+'_'+serie+' #folio_rma_rechazo').val(item.folio_rma_rechazo);
                    if(item.notacredito!=''){
                        $('.garantia_'+tipo+'_'+idventa+'_'+modelo+'_'+serie+' input').prop('readonly',true);
                        $('.garantia_'+tipo+'_'+idventa+'_'+modelo+'_'+serie+' #folio_rma').prop('disabled',true);
                        $('.garantia_'+tipo+'_'+idventa+'_'+modelo+'_'+serie).addClass('bloqueado');
                    }

                
            });
            

        }
    });
    		        
}