        </div>
      <!-- END WRAPPER -->
    </div>
    <!-- END MAIN -->
    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- START FOOTER -->
<!-- ================================================
    Scripts
    ================================================ -->
    
    <!--materialize js-->
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/js/materialize.min.js"></script>
    <!--scrollbar-->
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <!--plugins.js - Some Specific JS codes for Plugin Settings-->
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/js/plugins.js"></script>
    <!--custom-script.js - Add your own theme custom JS--
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/js/custom-script.js"></script>-->
    <!-- chartist -->
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/jquery-validation/jquery.validate.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/jquery-validation/additional-methods.min.js"></script>
    <!--plugins.js - Some Specific JS codes for Plugin Settings-->
    <!--<script type="text/javascript" src="<?php echo base_url(); ?>app-assets/js/plugins.js"></script>-->
    <!--form-validation.js - Page Specific JS codes-->
    <!--<script type="text/javascript" src="<?php echo base_url(); ?>app-assets/js/scripts/form-validation.js"></script>-->
    <!--sweetalert -->
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/sweetalert/dist/sweetalert.min.js"></script>
    <!--extra-components-sweetalert.js - Some Specific JS-->
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/js/scripts/extra-components-sweetalert.js"></script>
    <!--<script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/data-tables/js/jquery.dataTables.min.js"></script>-->
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/data-tables/js/jquery.dataTables.min.1.11.4.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/data-tables/js/dataTables.responsive.min.2.2.9.js"></script>
    <!-- dropify -->
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/dropify/js/dropify.min.js"></script>
    <!-- jquery confirm -->
    <script src="<?php echo base_url(); ?>app-assets/vendors/confirm/jquery-confirm.min.js"></script>
    <!-- JS Export -->
    <script src="<?php echo base_url(); ?>app-assets/vendors/exportJS/export.js"></script>
    <script src="<?php echo base_url(); ?>app-assets/vendors/exportJS/export.min.js"></script>
    <!-- Full Calendar -->
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/fullcalendar/lib/jquery-ui.custom.min.js"></script>--
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/fullcalendar/lib/moment.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/fullcalendar/js/fullcalendar.min.js"></script>
   
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/datatable-Editable/js/dataTables.editor.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/datatable-Editable/js/dataTables.editor.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/datatable-Editable/js/editor.bootstrap.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/datatable-Editable/js/editor.bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/datatable-Editable/js/editor.bootstrap4.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/datatable-Editable/js/editor.bootstrap4.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/datatable-Editable/js/editor.foundation.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/datatable-Editable/js/editor.foundation.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/datatable-Editable/js/editor.jqueryui.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/datatable-Editable/js/editor.jqueryui.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/datatable-Editable/js/editor.semanticui.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/datatable-Editable/js/editor.semanticui.min.js"></script>

    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/data-tables/js/dataTables.select.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/data-tables/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/data-tables/js/dataTables.fixedHeader.min.js"></script>
    

    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/js/jquery.jeditable.js"></script>

    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/js/chosen.jquery.js" ></script>
    <!--ckeditor js--
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/js/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/js/ckeditor/adapters/jquery.js"></script> -->  
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/data-tables/js/input.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/data-tables/js/select.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/toastr/toastr.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/plugins/toastr/toastr.css?v=1">
    <script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/loading/jquery.loading.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/plugins/loading/demo.css">
<script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/MaskFocus/MaskedPassword.js"></script>

    <!--<script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/mask/jquery.mask.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/mask/jquery.passwordify.js"></script>-->

    <script type="text/javascript" src="<?php echo base_url(); ?>public/js/servicioscal.js?v=<?php echo date('Ymd');?>"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>public/js/notificacion.js?v=<?php echo date('Ymd');?>"></script>
    <style type="text/css">
        .paginate_input,.paginate_select{width: 100px !important;}
        .paginate_input{margin: 0px;display: unset;text-align: center;}
        <?php if(isset($MenusubId)){ ?>
            ._su_m_<?php echo $MenusubId;?>{
                background: #e8e2e2db;
            }
        <?php } ?>
    </style>
  </body>
</html>
<style type="text/css">
    .menumovil{display: none;}
    @media only screen and (max-width: 600px) {
        .menumovil{background: white;display: block;bottom: 0;position: fixed;width: 100%;box-shadow: 0 2px 2px 0 rgb(0 0 0 / 14%), 0 1px 5px 0 rgb(0 0 0 / 12%), 0 3px 1px -2px rgb(0 0 0 / 20%);}
        .menumovil i{font-size: 40px;}
        .card-panel{margin-bottom: 55px;}
        .divmenucontect{text-align: center;}
    }
</style>
<div class="navbar-fixed menumovil">
    <div class="row">
        <div class="col s4 divmenucontect">
            <i class="fas fa-chevron-left" onclick="javascript:history.back()"></i>
        </div>
        <div class="col s4 divmenucontect">
            <i class="fas fa-home" onclick="$(location).attr('href','<?php echo base_url();?>')"></i>
        </div>
        <div class="col s4 divmenucontect">
            <i class="material-icons" onclick="$('.sidebar-collapse').click()">menu</i>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function($) {
        $(".mode").on("click", function () {
            $('.mode i').toggleClass("fa-moon").toggleClass("fa-lightbulb");
            // $('.mode-sun').toggleClass("show")
            $('body').toggleClass("dark-only");
            var color = $(this).attr("data-attr");
            localStorage.setItem('body', 'dark-only');
        });
        <?php 
            // igual se agrego en el footer
            $fecha = date('Y-m-d');
            $idpersonal = $this->session->userdata('idpersonal');
            $res_asi = $this->ModeloSession->verifi_asistencia($idpersonal,$fecha);
            if($res_asi->num_rows()>0){
              $registro='activo';
              
            }else{
                $registro='';
                if($idpersonal==1 || $idpersonal==17 || $idpersonal==18  || $idpersonal==9){
              ?>
                //$('body').loading({theme: 'dark',message: 'Favor de registrar entrada...'});
              <?php
                }else{
                    ?>$('body').loading({theme: 'dark',message: 'Favor de registrar entrada...'});<?php
                }
            }
        ?>
    });
</script>
<script type="text/javascript">
    /*
    var idper = $('#idper').val();
    var base_url = $('#base_url').val();
    var idperf = $('#idperf').val();
    var url_sw='<?php echo base_url();?>service-worker.js?idper='+idper+'&idperf='+idperf;
    console.log(url_sw);
        if ('serviceWorker' in navigator) {
          window.addEventListener('load', function() {
            navigator.serviceWorker.register(url_sw)
            .then(function(registration) {
              // Si es exitoso
              console.log('SW registrado correctamente');
            }, function(err) {
              // Si falla
              console.log('SW fallo', err);
            });
          });
        }
        */
    </script>
<?php include('inchatfot.php'); ?> 