var base_url = $('#base_url').val();
$(document).ready(function() {	
    // Destruye la tabla y la crea de nuevo con los datos cargados 
    table = $('#tabla_solicitudcontrato').DataTable({
        "ajax": {
            "url": base_url+"index.php/Rentas/getListadocontratosRentas"
        },
        "columns": [
            {"data": "id"},
            {"data": "empresa"},
            {"data": "modelo"},
            {"data": "area"},
            {"data": "plaza"},
            {"data": "costoporclick"},
            {"data": "excedente"},
            {"data": "tipo",
                 render:function(data,type,row)
                 {
                    if(data==1){
                        var html='Equipos';    
                    } 
                    else if(data==2){
                        var html='Consumibles';    
                    } 
                    else if(data==3){
                        var html='Refacciones';    
                    } 
                    else if(data==4){
                        var html='Pólizas';    
                    } 
                    
                    return html;
                }
            }, 
            {"data": "rentaVentaPoliza",
                 render:function(data,type,row)
                 {
                    if(data==1){
                        var html='<img src="'+base_url+'app-assets/images/kyoventa.png" style="width:100px;" >';    
                    } 
                    else if(data==2){
                        var html='<img src="'+base_url+'app-assets/images/kyorent.png" style="width:100px;" >';    
                    } 
                    else if(data==3){
                        var html='<img src="'+base_url+'app-assets/images/kyopo.png" style="width:100px;" >';    
                    } 
                    else
                    {
                        var html='N/A';
                    }
                    
                    return html;
                }
            }, 
            {"data": "rentaVentaPoliza",
                render:function(data,type,row)
                {
                    if(data==1){
                        var html='<a class="waves-effect green btn" href="'+base_url+'index.php/SolicitudesContrato/contrato/'+row.id+'"> Completar Venta &nbsp;</a>';    
                    } 
                    else {
                        var html='<a class="waves-effect cyan btn" href="'+base_url+'index.php/SolicitudesContrato/contrato/'+row.id+'">Generar contrato</a>';    
                    } 

                    return html;
                }
            }
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[25, 50, 100], [25, 50, 100]],
        "dom": 'Bfrtpl',
        // Muestra el botón de Excel para importar la tabla
        "buttons": [
            
        ],
        // Cambiamos lo principal a Español
        "language": {
            "lengthMenu": "Desplegando _MENU_ elementos por página",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
              }
        }
    });
});