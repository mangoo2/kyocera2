<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Reportes extends CI_Controller {
    public function __construct()     {
        parent::__construct();

        $this->load->model('ModeloCatalogos');
        $this->load->model('Modeloaccesorios');
        $this->load->model('Rentas_model');
        $this->load->model('ModeloDevhtml');
        
        $this->load->helper('url');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fechahoyc = date('Y-m-d');

        if($this->session->userdata('logeado')==true){
            $this->perfilid =$this->session->userdata('perfilid');
            $this->idpersonal = $this->session->userdata('idpersonal');
            
        }else{
            redirect('login'); 
        }
    }

    function index(){
            
    }
    public function rentadetalles($periodo,$contrato) {
        $idcontrato=$contrato;
        $data['idcontrato']=$contrato;
        $data['idpre']=$periodo;
        $data['Folio']=$periodo;
        $data['prefacturaId']=$periodo;
        $data['detalleperido']= $this->Rentas_model->detalleperido($periodo);
        $data['detalleconsumiblesfolios']= $this->Rentas_model->detalleconsumiblesfoliosp($contrato,$periodo);
        $data['detallecimages']= $this->Rentas_model->detalleimagesRentap($periodo);
        $data['rowcentros']=$this->Rentas_model->getobtenescentrocostosgroup($contrato);
        //===========================================
            $detalleresult=$this->ModeloCatalogos->getselectwheren('alta_rentas_prefactura',array('prefId'=>$periodo));
                $periodo='';
              foreach ($detalleresult->result() as $itemsr) {
                $data['idcondicionextra']=$itemsr->idcondicionextra;
                $periodo=$itemsr->periodo_inicial.' al '.$itemsr->periodo_final;


                $periodo_inicial_ante =date("d-m-Y",strtotime($itemsr->periodo_inicial.' -1 month'));
                //$periodo_inicial_ante =date("d-m-Y",strtotime($periodo_inicial_ante.' -1 day'));
                if (date('d',strtotime($periodo_inicial_ante))==1) {
                    $fecha2 = new DateTime($periodo_inicial_ante);
                    $fecha2->modify('last day of this month');
                    $periodo_final_ante = $fecha2->format('Y-m-d');
                }else{
                  $periodo_final_ante =date("d-m-Y",strtotime($itemsr->periodo_final.' -1 month'));  
                }
                
                //$periodo_final_ante =date("d-m-Y",strtotime($periodo_final_ante.' -1 day'));


                $periodoante=$periodo_inicial_ante.' al '.$periodo_final_ante;

                $data['descuentogclisck']=$itemsr->descuentogclisck;
                $data['descuentogscaneo']=$itemsr->descuentogscaneo;
              }
              $data['periodo']=$periodo;
              $data['periodoante']=$periodoante;
        //============================================================================================
            $contrato = $this->ModeloCatalogos->getselectwheren('contrato',array('idcontrato'=>$contrato));
            $contrato=$contrato->row();

            $idRentac=$contrato->idRenta;
            $data['foliocontrato']=$contrato->folio;
            $data['ordencompra']=$contrato->ordencompra;
            $data['oc_vigencia']=$contrato->oc_vigencia;

            $contrato = $this->ModeloCatalogos->getselectwheren('alta_rentas',array('idcontrato'=>$idcontrato));
            $contrato=$contrato->row();

            $data['tiporenta']=$contrato->tipo2;

            $data['grentadeposito']     =$contrato->monto;
            $data['grentacolor']        =$contrato->montoc;

            $data['gclicks_mono']       =$contrato->clicks_mono;
            $data['gprecio_c_e_mono']   =$contrato->precio_c_e_mono;
            $data['gclicks_color']      =$contrato->clicks_color;
            $data['gprecio_c_e_color']  =$contrato->precio_c_e_color;

            if ($contrato->tipo1==2) {
                    $data['periodoante']=$periodo;
            }

            $datosrentas = $this->ModeloCatalogos->getselectwheren('rentas',array('id'=>$idRentac));
            $datosrentas=$datosrentas->row();
            $datosrentas->idCliente;

            $datoscliente= $this->ModeloCatalogos->getselectwheren('clientes',array('id'=>$datosrentas->idCliente));
            $nombreclienter='';
            foreach ($datoscliente->result() as $itemc) {
              $nombreclienter=$itemc->empresa;
            }
            $data['nombreclienter']=$nombreclienter;

            
        //===============================================================================================
            if(isset($_GET['file'])){
                $file=$_GET['file'];
            }else{
                $file=0;
            }
            if(isset($_GET['fol'])){
                $data['gel_fol']=$_GET['fol'];
            }else{
                $data['gel_fol']=1;
            }
        if(isset($_GET['cc'])){
            if($_GET['cc']==1){
                $data['tipo_cc']=0;
            }else{
                $data['tipo_cc']=1;
            }
            $data['htmldev']=$this->ModeloDevhtml->detalleperiodo_cc($data);
            if($file==0){
                $this->load->view('Reportes/facturad2_cc_m',$data);
            }else{
                $this->load->view('Reportes/facturad2_m_xls',$data);
            }
        }else{
            $data['htmldev']=$this->ModeloDevhtml->detalleperiodo($data);

            if($file==0){
                $this->load->view('Reportes/facturad2_m',$data);    
            }else{
                $this->load->view('Reportes/facturad2_m_xls',$data);
            }
            
        }
        
    }
    function ventas(){
        $data['MenusubId']=13;
        $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,13);// 52 es el id del submenu
        if ($permiso==0) {
            redirect('Sistema');
        }
        $data['rowpersonal']=$this->ModeloCatalogos->getselectwheren('personal',array('estatus' => 1));
        $this->load->view('header');
        $this->load->view('main');
        $this->load->view('Reportes/rventas',$data);
        $this->load->view('footer');
        $this->load->view('Reportes/rventasjs');
    }
    function rventas($fechaini,$fechafin,$cliente,$vendedor,$tipovp,$tipof){
        $data['fechaini']=$fechaini;
        $data['fechafin']=$fechafin;
        $data['cliente']=$cliente;
        $data['vendedor']=$vendedor;
        $data['tipovp']=$tipovp;
        $data['tipof']=$tipof;
        $this->load->view('Reportes/rventasexport',$data);
    }
    function rventasnum($cliente){
        $fechainicial_reg = date("Y-m-d",strtotime($this->fechahoyc."- 10 month")); 
        $fechaini_reg_renta = date("Y-m-d",strtotime($this->fechahoyc."- 17 month")); 
            $numventaspendientes=0;
            $block_info='';
            //===================================ventas================================
                $queryv = $this->ModeloCatalogos->reporteventasnormal($fechainicial_reg,'',$cliente,0,0);
                foreach ($queryv->result() as $itemv) {
                    $reg_cre=date("Y-m-d",strtotime($itemv->reg." + ".$itemv->credito." days"));
                    //log_message('error',$itemv->reg.' + '.$itemv->credito.' = '.$reg_cre);
                    //log_message('error',$reg_cre.' <= '.$this->fechahoyc);
                    if($itemv->FacturasId>0){
                        $facturafolio=' Factura:'.$itemv->serie.' '.$itemv->Folio.'';
                    }else{
                        $facturafolio='';
                    }
                    if($reg_cre<=$this->fechahoyc){
                        if($itemv->activo==1 and $itemv->total_general>0){
                            if($itemv->estatus == 3){
                                /*
                                $monto = $this->ModeloCatalogos->montoventas2($itemv->id);
                                if($monto>0){
                                    $montopagado =$this->ModeloCatalogos->table_get_tipo_compra($itemv->id,0);  
                                }else{
                                    $montopagado=0;
                                }
                                
                                $saldo=$monto-$montopagado;
                                
                                if($saldo>0){
                                    $numventaspendientes++;
                                }
                                */
                            }else{
                                //log_message('error','venta:'.$itemv->id);
                                $block_info.='Venta:'.$itemv->id.' '.$facturafolio.'<a class="btn-floating green" onclick="detalle_vp('.$itemv->id.',0)"><i class="material-icons">assignment</i></a><br> ';
                                $numventaspendientes++;
                            }
                        }
                    }
                }
            //===================================================================
            //===================================ventas combinada================================
                $queryvc = $this->ModeloCatalogos->reporteventasconbinada($fechainicial_reg,'',$cliente,0,0);
                foreach ($queryvc->result() as $itemvv) {
                    $reg_cre=date("Y-m-d",strtotime($itemvv->reg." + ".$itemvv->credito." days"));
                    //log_message('error',$itemvv->reg.' + '.$itemvv->credito.' = '.$reg_cre);
                    if($itemvv->FacturasId>0){
                        $facturafolio=' Factura:'.$itemvv->serie.' '.$itemvv->Folio.'';
                    }else{
                        $facturafolio='';
                    }
                    if($reg_cre<=$this->fechahoyc){
                        if($itemvv->activo==1 and $itemvv->total_general>0){
                            if($itemvv->estatus == 3){
                                /*
                                $monto = $this->ModeloCatalogos->montocombinada2($itemvv->combinadaId);
                                if($monto>0){
                                    $montopagado =$this->ModeloCatalogos->table_get_tipo_compra($itemvv->combinadaId,1);    
                                }else{
                                    $montopagado=0;
                                }
                                
                                $saldo=$monto-$montopagado;
                               
                                if($saldo>0){
                                    $numventaspendientes++;
                                }
                                */
                            }else{
                                //log_message('error','ventac:'.$itemv->combinadaId);
                                $block_info.='Venta Combinada:<!--'.$itemvv->combinadaId.'-->'.$itemvv->equipo.' '.$facturafolio.'<a class="btn-floating green" onclick="detalle_vp('.$itemvv->combinadaId.',1)"><i class="material-icons">assignment</i></a><br> ';
                                $numventaspendientes++;
                            }
                        }
                    }
                }
            
            //======================== poliza ===========================================
                $queryv = $this->ModeloCatalogos->reporteventaspoliza($fechainicial_reg,'',$cliente,0,0);
                foreach ($queryv->result() as $itemv) {
                    $reg_cre=date("Y-m-d",strtotime($itemv->reg." + ".$itemv->credito." days"));
                    //log_message('error',$itemv->reg.' + '.$itemv->credito.' = '.$reg_cre);
                    if($itemv->FacturasId>0){
                        $facturafolio=' Factura:'.$itemv->serie.' '.$itemv->Folio.'';
                    }else{
                        $facturafolio='';
                    }
                    if($reg_cre<=$this->fechahoyc){
                        if($itemv->activo==1){
                            $monto = $this->ModeloCatalogos->montopoliza($itemv->id);
                            //log_message('error','0 Poliza:'.$itemv->id.' monto:'.$monto);
                            if($monto>0){
                                if($itemv->estatus == 3){
                                    
                                    
                                    
                                        if($monto>0){
                                            $montopagado =$this->ModeloCatalogos->table_get_tipo_compra_poliza($itemv->id); 
                                        }else{
                                            $montopagado=0;
                                        }
                                        //log_message('error','1 Poliza:'.$itemv->id.' montopagado:'.$montopagado);
                                        $saldo=round($monto,2)-round($montopagado,2);
                                        //log_message('error','1 Poliza:'.$itemv->id.' $saldo('.$saldo.')=round($monto('.round($monto).'))-round($montopagado('.round($montopagado).'));');
                                        if($saldo>0){
                                            //log_message('error','2 Poliza:'.$itemv->id.' saldo:'.$saldo);
                                            //log_message('error','poliza1:'.$itemv->id);
                                            $block_info.='poliza:'.$itemv->id.' '.$facturafolio.'<a class="btn-floating green" onclick="detalle_vp('.$itemv->id.',2)"><i class="material-icons">assignment</i></a><br> ';
                                            $numventaspendientes++;
                                        } 
                                    
                                    
                                    
                                }else{
                                    //log_message('error','3 Poliza:'.$itemv->id.' diferente de 3');
                                    //log_message('error','poliza2:'.$itemv->id);
                                    $block_info.='Poliza:'.$itemv->id.' '.$facturafolio.'<a class="btn-floating green" onclick="detalle_vp('.$itemv->id.',2)"><i class="material-icons">assignment</i></a><br> ';
                                    $numventaspendientes++;
                                }
                            }
                        }
                    }
                }
            //======================== rentas ===========================================
                $queryrp = $this->ModeloCatalogos->reporterentaspagoscomp($fechaini_reg_renta,$cliente);
                foreach ($queryrp->result() as $itemrp) {
                    $numreg=$itemrp->numreg;
                    if($itemrp->totalfactura>0){
                        $totalfactura = $itemrp->totalfactura/$numreg;
                    }else{
                        $totalfactura=0;
                    }
                    if($itemrp->totalpagoscomp>0){
                        $totalpagoscomp = round($itemrp->totalpagoscomp,2);
                    }else{
                        $totalpagoscomp=0;
                    }
                    if($itemrp->facpagodirecto>0){
                        $facpagodirecto = round($itemrp->facpagodirecto,2);
                    }else{
                        $facpagodirecto=0;
                    }
                    if($itemrp->FacturasId>0){
                        $sql="SELECT pf.idpago, pf.fecha, ff.formapago_text, pf.pago, pf.observaciones, per.nombre, per.apellido_paterno, per.apellido_materno, pf.reg  
                              FROM pagos_factura as pf  
                              INNER JOIN f_formapago as ff on ff.id=pf.idmetodo  
                              INNER JOIN personal as per on per.personalId=pf.idpersonal  
                              WHERE pf.factura='$itemrp->FacturasId' AND pf.activo=1"; 
                          $query = $this->db->query($sql); 
                          foreach ($query->result() as $item) { 
                            $facpagodirecto=$facpagodirecto+$item->pago;
                            
                          } 
                    }
                    $totalpagoscomp_directo=$totalpagoscomp+$facpagodirecto;
                    
                    if($itemrp->FacturasId>0){
                        $facturafolio=' Factura:'.$itemrp->serie.' '.$itemrp->Folio.'';
                    }else{
                        $facturafolio='';
                    }



                    if($totalfactura>0){
                        if($totalpagoscomp_directo<$totalfactura){
                            $reg_cre=date("Y-m-d",strtotime($itemrp->fechatimbre." + ".$itemrp->credito." days"));
                            if($reg_cre<=$this->fechahoyc){
                                $numventaspendientes++;
                                //log_message('error','Contrato:'.$itemrp->idcontrato.' Periodo: '.$itemrp->prefId);
                                $block_info.='Contrato:'.$itemrp->idcontrato.' Periodo: '.$itemrp->prefId.' '.$facturafolio.'<a href="'.base_url().'index.php/Configuracionrentas/ContratosPagos/'.$itemrp->idcontrato.'" target="_blank">Ir al contrato</a><br> ';
                            }
                        }
                    }else{
                        $reg_cre=date("Y-m-d",strtotime($itemrp->reg." + ".$itemrp->credito." days"));
                        if($reg_cre<=$this->fechahoyc){
                            $numventaspendientes++;
                            //log_message('error','Contrato:'.$itemrp->idcontrato.' Periodo: '.$itemrp->prefId);
                            $block_info.='Contrato:'.$itemrp->idcontrato.' Periodo: '.$itemrp->prefId.' '.$facturafolio.'<a href="'.base_url().'index.php/Configuracionrentas/ContratosPagos/'.$itemrp->idcontrato.'" target="_blank">Ir al contrato</a><br> ';
                        }
                    }
                }
            //===================================================================
                $queryfac = $this->ModeloCatalogos->obtenerfacturassinrelacion($cliente,$fechaini_reg_renta);
                foreach ($queryfac->result() as $itemfac) {
                    $reg_cre=date("Y-m-d",strtotime($itemfac->fechatimbre." + ".$itemfac->credito." days"));
                    if($reg_cre<=$this->fechahoyc){
                        $total=$itemfac->total;
                        $totalconjunto=$itemfac->pagom+$itemfac->pagoc;
                        if($total>$totalconjunto){
                            $numventaspendientes++;
                            //log_message('error','Contrato:'.$itemrp->idcontrato.' Periodo: '.$itemrp->prefId);
                            $block_info.='Factura:'.$itemfac->serie.$itemfac->Folio.' <a href="'.base_url().'index.php/Configuracionrentas/generarfacturas/'.$itemfac->FacturasId.'/1" target="_blank">Ver factura</a><br> ';
                        }
                    }
                    // code...
                }
            //======================================================================
                $info_block_row=0;
                if($numventaspendientes>0){
                    $infocli =$this->ModeloCatalogos->getselectwheren('clientes',array('id'=>$cliente,'bloqueo_fp'=>1));
                    if($infocli->num_rows()==0){
                        $this->ModeloCatalogos->updateCatalogo('clientes',array('bloqueo_fp'=>1,'bloqueo_fp_reg'=>$this->fechahoy,'bloqueo_fp_info'=>$block_info),array('id'=>$cliente));
                        $info_block_row++;
                    }
                }else{
                    $infocli =$this->ModeloCatalogos->getselectwheren('clientes',array('id'=>$cliente,'bloqueo_fp'=>0));
                    if($infocli->num_rows()==0){
                        $this->ModeloCatalogos->updateCatalogo('clientes',array('bloqueo_fp'=>0,'bloqueo_fp_reg'=>null,'bloqueo_fp_info'=>$block_info),array('id'=>$cliente));
                        $info_block_row++;
                    }
                }
                if($info_block_row==0){
                    $this->ModeloCatalogos->updateCatalogo('clientes',array('bloqueo_fp_info'=>$block_info),array('id'=>$cliente));
                }
                echo $numventaspendientes;
            
    }
    function pruebamonto($id){
        $monto = $this->ModeloCatalogos->montocombinada2($id);
        echo $monto;
    }
    public function rpdll_preview($periodo,$contrato) {
        $idcontrato=$contrato;
        $data['idcontrato']=$contrato;
        $data['Folio']=$periodo;
        $data['prefacturaId']=$periodo;
        //$data['detalleperido']= $this->Rentas_model->detalleperido($periodo);
        //$data['detalleconsumiblesfolios']= $this->Rentas_model->detalleconsumiblesfoliosp($contrato,$periodo);
        //$data['detallecimages']= $this->Rentas_model->detalleimagesRentap($periodo);
        $periodo_inicial=$_GET['p_ini'];
        $periodo_final=$_GET['p_fin'];
        $eqdetalle = $_GET['eqdetalle'];
        //===================================================
            $contrato = $this->ModeloCatalogos->getselectwheren('contrato',array('idcontrato'=>$contrato));
            $contrato=$contrato->row();
            $idRentac=$contrato->idRenta;
            $idrenta=$idRentac;
        //===========================================
            $strq_cdex = "SELECT * from rentas_condiciones WHERE renta='$idrenta' and activo=1 and fechainicial<='$this->fechahoy' ORDER BY fechainicial DESC LIMIT 1 ";
            $query_cdex = $this->db->query($strq_cdex);
            $idcondicionextra=0;
            foreach ($query_cdex->result() as $item_cdex) {
                $idcondicionextra=$item_cdex->id;
            }
            $data['idcondicionextra']=$idcondicionextra;
        //====================================================
            $periodo=$periodo_inicial.' al '.$periodo_final;

                $periodo_inicial_ante =date("d-m-Y",strtotime($periodo_inicial.' -1 month'));
                if (date('d',strtotime($periodo_inicial_ante))==1) {
                    $fecha2 = new DateTime($periodo_inicial_ante);
                    $fecha2->modify('last day of this month');
                    $periodo_final_ante = $fecha2->format('Y-m-d');
                }else{
                  $periodo_final_ante =date("d-m-Y",strtotime($periodo_final.' -1 month'));  
                }
                
                $periodoante=$periodo_inicial_ante.' al '.$periodo_final_ante;

                $periodoante=$periodo_inicial_ante.' al '.$periodo_final_ante;

                $data['periodo']=$periodo;
                $data['periodoante']=$periodoante;
        //=====================================================
                $data['descuentogclisck']=$contrato->descuento_g_clicks;
                $data['descuentogscaneo']=$contrato->descuento_g_scaneo;
                $data['foliocontrato']=$contrato->folio;   

                $contrato = $this->ModeloCatalogos->getselectwheren('alta_rentas',array('idcontrato'=>$idcontrato));
                $contrato=$contrato->row();
                $id_renta=$contrato->id_renta;

                $data['tiporenta']=$contrato->tipo2;

                $data['grentadeposito']     =$contrato->monto;
                $data['grentacolor']        =$contrato->montoc;

                $data['gclicks_mono']       =$contrato->clicks_mono;
                $data['gprecio_c_e_mono']   =$contrato->precio_c_e_mono;
                $data['gclicks_color']      =$contrato->clicks_color;
                $data['gprecio_c_e_color']  =$contrato->precio_c_e_color;

                if ($contrato->tipo1==2) {
                        $data['periodoante']=$periodo;
                }

                $datosrentas = $this->ModeloCatalogos->getselectwheren('rentas',array('id'=>$idRentac));
                $datosrentas=$datosrentas->row();
                $datosrentas->idCliente;

                $datoscliente= $this->ModeloCatalogos->getselectwheren('clientes',array('id'=>$datosrentas->idCliente));
                $nombreclienter='';
                foreach ($datoscliente->result() as $itemc) {
                  $nombreclienter=$itemc->empresa;
                }
                $data['nombreclienter']=$nombreclienter;
                $data['equiposrenta'] = $this->Rentas_model->equiposrentas($idcontrato);

                $strq_eq_ren="SELECT rdeq.modelo,ser.serie, are.* 
                          FROM alta_rentas_equipos as are 
                          INNER JOIN rentas_has_detallesEquipos as rdeq on rdeq.id=are.id_equipo 
                          INNER JOIN series_productos as ser on ser.serieId=are.serieId 
                          WHERE are.id_renta='$id_renta'";
                $query_eq_ren = $this->db->query($strq_eq_ren);
                $data['query_eq_ren']  = $query_eq_ren->result();
                //$eqdetalle=json_encode($eqdetalle);
                $eqdetalle=json_decode($eqdetalle);

                $data['arrayconceptos']=$eqdetalle;

        $this->load->view('Reportes/facturad2_preview',$data);
    }


    
}
?>
