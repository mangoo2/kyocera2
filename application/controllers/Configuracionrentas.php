<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Configuracionrentas extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->model('ModeloCatalogos');
        $this->load->model('Rentas_model');
        $this->load->model('ModeloAsignacion');
        $this->load->model('ModeloGeneral');
        $this->load->model('Modelofacturas');
        $this->load->model('Configuraciones_model');
        $this->load->helper('url');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d');
        $this->fechahoyc = date('Y-m-d H:i:s');
        $this->fechahoyL = date('Y-m-d_H_i_s');

        $this->version = date('YmdHi');
        
        $this->idpersonal = $this->session->userdata('idpersonal');
        $fecha = new DateTime();
        $fecha->modify('first day of this month');
        $this->fechainicio = $fecha->format('Y-m-d');
        $fecha2 = new DateTime();
        $fecha2->modify('last day of this month');
        $this->fechafin = $fecha2->format('Y-m-d');
        if($this->session->userdata('logeado')==true){
            $this->perfilid =$this->session->userdata('perfilid');
            $this->idpersonal = $this->session->userdata('idpersonal');
        }else{
            redirect('login');
        }
        $this->trunquearredondear=0;//0 redondeo 1 trunquear (dos dijitos)

    }

    //no subir todo el proyecto, si hay cambios solo subir la linea modificada

    function index(){
        $data['perfilid']=$this->perfilid;
        $data['idpersonal']=$this->idpersonal;
        $data['ejecutivos']=$this->ModeloCatalogos->ejecutivos();
        $data['clientes'] = $this->Rentas_model->clientesrenta();
        $this->ModeloCatalogos->updateCatalogo('alta_rentas',array('continuarcontadores'=>0,'conti_cont_fecha'=>null),array('conti_cont_fecha <='=>$this->fechahoy));
        $data['cargafiles']=1;
        $this->load->view('header');
        $this->load->view('main',$data);
        $this->load->view('rentas/configuracion');
        $this->load->view('footerl'); 
        $this->load->view('rentas/jsconfiguracion'); 
    }
    public function contratorentaclientec(){   
      $idcliente = $this->input->post('id');
        $html = '';
        $html.='<div class="col s12">
                    <table id="tabla_contratos" class="responsive-table display" cellspacing="0">
                      <thead>
                        <tr>
                          <th>Contrato</th>
                          <th>Equipo</th>
                          <th>Serie</th> 
                          <th>Fecha de solicitud</th>
                          <th>Vigencia</th>
                          <th>Folio contrato</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>';
                $contrato = $this->Rentas_model->contratorentaclientex($idcliente);
                foreach ($contrato as $item) {
                    $base_url=base_url();
                    if($item->vigencia==1){
                        $vigenciasuma = "+12 month";
                    }else if($item->vigencia==2){
                        $vigenciasuma = "+24 month";
                    }else if($item->vigencia==1){
                        $vigenciasuma = "+36 month";
                    }else{
                      $vigenciasuma = "+0 month";
                    } 
                    $messuma = date("d-m-Y",strtotime($item->fechainicio.$vigenciasuma)); 
                    $html.='<tr>
                              <td>'.$item->idcontrato.'</td> 
                              <td>'.$item->modelo.'</td>
                              <td>'.$item->serie.'</td> 
                              <td>'.date("d-m-Y",strtotime($item->fechainicio)).'</td>
                              <td>'.$messuma.'</td>
                              <td>'.$item->folio.'</td>
                              <td><div class="form-group">
                                  
                                  <a  class="btn btn-raised round btn-min-width mr-1 mb-1" style="background-color: green" href="'.$base_url.'Configuracionrentas/addcontadores/'.$item->id_equipo.'/'.$item->serieId.'">Agregar</a>
                                  </div>
                              </td>
                            </tr>';
                }
              $html.='</tbody>
                    </table>
                  </div>';
        echo $html;
    }
    public function contratorentacliente(){   
        $idcliente = $this->input->post('id');
        $ideje = $this->input->post('ideje');
        $estatus = $this->input->post('estatus');
        $html = '';
        $html.='<div class="col s12">
                    <table id="tabla_contratos" class="responsive-table display" cellspacing="0">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>vendedor</th>
                          <th>Fecha de solicitud</th>
                          <th>Vigencia</th>
                          <th>Folio contrato</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>';
                $contrato = $this->Rentas_model->contratorentaclienteeje($idcliente,$ideje,$estatus,0);
                foreach ($contrato as $item) {
                    $color_reg='';
                    if($item->estatus==2){
                      $color_reg='style="background: green;"';
                    }else{
                      $color_reg='';
                    }  
                    $base_url=base_url();
                    if($item->vigencia==1){
                        $vigenciasuma = "+12 month";
                    }else if($item->vigencia==2){
                        $vigenciasuma = "+24 month";
                    }else if($item->vigencia==3){
                        $vigenciasuma = "+36 month";
                    } else{
                      $vigenciasuma = "+0 month";
                    }
                    $messuma = date("d-m-Y",strtotime($item->fechainicio.$vigenciasuma)); 
                    $html.='<tr '.$color_reg.'>
                              <td class="contratossearch contratossearch_'.$item->idcontrato.'">'.$item->idcontrato.'</td> 
                              <td>'.$item->nombre.'</td>
                              <td>'.date("d-m-Y",strtotime($item->fechainicio)).'</td>
                              <td>'.$messuma.'</td>
                              <td>'.$item->folio.'</td>
                              <td><div class="form-group">
                                  
                                  <a  class="btn btn-raised round btn-min-width mr-1 mb-1" style="background-color: green" onclick="vercontrato('.$item->idcontrato.')">Agregar</a>
                                  </div>
                              </td>
                            </tr>';
                }
              $html.='</tbody>
                    </table>
                  </div>';
        echo $html;
    }

    public function ContratosPagos($idcontrato){
        $where_m = array('activo' =>1);   
        $data['get_f_formapago']=$this->ModeloCatalogos->getselectwheren('f_formapago',$where_m); 

        $result=$this->ModeloCatalogos->getselectwheren('personal',array('personalId' =>$this->idpersonal)); 
        foreach ($result->result() as $item) { 
           $data['nombre']=$item->nombre; 
           $data['apellido_paterno']=$item->apellido_paterno; 
           $data['apellido_materno']=$item->apellido_materno; 
        } 
        $strq_rent = "SELECT ren.idCliente,con.idRenta,con.permt_continuar FROM contrato as con INNER JOIN rentas as ren on ren.id=con.idRenta WHERE con.idcontrato='$idcontrato'";
        $query_rent = $this->db->query($strq_rent);
        $idcliente=0;
        $idRenta=0;$permt_continuar=0;
        foreach ($query_rent->result() as $item_ren) {
          $idcliente=$item_ren->idCliente;
          $idRenta=$item_ren->idRenta;
          $permt_continuar=$item_ren->permt_continuar;
        }
        $data['permt_continuar'] = $permt_continuar;
        $data['idRenta'] = $idRenta;
        $data['idcliente'] = $idcliente;
        $data['personalId'] = $this->idpersonal;
        $data['idcontrato'] = $idcontrato;
        $data['equiposrenta'] = $this->Rentas_model->equiposrentas($idcontrato);
        //$data['equiposrenta'] = $this->ModeloCatalogos->getselectwheren($table,$where);
        $data['fechainicio']=$this->fechainicio;
        $data['fechafin']=$this->fechafin;
        $data['get_bodegas']=$this->ModeloCatalogos->getselectwheren('bodegas',array('activo'=>1));
        $data['ejecutivos']=$this->ModeloCatalogos->ejecutivos();
        $data['clientes'] = $this->Rentas_model->clientesrenta();
        $data['perfilid']=$this->perfilid;
        $data['idpersonal']=$this->idpersonal;
        $data['perfirma']=$this->ModeloCatalogos->seleccionejecutivofirma();
        $data['lis_fac_prog']=$this->ModeloGeneral->listadofacturasprogramadas();
        $this->load->view('header');
        $this->load->view('main',$data);
        $this->load->view('rentas/contratospagos');
        $this->load->view('footer'); 
        $this->load->view('rentas/jsconfiguraciontabs'); 
    }

    /*
    function facturacion(){
        $this->load->view('header'); 
        $this->load->view('main');
        $this->load->view('rentas/facturacion');
        $this->load->view('rentas/facturacionjs');
        $this->load->view('footer');  
    
    }*/
    public function guardarcontratopago(){
        $data = $this->input->post();
        $tipo2=$data['tipo2'];
        $contactos=$data['contactos'];
        unset($data['contactos']);
        $equiposrow=$data['equiposrow'];
        unset($data['equiposrow']);
        $equiposclicks=$data['equiposclicks'];
        unset($data['equiposclicks']);
        $id_renta = $data['id_renta'];
        if (isset($data['fechadiferentev'])) {
          $data['fechadiferentev']=1;
        }else{
          $data['fechadiferentev']=0;
        }
        if (isset($data['fac_auto'])) {
          $data['fac_auto']=1;
        }else{
          $data['fac_auto']=0;
        }


        if($id_renta >=1){
          unset($data['id_renta']);
          $fecha_instalacion=$data['fecha_instalacion'];
          
          //=================================================
          $id_rentas_vf_array = array('id_renta' => $id_renta);
          $resultmodi=$this->ModeloCatalogos->db14_getselectwheren('alta_rentas',$id_rentas_vf_array);
          $deletecontadores=0;
          foreach ($resultmodi->result() as $itemc) {
            $fecha_instalaciono=$itemc->fecha_instalacion;
            if ($fecha_instalaciono!=$fecha_instalacion) {
              //log_message('error', 'alta_rentas: '.$id_renta.' '.$fecha_instalaciono.'  '.$fecha_instalacion);
              $deletecontadores=1;
            }
          }
          $this->ModeloCatalogos->updateCatalogo('alta_rentas',$data,$id_rentas_vf_array);
          //var_dump('update');

          if ($deletecontadores==1) {
            $this->ModeloCatalogos->updateCatalogo('alta_rentas_prefactura',array('activo'=>0),$id_rentas_vf_array);
            
            
          }
        }else{
          unset($data['id_renta']);
          $id_renta = $this->ModeloCatalogos->Insert('alta_rentas',$data);
          //var_dump('insert');die;
        } 
        //==============================================================
          $contactos = json_decode($contactos);
          for ($i=0;$i<count($contactos);$i++) { 
              $datac['id_renta'] = $id_renta;
              $arcId = $contactos[$i]->idtr;
              $datac['servicio'] = $contactos[$i]->servicio;
              $datac['nombre'] = $contactos[$i]->nombre;
              $datac['telefono'] = $contactos[$i]->telefono;
              $datac['correo'] = $contactos[$i]->correo;
              $datac['ubicacion'] = $contactos[$i]->ubicacion;
              $datac['ultima_actualizacion'] = $contactos[$i]->ultima_actualizacion;
              $datac['comentarios'] = $contactos[$i]->comentarios;           
              if($arcId == 0){
                $this->ModeloCatalogos->Insert('alta_rentas_contactos',$datac);
              }else{
                $arcIdarray = array('arcId' => $arcId);
                $this->ModeloCatalogos->updateCatalogo('alta_rentas_contactos',$datac,$arcIdarray);
              }
          }
        //==============================================================
        //==============================================================
          $dataequiposrarray=array();
          $equiposrow = json_decode($equiposrow);
          for ($i=0;$i<count($equiposrow);$i++) { 
              
              $datae['id_renta'] = $id_renta;
              $id_are = $equiposrow[$i]->areid;
              $datae['id_equipo'] = $equiposrow[$i]->id_equipo;
              $datae['orden'] = $equiposrow[$i]->orden;
              $datae['serieId'] = $equiposrow[$i]->serieId;
              $datae['ubicacion'] = $equiposrow[$i]->ubicacion;
              $datae['nuevo'] = $equiposrow[$i]->nuevo;
              $datae['arranque'] = $equiposrow[$i]->arranque;
              $datae['stock_mensual'] = $equiposrow[$i]->stock_mensual;
              $datae['contador_inicial'] = $equiposrow[$i]->contador_inicial;
              $datae['sol_consu_cant'] = $equiposrow[$i]->sol_consu_cant;
              $datae['sol_consu_periodo'] = $equiposrow[$i]->sol_consu_periodo;
              $datae['tipo'] = $equiposrow[$i]->tipo;
              if ($equiposrow[$i]->tipo>0) {
                $datae['tipo'] = $equiposrow[$i]->tipo;
              }else{
                $datae['tipo'] = $equiposrow[$i]->tipoconfir;
              }
              $datae['contador_iniciale'] = $equiposrow[$i]->contador_iniciale;        
              if($id_are == 0){
                $dataequiposrarray[]=$datae;
                //$id_are = $this->ModeloCatalogos->Insert('alta_rentas_equipos',$datae);
              }else{
                unset($data['tipo']);
                $areidarray = array('areid' => $id_are);
                $this->ModeloCatalogos->updateCatalogo('alta_rentas_equipos',$datae,$areidarray);
              }      
          }
          if(count($dataequiposrarray)>0){
            $this->ModeloCatalogos->insert_batch('alta_rentas_equipos',$dataequiposrarray);
          }
        //==============================================================
        if ($tipo2==1) {
          $equiposclicks = json_decode($equiposclicks);
          for ($i=0;$i<count($equiposclicks);$i++) { 
              //$dataer['id_renta'] = $id_renta;
              //$dataer['id_equipo'] = $equiposclicks[$i]->id_equipo;
              //$dataer['serieId'] = $equiposclicks[$i]->serieId;
              $dataer['renta'] = $equiposclicks[$i]->renta;
              $dataer['clicks_mono'] = $equiposclicks[$i]->clicks_mono;
              $dataer['clicks_color'] = $equiposclicks[$i]->clicks_color;
              $dataer['precio_c_e_mono'] = $equiposclicks[$i]->precio_c_e_mono;
              $dataer['precio_c_e_color'] = $equiposclicks[$i]->precio_c_e_color;

              $arrayid = array('id_renta' => $id_renta,'id_equipo' => $equiposclicks[$i]->equipos,'serieId' => $equiposclicks[$i]->serie);
              $this->ModeloCatalogos->updateCatalogo('alta_rentas_equipos',$dataer,$arrayid);
          }
        }
        //echo $resutl;
    }
    function contadores(){
        $data['clientes'] = $this->Rentas_model->clientesrenta();
        $this->load->view('header');
        $this->load->view('main',$data);
        $this->load->view('rentas/configuracionc');
        $this->load->view('rentas/configuracionjsc');
        $this->load->view('footerck');  
    } 
    public function addcontadores($id,$idserie){
        $data['id'] = $id;
        $data['idserie'] = $idserie;
        $this->load->view('header');
        $this->load->view('main');
        $this->load->view('rentas/addcontadores',$data);
        $this->load->view('footer'); 
        $this->load->view('rentas/addcontadoresjs'); 
    }
    function guardarimg(){
      $datax = $this->input->post();
      //var_dump($data);die;
      $equipo=$datax['equipo'];
      $idserie=$datax['idserie']; 
      $base64Image=$datax['image'];
      $comentario=$datax['comentario']; 
      $tipo=$datax['tipo'];
      //$decoded=base64_decode($base64Image);
      //$im = imageCreateFromString($decoded);
      //$image64 = imageCreateFromString($decoded);
      //log_message('error', 'eee'.$decoded);
      //log_message('error', $decoded);
      $namefile=date('Y_m_d-h_i_s');
      file_put_contents('uploads/rentas_hojasestado/'.$namefile.'.txt',$base64Image);
      
      $datos = array('id_equipo'=>$equipo,'nombre'=>$namefile.'.txt','comentario'=>$comentario ,'tipo'=>$tipo, 'serieId'=>$idserie);
      $this->ModeloCatalogos->Insert('alta_rentas_captura',$datos);
      //echo $base64Image;
    }
    /*
    function rentasequipos(){
        $datos = $this->input->post();
        $datos2 = $datos['datos2'];
        $datos3 = $datos['datos3'];
        $DATA = json_decode($datos2);
        $DATA3 = json_decode($datos3);
        for ($i=0;$i<count($DATA);$i++) { 
            $data['id_renta'] = $DATA[$i]->id_renta;
            $data['id_equipo'] = $DATA[$i]->id_equipo;
            $data['serieId'] = $DATA[$i]->serieId;
            $data['ubicacion'] = $DATA[$i]->ubicacion;
            $data['nuevo'] = $DATA[$i]->nuevo;
            $data['arranque'] = $DATA[$i]->arranque;
            $data['stock_mensual'] = $DATA[$i]->stock_mensual;
            $data['contador_inicial'] = $DATA[$i]->contador_inicial; 

            $id_are = $this->ModeloCatalogos->Insert('alta_rentas_equipos',$data);
            $arrayid = array('areid' => $id_are, );
            for ($i=0;$i<count($DATA3);$i++) {
              $data3['id_renta'] = $DATA[$i]->id_renta;
              $data3['id_equipo'] = $DATA[$i]->id_equipo;
              $data3['serieId'] = $DATA[$i]->serieId;
              $data3['ubicacion'] = $DATA[$i]->ubicacion;
              $data3['nuevo'] = $DATA[$i]->nuevo;

              $this->ModeloCatalogos->updateCatalogo('alta_rentas_equipos',$data3,$arrayid);
            }
        }    
    }*/
    public function gethojaestado() {  
       $idcontrato = $this->input->post('idcontrato');
       $fechainicio = $this->input->post('fechainicio');
       $fechafin = $this->input->post('fechafin');
       $tipo = $this->input->post('tipo');
       $hojasestado = $this->Rentas_model->hojasestado($idcontrato,$fechainicio,$fechafin,$tipo); 
       $html ='';
       $html.='<div class="mostrar_hojaestado2">
                <div class="row">
                  <div class="col s12">
                    <table id="tabla_estadorefaccion" class="responsive-table" cellspacing="0">
                      <thead>
                        <tr>
                          <th></th>
                          <th>Modelo</th>
                          <th>Foto</th>
                          <th>Cometarios</th>
                          <th>Fecha</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>';
                      foreach ($hojasestado as $item){
                      $fh = fopen(base_url()."uploads/rentas_hojasestado/".$item->nombre, 'r') or die("Se produjo un error al abrir el archivo");
                            $linea = fgets($fh);
                            fclose($fh);
                      $html.='<tr>
                          <td>'.$item->id_equipo.'</td>
                          <td>'.$item->modelo.'</td>';
                          if($item->tipoimg==0){ 
                            //onclick="detalle('.$item->idestado.')"
                            $html.='<td>
                              <a style="cursor: pointer !important;" ><img class="materialboxed" style="width: 100px;" src="'.$linea.'"><br></a></td>';
                          }else{
                            //onclick="detalle('.$item->idestado.')"
                            $html.='<td><a style="cursor: pointer !important;" ><img class="materialboxed" style="width: 100px;" src="'.base_url().'uploads/rentas_hojasestado/'.$item->nombre.'"></a></td>'; 
                          }
                          $html.='<td>'.$item->comentario.'</td>
                          <td>'.$item->reg.'</td>
                          <td><a class="btn darken-4" style="background-color: green" onclick="imgeditar('.$item->idestado.','.$tipo.')">Editar</a></th>
                        </tr>'; 
                      } 
                    $html.='</tbody>
                    </table>
                  </div> 
                </div>
              </div>';

       echo $html;
    }
    public function cargafiles(){
        $id_estado = $this->input->post('idestado');
        $idesta = array('idestado' => $id_estado);
        $this->ModeloCatalogos->updateCatalogo('alta_rentas_captura',array('status' => 0),$idesta);
        $captura = $this->ModeloCatalogos->getselectwherenall('alta_rentas_captura',$idesta);
        foreach ($captura as $item) {
          $id_equipo = $item->id_equipo;
          $serieId = $item->serieId;
          $comentario = $item->comentario;
          $reg = $item->reg;
          $tipo = $item->tipo;
        }
        $upload_folder ='uploads/rentas_hojasestado';
        $nombre_archivo = $_FILES['foto']['name'];
        $tipo_archivo = $_FILES['foto']['type'];
        $tamano_archivo = $_FILES['foto']['size'];
        $tmp_archivo = $_FILES['foto']['tmp_name'];
        //$archivador = $upload_folder . '/' . $nombre_archivo;
        $fecha=date('ymd-His');
        $newfile='r_-'.$fecha;        
        $archivador = $upload_folder . '/'.$newfile;
        if (!move_uploaded_file($tmp_archivo, $archivador)) {
            $return = Array('ok' => FALSE, 'msg' => 'Ocurrió un error al subir el archivo. No pudo guardarse.', 'status' => 'error');
        }else{
            $arrayfoto = array('nombre' => $newfile,'id_equipo'=> $id_equipo,'serieId'=> $serieId,'comentario'=> $comentario,'reg'=> $reg,'tipo'=> $tipo,'tipoimg'=>1);
            $this->ModeloCatalogos->Insert('alta_rentas_captura',$arrayfoto);
            $return = Array('ok'=>TRUE);
        }
        echo json_encode($return);
    }

    function viewcontratopago(){
      $xml='';
      $idcontrato = $this->input->post('idcontrato');
      $alta_rentas = $this->ModeloCatalogos->db3_getselectwheren('alta_rentas',array('idcontrato'=>$idcontrato));
      $contrato = $this->ModeloCatalogos->db3_getselectwheren('contrato',array('idcontrato'=>$idcontrato));
      $contrato=$contrato->row();
      $filas=$alta_rentas->num_rows();
      //==============================================
        $idRentac=$contrato->idRenta;
        $datosrentas = $this->ModeloCatalogos->db3_getselectwheren('rentas',array('id'=>$idRentac));
        $datosrentas=$datosrentas->row();
        $datosrentas->idCliente;

        $datoscliente= $this->ModeloCatalogos->db3_getselectwheren('clientes',array('id'=>$datosrentas->idCliente));
        $nombreclienter='';
        foreach ($datoscliente->result() as $itemc) {
          $nombreclienter=$itemc->empresa;
        }
        
      //==============================================
        switch ($contrato->vigencia) {
          case 1:
            $meses=12;
            break;
          case 2:
            $meses=24;
            break;
          case 3:
            $meses=36;
            break;
          
          default:
            $meses=0;
            break;
        }
        $ordenc_vigencia = strtotime ( '+ '.$meses.' month', strtotime ($contrato->fechainicio) ) ;
        $ordenc_vigencia = date ( 'Y-m-d' , $ordenc_vigencia );
        if($contrato->permt_continuar==1){
          $status_vigente=1;
        }else{
          if($this->fechahoy>=$ordenc_vigencia){
            $status_vigente=0;
          }else{
            $status_vigente=1;
          }
        }
      //===========================================
      if($filas>0){
        $altarentas=$alta_rentas->row();
        $datosfac= $this->ModeloCatalogos->db3_getselectwheren('f_facturas',array('FacturasId'=>$altarentas->factura_depo));
        foreach ($datosfac->result() as $itemfac) {
          $xml=$itemfac->rutaXml;
        }
        $alta_rentas_contactos = $this->ModeloCatalogos->db3_getselectwheren('alta_rentas_contactos',array('id_renta'=>$altarentas->id_renta));
        $alta_rentas_equipos = $this->ModeloCatalogos->db3_getselectwheren('alta_rentas_equipos',array('id_renta'=>$altarentas->id_renta));
        //log_message('error', 'id_rentaxxxxxxxxxxx_'.$altarentas->id_renta);


        $altarentastipo2=$altarentas->tipo2;
        $altarentasclicks_mono=$altarentas->clicks_mono;
        $altarentasclicks_color=$altarentas->clicks_color;
        $altarentasprecio_c_e_mono=$altarentas->precio_c_e_mono;
        $contratoprecio_c_e_color=$contrato->precio_c_e_color;
        $contratorentadeposito=$contrato->rentadeposito;
        $altarentasmontoc=$altarentas->montoc;

        $descuento_g_clicks=$contrato->descuento_g_clicks;
        $descuento_g_scaneo=$contrato->descuento_g_scaneo;

        $strq = "SELECT * from rentas_condiciones WHERE renta='$idRentac' and activo=1 and fechainicial<='$this->fechahoy' ORDER BY fechainicial DESC LIMIT 1 ";
        $condiciones_g = $this->db->query($strq);

        $idcondicionextra=0;
        foreach ($condiciones_g->result() as $itemcg) {
            $idcondicionextra=$itemcg->id;
            $altarentastipo2=$itemcg->tipo;
            $altarentasclicks_mono=$itemcg->click_m;
            $altarentasclicks_color=$itemcg->click_c;
            $altarentasprecio_c_e_mono=$itemcg->excedente_m;
            $contratoprecio_c_e_color=$itemcg->excedente_c;
            $contratorentadeposito=$itemcg->renta_m;
            $altarentasmontoc=$itemcg->renta_c;
        }
        $condiciones_e = $this->ModeloCatalogos->getselectwheren('rentas_condiciones_detalle',array('idc'=>$idcondicionextra));


        $filasrow= array('filas' => $filas,
                        'id_renta'=>$altarentas->id_renta,
                        'idcontrato'=>$altarentas->idcontrato,
                        'tipo1'=>$altarentas->tipo1,
                        'tipo2'=>$altarentastipo2,
                        'tipo3'=>$altarentas->tipo3,
                        'tipo4'=>$altarentas->tipo4,
                        'clicks_mono'=>$altarentasclicks_mono,
                        'clicks_color'=>$altarentasclicks_color,
                        'precio_c_e_mono'=>$altarentasprecio_c_e_mono,
                        'precio_c_e_color'=>$contratoprecio_c_e_color,
                        'ordenc_numero'=>$contrato->ordencompra,
                        'ordenc_vigencia'=>$ordenc_vigencia,
                        'monto'=>$altarentas->monto,
                        'saldo'=>$altarentas->saldo,
                        'fecha_instalacion'=>$altarentas->fecha_instalacion,
                        'periodo_ini'=>$altarentas->periodo_ini,
                        'perioro_fin'=>$altarentas->perioro_fin,
                        'servicio_horario'=>$altarentas->servicio_horario,
                        'servicio_equipo'=>$altarentas->servicio_equipo,
                        'servicio_doc'=>$altarentas->servicio_doc,
                        'compras_portal'=>$altarentas->compras_portal,
                        'compras_usuario'=>$altarentas->compras_usuario,
                        'compras_contra'=>$altarentas->compras_contra,
                        'cobranza'=>$altarentas->cobranza,
                        'descrip_fac'=>$altarentas->descrip_fac,
                        'personalId'=>$altarentas->personalId,
                        'activo'=>$altarentas->activo,
                        'contactos'=>$alta_rentas_contactos->result(),
                        'equipos'=>$alta_rentas_equipos->result(),
                        'rentadeposito'=>$contratorentadeposito,
                        'rentacolor'=>$altarentasmontoc,
                        'estatus'=>$contrato->estatus,
                        'fechadiferentev'=>$altarentas->fechadiferentev,
                        'fechadiferente'=>$altarentas->fechadiferente,
                        'nombrecliente'=>$nombreclienter,
                        'folio'=>$contrato->folio,
                        'idCliente'=>$datosrentas->idCliente,
                        'facturacionlibre'=>$contrato->facturacionlibre,
                        'continuarcontadores'=>$altarentas->continuarcontadores,
                        'idcondicionextra'=>$idcondicionextra,
                        'equipos_conextra'=>$condiciones_e->result(),
                        'descuento_g_clicks'=>$descuento_g_clicks,
                        'descuento_g_scaneo'=>$descuento_g_scaneo,
                        'factura_depo'=>$altarentas->factura_depo,
                        'factura_xml'=>$xml,
                        'oc_vigencia'=>$contrato->oc_vigencia,
                        'info_factura'=>$contrato->info_factura,
                        'fac_auto'=>$altarentas->fac_auto,
                        'status_vigente'=>$status_vigente
                        );
      }else{

        


        $filasrow= array('filas' => 0,
                        'rentadeposito'=>$contrato->rentadeposito,
                        'rentacolor'=>$contrato->rentacolor,
                        'fecha_instalacion'=>$contrato->fechainicio,
                        'vigencia'=>$contrato->vigencia,
                        'tipo1'=>$contrato->rentaadelantada,
                        'tipo2'=>$contrato->tiporenta,
                        'tipo3'=>$contrato->tipo3,
                        'clicks_mono'=>$contrato->clicks_mono,
                        'clicks_color'=>$contrato->clicks_color,
                        'precio_c_e_mono'=>$contrato->precio_c_e_mono,
                        'precio_c_e_color'=>$contrato->precio_c_e_color,
                        'ordenc_vigencia'=>$ordenc_vigencia,
                        'servicio_horario'=>$contrato->servicio_horario,
                        'servicio_equipo'=>$contrato->servicio_equipo,
                        'servicio_doc'=>$contrato->servicio_doc,
                        'estatus'=>$contrato->estatus,
                        'ordenc_numero'=>$contrato->ordencompra,
                        'nombrecliente'=>$nombreclienter,
                        'folio'=>$contrato->folio,
                        'idcontrato'=>$contrato->idcontrato,
                        'facturacionlibre'=>$contrato->facturacionlibre,
                        'oc_vigencia'=>$contrato->oc_vigencia,
                        'info_factura'=>$contrato->info_factura,
                        'status_vigente'=>$status_vigente
                        
                        );
      }
      echo json_encode($filasrow);
    }

    function datosToner(){
        $params =$this->input->post();
        $num_toner_cons = $this->Rentas_model->getRentasPrefDet($params);
        $dato=0;
        foreach ($num_toner_cons as $item) {
          $dato=$item->toner_consumido;
        }
        /*$datos=array(
          'toners_cant'=>$num_toner_cons->result()
        );*/
        echo $dato;
    }
    function datosprefactura(){
        $idrenta =$this->input->post('idrenta');
        $prefId =$this->input->post('prefId');
        $idcondicionextra=0;
        $altarentas=$this->ModeloCatalogos->getselectwheren('alta_rentas',array('id_renta'=>$idrenta));
        $arp=$this->ModeloCatalogos->getalta_rentas_prefactura($prefId);
        $numrow = $arp->num_rows();
        //log_message('error', 'numrow: '.$numrow);
        if ($numrow==0) {
            $statusnuevo=0;
            $alta_rentas_equipos = $this->ModeloCatalogos->db14_getselectwheren('alta_rentas_equipos',array('id_renta'=>$idrenta));
            $alta_rentas_equiposanteriores=array();
            foreach ($altarentas->result() as $itemr) {
                $idcontrato= $itemr->idcontrato;
                //log_message('error', 'id_contrato'.$idcontrato);
                //===========================================================
                if ($itemr->fechadiferentev==1) {
                  $periodoinicial=$itemr->fechadiferente;
                }else{
                  $periodoinicial=$itemr->fecha_instalacion;
                }
                
                /*
                $periodofinal = strtotime ( '+ 1 month', strtotime ( $periodoinicial ) ) ;
                $periodofinal = date ( 'Y-m-d' , $periodofinal );
                
                $periodofinal = strtotime ( '-1 day', strtotime ( $periodofinal ) ) ;
                $periodofinal = date ( 'Y-m-d' , $periodofinal );
                  */
                  $diaini = date ( 'd' , strtotime($periodoinicial));
                  if ($diaini==1) {
                    $periodofinal = new DateTime($periodoinicial);
                    $periodofinal->modify('last day of this month');
                    $periodofinal = $periodofinal->format('Y-m-d');
                  }else{
                    $periodofinal = strtotime ( '+ 1 month', strtotime ( $periodoinicial ) ) ;
                    $periodofinal = date ( 'Y-m-d' , $periodofinal );
                    
                    $periodofinal = strtotime ( '-1 day', strtotime ( $periodofinal ) ) ;
                    $periodofinal = date ( 'Y-m-d' , $periodofinal );
                  }
                  
                //==============================================================
                  $periodoiniciale = strtotime ( '- 1 month', strtotime ( $periodoinicial ) ) ;
                  $periodoiniciale = date ( 'Y-m-d' , $periodoiniciale);

                  //$periodoinicialef = strtotime ( '+ 1 month', strtotime ( $periodofinal ) ) ;
                  //$periodoinicialef = date ( 'Y-m-d' , $periodoinicialef);
                  $diainie = date ( 'd' , strtotime($periodoiniciale));
                  if ($diainie==1) {
                    $periodoinicialef = new DateTime($periodoiniciale);
                    $periodoinicialef->modify('last day of this month');
                    $periodoinicialef = $periodoinicialef->format('Y-m-d');
                  }else{
                    $periodoinicialef = strtotime ( '- 1 month', strtotime ( $periodofinal ) ) ;
                    $periodoinicialef = date ( 'Y-m-d' , $periodoinicialef);

                    $periodoinicialef = strtotime ( '-1 day', strtotime ( $periodoinicialef ) ) ;
                    $periodoinicialef = date ( 'Y-m-d' , $periodoinicialef );
                  }
                  if ($itemr->tipo1==2) {
                    $periodoiniciale=$periodoinicial;
                    $periodoinicialef=$periodofinal;
                  }
    
                //==============================================================
                  $tipo2=$itemr->tipo2;
                  $tipo4=$itemr->tipo4;
                  $clicks_mono=$itemr->clicks_mono;
                  $precio_c_e_mono=$itemr->precio_c_e_mono;

            }
        }else{
          $statusnuevo=1;
          
          foreach ($altarentas->result() as $itemr) {
                  $idcontrato= $itemr->idcontrato;
                  $tipo1=$itemr->tipo1;
                  $tipo2=$itemr->tipo2;
                  $tipo4=$itemr->tipo4;
                  $clicks_mono=$itemr->clicks_mono;
                  $precio_c_e_mono=$itemr->precio_c_e_mono;
          }
          foreach ($arp->result() as $itemr) {
            $idcondicionextra=$itemr->idcondicionextra;
            //$prefId=$itemr->prefId;
            $periodo_inicial=$itemr->periodo_inicial;
            $periodoinicial = strtotime ( '+ 1 month', strtotime ( $periodo_inicial ) ) ;
            $periodoinicial = date ( 'Y-m-d' , $periodoinicial );

            $diaini = date ( 'd' , strtotime($periodoinicial));
            if($diaini==1){
              $periodofinal = new DateTime($periodoinicial);
              $periodofinal->modify('last day of this month');
              $periodofinal = $periodofinal->format('Y-m-d');
            }else{
              $periodofinal = strtotime ( '+ 1 month', strtotime ( $periodoinicial ) ) ;
              $periodofinal = date ( 'Y-m-d' , $periodofinal );

              $periodofinal = strtotime ( '-1 day', strtotime ( $periodofinal ) ) ;
              $periodofinal = date ( 'Y-m-d' , $periodofinal );
                
            }
            

            //==============================================================
                  $periodoiniciale = strtotime ( '- 1 month', strtotime ( $periodoinicial ) ) ;
                  $periodoiniciale = date ( 'Y-m-d' , $periodoiniciale);

                  //$periodoinicialef = strtotime ( '+ 1 month', strtotime ( $periodofinal ) ) ;
                  //$periodoinicialef = date ( 'Y-m-d' , $periodoinicialef);
                  $diainie = date ( 'd' , strtotime($periodoiniciale));
                  if ($diainie==1) {
                    $periodoinicialef = new DateTime($periodoiniciale);
                    $periodoinicialef->modify('last day of this month');
                    $periodoinicialef = $periodoinicialef->format('Y-m-d');
                  }else{
                    $periodoinicialef = strtotime ( '- 1 month', strtotime ( $periodofinal ) ) ;
                    $periodoinicialef = date ( 'Y-m-d' , $periodoinicialef);

                    //$periodoinicialef = strtotime ( '-1 day', strtotime ( $periodoinicialef ) ) ;
                    //$periodoinicialef = date ( 'Y-m-d' , $periodoinicialef );
                  }
                if ($tipo1==2) {
                  $periodoiniciale=$periodoinicial;
                  $periodoinicialef=$periodofinal;
                }  
    
            //==============================================================
            $alta_rentas_equipos = $this->ModeloCatalogos->db14_getselectwheren('alta_rentas_prefactura_d',array('prefId'=>$prefId));//arthur
            $alta_rentas_equiposanteriores = $this->ModeloCatalogos->db14_getselectwheren('alta_rentas_equipos',array('id_renta'=>$idrenta));
            $alta_rentas_equiposanteriores=$alta_rentas_equiposanteriores->result();
          }
        }
        //=================== DATOS CONTRATOS ====================================
          $datoscontrato=$this->ModeloCatalogos->db14_getselectwheren('contrato',array('idcontrato'=>$idcontrato));
          foreach ($datoscontrato->result() as $itemc) {
            $rentaadelantada=$itemc->rentaadelantada;
            $rentadeposito=$itemc->rentadeposito;
            $rentacolor=$itemc->rentacolor;
          }
        //========================================================================
          /*
        $strq = "SELECT * from rentas_condiciones WHERE renta=$idRentac and activo=1 and fechainicial<='$this->fechahoy' ORDER BY fechainicial DESC LIMIT 1 ";
        $condiciones_g = $this->db->query($strq);

        $idcondicionextra=0;
        foreach ($condiciones_g->result() as $itemcg) {
          $idcondicionextra=$itemcg->id;
          if($itemcg->tipo==2){//global
            $rentadeposito=$itemcg->click_m;
            $rentacolor=$itemcg->click_c;
          }
          if($itemcg->tipo==1){//individual
            $condiciones_e = $this->ModeloCatalogos->getselectwheren('rentas_condiciones_detalle',array('idc'=>$idcondicionextra));
            $rentadeposito=0;
            $rentacolor=0;
            foreach ($condiciones_e->result() as $itemce) {
              $rentadeposito=$rentadeposito+$itemce->renta_m;
              $rentacolor=$rentacolor+$itemce->renta_c;
            }
          }
            

        }
        */
        //========================================================================

        $perriodon=$this->periodoletras($periodoinicial,$periodofinal);
        //$perriodone=$this->periodoletras($periodoiniciale,$periodoinicialef);
        $perriodone=$this->periodoletrase($periodoiniciale,$periodoinicialef);
        $datos=array(
          'statusnuevo'=>$statusnuevo,
          'periodoinicial'=>$periodoinicial,
          'periodofinal'=>$periodofinal,
          'periodon'=>$perriodon,
          'periodone'=>$perriodone,
          'tipo2'=>$tipo2,
          'tipo4'=>$tipo4,
          'clicks_mono'=>$clicks_mono,
          'precio_c_e_mono'=>$precio_c_e_mono,
          'equipos'=>$alta_rentas_equipos->result(),
          'equiposanteriores'=>$alta_rentas_equiposanteriores,
          'rentaadelantada'=>$rentaadelantada,
          'rentadeposito'=>$rentadeposito,
          'rentacolor'=>$rentacolor,
          'idcondicionextra'=>$idcondicionextra
        );
        echo json_encode($datos);
    }
    function datosprefactura0(){
        $idrenta =$this->input->post('idrenta');

        $altarentas=$this->ModeloCatalogos->getselectwheren('alta_rentas',array('id_renta'=>$idrenta));
        $arp=$this->ModeloCatalogos->getalta_rentas_prefactura0($idrenta);
        $numrow = $arp->num_rows();
        //log_message('error', 'numrow: '.$numrow);
        if ($numrow==0) {
            $statusnuevo=0;
            $alta_rentas_equipos = $this->ModeloCatalogos->db14_getselectwheren('alta_rentas_equipos',array('id_renta'=>$idrenta));
            $alta_rentas_equiposanteriores=array();
            foreach ($altarentas->result() as $itemr) {
                $idcontrato= $itemr->idcontrato;
                //log_message('error', 'id_contrato'.$idcontrato);
                //===========================================================
                if ($itemr->fechadiferentev==1) {
                  $periodoinicial=$itemr->fechadiferente;
                }else{
                  $periodoinicial=$itemr->fecha_instalacion;
                }
                
                /*
                $periodofinal = strtotime ( '+ 1 month', strtotime ( $periodoinicial ) ) ;
                $periodofinal = date ( 'Y-m-d' , $periodofinal );
                
                $periodofinal = strtotime ( '-1 day', strtotime ( $periodofinal ) ) ;
                $periodofinal = date ( 'Y-m-d' , $periodofinal );
                  */
                  $diaini = date ( 'd' , strtotime($periodoinicial));
                  if ($diaini==1) {
                    $periodofinal = new DateTime($periodoinicial);
                    $periodofinal->modify('last day of this month');
                    $periodofinal = $periodofinal->format('Y-m-d');
                  }else{
                    $periodofinal = strtotime ( '+ 1 month', strtotime ( $periodoinicial ) ) ;
                    $periodofinal = date ( 'Y-m-d' , $periodofinal );
                    
                    $periodofinal = strtotime ( '-1 day', strtotime ( $periodofinal ) ) ;
                    $periodofinal = date ( 'Y-m-d' , $periodofinal );
                  }
                  
                //==============================================================
                  $periodoiniciale = strtotime ( '- 1 month', strtotime ( $periodoinicial ) ) ;
                  $periodoiniciale = date ( 'Y-m-d' , $periodoiniciale);

                  //$periodoinicialef = strtotime ( '+ 1 month', strtotime ( $periodofinal ) ) ;
                  //$periodoinicialef = date ( 'Y-m-d' , $periodoinicialef);
                  $diainie = date ( 'd' , strtotime($periodoiniciale));
                  if ($diainie==1) {
                    $periodoinicialef = new DateTime($periodoiniciale);
                    $periodoinicialef->modify('last day of this month');
                    $periodoinicialef = $periodoinicialef->format('Y-m-d');
                  }else{
                    $periodoinicialef = strtotime ( '- 1 month', strtotime ( $periodofinal ) ) ;
                    $periodoinicialef = date ( 'Y-m-d' , $periodoinicialef);

                    $periodoinicialef = strtotime ( '-1 day', strtotime ( $periodoinicialef ) ) ;
                    $periodoinicialef = date ( 'Y-m-d' , $periodoinicialef );
                  }
                  if ($itemr->tipo1==2) {
                    $periodoiniciale=$periodoinicial;
                    $periodoinicialef=$periodofinal;
                  }
    
                //==============================================================
                  $tipo2=$itemr->tipo2;
                  $tipo4=$itemr->tipo4;
                  $clicks_mono=$itemr->clicks_mono;
                  $precio_c_e_mono=$itemr->precio_c_e_mono;

            }
        }else{
          $statusnuevo=1;
          
          foreach ($altarentas->result() as $itemr) {
                  $idcontrato= $itemr->idcontrato;
                  $tipo1=$itemr->tipo1;
                  $tipo2=$itemr->tipo2;
                  $tipo4=$itemr->tipo4;
                  $clicks_mono=$itemr->clicks_mono;
                  $precio_c_e_mono=$itemr->precio_c_e_mono;
          }
          foreach ($arp->result() as $itemr) {
            $prefId=$itemr->prefId;
            $periodo_inicial=$itemr->periodo_inicial;
            $periodoinicial = strtotime ( '+ 1 month', strtotime ( $periodo_inicial ) ) ;
            $periodoinicial = date ( 'Y-m-d' , $periodoinicial );

            $diaini = date ( 'd' , strtotime($periodoinicial));
            if($diaini==1){
              $periodofinal = new DateTime($periodoinicial);
              $periodofinal->modify('last day of this month');
              $periodofinal = $periodofinal->format('Y-m-d');
            }else{
              $periodofinal = strtotime ( '+ 1 month', strtotime ( $periodoinicial ) ) ;
              $periodofinal = date ( 'Y-m-d' , $periodofinal );

              $periodofinal = strtotime ( '-1 day', strtotime ( $periodofinal ) ) ;
              $periodofinal = date ( 'Y-m-d' , $periodofinal );
                
            }
            

            //==============================================================
                  $periodoiniciale = strtotime ( '- 1 month', strtotime ( $periodoinicial ) ) ;
                  $periodoiniciale = date ( 'Y-m-d' , $periodoiniciale);

                  //$periodoinicialef = strtotime ( '+ 1 month', strtotime ( $periodofinal ) ) ;
                  //$periodoinicialef = date ( 'Y-m-d' , $periodoinicialef);
                  $diainie = date ( 'd' , strtotime($periodoiniciale));
                  if ($diainie==1) {
                    $periodoinicialef = new DateTime($periodoiniciale);
                    $periodoinicialef->modify('last day of this month');
                    $periodoinicialef = $periodoinicialef->format('Y-m-d');
                  }else{
                    $periodoinicialef = strtotime ( '- 1 month', strtotime ( $periodofinal ) ) ;
                    $periodoinicialef = date ( 'Y-m-d' , $periodoinicialef);

                    //$periodoinicialef = strtotime ( '-1 day', strtotime ( $periodoinicialef ) ) ;
                    //$periodoinicialef = date ( 'Y-m-d' , $periodoinicialef );
                  }
                if ($tipo1==2) {
                  $periodoiniciale=$periodoinicial;
                  $periodoinicialef=$periodofinal;
                }  
    
            //==============================================================
            $alta_rentas_equipos = $this->ModeloCatalogos->db14_getselectwheren('alta_rentas_prefactura_d',array('prefId'=>$prefId));//arthur
            $alta_rentas_equiposanteriores = $this->ModeloCatalogos->db14_getselectwheren('alta_rentas_equipos',array('id_renta'=>$idrenta));
            $alta_rentas_equiposanteriores=$alta_rentas_equiposanteriores->result();
          }
        }
        //=================== DATOS CONTRATOS ====================================
          $datoscontrato=$this->ModeloCatalogos->db14_getselectwheren('contrato',array('idcontrato'=>$idcontrato));
          foreach ($datoscontrato->result() as $itemc) {
            $rentaadelantada=$itemc->rentaadelantada;
            $rentadeposito=$itemc->rentadeposito;
            $rentacolor=$itemc->rentacolor;
            $idRentac=$itemc->idRenta;
          }
        //========================================================================
        //========================================================================
        $strq = "SELECT * from rentas_condiciones WHERE renta='$idRentac' and activo=1 and fechainicial<='$this->fechahoy' ORDER BY fechainicial DESC LIMIT 1 ";
        $condiciones_g = $this->db->query($strq);

        $idcondicionextra=0;
        foreach ($condiciones_g->result() as $itemcg) {
          $idcondicionextra=$itemcg->id;
          if($itemcg->tipo==2){//global
            $rentadeposito=$itemcg->renta_m;
            $rentacolor=$itemcg->renta_c;
          }
          if($itemcg->tipo==1){//individual
            $condiciones_e = $this->ModeloCatalogos->getselectwheren('rentas_condiciones_detalle',array('idc'=>$idcondicionextra));
            $rentadeposito=0;
            $rentacolor=0;
            foreach ($condiciones_e->result() as $itemce) {
              $rentadeposito=$rentadeposito+$itemce->renta_m;
              $rentacolor=$rentacolor+$itemce->renta_c;
            }
          }
            

        }
        
        //========================================================================
        $perriodon=$this->periodoletras($periodoinicial,$periodofinal);
        //$perriodone=$this->periodoletras($periodoiniciale,$periodoinicialef);
        $perriodone=$this->periodoletrase($periodoiniciale,$periodoinicialef);
        $datos=array(
          'statusnuevo'=>$statusnuevo,
          'periodoinicial'=>$periodoinicial,
          'periodofinal'=>$periodofinal,
          'periodon'=>$perriodon,
          'periodone'=>$perriodone,
          'tipo2'=>$tipo2,
          'tipo4'=>$tipo4,
          'clicks_mono'=>$clicks_mono,
          'precio_c_e_mono'=>$precio_c_e_mono,
          'equipos'=>$alta_rentas_equipos->result(),
          'equiposanteriores'=>$alta_rentas_equiposanteriores,
          'rentaadelantada'=>$rentaadelantada,
          'rentadeposito'=>$rentadeposito,
          'rentacolor'=>$rentacolor
        );
        echo json_encode($datos);
    }
    function periodoletras($inicial,$final){
        $dia_inicial=date('d' , strtotime($inicial));
        $dia_final=date('d' , strtotime($final));
        $mes_inicial=date('m' , strtotime($inicial));
        $mes_final=date('m' , strtotime($final));
        $ano_inicial=date('Y' , strtotime($inicial));
        $ano_final=date('Y' , strtotime($final));
        $texto='RENTA DEL ';
        if($ano_inicial==$ano_final){
          if($mes_inicial==$mes_final){
            $texto.=$dia_inicial.' AL '.$dia_final.' '.$this->messpanol($mes_inicial).' DE '.$ano_final;
          }else{
            $texto.=$dia_inicial.' DE '.$this->messpanol($mes_inicial).' AL '.$dia_final.' DE '.$this->messpanol($mes_final).' DE '.$ano_final;
          }
        }else{
          $texto.=$dia_inicial.' DE '.$this->messpanol($mes_inicial).' DE '.$ano_inicial.' AL '.$dia_final.' DE '.$this->messpanol($mes_final).' DE '.$ano_final;
        }
        return $texto;
    }
    function periodoletrase($inicial,$final){
        $dia_inicial=date('d' , strtotime($inicial));
        $dia_final=date('d' , strtotime($final));
        $mes_inicial=date('m' , strtotime($inicial));
        $mes_final=date('m' , strtotime($final));
        $ano_inicial=date('Y' , strtotime($inicial));
        $ano_final=date('Y' , strtotime($final));
        $texto='EXCEDENTE DEL ';
        if($ano_inicial==$ano_final){
          if($mes_inicial==$mes_final){
            $texto.=$dia_inicial.' AL '.$dia_final.' '.$this->messpanol($mes_inicial).' DE '.$ano_final;
          }else{
            $texto.=$dia_inicial.' DE '.$this->messpanol($mes_inicial).' AL '.$dia_final.' DE '.$this->messpanol($mes_final).' DE '.$ano_final;
          }
        }else{
          $texto.=$dia_inicial.' DE '.$this->messpanol($mes_inicial).' DE '.$ano_inicial.' AL '.$dia_final.' DE '.$this->messpanol($mes_final).' DE '.$ano_final;
        }
        return $texto;
    }
    function messpanol($mes){
      switch ($mes) {
        case '01':
          $mes ="ENERO";
          break;
        case '02':
          $mes ="FEBRERO";
          break;
        case '03':
          $mes ="MARZO";
          break;
        case '04':
          $mes ="ABRIL";
          break;
        case '05':
          $mes ="MAYO";
          break;
        case '06':
          $mes ="JUNIO";
          break;
        case '07':
          $mes ="JULIO";
          break;
        case '08':
          $mes ="AGOSTO";
          break;
        case '09':
          $mes ="SEPTIEMBRE";
          break;
        case '10':
          $mes ="OCTUBRE";
          break;
        case '11':
          $mes ="NOVIEMBRE";
          break;
        case '12':
          $mes ="DICIEMBRE";
          break;
        
        default:
          $mes ="";
          break;
      }
      return $mes;
    }
    function addprefactura(){
      $data = $this->input->post();
      $equiposrow=$data['equipos'];
      unset($data['equipos']);
      $id_renta=$data['id_renta'];
      $prefId = $this->ModeloCatalogos->Insert('alta_rentas_prefactura',$data);
      $equiposrow = json_decode($equiposrow);
      $dataeqarray=array();
      for ($i=0;$i<count($equiposrow);$i++) { 
        $this->retirarequipo($equiposrow[$i]->productoId,$equiposrow[$i]->serieId);
        $dataeq['prefId']=$prefId;
        $dataeq['productoId']=$equiposrow[$i]->productoId;
        $dataeq['serieId']=$equiposrow[$i]->serieId;
        $dataeq['c_c_i']=$equiposrow[$i]->c_c_i;
        $dataeq['c_c_f']=$equiposrow[$i]->c_c_f;
        $dataeq['e_c_i']=$equiposrow[$i]->e_c_i;
        $dataeq['e_c_f']=$equiposrow[$i]->e_c_f;
        $dataeq['produccion']=$equiposrow[$i]->produccion;
        $dataeq['toner_consumido']=$equiposrow[$i]->toner_consumido;
        $dataeq['produccion_total']=$equiposrow[$i]->produccion_total;
        $dataeq['descuento']=$equiposrow[$i]->descuento;
        $dataeq['descuentoe']=$equiposrow[$i]->descuentoe;
        $dataeq['tipo']=$equiposrow[$i]->tipo;
        if ($equiposrow[$i]->tipo2==1) {
          $dataeq['excedentes']=$equiposrow[$i]->excedentes;
          $dataeq['costoexcedente']=$equiposrow[$i]->costoexcedente;
          $dataeq['totalexcedente']=$equiposrow[$i]->totalexcedente;
        }
        //$this->ModeloCatalogos->Insert('alta_rentas_prefactura_d',$dataeq);
        //================================================================
        /*
          $r_ar_eq=$this->ModeloCatalogos->getselectwheren('alta_rentas_equipos',array('id_renta'=>$id_renta,'id_equipo'=>$equiposrow[$i]->productoId,'serieId'=>$equiposrow[$i]->serieId,'tipo'=>$equiposrow[$i]->tipo));
          if($r_ar_eq->num_rows()==0){
            $data_r_ar_eq=array(
                                'id_renta'=>$id_renta,
                                'id_equipo'=>$equiposrow[$i]->productoId,
                                'serieId'=>$equiposrow[$i]->serieId,
                                'tipo'=>$equiposrow[$i]->tipo,
                                'ubicacion'=>'','promedio'=>0,'nuevo'=>0,'arranque'=>0,'stock_mensual'=>0,'contador_inicial'=>0,'contador_iniciale'=>0
                              );
            $this->ModeloCatalogos->insert_batch('alta_rentas_equipos',$data_r_ar_eq);
          }*/
        //================================================================
        $dataeqarray[]=$dataeq;
        $this->ModeloCatalogos->updateCatalogo('carga_contadores',array('pintado'=>1),array('serieId_cc'=>$equiposrow[$i]->serieId,'activo'=>1,'pintado'=>0));
      }
      if (count($equiposrow)>0) {
        $this->ModeloCatalogos->insert_batch('alta_rentas_prefactura_d',$dataeqarray);
      }
      echo $prefId;
    }
    
    function retirarequipo($idequipo,$serieid){
      $resultserie=$this->ModeloCatalogos->getselectwheren('asignacion_series_r_equipos',array('id_equipo'=>$idequipo,'serieId'=>$serieid,'activo'=>1));
      $poreliminar=0;$idmodelo=0;$modeloeq=0;
      foreach ($resultserie->result() as $items) {
        $poreliminar=$items->poreliminar;
      }
      //============================
        $strq="SELECT asca.equipostext,ascae.modelo,ascae.serie,asca.fecha,ascae.asignacionIdd,ascae.status,ascae.rowequipo,ascae.serieId,per.nombre,per.apellido_paterno,per.apellido_materno
            FROM asignacion_ser_cliente_a as asca
            INNER JOIN asignacion_ser_cliente_a_d as ascae ON ascae.asignacionId=asca.asignacionId
            INNER JOIN personal as per on per.personalId=asca.tecnico
            WHERE ascae.tpoliza=22 AND ascae.activo=1 AND asca.activo=1  AND ascae.nr=0 AND ascae.rowequipo='$idequipo' AND ascae.serieId='$serieid' and ascae.status=2
            ORDER BY `ascae`.`asignacionIdd` DESC LIMIT 1";
            $result_ser = $this->db->query($strq);
            $poreliminar=0;
            foreach ($result_ser->result() as $itemser) {
              if($itemser->status==2){
                $poreliminar=1;
              }else{
                $poreliminar=0;
              }
            }
      //============================
      if($poreliminar==1){
            $equipo=$idequipo;
            $serie=$serieid;
            $this->ModeloCatalogos->updateCatalogo('series_productos',array('status'=>0,'bodegaId'=>2,'usado'=>1),array('serieId'=>$serie));
            $this->Configuraciones_model->registrarmovimientosseries($serie,1);
            //$this->ModeloCatalogos->getdeletewheren('asignacion_series_r_equipos',array('id_equipo'=>$equipo,'serieId'=>$serie));
            $this->ModeloCatalogos->updateCatalogo('asignacion_series_r_equipos',array('activo'=>0),array('id_equipo'=>$equipo,'serieId'=>$serie));
            $this->ModeloCatalogos->updatestock('rentas_has_detallesEquipos','cantidad','-',1,'id',$equipo);
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_contrato_a_e',array('status'=>3,'motivosuspencion'=>'Eliminacion y/o Cambio'),array('serieId'=>$serie,'idequipo'=>$equipo,'fecha >='=>$this->fechahoy,'status'=>0));
            //==========================================================
                $resultac=$this->ModeloCatalogos->db13_getselectwheren('rentas_has_detallesEquipos_accesorios',array('rowequipo'=>$equipo));
                foreach ($resultac->result() as $itemac) {
                    $id_accesoriod=$itemac->id_accesoriod;//idrow
                    $id_accesorio=$itemac->id_accesorio;//modelo
                    $resultac2=$this->ModeloCatalogos->db13_getselectwheren('asignacion_series_r_accesorios',array('id_accesorio'=>$id_accesoriod));
                    foreach ($resultac2->result() as $itemara) {
                        //$itemara->serieId;
                        $this->ModeloCatalogos->updateCatalogo('series_accesorios',array('status'=>0),array('serieId'=>$itemara->serieId));
                        $this->Configuraciones_model->registrarmovimientosseries($itemara->serieId,2);
                        $this->ModeloCatalogos->Insert('bitacora_movimientos',array('entrada_salida'=>1,'descripcion_evento'=>'Entrada del accesorio por retiro por ultimo contador','personal'=>$this->idpersonal,'tipo'=>2,'id_item'=>$id_accesoriod,'modeloId'=>$id_accesorio,'modelo'=>'','serieId'=>$itemara->serieId,'bodega_d'=>0,'clienteId'=>0));
                    }
                }
                //$this->ModeloCatalogos->getdeletewheren('rentas_has_detallesEquipos_accesorios',array('rowequipo'=>$equipo)); se deja comentado y se vera si se ocupa
                //$this->ModeloCatalogos->getdeletewheren('rentas_has_detallesequipos_consumible',array('rowequipo'=>$equipo)); se deja comentado y se vera si se ocupa

            //==========================================================
                $resultve=$this->ModeloCatalogos->db13_getselectwheren('rentas_has_detallesEquipos',array('id'=>$equipo));
                foreach ($resultve->result() as $itemv) {
                    $idmodelo=$itemv->idEquipo;
                    $modeloeq=$itemv->modelo;
                    if($itemv->cantidad<=0){
                        $this->ModeloCatalogos->updateCatalogo('rentas_has_detallesEquipos',array('estatus'=>0),array('id'=>$equipo));
                    }
                }
            //==========================================================
            $this->ModeloCatalogos->Insert('bitacora_movimientos',array('entrada_salida'=>1,'descripcion_evento'=>'Entrada del equipo por retiro equipo:'.$idequipo.' serie:'.$serieid.' por ultimo contador','personal'=>$this->idpersonal,'tipo'=>1,'id_item'=>$equipo,'modeloId'=>$idmodelo,'modelo'=>$modeloeq,'serieId'=>$serieid,'bodega_d'=>2,'clienteId'=>0));
      }
    }
    
    function viewfacturas(){
      $idrenta = $this->input->post('idrenta');
      $idcontrato = $this->input->post('idcontrato');
      $results = $this->ModeloCatalogos->viewprefacturas($idrenta);
      //==============================
        $resulcc=$this->ModeloCatalogos->db13_getselectwheren('centro_costos',array('idcontrato'=>$idcontrato,'activo'=>1));
        if($resulcc->num_rows()>0){
          $centrocosto=1;
        }else{
          $centrocosto=0;
        }
      //==================================
      $html='<table id="tablefacturasall">
              <thead><tr><th>Periodo</th><th>Renta</th><th>Periodo Inicial</th><th>Periodo Final</th><th>Subtotal</th><th>Excedentes</th><th>Total</th><th>vendedor</th><th>Folio</th><th>Registro</th><th>Acciones</th></tr><thead>
              <tbody>';
              $edit=1;
              $view_delete=1;
        foreach ($results->result() as $item) {
          $btnfacturar=$this->ModeloCatalogos->viewfacturasstatusbtn($item->prefId,$idcontrato,0);
          //================================================
            $html_folio='';
            $factotal=0;
            $result_f=$this->ModeloCatalogos->viewprefacturas_folio($item->prefId);
            foreach ($result_f as $f) {
              if($f->Estado==0){
                $textred=' text-red ';
              }else{
                $textred='';
              }
              if($f->Estado==2){
                $st='<b>ST</b>';
              }else{
                $st='';
              }
              $html_folio.='<span class="lis_folio notification-button '.$textred.'" onclick="deletefactura('.$item->prefId.','.$f->facId.','.$f->Folio.')">'.$st.$f->Folio.'<small class="notification-badge colorf  accent-3 ">x</small></span> ';
              $factotal+=$f->total;
              $view_delete=0;
            }
          //================================================
          $totalg=round($item->total+($item->total*0.16), 2);
          $btnfacturar = str_replace('onclick_btn_parcial_v', ' data-total="'.$totalg.'" data-prefid="'.$item->prefId.'" data-factotal="'.$factotal.'" ', $btnfacturar);

          if ($item->statuspago==0) {
            $btncolor='btn-floating';
          }else{
            $btn='btn-floating green';
          }
          $btnedelete='<a class="waves-effect btn-bmz red" onclick="deleteprefacturax('.$item->prefId.')"><i class="fas fa-trash-alt"></i></a>';
          if ($edit==1) {
            //$btnedit='<a class="waves-effect btn-bmz blue" onclick="editarcontadores('.$item->prefId.',0)"><i class="fas fa-pencil-alt"></i></a>'; se cambio a solicitud de poder editar los equipos ya retirados
            $btnedit='<a class="b-btn b-btn-primary tooltipped" data-position="top" 
                data-delay="50" 
                data-tooltip="Editar contadores" onclick="editarcontadores('.$item->prefId.',1)"><i class="fas fa-pencil-alt"></i></a> ';
            //$btnedit='';// edicion de contadores por el momento desabilitada
            $edit=0;
          }else{
            $btnedit='<a class="waves-effect btn-bmz blue" onclick="editarcontadores_permiso('.$item->prefId.')"><i class="fas fa-pencil-alt"></i></a>';
            $edit=0;
            
          }
          if ($item->excedente==0 && $item->excedentecolor==0 && $item->periodo==null) {
            $btnexportar='<a class="b-btn b-btn-primary tooltipped sologlobales" data-position="top" 
                data-delay="50" 
                data-tooltip="Exportar Contadores" onclick="exportarcontsobra('.$item->prefId.','.$idcontrato.')" title="Exportar Contadores"><i class="fas fa-file-export"></i></a> ';
          }else{
            $btnexportar='';
          }
          if($item->comentario!='' or $item->comentario!= null){
            
            $btn_comen='b-btn-success';
          }else{
            $btn_comen='b-btn-primary';
          }
          if($view_delete==0){
            $btnedelete='';
          }
          $btneditcond='<a class="b-btn b-btn-primary tooltipped" onclick="editarcondicion('.$item->prefId.','.$item->idcondicionextra.')" data-position="top" data-delay="50" data-tooltip="Editar Condiciones" title="Editar Condiciones"><i class="fas fa-calendar-week"></i></a>';
          $html.='<tr>';
              $html.='<td>'.$item->prefId.'</td>';
              $html.='<td>'.$item->id_renta.'</td>';
              $html.='<td>'.$item->periodo_inicial.'</td>';
              $html.='<td>'.$item->periodo_final.'</td>';
              $html.='<td>'.$item->subtotal.'</td>';
              $html.='<td>'.$item->excedente.'</td>';
              $html.='<td>'.$item->total.'</td>';
              $html.='<td>'.$item->nombre.' '.$item->apellido_paterno.' '.$item->apellido_materno.'</td>';
              $html.='<td>'.$html_folio.'</td>';
              $html.='<td>'.$item->reg.'</td>';
              $html.='<td>';
                      $html.=$btnfacturar;
                      $html.=$btnedit;
                      $html.=$btnexportar;
                      $html.=$btnedelete;
                      
                      /*
                      <!--<bottom class="btn-floating waves-effect waves-light tooltipped" 
                              onclick="addpagosmodal('.$item->prefId.','.$item->statuspago.','.$item->total.')"  
                              data-position="left" 
                              data-delay="50" 
                              data-tooltip="Pagos"
                              >
                        <i class="material-icons">assignment</i>
                      </bottom>-->
                      */
                $html.='<a class="b-btn b-btn-primary tooltipped" onclick="view_pre_renta('.$item->prefId.','.$idcontrato.','.$centrocosto.','.count($result_f).')"  data-position="top" data-delay="50" data-tooltip="Detalles"><i class="fas fa-clipboard-list"></i></a> ';
                      $html.='<a class="b-btn '.$btn_comen.' tooltipped" onclick="comentariop('.$item->prefId.')" data-position="top" data-delay="50" data-tooltip="Comentario" title="comentario"><i class="fas fa-comment fa-fw"></i></a> ';
                      $html.=$btneditcond;
                    $html.='</td></tr>';
        }
      $html.='</tbody></table>';
      echo  $html;
    }
    /*
    function viewfacturasstatusbtn($prefactura,$contrato,$view){
      $alta_rentas = $this->ModeloCatalogos->db3_getselectwheren('alta_rentas',array('idcontrato'=>$contrato));
      $altarentas=$alta_rentas->row();
      $status_btn=0;
      $total=0;
      $prefacturaresul = $this->ModeloCatalogos->db14_getselectwheren('alta_rentas_prefactura',array('prefId'=>$prefactura));
      $factura_renta =0;
      $factura_excedente =0;
      $statusfactura=0;
      $tipo2=$altarentas->tipo2;
      $btn1='';
      $descartar_not=0;
      $factura_excedente_pendiente=0;
      $fac_exc_pen=0;
      foreach ($prefacturaresul->result() as $item) {
        $factura_renta = $item->factura_renta;
        $factura_excedente = $item->factura_excedente;
        $total = ($item->total+round(($item->total*0.16),2));
        $descartar_not=$item->descartar_not;
        $factura_excedente_pendiente=$item->factura_excedente_pendiente;

      }
      if($factura_excedente_pendiente==1){
        $factura_excedente=0;
        $fac_exc_pen=1;
      }
      if ($factura_renta==1 or $factura_excedente==1) {
        $totalf =$factura_renta+$factura_excedente;
        if ($totalf==2) {
          $statusfactura=2;
          $btn1='<!--statusfactura:'.$statusfactura.'-->';
            $prefacturadx = $this->db->query("SELECT * FROM alta_rentas_prefactura_d WHERE prefId = $prefactura AND (statusfacturar=0 or statusfacturae=0)");
            foreach ($prefacturadx->result() as $item) {
              $statusfactura=1;
            }
            $btn1.='<!--statusfactura:'.$statusfactura.'-->';
        }elseif ($totalf==1) {
          $statusfactura=1;
        }else{
          $statusfactura=0;
        }

        


      }else{
        $prefacturad = $this->ModeloCatalogos->db14_getselectwheren('alta_rentas_prefactura_d',array('prefId'=>$prefactura));
        $rowpred=0;
        $statusfacturar=0;
        $statusfacturae=0;
        foreach ($prefacturad->result() as $item) {
          $statusfacturar=$statusfacturar+$item->statusfacturar;
          $statusfacturae=$statusfacturae+$item->statusfacturae;
          if($factura_excedente_pendiente==1){
            $statusfacturae=0;
            $fac_exc_pen=1;
          }
          $rowpred++;
          //=====================================
          //funcion a solicitud de guadalipe para las cuestion de que no se cuente la parte de color si en dado caso este no se cobra
            if($item->tipo==2){
              if($tipo2==1){//individual
                $rs_are=$this->ModeloCatalogos->db14_getselectwheren('alta_rentas_equipos',array('id_equipo'=>$item->productoId,'serieId'=>$item->serieId,'tipo'=>2));
                $rs_are_result=$rs_are->row();
                if($rs_are_result->rentac>0 or $rs_are_result->precio_c_e_color>0){

                }else{
                  $rowpred--;
                }
              }
            }
          //====================================
        }
        $btn1='<!--$statusfacturar: '.$statusfacturar.'-->';
        $btn1.='<!--$statusfacturae: '.$statusfacturae.'-->';
        //log_message('error', 'prefactura: '.$prefactura.'---'.$statusfacturar.'+'.$statusfacturae);
        $totalfrow=$rowpred*2;
        $totalf=$statusfacturar+$statusfacturae;
        if($totalf>0){
          if($totalf==$totalfrow){
            $statusfactura=2;
          }else{
            $statusfactura=1;
          }
        }else{
          $statusfactura=0;
        }
      }
      $statusfacturaff=1;
      $prefacturaresulff = $this->ModeloCatalogos->db14_getselectwheren('factura_prefactura',array('prefacturaId'=>$prefactura));
      $facturaId=0;
      foreach ($prefacturaresulff->result()  as $itempf) {
          $facturaId=$itempf->facturaId;
          $prefacturaresulfff = $this->ModeloCatalogos->db14_getselectwheren('f_facturas',array('FacturasId'=>$itempf->facturaId));
          foreach ($prefacturaresulfff->result() as $itempff) {
            if($itempff->Estado==0){
              $statusfacturaff=0;
            }
          }
      }
      $btn_addfactura='<a class="b-btn b-btn-primary tooltipped" 
                data-position="top" 
                data-delay="50" 
                data-tooltip="Agregar Factura" onclick="addfactura('.$prefactura.','.$total.')">
                        <i class="fas fa-folder-plus"></i></a> ';
      if ($statusfactura==2) {
        $btn='<button class="btn-bmz waves-effect waves-light" type="button" style=" background-color: #56ea36;">Facturada</button>';
        $status_btn=2;
        $btn.=$btn_addfactura;
      }elseif ($statusfactura==1) {
        if($fac_exc_pen==1){
          $btn_parcial_v='Parcial (Excedentes pendientes)';
        }else{
          $btn_parcial_v='Parcial';
        }
        $status_btn=1;
        $btn='<button class="btn-bmz waves-effect waves-light btn_parcial_v" onclick_btn_parcial_v type="button" style=" background-color: #f9ad5d;" onclick="Fact('.$prefactura.','.$contrato.')" >'.$btn_parcial_v.'</button>';
        $btn.=$btn_addfactura;
      }else{
        $btn='<button class="btn-bmz waves-effect waves-light" type="button" style=" background-color: #e34740;" onclick="Fact('.$prefactura.','.$contrato.')" >Facturar</button>';
        $status_btn=0;
        $btn.=$btn_addfactura;
        if($descartar_not==1){
          $desnot=' style="color:red" ';
          $desnot_title='Periodo Cerrado';
        }else{
          $desnot='';
          $desnot_title='Cerrar periodo';
        }
        $btn.='<button class="b-btn b-btn-primary tooltipped" type="button" style=" title="'.$desnot_title.'" data-position="top" 
                data-delay="50" 
                data-tooltip="'.$desnot_title.'" onclick="cerrarperiodo('.$prefactura.')" ><i class="far fa-window-close" '.$desnot.'></i></button> ';
      }
      if ($statusfactura==2 or $statusfactura==1) {
        $btn.='<a class="waves-effect btn-bmz blue" onclick="refacturarperiodo('.$prefactura.','.$contrato.','.$facturaId.')" title="Refacturar para cancelacion"><i class="fas fa-file-prescription"></i></a>';
      }
      if($statusfacturaff==0){
        $btn.=$btn_addfactura;
      }
      if($view==0){
        $return_btn=$btn.$btn1;
      }else{
        $return_btn=$status_btn;
      }
      return $return_btn;
    }
    */
    function imgrentas($idestado){   
        $hojasestado = $this->Rentas_model->hojasestado_id($idestado);
        ///var_dump($hojasestado);
        foreach ($hojasestado as $item) {
          if($item->tipoimg == 0){
            $fh = fopen(base_url()."uploads/rentas_hojasestado/".$item->nombre, 'r') or die("Se produjo un error al abrir el archivo");
            $linea = fgets($fh);
            fclose($fh);
          }else{
            $linea = $item->nombre;
          }
            $data['tipoimg'] = $item->tipoimg;
            $data['nombre'] = $linea;
        } 
        $this->load->view('rentas/imgrentas',$data);
    }
    public function Facturar($idfactura,$idcontrato){
      $clientes = $this->Rentas_model->clientefactura($idcontrato);
      foreach ($clientes as $item) {
        $data['cliente'] = $item->empresa;
        $data['idcliente'] = $item->id;
        $data['rfc'] = $item->rfc;
        $data['tiporenta']=$item->tiporenta;
        $idRenta = $item->idRenta;
      }
      $where = array('ventaId'=>$idRenta);
      $result_pref = $this->ModeloCatalogos->db14_getselectwheren('prefactura',$where);
      $data['metodopagoId']=0; 
      $data['formapagoId']=0; 
      $data['usocfdiId']=0; 
      foreach ($result_pref->result() as $item) {
          $data['metodopagoId']=$item->metodopagoId; 
          $data['formapagoId']=$item->formapagoId; 
          $data['usocfdiId']=$item->usocfdiId; 
      }

      $data['clientes'] = $this->Rentas_model->clientesrenta();
      $data['equipos'] = $this->Rentas_model->equiposfactura($idfactura);
      $data['idfactura'] = $idfactura;
      $data['idcontrato'] = $idcontrato;
      $data['cfdi']=$this->ModeloGeneral->genSelect('f_uso_cfdi'); 
      $data['metodo']=$this->ModeloGeneral->genSelect('f_metodopago'); 
      $data['forma']=$this->ModeloGeneral->genSelect('f_formapago');
      $data['version']=$this->version; 
      $result_pre = $this->ModeloCatalogos->db14_getselectwheren('alta_rentas_prefactura',array('prefId'=>$idfactura));
      $data['result_pre'] = $result_pre->row();
      $data['result_cc'] = $this->ModeloCatalogos->getselectwheren('centro_costos',array('idcontrato'=>$idcontrato,'activo'=>1));
      $data['idpersonal']=$this->idpersonal;
      $data['fac_pue_pend']=$this->ModeloGeneral->verificar_pue_pendientes_pago($this->idpersonal);
      $this->load->view('header');
      $this->load->view('main',$data);
      $this->load->view('rentas/facturar',$data);
      $this->load->view('footer');
      $this->load->view('rentas/jsfacturar');
    }
    function imgrentasfactura($idequipo,$idserie,$fechainicio,$fechafin){   
        $hojasestado = $this->Rentas_model->hojasestado_id_factura($idequipo,$idserie,$fechainicio,$fechafin);
        ///var_dump($hojasestado);
        foreach ($hojasestado as $item) {
          if($item->tipoimg == 0){
            $fh = fopen(base_url()."uploads/rentas_hojasestado/".$item->nombre, 'r') or die("Se produjo un error al abrir el archivo");
            $linea = fgets($fh);
            fclose($fh);
          }else{
            $linea = $item->nombre;
          }
            $data['tipoimg'] = $item->tipoimg;
            $data['nombre'] = $linea;
            $this->load->view('rentas/imgrentas',$data);
        } 
    }
    function consultaimg(){    
      $idequipo = $this->input->post('equipo');
      $idserie = $this->input->post('serie');
      $fechainicio = $this->input->post('fechaini');
      $fechafin = $this->input->post('fechafin');
      $hojasestado = $this->Rentas_model->hojasestado_id_factura($idequipo,$idserie,$fechainicio,$fechafin);
      $verifica = 0;
      foreach ($hojasestado as $item) {
         $verifica = 1;    
      } 
      echo $verifica; 
    }
    function vericar_pass(){
      $pass = $this->input->post('pass'); 
      $verifica = $this->Rentas_model->verificar_pass($pass);
      $aux = 0;
      if($verifica == 1){
         $aux=1;
      }
      echo $aux;
    }
    
    public function factura_requerimientos() { 
      $idfactura = $this->input->post('id');
      $tipo = $this->input->post('tip');//tipo de facturas
      $mon = $this->input->post('mon');//tipo de monocromaticas
      $col = $this->input->post('col');//tipo de color
      $where = array('prefId' => $idfactura, );
      $result = $this->ModeloCatalogos->getselectwherenall('alta_rentas_prefactura',$where);
      $id_renta=0;
      $rowcobrar=0;
      foreach ($result as $item) {
         $fechai = date("d/m/Y", strtotime($item->periodo_inicial));
         $fechaf = date("d/m/Y", strtotime($item->periodo_final));

         $fechaie = date("d/m/Y", strtotime($item->periodo_inicial."- 1 month"));
         $fechafe = date("d/m/Y", strtotime($item->periodo_final."- 1 month"));
         if (date('d',strtotime($item->periodo_inicial))==1) {
            //log_message('error', 'entra al primero del mes');
            //log_message('error', 'entra al primero del mes'.$item->periodo_inicial);
            $fechaie1 = date("Y-m-d", strtotime($item->periodo_inicial."- 1 month"));
            $fechaie = date("d/m/Y", strtotime($fechaie1));
            $fecha2 = new DateTime($fechaie1);
            $fecha2->modify('last day of this month');
            $fechafe = $fecha2->format('d/m/Y');
            //log_message('error', 'entra al primero del mes'.$fechafe);
         }

         $totals = $item->subtotal;
         $totalcolor = $item->subtotalcolor;
         $excedente = $item->excedente;
         $excedentec = $item->excedentecolor; 
         $id_renta = $item->id_renta;
         $cant_ext_mono=$item->cant_ext_mono;
         $prec_ext_mono=$item->prec_ext_mono;
         $cant_ext_color=$item->cant_ext_color;
         $prec_ext_color=$item->prec_ext_color;  
      }
      $resultrentas = $this->ModeloCatalogos->getselectwherenall('alta_rentas',array('id_renta'=>$id_renta));
        $tipoindiglo=0;
      foreach ($resultrentas as $itemr) {
        $tipoindiglo=$itemr->tipo2;//1 individual 2 global
        $descrip_fac=$itemr->descrip_fac.' ';
        if ($itemr->tipo1==2) {
          $fechaie=$fechai;
          $fechafe=$fechaf;
        }
      }
      //=============================================
        $option_unidad_renta = $this->ModeloCatalogos->obtenerunidadservicio(1,1);
        $option_concepto_renta = $this->ModeloCatalogos->obtenerunidadservicio(1,2);
      //=============================================


      $html = '';
      if ($tipoindiglo==2) {
        //==================================================================================
          $prefacturadx = $this->db->query("SELECT tipo,statusfacturar,statusfacturae FROM alta_rentas_prefactura_d WHERE prefId = $idfactura GROUP BY tipo,statusfacturar,statusfacturae");
            foreach ($prefacturadx->result() as $item) {
              //log_message('error', 'tipo: '.$item->tipo.' statusfacturar:'.$item->statusfacturar.' statusfacturae:'.$item->statusfacturae);
              if ($item->tipo==1) {
                if ($item->statusfacturar==1) {
                  $totals=0;
                  //log_message('error', 'totals');
                }
                if ($item->statusfacturae==1) {
                  $excedente=0;
                  //log_message('error', 'excedente');
                }
              }
              if ($item->tipo==2){
                if ($item->statusfacturar==1) {
                  $totalcolor=0;
                  //log_message('error', 'totalcolor');
                }
                if ($item->statusfacturae==1) {
                  $excedentec=0;
                  //log_message('error', 'excedentec');
                }
              }



                
            }
        //==================================================================================
        if ($mon==0) {
          $totals=0;
          $excedente=0;
        }
        if ($col==0) {
          $totalcolor=0;
          $excedentec=0;
        }
        if($tipo == 1){// rentas
          if ($totals>0) {
             $html.='<tr class="datos_principal tr_1">
                      <td>
                        <input id="selectedequipo" name="selectedequipo" type="hidden" value="0" readonly>
                        <input id="selectedprefactura" name="selectedprefactura" type="hidden" value="'.$idfactura.'" readonly>
                        <input id="facturarenta" name="facturarenta" type="hidden" value="1" readonly>
                        <input id="facturaexcedente" name="facturaexcedente" type="hidden" value="0" readonly>
                        <input id="cantidad" name="cantidad" type="text" value="1" readonly></td>
                      <td>
                          <div style="width:250px">
                            <select id="unidadsat" class="unidadsat browser-default">
                            '.$option_unidad_renta.'
                            </select>
                          <div>
                      </td>
                      <td>
                        <div style="width:250px">
                          <select id="conseptosat" class="conseptosat browser-default">
                          '.$option_concepto_renta.'
                          </select>
                        <div>
                      </td>
                      <td><input id="descripcion" name="descripcion" type="text" value="'.$descrip_fac.'Renta Monocromaticas del '.$fechai.' al '.$fechaf.'"></td>
                      <td><input id="precio" name="precio" class="precio" type="text" value="'.$totals.'" readonly></td>
                      <td>
                          <input type="number"
                            name="descuento"
                            id="descuento"
                            class="form-control-bmz cdescuento cdescuento_'.$rowcobrar.'"
                            onclick="activardescuento('.$rowcobrar.')"
                            value="0" readonly>
                      </td>
                      <td><a class="btn-bmz red deletetrrow"><i class="fas fa-trash-alt"></i></a></td>
                    </tr>';
                    $rowcobrar++;
          }
          if ($totalcolor>0) {
             $html.='<tr class="datos_principal tr_2">
                      <td>
                        <input id="selectedequipo" name="selectedequipo" type="hidden" value="0" readonly>
                        <input id="selectedprefactura" name="selectedprefactura" type="hidden" value="'.$idfactura.'" readonly>
                        <input id="facturarenta" name="facturarenta" type="hidden" value="1" readonly>
                        <input id="facturaexcedente" name="facturaexcedente" type="hidden" value="0" readonly>
                        <input id="cantidad" name="cantidad" type="text" value="1" readonly></td>
                      <td>
                          <div style="width:250px">
                            <select id="unidadsat" class="unidadsat browser-default">
                            '.$option_unidad_renta.'
                            </select>
                          <div>
                      </td>
                      <td>
                        <div style="width:250px">
                          <select id="conseptosat" class="conseptosat browser-default">
                          '.$option_concepto_renta.'
                          </select>
                        <div>
                      </td>
                      <td><input id="descripcion" name="descripcion" type="text" value="'.$descrip_fac.'Renta color del '.$fechai.' al '.$fechaf.'"></td>
                      <td><input id="precio" name="precio" class="precio" type="text" value="'.$totalcolor.'" readonly></td>
                      <td>
                          <input type="number"
                            name="descuento"
                            id="descuento"
                            class="form-control-bmz cdescuento cdescuento_'.$rowcobrar.'"
                            onclick="activardescuento('.$rowcobrar.')"
                            value="0" readonly>
                      </td>
                      <td><a class="btn-bmz red deletetrrow"><i class="fas fa-trash-alt"></i></a></td>
                    </tr>';
                    $rowcobrar++;
          } 
        }else if($tipo == 2){ // rentas y excedentes
          // rentas
            if ($totals>0) {
               $html.='<tr class="datos_principal tr_3">
                        <td>
                          <input id="selectedequipo" name="selectedequipo" type="hidden" value="0" readonly>
                          <input id="selectedprefactura" name="selectedprefactura" type="hidden" value="'.$idfactura.'" readonly>
                          <input id="facturarenta" name="facturarenta" type="hidden" value="1" readonly>
                          <input id="facturaexcedente" name="facturaexcedente" type="hidden" value="1" readonly>
                          <input id="cantidad" name="cantidad" type="text" value="1" readonly></td>
                        <td>
                            <div style="width:250px">
                              <select id="unidadsat" class="unidadsat browser-default">
                              '.$option_unidad_renta.'
                              </select>
                            <div>
                        </td>
                        <td>
                          <div style="width:250px">
                            <select id="conseptosat" class="conseptosat browser-default">
                            '.$option_concepto_renta.'
                            </select>
                          <div>
                        </td>
                        <td><input id="descripcion" name="descripcion" type="text" value="'.$descrip_fac.'Renta Monocromaticas del '.$fechai.' al '.$fechaf.'"></td>
                        <td><input id="precio" name="precio" class="precio" type="text" value="'.$totals.'" readonly></td>
                        <td>
                          <input type="number"
                            name="descuento"
                            id="descuento"
                            class="form-control-bmz cdescuento cdescuento_'.$rowcobrar.'"
                            onclick="activardescuento('.$rowcobrar.')"
                            value="0" readonly>
                        </td>
                        <td><a class="btn-bmz red deletetrrow"><i class="fas fa-trash-alt"></i></a></td>
                      </tr>';
                      $rowcobrar++;
            }
            if ($totalcolor>0) {
               $html.='<tr class="datos_principal tr_4">
                        <td>
                          <input id="selectedequipo" name="selectedequipo" type="hidden" value="0" readonly>
                          <input id="selectedprefactura" name="selectedprefactura" type="hidden" value="'.$idfactura.'" readonly>
                          <input id="facturarenta" name="facturarenta" type="hidden" value="1" readonly>
                        <input id="facturaexcedente" name="facturaexcedente" type="hidden" value="1" readonly>
                          <input id="cantidad" name="cantidad" type="text" value="1" readonly></td>
                        <td>
                            <div style="width:250px">
                              <select id="unidadsat" class="unidadsat browser-default">
                              '.$option_unidad_renta.'
                              </select>
                            <div>
                        </td>
                        <td>
                          <div style="width:250px">
                            <select id="conseptosat" class="conseptosat browser-default">
                            '.$option_concepto_renta.'
                            </select>
                          <div>
                        </td>
                        <td><input id="descripcion" name="descripcion" type="text" value="'.$descrip_fac.'Renta color del '.$fechai.' al '.$fechaf.'"></td>
                        <td><input id="precio" name="precio" class="precio" type="text" value="'.$totalcolor.'" readonly></td>
                        <td>
                          <input type="number"
                            name="descuento"
                            id="descuento"
                            class="form-control-bmz cdescuento cdescuento_'.$rowcobrar.'"
                            onclick="activardescuento('.$rowcobrar.')"
                            value="0" readonly>
                      </td>
                      <td><a class="btn-bmz red deletetrrow"><i class="fas fa-trash-alt"></i></a></td>
                      </tr>';
                      $rowcobrar++;
            }

            if($excedente > 0){ // excedentes monocromatico        
             $html.='<tr class="datos_principal tr_5">
                      <td>
                        <input id="selectedequipo" name="selectedequipo" type="hidden" value="0" readonly>
                        <input id="selectedprefactura" name="selectedprefactura" type="hidden" value="'.$idfactura.'" readonly>
                        <input id="facturarenta" name="facturarenta" type="hidden" value="1" readonly>
                        <input id="facturaexcedente" name="facturaexcedente" type="hidden" value="1" readonly>
                        <input id="cantidad" name="cantidad" type="text" value="1" class="excedente info cant_'.$rowcobrar.'" data-cantidad="'.$cant_ext_mono.'" data-precioexc="'.$prec_ext_mono.'" data-preid="'.$rowcobrar.'" readonly></td>
                      <td>
                        <div style="width:250px">
                          <select id="unidadsat" class="unidadsat browser-default">
                          '.$option_unidad_renta.'
                          </select>
                        <div>
                      </td>
                      <td>
                        <div style="width:250px">
                          <select id="conseptosat" class="conseptosat browser-default">
                          '.$option_concepto_renta.'
                          </select>
                        <div>
                      </td>
                      <td><input id="descripcion" name="descripcion" type="text" value="'.$descrip_fac.'Excedente monocromatico del '.$fechaie.' al '.$fechafe.'"></td>
                      <td><input id="precio" name="precio" class="precio excedente precio_'.$rowcobrar.'" type="text" value="'.$excedente.'" readonly></td>
                      <td>
                          <input type="number"
                            name="descuento"
                            id="descuento"
                            class="form-control-bmz cdescuento cdescuento_'.$rowcobrar.'"
                            onclick="activardescuento('.$rowcobrar.')"
                            value="0" readonly>
                      </td>
                      <td><a class="btn-bmz red deletetrrow"><i class="fas fa-trash-alt"></i></a></td>
                    </tr>';
                    $rowcobrar++;
            }
            if($excedentec > 0){// excedentes a color        
             $html.='<tr class="datos_principal tr_6">
                      <td>
                        <input id="selectedequipo" name="selectedequipo" type="hidden" value="0" readonly>
                        <input id="selectedprefactura" name="selectedprefactura" type="hidden" value="'.$idfactura.'" readonly>
                        <input id="facturarenta" name="facturarenta" type="hidden" value="1" readonly>
                        <input id="facturaexcedente" name="facturaexcedente" type="hidden" value="1" readonly>
                        <input id="cantidad" name="cantidad" type="text" value="1" class="excedente info cant_'.$rowcobrar.'" data-cantidad="'.$cant_ext_color.'" data-precioexc="'.$prec_ext_color.'" data-preid="'.$rowcobrar.'" readonly></td>
                      <td><div style="width:250px">
                          <select id="unidadsat" class="unidadsat browser-default">
                          '.$option_unidad_renta.'
                          </select>
                        <div>
                      </td>
                      <td>
                          <div style="width:250px">
                            <select id="conseptosat" class="conseptosat browser-default">
                            '.$option_concepto_renta.'
                            </select>
                          <div>
                      </td>
                      <td><input id="descripcion" name="descripcion" type="text" value="'.$descrip_fac.'Excedente color del '.$fechaie.' al '.$fechafe.'"></td>
                      <td><input id="precio" name="precio" class="precio excedente precio_'.$rowcobrar.'" type="text" value="'.$excedentec.'" readonly></td>
                      <td>
                          <input type="number"
                            name="descuento"
                            id="descuento"
                            class="form-control-bmz cdescuento cdescuento_'.$rowcobrar.'"
                            onclick="activardescuento('.$rowcobrar.')"
                            value="0" readonly>
                      </td>
                      <td><a class="btn-bmz red deletetrrow"><i class="fas fa-trash-alt"></i></a></td>
                    </tr>'; 
                    $rowcobrar++;   
            }            
        }else if($tipo == 3){
            if($excedente > 0){ // excedentes a monocromatico        
              $html.='<tr class="datos_principal tr_7">
                      <td>
                        <input id="selectedequipo" name="selectedequipo" type="hidden" value="0" readonly>
                        <input id="selectedprefactura" name="selectedprefactura" type="hidden" value="'.$idfactura.'" readonly>
                        <input id="facturarenta" name="facturarenta" type="hidden" value="0" readonly>
                        <input id="facturaexcedente" name="facturaexcedente" type="hidden" value="1" readonly>
                        <input id="cantidad" name="cantidad" type="text" value="1" class="excedente info cant_'.$rowcobrar.'" data-cantidad="'.$cant_ext_mono.'" data-precioexc="'.$prec_ext_mono.'" data-preid="'.$rowcobrar.'" readonly></td>
                      <td>
                        <div style="width:250px">
                          <select id="unidadsat" class="unidadsat browser-default">
                          '.$option_unidad_renta.'
                          </select>
                        <div>
                      </td>
                      <td>
                          <div style="width:250px">
                            <select id="conseptosat" class="conseptosat browser-default">
                            '.$option_concepto_renta.'
                            </select>
                          <div>
                      </td>
                      <td><input id="descripcion" name="descripcion" type="text" value="'.$descrip_fac.'Excedente monocromatico del '.$fechaie.' al '.$fechafe.'"></td>
                      <td><input id="precio" name="precio" class="precio  excedente precio_'.$rowcobrar.'" type="text" value="'.$excedente.'" readonly></td>
                      <td>
                          <input type="number"
                            name="descuento"
                            id="descuento"
                            class="form-control-bmz cdescuento cdescuento_'.$rowcobrar.'"
                            onclick="activardescuento('.$rowcobrar.')"
                            value="0" readonly>
                      </td>
                      <td><a class="btn-bmz red deletetrrow"><i class="fas fa-trash-alt"></i></a></td>
                    </tr>';
                    $rowcobrar++;
            }
            if($excedentec > 0){ // excedentes a monocromatico        
              $html.='<tr class="datos_principal tr_8">
                      <td>
                        <input id="selectedequipo" name="selectedequipo" type="hidden" value="0" readonly>
                        <input id="selectedprefactura" name="selectedprefactura" type="hidden" value="'.$idfactura.'" readonly>
                        <input id="facturarenta" name="facturarenta" type="hidden" value="0" readonly>
                        <input id="facturaexcedente" name="facturaexcedente" type="hidden" value="1" readonly>
                        <input id="cantidad" name="cantidad" type="text" value="1" class="excedente info cant_'.$rowcobrar.'" data-cantidad="'.$cant_ext_color.'" data-precioexc="'.$prec_ext_color.'" data-preid="'.$rowcobrar.'" readonly></td>
                      <td>
                        <div style="width:250px">
                          <select id="unidadsat" class="unidadsat browser-default">
                          '.$option_unidad_renta.'
                          </select>
                        <div>
                      </td>
                      <td>
                          <div style="width:250px">
                            <select id="conseptosat" class="conseptosat browser-default">
                            '.$option_concepto_renta.'
                            </select>
                          <div>
                      </td>
                      <td><input id="descripcion" name="descripcion" type="text" value="'.$descrip_fac.'Excedente color del '.$fechaie.' al '.$fechafe.'"></td>
                      <td><input id="precio" name="precio" class="precio  excedente precio_'.$rowcobrar.'" type="text" value="'.$excedentec.'" readonly></td>
                      <td>
                          <input type="number"
                            name="descuento"
                            id="descuento"
                            class="form-control-bmz cdescuento cdescuento_'.$rowcobrar.'"
                            onclick="activardescuento('.$rowcobrar.')"
                            value="0" readonly>
                      </td>
                      <td><a class="btn-bmz red deletetrrow"><i class="fas fa-trash-alt"></i></a></td>
                    </tr>';
                    $rowcobrar++;
            }
        }
      }else{
        $equipos = $this->input->post('equipos');
        $DATAeq = json_decode($equipos);
        for ($i=0;$i<count($DATAeq);$i++) {
          $equiposfacturar=$this->ModeloCatalogos->datosequiporentafactura($idfactura,$DATAeq[$i]->equipos,$DATAeq[$i]->serie,$DATAeq[$i]->tipo);
          foreach ($equiposfacturar as $item) {
            if($tipo == 1){//renta
              if ($item->statusfacturar==0) {
                if ($item->renta>0) {
                  if($item->tipo==1 and $mon==1){
                  $html.='<tr class="datos_principal tr_9">
                      <td>
                        <input id="selectedequipo" name="selectedequipo" type="hidden" value="'.$item->prefdId.'" readonly>
                        <input id="selectedprefactura" name="selectedprefactura" type="hidden" value="0" readonly>
                        <input id="facturarenta" name="facturarenta" type="hidden" value="1" readonly>
                        <input id="facturaexcedente" name="facturaexcedente" type="hidden" value="0" readonly>
                        <input id="cantidad" name="cantidad" type="text" value="1" readonly></td>
                      <td>
                          <div style="width:250px">
                            <select id="unidadsat" class="unidadsat browser-default">
                            '.$option_unidad_renta.'
                            </select>
                          <div>
                      </td>
                      <td>
                        <div style="width:250px">
                          <select id="conseptosat" class="conseptosat browser-default">
                          '.$option_concepto_renta.'
                          </select>
                        <div>
                      </td>
                      <td><input id="descripcion" name="descripcion" type="text" value="'.$descrip_fac.'Renta '.$item->modelo.' '.$item->serie.' Monocromaticas del '.$fechai.' al '.$fechaf.'"></td>
                      <td><input id="precio" name="precio" class="precio" type="text" value="'.$item->renta.'" readonly></td>
                      <td>
                          <input type="number"
                            name="descuento"
                            id="descuento"
                            class="form-control-bmz cdescuento cdescuento_'.$rowcobrar.'"
                            onclick="activardescuento('.$rowcobrar.')"
                            value="0" readonly>
                      </td>
                      <td><a class="btn-bmz red deletetrrow"><i class="fas fa-trash-alt"></i></a></td>
                    </tr>';
                    $rowcobrar++;
                  }
                }
                if ($item->rentac>0) {
                  if($item->tipo==2 and $col==2){
                  $html.='<tr class="datos_principal tr_10">
                      <td>
                        <input id="selectedequipo" name="selectedequipo" type="hidden" value="'.$item->prefdId.'" readonly>
                        <input id="selectedprefactura" name="selectedprefactura" type="hidden" value="0" readonly>
                        <input id="facturarenta" name="facturarenta" type="hidden" value="1" readonly>
                        <input id="facturaexcedente" name="facturaexcedente" type="hidden" value="0" readonly>
                        <input id="cantidad" name="cantidad" type="text" value="1" readonly></td>
                      <td>
                          <div style="width:250px">
                            <select id="unidadsat" class="unidadsat browser-default">
                            '.$option_unidad_renta.'
                            </select>
                          <div>
                      </td>
                      <td>
                        <div style="width:250px">
                          <select id="conseptosat" class="conseptosat browser-default">
                          '.$option_concepto_renta.'
                          </select>
                        <div>
                      </td>
                      <td><input id="descripcion" name="descripcion" type="text" value="'.$descrip_fac.'Renta '.$item->modelo.' '.$item->serie.' Color del '.$fechai.' al '.$fechaf.'"></td>
                      <td><input id="precio" name="precio" class="precio" type="text" value="'.$item->rentac.'" readonly></td>
                      <td>
                          <input type="number"
                            name="descuento"
                            id="descuento"
                            class="form-control-bmz cdescuento cdescuento_'.$rowcobrar.'"
                            onclick="activardescuento('.$rowcobrar.')"
                            value="0" readonly>
                      </td>
                      <td><a class="btn-bmz red deletetrrow"><i class="fas fa-trash-alt"></i></a></td>
                    </tr>';
                    $rowcobrar++;
                  }
                }
                
              }
            }
            if($tipo == 3){//excedentes
              if ($item->statusfacturae==0) {
                if ($item->totalexcedente>0 and $mon==1) {
                  $html.='<tr class="datos_principal tr_11">
                      <td>
                        <input id="selectedequipo" name="selectedequipo" type="hidden" value="'.$item->prefdId.'" readonly>
                        <input id="selectedprefactura" name="selectedprefactura" type="hidden" value="0" readonly>
                        <input id="facturarenta" name="facturarenta" type="hidden" value="0" readonly>
                        <input id="facturaexcedente" name="facturaexcedente" type="hidden" value="1" readonly>
                        <input id="cantidad" name="cantidad" type="text" value="1" class="excedente info cant_'.$item->prefdId.'" data-cantidad="'.$item->excedentes.'" data-precioexc="'.$item->costoexcedente.'" data-preid="'.$item->prefdId.'" readonly></td>
                      <td>
                          <div style="width:250px">
                            <select id="unidadsat" class="unidadsat browser-default">
                            '.$option_unidad_renta.'
                            </select>
                          <div>
                      </td>
                      <td>
                        <div style="width:250px">
                          <select id="conseptosat" class="conseptosat browser-default">
                          '.$option_concepto_renta.'
                          </select>
                        <div>
                      </td>
                      <td><input id="descripcion" name="descripcion" type="text" value="'.$descrip_fac.'Excedente '.$item->modelo.' '.$item->serie.' Monocromaticas del '.$fechaie.' al '.$fechafe.'"></td>
                      <td><input id="precio" name="precio" class="precio excedente precio_'.$item->prefdId.'" type="text" value="'.$item->totalexcedente.'" readonly></td>
                      <td>
                          <input type="number"
                            name="descuento"
                            id="descuento"
                            class="form-control-bmz cdescuento cdescuento_'.$rowcobrar.'"
                            onclick="activardescuento('.$rowcobrar.')"
                            value="0" readonly>
                      </td>
                      <td><a class="btn-bmz red deletetrrow"><i class="fas fa-trash-alt"></i></a></td>
                    </tr>';
                    $rowcobrar++;
                }
              }
            }
            if($tipo == 2){//renta y excendentes
              if ($item->statusfacturar==0) {
                if ($item->renta>0) {
                  if($item->tipo==1 and $mon==1){
                  $html.='<tr class="datos_principal tr_12">
                      <td>
                        <input id="selectedequipo" name="selectedequipo" type="hidden" value="'.$item->prefdId.'" readonly>
                        <input id="selectedprefactura" name="selectedprefactura" type="hidden" value="0" readonly>
                        <input id="facturarenta" name="facturarenta" type="hidden" value="1" readonly>
                        <input id="facturaexcedente" name="facturaexcedente" type="hidden" value="1" readonly>
                        <input id="cantidad" name="cantidad" type="text" value="1" readonly></td>
                      <td>
                          <div style="width:250px">
                            <select id="unidadsat" class="unidadsat browser-default">
                            '.$option_unidad_renta.'
                            </select>
                          <div>
                      </td>
                      <td>
                        <div style="width:250px">
                          <select id="conseptosat" class="conseptosat browser-default">
                          '.$option_concepto_renta.'
                          </select>
                        <div>
                      </td>
                      <td><input id="descripcion" name="descripcion" type="text" value="'.$descrip_fac.'Renta '.$item->modelo.' '.$item->serie.' Monocromaticas del '.$fechai.' al '.$fechaf.'"></td>
                      <td><input id="precio" name="precio" class="precio" type="text" value="'.$item->renta.'" readonly></td>
                      <td>
                          <input type="number"
                            name="descuento"
                            id="descuento"
                            class="form-control-bmz cdescuento cdescuento_'.$rowcobrar.'"
                            onclick="activardescuento('.$rowcobrar.')"
                            value="0" readonly>
                      </td>
                      <td><a class="btn-bmz red deletetrrow"><i class="fas fa-trash-alt"></i></a></td>
                    </tr>';
                    $rowcobrar++;
                    }
                  }
                  if ($item->rentac>0) {
                    if($item->tipo==2 and $col==2){
                    $html.='<tr class="datos_principal tr_13">
                        <td>
                          <input id="selectedequipo" name="selectedequipo" type="hidden" value="'.$item->prefdId.'" readonly>
                          <input id="selectedprefactura" name="selectedprefactura" type="hidden" value="0" readonly>
                          <input id="facturarenta" name="facturarenta" type="hidden" value="1" readonly>
                          <input id="facturaexcedente" name="facturaexcedente" type="hidden" value="1" readonly>
                          <input id="cantidad" name="cantidad" type="text" value="1" readonly></td>
                        <td>
                            <div style="width:250px">
                              <select id="unidadsat" class="unidadsat browser-default">
                              '.$option_unidad_renta.'
                              </select>
                            <div>
                        </td>
                        <td>
                          <div style="width:250px">
                            <select id="conseptosat" class="conseptosat browser-default">
                            '.$option_concepto_renta.'
                            </select>
                          <div>
                        </td>
                        <td><input id="descripcion" name="descripcion" type="text" value="'.$descrip_fac.'Renta '.$item->modelo.' '.$item->serie.' Color del '.$fechai.' al '.$fechaf.'"></td>
                        <td><input id="precio" name="precio" class="precio" type="text" value="'.$item->rentac.'" readonly></td>
                        <td>
                          <input type="number"
                            name="descuento"
                            id="descuento"
                            class="form-control-bmz cdescuento cdescuento_'.$rowcobrar.'"
                            onclick="activardescuento('.$rowcobrar.')"
                            value="0" readonly>
                      </td>
                      <td><a class="btn-bmz red deletetrrow"><i class="fas fa-trash-alt"></i></a></td>
                      </tr>';
                      $rowcobrar++;
                    }
                  }
              }
              if ($item->statusfacturae==0) {
                if ($item->totalexcedente>0 and $mon==1) {
                    if ($item->tipo==2) {
                      $tipocequipo=' Color ';
                    }else{
                      $tipocequipo=' Monocromaticas ';
                    }
                  $html.='<tr class="datos_principal tr_14">
                      <td>
                        <input id="selectedequipo" name="selectedequipo" type="hidden" value="'.$item->prefdId.'" readonly>
                        <input id="selectedprefactura" name="selectedprefactura" type="hidden" value="0" readonly>
                        <input id="facturarenta" name="facturarenta" type="hidden" value="1" readonly>
                        <input id="facturaexcedente" name="facturaexcedente" type="hidden" value="1" readonly>
                        <input id="cantidad" name="cantidad" type="text" value="1" class="excedente info cant_'.$item->prefdId.'" data-cantidad="'.$item->excedentes.'" data-precioexc="'.$item->costoexcedente.'" data-preid="'.$item->prefdId.'" readonly></td>
                      <td>
                          <div style="width:250px">
                            <select id="unidadsat" class="unidadsat browser-default">
                            '.$option_unidad_renta.'
                            </select>
                          <div>
                      </td>
                      <td>
                        <div style="width:250px">
                          <select id="conseptosat" class="conseptosat browser-default">
                          '.$option_concepto_renta.'
                          </select>
                        <div>
                      </td>
                      <td><input id="descripcion" name="descripcion" type="text" value="'.$descrip_fac.'Excedente '.$item->modelo.' '.$item->serie.' '.$tipocequipo.' del '.$fechaie.' al '.$fechafe.'"></td>
                      <td><input id="precio" name="precio" class="precio excedente precio_'.$item->prefdId.'" type="text" value="'.$item->totalexcedente.'" readonly></td>
                      <td>
                          <input type="number"
                            name="descuento"
                            id="descuento"
                            class="form-control-bmz cdescuento cdescuento_'.$rowcobrar.'"
                            onclick="activardescuento('.$rowcobrar.')"
                            value="0" readonly>
                      </td>
                      <td><a class="btn-bmz red deletetrrow"><i class="fas fa-trash-alt"></i></a></td>
                    </tr>';
                    $rowcobrar++;
                }
              }
            }
            
          }
        }
      }
      
      echo $html;
    }
    public function selectcontratos(){
      $idcliente = $this->input->post('id');
      $html = '';
      $contrato = $this->Rentas_model->contratorentacliente($idcliente);
        $html.='<div class="row">
                  <div class="col s4">
                    <h6>Seleccionar contrato</h6>
                  </div>  
                  <div class="col s7">
                    <select id="idcontrato_f" name="idcontrato_f" class="browser-default chosen-select" onchange="facturaselect()">
                      <option value="" disabled selected>Selecciona una opción</option>';
                      foreach ($contrato as $item) {
                      $html.='<option value="'.$item->idcontrato.'" >'.date("d-m-Y",strtotime($item->fechainicio)).' Folio:'.$item->folio.'</option>';
                      }    
              $html.='</select>
                  </div>
                </div>';
      echo $html;
    }
    public function selectcontratos2(){
      $idcliente = $this->input->post('id');
      $html = '';
      $contrato = $this->Rentas_model->contratorentacliente($idcliente);
        foreach ($contrato as $item) {
          $html.='<option value="'.$item->idcontrato.'" >'.date("d-m-Y",strtotime($item->fechainicio)).' Folio:'.$item->folio.'</option>';
        }    
              
      echo $html;
    }
    public function selectfacturas()    {  
      $idcontrato = $this->input->post('id');
       $html = '';
       $factura = $this->Rentas_model->contratorentafactura($idcontrato);
        $html.='<div class="row">
                  <div class="col s4">
                    <h6>Seleccionar prefactura</h6>
                  </div>  
                  <div class="col s7">
                    <select id="idfactura_f" name="idfactura_f" class="browser-default chosen-select" onchange="mostrar_check_rentas()">
                      <option value="" disabled selected>Selecciona una opción</option>';
                      foreach ($factura as $item) {
                      $html.='<option value="'.$item->prefId.'" >'.$item->prefId.' '.date("d-m-Y",strtotime($item->periodo_inicial)).' al '.date("d-m-Y",strtotime($item->periodo_final)).'</option>';
                      }    
              $html.='</select>
                  </div>
                </div>';
      echo $html;  
    }
    public function selectfacturas2()    {  
      $idcontrato = $this->input->post('id');
       $html = '';
       $factura = $this->Rentas_model->contratorentafactura2($idcontrato);
        $html.='<option value="" disabled selected>Selecciona una opción</option>';
                foreach ($factura as $item) {
                  $excedentetotal=$item->excedente+$item->excedentecolor;
                  if ($excedentetotal>0) {
                    $html.='<option value="'.$item->prefId.'"  data-exedente="'.$excedentetotal.'">
                              '.$item->prefId.' '.date("d-m-Y",strtotime($item->periodo_inicial)).' al '.date("d-m-Y",strtotime($item->periodo_final)).'
                            </option>';
                  }
                
                }    
              
      echo $html;  
    }
    public function factura_requerimientos2(){
      $params = $this->input->post();
      $idfactura = $params['id'];
      $pediodoids=$idfactura;
      $tipo = $params['tip'];//tipo de facturas
      $id_renta=0;
      if (isset($params['rowcobrartable'])) {
        $rowcobrar=$params['rowcobrartable']+1;
      }else{
        $rowcobrar=0;
      }
      $where = array('prefId' => $idfactura);
      $result = $this->ModeloCatalogos->getselectwherenall('alta_rentas_prefactura',$where);
      foreach ($result as $item) {
         $fechai = date("d/m/Y", strtotime($item->periodo_inicial));
         $fechaf = date("d/m/Y", strtotime($item->periodo_final));

         $fechaie = date("d/m/Y", strtotime($item->periodo_inicial."- 1 month"));
         $fechafe = date("d/m/Y", strtotime($item->periodo_final."- 1 month"));

         $totals = $item->subtotal;
         $totalcolor = $item->subtotalcolor;
         $excedente = $item->excedente;
         $excedentec = $item->excedentecolor;
         $id_renta = $item->id_renta;
         $cant_ext_mono=$item->cant_ext_mono;
         $prec_ext_mono=$item->prec_ext_mono;
         $cant_ext_color=$item->cant_ext_color;
         $prec_ext_color=$item->prec_ext_color;    
      }
      $resultrentas = $this->ModeloCatalogos->getselectwherenall('alta_rentas',array('id_renta'=>$id_renta));
        $tipoindiglo=0;
      foreach ($resultrentas as $itemr) {
        $tipoindiglo=$itemr->tipo2;//1 individual 2 global
        $descrip_fac=$itemr->descrip_fac.' ';
        if ($itemr->tipo1==2) {
          $fechaie=$fechai;
          $fechafe=$fechaf;
        }
      }
      //=============================================
        $option_unidad_renta = $this->ModeloCatalogos->obtenerunidadservicio(1,1);
        $option_concepto_renta = $this->ModeloCatalogos->obtenerunidadservicio(1,2);
      //=============================================
      $html = '';
      if ($tipoindiglo==2) {
        if($tipo == 1){// rentas
          if ($totals>0) {
             $html.='<tr class="datos_agregagados datos_agregagados_'.$idfactura.' datos_agregagados_periodo_'.$pediodoids.'">
                      <td>
                        <input id="selectedequipo" name="selectedequipo" type="hidden" value="0" readonly>
                        <input id="selectedprefactura" name="selectedprefactura" type="hidden" value="'.$idfactura.'" readonly>
                        <input id="facturarenta" name="facturarenta" type="hidden" value="1" readonly>
                        <input id="facturaexcedente" name="facturaexcedente" type="hidden" value="0" readonly>
                        <input id="cantidad" name="cantidad" type="text" value="1"  readonly></td>
                      <td>
                          <div style="width:250px">
                            <select id="unidadsat" class="unidadsat browser-default">
                            '.$option_unidad_renta.'
                            </select>
                          <div>
                      </td>
                      <td>
                        <div style="width:250px">
                          <select id="conseptosat" class="conseptosat browser-default">
                          '.$option_concepto_renta.'
                          </select>
                        <div>
                      </td>
                      <td><input id="descripcion" name="descripcion" type="text" value="'.$descrip_fac.'Renta Monocromaticas del '.$fechai.' al '.$fechaf.'"></td>
                      <td><input id="precio" name="precio" class="precio" type="text" value="'.$totals.'" readonly></td>
                      <td>
                          <input type="number"
                            name="descuento"
                            id="descuento"
                            class="form-control-bmz cdescuento cdescuento_'.$rowcobrar.'"
                            onclick="activardescuento('.$rowcobrar.')"
                            value="0" readonly>
                      </td>
                      <td><a class="btn-floating waves-effect waves-light purple lightrn-1" onclick="deleteprefactura('.$idfactura.')"><i class="material-icons">delete_forever</i></a></td>
                      
                    </tr>';
            $rowcobrar++;
          }
          if ($totalcolor>0) {
             $html.='<tr class="datos_agregagados datos_agregagados_'.$idfactura.' datos_agregagados_periodo_'.$pediodoids.'">
                      <td>
                        <input id="selectedequipo" name="selectedequipo" type="hidden" value="0" readonly>
                        <input id="selectedprefactura" name="selectedprefactura" type="hidden" value="'.$idfactura.'" readonly>
                        <input id="facturarenta" name="facturarenta" type="hidden" value="1" readonly>
                        <input id="facturaexcedente" name="facturaexcedente" type="hidden" value="0" readonly>
                        <input id="cantidad" name="cantidad" type="text" value="1" readonly></td>
                      <td>
                          <div style="width:250px">
                            <select id="unidadsat" class="unidadsat browser-default">
                            '.$option_unidad_renta.'
                            </select>
                          <div>
                      </td>
                      <td>
                        <div style="width:250px">
                          <select id="conseptosat" class="conseptosat browser-default">
                          '.$option_concepto_renta.'
                          </select>
                        <div>
                      </td>
                      <td><input id="descripcion" name="descripcion" type="text" value="'.$descrip_fac.'Renta color del '.$fechai.' al '.$fechaf.'"></td>
                      <td><input id="precio" name="precio" class="precio" type="text" value="'.$totalcolor.'" readonly></td>
                      <td>
                          <input type="number"
                            name="descuento"
                            id="descuento"
                            class="form-control-bmz cdescuento cdescuento_'.$rowcobrar.'"
                            onclick="activardescuento('.$rowcobrar.')"
                            value="0" readonly>
                      </td>
                      <td><a class="btn-floating waves-effect waves-light purple lightrn-1" onclick="deleteprefactura('.$idfactura.')"><i class="material-icons">delete_forever</i></a></td>
                    </tr>';
            $rowcobrar++;
          } 
        }else if($tipo == 2){ // rentas y excedentes
          // rentas
            if ($totals>0) {
               $html.='<tr class="datos_agregagados datos_agregagados_'.$idfactura.' datos_agregagados_periodo_'.$pediodoids.'">
                        <td>
                          <input id="selectedequipo" name="selectedequipo" type="hidden" value="0" readonly>
                          <input id="selectedprefactura" name="selectedprefactura" type="hidden" value="'.$idfactura.'" readonly>
                          <input id="facturarenta" name="facturarenta" type="hidden" value="1" readonly>
                          <input id="facturaexcedente" name="facturaexcedente" type="hidden" value="1" readonly>
                          <input id="cantidad" name="cantidad" type="text" value="1" readonly></td>
                        <td>
                            <div style="width:250px">
                              <select id="unidadsat" class="unidadsat browser-default">
                              '.$option_unidad_renta.'
                              </select>
                            <div>
                        </td>
                        <td>
                          <div style="width:250px">
                            <select id="conseptosat" class="conseptosat browser-default">
                            '.$option_concepto_renta.'
                            </select>
                          <div>
                        </td>
                        <td><input id="descripcion" name="descripcion" type="text" value="'.$descrip_fac.'Renta Monocromaticas del '.$fechai.' al '.$fechaf.'"></td>
                        <td><input id="precio" name="precio" class="precio" type="text" value="'.$totals.'" readonly></td>
                        <td>
                          <input type="number"
                            name="descuento"
                            id="descuento"
                            class="form-control-bmz cdescuento cdescuento_'.$rowcobrar.'"
                            onclick="activardescuento('.$rowcobrar.')"
                            value="0" readonly>
                      </td>
                        <td><a class="btn-floating waves-effect waves-light purple lightrn-1" onclick="deleteprefactura('.$idfactura.')"><i class="material-icons">delete_forever</i></a></td>
                      </tr>';
                $rowcobrar++;
            }
            if ($totalcolor>0) {
               $html.='<tr class="datos_agregagados datos_agregagados_'.$idfactura.' datos_agregagados_periodo_'.$pediodoids.'">
                        <td>
                          <input id="selectedequipo" name="selectedequipo" type="hidden" value="0" readonly>
                          <input id="selectedprefactura" name="selectedprefactura" type="hidden" value="'.$idfactura.'" readonly>
                          <input id="facturarenta" name="facturarenta" type="hidden" value="1" readonly>
                        <input id="facturaexcedente" name="facturaexcedente" type="hidden" value="1" readonly>
                          <input id="cantidad" name="cantidad" type="text" value="1" readonly></td>
                        <td>
                            <div style="width:250px">
                              <select id="unidadsat" class="unidadsat browser-default">
                              '.$option_unidad_renta.'
                              </select>
                            <div>
                        </td>
                        <td>
                          <div style="width:250px">
                            <select id="conseptosat" class="conseptosat browser-default">
                            '.$option_concepto_renta.'
                            </select>
                          <div>
                        </td>
                        <td><input id="descripcion" name="descripcion" type="text" value="'.$descrip_fac.'Renta color del '.$fechai.' al '.$fechaf.'"></td>
                        <td><input id="precio" name="precio" class="precio" type="text" value="'.$totalcolor.'" readonly></td>
                        <td>
                          <input type="number"
                            name="descuento"
                            id="descuento"
                            class="form-control-bmz cdescuento cdescuento_'.$rowcobrar.'"
                            onclick="activardescuento('.$rowcobrar.')"
                            value="0" readonly>
                      </td>
                        <td><a class="btn-floating waves-effect waves-light purple lightrn-1" onclick="deleteprefactura('.$idfactura.')"><i class="material-icons">delete_forever</i></a></td>
                      </tr>';
                    $rowcobrar++;
            }

            if($excedente > 0){ // excedentes monocromatico        
             $html.='<tr class="datos_agregagados datos_agregagados_'.$idfactura.' datos_agregagados_periodo_'.$pediodoids.'">
                      <td>
                        <input id="selectedequipo" name="selectedequipo" type="hidden" value="0" readonly>
                        <input id="selectedprefactura" name="selectedprefactura" type="hidden" value="'.$idfactura.'" readonly>
                        <input id="facturarenta" name="facturarenta" type="hidden" value="1" readonly>
                        <input id="facturaexcedente" name="facturaexcedente" type="hidden" value="1" readonly>
                        <input id="cantidad" name="cantidad" type="text" value="1" class="excedente info cant_'.$rowcobrar.'" data-cantidad="'.$cant_ext_mono.'" data-precioexc="'.$prec_ext_mono.'" data-preid="'.$rowcobrar.'" readonly></td>
                      <td>
                        <div style="width:250px">
                          <select id="unidadsat" class="unidadsat browser-default">
                          '.$option_unidad_renta.'
                          </select>
                        <div>
                      </td>
                      <td>
                        <div style="width:250px">
                          <select id="conseptosat" class="conseptosat browser-default">
                          '.$option_concepto_renta.'
                          </select>
                        <div>
                      </td>
                      <td><input id="descripcion" name="descripcion" type="text" value="'.$descrip_fac.'Excedente monocromatico del '.$fechaie.' al '.$fechafe.'"></td>
                      <td><input id="precio" name="precio" class="precio excedente precio_'.$rowcobrar.'" type="text" value="'.$excedente.'" readonly></td>
                      <td>
                          <input type="number"
                            name="descuento"
                            id="descuento"
                            class="form-control-bmz cdescuento cdescuento_'.$rowcobrar.'"
                            onclick="activardescuento('.$rowcobrar.')"
                            value="0" readonly>
                      </td>
                      <td><a class="btn-floating waves-effect waves-light purple lightrn-1" onclick="deleteprefactura('.$idfactura.')"><i class="material-icons">delete_forever</i></a></td>
                    </tr>';
                    $rowcobrar++;
            }
            if($excedentec > 0){// excedentes a color        
             $html.='<tr class="datos_agregagados datos_agregagados_'.$idfactura.' datos_agregagados_periodo_'.$pediodoids.'">
                      <td>
                        <input id="selectedequipo" name="selectedequipo" type="hidden" value="0" readonly>
                        <input id="selectedprefactura" name="selectedprefactura" type="hidden" value="'.$idfactura.'" readonly>
                        <input id="facturarenta" name="facturarenta" type="hidden" value="1" readonly>
                        <input id="facturaexcedente" name="facturaexcedente" type="hidden" value="1" readonly>
                        <input id="cantidad" name="cantidad" type="text" value="1" class="excedente info cant_'.$rowcobrar.'" data-cantidad="'.$cant_ext_color.'" data-precioexc="'.$prec_ext_color.'" data-preid="'.$rowcobrar.'" readonly></td>
                      <td><div style="width:250px">
                          <select id="unidadsat" class="unidadsat browser-default">
                          '.$option_unidad_renta.'
                          </select>
                        <div>
                      </td>
                      <td>
                          <div style="width:250px">
                            <select id="conseptosat" class="conseptosat browser-default">
                            '.$option_concepto_renta.'
                            </select>
                          <div>
                      </td>
                      <td><input id="descripcion" name="descripcion" type="text" value="'.$descrip_fac.'Excedente color del '.$fechaie.' al '.$fechafe.'"></td>
                      <td><input id="precio" name="precio" class="precio excedente precio_'.$rowcobrar.'" type="text" value="'.$excedentec.'" readonly></td>
                      <td>
                          <input type="number"
                            name="descuento"
                            id="descuento"
                            class="form-control-bmz cdescuento cdescuento_'.$rowcobrar.'"
                            onclick="activardescuento('.$rowcobrar.')"
                            value="0" readonly>
                      </td>
                      <td><a class="btn-floating waves-effect waves-light purple lightrn-1" onclick="deleteprefactura('.$idfactura.')"><i class="material-icons">delete_forever</i></a></td>
                    </tr>';  
                    $rowcobrar++;  
            }            
        }else if($tipo == 3){
            if($excedente > 0){ // excedentes a monocromatico        
              $html.='<tr class="datos_agregagados datos_agregagados_'.$idfactura.' datos_agregagados_periodo_'.$pediodoids.'">
                      <td>
                        <input id="selectedequipo" name="selectedequipo" type="hidden" value="0" readonly>
                        <input id="selectedprefactura" name="selectedprefactura" type="hidden" value="'.$idfactura.'" readonly>
                        <input id="facturarenta" name="facturarenta" type="hidden" value="0" readonly>
                        <input id="facturaexcedente" name="facturaexcedente" type="hidden" value="1" readonly>
                        <input id="cantidad" name="cantidad" type="text" value="1" class="excedente info cant_'.$rowcobrar.'" data-cantidad="'.$cant_ext_mono.'" data-precioexc="'.$prec_ext_mono.'" data-preid="'.$rowcobrar.'" readonly></td>
                      <td>
                        <div style="width:250px">
                          <select id="unidadsat" class="unidadsat browser-default">
                          '.$option_unidad_renta.'
                          </select>
                        <div>
                      </td>
                      <td>
                          <div style="width:250px">
                            <select id="conseptosat" class="conseptosat browser-default">
                            '.$option_concepto_renta.'
                            </select>
                          <div>
                      </td>
                      <td><input id="descripcion" name="descripcion" type="text" value="'.$descrip_fac.'Excedente monocromatico del '.$fechaie.' al '.$fechafe.'"></td>
                      <td><input id="precio" name="precio" class="precio  excedente precio_'.$rowcobrar.'" type="text" value="'.$excedente.'" readonly></td>
                      <td>
                          <input type="number"
                            name="descuento"
                            id="descuento"
                            class="form-control-bmz cdescuento cdescuento_'.$rowcobrar.'"
                            onclick="activardescuento('.$rowcobrar.')"
                            value="0" readonly>
                      </td>
                      <td><a class="btn-floating waves-effect waves-light purple lightrn-1" onclick="deleteprefactura('.$idfactura.')"><i class="material-icons">delete_forever</i></a></td>
                    </tr>';
                    $rowcobrar++;
            }
            if($excedentec > 0){ // excedentes a monocromatico        
              $html.='<tr class="datos_agregagados datos_agregagados_'.$idfactura.' datos_agregagados_periodo_'.$pediodoids.'">
                      <td>
                        <input id="selectedequipo" name="selectedequipo" type="hidden" value="0" readonly>
                        <input id="selectedprefactura" name="selectedprefactura" type="hidden" value="'.$idfactura.'" readonly>
                        <input id="facturarenta" name="facturarenta" type="hidden" value="0" readonly>
                        <input id="facturaexcedente" name="facturaexcedente" type="hidden" value="1" readonly>
                        <input id="cantidad" name="cantidad" type="text" value="1" class="excedente info cant_'.$rowcobrar.'" data-cantidad="'.$cant_ext_color.'" data-precioexc="'.$prec_ext_color.'" data-preid="'.$rowcobrar.'" readonly></td>
                      <td>
                        <div style="width:250px">
                          <select id="unidadsat" class="unidadsat browser-default">
                          '.$option_unidad_renta.'
                          </select>
                        <div>
                      </td>
                      <td>
                          <div style="width:250px">
                            <select id="conseptosat" class="conseptosat browser-default">
                            '.$option_concepto_renta.'
                            </select>
                          <div>
                      </td>
                      <td><input id="descripcion" name="descripcion" type="text" value="'.$descrip_fac.'Excedente color del '.$fechaie.' al '.$fechafe.'"></td>
                      <td><input id="precio" name="precio" class="precio  excedente precio_'.$rowcobrar.'" type="text" value="'.$excedentec.'" readonly></td>
                      <td>
                          <input type="number"
                            name="descuento"
                            id="descuento"
                            class="form-control-bmz cdescuento cdescuento_'.$rowcobrar.'"
                            onclick="activardescuento('.$rowcobrar.')"
                            value="0" readonly>
                      </td>
                      <td><a class="btn-floating waves-effect waves-light purple lightrn-1" onclick="deleteprefactura('.$idfactura.')"><i class="material-icons">delete_forever</i></a></td>
                    </tr>';
                $rowcobrar++;
            }
        }
      }else{
        $resulte = $this->ModeloCatalogos->db14_getselectwheren('alta_rentas_prefactura_d',array('prefId'=>$idfactura));
        foreach ($resulte->result() as $item) {
         
        //$equipos = $this->input->post('equipos');
        //$DATAeq = json_decode($equipos);
        //for ($i=0;$i<count($DATAeq);$i++) {
          $equiposfacturar=$this->ModeloCatalogos->datosequiporentafactura($idfactura,$item->prefdId,$item->serieId,$item->tipo);
          foreach ($equiposfacturar as $item) {
            if($tipo == 1){//renta
              if ($item->statusfacturar==0) {
                if ($item->renta>0) {
                  if($item->tipo==1){
                  $html.='<tr class="datos_agregagados datos_agregagados_'.$item->prefdId.' datos_agregagados_periodo_'.$pediodoids.'">
                      <td>
                        <input id="selectedequipo" name="selectedequipo" type="hidden" value="'.$item->prefdId.'" readonly>
                        <input id="selectedprefactura" name="selectedprefactura" type="hidden" value="0" readonly>
                        <input id="facturarenta" name="facturarenta" type="hidden" value="1" readonly>
                        <input id="facturaexcedente" name="facturaexcedente" type="hidden" value="0" readonly>
                        <input id="cantidad" name="cantidad" type="text" value="1" readonly></td>
                      <td>
                          <div style="width:250px">
                            <select id="unidadsat" class="unidadsat browser-default">
                            '.$option_unidad_renta.'
                            </select>
                          <div>
                      </td>
                      <td>
                        <div style="width:250px">
                          <select id="conseptosat" class="conseptosat browser-default">
                          '.$option_concepto_renta.'
                          </select>
                        <div>
                      </td>
                      <td><input id="descripcion" name="descripcion" type="text" value="Renta '.$item->modelo.' '.$item->serie.' Monocromaticas del '.$fechai.' al '.$fechaf.'"></td>
                      <td><input id="precio" name="precio" class="precio" type="text" value="'.$item->renta.'" readonly></td>
                      <td>
                          <input type="number"
                            name="descuento"
                            id="descuento"
                            class="form-control-bmz cdescuento cdescuento_'.$rowcobrar.'"
                            onclick="activardescuento('.$rowcobrar.')"
                            value="0" readonly>
                      </td>
                      <td><a class="btn-floating waves-effect waves-light purple lightrn-1" onclick="deleteprefactura('.$item->prefdId.')"><i class="material-icons">delete_forever</i></a></td>
                    </tr>';
                    $rowcobrar++;
                  }
                }
                if ($item->rentac>0) {
                  if($item->tipo==2){
                  $html.='<tr class="datos_agregagados datos_agregagados_'.$item->prefdId.' datos_agregagados_periodo_'.$pediodoids.'">
                      <td>
                        <input id="selectedequipo" name="selectedequipo" type="hidden" value="'.$item->prefdId.'" readonly>
                        <input id="selectedprefactura" name="selectedprefactura" type="hidden" value="0" readonly>
                        <input id="facturarenta" name="facturarenta" type="hidden" value="1" readonly>
                        <input id="facturaexcedente" name="facturaexcedente" type="hidden" value="0" readonly>
                        <input id="cantidad" name="cantidad" type="text" value="1" readonly></td>
                      <td>
                          <div style="width:250px">
                            <select id="unidadsat" class="unidadsat browser-default">
                            '.$option_unidad_renta.'
                            </select>
                          <div>
                      </td>
                      <td>
                        <div style="width:250px">
                          <select id="conseptosat" class="conseptosat browser-default">
                          '.$option_concepto_renta.'
                          </select>
                        <div>
                      </td>
                      <td><input id="descripcion" name="descripcion" type="text" value="Renta '.$item->modelo.' '.$item->serie.' Color del '.$fechai.' al '.$fechaf.'"></td>
                      <td><input id="precio" name="precio" class="precio" type="text" value="'.$item->rentac.'" readonly></td>
                      <td>
                          <input type="number"
                            name="descuento"
                            id="descuento"
                            class="form-control-bmz cdescuento cdescuento_'.$rowcobrar.'"
                            onclick="activardescuento('.$rowcobrar.')"
                            value="0" readonly>
                      </td>
                      <td><a class="btn-floating waves-effect waves-light purple lightrn-1" onclick="deleteprefactura('.$item->prefdId.')"><i class="material-icons">delete_forever</i></a></td>
                    </tr>';
                    $rowcobrar++;
                  }
                }
                
              }
            }
            if($tipo == 3){//excedentes
              if ($item->statusfacturae==0) {
                if ($item->totalexcedente>0) {
                  $html.='<tr class="datos_agregagados datos_agregagados_'.$item->prefdId.' datos_agregagados_periodo_'.$pediodoids.'">
                      <td>
                        <input id="selectedequipo" name="selectedequipo" type="hidden" value="'.$item->prefdId.'" readonly>
                        <input id="selectedprefactura" name="selectedprefactura" type="hidden" value="0" readonly>
                        <input id="facturarenta" name="facturarenta" type="hidden" value="0" readonly>
                        <input id="facturaexcedente" name="facturaexcedente" type="hidden" value="1" readonly>
                        <input id="cantidad" name="cantidad" type="text" value="1" class="excedente info cant_'.$item->prefdId.'" data-cantidad="'.$item->excedentes.'" data-precioexc="'.$item->costoexcedente.'" data-preid="'.$item->prefdId.'" readonly></td>
                      <td>
                          <div style="width:250px">
                            <select id="unidadsat" class="unidadsat browser-default">
                            '.$option_unidad_renta.'
                            </select>
                          <div>
                      </td>
                      <td>
                        <div style="width:250px">
                          <select id="conseptosat" class="conseptosat browser-default">
                          '.$option_concepto_renta.'
                          </select>
                        <div>
                      </td>
                      <td><input id="descripcion" name="descripcion" type="text" value="Excedente '.$item->modelo.' '.$item->serie.' Monocromaticas del '.$fechaie.' al '.$fechafe.'"></td>
                      <td><input id="precio" name="precio" class="precio excedente precio_'.$item->prefdId.'" type="text" value="'.$item->totalexcedente.'" readonly></td>
                      <td>
                          <input type="number"
                            name="descuento"
                            id="descuento"
                            class="form-control-bmz cdescuento cdescuento_'.$rowcobrar.'"
                            onclick="activardescuento('.$rowcobrar.')"
                            value="0" readonly>
                      </td>
                      <td><a class="btn-floating waves-effect waves-light purple lightrn-1" onclick="deleteprefactura('.$item->prefdId.')"><i class="material-icons">delete_forever</i></a></td>
                    </tr>';
                    $rowcobrar++;
                }
              }
            }
            if($tipo == 2){//renta y excendentes
              if ($item->statusfacturar==0) {
                if ($item->renta>0) {
                  if($item->tipo==1){
                  $html.='<tr class="datos_agregagados datos_agregagados_'.$item->prefdId.' datos_agregagados_periodo_'.$pediodoids.'">
                      <td>
                        <input id="selectedequipo" name="selectedequipo" type="hidden" value="'.$item->prefdId.'" readonly>
                        <input id="selectedprefactura" name="selectedprefactura" type="hidden" value="0" readonly>
                        <input id="facturarenta" name="facturarenta" type="hidden" value="1" readonly>
                        <input id="facturaexcedente" name="facturaexcedente" type="hidden" value="1" readonly>
                        <input id="cantidad" name="cantidad" type="text" value="1" readonly></td>
                      <td>
                          <div style="width:250px">
                            <select id="unidadsat" class="unidadsat browser-default">
                            '.$option_unidad_renta.'
                            </select>
                          <div>
                      </td>
                      <td>
                        <div style="width:250px">
                          <select id="conseptosat" class="conseptosat browser-default">
                          '.$option_concepto_renta.'
                          </select>
                        <div>
                      </td>
                      <td><input id="descripcion" name="descripcion" type="text" value="Renta '.$item->modelo.' '.$item->serie.' Monocromaticas del '.$fechai.' al '.$fechaf.'"></td>
                      <td><input id="precio" name="precio" class="precio" type="text" value="'.$item->renta.'" readonly></td>
                      <td>
                          <input type="number"
                            name="descuento"
                            id="descuento"
                            class="form-control-bmz cdescuento cdescuento_'.$rowcobrar.'"
                            onclick="activardescuento('.$rowcobrar.')"
                            value="0" readonly>
                      </td>
                      <td><a class="btn-floating waves-effect waves-light purple lightrn-1" onclick="deleteprefactura('.$item->prefdId.')"><i class="material-icons">delete_forever</i></a></td>
                    </tr>';
                    $rowcobrar++;
                    }
                  }
                  if ($item->rentac>0) {
                    if($item->tipo==1){
                  $html.='<tr class="datos_agregagados datos_agregagados_'.$item->prefdId.' datos_agregagados_periodo_'.$pediodoids.'">
                      <td>
                        <input id="selectedequipo" name="selectedequipo" type="hidden" value="'.$item->prefdId.'" readonly>
                        <input id="selectedprefactura" name="selectedprefactura" type="hidden" value="0" readonly>
                        <input id="facturarenta" name="facturarenta" type="hidden" value="1" readonly>
                        <input id="facturaexcedente" name="facturaexcedente" type="hidden" value="1" readonly>
                        <input id="cantidad" name="cantidad" type="text" value="1" readonly></td>
                      <td>
                          <div style="width:250px">
                            <select id="unidadsat" class="unidadsat browser-default">
                            '.$option_unidad_renta.'
                            </select>
                          <div>
                      </td>
                      <td>
                        <div style="width:250px">
                          <select id="conseptosat" class="conseptosat browser-default">
                          '.$option_concepto_renta.'
                          </select>
                        <div>
                      </td>
                      <td><input id="descripcion" name="descripcion" type="text" value="Renta '.$item->modelo.' '.$item->serie.' Color del '.$fechai.' al '.$fechaf.'"></td>
                      <td><input id="precio" name="precio" class="precio" type="text" value="'.$item->rentc.'" readonly></td>
                      <td>
                          <input type="number"
                            name="descuento"
                            id="descuento"
                            class="form-control-bmz cdescuento cdescuento_'.$rowcobrar.'"
                            onclick="activardescuento('.$rowcobrar.')"
                            value="0" readonly>
                      </td>
                      <td><a class="btn-floating waves-effect waves-light purple lightrn-1" onclick="deleteprefactura('.$item->prefdId.')"><i class="material-icons">delete_forever</i></a></td>
                    </tr>';
                    $rowcobrar++;
                  }
                  }
              }
              if ($item->statusfacturae==0) {
                if ($item->totalexcedente>0) {
                  $monocolor='';
                  if($item->tipo==1){
                    $monocolor='Monocromaticas';
                  }
                  if($item->tipo==2){
                    $monocolor='Color';
                  }
                  $html.='<tr class="datos_agregagados datos_agregagados_'.$item->prefdId.' datos_agregagados_periodo_'.$pediodoids.'">
                      <td>
                        <input id="selectedequipo" name="selectedequipo" type="hidden" value="'.$item->prefdId.'" readonly>
                        <input id="selectedprefactura" name="selectedprefactura" type="hidden" value="0" readonly>
                        <input id="facturarenta" name="facturarenta" type="hidden" value="1" readonly>
                        <input id="facturaexcedente" name="facturaexcedente" type="hidden" value="1" readonly>
                        <input id="cantidad" name="cantidad" type="text" value="1" class="excedente info cant_'.$item->prefdId.'" data-cantidad="'.$item->excedentes.'" data-precioexc="'.$item->costoexcedente.'" data-preid="'.$item->prefdId.'" readonly></td>
                      <td>
                          <div style="width:250px">
                            <select id="unidadsat" class="unidadsat browser-default">
                            '.$option_unidad_renta.'
                            </select>
                          <div>
                      </td>
                      <td>
                        <div style="width:250px">
                          <select id="conseptosat" class="conseptosat browser-default">
                          '.$option_concepto_renta.'
                          </select>
                        <div>
                      </td>
                      <td><input id="descripcion" name="descripcion" type="text" value="Excedente '.$item->modelo.' '.$item->serie.' '.$monocolor.' del '.$fechaie.' al '.$fechafe.'"></td>
                      <td><input id="precio" name="precio" class="precio excedente precio_'.$item->prefdId.'" type="text" value="'.$item->totalexcedente.'" readonly></td>
                      <td>
                          <input type="number"
                            name="descuento"
                            id="descuento"
                            class="form-control-bmz cdescuento cdescuento_'.$rowcobrar.'"
                            onclick="activardescuento('.$rowcobrar.')"
                            value="0" readonly>
                      </td>
                      <td><a class="btn-floating waves-effect waves-light purple lightrn-1" onclick="deleteprefactura('.$item->prefdId.')"><i class="material-icons">delete_forever</i></a></td>
                    </tr>';
                    $rowcobrar++;
                }
              }
            }
            
          }
        //}
        }
      }







      /*
      if($tipo == 1){// rentas
        $html.='<tr class="datos_agregagados_'.$idfactura.'">
                  <td>
                      <input id="selectedequipo" type="hidden" value="0" readonly>
                      <input id="cantidad" name="cantidad" type="text" value="1" readonly></td>
                  <td>
                      <div style="width:250px">
                        <select id="unidadsat" class="unidadsat browser-default">
                        </select>
                      <div>
                  </td>
                  <td>
                    <div style="width:250px">
                          <select id="conseptosat" class="conseptosat browser-default">
                          </select>
                        <div>
                  </td>
                  <td><input id="descripcion" name="descripcion" type="text" value="Renta del '.$fechai.' al '.$fechaf.'"></td>
                  <td><input id="precio" name="precio" class="precio" type="text" value="'.$totals.'" readonly></td>
                  <td><a class="btn-floating waves-effect waves-light purple lightrn-1" onclick="deleteprefactura('.$idfactura.')"><i class="material-icons">delete_forever</i></a></td>
                </tr>';
      }else if($tipo == 2){ // rentas y excedentes
        // rentas
        $html.='<tr class="datos_agregagados_'.$idfactura.'">
                  <td>
                    <input id="selectedequipo" type="hidden" value="0" readonly>
                    <input id="cantidad" name="cantidad" type="text" value="1" readonly></td>
                  <td>
                    <div style="width:250px">
                        <select id="unidadsat" class="unidadsat browser-default">
                        </select>
                      <div>
                  </td>
                  <td>
                    <div style="width:250px">
                      <select id="conseptosat" class="conseptosat browser-default">
                      </select>
                    <div>
                  </td>
                  <td><input id="descripcion" name="descripcion" type="text" value="Renta del '.$fechai.' al '.$fechaf.'"></td>
                  <td><input id="precio" name="precio" class="precio" type="text" value="'.$totals.'" readonly></td>
                  <td><a class="btn-floating waves-effect waves-light purple lightrn-1" onclick="deleteprefactura('.$idfactura.')"><i class="material-icons">delete_forever</i></a></td>
                </tr>';

          if($excedente > 0){ // excedentes monocromatico        
          $html.='<tr class="datos_agregagados_'.$idfactura.'">
                    <td>
                      <input id="selectedequipo" type="hidden" value="0" readonly>
                      <input id="cantidad" name="cantidad" type="text" value="1" readonly></td>
                    <td>
                      <div style="width:250px">
                        <select id="unidadsat" class="unidadsat browser-default">
                        </select>
                      <div>
                    </td>
                    <td>
                      <div style="width:250px">
                        <select id="conseptosat" class="conseptosat browser-default">
                        </select>
                      <div>
                    </td>
                    <td><input id="descripcion" name="descripcion" type="text" value="EXCEDENTE del '.$fechaie.' al '.$fechafe.'"></td>
                    <td><input id="precio" name="precio" class="precio" type="text" value="'.$excedente.'" readonly></td>
                    <td></td>
                  </tr>';
          }
          if($excedentec > 0){// excedentes a color        
          $html.='<tr class="datos_agregagados_'.$idfactura.'">
                    <td>
                      <input id="selectedequipo" type="hidden" value="0" readonly>
                      <input id="cantidad" name="cantidad" type="text" value="1" readonly></td>
                    <td>
                      <div style="width:250px">
                        <select id="unidadsat" class="unidadsat browser-default">
                        </select>
                      <div>
                    </td>
                    <td>
                      <div style="width:250px">
                        <select id="conseptosat" class="conseptosat browser-default">
                        </select>
                      <div>
                    </td>
                    <td><input id="descripcion" name="descripcion" type="text" value="EXCEDENTE del '.$fechaie.' al '.$fechafe.'"></td>
                    <td><input id="precio" name="precio" class="precio" type="text" value="'.$excedentec.'" readonly></td>
                    <td></td>
                  </tr>';    
          }            
      }else if($tipo == 3){
          if($excedente > 0){ // excedentes a color        
          $html.='<tr class="datos_agregagados_'.$idfactura.'">
                    <td>
                      <input id="selectedequipo" type="hidden" value="0" readonly>
                      <input id="cantidad" name="cantidad" type="text" value="1" readonly></td>
                    <td><input id="unidadsat" name="" type="text" value=""></td>
                    <td>
                      <div style="width:250px">
                        <select id="conseptosat" class="conseptosat browser-default">
                        </select>
                      <div>
                    </td>
                    <td><input id="descripcion" name="descripcion" type="text" value="EXCEDENTE del '.$fechaie.' al '.$fechafe.'"></td>
                    <td><input id="precio" name="precio" class="precio" type="text" value="'.$excedente.'" readonly></td>
                    <td><a class="btn-floating waves-effect waves-light purple lightrn-1" onclick="deleteprefactura('.$idfactura.')"><i class="material-icons">delete_forever</i></a></td>
                  </tr>';
          }
      } */
      echo $html;
    }
    public function generarfacturas($FacturasId,$file){
      //$file 1 factura 2xml 3 completo 4 detalle
      $data["Nombrerasonsocial"]='Nombrerasonsocial';
      $data['rrfc']='erftgyuio5678';
      $data['rdireccion']='direccion conocida';
      $data['regimenf']='601 General de Ley Personas Morales';
      $idcliente=0;
      $data['empresa']='';
      $data['FacturasId']=$FacturasId;
      $factura = $this->ModeloCatalogos->getselectwheren('f_facturas',array('FacturasId'=>$FacturasId));
      foreach ($factura->result() as $item) {
          $MetodoPago=$item->MetodoPago;
          $FormaPago=$item->FormaPago;
          $observaciones=$item->observaciones;
          $Folio=str_pad($item->Folio, 4, "0", STR_PAD_LEFT);
          $isr=$item->isr;
          $uuid=$item->uuid;
          $nocertificadosat=$item->nocertificadosat;
          $certificado=$item->nocertificado;
          $fechatimbre=$item->fechatimbre;
          $uso_cfdi=$item->uso_cfdi;
          $cliente=$item->Nombre;
          $clirfc=$item->Rfc;
          $clidireccion=$item->Direccion;
          $numproveedor=$item->numproveedor;
          $numordencompra=$item->numordencompra;
          $moneda=$item->moneda;
          $subtotal  = $item->subtotal;
          $iva = $item->iva;
          $total = $item->total;
          $ivaretenido = $item->ivaretenido;
          $cedular = $item->cedular;
          $selloemisor = $item->sellocadena;
          $sellosat = $item->sellosat;
          $cadenaoriginal = $item->cadenaoriginal;
          $tarjeta=$item->tarjeta;
          $tipoComprobante=$item->TipoComprobante;
          $cp=$item->Cp;
          
          $isr = $item->isr; 
          $ivaretenido = $item->ivaretenido; 
          $cincoalmillarval = $item->cincoalmillarval; 
          $outsourcing = $item->outsourcing; 
          $r_regimenfiscal = $item->r_regimenfiscal; 

          $f_relacion = $item->f_relacion;
          $f_r_tipo = $item->f_r_tipo;
          $f_r_uuid = $item->f_r_uuid;
          $idcliente =$item->Clientes_ClientesId;
          $serie = $item->serie;
      }
          if($serie=='U'){
            $id_info=1;
          }else{
            $id_info=2;
            $data['regimenf']='626 Régimen Simplificado de Confianza';
          }
          $confic=$this->ModeloCatalogos->db10_getselectwheren('configuracionCotizaciones',array('id'=>$id_info));
          $data['confic']=$confic->row();

          $confif=$this->ModeloCatalogos->db10_getselectwheren('f_configuraciones',array('ConfiguracionesId'=>$id_info));
          $data['confif']=$confif->row();

          $dcliente=$this->ModeloCatalogos->db10_getselectwheren('clientes',array('id'=>$idcliente));
        foreach ($dcliente->result() as $item) {
            $empresa=$item->empresa;
        }
        $data['empresa']=$empresa;
      $facturadetalles = $this->Rentas_model->facturadetalle($FacturasId);
          $data['cp']=$cp;
      //=============
          $data['FormaPago']=$MetodoPago;
          //log_message('error', 'FormaPago c: '.$MetodoPago);
          $data['FormaPagol']=$this->gf_FormaPago($MetodoPago);
          //$data['FormaPagol']='vv';
        //=================
          $data['MetodoPago']=$FormaPago;
          $data['MetodoPagol']=$this->gt_MetodoPago($FormaPago);
          //$data['MetodoPagol']='cc';
        //=================
          $data['observaciones']=$observaciones;
          $data['Folio']=$Folio;
        //=================
          $data["isr"]=$isr;
        //=================
          $data['folio_fiscal']=$uuid;
          $data['nocertificadosat']=$nocertificadosat;
          $data['certificado']=$certificado;
          $data['fechatimbre']=$fechatimbre;
          $data['cfdi']=$this->gt_uso_cfdi($uso_cfdi);
          $data['cliente']=$cliente;
          $data['clirfc']=$clirfc;
          $data['clidireccion']=$clidireccion;
          $data['numproveedor']=$numproveedor;
          $data['numordencompra']=$numordencompra;
          $data['moneda']=$moneda;
          $data['subtotal']=$subtotal;
          $data['iva']=$iva;

          $data['isr']=$isr;
          $data['ivaretenido']=$ivaretenido;
          $data['cincoalmillarval']=$cincoalmillarval;
          $data['outsourcing']=$outsourcing;

          $data['total']=$total;
          $data['ivaretenido']=$ivaretenido;
          $data['cedular']=$cedular;
          $data['selloemisor']=$selloemisor;
          $data['sellosat']=$sellosat;
          $data['cadenaoriginal']=$cadenaoriginal;
          $data['tarjeta']=$tarjeta;
          $data['facturadetalles']=$facturadetalles->result();
          $data['tipoComprobante']=$tipoComprobante;
          $data['detalleperidofactura']= $this->Rentas_model->detalleperidofactura($FacturasId);
          $data['detalleconsumiblesfolios']= $this->Rentas_model->detalleconsumiblesfolios($FacturasId);
          $data['detallecimages']= $this->Rentas_model->detalleimagesRenta($FacturasId);
          $data['RegimenFiscalReceptor']=$this->Modelofacturas->regimenf($r_regimenfiscal);
          $data['f_relacion']=$f_relacion;
          $data['f_r_tipo']=$f_r_tipo;
          $data['f_r_uuid']=$f_r_uuid;



          if ($file==1) {
            $this->load->view('Reportes/factura',$data);
          }elseif ($file==2) {
            # code...
          }elseif ($file==3) {
            $this->load->view('Reportes/facturac',$data);
          }elseif ($file==4) {
            $this->load->view('Reportes/facturad',$data);
          }else{

          }
      //$this->load->view('Reportes/factura2',$data);
    }
    public function cancelarFactura(){
      $idFact = $this->input->post('idfactura');
      $idc = $this->ModeloCatalogos->updateCatalogo('f_facturas',array('estado'=>0),array('FacturasId'=>$idFact));
      echo $idc;
    }
    public function reFactura(){
      $idFact = $this->input->post('idfactura');
      $idCont = $this->input->post('idcontrato');
      $datos = $this->Rentas_model->getFactura($idFact);
      foreach ($datos as $value) {
        unset($value->FacturasId);
        unset($value->Estado);
        //$value->Estado=1;
        $idn = $this->ModeloCatalogos->Insert('f_facturas',$value);
      }
      $datoRel = $this->Rentas_model->getFacturaContrato($idFact,$idCont);
      foreach ($datoRel as $value) {
        unset($value->facId);
        unset($value->facturaId);
        $value->facturaId=$idn;

        $idn2 = $this->ModeloCatalogos->Insert('factura_prefactura',$value);
      }
      echo $idn;
      echo $idn2;
    }
    function gf_FormaPago($text){
      //log_message('error', 'gf_FormaPago:'.$text);
        if ($text=='Efectivo') {
          $textl='01 Efectivo';
        }elseif ($text=='ChequeNominativo') {
          $textl='02 Cheque Nominativo';
        }elseif ($text=='TransferenciaElectronicaFondos') {
          $textl='03 Transferencia ElectronicaFondos';
        }elseif ($text=='TarjetasDeCredito') {
          $textl='04 Tarjetas De Credito';
        }elseif ($text=='MonederoElectronico') {
          $textl='05 Monedero Electronico';
        }elseif ($text=='DineroElectronico') {
          $textl='06 Dinero Electronico';
        }elseif ($text=='Tarjetas digitales') {
          $textl='07 Tarjetas digitales';
        }elseif ($text=='ValesDeDespensa') {
          $textl='08 Vales De Despensa';
        }elseif ($text=='TarjetaDebito') {
          $textl='28 Tarjeta Debito';
        }elseif ($text=='TarjetaServicio') {
          $textl='29 Tarjeta Servicio';
        }elseif ($text=='Otros') {
          $textl='99 Por definir';
        }elseif ($text=='DacionPago') {
          $textl='12 Dacion Pago';
        }elseif ($text=='PagoSubrogacion') {
          $textl='13 Pago Subrogacion';
        }elseif ($text=='PagoConsignacion') {
          $textl='14 Pago Consignacion';
        }elseif ($text=='Condonacion') {
          $textl='15 Condonacion';
        }elseif ($text=='Compensacion') {
          $textl='17 Compensacion';
        }elseif ($text=='Novacion') {
          $textl='23 Novacion';
        }elseif ($text=='Confusion') {
          $textl='24 Confusion';
        }elseif ($text=='RemisionDeuda') {
          $textl='25 RemisionDeuda';
        }elseif ($text=='PrescripcionoCaducidad') {
          $textl='26 Prescripciono Caducidad';
        }elseif ($text=='SatisfaccionAcreedor') {
          $textl='27 Satisfaccion Acreedor';
        }elseif ($text=='AplicacionAnticipos') {
          $textl='30 Aplicacion Anticipos';
        }elseif ($text=='PorDefinir') {
          $textl='99 Por definir';
        }elseif ($text=='intermediariopagos') {
          $textl='31 Intermediario pagos';
        }
        if ($text=='01') {
          $textl='01 Efectivo';
        }elseif ($text=='02') {
          $textl='02 Cheque Nominativo';
        }elseif ($text=='03') {
          $textl='03 Transferencia ElectronicaFondos';
        }elseif ($text=='04') {
          $textl='04 Tarjetas De Credito';
        }elseif ($text=='05') {
          $textl='05 Monedero Electronico';
        }elseif ($text=='06') {
          $textl='06 Dinero Electronico';
        }elseif ($text=='07') {
          $textl='07 Tarjetas digitales';
        }elseif ($text=='08') {
          $textl='08 Vales De Despensa';
        }elseif ($text=='28') {
          $textl='28 Tarjeta Debito';
        }elseif ($text=='29') {
          $textl='29 Tarjeta Servicio';
        }elseif ($text=='99') {
          $textl='99 Por definir';
        }elseif ($text=='12') {
          $textl='12 Dacion Pago';
        }elseif ($text=='13') {
          $textl='13 Pago Subrogacion';
        }elseif ($text=='14') {
          $textl='14 Pago Consignacion';
        }elseif ($text=='15') {
          $textl='15 Condonacion';
        }elseif ($text=='17') {
          $textl='17 Compensacion';
        }elseif ($text=='23') {
          $textl='23 Novacion';
        }elseif ($text=='24') {
          $textl='24 Confusion';
        }elseif ($text=='25') {
          $textl='25 RemisionDeuda';
        }elseif ($text=='26') {
          $textl='26 Prescripciono Caducidad';
        }elseif ($text=='27') {
          $textl='27 Satisfaccion Acreedor';
        }elseif ($text=='30') {
          $textl='30 Aplicacion Anticipos';
        }elseif ($text=='99') {
          $textl='99 Por definir';
        }elseif ($text=='31') {
          $textl='31 Intermediario pagos';
        }
        //log_message('error', 'gf_FormaPago:'.$textl);
        return $textl; 
    }
    function gt_MetodoPago($text){
      //log_message('error', 'gt_MetodoPago:'.$text);
      if ($text=='PUE') {
          $textl='PUE Pago en una sola exhibicion';
      }else{
          $textl='PPD Pago en parcialidades o diferido';
      }
      //log_message('error', 'gt_MetodoPago:'.$textl);
      return $textl;
    }
    function gt_uso_cfdi($text){
          if ($text=='G01') {
            $textl=' G01 Adquisición de mercancias';
          }elseif ($text=='G02') {
            $textl=' G02 Devoluciones, descuentos o bonificaciones';
          }elseif ($text=='G03') {
            $textl=' G03 Gastos en general';
          }elseif ($text=='I01') {
            $textl=' I01 Construcciones';
          }elseif ($text=='I02') {
            $textl=' I02 Mobilario y equipo de oficina por inversiones';
          }elseif ($text=='I03') {
            $textl=' I03 Equipo de transporte';
          }elseif ($text=='I04') {
            $textl=' I04 Equipo de computo y accesorios';
          }elseif ($text=='I05') {
            $textl=' I05 Dados, troqueles, moldes, matrices y herramental';
          }elseif ($text=='I06') {
            $textl=' I06 Comunicaciones telefónicas';
          }elseif ($text=='I07') {
            $textl=' I07 Comunicaciones satelitales';
          }elseif ($text=='I08') {
            $textl=' I08 Otra maquinaria y equipo';
          }elseif ($text=='D01') {
            $textl=' D01 Honorarios médicos, dentales y gastos hospitalarios.';
          }elseif ($text=='D02') {
            $textl=' D02 Gastos médicos por incapacidad o discapacidad';
          }elseif ($text=='D03') {
            $textl=' D03 Gastos funerales.';
          }elseif ($text=='D04') {
            $textl=' D04 Donativos.';
          }elseif ($text=='D05') {
            $textl=' D05 Intereses reales efectivamente pagados por créditos hipotecarios (casa habitación).';
          }elseif ($text=='D06') {
            $textl=' >D06 Aportaciones voluntarias al SAR.';
          }elseif ($text=='D07') {
            $textl=' D07 Primas por seguros de gastos médicos.';
          }elseif ($text=='D08') {
            $textl=' D08 Gastos de transportación escolar obligatoria.';
          }elseif ($text=='D09') {
            $textl=' D09 Depósitos en cuentas para el ahorro, primas que tengan como base planes de pensiones.';
          }elseif ($text=='D10') {
            $textl=' D10 Pagos por servicios educativos (colegiaturas)';
          }elseif ($text=='P01') {
            $textl=' P01 Por definir';
          }elseif ($text=='S01') {
            $textl=' S01 Sin efectos fiscales';
          }else{
            $textl='';
          }
          return $textl;
    }
    function searchunidadsat(){
      $search = $this->input->get('search');
      $results=$this->Rentas_model->getseleclikeunidadsat($search);
        echo json_encode($results->result());
    }
    function searchconceptosat(){
      $search = $this->input->get('search');
      $results=$this->Rentas_model->getseleclikeconceptosa($search);
        echo json_encode($results->result());
    }
    function generafacturar(){
      $datas = $this->input->post();
      $tipof=$datas['tipof'];
      $tipofac = $datas['tipofac'];
      $tiporenta=$datas['tiporenta'];
      if(isset($datas['fac_pen'])){
        $fac_pen=$datas['fac_pen'];
      }else{
        $fac_pen=0;
      }
      if(isset($datas['conceptos'])){
            $conceptos = $datas['conceptos'];
            unset($datas['conceptos']);
      }
      if(isset($datas['equipos'])){
            $equipos = $datas['equipos'];
            unset($datas['equipos']);
      }
      $idcliente=$datas['idcliente'];
      $rfc=$datas['rfc'];
      
      $DATAc = json_decode($conceptos);
      //===========================
        for ($j=0;$j<count($DATAc);$j++) {
          $this->ModeloCatalogos->updateCatalogo('alta_rentas_prefactura',array('factura_excedente_pendiente'=>0),array('prefId'=>$DATAc[$j]->selectedprefactura));
        }
      //===========================
      $dcliente=$this->ModeloCatalogos->db14_getselectwheren('clientes',array('id'=>$idcliente));
      foreach ($dcliente->result() as $item) {
        $municipio=$item->municipio;
        $idestado=$item->estado;
      }
      $destado=$this->ModeloCatalogos->db14_getselectwheren('estado',array('EstadoId'=>$idestado));
      foreach ($destado->result() as $item) {
        $estado=$item->Nombre;
      }
      $direccion=$this->ModeloCatalogos->db14_getselectwheren('cliente_has_datos_fiscales',array('rfc'=>$rfc,'idCliente'=>$idcliente,'activo'=>1));
      foreach ($direccion->result() as $item) {
        $rfc_id = $item->id;
        $razon_social = $item->razon_social;
        $rfc = $item->rfc;
        $cp = $item->cp;
        $num_ext = $item->num_ext;
        $num_int = $item->num_int;
        $colonia = $item->colonia;
        $calle = $item->calle;
        if ($item->estado!=null) {
          $estado=$item->estado;
        }
        if ($item->municipio!=null) {
          $municipio=$item->municipio;
        }
        $localidad=$item->localidad;
        $RegimenFiscalReceptor=$item->RegimenFiscalReceptor;
      }
      $pais = 'MEXICO';
      $TipoComprobante='I';
      //===========================
        $info_subtotal=floatval($datas['subtotal']);
        $info_iva=floatval($datas['iva']);
        $info_total=floatval($datas['total']);
        $recalcular=1;
        if($datas['des']>0){
          $recalcular=0;
          $subtotaldes=$info_subtotal-floatval($datas['des']);

          $info_iva=round($subtotaldes*0.16,2);
          $info_total=$subtotaldes+$info_iva;

        }
        if($recalcular==1){
          $info_iva=round($info_subtotal*0.16,2);
          $info_total=$info_subtotal+$info_iva;
        }
      //=====================================
        if ($tipofac==1) {
          $folio = $this->Rentas_model->ultimoFolio('U') + 1;
        }else{
          $folio=0;
        }
      $data = array(
                "serie"         => 'U',
                "nombre"        => $razon_social,
                "direccion"     => $calle . ' No ' . $num_ext . ' ' . $num_int . ' Col: ' . 
                                   $colonia . ' , ' . 
                                   $localidad . ' , '.$municipio . ' , ' . $estado . ' . ', 
                "cp"            => $cp,
                "rfc"           => $rfc,
                "rfc_id"        => $rfc_id,
                "folio"         => $folio,
                "PaisReceptor"  =>$pais,
                "Clientes_ClientesId"       => $idcliente,
                //"status"        => 1,
                "TipoComprobante"=>$TipoComprobante,
                "usuario_id"       => $this->idpersonal,
                "creada_sesionId"=> $this->idpersonal,
                "rutaXml"           => '',
                "FormaPago"         => $datas['FormaPago'],
                "tarjeta"       => $datas['tarjeta'],
                "MetodoPago"        => $this->getCodigoMetodoPago($datas['MetodoPago']),
                "ordenCompra"   => '',//no se ocupa
                "moneda"        => $datas['moneda'],
                "observaciones" => $datas['observaciones'],
                "numproveedor"  => $datas['numproveedor'],
                "numordencompra"=> $datas['numordencompra'],
                "Lote"          => '',//no se ocupa
                "Paciente"      => '',//no se ocupa
                "Caducidad"     => '',//no se ocupa
                "uso_cfdi"      => $datas['uso_cfdi'],
                "subtotal"      => $info_subtotal,
                "iva"           => $info_iva,
                "total"         => $info_total,
                "honorario"     => $datas['subtotal'],
                "ivaretenido"   => $datas['vriva'],
                "isr"           => $datas['visr'],
                "cincoalmillarval"  => $datas['v5millar'],
                "CondicionesDePago" => $datas['CondicionesDePago'],
                "outsourcing" => $datas['outsourcing'],
                "facturaabierta"=>0,

                "f_relacion"    => $datas['f_r'],
                "f_r_tipo"      => $datas['f_r_t'],
                "f_r_uuid"      => $datas['f_r_uuid'],
                "r_regimenfiscal"=>$RegimenFiscalReceptor,
                "save"=>$tipofac
            
            );
      if(isset($datas['pg_periodicidad'])){
          $data['pg_periodicidad']=$datas['pg_periodicidad'];
      }
      if(isset($datas['pg_meses'])){
          $data['pg_meses']=$datas['pg_meses'];
      }
      if(isset($datas['pg_anio'])){
          $data['pg_anio']=$datas['pg_anio'];
      }

      $FacturasId=$this->ModeloCatalogos->Insert('f_facturas',$data);
      
      for ($i=0;$i<count($DATAc);$i++) {

            $dataco['FacturasId']=$FacturasId;
            $dataco['Cantidad'] =$DATAc[$i]->Cantidad;
            $dataco['Unidad'] =$DATAc[$i]->Unidad;
            $dataco['servicioId'] =$DATAc[$i]->servicioId;
            $dataco['Descripcion'] =trim($DATAc[$i]->Descripcion);
            $dataco['Descripcion2'] =$DATAc[$i]->Descripcion2;
            $dataco['Cu'] =$DATAc[$i]->Cu;
            $dataco['descuento'] =$DATAc[$i]->descuento;
            $dataco['Importe'] =($DATAc[$i]->Cantidad*$DATAc[$i]->Cu);
            if($this->trunquearredondear==0){
              $dataco['iva'] =round(($DATAc[$i]->Cantidad*$DATAc[$i]->Cu)*0.16, 4);
            }else{
              $dataco['iva'] = floor((($DATAc[$i]->Cantidad*$DATAc[$i]->Cu)*100))/100;
            }
            $this->ModeloCatalogos->Insert('f_facturas_servicios',$dataco);
            $DATAc[$i]->selectedequipo;
            $DATAc[$i]->facturarenta;
            $DATAc[$i]->facturaexcedente;
            if (intval($DATAc[$i]->selectedequipo)==0) { // para el caso de las globales
              if($DATAc[$i]->facturarenta==1){
                $datastatusfg['factura_renta']=1;
                $datastatusfgd['statusfacturar']=1;
              }
              if ($DATAc[$i]->facturaexcedente==1) {
                $datastatusfg['factura_excedente']=1;
                $datastatusfgd['statusfacturae']=1;
              }
              $this->ModeloCatalogos->updateCatalogo('alta_rentas_prefactura',$datastatusfg,array('prefId'=>$DATAc[$i]->selectedprefactura));
              $this->ModeloCatalogos->updateCatalogo('alta_rentas_prefactura_d',$datastatusfgd,array('prefId'=>$DATAc[$i]->selectedprefactura));
            }else{
              if($DATAc[$i]->facturarenta==1){
                $datastatusfi['statusfacturar']=1;
              }
              if ($DATAc[$i]->facturaexcedente==1) {
                $datastatusfi['statusfacturae']=1;
              }
              $this->ModeloCatalogos->updateCatalogo('alta_rentas_prefactura_d',$datastatusfi,array('prefdId'=>$DATAc[$i]->selectedequipo));
            }
            if($fac_pen==1){
              $this->ModeloCatalogos->updateCatalogo('alta_rentas_prefactura',array('factura_excedente_pendiente'=>1),array('prefId'=>$DATAc[$i]->selectedprefactura));
            }
      }
      //=======================================
      if($datas['prefacturaId']>0){
        $this->ModeloCatalogos->Insert('factura_prefactura',array('contratoId'=>$datas['contratoId'],'prefacturaId'=>$datas['prefacturaId'],'facturaId'=>$FacturasId));
      }
      

      if (isset($datas['periodosextras'])) {
        $DATApe = json_decode($datas['periodosextras']);
        for ($l=0;$l<count($DATApe);$l++) {
          if($DATApe[$l]->periodo>0){
            $this->ModeloCatalogos->Insert('factura_prefactura',array('contratoId'=>$DATApe[$l]->contrato,'prefacturaId'=>$DATApe[$l]->periodo,'facturaId'=>$FacturasId));
          }
          
        }

        
      }
      
      if ($tiporenta==2) {
        $DATAe = json_decode($equipos);
        for ($i=0;$i<count($DATAe);$i++) {
            $prefdId =$DATAe[$i]->equipos;
            if ($tipof==1) {
              $datastatus['statusfacturar']=1;
            }elseif($tipof==2){
              $datastatus['statusfacturar']=1;
              $datastatus['statusfacturae']=1;
            }elseif($tipof==2){
              $datastatus['statusfacturae']=1;
            }else{
              $datastatus['statusfacturar']=0;
              $datastatus['statusfacturae']=0;
            }
            $this->ModeloCatalogos->updateCatalogo('alta_rentas_prefactura_d',$datastatus,array('prefdId'=>$prefdId));
        }
      }
      if($tipofac==1){
        $respuesta=$this->emitirfacturas($FacturasId);
      }else{
        $respuesta=array('resultado'=>'save','facturasid'=>$FacturasId);
        $this->ModeloCatalogos->updateCatalogo('f_facturas',array('Estado'=>2),array('FacturasId'=>$FacturasId));
      }
      
      echo json_encode($respuesta);
    }
    function listfacturas(){
      $idcontrato = $this->input->post('idcontrato');
      $tipo = $this->input->post('tipo');
      $results = $this->ModeloCatalogos->viewprefacturaslis($idcontrato,$tipo);
      $html='<table id="tablefacturaslis">
              <thead>
                <tr>
                  <th></th>
                  <th>Periodo</th>
                  <th>Folio</th>
                  <th>Cliente</th>
                  <th>RFC</th>
                  <th>Monto</th>
                  <th>Estatus</th>
                  <th>Fecha</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>';
        foreach ($results->result() as $item) {
          if ($item->statuspago==0) {
           $statuspagoc='';
          }else{
            $statuspagoc='green';
          }
          $html.='<tr>
                    <td>'.$item->FacturasId.'</td>
                    <td>'.$item->prefacturaId.'</td>
                    <td>'.$item->Folio.'</td>
                    <td>'.$item->Nombre.'</td>
                    <td>'.$item->Rfc.'</td>
                    <td>'.number_format($item->total, 2, '.', ' ').'</td>';
                    if($item->Estado==2){
                      $html.= "<td>Sin Timbrar</td>";
                    }elseif($item->Estado==1){
                      $html.= "<td>Timbrada</td>";
                    }elseif($item->Estado==0){
                      $html.= "<td>Cancelada</td>";
                    }
                    $html.='
                    <td>'.$item->fechatimbre.'</td>
                    <td>';
                if($item->Estado==1 or $item->Estado==0){
                $html.='<a 
                        class="b-btn b-btn-primary tooltipped btn_rcp_fac" 
                        href="'.base_url().'Configuracionrentas/generarfacturas/'.$item->FacturasId.'/1" 
                        target="_blank"
                        data-position="top" data-delay="50" data-tooltip="Factura"
                      >
                        <i class="fas fa-file-alt fa-2x"></i>
                      </a>
                      <a 
                        class="b-btn b-btn-primary tooltipped btn_rcp_xml" 
                        href="'.base_url().$item->rutaXml.'" 
                        target="_blank"
                        data-position="top" data-delay="50" data-tooltip="XML"
                      >
                        <i class="far fa-file-code fa-2x"></i>
                      </a>
                      <a 
                        class="b-btn b-btn-primary tooltipped btn_rcp_facc" 
                        href="'.base_url().'Configuracionrentas/generarfacturas/'.$item->FacturasId.'/3" 
                        target="_blank"
                        data-position="top" data-delay="50" data-tooltip="Completo"
                      >
                        <i class="fas fa-file-pdf fa-2x"></i>
                      </a>
                      <a 
                        class="b-btn b-btn-primary tooltipped btn_rcp_facd" 
                        href="'.base_url().'Reportes/rentadetalles/'.$item->prefacturaId.'/'.$idcontrato.'" 
                        target="_blank"
                        data-position="top" data-delay="50" data-tooltip="Detalles"
                      >
                        <i class="material-icons">assignment</i>
                      </a>';
                    }
                      if($item->Estado==1){
                      $html.='
                        <button type="button"
                          class="b-btn b-btn-primary tooltipped btn_rcp_faccan" style="background-color: red; text-color: white"
                          onclick="cancelar('.$item->FacturasId.','.$idcontrato.')";
                          data-position="top" data-delay="50" data-tooltip="Cancelar" id="cancelaF">
                          <i class="fas fa-ban fa-2x"></i>
                        </button> ';
                      }else{
                         $html.='<button type="button"
                          class="b-btn b-btn-primary tooltipped btn_rcp_facref" 
                          onclick="refacturar('.$item->FacturasId.','.$idcontrato.')";
                          data-position="top" data-delay="50" data-tooltip="Refacturar" id="refacturar">
                          <i class="material-icons">restore_page</i>
                        </button> ';
                      }
                      if ($item->Estado==2) {
                        $html.='<button type="button"
                          class="b-btn b-btn-primary tooltipped btn_rcp_facref" 
                          onclick="retimbrar('.$item->FacturasId.','.$idcontrato.')";
                          data-position="top" data-delay="50" data-tooltip="Retimbrar" id="refacturar">
                          <span class="fa-stack">
                          <i class="fas fa-file fa-stack-2x"></i>
                          <i class="fas fa-sync fa-stack-1x" style="color: red;"></i>
                          </span>
                        </button> ';
                      }

                        if ($item->Estado==2) {
                          $displayp='style="display:none"';
                        }else{
                          $displayp='';
                        }


                        $html.='<button type="button"
                          class="b-btn b-btn-primary tooltipped '.$statuspagoc.' modal_pagos verificar_'.$item->facId.'" 
                          onclick="facturapago('.$item->facId.','.$item->total.')";
                          data-position="top" data-delay="50" data-tooltip="Pagos" '.$displayp.' data-idpre="'.$item->facId.'">
                          <i class="fas fa-file-invoice-dollar fa-2x"></i>
                        </button>';

                    $html.='
                    </td>
                  </tr>';
        }
      $html.='</tbody>
            </table>';
      echo  $html;
    }
    function lisfolios(){
      $idcontrato = $this->input->post('idcontrato');
      $results = $this->ModeloCatalogos->viewlisfolioscli($idcontrato);


      $html='<table id="tablelisfolios">
              <thead>
                <tr>
                  <th>Consumible</th>
                  <th>Folio</th>
                  <th>Tecnico</th>
                  <th>Fecha Generado</th>
                  <th>Fecha Retorno</th>
                  <th>Perido</th>
                  <th></th>
                  <th>Comentario</th>
                </tr>
              <thead>
              <tbody>';
          $toner_stockcliente=0;
          $toner_retorno=0;
          $toner_pendiente=0;
        foreach ($results as $item) {
          if ($item->status==1) {
            $stock='<p style="background-color: #26d744; text-align: center; color:white; border-radius:5px; margin:0px;">Stock Cliente</p>';
            $toner_stockcliente++;
          }elseif($item->status==2){
            $stock='<p style="background-color: #00e5ff; text-align: center; color:white; border-radius:5px; margin:0px;">Retorno</p>';
            $toner_retorno++;
          }else{
            $stock='<p style="background-color: #ea6749; text-align: center; color:white; border-radius:5px; margin:0px;">Pendiente</p>';
            $toner_pendiente++;
          }
          $html.='<tr>
                    <td>'.$item->modelo.'</td>
                    <td>'.$item->foliotext.'</td>
                    <td>'.$item->tecnico.'</td>
                    <td>'.$item->fechagenera.'</td>
                    <td>'.$item->fecharetorno.'</td>
                    <td>'.$item->periodo.'</td>
                    <td>'.$stock.'</td>
                    <td>'.$item->comentario.'</td>
                  </tr>';
        }
      $html.='</tbody>
              <tfoot>
                <tr>
                  <td style="background-color: #26d744; text-align: center; color:white;">Stock Cliente</td>
                  <td colspan="7">'.$toner_stockcliente.'</td>
                </tr>
                <tr>
                  <td style="background-color: #00e5ff; text-align: center; color:white;">Retorno</td>
                  <td colspan="7">'.$toner_retorno.'</td>
                </tr>
                <tr>
                  <td style="background-color: #ea6749; text-align: center; color:white;">Pendiente</td>
                  <td colspan="7">'.$toner_pendiente.'</td>
                </tr>
              </tfoot>
            </table>';
      echo  $html;
    }
    public function table_get_pagos_factura(){ 
        $id = $this->input->post('id'); 
        $html='<table class="responsive-table display striped" cellspacing="0"> 
                    <thead> 
                      <tr> 
                        <th>Fecha</th> 
                        <th>Método de pago</th> 
                        <th>Monto</th>
                        <th>Comentario</th> 
                        <!--<th>Comprobante</th>-->
                        <th></th>
                        <th></th> 
                      </tr> 
                    </thead> 
                    <tbody>'; 
        
           $result = $this->ModeloCatalogos->pagos_factura($id); 
           foreach ($result as $item) {
             if($item->confirmado==1){
                  $itemconfirmado='Confirmado <i class="material-icons" style="color:green;">done_all</i>';
             }else{
                  $itemconfirmado='Sin Confirmar <a class="new badge yellow" style="padding: 9px 2px 0px;" onclick="confirmarpagov('.$item->fp_pagosId.',4)"><i class="material-icons">warning</i> </a>';
             } 
            $html.='<tr> 
                     <td>'.$item->fecha.'</td> 
                     <td>'.$item->formapago_text.'</td> 
                     <td class="montoagregado">'.$item->pago.'</td> 
                     <td>'.$item->observacion.'</td> 
                     <!--<td>'.$item->comprobante.'</td>--> 
                     <td>'.$item->nombre.' '.$item->apellido_paterno.' '.$item->apellido_materno.'</td>
                     <td>
                        <span 
                            class="new badge red" 
                            style="cursor: pointer;" 
                            onclick="deletepagov('.$item->fp_pagosId.')">
                            <i class="material-icons">delete</i>
                        </span>
                        '.$itemconfirmado.'
                    </td>
                    </tr>'; 
            }
            $resultf = $this->ModeloCatalogos->facturasdelperiodo($id); 
            foreach ($resultf->result() as $itemf) {
                $resultvcp_cp = $this->ModeloCatalogos->obteciondecomplememtofactura($itemf->facturaId);
                foreach ($resultvcp_cp->result() as $itemcp) {
                        $html.='<tr> 
                                     <td class="complemento_'.$itemcp->complementoId.'">
                                        '.$itemcp->Fecha.'
                                    </td> 
                                     <td></td> 
                                     <td class="montoagregado">'.$itemcp->ImpPagado.'</td> 
                                     <td>Complemento</td> 
                                     <!--<td></td>-->
                                     <td></td> 
                                     <td>
                                        
                                    </td>
                                    </tr>'; 
                }
                $sql="SELECT pf.idpago, pf.fecha, ff.formapago_text, pf.pago, pf.observaciones, per.nombre, per.apellido_paterno, per.apellido_materno, pf.reg 
                      FROM pagos_factura as pf 
                      INNER JOIN f_formapago as ff on ff.id=pf.idmetodo 
                      INNER JOIN personal as per on per.personalId=pf.idpersonal 
                      WHERE pf.factura='$itemf->facturaId' AND pf.activo=1";
                  $query = $this->db->query($sql);
                  foreach ($query->result() as $item) {
                    $html.='<tr> 
                     <td>'.$item->fecha.'</td> 
                     <td>'.$item->formapago_text.'</td> 
                     <td class="montoagregado">'.$item->pago.'</td> 
                     <td>'.$item->observaciones.'</td> 
                     <td>'.$item->nombre.' '.$item->apellido_paterno.' '.$item->apellido_materno.'</td>
                     <td></td>
                    </tr>';
                  }
            }
            
        
        $html.='</tbody> 
                  </table>'; 
        echo $html; 
    }
    public function guardar_pago_factura(){ 
        $data = $this->input->post(); 
        $id_factura = $data['id_factura']; 
        $data['idpersonal']=$this->idpersonal; 
        unset($data['id_factura']); 
         
            $data['facId']=$id_factura; 
            $this->ModeloCatalogos->Insert('factura_prefactura_pagos',$data); 
            //===================================================================
            $rowpagos=$this->ModeloCatalogos->db14_getselectwheren('factura_prefactura_pagos',array('facId'=>$id_factura));
            $totalpagos=0;
            foreach ($rowpagos->result() as $item) {
                $totalpagos=$totalpagos+$item->pago;
            }
            
            $totalpre=$this->montopagosf($id_factura);
            if ($totalpagos>=$totalpre) {
                $this->ModeloCatalogos->updateCatalogo('factura_prefactura',array('statuspago'=>1),array('facId'=>$id_factura));
            }   
    }
    function montopagosf($id){
      $totalgeneral=0; 
        $rowequipos=$this->ModeloCatalogos->getselectwheren('factura_prefactura_pagos',array('facId'=>$id)); 
        foreach ($rowequipos->result() as $item) { 
            $totalgeneral=$totalgeneral+$item->pago; 
        } 
        return $totalgeneral; 
    }
    function eliminarpago(){
      $pagoId = $this->input->post('pagoId'); 

      $rowequipos=$this->ModeloCatalogos->getselectwheren('factura_prefactura_pagos',array('fp_pagosId'=>$pagoId)); 
      $facId=0;
      foreach ($rowequipos->result() as $item) {
        $facId = $item->facId;
        $this->ModeloCatalogos->Insert('bitacora',array('contenido'=>'Se elimino pago de periodo: '.json_encode($item),'nombretabla'=>'factura_prefactura_pagos','idtable'=>$pagoId,'tipo'=>'Delete','personalId'=>$this->idpersonal));
      }

      $this->ModeloCatalogos->getdeletewheren('factura_prefactura_pagos',array('fp_pagosId'=>$pagoId));

      if($facId>0){
        $this->ModeloCatalogos->updateCatalogo('factura_prefactura',array('statuspago'=>0),array('facId'=>$facId));
      }

      
    }
    function Estadodecuenta($id,$cliente=0){
      $data['idc']=$id;
      $data['cliente']=$cliente;
      //$data['facturas']=$this->ModeloCatalogos->viewprefacturaslis_general($id);
      //$data['datoscontrato'] =$this->Rentas_model->getdatosdeestadodecuenta($id);
      $this->load->view('Reportes/estadodecuenta',$data);
    }
    /*
    function consultarcontadores(){
      $idcontrato = $this->input->post('idcontrato'); 
      $idpoliza = $this->input->post('idpoliza'); 
      $contadoreshtml='';

      $where_ventas=array('prefId'=>$idpoliza);
      $resultadov = $this->ModeloCatalogos->db14_getselectwheren('alta_rentas_prefactura',$where_ventas);
      $id_personal=0;
      foreach ($resultadov->result() as $item) {
          $periodoinicial=$item->periodo_inicial;
          $periodofinal=$item->periodo_final;
          //==============================================================
                  $periodoiniciale = strtotime ( '- 1 month', strtotime ( $periodoinicial ) ) ;
                  $periodoiniciale = date ( 'Y-m-d' , $periodoiniciale);

                  //$periodoinicialef = strtotime ( '+ 1 month', strtotime ( $periodofinal ) ) ;
                  //$periodoinicialef = date ( 'Y-m-d' , $periodoinicialef);
                  $diainie = date ( 'd' , strtotime($periodoiniciale));
                  if ($diainie==1) {
                    $periodoinicialef = new DateTime($periodoiniciale);
                    $periodoinicialef->modify('last day of this month');
                    $periodoinicialef = $periodoinicialef->format('Y-m-d');
                  }else{
                    $periodoinicialef = strtotime ( '- 1 month', strtotime ( $periodofinal ) ) ;
                    $periodoinicialef = date ( 'Y-m-d' , $periodoinicialef);

                    //$periodoinicialef = strtotime ( '-1 day', strtotime ( $periodoinicialef ) ) ;
                    //$periodoinicialef = date ( 'Y-m-d' , $periodoinicialef );
                  }
                  
    
            //==============================================================
          $fechaperiodo=$this->periodoletras($item->periodo_inicial,$item->periodo_final);
          $perriodone=$this->periodoletrase($periodoiniciale,$periodoinicialef);
          $contadoreshtml.='<input type="hidden" id="idpolizaedit" value="'.$idpoliza.'" readonly>
                            <div class="row">
                              <div class="col s3 m1">
                                <label>PERIODO</label>
                              </div>
                              <div class="col s6 m3">
                                <input id="periodo_inicial_edit" name="periodo_inicial_edit" class="form-control-bmz" type="date" value="'.$item->periodo_inicial.'">
                              </div>
                              <div class="col s2 m1">
                                <label>AL</label>
                              </div>
                              <div class="col s6 m3">
                                <input id="periodo_final_edit" name="periodo_final_edit" class="form-control-bmz" type="date" value="'.$item->periodo_final.'" >
                              </div>
                            </div>
                            <div class="row">
                            <div class="col s3 m2">
                                <label>PERIODO RENTA MONOCROMATICO</label>
                            </div>
                            <div class="col s3 m5">
                              <input type="text" name="" value="'.$fechaperiodo.'" readonly="">
                            </div>
                            <div class="col s3 m1">
                              <input type="text" id="costoperiodo_edit" value="'.$item->subtotal.'" readonly="">
                            </div>

                            <div class="col s3 m2">
                              <label>TOTAL FACTURA</label>
                            </div>
                            <div class="col s3 m2">
                              <input type="text" id="totalperiodo_edit" value="0" readonly="">
                            </div>
                          </div>';
            $contadoreshtml.='<div class="row removecostoperiodocolor_edit" style="display: none;">
                              <div class="col s3 m2">
                                  <label>PERIODO RENTA COLOR</label>
                              </div>
                              <div class="col s3 m5">
                                <input type="text" name="" value="'.$fechaperiodo.'" readonly="">
                              </div>
                              <div class="col s3 m1">
                                <input type="text" id="costoperiodocolor_edit" value="'.$item->subtotalcolor.'" readonly="">
                              </div>
                            </div>';
            $contadoreshtml.='<div class="row excedentesmono_edit">
                                <div class="col s3 m2">
                                    <label>EXCEDENTES MONOCROMATICO</label>
                                </div>
                                <div class="col s3 m5">
                                  <input type="text" value="'.$perriodone.'" readonly="">
                                </div>
                                <div class="col s3 m1">
                                  <input type="text" id="costoperiodoexcedente_edit" name="" value="0" readonly="">
                                </div>
                              </div>';
            $contadoreshtml.='<div class="row excedentescolor_edit" style="display: none;">
                                <div class="col s3 m2">
                                    <label>EXCEDENTES COLOR</label>
                                </div>
                                <div class="col s3 m5">
                                  <input type="text" value="'.$perriodone.'" readonly="">
                                </div>
                                <div class="col s3 m1">
                                  <input type="text" id="costoperiodoexcedente_c_edit" name="" value="0" readonly="">
                                </div>
                              </div>';
      }

      
      $contadoreshtml.='';
      





      //=========================================================================
      $equiposrenta = $this->Rentas_model->equiposrentas($idcontrato);
      $contadoreshtml.='
        <table class="responsive-table display centered" id="profactura_edit">
          <thead>
            <tr>
              <!--<th rowspan="2">Img</th>-->
              <th rowspan="2">TIPO</th>
              <th rowspan="2">MODELO</th>
              <th rowspan="2">SERIE</th>
              <th colspan="3">COPIA / IMPRESIÓN</th>
              <th colspan="3">ESCANEO</th>
              
              <th rowspan="2" class="removeproduccion">PRODUCCIÓN</th>
              <th rowspan="2">TONER CONSUMIDO</th>
              <th rowspan="2">PRODUCCIÓN TOTAL</th>
              <th rowspan="2"># TONER CONSUMIDO</th>
              <th rowspan="2" class="removetipo2">EXCEDENTES</th>
              <th rowspan="2" class="removetipo2">COSTO EXCEDENTES</th>
              <th rowspan="2" class="removetipo2">TOTAL EXCEDENTE</th>
            </tr>
            <tr>
              <th>CONTADOR INICIAL</th>
              <th>CONTADOR FINAL</th>
              <th>PRODUCCIÓN</th>
              <th>CONTADOR INICIAL</th>
              <th>CONTADOR FINAL</th>
              <th>PRODUCCIÓN</th>
            </tr>
          </thead>
          <tbody>';
            $equiporow=1;
          foreach ($equiposrenta as $item){ 
            if ($item->tipo==1||$item->tipo==3) { 
              $contadoreshtml.='<tr>
                <!--<td class="img_captura_'.$item->id.'_'.$item->serieId.'_edit">
                </td>-->
                <td>
                    Monocromáticos
                    <input type="hidden" class="fd_equipo_edit" value="'.$item->id.'" readonly>
                    <input type="hidden" class="fd_serie_edit" value="'.$item->serieId.'" readonly>
                    <input type="hidden" class="fd_tipo_edit" value="1" readonly>
                    <input type="hidden" class="prefdId_edit prefdId_'.$item->id.'_'.$item->serieId.'_1_edit" readonly>
                </td>
                <td>'.$item->modelo.'</td>
                <td>'.$item->serie.'</td>
                <td>
                  <input type="number" class=" f_c_c_i_edit f_c_cont_ini_'.$item->id.'_'.$item->serieId.'_1_edit form-control-bmz" readonly>
                </td>
                <td>
                  <input type="number" 
                    class=" f_c_c_f_edit f_c_cont_fin_'.$item->id.'_'.$item->serieId.'_1_edit prfprocontador_edit form-control-bmz" 
                    data-equipo="'.$item->id.'" 
                    data-serie="'.$item->serieId.'" 
                    data-moco="1"
                    data-tomarescaner="1" 
                    data-tipo="1" 
                    value="0" >
                </td>
                <td>
                  <input type="number" class=" f_c_c_p_edit f_c_cont_pro_'.$item->id.'_'.$item->serieId.'_1_edit form-control-bmz" readonly>
                </td>
                <td>
                  <input type="number" class=" f_e_c_i_edit f_e_cont_ini_'.$item->id.'_'.$item->serieId.'_1_edit form-control-bmz" readonly>
                </td>
                <td>
                  <input type="number" 
                    class=" f_e_c_f_edit f_e_cont_fin_'.$item->id.'_'.$item->serieId.'_1_edit prfprocontador_edit form-control-bmz" 
                    data-equipo="'.$item->id.'" 
                    data-serie="'.$item->serieId.'" 
                    data-moco="1"
                    data-tomarescaner="1"
                    data-tipo="1"  
                    value="0" >
                </td>
                <td>
                  <input type="number" class=" f_e_c_p_edit f_e_cont_pro_'.$item->id.'_'.$item->serieId.'_1_edit form-control-bmz" readonly>
                </td>
                <td class="removeproduccion">
                  <input type="number" class="f_produccion_edit f_produccion_'.$item->id.'_'.$item->serieId.'_1_edit form-control-bmz" readonly>
                </td>
                <td>
                  <input type="number" placeholder="%" class="f_toner_consumido_edit f_toner_consumido_'.$item->id.'_'.$item->serieId.'_1_edit form-control-bmz"  readonly>
                </td>
                <td>
                  <input type="number" class="produccion_total_edit produccion_mono_edit produccion_total_'.$item->id.'_'.$item->serieId.'_1_edit form-control-bmz" readonly>
                </td>
                <td>
                  <input type="number" class="total_toner_edit produccion_monot_edit total_toner_'.$item->id.'_'.$item->serieId.'_1_edit form-control-bmz" readonly>
                </td>
                <td class="removetipo2">
                  <input type="number" class="excedentes_edit excedentes_'.$item->id.'_'.$item->serieId.'_1_edit form-control-bmz" readonly>
                </td>
                <td class="removetipo2">
                  <input type="number" class="excedentescosto_edit excedentescosto_'.$item->id.'_'.$item->serieId.'_1_edit form-control-bmz" readonly>
                </td>
                <td class="removetipo2">
                  <input type="number" class="excedente_mono_edit excedentestotal_edit excedentetotal_'.$item->id.'_'.$item->serieId.'_1_edit form-control-bmz" value="0" readonly>
                </td>
              </tr>';
                $equiporow++; 
            }
          } 
        foreach ($equiposrenta as $item){ 
            if ($item->tipo==2||$item->tipo==3) { 
              if ($item->tipo==3) {
                $readonlyh='readonly';
                $display='style="display:none"';
                $tomarescaner=0;
              }else{
                $display='';
                $tomarescaner=1;
                $readonlyh='';
              }
            $contadoreshtml.='
              <tr>
                <!--<td class="img_captura_'.$item->id.'_'.$item->serieId.'_edit">
                </td>-->
                <td>
                    Color
                    <input type="hidden" class="fd_equipo_edit" value="'.$item->id.'" readonly>
                    <input type="hidden" class="fd_serie_edit" value="'.$item->serieId.'" readonly>
                    <input type="hidden" class="fd_tipo_edit" value="2" readonly>
                    <input type="hidden" class="prefdId_edit prefdId_'.$item->id.'_'.$item->serieId.'_2_edit" readonly>
                </td>
                <td>'.$item->modelo.'</td>
                <td>'.$item->serie.'</td>
                <td>
                  <input type="number" class=" f_c_c_i_edit f_c_cont_ini_'.$item->id.'_'.$item->serieId.'_2_edit form-control-bmz" readonly>
                </td>
                <td>
                  <input type="number" 
                    class=" f_c_c_f_edit f_c_cont_fin_'.$item->id.'_'.$item->serieId.'_2_edit prfprocontador_edit form-control-bmz" 
                    data-equipo="'.$item->id.'" 
                    data-serie="'.$item->serieId.'"
                    data-moco="2"
                    data-tipo="2" 
                    data-tomarescaner="'.$tomarescaner.'" 
                    value="0" >
                </td>
                <td>
                  <input type="number" class=" f_c_c_p_edit f_c_cont_pro_'.$item->id.'_'.$item->serieId.'_2_edit form-control-bmz" readonly>
                </td>
                <td>
                  <input type="number" class=" f_e_c_i_edit f_e_cont_ini_'.$item->id.'_'.$item->serieId.'_2_edit form-control-bmz" readonly '.$display.'>
                </td>
                <td>
                  <input type="number" 
                    class=" f_e_c_f_edit f_e_cont_fin_'.$item->id.'_'.$item->serieId.'_2_edit prfprocontador_edit form-control-bmz" 
                    data-equipo="'.$item->id.'" 
                    data-serie="'.$item->serieId.'" 
                    data-moco="2"
                    data-tipo="2" 
                    data-tomarescaner="'.$tomarescaner.'" 
                    value="0" '.$readonlyh.' '.$display.'>
                </td>
                <td>
                  <input type="number" class=" f_e_c_p_edit f_e_cont_pro_'.$item->id.'_'.$item->serieId.'_2_edit form-control-bmz" '.$display.'  readonly>
                </td>
                <td class="removeproduccion">
                  <input type="number" class="f_produccion_edit f_produccion_'.$item->id.'_'.$item->serieId.'_2_edit form-control-bmz" readonly>
                </td>
                <td>
                  <input type="number" placeholder="%" class="f_toner_consumido_edit f_toner_consumido_'.$item->id.'_'.$item->serieId.'_2_edit form-control-bmz"  readonly>
                </td>
                <td>
                  <input type="number" class="produccion_total_edit produccion_color_edit produccion_total_'.$item->id.'_'.$item->serieId.'_2_edit form-control-bmz" readonly>
                </td>
                <td>
                  <input type="number" class="total_toner_edit produccion_colort_edit total_toner_'.$item->id.'_'.$item->serieId.'_2_edit form-control-bmz" readonly>
                </td>
                <td class="removetipo2">
                  <input type="number" class="excedentes_edit excedentes_'.$item->id.'_'.$item->serieId.'_2_edit form-control-bmz" readonly>
                </td>
                <td class="removetipo2">
                  <input type="number" class="excedentescosto_edit excedentescosto_'.$item->id.'_'.$item->serieId.'_2_edit form-control-bmz" readonly>
                </td>
                <td class="removetipo2">
                  <input type="number" class="excedente_color_edit  excedentestotal_edit excedentetotal_'.$item->id.'_'.$item->serieId.'_2_edit form-control-bmz" value="0" readonly>
                </td>
              </tr>';
            $equiporow++;
          }
        } 
          
      $contadoreshtml.='</tbody>
     </table>';
      $contadoreshtml.='
          <div class="row">
            <div class="col s9">
            </div>
            <div class="col s3">
              <button class="btn waves-effect waves-light" type="button" style=" background-color: #56ea36;" onclick="editarcontadoress()">Editar</button>
            </div>
          </div>



          
      ';
      //=========================================================================











      $array  = array(
        'contadoreshtml' =>$contadoreshtml );
      echo json_encode($array);
    }
    */
    function editcontadores(){
      $data = $this->input->post();
      $equiposrow=$data['equipos'];
      unset($data['equipos']);
      //$prefId = $this->ModeloCatalogos->Insert('alta_rentas_prefactura',$data);
      $subtotal = $data['subtotal'];
      $subtotalcolor = $data['subtotalcolor'];
      $excedente = $data['excedente'];
      $excedentecolor = $data['excedentecolor'];
      $fechainicial = $data['fechainicial'];
      $fechafin = $data['fechafin'];
      $total = $data['total'];
      $datap  = array(
                  'subtotal'=> $subtotal,
                  'subtotalcolor'=> $subtotalcolor,
                  'excedente'=> $excedente,
                  'excedentecolor'=> $excedentecolor,
                  'total'=> $total,
                  'factura_renta'=>0,
                  'factura_excedente'=>0,
                  'periodo_inicial'=>$fechainicial,
                  'periodo_final'=>$fechafin
                );

      $this->ModeloCatalogos->updateCatalogo('alta_rentas_prefactura',$datap,array('prefId'=>$data['id_polizar']));

      $equiposrow = json_decode($equiposrow);
      for ($i=0;$i<count($equiposrow);$i++) { 
        $prefdId=$equiposrow[$i]->prefdId;
        $dataeq['c_c_i']=$equiposrow[$i]->c_c_i;
        $dataeq['c_c_f']=$equiposrow[$i]->c_c_f;
        $dataeq['e_c_i']=$equiposrow[$i]->e_c_i;
        $dataeq['e_c_f']=$equiposrow[$i]->e_c_f;
        $dataeq['descuento']=$equiposrow[$i]->descuento;
        $dataeq['descuentoe']=$equiposrow[$i]->descuentoe;

        $dataeq['produccion']=$equiposrow[$i]->produccion;
        $dataeq['toner_consumido']=$equiposrow[$i]->toner_consumido;
        $dataeq['produccion_total']=$equiposrow[$i]->produccion_total;
      
        $dataeq['excedentes']=$equiposrow[$i]->excedentes;
        $dataeq['costoexcedente']=$equiposrow[$i]->costoexcedente;
        $dataeq['totalexcedente']=$equiposrow[$i]->totalexcedente;

        $dataeq['statusfacturar']=0;
        $dataeq['statusfacturae']=0;
        
        $this->ModeloCatalogos->updateCatalogo('alta_rentas_prefactura_d',$dataeq,array('prefdId'=>$prefdId));
      }
    }
    function servicios(){
        $data['MenusubId']=56;
        //$data['resultstec']=$this->Configuraciones_model->searchTecnico(); 
        $data['resultstec']=$this->Configuraciones_model->view_session_searchTecnico();
        $data['perfilid']=$this->perfilid;
        $data['idpersonal']=$this->idpersonal;
        if (isset($_GET['fech'])) {
          $fechahoy=$_GET['fech'];
        }else{
          $fechahoy=$this->fechahoy;
        }
        //====================
          switch (date('N',strtotime($fechahoy))) {
            case 1:
              $dia='LUNES';
              break;
            case 2:
              $dia='MARTES';
              break;
            case 3:
              $dia='MIERCOLES';
              break;
            case 4:
              $dia='JUEVES';
              break;
            case 5:
              $dia='VIERNES';
              break;
            case 6:
              $dia='SABADO';
              break;
            case 7:
              $dia='DOMINGO';
              break;
            
            default:
              $dia='';
              break;
          }
          $data['dial']=$dia;
        //====================
        $data['version']=$this->version; 
        $data['fechahoy']=$fechahoy;
        $data['oserier']=1;
        $this->load->view('header');
        $this->load->view('main',$data);
        $this->load->view('rentas/servicios');
        $this->load->view('rentas/serviciosjs');
        $this->load->view('rentas/servicios_modal');
        $this->load->view('footerck'); 
        $this->load->view('info_m_servisios');  
        $this->load->view('asignacionestecnico/ser_modal_report');
    }
    function obtenciondatos(){
        $tecnico=$this->input->post('tecnico');
        $fechahoy=$this->input->post('fechaselect');
        //$fechahoy='2020-05-25';//borrar xxx
        $contratos=0;
        $poliza=0;
        $evento=0;
        $ventas=0;
            $resultado1=$this->ModeloAsignacion->getdatosrentaft($tecnico,$fechahoy);
            foreach ($resultado1->result() as $item) {
                $contratos++;
            }
            $resultado2=$this->ModeloAsignacion->getdatospolizaft($tecnico,$fechahoy);
            foreach ($resultado2->result() as $item) {
                $poliza++;
            }
            $resultado3=$this->ModeloAsignacion->getdatosclienteft($tecnico,$fechahoy);
            foreach ($resultado3->result() as $item) {
                $evento++;
            }
            $resultado4=$this->ModeloAsignacion->getdatosventasft($tecnico,$fechahoy);
            foreach ($resultado4->result() as $item) {
                $ventas++;
            }
            $resultiti = $this->ModeloCatalogos->get_detalles_itinerario($tecnico,$fechahoy);
      $array = array(
              'contratos'=>$contratos,
              'poliza'=>$poliza,
              'evento'=>$evento,
              'venta'=>$ventas,
              'itinerario'=>$resultiti->num_rows()
              );
      echo json_encode($array);
    }
    function exportarcontsobra(){
      $data = $this->input->post();
      $periodo =$data['periodo'];
      $contrato =$data['contrato'];
      
      $confcontrato = $this->ModeloCatalogos->getselectwheren('alta_rentas',array('idcontrato'=>$contrato));
      foreach ($confcontrato->result() as $item) {
        $clicks_mono=$item->clicks_mono;
        $clicks_color=$item->clicks_color;
      }
      $produccionmono=0;
      $produccioncolor=0;
      $consmono = $this->ModeloCatalogos->getselectwheren('alta_rentas_prefactura_d',array('prefId'=>$periodo,'tipo'=>1));
      foreach ($consmono->result() as $item) {
        $produccionmono=$produccionmono+$item->produccion;
      }
      $conscolo = $this->ModeloCatalogos->getselectwheren('alta_rentas_prefactura_d',array('prefId'=>$periodo,'tipo'=>2));
      foreach ($conscolo->result() as $item) {
        $produccioncolor=$produccioncolor+$item->produccion;
      }
      $restantemono=$clicks_mono-$produccionmono;
      $restantecolor=$clicks_color-$produccioncolor;


      $html='<div class="col s3 ">
              Monocromaticos
              </div>
              <div class="col s3">
                <input type="hidden" class="form-control-bmz" id="periodoexport" value="'.$periodo.'" readonly> 
                <input class="form-control-bmz" id="contadorexportmono" value="'.$restantemono.'" readonly>
              </div>
              <div class="col s3 ">
                Color
              </div>
              <div class="col s3 ">
                <input class="form-control-bmz" id="contadorexportcolor" value="'.$restantecolor.'" readonly>
              </div>';
      echo $html;
    }
    function exportarcontsobraresibe(){
      $data = $this->input->post();
      $periodoini = $data['periodoini'];
      $cotamono = $data['cotamono'];
      $contacolor = $data['contacolor'];
      $periodoreseptor = $data['periodoreseptor'];


      $infoperiodoreceptor = $this->ModeloCatalogos->getselectwheren('alta_rentas_prefactura',array('prefId'=>$periodoreseptor));
      foreach ($infoperiodoreceptor->result() as $item) {
        $id_renta=$item->id_renta;
        $excedente_o=$item->excedente;
        $excedentecolor_o=$item->excedentecolor;
      }
      $infoperiodorenta = $this->ModeloCatalogos->getselectwheren('alta_rentas',array('id_renta'=>$id_renta));
      foreach ($infoperiodorenta->result() as $item) {
        $precio_c_e_mono=$item->precio_c_e_mono;
        $precio_c_e_color=$item->precio_c_e_color;
        # code...
      }
      $excedente_n=$cotamono*$precio_c_e_mono;
      $excedentecolor_n=$contacolor*$precio_c_e_color;


      $excedente_n_f=$excedente_o-$excedente_n;
      $excedentecolor_n_f=$excedentecolor_o-$excedentecolor_n;

      if ($excedente_n_f<0) {
        $excedente_n_f=0;
      }
      if ($excedentecolor_n_f<0) {
        $excedentecolor_n_f=0;
      }

      $newdatosreceptor = array(
        'excedente'=>$excedente_n_f,
        'excedentecolor'=>$excedentecolor_n_f,
        'periodo'=>$periodoini,
        'contadores'=>$cotamono,
        'contadoresc'=>$contacolor,
      );
      $this->ModeloCatalogos->updateCatalogo('alta_rentas_prefactura',$newdatosreceptor,array('prefId'=>$periodoreseptor));

      $this->ModeloCatalogos->updateCatalogo('alta_rentas_prefactura',array('periodo'=>$periodoreseptor,'contadores'=>'-'.$cotamono,'contadoresc'=>'-'.$contacolor,
      ),array('prefId'=>$periodoini));
    }
    function produccionlibre(){
      $data = $this->input->post();
      $prefac =$data['prefac'];
      $valor =$data['valor'];
      $this->ModeloCatalogos->updateCatalogo('alta_rentas_prefactura',array('valorlibre'=>$valor),array('prefId'=>$prefac));

      $where = array('prefId' =>$prefac);
      $result = $this->ModeloCatalogos->getselectwherenall('alta_rentas_prefactura',$where);
      $id_renta=0;
      foreach ($result as $item) {
         $fechai = date("d/m/Y", strtotime($item->periodo_inicial));
         $fechaf = date("d/m/Y", strtotime($item->periodo_final));

         $fechaie = date("d/m/Y", strtotime($item->periodo_inicial."- 1 month"));
         $fechafe = date("d/m/Y", strtotime($item->periodo_final."- 1 month"));

         $totals = $item->subtotal;
         $totalcolor = $item->subtotalcolor;
         $excedente = $item->excedente;
         $excedentec = $item->excedentecolor; 
         $id_renta = $item->id_renta; 
         $valorlibre = $item->valorlibre;  
      }
      $totaldeproduccion=0;
      $resutdatos=$this->ModeloCatalogos->getselectwheren('alta_rentas_prefactura_d',array('prefId'=>$prefac));
      foreach ($resutdatos->result() as $item) {
        $totaldeproduccion=$totaldeproduccion+($item->c_c_f-$item->c_c_i);
        $totaldeproduccion=$totaldeproduccion+($item->e_c_f-$item->e_c_i);
      }
      $totaldeproduccionv=$totaldeproduccion*$valorlibre;
      //log_message('error', 'totalproduccion '.$totaldeproduccion.'*'.$valorlibre);
      $html='';
      $html.='<tr class="datos_principal">
                      <td>
                        <input id="selectedequipo" name="selectedequipo" type="hidden" value="0" readonly>
                        <input id="selectedprefactura" name="selectedprefactura" type="hidden" value="'.$prefac.'" readonly>
                        <input id="facturarenta" name="facturarenta" type="hidden" value="1" readonly>
                        <input id="facturaexcedente" name="facturaexcedente" type="hidden" value="1" readonly>
                        <input id="cantidad" name="cantidad" type="text" value="1" readonly></td>
                      <td>
                          <div style="width:250px">
                            <select id="unidadsat" class="unidadsat browser-default">
                            </select>
                          <div>
                      </td>
                      <td>
                        <div style="width:250px">
                          <select id="conseptosat" class="conseptosat browser-default">
                          </select>
                        <div>
                      </td>
                      <td><input id="descripcion" name="descripcion" type="text" value="Renta del '.$fechai.' al '.$fechaf.'"></td>
                      <td><input id="precio" name="precio" class="precio" type="text" value="'.$totaldeproduccionv.'" readonly></td>
                    </tr>';
      echo $html;
    }
    function retimbrar(){
      $factura = $this->input->post('factura');
      $facturaresult=$this->ModeloCatalogos->getselectwheren('f_facturas',array('activo'=>1,'FacturasId'=>$factura,'fechatimbre'=>'0000-00-00 00:00:00'));
      $respuesta=array();
      foreach ($facturaresult->result() as $item) {
        $rfc_id=$item->rfc_id;
        $Folio=$item->Folio;
        $rfc=$item->Rfc;
        $serie=$item->serie;
        $idcliente=$item->Clientes_ClientesId;
        //if($Folio==0){
          $newfolio=$this->Rentas_model->ultimoFolio($serie) + 1;
          $this->ModeloCatalogos->updateCatalogo('f_facturas',array('Folio'=>$newfolio),array('FacturasId'=>$factura));
        //}
        //==================================================
          $dafi=$this->ModeloCatalogos->getselectwheren('cliente_has_datos_fiscales',array('rfc'=>$rfc,'idCliente'=>$idcliente,'activo'=>1));
          foreach ($dafi->result() as $item) {
            if($item->razon_social!='PUBLICO EN GENERAL'){
              $dafii['Nombre']=$item->razon_social;
            }
            $dafii['Cp']=$item->cp;

            $num_ext = $item->num_ext;
            $num_int = $item->num_int;
            $colonia = $item->colonia;
            $calle = $item->calle;
            $estado=$item->estado;
            $municipio=$item->municipio;
            $localidad=$item->localidad;
            
            $dafii['r_regimenfiscal']=$item->RegimenFiscalReceptor;
            $dafii['direccion']=$calle.' No '.$num_ext.' '.$num_int.' Col: '.$colonia.' , '.$localidad.' , '.$municipio.' , '.$estado.'.';
            $this->ModeloCatalogos->updateCatalogo('f_facturas',$dafii,array('FacturasId'=>$factura));

          }
          if($dafi->num_rows()==0){
            $dafi=$this->ModeloCatalogos->getselectwheren('cliente_has_datos_fiscales',array('id'=>$rfc_id));
            foreach ($dafi->result() as $item) {
              $rfc = $item->rfc;
              if($item->razon_social!='PUBLICO EN GENERAL'){
                $dafii['Nombre']=$item->razon_social;
              }
              $dafii['Cp']=$item->cp;

              $num_ext = $item->num_ext;
              $num_int = $item->num_int;
              $colonia = $item->colonia;
              $calle = $item->calle;
              $estado=$item->estado;
              $municipio=$item->municipio;
              $localidad=$item->localidad;
              $dafii['Rfc']=$rfc;
              $dafii['r_regimenfiscal']=$item->RegimenFiscalReceptor;
              $dafii['direccion']=$calle.' No '.$num_ext.' '.$num_int.' Col: '.$colonia.' , '.$localidad.' , '.$municipio.' , '.$estado.'.';
              $this->ModeloCatalogos->updateCatalogo('f_facturas',$dafii,array('FacturasId'=>$factura));

            }
          }
        //==================================================
          $respuesta=$this->emitirfacturas($factura);
      }
      //======================================
      echo json_encode($respuesta);
    }
    function emitirfacturas($facturaId){
        $productivo=0;//0 demo 1 producccion
        //$this->load->library('Nusoap');
        //require_once APPPATH."/third_party/nusoap/nusoap.php"; 
        
        //========================
        $datosFacturaa=$this->ModeloCatalogos->getselectwheren('f_facturas',array('FacturasId'=>$facturaId));
        $datosFacturaa=$datosFacturaa->result();
        $datosFacturaa=$datosFacturaa[0];
        //============
        $datosFactura = array(
          'carpeta'=>'kyocera',
          'pwskey'=>'',
          'archivo_key'=>'',
          'archivo_cer'=>'',
          'factura_id'=>$facturaId
        );
        //=======================================================
          if($datosFacturaa->serie=='D'){
            $ConfiguracionesId=2;
            $rut_file=2;
          }else{
            $ConfiguracionesId=1;
            $rut_file='';
          }
          $datosconfiguracion=$this->ModeloCatalogos->getselectwheren('f_configuraciones',array('ConfiguracionesId'=>$ConfiguracionesId));
          $datosconfiguracion=$datosconfiguracion->result();
          $datosconfiguracion=$datosconfiguracion[0];
        //================================================
          $respuesta=array();
          $passwordLlavePrivada   = $datosFactura['pwskey'];
          $nombreArchivoKey       = base_url() . $datosFactura['archivo_key'];
          $nombreArchivoCer       = base_url() . $datosFactura['archivo_cer'];
          // ---------------- INICIAN COMANDOS OPENSSL --------------------
          //Generar archivo que contendra cadena de certificado (Convertir Certificado .cer a .pem)
          $comando='openssl x509 -inform der -in '.$nombreArchivoCer.' -out ' . base_url() . $datosFactura['carpeta'] . '/temporalsat/certificate.pem';    
          //echo '<br><br>' . $comando .'<br><br>';
          //exec($comando,$salida_script,$valor_exit);
          $comando='openssl x509 -inform DER -in '.$nombreArchivoCer.' -noout -startdate > ' . base_url() . $datosFactura['carpeta'] . '/temporalsat/IniciaVigencia.txt';
          //echo '<br><br>' . $comando .'<br><br>';
          //exec($comando,$salida_script,$valor_exit);
          $comando='openssl x509 -inform DER -in '.$nombreArchivoCer.' -noout -enddate > ' . base_url() . $datosFactura['carpeta'] . '/temporalsat/FinVigencia.txt';
          //echo '<br><br>' . $comando .'<br><br>';
          //exec($comando,$salida_script,$valor_exit);
          $comando='openssl x509 -inform DER -in '.$nombreArchivoCer.' -noout -serial > ' . base_url() . $datosFactura['carpeta'] . '/temporalsat/Serial.txt';
          //echo '<br><br>' . $comando .'<br><br>';
          //exec($comando,$salida_script,$valor_exit);
          $comando='openssl x509 -inform DER -in '.$nombreArchivoCer.' -noout -text > ' . base_url() . $datosFactura['carpeta'] . '/temporalsat/datos.txt';
          //echo '<br><br>' . $comando .'<br><br>';
          //exec($comando,$salida_script,$valor_exit);
          $comando='openssl pkcs8 -inform DER -in '.$nombreArchivoKey.' -passin pass:'.$passwordLlavePrivada.' > ' . base_url() . $datosFactura['carpeta'] . '/temporalsat/llaveprivada_pem.txt';
          //exec($comando,$salida_script,$valor_exit);
          $comando='openssl pkcs8 -inform DER -in '.$nombreArchivoKey.' -passin pass:'.$passwordLlavePrivada.' > ' . base_url() . $datosFactura['carpeta'] . '/temporalsat/llaveprivada.pem';
          //echo '<br><br>' . $comando .'<br><br>';
          //exec($comando,$salida_script,$valor_exit);
        // ---------------- TERMINAN COMANDOS OPENSSL --------------------      
          //GUARDAR NUMERO DE SERIE EN ARCHIVO     
          //$archivo=fopen(base_url() . $datosFactura['carpeta'] . '/temporalsat/Serial.txt','r');
          //$numeroCertificado= fread($archivo, filesize(base_url() . $datosFactura['carpeta'] . '/temporalsat/Serial.txt'));   
          $numeroCertificado=file_get_contents(base_url() . $datosFactura['carpeta'] . '/temporalsat'.$rut_file.'/Serial.txt');
          //fclose($archivo);
          $numeroCertificado=  str_replace("serial=", "", $numeroCertificado);//quitar letras
        
          $temporal=  str_split($numeroCertificado);//Pasar todos los digitos a un array
        
          $numeroCertificado="";
          $i=0;  
          foreach ($temporal as $value) {
              
              if(($i%2))
              
                  $numeroCertificado .= $value;
          
              $i++;
          }
        
        $numeroCertificado = str_replace('\n','',$numeroCertificado);
        
        $numeroCertificado = trim($numeroCertificado);
        
        $this->ModeloCatalogos->updateCatalogo('f_facturas',array('certificado'=>$numeroCertificado,'nocertificado'=>$numeroCertificado),array('FacturasId'=>$facturaId));
        //$this->model_mango_invoice->actualizaCampoFactura('certificado',$numeroCertificado,$datosFactura['factura_id']);
        //$this->ModeloCatalogos->updateCatalogo('f_facturas',array('nocertificado'=>$numeroCertificado),array('FacturasId'=>$facturaId));
        //$this->model_mango_invoice->actualizaCampoFactura('nocertificado',$numeroCertificado,$datosFactura['factura_id']);
        
        $respuesta['numeroCertificado']=$numeroCertificado;

        //$archivo = fopen(base_url() . $datosFactura['carpeta'] . '/temporalsat/certificate.pem', "r");
        
        //$certificado = fread($archivo, filesize(base_url() . $datosFactura['carpeta'] . '/temporalsat/certificate.pem'));
        $certificado=file_get_contents(base_url() . $datosFactura['carpeta'] . '/temporalsat'.$rut_file.'/certificate.pem');
        
        //fclose($archivo);
        $certificado=  str_replace("-----BEGIN CERTIFICATE-----", "", $certificado);
        
        $certificado=  str_replace("-----END CERTIFICATE-----", "", $certificado);
        
        $certificado=  str_replace("\n", "", $certificado);
        $certificado=  str_replace(" ", "", $certificado);
        $certificado=  str_replace("\n", "", $certificado);
        $certificado= preg_replace("[\n|\r|\n\r]", '', $certificado);
        $certificado= trim($certificado);
        
        $respuesta['certificado']=$certificado;
            
        $arrayDatosFactura=  $this->getArrayParaCadenaOriginal($facturaId);  
        //=================================================================
          $horaactualm1=date('H:i:s');
          $horaactualm1 = strtotime ( '-1 hours -1 minute' , strtotime ( $horaactualm1 ) ) ;
          $horaactualm1=date ( 'H:i:s' , $horaactualm1 );
          $datosfacturaadd['fechageneracion']=date('Y-m-d').'T'.$horaactualm1;
        //==================================================================
        $xml=  $this->getXML($facturaId,'',$certificado,$numeroCertificado,$datosfacturaadd);
        //echo "contruccion de xml ===========================";  //borrar despues 
        //echo $xml;
        //echo "=====";
        
        //log_message('error', 'generaCadenaOriginalReal xml1: '.$xml);
        //log_message('error', '-');log_message('error', '-');log_message('error', '-');log_message('error', '-');log_message('error', '-');
        $str = trim($this->generaCadenaOriginalReal($xml,$datosFactura['carpeta']));    
        log_message('error', 'generaCadenaOriginalReal xml2: '.$str);
        $cadena=$str;
        $this->ModeloCatalogos->updateCatalogo('f_facturas',array('cadenaoriginal'=>$str),array('FacturasId'=>$datosFactura['factura_id']));
        //$clienteSOAP = new nusoap_client('https://www.fel.mx/WSTimbrado33/WSCFDI33.svc?WSDL',array('soap_version' => SOAP_1_2)); //productivo
        //$clienteSOAP = new nusoap_client('https://www.fel.mx/WSTimbrado33/WSCFDI33.svc?WSDL');//productivo
        if($productivo==0){
          $URL_WS='https://app.fel.mx/WSTimbrado33Test/WSCFDI33.svc?WSDL';
          //$URL_WS='https://app.appfacturainteligente.com/WSTimbrado33Test/WSCFDI33.svc?WSDL';//solo para pruebas ya que no tengo los accesos al demo
        }elseif ($productivo==1) {
          //Produccion
          $URL_WS='https://www.fel.mx/WSTimbrado33/WSCFDI33.svc?WSDL';
        }else{
          $URL_WS='';
        }
        
        //$clienteSOAP = new nusoap_client('https://app.fel.mx/WSTimbrado33Test/WSCFDI33.svc?WSDL');//pruebas
        $clienteSOAP = new SoapClient($URL_WS);

        //$clienteSOAP->soap_defencoding='UTF-8';
        
        $referencia=$datosFactura['factura_id']."-C-". $this->idpersonal;
        
        if($productivo==0){
          //usuario demo
          $password= "contRa$3na";
          $usuario= "HERD890308D33";
          //solo para pruebas ya que no tengo los accesos al demo
          //$password= "contRa$3na";
          //$usuario= "COE960119D33";
        }elseif ($productivo==1) {
          //Produccion
          $password= "@fuJXsS@";
          $usuario= "HERD890308UAA";

        }else{
          //usuario demo
          $password= "contRa$3na";
          $usuario= "HERD890308D33";
        }
        
        
        $fp = fopen(base_url() . $datosFactura['carpeta'] . '/temporalsat'.$rut_file.'/llaveprivada.pem', "r"); 
        
        $priv_key = fread($fp, 8192); 
        
        fclose($fp); 
        
        $pkeyid = openssl_get_privatekey($priv_key);
        
        //openssl_sign($cadena, $sig, $pkeyid);
        openssl_sign(utf8_encode($cadena), $sig, $pkeyid,'sha256');
        
        openssl_free_key($pkeyid);
        
        $sello = base64_encode($sig);
        //echo $sello;
        $respuesta['sello']=$sello;
        
        //$this->model_mango_invoice->actualizaCampoFactura('sellocadena',$sello,$datosFactura['factura_id']);
        $this->ModeloCatalogos->updateCatalogo('f_facturas',array('sellocadena'=>$sello),array('FacturasId'=>$datosFactura['factura_id']));
        //Gererar XML 2013-12-05T12:05:56
        $xml=  $this->getXML($facturaId,$sello,$certificado,$numeroCertificado,$datosfacturaadd);                
        //$fp = fopen(DIR_FILES . $datosFactura['carpeta'] . '/facturas/preview_'. $datosFactura['rfcEmisor'].'_'. $datosFactura['folio'].'.xml', 'w');//Cadena original 
        file_put_contents($datosFactura['carpeta'].'/facturas/preview_'. $datosconfiguracion->Rfc.'_'. $datosFacturaa->Folio.'_'.$facturaId.'.xml',$xml);
                
        //Preparar parametros para web service    
        //cadenaXML

        $result = $clienteSOAP->TimbrarCFDI(array(
                    'usuario' => $usuario,
                    'password' => $password,
                    'cadenaXML' => $xml,
                    'referencia' => $referencia));
        file_put_contents($datosFactura['carpeta'] . '/facturaslog/'.'log_facturas_result_'.$this->fechahoyL.'_.txt', json_encode($result));

        $this->ModeloCatalogos->updateCatalogo('f_facturas',array('Referencia'=>$referencia),array('FacturasId'=>$datosFactura['factura_id']));
    
        if( $result->TimbrarCFDIResult->CodigoRespuesta != '0' ) {                       
            
            $resultado = array(
                          'resultado'=>'error',
                          'CodigoRespuesta'=>$result->TimbrarCFDIResult->CodigoRespuesta,
                          'MensajeError'=>$result->TimbrarCFDIResult->MensajeError,
                          'Mensajeadicional'=>'PORFAVOR RETIMBRAR MAS TARDE',
                          'facturaId'=>$facturaId,
                          'info'=>$result
                          );
            
            $this->ModeloCatalogos->updateCatalogo('f_facturas',array('Estado'=>2,'mensajeerror'=>json_encode($result)),array('FacturasId'=>$datosFactura['factura_id']));
            
            return $resultado;
        
        } else {
          
            try {
                
                log_message('error', 'resultado correcto'.json_encode($result));

                $xmlCompleto=utf8_decode($result->TimbrarCFDIResult->XMLResultado);//Contiene XML
            
                //Guardar en archivo     y la ruta en la bd
                $ruta = $datosFactura['carpeta'] . '/facturas/'.$datosconfiguracion->Rfc.'_'.$datosFacturaa->Folio.'_'.$facturaId.'.xml';
            
                file_put_contents($ruta,"\xEF\xBB\xBF".$xmlCompleto);
                $this->ModeloCatalogos->updateCatalogo('f_facturas',array('rutaXml'=>$ruta),array('FacturasId'=>$datosFactura['factura_id']));            
                
                //================================================== verificar correcto y sustituir por la anterior
                  $xmlCompleto2=$result->TimbrarCFDIResult->XMLResultado;//Contiene XML
                  $ruta2 = $datosFactura['carpeta'] . '/facturas/'.$datosconfiguracion->Rfc.'_'.$datosFacturaa->Folio.'_'.$facturaId.'_2.xml';
                  file_put_contents($ruta2,"\xEF\xBB\xBF".$xmlCompleto2);
                //==================================================
                //$sxe = new SimpleXMLElement($xmlCompleto);
                         
                //$ns = $sxe->getNamespaces(true);
            
                //$sxe->registerXPathNamespace('c', $ns['cfdi']);
            
                //$sxe->registerXPathNamespace('t', $ns['tfd']);
 
                //$uuid = '';
            
                //foreach ($sxe->xpath('//t:TimbreFiscalDigital') as $tfd) {          
                 
                    //Actualizamos el Folio Fiscal UUID
                    //$uuid = $tfd['UUID'];
                    $updatedatossat=array(
                      'uuid'=>$result->TimbrarCFDIResult->Timbre->UUID,
                      'sellosat'=>$result->TimbrarCFDIResult->Timbre->SelloSAT,
                      'nocertificadosat'=>$result->TimbrarCFDIResult->Timbre->NumeroCertificadoSAT,
                      'nocertificado'=>$numeroCertificado,
                      'certificado'=>$numeroCertificado,
                      'fechatimbre'=>date('Y-m-d G:i:s'),
                      'Estado'=>1
                    );
                    $this->ModeloCatalogos->updateCatalogo('f_facturas',$updatedatossat,array('FacturasId'=>$datosFactura['factura_id'])); 
                //} 
            
                //$this->ModeloCatalogos->updateCatalogo('f_facturas',array('Estado'=>1),array('FacturasId'=>$datosFactura['factura_id']));

                $resultado = array(
                          'resultado'=>'correcto',
                          'Mensajeadicional'=>'Factura Generada',
                          'facturaId'=>$facturaId
                          );
            return $resultado;
           }  catch (Exception $e) {
              $resultado = array(
                          'resultado'=>'error',
                          'CodigoRespuesta'=>$result->TimbrarCFDIResult->CodigoRespuesta,
                          'MensajeError'=>$result->TimbrarCFDIResult->MensajeError,
                          'Mensajeadicional'=>'PORFAVOR RETIMBRAR MAS TARDE',
                          'facturaId'=>$facturaId,
                          'info'=>$result
                          );
             
            $this->ModeloCatalogos->updateCatalogo('f_facturas',array('Estado'=>2,'mensajeerror'=>json_encode($result)),array('FacturasId'=>$datosFactura['factura_id']));
            
            return $resultado;
                
           }
        
        }
        
        return $resultado;

    }
    function getArrayParaCadenaOriginal($facturaId){
      $datosFactura=$this->ModeloCatalogos->getselectwheren('f_facturas',array('FacturasId'=>$facturaId));
      $datosFactura=$datosFactura->result();
      $datosFactura=$datosFactura[0];

      $datosconfiguracion=$this->ModeloCatalogos->getselectwheren('f_configuraciones',array('ConfiguracionesId'=>1));
      $datosconfiguracion=$datosconfiguracion->result();
      $datosconfiguracion=$datosconfiguracion[0];
      //=======================================================================
            $conceptos = array();
            $datosconceptos=$this->ModeloCatalogos->getselectwheren('f_facturas_servicios',array('FacturasId'=>$facturaId));
            foreach ($datosconceptos->result() as $item) {
              $unidad=$this->ModeloCatalogos->nombreunidadfacturacion($item->Unidad);
              $conceptos[]    =   array(
                                       'cantidad' => $item->Cantidad,
                                       'unidad' =>  $unidad,
                                       'descripcion' =>  $item->Descripcion,
                                       'valorUnnitario' =>  $item->Cu,
                                       'importe' =>  $item->Importe
                                    );
            }
      //=======================================================================
      $datos=array(
                  'comprobante' => array(
                                            'version' => '3.2',  //Requerido                     
                                            'fecha' => $this->fechahoyc,       //Requerido                
                                            'tipoDeComprobante' => $datosFactura->TipoComprobante,//Requedido                       
                                            'formaDePago' => $datosFactura->FormaPago,                       
                                            'subtotal' => $datosFactura->subtotal,                       
                                            'total' => $datosFactura->total,                       
                                            'metodoDePago' => $datosFactura->MetodoPago,                       
                                            'lugarExpedicion' => $datosconfiguracion->LugarExpedicion                       
                                        ),
                  'emisor'    => array(
                                            'rfc' => $datosconfiguracion->Rfc,//Requedido
                                            'domocilioFiscal' => array(
                                                                        'calle' => $datosconfiguracion->Calle,//Requerido
                                                                        'municipio' => $datosconfiguracion->Municipio,//Requerido 
                                                                        'estado' => $datosconfiguracion->Estado,//Requerido 
                                                                        'pais' => $datosconfiguracion->PaisExpedicion,//Requedido
                                                                        'codigoPostal' => $datosconfiguracion->CodigoPostal//Requedido
                                                                        ),
                                            'expedidoEn' => array(
                                                                        'pais' => $datosconfiguracion->PaisExpedicion//Requedido
                                                                 ),
                                            'regimenFiscal' => array(
                                                                        'regimen' => $datosconfiguracion->CodigoPostal//Requedido
                                                                    )
                                      ),
                  'receptor'    => array(
                                            'rfc' => $datosFactura->Rfc,//Requedido
                                            'nombreCliente' => $datosFactura->Nombre,//Opcional
                                            'domicilioFiscal' => array( 
        
           
                                                                        'calle' => '',//Opcional
                                                                        'noExterior' => '',//Opcional
                                                                        'colonia' => '',//Opcional                   todos son datos del receptor
                                                                        'municipio' => '',//Opcional
                                                                        'estado' => '',//Opcional
                                                                        'pais' => $datosFactura->PaisReceptor //Requedido
                                                                       )
                                      ),
                  'conceptos' => $conceptos,
             
                  'traslados' => array(
                                        'impuesto' => '002',//iva
                                        'tasa' => 16.00,
                                        'importe' => $datosFactura->subtotal,
                                        'totalImpuestosTrasladados' => $datosFactura->iva
                                      )
                                      
                );
         return $datos;
    }
    private function getXML($facturaId,$sello,$certificado,$numeroCertificado,$datosfacturaadd) {
      log_message('error', '$sello c: '.$sello);
        
        //==============================================================================================================

        $datosFactura=$this->ModeloCatalogos->getselectwheren('f_facturas',array('FacturasId'=>$facturaId));
        $datosFactura=$datosFactura->result();
        $datosFactura=$datosFactura[0];
        if($datosFactura->serie=='D'){
            $ConfiguracionesId=2;
            $serie='D';
          }else{
            $ConfiguracionesId=1;
            $serie='U';
          }
        //=============================================================================================
        $datosconfiguracion=$this->ModeloCatalogos->getselectwheren('f_configuraciones',array('ConfiguracionesId'=>$ConfiguracionesId));
        $datosconfiguracion=$datosconfiguracion->result();
        $datosconfiguracion=$datosconfiguracion[0];
        //=========================================
        $clienteId=$datosFactura->Clientes_ClientesId;
        $fac_Rfc=$datosFactura->Rfc;
        $datoscliente=$this->ModeloCatalogos->getselectwheren('cliente_has_datos_fiscales',array('rfc'=>$fac_Rfc,'idCliente'=>$clienteId));
        $datoscliente=$datoscliente->result();
        $datoscliente=$datoscliente[0];
        //===============================================================================================================
            $xmlConceptos = '';
            $conceptos = array();
            $datosconceptos=$this->ModeloCatalogos->getselectwheren('f_facturas_servicios',array('FacturasId'=>$facturaId));
            foreach ($datosconceptos->result() as $item) {
                $unidad=$this->ModeloCatalogos->nombreunidadfacturacion($item->Unidad);
                $xmlConceptos.= '<cfdi:Concepto cantidad="' . chop(ltrim(trim($item->Cantidad))) . '" ';
                $xmlConceptos.= 'unidad="' . chop(ltrim($unidad)) . '" descripcion="' . chop(ltrim($item->servicioId)) . '"';
                $xmlConceptos.= ' valorUnitario="' . chop(ltrim(trim(round($item->Cu, 2)))) . '" importe="' . round(($item->Importe), 2) . '"/>';

              $conceptos[]    =   array(
                                       'cantidad' => $item->Cantidad,
                                       'unidad' =>  $item->Unidad,
                                       'descripcion' =>  $item->Descripcion,
                                       'valorUnnitario' =>  $item->Cu,
                                       'importe' =>  $item->Importe
                                    );
            }
        //===============================================================================================================
        
        /******************************************************/
        /*          INICIA ASIGNACION DE VARIABLES           */
        /****************************************************/
        //$certificado=str_replace("\n", "", $certificado);
        $caract   = array('&','°','á','é','í','ó','ú','Á','É','Í','Ó','Ú','Ã‘','"',"'");
        $caract2  = array('&amp;','','a','e','i','o','u','A','E','I','O','U','Ñ','&quot;','&apos;'); 
        $caractc   = array('&','°','á','é','í','ó','ú','Á','É','Í','Ó','Ú');
        $caract2c  = array('&amp;','','a','e','i','o','u','A','E','I','O','U'); 
        $fecha=$this->fechahoyc;
        $calleEmisor=$datosconfiguracion->Calle;
        $NoexteriorEmisor=$datosconfiguracion->Noexterior;
        $coloniaEmisor=$datosconfiguracion->Colonia;
        $localidadEmisor=$datosconfiguracion->localidad;
        $estadoEmisor=$datosconfiguracion->Estado;
        $municipioEmisor=$datosconfiguracion->Municipio;
        $codigoPostalEmisor=$datosconfiguracion->CodigoPostal;
        $formaPago=$datosFactura->FormaPago;
        $subtotal=$datosFactura->subtotal;
        $total=$datosFactura->total;
        $uso_cfdi=$datosFactura->uso_cfdi;
        $FacturasId=$datosFactura->FacturasId;
        $f_relacion=$datosFactura->f_relacion;
        $f_r_tipo=$datosFactura->f_r_tipo;
        $f_r_uuid=$datosFactura->f_r_uuid;
        $impuesto='002';
        $tasa=16.00;
        $impuestosTrasladados=$datosFactura->iva;
        if($this->trunquearredondear==0){
          $impuestosTrasladados=round($impuestosTrasladados,2);
        }else{
          $impuestosTrasladados=floor(($impuestosTrasladados*100))/100;
        }
        $tipoComprobante=$datosFactura->TipoComprobante;
        $metodoPago=$datosFactura->MetodoPago;
        $lugarExpedicion=$datosconfiguracion->LugarExpedicion;
        $rfcEmisor=$datosconfiguracion->Rfc;
        $nombreEmisor=$datosconfiguracion->Nombre;
        $paisEmisor=$datosconfiguracion->Pais;
        $paisExpedicion=$datosconfiguracion->PaisExpedicion;
        $regimenFiscal=$datosconfiguracion->Regimen;
        //if($sello===''){
          $rfcReceptor=str_replace(array('&','ñ'),array('&amp;','&ntilde;'), $datosFactura->Rfc);// cuando el RFC tenga & se remplazara con &amp; para poder ser procesado, este mismo metodo se podria ocupar en el nombre
        //}else{
          //$rfcReceptor=str_replace(' ', '', $datosFactura->Rfc);  
        //}
        
        $paisReceptor=$datosFactura->PaisReceptor;
        
        $localidadReceptor='';
        
        $xmlConceptos=$xmlConceptos;
        $nombreCliente=strval(str_replace($caract, $caract2, $datosFactura->Nombre));
        $CondicionesDePago = strval(str_replace($caractc, $caract2c, $datosFactura->CondicionesDePago));
        //$calleReceptor=$datosFactura['calleReceptor'];
        //$noExteriorReceptor=$datosFactura['noExteriorReceptor'];
        //$municipioReceptor=$datosFactura['municipioReceptor'];
        //$estadoReceptor=$datosFactura['estadoReceptor'];
        //$coloniaReceptor=$datosFactura['coloniaReceptor'];
        $folio=$datosFactura->Folio;

        $isr=$datosFactura->isr;
        $ivaretenido=$datosFactura->ivaretenido;
        $cedular=$datosFactura->cedular;
        $cincoalmillarval=$datosFactura->cincoalmillarval;
        $outsourcing=$datosFactura->outsourcing;
        $totalimpuestosretenido=$isr+$ivaretenido+$outsourcing;

        if ($totalimpuestosretenido==0) {
            $TotalImpuestosRetenidoss='';
        }else{
            $TotalImpuestosRetenidoss='TotalImpuestosRetenidos="'.round($totalimpuestosretenido,2).'"';
        }
        if ($cedular!=0) {
            $impuestoslocal='xmlns:implocal="http://www.sat.gob.mx/implocal"';
            $impuestoslocal2=' http://www.sat.gob.mx/implocal http://www.sat.gob.mx/sitio_internet/cfd/implocal/implocal.xsd';
        }else{
            $impuestoslocal='';
            $impuestoslocal2='';
        }
        if ($cincoalmillarval>0) {
            $impuestoslocal='xmlns:implocal="http://www.sat.gob.mx/implocal"';
            $impuestoslocal2=' http://www.sat.gob.mx/implocal http://www.sat.gob.mx/sitio_internet/cfd/implocal/implocal.xsd';
        }else{
            $impuestoslocal='';
            $impuestoslocal2='';
        }

        
        //cambios para poner el acr?nimo de la moneda en el XML
        if ($datosFactura->moneda=="USD") {
            $moneda="USD";
            $TipoCambio='TipoCambio="19.35"';
        }elseif ($datosFactura->moneda=="EUR") {
            $moneda="EUR";
            $TipoCambio='';
        }else{
            $moneda="MXN";
            $TipoCambio='';
        }
        //DATOS DE TIMBRE
        if(isset($datosFactura->uuid)) { 
        
            $uuid = $datosFactura->uuid;
            
        } else {
            
            $uuid= '';   
        }
        if(isset($datosFactura->sellosat)) {
            
            $selloSAT = $datosFactura->sellosat;
        } else {
            
            $selloSAT = '';
        }
        if(isset($datosFactura->certificado)) {   
            $certificadoSAT=$datosFactura->certificado;
        } else {
            $certificadoSAT='';
        }
        /*
        if(!empty($datosFactura['noInteriorReceptor'])) {   
            $noInt = '" noInterior="' . $datosFactura['noInteriorReceptor'] . '';
        } else {
            
            $noInt = '';
        }
        */
        /*  $minuto=date('i');
        $m=$minuto-1;
        $contar=strlen($m);
        if($contar==1)
        {
        $min='0'.$m;
        }else{$min=$m;} */  
        
        
        
        //.date('Y-m-d').'T'.date('H:i').':'.$segundos.

        $importabasetotal=0; 
        
        /******************************************************/
        /*          TERMINA ASIGNACION DE VARIABLES          */
        /****************************************************/   
        /*
        $horaactualm1=date('H:i:s');
        $horaactualm1 = strtotime ( '-1 minute' , strtotime ( $horaactualm1 ) ) ;
        $horaactualm1=date ( 'H:i:s' , $horaactualm1 );
        */
        $factura_detalle = $this->ModeloCatalogos->traeProductosFactura($FacturasId); 
        $descuentogeneral=0;
        foreach ($factura_detalle->result() as $facturadell) {
          $descuentogeneral=$descuentogeneral+$facturadell->descuento;
        }

        if ($descuentogeneral>0) {
          $xmldescuentogene=' Descuento="'.$descuentogeneral.'" ';
        }else{
          $xmldescuentogene=' ';
        }
        if($rfcReceptor=='XAXX010101000'){
            $DomicilioFiscalReceptor=$codigoPostalEmisor;
            $RegimenFiscalReceptor=616;
            if($datosFactura->pg_periodicidad=='05'){
              $RegimenFiscalReceptor=621;
              //$regimenFiscal=621;
            }
            if($datosFactura->pg_periodicidad==5){
              $RegimenFiscalReceptor=621;
              //$regimenFiscal=621;
            }
            $uso_cfdinew='S01';
        }else{
            $DomicilioFiscalReceptor=$datosFactura->Cp;
            $RegimenFiscalReceptor=$datosFactura->r_regimenfiscal;
            $uso_cfdinew=$uso_cfdi;
        }
        $fechageneracion=$datosfacturaadd['fechageneracion'];
        $xml= '<?xml version="1.0" encoding="utf-8"?>';
        $xml.='<cfdi:Comprobante xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" '.$impuestoslocal.' xsi:schemaLocation="http://www.sat.gob.mx/cfd/4 http://www.sat.gob.mx/sitio_internet/cfd/4/cfdv40.xsd'.$impuestoslocal2.'" Exportacion="01" ';
        $xml .= 'Version="4.0" Serie="'.$serie.'" Folio="'.$folio.'" Fecha="'.$fechageneracion.'" FormaPago="'.$metodoPago.'" ';
          if($CondicionesDePago!=''){
            $xml.='CondicionesDePago="'.$CondicionesDePago.'" ';
          }
        //$xml .= 'SubTotal="'.$subtotal.'" Descuento="0.00" Moneda="'.$moneda.'" TipoCambio="1" Total="'.$total.'" TipoDeComprobante="I" MetodoPago="'.$formaPago.'" LugarExpedicion="' .$codigoPostalEmisor . '" ';
        $xml .= 'SubTotal="'.$subtotal.'" Moneda="'.$moneda.'" '.$xmldescuentogene.' '.$TipoCambio.'  Total="'.round($total,2).'" TipoDeComprobante="'.$tipoComprobante.'" MetodoPago="'.$formaPago.'" LugarExpedicion="' .$codigoPostalEmisor . '" ';
        $xml .='xmlns:cfdi="http://www.sat.gob.mx/cfd/4" NoCertificado="'.$numeroCertificado.'" Certificado="'.$certificado.'" Sello="'.$sello.'">';
                if($f_relacion==1){
                    $xml .='<cfdi:CfdiRelacionados TipoRelacion="'.$f_r_tipo.'">
                                <cfdi:CfdiRelacionado UUID="'.$f_r_uuid.'"/>
                            </cfdi:CfdiRelacionados>';
                }
                //if($rfcReceptor=='XAXX010101000' and $datosFactura->pg_global==1){//para publico en general si es global se despliegan estos campos, pero si es individual no y el Nombre receptor debera de ser diferente a PUBLICO EN GENERAL por ejemplo el nombre del cliente, esto se modificara asta platicarlo
                if($rfcReceptor=='XAXX010101000' and $datosFactura->pg_global==1){
                    $xml .= '<cfdi:InformacionGlobal Año="'.$datosFactura->pg_anio.'" Meses="'.$datosFactura->pg_meses.'" Periodicidad="'.$datosFactura->pg_periodicidad.'"/>';
                }
        $xml .= '<cfdi:Emisor Nombre="'.$nombreEmisor.'" RegimenFiscal="'.$regimenFiscal.'" Rfc="'.$rfcEmisor.'"></cfdi:Emisor>';
        $xml .= '<cfdi:Receptor Rfc="'.$rfcReceptor.'" Nombre="'.$nombreCliente.'" DomicilioFiscalReceptor="'.$DomicilioFiscalReceptor.'" RegimenFiscalReceptor="'.$RegimenFiscalReceptor.'" UsoCFDI="'.$uso_cfdinew.'" ></cfdi:Receptor>';
        $xml .='<cfdi:Conceptos>';
        
        //$isr=$datosFactura['isr'];
        //$ivaretenido=$datosFactura['ivaretenido'];
        //$cedular=$datosFactura['cedular'];

        
        $partidaproducto=1;
        $niva=$tasa/100;
        $comparariva = $datosFactura->iva;//colocar
        foreach ($factura_detalle->result() as $facturadell) {
            $valorunitario=$facturadell->Cu;
            if($facturadell->descuento>0){
              $xmldescuento=' Descuento="'.$facturadell->descuento.'" ';
            }else{
              $xmldescuento='';
            }
            if($facturadell->Importe==$facturadell->descuento){
                $ObjetoImp='01';//no objeto de impuesto
            }else{
                $ObjetoImp='02';//si, objeto de impuestos
            }
            if($rfcReceptor=='XAXX010101000'){
              $xml_unidad='';
            }else{
              $xml_unidad=' Unidad="'.$facturadell->nombre.'" ';
            }
            
            //$xml .='<cfdi:Concepto ClaveUnidad="'.$facturadell['cunidad'].'" Unidad="'.$facturadell['nombre'].'" ClaveProdServ="'.$facturadell['ClaveProdServ'].'" NoIdentificacion="'.str_pad((int) $partidaproducto,10,"0",STR_PAD_LEFT).'" Cantidad="'.$facturadell['Cantidad'].'" Descripcion="'.$facturadell['Descripcion'].'" ValorUnitario="'.$facturadell['Cu'].'" Importe="'.$facturadell['Importe'].'" Descuento="0.00">';
            $xml .='<cfdi:Concepto ClaveUnidad="'.$facturadell->cunidad.'" ClaveProdServ="'.$facturadell->ClaveProdServ.'"';
             //$xml.='NoIdentificacion="'.str_pad((int) $partidaproducto,10,"0",STR_PAD_LEFT).'" ';
             $xml.=' Cantidad="'.$facturadell->Cantidad.'" '.$xml_unidad.' Descripcion="'.utf8_encode(str_replace($caract, $caract2, $facturadell->Descripcion)).'" ValorUnitario="'.$valorunitario.'" Importe="'.number_format(round($facturadell->Importe,2), 2, '.', '').'" '.$xmldescuento.' ObjetoImp="'.$ObjetoImp.'">';
                if($comparariva>0){
                //if($facturadell->iva>0){
                  $importabase=$facturadell->Importe-$facturadell->descuento;
                  if($this->trunquearredondear==0){
                    $trasladadoimporte=round($importabase*$niva,4);
                  }else{
                    $trasladadoimporte=$importabase*$niva;
                    $trasladadoimporte=floor(($trasladadoimporte*100))/100;
                  }
                  if($importabase>0){
                    //log_message('error', $inicio.'      '.$fin);
                    
                $xml .='<cfdi:Impuestos>';
                    $xml .='<cfdi:Traslados>';
                    $importabasetotal=$importabasetotal+$importabase;
                        
                    if($facturadell->iva>0){
                        $xml .='<cfdi:Traslado Base="'.$importabase.'" Impuesto="002" TipoFactor="Tasa" TasaOCuota="'.$niva.'0000" Importe="'.$trasladadoimporte.'"></cfdi:Traslado>';
                    }else{
                      $importabasetotal=$importabasetotal-$importabase;
                        $xml .='<cfdi:Traslado Base="'.$importabase.'" Impuesto="002" TipoFactor="Exento" ></cfdi:Traslado>';
                    }
                    $xml .='</cfdi:Traslados>';
                    //-----------------------------------------------------------------------------------
                    if($isr!=0 ||$ivaretenido!=0||$outsourcing>0){
                        $xml .='<cfdi:Retenciones>';
                        if ($isr!=0) {
                            $xml .='<cfdi:Retencion Base="'.$facturadell->Importe.'" Impuesto="001" TipoFactor="Tasa" TasaOCuota="0.100000" Importe="'.round($facturadell->Importe*0.100000,2).'"></cfdi:Retencion>';
                        }
                        if ($ivaretenido!=0) {
                            $xml .='<cfdi:Retencion Base="'.$facturadell->Importe.'" Impuesto="002" TipoFactor="Tasa" TasaOCuota="0.106666" Importe="'.round($facturadell->Importe*0.106666,2).'"></cfdi:Retencion>'; 
                        }   
                        if ($outsourcing!=0) {
                            $xml .='<cfdi:Retencion Base="'.$facturadell->Importe.'" Impuesto="002" TipoFactor="Tasa" TasaOCuota="0.060000" Importe="'.round($facturadell->Importe*0.06,2).'"></cfdi:Retencion>'; 
                        }      
                        $xml .='</cfdi:Retenciones>';
                    }
                    //------------------------------------------------------------------------------------- 
                    //$xml .='<cfdi:Retenciones>';
                    //    $xml .='<cfdi:Retencion Base="1000" Impuesto="002" TipoFactor="Tasa" TasaOCuota="0.160000" Importe="0.00" />';      
                    //$xml .='</cfdi:Retenciones>';
                $xml .='</cfdi:Impuestos>';
              }
            }
            $xml .='</cfdi:Concepto>';
            $partidaproducto++;
        }
        $xml .='</cfdi:Conceptos>';
        if($comparariva>0){
            //$xml .='<cfdi:Impuestos TotalImpuestosRetenidos="0.00" TotalImpuestosTrasladados="'.$impuestosTrasladados.'">';
            $xml .='<cfdi:Impuestos '.$TotalImpuestosRetenidoss.' TotalImpuestosTrasladados="'.$impuestosTrasladados.'">';
                if($isr!=0 ||$ivaretenido!=0||$outsourcing>0){
                    $xml .='<cfdi:Retenciones>';
                    if ($isr!=0) {
                        $xml .='<cfdi:Retencion Impuesto="001" Importe="'.number_format(round($isr,2), 2, ".", "").'"></cfdi:Retencion>';
                    }
                    if ($ivaretenido!=0) {
                        $xml .='<cfdi:Retencion Impuesto="002" Importe="'.number_format(round($ivaretenido,2), 2, ".", "").'"></cfdi:Retencion>'; 
                    } 
                    if ($outsourcing>0) {
                        $xml .='<cfdi:Retencion Impuesto="002" Importe="'.number_format(round($outsourcing,2), 2, ".", "").'"></cfdi:Retencion>'; 
                    }      
                    $xml .='</cfdi:Retenciones>';
                }
               
                
                
                $xml .='<cfdi:Traslados>';
                    
                        $xml .='<cfdi:Traslado Base="'.$importabasetotal.'" Impuesto="002" TipoFactor="Tasa" TasaOCuota="0.160000" Importe="'.$impuestosTrasladados.'"></cfdi:Traslado>';    
                $xml .='</cfdi:Traslados>';
            $xml .='</cfdi:Impuestos>';
        }
        /*
        if($cedular!=0){
            
        }
        */
        if($cedular!=0||$uuid||$cincoalmillarval>0) {            
            $xml .= '<cfdi:Complemento>';
            if ($cedular!=0) {
                //$xml .= '<cfdi:Complemento xmlns:cfdi="http://www.sat.gob.mx/cfd/3" >';
                $xml .= '<implocal:ImpuestosLocales version="1.0" TotaldeTraslados="0.00" TotaldeRetenciones="'.$cedular.'" >';
                $xml .= '<implocal:RetencionesLocales  TasadeRetencion="01.00" ImpLocRetenido="CEDULAR" Importe="'.$cedular.'"/>';
                $xml .= '</implocal:ImpuestosLocales>';
            }
            if ($cincoalmillarval>0) {
                $xml .= '<implocal:ImpuestosLocales version="1.0" TotaldeTraslados="0.00" TotaldeRetenciones="'.$cincoalmillarval.'" >';
                  $xml .= '<implocal:RetencionesLocales Importe="'.$cincoalmillarval.'" TasadeRetencion="0.50" ImpLocRetenido=".005 Insp y Vig" />';
                $xml .= '</implocal:ImpuestosLocales>';
            }
            if ($uuid) {
                $xml .='<tfd:TimbreFiscalDigital Version="1.1" RfcProvCertif="FLI081010EK2"';
                $xml .='UUID="'.$uuid.'" FechaTimbrado="'.$fecha.'" ';
                $xml .='SelloCFD="'.$sello.'" NoCertificadoSAT="'.$certificadoSAT.'" SelloSAT="'.$selloSAT.'"';
                $xml .='xmlns:tfd="http://www.sat.gob.mx/TimbreFiscalDigital"';
                $xml .= 'xsi:schemaLocation="http://www.sat.gob.mx/TimbreFiscalDigital http://www.sat.gob.mx/sitio_internet/cfd/TimbreFiscalDigital/TimbreFiscalDigitalv11.xsd" />';
            }
            







                
            $xml .= '</cfdi:Complemento>';
        }
        
        $xml .= '</cfdi:Comprobante>';
        
       
        return $xml;
    }
    public function generaCadenaOriginalReal($xml,$carpeta) {   
        error_reporting(0); 
        $paso = new DOMDocument();
        //log_message('error', 'generaCadenaOriginalReal xml: '.$xml);
        $paso->loadXML($xml);
        
        $xsl = new DOMDocument();               
        
        $file= base_url().$carpeta."/xslt40/cadenaoriginal_4_0.xslt";
        
        $xsl->load($file);
        
        $proc = new XSLTProcessor();
        // activar php_xsl.dll
        
        $proc->importStyleSheet($xsl); 
        
        $cadena_original = $proc->transformToXML($paso);
        
        //echo '<br><br>' . $cadena_original . '<br><br>';
        
        $str = mb_convert_encoding($cadena_original, 'UTF-8');                      
   
        //GUARDAR CADENA ORIGINAL EN ARCHIVO  
         
        $sello = utf8_decode($str);
                        
        $sello = str_replace('#13','',$sello);
        
        $sello = str_replace('#10','',$sello);
        
        //$fp = fopen(DIR_FILES . $carpeta . '/temporalsat/CadenaOriginal.txt', 'w');//Cadena original 
        /*
        $fp = fopen(base_url() . $carpeta . '/temporalsat/CadenaOriginal.txt', 'w');//Cadena original 
        
        fwrite($fp, $sello);
        
        fclose($fp);
        */
        file_put_contents( $carpeta . '/temporalsat/CadenaOriginal.txt',$sello);
        return $sello;    
    }
    private function getCodigoMetodoPago($text){ 
        //log_message('error', 'getCodigoMetodoPago: '.$text);   
        if ($text=='Efectivo') {
          $textl='01';
        }elseif ($text=='ChequeNominativo') {
          $textl='02';
        }elseif ($text=='TransferenciaElectronicaFondos') {
          $textl='03';
        }elseif ($text=='TarjetasDeCredito') {
          $textl='04';
        }elseif ($text=='MonederoElectronico') {
          $textl='05';
        }elseif ($text=='DineroElectronico') {
          $textl='06';
        }elseif ($text=='Tarjetas digitales') {
          $textl='07';
        }elseif ($text=='ValesDeDespensa') {
          $textl='08';
        }elseif ($text=='TarjetaDebito') {
          $textl='28';
        }elseif ($text=='TarjetaServicio') {
          $textl='29';
        }elseif ($text=='Otros') {
          $textl='99';
        }elseif ($text=='DacionPago') {
          $textl='12';
        }elseif ($text=='PagoSubrogacion') {
          $textl='13';
        }elseif ($text=='PagoConsignacion') {
          $textl='14';
        }elseif ($text=='Condonacion') {
          $textl='15';
        }elseif ($text=='Compensacion') {
          $textl='17';
        }elseif ($text=='Novacion') {
          $textl='23';
        }elseif ($text=='Confusion') {
          $textl='24';
        }elseif ($text=='RemisionDeuda') {
          $textl='25';
        }elseif ($text=='PrescripcionoCaducidad') {
          $textl='26';
        }elseif ($text=='SatisfaccionAcreedor') {
          $textl='27';
        }elseif ($text=='AplicacionAnticipos') {
          $textl='30';
        }elseif ($text=='PorDefinir') {
          $textl='99';
        }elseif ($text=='intermediariopagos') {
          $textl='31';
        }
            return  $textl;
    }
    function pruebas(){
      $file=file_get_contents(base_url().'kyocera/temporalsat/Serial.txt');
      echo $file;
    }

    function generafacturarabierta(){
      $datas = $this->input->post();
      if(isset($datas['conceptos'])){
            $conceptos = $datas['conceptos'];
            unset($datas['conceptos']);
      }
      $save=$datas['save'];
      $saveante=$datas['saveante'];
      //log_message('error', 'save: '.$save);
      //log_message('error', 'saveante: '.$saveante);

      $idcliente=$datas['idcliente'];
      $rfc=$datas['rfc'];
      $dcliente=$this->ModeloCatalogos->getselectwheren('clientes',array('id'=>$idcliente));
      foreach ($dcliente->result() as $item) {
        $empresa=$item->empresa;
        $municipio=$item->municipio;
        $idestado=$item->estado;
      }
      $destado=$this->ModeloCatalogos->getselectwheren('estado',array('EstadoId'=>$idestado));
      foreach ($destado->result() as $item) {
        $estado=$item->Nombre;
      }
      $direccion=$this->ModeloCatalogos->getselectwheren('cliente_has_datos_fiscales',array('id'=>$rfc,'activo'=>1));
      foreach ($direccion->result() as $item) {
        $rfc_id = $item->id;
        $razon_social = $item->razon_social;
        $rfc = $item->rfc;
        $cp = $item->cp;
        $num_ext = $item->num_ext;
        $num_int = $item->num_int;
        $colonia = $item->colonia;
        $calle = $item->calle;
        if ($item->estado!=null) {
          $estado=$item->estado;
        }
        if ($item->municipio!=null) {
          $municipio=$item->municipio;
        }
        $RegimenFiscalReceptor=$item->RegimenFiscalReceptor;
      }
      if($rfc=='XAXX010101000'){
        if(isset($datas['pg_global'])){
          if($datas['pg_global']==0){
            $razon_social=$empresa;
          }
        }else{
          if($razon_social=='PUBLICO EN GENERAL'){
            $razon_social=$empresa;
          }
          
        }
      }
      $pais = 'MEXICO';
      $TipoComprobante='I';
      if($datas['f_r']==1){
        if($datas['f_r_t']=='01'){
          $TipoComprobante='E';
        }
      }
      $TipoComprobante=$datas['TipoComprobante'];
      if($datas['tipov']==1){
        $serie='U';
      }else{
        $serie='D';
      }
      if($save==0){
        $ultimoFolioval=0;
      }else{
        $ultimoFolioval=$this->Rentas_model->ultimoFolio($serie) + 1;
      }
      //===========================
        $info_subtotal=floatval($datas['subtotal']);
        $info_iva=floatval($datas['iva']);
        $info_total=floatval($datas['total']);
        $recalcular=1;
        if($datas['vriva']>0){
          $recalcular=0;
        }
        if($datas['visr']>0){
          $recalcular=0;
        }
        if($datas['v5millar']>0){
          $recalcular=0;
        }
        if($datas['outsourcing']>0){
          $recalcular=0;
        }
        if($datas['des']>0){
          $recalcular=0;
          $subtotaldes=floatval($info_subtotal)-floatval($datas['des']);

          $info_iva=round($subtotaldes*0.16,2);
          $info_total=$subtotaldes+$info_iva;

        }
        if($recalcular==1){
          //log_message('error',' original subtotal: '.$info_subtotal.' iva: '.$info_iva.' total: '.$info_total);
          $info_iva=round($info_subtotal*0.16,2);
          $info_total=$info_subtotal+$info_iva;
          //log_message('error',' nueva subtotal: '.$info_subtotal.' iva: '.$info_iva.' total: '.$info_total);
        }
      //=====================================
      $data = array(
                "serie"         => $serie,
                "nombre"        => $razon_social,
                "direccion"     => $calle.' No '. $num_ext.' '.$num_int.' Col: '. 
                                   $colonia.', '. 
                                   $municipio.', '.$estado . '. ', 
                "cp"            => $cp,
                "rfc"           => $rfc,
                "rfc_id"        => $rfc_id,
                "folio"         => $ultimoFolioval,
                "PaisReceptor"  =>$pais,
                "Clientes_ClientesId"       => $idcliente,
                //"status"        => 1,
                "TipoComprobante"=>$TipoComprobante,
                "usuario_id"       => $this->idpersonal,
                "creada_sesionId"=> $this->idpersonal,
                "rutaXml"           => '',
                "FormaPago"         => $datas['FormaPago'],
                "tarjeta"       => $datas['tarjeta'],
                "MetodoPago"        => $this->getCodigoMetodoPago($datas['MetodoPago']),
                "ordenCompra"   => '',//no se ocupa
                "moneda"        => $datas['moneda'],
                "observaciones" => $datas['observaciones'],
                "numproveedor"  => $datas['numproveedor'],
                "numordencompra"=> $datas['numordencompra'],
                "Lote"          => '',//no se ocupa
                "Paciente"      => '',//no se ocupa
                "Caducidad"     => '',//no se ocupa
                "uso_cfdi"      => $datas['uso_cfdi'],
                "subtotal"      => $info_subtotal,
                "iva"           => $info_iva,
                "total"         => $info_total,
                "honorario"     => $datas['subtotal'],
                "ivaretenido"   => $datas['vriva'],
                "isr"           => $datas['visr'],
                "cincoalmillarval"  => $datas['v5millar'],
                "CondicionesDePago" => $datas['CondicionesDePago'],
                "outsourcing" => $datas['outsourcing'],
                "facturaabierta"=>1,

                "f_relacion"    => $datas['f_r'],
                "f_r_tipo"      => $datas['f_r_t'],
                "f_r_uuid"      =>str_replace(' ', '', $datas['f_r_uuid']),
                "r_regimenfiscal"=>$RegimenFiscalReceptor,
                "save"=>$save
            
            );
      if(isset($datas['pg_global'])){
          $data['pg_global']=$datas['pg_global'];
      }
      if(isset($datas['pg_periodicidad'])){
          $data['pg_periodicidad']=$datas['pg_periodicidad'];
      }
      if(isset($datas['pg_meses'])){
          $data['pg_meses']=$datas['pg_meses'];
      }
      if(isset($datas['pg_anio'])){
          $data['pg_anio']=$datas['pg_anio'];
      }
        
      $FacturasId=$this->ModeloCatalogos->Insert('f_facturas',$data);
      $DATAc = json_decode($conceptos);
      for ($i=0;$i<count($DATAc);$i++) {

            $dataco['FacturasId']=$FacturasId;
            $dataco['Cantidad'] =$DATAc[$i]->Cantidad;
            $dataco['Unidad'] =$DATAc[$i]->Unidad;
            $dataco['servicioId'] =$DATAc[$i]->servicioId;
            $dataco['Descripcion'] =rtrim($DATAc[$i]->Descripcion);
            $dataco['Descripcion2'] =$DATAc[$i]->Descripcion2;
            $dataco['Cu'] =$DATAc[$i]->Cu;
            $dataco['descuento'] =$DATAc[$i]->descuento;
            $dataco['Importe'] =($DATAc[$i]->Cantidad*$DATAc[$i]->Cu);
            $dataco['iva'] =$DATAc[$i]->iva;
            $this->ModeloCatalogos->Insert('f_facturas_servicios',$dataco);
      }
      //=======================================
      /*
      if ($datas['tiporelacion']>0) {
        if ($datas['tiporelacion']==1) {//venta normal
          $this->ModeloCatalogos->Insert('factura_venta',array('combinada'=>0,'ventaId'=>$datas['ventaviculada'],'facturaId'=>$FacturasId));
        }
        if ($datas['tiporelacion']==2) {//venta combinada
          $this->ModeloCatalogos->Insert('factura_venta',array('combinada'=>1,'ventaId'=>$datas['ventaviculada'],'facturaId'=>$FacturasId));
        }
        if ($datas['tiporelacion']==3) {//poliza
          $this->ModeloCatalogos->Insert('factura_poliza',array('polizaId'=>$datas['ventaviculada'],'facturaId'=>$FacturasId));
        }
        if ($datas['tiporelacion']==4) {
          $this->ModeloCatalogos->Insert('factura_servicio',array('servicioId'=>$datas['ventaviculada'],'tipo'=>$datas['ventaviculadatipo'],'facturaId'=>$FacturasId));
        }
      }
      */
      $DATAvv = json_decode($datas['ventaviculada']);
      for ($i=0;$i<count($DATAvv);$i++) {
        
        if ($DATAvv[$i]->tiporelacion>0) {
          if ($DATAvv[$i]->tiporelacion==1) {//venta normal
            $this->ModeloCatalogos->Insert('factura_venta',array('combinada'=>0,'ventaId'=>$DATAvv[$i]->ventaviculada,'facturaId'=>$FacturasId));
          }
          if ($DATAvv[$i]->tiporelacion==2) {//venta combinada
            $this->ModeloCatalogos->Insert('factura_venta',array('combinada'=>1,'ventaId'=>$DATAvv[$i]->ventaviculada,'facturaId'=>$FacturasId));
          }
          if ($DATAvv[$i]->tiporelacion==3) {//poliza
            $this->ModeloCatalogos->Insert('factura_poliza',array('polizaId'=>$DATAvv[$i]->ventaviculada,'facturaId'=>$FacturasId));
          }
          if ($DATAvv[$i]->tiporelacion==4) {
            $this->ModeloCatalogos->Insert('factura_servicio',array('servicioId'=>$DATAvv[$i]->ventaviculada,'tipo'=>$DATAvv[$i]->ventaviculadatipo,'facturaId'=>$FacturasId));
          }
        }

      }
      //=======================================
      
      
      if($save==0){
        $respuesta = array(
                          'resultado'=>'correcto',
                          'Mensajeadicional'=>'Factura Guardada',
                          'facturaId'=>$FacturasId
                          );
        $this->ModeloCatalogos->updateCatalogo('f_facturas',array('Estado'=>2),array('FacturasId'=>$FacturasId));
        if($saveante>0){
          $this->ModeloCatalogos->updateCatalogo('f_facturas',array('activo'=>0),array('FacturasId'=>$saveante,'Estado!='=>1));
        }
        
      }else{
        $respuesta=$this->emitirfacturas($FacturasId);
      }
      
      echo json_encode($respuesta);
    }
    function cancelarCfdi(){
      $rutainterna= $_SERVER['DOCUMENT_ROOT'].'/kyocera2';//rutalocal
      //$rutainterna= $_SERVER['DOCUMENT_ROOT'];//rutaserver
      $ruta=$rutainterna.'/kyocera/temporalsat/';
      $rutaf=$rutainterna.'/kyocera/facturas/';
      $rutalog=$rutainterna.'/kyocera/facturaslog/';
      $rutaarchivos=$rutainterna.'/kyocera/elementos/';
      $productivo=0;//0 demo 1 producccion
      $datas = $this->input->post();
      $facturas=$datas['facturas'];
      $motivo=$datas['motivo'];
      $uuidrelacionado=$datas['uuidrelacionado'];

      unset($datas['facturas']);
      unset($datas['motivo']);
      unset($datas['uuidrelacionado']);

      $datosCancelacion=array();
      $DATAf = json_decode($facturas);
      $serie='U';
      for ($i=0;$i<count($DATAf);$i++) {
        $datosfactura=$this->ModeloCatalogos->getselectwheren('f_facturas',array('FacturasId'=>$DATAf[$i]->FacturasIds));
        $datosfactura=$datosfactura->result();
        $datosfactura=$datosfactura[0];
        $FacturasId=$datosfactura->FacturasId;
        $serie=$datosfactura->serie;
        
        $datosCancelacion[] = array(
                                  'RFCReceptor'=>$datosfactura->Rfc,
                                  'Total'=>round($datosfactura->total, 2),
                                  'UUID'=>$datosfactura->uuid,
                                  'Motivo'=>$motivo,
                                  'FolioSustitucion'=>$uuidrelacionado

                                );
        $this->liberarfacturas($DATAf[$i]->FacturasIds);
      }
      if($serie=='U'){
        $ConfiguracionesId=1;
        $ruta=$rutainterna.'/kyocera/temporalsat/';
        $rutaarchivos=$rutainterna.'/kyocera/elementos/';
      }
      if($serie=='D'){
        $ConfiguracionesId=2;
        $ruta=$rutainterna.'/kyocera/temporalsat2/';
        $rutaarchivos=$rutainterna.'/kyocera/elementos2/';
      }
      $datosconfiguracion=$this->ModeloCatalogos->getselectwheren('f_configuraciones',array('ConfiguracionesId'=>$ConfiguracionesId));
      $datosconfiguracion=$datosconfiguracion->result();
      $datosconfiguracion=$datosconfiguracion[0];
      $rFCEmisor=$datosconfiguracion->Rfc;
      $passwordClavePrivada=$datosconfiguracion->paswordkey;
      //===========================================================
        if($productivo==0){
          $URL_WS='https://app.fel.mx/WSTimbrado33Test/WSCFDI33.svc?WSDL';
        }elseif ($productivo==1) {
          //Produccion
          $URL_WS='https://www.fel.mx/WSTimbrado33/WSCFDI33.svc?WSDL';
        }else{
          $URL_WS='';
        }
        if($productivo==0){
          //usuario demo
          $password= "contRa$3na";
          $usuario= "HERD890308D33";
        }elseif ($productivo==1) {
          //Produccion
          $password= "@fuJXsS@";
          $usuario= "HERD890308UAA";
        }else{
          //usuario demo
          $password= "contRa$3na";
          $usuario= "HERD890308D33";
        }
      //=======================================================
        
        $fp = fopen($ruta.'pfx.pem', "r");
        $clavePrivada = fread($fp, filesize($ruta.'pfx.pem'));
        fclose($fp);
        
        
        
        $rutapass=$rutaarchivos.'passs.txt';
        $passwordClavePrivada = file_get_contents($rutapass);
        
        //$passwordClavePrivada='Daniel182 ';// solo para las pruebas
        
        $parametros = array(
            'usuario' => $usuario,
            'password' => $password,
            'rFCEmisor' => $rFCEmisor,
            'listaCFDI' => $datosCancelacion,
            'clavePrivada_Base64'=>$clavePrivada,
            'passwordClavePrivada'=> $passwordClavePrivada  );
        file_put_contents($rutalog.'log_cancelcion_envio_'.$this->fechahoyL.'xxxxx_.txt', json_encode($parametros));
        $clienteSOAP = new SoapClient($URL_WS);

        $result = $clienteSOAP->CancelarCFDI($parametros);

        $fileacuse1='log_cancelcion_success_'.$FacturasId.'_'.$datosfactura->uuid.'.xml';
        $ruta1 = $rutaf .$fileacuse1;

        file_put_contents($ruta1,json_encode($result));
        if($result->CancelarCFDIResult->OperacionExitosa) {
          $resultado = array(
                          'resultado'=>'correcto',
                          'Mensajeadicional'=>'Factura Cancelada',
                          'info'=>$result
                          );
          //Guardar en archivo  y la ruta en la bd del acuse de la cancelacion
                $fileacuse='acuseCancelacion_'.$FacturasId.'_'.$datosfactura->uuid.'.xml';
                $ruta = $rutaf .$fileacuse;
                //file_put_contents($rutalog.'log_'.$this->fechahoyL.'_.txt', json_encode($result));
                file_put_contents($ruta,$result->CancelarCFDIResult->XMLAcuse);

                $datosdecancelacion = array(
                                            'Estado'=>0,
                                            'rutaAcuseCancelacion '=>$fileacuse,
                                            'fechacancelacion'=>$this->fechahoyc
                                          );
                $this->ModeloCatalogos->updateCatalogo('f_facturas',$datosdecancelacion,array('FacturasId'=>$FacturasId));
                $this->ModeloCatalogos->Insert('bitacora_cancelaciones',array('tipo'=>1,'idfaccomp'=>$FacturasId,'motivo'=>$motivo,'foliorelacionado'=>$uuidrelacionado,'idpersonal'=>$this->idpersonal));
          
        }else{
          $resultado = array(
                          'resultado'=>'error',
                          'CodigoRespuesta'=>$result->CancelarCFDIResult->MensajeError,
                          'MensajeError'=>$result->CancelarCFDIResult->MensajeErrorDetallado,
                          'Mensajeadicional'=>'PORFAVOR CANCELAR MAS TARDE',
                          'info'=>$result
                          );
          file_put_contents($rutalog.'log_cancelacion_error_'.$this->fechahoyL.'_.txt', json_encode($result));
        }
        
        echo json_encode($resultado);
    }
    function cancelarCfdicomplemento(){
      $rutainterna= $_SERVER['DOCUMENT_ROOT'].'/kyocera2';//rutalocal
      //$rutainterna= $_SERVER['DOCUMENT_ROOT'];//rutaserver
      $ruta=$rutainterna.'/kyocera/temporalsat/';
      $rutaf=$rutainterna.'/kyocera/facturas/';
      $rutalog=$rutainterna.'/kyocera/facturaslog/';

      $productivo=1;//0 demo 1 producccion
      $datas = $this->input->post();
      $facturas=$datas['facturas'];
      $motivo=$datas['motivo'];
      $uuidrelacionado=$datas['uuidrelacionado'];
      
      unset($datas['facturas']);
      unset($datas['motivo']);
      unset($datas['uuidrelacionado']);

      $datosconfiguracion=$this->ModeloCatalogos->getselectwheren('f_configuraciones',array('ConfiguracionesId'=>1));
      $datosconfiguracion=$datosconfiguracion->result();
      $datosconfiguracion=$datosconfiguracion[0];
      $rFCEmisor=$datosconfiguracion->Rfc;
      $passwordClavePrivada=$datosconfiguracion->paswordkey;

      $datosCancelacion=array();
      //$DATAf = json_decode($facturas);
      //for ($i=0;$i<count($DATAf);$i++) {
        $datosfactura=$this->ModeloCatalogos->getselectwheren('f_complementopago',array('complementoId'=>$facturas));
        $datosfactura=$datosfactura->result();
        $datosfactura=$datosfactura[0];
        $FacturasId=$datosfactura->FacturasId;
        $datosCancelacion[] = array(
                                  'RFCReceptor'=>$datosfactura->R_rfc,
                                  'Total'=>0,
                                  'UUID'=>$datosfactura->uuid,
                                  'Motivo'=>$motivo,
                                  'FolioSustitucion'=>$uuidrelacionado

                                );
        //$this->liberarfacturas($DATAf[$i]->FacturasIds);
      //}
      //===========================================================
        if($productivo==0){
          $URL_WS='https://app.fel.mx/WSTimbrado33Test/WSCFDI33.svc?WSDL';
        }elseif ($productivo==1) {
          //Produccion
          $URL_WS='https://www.fel.mx/WSTimbrado33/WSCFDI33.svc?WSDL';
        }else{
          $URL_WS='';
        }
        if($productivo==0){
          //usuario demo
          $password= "contRa$3na";
          $usuario= "HERD890308D33";
        }elseif ($productivo==1) {
          //Produccion
          $password= "@fuJXsS@";
          $usuario= "HERD890308UAA";
        }else{
          //usuario demo
          $password= "contRa$3na";
          $usuario= "HERD890308D33";
        }
      //=======================================================
        
        $fp = fopen($ruta.'pfx.pem', "r");
        $clavePrivada = fread($fp, filesize($ruta.'pfx.pem'));
        fclose($fp);
        
        
        $rutaarchivos=$rutainterna.'/kyocera/elementos/';
        $rutapass=$rutaarchivos.'passs.txt';
        $passwordClavePrivada = file_get_contents($rutapass);
        
        //$passwordClavePrivada='Daniel182 ';// solo para las pruebas
        
        $parametros = array(
            'usuario' => $usuario,
            'password' => $password,
            'rFCEmisor' => $rFCEmisor,
            'listaCFDI' => $datosCancelacion,
            'clavePrivada_Base64'=>$clavePrivada,
            'passwordClavePrivada'=> $passwordClavePrivada  );
        file_put_contents($rutalog.'log_'.$this->fechahoyL.'xxxxx_.txt', json_encode($parametros));
        $clienteSOAP = new SoapClient($URL_WS);

        $result = $clienteSOAP->CancelarCFDI($parametros);

        if($result->CancelarCFDIResult->OperacionExitosa) {
          $resultado = array(
                          'resultado'=>'correcto',
                          'Mensajeadicional'=>'Factura Cancelada',
                          'info'=>$result
                          );
          //Guardar en archivo  y la ruta en la bd del acuse de la cancelacion
                $fileacuse='acuseCancelacion_'.$facturas.'_'.$datosfactura->uuid.'.xml';
                $ruta = $rutaf .$fileacuse;
                //file_put_contents($rutalog.'log_'.$this->fechahoyL.'_.txt', json_encode($result));
                file_put_contents($ruta,$result->CancelarCFDIResult->XMLAcuse);

                $datosdecancelacion = array(
                                            'Estado'=>0,
                                            'rutaAcuseCancelacion '=>$fileacuse,
                                            'fechacancelacion'=>$this->fechahoyc
                                          );
                $this->ModeloCatalogos->updateCatalogo('f_complementopago',$datosdecancelacion,array('complementoId'=>$facturas));
                $this->ModeloCatalogos->updateCatalogo('f_complementopago_documento',array('Estado'=>0),array('complementoId'=>$facturas));
                $this->ModeloCatalogos->Insert('bitacora_cancelaciones',array('tipo'=>2,'idfaccomp'=>$facturas,'motivo'=>$motivo,'foliorelacionado'=>$uuidrelacionado,'idpersonal'=>$this->idpersonal));
                $this->ModeloCatalogos->verificarpagosalperiodo($facturas);
          
        }else{
          $resultado = array(
                          'resultado'=>'error',
                          'CodigoRespuesta'=>$result->CancelarCFDIResult->MensajeError,
                          'MensajeError'=>$result->CancelarCFDIResult->MensajeErrorDetallado,
                          'Mensajeadicional'=>'PORFAVOR CANCELAR MAS TARDE',
                          'info'=>$result
                          );
          file_put_contents($rutalog.'log_'.$this->fechahoyL.'_.txt', json_encode($result));
        }
        
        echo json_encode($resultado);
    }
    function addcomplemento(){
      $data = $this->input->post();
      $rfcreceptor=$data['rfcreceptor'];
        $Serie = $data['Serie'];
        if($Serie=='D'){
          $confid=2;
        }else{
          $confid=1;
        }
        $arraydoc=$data['arraydocumento'];
        unset($data['arraydocumento']);
        //$idfactura=$data['idfactura'];
        //==================================================================
          //$datosfactura=$this->ModeloCatalogos->getselectwheren('f_facturas',array('FacturasId'=>$idfactura));
          //$datosfactura=$datosfactura->result();
          //$datosfactura=$datosfactura[0];
        //==================================================================
          $datosconfiguracion=$this->ModeloCatalogos->getselectwheren('f_configuraciones',array('ConfiguracionesId'=>$confid));
          $datosconfiguracion=$datosconfiguracion->result();
          $datosconfiguracion=$datosconfiguracion[0];
        //==================================================================

        //$Sello=$datosfactura->sellosat;
        //$Sello=$datosfactura->sellocadena;
        //========================================
          $direccion=$this->ModeloCatalogos->getselectwheren('cliente_has_datos_fiscales',array('rfc'=>$rfcreceptor,'activo'=>1));
          $razon_social=$data['razonsocialreceptor'];
          foreach ($direccion->result() as $item) {
            $razon_social = $item->razon_social;
          }
        //========================================
          if($rfcreceptor=='XAXX010101000'){
            $razon_social=$data['razonsocialreceptor'];
          }

        $datacp['Folio']=$data['Folio'];
        $datacp['Fecha']=$data['Fecha'];
        $datacp['Sello']='';
        //$datacp['NoCertificado']=$datosfactura->nocertificado;
        //$datacp['Certificado']=$datosfactura->certificado;
        $datacp['Certificado']='';
        $datacp['LugarExpedicion']=$data['LugarExpedicion'];
        $datacp['E_rfc']=$datosconfiguracion->Rfc;
        $datacp['E_nombre']=$datosconfiguracion->Nombre;
        $datacp['E_regimenfiscal']=$datosconfiguracion->Regimen;
        $datacp['R_rfc']=$rfcreceptor;
        $datacp['R_nombre']=$razon_social;
        $datacp['R_regimenfiscal']='';
        $datacp['FechaPago']=$data['Fechatimbre'];
        $datacp['FormaDePagoP']=$data['FormaDePagoP'];
        $datacp['MonedaP']=$data['ModedaP'];
        $datacp['Monto']=$data['Monto'];
        $datacp['NumOperacion']=str_replace(' ', '', $data['NumOperacion']);
        $datacp['CtaBeneficiario']=$data['CtaBeneficiario'];
        
        $datacp['Serie']=$Serie;//xxx
        $datacp['Foliod']=$data['Folio'];
        $datacp['MonedaDR']=$data['ModedaP'];
        $datacp['personalcreo']=$this->idpersonal;
        
        $datacp['f_relacion']    = $data['f_r'];
        $datacp['f_r_tipo']      = $data['f_r_t'];
        $datacp['f_r_uuid']      = $data['f_r_uuid'];
        $datacp['clienteId']      = $data['clienteId'];

        //==============================================================
        //$datacp['FacturasId']=$data['idfactura'];
        //$datacp['IdDocumento']=$data['IdDocumento'];
        //$datacp['NumParcialidad']=$data['NumParcialidad'];
        //$datacp['ImpPagado']=$data['Monto'];
        //$datacp['ImpSaldoAnt']=$data['ImpSaldoAnt'];
        //$datacp['ImpSaldoInsoluto']=$data['ImpSaldoAnt']-$data['Monto'];
        //$datacp['MetodoDePagoDR']=$datosfactura->FormaPago;
        //============================================================
        $datacp['UsoCFDI']='';
        $idcomplemento=$this->ModeloCatalogos->Insert('f_complementopago',$datacp);
        $DATAdoc = json_decode($arraydoc);
        for ($j=0;$j<count($DATAdoc);$j++){
          $datacdoc['complementoId']=$idcomplemento;
          $datacdoc['facturasId']=$DATAdoc[$j]->idfactura;
          $datacdoc['IdDocumento']=$DATAdoc[$j]->IdDocumento;
          $datacdoc['NumParcialidad']=$DATAdoc[$j]->NumParcialidad;
          $datacdoc['ImpSaldoAnt']=$DATAdoc[$j]->ImpSaldoAnt;
          $datacdoc['ImpPagado']=$DATAdoc[$j]->ImpPagado;
          $datacdoc['ImpSaldoInsoluto']=$DATAdoc[$j]->ImpSaldoInsoluto;
          $datacdoc['MetodoDePagoDR']=$DATAdoc[$j]->MetodoDePagoDR;
          $this->ModeloCatalogos->Insert('f_complementopago_documento',$datacdoc);
        }





        $respuesta=$this->procesarcomplemento($idcomplemento);

        echo json_encode($respuesta);
    }
    function retimbrarcomplemento(){
      $complemento = $this->input->post('complemento');
      $procesarcomplemento=1;

      $datoscomplemento=$this->ModeloCatalogos->getselectwheren('f_complementopago_documento',array('complementoId'=>$complemento));
      foreach ($datoscomplemento->result() as $item) {
        $idfactura=$item->facturasId;

        $datosfactura=$this->ModeloCatalogos->getselectwheren('f_facturas',array('FacturasId'=>$idfactura));
        $datosfactura=$datosfactura->result();
        $datosfactura=$datosfactura[0];

        $datoscop=$this->ModeloCatalogos->consultarcomplementosfactura($idfactura);
        $datoscopnum=$datoscop->num_rows();
        $saldo=0;
        foreach ($datoscop->result() as $item) {
            $saldo=$saldo+$item->ImpPagado;
        }
        $saldofac=$datosfactura->total-$saldo;
        if($saldofac<=0){
          $procesarcomplemento=0;
        }
      }
      //==========================================================
      if($procesarcomplemento==1){
        $respuesta=$this->procesarcomplemento($complemento);
      }else{
        $resultado = array(
                          'resultado'=>'error',
                          'CodigoRespuesta'=>'',
                          'MensajeError'=>'Uno de los documentos ya fue completado el pago',
                          'Mensajeadicional'=>'',
                          'idcomplemento'=>0,
                          'info'=>'',
                          'info2'=>''
                          );
      }
      //$respuesta=$this->procesarcomplemento($complemento);
      echo json_encode($respuesta);
    }
    function procesarcomplemento($idcomplemento){
      $datoscomplemento=$this->ModeloCatalogos->getselectwheren('f_complementopago',array('complementoId'=>$idcomplemento));
      $datoscomplemento=$datoscomplemento->result();
      $datoscomplemento=$datoscomplemento[0];
      if($datoscomplemento->Serie=='D'){
            $urlfilesc ='2';
      }else{
            $urlfilesc ='';
      }
      //========================================================================
      $rutainterna= $_SERVER['DOCUMENT_ROOT'].'/kyocera2';//rutalocal
      //$rutainterna= $_SERVER['DOCUMENT_ROOT'];//rutaserver
      $ruta=$rutainterna.'/kyocera/temporalsat/';
      $rutaf=$rutainterna.'/kyocera/facturas/';
      $rutalog=$rutainterna.'/kyocera/facturaslog/';
        $datosFactura = array(
          'carpeta'=>'kyocera',
          'pwskey'=>'',
          'archivo_key'=>'',
          'archivo_cer'=>'',
          'factura_id'=>0
        );
        $numeroCertificado=file_get_contents(base_url() . $datosFactura['carpeta'] . '/temporalsat'.$urlfilesc.'/Serial.txt');
          //fclose($archivo);
          $numeroCertificado=  str_replace("serial=", "", $numeroCertificado);//quitar letras
        
          $temporal=  str_split($numeroCertificado);//Pasar todos los digitos a un array
        
          $numeroCertificado="";
          $i=0;  
          foreach ($temporal as $value) {
              
              if(($i%2))
              
                  $numeroCertificado .= $value;
          
              $i++;
          }
        
        $numeroCertificado = str_replace('\n','',$numeroCertificado);
        
        $numeroCertificado = trim($numeroCertificado);

        $certificado=file_get_contents(base_url() . $datosFactura['carpeta'] . '/temporalsat'.$urlfilesc.'/certificate.pem');
        
        //fclose($archivo);
        $certificado=  str_replace("-----BEGIN CERTIFICATE-----", "", $certificado);
        
        $certificado=  str_replace("-----END CERTIFICATE-----", "", $certificado);
        
        $certificado=  str_replace("\n", "", $certificado);
        $certificado=  str_replace(" ", "", $certificado);
        $certificado=  str_replace("\n", "", $certificado);
        $certificado= preg_replace("[\n|\r|\n\r]", '', $certificado);
        $certificado= trim($certificado);
      //========================================================================================
        $xml=$this->generaxmlcomplemento('',$certificado,$numeroCertificado,$idcomplemento);
        //log_message('error', 'se genera xml2: '.$xml);
        $str = trim($this->generaCadenaOriginalReal($xml,$datosFactura['carpeta']));    
        
        $cadena=$str;
      //===========================================================================================
        $fp = fopen(base_url() . $datosFactura['carpeta'] . '/temporalsat'.$urlfilesc.'/llaveprivada.pem', "r"); 
        
        $priv_key = fread($fp, 8192); 
        
        fclose($fp); 
        
        $pkeyid = openssl_get_privatekey($priv_key);
        
        //openssl_sign($cadena, $sig, $pkeyid);
        openssl_sign(utf8_encode($cadena), $sig, $pkeyid,'sha256');
        
        openssl_free_key($pkeyid);
        
        $sello = base64_encode($sig);
      //===============accessos al webservice===================================================

        $productivo=0;//0 demo 1 productivo
        if($productivo==0){
          $URL_WS='https://app.fel.mx/WSTimbrado33Test/WSCFDI33.svc?WSDL';
          $URL_WS='https://app.appfacturainteligente.com/WSTimbrado33Test/WSCFDI33.svc?WSDL';//solo para pruebas ya que no tengo los accesos al demo
        }elseif ($productivo==1) {
          //Produccion
          $URL_WS='https://www.fel.mx/WSTimbrado33/WSCFDI33.svc?WSDL';
        }else{
          $URL_WS='';
        }
        if($productivo==0){
          //usuario demo
          $password= "contRa$3na";
          $usuario= "HERD890308D33";
          //solo para pruebas ya que no tengo los accesos al demo
          $password= "contRa$3na";
          $usuario= "COE960119D33";
        }elseif ($productivo==1) {
          //Produccion
          $password= "@fuJXsS@";
          $usuario= "HERD890308UAA";
        }else{
          //usuario demo
          $password= "contRa$3na";
          $usuario= "HERD890308D33";
        }
        $datosconfiguracion=$this->ModeloCatalogos->getselectwheren('f_configuraciones',array('ConfiguracionesId'=>1));
        $datosconfiguracion=$datosconfiguracion->result();
        $datosconfiguracion=$datosconfiguracion[0];
        $rFCEmisor=$datosconfiguracion->Rfc;
        $passwordClavePrivada=$datosconfiguracion->paswordkey;

        $clienteSOAP = new SoapClient($URL_WS);

      //==================================================================
          $datoscomplemento=$this->ModeloCatalogos->getselectwheren('f_complementopago',array('complementoId'=>$idcomplemento));
          $datoscomplemento=$datoscomplemento->result();
          $datoscomplemento=$datoscomplemento[0];
        //==================================================================
          
          $xmlcomplemento=$this->generaxmlcomplemento($sello,$certificado,$numeroCertificado,$idcomplemento);
          //log_message('error', 'se genera xml3: '.$xmlcomplemento);
            file_put_contents('kyocera/facturas/preview_complemento_'.$idcomplemento.'.xml',$xmlcomplemento);
            
            $referencia=$datoscomplemento->E_rfc.'_'.$idcomplemento;
        //======================================================================================
            
          $result = $clienteSOAP->TimbrarCFDI(array(
                    'usuario' => $usuario,
                    'password' => $password,
                    'cadenaXML' => $xmlcomplemento,
                    'referencia' => $referencia));
          file_put_contents($rutalog.'log_complemento'.$this->fechahoyL.'_.txt', json_encode($result));
          if($result->TimbrarCFDIResult->CodigoRespuesta != '0'  ) {                       
            
            $resultado = array(
                          'resultado'=>'error',
                          'CodigoRespuesta'=>$result->TimbrarCFDIResult->CodigoRespuesta,
                          'MensajeError'=>$result->TimbrarCFDIResult->MensajeError,
                          'Mensajeadicional'=>'PORFAVOR RETIMBRAR MAS TARDE',
                          'idcomplemento'=>$idcomplemento,
                          'info'=>$result,
                          'info2'=>$result->TimbrarCFDIResult->MensajeErrorDetallado
                          );
            
            $this->ModeloCatalogos->updateCatalogo('f_complementopago',array('Estado'=>2,'mensajeerror'=>json_encode($result)),array('complementoId'=>$idcomplemento));
                        
            return $resultado;
        
        } else {
          
            try {
           
                $xmlCompleto=utf8_decode($result->TimbrarCFDIResult->XMLResultado);//Contiene XML
            
                //Guardar en archivo     y la ruta en la bd
                $ruta = $datosFactura['carpeta'] . '/facturas/complemento_'.$idcomplemento.'.xml';
            
                file_put_contents($ruta,"\xEF\xBB\xBF".$xmlCompleto);
                $this->ModeloCatalogos->updateCatalogo('f_complementopago',array('rutaXml'=>$ruta),array('complementoId'=>$idcomplemento));            
            
                //$sxe = new SimpleXMLElement($xmlCompleto);
                         
                //$ns = $sxe->getNamespaces(true);
            
                //$sxe->registerXPathNamespace('c', $ns['cfdi']);
            
                //$sxe->registerXPathNamespace('t', $ns['tfd']);
 
                //$uuid = '';
            
                //foreach ($sxe->xpath('//t:TimbreFiscalDigital') as $tfd) {          
                 
                    //Actualizamos el Folio Fiscal UUID
                    //$uuid = $tfd['UUID'];
                    $updatedatossat=array(
                      'uuid'=>$result->TimbrarCFDIResult->Timbre->UUID,
                      'Sello'=>$result->TimbrarCFDIResult->Timbre->SelloCFD,
                      'sellosat'=>$result->TimbrarCFDIResult->Timbre->SelloSAT,
                      'NoCertificado'=>$numeroCertificado,
                      'nocertificadosat'=>$result->TimbrarCFDIResult->Timbre->NumeroCertificadoSAT,
                      'fechatimbre'=>date('Y-m-d G:i:s'),
                      'cadenaoriginal'=>$cadena,
                      'Estado'=>1
                    );
                    $this->ModeloCatalogos->updateCatalogo('f_complementopago',$updatedatossat,array('complementoId'=>$idcomplemento)); 
                //} 
            
                //$this->ModeloCatalogos->updateCatalogo('f_facturas',array('Estado'=>1),array('FacturasId'=>$datosFactura['factura_id']));

                $resultado = array(
                          'resultado'=>'correcto',
                          'Mensajeadicional'=>'Complemento Generado',
                          'idcomplemento'=>$idcomplemento
                          );
            return $resultado;
           }  catch (Exception $e) {
              $resultado = array(
                          'resultado'=>'error',
                          'CodigoRespuesta'=>$result->TimbrarCFDIResult->CodigoRespuesta,
                          'MensajeError'=>$result->TimbrarCFDIResult->MensajeError,
                          'Mensajeadicional'=>'PORFAVOR RETIMBRAR MAS TARDE',
                          'idcomplemento'=>$idcomplemento,
                          'info'=>$result,
                          'info2'=>$result->TimbrarCFDIResult->MensajeErrorDetallado
                          );
             
            $this->ModeloCatalogos->updateCatalogo('f_complementopago',array('Estado'=>2,'mensajeerror'=>json_encode($result)),array('complementoId'=>$idcomplemento));
            
            return $resultado;
                
           }
        
        }
          //echo json_encode($result);
          
          return $resultado;

    }
    function generaxmlcomplemento($sello,$certificado,$numeroCertificado,$idcomplemento){
      //==================================================================
          $datoscomplemento=$this->ModeloCatalogos->getselectwheren('f_complementopago',array('complementoId'=>$idcomplemento));
          $datoscomplemento=$datoscomplemento->result();
          $datoscomplemento=$datoscomplemento[0];
          $caract   = array('&','ñ','Ñ','°','á','é','í','ó','ú','Á','É','Í','Ó','Ú','"',"'");
          $caract2  = array('&amp','&ntilde','&ntilde','','a','e','i','o','u','A','E','I','O','U','&quot;','&apos;');
          $R_nombre=strval(str_replace($caract, $caract2, $datoscomplemento->R_nombre));
          
          $f_relacion=$datoscomplemento->f_relacion;
          $f_r_tipo=$datoscomplemento->f_r_tipo;
          $f_r_uuid=$datoscomplemento->f_r_uuid;
        //==================================================================
            $datoscliente=$this->ModeloCatalogos->getselectwheren('cliente_has_datos_fiscales',array('rfc'=>$datoscomplemento->R_rfc,'idCliente'=>$datoscomplemento->clienteId,'activo'=>1));
            $datoscliente=$datoscliente->result();
            $datoscliente=$datoscliente[0];
            $DomicilioFiscalReceptor=$datoscliente->cp;
            if($datoscomplemento->R_rfc=='XAXX010101000'){
              $DomicilioFiscalReceptor=$datoscomplemento->LugarExpedicion;
            }
        //==================================================================
          $NumOperacion=str_replace(' ', '', $datoscomplemento->NumOperacion);

          $xmlcomplemento='<?xml version="1.0" encoding="utf-8"?>';
          $xmlcomplemento.='
            <cfdi:Comprobante xmlns:pago20="http://www.sat.gob.mx/Pagos20" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:cfdi="http://www.sat.gob.mx/cfd/4" xsi:schemaLocation="http://www.sat.gob.mx/cfd/4 http://www.sat.gob.mx/sitio_internet/cfd/4/cfdv40.xsd http://www.sat.gob.mx/Pagos20 http://www.sat.gob.mx/sitio_internet/cfd/Pagos/Pagos20.xsd" Version="4.0" Exportacion="01"
              Folio="'.$datoscomplemento->Folio.'" Fecha="'.date('Y-m-d',strtotime($datoscomplemento->Fecha)).'T'.date('H:i:s',strtotime($datoscomplemento->Fecha)).'" Sello="'.$sello.'" NoCertificado="'.$numeroCertificado.'" Certificado="'.$certificado.'" SubTotal="'.$datoscomplemento->SubTotal.'" Moneda="'.$datoscomplemento->Moneda.'" Total="'.$datoscomplemento->Total.'" TipoDeComprobante="'.$datoscomplemento->TipoDeComprobante.'" LugarExpedicion="'.$datoscomplemento->LugarExpedicion.'">';
              if($f_relacion==1){
                    $xmlcomplemento .='<cfdi:CfdiRelacionados TipoRelacion="'.$f_r_tipo.'">
                                <cfdi:CfdiRelacionado UUID="'.$f_r_uuid.'"/>
                            </cfdi:CfdiRelacionados>';
                }$xmlcomplemento .='
            <cfdi:Emisor Rfc="'.$datoscomplemento->E_rfc.'" Nombre="'.$datoscomplemento->E_nombre.'" RegimenFiscal="'.$datoscomplemento->E_regimenfiscal.'"/>
            <cfdi:Receptor Rfc="'.$datoscomplemento->R_rfc.'" Nombre="'.$R_nombre.'" DomicilioFiscalReceptor="'.$DomicilioFiscalReceptor.'" RegimenFiscalReceptor="'.$datoscliente->RegimenFiscalReceptor.'" UsoCFDI="CP01"/>
            <cfdi:Conceptos>
              <cfdi:Concepto ClaveUnidad="'.$datoscomplemento->ClaveUnidad.'" ClaveProdServ="'.$datoscomplemento->ClaveProdServ.'" Cantidad="'.$datoscomplemento->Cantidad.'" Descripcion="'.$datoscomplemento->Descripcion.'" ValorUnitario="'.$datoscomplemento->ValorUnitario.'" Importe="'.$datoscomplemento->Importe.'" ObjetoImp="01"/>
            </cfdi:Conceptos>
            <cfdi:Complemento>';
            $dcompdoc=$this->Modelofacturas->documentorelacionado($idcomplemento);
            $TotalTrasladosBaseIVA16=0;
            $TotalTrasladosImpuestoIVA16=0;
            foreach ($dcompdoc->result() as $itemdoc) {
              if($itemdoc->iva>0){                
                $basedr0=$itemdoc->ImpPagado/1.16;
                $ImporteDR0=$basedr0*0.16;
                //$TotalTrasladosBaseIVA16=$TotalTrasladosBaseIVA16+$basedr0;
                $TotalTrasladosBaseIVA16=$TotalTrasladosBaseIVA16+round($basedr0,2);
                //$TotalTrasladosImpuestoIVA16=$TotalTrasladosImpuestoIVA16+$ImporteDR0;
                $TotalTrasladosImpuestoIVA16=$TotalTrasladosImpuestoIVA16+round($ImporteDR0,2);
              }
            }
$xmlcomplemento.='<pago20:Pagos Version="2.0">
              <pago20:Totales MontoTotalPagos="'.$datoscomplemento->Monto.'" TotalTrasladosBaseIVA16="'.number_format(round($TotalTrasladosBaseIVA16,2), 2, ".", "").'" TotalTrasladosImpuestoIVA16="'.number_format(round($TotalTrasladosImpuestoIVA16,2), 2, ".", "").'"/>
              <pago20:Pago FechaPago="'.date('Y-m-d',strtotime($datoscomplemento->FechaPago)).'T'.date('H:i:s',strtotime($datoscomplemento->FechaPago)).'" FormaDePagoP="'.$datoscomplemento->FormaDePagoP.'" MonedaP="MXN" Monto="'.$datoscomplemento->Monto.'" TipoCambioP="1" ';
            $xmlcomplemento.=' NumOperacion="'.$NumOperacion.'" '; 
            //$xmlcomplemento.=' CtaBeneficiario="9680096800" ';
            $xmlcomplemento.=' >';
            //$dcompdoc=$this->ModeloCatalogos->getselectwheren('f_complementopago_documento',array('complementoId'=>$idcomplemento));
            
            $RetencionesP=0;
            $TrasladosP=0;
            $ImporteDRtotal=0;
            $basedrtotal=0;
            $ImporteDRtotal0=0;
            foreach ($dcompdoc->result() as $itemdoc) {
             $xmlcomplemento.='<pago20:DoctoRelacionado IdDocumento="'.$itemdoc->IdDocumento.'" Serie="'.$datoscomplemento->Serie.'" Folio="'.$itemdoc->Folio.'" MonedaDR="'.$datoscomplemento->MonedaDR.'" NumParcialidad="'.$itemdoc->NumParcialidad.'" ImpSaldoAnt="'.$itemdoc->ImpSaldoAnt.'" ImpPagado="'.$itemdoc->ImpPagado.'" ImpSaldoInsoluto="'.$itemdoc->ImpSaldoInsoluto.'" ObjetoImpDR="02" EquivalenciaDR="1">';
              $xmlcomplemento.='<pago20:ImpuestosDR>';
                                if($itemdoc->iva>0){
                                  $TrasladosP++;
                                  
                                  $basedr=$itemdoc->ImpPagado/1.16;
                                  $ImporteDR=$basedr*0.16;
                                  //$basedrtotal=$basedrtotal+$basedr;
                                  $basedrtotal=$basedrtotal+round($basedr,2);
                                  //$ImporteDRtotal0=$ImporteDRtotal0+$ImporteDR;
                                  $ImporteDRtotal0=$ImporteDRtotal0+round($ImporteDR,2);
                                  $xmlcomplemento.='<pago20:TrasladosDR>
                                                        <pago20:TrasladoDR BaseDR="'.number_format(round($basedr,2), 2, ".", "").'" ImporteDR="'.number_format(round($ImporteDR,2), 2, ".", "").'" ImpuestoDR="002" TasaOCuotaDR="0.160000" TipoFactorDR="Tasa"/>
                                                      </pago20:TrasladosDR>';
                                }
                                if($itemdoc->ivaretenido>0){
                                  $RetencionesP++;
                                  $basedr=$itemdoc->ImpPagado/1.16;
                                  $ImporteDR=$basedr*0.16;
                                  $ImporteDRtotal=$ImporteDRtotal+$ImporteDR;
                                  $xmlcomplemento.='<pago20:RetencionesDR>
                                                      <pago20:RetencionDR BaseDR="'.number_format(round($basedr,2), 2, ".", "").'" ImporteDR="'.number_format(round($ImporteDR,2), 2, ".", "").'" ImpuestoDR="002" TasaOCuotaDR="0.160000" TipoFactorDR="Tasa"/>
                                                    </pago20:RetencionesDR>';
                                }
              $xmlcomplemento.='</pago20:ImpuestosDR>';
                                  
             $xmlcomplemento.='</pago20:DoctoRelacionado>';
            }
            $xmlcomplemento.='<pago20:ImpuestosP>';
                              if($RetencionesP>0){
                                $xmlcomplemento.='<pago20:RetencionesP>';
                                    $xmlcomplemento.='<pago20:RetencionP ImporteP="'.number_format(round($ImporteDRtotal,2), 2, ".", "").'" ImpuestoP="002"/>';
                                $xmlcomplemento.='</pago20:RetencionesP>';
                              }
                              if($TrasladosP>0){
                                $xmlcomplemento.='<pago20:TrasladosP>';
                                   $xmlcomplemento.='<pago20:TrasladoP BaseP="'.number_format(round($basedrtotal,2), 2, ".", "").'" ImporteP="'.number_format(round($ImporteDRtotal0,2), 2, ".", "").'" ImpuestoP="002" TasaOCuotaP="0.160000" TipoFactorP="Tasa"/>';
                                $xmlcomplemento.='</pago20:TrasladosP>';
                              }
            $xmlcomplemento.='</pago20:ImpuestosP>';
            $xmlcomplemento.='</pago20:Pago>
            </pago20:Pagos>
            </cfdi:Complemento>
            </cfdi:Comprobante>';
            //log_message('error', 'se genera xml: '.$xmlcomplemento);
      return $xmlcomplemento;
    }
    function liberarfacturas($factura){
      $this->ModeloCatalogos->getdeletewheren('factura_venta',array('facturaId'=>$factura));
      $this->ModeloCatalogos->getdeletewheren('factura_poliza',array('facturaId'=>$factura));
      //===============Liberar periodo=============
        $result=$this->ModeloCatalogos->getselectwheren('factura_prefactura',array('facturaId'=>$factura));
        foreach ($result->result() as $item) {
          $prefId=$item->prefacturaId;
          $this->ModeloCatalogos->updateCatalogo('alta_rentas_prefactura',array('factura_excedente'=>0,'factura_renta'=>0),array('prefId'=>$prefId));
          $this->ModeloCatalogos->updateCatalogo('alta_rentas_prefactura_d',array('statusfacturar'=>0,'statusfacturae'=>0),array('prefId'=>$prefId));
        }
      //================================================================================
    }
    //==========================================================================
    function estatuscancelacionCfdi($uuid){
      //$rutainterna= $_SERVER['DOCUMENT_ROOT'].'/kyocera2';//rutalocal
      //$rutainterna= $_SERVER['DOCUMENT_ROOT'];//rutaserver
      //$ruta=$rutainterna.'/kyocera/temporalsat/';
      //$rutaf=$rutainterna.'/kyocera/facturas/';
      //$rutalog=$rutainterna.'/kyocera/facturaslog/';

      $productivo=1;//0 demo 1 producccion
      
        $res_uuid=$uuid;
        $res_facturaid=0;
      
      

      //var_dump($datosfactura->uuid);
      //===========================================================
        if($productivo==0){
          $URL_WS='https://app.fel.mx/WSTimbrado33Test/WSCFDI33.svc?WSDL';
        }elseif ($productivo==1) {
          //Produccion
          $URL_WS='https://www.fel.mx/WSTimbrado33/WSCFDI33.svc?WSDL';
        }else{
          $URL_WS='';
        }
        if($productivo==0){
          //usuario demo
          $password= "contRa$3na";
          $usuario= "HERD890308D33";
        }elseif ($productivo==1) {
          //Produccion
          $password= "@fuJXsS@";
          $usuario= "HERD890308UAA";
        }else{
          //usuario demo
          $password= "contRa$3na";
          $usuario= "HERD890308D33";
        }
      //=======================================================
        /*
        $parametros = array(
            'usuario' => $usuario,
            'password' => $password,
            'UUID' => $datosfactura->uuid);
        */
        $parametros = array(
            'usuario' => $usuario,
            'password' => $password,
            'uUID' => $res_uuid);
        //echo json_encode($parametros);
        $clienteSOAP = new SoapClient($URL_WS);

        $result = $clienteSOAP->ObtenerAcuseCancelacion($parametros);

        //$fileacuse1='log_cancelcion_estatus_'.$res_facturaid.'_'.$res_uuid.'.xml';
        //$ruta1 = $rutalog .$fileacuse1;
        $resultado=$result;
        
        //file_put_contents($ruta1,json_encode($result));
        /*if($result->CancelarCFDIResult->OperacionExitosa) {
          $resultado = array(
                          'resultado'=>'correcto',
                          'Mensajeadicional'=>'Factura Cancelada',
                          'info'=>$result
                          );
          //Guardar en archivo  y la ruta en la bd del acuse de la cancelacion
                $fileacuse='acuseCancelacion_'.$FacturasId.'_'.$datosfactura->uuid.'.xml';
                $ruta = $rutaf .$fileacuse;
                //file_put_contents($rutalog.'log_'.$this->fechahoyL.'_.txt', json_encode($result));
                file_put_contents($ruta,$result->CancelarCFDIResult->XMLAcuse);

                $datosdecancelacion = array(
                                            'Estado'=>0,
                                            'rutaAcuseCancelacion '=>$fileacuse,
                                            'fechacancelacion'=>$this->fechahoyc
                                          );
                $this->ModeloCatalogos->updateCatalogo('f_facturas',$datosdecancelacion,array('FacturasId'=>$FacturasId));
          
        }else{
          $resultado = array(
                          'resultado'=>'error',
                          'CodigoRespuesta'=>$result->CancelarCFDIResult->MensajeError,
                          'MensajeError'=>$result->CancelarCFDIResult->MensajeErrorDetallado,
                          'Mensajeadicional'=>'PORFAVOR CANCELAR MAS TARDE',
                          'info'=>$result
                          );
          file_put_contents($rutalog.'log_'.$this->fechahoyL.'_.txt', json_encode($result));
        }
        */
        /*
          CodigoRespuesta":"825" //El UUID aún no ha sido cancelado
          {"ObtenerAcuseCancelacionResult":{"CodigoConfirmacion":null,"CodigoRespuesta":"825","CreditosRestantes":0,"MensajeError":"El UUID aun no ha sido cancelado.","MensajeErrorDetallado":null,"OperacionExitosa":false,"PDFResultado":null,"Timbre":null,"XMLResultado":null}}


          CodigoRespuesta":"800" //Operación exitosa
          {"ObtenerAcuseCancelacionResult":{"CodigoConfirmacion":null,"CodigoRespuesta":"800","CreditosRestantes":0,"MensajeError":"","MensajeErrorDetallado":null,"OperacionExitosa":true,"PDFResultado":null,"Timbre":{"Estado":"Cancelado","FechaTimbrado":"2023-03-23T14:26:13","NumeroCertificadoSAT":"","SelloCFD":"mnWkSb9U8xhqvhB6cQOZr5hgJHRZOcZDoNk4vR7XHMJVZX78p\/jZLeYKH6I8WuD\/lrxEtl0dPLaaDzy6PcsFQibzA1mGLWCOi3Z4vBFvtmgcNGrZdWn5hZG\/EMUvE8m667HGaCZSRLxoIjT73ClvQJ640XG+awGTzd8xhcQvMfjNwxQMorA4yVFdivzbYSHChBDMsDmSnGZoCbCaL\/HsTJVM5vHW+sEV3qDZ1IHcp9TqKOU7+YFqxY\/Xrzu2mOHTGPXnEp2tT2a1oa3zf2xExngkVg72O+w\/E6jk7HuyE2iri3b15GwDiZGU\/tbYm\/kyf+wnR3tiw\/VVollajVnCtQ==","SelloSAT":"","UUID":"F615D95D-5F1D-490A-98AB-903397D60B4F"},"XMLResultado":"\r\n\r\n \r\n F615D95D-5F1D-490A-98AB-903397D60B4F<\/UUID>\r\n 201<\/EstatusUUID>\r\n <\/Folios>\r\n \r\n \r\n \r\n \r\n \r\n \r\n \r\n not(ancestor-or-self::*[local-name()='Signature'])<\/XPath>\r\n <\/Transform>\r\n <\/Transforms>\r\n \r\n jxzMoRslxiV3czAkeXpq5qwIC6xb9a4RYzpgPKBkXWJomCcng5lu8xoTUR\/y2RU9LYWmWZv0iKZTO9HPjTlZvA==<\/DigestValue>\r\n <\/Reference>\r\n <\/SignedInfo>\r\n LoEndrN4K7U4FmhFJS\/O\/DpNNDxJWEowmBlg2lrgkAZbwKqYnzgSdEJppkBm5av36q2F\/3c6H\/\/86j3XFy\/vjQ==<\/SignatureValue>\r\n \r\n 00001088888800000038<\/KeyName>\r\n \r\n \r\n qRWzHPVtRHYyDQTnnpPYtOBmb5Raaddb4XZH1DNlIhuhUrh7RKwfvcwh05wEu1lgUnej9BwsLd4u1SeYyawmaF2zIhzssP19yhOQUN9dRBeSg51m8XqCiodxoGilgNm7eDdZQ7j0ZYV0UOkMgGfomw\/3L0z\/O1gTbRDSg8gwM4BMSSu+iSMTcIKqn0oh4C+u77vKRui9NX6WZW2uRnmvEnDybxxtxTQVR3VlM4tdXeYTfkr5WUUrtaFyFW\/B7zs9Mb+FO5Cr\/TbWmu+xJP6LlN4UbZdPVe7+7rrv\/hwzeYe2LeFT0Jtmvuy1NfR\/uUZa35+UMou+RbsDOTm4vfJyuw==<\/Modulus>\r\n AQAB<\/Exponent>\r\n <\/RSAKeyValue>\r\n <\/KeyValue>\r\n <\/KeyInfo>\r\n <\/Signature>\r\n<\/Acuse>"}}

        */
          $MensajeError='';
          if($resultado->ObtenerAcuseCancelacionResult->CodigoRespuesta=='825'){
            $MensajeError=$resultado->ObtenerAcuseCancelacionResult->MensajeError;
          }else{

          }
        
        //echo '['.json_encode($resultado).']';
          return $MensajeError;
    }
    function estatuscancelacionCfdi_all(){
        $datos = $this->input->post('datos');
        //log_message('error', $datos);
        $DATA = json_decode($datos);
        $facturas=array();
        for ($i=0;$i<count($DATA);$i++) {
            $complemento=0;
            $uuid=$DATA[$i]->uuid;
            $resultado=$this->estatuscancelacionCfdi($uuid);

            $facturas[]=array(
                    'uuid'=>$uuid,
                    'resultado'=>$resultado
                );
        }
        echo json_encode($facturas);
    }
    //==========================================================================
    public function get_serie_color()
    {
      $serie=$this->input->post('serie');
      $this->db->select('*');
      $this->db->from('series_productos');
      $this->db->like(array('serie'=>$serie));
      $query=$this->db->get(); 
      

      echo json_encode($query->result());
    }
}
    