<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class PolizasCreadas extends CI_Controller 
{
    public function __construct() 
    {
        parent::__construct();
        $this->load->library('menus');
        $this->load->model('login_model');
        $this->load->model('Polizas_model');
        $this->load->model('ModeloGeneral');
        $this->load->model('ModeloCatalogos');
        $this->load->model('Ventas_model');
        $this->load->model('Clientes_model');
        $this->load->model('ConfiguracionDocumentos_Model');
        $this->load->model('Rentas_model');
        $this->load->model('Login_model');
        $this->submenu=29;
        $this->load->helper('url');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d H:i:s');
        if($this->session->userdata('logeado')==true){
            $this->perfilid =$this->session->userdata('perfilid');
            $this->idpersonal = $this->session->userdata('idpersonal');
            /*
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,29);// 20 es el id del submenu
            if ($permiso==0) {
                redirect('Login');
            }*/
        }else{
            redirect('login');
        }
    }

    // Listado de Empleados
    function index(){
        $data['MenusubId']=$this->submenu;
        $where = array('personalId' =>$this->idpersonal);  
        $result=$this->ModeloCatalogos->getselectwheren('personal',$where);
        foreach ($result->result() as $item) {
           $data['nombre']=$item->nombre;
           $data['apellido_paterno']=$item->apellido_paterno;
           $data['apellido_materno']=$item->apellido_materno;
        }
        $where_m = array('activo' =>1);  
        $data['get_f_formapago']=$this->ModeloCatalogos->getselectwheren('f_formapago',$where_m);
        $menus = $this->menus->getMenus($this->session->userdata('perfilid'));
        $data['menus'] = $menus['menus'];
        $data['submenusArray'] = $menus['submenusArray'];
        $data['personalrows']=$this->ModeloCatalogos->getselectwheren('personal',array('estatus'=>1));
        $data['idpersonal']=$this->idpersonal;
        $data['fechainicial_reg'] = date("Y-m-d",strtotime($this->fechahoy."- 1 year")); 
        $data['lispagosvp']=$this->ModeloGeneral->verificarpagospolizas();

        $pfectivo=0;
        $r_p_e=$this->ModeloCatalogos->getselectwheren('personal',array('p_efectivo'=>1,'personalId'=>$this->idpersonal));
        if($r_p_e->num_rows()>0){
            $pfectivo=1;
        }
        $data['pfectivo']=$pfectivo;
        
        $this->load->view('header');
        $this->load->view('main',$data);
        $this->load->view('polizas/listadoCreadas');
        $this->load->view('footerl');
        $this->load->view('polizas/listadoCreadas_js');
        
    }

    function getListadocontratosPolizasIncompletas(){
        $datos = $this->Polizas_model->getListadocontratosPolizasIncompletas();
        $json_data = array("data" => $datos);
        echo json_encode($json_data);
    }
    public function getListadocontratosPolizasIncompletasasi() {
        $params = $this->input->post();
        //log_message('error','personal'.$params['personal']);
        $productos = $this->Polizas_model->getListadocontratosPolizasIncompletasasi($params);
       $totalRecords=$this->Polizas_model->getListadocontratosPolizasIncompletasasit($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totalRecords),  
            "recordsFiltered" => intval($totalRecords),
            "data"            => $productos->result(),
            "query"           =>$this->db->last_query()   
        );
        
        echo json_encode($json_data);
    }
    public function view($id=0,$tipoview=0){
        $data['ventaId'] = $id;
        $data['proinven']=$id;
        $data['dia_reg']=date('d');
        $data['mes_reg']=date('m');
        $data['ano_reg']=date('y');
        //$data['proinven']=''; // es el numero de folio de la factura aparecera cuando se guarden los campos restantes o se finalise la edicion
        $data['cfd']='';
        $data['vence']='';// sera la fecha de vencimiento sera remplazada una ves guardad
        $data['vence_block']='';//el campo de vencimiento sera bloqueado una ves guardado
        $data['formapago']=0;
        $data['formapago_block']='';
        $data['cfdi']=0;
        $data['cfdi_block']='';
        $data['formacobro']='';
        $data['formacobro_block']='';
        $data['estado']=0;
        $data['cargo']='';
        $data['cargo_block']='';
        $data['metodopagoId']=0;
        $data['metodopagoId_block']='';

        $data['telId']=0;
        $data['telId_block']='';        
        $data['contactoId']=0;
        $data['contactoId_block']='';            
        $data['cargo']=0;
        $data['cargo_block']='';
        $data['block_button']=0;
        $data['observaciones']='';
        $data['observaciones_block']='';
        $data['rfc_id']=0;
        $data['rfc_id_button']='';
        $data['estadovals']='';
        $data['cfdiv']='';
        $data['comentario']='';
        $data['comentario_block']='';
        $data['prefacturaId']=0;

        $tipov=1;
        //============================================
            $resultadovfac = $this->ModeloCatalogos->poliza_facturas($id);
            $row_vfac=0;
            foreach ($resultadovfac->result() as $itemvf) {
                $row_vfac=1;
            }
            $data['row_vfac']=$row_vfac;
        //============================================
        //=================================================================
            $ventacombinada = $this->ModeloCatalogos->getselectwheren('ventacombinada',array('poliza'=>$id));
            foreach ($ventacombinada->result() as $itemvc) {
                $itemvc->combinadaId;
                redirect('/Prefactura/viewc/'.$itemvc->combinadaId.'?vcpoliza='.$id);
            }
        //=======================
        $where_ventas=array('id'=>$id);
        $resultadov = $this->ModeloCatalogos->db6_getselectwheren('polizasCreadas',$where_ventas);
        $id_personal=0;
        foreach ($resultadov->result() as $item) {
            $id_personal=$item->id_personal;
            $idCliente=$item->idCliente;
            $tipov=$item->tipov;
            $data['siniva']=$item->siniva;
            $data['fechaentrega']=$item->fechaentrega;
            $data['viewcont']=$item->viewcont;
        }
        $data['idCliente']=$idCliente;
        $data['rfc_datos']=$this->Clientes_model->getListadoDatosFiscales($idCliente); 
        //==========================================================================
        $where_personal=array('personalId'=>$id_personal);
        $resultadop = $this->ModeloCatalogos->db6_getselectwheren('personal',$where_personal);
        $personal_nombre='';
        $personal_apellidop='';
        $personal_apellidom='';
        foreach ($resultadop->result() as $item) {
            $personal_nombre= $item->nombre;
            $personal_apellidop= $item->apellido_paterno;
            $personal_apellidom= $item->apellido_materno;
        }
        $data['ini_personal']=substr($personal_nombre, 0, 1).substr($personal_apellidop, 0, 1).substr($personal_apellidom, 0, 1);
        //============================================
            $resultadocli = $this->ModeloCatalogos->db6_getselectwheren('clientes',array('id'=>$idCliente));
            foreach ($resultadocli->result() as $item) {
                $data['empresa']=$item->empresa;
                $data['empresars']=$item->empresa;
                $data['razon_social']=$item->razon_social;
                $data['rfc']=$item->rfc;
                $data['calle']=$item->calle;
                $data['num_ext']=$item->num_ext;
                $data['colonia']=$item->colonia;
                $data['estado']=$item->estado;
                $data['cp']=$item->cp;
                $data['municipio']=$item->municipio;
                $data['email']=$item->email;
                $data['cargo']=$item->puesto_contacto;
                $data['credito']=$item->credito;

                if($item->horario_disponible!=''){
                    $horario_disponible='Horario Disponible: '.$item->horario_disponible.'';
                }else{
                    $horario_disponible='';
                }
                if($item->equipo_acceso!=''){
                    $equipo_acceso=' Equipo de acceso: '.$item->equipo_acceso.'';
                }else{
                    $equipo_acceso='';
                }
                if($item->documentacion_acceso!=''){
                    $documentacion_acceso='  Documentacion de acceso: '.$item->documentacion_acceso.'';
                }else{
                    $documentacion_acceso='';
                }
                
                $maxcredito = strtotime ( '+'.$item->credito.' day', strtotime ($this->fechahoy));
                $maxcredito = date ( 'Y-m-d' , $maxcredito );

                $data['maxcredito']= 'max="'.$maxcredito.'"'; 


                $data['observaciones']=$horario_disponible.' '.$equipo_acceso.' '.$documentacion_acceso;
            }
            $resultadoclidf = $this->ModeloCatalogos->db6_getselectwheren('cliente_has_datos_fiscales',array('idCliente'=>$idCliente));
            foreach ($resultadoclidf->result() as $item) {
                $data['razon_social']=$item->razon_social;
                $data['rfc']=$item->rfc;
                $data['calle']=$item->calle;
                $data['num_ext']=$item->num_ext;
                $data['colonia']=$item->colonia;
                $data['cp']=$item->cp;
            }


            $data['resultadoclitel'] = $this->ModeloCatalogos->db6_getselectwheren('cliente_has_telefono',array('idCliente'=>$idCliente));
            $data['cliente_datoscontacto'] = $this->ModeloCatalogos->db6_getselectwheren('cliente_datoscontacto',array('clienteId'=>$idCliente));
            $data['resultadoclipcontacto'] = $this->ModeloCatalogos->db6_getselectwheren('cliente_has_persona_contacto',array('idCliente'=>$idCliente));

            $data['polizaventadetalles'] = $this->ModeloCatalogos->polizaventadetalles3($id);
            $data['polizaventadetalles_dev'] = $this->ModeloCatalogos->polizaventadetalles3_dev($id);
        //===========================================
            $data['metodopagorow']=$this->ModeloCatalogos->view_session_f_metodopago();
            $data['formapagorow']=$this->ModeloCatalogos->db6_getselectwheren('f_formapago',array('activo'=>1));
            $data['cfdirow']=$this->ModeloCatalogos->db6_getselectwheren('f_uso_cfdi',array('activo'=>1));
            $data['estadorow']=$this->ModeloCatalogos->db6_getselectwheren('estado',array('activo'=>1));
            $data['configuracionCotizacion'] = $this->ConfiguracionDocumentos_Model->getConfiguracion($tipov);
            $confif=$this->ModeloCatalogos->db10_getselectwheren('f_configuraciones',array('ConfiguracionesId'=>$tipov));
            $data['confif']=$confif->row();
        //============================================================================================
            $i_pol_result= $this->ModeloGeneral->info_ser_poliza($id);
            if($i_pol_result->num_rows()>0){
                $data['iser']=1;
            }else{
                $data['iser']=0;
            }
        //============================================================================================
        $resultprefactura = $this->ModeloCatalogos->db6_getselectwheren('prefactura',array('ventaId'=>$id,'tipo'=>1));
        foreach ($resultprefactura->result() as $item) {
            //$data['proinven']=$item->prefacturaId;
            $data['prefacturaId']=$item->prefacturaId;
            $data['vence']=$item->vencimiento;
            $data['vence_block']='disabled';
            $data['metodopagoId']=$item->metodopagoId;
            $data['metodopagoId_block']='disabled';
            $data['formapago']=$item->formapagoId;
            $data['formapago_block']='disabled';
            $data['cfdi']=$item->usocfdiId;
            $data['cfdi_block']='disabled';
            $data['formacobro']=$item->formadecobro;
            $data['formacobro_block']='disabled';

        
            $data['telId']=$item->telId;
            $data['telId_block']='disabled';
        
            $data['contactoId']=$item->contactoId;
            $data['contactoId_block']='disabled';
            
            $data['cargo']=$item->cargo;
            $data['cargo_block']='readonly';
            $data['block_button']=1;
            $fechareg=$item->reg;
            $data['dia_reg']=date('d',strtotime($fechareg));
            $data['mes_reg']=date('m',strtotime($fechareg));
            $data['ano_reg']=date('y',strtotime($fechareg));
            $data['observaciones']=$item->observaciones;
            $data['observaciones_block']='readonly'; 
            $data['rfc_id']=$item->rfc_id;
            $data['rfc_id_button']='disabled';


            $data['colonia']=$item->d_colonia;
            $data['calle']=$item->d_calle;
            $data['num_ext']=$item->d_numero;
            $data['municipio']=$item->d_ciudad;
            $data['estadovals']=$item->d_estado;
            $data['cfdiv']=$item->cfdi;
            $data['comentario_block']='readonly';

            $data['maxcredito']='';
        }
        //============================================================================================
        if($tipoview==0){
            $this->load->view('polizas/prefactura',$data);
        }else{
            $this->load->view('Reportes/prefactura_p',$data);
        }
        
    }
    public function save(){
        $data = $this->input->post();
        
        $prefacturaId=$data["prefacturaId"];
        unset($data["prefacturaId"]);

        $direcciones=$data["direcciones"];
        unset($data["direcciones"]);
        $comentarios=$data["comentarios"];
        unset($data["comentarios"]);
        $data['observaciones']=str_replace(array('"','  '), array(' ',' '), $data['observaciones']);
        $id=$data['ventaId'];
        $data['tipo']=1;
        if($prefacturaId==0){
            $this->ModeloCatalogos->Insert('prefactura',$data);
        }
        $dataup['prefactura']=1;
        $this->ModeloCatalogos->updateCatalogo('polizasCreadas',$dataup,array('id' => $id));

        $DATAdir = json_decode($direcciones);
        for ($j=0;$j<count($DATAdir);$j++){
            if($DATAdir[$j]->dir!=''){
                $this->ModeloCatalogos->updateCatalogo('polizasCreadas_has_detallesPoliza',array('direccionservicio'=>$DATAdir[$j]->dir,'iddir'=>$DATAdir[$j]->id),array('id'=>$DATAdir[$j]->id));
            }
        }
        $DATAcom = json_decode($comentarios);
        for ($l=0;$l<count($DATAcom);$l++){
            if($DATAcom[$l]->comen!=''){
                $this->ModeloCatalogos->updateCatalogo('polizasCreadas_has_detallesPoliza',array('comentario'=>$DATAcom[$l]->comen),array('id'=>$DATAcom[$l]->id));
            }
        }
    }
    function finalizar_factura(){
        $id = $this->input->post('id');
        //$resultfacpol=$this->ModeloCatalogos->getselectwheren('factura_poliza',array('polizaId'=>$id));
        $resultfacpol=$this->Polizas_model->verificarfactura($id);

        if ($resultfacpol->num_rows()>0) {
           $eliminacion=0;
        }else{
            $this->ModeloCatalogos->updateCatalogo('polizasCreadas',array('activo' => 0),array('id' => $id));
            $eliminacion=1;
            //$this->Modelobitacoras->Insert(array('contenido'=>'Se Elimino poliza creada','nombretabla'=>'polizascreadas','idtable'=>$id,'tipo'=>'Update','personalId'=>$this->idpersonal)); 
        }      
        echo $eliminacion;
    }
    function pintardireccionesclientes(){
        $idCliente = $this->input->post('clienteid');
        $resultadosdir=$this->Clientes_model->getClientedirecPorId($idCliente);
        $direcciones='<table id="table_pinta_dir" class="table display"><thead><tr><th>Direcciones</th></tr></thead><tbody>';
        foreach ($resultadosdir as $item) {
            $direcciones.='<tr><td class="adddirecciones collection-item dismissable">'.$item->direccion.'</td></tr>';
        }
        $direcciones.='<tbody></table>';
        echo $direcciones;
    }
    ////////////////7
    public function verificar_fecha_vencimiento(){
        $id = $this->input->post('id');
        $ven='';
        $aux='';
        $arraywhere = array('ventaId'=>$id);
        $result=$this->ModeloCatalogos->db3_getselectwheren_2('vencimiento','prefactura',$arraywhere);
        foreach ($result as $item) {
           $ven=$item->vencimiento;
        }
        if($ven=='0000-00-00'){
           $aux='';
        }else{
           $aux=$ven;
        }
        echo $aux;
    }
    public function update_cfdi_registro(){
        $id_venta = $this->input->post('id_venta');
        $cfdi = $this->input->post('cfdi');
        $arraydata = array('ventaId' =>$id_venta);
        $data = array('cfdi'=>$cfdi);
        $this->ModeloCatalogos->updateCatalogo('prefactura',$data,$arraydata);
    }
    function montopoliza(){//polizas creadas
        $id=$this->input->post('id');
        $totalgeneral=0;

        $rowventa=$this->ModeloCatalogos->db10_getselectwheren('polizasCreadas',array('id'=>$id)); 
        $siniva=0;
        foreach ($rowventa->result() as $itemv) {
            $siniva=$itemv->siniva;
        }

        $resultfactura=$this->ModeloCatalogos->ontenerdatosfacturapoliza($id);
        if($resultfactura->num_rows()==0){
            $totalc=0;
            $polizaventadetalles = $this->ModeloCatalogos->polizaventadetalles3($id);
            foreach ($polizaventadetalles->result() as $item) { 
                if($item->precio_local>0) {
                    $totalc=$item->precio_local;
                }
                if($item->precio_semi>0) {
                    $totalc=$item->precio_semi;
                }
                if($item->precio_foraneo>0) {
                    $totalc=$item->precio_foraneo;
                }
                if($item->precio_especial>0) {
                    $totalc=$item->precio_especial;
                }
                $totalgeneral=$totalgeneral+($totalc);
                //$totalgeneral=$totalgeneral+($totalc*item->cantidad);
            }
            if($siniva==1){

            }else{
                $totalgeneral=$totalgeneral+($totalgeneral*0.16);
            }
            
        }else{
            foreach ($resultfactura->result() as $item) {
                $totalgeneral=$totalgeneral+$item->total;
            }
        }

        echo round($totalgeneral, 2); 
    }
    function montopoliza2($id){//polizas creadas
        $totalgeneral=0;
        $polizaventadetalles = $this->ModeloCatalogos->polizaventadetalles3($id);
        foreach ($polizaventadetalles->result() as $item) { 
            if($item->precio_local !==NULL) {
                $totalc=$item->precio_local;
            }
            if($item->precio_semi !==NULL) {
                $totalc=$item->precio_semi;
            }
            if($item->precio_foraneo !==NULL) {
                $totalc=$item->precio_foraneo;
            }
            $totalgeneral=$totalgeneral+($totalc);
            //$totalgeneral=$totalgeneral+($totalc*item->cantidad);
        }
        $totalgeneral=$totalgeneral+($totalgeneral*0.16);

        return round($totalgeneral, 2); 
    }
    public function guardar_pago_tipo(){
        $data = $this->input->post();
        $idcompra = $data['idcompra'];
        unset($data['idcompra']);
        $data['idpersonal']=$this->idpersonal;
        //poliza
        $data['idpoliza']=$idcompra;

        if($this->idpersonal==18){// si diana es la que deposito se confirma en automatico
            $data['confirmado']=1;
        }
        $this->ModeloCatalogos->Insert('pagos_poliza',$data);

        //=============================================
        $rowpagos=$this->ModeloCatalogos->getselectwheren('pagos_poliza',array('idpoliza'=>$idcompra));
        $totalpagos=0;
        foreach ($rowpagos->result() as $item) {
            $totalpagos=$totalpagos+$item->pago;
        }
        $totalpre=$this->montopoliza2($idcompra);
        if ($totalpagos>=$totalpre) {
            $this->ModeloCatalogos->updateCatalogo('polizasCreadas',array('statuspago'=>1),array('id'=>$idcompra));
        }

    }
    public function table_get_tipo_compra(){
        $id = $this->input->post('id');
        $tip = $this->input->post('tip');
        $resultfactura=$this->ModeloCatalogos->ontenerdatosfacturapoliza($id);
        $html='<table class="responsive-table display striped" cellspacing="0" id="tablepagosg">
                    <thead>
                      <tr>
                        <th>Fecha</th>
                        <th>Método de pago</th>
                        <th>Monto</th>
                        <th>Comentario</th>
                        <th></th>
                        <!--<th>Comprobante</th>-->
                        <th></th>
                      </tr>
                    </thead>
                    <tbody>
                        <tr style="display: none;">
                            <td><input type="hidden" id="idventapoliza" value="'.$id.'" data-tipovp="2" ></td>
                             <td></td>
                             <td></td>
                             <td></td> 
                             <!--<td></td>-->
                             <td></td> 
                        </tr>';
        if($resultfactura->num_rows()==0){
            $result = $this->ModeloCatalogos->pagos_poliza($id);
            foreach ($result as $item) {
                if($item->confirmado==1){
                        $itemconfirmado='Confirmado <i class="material-icons" style="color:green;">done_all</i>';
                   }else{
                        $itemconfirmado='Sin Confirmar <a class="new badge yellow" style="padding: 9px 2px 0px;" onclick="confirmarpagov('.$item->idpago.',3)"><i class="material-icons">warning</i> </a>';
                   }
            $html.='<tr>
                     <td>
                        <input type="hidden" id="idventapoliza" value="'.$id.'" data-tipovp="2" >
                        '.$item->fecha.'</td>
                     <td>'.$item->formapago_text.'</td>
                     <td class="montoagregado">'.$item->pago.'</td>
                     <td>'.$item->observacion.'</td> 
                     <td>'.$item->nombre.' '.$item->apellido_paterno.' </td> 
                     <!--<td>'.$item->comprobante.'</td>-->
                     <td>
                        <span 
                            class="new badge red" 
                            style="cursor: pointer;" onclick="deletepagop('.$item->idpago.')">
                            <i class="material-icons">delete</i>
                        </span>
                        '.$itemconfirmado.'
                     </td> 
                    </tr>';
            }
        }else{
            foreach($resultfactura->result() as $item){
                    $FacturasIdselected=$item->FacturasId;
                    $resultvcp = $this->ModeloCatalogos->obtencionventapolizafactura($item->FacturasId);

                    foreach ($resultvcp->result() as $itemvcp) {
                        if($itemvcp->combinada==0){
                                $html.='<tr style="display: none;">
                                            <td><input type="hidden" id="idventapoliza" value="'.$itemvcp->ventaId.'" data-tipovp="0" ></td>
                                            <td></td><td></td><td></td><!--<td></td>--><td></td></tr>';
                            //=======================================
                                 $result = $this->ModeloCatalogos->pagos_ventas($itemvcp->ventaId); 
                                   foreach ($result as $item) { 
                                    if($item->confirmado==1){
                                        $itemconfirmado='Confirmado <i class="material-icons" style="color:green;">done_all</i>';
                                   }else{
                                        $itemconfirmado='Sin Confirmar <a class="new badge yellow" style="padding: 9px 2px 0px;" onclick="confirmarpagov('.$item->idpago.',0)"><i class="material-icons">warning</i> </a>';
                                   } 
                                    $html.='<tr> 
                                             <td>
                                                <input type="hidden" id="idventapoliza" value="'.$itemvcp->ventaId.'" data-tipovp="0" >
                                                '.$item->fecha.'</td> 
                                             <td>'.$item->formapago_text.'</td> 
                                             <td class="montoagregado">'.$item->pago.'</td> 
                                             <td>'.$item->observacion.'</td> 
                                             <td>'.$item->nombre.' '.$item->apellido_paterno.' </td> 
                                             <!--<td>'.$item->comprobante.'</td>--> 
                                             <td>
                                                <span 
                                                    class="new badge red" 
                                                    style="cursor: pointer;" 
                                                    onclick="deletepagov('.$item->idpago.',0)">
                                                    <i class="material-icons">delete</i>
                                                </span>
                                                '.$itemconfirmado.'
                                            </td>
                                            </tr>'; 
                                    } 
                            //=======================================
                        }elseif ($itemvcp->combinada==1) {
                                $html.='<tr style="display: none;">
                                            <td><input type="hidden" id="idventapoliza" value="'.$itemvcp->ventaId.'" data-tipovp="1" ></td>
                                            <td></td><td></td><td></td><!--<td></td>--><td></td></tr>';
                            //========================================
                                $result = $this->ModeloCatalogos->pagos_combinada($itemvcp->ventaId); 
                                foreach ($result as $item) { 
                                    if($item->confirmado==1){
                                        $itemconfirmado='Confirmado <i class="material-icons" style="color:green;">done_all</i>';
                                   }else{
                                        $itemconfirmado='Sin Confirmar <a class="new badge yellow" style="padding: 9px 2px 0px;" onclick="confirmarpagov('.$item->idpago.',1)"><i class="material-icons">warning</i> </a>';
                                   }
                                $html.='<tr> 
                                         <td>
                                            <input type="hidden" id="idventapoliza" value="'.$itemvcp->ventaId.'" data-tipovp="1">
                                            '.$item->fecha.'</td> 
                                         <td>'.$item->formapago_text.'</td> 
                                         <td class="montoagregado">'.$item->pago.'</td> 
                                         <td>'.$item->observacion.'</td>
                                         <td>'.$item->nombre.' '.$item->apellido_paterno.' </td>  
                                         <!--<td>'.$item->comprobante.'</td>-->

                                         <td>
                                            <span 
                                                class="new badge red" 
                                                style="cursor: pointer;" 
                                                onclick="deletepagov('.$item->idpago.',1)">
                                                <i class="material-icons">delete</i>
                                            </span>
                                            '.$itemconfirmado.'
                                        </td> 
                                        </tr>'; 
                                } 
                            //========================================
                        }elseif ($itemvcp->combinada==2) {
                                $html.='<tr style="display: none;">
                                            <td><input type="hidden" id="idventapoliza" value="'.$itemvcp->ventaId.'" data-tipovp="2" ></td>
                                            <td></td><td></td><td></td><!--<td></td>--><td></td></tr>';
                            //========================================
                                $result = $this->ModeloCatalogos->pagos_poliza($itemvcp->ventaId);
                                foreach ($result as $item) {
                                    if($item->confirmado==1){
                                        $itemconfirmado='Confirmado <i class="material-icons" style="color:green;">done_all</i>';
                                   }else{
                                        $itemconfirmado='Sin Confirmar <a class="new badge yellow" style="padding: 9px 2px 0px;" onclick="confirmarpagov('.$item->idpago.',3)"><i class="material-icons">warning</i> </a>';
                                   }
                                $html.='<tr>
                                         <td>
                                            <input type="hidden" id="idventapoliza" value="'.$itemvcp->ventaId.'" data-tipovp="2">
                                            '.$item->fecha.'</td>
                                         <td>'.$item->formapago_text.'</td>
                                         <td class="montoagregado">'.$item->pago.'</td>
                                         <td>'.$item->observacion.'</td> 
                                         <td>'.$item->nombre.' '.$item->apellido_paterno.' </td> 
                                         <!--<td>'.$item->comprobante.'</td>-->
                                         <td>
                                            <span 
                                                class="new badge red" 
                                                style="cursor: pointer;" onclick="deletepagop('.$item->idpago.')">
                                                <i class="material-icons">delete</i>
                                            </span>
                                            '.$itemconfirmado.'
                                         </td> 
                                        </tr>';
                                }
                            //========================================
                        }
                    }
                    $resultvcp_cp = $this->ModeloCatalogos->obteciondecomplememtofactura($FacturasIdselected);
                    foreach ($resultvcp_cp->result() as $itemcp) {
                        $html.='<tr> 
                                     <td>
                                        <input type="hidden" id="idventapoliza" value="'.$id.'" data-tipovp="'.$tip.'" class="pagodecomplemento">
                                        '.$itemcp->Fecha.'
                                    </td> 
                                     <td></td> 
                                     <td class="montoagregado">'.$itemcp->ImpPagado.'</td> 
                                     <td>Complemento</td> 
                                     <!--<td></td>--> 
                                     <td>
                                        
                                    </td>
                                    </tr>'; 
                    }
                }
        }
        $html.='</tbody>
                  </table>';
        echo $html;
    }
    function vericar_pass(){
      $pass = $this->input->post('pass'); 
      $pago = $this->input->post('pago'); 
      $verifica = $this->Rentas_model->verificar_pass2($pass);
      $aux = 0;
      if($pass=='x1a2b3'){
        $verifica=1;
      }
      if($verifica == 1){
         $aux=1;
         $this->ModeloCatalogos->getdeletewheren2('pagos_poliza',$pago);
        $this->Modelobitacoras->Insert(array('contenido'=>'Se Elimino pago de Poliza','nombretabla'=>'pagos_poliza','idtable'=>$pago,'tipo'=>'Delete','personalId'=>$this->idpersonal));
      }
      echo $aux;
    }
    function obtencionfacturas(){
        $poliza = $this->input->post('poliza');
        $result = $this->ModeloCatalogos->obtencionfacturaspoliza($poliza);
        $folio='';
        foreach ($result->result() as $item) {
            $folio=$item->Folio;
        }
        echo $folio;
    }
    function servicios($id,$id_row_eq=0){
        $idpoliza=$id;
        //=========================================================
            $data['idPoliza']=$id;
            $data['ventaId'] = $id;
            $data['proinven']=$id;
            $data['dia_reg']=date('d');
            $data['mes_reg']=date('m');
            $data['ano_reg']=date('y');
            //$data['proinven']=''; // es el numero de folio de la factura aparecera cuando se guarden los campos restantes o se finalise la edicion
            $data['cfd']='';
            $data['vence']='';// sera la fecha de vencimiento sera remplazada una ves guardad
            $data['vence_block']='';//el campo de vencimiento sera bloqueado una ves guardado
            $data['formapago']=0;
            $data['formapago_block']='';
            $data['cfdi']=0;
            $data['cfdi_block']='';
            $data['formacobro']='';
            $data['formacobro_block']='';
            $data['estado']=0;
            $data['cargo']='';
            $data['cargo_block']='';
            $data['metodopagoId']=0;
            $data['metodopagoId_block']='';

            $data['telId']=0;
            $data['telId_block']='';        
            $data['contactoId']=0;
            $data['contactoId_block']='';            
            $data['cargo']=0;
            $data['cargo_block']='';
            $data['block_button']=0;
            $data['observaciones']='';
            $data['observaciones_block']='';
            $data['rfc_id']=0;
            $data['rfc_id_button']='';
            $data['estadovals']='';
            $data['cfdiv']='';
            $data['comentario']='';
            $data['comentario_block']='';
            $data['prefacturaId']=0;

            $tipov=1;
            if($id_row_eq>0){
                $idroweq=$id_row_eq;
                $res_pold = $this->ModeloCatalogos->db6_getselectwheren('polizasCreadas_has_detallesPoliza ',array('id'=>$idroweq));
                foreach ($res_pold->result() as $item_rpold) {
                    $idroweq_serie=$item_rpold->serie;
                    $idroweq_eq=$item_rpold->idEquipo;
                }
            }else{
                $idroweq=0;
                $idroweq_eq=0;
                $idroweq_serie='';
            }
            //============================================
                $resultadovfac = $this->ModeloCatalogos->poliza_facturas($id);
                $row_vfac=0;
                foreach ($resultadovfac->result() as $itemvf) {
                    $row_vfac=1;
                }
                $data['row_vfac']=$row_vfac;
            //============================================
            //=================================================================
                $ventacombinada = $this->ModeloCatalogos->getselectwheren('ventacombinada',array('poliza'=>$id));
                foreach ($ventacombinada->result() as $itemvc) {
                    $itemvc->combinadaId;
                    //redirect('/Prefactura/viewc/'.$itemvc->combinadaId.'?vcpoliza='.$id);
                }
            //=======================
            $where_ventas=array('id'=>$id);
            $resultadov = $this->ModeloCatalogos->db6_getselectwheren('polizasCreadas',$where_ventas);
            $id_personal=0;
            foreach ($resultadov->result() as $item) {
                $id_personal=$item->id_personal;
                $idCliente=$item->idCliente;
                $tipov=$item->tipov;
                $data['siniva']=$item->siniva;
                $data['fechaentrega']=$item->fechaentrega;
                $data['serviciocount']=$item->serviciocount;
                $data['file']=$item->file;
                $data['ser_rest']=$item->ser_rest;
            }
            $data['idCliente']=$idCliente;
            $data['rfc_datos']=$this->Clientes_model->getListadoDatosFiscales($idCliente); 
            //==========================================================================
            $where_personal=array('personalId'=>$id_personal);
            $resultadop = $this->ModeloCatalogos->db6_getselectwheren('personal',$where_personal);
            $personal_nombre='';
            $personal_apellidop='';
            $personal_apellidom='';
            foreach ($resultadop->result() as $item) {
                $personal_nombre= $item->nombre;
                $personal_apellidop= $item->apellido_paterno;
                $personal_apellidom= $item->apellido_materno;
            }
            $data['ini_personal']=substr($personal_nombre, 0, 1).substr($personal_apellidop, 0, 1).substr($personal_apellidom, 0, 1);
            //============================================
                $resultadocli = $this->ModeloCatalogos->db6_getselectwheren('clientes',array('id'=>$idCliente));
                foreach ($resultadocli->result() as $item) {
                    $data['empresa']=$item->empresa;
                    $data['empresars']=$item->empresa;
                    $data['razon_social']=$item->razon_social;
                    $data['rfc']=$item->rfc;
                    $data['calle']=$item->calle;
                    $data['num_ext']=$item->num_ext;
                    $data['colonia']=$item->colonia;
                    $data['estado']=$item->estado;
                    $data['cp']=$item->cp;
                    $data['municipio']=$item->municipio;
                    $data['email']=$item->email;
                    $data['cargo']=$item->puesto_contacto;
                    $data['credito']=$item->credito;

                    if($item->horario_disponible!=''){
                        $horario_disponible='Horario Disponible: '.$item->horario_disponible.'';
                    }else{
                        $horario_disponible='';
                    }
                    if($item->equipo_acceso!=''){
                        $equipo_acceso=' Equipo de acceso: '.$item->equipo_acceso.'';
                    }else{
                        $equipo_acceso='';
                    }
                    if($item->documentacion_acceso!=''){
                        $documentacion_acceso='  Documentacion de acceso: '.$item->documentacion_acceso.'';
                    }else{
                        $documentacion_acceso='';
                    }
                    
                    $maxcredito = strtotime ( '+'.$item->credito.' day', strtotime ($this->fechahoy));
                    $maxcredito = date ( 'Y-m-d' , $maxcredito );

                    $data['maxcredito']= 'max="'.$maxcredito.'"'; 


                    $data['observaciones']=$horario_disponible.' '.$equipo_acceso.' '.$documentacion_acceso;
                }
                $resultadoclidf = $this->ModeloCatalogos->db6_getselectwheren('cliente_has_datos_fiscales',array('idCliente'=>$idCliente));
                foreach ($resultadoclidf->result() as $item) {
                    $data['razon_social']=$item->razon_social;
                    $data['rfc']=$item->rfc;
                    $data['calle']=$item->calle;
                    $data['num_ext']=$item->num_ext;
                    $data['colonia']=$item->colonia;
                    $data['cp']=$item->cp;
                }


                $data['resultadoclitel'] = $this->ModeloCatalogos->db6_getselectwheren('cliente_has_telefono',array('idCliente'=>$idCliente));
                $data['cliente_datoscontacto'] = $this->ModeloCatalogos->db6_getselectwheren('cliente_datoscontacto',array('clienteId'=>$idCliente));
                $data['resultadoclipcontacto'] = $this->ModeloCatalogos->db6_getselectwheren('cliente_has_persona_contacto',array('idCliente'=>$idCliente));
                if($id_row_eq>0){
                    $data['polizaventadetalles'] = $this->ModeloCatalogos->polizaventadetalles0_0($id_row_eq);
                }else{
                    $data['polizaventadetalles'] = $this->ModeloCatalogos->polizaventadetalles3($id);
                }
                
            //===========================================
                $data['metodopagorow']=$this->ModeloCatalogos->view_session_f_metodopago();
                $data['formapagorow']=$this->ModeloCatalogos->db6_getselectwheren('f_formapago',array('activo'=>1));
                $data['cfdirow']=$this->ModeloCatalogos->db6_getselectwheren('f_uso_cfdi',array('activo'=>1));
                $data['estadorow']=$this->ModeloCatalogos->db6_getselectwheren('estado',array('activo'=>1));
                $data['configuracionCotizacion'] = $this->ConfiguracionDocumentos_Model->getConfiguracion($tipov);
            //============================================================================================
            $resultprefactura = $this->ModeloCatalogos->db6_getselectwheren('prefactura',array('ventaId'=>$id,'tipo'=>1));
            foreach ($resultprefactura->result() as $item) {
                //$data['proinven']=$item->prefacturaId;
                $data['prefacturaId']=$item->prefacturaId;
                $data['vence']=$item->vencimiento;
                $data['vence_block']='disabled';
                $data['metodopagoId']=$item->metodopagoId;
                $data['metodopagoId_block']='disabled';
                $data['formapago']=$item->formapagoId;
                $data['formapago_block']='disabled';
                $data['cfdi']=$item->usocfdiId;
                $data['cfdi_block']='disabled';
                $data['formacobro']=$item->formadecobro;
                $data['formacobro_block']='disabled';

            
                $data['telId']=$item->telId;
                $data['telId_block']='disabled';
            
                $data['contactoId']=$item->contactoId;
                $data['contactoId_block']='disabled';
                
                $data['cargo']=$item->cargo;
                $data['cargo_block']='readonly';
                $data['block_button']=1;
                $fechareg=$item->reg;
                $data['fechareg']=$fechareg;
                $data['dia_reg']=date('d',strtotime($fechareg));
                $data['mes_reg']=date('m',strtotime($fechareg));
                $data['ano_reg']=date('y',strtotime($fechareg));
                $data['observaciones']=$item->observaciones;
                $data['observaciones_block']='readonly'; 
                $data['rfc_id']=$item->rfc_id;
                $data['rfc_id_button']='disabled';


                $data['colonia']=$item->d_colonia;
                $data['calle']=$item->d_calle;
                $data['num_ext']=$item->d_numero;
                $data['municipio']=$item->d_ciudad;
                $data['estadovals']=$item->d_estado;
                $data['cfdiv']=$item->cfdi;
                $data['comentario_block']='readonly';

                $data['maxcredito']='';
            }
            //============================================================================================
        //=========================================================
        //=====================SERVICIOS====================================
            $columns = array( 
                0=>'ape.asignacionIde',
                1=>'per.personalId',
                2=>"CONCAT(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno) as tecnico",
                3=>'cli.id',
                4=>'cli.empresa',
                5=>'ape.fecha',
                6=>'ape.hora',
                7=>'ap.zonaId',
                8=>'ru.nombre',
                9=>'equ.modelo',
                10=>'ape.serie',
                11=>'ape.status',
                12=>'ap.tiposervicio',
                13=>'ape.horafin',
                14=>'ap.asignacionId',
                15=>'sereve.nombre as servicio',
                16=>'ape.t_fecha',
                17=>'ape.horaserinicio',
                18=>'ape.horaserfin',
                19=>'ape.horaserinicio',
                20=>'ape.horaserfin',
                21=>'ape.cot_referencia',
                22=>'ape.horaserinicioext',
                23=>'ape.horaserfinext',
                24=>'ape.editado',
                25=>'ape.hora_comida',
                26=>'ape.horafin_comida',
                27=>'ape.t_horainicio',
                28=>'ape.t_horafin',
                29=>'ape.activo',
                30=>'ape.infodelete',
                31=>'pocde.direccionservicio',
                32=>'ap.polizaId',
                33=>'ape.prioridad',
                34=>'ape.notificarerror_estatus',
                35=>'ape.notificarerror',
                37=>'ape.nr',
                38=>'ape.nr_coment',
                39=>'ape.nr_coment_tipo',
                40=>"CONCAT(per2.nombre,' ',per2.apellido_paterno,' ',per2.apellido_materno) as tecnico2",
                41=>'per2.personalId as personalId2',
                42=>"CONCAT(per3.nombre,' ',per3.apellido_paterno,' ',per3.apellido_materno) as tecnico3",
                43=>'per3.personalId as personalId3',
                44=>"CONCAT(per4.nombre,' ',per4.apellido_paterno,' ',per4.apellido_materno) as tecnico4",
                45=>'per4.personalId as personalId4',
                46=>'ape.comentario'
            );
            $select="";
            foreach ($columns as $c) {
                $select.="$c, ";
            }
            $this->db->select($select);
            $this->db->from('asignacion_ser_poliza_a_e ape');
            $this->db->join('asignacion_ser_poliza_a ap','ap.asignacionId=ape.asignacionId');
            $this->db->join('personal per','per.personalId=ape.tecnico','left');
            $this->db->join('personal per2','per2.personalId=ape.tecnico2','left');
            $this->db->join('personal per3','per3.personalId=ape.tecnico3','left');
            $this->db->join('personal per4','per4.personalId=ape.tecnico4','left');
            $this->db->join('polizasCreadas poc','poc.id=ap.polizaId');
            $this->db->join('clientes cli','cli.id=poc.idCliente');
            $this->db->join('rutas ru','ru.id=ap.zonaId');
            $this->db->join('polizasCreadas_has_detallesPoliza pocde','pocde.id=ape.idequipo');
            $this->db->join('equipos equ','equ.id=pocde.idEquipo');
            $this->db->join('servicio_evento sereve','sereve.id=ap.tservicio','left');
            $this->db->where(array('ap.polizaId'=>$idpoliza));

            if($id_row_eq>0){
                $this->db->where("(ape.idvinculo='$id_row_eq' or ape.serie='$idroweq_serie' )");
            }
            
            $this->db->where(array('ap.activo'=>1));
            
            $query=$this->db->get();
            $data['query_result']=$query;
        //=========================================================
            $data['row_eq'] = $this->ModeloCatalogos->getselectwheren('equipos',array('estatus'=>1));
            $data['rows_pol'] = $this->ModeloCatalogos->obtencion_polisas_cliente_viewcont($idCliente);
            $data['rows_pol_vinc'] = $this->ModeloCatalogos->obtencion_polisas_vinculadas($idpoliza);
            if($id_row_eq>0){
                $data['rows_ser_eve'] = $this->Polizas_model->servicioseventocliente_dll($idCliente,$idpoliza,$id_row_eq,$idroweq_serie);
                $data['rows_ser_pol'] = $this->Polizas_model->serviciospolizacliente_dll($idCliente,$idpoliza,$idpoliza,$idroweq_eq,$idroweq_serie);
            }else{
                $data['rows_ser_eve'] = $this->Polizas_model->servicioseventocliente($idCliente,$idpoliza);
                $data['rows_ser_pol'] = $this->Polizas_model->serviciospolizacliente($idCliente,$idpoliza,$idpoliza);
            }
            
            $data['r_direcciones'] = $this->ModeloCatalogos->db6_getselectwheren('clientes_direccion',array('idcliente'=>$idCliente,'status'=>1));

        $this->load->view('header');
        $this->load->view('main',$data);
        $this->load->view('polizas/servicios');
        $this->load->view('footerl');
        $this->load->view('polizas/serviciosjs');
        $this->load->view('info_m_servisios');
    }
    function dejarderealizarservicios(){
        $poliza = $this->input->post('id');
        $this->ModeloCatalogos->updateCatalogo('polizasCreadas',array(' serviciocount'=>0),array('id'=>$poliza));
    }
    function filepoliza(){
        $idPoliza=$_POST['idPoliza'];

        

        $configUpload['upload_path'] = FCPATH.'uploads/poliza/';
        $configUpload['allowed_types'] = '*';
        $configUpload['max_size'] = '9000';
        $configUpload['file_name'] = date('Y-m-d_H_i_s');
        $this->load->library('upload', $configUpload);
        $this->upload->do_upload('filepoliza');  
        $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
        $file_name = $upload_data['file_name']; //uploded file name
        $extension=$upload_data['file_ext'];    // uploded file extension
        

        $this->ModeloCatalogos->updateCatalogo('polizasCreadas',array('file'=>$file_name),array('id'=>$idPoliza));
        
        

        $output = [];
        echo json_encode($output);
    }
    function agregarequiposcont(){
        $params = $this->input->post();
        $idPoliza=$params['idPoliza'];
        $conceptos=$params['conceptos'];
        
        $pass=$params['pass'];

        $permiso=0;
        $permisos = $this->Login_model->permisoadminarray(array('1','9','17','18'));//1admin 9 conta 17 julio 18 diana

        foreach ($permisos as $item) {
            $permisotemp=0;

            $usuario=$item->Usuario;

            $verificar = password_verify($pass,$item->contrasena);

            if ($verificar==false) {
                $verificar = password_verify($pass,$item->clavestemp);
                if ($verificar) {
                    $permisotemp=1;
                }
            }
            if ($verificar) {
                $permiso=1;
                if($permisotemp==1){
                    $this->ModeloCatalogos->updateCatalogo('usuarios',array('clavestemp'=>null),array('UsuarioID'=>$item->UsuarioID));
                }

            }
        }
        if($permiso==1){
            //==========================================================================
                $DATAc = json_decode($conceptos);
                
                $result=$this->ModeloCatalogos->getselectwheren('polizasCreadas_has_detallesPoliza',array('idPoliza'=>$idPoliza));
                $result=$result->result();
                $dataco=$result[0];
                unset($dataco->id);
                $datainsertrarray=array();

                for ($i=0;$i<count($DATAc);$i++) {
                    $dataco->cantidad=1;
                    $dataco->serie = $DATAc[$i]->serie;
                    $dataco->idEquipo = $DATAc[$i]->eq;
                    $dataco->modelo = $DATAc[$i]->eq;
                    
                    $dataco->direccionservicio= $DATAc[$i]->dir;
                    $dataco->comentario= $DATAc[$i]->dir;

                    $dataco->precio_local = 0;
                    $dataco->precio_semi = 0;
                    $dataco->precio_foraneo = 0;
                    $dataco->precio_especial = 0;

                    $datainsertrarray[]=$dataco;

                    $this->ModeloCatalogos->Insert('bitacora',array('contenido'=>'se agrego equipo a la poliza  '.$idPoliza.' con serie '.$DATAc[$i]->serie, 'nombretabla'=>'polizasCreadas_has_detallesPoliza','idtable'=>0,'tipo'=>'Insert','personalId'=>$this->idpersonal));
                }
                if(count($datainsertrarray)>0){
                    $this->ModeloCatalogos->insert_batch('polizasCreadas_has_detallesPoliza',$datainsertrarray);
                }
            //==================================================================================
        }
        echo $permiso;
    }
    function add_pol(){
        $params = $this->input->post();
        $idPoliza = $params['idPoliza'];

        $asig = $params['asig'];
        $asigtipo = $params['asigtipo'];

        $ser = $params['ser'];
        $sert = $params['sert'];
        
        //$this->ModeloCatalogos->Insert('polizascreadas_ext_ser',array('poliza_principal'=>$idPoliza,'tipo'=>$sert, 'idpoliza'=>$id,'idser'=>$ser));
        $datos=array(
                    'poliza_principal'=>$idPoliza,
                    'id_ser'=>$ser,
                    'id_tipo'=>$sert,
                    'id_ser_garantia'=>$asig,
                    'id_tipo_garantia'=>$asigtipo,
                    'personal'=>$this->idpersonal 
                    );
        $this->ModeloCatalogos->Insert('polizascreadas_ext_servisios',$datos);
    }
    function notificardeservicio(){
        $params = $this->input->post();
        $id = $params['id'];
        $date = $params['date'];
        $info = $params['info'];
        $tipopc = $params['tipopc'];
        $this->ModeloCatalogos->updateCatalogo('polizasCreadas',array('alert'=>1,'alert_date'=>$date),array('id' => $id));
        $idpolizaser=$this->ModeloCatalogos->Insert('polizascreadas_sercontr',array('id'=>$id,'alert_date'=>$date,'personal'=>$this->idpersonal,'tipo_pre_corr'=>$tipopc));

        $DATAc = json_decode($info);
        for ($i=0;$i<count($DATAc);$i++) {
            $idrow = $DATAc[$i]->idrow;
            $status = $DATAc[$i]->status;
            $comen = $DATAc[$i]->comen;

            $this->ModeloCatalogos->updateCatalogo('polizasCreadas_has_detallesPoliza',array('viewser'=>$status,'comeninfo'=>$comen),array('id' => $idrow));

            if($status==1){
                $this->ModeloCatalogos->Insert('polizascreadas_sercontr_has_detallespoliza',array('idpolizaser'=>$idpolizaser,'idequipopol'=>$idrow,'comeninfo'=>$comen));
                $this->ModeloCatalogos->Insert('bitacora',array('contenido'=>'Se realizo una solicitud de servicio de la poliza '.$id.' para el equipo idrow: '.$idrow.' para la fecha: '.$date,'nombretabla'=>'polizasCreadas_has_detallesPoliza','idtable'=>$idrow,'tipo'=>'solicitud servicio poliza equipo','personalId'=>$this->idpersonal));
            }
        }
    }
    function activarsuspender(){
        $params = $this->input->post();
        $id = $params['id'];
        $tipo = $params['tipo'];
        $this->ModeloCatalogos->updateCatalogo('polizasCreadas_has_detallesPoliza',array('suspender'=>$tipo),array('id' => $id));
    }
    function viewagenda(){
        $params = $this->input->post();
        $idPoliza = $params['idPoliza'];
        $result=$this->ModeloCatalogos->getselectwheren('polizascreadas_ext_fechas',array('idpoliza'=>$idPoliza,'activo'=>1));
        $html='';
        foreach ($result->result() as $item) {
            $fecha="'$item->fecha'";
            $html.='<tr>
                        <td>'.$item->fecha.'</td>
                        <td>
                            <a class="waves-effect blue btn-bmz" onclick="notificardeservicio('.$item->idpoliza.','.$fecha.')"><i class="fas fa-calendar-check"></i></i></a>
                            <a class="waves-effect red btn-bmz" onclick="deleteagenda('.$item->id.')"><i class="fas fa-trash"></i></a>
                        </td></tr>';
        }
        echo $html;
    }
    function agregar_agenda(){
        $params = $this->input->post();
        $idPoliza = $params['idPoliza'];
        $age = $params['age'];
        $this->ModeloCatalogos->Insert('polizascreadas_ext_fechas',array('idpoliza'=>$idPoliza, 'fecha'=>$age));
    }
    function deleteagenda(){
        $params = $this->input->post();
        $id = $params['id'];
        $this->ModeloCatalogos->updateCatalogo('polizascreadas_ext_fechas',array('activo'=>0),array('id' => $id));
    }
    function addserex(){
        $params = $this->input->post();
        $idcliente =$params['idcliente'];
        $idPoliza = $params['idPoliza'];
        //tipo 1 contrato
        //tipo 2 poliza
        //tipo 3 evento
        $datos =array();
        $result_eve = $this->Polizas_model->servicioseventocliente($idcliente,0);
        foreach ($result_eve->result() as $item) {
            $hora = $item->hora.' a '.$item->horafin;
                if($item->hora_comida!=null && $item->horafin_comida!=null){
                    if($item->hora_comida!='' && $item->horafin_comida!=''){
                        $hora.='<br>Comida: '.$item->hora_comida.' a '.$item->horafin_comida;
                    }
                }
            $datos[]=array(
                        'id'=>$item->asignacionId,
                        'fecha'=>$item->fecha,
                        'hora'=>$hora,
                        'servicio'=>$item->servicio,
                        'series'=>$item->equiposserie,
                        'tipo'=>3,
                        'seriesgroup'=>$item->equiposseries_group
                        );
        }
        $result_pol = $this->Polizas_model->serviciospolizacliente($idcliente,$idPoliza,0);
        foreach ($result_pol->result() as $item) {
            $hora = $item->hora.' a '.$item->horafin;
                if($item->hora_comida!=null && $item->horafin_comida!=null){
                    if($item->hora_comida!='' && $item->horafin_comida!=''){
                        $hora.='<br>Comida: '.$item->hora_comida.' a '.$item->horafin_comida;
                    }
                }
            $datos[]=array(
                        'id'=>$item->asignacionId,
                        'fecha'=>$item->fecha,
                        'hora'=>$hora,
                        'servicio'=>$item->serviciopol,
                        'series'=>$item->equiposserie,
                        'tipo'=>2,
                        'seriesgroup'=>$item->equiposseries_group
                        );
        }

        echo json_encode($datos);

    }
    function addserex_add(){
        $params = $this->input->post();
        $idasig = $params['idasig'];
        $tipo = $params['tipo'];
        $idPoliza = $params['idPoliza'];

        $data['idpoliza'] = $idPoliza;
        $data['idservicio']=$idasig;
        $data['tipo_ser'] =$tipo;

        $this->ModeloCatalogos->Insert('polizascreadas_ext_ser_add',$data);
    }
    function editarrestante(){
        $params = $this->input->post();
        $poliza = $params['poliza'];
        $realizados = $params['realizados'];
        $monto = $params['monto'];
        $ser_rest=$monto+$realizados;
        $this->ModeloCatalogos->updateCatalogo('polizasCreadas',array('ser_rest'=>$ser_rest,'ser_rest_pers'=>$this->idpersonal),array('id'=>$poliza));
    }
    function deletedoc(){
        $params = $this->input->post();
        $id = $params['id'];
        //$this->ModeloCatalogos->getdeletewheren('polizascreadas_ext_ser_add',array('id'=>$id));
        $this->ModeloCatalogos->updateCatalogo('polizascreadas_ext_servisios',array('activo'=>0),array('id'=>$id));
        $this->ModeloCatalogos->Insert('bitacora',array('contenido'=>'se realizo una eliminacion de servicio vinculado', 'nombretabla'=>'polizascreadas_ext_servisios','idtable'=>$id,'tipo'=>'delete','personalId'=>$this->idpersonal));
    }
    function deletegarantia(){
        $params = $this->input->post();
        $id = $params['id'];
        $this->ModeloCatalogos->updateCatalogo('polizascreadas_ext_ser',array('activo'=>0),array('id'=>$id));
    }
    function deletedll(){
        $params = $this->input->post();
        $id = $params['id'];
        //$this->ModeloCatalogos->getdeletewheren('polizasCreadas_has_detallesPoliza',array('id'=>$id));
        $dat_dll_pol= $this->ModeloCatalogos->getselectwheren('polizasCreadas_has_detallesPoliza',array('id'=>$id));
        foreach ($dat_dll_pol->result() as $itemserpol) {
            $idPoliza=$itemserpol->idPoliza;
            //$this->ModeloCatalogos->Insert('polizasCreadas_has_detallesPoliza_dev',$itemserpol);

            $this->ModeloCatalogos->getdeletewheren('polizasCreadas_has_detallesPoliza',array('id'=>$id));
            $this->ModeloCatalogos->Insert('bitacora',array('contenido'=>'se realizo una eliminacion de un equipo vinculado a la poliza '.$idPoliza, 'nombretabla'=>'polizasCreadas_has_detallesPoliza','idtable'=>$id,'tipo'=>'delete','personalId'=>$this->idpersonal));
        }
    }
    function servicios_cliente(){
        $params = $this->input->post();
        $idcliente = $params['idcliente'];
        $idPoliza = $params['idPoliza'];

        $result = $this->Polizas_model->info_servicios_cliente($idcliente,$idPoliza);
        $html='<table class="table" id="table_pol"><thead>
          <tr><th>#</th><th>Cliente</th><th>Pre</th><th>Fecha de creación</th><th></th></tr>
        </thead><tbody>';
        foreach ($result->result() as $item) {
            $html.='<tr class="series_selecte '.$item->equiposseries_group.'">';
                $html.='<td>'.$item->asignacionId.'</td>';
                $html.='<td>'.$item->empresa.'</td>';
                $html.='<td>';
                        $html.='<a class="btn-bmz blue " onclick="obternerdatosservicio('.$item->asignacionId.','.$item->tiposer.')"><i class="fas fa-info-circle"></i></a>';
                $html.='</td>';
                $html.='<td>'.$item->fecha.'</td>';
                $html.='<td>';
                $html.='<a class="green btn-bmz" onclick="add_pol('.$item->asignacionId.','.$item->tiposer.')">Seleccionar</a>';
                $html.='</td>';
            $html.='</tr>';
        }
        $html.='</tbody></table>';

        echo $html;
    }
    function detalle_pol_op(){
        $params = $this->input->post();
        $idPoliza=$params['id'];
        $polizaventadetalles = $this->ModeloCatalogos->polizaventadetalles3($idPoliza);
        $html='';
        foreach ($polizaventadetalles->result() as $item) {
            $html.='<div class="div_btn_pol_eq">';
            $html.='<a href="'.base_url().'PolizasCreadas/servicios/'.$idPoliza.'/'.$item->id.'" class="btn-floating tooltipped" data-position="top" data-delay="50" data-tooltip="Servicio" target="_black"><i class="fas fa-file-invoice"></i></a>';
            $html.='<p>'.$item->modeloe.'<br>'.$item->serie.'</p>';
            $html.='</div>';
        }
        echo $html;
    }
    function editar_pold(){
        $params = $this->input->post();
        $id = $params['idd'];
        $eq = $params['eq'];
        $serie = $params['ser'];

        $this->ModeloCatalogos->getdeletewheren('polizasCreadas_has_detallesPoliza_series',array('iddetalle'=>$id));
        $this->ModeloCatalogos->Insert('polizasCreadas_has_detallesPoliza_series',array('serie'=>$serie,'iddetalle'=>$id));
        if($serie!=''){
            $this->ModeloCatalogos->updateCatalogo('polizasCreadas_has_detallesPoliza',array('serie'=>$serie,'idEquipo'=>$eq),array('id'=>$id));

            $get_result=$this->ModeloCatalogos->getselectwheren('polizasCreadas_has_detallesPoliza',array('id'=>$id));
            foreach ($get_result->result() as $itemp) {
                if($itemp->idvinculo>0){
                    $this->ModeloCatalogos->updateCatalogo('cotizaciones_has_detallesPoliza',array('serie'=>$serie,'idEquipo'=>$eq),array('id'=>$itemp->idvinculo));
                }
                // code...
            }
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_poliza_a_e',array('serie'=>$serie),array('idequipo'=>$id));
        }
    }
}   
