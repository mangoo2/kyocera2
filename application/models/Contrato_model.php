<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class Contrato_Model extends CI_Model 
{
    public function __construct() 
    {
        parent::__construct();
        $this->DB5 = $this->load->database('other5_db', TRUE);
    }

    public function getListadocontratos(){
        $sql = "SELECT c.*, p.nombre AS vendedor, cl.empresa, r.id AS idrenta, cf.rfc AS rfc_df 
                FROM contrato AS c
                INNER JOIN personal AS p ON p.personalId = c.personalId
                INNER JOIN rentas AS r ON r.id = c.idRenta
                INNER JOIN clientes AS cl ON cl.id = r.idCliente
                LEFT JOIN cliente_has_datos_fiscales AS cf ON cf.id = c.rfc
                WHERE c.estatus!=0";
        $query = $this->db->query($sql);
        return $query->result();
    }
    //========================
        function getListadocontratosasi($params){
            $tipo=$params['tipo'];
            $perfilid = $params['perfilid'];
            $idpersonal = $params['idpersonal'];
            $columns = array( 
                0=>'c.idcontrato',
                1=>'cl.empresa',
                2=>'c.folio',
                3=>'c.tipopersona',
                4=>'c.vigencia',
                5=>'p.nombre AS vendedor',
                6=>'c.fechasolicitud',
                7=>'c.fechainicio',
                8=>'c.arrendataria',
                9=>'c.rfc',
                10=>'c.file',
                11=>'c.estatus',
                12=>'c.idrenta',
                13=>'r.id AS idrenta',
                14=>'cf.rfc AS rfc_df',
                15=>'c.ejecutivo',
                16=>'pj.nombre as ej',
                17=>'pj.apellido_paterno as ejap',
                18=>'pj.apellido_materno as ejam',
                19=>'r.estatus as estatusr',
                20=>'r.prefactura',
                21=>'r.id as idrenta'
            );
            $columnss = array( 
                0=>'c.idcontrato',
                1=>'cl.empresa',
                2=>'c.folio',
                3=>'c.tipopersona',
                4=>'c.vigencia',
                5=>'p.nombre',
                6=>'c.fechasolicitud',
                7=>'c.fechainicio',
                8=>'c.arrendataria',
                9=>'c.rfc',
                10=>'c.file',
                11=>'c.estatus',
                12=>'c.idrenta',
                13=>'r.id',
                14=>'cf.rfc'
            );
            $select="";
            foreach ($columns as $c) {
                $select.="$c, ";
            }
            $this->DB5->select($select);
            $this->DB5->from('contrato c');

            $this->DB5->join('personal p', 'p.personalId = c.personalId');
            $this->DB5->join('rentas r', 'r.id = c.idRenta');
            $this->DB5->join('clientes cl', 'cl.id = r.idCliente');
            $this->DB5->join('cliente_has_datos_fiscales cf', 'cf.id = c.rfc','LEFT');
            $this->DB5->join('personal pj', 'c.ejecutivo = pj.personalId','LEFT');
            if($tipo==1){
                $this->DB5->where('c.estatus !=0');
            }else{
                $this->DB5->where('c.estatus =0');
            }
            if ($perfilid==1 or $perfilid==6) {
               
            }else{
                $this->DB5->where('c.ejecutivo='.$idpersonal);
            }
            
            

            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->DB5->group_start();
                foreach($columnss as $c){
                    $this->DB5->or_like($c,$search);
                }
                $this->DB5->group_end();  
            }            
            $this->DB5->order_by($columnss[$params['order'][0]['column']], $params['order'][0]['dir']);
            $this->DB5->limit($params['length'],$params['start']);
            //echo $this->db->get_compiled_select();
            $query=$this->DB5->get();
            // print_r($query); die;
            return $query;
        }
        function total_Listadocontratosasi($params){
            $tipo=$params['tipo'];
            $columns = array( 
                0=>'c.idcontrato',
                1=>'c.folio',
                2=>'c.tipopersona',
                3=>'c.vigencia',
                4=>'c.fechasolicitud',
                5=>'c.fechainicio',
                6=>'c.arrendataria',
                7=>'c.rfc',
                8=>'c.file',
                9=>'c.estatus',
                10=>'c.idrenta',
                11=>'p.nombre AS vendedor',
                12=>'cl.empresa',
                13=>'r.id AS idrenta',
                14=>'cf.rfc AS rfc_df'
            );
            $columnss = array( 
                0=>'c.idcontrato',
                1=>'c.tipocontrato',
                2=>'c.tipopersona',
                3=>'c.vigencia',
                4=>'c.fechasolicitud',
                5=>'c.fechainicio',
                6=>'c.arrendataria',
                7=>'c.rfc',
                8=>'c.file',
                9=>'c.estatus',
                10=>'c.idrenta',
                11=>'p.nombre',
                12=>'cl.empresa',
                13=>'r.id',
                14=>'cf.rfc'
            );
            
            $this->DB5->select('COUNT(*) as total');
            $this->DB5->from('contrato c');
            $this->DB5->join('personal p', 'p.personalId = c.personalId');
            $this->DB5->join('rentas r', 'r.id = c.idRenta');
            $this->DB5->join('clientes cl', 'cl.id = r.idCliente');
            $this->DB5->join('cliente_has_datos_fiscales cf', 'cf.id = c.rfc','LEFT');
            
            if($tipo==1){
                $this->DB5->where('c.estatus !=0');
            }else{
                $this->DB5->where('c.estatus =0');
            }

            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->DB5->group_start();
                foreach($columnss as $c){
                    $this->DB5->or_like($c,$search);
                }
                $this->DB5->group_end();  
            }            
            
            $query=$this->DB5->get();
            return $query->row()->total;
        }
    //========================================================================
    public function actualizaContrato($data,$id){
        $this->db->set($data);
        $this->db->where('idcontrato', $id);
        return $this->db->update('contrato');
    }
    
    public function actualizaRentaEquipo($data,$id){
        $this->DB5->set($data);
        $this->DB5->where('id', $id);
        return $this->DB5->update('rentas_has_detallesEquipos');
    }

    public function actualizaRentaEquipoT($data,$id,$tabla){
        $this->DB5->set($data);
        $this->DB5->where('idrentas', $id);
        return $this->DB5->update($tabla);
    }

    public function eliminar_renta_equipo_detalle_consu($id,$tabla) {
        $this->DB5->where('idrentas', $id);
        return $this->DB5->delete($tabla);
    }

    public function verificarFechaFin($id){
        date_default_timezone_set('America/Mexico_City');
        $date = date('Y-m-d');

        $sql = "SELECT id from rentas_has_detallesEquipos
        where idRenta=$id
        and fecha_fin_susp < '$date' and estatus=1";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function cambiarFechaFin($id){
        //puede que ya no se ocupe
        $sql = "UPDATE rentas_has_detallesEquipos set fecha_fin_susp = '', fecha_suspendido='', estatus_suspendido='0'
        where id=$id";
        $query = $this->db->query($sql);
        return $query;
    }
    public function RentaEquipodetalles($id){
        $sql = "SELECT rhde.id, 
                       rhde.idEquipo, 
                       rhde.modelo, 
                       asre.serieId, 
                       sep.serie, 
                       rhde.cantidad, 
                       equi.categoriaId, 
                       cteq.tipo, 
                       equi.pag_monocromo_incluidos, 
                       equi.pag_color_incluidos, 
                       equi.excedente, 
                       equi.excedentecolor,
                       equi.costo_renta,
                       asre.estatus_suspendido
                       FROM rentas_has_detallesEquipos as rhde 
                       inner join asignacion_series_r_equipos as asre on asre.id_equipo=rhde.id 
                       inner JOIN series_productos as sep on sep.serieId=asre.serieId 
                       inner join equipos as equi on equi.id=rhde.idEquipo 
                       inner join categoria_equipos as cteq on cteq.id=equi.categoriaId 
                       WHERE rhde.id=$id and asre.activo=1
                       group by rhde.idEquipo,asre.serieId
                       ";
        $query = $this->DB5->query($sql);
        return $query;
    }
    /*public function verificarFechaFin($id){
        date_default_timezone_set('America/Mexico_City');
        $date = date('Y-m-d');

        /*$this->db->update('rentas_has_detallesEquipos');
        $this->db->set('estatus_suspendido = IF fecha_fin_susp > $date THEN
            SET fecha_fin_susp = "";
        END IF'); 
        $this->db->where('id', $id);
        return $id;*/
        /*$sql = "SELECT * from rentas_has_detallesEquipos
        if fecha_fin_susp>='2019-12-18' then
            UPDATE rentas_has_detallesEquipos set fecha_fin_susp="" WHERE idRenta=17;
        end if";
        $query = $this->db->query($sql);
        return $query;
    }*/


}