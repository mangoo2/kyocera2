<link href="<?php echo base_url(); ?>app-assets/css/layouts/page-center.css" type="text/css" rel="stylesheet">
  <body style="background-color: #DB1E34;">
<!-- Start Page Loading -->
    <div id="loader-wrapper" >
      <div id="loader"></div>
      <div class="loader-section section-left"></div>
      <div class="loader-section section-right"></div>
    </div>
    <!-- End Page Loading -->
    <div id="login-page" class="row " >
      <div class="col s3 z-depth-4 card-panel " style="width: 100%;"  >
        <input id="base_url" type="hidden" name="base_url" val="<?php echo base_url(); ?>">
        <form class="login-form" id="login-form" >
          <div class="row">
            <div class="input-field col s12 center">
              <img src="<?php echo base_url(); ?>app-assets/images/1024_kyocera_logo.svg" alt="" class="responsive-img">
              <p class="center login-form-text">Inicio de sesión</p>
            </div>
          </div>
          <div class="row margin">
            <div class="input-field col s12">
              <i class="material-icons prefix pt-5">person_outline</i>
              <input id="username" type="text" name="username" placeholder="Nombre de Usuario">
              <!--<label for="username" class="center-align">Nombre de Usuario</label>-->
            </div>
          </div>
          <div class="row margin">
            <div class="input-field col s12">
              <i class="material-icons prefix pt-5">lock_outline</i>
              <input id="password" type="password" name="password" placeholder="Contraseña">
              <!--<label for="password">Contraseña</label>-->
            </div>
          </div>
          <div class="row">
            <div class="input-field col s12">
              <button type="submit" class="btn waves-effect waves-light col s12" id="login-submit">Entrar</button>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s6 m6 l6">
              <p class="margin medium-small"></p>
            </div>
            <div class="input-field col s6 m6 l6">
              <p class="margin right-align medium-small"></p>
            </div>
          </div>
        </form>

      </div>
    </div>