<style type="text/css">
  .btnaction{
    width: 102px;
  }
</style>
<!-- START CONTENT -->
<section id="content">
  <!--start container-->
  <div class="container">
    <div class="section">
      <div class="card-panel">
          <div id="table-datatables">
            <h4 class="header">Formulario de registro de Servicio o Evento</h4>
            <form class="form" id="form_evens" method="post">
              <input type="hidden" name="id" id="id" value="0">
              <div class="row">
                <div class="col s2">
                  <label for="poliza" class="active">Seleccionar póliza</label>
                  <select class="browser-default" id="poliza" name="poliza">
                    <option value="0">Seleccionar poliza</option>
                    <?php foreach ($polizas as $item) { ?>
                        <option value="<?php echo $item->id; ?>" 
                          data-cubre="<?php echo $item->cubre; ?>"
                          data-vigencia="<?php echo $item->vigencia_meses; ?>"
                          data-vigenciac="<?php echo $item->vigencia_clicks; ?>"
                          ><?php echo $item->nombre; ?></option>
                    <?php   } ?>
                    
                  </select>
                </div>
                <div class="col s2 polizamodelo">
                  <label for="polizamodelo" class="active">Seleccionar Modelo</label>
                  <select class="browser-default" id="polizamodelo" name="polizamodelo">
                  </select>
                </div>
                <div class="col s2 newmodelo" style="display: none;">
                  <label class="active" for="newmodelo">Nuevo modelo</label>
                  <input type="text" name="newmodelo" id="newmodelo" class="form-control-bmz" readonly>
                </div>
                <div class="col s2">
                  
                  <input type="checkbox" id="sinmodelo" name="sinmodelo" value="1" >
                  <label for="sinmodelo">Sin modelo</label>
              
                </div>
                <div class="col s3">
                  <label for="nombre" class="active">Nombre del servicio o evento</label>
                  <input id="nombre" name="nombre" type="text" class="form-control-bmz">
                </div>
                
                
              </div>
              
              <div class="row">
                  <div class="col s12">
                    <label for="descripcion" class="active">Descripcion</label>
                    <input id="descripcion" name="descripcion" type="text" class="form-control-bmz">
                  </div>
              </div>
              <div class="row">
                <div class="col s2">
                  <label>Local</label>
                  <input type="text" name="local" id="local">
                </div>
                <div class="col s2">
                  <label>Semi</label>
                  <input type="text" name="semi" id="semi">
                </div>
                <div class="col s2">
                  <label>Foraneo</label>
                  <input type="text" name="foraneo" id="foraneo">
                </div>
                <div class="col s2">
                  <label>Especial</label>
                  <input type="text" name="especial" id="especial">
                </div>
                <div class="col s2">
                </div>
                <div class="col s2">
                  <a class="waves-effect cyan btn-bmz addserveven" >Agregar</a>
                  <a class="waves-effect btn-bmz red cancelarserveven" style="display: none;">Cancelar</a>
                </div>
              </div>
            </form>




            <div class="row">
              <div class="col s12">

                

                  

                  <div class="row">

                    <div class="col s12">
  
                     <table id="tabla_even" class="responsive-table display" cellspacing="0">
                      <thead>
                        <tr>
                            <th></th>
                            <th>Servicio o evento</th>
                            <th>Equipo</th>
                            <th>Descripcion</th>
                            <th>Local</th>
                            <th>Semi</th>
                            <th>Foraneo</th>
                            <th>Especial</th>
                            <th></th>
                        </tr>
                      </thead>

                      <tbody >
                        
                      </tbody>
                    </table>
                    </div>
                  </div>

                
              </div>
            </div>
          </div>
      </div>
    </div>  
  </div>
</section>
