<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Garantias extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloGarantia');
        $this->submenu=72;
        if($this->session->userdata('logeado')==true){
            $this->perfilid =$this->session->userdata('perfilid');
            $this->idpersonal = $this->session->userdata('idpersonal');
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,$this->submenu);// 72 es el id del submenu
            if ($permiso==0) {
                redirect('Sistema');
            }
        }else{
            redirect('Sistema'); 
        }
    }

    public function index(){
        $data['MenusubId']=$this->submenu;
    	$this->load->view('header');
        $this->load->view('main');
        $this->load->view('garantias/listado',$data);
        $this->load->view('footer');
        $this->load->view('garantias/listadojs');
    }
    public function getListadoequipos() {
        $params = $this->input->post();

        $getdata = $this->ModeloGarantia->get_equipos($params);
        $totaldata= $this->ModeloGarantia->get_equipos_total($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    function saveeditgarantia(){
        $params = $this->input->post();
        $tipo=$params['tipo'];
        $idventa=$params['idventa'];
        $modelo=$params['modeloid'];
        $serie=$params['serieid'];
        $folio_rma=$params['folio_rma'];
        $serie_instalada=$params['serie_instalada'];
        $serie_retirada=$params['serie_retirada'];
        $fecha_tramite=$params['fecha_tramite'];
        $fecha_envio=$params['fecha_envio'];
        $notacredito=$params['notacredito'];
        $folio_rma_rechazo = $params['folio_rma_rechazo'];

        $res = $this->ModeloCatalogos->getselectwheren('garantias',array('tipo'=>$tipo,'idventa'=>$idventa,'serieid'=>$serie,'modeloid'=>$modelo));
        if($res->num_rows()>0){
            $rest = $res->row();
            unset($params['tipo']);
            unset($params['idventa']);
            unset($params['modeloid']);
            unset($params['serieid']);
            $this->ModeloCatalogos->updateCatalogo('garantias',$params,array('id'=>$rest->id));
        }else{
            $this->ModeloCatalogos->Insert('garantias',$params);
        }
    }
    function verificarregistrosgarantias(){
        $params = $this->input->post();
        $productos=$params['productos'];
        $DATAc = json_decode($productos);
        $pro=array();
        for ($i=0;$i<count($DATAc);$i++) {

            $res = $this->ModeloCatalogos->getselectwheren('garantias',array('tipo'=>$DATAc[$i]->tipop,'idventa'=>$DATAc[$i]->idVenta,'serieid'=>$DATAc[$i]->serieId,'modeloid'=>$DATAc[$i]->productoid));
            if($res->num_rows()>0){

                foreach ($res->result() as $item) {
                    $pro[]=array(
                                'tipo'=>$item->tipo,
                                'idventa'=>$item->idventa,
                                'modeloid'=>$item->modeloid,
                                'serieid'=>$item->serieid,
                                'folio_rma'=>$item->folio_rma,
                                'folio_rma_text'=>$item->folio_rma_text,
                                'serie_instalada'=>$item->serie_instalada,
                                'serie_retirada'=>$item->serie_retirada,
                                'fecha_tramite'=>$item->fecha_tramite,
                                'fecha_envio'=>$item->fecha_envio,
                                'notacredito'=>$item->notacredito,
                                'folio_rma_rechazo'=>$item->folio_rma_rechazo
                                );
                }
            }
        }
        echo json_encode($pro);
    }





    

}
?>