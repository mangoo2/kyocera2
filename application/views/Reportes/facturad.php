<?php
require_once dirname(__FILE__) . '/TCPDF4/examples/tcpdf_include.php'; 
require_once dirname(__FILE__) . '/TCPDF4/tcpdf.php';
$GLOBALS["Folio"]=$Folio;
//====================================================
class MYPDF extends TCPDF {
  //Page header
  public function Header() {
      $logos = base_url().'public/img/alta.png';
      
        $tamano1='30';//30
        $tamano2='40';//40
        $tamano3='30';//30
      $html = '
          <style type="text/css">
            .info_fac{
              font-size: 9px;
            }
            .info_facd{
              font-size: 8px;
            }
            .httablelinea{
              vertical-align: center;
              border-bottom: 1px solid #9e9e9e;
            }
            
            .httableleft{
              border-left: 1px solid #9e9e9e;
            }
            .httableright{
              border-right: 1px solid #9e9e9e;
            }
            .httabletop{
              border-top: 1px solid #9e9e9e;
            }
          </style>
          <table width="100%" border="0" cellpadding="4px" class="info_fac">
            <tr>
              <td rowspan="10" width="'.$tamano1.'%"><img src="'.$logos.'"></td>
              <td width="50%" valign="top"><h1> Factura Detalles </h1></td>
            </tr>
          </table>

          ';
        $this->writeHTML($html, true, false, true, false, '');
  }
    // Page footer
  public function Footer() {
    $html2='';
    $styleQR = array('border' => 0, 
         'vpadding' => '0', 
         'hpadding' => '0', 
         'fgcolor' => array(0, 0, 0), 
         'bgcolor' => false, 
         'module_width' => 1, 
         'module_height' => 1);
    $html='';

    $html2 .= ' 
          <style type="text/css">
              .fontFooter10{
                font-size: 10px;
              }
              .fontFooterp{
                font-size: 9px;
                margin-top:0px;
              }
              .fontFooter{
                font-size: 9px;
                margin-top:0px;
              }

              .fontFooterp{
                font-size: 8px;
              }
              .fontFooterpt{
                font-size: 7px;
              }
              .fontFooterpt6{
                font-size: 6px;
              }
              p{
                margin:0px;
              }
              .valign{
                vertical-align:middle;
              }
              .httablelinea{
                vertical-align: center;
                border-bottom: 1px solid #9e9e9e;
              }
              .footerpage{
                font-size: 9px;
                color: #9e9e9e;
              }
          </style>';

    $html2 .= '
      <table width="100%" border="0" cellpadding="2" class="fontFooterp">
        <tr>
          <td align="right" class="footerpage">
          Pagina '.$this->getAliasNumPage().' de '.$this->getAliasNbPages().'</td>
        </tr>
      </table>
    ';
      $this->writeHTML($html2, true, false, true, false, '');
      
  }
} 
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Kyocera');
$pdf->SetTitle('Folio_'.$GLOBALS["Folio"]);
$pdf->SetSubject('factura');
$pdf->SetKeywords('factura');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins('10', '40', '10');
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin('20');

// set auto page breaks
$pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('dejavusans', '', 7);

//====================================================
//$pdf->setPrintHeader(false);
//$pdf->setPrintFooter(false);
//$pdf->SetMargins('10', '10', '10');
//$pdf->SetFooterMargin('10');
$pdf->AddPage('P', 'A4');
  $html='<table border="1" align="center" cellpadding="2" style="background-color: #d8d4d4; color: black">
            <tr valign="middle">
              <th class="httable" style="font-size:12px"><b>CONTADORES</b></th>
            </tr>
          </table>';
  $html.='<table border="1" align="center" cellpadding="2">
          <thead> 
            <tr valign="middle">
              <th class="httable" rowspan="2"><b>Equipo</b></th>
              <th class="httable" rowspan="2"><b>Serie</b></th>
              <th class="httable" colspan="3" ><b>Copias</b></th>
              <th class="httable" colspan="3" ><b>Escaner</b></th>
              <th class="httable" rowspan="2"><b>Produccion</b></th>
              <th class="httable" rowspan="2"><b>Toner consumido</b></th>
            </tr>
            <tr valign="middle">
              <th class="httable"><b>Contador Inicial</b></th>
              <th class="httable"><b>Contador Final</b></th>
              <th class="httable"><b>Producción</b></th>
              <th class="httable"><b>Contador Inicial</b></th>
              <th class="httable"><b>Contador Final</b></th>
              <th class="httable"><b>Producción</b></th>
            </tr>
          </thead>
        ';
$prefacturaId=0;
$producciontotal=0;
foreach ($detalleperidofactura->result() as $item) {
  if ($prefacturaId!=$item->prefacturaId) {
      $prefacturaId=$item->prefacturaId;
      $detalleresult=$this->ModeloCatalogos->getselectwheren('alta_rentas_prefactura',array('prefId'=>$prefacturaId));
      $perdiodo='';
      foreach ($detalleresult->result() as $itemsr) {
        $perdiodo='Periodo de '.$itemsr->periodo_inicial.' al '.$itemsr->periodo_final.' / Fecha de captura '.$itemsr->reg;
      }
      $html.=' 
            <tr valign="middle">
              <td class="httable" colspan="10">'.$perdiodo.'</td>
            </tr>
          
        ';
    }
    $html.=' 
            <tr valign="middle">
              <td class="httable">'.$item->modelo.'</td>
              <td class="httable">'.$item->serie.'</td>
              <td class="httable">'.$item->c_c_i.'</td>
              <td class="httable">'.$item->c_c_f.'</td>
              <td class="httable">'.$tc=$item->c_c_f-$item->c_c_i.'</td>
              <td class="httable">'.$item->e_c_i.'</td>
              <td class="httable">'.$item->e_c_f.'</td>
              <td class="httable">'.$tc=$item->e_c_f-$item->e_c_i.'</td>
              <td class="httable">'.$item->produccion.'</td>
              <td class="httable">'.$item->toner_consumido.'</td>
            </tr>
          
        ';
        $producciontotal=$producciontotal+$item->produccion;
}
  $html.='
          <tr valign="middle">
              <td class="httable" colspan="8" align="right">Produccion Total</td>
              <td class="httable">'.$producciontotal.'</td>
              <td class="httable"></td>
            </tr>

  </table>';

$pdf->writeHTML($html, true, false, true, false, '');
//=======================================================================

  $html='<br><br><br><br>
          <table border="1" align="center" cellpadding="2" style="background-color: #d8d4d4; color: black">
            <tr valign="middle">
              <th class="httable" style="font-size:12px"><b>TONER</b></th>
            </tr>
          </table>';
  $html.='<table border="1" align="center" cellpadding="2">
          <thead> 
            <tr valign="middle" style="background-color: #d88888; color: black">
              <th class="httable" ><b>TONER</b></th>
              <th class="httable" ><b>FOLIO</b></th>
              <th class="httable"  ><b>ESTATUS</b></th>
              
            </tr>
          </thead>
        ';
foreach ($detalleconsumiblesfolios->result() as $item) {
  if ($item->status==0) {
    $status='';
  }elseif ($item->status==1) {
    $status='stock Cliente';
  }else{
    $status='Retorno';
  }
    $html.=' 
            <tr valign="middle">
              <td class="httable">'.$item->modelo.'</td>
              <td class="httable">'.$item->foliotext.'</td>
              <td class="httable">'.$status.'</td>
            </tr>
          
        ';  
}
 $html.='</table>';
$pdf->writeHTML($html, true, false, true, false, '');
  foreach ($detallecimages->result() as $item) {
    $pdf->AddPage('P', 'A4');
    if($item->tipoimg==0){
      $file = fopen(base_url()."uploads/rentas_hojasestado/".$item->nombre, "r");
      $line = fgets($file);
      $html= $line;
      fclose($file);
    }else{
      $html=base_url()."uploads/rentas_hojasestado/".$item->nombre;
    }
    $pdf->Image($html, 'C', 0, 250, 297, '', '', '', true, 200, 'C', false, false, 0, false, false, true);
    //else  
    //log_message('error', base64_decode(base_url()."uploads/rentas_hojaestado/".$line));
    //log_message('error', '<th class="httable"><img src="'.base64_decode(base_url()."uploads/rentas_hojasestado/".$line).'"></th>');
  }//for

  
//=======================================================================
$pdf->Output('Folios_.pdf', 'I');
//$pdf->Output('../../facturas/'.$GLOBALS["Folio"].'.pdf', 'F');
//$pdf->Output('/facturas/'.$GLOBALS["Folio"].'.pdf', 'F');
?>