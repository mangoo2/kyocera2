<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Viewcuestionario extends CI_Controller {
	function __construct(){
        parent::__construct();
        //$this->load->model('Solicitud');
        $this->load->model('ModeloSolicitud');
    }
	public function index(){
		$id=$this->input->get('id');
		//$data['aspirante']=$this->ModeloSolicitud->datoaspiranteget($id);




		$data['datosescolares']=$this->ModeloSolicitud->datosescolaresget($id);
		$data['universidadanahuac']=$this->ModeloSolicitud->datosuniversidadanahuacget($id);
		$data['familiares']=$this->ModeloSolicitud->datosrelacionesfamiliaresget($id);
		$data['familiareshermanos']=$this->ModeloSolicitud->datosrfhermanosget($id);
		$data['sociales']=$this->ModeloSolicitud->datossocialesget($id);
		$data['personalizacion']=$this->ModeloSolicitud->datospersonalizacionget($id);
		$data['creencias']=$this->ModeloSolicitud->datoscreenciasvaloresget($id);
		$data['ocio']=$this->ModeloSolicitud->datosocioget($id);
		$data['colegiomayor']=$this->ModeloSolicitud->datocolegiomayorget($id);
		$data['salud']=$this->ModeloSolicitud->datosaludget($id);
		$data['aspirante']=$this->ModeloSolicitud->solicitante($id);

		$data['padre']=$this->ModeloSolicitud->datospadreget($id);
		$data['madre']=$this->ModeloSolicitud->datosmadreget($id);
		$data['fotro']=$this->ModeloSolicitud->datosotroget($id);
		$data['emergencia']=$this->ModeloSolicitud->datosemergenciaget($id);
		$this->load->view('Reportes/captura',$data);
	        
	}
}