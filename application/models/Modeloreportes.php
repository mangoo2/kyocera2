<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class Modeloreportes extends CI_Model {
    public function __construct(){
        parent::__construct();
    }
    function getlistcontratosvencidos(){

        
    }	

    function sql_contratos_vencidos($params){
        $sql = "";
        $sql .= "(";
        $sql .= "SELECT 
                    con.idcontrato,
                    c.empresa,
                    r.idCliente,
                    con.fechainicio,
                    con.vigencia,
                    (CASE 
                            WHEN con.vigencia = 1 THEN 12
                            WHEN con.vigencia = 2 THEN 24
                            WHEN con.vigencia = 3 THEN 36
                            ELSE con.vigencia -- Mantener el valor original si no coincide
                        END) as meses,
                     con.folio,
                     con.idRenta,
                     con.estatus,
                     DATE_ADD(con.fechainicio, INTERVAL (CASE 
                            WHEN con.vigencia = 1 THEN 12
                            WHEN con.vigencia = 2 THEN 24
                            WHEN con.vigencia = 3 THEN 36
                            ELSE con.vigencia -- Mantener el valor original si no coincide
                        END) MONTH) AS fecha_final,
                     ar.continuarcontadores,
                     ar.conti_cont_fecha,
                     con.permt_continuar
                FROM rentas AS r
                INNER JOIN clientes AS c ON c.id = r.idCliente
                INNER JOIN contrato AS con ON con.idRenta = r.id 
                left join alta_rentas as ar on ar.idcontrato=con.idcontrato
                WHERE 
                    con.estatus=1 AND 
                    DATE_ADD(con.fechainicio, INTERVAL (CASE 
                            WHEN con.vigencia = 1 THEN 12
                            WHEN con.vigencia = 2 THEN 24
                            WHEN con.vigencia = 3 THEN 36
                            ELSE con.vigencia -- Mantener el valor original si no coincide
                        END) MONTH)<=CURDATE()
                group by con.idcontrato
                ORDER BY fecha_final DESC";
        $sql.=') as datos';
        return $sql;
    }
    function getlistadocontratosvencidos($params){
            
            $subconsulta = $this->sql_contratos_vencidos($params);
        
            $columns = array(
                0=>'idcontrato',
                1=>'folio',
                2=>'empresa',
                3=>'fechainicio',
                4=>'meses',
                5=>'fecha_final',
                6=>'conti_cont_fecha',
                7=>'permt_continuar'
            );
            $columnsss = array(
                0=>'idcontrato',
                1=>'folio',
                2=>'empresa',
                3=>'fechainicio',
                4=>'meses',
                5=>'fecha_final',
                6=>'conti_cont_fecha',
                7=>'permt_continuar'
            );
            
            $select="";
            foreach ($columns as $c) {
                $select.="$c, ";
            }
            $this->db->select($select);
            $this->db->from($subconsulta);
                
            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->db->group_start();
                foreach($columnsss as $c){
                    $this->db->or_like($c,$search);
                }
                $this->db->group_end();  
            }            
            $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
            $this->db->limit($params['length'],$params['start']);
            //echo $this->db->get_compiled_select();
            $query=$this->db->get();
            // print_r($query); die;
            return $query;
    }
    function getlistadocontratosvencidost($params){
        
        $subconsulta = $this->sql_contratos_vencidos($params);
        $columns = array(
            0=>'idcontrato',
            1=>'folio',
            2=>'empresa',
            3=>'fechainicio',
            4=>'meses',
            5=>'fecha_final',
            6=>'conti_cont_fecha',
            7=>'permt_continuar'
        );
        $this->db->select('COUNT(*) as total');
        $this->db->from($subconsulta);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        //$this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        ///$this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query->row()->total;
    }
}

?>