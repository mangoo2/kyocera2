<style type="text/css">
  table td{
    font-size: 12px;
    padding: 6px 7px;
  }
  .no_tienerelacion{
    width: 40px;
  }
</style>
<!-- START CONTENT -->
    <section id="content">
      <!--start container-->
        <div class="container">
          <div class="section">
            <div class="card-panel">
              <input id="base_url" type="hidden" name="base_url" value="<?php echo base_url(); ?>">
              <input id="idCliente" type="hidden" name="idCliente" value="<?php echo $idCliente; ?>">
              
              <div id="table-datatables">
                <br>
                    <h4 class="header">Facturación</h4>
                    <div class="row">
                      

                      <div class="col s12">
                        <br><br>  
                        <table id="tabla_facturacion_cliente" name="tabla_facturacion_cliente" class="responsive-table display" cellspacing="0">
                          <thead>
                            <tr>
                              <th>#</th>
                              <th>Cliente</th>
                              <th>Equipo</th>
                              <th>Foto</th>
                              <th>Modelo</th>
                              <th>Área de la empresa</th>
                              <th>Plaza</th>
                              <th>Costo por click</th>
                              <th>Excedente</th>
                              <th>Acciones</th>
                              
                            </tr>
                          </thead>
                          <tbody>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>

                  <div class="row" align="center">
                    <button class="btn waves-effect waves-light green " type="button" id="generarFacturaEquipos" name="generarFacturaEquipos">
                      Generar una sola factura para varios equipos
                    </button>
                    
                  </div>
              </div> 

            </div> 
          </div>



          <div id="modalAcciones" class="modal"  >
            <div class="modal-content ">

              <h4>  Seleccione </h4>

              <div class="col s12 m12 l6">
                    <div class="row" align="center">
                        <form action="#">
                        <?php foreach($facturasPorCliente as $factura)
                        {
                        ?>
                        <input type="checkbox" id="test<?php echo $factura->id?>" />
                        <label for="test<?php echo $factura->id?>"><?php echo $factura->modelo?></label>
                        &nbsp;&nbsp;
                        <?php
                        }?>
                          </form>                 
                      </div>
                </div>
                
            <div class="modal-footer">
              <a href="<?php echo base_url(); ?>index.php/SolicitudesContrato/factura/1" class="btn waves-effect waves-light green ">
                Facturar
              </a>
              <a href="<?php echo base_url(); ?>index.php/SolicitudesContrato/factura/1" class="btn waves-effect waves-light cyan ">
                Excedentes
              </a>
              <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
            </div>
          </div>
        </div> 
     
    </section>