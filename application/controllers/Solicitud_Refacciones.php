<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Solicitud_Refacciones extends CI_Controller {
    public function __construct()     {
        parent::__construct();

        $this->load->model('ModeloCatalogos');
        $this->load->model('Configuraciones_model');
        $this->load->model('ModeloServicio');
        $this->load->model('ModeloSolRefacciones');
        $this->load->model('Ventas_model');
        $this->load->model('Equipos_model');
        $this->load->model('Clientes_model');
        $this->load->model('Polizas_model');
        $this->load->helper('url');
        date_default_timezone_set('America/Mexico_City');
        $this->submenu=57;
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fechah = date('Y-m-d');
        if($this->session->userdata('logeado')==true){
            $this->perfilid =$this->session->userdata('perfilid');
            $this->idpersonal = $this->session->userdata('idpersonal');
            /*
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,52);// 52 es el id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
            */
        }else{
            redirect('login'); 
        }
    }

    function index(){
        $data['MenusubId']=$this->submenu;
            //$data['bodegas']=$this->ModeloCatalogos->getselectwheren('bodegas',array('activo' => 1));
            $data['bodegas']=$this->ModeloCatalogos->view_session_bodegas();

            $data['equiposs'] = $this->Equipos_model->getEquiposParaSelect();
            //$data['familia'] = $this->Clientes_model->getDataFamilia();
            $data['equipos'] = $this->Equipos_model->getEquiposParaSelect();  
            //$data['resultzonas']=$this->ModeloCatalogos->getselectwheren('rutas',array('status'=>1));
            $data['resultzonas']=$this->ModeloCatalogos->view_session_rutas();

            //$data['resultstec']=$this->Configuraciones_model->searchTecnico(); 
            $data['resultstec']=$this->Configuraciones_model->view_session_searchTecnico();
            $data['consumiblesresult']=$this->ModeloCatalogos->getselectwheren('consumibles',array('status'=>1)); 
            $data['refaccionesresult']=$this->ModeloCatalogos->getselectwheren('refacciones',array('status'=>1));
            $data['rowsejecutivas']=$this->ModeloCatalogos->ejecutivos_rentas_ventas(); 
            
            $data['equiposimg'] = $this->Clientes_model->getDataEquipo2();
            $data['polizas'] = $this->Polizas_model->getListadoPolizas(); 
            $data['perfilid'] = $this->perfilid;
            $this->load->view('header');
            $this->load->view('main');
            $this->load->view('solicitud_refaccion/listaservicios',$data);
            $this->load->view('footer');
            $this->load->view('solicitud_refaccion/listaserviciosjs');
            $this->load->view('info_m_servisios');
    }
    
    public function getlistaservicioc() {
        $params = $this->input->post();
        $getdata = $this->ModeloServicio->getlistasolicitud_Refacciones($params);
        $totaldata= $this->ModeloServicio->getlistasolicitud_Refaccionest($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    public function getlistaserviciop() {
        $params = $this->input->post();
        $getdata = $this->ModeloServicio->getlistaserviciop_solicitud($params);
        $totaldata= $this->ModeloServicio->getlistaserviciopt_solicitud($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    public function getlistaserviciocl() {
        $params = $this->input->post();
        $getdata = $this->ModeloServicio->getlistaserviciocl_solicitudes($params);
        $totaldata= $this->ModeloServicio->getlistaservicioclt_solicitudes($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    public function getlistaserviciovent() {
        $params = $this->input->post();
        $getdata = $this->ModeloServicio->getlistaserviciovent_solicitudes($params);
        $totaldata= $this->ModeloServicio->getlistaservicioventt_solicitudes($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    public function getlistaserviciotodos() {
        $params = $this->input->post();
        $getdata = $this->ModeloSolRefacciones->getsolicitudes($params);
        $totaldata= $this->ModeloSolRefacciones->getsolicitudes_total($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    function editarfecha(){
        $params = $this->input->post();
        $idrow =$params['idrow'];
        $table =$params['table'];
        $fecha =$params['fecha'];
        $hora =$params['hora'];
        $horafin =$params['horafin'];
        if ($table==1) {
            $tables='asignacion_ser_contrato_a_e';
            $idname='asignacionIde';
        }elseif ($table==2) {
            $tables='asignacion_ser_poliza_a_e';
            $idname='asignacionIde';
        }elseif ($table==3) {
            $tables='asignacion_ser_cliente_a';
            $idname='asignacionId';
        }
        $datas=array(
            'fecha'=>$fecha,
            'hora'=>$hora,
            'horafin'=>$horafin
        );
        $this->ModeloCatalogos->updateCatalogo($tables,$datas,array($idname=>$idrow));
    }
    function edittecnico(){
        $params = $this->input->post();
        $idrow = $params['idrow'];
        $idrowt = $params['idrowt'];
        $table = $params['table']; //1 contrato 2 poliza 3 cliente
        $tipo = $params['tipo'];//0 total 1 individual
        $tecnicoo = $params['tecnicoo'];
        $tecnico = $params['tecnico'];
        if ($table==1) {
            $tables='asignacion_ser_contrato_a_e';
            $idname='asignacionIde';
        }elseif ($table==2) {
            $tables='asignacion_ser_poliza_a_e';
            $idname='asignacionIde';
        }elseif ($table==3) {
            $tables='asignacion_ser_cliente_a';
            $idname='asignacionId';
        }
        if ($tipo==1) {
            $this->ModeloCatalogos->updateCatalogo($tables,array('tecnico'=>$tecnico),array($idname=>$idrow));
            $this->ModeloCatalogos->Insert('asignacion_ser_historial_t',array('asignacionId'=>$idrow,'tecnicoId'=>$tecnicoo,'tabless'=>$table));
        }else{
            $resultrow=$this->ModeloCatalogos->getselectwheren($tables,array('asignacionId'=>$idrowt));
            foreach ($resultrow->result() as $item) {
                if ($table==1) {
                    $idrowrow=$item->asignacionIde;
                }elseif ($table==2) {
                    $idrowrow=$item->asignacionIde;
                }elseif ($table==3) {
                    $idrowrow=$item->asignacionId;
                }
                $tecnico=$item->tecnico;
                $this->ModeloCatalogos->updateCatalogo($tables,array('tecnico'=>$tecnico),array($idname=>$idrowrow));
                $this->ModeloCatalogos->Insert('asignacion_ser_historial_t',array('asignacionId'=>$idrowrow,'tecnicoId'=>$tecnicoo,'tabless'=>$tecnico));
            }
        }
    }
    function viewhistorial(){
        $params = $this->input->post();
        $asi =$params['idrow'];
        $table =$params['table'];
        $resultrow = $this->ModeloServicio->historiatecnicos($asi,$table);
        $html='<table class="table">';
        foreach ($resultrow->result() as $item) {
            $html.='<tr>
                        <td>'.$item->nombre.' '.$item->apellido_paterno.' '.$item->apellido_materno.'</td>
                        <td>'.$item->reg.'</td>
                    </tr>
                    ';
        }
        $html.='</table>';
        echo $html;
    }
    function suspender(){
        $params = $this->input->post();
        $idrow =$params['idrow'];
        $table =$params['table'];
        $tserviciomotivo =$params['tserviciomotivo'];
        if ($table==1) {
            $tables='asignacion_ser_contrato_a_e';
            $idname='asignacionIde';
        }elseif ($table==2) {
            $tables='asignacion_ser_poliza_a_e';
            $idname='asignacionIde';
        }elseif ($table==3) {
            $tables='asignacion_ser_cliente_a';
            $idname='asignacionId';
        }
        $datas=array(
            'motivosuspencion'=>$tserviciomotivo,
            'status'=>3
        );
        $this->ModeloCatalogos->updateCatalogo($tables,$datas,array($idname=>$idrow));
    }
    function datosventaservicio(){
        $params = $this->input->post();
        $id =$params['id'];
        $tipo =$params['tipo'];
        $resultadoconsumibles=$this->Ventas_model->datosserviciosconsumibles($id,$tipo);
        $resultadorefacciones=$this->Ventas_model->datosserviciosrefacciones($id,$tipo);
        $array = array(
                    'consumibles'=>$resultadoconsumibles->result(),
                    'refacciones'=>$resultadorefacciones->result()
        );
        echo json_encode($array);
    }
    function editarventaservicio(){
        $params = $this->input->post();
        $tasign=$params['tasign'];
        $tipo=$params['tipo'];
        $vconsumible=$params['vconsumible'];
        $vrefaccion  =$params['vrefaccion'];

        $DATAvc = json_decode($vconsumible);
        for ($s=0;$s<count($DATAvc);$s++){
            if ($DATAvc[$s]->id > 0) {

                $this->ModeloCatalogos->updateCatalogo('asignacion_ser_avc',array('cantidad'=>$DATAvc[$s]->cantidad),array('asId'=>$DATAvc[$s]->id));
            }else{
                $datavca =array(
                    'asignacionId'=>$tasign,
                    'cantidad'=>$DATAvc[$s]->cantidad,
                    'consumible'=>$DATAvc[$s]->consumible,
                    'costo'=>$DATAvc[$s]->precio,
                    'tipo'=>$tipo
                );
                $this->ModeloCatalogos->Insert('asignacion_ser_avc',$datavca);
            }
            
        }
        $DATAvr = json_decode($vrefaccion);
        for ($c=0;$c<count($DATAvr);$c++){
            if ($DATAvr[$c]->id > 0) {

                $this->ModeloCatalogos->updateCatalogo('asignacion_ser_avr',array('cantidad'=>$DATAvr[$c]->cantidad),array('asId'=>$DATAvr[$c]->id));
            }else{
                $datavcra =array(
                    'asignacionId'=>$tasign,
                    'cantidad'=>$DATAvr[$c]->cantidad,
                    'refacciones'=>$DATAvr[$c]->refaccion,
                    'costo'=>$DATAvr[$c]->precio,
                    'tipo'=>$tipo
                );
                $this->ModeloCatalogos->Insert('asignacion_ser_avr',$datavcra);
            }
            
        }
    }
    function deleteacre(){
        $params = $this->input->post();
        $rowid = $params['rowid'];
        $tipo = $params['tipo'];
        if ($tipo==1) {//consumible
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_avc',array('activo'=>0),array('asId'=>$rowid));
        }
        if ($tipo==2) {//refaccion
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_avr',array('activo'=>0),array('asId'=>$rowid));
        }
    }
    function cancelartotalvs(){
        $params = $this->input->post();
        $tasign = $params['tasign'];
        $this->ModeloCatalogos->updateCatalogo('asignacion_ser_avc',array('activo'=>0),array('asignacionId'=>$tasign));
        $this->ModeloCatalogos->updateCatalogo('asignacion_ser_avr',array('activo'=>0),array('asignacionId'=>$tasign));
    }
    function actualizarcotizacion(){
        $asig = $this->input->post('asig');
        $asige = $this->input->post('asige');
        $tipo = $this->input->post('tipo');
        $coti = $this->input->post('coti');
        $tipocot = $this->input->post('tipocot');

        //============================
            $result = array();
            if ($tipo==1) {
                $result = $this->ModeloCatalogos->getselectwheren('asignacion_ser_contrato_a_e',array('asignacionIde'=>$asige));
            }
            if ($tipo==2) {
                $result = $this->ModeloCatalogos->getselectwheren('asignacion_ser_poliza_a_e',array('asignacionIde'=>$asige));
            }
            if ($tipo==3) {
                $result = $this->ModeloCatalogos->getselectwheren('asignacion_ser_cliente_a_d',array('asignacionIdd'=>$asige));
            }
            if ($tipo==4) {
                $result = $this->ModeloCatalogos->getselectwheren('asignacion_ser_venta_a',array('asignacionId'=>$asig));
            }
            $cottotal=0;
            $cottotal_cot=1;
            foreach ($result->result() as $item) {
                $cottotal=0;
                $cottotal_cot=1;
                if($item->cot_refaccion!=null || $item->cot_refaccion!=''){
                    $cottotal++;
                    if($item->cot_referencia>0){
                        $cottotal_cot++;
                    }
                }
                if($item->cot_refaccion2!=null || $item->cot_refaccion2!=''){
                    $cottotal++;
                    if($item->cot_referencia2>0){
                        $cottotal_cot++;
                    }

                }
                if($item->cot_refaccion3!=null || $item->cot_refaccion3!=''){
                    $cottotal++;
                    if($item->cot_referencia3>0){
                        $cottotal_cot++;
                    }

                }
            }
        //=======================================
        $arraydatos=array('cot_status'=>2,'cot_referencia'=>$coti);
        if($tipocot==1){
            $arraydatos=array('cot_referencia'=>$coti);
        }
        if($tipocot==2){
            $arraydatos=array('cot_referencia2'=>$coti);
        }
        if($tipocot==3){
            $arraydatos=array('cot_referencia3'=>$coti);
        }
        if($cottotal_cot>=$cottotal){
            $arraydatos['cot_status']=2;
        }


        if ($tipo==1) {
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_contrato_a_e',$arraydatos,array('asignacionIde'=>$asige));
        }
        if ($tipo==2) {
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_poliza_a_e',$arraydatos,array('asignacionIde'=>$asige));
        }
        if ($tipo==3) {
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_cliente_a_d',$arraydatos,array('asignacionIdd'=>$asige));
        }
        if ($tipo==4) {
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_venta_a',$arraydatos,array('asignacionId'=>$asig));
        }
    }
    function actualizarcotizaciona(){
        $asig = $this->input->post('asig');
        $asige = $this->input->post('asige');
        $tipo = $this->input->post('tipo');
        $coti = $this->input->post('coti');
        if ($tipo==1) {
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_contrato_a_e',array('cot_referencia_tipo'=>$coti),array('asignacionIde'=>$asige));
        }
        if ($tipo==2) {
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_poliza_a_e',array('cot_referencia_tipo'=>$coti),array('asignacionIde'=>$asige));
        }
        if ($tipo==3) {
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_cliente_a',array('cot_referencia_tipo'=>$coti),array('asignacionId'=>$asig));
        }
    }
    function solicitar(){
        $params = $this->input->post();
        $cantidad=$params['cantidad'];
        $refaccion=$params['refaccion'];
        $this->ModeloCatalogos->Insert('solicitud_refaccion',array('cantidad'=>$cantidad,'idrefaccion'=>$refaccion,'personal'=>$this->idpersonal));
    }
    function f_priorizar_cot(){
        $params = $this->input->post();
        $tipo = $params['tipo'];
        $idrow = $params['idrow'];
        $pri = $params['pri'];
        $cot_ref1=$params['cot_ref1'];
        $cot_ref2=$params['cot_ref2'];
        $cot_ref3=$params['cot_ref3'];

        if ($tipo==1) {
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_contrato_a_e',array('priorizar_cot'=>$pri),array('asignacionIde'=>$idrow));
        }
        if ($tipo==2) {
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_poliza_a_e',array('priorizar_cot'=>$pri),array('asignacionIde'=>$idrow));
        }
        if ($tipo==3) {
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_cliente_a_d',array('priorizar_cot'=>$pri),array('asignacionIdd'=>$idrow));
        }
        if ($tipo==4) {
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_venta_a',array('priorizar_cot'=>$pri),array('asignacionId'=>$idrow));
        }
        if($cot_ref1>0){
            $this->ModeloCatalogos->updateCatalogo('cotizaciones',array('priorizar_cot'=>$pri),array('id'=>$cot_ref1));    
        }
        if($cot_ref2>0){
            $this->ModeloCatalogos->updateCatalogo('cotizaciones',array('priorizar_cot'=>$pri),array('id'=>$cot_ref2));    
        }
        if($cot_ref3>0){
            $this->ModeloCatalogos->updateCatalogo('cotizaciones',array('priorizar_cot'=>$pri),array('id'=>$cot_ref3));    
        }
    }
    function cambiarestatus(){
        $params = $this->input->post();
        $tipo = $params['tipo'];
        $idrow = $params['idrow'];
        $obsercs = $params['obsercs'];
        //$cot_referencia=$params['cot_referencia'];

        if ($tipo==1) {
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_contrato_a_e',array('cot_status'=>2,'cot_status_ob'=>$obsercs),array('asignacionIde'=>$idrow));
        }
        if ($tipo==2) {
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_poliza_a_e',array('cot_status'=>2,'cot_status_ob'=>$obsercs),array('asignacionIde'=>$idrow));
        }
        if ($tipo==3) {
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_cliente_a_d',array('cot_status'=>2,'cot_status_ob'=>$obsercs),array('asignacionIdd'=>$idrow));
        }
        if ($tipo==4) {
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_venta_a',array('cot_status'=>2,'cot_status_ob'=>$obsercs),array('asignacionId'=>$idrow));
        }
        //$this->ModeloCatalogos->updateCatalogo('cotizaciones',array('priorizar_cot'=>$pri),array('id'=>$cot_referencia));
    }
    function consultarcompraservicio(){
        $params = $this->input->post();
        $serie=$params['serie'];

        $estatus_compra='<span class="text-red">Equipo sin fecha de compra</span>';
        $estatus_servicio='<span class="text-red">Equipo sin fecha de servicio</span>';
        $fechacompra='';$fechaser='';
        //=================================
            $strq="SELECT * FROM `series_compra_productos` WHERE serie='$serie' ORDER BY `series_compra_productos`.`serieId` ASC LIMIT 1";
            $query_com = $this->db->query($strq);
            foreach ($query_com->result() as $item) {
                if($item->factura_fecha!=''){
                    $fechacompra=$item->factura_fecha;
                }else{
                    $fechacompra=$item->reg;
                }
                $estatus_compra='<span class="text-green">Equipo con fecha de compra: '.$fechacompra.'</span>';
            }
        //=================================
        //=================================
            $strq="SELECT asce.* 
                    FROM series_productos as sp 
                    INNER JOIN asignacion_ser_contrato_a_e as asce on asce.serieId=sp.serieId 
                    WHERE sp.serie='$serie' AND asce.status=2 AND asce.activo=1 ORDER BY `asce`.`fecha` ASC LIMIT 1";
            $query_com = $this->db->query($strq);
            foreach ($query_com->result() as $item) {
                $fechaser=$item->fecha;
                
                $estatus_servicio='<span class="text-green">Primera fecha de servicio del equipo: '.$fechaser.'</span>';
            }
        //=================================
            $array = array(
                    'compra'=>$estatus_compra,
                    'compraf'=>$fechacompra,
                    'servicio'=>$estatus_servicio,
                    'serviciof'=>$fechaser
                );
        echo json_encode($array);
    }
    function infoserdll(){
        $params = $this->input->post();
        $idser=$params['idser'];
        $tipo=$params['tipo']; //1 contrato 2 poliza 3 cliente 4 venta
        
        $result =$this->ModeloSolRefacciones->infoequipossolicitudrefacciones($idser,$tipo);
        $html='';
        foreach ($result->result() as $item) {
                $pri1_op_1='';$pri1_op_2='';$pri1_op_3='';$pri2_op_1='';$pri2_op_2='';$pri2_op_3='';$pri3_op_1='';$pri3_op_2='';$pri3_op_3='';

                if($item->prioridad==1){
                    $pri1_op_1=' selected ';
                }
                if($item->prioridad==2){
                    $pri1_op_2=' selected ';
                }
                if($item->prioridad==3){
                    $pri1_op_3=' selected ';
                }
                if($item->prioridad2==1){
                    $pri2_op_1=' selected ';
                }
                if($item->prioridad2==2){
                    $pri2_op_2=' selected ';
                }
                if($item->prioridad2==3){
                    $pri2_op_3=' selected ';
                }
                if($item->prioridad3==1){
                    $pri3_op_1=' selected ';
                }
                if($item->prioridad3==2){
                    $pri3_op_2=' selected ';
                }
                if($item->prioridad3==3){
                    $pri3_op_3=' selected ';
                }
            
            $html.='<tr class="tr_'.$item->asignacionId.'_'.$item->asignacionIde.'">';
                $html.='<td>'.$item->modelo.'
                    <input type="hidden" id="ser" value="'.$item->asignacionId.'">
                    <input type="hidden" id="serve" value="'.$item->asignacionIde.'">
                    <input type="hidden" id="tipo" value="'.$item->cpev.'">
                </td>';
                $html.='<td>';
                    $html.=$item->serie;
                $html.='</td>';
                $html.='<td>';
                    $html.='<br><label>Contador</label>';
                    $html.='<input type="number" id="cot_contador" class="fc-bmz ct_c" value="'.$item->cot_contador.'">';
                $html.='</td>';
                
                $html.='<td>';
                    $html.='<label>POR DESGASTE</label>';
                    $html.='<textarea class="form-control-bmz" id="cot_refaccion" >'.$item->cot_refaccion.'</textarea>';
                    $html.='<label>Prioridad</label><select class="fc-bmz browser-default prioridadr" id="prioridad1">';
                        $html.='<option value="0">Seleccione Prioridad</option>';
                        $html.='<option value="1" '.$pri1_op_1.'>Prioridad 1</option>';
                        $html.='<option value="2" '.$pri1_op_2.'>Prioridad 2</option>';
                        $html.='<option value="3" '.$pri1_op_3.'>Prioridad 3</option>';
                    $html.='</select>';
                $html.='</td>';
                $html.='<td>';
                    $html.='<label>POR DAÑO</label>';
                    $html.='<textarea class="form-control-bmz" id="cot_refaccion2" >'.$item->cot_refaccion2.'</textarea>';
                    $html.='<label>Prioridad</label><select class="fc-bmz browser-default prioridadr" id="prioridad2">';
                        $html.='<option value="0">Seleccione Prioridad</option>';
                        $html.='<option value="1" '.$pri2_op_1.'>Prioridad 1</option>';
                        $html.='<option value="2" '.$pri2_op_2.'>Prioridad 2</option>';
                        $html.='<option value="3" '.$pri2_op_3.'>Prioridad 3</option>';
                    $html.='</select>';
                $html.='</td>';
                $html.='<td>';
                    $html.='<label>POR GARANTIA</label>';
                    $html.='<textarea class="form-control-bmz" id="cot_refaccion3" >'.$item->cot_refaccion3.'</textarea>';
                    $html.='<label>Prioridad</label><select class="fc-bmz browser-default prioridadr" id="prioridad3">';
                        $html.='<option value="0">Seleccione Prioridad</option>';
                        $html.='<option value="1" '.$pri3_op_1.'>Prioridad 1</option>';
                        $html.='<option value="2" '.$pri3_op_2.'>Prioridad 2</option>';
                        $html.='<option value="3" '.$pri3_op_3.'>Prioridad 3</option>';
                    $html.='</select>';
                $html.='</td>';
                $html.='<td></td>';
                $html.='<td><span class="b-btn b-btn-primary" onclick="limpiartr('.$item->asignacionId.','.$item->asignacionIde.')">Limpiar</span></td>';
            $html.='</tr>';
        }
        
        echo $html;
    }
    function saveactualizacioncot(){
        $paramss = $this->input->post();
        $datos=$paramss['datos'];

        $DATAvr = json_decode($datos);
        for ($i=0;$i<count($DATAvr);$i++){
            $tipo=$DATAvr[$i]->tipo;
            $idrow=$DATAvr[$i]->ser;
            $idrowe=$DATAvr[$i]->serve;

            $cot_status_v=0;
            if ($DATAvr[$i]->cot_refaccion!='') {
                $cot_status_v=1;
            }
            if ($DATAvr[$i]->cot_refaccion2!='') {
                $cot_status_v=1;
            }
            if ($DATAvr[$i]->cot_refaccion3!='') {
                $cot_status_v=1;
            }

            if($cot_status_v==1){
                $params['cot_status']=1;
                $params['cot_contador']=$DATAvr[$i]->cot_contador;
                $params['cot_refaccion']=$DATAvr[$i]->cot_refaccion;
                $params['cot_refaccion2']=$DATAvr[$i]->cot_refaccion2;
                $params['cot_refaccion3']=$DATAvr[$i]->cot_refaccion3;
                if($tipo==4){
                    $params['prioridadr']=$DATAvr[$i]->prioridad1;
                    $params['prioridadr2']=$DATAvr[$i]->prioridad2;
                    $params['prioridadr3']=$DATAvr[$i]->prioridad3;
                }else{
                    $params['prioridad']=$DATAvr[$i]->prioridad1;
                    $params['prioridad2']=$DATAvr[$i]->prioridad2;
                    $params['prioridad3']=$DATAvr[$i]->prioridad3;
                }
            }else{
                $params['cot_status']=0;
                $params['cot_refaccion']='';
                $params['cot_refaccion2']='';
                $params['cot_refaccion3']='';
                $params['prioridad']=0;
                if($tipo==4){
                    $params['prioridadr']=0;
                    $params['prioridadr2']=0;
                    $params['prioridadr3']=0;
                }else{
                    $params['prioridad']=0;
                    $params['prioridad2']=0;
                    $params['prioridad3']=0;
                }
            }

            if($tipo==1){
                $this->ModeloCatalogos->updateCatalogo('asignacion_ser_contrato_a_e',$params,array('asignacionIde'=>$idrowe));
            }
            if($tipo==2){
                $this->ModeloCatalogos->updateCatalogo('asignacion_ser_poliza_a_e',$params,array('asignacionIde'=>$idrowe));
            }
            if($tipo==3){
                $this->ModeloCatalogos->updateCatalogo('asignacion_ser_cliente_a_d',$params,array('asignacionIdd'=>$idrowe));
            }
            if($tipo==4){
                $this->ModeloCatalogos->updateCatalogo('asignacion_ser_venta_a',$params,array('asignacionId'=>$idrow));
            }
        }
    }
    function obtenerservicioscli(){
        $params = $this->input->post();
        $cli=$params['idcli'];

        $result_con = $this->ModeloCatalogos->get_obtener_ser_cli($cli,$this->fechah,1);
        $result_pol = $this->ModeloCatalogos->get_obtener_ser_cli($cli,$this->fechah,2);
        $result_cli = $this->ModeloCatalogos->get_obtener_ser_cli($cli,$this->fechah,3);
        $result_ven = $this->ModeloCatalogos->get_obtener_ser_cli($cli,$this->fechah,4);
        $html='<option data-idser="0" data-tipo="0" data-view="0">Seleccione</option>';
        foreach ($result_con->result() as $item_c) {
            $html.='<option data-idser="'.$item_c->asignacionId.'" data-tipo="1" data-view="1">Rentas '.$item_c->fecha.'</option>';
        }
        foreach ($result_pol->result() as $item_p) {
            $html.='<option data-idser="'.$item_p->asignacionId.'" data-tipo="2" data-view="1">Poliza '.$item_p->fecha.'</option>';

        }
        foreach ($result_cli->result() as $item_cl) {
            $html.='<option data-idser="'.$item_cl->asignacionId.'" data-tipo="3" data-view="1">Evento '.$item_cl->fecha.'</option>';

        }
        foreach ($result_ven->result() as $item_cl) {
            $html.='<option data-idser="'.$item_cl->asignacionId.'" data-tipo="4" data-view="1">Venta '.$item_cl->fecha.'</option>';

        }
        echo $html;
    }


    
}
?>
