<?php
$a=session_id();
if(empty($a)) session_start();
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloCatalogos extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    function getadminsu(){
        $strq = "CALL SP_GET_PERSONAL_ADMINSU";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function getestados(){
        $strq = "SELECT * FROM estado";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
}