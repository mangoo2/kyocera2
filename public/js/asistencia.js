var base_url = $('#base_url').val();
var fechahoy = $('#fechahoy').val();
var horahoy = $('#horahoy').val();
var table;
var tablef;
var tablefhp;
$(document).ready(function() {	
	table = $('#tablelist').DataTable();
    tablef = $('#tableasistencia').DataTable();
    tablefhp = $('#table_permisosfuera').DataTable();
	load();
    
    $('#empleado').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un Empleado',
          ajax: {
            url: base_url+'Asistencias/searchempleados',
            dataType: "json",
            data: function (params) {
              var query = {
                search: params.term,
                type: 'public'
              }
              return query;
          },
          processResults: function(data){
            var clientes=data;
            var itemscli = [];
            data.forEach(function(element) {
                    itemscli.push({
                        id: element.personalId,
                        text: element.nombre+' '+element.apellido_paterno+' '+element.apellido_materno
                    });
                });
                return {
                    results: itemscli
                };        
          },  
        }
    });
    $('#pfh_empleado').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un Empleado',
          ajax: {
            url: base_url+'Asistencias/searchempleados',
            dataType: "json",
            data: function (params) {
              var query = {
                search: params.term,
                type: 'public'
              }
              return query;
          },
          processResults: function(data){
            var clientes=data;
            var itemscli = [];
            data.forEach(function(element) {
                    itemscli.push({
                        id: element.personalId,
                        text: element.nombre+' '+element.apellido_paterno+' '+element.apellido_materno
                    });
                });
                return {
                    results: itemscli
                };        
          },  
        }
    });
    //    load_calendario();
    $('#registrarvacacion').click(function(event) {
        $.ajax({
            type:'POST',
            url: base_url+"index.php/Asistencias/vacacionesadd",
            data: {
                personalId:$('#empleado option:selected').val(),
                tipo:$('#tipo option:selected').val(),
                fi:$('#fechainicio').val(),
                ff:$('#fechafin').val()
            },
            success: function (response){
                toastr["success"]("Cambio Realizado"); 
                $('#calendario').fullCalendar( 'refetchEvents' );
                loadtable();
                
            },
            error: function(response){
                toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema"); 
                 
            }
        }); 
    });
    $('#registrarpermiso').click(function(event) {
        $.ajax({
            type:'POST',
            url: base_url+"index.php/Asistencias/registrarpermiso",
            data: {
                personalId:$('#pfh_empleado option:selected').val(),
                fecha:$('#pfh_fecha').val()
            },
            success: function (response){
                toastr["success"]("Permiso Registrado"); 
                loadfhp();
                
            },
            error: function(response){
                toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema"); 
                 
            }
        }); 
    });
});
function load(){
    // esta funcion tiene que tener la misma estructura que el controlador del reporte de asistencias Asistencias/exportar
    var sdia=$('#sdia').val();
    const fechaComoCadena = sdia+" 00:00:00"; // día lunes
    const numeroDia = new Date(fechaComoCadena).getDay();//sabado 6 domingo 0
    console.log("Número de día de la semana: ", numeroDia); 
    
    var emp=$('#emp option:selected').val();
	table.destroy();
	table = $('#tablelist').DataTable({
        pagingType: 'input',
        dom: 'Blfrtip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'pdfHtml5',
        ],
        search: {
                return: true
            }, 
        //stateSave: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url+"index.php/Asistencias/getlists",
            type: "post",
            "data": {
                'sdia':sdia,
                'emp':emp
            },
        },
        "columns": [
            {"data": "id"},
            {"data": "emp",
                render:function(data,type,row){
                    var html ='';
                        if(row.emp==1){
                            html='Alta Productividad';
                        }else{
                            html='D-Impresión';
                        }
                        
                    return html;
                }
            },
            {"data": "personalId"},
            
            {"data": "nombre",
                render:function(data,type,row){
                    var html ='';
                        
                        html=row.nombre+' '+row.apellido_paterno+' '+row.apellido_materno;
                    return html;
                }
            },
            {"data": "fecha"},
            {"data": "hora_entrada"},
            {"data": "hora_salida",
                render:function(data,type,row){
                    var html ='';
                    if(row.hora_salida=='' || row.hora_salida==null){
                        if(row.fecha<fechahoy){
                            html ='17:00:00';
                            if(row.emp==2){
                                html ='18:00:00';    
                            }
                        }
                        if(row.fecha==fechahoy){
                            if(horahoy>=17){
                                html ='17:00:00';
                                if(row.emp==2){
                                    html ='18:00:00';    
                                }
                            }
                        }
                        
                    }else{
                        html = row.hora_salida;
                    }
                    return html;
                }
            },
            {"data": "nombre",
                render:function(data,type,row){
                    var html ='';
                    if(row.longitud>0 || row.latitud>0){

                        html='<a onclick="ubicacion('+row.longitud+','+row.latitud+')"><i class="material-icons">add_location</i></a>'
                    }
                    return html;
                }
            },
            {"data": "nombre",
                render:function(data,type,row){
                    var html ='';
                    if(row.longitud_salida>0 || row.latitud_salida>0){

                        html='<a onclick="ubicacion('+row.longitud_salida+','+row.latitud_salida+')"><i class="material-icons">add_location</i></a>'
                    }
                    return html;
                }
            },
            {"data": "nombre",
                render:function(data,type,row){
                    var html ='';
                        var btn_falta='<a class="b-btn b-btn-danger tooltipped" data-position="top" data-delay="50" data-tooltip="Falta">F</a>';

                        if(row.entrada_salida>0){
                            html = '<a class="b-btn b-btn-success tooltipped" data-position="top" data-delay="50" data-tooltip="Asistencia">A</a>';
                        }
                        if(numeroDia>0){
                            if(row.fecha==null){
                                html = btn_falta;
                                console.log('f1');
                            }else{
                                if(row.fecha==fechahoy){
                                    if(horahoy>=17){
                                        if(row.entrada_salida>0){
                                            //html = btn_falta;
                                        }else{
                                            html = btn_falta;
                                            console.log('f2');
                                        }

                                    }
                                }else{
                                    if(row.latitud_salida>0){
                                            //html = btn_falta;
                                    }else{
                                        //html = btn_falta;
                                        console.log('f3');
                                    }
                                }
                            }
                        }
                        if (row.estatus==1) {
                            html = '<a class="b-btn b-btn-success tooltipped" data-position="top" data-delay="50" data-tooltip="Permiso">P</a>';
                        }
                        if (row.estatus==2) {
                            html = '<a class="b-btn b-btn-success tooltipped" data-position="top" data-delay="50" data-tooltip="Vacaciones">V</a>';
                        }
                        if (row.estatus==3) {
                            html = '<a class="b-btn b-btn-success tooltipped" data-position="top" data-delay="50" data-tooltip="Incapacidad">V</a>';
                        }
                        if (row.estatus==4) {
                            html = '<a class="b-btn b-btn-success tooltipped" data-position="top" data-delay="50" data-tooltip="Retardo">V</a>';
                        }
                        if (row.tipo_av==1) {
                            html = '<a class="b-btn b-btn-success tooltipped" data-position="top" data-delay="50" data-tooltip="Vacaciones">V</a>';
                        }
                        if (row.tipo_av==2) {
                            html = '<a class="b-btn b-btn-success tooltipped" data-position="top" data-delay="50" data-tooltip="Incapacidad">V</a>';
                        }

                    return html;
                }
            },
            {"data": "nombre",
                render:function(data,type,row){
                    var html ='<div class="div-btn-gropu">';
                        var btn_falta='<a class="b-btn b-btn-primary tooltipped" data-position="top" data-delay="50" data-tooltip="Especificación de falta" title="Especificación de falta" onclick="espefalta('+row.id+','+row.personalId+')"><i class="fas fa-cog"></i></a> ';
                        if(numeroDia>0){
                            if(row.fecha==null){
                                html += btn_falta;
                            }else{
                                if(row.fecha==fechahoy){
                                    if(horahoy>=17){
                                        if(row.entrada_salida<2){
                                            html += btn_falta;
                                        }
                                    }
                                }else{
                                    if(row.latitud_salida<2){
                                            html += btn_falta;
                                    }
                                }
                            }
                        }
                        if(row.entrada_salida>0){
                            html+='<a class="b-btn b-btn-primary editarhorario_'+row.id+'"  title="Editar Hora" onclick="editarhorario('+row.id+','+row.personalId+')" data-he="'+row.hora_entrada+'" data-hs="'+row.hora_salida+'"><i class="fas fa-clock"></i></a> ';
                            html+='<a class="b-btn b-btn-primary"  title="Eliminar registro" onclick="deleteentrada('+row.id+')"><i class="fas fa-trash"></i></a> ';
                        }else{
                            html+='<a class="b-btn b-btn-primary"  title="Registrar entrada" onclick="darentrada('+row.personalId+')"><i class="fas fa-user-clock"></i></a> ';
                            
                        }

                        html+='</div>';
                    return html;
                }
            },
            
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[25, 50, 100,200,400,-1], [25, 50,100,200,400,'todo']],
        //"lengthMenu": [[25, 50, 100,200,400], [25, 50,100,200,400]],
        //"searching": false
        //columnDefs: [ { orderable: false, targets: [8,9] }],
        
    }).on('draw',function(){
        
    });
}
function ubicacion(longitud,latitud){
    window.open(base_url+"index.php/Asistencias/ubicacion/"+longitud+"/"+latitud, "Prefactura", "width=880, height=612"); 
}
function espefalta(asi,per){
    var sdia=$('#sdia').val();
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Desea especificar la falta para la fecha '+sdia+'?<br><select class="form-control-bmz" id="selectespefalta"><option value="0">No especificar</option><option value="1">Permiso</option><option value="2">Vacaciones</option><option value="3">Incapacidad</option><option value="4">Retardo</option></select>',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var selectespefalta = $('#selectespefalta option:selected').val();
                if(sdia!=''){
                    $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Asistencias/espefalta",
                        data: {
                            asi:asi,
                            per:per,
                            fecha:sdia,
                            efa:selectespefalta
                        },
                        success:function(response){  
                            toastr["success"]("Cambio Realizado"); 
                            load();
                        }
                    });
                }else{
                    toastr["error"]("Favor de especificar la fecha"); 
                }
                
            },
            cancelar: function () 
            {
                
            }
        }
    });
}
function darentrada(idpersonal){
    var fechahoy=$('#sdia').val();
    var html='<div style="width: 99%;">¿Desea Registrar entrada del personal seleccionado?<br>';
        html+='Se necesita permisos de administrador<br>';
        html+='<input type="password" id="pass_cambio" class="form-control-bmz"><label>Hora de entrada</label>';
        html+='<input type="time" id="new_hora_e" class="form-control-bmz"> </div>';
    $.confirm({
        boxWidth: '40%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Registrar entrada',
        content: html,
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var horae=$('#new_hora_e').val();
                var pass=$('#pass_cambio').val();
                $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Asistencias/darentrada",
                    data: {
                        idp:idpersonal,
                        horae:horae,
                        fechahoy:fechahoy,
                        pass:pass
                    },
                    success: function (response){
                        if(parseInt(response)==1){
                            toastr["success"]("Cambio Realizado");  
                            load();  
                        }else{
                            toastr["error"]("No se tiene permiso"); 
                        }
                        
                    },
                    error: function(response){
                        toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema"); 
                         
                    }
                }); 
            },
            cancelar: function (){
                
            }
        }
    });
    
}
function editarhorario(id,idpersonal,he,hs){
    var fechahoy=$('#sdia').val();
    var html='<div style="width: 99%;" class="row">¿Desea Editar el horario registrado?<br>';
        html+='Se necesita permisos de administrador<br>';
        html+='<input type="text" id="pass_cambio" name="pass_cambio" class="form-control-bmz">';
        html+='<label>Hora de Entrada *</label><input type="time" id="new_hora_e" value="'+he+'" class="form-control-bmz">';
        html+='<label>Hora de Salida</label><input type="time" id="new_hora_s" value="'+hs+'" class="form-control-bmz"> </div>';
    $.confirm({
        boxWidth: '40%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Editar Registro',
        content: html,
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var horae=$('#new_hora_e').val();
                var horas=$('#new_hora_s').val();
                //var pass=$('#pass_cambio').val();
                var pass=$("input[name=pass_cambio]").val();
                if(horae!=''){
                    $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Asistencias/editarhorario",
                        data: {
                            id:id,
                            horae:horae,
                            horas:horas,
                            pass:pass
                        },
                        success: function (response){
                            if(parseInt(response)==1){
                                toastr["success"]("Cambio Realizado");  
                                load();  
                            }else{
                                toastr["error"]("No se tiene permiso"); 
                            }
                            
                        },
                        error: function(response){
                            toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema"); 
                             
                        }
                    }); 
                }else{
                    toastr["error"]("Especificar la hora de entrada"); 
                }
                
            },
            cancelar: function (){
                
            }
        }
    });
    setTimeout(function(){
            new MaskedPassword(document.getElementById("pass_cambio"), '\u25CF');
        },2000);
}
function load_calendario(){
    
    $('#calendario').fullCalendar({
        defaultView: 'month',
        header: {
          left: 'prev,next today',
          center: 'title',
          right: 'month,agendaWeek,agendaDay,resourceTimelineDay',
        },
        
        events: function(start, end, timezone, callback) {
           console.log(start);
            $.ajax({
                url: base_url+'Asistencias/liscalendario',
                dataType: 'json',
                type:'POST',
                data: {
                    start: start.unix(),
                    end: end.unix()
                },
                success: function(doc) {
                    
                    var datos=doc;
                    var events = [];
                    datos.forEach(function(r){
                        //console.log(r);
                        if(r.tipo==1){
                            var color='#ff0000';
                        }else{
                            var color='#00bcd4';
                        }
                        events.push({
                            asignacionId: r.id,
                            title: r.nombre+' '+r.apellido_materno+' '+r.apellido_paterno,
                            start: r.fechainicio+'T08:00:00',
                            end: r.fechafin+'T19:59:59',
                            
                            color:color,
                            eventBackgroundColor:color 
                        });
                        
                    });
                    callback(events);
                }
            });
        },
        eventRender: function(event, element){ 
            /*
            element.dblclick(function() {
                console.log(event.asignacion+'_'+event.tipo+'_'+event.calfecha);
                obternerdatosservicio(event.asignacion,event.tipo,event.calfecha);

                $('#modalinfocalendario').modal();
                $('#modalinfocalendario').modal('open');
                re_agendar_c();
                notificacionsercot();
            });
            if(event.confimardo==1) {
                element.find(".fc-title").prepend('<i class="far fa-check-circle"></i> ');
           }
            console.log(event.status);
            if(event.status==2) {
                element.find(".fc-title").prepend('<i class="fas fa-clipboard-check fa-2x colordeiconofinalizado"></i> ');
            }
            if(event.status==1) {
                element.find(".fc-title").prepend('<i class="fas fa-sync fa-spin"></i> ');
            }
            if(event.nr==1) {
                element.find(".fc-title").prepend('<i class="fas fa-times-circle fa-fw"></i> ');
            }
            var elementtop='Fecha: '+event.fecha+'<br>Horario: '+event.horas+'<br>Tecnico: '+event.tecnico+'<br>'+event.poliza+'<br>'+event.tserviciomotivo;
            Tipped.create(element, elementtop, {
                          title: event.title,
                          size: "large",
                          skin: "light",
                        });
            */
        }
    });
}
function cargavaca(){
    //$('#calendario').fullCalendar( 'refetchEvents' )
    setTimeout(function(){ 
                        load_calendario();
                    }, 1000);
    loadtable();
}

function loadtable(){
    tablef.destroy();
    tablef = $('#tableasistencia').DataTable({
        //stateSave: true,
        "ordering": false,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url+"index.php/Asistencias/getlistvacaciones",
            type: "post",
            "data": {
                'personal':0
            },
        },
        "columns": [
            {"data": "id"},
            {"data": "idpersonal",
                render:function(data,type,row){
                    var html ='';
                        
                        html=row.nombre+' '+row.apellido_paterno+' '+row.apellido_materno;
                    return html;
                }
            },
            {"data": "fechainicio"},
            {"data": "fechafin"},
            {"data": "fechapermitida"},
            {"data": "tipo",
                render:function(data,type,row){
                    var html ='';
                    if(row.tipo==1){
                        html='<div class="task-cat red accent-2">Vacaciones</div>';
                    }
                    if(row.tipo==2){
                        html='<div class="task-cat cyan">Incapacidad</div>';
                    }
                    if(row.tipo==3){
                        html='<div class="task-cat grey">Solicitud Vacaciones</div>';
                        if(row.aceptada_rechzada==0){
                            html='<div class="task-cat orange">Solicitud rechazada</div>';
                        }
                    }
                    return html;
                }
            },
            {"data": null,
                render:function(data,type,row){
                    var html='<div class="group-btns">';
                        html+='<button class="b-btn b-btn-success vaca_'+row.id+'" data-fi="'+row.fechainicio+'" data-ff="'+row.fechafin+'" data-fp="'+row.fechapermitida+'" onClick="editarvc('+row.id+')"><i class="fas fa-edit"></i></button>';
                        
                        if(row.tipo==3){
                            html+='<button class="b-btn b-btn-primary" onClick="confirmassolicitud('+row.id+')" title="Autorizar / Rechazar"><i class="fas fa-vote-yea"></i></button>';
                        }
                        html+='<button class="b-btn b-btn-danger" onClick="deletevc('+row.id+')"><i class="fas fa-trash"></i></button>';
                        html+='</div>';
                    return html;
                }
            }
        ],
        //"order": [[ 0, "desc" ]],
        "lengthMenu": [[20,25, 50, 100,200,400,-1], [20,25, 50,100,200,400,'todo']],
        //"lengthMenu": [[25, 50, 100,200,400], [25, 50,100,200,400]],
        //"searching": false
        //columnDefs: [ { orderable: false, targets: [8,7] }],//desabilita el poder ordenar la columna
        
    }).on('draw',function(){
        
    });
}
function editarvc(idrow){
    var fi=$('.vaca_'+idrow).data('fi');
    var ff=$('.vaca_'+idrow).data('ff');
    var fp=$('.vaca_'+idrow).data('fp');

    var html='<div style="width: 99%;" class="row">¿Desea Editar la fecha?<br>';
        html+='<label>Fecha Inicio</label><input type="date" id="new_fecha_i" value="'+fi+'" class="form-control-bmz">';
        html+='<label>Fecha Fin</label><input type="date" id="new_fecha_f" value="'+ff+'" class="form-control-bmz"> ';
        html+='<label>Fecha Permitida</label><input type="date" id="new_fecha_p" value="'+fp+'" class="form-control-bmz"> </div>';
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: html,
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var nfi=$('#new_fecha_i').val();
                var nff=$('#new_fecha_f').val();
                var nfp=$('#new_fecha_p').val();
                $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Asistencias/editarfechavaca",
                        data: {
                            id:idrow,
                            nfi:nfi,
                            nff:nff,
                            nfp:nfp
                        },
                        success: function (response){
                            toastr["success"]("Cambio Realizado");  
                            loadtable();  
                            $('#calendario').fullCalendar( 'refetchEvents' );
                        },
                        error: function(response){
                            toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema"); 
                             
                        }
                    }); 
            },
            cancelar: function (){
                
            }
        }
    });
}
function deletevc(idrow){
    var html='<div style="width: 99%;" class="row">¿Desea eliminar el Registro?';
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: html,
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Asistencias/deletevaca",
                        data: {
                            id:idrow,
                        },
                        success: function (response){
                            toastr["success"]("Cambio Realizado");  
                            loadtable();  
                            $('#calendario').fullCalendar( 'refetchEvents' );
                        },
                        error: function(response){
                            toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema"); 
                             
                        }
                    }); 
            },
            cancelar: function (){
                
            }
        }
    });
}
function deleteentrada(idrow){

}
function deleteentrada(idrow){
    var bodegastext=$('#bodegaselect_none').html();
    a_confirm=$.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: 'Confirma la eliminacion de la asistencia<br>'+
                 'Para continuar se necesita permisos de administrador<br>'+
                 '<input type="text" placeholder="Contraseña" id="contrasena" name="contrasena" class="name form-control"  maxlength="10"/>' ,
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var pass=$("input[name=contrasena]").val();
                 $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Asistencias/deleteentrada",
                    data: {
                        pass:pass,
                        id:idrow
                    },
                    success: function (data){
                        if (data==1) {
                            swal("Hecho!", "Realizado", "success");
                            //setTimeout(function(){ location.reload();  }, 3000);
                            setTimeout(function(){ load();  }, 3000);
                        }else{
                            swal("Error", "No tiene permiso", "error"); 
                        }
                    },
                    error: function(response){
                        $.alert({
                                boxWidth: '30%',
                                useBootstrap: false,
                                title: 'Error!',
                                content: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema'});   
                         
                    }
                });
               
                
            },
            cancelar: function (){
                
            }
        }
    });
    setTimeout(function(){
        new MaskedPassword(document.getElementById("contrasena"), '\u25CF');
    },1000);

}
/*
function passinput(){
    $("#contrasena").change();
    var pass=$('#contrasena').val();
    if(pass==''){
        $('#contrasena').data('val','');
    }
}
*/
function generarreporte(){
    var html='<div>\
                <b>Favor de especificar un rango</b><br>\
                <label>Fecha Inicio</label><input type="date" class="form-control-bmz" id="expor_fechaini" value="'+fechahoy+'">\
                <label>Fecha Final</label><input type="date" class="form-control-bmz" id="expor_fechafin" value="'+fechahoy+'">\
            </div>'
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Generación de reporte',
        content: html ,
        type: 'green',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var fechaini = $('#expor_fechaini').val();
                var fechafin = $('#expor_fechafin').val();
                if(fechaini!=''){
                    if(fechafin!=''){
                        window.open(base_url+'Asistencias/exportar?finicio='+fechaini+'&ffin='+fechafin, '_blank');
                    }else{
                        toastr["error"]("Especifique una fecha final"); 
                    }
                }else{
                    toastr["error"]("Especifique una fecha inicial"); 
                }
               
                
            },
            cancelar: function (){
                
            }
        }
    });
}
function loadfhp(){
    tablefhp.destroy();
    tablefhp = $('#table_permisosfuera').DataTable({
        //stateSave: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url+"index.php/Asistencias/getlistpermisosfuera",
            type: "post",
            "data": {
                'personal':0
            },
        },
        "columns": [
            {"data": "id"},
            {"data": "personalid",
                render:function(data,type,row){
                    var html ='';
                        
                        html=row.nombre+' '+row.apellido_paterno+' '+row.apellido_materno;
                    return html;
                }
            },
            {"data": "fecha"},
            {"data": "reg"},
            
            {"data": null,
                render:function(data,type,row){
                    var html='';
                        html+='<button class="b-btn b-btn-danger" onClick="deletefhp('+row.id+')"><i class="fas fa-trash"></i></button>';
                    return html;
                }
            }
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[20,25, 50, 100,200,400,-1], [20,25, 50,100,200,400,'todo']],
        //"lengthMenu": [[25, 50, 100,200,400], [25, 50,100,200,400]],
        //"searching": false
        //columnDefs: [ { orderable: false, targets: [8,7] }],//desabilita el poder ordenar la columna
        
    }).on('draw',function(){
        
    });
}
function deletefhp(idrow){
    var html='<div style="width: 99%;" class="row">¿Desea eliminar el Registro?';
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: html,
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Asistencias/deletefhp",
                        data: {
                            id:idrow,
                        },
                        success: function (response){
                            toastr["success"]("Cambio Realizado");  
                            loadfhp();
                        },
                        error: function(response){
                            toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema"); 
                             
                        }
                    }); 
            },
            cancelar: function (){
                
            }
        }
    });
}
function confirmassolicitud(id){
    var html='';
        html+='Seleccione la opción<br>';
        html+='<select class="form-control-bmz browser-default" id="aut_rech">';
            html+='<option value="0">Seleccione</option>';
            html+='<option value="1">Autorizar</option>';
            html+='<option value="2">Rechazar solicitud</option>';
        html+='</select>';
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Autorizar / Rechazar',
        content: html,
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var aut_rech = $('#aut_rech option:selected').val();
                $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Asistencias/confirmassolicitud",
                        data: {
                            id:id,
                            autrech:aut_rech
                        },
                        success: function (response){
                            toastr["success"]("Cambio Realizado");  
                            loadtable();
                            $('#calendario').fullCalendar( 'refetchEvents' );
                        },
                        error: function(response){
                            toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema"); 
                             
                        }
                    }); 
            },
            cancelar: function (){
                
            }
        }
    });
}