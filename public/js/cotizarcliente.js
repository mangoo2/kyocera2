var base_url = $('#base_url').val();
var editor; 


$(document).ready(function(){  

    editor = new $.fn.dataTable.Editor( {
            ajax: base_url+"index.php/Cotizaciones/actualizaDetallePoliza",
            table: "#tablaBordesPrincipal",
            fields: [ {
                    label: "Cobertura:",
                    name: "cubre"
                },
                {
                    label: "Vigencia:",
                    name: "vigencia_meses"
                }
            ]
        } );

    table = $('#tablaBordesPrincipal').DataTable({
      "dom": 't',
      "ordering": false
    });

    // Activa la edición por casilla en cada ROW de la tabla
    $('#tablaBordesPrincipal').on( 'click', 'tbody td:not(:first-child)', function (e){
        editor.inline(this);
    });


      // Basic
    $('.dropify').dropify({
        messages: {
            default: 'Arrastra y suelta un archivo aquí o haz clic',
            replace: 'Arrastre y suelte un archivo o haga clic para reemplazar',
            remove:  'Eliminar',
            error:  'Lo sentimos, el archivo es demasiado grande.'
        }
    });
    // Used events
    var drEvent = $('.dropify-event').dropify();
    drEvent.on('dropify.beforeClear', function(event, element){
        return confirm("Do you really want to delete \"" + element.filename + "\" ?");
    });
    drEvent.on('dropify.afterClear', function(event, element){
        alert('File deleted');
    });

    $('.edit').editable();


});
function btn_imprecion(){
    var id = $('#idCliente').val();
    var tipo = $('#tipo').val();
    var cotiza = $('#cotizar').val();
    window.location = base_url+'index.php/Clientes/documento/'+id+'/'+tipo+'/'+cotiza;
}
function btn_guardar(){
    $('.print_css').append('.cot{color: red}');
    $('.pantalla').remove();
    $('.usuario').remove();
    $('.menu').remove();
    $('.kyoc').remove();
    $('.log').append("<img class='kyoc' src='"+base_url+"app-assets/images/altakyo.png' alt='materialize logo' width='200px;' height='100px;'>");
    $('.venta_text').append("<h6 align='center' style='font-size:15px; color:black;'>Venta-Renta-Mantenimiento<br>Impresoras y Multifuncionales monocromáticos y color<br> Blvd. Norte No. 18831, San Alejandro, CP 72090, Puebla, Pue.<br> Call Center: 273 3400, 249 5393<br> Kyoceraap.com</h6>");
   // $('.venta_img').append("<img class='kyoc' src='"+base_url+"app-assets/images/1024_kyocera_logo.png' alt='materialize logo' width='200px;' height='80px;'>");
    $('.btn_imp').remove();
    setTimeout(function() {window.print();}, 500);
    setTimeout(function() {location.reload();}, 5000);
}

  
  function selectcotiza(){
    var selection=$('#cotizar option:selected').val(); 
      if(selection==1){
        $('.select_familia').css('display','none');
      }else if(selection==2){
        $('.select_familia').css('display','inline');
      }
}

