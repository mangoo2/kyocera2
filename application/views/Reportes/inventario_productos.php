<style type="text/css">
  .selecte, .selectr, .selecta, .selectc{
    display: none;
  }
</style>
<!-- START CONTENT -->
    <section id="content">
      <!--start container-->
        <div class="container">
          <div class="section">

          <input id="base_url" type="hidden" name="base_url" value="<?php echo base_url(); ?>">

          <div id="table-datatables">
            <br>
                <h4 class="header">Inventario Productos</h4>
                <div class="row">
                  <div class="col s12">
                    <div class="card-panel">
                      <div class="row">
                        <div class="col s2">
                          <label>Tipo</label>
                          <select class="browser-default form-control-bmz" id="tipop" onchange="tipopselec()">
                            <option value="0">Todos</option>
                            <option value="1">Equipos</option>
                            <option value="2">Refacciones</option>
                            <option value="3">Accesorios</option>
                            <option value="4">Consumibles</option>
                            

                          </select>
                        </div>
                        <div class="col s2">
                          <label>Bodega</label>
                          <select class="browser-default form-control-bmz" id="bodega">
                            <option value="0">Todos</option>
                            <?php 
                              foreach ($resultbodega->result() as $itemb) {
                                echo '<option value="'.$itemb->bodegaId.'">'.$itemb->bodega.'</option>';
                              }
                            ?>
                            

                          </select>
                        </div>
                        <div class="col s2 selecte">
                          <label>Equipos</label>
                          <select class="browser-default form-control-bmz" id="selecte"></select>
                        </div>
                        <div class="col s2 selectr">
                          <label>Refacciones</label>
                          <select class="browser-default form-control-bmz" id="selectr"></select>
                        </div>
                        <div class="col s2 selecta">
                          <label>Accesorios</label>
                          <select class="browser-default form-control-bmz" id="selecta"></select>
                        </div>
                        <div class="col s2 selectc">
                          <label>Consumibles</label>
                          <select class="browser-default form-control-bmz" id="selectc"></select>
                        </div>
                        <div class="col s2 mostrar_serie">
                          <td class="dtr-control"><input type="checkbox" class="filled-in" id="mostrar_serie"  ><label for="mostrar_serie"> Mostrar serie</label></td>
                          
                        </div>
                        
                        <div class="col s2">
                          <button class="btn-bmz cyan waves-effect waves-light" type="button" onclick="generarreporte()">Generar</button>
                        </div>
                      </div>
                    </div>
                  </div> 
                      
                </div>
                   
              </div>
            </div>  
               </div>
        </section>

