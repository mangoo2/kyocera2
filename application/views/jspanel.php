<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/chat/chartist-js/chartist.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/chat/vendors/chartist-js/chartist-plugin-tooltip.css">
<script src="<?php echo base_url(); ?>app-assets/chat/vendors/chartjs/chart.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>app-assets/chat/vendors/chartist-js/chartist.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>app-assets/chat/vendors/chartist-js/chartist-plugin-tooltip.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>app-assets/chat/vendors/chartist-js/chartist-plugin-fill-donut.min.js" type="text/javascript"></script>
<link href="<?php echo base_url(); ?>public/plugins/select2/select2.min.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/select2/select2.full.min.js"></script>
<script type="text/javascript">
	
    var CurrentBalanceDonutChart = new Chartist.Pie(
        "#current-balance-donut-chart",
        {
            labels: [1, 2],
            series: [{ meta: "Completed", value: 80 }, { meta: "Remaining", value: 20 }]
        },
        {
            donut: true,
            donutWidth: 8,
            showLabel: false,
            plugins: [
                Chartist.plugins.tooltip({ class: "current-balance-tooltip", appendToBody: true }),
                Chartist.plugins.fillDonut({
                    items: [
                        {
                            content: '<p class="small">Balance</p><h5 class="mt-0 mb-0">100%</h5>'
                        }
                    ]
                })
            ]
        }
    );

</script>