<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Listado_unidades extends CI_Controller {
    public function __construct()     {
        parent::__construct();
        $this->load->model('ModeloCatalogos');
        $this->load->model('Configuraciones_model');
        $this->load->model('login_model');
        $this->load->model('Modelunidad');
        $this->load->model('ModeloAsignacion');
        $this->load->helper('url');
        $this->load->library('menus');
        $this->submenu=70;
        if($this->session->userdata('logeado')==true){
            $this->perfilid =$this->session->userdata('perfilid');
            $this->idpersonal = $this->session->userdata('idpersonal');
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,$this->submenu);// 43 es el id del submenu
            if ($permiso==0) {
                //redirect('Login');
            }
        }else{
            redirect('login');
        }
    }

    // Listado de Precios de Equipos
    function index(){
        $data['MenusubId']=$this->submenu;
        $data['perfilid']=$this->perfilid;
        $data['resultstec']=$this->Configuraciones_model->view_session_searchTecnico();
        $data['resultsuni']=$this->ModeloCatalogos->getselectwheren('unidades',array('activo'=>1));
        // Cargamos las vistas
        $this->load->view('header');
        $this->load->view('main',$data);
        $this->load->view('unidades/index');
        $this->load->view('unidades/indexjs');
        $this->load->view('footer');    
    }

    function registro($id=0){  
        $data['MenusubId']=$this->submenu; 
        if($id==0){
            $data['label']='Formulario de Alta de unidad.';
            $data['id']=0;
            $data['modelo']='';
            $data['anio']='';
            $data['marca']='';
            $data['file']='';
            $data['num_serie']='';
            $data['poliza_seguro']='';
            $data['kilometraje']='';
            $data['proximo_servicio']='';
            $data['vigencia_poliza_seguro']='';
            $data['placas']='';
            $data['tipo_unidad']='';
            $data['tipo_unidad_texto']='';
            $data['vigencia_contrato']='';
            $data['activo']='';
            $data['reg']='';
            $data['idpersonal']='';
            $data['file_poliza']='';
            $data['tecnico_asignado']=0;
        }else{
            $data['label']='Formulario de Edición de unidad.';
            $result=$this->ModeloCatalogos->getselectwheren('unidades',array('id'=>$id));
            foreach ($result->result() as $x){
                $data['id']=$x->id;
                $data['modelo']=$x->modelo;
                $data['anio']=$x->anio;
                $data['marca']=$x->marca;
                $data['file']=$x->file;
                $data['num_serie']=$x->num_serie;
                $data['poliza_seguro']=$x->poliza_seguro;
                $data['kilometraje']=$x->kilometraje;
                $data['proximo_servicio']=$x->proximo_servicio;
                $data['vigencia_poliza_seguro']=$x->vigencia_poliza_seguro;
                $data['placas']=$x->placas;
                $data['tipo_unidad']=$x->tipo_unidad;
                $data['tipo_unidad_texto']=$x->tipo_unidad_texto;
                $data['vigencia_contrato']=$x->vigencia_contrato;
                $data['activo']=$x->activo;
                $data['reg']=$x->reg;
                $data['idpersonal']=$x->idpersonal;
                $data['file_poliza']=$x->file_poliza;
                $data['tecnico_asignado']=$x->tecnico_asignado;
            }
        }
        $data['perfilid']=$this->perfilid;
        $data['resultstec']=$this->Configuraciones_model->view_session_searchTecnico(); 
        $this->load->view('header');
        $this->load->view('main',$data);
        $this->load->view('unidades/form',$data);
        $this->load->view('footer');
        $this->load->view('unidades/formjs');
    }

    public function insertaActualiza(){
        $data = $this->input->post();
        $id = $data['id']; 
        $data['idpersonal']=$this->idpersonal;
        if($id!=0){
            $idarray = array('id'=>$id);
            $this->ModeloCatalogos->updateCatalogo('unidades',$data,$idarray);
        }else{
            $id=$this->ModeloCatalogos->Insert('unidades',$data);
        }   
        $databit=array(
                        'unidadid'=>$id,
                        'tecnico'=>$data['tecnico_asignado'],
                        'kilometraje'=>$data['kilometraje'],
                        'personalId'=>$this->idpersonal
                    );
        $this->ModeloCatalogos->Insert('unidades_bitacora_tecnico',$databit);
        echo $id;
    }

    function cargafiles(){
        $idunidad = $this->input->post('idunidad');
        $upload_folder ='uploads/unidades';
        $nombre_archivo = $_FILES['foto']['name'];
        $tipo_archivo = $_FILES['foto']['type'];
        $tamano_archivo = $_FILES['foto']['size'];
        $tmp_archivo = $_FILES['foto']['tmp_name'];
        //$archivador = $upload_folder . '/' . $nombre_archivo;
        $fecha=date('ymd-His');
        $newfile='r_-'.$fecha.'-'.$nombre_archivo;        
        $archivador = $upload_folder . '/'.$newfile;
        if (!move_uploaded_file($tmp_archivo, $archivador)) {
            $return = Array('ok' => FALSE, 'msg' => 'Ocurrió un error al subir el archivo. No pudo guardarse.', 'status' => 'error');
        }else{
            $arrayfoto = array('file'=>$newfile);
            $idarray = array('id'=>$idunidad);
            $this->ModeloCatalogos->updateCatalogo('unidades',$arrayfoto,$idarray);
            $return = Array('ok'=>TRUE);
        }
        echo json_encode($return);
    }

    function eliminarregistro(){
        $id = $this->input->post('id');
        $data = array('activo'=>0);
        $this->ModeloCatalogos->updateCatalogo('unidades',$data,array('id'=>$id));
        echo $id;
    }

    public function getlistasunidades() {
        $params = $this->input->post();
        $getdata = $this->Modelunidad->get_listado($params);
        $totaldata= $this->Modelunidad->total_listado($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    
    function cargafiles_poliza(){
        $idunidad = $this->input->post('idunidad');
        $upload_folder ='uploads/unidades_poliza';
        $nombre_archivo = $_FILES['foto']['name'];
        $tipo_archivo = $_FILES['foto']['type'];
        $tamano_archivo = $_FILES['foto']['size'];
        $tmp_archivo = $_FILES['foto']['tmp_name'];
        //$archivador = $upload_folder . '/' . $nombre_archivo;
        $fecha=date('ymd-His');
        $newfile='r_-'.$fecha.'-'.$nombre_archivo;        
        $archivador = $upload_folder . '/'.$newfile;
        if (!move_uploaded_file($tmp_archivo, $archivador)) {
            $return = Array('ok' => FALSE, 'msg' => 'Ocurrió un error al subir el archivo. No pudo guardarse.', 'status' => 'error');
        }else{
            $arrayfoto = array('file_poliza'=>$newfile);
            $idarray = array('id'=>$idunidad);
            $this->ModeloCatalogos->updateCatalogo('unidades',$arrayfoto,$idarray);
            $return = Array('ok'=>TRUE);
        }
        echo json_encode($return);
    }
    function reg_incidencias(){
        $unidadid=$_POST['id'];
        $detalle=$_POST['detalle'];
        //======================================
            $result=$this->ModeloCatalogos->getselectwheren('unidades',array('id'=>$unidadid));
            $tecnico=0;
            foreach ($result->result() as $item) {
                $tecnico=$item->tecnico_asignado;
            }
        //======================================
        $data = [];
        $output = [];
        if(isset($_FILES['files'])){
            $count = count($_FILES['files']['name']);
            
            for($i=0;$i<$count;$i++){
            
                if(!empty($_FILES['files']['name'][$i])){
            
                  $_FILES['file']['name'] = $_FILES['files']['name'][$i];
                  $_FILES['file']['type'] = $_FILES['files']['type'][$i];
                  $_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
                  $_FILES['file']['error'] = $_FILES['files']['error'][$i];
                  $_FILES['file']['size'] = $_FILES['files']['size'][$i];
                  $DIR_SUC=FCPATH.'uploads/unidades';

                  $config['upload_path'] = $DIR_SUC; 
                  //$config['allowed_types'] = 'docx|xlsx|pdf|doc|xls|pptx';
                  $config['allowed_types'] = '*';

                  //$config['allowed_types'] = 'docx|xlsx|pdf|jpg|jpeg|png|gif';
                  $config['max_size'] = 5000;
                  $file_names=date('Ymd_His').'_'.rand(0, 500);
                  //$config['file_name'] = $file_names.'_'.str_replace(' ', '_', $_FILES['files']['name'][$i]);
                  $config['file_name'] = $file_names;
                  //$config['file_name'] = $file_names;
           
                  $this->load->library('upload',$config); 
            
                  if($this->upload->do_upload('file')){
                    $uploadData = $this->upload->data();
                    $filename = $uploadData['file_name'];
                    $filenametype = $uploadData['file_ext'];
           
                    $data['totalFiles'][] = $filename;
                    //$this->ModeloGeneral->tabla_inserta('equipo_imgs',array('idequipo'=>$idequipo,'imagen'=>$filename,'filetype'=>$filenametype,'usuario'=>$this->idpersonal));
                    $this->ModeloCatalogos->Insert('unidades_bitacora_incidentes',array('unidadid'=>$unidadid,'tecnico_responsable'=>$tecnico,'personalId'=>$this->idpersonal,'detalle'=>$detalle,'evidencia'=>$filename));
                  }else{
                    $data = array('error' => $this->upload->display_errors());
                    $output=$data;
                    log_message('error', json_encode($data)); 
                  }
                }
           
            }
        }else{
            $count=0;   
        }
        if($count==0){
            $this->ModeloCatalogos->Insert('unidades_bitacora_incidentes',array('unidadid'=>$unidadid,'tecnico_responsable'=>$tecnico,'personalId'=>$this->idpersonal,'detalle'=>$detalle,'evidencia'=>''));
        }
        echo json_encode($output);
    }
    function lis_incidencias(){
        $params = $this->input->post();
        $id=$params['id'];
        //$fecha=$params['fecha'];
        $html='<table id="table_inci"><thead><tr>
                <th>#</th>
                <th>Unidad</th>
                <th></th>
                <th>Detalle</th>
                <th>Tecnico Responsable</th>
                <th>Fecha</th>
                </tr></thead><tbody>';
            $result = $this->ModeloCatalogos->get_detalles_incidentes($id);
            foreach ($result->result() as $item) {
                $img='';
                if($item->evidencia!=''){
                    $img='<a href="'.base_url().'uploads/unidades/'.$item->evidencia.'" class="btn-bmz blue gradient-shadow" target="_black"><i class="fas fa-clipboard-list fa-fw"></i></a>';
                }
                $html.='<tr><td>'.$item->id.'</td><td>'.$item->modelo.'</td><td>'.$img.'</td><td>'.$item->detalle.'</td><td>'.$item->nombre.' '.$item->apellido_paterno.'</td><td>'.$item->reg.'</td></tr>';
            }
        $html.='</tbody></table>';
        echo $html;
    }
    function lis_itinerario(){
        $params = $this->input->post();
        $id=$params['id'];
        $iduactual=$id;
        $fecha=$params['fecha'];
        $idpersonal=$params['idpersonal'];
        $fechaactual=date('Y-m-d');
        $html='<table id="table_itinerario"><thead><tr><th>Descripcion</th><th>Hora inicio</th><!--<th></th>--><th>KM final</th><th>Fecha</th><th></th></tr></thead><tbody>';

        $caract   = array('|','°');
        $caract2  = array('<a class="vinc_c b-btn b-btn-primary btn-sm" onclick="obternerdatosservicio(',')"><i class="fas fa-info-circle infoicon"></i></a> '); 
            $result = $this->ModeloCatalogos->get_detalles_itinerario($idpersonal,$fecha);
            foreach ($result->result() as $item) {
                $rest_dir = $this->ModeloAsignacion->otenerdireccion($item->asignacion,$item->tipo_asignacion);
                    $btn_coordenadas='';
                    if($rest_dir['idrow']!='0'){
                        if($rest_dir['longitud']>0 || $rest_dir['latitud']>0){
                            $btn_coordenadas='<a class="b-btn b-btn-primary" onclick="view_coor('.$rest_dir['longitud'].','.$rest_dir['latitud'].')"><i class="fas fa-map-marked-alt"></i></a>';
                        }else{
                            $idrowbtn="'".$rest_dir['idrow']."'";
                            $btn_coordenadas='<a class="b-btn b-btn-primary" onclick="savecoor('.$idrowbtn.')"><i class="fas fa-map-marker"></i></a>';
                        }    
                    }
                    $btn_rechazo='';
                    if($fecha==$fechaactual){
                        $btn_rechazo=' <a class="b-btn b-btn-danger" onclick="notificacionrechazogeneral('.$item->asignacion.','.$item->tipo_asignacion.')"><i class="fas fa-ban"></i></a>';
                    }
                    if($item->nr==1){
                        $btn_rechazo=' <a class="b-btn b-btn-danger">Rechazado</a>';
                    }
                    if($item->tipoagregado==1){
                        $new_servicio='new_servicio';
                        $new_servicio_t=' title="Pendiente de ajustar" ';
                        $accion_block='x';
                    }else{
                        $new_servicio='';
                        $new_servicio_t='';
                        $accion_block=' readonly  onclick="primiso_desbloqueo('.$item->id.') " ';
                    }
                $html.='<tr class="tr_itinerario '.$new_servicio.'" '.$new_servicio_t.'>
                            <td>'.$item->descripcion.'<br><span class="iti_dir">'.$rest_dir['direccion'].'</span><br>'.$btn_coordenadas.''.str_replace($caract, $caract2, $item->vinc).'</td>
                            <td>';
                    if($fecha==$fechaactual){
                        $html.='<input type="time" class="form-control-bmz in_hora_i xxx in_hora in_hora_i_'.$item->id.'" id="in_hora_i_'.$item->id.'" name="in_hora_i_'.$item->id.'" 
                                min="07:00:00"
                                max="'.$item->hora_limite.'" 
                                data-cli="'.$item->cli.'" 
                                data-row="'.$item->id.'" 
                                data-horacomida="'.$item->hora_comida.'"
                                data-horafincomida="'.$item->horafin_comida.'"
                                data-horafin="'.$item->hora_fin.'"
                                oninput="editar_hora('.$item->id.','.$item->unidadid.','.$iduactual.')" value="'.$item->hora_inicio.'" '.$accion_block.'><span style="color:transparent">'.$item->tipoagregado.' '.$item->hora_inicio.'</span>';
                    }else{
                        $html.=$item->hora_inicio;
                    }
                    
                $html.='</td>';
                $html.='<!--<td>';
                    if($fecha==$fechaactual){
                        $html.='<input type="time" class="form-control-bmz in_hora_f in_hora in_hora_f_'.$item->id.'" oninput="editar_hora('.$item->id.','.$item->unidadid.','.$iduactual.')" value="'.$item->hora_fin.'"><span style="color:transparent">'.$item->hora_fin.'</span>';
                    }else{
                        //$html.='<!--'.$item->hora_fin.'-->';
                    }
                
                $html.='</td>-->
                       <td>';
                    if($fecha==$fechaactual){
                        if($item->unidadid>0){
                            $unidadid=$item->unidadid;
                        }else{
                            $unidadid=0;
                        }
                        $html.='<input type="number" class="form-control-bmz in_hora in_km_f_'.$item->id.'" oninput="editar_km('.$item->id.','.$unidadid.','.$iduactual.')" value="'.$item->km_fin.'"><span style="color:transparent">'.$item->km_fin.'</span>';
                    }else{
                        $html.=$item->km_fin;
                    }
                
                $html.='</td>
                        <td>'.$item->fecha.'</td>
                        <!--<td>'.$item->km_fin.'</td>-->
                        <td>
                            <!--<a onclick="edititinerario('.$item->id.')" class="btn-bmz blue gradient-shadow ocutarfechadiferente edititinerario_'.$item->id.'" data-des="'.$item->descripcion.'" data-hi="'.$item->hora_inicio.'" data-hf="'.$item->hora_fin.'" data-kmf="'.$item->km_fin.'" ><i class="fas fa-pencil-alt"></i></a>-->
                            <!--<a onclick="deleteitiner('.$item->id.')" class="btn-bmz red gradient-shadow ocutarfechadiferente"><i class="fas fa-trash-alt"></i></a>-->
                            '.$btn_rechazo.'
                        </td></tr>';
            }
        $html.='</tbody></table>';
        echo $html;
    }
    function lis_itinerario_gen(){
        $params = $this->input->post();
        $id=$params['id'];
        //$fecha=$params['fecha'];
        $html='<table id="table_itinerario"><thead><tr><th>#</th><th>personal</th><th>Descripcion</th><th>Hora inicio</th><th>Hora fin</th><th>KM final</th><th>Fecha</th><th></th></tr></thead><tbody>';
            $result = $this->ModeloCatalogos->get_detalles_itinerario_get($id);
            foreach ($result->result() as $item) {
                
                $html.='<tr><td>'.$item->id.'</td><td>'.$item->nombre.' '.$item->apellido_paterno.' '.$item->apellido_materno.'</td><td>'.$item->descripcion.'</td><td>'.$item->hora_inicio.'</td><td>'.$item->hora_fin.'</td><td>'.$item->km_fin.'</td><td>'.$item->fecha.'</td>
                        <td>
                            <!--<a onclick="edititinerario('.$item->id.')" class="btn-bmz blue gradient-shadow ocutarfechadiferente edititinerario_'.$item->id.'" data-des="'.$item->descripcion.'" data-hi="'.$item->hora_inicio.'" data-hf="'.$item->hora_fin.'" data-kmf="'.$item->km_fin.'" ><i class="fas fa-pencil-alt"></i></a>
                            <a onclick="deleteitiner('.$item->id.')" class="btn-bmz red gradient-shadow ocutarfechadiferente"><i class="fas fa-trash-alt"></i></a>-->
                        </td></tr>';
            }
        $html.='</tbody></table>';
        echo $html;
    }
    function registraritinerario(){
        $params = $this->input->post();
        //log_message('error',json_encode($params));
        $clientes = $params['clientes'];

        $dataeqarrayinsert=array();
        $DATAc = json_decode($clientes);
        for ($i=0;$i<count($DATAc);$i++) {
            $hora_inicio =$this->ModeloCatalogos->verificarhora($DATAc[$i]->hora_inicio);
            $hora_fin =$this->ModeloCatalogos->verificarhora($DATAc[$i]->hora_fin);
            $fecha=date('Y-m-d');
            $asignacion_gen=$DATAc[$i]->asignaciong;

            $resulti2=$this->ModeloAsignacion->get_detalles_itinerario_validar($DATAc[$i]->personal,$fecha,$asignacion_gen);
            if($resulti2->num_rows()==0){
                
                $r_info_ser=$this->ModeloAsignacion->get_detalles_itinerario_info($fecha,$asignacion_gen);
                foreach ($r_info_ser->result() as $item_ser) {
                    $hora_inicio=$item_ser->hora_inicio;
                }
                //===========================================
                $unidadid=0;
                if(isset($DATAc[$i]->unidadid)){
                    $unidadid=$DATAc[$i]->unidadid;
                }
                $dataeqarrayinsert[]=array(
                                        'unidadid'=>$unidadid,
                                        'descripcion'=>$DATAc[$i]->descripcion,
                                        'cli'=>$DATAc[$i]->cli,
                                        'hora_inicio'=>$hora_inicio,
                                        'hora_fin'=>$hora_fin,
                                        'km_inicio'=>$DATAc[$i]->km_inicio,
                                        'km_fin'=>$DATAc[$i]->km_fin,
                                        'hora_limite'=>$DATAc[$i]->hora_limite,
                                        'fecha'=>$fecha,
                                        'personal'=>$DATAc[$i]->personal,
                                        'asignacion'=>$DATAc[$i]->asignacion,
                                        'asignacion_gen'=>$asignacion_gen,
                                        'tipo_asignacion'=>$DATAc[$i]->tipo_asignacion,
                                        'vinc'=>$DATAc[$i]->vinc,

                );
                $this->registrarkm($unidadid,$DATAc[$i]->km_fin);
            }


        }
        if(count($dataeqarrayinsert)>0){
            $this->ModeloCatalogos->insert_batch('unidades_bitacora_itinerario',$dataeqarrayinsert);
            //$this->ModeloCatalogos->updateCatalogo('personal',array('servicio'=>0),array('personalId'=>$this->idpersonal));//se comento por que los tecnicos tienes que saber cual no cerraron
        }

    }
    function registrarkm($idunidad,$kmf){
        $km_fin = $kmf;
        $unidadid = $idunidad;
        if($km_fin>0){
           $result=$this->ModeloCatalogos->getselectwheren('unidades',array('id'=>$unidadid));
            foreach ($result->result() as $item) {
                if($km_fin>$item->kilometraje){
                    $this->ModeloCatalogos->Insert('unidades_bitacora_tecnico',array('unidadid'=>$unidadid,'tecnico'=>$item->tecnico_asignado,'kilometraje'=>$km_fin,'personalId'=>$this->idpersonal));
                    $this->ModeloCatalogos->updateCatalogo('unidades',array('kilometraje'=>$km_fin),array('id'=>$unidadid));
                }
            } 
        }
    }
    function deleteitiner(){
        $params = $this->input->post();
        $id=$params['id'];
        $this->ModeloCatalogos->updateCatalogo('unidades_bitacora_itinerario',array('activo'=>0),array('id'=>$id));
    }
    function editar_hora(){
        $params = $this->input->post();
        $id=$params['id'];
        $unidadid=$params['idunidadactual'];
        $km_fin=$params['km_fin'];
        unset($params['hora_fin']);
        unset($params['unidadid']);
        unset($params['idunidadactual']);
        unset($params['id']);
        $params['unidadid']=$unidadid;
        $params['tipoagregado']=0;
        $hora_inicio=$this->ModeloCatalogos->verificarhora($params['hora_inicio']);
        $params['hora_inicio']=$hora_inicio;
        $this->ModeloCatalogos->updateCatalogo('unidades_bitacora_itinerario',$params,array('id'=>$id));
        $this->registrarkm($unidadid,$km_fin);
        $this->ModeloCatalogos->updateCatalogo('personal',array('not_new_ser'=>0),array('personalId'=>$this->idpersonal));

        $res_get = $this->ModeloCatalogos->getselectwheren('unidades_bitacora_itinerario',array('id'=>$id));
        foreach ($res_get->result() as $item) {
            $fecha=$item->fecha;
            $asignacion_gen=$item->asignacion_gen;

            $r_info_ser=$this->ModeloAsignacion->get_detalles_itinerario_info($fecha,$asignacion_gen);
            foreach ($r_info_ser->result() as $item_ser) {
                $this->ModeloCatalogos->updateCatalogo('unidades_bitacora_itinerario',array('hora_inicio'=>$hora_inicio),array('id'=>$item_ser->id));
            }
        }

    }
    function editar_km(){
        $params = $this->input->post();
        $id=$params['id'];
        $unidadid=$params['idunidadactual'];
        $km_fin=$params['km_fin'];
        unset($params['idunidadactual']);
        unset($params['id']);
        $params['unidadid']=$unidadid;

        $this->ModeloCatalogos->updateCatalogo('unidades_bitacora_itinerario',$params,array('id'=>$id));
        $this->registrarkm($unidadid,$km_fin);
    }
    /*
    function verificarhora($hora_inicio){
        $hora_h=date('H', strtotime($hora_inicio));//24 horas con ceros iniciales
        $hora_g=date('G', strtotime($hora_inicio));//24 horas sin ceros iniciales
        $hora_i=date('i', strtotime($hora_inicio));//minutos
        switch ($hora_g) {
            case 0:
                $fechanew='12:'.$hora_i;
                break;
            case 1:
                $fechanew='13:'.$hora_i;
                break;
            case 2:
                $fechanew='14:'.$hora_i;
                break;
            case 3:
                $fechanew='15:'.$hora_i;
                break;
            case 4:
                $fechanew='16:'.$hora_i;
                break;
            case 5:
                $fechanew='17:'.$hora_i;
                break;
            case 6:
                $fechanew='18:'.$hora_i;
                break;
            
            default:
                $fechanew=$hora_h.':'.$hora_i;
                break;
        }
        return $fechanew;
    }
    */
    function reporte($eje,$uni,$fini,$ffin){
        //===================================
            header("Pragma: public");
            header("Expires: 0");
            $filename = "Reporte unidades.xls";
            header("Content-type: application/x-msdownload");
            header("Content-Disposition: attachment; filename=$filename");
            header("Pragma: no-cache");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        //===================================
        if($eje>0){
            $w_eje=" and ubi.personal='$eje' ";
        }else{
            $w_eje="";
        }
        if($uni>0){
            $w_uni=" and ubi.unidadid='$uni' ";
        }else{
            $w_uni="";
        }
        if($fini>0){
            $w_fini=" and ubi.reg>'$fini 00:00:00' ";
        }else{
            $w_fini="";
        }
        if($ffin>0){
            $w_ffin=" and ubi.reg<'$ffin 23:59:59' ";
        }else{
            $w_ffin="";
        }

        $strq="SELECT unidadid,modelo,placas,personal,personalId,GROUP_CONCAT(fecha_sin_hora SEPARATOR ' ') as fechas,COUNT(*) as dias FROM(
                SELECT ubi.unidadid,uni.modelo,uni.placas,concat(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno) as personal,per.personalId,ubi.reg,DATE(ubi.reg) as fecha_sin_hora
                FROM unidades_bitacora_itinerario as ubi 
                INNER JOIN unidades as uni on uni.id=ubi.unidadid
                INNER JOIN personal as per on per.personalId=ubi.personal
                WHERE ubi.id>0 $w_eje $w_uni $w_fini $w_ffin
                GROUP BY ubi.unidadid,ubi.personal,fecha_sin_hora
                ) as datos GROUP BY unidadid,personalId";
        $query = $this->db->query($strq);
        $html='';
        $html.='<table border="1">
                <thead>
                    <tr>
                        <th>Unidad</th>
                        <th>Placas</th>
                        <th>Tecnico</th>
                        <th>Fechas</th>
                        <th>Dias</th>
                    </tr>
                </thead><tbody>';
                foreach ($query->result() as $item) {
                    $html.='<tr>
                        <td>'.$item->modelo.'</td>
                        <td>'.$item->placas.'</td>
                        <td>'.utf8_decode($item->personal).'</td>
                        <td>'.$item->fechas.'</td>
                        <td>'.$item->dias.'</td>
                    </tr>';
                }
        $html.='</tbody></table>';


        echo $html;
    }
}