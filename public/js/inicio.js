var table;
var fechahoy = $('#fechahoy').val();
var base_url = $('#base_url').val();
$(document).ready(function($) {
    table = $('#tabla_ventas_incompletas').DataTable();
    load();
    $('#table_pe_fe').DataTable();
});
function load() {
    var cliente= 0;
    var estatus= 0;
    var tipoventa= 5;//1 venta 2 combinada 3 servicios
    var estatusventa= 1;
    var tipoview = 0;
    var idpersonal = $('#ejecutivoselect').val();
    var fechainicial_reg = $('#fechainicial_reg').val();
    var emp=1;
    var filf=0;
    
    // Destruye la tabla y la crea de nuevo con los datos cargados 
    table.destroy();
    table = $('#tabla_ventas_incompletas').DataTable({
           //pagingType: 'full_numbers',
           pagingType: 'input',
           //"sPaginationType": "listbox",
        //stateSave: true,
        responsive: !0,
        "bProcessing": true,
         search: {
                return: true
            }, 
        "serverSide": true,
        "ajax": {
            "url": base_url+"index.php/Ventas/getListaVentasIncompletasasi/",
            type: "post",
            "data": {
                'cliente':cliente,
                'status':estatus,
                'tipoventa':tipoventa,
                'estatusventa':estatusventa,
                'tipoview':tipoview,
                'idpersonal':idpersonal,
                'fechainicial_reg':fechainicial_reg,
                'emp':emp,
                'filf':filf,
                'pagosview':1
            },
        },
        "columns": [
            {"data": "id",
                render:function(data,type,row){
                    var html='';
                    if(row.combinada==0){
                        html='Venta: ';
                    }
                    if(row.combinada==1){
                        html='Venta Combinada: ';
                    }
                    if(row.combinada==3){
                        html='Poliza: ';
                    }
                    if (row.combinada==1) {
                        if(row.equipo>0){
                            html+=row.equipo;
                        }else if(row.consumibles>0){
                            html+=row.consumibles;
                        }else if(row.refacciones>0){
                            html+=row.refacciones;
                        }
                    }else{
                        html+=row.id;
                    }
                    if(row.combinada==4){
                        html='Contrato: '+row.id+'';
                    }
                    return html;
                }
            },
            {"data": "facturas",
                render:function(data,type,row){
                    var html='';
                    if(row.facturas!='' && row.facturas!='null' && row.facturas!=null){
                        html+=row.facturas;
                    }
                    return html;
                }
            },
            {"data": "tipopagofacman",
                render:function(data,type,row){
                    var html='';
                    if(row.tipopagofacman==1){
                        html='Complemento';
                    }
                    if(row.tipopagofacman==0){
                        html='Manual';
                    }
                    
                    return html;
                }
            },
            {"data": "combinada",
                 render:function(data,type,row){
                    var html='';
                    var totalcon= 0;
                    var totalref= 0;
                    var totalequ= 0;
                    var totalacc= 0;
                    var totalcon2= 0;
                    /*
                    if(data==1){
                        var html='Venta combinada';    
                    } else{
                        var html='Venta'; 
                    }
                    */
                    
                    if(row.total_general>0){
                        var totalventa=row.total_general;
                        if (row.combinada==3) {
                            if(totalventa>0){
                                totalventa=parseFloat(totalventa)+(parseFloat(totalventa)*0.16);
                            }
                            
                        }
                    }else{
                        if(row.totalcon>0){
                            var totalcon=row.totalcon;
                        }
                        if(row.totalref>0){
                            var totalref=row.totalref;
                        }
                        if(row.totalequ>0){
                            var totalequ=row.totalequ;
                        }
                        if(row.totalacc>0){
                            var totalacc=row.totalacc;
                        }
                        if(row.totalcon2>0){
                            var totalcon2=row.totalcon2;
                        }
                        var totalventa=parseFloat(totalcon)+parseFloat(totalref)+parseFloat(totalequ)+parseFloat(totalacc)+parseFloat(totalcon2);
                        if (row.combinada==1) {
                            if(row.totalpoli>0){
                                var totalpoli=row.totalpoli;
                            }else{
                                var totalpoli= 0;
                            }
                            totalventa=parseFloat(totalventa)+parseFloat(totalpoli);
                        }
                        totalventa=totalventa+(parseFloat(totalventa)*0.16);
                        html+='<!--consumibles:'+parseFloat(totalcon)+'-->\
                                <!--consumibles:'+parseFloat(totalcon2)+'-->\
                                <!--refacciones'+parseFloat(totalref)+'-->\
                                <!--equipos'+parseFloat(totalequ)+'-->\
                                <!--accesorios'+parseFloat(totalacc)+'-->\
                                <!--Total:'+totalventa+'-->';
                        }
                        
                    html+=new Intl.NumberFormat('es-MX',{style: "currency", currency: "MXN"}).format(totalventa);
                        return html;
                }
            }, 
            {"data": "id",
                 render:function(data,type,row){
                    var totalcon= 0;
                    var totalref= 0;
                    var totalequ= 0;
                    var totalacc= 0;
                    var totalcon2= 0;
                    var html='<div class="btn_list">';
                   
                    if(row.total_general>0){
                        var totalventa=row.total_general;
                    }else{
                        if(row.totalcon>0){
                            var totalcon=row.totalcon;
                        }
                        if(row.totalref>0){
                            var totalref=row.totalref;
                        }
                        if(row.totalequ>0){
                            var totalequ=row.totalequ;
                        }
                        if(row.totalacc>0){
                            var totalacc=row.totalacc;
                        }
                        if(row.totalcon2>0){
                            var totalcon2=row.totalcon2;
                        }
                        var totalventa=parseFloat(totalcon)+parseFloat(totalref)+parseFloat(totalequ)+parseFloat(totalacc)+parseFloat(totalcon2);
                            totalventa=(totalventa+(parseFloat(totalventa)*0.16)).toFixed(2);
                    }
                    $('.tooltipped').tooltip();
                    if(row.e_entrega==1){
                        var disabledbtn='disabled';
                    }else{
                        var disabledbtn='';
                    }
                    
                    

                    //if(parseInt(row.estatus)==1){
                        
                        if (row.prefactura==1 && totalventa>0) {
                            if(row.estatus==3){
                                var colorbuton='green';
                            }else{
                                var colorbuton='blue';
                                //if(row.vencimiento!='0000-00-00'){
                                    if(row.vencimiento<fechahoy){
                                        var colorbuton='red';
                                    }
                                //}
                            }
                            html+='<a class="btn-floating '+colorbuton+' tooltipped modal_pagos modal_pagos_'+row.id+' verificar_'+row.id+'_'+row.combinada+'" \
                                    data-position="top" data-delay="50" \
                                    data-tooltip="Pago" \
                                    data-idventa="'+row.id+'" \
                                    data-tipoventa="'+row.combinada+'" \
                                    onclick="modal_pago('+row.id+','+row.combinada+')">\
                                        <i class="material-icons">attach_money</i>\
                                      </a> ';
                        }
                        
                    //}
                      //if (row.prefactura==1) {
                        
                        //}
                        if (totalventa==0) {
                            /*
                            if(row.garantia==1){
                                var icong='<i class="fas fa-tags" ></i>';
                                var cgarantia=0;
                                var cgarantiat='Quitar Garantia';
                                var garcolor='b-btn-success';
                            }else{
                                var icong='<i class="fas fa-tag"></i>';
                                var cgarantia=1;
                                var cgarantiat='Garantia';
                                var garcolor='b-btn-primary';
                            }
                            html+=' <a class="b-btn '+garcolor+' tooltipped" \
                                    data-position="top" data-delay="50" data-tooltip="'+cgarantiat+'" \
                                    data-tooltip-id="6df07467-d305-bd8f-2908-fc7be2299d57" onclick="garantia('+row.id+','+row.combinada+','+cgarantia+')"> \
                                            '+icong+'</a> ';
                            */
                        }
                    html+='</div">';
                    if(row.combinada==3){
                        var html='<div class="btn_list">';
                        var totalventa=(parseFloat(row.total_general)+(parseFloat(row.total_general)*0.16)).toFixed(2);
                        $('.tooltipped').tooltip();
                        var btn_cfdi='';
                            var btn_pago='';
                            if (row.prefactura==1) {
                               btn_cfdi='<a class="btn-floating blue tooltipped" data-position="top" data-delay="50" data-tooltip="CFDI" onclick="modal_cdfi('+row.id+')">CFDI</a>';
                                if(totalventa>0){
                                    if(row.estatus==3){
                                        var colorbuton='green';
                                    }else{
                                        var colorbuton='blue';
                                        //if(row.vencimiento!='0000-00-00'){
                                            console.log('Poliza '+row.id+' '+row.vencimiento+'<'+fechahoy);
                                            if(row.vencimiento<fechahoy){
                                                var colorbuton='red';
                                            }
                                        //}
                                    }
                                    btn_pago='<a class="btn-floating '+colorbuton+' tooltipped modal_pagosp verificarp_'+row.id+'" data-position="top" data-delay="50" data-tooltip="Pago" data-poliza="'+row.id+'" onclick="modal_pago('+row.id+',4)">\
                                            <i class="material-icons">attach_money</i></a>';
                                }
                            }
                            

                            html+=''+btn_pago+'';
                        
                        
                        html+='</div">';
                    }
                    
                    return html;
                }
            },
            {"data": "empresa"},
            {"data": "reg"},
            
            //{"data": "idCotizacion"}, 
            
            
        ],
        "order": [[ 5, "desc" ]],
        "language": {
            "processing": "<div class='row'><div class='col s12 procesandoclass'><img src='"+base_url+"public/img/tenor.gif' width='50px;' height='50px;'>Procesando</div></div>"
        },
        createdRow:function(row,data,dataIndex){
            $(row).find('td:eq(8)')
                .addClass('obtencionfacturas venta_tipo_'+data.combinada+'_id_'+data.id)
                .attr('data-tipo',data.combinada).attr('data-idv',data.id);
        }
    }).on('draw',function(){
        $('.dataTables_filter').append('<i class="fa fa-search buttonsearch" onclick="searchtable_v()"></i>')
        vericarpagosventas();
        if(tipoventa==5){
            vericarpagospoliza();
        }
    });

}
function vericarpagosventas(){
    var tipoventa = $('#tipoventa option:selected').val();
    if(tipoventa!=4){
        console.log('vericarpagosventas');
        var DATAf  = [];
        $(".modal_pagos").each(function() {
            item = {};
            item ["idventa"] = $(this).data('idventa');
            item ["tipoventa"] = $(this).data('tipoventa');
            DATAf.push(item);
        });
        aInfo   = JSON.stringify(DATAf);
        //console.log(aInfo);
        var datos=aInfo;
        
        $.ajax({
            type:'POST',
            url: base_url+"Generales/vericarpagosventas",
            data: {
                datos:aInfo
            },
            success: function (data){
                //console.log(data);
                var array = $.parseJSON(data);
                console.log(array);
                $.each(array, function(index, item) {
                        $(".verificar_"+item.idventa+"_"+item.tipo).addClass( "yellowc" );
                    
                });
            }
        });
    }
    
}
function vericarpagospoliza(){
    console.log('vericarpagospoliza');
    var DATAf  = [];
    $(".modal_pagosp").each(function() {
        item = {};
        item ["poliza"] = $(this).data('poliza');
        DATAf.push(item);
    });
    aInfo   = JSON.stringify(DATAf);
    //console.log(aInfo);
    var datos=aInfo;
    
    $.ajax({
        type:'POST',
        url: base_url+"Generales/vericarpagospoliza",
        data: {
            datos:aInfo
        },
        success: function (data){
            //console.log(data);
            var array = $.parseJSON(data);
            console.log(array);
            $.each(array, function(index, item) {
                $(".verificarp_"+item.idpoliza).addClass( "yellowc" );
                
            });
        }
    });
}

function carga_ct(){
    setTimeout(function(){ 
        if($(".c_b_control_t_header.active").length>0){
            $('.c_b_control_t').html('<iframe src="'+base_url+'index.php/Control_tareas/iframe"></iframe>');
        }
    }, 1000);
}
function carga_a_i(){
    setTimeout(function(){ 
        if($(".c_b_acuinci_t_header.active").length>0){
            $('.c_b_acuinci_t').html('<iframe src="'+base_url+'index.php/Control_tareas/iframe/1"></iframe>');
        }
    }, 1000);
}
function detallev(id,tipo){
    if (tipo==0) {
        window.open(base_url+"Prefactura/view/"+id, "Prefactura", "width=780, height=612");
    }else{
        window.open(base_url+"Prefactura/viewc/"+id, "Prefactura", "width=780, height=612");
    }
}
function detallep(id){
    window.open(base_url+"PolizasCreadas/view/"+id, "Prefactura", "width=780, height=612");
}

function carga_pa_ef(){
    setTimeout(function(){ 
        if($(".carga_pagos_efe.active").length>0){
           carga_pa_ef_view();
        }
    }, 1000);
}
function carga_pa_ef_view(){
    $.ajax({
        type:'POST',
        url: base_url+"Main/pagosenefectivo",
        data: {
            datos:0
        },
        success: function (data){
            $('.carga_pa_ef').html(data);
            $('#table_pagos_e').DataTable();
        }
    });
}
function pago_visto(tipo,id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Confirma de marcar como visto el pago seleccionado?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                
                    $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Main/confirmarpagos",
                        data: {
                            tipo:tipo,
                            id:id
                        },
                        success: function (response){
                            toastr.success('Pago marcado como visto');
                                carga_pa_ef_view();
                        },
                        error: function(response){
                            $.alert({boxWidth: '30%',useBootstrap: false,title: 'Error!',content: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema'});   
                             
                        }
                    });

               
                
            },
            cancelar: function (){
                
            }
        }
    });
}