<?php
$a=session_id();
if(empty($a)) session_start();
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloAsistencia extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    function getlists($params){
            $sdia=$params['sdia'];
            $emp=$params['emp'];
            
            $columns = array( 
                0=>'asi.id',
                1=>'per.emp', 
                2=>'per.personalId',
                3=>'per.nombre',
                4=>'asi.fecha', 
                5=>'asi.hora_entrada', 
                6=>'asi.hora_salida', 
                7=>'per.apellido_paterno', 
                8=>'per.apellido_materno', 
                9=>'asi.longitud', 
                10=>'asi.latitud',
                11=>'asi.ubicacion',
                12=>'asi.ubicacion_salida', 
                13=>'asi.longitud_salida',
                14=>'asi.latitud_salida',
                15=>'asi.entrada_salida',
                16=>'asi.estatus',
                17=>"(SELECT av.tipo FROM asistencias_vacaciones as av WHERE av.activo=1 and av.idpersonal=per.personalId AND av.fechainicio <= '$sdia' AND av.fechafin >= '$sdia' limit 1) as tipo_av"
            );
            $columnsss = array( 
                0=>'asi.id',
                1=>'per.emp', 
                2=>'per.personalId',
                3=>'per.nombre',
                4=>'asi.fecha', 
                5=>'asi.hora_entrada', 
                6=>'asi.hora_salida', 
                7=>'per.apellido_paterno', 
                8=>'per.apellido_materno', 
                9=>'asi.longitud', 
                10=>'asi.latitud',
                11=>'asi.ubicacion',
                12=>'asi.ubicacion_salida', 
                13=>'asi.longitud_salida',
                14=>'asi.latitud_salida',
                15=>'asi.entrada_salida',
                16=>'asi.estatus'
            );

            $select="";
            foreach ($columns as $c) {
                $select.="$c, ";
            }
            $this->db->select($select);
            $this->db->from('personal per');
            if($sdia!=''){
                //$this->db->where(array('asi.fecha'=>$sdia));
                $w_fecha=" and asi.fecha='$sdia' ";
            }else{
                $w_fecha='';
            }
            $this->db->join('asistencias asi', "asi.personal=per.personalId $w_fecha",'left');

            $this->db->where('(per.estatus=1 or asi.activo=1)');
            if($sdia!=''){
                //$this->db->where(array('asi.fecha'=>$sdia));
            }
            if($emp>0){
                $this->db->where(array('per.emp'=>$emp));
            }
            
            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->db->group_start();
                foreach($columnsss as $c){
                    $this->db->or_like($c,$search);
                }
                $this->db->group_end();
                
            }
            
            $this->db->order_by($columnsss[$params['order'][0]['column']], $params['order'][0]['dir']);
            $this->db->limit($params['length'],$params['start']);
            //echo $this->db->get_compiled_select();
            $query=$this->db->get();
            // print_r($query); die;
            return $query;
    }
    function getliststotal($params){
            $sdia=$params['sdia'];
            $emp=$params['emp'];
            $columns = array( 
                0=>'asi.id',
                1=>'per.emp', 
                2=>'per.personalId',
                3=>'per.nombre',
                4=>'asi.fecha', 
                5=>'asi.hora_entrada', 
                6=>'asi.hora_salida', 
                7=>'per.apellido_paterno', 
                8=>'per.apellido_materno', 
                9=>'asi.longitud', 
                10=>'asi.latitud'
            );

            $select="";
            foreach ($columns as $c) {
                $select.="$c, ";
            }
            $this->db->select("*");
            $this->db->from('personal per');
            if($sdia!=''){
                //$this->db->where(array('asi.fecha'=>$sdia));
                $w_fecha=" and asi.fecha='$sdia' ";
            }else{
                $w_fecha='';
            }
            $this->db->join('asistencias asi', "asi.personal=per.personalId $w_fecha",'left');

            $this->db->where('(per.estatus=1 or asi.activo=1)');

            if($emp>0){
                $this->db->where(array('per.emp'=>$emp));
            }
            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->db->group_start();
                foreach($columns as $c){
                    $this->db->or_like($c,$search);
                }
                $this->db->group_end();
                
            }
            
            //$this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
            //$this->db->limit($params['length'],$params['start']);
            //echo $this->db->get_compiled_select();
            $query=$this->db->get();
            // print_r($query); die;
            return $query->num_rows();
    }
    public function searchempleados($search){
        $strq = "SELECT * from personal WHERE estatus=1 and (nombre like '%$search%' or apellido_materno like '%$search%'  or apellido_paterno like '%$search%' )";
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }
    function liscalendar($fechaini,$fechafin){
        $strq="SELECT av.id,av.idpersonal,av.fechainicio,av.fechafin,per.nombre,per.apellido_materno,per.apellido_paterno,av.tipo
        FROM asistencias_vacaciones as av
        INNER JOIN personal as per on per.personalId=av.idpersonal
        WHERE av.activo=1 and (av.tipo=1 or av.tipo=2) AND (av.fechainicio BETWEEN '$fechaini' AND '$fechafin' OR av.fechafin BETWEEN '$fechaini' AND '$fechafin')";
        $query = $this->db->query($strq);
        return $query;
    }
    function getlistsvc($params){
            $columns = array( 
                0=>'av.id',
                1=>'av.idpersonal', 
                2=>'av.fechainicio',
                3=>'av.fechafin',
                4=>'av.fechapermitida',
                5=>'av.tipo',
                6=>'per.nombre', 
                7=>'per.apellido_paterno', 
                8=>'per.apellido_materno',
                9=>'av.aceptada_rechzada',
            );

            $select="";
            foreach ($columns as $c) {
                $select.="$c, ";
            }
            $this->db->select($select);
            $this->db->from('asistencias_vacaciones av');
            $this->db->join('personal per', 'per.personalId = av.idpersonal');
            $this->db->where(array('av.activo'=>1));
            
            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->db->group_start();
                foreach($columns as $c){
                    $this->db->or_like($c,$search);
                }
                $this->db->group_end();
                
            }
            $this->db->order_by('tipo DESC,aceptada_rechzada ASC,fechainicio DESC');
            //$this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
            $this->db->limit($params['length'],$params['start']);
            //echo $this->db->get_compiled_select();
            $query=$this->db->get();
            // print_r($query); die;
            return $query;
    }
    function getlistsvctotal($params){
            $columns = array( 
                0=>'av.id',
                1=>'av.idpersonal', 
                2=>'av.fechainicio',
                3=>'av.fechafin',
                4=>'av.fechapermitida',
                5=>'av.tipo',
                6=>'per.nombre', 
                7=>'per.apellido_paterno', 
                8=>'per.apellido_materno'
            );

            $select="";
            foreach ($columns as $c) {
                $select.="$c, ";
            }
            $this->db->select("*");
            $this->db->from('asistencias_vacaciones av');
            $this->db->join('personal per', 'per.personalId = av.idpersonal');
            $this->db->where(array('av.activo'=>1));
            
            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->db->group_start();
                foreach($columns as $c){
                    $this->db->or_like($c,$search);
                }
                $this->db->group_end();
                
            }
            
            //$this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
            //$this->db->limit($params['length'],$params['start']);
            //echo $this->db->get_compiled_select();
            $query=$this->db->get();
            // print_r($query); die;
            return $query->num_rows();
    }
    function getlistfhp($params){
            $columns = array( 
                0=>'apfh.id',
                1=>'apfh.personalid',
                2=>'apfh.fecha',
                3=>'apfh.reg',
                4=>'per.nombre',
                5=>'per.apellido_paterno',
                6=>'per.apellido_materno'
            );

            $select="";
            foreach ($columns as $c) {
                $select.="$c, ";
            }
            $this->db->select($select);
            $this->db->from('asistencias_per_fhorario apfh');
            $this->db->join('personal per', 'per.personalId=apfh.personalid');
            $this->db->where(array('apfh.activo'=>1));
            
            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->db->group_start();
                foreach($columns as $c){
                    $this->db->or_like($c,$search);
                }
                $this->db->group_end();
                
            }
            
            $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
            $this->db->limit($params['length'],$params['start']);
            //echo $this->db->get_compiled_select();
            $query=$this->db->get();
            // print_r($query); die;
            return $query;
    }
    function getlistfhptotal($params){
            $columns = array( 
                0=>'apfh.id',
                1=>'apfh.personalid',
                2=>'apfh.fecha',
                3=>'apfh.reg',
                4=>'per.nombre',
                5=>'per.apellido_paterno',
                6=>'per.apellido_materno'
            );

            $select="";
            foreach ($columns as $c) {
                $select.="$c, ";
            }
            $this->db->select("*");
            $this->db->from('asistencias_per_fhorario apfh');
            $this->db->join('personal per', 'per.personalId=apfh.personalid');
            $this->db->where(array('apfh.activo'=>1));
            
            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->db->group_start();
                foreach($columns as $c){
                    $this->db->or_like($c,$search);
                }
                $this->db->group_end();
                
            }
            
            //$this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
            //$this->db->limit($params['length'],$params['start']);
            //echo $this->db->get_compiled_select();
            $query=$this->db->get();
            // print_r($query); die;
            return $query->num_rows();
    }

}