<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Rtickets extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloCatalogos');
        $this->submenu=66;
        if($this->session->userdata('logeado')==true){
            $this->perfilid =$this->session->userdata('perfilid');
            $this->idpersonal = $this->session->userdata('idpersonal');
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,$this->submenu);// 52 es el id del submenu
            if ($permiso==0) {
                redirect('Sistema');
            }
        }else{
            redirect('Sistema'); 
        }
    }

    public function index(){
        $data['MenusubId']=$this->submenu;
    	$this->load->view('header');
        $this->load->view('main',$data);
        $this->load->view('Reportes/rtickets');
        $this->load->view('footer');
        $this->load->view('Reportes/rticketsjs');
    }
    function exportar($fechaini,$fechafin){
        $data['fechaini']=$fechaini;
        $data['fechafin']=$fechafin;
        $data['resultti']=$this->ModeloCatalogos->reporteticket($fechaini,$fechafin);
        //$this->load->view('Reportes/rticketsexport',$data);
        $this->load->view('Reportes/rticketsexport2',$data);
    }



    

}
?>