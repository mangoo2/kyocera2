<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Rutas con Iconos Personalizados en Leaflet.js</title>
    <link rel="stylesheet" href="<?php echo base_url();?>public/plugins/leaflet/leaflet.css" />
    <style>
        body{
          margin: 0px;
        }
        #map {
          height: 98vh;
          width: 100%;
        }
    </style>
</head>
<body>
    <div id="map"></div>
    <script src="<?php echo base_url();?>public/plugins/leaflet/leaflet.js"></script>
    <script>
        // Inicializar el mapa
        var map = L.map('map').setView([19.4326, -99.1332], 13);

        // Añadir una capa de mapa base
        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            maxZoom: 18,
        }).addTo(map);

        // Definir las coordenadas y mensajes personalizados para las dos rutas
        var rutaAzul =[];
        var rutaRoja =[];
        /*
        var rutaRoja = [
            { coords: [19.4326, -99.1332], mensaje: "Inicio de la ruta roja" },
            { coords: [19.4270, -99.1356], mensaje: "Punto intermedio 1 de la ruta roja" },
            { coords: [19.4268, -99.1400], mensaje: "Punto intermedio 2 de la ruta roja" },
            { coords: [19.4253, -99.1450], mensaje: "Fin de la ruta roja" }
        ];

        var rutaAzul = [
            { coords: [19.4326, -99.1332], mensaje: "Inicio de la ruta azul" },
            { coords: [19.4335, -99.1380], mensaje: "Punto intermedio 1 de la ruta azul" },
            { coords: [19.4350, -99.1425], mensaje: "Punto intermedio 2 de la ruta azul" },
            { coords: [19.4370, -99.1455], mensaje: "Fin de la ruta azul" }
        ];
        */
        <?php if($result->num_rows()==0){ ?>
            var rutaAzul = [
                { coords: [19.08782533671139, -98.22777569573624], mensaje: "Sin registros" }
            ];
        <?php } ?>
        <?php foreach ($result->result() as $item) { ?>
          item1 = {};
          item1 ['coords']=[<?php echo $item->latitud; ?>,<?php echo $item->longitud ?>];
          item1 ["mensaje"]  = '<?php echo str_replace("'", "", $item->reg) ?>';

          rutaAzul.push(item1);
        <?php } ?>
        <?php foreach ($arrayubicli as $itemc) { ?>
          item2 = {};
          item2 ['coords']=[<?php echo $itemc['latitud'] ?>,<?php echo $itemc['longitud'] ?>];
          item2 ["mensaje"]  = '<?php echo str_replace("'", "", $itemc['message']) ?>';
          rutaRoja.push(item2);
        <?php } ?>

        // Crear íconos personalizados para cada ruta
        var iconoRojo = L.icon({
            iconUrl: 'https://altaproductividadapr.com/public/img/edificio.png',
            iconSize: [32, 32],
            iconAnchor: [16, 32],
            popupAnchor: [0, -32]
        });

        var iconoAzul = L.icon({
            iconUrl: 'https://altaproductividadapr.com/public/img/file-info_40446.png',
            iconSize: [32, 32],
            iconAnchor: [16, 32],
            popupAnchor: [0, -32]
        });

        // Crear y añadir la polilínea roja al mapa
        var lineaRoja = L.polyline(rutaRoja.map(p => p.coords), { color: 'red' }).addTo(map);

        // Crear y añadir la polilínea azul al mapa
        var lineaAzul = L.polyline(rutaAzul.map(p => p.coords), { color: 'blue' }).addTo(map);

        // Añadir marcadores personalizados a cada coordenada de la ruta roja
        rutaRoja.forEach((punto) => {
            var marcadorRojo = L.marker(punto.coords, { icon: iconoRojo }).addTo(map);
            marcadorRojo.bindPopup(punto.mensaje);
        });

        // Añadir marcadores personalizados a cada coordenada de la ruta azul
        rutaAzul.forEach((punto) => {
            var marcadorAzul = L.marker(punto.coords, { icon: iconoAzul }).addTo(map);
            marcadorAzul.bindPopup(punto.mensaje);
        });

        // Ajustar el mapa para mostrar ambas rutas
        map.fitBounds(lineaRoja.getBounds().extend(lineaAzul.getBounds()));
    </script>
</body>
</html>