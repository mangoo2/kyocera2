<style type="text/css">
  table td{
    font-size: 12px;
    padding: 6px 7px;
  }
</style>
<!-- START CONTENT -->
    <section id="content">
      <!--start container-->
        <div class="container">
          <div class="section">
            <div class="row">
              <div class="col s12">
                <input type="checkbox" name="checkbox1" id="tipoview" value="1">
              </div>
            </div>
          <div id="table-datatables">
            <br>
                <h4 class="header">Agregar Folios</h4>
                <div class="row">
                  <div class="col s2">
                   <label>Clientes</label> 
                   <select class="browser-default" id="nfclientes"></select>
                  </div>
                  <div class="col s2">
                   <label>Contratos</label> 
                   <select class="browser-default" id="nfcontratos" onchange="cargaequipos()"></select>
                  </div>
                  <div class="col s2 nfservicios">
                    <label>Servicios</label>
                    <div class="b-input-group mb-3">
                      <select class="browser-default form-control-bmz" id="nfservicios" onchange="seleccionser()"></select>
                      <div class="b-input-group-append">
                        
                      </div>
                    </div>
                    
                  </div>
                  <div class="col s2">
                   <label>Equipos</label> 
                   <select class="browser-default" id="nfequipos" onchange="cargaconsumibles()"></select>
                  </div>
                  <div class="col s2">
                    <label>Bodega</label>
                    <select id="nbodega" class="browser-default form-control-bmz" onchange="cargaconsumibles()">
                      <?php foreach ($get_bodegas as $item) { 
                        if($item->tipov==1){
                          echo '<option value="'.$item->bodegaId.'">'.$item->bodega.'</option>';
                        }
                      } ?>
                    </select> 
                  </div>
                  <div class="col s2">
                   <label>Consumibles</label> 
                   <select class="browser-default" id="nfconsumibles">
                     
                   </select>
                  </div>
                </div>
                <div class="row">
                  <div class="col s2">
                   <label>Fecha</label> 
                   <input type="date" class="browser-default form-control-bmz" id="nfecha">
                  </div>
                  <div class="col s9"></div>
                  <div class="col s1">
                    <a class="btn-floating waves-effect waves-light green darken-1 addconsumibles"><i class="material-icons">play_for_work</i></a>
                  </div>
                </div>
                <div class="row">
                  <div class="col s12">
                    <table id="tabla_cons" class="responsive-table display" cellspacing="0">
                      <thead>
                        <tr>
                          <th>Cliente</th>
                          <th>Contrato</th>
                          <th>Servicio</th>
                          <th>Equipo</th>
                          <th>Consumible</th>
                          <th>Fecha</th>
                          <th>Bodega</th>
                          <th>Acciones</th>
                        </tr>
                      </thead>
                      <tbody class="addconsumiblesbody">
                      </tbody>
                    </table>
                  </div>
                </div>
                <div class="row">
                  <div class="col s12">
                    <button class="btn waves-effect waves-light green darken-1 agregarconsumibles">Agregar</button>
                  </div>
                </div>
              </div>
          </div> 
          <div id="table-datatables_polizas" style="display:none;">
            <br>
                <h4 class="header">Agregar Folios</h4>
                <div class="row">
                  <div class="col s2">
                   <label>Clientes</label> 
                   <select class="browser-default" id="nfclientes_poliza">
                     
                   </select>
                  </div>
                  <div class="col s2">
                   <label>Pólizas</label> 
                   <select class="browser-default" id="nfcontratos_poliza" onchange="cargaequipos_poliza()">
                     
                   </select>
                  </div>
                  <div class="col s2">
                    <label>Servicios</label>
                    <select class="browser-default form-control-bmz" id="nfservicios_poliza">
                     
                   </select>
                  </div>
                  <div class="col s2">
                   <label>Equipos</label> 
                   <select class="browser-default" id="nfequipos_poliza" onchange="cargaconsumibles_poliza()">
                     
                   </select>
                  </div>
                  <div class="col s2">
                    <label>Bodega</label>
                    <select id="nbodega_poliza" class="browser-default form-control-bmz" onchange="cargaconsumibles_poliza()">
                      <?php foreach ($get_bodegas as $item) { ?>
                          <option value="<?php echo $item->bodegaId;?>"><?php echo $item->bodega;?></option>
                      <?php } ?>
                    </select> 
                  </div>
                  <div class="col s2">
                   <label>Consumibles</label> 
                   <select class="browser-default" id="nfconsumibles_poliza">
                     
                   </select>
                  </div>
                </div>
                <div class="row">
                  <div class="col s2">
                   <label>Fecha</label> 
                   <input type="date" class="browser-default form-control-bmz" id="nfecha_poliza">
                  </div>
                  
                  <div class="col s9">
                  </div>
                  <div class="col s1">
                        <a class="btn-floating waves-effect waves-light green darken-1 addconsumibles_poliza"><i class="material-icons">play_for_work</i></a>
                  </div>
                </div>
                <div class="row">
                  <div class="col s12">
                    <table id="tabla_cons" class="responsive-table display" cellspacing="0">
                      <thead>
                        <tr>
                          <th>Cliente</th>
                          <th>Póliza</th>
                          <th>Servicio</th>
                          <th>Equipo</th>
                          <th>Consumible</th>
                          <th>Fecha</th>
                          <th>Bodega</th>

                          <th>Acciones</th>
                        </tr>
                      </thead>
                      <tbody class="addconsumiblesbody_poliza">
                      </tbody>
                    </table>
                  </div>
                </div>
                <div class="row">
                  <div class="col s12">
                    <a class="btn waves-effect waves-light green darken-1 agregarconsumibles_poliza">Agregar</a>
                  </div>
                </div>
              </div>
          </div> 
        </div>
     </div>
    </section>
