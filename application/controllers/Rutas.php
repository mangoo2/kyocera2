<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rutas extends CI_Controller 
{
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        //$this->load->model('Servicios_model');
        $this->load->model('ModeloCatalogos');
        $this->load->model('Configuraciones_model');
        $this->submenu=50;
        if($this->session->userdata('logeado')==true){
            $this->idpersonal=$this->session->userdata('idpersonal');
        }else{
            redirect('login');
        }
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
    }

    public function index(){ 
    $data['MenusubId']=$this->submenu;  
        $data['rutas'] = $this->Configuraciones_model->getDataRutas();
        $this->load->view('header');
        $this->load->view('main');
        $this->load->view('configuraciones/rutas/ListaRutas',$data);
        $this->load->view('configuraciones/rutas/listado_rutas_js');
        $this->load->view('footer');
	}

     public function insertar(){
        $result = 0; 
        $data = $this->input->post();
               $datos = array(
                            'nombre' => $data["nombre"],
                            );
           $result = $this->ModeloCatalogos->Insert('rutas',$datos);
        echo $result;  
        //$this->Modelobitacoras->Insert(array('contenido'=>'Se Insertó ruta: '.$data["nombre"],'nombretabla'=>'rutas','idtable'=>$result,'tipo'=>'Insert','personalId'=>$this->idpersonal));
    }

    public function eliminar()
    {
        $id = $this->input->post('id');
        $result = $this->ModeloCatalogos->updateCatalogo('rutas',array('status'=>'0'),array('id'=>$id));

        echo $result; 
        //$this->Modelobitacoras->Insert(array('contenido'=>'Se eliminó una ruta: ','nombretabla'=>'rutas','idtable'=>$id,'tipo'=>'delete','personalId'=>$this->idpersonal));
    }  

}

?>