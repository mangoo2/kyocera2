<style type="text/css">
  .imgpro{width: 100px;}
  .imgpro:hover {
    transform: scale(2.9); 
    filter: drop-shadow(5px 9px 12px #444);
  }
  .btn_img{cursor: pointer;  }
  .chosen-container{margin: 0px;}
  td{font-size: 12px;}
  .fechades{display: none;}
  <?php if($idCliente>0){ ?>
    .sologlobal{display: none;}
  <?php }else{ ?>
    h4.header{content: 'Listado de Cotizaciones';}
  <?php } ?>
  #ifracotiza{
    border: 0px;
    width: 100%;
    min-height: 217px;
  }
  <?php if($perfilid==5 || $perfilid==12){ ?>
    .ocultar_tec{
      display: none;
    }
  <?php } ?>
</style>
<input type="date" id="fechades" class="form-control-bmz fechades" value="<?php 
                  $fechameses=date('Y-m-d H:i:s');
                echo date("Y-m-d",strtotime($fechameses."- 1 month"));?>">
<input type="hidden" id="bloqueo" value="<?php echo $bloqueo;?>">
<input type="hidden" id="perfilid" value="<?php echo $perfilid;?>">
<input type="hidden" id="idpersonal" value="<?php echo $idpersonal;?>">
<!-- START CONTENT -->
    <section id="content">
      <!--start container-->
        <div class="container">
          <div class="section">
            <div class="card-panel">
            <input id="base_url" type="hidden" name="base_url" value="<?php echo base_url(); ?>">
            <input id="idCliente" type="hidden" name="idCliente" value="<?php echo $idCliente; ?>">
            <input id="prospectoCliente" type="hidden" name="idCliente" value="<?php echo $label; ?>">
            <div id="table-datatables">
                  <h4 class="header">Listado de Cotizaciones del <?php echo $label; ?>: <?php echo $nombreCliente; ?></h4>
                  <div class="row">
                    
                    <div class="col s6 m10 l10">
                      <a class="b-btn b-btn-success btn-new-cot" href="<?php echo base_url(); ?>index.php/Cotizaciones/altaCotizacion/<?php echo $idCliente; ?>">Nuevo</a>
                    </div>
                    <div class="col s6 m2 l2">
                      <a class="b-btn b-btn-success  btn-new-cot" href="<?php echo base_url(); ?>index.php/Cotizaciones/listaCotizacionesPorCliente">Listado General</a>
                    </div>
                    <div class="col s6 m2 l2">
                        <label>Tipo</label>
                        <select id="tipocotizacion" class="browser-default form-control-bmz">
                          <option value="0">Activos</option>
                          <option value="1">Eliminadas</option>
                        </select>
                    </div>
                    <div class="col s6 m2 l2">
                        <label>Cotizacion</label>
                        <select id="tipog" class="browser-default form-control-bmz">
                          <option value="0">Todo</option>
                          <option value="1">Equipos</option>
                          <option value="2">Consumible</option>
                          <option value="3">Refacciones</option>
                          <option value="4">Polizas</option>
                          <option value="5">Accesorios</option>
                        </select>
                    </div>
                    <div class="col s6 m2 l2 sologlobal">
                      <label>Cliente</label>
                      <select id="idclientes" class="browser-default form-control-bmz">
                                
                      </select>
                    </div>
                    <div class="col s6 m2 l2">
                      <label>Fecha Inicio</label>
                      <input type="date" id="finicial" class="form-control-bmz" value="<?php echo $fechaini;?>">
                    </div>
                    <div class="col s6 m2 l2">
                      <label>Fecha Fin</label>
                      <input type="date" id="ffinal" class="form-control-bmz">
                    </div>
                    <div class="col s6 m2 l2">
                        <label>Tipo</label>
                        <select id="tipocot" class="browser-default form-control-bmz">
                          <option value="0">Todo</option>
                          <option value="1">POR DESGASTE</option>
                          <option value="2">POR DAÑO</option>
                          <option value="3">POR GARANTIA</option>
                        </select>
                    </div>
                    <div class="col s6 m2  l2" style="display:none;">
                        <label>Ejecutivo</label>
                        <select id="ejecutivoselect" class="browser-default selecte_general filtrosdatatable">
                          <?php 
                              
                            foreach ($personalrows->result() as $item) { ?>
                            <option  value="<?php echo $item->personalId;?>" ><?php echo $item->nombre.' '.$item->apellido_paterno.' '.$item->apellido_paterno;?></option>
                          <?php } ?>
                        </select>
                      </div>
                  </div>
                  <div class="row">
                    <div class="col s12" style="text-align:end">
                      <button class="b-btn b-btn-primary" onclick="load()" style="margin-top: 20px;" title="Realizar consulta"><i class="fas fa-filter"></i> <span class="btn-filtrar"></span></button>
                    </div> 
                  </div>
                <div class="row">
                  <div class="col s12">

                      <table id="tabla_cotizaciones" class="table display" cellspacing="0">
                        <thead>
                          <tr>
                            <th>#</th><th>Empresa</th><th>Cotización</th><th>Tipo</th><th>Prioridad</th><th>Estatus</th><th>Fecha</th><th>Observaciones</th><th>Series</th><th></th><th>Acciones</th>
                          </tr>
                        </thead>
                        <tbody>
                        </tbody>
                      </table>
                    </div>
                  </div>
            </div>
          </div>
          </div>  
        </div>
    </section>

    <div id="modalAutorizacion" class="modal"  >
        <div class="modal-content" style="padding: 15px;">

          <input type="hidden" name="idCotizacion" id="idCotizacion">
          <h5 >Seleccione los equipo que desea de los que posee la cotización </h5>
          <h6 >Equipos</h6>
          <br>
          <div class="row" style="margin-bottom: 5px;">
            <div class="col s12 m12 12 ">
              <ul class="collapsible collapsible-accordion" data-collapsible="accordion">
                <li class="">
                  <div class="collapsible-header">
                    <i class="material-icons">print</i> Equipos</div>
                  <div class="collapsible-body " style="display: none;">
                    <table class="striped Highlight " id="divcotizaciondataequipos">
                      <tbody class="divcotizaciondataequipos"></tbody>
                    </table>
                    
                  </div>
                </li>
                <li class="">
                  <div class="collapsible-header">
                    <i class="material-icons">toc</i> Consumibles</div>
                  <div class="collapsible-body " style="display: none; padding-top: 30px; margin-top: 0px; padding-bottom: 30px; margin-bottom: 0px;">
                    <table class="responsive-table display striped " id="divcotizaciondataconsumibles">
                      <tbody class="divcotizaciondataconsumibles"></tbody>
                    </table>
                  </div>
                </li>
                <li>
                  <div class="collapsible-header">
                    <i class="material-icons">rounded_corner</i> Accesorios</div>
                  <div class="collapsible-body ">
                    <table class="responsive-table display  striped " id="divcotizaciondataaccessorios">
                      <tbody class="divcotizaciondataaccessorios"></tbody>
                    </table>
                  </div>
                </li>
              </ul>
            </div>
          </div>
          <div class="row" style="margin-bottom: 0px;">
            <div class="col s12 m12 12 ">
              <div class="switch">
                <label style="font-size: 20px;">
                ¿Desea incluir instalación y/o servicio o póliza de cortesía?
                <input type="checkbox" id="aceptarcservicio">
                <span class="lever"></span>
                </label>
              </div>
            </div>
          </div>
          <div class="row modalAutorizacion_ventaservicio_fe limpiarservicio_fe">
            
          </div>
          <div class="row modalAutorizacion_ventaservicio limpiarservicio" style="display:none;"></div>
          
          
          
            
            <br>
        <div class="modal-footer">
          <button class="btn waves-effect waves-light green " name="botonEscoger" id="botonEscoger">
            Escoger
          </button>
          <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cancelar</a>
        </div>
      </div>
    </div>


    <div id="modalAutorizacionPoliza" class="modal"  >
        <div class="modal-content " >

          <input type="hidden" name="idCotizacionPoliza" id="idCotizacionPoliza">
          <h4 >  Seleccione la póliza que desea, de las que posee la cotización.</h4>
          <br>
          <div class="col s6 m6 12">
            <div class="row" style="min-height: 105px;" >
              
              <table class="table responsive-table" id="tablepolizas">
                <thead>
                  <tr>
                   <th></th> 
                   <th>Cantidad</th> 
                   <th>Poliza</th> 
                   <th>Serie</th> 
                  </tr>
                </thead>
                <tbody class="tableselectedpoliza">
                  <tr>
                   <td>
                      <input type="checkbox" class="filled-in" id="polizaselected"/>
                      <label for="polizaselected"></label>
                   </td> 
                   <td>Poliza</td> 
                   <td>
                     <input type="text" id="polizaserie" placeholder="">
                   </td> 
                  </tr>
                </tbody>
              </table>                
            </div>
          </div>
            
            <br><br><br>
        <div class="modal-footer">
          <button class="btn waves-effect waves-light green botonEscogerPoliza" name="botonEscogerPoliza" id="botonEscogerPoliza">
            Escoger
          </button>
          <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cancelar</a>
        </div>
      </div>
    </div>

    <div id="modalAutorizacionconsumible" class="modal"  >
        <div class="modal-content modal-lg" >

          <input type="hidden" name="idCotizacionConsumible" id="idCotizacionConsumible">
          <h5 >Consumibles cotizados</h5>
          <h6 >Consumibles</h6>
          <br>
          <div class="row">
            <div class="tabla_cotizarmostrar"></div>  
          </div>
          <!--Agregar mas detalles de cotizacion-->
          <div class="row">
              <div class="input-field col s3">  
                  <i class="material-icons prefix">format_list_bulleted</i>
                  <label for="estado">Equipos</label> 
                  <br><br>
                  <select id="equipoListaConsumibles" name="equipoListaConsumibles" class="browser-default chosen-select equipoListaConsumibles" onchange="selectequipo()">
                      <option value="" disabled selected></option>
                      <?php foreach($equipos as $equipo){
                          echo '<option value="'.$equipo->id.'" >'.$equipo->modelo.'</option>';
                       } ?>
                  </select>
              </div>
              <div class="input-field col s3">  
                  <i class="material-icons prefix">person</i>
                  <label for="estado">Disponibles</label> 
                  <br><br>
                  <div class="toner_mostrar"></div>
              </div>
              <div class="input-field col s3">  
                  <i class="material-icons prefix">attach_money</i>
                  <label for="estado">Precios</label> 
                  <br><br>
                  <div class="tonerp_mostrar"></div>
              </div>
              <div class="input-field col s2">  
                  <i class="material-icons prefix">format_list_bulleted</i>
                  <label for="">Cantidad</label>
                  <br><br>
                  <input type="number" id="piezas" name="piezas" class="form-control-bmz" min="1" max="5">
              </div>
              <div class="input-field col s1">
                <br>
                <a class="btn-floating waves-effect waves-light purple lightrn-1 btn tooltipped" data-position="top" data-tooltip="Nuevo" id="botonAgregarConsumible" name="botonAgregarConsumible" onclick="addconsutable()">
                <i class="material-icons">add</i>
                </a>
              </div>
          </div>  
          <div class="row">
             <div class="col s12 m12 12 equipotableconsumicle">
                  <table class="col s12 m12 12 tableconsumible Highlight striped">
                      <tbody>
                      </tbody>
                  </table>
              </div>
          </div>
          <div class="row" style="margin-bottom: 0px;">
            <div class="col s12 m12 12 ">
              <div class="switch">
                <label style="font-size: 20px;">
                ¿Desea incluir instalación y/o servicio o póliza de cortesía?
                <input type="checkbox" id="aceptarcserviciorcc">
                <span class="lever"></span>
                </label>
              </div>
            </div>
          </div>
          <div class="row modalAutorizacionconsumible_ventaservicio_fe limpiarservicio_fe">
            
          </div>
          <div class="row modalAutorizacionconsumible_ventaservicio limpiarservicio" style="display:none;"></div>
          <!-- --> 
          <div class="modal-footer">
            <button class="btn waves-effect waves-light green checkconsumibles"   onclick="checkconsumibles()">
              Aceptar
            </button>
            <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cancelar</a>
          </div>
        </div>
    </div>
    <!--
    <div id="modalAutorizacionrefacciones" class="modal"  >
        <div class="modal-content modal-lg" >

          <input type="hidden" name="idCotizacionRefaccion" id="idCotizacionRefaccion">
          <h5 >Refacciones Cotizados</h5>
          <h6 >Refacciones</h6>
          <br>
          <div class="row">
            <div class="tabla_cotizarmostrarre"></div>  
          </div>
          <div class="row">
              <div class="input-field col s3">  
                  <i class="material-icons prefix">format_list_bulleted</i>
                  <label for="estado">Equipos</label> 
                  <br><br>
                  <select id="equipoListarefacciones" name="equipoListarefacciones" class="browser-default chosen-select equipoListarefacciones" onchange="selectequipor()">
                      <option value="" disabled selected></option>
                      <?php foreach($equipos as $equipo){
                            echo '<option value="'.$equipo->id.'">'.$equipo->modelo.'</option>';
                          } ?>
                  </select>
              </div>
              <div class="input-field col s3">  
                  <i class="material-icons prefix">person</i>
                  <label for="estado">Disponibles</label> 
                  <br><br>
                  <div class="refacciones_mostrar"></div>
              </div>
              <div class="input-field col s3">  
                  <i class="material-icons prefix">attach_money</i>
                  <label for="estado">Precios</label> 
                  <br><br>
                  <div class="refaccionesp_mostrar"></div>
              </div>
              
              <div class="input-field col s1">
                <br>
                <a class="btn-floating waves-effect waves-light purple lightrn-1 btn tooltipped" data-position="top" data-tooltip="Nuevo" id="botonAgregarConsumible" name="botonAgregarConsumible" onclick="addrefaccion()">
                <i class="material-icons">add</i>
                </a>
              </div>
          </div> 
          <div class="row">
            <div class=" col s3">  
                  <label for="estado" class="active">Cantidad</label>
                  
                  <input type="number" id="piezasr" name="piezasr" class="form-control-bmz" min="1" max="5">
            </div>
            <div class="col s3">
              <label for="serie_bodegarrs">Bodega</label>
                <select id="serie_bodegarrs" name="serie_bodegarrs" class="browser-default chosen-select" >
                  <?php foreach ($bodegas as $itemb) { ?> 
                      <option value="<?php echo $itemb->bodegaId; ?>"><?php echo $itemb->bodega; ?></option>
                  <?php } ?> 
                </select>
            </div>
          </div> 
          <div class="row">
             <div class="col s12 m12 12 equipotableconsumicle">
                  <table class="col s12 m12 12 tablerefacciones Highlight striped">
                      <tbody>
                      </tbody>
                  </table>
              </div>
          </div> 
          <div class="row" style="margin-bottom: 0px;">
            <div class="col s12 m12 12 ">
              <div class="switch">
                <label style="font-size: 20px;">
                ¿Desea incluir instalación y/o servicio o póliza de cortesía?
                <input type="checkbox" id="aceptarcserviciorf">
                <span class="lever"></span>
                </label>
              </div>
            </div>
          </div>
          <div class="row modalAutorizacionrefacciones_ventaservicio_fe limpiarservicio_fe">
            
          </div>
          <div class="row modalAutorizacionrefacciones_ventaservicio limpiarservicio" style="display:none;"></div>
          <div class="modal-footer">
            <button class="btn waves-effect waves-light green checkrefacciones"   onclick="checkrefacciones()">
              Aceptar
            </button>
            <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cancelar</a>
          </div>
        </div>
    </div>
  -->
    <div id="modaldenegar" class="modal"  >
        <div class="modal-content">
          <h5 >Eliminar Cotizacion </h5>
          <br>
          <div class="row">
            <div class="col s12 m12 12 ">
              <label>Motivo</label>
              <textarea id="motivodenegacion"></textarea>
              <input type="hidden" id="denegacionId">
            </div>
          </div>
          <br>
        <div class="modal-footer">
          <a class="btn waves-effect waves-light green " id="btndenegar">
            Aceptar
          </a>
          <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cancelar</a>
        </div>
      </div>
    </div>
    
    <div id="modalEditarcontacto" class="modal"  >
        <div class="modal-content">
          <h5 >Editar contacto </h5>
          <br>
          <div class="row">
            <div class="col-md-12">
              <input type="hidden" id="editarcontactocotizacion" value="">
              <select id="atencionselect" class="browser-default form-control-bmz" onchange="obtenercontactosss()">
                      
                </select>
            </div>
          </div>
          <div class="row">
            <div class="col s4 m4 4 ">
              <label>Atencion</label>
              <input type="text" id="mec_atencion" class="form-control-bmz" readonly>
            </div>
            <div class="col s4 m4 4 ">
              <label>Correo</label>
              <input type="email" id="mec_correo" class="form-control-bmz" readonly>
            </div>
            <div class="col s4 m4 4 ">
              <label>Telefono</label>
              <input type="text" id="mec_telefono" class="form-control-bmz" readonly>
            </div>
          </div>
          <br>
        <div class="modal-footer">
          <a class="btn waves-effect waves-light green " onclick="okbotoneditarcontacto()">
            Aceptar
          </a>
          <a class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cancelar</a>
        </div>
      </div>
    </div>

    <div id="modalinfoeliminada" class="modal"  >
        <div class="modal-content">
          <h5 >Motivo de la eliminacion</h5>
          <br>
          <div class="row">
            <div class="col s12 m12 12 infoeliminada">
              
            </div>
          </div>
          <br>
        <div class="modal-footer">
          <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
        </div>
      </div>
    </div>




    <div id="modalAutorizacionGeneral" class="modal"  >
        <div class="modal-content modal-lg" >

          <input type="hidden" name="idcotizaciongeneral" id="idcotizaciongeneral">
          <h5 >Productos Cotizados</h5>
          <br>
          <div class="row">
            <form class="form form-cot" id="form-cot">
              
            </form>
            <div class="col s6 soloventas">
              <div style="text-align: end;"><a class="btn-bmz blue" onclick="servicioaddcliente()" style="margin-bottom: 10px;">Agregar un servicio existente</a></div>
              <div>
                <input type="number" name="v_ser_tipo" id="v_ser_tipo" style="display:none">
                <input type="number" name="v_ser_id" id="v_ser_id" style="display:none">
              </div>
              <div class="addinfoser"></div>
            </div>
          </div>
          <div class="row">
            <div class="tabla_cotizarmostrarre"></div>  
          </div>
          
          
          <div class="row" style="margin-bottom: 0px; display: none;">
            <div class="col s12 m12 12 ">
              <div class="switch">
                <label style="font-size: 20px;">
                ¿Desea incluir instalación y/o servicio o póliza de cortesía?
                <input type="checkbox" id="aceptarcserviciorf">
                <span class="lever"></span>
                </label>
              </div>
            </div>
          </div>
          <div class="row modalAutorizacionrefacciones_ventaservicio_fe limpiarservicio_fe">
            
          </div>
          <div class="row modalAutorizacionrefacciones_ventaservicio limpiarservicio" style="display:none;"></div>
          <!-- --> 
          <div class="modal-footer">
            <button class="btn waves-effect waves-light green autorizaciongeneral"   onclick="autorizaciongeneral()">
              Aceptar
            </button>
            <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cancelar</a>
          </div>
        </div>
    </div>





    <div class="row" style="display:none;">
      <select id="selectedviewpoliza">
        <?php foreach($resultpolizas->result() as $item){ ?>
          <option value="<?php echo $item->id;?>"><?php echo $item->nombre;?></option>
        <?php } ?>
      </select>
    </div>
<div id="modal_servicio" class="modal">
    <div class="modal-content">
      <div class="col s12">
        <label>Firma del ejecutivo</label>
        <select class="browser-default form-control-bmz" id="firma_m_s">
          <?php foreach ($perfirma->result() as $itemf) {
            echo '<option value="'.$itemf->personalId.'">'.$itemf->nombre.' '.$itemf->apellido_paterno.' '.$itemf->apellido_materno.' / '.$itemf->email.' / '.$itemf->celular.'</option>';
          }?>
        </select>
      </div>
      <div class="col s12">
        <label>Copia oculta (BBC)</label>
        <select class="browser-default form-control-bmz" id="bbc_m_s"><option value="0"></option>
          <?php foreach ($perfirma->result() as $itemf) {
            echo '<option value="'.$itemf->personalId.'">'.$itemf->nombre.' '.$itemf->apellido_paterno.' '.$itemf->apellido_materno.' / '.$itemf->email.' / '.$itemf->celular.'</option>';
          }?>
        </select>
      </div>
      <div class="col s12">
          <label>Correo envio</label>
          <div class="m_s_contac"></div>
        </div>
      <div class="row">
        <div class="col s12">
          <label>Asunto</label>
          <input class="form-control-bmz" id="ms_asucto2" value="Servicio">
        </div>
        <div class="col s7">
          <label>Comentario</label>
          <textarea class="form-control-bmz" id="ms_coment2" style="min-height: 150px;"></textarea>
        </div>
        <div class="col s5 input_envio_s">
        </div>
        
      </div>
      <div class="row"><div class="col s12 addbtnservicio"></div></div>
 
    </div>
    <div class="row">
      <div class="col m12" align="right">
        <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
      </div>
    </div>        
</div>



<div id="modalservicioadd" class="modal" style="z-index: 1003; display: none; opacity: 0; transform: scaleX(0.7); top: 4%;">
  <div class="modal-content ">
    <h5>Seleccione servicio existente</h5>
    <div class="row">
      <div class="col s12 servicioaddcliente">
        
      </div>
    </div>
  </div>
  <div class="modal-footer">
    <button class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</button>
  </div>
</div>