<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="icon" href="<?php echo base_url(); ?>app-assets/images/favicon/favicon_kyocera.png" sizes="32x32">
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<title>Prefactura</title>
		<!-- jQuery Library -->
    	<script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/jquery-3.3.1.min.js"></script>
    	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    	<link href="<?php echo base_url(); ?>app-assets/vendors/sweetalert/dist/sweetalert.css" type="text/css" rel="stylesheet">
    	<!--
    	<script type="text/javascript" src="<?php echo base_url(); ?>app-assets/js/ckeditor/ckeditor.js"></script>
    	<script type="text/javascript" src="<?php echo base_url(); ?>app-assets/js/ckeditor/adapters/jquery.js"></script>-->
		<style type="text/css">
			.optionred{
				background: red;
			}
			.tdstitle{
				background: #c1bcbc;
				font-weight: bold;
				color: white;
			}
			html{
			        -webkit-print-color-adjust: exact;
			}
			input{
				
				padding: 0px !important;
				height: 28px !important;
			}
			td,th{
				padding-top: 5px !important;
				padding-bottom: 5px !important;

			}
			select{
				
				padding: 0px !important;
				height: 28px !important;
				
				 font-size: 11px !important;
			}
			.optionred{
				background: #e29696;
			}
			@media print{
				input{
					background: transparent;
					border: 0 !important;
					
				}
				.buttonenvio{
					display: none;
				}
				html{
			        -webkit-print-color-adjust: exact;
				}
				.buttoimprimir{
					display: none;
				}
				.tdstitle{
					background: #c1bcbc;
					font-weight: bold;
					color: white;
				}
				select{
					background: transparent;
					border: 0 !important;
					-moz-appearance: none;
					 -webkit-appearance: none;
					 appearance: none;
				}
				.form-control:disabled, .form-control[readonly] {
				    background-color: transparent;
				    opacity: 1;
				}
			}
			#modaldirecciones .modal-dialog{
				max-width: 90% !important;
			}
			.viewdirecciones{
				max-height: 385px !important
			}
		</style>
	</head>
	<body>
		<input type="hidden" id="base_url" name="base_url" value="<?php echo base_url(); ?>">
		<form method="post" id="formprefactura">
			<input type="hidden" id="asignacionId" name="asignacionId" value="<?php echo $ventaId;?>">
			<input type="hidden" id="tipo" name="tipo" value="<?php echo $tipo;?>">
			<table border="1" width="100%">
				<tr style="height: 25%">
					<td style="width: 20%">
						<img src="<?php echo base_url(); ?>app-assets/images/<?php echo $configuracionCotizacion->logo1; ?>">
					</td>
					<td style="width: 55%">
						<?php echo $configuracionCotizacion->textoHeader; ?>
					</td>
					<td style="width: 20%">
						<img class="" src="<?php echo base_url(); ?>app-assets/images/1024_kyocera_logo_mds.png" alt="materialize logo"  style="max-width:200px; width: 100%;">
					</td>
				</tr>
			</table>
			<table border="1" width="100%">
				<tr>
					<td class="tdstitle" style="width: 10%;">FECHA</td>
					<td style="width: 5%;"><?php echo $dia_reg;?></td>
					<td style="width: 5%;"><?php echo $mes_reg;?></td>
					<td style="width: 5%;"><?php echo $ano_reg;?></td>
					<td class="tdstitle" style="width: 5%">AC</td>
					<td style="width: 10%;"><?php echo $ini_personal;?></td>
					<td class="tdstitle" style="width: 20%">PROINVEN No.</td>
					<td style="width: 10%;"><?php echo $proinven;?></td>
					<td class="tdstitle" style="width: 10%">CDFI</td>
					<td style="width: 20%"><?php echo $cdfif;?></td>
				</tr>
			</table>
			<table border="1" width="100%">
				<tr>
					<td style="width: 5%;">Cia.</td>
					<td style="width: 80%;" colspan="5"><output id="razon_social"><?php echo $empresa_t ?></output></td>
					<td style="width: 5%;">RFC</td>
					<td style="width: 10%;">

					    <select class="form-control" id="rfc_id" name="rfc_id" <?php echo $rfc_id_button;?> onchange="selecrfcp()">>
						        <option value="">Seleccione</option>
						<?php foreach ($rfc_datos as $item) { ?>
								<option value="<?php echo $item->id; ?>" <?php if($item->id==$rfc_id){ echo 'selected';}?> ><?php echo $item->rfc; ?></option>
						<?php } ?>
					    </select>
						</td>
				</tr>
				<tr>
					<td style="width: 5%; font-size: 12px">VENCE</td>
					<td style="width: 12%;">
						<input type="date" id="vencimiento" name="vencimiento" class="form-control" value="<?php echo $vence;?>" <?php echo $vence_block;?> style="max-width: 138px;"></td>
					<td style="width: 10%; font-size: 12px; text-align: center;">METODO DE PAGO</td>
					<td style="width: 20%; font-size: 12px; text-align: center;">
					<select class="form-control" id="metodopagoId" name="metodopagoId" <?php echo $metodopagoId_block;?> >
						<?php foreach ($metodopagorow as $item) { ?>
							<option value="<?php echo $item->id; ?>" <?php if($item->id==$metodopagoId){ echo 'selected';} ?>><?php echo $item->metodopago_text; ?></option>
						<?php } ?>
					</select>
					</td>
					<td style="width: 12%; font-size: 12px; text-align: center;">FORMA DE PAGO</td>
					<td style="width: 14%; font-size: 12px; text-align: center;">
						<select class="form-control" id="formapagoId" name="formapagoId" <?php echo $formapago_block;?> >
						<?php foreach ($formapagorow->result() as $item) { ?>
							<option value="<?php echo $item->id; ?>" <?php if($item->id==$formapago){ echo 'selected';} ?>><?php echo $item->formapago_text; ?></option>
						<?php } ?>
					</select>
					</td>
					<td style="width: 10%; font-size: 12px;">USO DE CFDI</td>
					<td style="width: 17%; font-size: 12px;">
						<select class="form-control" id="usocfdiId" name="usocfdiId" <?php echo $cfdi_block;?>>
							<?php foreach ($cfdirow->result() as $item) { ?>
								<option value="<?php echo $item->id; ?>" <?php if($item->id==$cfdi){ echo 'selected';} ?>><?php echo $item->uso_cfdi_text; ?></option>
							<?php } ?>
						</select>
					</td>	
					
				</tr>
			</table>
			<table border="1" width="100%">
				<tr>
					<td style="font-size: 12px; width: 5%;">Calle</td>
					<td style="font-size: 12px; width: 26%;" colspan="3"><input type="text" value="<?php echo $calle;?>" id="calle" name="d_calle" style="width: 100%; border: 0px;" onclick="editardireccion(<?php echo $idCliente;?>)" readonly></td>
					<td style="font-size: 12px; width: 5%;">No.</td>
					<td style="font-size: 12px; width: 5%;"><input type="text" value="<?php echo $num_ext;?>" id="num_ext" name="d_numero" style="width: 100%; border: 0px;" onclick="editardireccion(<?php echo $idCliente;?>)" readonly></td>
					<td style="font-size: 12px; width: 5%;">Col.</td>
					<td style="font-size: 12px; width: 25%; " colspan="2"><input type="text" value="<?php echo $colonia;?>" id="colonia" name="d_colonia" style="width: 100%; border: 0px;" onclick="editardireccion(<?php echo $idCliente;?>)"readonly></td>
					<td style="font-size: 12px; width: 9%; text-align: center;">FORMA DE COBRO</td>
					<td style="font-size: 12px; width: 20%;">
						<input type="tex" id="formadecobro" name="formadecobro" class="form-control" value="<?php echo $formacobro;?>" <?php echo $formacobro_block;?> >
					</td>
				</tr>
				<tr>
					<td style="font-size: 12px; width: 5%;">Cd</td>
					<td style="font-size: 12px;" colspan="2"><?php echo $municipio;?></td>
					<td style="font-size: 12px; width: 5%;">Edo.</td>
					<td style="font-size: 12px; width: 5%;" colspan="4">
							<?php 
								$estadoval='';
								foreach ($estadorow->result() as $item) { 
									if($item->EstadoId==$estado){ 
									 	$estadoval = $item->Nombre;
									}
								} ?>
						<input type="text" value="<?php if($estadovals!=''){echo $estadovals;}else{echo $estadoval.' C.P.'.$cp;}?>" id="d_estado" name="d_estado" onclick="editardireccion(<?php echo $idCliente;?>)" style="width: 100%; border: 0px;" readonly>
					</td>
					<td style="font-size: 12px; width: 5%;">tel.</td>
					<td style="font-size: 12px;" colspan="2">
						<select class="form-control" id="telId" name="telId" <?php echo $telId_block; ?>>
						<?php foreach ($resultadoclitel->result() as $item) { ?>
							<option value="<?php echo $item->id;?>"   <?php if($telId==$item->id){echo 'selected'; }?> ><?php echo $item->tel_local;?></option>		
					    <?php } 
					    	foreach ($datoscontacto->result() as $item) { ?>
					    	<option data-contacto="1"  value="<?php echo $item->datosId;?>" <?php if($telId==$item->datosId){echo 'selected'; }?>><?php echo $item->telefono;?></option>	
					    <?php }
					    ?>
						</select>
					</td>
					
				</tr>
				<tr>
					<td style="font-size: 12px; width: 10%;" colspan="2">Contacto</td>
					<td style="font-size: 12px;" colspan="2">
						<select class="form-control" id="contactoId" name="contactoId" <?php echo $contactoId_block; ?> onchange="obtenercargo()">
						
					    <?php foreach ($datoscontacto->result() as $item) { ?>
					    	<option data-contacto="1" data-puesto="<?php echo $item->puesto;?>" value="<?php echo $item->datosId;?>" <?php if($contactoId==$item->datosId){echo 'selected'; }?>><?php echo $item->atencionpara;?></option>	
					    	<?php 
					    		if ($contactoId==$item->datosId) {
					    			$email=$item->email;
					    		}
							}
					    ?>
					    <?php foreach ($resultadoclipcontacto->result() as $item) { ?>
							<option class="optionred" value="<?php echo $item->id;?>" <?php if($contactoId==$item->id){echo 'selected'; }?>><?php echo $item->persona_contacto;?></option>		
					    <?php } ?>
						</select>
					</td>
					<td style="font-size: 12px; width: 5%;">Cargo</td>
					<td style="font-size: 12px; width: 5%;" colspan="3">
						<input type="tex" id="cargo" name="cargo" class="form-control" value="<?php echo $cargo;?>" <?php echo $cargo_block;?> >
					</td>
					<td style="font-size: 12px; width: 5%;">email:</td>
					<td style="font-size: 12px;" colspan="2"><?php echo $email;?></td>
				</tr>
				
			</table>
			<table border="1" width="100%">
				<?php if($resultadoconsumibles->num_rows()>0){ ?>
                <tr>
					<th colspan="7" style="background-color:#5a5959;color:white;text-align:center;width: 12%;">Consumibles</th>
				</tr>
                		
				<tr>
					<th style="font-size: 12px; ">Cantidad</th>
					<th style="font-size: 12px; ">No. de Parte</th>
					<th style="font-size: 12px; " colspan="3">Descripcion</th>
					<th style="font-size: 12px; ">Precio Unitario</th>
					<th style="font-size: 12px; " >Total</th>
				</tr>
				<?php } ?>
					<?php 
						$totalgeneral=0;
						$block_envio=0; 
						foreach ($resultadoconsumibles->result() as $item) { 
							$costo=$item->costo;

							$totalc=$item->cantidad*$costo;
							$totalgeneral=$totalgeneral+$totalc;
							?>
							<tr>
								<td style="font-size: 12px; text-align: center;"><?php echo $item->cantidad;?></td>
								<td style="font-size: 12px; "><?php echo $item->parte;?></td>
								<td style="font-size: 12px; " colspan="3">
									<table border="1" width="100%">
										<tr>
											<td width="30%">TIPO</td>
											<td width="70%">CONSUMIBLE</td>		
										</tr>
										<tr>
											<td>MARCA</td>
											<td>KYOCERA</td>		
										</tr>
										<tr>
											<td>MODELO</td>
											<td><?php echo $item->modelo;?></td>		
										</tr>									
									</table>
								</td>
								<td style="font-size: 12px; text-align: center;"  >$<?php echo number_format($costo,2,'.',',');?></td>
								<td style="font-size: 12px; text-align: center;" >$<?php echo number_format($totalc,2,'.',',');?></td>
							</tr>
							

							<?php }
			                if($resultadorefacciones->num_rows()>0){
			                ?>
							<tr>
					            <th colspan="7" style="background-color:#5a5959;color:white;text-align:center;width: 12%;">Refacciones</th>
				            </tr>
				            <tr>
								<th style="font-size: 12px; ">Cantidad</th>
								<th style="font-size: 12px; ">No. de Parte</th>
								<th style="font-size: 12px; " colspan="3">Descripción</th>
								<th style="font-size: 12px; ">Precio Unitario</th>
								<th style="font-size: 12px; " >Total</th>
							</tr>
                            <?php } ?>
						<?php	
                        foreach ($resultadorefacciones->result() as $item) { 
							$totala=$item->cantidad*$item->costo;
							$totalgeneral=$totalgeneral+$totala;
							
							?>
							<tr>
								<td style="font-size: 12px; text-align: center;"><?php echo $item->cantidad;?></td>
								
								<td style="font-size: 12px; "></td>
								<td style="font-size: 12px; " colspan="3">
									<table border="1" width="100%">
										<tr>
											<td width="30%">TIPO</td>
											<td width="70%">REFACCION</td>		
										</tr>
										<tr>
											<td>MARCA</td>
											<td>KYOCERA</td>		
										</tr>
										<tr>
											<td>MODELO</td>
											<td><?php echo $item->nombre;?></td>		
										</tr>
										
									</table>
								</td>
								<td style="font-size: 12px; text-align: center;"  >$<?php echo number_format($item->costo,2,'.',',');?></td>
								<td style="font-size: 12px; text-align: center;" >$<?php echo number_format($totala,2,'.',',');?></td>
							</tr>
							
							<?php	
						} 
						
						?>	
						
						<?php if ($resultadoaccesorios->num_rows()>0) { ?>
						<tr>
					        <th colspan="7" style="background-color:#5a5959;color:white;text-align:center;width: 12%;">Accesorios</th>
				        </tr>
				        <tr>
							<th style="font-size: 12px; ">Cantidad</th>
							<th style="font-size: 12px; ">No. de Parte</th>
							<th style="font-size: 12px; " colspan="3">Descripcion</th>
							<th style="font-size: 12px; ">Precio Unitario</th>
							<th style="font-size: 12px; " >Total</th>
						</tr>
						<?php } 
							foreach ($resultadoaccesorios->result() as $item) { 
								$totala=$item->cantidad*$item->costo;
								$totalgeneral=$totalgeneral+$totala;
						?>
								<tr>
									<td style="font-size: 12px; text-align: center;"><?php echo $item->cantidad;?></td>
									
									<td style="font-size: 12px; "><?php echo $item->no_parte;?></td>
									<td style="font-size: 12px; " colspan="3">
										<table border="1" width="100%">
											<tr>
												<td width="30%">TIPO</td>
												<td width="70%">Accesorio</td>		
											</tr>
											<tr>
												<td>MARCA</td>
												<td>KYOCERA</td>		
											</tr>
											<tr>
												<td>MODELO</td>
												<td><?php echo $item->nombre;?></td>		
											</tr>
											
										</table>
									</td>
									<td style="font-size: 12px; text-align: center;"  >$<?php echo number_format($item->costo,2,'.',',');?></td>
									<td style="font-size: 12px; text-align: center;" >$<?php echo number_format($totala,2,'.',',');?></td>
								</tr>
						<?php }
						?>
					
				
					
					<tr>
						<th colspan="7" style="background-color:#5a5959;color:white;text-align:center;width: 12%;">Servicios</th>
					</tr>	
					<tr>
						<td style="font-size: 12px; " colspan="2">Tipo de servicio: <?php echo $t_servicio ?></td>
						<td style="font-size: 12px; " colspan="5">Descripcion de falla: <?php echo $t_servicio_motivo ?></td>
					</tr>
					<tr>
						<td style="font-size: 12px; " colspan="3">Equipos: <?php echo $t_modelo?></td>
						<td style="font-size: 12px; " colspan="4">Costo de servicio: $<?php echo $t_precio ?></td>
					</tr>
                    <?php 
                        $totalgeneral=$totalgeneral+$t_precio;
                    ?>
					<tr>
						<td style="font-size: 12px; " >Subtotal</td>
						<td style="font-size: 12px; " colspan="2">$<?php echo number_format($totalgeneral,2,'.',',');?></td>
						<td style="font-size: 12px; ">Iva</td>
						<td style="font-size: 12px; " >$<?php echo number_format($totalgeneral*0.16,2,'.',',');?></td>
						<td style="font-size: 12px; ">Total</td>
						<td style="font-size: 12px; " >$<?php echo number_format(($totalgeneral*1.16),2,'.',',');?></td>
					</tr>
			</table>
			<label for="observaciones">Observaciones:</label>
			<textarea name="observaciones" id="observaciones" class="form-control" <?php echo $observaciones_block;?> ><?php echo $observaciones;?></textarea>
            <?php if($firma!=''){?>
				<?php 
				    $fh = fopen(base_url()."uploads/firmasservicios/".$firma, 'r') or die("Se produjo un error al abrir el archivo");
		            $linea = fgets($fh);
		            fclose($fh);     
		        ?>
                <div align="center">
		            <h5>Firma</h5>
		        </div>
		        <div style="text-align:center;margin-top:0.05cm;padding:0.1cm;">
	            <img src="<?php echo $linea ?>" width="355" height="160" style="border:dotted 1px black;">
	            </div>
            <?php }?>
		</form>
		<div class="row">
		    <div class="col-md-12">
		        <div class="col-md-12 text-right">
		        	<?php 
		        		if($block_button==0){ 
		        			if($block_envio==0){ ?>
		        			<button type="button" class="btn btn-success buttonenvio">Enviar</button>
		        	<?php 	}  
		        		}else{  	?>	
		        		<button type="button" class="btn btn-success buttoimprimir">Imprimir</button>
		        	<?php  }  	?>	
		        </div>
		    </div>
		</div>
		<div class="modal fade" id="modalconfirmacionenvio" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h5 class="modal-title" id="exampleModalLabel">Enviar</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      <div class="modal-body">
		        <div class="row">
		        	<div class="col-md-12">
		        		<h4>¿Desea enviar la prefactura?</h4>
		        	</div>
		        </div>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
		        <button type="button" class="btn btn-primary buttonenvioconfirm">Enviar</button>
		      </div>
		    </div>
		  </div>
		</div>
		<!-- f1 -->
		<div class="modal fade" id="modaleditar_d_equipo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h5 class="modal-title" id="exampleModalLabel">Modificar Surtir</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      <div class="modal-body">
		        <div class="row">
	              <div class="input-field col s12">
	              	<label for="newprecio">Nuevo surtir</label>
	              	<input id="idvdetalle" type="hidden" class="validate" placeholder="Texto" required>
	                <input id="surtir" type="text" class="validate" placeholder="Texto" required>
	                
	              </div>
	            </div>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
		        <button type="button" class="btn btn-primary" onclick="aceptar_d_e()">Aceptar</button>
		      </div>
		    </div>
		  </div>
		</div>
		<!-- f3 -->
		<div class="modal fade" id="modaleditarConsumible" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h5 class="modal-title" id="exampleModalLabel">Modificar Surtir</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      <div class="modal-body">
		        <div class="row">
	              <div class="input-field col s12">
	              	<label for="newprecio">Nuevo surtir</label>
	              	<input id="idadetalle" type="hidden" class="validate" placeholder="Texto" required>
	                <input id="surtira" type="text" class="validate" placeholder="Texto" required>
	                
	              </div>
	            </div>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
		        <button type="button" class="btn btn-primary" onclick="aceptar_c()">Aceptar</button>
		      </div>
		    </div>
		  </div>
		</div>
		<div class="modal fade" id="modaleditarvdc" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h5 class="modal-title" id="exampleModalLabel">Modificar Surtir</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      <div class="modal-body">
		        <div class="row">
	              <div class="input-field col s12">
	              	<label for="newprecio">Nuevo surtir</label>
	              	<input id="idvdcdetalle" type="hidden" class="validate" placeholder="Texto" required>
	                <input id="surtirvdc" type="text" class="validate" placeholder="Texto" required>
	                
	              </div>
	            </div>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
		        <button type="button" class="btn btn-primary" onclick="aceptar_vdc()">Aceptar</button>
		      </div>
		    </div>
		  </div>
		</div>
		<!-- f4-->
		<div class="modal fade" id="modaleditar_ra" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h5 class="modal-title" id="exampleModalLabel">Modificar Surtir</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      <div class="modal-body">
		        <div class="row">
	              <div class="input-field col s12">
	              	<label for="newprecio">Nuevo surtir</label>
	              	<input id="idradetalle" type="hidden" class="validate" placeholder="Texto" required>
	                <input id="surtirra" type="text" class="validate" placeholder="Texto" required>
	                
	              </div>
	            </div>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
		        <button type="button" class="btn btn-primary" onclick="aceptar_ra()">Aceptar</button>
		      </div>
		    </div>
		  </div>
		</div>
		<!-- f5-->
		<div class="modal fade" id="modaleditar_pvd" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h5 class="modal-title" id="exampleModalLabel">Modificar Surtir</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      <div class="modal-body">
		        <div class="row">
	              <div class="input-field col s12">
	              	<label for="newprecio">Nuevo surtir</label>
	              	<input id="idpvdadetalle" type="hidden" class="validate" placeholder="Texto" required>
	                <input id="surtirpvd" type="text" class="validate" placeholder="Texto" required>
	                
	              </div>
	            </div>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
		        <button type="button" class="btn btn-primary" onclick="aceptar_pvd()">Aceptar</button>
		      </div>
		    </div>
		  </div>
		</div>
		<!-- f6-->
		<div class="modal fade" id="modaleditar_vdr" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h5 class="modal-title" id="exampleModalLabel">Modificar Surtir</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      <div class="modal-body">
		        <div class="row">
	              <div class="input-field col s12">
	              	<label for="newprecio">Nuevo surtir</label>
	              	<input id="idvdrdetalle" type="hidden" class="validate" placeholder="Texto" required>
	                <input id="surtirvdr" type="text" class="validate" placeholder="Texto" required>
	                
	              </div>
	            </div>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
		        <button type="button" class="btn btn-primary" onclick="aceptar_vdr()">Aceptar</button>
		      </div>
		    </div>
		  </div>
		</div>
		<div class="modal fade" id="modaldirecciones" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h5 class="modal-title" id="exampleModalLabel">Direcciones</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      <div class="modal-body">
		        <div class="row">
		        	<div class="col-md-12 viewdirecciones" style="overflow: auto;max-height: 72px;font-size: 12px;">
		        		
		        	</div>
		        </div>
		        <div class="row">
		        	<div class="col-md-10">
		        		<label>Calle</label>
		        		<input type="text" class="form-control" id="new_calle">
		        	</div>
		        	<div class="col-md-2">
		        		<label>No.</label>
		        		<input type="text" class="form-control" id="new_no">
		        	</div>
		        	<div class="col-md-6">
		        		<label>Colonia</label>
		        		<input type="text" class="form-control" id="new_colonia">
		        	</div>
		        	<div class="col-md-6">
		        		<label>Ciudad</label>
		        		<input type="text" class="form-control" id="new_ciudad">
		        	</div>
		        	<div class="col-md-6">
		        		<label>Estado</label>
		        		<input type="text" class="form-control" id="new_estado">
		        	</div>
		        </div>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
		        <button type="button" class="btn btn-primary buttoneditardireccion">Enviar</button>
		      </div>
		    </div>
		  </div>
		</div>
	</body>
	<script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/sweetalert/dist/sweetalert.min.js"></script>
	<script type="text/javascript">
		var base_url = $('#base_url').val();
		$(document).ready(function(){ 
			$('.buttonenvio').click(function(event) {
				$('#modalconfirmacionenvio').modal();
			});
			$('.buttonenvioconfirm').click(function(event) {
				var datos = $('#formprefactura').serialize();
				$.ajax({
                    type:'POST',
                    url: base_url+'Prefactura/saveser',
                    data: datos,
                    async: false,
                    statusCode:{
                            404: function(data){
                                swal("Error", "404", "error");
                            },
                            500: function(){
                                swal("Error", "500", "error"); 
                            }
                        },
                        success:function(data){
                          
                            setTimeout(function(){ window.location.href=''; }, 3000);
                         

                        }
                    });
			});
			$('.buttoimprimir').click(function(event) {
				window.print();
			});
			$('.buttoneditardireccion').click(function(event) {
				var new_calle = $('#new_calle').val();
				var new_no = $('#new_no').val();
				var new_colonia = $('#new_colonia').val();
				var new_ciudad = $('#new_ciudad').val();
				var new_estado = $('#new_estado').val();
                $('#num_ext').val(new_no);
                $('#colonia').val(new_colonia);
                $('#calle').val(new_calle);
                $('#d_ciudad').val(new_ciudad);
				$('#d_estado').val(new_estado);
				$('#modaldirecciones').modal('hide');
			});
			<?php if ($rfc_id>0) { ?>
				selectrcfv(<?php echo $rfc_id;?>);
			<?php } ?>

		});
		function selecrfcp() {
			var rfc_id = $('#rfc_id option:selected').val();
			selectrcfv(rfc_id);
			
		}
		function selectrcfv(rfc_id){
			$.ajax({
                url: base_url+'Prefactura/selecciontrcf',
                dataType: 'json',
                data:{id:rfc_id},
                statusCode:{
                        404: function(data){
                            swal("Error", "404", "error");
                        },
                        500: function(){
                            swal("Error", "500", "error"); 
                        }
                    },
                    success:function(data){
                    	var datos=data;
                        datos.forEach(function(r) {
                        if(r.rfc=='XAXX010101000'){
                    		$('#razon_social').val(r.razon_social+' ('+r.empresa+')');
                    	}else{
                    		$('#razon_social').val(r.razon_social);
                    	}
                        <?php if ($rfc_id==0) { ?>
	                        $('#num_ext').val(r.num_ext);
	                        $('#colonia').val(r.colonia);
	                        $('#calle').val(r.calle);
                        <?php } ?>
                        });
                    }
            });
		}
		//f1
		function editar_d_e(id,text) {
		    $('#modaleditar_d_equipo').modal();
		    $('#idvdetalle').val(id);
		    $('#surtir').val(text);
		}
		//f2
		function editar_ra(id,text) {
			$('#modaleditar_ra').modal();
		    $('#idradetalle').val(id);
		    $('#surtirra').val(text);
		}
	    // f3
		function editar_c(id,text) {
		    $('#modaleditarConsumible').modal();
		    $('#idadetalle').val(id);
		    $('#surtira').val(text);	    
		}
		// f4
		function editar_cvd(id,text) {
			$('#modaleditarvdc').modal();
		    $('#idvdcdetalle').val(id);
		    $('#surtirvdc').val(text);
		}
		// f5
		function editar_pvd(id,text) {
		 	$('#modaleditar_pvd').modal();
		    $('#idpvdadetalle').val(id);
		    $('#surtirpvd').val(text);
		 } 
		 // f6
		 function editar_vdr(id,text) {
		 	$('#modaleditar_vdr').modal();
		    $('#idvdrdetalle').val(id);
		    $('#surtirvdr').val(text);
		 }
		// f1
		function aceptar_d_e() {
		    var idv = $('#idvdetalle').val();
		    var sur = $('#surtir').val();	
			$.ajax({
                type:'POST',
                url: base_url+'Prefactura/updatevequipodetalle',
                data:{id:idv,surtir:sur},
                async: false,
                statusCode:{
                        404: function(data){
                            swal("Error", "404", "error");
                        },
                        500: function(){
                            swal("Error", "500", "error"); 
                        }
                    },
                    success:function(data){
                        swal("Éxito", "Se actulizo correctamente", "success");
                        setTimeout(function(){ window.location.href=''; }, 3000);
                    }
            });  
        }    
        // f2
        function aceptar_ra(argument) {
        	var idv = $('#idradetalle').val();
		    var sur = $('#surtirra').val(); 	
			$.ajax({
                type:'POST',
                url: base_url+'Prefactura/updateaccesoriosventa',
                data:{id:idv,surtir:sur},
                async: false,
                statusCode:{
                        404: function(data){
                            swal("Error", "404", "error");
                        },
                        500: function(){
                            swal("Error", "500", "error"); 
                        }
                    },
                    success:function(data){
                        swal("Éxito", "Se actulizo correctamente", "success");
                        setTimeout(function(){ window.location.href=''; }, 3000);
                    }
            });
        }
        //f3
        function aceptar_c(){ 
		    var idv = $('#idadetalle').val();
		    var sur = $('#surtira').val(); 	
			$.ajax({
                type:'POST',
                url: base_url+'Prefactura/updatevequipodetalleaccesorio',
                data:{id:idv,surtir:sur},
                async: false,
                statusCode:{
                        404: function(data){
                            swal("Error", "404", "error");
                        },
                        500: function(){
                            swal("Error", "500", "error"); 
                        }
                    },
                    success:function(data){
                        swal("Éxito", "Se actulizo correctamente", "success");
                        setTimeout(function(){ window.location.href=''; }, 3000);
                    }
            });
		}
		//f4
        function aceptar_vdc(){ 
		    var idv = $('#idvdcdetalle').val();
		    var sur = $('#surtirvdc').val(); 	
			$.ajax({
                type:'POST',
                url: base_url+'Prefactura/updateconsumiblesventadetalles',
                data:{id:idv,surtir:sur},
                async: false,
                statusCode:{
                        404: function(data){
                            swal("Error", "404", "error");
                        },
                        500: function(){
                            swal("Error", "500", "error"); 
                        }
                    },
                    success:function(data){
                        swal("Éxito", "Se actulizo correctamente", "success");
                        setTimeout(function(){ window.location.href=''; }, 3000);
                    }
            });
		}
		//f5
        function aceptar_pvd(){ 
		    var idv = $('#idpvdadetalle').val();
		    var sur = $('#surtirpvd').val(); 	
			$.ajax({
                type:'POST',
                url: base_url+'Prefactura/updatepolizasCreadas_has_detallesPoliza',
                data:{id:idv,surtir:sur},
                async: false,
                statusCode:{
                        404: function(data){
                            swal("Error", "404", "error");
                        },
                        500: function(){
                            swal("Error", "500", "error"); 
                        }
                    },
                    success:function(data){
                        swal("Éxito", "Se actulizo correctamente", "success");
                        setTimeout(function(){ window.location.href=''; }, 3000);
                    }
            });
		}
		//f6
        function aceptar_vdr(){ 
		    var idv = $('#idvdrdetalle').val();
		    var sur = $('#surtirvdr').val(); 	
			$.ajax({
                type:'POST',
                url: base_url+'Prefactura/updateventas_has_detallesRefacciones',
                data:{id:idv,surtir:sur},
                async: false,
                statusCode:{
                        404: function(data){
                            swal("Error", "404", "error");
                        },
                        500: function(){
                            swal("Error", "500", "error"); 
                        }
                    },
                    success:function(data){
                        swal("Éxito", "Se actulizo correctamente", "success");
                        setTimeout(function(){ window.location.href=''; }, 3000);
                    }
            });
		}
		function editardireccion(cliente){
			<?php if ($rfc_id==0) { ?>
			$('#modaldirecciones').modal();
			$.ajax({
				type:'POST',
                url: base_url+'PolizasCreadas/pintardireccionesclientes',
                data:{clienteid:cliente},
                statusCode:{
                        404: function(data){
                            swal("Error", "404", "error");
                        },
                        500: function(){
                            swal("Error", "500", "error"); 
                        }
                    },
                    success:function(data){
                    	$('.viewdirecciones').html(data);
                    	

                    }
            });
           	<?php } ?> 
		}
		function obtenercargo(){
			var contacto=$('#contactoId option:selected').data('contacto');
			if (contacto==1) {
				var contactoid=$('#contactoId option:selected').val();
				//$('#telId option:selected').val(contactoid);
				$("#telId option").removeAttr("selected");
				$("#telId option[value="+contactoid+"]").attr('selected', 'selected');

				var puesto=$('#contactoId option:selected').data('puesto');

				$('#cargo').val(puesto);
			}
		}
	</script>
</html>
