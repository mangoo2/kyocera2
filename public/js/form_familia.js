 var base_url = $('#base_url').val();
$(document).ready(function(){
    
    var base_url = $('#base_url').val();
    $('#tabla_familia tbody').on('click', 'a.editar', function () {
      var row=$(this).closest("tr");
      var id = row.find("td").eq(1).html();
      var nombre = row.find("td").eq(2).html();
      $('.modal').modal({
            dismissible: true
        });
      $('#modalFamilia').modal('open');
      $('#id_familia').val(id);
      $('#name').val(nombre);
    });  

    var form_familia = $('#form_familia');
    
    form_familia.validate({
            rules: 
            {
                
                nombre: {
                    required: true
                }
            },
            // Para mensajes personalizados
            messages: 
            {
                 
                nombre:{
                    required: "Ingrese un nombre"
                }
            },
        errorElement : 'div',
            // Si están correctos los campos, procesamos el formulario mediante Ajax
        submitHandler: function (form) 
        {
            var datos = form_familia.serialize();
            var texto=$("#nombre").val();
            $.ajax({
                type:'POST',
                url: base_url+'index.php/Familias/insertarFamilia',
                data: datos,
                success:function(data)
                {
                    // Si existe el familiar, mostramos el mensaje de entrada y redirigimos
                    if(data>0)
                    {
                        swal({ title: "Éxito",
                            text: "Se inserto familiar",
                            type: 'success',
                            showCancelButton: false,
                            allowOutsideClick: false,
                        });
                        $('#form_familia')[0].reset();
                        setTimeout(function(){
                        location.reload();
                      },1000);
                      

                    }
                    else
                    {
                        swal({ title: "Error",
                            text: "Los datos del son erroneos",
                            type: 'error',
                            showCancelButton: false,
                            allowOutsideClick: false,
                        });
                    }
                }
            }); 
        }
    
    });
});
function editar(){
      var id = $('#id_familia').val();
      var nombre = $('#name').val();
      $.ajax({
      type: "POST",
      url: base_url+"index.php/Familias/editar_familia",
      data:{id:id,nombre:nombre},
      success: function(data){
        swal({ title: "Éxito",
                            text: "Se actualizo familiar",
                            type: 'success',
                            showCancelButton: false,
                            allowOutsideClick: false,
                        });
                        setTimeout(function(){
                        location.reload();
                        },1000);
      }
      });
}