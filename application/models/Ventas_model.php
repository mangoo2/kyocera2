<?php
defined('BASEPATH') OR exit ('No direct script access allowed');

class Ventas_model extends CI_Model {
    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
        $this->DB3 = $this->load->database('other3_db', TRUE);
    }
    public function insertarVenta($dato){
	    $this->db->insert('ventas',$dato);   
	    return $this->db->insert_id();
    }  
    public function insertarDetallesVentasEquipos($dato){
        $this->db->insert('ventas_has_detallesEquipos',$dato);   
        return $this->db->insert_id();
    }
    public  function detallesventas($id){
        $sql = "SELECT v.id, vd.modelo, vd.serie FROM ventas AS v
                INNER JOIN ventas_has_detallesEquipos AS vd ON vd.idVenta = v.id
                WHERE v.id =$id";
        $query = $this->DB3->query($sql);
        return $query->result();
    }
    function detallesventasconsumibles($id){
        $sql = "SELECT con.modelo,vdc.* 
                FROM ventas_has_detallesequipos_consumible as vdc 
                inner join consumibles as con on con.id=vdc.id_consumibles
                WHERE vdc.idventa=$id";
        $query = $this->DB3->query($sql);
        return $query;
    }
    function detallesaccesoriosv($id){
        $sql="SELECT acc.nombre,vda.serie 
        FROM `ventas_has_detallesEquipos_accesorios` as vda
        inner JOIN catalogo_accesorios as acc on acc.id=vda.id_accesorio
        WHERE vda.id_venta=".$id;
        $query = $this->DB3->query($sql);
        return $query;
    }
    function updateCatalogo($data,$idname,$id,$catalogo){
        $this->db->set($data);
        $this->db->where($idname, $id);/// Se puede ocupar un array para n condiciones
        $this->db->update($catalogo);
        return $id;
    }
    // Controlador Ventasi
    /*
    public function getListaVentasIncompletasfactura($params){
            $tipo=$params['tipo'];
            $cliente=$params['cliente'];
            $status=$params['status'];
            if ($tipo==1) { //venta
                $columns = array( 
                    0=>'v.estatus',
                    1=>'v.id',
                    2=>'v.prefactura',
                    3=>'v.telefono',
                    4=>'v.correo',
                    5=>'cli.empresa',
                    6=>'per.nombre vendedor',  
                    7=>"'ventas' tabla",
                    8=>'v.fechaentrega',
                    9=>'v.e_entrega'

                );
                $columns2 = array( 
                    0=>'v.estatus',
                    1=>'v.id',
                    2=>'v.prefactura',
                    3=>'v.telefono',
                    4=>'v.correo',
                    5=>'cli.empresa',
                    6=>'per.nombre',
                    7=>'v.fechaentrega'                    
                );
                $select="";
                foreach ($columns as $c) {
                    $select.="$c, ";
                }
                $this->DB3->select($select);
                $this->DB3->from('ventas v ');
                $this->DB3->join('clientes cli', 'cli.id=v.idCliente');
                $this->DB3->join('personal per', 'per.personalId=v.id_personal','left');
                $where = array('v.prefactura'=>1);
                if ($cliente>0) {
                    $where['v.idCliente']=$cliente;
                }
                if ($status==1) {
                    $where['v.statuspago']=1;
                }elseif ($status==2) {
                    $where['v.statuspago']=0;
                }
                    $where['v.activo']=1;
                
                
                $this->DB3->where($where);
            }elseif ($tipo==2) { //renta
                $columns = array( 
                    0=>'v.estatus',
                    1=>'v.id',
                    2=>'v.prefactura',
                    3=>'v.telefono',
                    4=>'v.correo',
                    5=>'cli.empresa',
                    6=>'per.nombre vendedor',
                    7=>"'rentas' tabla",
                    8=>"'' fechaentrega",
                    9=>'0 e_entrega'                   
                );
                $columns2 = array( 
                    0=>'v.estatus',
                    1=>'v.id',
                    2=>'v.prefactura',
                    3=>'v.telefono',
                    4=>'v.correo',
                    5=>'cli.empresa',
                    6=>'per.nombre',                     
                );
                $select="";
                foreach ($columns as $c) {
                    $select.="$c, ";
                }
                $this->DB3->select($select);
                $this->DB3->from('rentas v ');
                $this->DB3->join('clientes cli', 'cli.id=v.idCliente');
                $this->DB3->join('personal per', 'per.personalId=v.id_personal','left');
                $where = array('v.prefactura'=>1);
                 if ($cliente>0) {
                    $where['v.idCliente']=$cliente;
                }
                if ($status==1) {
                    $where['v.statuspago']=1;
                }elseif ($status==2) {
                    $where['v.statuspago']=0;
                }
                $this->DB3->where($where);
            }elseif ($tipo==3) { //polizas
                $columns = array( 
                    0=>'v.estatus',
                    1=>'v.id',
                    2=>'v.prefactura',
                    3=>'v.telefono',
                    4=>'v.correo',
                    5=>'cli.empresa',
                    6=>'per.nombre vendedor',
                    7=>"'polizasCreadas' tabla", 
                    8=>"'' fechaentrega", 
                    9=>'0 e_entrega'                   
                );
                $columns2 = array( 
                    0=>'v.estatus',
                    1=>'v.id',
                    2=>'v.prefactura',
                    3=>'v.telefono',
                    4=>'v.correo',
                    5=>'cli.empresa',
                    6=>'per.nombre',                     
                );
                $select="";
                foreach ($columns as $c) {
                    $select.="$c, ";
                }
                $this->DB3->select($select);
                $this->DB3->from('polizasCreadas v ');
                $this->DB3->join('clientes cli', 'cli.id=v.idCliente');
                $this->DB3->join('personal per', 'per.personalId=v.id_personal','left');
                $where = array('v.prefactura'=>1,'v.activo'=>1);
                 if ($cliente>0) {
                    $where['v.idCliente']=$cliente;
                }
                if ($status==1) {
                    $where['v.statuspago']=1;
                }elseif ($status==2) {
                    $where['v.statuspago']=0;
                }
                $this->DB3->where($where);
            }elseif ($tipo==4) { //conbinado
                $columns = array( 
                    0=>'vc.estatus',
                    1=>'vc.combinadaId id',
                    2=>'vc.prefactura',
                    3=>'vc.telefono',
                    4=>'vc.correo',
                    5=>'cli.empresa',
                    6=>'per.nombre vendedor',
                    7=>"'combinada' tabla",
                    8=>'vc.fechaentrega',
                    9=>'vc.e_entrega'
                );
                $columns2 = array( 
                    0=>'vc.combinadaId',
                    1=>'vc.estatus',
                    2=>'vc.prefactura',
                    3=>'vc.telefono',
                    4=>'vc.correo',
                    5=>'cli.empresa',
                    6=>'per.nombre',                     
                );
                $select="";
                foreach ($columns as $c) {
                    $select.="$c, ";
                }
                $this->DB3->select($select);
                $this->DB3->from('ventacombinada vc ');
                $this->DB3->join('clientes cli', 'cli.id=vc.idCliente');
                $this->DB3->join('personal per', 'per.personalId=vc.id_personal','left');
                $where = array('vc.prefactura'=>1);
                 if ($cliente>0) {
                    $where['vc.idCliente']=$cliente;
                }
                if ($status==1) {
                    $where['vc.statuspago']=1;
                }elseif ($status==2) {
                    $where['vc.statuspago']=0;
                }
                $this->DB3->where($where);
            }           
            if( !empty($params2['search']['value']) ) {
                $search=$params2['search']['value'];
                $this->DB3->group_start();
                foreach($columns as $c){
                    $this->DB3->or_like($c,$search);
                }
                $this->DB3->group_end();
                
            }
            //==========================================
            $this->DB3->order_by($columns2[$params['order'][0]['column']], $params['order'][0]['dir']);
            $this->DB3->limit($params['length'],$params['start']);
            //echo $this->db->get_compiled_select();
            $query=$this->DB3->get();
            // print_r($query); die;
            return $query;
    }
    function getListaVentasIncompletasfacturat($params){
        $tipo=$params['tipo'];
        if ($tipo==1) {
            
            $strq = "SELECT COUNT(*) as total
                FROM ventas as v 
                WHERE v.prefactura = 1";
        }elseif ($tipo==2) {
            $strq = "SELECT COUNT(*) as total
                FROM rentas as v 
                WHERE v.prefactura = 1";
        }
        elseif ($tipo==3) {
            $strq = "SELECT COUNT(*) as total
                FROM polizasCreadas as v 
                WHERE v.activo=1 and v.prefactura = 1";
        }elseif ($tipo==4) {
            $strq = "SELECT COUNT(*) as total
                FROM ventacombinada as vc
                WHERE vc.prefactura = 1";
        }
        $query = $this->DB3->query($strq);
        //$this->db->close();
        $total=0;
        foreach ($query->result() as $row) {
            $total =$row->total;
        } 
        return $total;
    }
    */
    /*
    public function getListaVentasIncompletasfactura($params){
            $tipo=$params['tipo'];
            $cliente=$params['cliente'];
            $status=$params['status'];
            $fecha=$params['fecha'];
                $columns = array( 
                    0=>'v.id',
                    1=>'v.empresa',
                    2=>'v.prefactura',
                    3=>"v.tabla",
                    4=>'v.vendedor',  
                    5=>'v.estatus',
                    6=>'v.reg',
                    7=>'v.fechaentrega',
                    8=>'v.e_entrega',
                    9=>'v.id_rh',
                    10=>'v.renta_rh',
                    11=>'v.tipotabla',
                    12=>'ep.nombre',
                    13=>'ep.apellido_paterno',
                    14=>'v.equipo',
                    15=>'v.consumibles',
                    16=>'v.refacciones',
                    17=>'v.poliza'

                );
                
                $select="";
                foreach ($columns as $c) {
                    $select.="$c, ";
                }
                $this->db->select($select);
                $this->db->from('vista_entregas_dia v ');
                $this->db->join('personal ep', 'ep.personalId = v.e_entrega_p','left');
                $where = array('v.prefactura'=>1);
                if ($cliente>0) {
                    $where['v.idCliente']=$cliente;
                }
                if ($status==1) {
                    $where['v.e_entrega']=1;
                }elseif ($status==2) {
                    $where['v.e_entrega']=0;
                }
                    $where['v.activo']=1;
                if ($tipo>0) {
                    $where['v.tipotabla']=$tipo;
                }
                if($fecha!=''){
                    $where['v.fechaentrega']=$fecha;   
                }
                
                $this->db->where($where);
                     
            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->db->group_start();
                foreach($columns as $c){
                    $this->db->or_like($c,$search);
                }
                $this->db->group_end();
                
            }
            //==========================================
            $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
            $this->db->limit($params['length'],$params['start']);
            //echo $this->db->get_compiled_select();
            $query=$this->db->get();
            // print_r($query); die;
            return $query;
    }
    public function getListaVentasIncompletasfacturat($params){
            $tipo=$params['tipo'];
            $cliente=$params['cliente'];
            $status=$params['status'];
            $fecha=$params['fecha'];
                $columns = array( 
                    0=>'v.id',
                    1=>'v.empresa',
                    2=>'v.prefactura',
                    3=>"v.tabla",
                    4=>'v.vendedor',  
                    5=>'v.estatus',
                    6=>'v.reg',
                    7=>'v.fechaentrega',
                    8=>'v.e_entrega',
                    9=>'v.id_rh',
                    10=>'v.renta_rh',
                    11=>'v.tipotabla',
                    12=>'v.equipo',
                    13=>'v.consumibles',
                    14=>'v.refacciones',
                    15=>'v.poliza'

                );
                
                $select="";
                foreach ($columns as $c) {
                    $select.="$c, ";
                }
                $this->db->select('COUNT(*) as total');
                $this->db->from('vista_entregas_dia v ');
                $where = array('v.prefactura'=>1);
                if ($cliente>0) {
                    $where['v.idCliente']=$cliente;
                }
                if ($status==1) {
                    $where['v.statuspago']=1;
                }elseif ($status==2) {
                    $where['v.statuspago']=0;
                }
                    $where['v.activo']=1;
                if ($tipo>0) {
                    $where['v.tipotabla']=$tipo;
                }
                if($fecha!=''){
                    $where['v.fechaentrega']=$fecha;   
                }
                $this->db->where($where);
                     
            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->db->group_start();
                foreach($columns as $c){
                    $this->db->or_like($c,$search);
                }
                $this->db->group_end();
                
            }
            //==========================================
            
            $query=$this->db->get();

            return $query->row()->total;
    }
    */
    
    /*
    public function getListaVentasIncompletas($data){
        $cliente=$data['cliente'];
        $status=$data['status'];
        if ($cliente>0) {
            $wherecli=' and v.idCliente='.$cliente;
            $wherecli2=' and vc.idCliente='.$cliente;
        }else{
            $wherecli='';
            $wherecli2='';
        }
        if ($status==1) {
            $wherestatatus=' and v.statuspago=1';
            $wherestatatus2=' and vc.statuspago=1';
        }elseif ($status==2) {
            $wherestatatus=' and v.statuspago=0';
            $wherestatatus2=' and vc.statuspago=0';
        }else{
            $wherestatatus='';
            $wherestatatus2='';
        }
        $sql = "SELECT 
                    vc.estatus,
                    vc.combinadaId as id,
                    vc.prefactura,
                    vc.telefono,
                    vc.correo,
                    cli.empresa,
                    per.nombre as vendedor,
                    '1' as combinada
                FROM ventacombinada as vc
                inner JOIN clientes as cli on cli.id=vc.idCliente
                inner join personal as per on per.personalId=vc.id_personal
                where vc.estatus=1 $wherecli2 $wherestatatus2
                UNION
                SELECT
                    v.estatus,
                    v.id,
                    v.prefactura,
                    v.telefono,
                    v.correo,
                    cli.empresa,
                    per.nombre as vendedor,
                    '0' as combinada
                    FROM ventas as v 
                    inner join clientes as cli on cli.id=v.idCliente
                    left join personal as per on per.personalId=v.id_personal
                where v.combinada=0 $wherecli $wherestatatus";
        $query = $this->db->query($sql);
        return $query->result();
    }*/
    //===========================================================================
        /*
            se creo una vista para simplificar el codigo de la cual es la siguiente
            create view vista_ventas as
                SELECT 
                            vc.combinadaId as id,
                            vc.estatus,
                            vc.prefactura,
                            vc.telefono,
                            vc.correo,
                            cli.empresa,
                            per.nombre as vendedor,
                            '1' as combinada,
                            vc.idCliente,
                            vc.statuspago
                            vc.motivo,
                            vc.activo
                        FROM ventacombinada as vc
                        inner JOIN clientes as cli on cli.id=vc.idCliente
                        inner join personal as per on per.personalId=vc.id_personal
                        where vc.estatus=1
                        UNION
                        SELECT
                            v.id,
                            v.estatus,
                            v.prefactura,
                            v.telefono,
                            v.correo,
                            cli.empresa,
                            per.nombre as vendedor,
                            '0' as combinada,
                            v.idCliente,
                            v.statuspago
                            v.motivo,
                            v.activo
                            FROM ventas as v 
                            inner join clientes as cli on cli.id=v.idCliente
                            left join personal as per on per.personalId=v.id_personal
                        where v.combinada=0
        */
    function getListaVentasIncompletasasi($params){
        $cliente=$params['cliente'];
        $status=$params['status'];
        $tipoventa=$params['tipoventa'];
        $estatusventa=$params['estatusventa'];

        //$tipoview=$params['tipoview'];
        $idpersonal=$params['idpersonal'];
        $columns = array( 
            0=>'id', 
            1=>'empresa',
            2=>'prefactura',
            3=>'vendedor',
            4=>"estatus",
            5=>'reg',
            6=>'vencimiento',
            7=>'fechaentrega',
            8=>'combinada',
            9=>'facturas',
            10=>'idCliente',
            11=>'equipo',
            12=>'consumibles',
            13=>'refacciones',
            14=>'statuspago',
            15=>'totalcon',
            16=>'totalref',
            17=>'totalequ',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->DB3->select($select);
        $this->DB3->from('vista_ventas3');
            
            if ($cliente>0) {
                $this->DB3->where('idCliente',$cliente);
            }
            if($status==1){
                $this->DB3->where('statuspago',1);
            }elseif ($status==2) {
                $this->DB3->where('statuspago',0);
            }
            if ($tipoventa>0) {
                if ($tipoventa==1) {
                    $this->DB3->where('combinada',0);
                }else{
                    $this->DB3->where('combinada',1);
                }
            }
            $this->DB3->where('activo',$estatusventa);
            if($idpersonal>0){
                $this->DB3->where('id_personal',$idpersonal);
            }
            
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->DB3->group_start();
            foreach($columns as $c){
                $this->DB3->or_like($c,$search);
            }
            $this->DB3->group_end();  
        }            
        $this->DB3->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->DB3->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->DB3->get();
        // print_r($query); die;
        return $query;
    }
    function getListaVentasIncompletasasit($params){
        $cliente=$params['cliente'];
        $status=$params['status'];
        $tipoventa=$params['tipoventa'];
        $estatusventa=$params['estatusventa'];
        //log_message('error', 'cliente '.$cliente);
        //$tipoview=$params['tipoview'];
        $idpersonal=$params['idpersonal'];
        $columns = array( 
            0=>'id', 
            1=>'empresa',
            2=>'prefactura',
            3=>'vendedor',
            4=>"estatus",
            5=>'reg',
            6=>'vencimiento',
            7=>'fechaentrega',
            8=>'combinada',
            9=>'facturas',
            10=>'idCliente',
            11=>'equipo',
            12=>'consumibles',
            13=>'refacciones',
            14=>'statuspago',
        );
        
        $this->DB3->select('COUNT(*) as total');
        $this->DB3->from('vista_ventas3');
            if ($cliente>0) {
                $this->DB3->where('idCliente',$cliente);
            }
            if($status==1){
                $this->DB3->where('statuspago',1);
            }elseif ($status==2) {
                $this->DB3->where('statuspago',0);
            }
            if ($tipoventa>0) {
                if ($tipoventa==1) {
                    $this->DB3->where('combinada',0);
                }else{
                    $this->DB3->where('combinada',1);
                }
            }
            $this->DB3->where('activo',$estatusventa);
            if($idpersonal>0){
                $this->DB3->where('id_personal',$idpersonal);
            }
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->DB3->group_start();
            foreach($columns as $c){
                $this->DB3->or_like($c,$search);
            }
            $this->DB3->group_end();  
        }            
        //$this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        ///$this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->DB3->get();
        // print_r($query); die;
        return $query->row()->total;

    }
    //==============================================================================
        /*
            para las funciones getlistaservicios y getlistaserviciost se creo una vista de la siguiente consulta para simplificarlo
            si se necesita modificar la vista borrar la anterior y cargarla nuevamente DROP VIEW ` ventas_servicios `
            CREATE VIEW  ventas_servicios AS 
                SELECT ac.asignacionId,ac.tipo,cli.empresa, cli.email,cli.id
                FROM asignacion_ser_avc as ac 
                inner join contrato as con on con.idcontrato=ac.asignacionId
                inner join rentas as ren on ren.id=con.idRenta
                inner join clientes as cli on cli.id=ren.idCliente
                WHERE ac.activo=1 and ac.tipo=1
                UNION
                SELECT ac.asignacionId,ac.tipo,cli.empresa, cli.email,cli.id
                FROM asignacion_ser_avc as ac 
                inner join polizasCreadas as pol on pol.id=ac.asignacionId
                inner join clientes as cli on cli.id=pol.idCliente
                WHERE ac.activo=1 and ac.tipo=2
                UNION
                SELECT ac.asignacionId,ac.tipo,cli.empresa, cli.email,cli.id
                FROM asignacion_ser_avc as ac 
                inner join asignacion_ser_cliente_a as asig on asig.asignacionId=ac.asignacionId
                inner join clientes as cli on cli.id=asig.clienteId
                WHERE ac.activo=1 and ac.tipo=3
                UNION
                SELECT ac.asignacionId,ac.tipo,cli.empresa, cli.email,cli.id
                FROM asignacion_ser_avr as ac 
                inner join contrato as con on con.idcontrato=ac.asignacionId
                inner join rentas as ren on ren.id=con.idRenta
                inner join clientes as cli on cli.id=ren.idCliente
                WHERE ac.activo=1 and ac.tipo=1
                UNION
                SELECT ac.asignacionId,ac.tipo,cli.empresa, cli.email,cli.id
                FROM asignacion_ser_avr as ac 
                inner join polizasCreadas as pol on pol.id=ac.asignacionId
                inner join clientes as cli on cli.id=pol.idCliente
                WHERE ac.activo=1 and ac.tipo=2
                UNION
                SELECT ac.asignacionId,ac.tipo,cli.empresa, cli.email,cli.id
                FROM asignacion_ser_avr as ac 
                inner join clientes as cli on cli.id=ac.asignacionId
                WHERE ac.activo=1 and ac.tipo=3
                --------------------------------------------------------------------------
                DROP VIEW ` ventas_servicios_e `
                CREATE VIEW  ventas_servicios_e AS 
                    SELECT asig.asignacionId, 3 as tipo, cli.empresa, cli.email,cli.id,asig.tiposervicio
                    from asignacion_ser_cliente_a as asig
                    inner join clientes as cli on cli.id=asig.clienteId
                    where asig.status!=3
                    UNION
                    SELECT asic.asignacionId,1 as tipo, cli.empresa, cli.email,cli.id,asic.tiposervicio
                    from asignacion_ser_contrato_a as asic
                    inner join contrato as con on con.idcontrato=asic.contratoId
                    inner join rentas as ren on ren.id=con.idRenta
                    inner join clientes as cli on cli.id=ren.idCliente
                    inner join asignacion_ser_contrato_a_e as asice on asice.asignacionId=asic.asignacionId
                    WHERE asice.status!=3
                    UNION
                    SELECT asip.asignacionId,2 as tipo,cli.empresa, cli.email,cli.id,asip.tiposervicio
                    from asignacion_ser_poliza_a as asip
                    inner join polizasCreadas as pol on pol.id=asip.polizaId
                    inner join clientes as cli on cli.id=pol.idCliente
                    inner join asignacion_ser_poliza_a_e as asipe on asipe.asignacionId=asip.asignacionId
                    WHERE asipe.status!=3
        */
        function getlistaservicios($params){
            $cliente=$params['cliente'];
            $tipo_servicio=$params['tipo_servicio'];
            $activo=$params['activo'];
            $fechainicial_reg=$params['fechainicial_reg'];
            $columns = array(
                0=>'ve.asignacionId',
                1=>'ve.empresa',
                2=>'pre.prefacturaId',
                3=>'ve.tipo',
                4=>'ve.id',
                5=>'ve.activo',
                6=>'ve.motivoeliminado',
                7=>'ve.reg',
            );
            $select="";
            foreach ($columns as $c) {
                $select.="$c, ";
            }
            $this->db->select($select);
            $this->db->from('ventas_servicios_e ve');
            $this->db->join('prefactura_servicio pre', 'pre.asignacionId=ve.asignacionId and pre.tipo=ve.tipo','left');
            if ($cliente>0) {
                $this->db->where(array('id'=>$cliente));
            }
            if($tipo_servicio==1) {
                $this->db->where(array('ve.tipo'=>1));
            }else if($tipo_servicio==2) {
                $this->db->where(array('ve.tipo'=>2));
            }else if($tipo_servicio==3) {
                $this->db->where(array('ve.tipo'=>3));
            }
            if($fechainicial_reg!='') {
                $this->db->where(array('ve.reg >='=>$fechainicial_reg));
            }
            $this->db->where(array('ve.tiposervicio'=>1,'ve.activo'=>$activo));
            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->db->group_start();
                foreach($columns as $c){
                    $this->db->or_like($c,$search);
                }
                $this->db->group_end();  
            }            
            $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
            $this->db->limit($params['length'],$params['start']);
            //echo $this->db->get_compiled_select();
            $query=$this->db->get();
            // print_r($query); die;
            return $query;
        }
        function getlistaserviciost($params){
            $cliente=$params['cliente'];
            $tipo_servicio=$params['tipo_servicio'];
            $activo=$params['activo'];
            $fechainicial_reg=$params['fechainicial_reg'];
            //log_message('error', 'cliente '.$cliente);
            $columns = array(
                0=>'asignacionId',
                1=>'tipo',
                2=>'empresa',
                3=>'id',
                4=>'reg',
            );
            
            $this->db->select('COUNT(*) as total');
            $this->db->from('ventas_servicios_e');
            if ($cliente>0) {
                $this->db->where(array('id'=>$cliente));
            }
            if($tipo_servicio==1) {
                $this->db->where(array('tipo'=>1));
            }else if($tipo_servicio==2) {
                $this->db->where(array('tipo'=>2));
            }else if($tipo_servicio==3) {
                $this->db->where(array('tipo'=>3));
            }
            if($fechainicial_reg!='') {
                $this->db->where(array('reg >='=>$fechainicial_reg));
            }
            $this->db->where(array('tiposervicio'=>1,'activo'=>$activo));
            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->db->group_start();
                foreach($columns as $c){
                    $this->db->or_like($c,$search);
                }
                $this->db->group_end();  
            }            
            //$this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
            ///$this->db->limit($params['length'],$params['start']);
            //echo $this->db->get_compiled_select();
            $query=$this->db->get();
            // print_r($query); die;
            return $query->row()->total;
        }
    //================================================================================
        function datosserviciosrefacciones($id,$tipo){
            $strq='SELECT ar.asId,ar.cantidad, ref.id,ref.nombre,ref.codigo,ar.costo
                    FROM asignacion_ser_avr as ar
                    inner JOIN refacciones as ref on ref.id=ar.refacciones
                    WHERE ar.activo=1 and ar.asignacionId='.$id.' AND ar.tipo='.$tipo;
            $query = $this->db->query($strq);
            //$this->db->close();
            return $query;
        }
        function datosserviciosconsumibles($id,$tipo){
            $strq='SELECT ac.asId,ac.cantidad,cons.id,cons.modelo,cons.parte,ac.costo
                FROM asignacion_ser_avc as ac
                inner JOIN consumibles as cons on cons.id=ac.consumible
                WHERE ac.activo=1 and ac.asignacionId='.$id.' and ac.tipo='.$tipo;
            $query = $this->db->query($strq);
            //$this->db->close();
            return $query;
        }
        function datosserviciosaccesorios($id,$tipo){
            $strq='SELECT aa.asId,aa.cantidad,acce.id,acce.nombre,acce.no_parte,aa.costo
                FROM asignacion_ser_ava as aa
                inner JOIN catalogo_accesorios as acce on acce.id=aa.accessorio
                WHERE aa.activo=1 and aa.asignacionId='.$id.' and aa.tipo='.$tipo;
            $query = $this->db->query($strq);
            //$this->db->close();
            return $query;
        }
    //================================================================================
        function getlistadosoloventas($params){
            $cliente=$params['cliente'];
            $status=$params['status'];
            $tipoventa=$params['tipoventa'];
            $estatusventa=$params['estatusventa'];
            $rfc=$params['rfc'];
            if(isset($params['pre'])){
                $pre=$params['pre'];
            }else{
                $pre='x';    
            }
            

            //$tipoview=$params['tipoview'];
            $idpersonal=$params['idpersonal'];
            $perfilid=$params['perfilid'];
            $fechainicial_reg=$params['fechainicial_reg'];
            if(isset($params['fechafinal_reg'])){
                $fechafinal_reg=$params['fechafinal_reg'];
            }else{
                $fechafinal_reg='';
            }
            $emp=$params['emp'];
            $filf=$params['filf'];
            $columns = array(
                0=>'v.id',
                1=>'v.idCliente',
                2=>'v.prefactura',
                3=>'v.id_personal',
                4=>'v.statuspago',
                5=>'v.reg',
                6=>'pre.vencimiento',
                7=>'v.fechaentrega',
                8=>'v.tipov',
                9=>'v.total_general',
                10=>'v.estatus',
                11=>'per.nombre as vendedor',
                12=>'0 as combinada',
                13=>'v.motivo',
                14=>'v.activo',
                15=>'cli.empresa',
                16=>'0 as equipo',
                17=>'0 as consumibles',
                18=>'0 as refacciones',
                /*
                19=>'(GROUP_CONCAT(fac.Folio)
                        FROM `factura_venta` as ven 
                        INNER JOIN f_facturas as fac on fac.FacturasId=ven.facturaId 
                        WHERE ven.combinada=0 and ven.ventaId=v.id and fac.Estado=1) as facturas',
                */
                19=>"GROUP_CONCAT(CASE
        WHEN fac.Estado > 0 THEN
            CASE 
                WHEN fac.Estado = 2 THEN 'ST'
                ELSE ''
            END
        ELSE ''
    END,' ',fac.Folio) as facturas",
                20=>'IF(v.total_general>0, "SI", (SELECT sum(vhdc.piezas*vhdc.precioGeneral) as total FROM ventas_has_detallesConsumibles as vhdc WHERE vhdc.idVentas=v.id)) as totalcon',
                21=>'IF(v.total_general>0, "SI",(SELECT sum(vhdr.piezas*vhdr.precioGeneral) as total FROM ventas_has_detallesRefacciones as vhdr WHERE vhdr.idVentas=v.id)) as totalref',
                22=>'IF(v.total_general>0, "SI",(SELECT sum(vhde.cantidad*vhde.precio) AS total FROM ventas_has_detallesEquipos AS vhde WHERE vhde.idVenta=v.id )) as totalequ',
                23=>'IF(v.total_general>0, "SI",(SELECT sum(vhdac.cantidad*vhdac.costo) AS total FROM ventas_has_detallesEquipos_accesorios AS vhdac WHERE vhdac.id_venta=v.id )) as totalacc',
                24=>'IF(v.total_general>0, "SI",(SELECT sum(vhdc.cantidad*vhdc.costo_toner) as total FROM ventas_has_detallesequipos_consumible as vhdc WHERE vhdc.idVenta=v.id)) as totalcon2',
                25=>'v.e_entrega',
                26=>'v.comentario',
                27=>'v.garantia',
                28=>'v.siniva',
                29=>'v.requ_fac'
                
            );
            $columnsss = array(
                0=>'v.id',
                1=>'v.prefactura',
                2=>'v.reg',
                3=>'pre.vencimiento',
                4=>'v.fechaentrega',
                5=>'per.nombre',  
                6=>'v.motivo',
                7=>'cli.empresa',
                8=>'fac.Folio',
                9=>'v.total_general',
            );
            if($filf==1){
                $columnsss = array(
                    0=>'v.folio_venta'
                );
            }
            $select="";
            foreach ($columns as $c) {
                $select.="$c, ";
            }
            $this->db->select($select);
            $this->db->from('ventas v');
            $this->db->join('clientes cli', 'cli.id=v.idCliente');
            $this->db->join('personal per', 'per.personalId=v.id_personal','left');
            $this->db->join('prefactura pre', 'pre.ventaId=v.id and pre.tipo=0','left');
            $this->db->join('cliente_has_datos_fiscales clif ', 'clif.id=pre.rfc_id','left');
            
            //========================================================================
            $this->db->join('factura_venta ven', 'ven.combinada=0 and ven.ventaId=v.id','left');
            $this->db->join('f_facturas fac', 'fac.FacturasId=ven.facturaId and fac.Estado=1','left');

            $this->db->group_by("v.id");
            //========================================================================
                if ($cliente>0) {
                    $this->db->where('v.idCliente',$cliente);
                }
                if($status==1){
                    $this->db->where('v.estatus',3);
                }elseif ($status==2) {
                    $this->db->where('v.estatus',1);
                }
                
                $this->db->where('v.combinada',0);
                    
                $this->db->where('v.activo',$estatusventa);
                if($idpersonal>0){
                    $this->db->where('v.id_personal',$idpersonal);
                }
                if($fechainicial_reg!=''){
                    $this->db->where('v.reg >=',$fechainicial_reg);
                }
                if($fechafinal_reg!=''){
                    $this->db->where('v.reg <=',$fechafinal_reg.' 23:59:59');
                }
                if($emp>0){
                    $this->db->where('v.tipov',$emp);
                }
                if($rfc!=''){
                    $this->db->where('clif.rfc',$rfc);
                }
                if($pre!='x'){
                    $this->db->where('v.prefactura',$pre);
                }
                
            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->db->group_start();
                foreach($columnsss as $c){
                    $this->db->or_like($c,$search);
                }
                $this->db->group_end();  
            }            
            $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
            $this->db->limit($params['length'],$params['start']);
            //echo $this->db->get_compiled_select();
            $query=$this->db->get();
            // print_r($query); die;
            return $query;
        }
        function getlistadosoloventast($params){
            $cliente=$params['cliente'];
            $status=$params['status'];
            $tipoventa=$params['tipoventa'];
            $estatusventa=$params['estatusventa'];
            $rfc=$params['rfc'];
            if(isset($params['pre'])){
                $pre=$params['pre'];
            }else{
                $pre='x';    
            }
            //log_message('error', 'cliente '.$cliente);
            //$tipoview=$params['tipoview'];
            $idpersonal=$params['idpersonal'];
            $perfilid=$params['perfilid'];
            $fechainicial_reg=$params['fechainicial_reg'];
            if(isset($params['fechafinal_reg'])){
                $fechafinal_reg=$params['fechafinal_reg'];
            }else{
                $fechafinal_reg='';
            }
            $emp=$params['emp'];
            $filf=$params['filf'];
            $columns = array(
                0=>'v.id',
                1=>'v.estatus',
                2=>'v.prefactura',
                3=>'v.reg',
                4=>'per.nombre',
                5=>'v.motivo',
                6=>'cli.empresa',
                /*
                9=>'(SELECT GROUP_CONCAT(fac.Folio)
                        FROM `factura_venta` as ven 
                        INNER JOIN f_facturas as fac on fac.FacturasId=ven.facturaId 
                        WHERE ven.combinada=0 and ven.ventaId=v.id)',
                */
                7=>'pre.vencimiento',
                8=>'v.fechaentrega'
                /*
                11=>'((SELECT IFNULL(sum(vhdc.piezas*vhdc.precioGeneral),0) as total 
                        FROM ventas_has_detallesConsumibles as vhdc 
                        WHERE vhdc.idVentas=v.id)+(SELECT IFNULL(sum(vhdr.piezas*vhdr.precioGeneral),0) as total 
                        FROM ventas_has_detallesRefacciones as vhdr 
                        WHERE vhdr.idVentas=v.id)+(SELECT IFNULL(sum(vhde.cantidad*vhde.precio),0) AS total 
                        FROM ventas_has_detallesEquipos AS vhde 
                        WHERE vhde.idVenta=v.id ))'
                */
            );
            $columnsss=$columns;
            if($perfilid==1){
                $columnsss = array(
                    0=>'v.id',
                    1=>'v.reg',
                    2=>'cli.empresa'
                );
            }
            if($filf==1){
                $columnsss = array(
                    0=>'v.folio_venta'
                );
            }
            $this->db->select('COUNT(*) as total');
            $this->db->from('ventas v');
            $this->db->join('clientes cli', 'cli.id=v.idCliente');
            $this->db->join('personal per', 'per.personalId=v.id_personal','left');
            $this->db->join('prefactura pre', 'pre.ventaId=v.id and pre.tipo=0','left');
            $this->db->join('cliente_has_datos_fiscales clif ', 'clif.id=pre.rfc_id','left');

                if ($cliente>0) {
                    $this->db->where('v.idCliente',$cliente);
                }
                if($status==1){
                    $this->db->where('v.estatus',3);
                }elseif ($status==2) {
                    $this->db->where('v.estatus',1);
                }
                
                $this->db->where('v.combinada',0);
                    
                $this->db->where('v.activo',$estatusventa);
                if($idpersonal>0){
                    $this->db->where('v.id_personal',$idpersonal);
                }
                if($fechainicial_reg!=''){
                    $this->db->where('v.reg >=',$fechainicial_reg);
                }
                if($fechafinal_reg!=''){
                    $this->db->where('v.reg <=',$fechafinal_reg.' 23:59:59');
                }
                if($emp>0){
                    $this->db->where('v.tipov',$emp);
                }
                if($rfc!=''){
                    $this->db->where('clif.rfc',$rfc);
                }
                if($pre!='x'){
                    $this->db->where('v.prefactura',$pre);
                }
            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->db->group_start();
                foreach($columnsss as $c){
                    $this->db->or_like($c,$search);
                }
                $this->db->group_end();  
            }            
            //$this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
            ///$this->db->limit($params['length'],$params['start']);
            //echo $this->db->get_compiled_select();
            $query=$this->db->get();
            // print_r($query); die;
            return $query->row()->total;
        }
        function getlistadosoloventasc($params){
            $cliente=$params['cliente'];
            $status=$params['status'];
            $tipoventa=$params['tipoventa'];
            $estatusventa=$params['estatusventa'];
            $rfc=$params['rfc'];
            
            if(isset($params['pre'])){
                $pre=$params['pre'];
            }else{
                $pre='x';
            }

            //$tipoview=$params['tipoview'];
            $idpersonal=$params['idpersonal'];
            $fechainicial_reg=$params['fechainicial_reg'];
            if(isset($params['fechafinal_reg'])){
                $fechafinal_reg=$params['fechafinal_reg'];
            }else{
                $fechafinal_reg='';
            }
            $emp=$params['emp'];
            $filf=$params['filf'];
            $columns = array(
                    0=>'vc.combinadaId as id',
                    1=>'vc.estatus',
                    2=>'vc.prefactura',
                    3=>'vc.telefono',
                    4=>'vc.correo',
                    5=>'vc.reg',
                    6=>'cli.empresa',
                    7=>'per.nombre as vendedor',
                    8=>'1 as combinada',
                    9=>'vc.idCliente',
                    10=>'vc.statuspago',
                    11=>'vc.motivo',
                    12=>'vc.activo',
                    13=>'vc.equipo',
                    14=>'vc.consumibles',
                    15=>'vc.refacciones',
                    16=>'vc.id_personal',
                    /*
                    17=>'(SELECT GROUP_CONCAT(fac.Folio) FROM `factura_venta` as ven 
                            INNER JOIN f_facturas as fac on fac.FacturasId=ven.facturaId 
                            WHERE ven.combinada=1 and ven.ventaId=vc.combinadaId and fac.Estado=1
                        ) as facturas',
                    */
                    17=>"GROUP_CONCAT(CASE
        WHEN fac.Estado > 0 THEN
            CASE 
                WHEN fac.Estado = 2 THEN 'ST'
                ELSE ''
            END
        ELSE ''
    END,' ',fac.Folio) as facturas",
                    18=>'pre.vencimiento',
                    19=>'vc.fechaentrega',
                    20=>'IF(vc.total_general>0, "SI",(SELECT sum(vhdc.piezas*vhdc.precioGeneral) as total FROM ventas_has_detallesConsumibles as vhdc 
                            WHERE vhdc.idVentas=vc.consumibles)) as totalcon',
                    21=>'IF(vc.total_general>0, "SI",(SELECT sum(vhdr.totalGeneral) as total FROM ventas_has_detallesRefacciones as vhdr 
                            WHERE vhdr.idVentas=vc.refacciones)) as totalref',
                    22=>'IF(vc.total_general>0, "SI",(SELECT sum(vhde.cantidad*vhde.precio) AS total FROM ventas_has_detallesEquipos AS vhde 
                            WHERE vhde.idVenta=vc.equipo)) as totalequ',
                    23=>'IF(vc.total_general>0, "SI",(SELECT SUM(vpd.cantidad*(IF(vpd.precio_local>0, vpd.precio_local, 0)+IF(vpd.precio_semi>0, vpd.precio_semi, 0)+IF(vpd.precio_foraneo>0, vpd.precio_foraneo, 0)+IF(vpd.precio_especial>0, vpd.precio_especial, 0))) as total FROM polizasCreadas_has_detallesPoliza as vpd WHERE vpd.idPoliza=vc.poliza)) as totalpoli',
                    24=>'IF(vc.total_general>0, "SI",(SELECT sum(vhdc2.cantidad*vhdc2.costo_toner) as total 
                        FROM ventas_has_detallesequipos_consumible as vhdc2 
                        WHERE vhdc2.idVenta=vc.equipo)) as totalcon2',
                    25=>'vc.e_entrega',
                    26=>'IF(vc.total_general>0, "SI",(SELECT sum(vhdac.cantidad*vhdac.costo) AS total 
                        FROM ventas_has_detallesEquipos_accesorios AS vhdac 
                        WHERE vhdac.id_venta=vc.equipo )) as totalacc',
                    27=>'vc.comentario',
                    28=>'vc.tipov',
                    29=>'vc.total_general',
                    30=>'vc.garantia',
                    31=>'vc.siniva',
                    32=>'vc.requ_fac'
                );
            $columnsss = array(
                0=>'vc.combinadaId',
                1=>'cli.empresa',
                2=>'vc.prefactura',
                3=>'per.nombre',
                4=>'vc.statuspago',
                5=>'vc.reg',
                6=>'pre.vencimiento',
                7=>'vc.fechaentrega',
                8=>'vc.motivo',
                9=>'vc.equipo',
                10=>'vc.consumibles',
                11=>'vc.refacciones',
                12=>'vc.tipov',
                13=>'vc.total_general',
                14=>'fac.Folio',

                /*
                15=>'(
                        (SELECT IFNULL(sum(vhdc.totalGeneral),0) as total FROM ventas_has_detallesConsumibles as vhdc WHERE vhdc.idVentas=vc.consumibles)+
                        (SELECT IFNULL(sum(vhdr.totalGeneral),0) as total FROM ventas_has_detallesRefacciones as vhdr WHERE vhdr.idVentas=vc.refacciones)+
                        (SELECT IFNULL(sum(vhde.cantidad*vhde.precio),0) AS total FROM ventas_has_detallesEquipos AS vhde WHERE vhde.idVenta=vc.equipo)
                        )'
                */
                /*
                15=>'(SELECT GROUP_CONCAT(fac.Folio) FROM `factura_venta` as ven 
                        INNER JOIN f_facturas as fac on fac.FacturasId=ven.facturaId 
                        WHERE ven.combinada=1 and ven.ventaId=vc.combinadaId
                    ) as facturas',
                */
                
            );
            if($filf==1){
                $columnsss = array(
                    0=>'vc.folio_venta'
                );
            }
            $select="";
            foreach ($columns as $c) {
                $select.="$c, ";
            }
            $this->db->select($select);
            $this->db->from('ventacombinada vc');
            $this->db->join('clientes cli', 'cli.id=vc.idCliente');
            $this->db->join('personal per', 'per.personalId=vc.id_personal','left');
            $this->db->join('prefactura pre', 'pre.ventaId=vc.combinadaId and pre.tipo=3','left');
            $this->db->join('cliente_has_datos_fiscales clif ', 'clif.id=pre.rfc_id','left');
            //========================================================================
            $this->db->join('factura_venta ven', 'ven.combinada=1 and ven.ventaId=vc.combinadaId','left');
            $this->db->join('f_facturas fac', 'fac.FacturasId=ven.facturaId and fac.Estado=1','left');

            $this->db->group_by("vc.combinadaId");
            //========================================================================
                if ($cliente>0) {
                    $this->db->where('vc.idCliente',$cliente);
                }
                if($status==1){
                    $this->db->where('vc.estatus',3);
                }elseif ($status==2) {
                    $this->db->where('vc.estatus',1);
                }
                
                //$this->db->where('combinada',0);
                    
                $this->db->where('vc.activo',$estatusventa);
                if($idpersonal>0){
                    $this->db->where('vc.id_personal',$idpersonal);
                }
                if($fechainicial_reg!=''){
                    $this->db->where('vc.reg >=',$fechainicial_reg);
                }
                if($fechafinal_reg!=''){
                    $this->db->where('vc.reg <=',$fechafinal_reg.' 23:59:59');
                }
                if($emp>0){
                    $this->db->where('vc.tipov',$emp);
                }
                if($rfc!=''){
                    $this->db->where('clif.rfc',$rfc);
                }
                if($pre!='x'){
                    $this->db->where('vc.prefactura',$pre);
                }
                
            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->db->group_start();
                foreach($columnsss as $c){
                    $this->db->or_like($c,$search);
                }
                $this->db->group_end();  
            }            
            $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
            $this->db->limit($params['length'],$params['start']);
            //echo $this->db->get_compiled_select();
            $query=$this->db->get();
            // print_r($query); die;
            return $query;
        }
        function getlistadosoloventasct($params){
            $cliente=$params['cliente'];
            $status=$params['status'];
            $tipoventa=$params['tipoventa'];
            $estatusventa=$params['estatusventa'];
            $rfc=$params['rfc'];
            if(isset($params['pre'])){
                $pre=$params['pre'];
            }else{
                $pre='x';    
            }
            //log_message('error', 'cliente '.$cliente);
            //$tipoview=$params['tipoview'];
            $idpersonal=$params['idpersonal'];
            $fechainicial_reg=$params['fechainicial_reg'];
            if(isset($params['fechafinal_reg'])){
                $fechafinal_reg=$params['fechafinal_reg'];
            }else{
                $fechafinal_reg='';
            }
            $emp=$params['emp'];
            $filf=$params['filf'];
            $columns = array(
                0=>'vc.combinadaId',
                1=>'vc.reg',
                2=>'cli.empresa',
                3=>'per.nombre',
                4=>'vc.motivo',
                5=>'vc.equipo',
                6=>'vc.consumibles',
                7=>'vc.refacciones',
                8=>'pre.vencimiento',
                9=>'vc.fechaentrega'

                
                /*
                15=>'(
                        (SELECT IFNULL(sum(vhdc.totalGeneral),0) as total FROM ventas_has_detallesConsumibles as vhdc WHERE vhdc.idVentas=vc.consumibles)+
                        (SELECT IFNULL(sum(vhdr.totalGeneral),0) as total FROM ventas_has_detallesRefacciones as vhdr WHERE vhdr.idVentas=vc.refacciones)+
                        (SELECT IFNULL(sum(vhde.cantidad*vhde.precio),0) AS total FROM ventas_has_detallesEquipos AS vhde WHERE vhde.idVenta=vc.equipo)
                        )'
                */
                /*
                15=>'(SELECT GROUP_CONCAT(fac.Folio) FROM `factura_venta` as ven 
                        INNER JOIN f_facturas as fac on fac.FacturasId=ven.facturaId 
                        WHERE ven.combinada=1 and ven.ventaId=vc.combinadaId
                    ) as facturas',
                */
            );
            
            $this->db->select('COUNT(*) as total');
            $this->db->from('ventacombinada vc');
            $this->db->join('clientes cli', 'cli.id=vc.idCliente');
            $this->db->join('personal per', 'per.personalId=vc.id_personal','left');
            $this->db->join('prefactura pre', 'pre.ventaId=vc.combinadaId and pre.tipo=3','left');
            $this->db->join('cliente_has_datos_fiscales clif ', 'clif.id=pre.rfc_id','left');

                if ($cliente>0) {
                    $this->db->where('vc.idCliente',$cliente);
                }
                if($status==1){
                    $this->db->where('vc.estatus',3);
                }elseif ($status==2) {
                    $this->db->where('vc.estatus',1);
                }
                
                //$this->db->where('v.combinada',0);
                    
                $this->db->where('vc.activo',$estatusventa);
                if($idpersonal>0){
                    $this->db->where('vc.id_personal',$idpersonal);
                }
                if($fechainicial_reg!=''){
                    $this->db->where('vc.reg >=',$fechainicial_reg);
                }
                if($fechafinal_reg!=''){
                    $this->db->where('vc.reg <=',$fechafinal_reg.' 23:59:59');
                }
                if($emp>0){
                    $this->db->where('vc.tipov',$emp);
                }
                if($filf==1){
                    $columns = array(
                        0=>'vc.folio_venta'
                    );
                }
                if($rfc!=''){
                    $this->db->where('clif.rfc',$rfc);
                }
                if($pre!='x'){
                    $this->db->where('vc.prefactura',$pre);
                }
            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->db->group_start();
                foreach($columns as $c){
                    $this->db->or_like($c,$search);
                }
                $this->db->group_end();  
            }            
            //$this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
            ///$this->db->limit($params['length'],$params['start']);
            //echo $this->db->get_compiled_select();
            $query=$this->db->get();
            // print_r($query); die;
            return $query->row()->total;
        }
        function sqlgarantias($params){
            $cliente=$params['cliente'];
            $status=$params['status'];
            $tipoventa=$params['tipoventa'];
            $estatusventa=$params['estatusventa'];

            //$tipoview=$params['tipoview'];
            $idpersonal=$params['idpersonal'];
            $perfilid=$params['perfilid'];
            $fechainicial_reg=$params['fechainicial_reg'];
            if(isset($params['fechafinal_reg'])){
                $fechafinal_reg=$params['fechafinal_reg'];
            }else{
                $fechafinal_reg='';
            }
            if(isset($params['rfc'])){
                $rfc=$params['rfc'];
                if($rfc!=''){
                    $w_rfc=" and clif.rfc='$rfc' ";
                }else{
                    $w_rfc='';    
                }
            }else{
                $w_rfc='';
            }
            $emp=$params['emp'];
            $filf=$params['filf'];
            $pagosview=$params['pagosview'];

            //===========================================================
                if ($cliente>0) {
                    $cliente_where=" and v.idCliente='$cliente' ";
                    $cliente_wherec=" and vc.idCliente='$cliente' ";
                    $cliente_wherep=" and pc.idCliente='$cliente' ";
                }else{
                    $cliente_where='';
                    $cliente_wherec='';
                    $cliente_wherep='';
                }
                if($status==1){
                    $status_where=" and v.estatus=3 ";
                    $status_wherec=" and vc.estatus=3 ";
                    $status_wherep=" and pc.estatus=3 ";
                }elseif ($status==2) {
                    $status_where=" and v.estatus=1 ";
                    $status_wherec=" and vc.estatus=1 ";
                    $status_wherep=" and pc.estatus=1 ";
                }else{
                    $status_where="";
                    $status_wherec="";
                    $status_wherep='';
                }
                if($idpersonal>0){
                    $idpersonal_where=" and v.id_personal='$idpersonal' ";
                    $idpersonal_wherec=" and vc.id_personal='$idpersonal' ";
                    $idpersonal_wherep=" and pc.id_personal='$idpersonal' ";
                }else{
                    $idpersonal_where="";
                    $idpersonal_wherec="";
                    $idpersonal_wherep='';
                }
                if($fechainicial_reg!=''){
                    $fechainicial_reg_where=" and v.reg >= '$fechainicial_reg' ";
                    $fechainicial_reg_wherec=" and vc.reg >= '$fechainicial_reg' ";
                    $fechainicial_reg_wherep=" and pc.reg >= '$fechainicial_reg' ";
                }else{
                    $fechainicial_reg_where="";
                    $fechainicial_reg_wherec="";
                    $fechainicial_reg_wherep='';
                }
                if($fechafinal_reg!=''){
                    $fechafinal_reg_where=" and v.reg <= '$fechafinal_reg 23:59:59' ";
                    $fechafinal_reg_wherec=" and vc.reg <= '$fechafinal_reg 23:59:59' ";
                    $fechafinal_reg_wherep=" and pc.reg <= '$fechafinal_reg 23:59:59' ";
                }else{
                    $fechafinal_reg_where="";
                    $fechafinal_reg_wherec="";
                    $fechafinal_reg_wherep='';
                }
                if($emp>0){
                    $emp_where=" and v.tipov='$emp' ";
                    $emp_wherec=" and vc.tipov='$emp' ";
                    $emp_wherep=" and pc.tipov='$emp' ";
                }else{
                    $emp_where="";
                    $emp_wherec="";
                    $emp_wherep='';
                }
            //=================================================================
            $sql="(";
                $sql.="(";
                    $sql.="SELECT 
                            v.id, 
                            v.idCliente, 
                            v.prefactura, 
                            v.id_personal, 
                            v.statuspago, 
                            v.reg, 
                            pre.vencimiento, 
                            v.fechaentrega, 
                            v.tipov, 
                            v.total_general, 
                            v.estatus, 
                            per.nombre as vendedor, 
                            0 as combinada, 
                            v.motivo, 
                            v.activo, 
                            cli.empresa, 
                            0 as equipo, 
                            0 as consumibles, 
                            0 as refacciones, 
                            GROUP_CONCAT(fac.Folio) as facturas, 
                            0 as totalcon, 
                            0 as totalref, 
                            0 as totalequ, 
                            0 as totalpoli,
                            0 as totalacc, 
                            0 as totalcon2, 
                            v.e_entrega, 
                            v.comentario, 
                            v.garantia,
                            v.folio_venta,
                            0 as viewcont,
                            0 as tipopagofacman,
                            v.siniva,
                            v.requ_fac
                            FROM ventas as v
                            JOIN clientes as cli ON cli.id=v.idCliente
                            LEFT JOIN personal as per ON per.personalId=v.id_personal
                            LEFT JOIN prefactura as pre ON pre.ventaId=v.id and pre.tipo=0
                            LEFT join cliente_has_datos_fiscales as clif on clif.id=pre.rfc_id
                            LEFT JOIN factura_venta as ven ON ven.combinada=0 and ven.ventaId=v.id
                            LEFT JOIN f_facturas as fac ON fac.FacturasId=ven.facturaId and fac.Estado=1
                            WHERE v.combinada=0 and v.activo=$estatusventa $cliente_where $status_where $idpersonal_where $fechainicial_reg_where $fechafinal_reg_where $emp_where $w_rfc
                            GROUP BY v.id";
                $sql.=")";
                $sql.=" UNION ";
                $sql.="(";
                    $sql.="SELECT 
                            vc.combinadaId as id, 
                            vc.idCliente,
                            vc.prefactura,
                            vc.id_personal,
                            vc.statuspago,
                            vc.reg,
                            pre.vencimiento,
                            vc.fechaentrega, 
                            vc.tipov,
                            vc.total_general,
                            vc.estatus, 
                            per.nombre as vendedor, 
                            1 as combinada,
                            vc.motivo,
                            vc.activo, 
                            cli.empresa,
                            vc.equipo, 
                            vc.consumibles,
                            vc.refacciones,
                            GROUP_CONCAT(fac.Folio) as facturas, 
                            0 as totalcon, 
                            0 as totalref, 
                            0 as totalequ, 
                            0 as totalpoli, 
                            0 as totalacc,
                            0 as totalcon2, 
                            vc.e_entrega,
                            vc.comentario, 
                            vc.garantia,
                            vc.folio_venta,
                            0 as viewcont,
                            0 as tipopagofacman,
                            vc.siniva,
                            vc.requ_fac
                            FROM ventacombinada as vc
                            JOIN clientes as cli ON cli.id=vc.idCliente
                            LEFT JOIN personal as per ON per.personalId=vc.id_personal
                            LEFT JOIN prefactura as pre ON pre.ventaId=vc.combinadaId and pre.tipo=3
                            LEFT join cliente_has_datos_fiscales as clif on clif.id=pre.rfc_id
                            LEFT JOIN factura_venta as ven ON ven.combinada=1 and ven.ventaId=vc.combinadaId
                            LEFT JOIN f_facturas as fac ON fac.FacturasId=ven.facturaId and fac.Estado=1
                            WHERE vc.activo=$estatusventa $cliente_wherec $status_wherec $idpersonal_wherec $fechainicial_reg_wherec $fechafinal_reg_wherec $emp_wherec $w_rfc
                            GROUP BY vc.combinadaId";
                $sql.=")";
                $sql.=" UNION ";
                $sql.="(";
                    $sql.="SELECT 
                            pc.id, 
                            pc.idCliente,
                            pc.prefactura,
                            pc.id_personal,
                            pc.estatus as statuspago,
                            pc.reg,
                            pre.vencimiento,
                            '' as fechaentrega,
                            pc.tipov,
                            (SELECT IFNULL(sum(pd.precio_local), 0)+ IFNULL(sum(pd.precio_semi), 0)+ IFNULL(sum(pd.precio_foraneo), 0)+ IFNULL(sum(pd.precio_especial), 0) FROM `polizasCreadas_has_detallesPoliza` as pd WHERE pd.idPoliza=pc.id) as total_general,
                            pc.estatus,
                            per.nombre as vendedor,
                            3 as combinada,
                            '' as motivo,
                            pc.activo,
                            c.empresa,
                            0 as equipo, 
                            0 as consumibles, 
                            0 as refacciones, 
                            (
                                SELECT GROUP_CONCAT(fac.Folio)
                                FROM `factura_poliza` as pol 
                                INNER JOIN f_facturas as fac on fac.FacturasId=pol.facturaId and fac.Estado=1
                                WHERE pol.polizaId=pc.id
                                    ) as facturas, 
                            0 as totalcon, 
                            0 as totalref, 
                            0 as totalequ, 
                            0 as totalpoli,
                            0 as totalacc, 
                            0 as totalcon2, 
                            '' as e_entrega,
                            '' as comentario,
                            '' as garantia,
                            '' as folio_venta,
                            pc.viewcont,
                            0 as tipopagofacman,
                            pc.siniva,
                            pc.requ_fac
                        FROM `polizasCreadas` `pc`
                        JOIN `clientes` `c` ON `pc`.`idCliente`=`c`.`id`
                        LEFT JOIN `prefactura` `pre` ON `pre`.`ventaId`=`pc`.`id` and `pre`.`tipo`=1
                        LEFT join cliente_has_datos_fiscales as clif on clif.id=pre.rfc_id
                        LEFT JOIN `personal` `per` ON `per`.`personalId`=`pc`.`id_personal`
                        WHERE `pc`.`combinada` = 0
                        AND `pc`.`activo` = $estatusventa $cliente_wherep $status_wherep $idpersonal_wherep $fechainicial_reg_wherep $fechafinal_reg_wherep $emp_wherep $w_rfc";
                $sql.=")";
            $sql.=") as datos";

            return $sql;
        }
        function sqlgarantias_pagos($params){
            $cliente=$params['cliente'];
            $status=$params['status'];
            $tipoventa=$params['tipoventa'];
            $estatusventa=$params['estatusventa'];

            //$tipoview=$params['tipoview'];
            $idpersonal=$params['idpersonal'];
            $perfilid=$params['perfilid'];
            $fechainicial_reg=$params['fechainicial_reg'];
            $emp=$params['emp'];
            $filf=$params['filf'];
            $pagosview=$params['pagosview'];

            //===========================================================
                if ($cliente>0) {
                    $cliente_where=" and v.idCliente='$cliente' ";
                    $cliente_wherec=" and vc.idCliente='$cliente' ";
                    $cliente_wherep=" and pc.idCliente='$cliente' ";
                }else{
                    $cliente_where='';
                    $cliente_wherec='';
                    $cliente_wherep='';
                }
                if($status==1){
                    $status_where=" and v.estatus=3 ";
                    $status_wherec=" and vc.estatus=3 ";
                    $status_wherep=" and pc.estatus=3 ";
                }elseif ($status==2) {
                    $status_where=" and v.estatus=1 ";
                    $status_wherec=" and vc.estatus=1 ";
                    $status_wherep=" and pc.estatus=1 ";
                }else{
                    $status_where="";
                    $status_wherec="";
                    $status_wherep='';
                }
                if($idpersonal>0){
                    if($idpersonal==56){
                        $idpersonal_where=" and (v.id_personal=56 or v.id_personal=13 ) ";
                        $idpersonal_wherec=" and (vc.id_personal=56 or vc.id_personal=13 ) ";
                        $idpersonal_wherep=" and (pc.id_personal=56 or  pc.id_personal=13 )"; 
                        $idpersonal_whereren=" and (facp.idpersonal=56 or  facp.idpersonal=13 )"; 
                        $idpersonal_whereren_c=" and (fcomp.personalcreo=56 or  fcomp.personalcreo=13 )"; 
                    }else{
                        $idpersonal_where=" and v.id_personal='$idpersonal' ";
                        $idpersonal_wherec=" and vc.id_personal='$idpersonal' ";
                        $idpersonal_wherep=" and pc.id_personal='$idpersonal' "; 
                        $idpersonal_whereren=" and facp.idpersonal='$idpersonal' "; 
                        $idpersonal_whereren_c=" and fcomp.personalcreo='$idpersonal' "; 
                    }
                    
                }else{
                    $idpersonal_where="";
                    $idpersonal_wherec="";
                    $idpersonal_wherep='';
                    $idpersonal_whereren='';
                    $idpersonal_whereren_c='';
                }
                if($fechainicial_reg!=''){
                    $fechainicial_reg_where=" and vp.fecha >= '$fechainicial_reg' ";
                    $fechainicial_reg_where_c=" and fcomp.fechatimbre >= '$fechainicial_reg 00:00:00' ";
                    $fechainicial_reg_wherec=" and vp.reg >= '$fechainicial_reg' ";
                    $fechainicial_reg_wherec_c=" and fcomp.fechatimbre >= '$fechainicial_reg 00:00:00' ";
                    $fechainicial_reg_wherep=" and pp.reg >= '$fechainicial_reg' ";
                    $fechainicial_reg_wherep_c=" and fcomp.fechatimbre >= '$fechainicial_reg 00:00:00' ";
                    $fechainicial_reg_whereren=" and facp.reg >= '$fechainicial_reg' ";
                    $fechainicial_reg_whereren_c=" and fcomp.fechatimbre >= '$fechainicial_reg 00:00:00' ";
                }else{
                    $fechainicial_reg_where="";
                    $fechainicial_reg_where_c="";
                    $fechainicial_reg_wherec="";
                    $fechainicial_reg_wherec_c="";
                    $fechainicial_reg_wherep='';
                    $fechainicial_reg_wherep_c='';
                    $fechainicial_reg_whereren='';
                    $fechainicial_reg_whereren_c='';
                }
                if($emp>0){
                    $emp_where=" and v.tipov='$emp' ";
                    $emp_wherec=" and vc.tipov='$emp' ";
                    $emp_wherep=" and pc.tipov='$emp' ";
                }else{
                    $emp_where="";
                    $emp_wherec="";
                    $emp_wherep='';
                }
            //=================================================================
            $sql="(";
                $sql.="(";
                    $sql.="SELECT 
                            v.id, 
                            v.idCliente, 
                            v.prefactura, 
                            v.id_personal, 
                            v.statuspago, 
                            v.reg, 
                            pre.vencimiento, 
                            v.fechaentrega, 
                            v.tipov, 
                            vp.pago as total_general, 
                            v.estatus, 
                            per.nombre as vendedor, 
                            0 as combinada, 
                            v.motivo, 
                            v.activo, 
                            cli.empresa, 
                            0 as equipo, 
                            0 as consumibles, 
                            0 as refacciones, 
                            GROUP_CONCAT(fac.Folio) as facturas, 
                            0 as totalcon, 
                            0 as totalref, 
                            0 as totalequ, 
                            0 as totalpoli,
                            0 as totalacc, 
                            0 as totalcon2, 
                            v.e_entrega, 
                            v.comentario, 
                            v.garantia,
                            v.folio_venta,
                            0 as viewcont,
                            0 as tipopagofacman,
                            v.requ_fac
                            FROM ventas as v
                            JOIN clientes as cli ON cli.id=v.idCliente
                            LEFT JOIN personal as per ON per.personalId=v.id_personal
                            LEFT JOIN prefactura as pre ON pre.ventaId=v.id and pre.tipo=0
                            LEFT JOIN factura_venta as ven ON ven.combinada=0 and ven.ventaId=v.id
                            LEFT JOIN f_facturas as fac ON fac.FacturasId=ven.facturaId and fac.Estado=1
                            inner join pagos_ventas as vp on vp.idventa=v.id
                            WHERE v.combinada=0 and v.activo=$estatusventa $cliente_where $status_where $idpersonal_where $fechainicial_reg_where $emp_where
                            GROUP BY v.id";
                $sql.=")";
                $sql.=" UNION ";
                $sql.="(";
                    $sql.="SELECT 
                            v.id, 
                            v.idCliente, 
                            v.prefactura, 
                            v.id_personal, 
                            v.statuspago, 
                            v.reg, 
                            pre.vencimiento, 
                            v.fechaentrega, 
                            v.tipov, 
                            fcompd.ImpPagado as total_general, 
                            v.estatus, 
                            per.nombre as vendedor, 
                            0 as combinada, 
                            v.motivo, 
                            v.activo, 
                            cli.empresa, 
                            0 as equipo, 
                            0 as consumibles, 
                            0 as refacciones, 
                            GROUP_CONCAT(fac.Folio) as facturas, 
                            0 as totalcon, 
                            0 as totalref, 
                            0 as totalequ, 
                            0 as totalpoli,
                            0 as totalacc, 
                            0 as totalcon2, 
                            v.e_entrega, 
                            v.comentario, 
                            v.garantia,
                            v.folio_venta,
                            0 as viewcont,
                            1 as tipopagofacman,
                            v.requ_fac
                            FROM ventas as v
                            JOIN clientes as cli ON cli.id=v.idCliente
                            LEFT JOIN personal as per ON per.personalId=v.id_personal
                            LEFT JOIN prefactura as pre ON pre.ventaId=v.id and pre.tipo=0
                            inner JOIN factura_venta as ven ON ven.combinada=0 and ven.ventaId=v.id
                            inner JOIN f_facturas as fac ON fac.FacturasId=ven.facturaId and fac.Estado=1
                            inner join f_complementopago_documento as fcompd on fcompd.facturasId=fac.FacturasId
                            inner join f_complementopago as fcomp on fcomp.complementoId=fcompd.complementoId

                            WHERE fcomp.Estado=1 and v.combinada=0 and v.activo=$estatusventa $cliente_where $status_where $idpersonal_where $fechainicial_reg_where_c $emp_where
                            GROUP BY v.id";
                $sql.=")";
                $sql.=" UNION ";
                $sql.="(";
                    $sql.="SELECT 
                            vc.combinadaId as id, 
                            vc.idCliente,
                            vc.prefactura,
                            vc.id_personal,
                            vc.statuspago,
                            vc.reg,
                            pre.vencimiento,
                            vc.fechaentrega, 
                            vc.tipov,
                            vp.pago as total_general,
                            vc.estatus, 
                            per.nombre as vendedor, 
                            1 as combinada,
                            vc.motivo,
                            vc.activo, 
                            cli.empresa,
                            vc.equipo, 
                            vc.consumibles,
                            vc.refacciones,
                            GROUP_CONCAT(fac.Folio) as facturas, 
                            0 as totalcon, 
                            0 as totalref, 
                            0 as totalequ, 
                            0 as totalpoli, 
                            0 as totalacc,
                            0 as totalcon2, 
                            vc.e_entrega,
                            vc.comentario, 
                            vc.garantia,
                            vc.folio_venta,
                            0 as viewcont,
                            0 as tipopagofacman,
                            vc.requ_fac
                            FROM ventacombinada as vc
                            JOIN clientes as cli ON cli.id=vc.idCliente
                            LEFT JOIN personal as per ON per.personalId=vc.id_personal
                            LEFT JOIN prefactura as pre ON pre.ventaId=vc.combinadaId and pre.tipo=3
                            LEFT JOIN factura_venta as ven ON ven.combinada=1 and ven.ventaId=vc.combinadaId
                            LEFT JOIN f_facturas as fac ON fac.FacturasId=ven.facturaId and fac.Estado=1
                            inner join pagos_combinada as vp on vp.idventa=vc.combinadaId
                            WHERE vc.activo=$estatusventa $cliente_wherec $status_wherec $idpersonal_wherec $fechainicial_reg_wherec $emp_wherec
                            GROUP BY vc.combinadaId";
                $sql.=")";
                $sql.=" UNION ";
                $sql.="(";
                    $sql.="SELECT 
                            vc.combinadaId as id, 
                            vc.idCliente,
                            vc.prefactura,
                            vc.id_personal,
                            vc.statuspago,
                            vc.reg,
                            pre.vencimiento,
                            vc.fechaentrega, 
                            vc.tipov,
                            fcompd.ImpPagado as total_general,
                            vc.estatus, 
                            per.nombre as vendedor, 
                            1 as combinada,
                            vc.motivo,
                            vc.activo, 
                            cli.empresa,
                            vc.equipo, 
                            vc.consumibles,
                            vc.refacciones,
                            GROUP_CONCAT(fac.Folio) as facturas, 
                            0 as totalcon, 
                            0 as totalref, 
                            0 as totalequ, 
                            0 as totalpoli, 
                            0 as totalacc,
                            0 as totalcon2, 
                            vc.e_entrega,
                            vc.comentario, 
                            vc.garantia,
                            vc.folio_venta,
                            0 as viewcont,
                            1 as tipopagofacman,
                            vc.requ_fac
                            FROM ventacombinada as vc
                            JOIN clientes as cli ON cli.id=vc.idCliente
                            LEFT JOIN personal as per ON per.personalId=vc.id_personal
                            LEFT JOIN prefactura as pre ON pre.ventaId=vc.combinadaId and pre.tipo=3
                            LEFT JOIN factura_venta as ven ON ven.combinada=1 and ven.ventaId=vc.combinadaId
                            inner JOIN f_facturas as fac ON fac.FacturasId=ven.facturaId and fac.Estado=1                            
                            inner join f_complementopago_documento as fcompd on fcompd.facturasId=fac.FacturasId
                            inner join f_complementopago as fcomp on fcomp.complementoId=fcompd.complementoId

                            WHERE fcomp.Estado=1 and vc.activo=$estatusventa $cliente_wherec $status_wherec $idpersonal_wherec $fechainicial_reg_wherec_c $emp_wherec
                            GROUP BY vc.combinadaId";
                $sql.=")";
                $sql.=" UNION ";
                $sql.="(";
                    $sql.="SELECT 
                            pc.id, 
                            pc.idCliente,
                            pc.prefactura,
                            pc.id_personal,
                            pc.estatus as statuspago,
                            pc.reg,
                            pre.vencimiento,
                            '' as fechaentrega,
                            pc.tipov,
                            pp.pago as total_general,
                            pc.estatus,
                            per.nombre as vendedor,
                            3 as combinada,
                            '' as motivo,
                            pc.activo,
                            c.empresa,
                            0 as equipo, 
                            0 as consumibles, 
                            0 as refacciones, 
                            (
                                SELECT GROUP_CONCAT(fac.Folio)
                                FROM `factura_poliza` as pol 
                                INNER JOIN f_facturas as fac on fac.FacturasId=pol.facturaId and fac.Estado=1
                                WHERE pol.polizaId=pc.id
                                    ) as facturas, 
                            0 as totalcon, 
                            0 as totalref, 
                            0 as totalequ, 
                            0 as totalpoli,
                            0 as totalacc, 
                            0 as totalcon2, 
                            '' as e_entrega,
                            '' as comentario,
                            '' as garantia,
                            '' as folio_venta,
                            pc.viewcont,
                            0 as tipopagofacman,
                            pc.requ_fac
                        FROM `polizasCreadas` `pc`
                        JOIN `clientes` `c` ON `pc`.`idCliente`=`c`.`id`
                        LEFT JOIN `prefactura` `pre` ON `pre`.`ventaId`=`pc`.`id` and `pre`.`tipo`=1
                        LEFT JOIN `personal` `per` ON `per`.`personalId`=`pc`.`id_personal`
                        inner join pagos_poliza as pp on pp.idpoliza=pc.id
                        WHERE `pc`.`combinada` = 0
                        AND `pc`.`activo` = $estatusventa $cliente_wherep $status_wherep $idpersonal_wherep $fechainicial_reg_wherep $emp_wherep";
                $sql.=")";
                $sql.=" UNION ";
                $sql.="(";
                    $sql.="SELECT 
                            pc.id, 
                            pc.idCliente,
                            pc.prefactura,
                            pc.id_personal,
                            pc.estatus as statuspago,
                            pc.reg,
                            pre.vencimiento,
                            '' as fechaentrega,
                            pc.tipov,
                            fcompd.ImpPagado as total_general,
                            pc.estatus,
                            per.nombre as vendedor,
                            3 as combinada,
                            '' as motivo,
                            pc.activo,
                            c.empresa,
                            0 as equipo, 
                            0 as consumibles, 
                            0 as refacciones, 
                            (
                                SELECT GROUP_CONCAT(fac.Folio)
                                FROM `factura_poliza` as pol 
                                INNER JOIN f_facturas as fac on fac.FacturasId=pol.facturaId and fac.Estado=1
                                WHERE pol.polizaId=pc.id
                                    ) as facturas, 
                            0 as totalcon, 
                            0 as totalref, 
                            0 as totalequ, 
                            0 as totalpoli,
                            0 as totalacc, 
                            0 as totalcon2, 
                            '' as e_entrega,
                            '' as comentario,
                            '' as garantia,
                            '' as folio_venta,
                            pc.viewcont,
                            1 as tipopagofacman,
                            pc.requ_fac
                        FROM `polizasCreadas` `pc`
                        JOIN `clientes` `c` ON `pc`.`idCliente`=`c`.`id`
                        LEFT JOIN `prefactura` `pre` ON `pre`.`ventaId`=`pc`.`id` and `pre`.`tipo`=1
                        LEFT JOIN `personal` `per` ON `per`.`personalId`=`pc`.`id_personal`
                        inner join factura_poliza as pp on pp.polizaId=pc.id

                        inner JOIN f_facturas as fac ON fac.FacturasId=pp.facturaId and fac.Estado=1                            
                        inner join f_complementopago_documento as fcompd on fcompd.facturasId=fac.FacturasId
                        inner join f_complementopago as fcomp on fcomp.complementoId=fcompd.complementoId

                        WHERE fcomp.Estado=1 and pc.combinada = 0 AND pc.activo = $estatusventa $cliente_wherep $status_wherep $idpersonal_wherep $fechainicial_reg_wherep_c $emp_wherep";
                $sql.=")";
                $sql.=" UNION ";
                $sql.="(";
                        $sql.="SELECT 
                            facpre.contratoId as id,
                            fac.Clientes_ClientesId as idCliente,
                            0 prefactura,
                            facp.idpersonal as id_personal,
                            0 as statuspago,
                            facp.reg,
                            '' as vencimiento,
                            '' as fechaentrega,
                            1 as tipov,
                            facp.pago as total_general,
                            1 as estatus,
                            '' as vendedor,
                            4 as combinada,
                            '' as motivo,
                            1 as activo,
                            cli.empresa,
                            0 as equipo, 
                            0 as consumibles, 
                            0 as refacciones,
                            fac.Folio as facturas,
                            0 as totalcon, 
                            0 as totalref, 
                            0 as totalequ, 
                            0 as totalpoli,
                            0 as totalacc, 
                            0 as totalcon2, 
                            '' as e_entrega,
                            '' as comentario,
                            '' as garantia,
                            '' as folio_venta,
                            0 as viewcont,
                            0 as tipopagofacman,
                            '' as requ_fac
                            
                                        FROM factura_prefactura as facpre
                                        inner JOIN alta_rentas_prefactura as apre on apre.prefId=facpre.prefacturaId
                                        inner JOIN f_facturas as fac on fac.FacturasId=facpre.facturaId
                                        INNER JOIN factura_prefactura_pagos as facp on facp.facId=facpre.facId
                                        INNER JOIN clientes as cli on cli.id=fac.Clientes_ClientesId
                                        WHERE facpre.contratoId>0 $idpersonal_whereren $fechainicial_reg_whereren ";
                $sql.=")";
                $sql.=" UNION ";
                $sql.="(";
                        $sql.="SELECT 
                            facpre.contratoId as id,
                            fac.Clientes_ClientesId as idCliente,
                            0 prefactura,
                            fcomp.personalcreo as id_personal,
                            0 as statuspago,
                            fcomp.fechatimbre as reg,
                            '' as vencimiento,
                            '' as fechaentrega,
                            1 as tipov,
                            fcompd.ImpPagado as total_general,
                            1 as estatus,
                            '' as vendedor,
                            4 as combinada,
                            '' as motivo,
                            1 as activo,
                            cli.empresa,
                            0 as equipo, 
                            0 as consumibles, 
                            0 as refacciones,
                            fac.Folio as facturas,
                            0 as totalcon, 
                            0 as totalref, 
                            0 as totalequ, 
                            0 as totalpoli,
                            0 as totalacc, 
                            0 as totalcon2, 
                            '' as e_entrega,
                            '' as comentario,
                            '' as garantia,
                            '' as folio_venta,
                            0 as viewcont,
                            0 as tipopagofacman,
                            '' as requ_fac
                            
                                        FROM factura_prefactura as facpre
                                        inner JOIN alta_rentas_prefactura as apre on apre.prefId=facpre.prefacturaId
                                        inner JOIN f_facturas as fac on fac.FacturasId=facpre.facturaId
                                        inner join f_complementopago_documento as fcompd on fcompd.facturasId=fac.FacturasId
                                        inner join f_complementopago as fcomp on fcomp.complementoId=fcompd.complementoId


                                        INNER JOIN clientes as cli on cli.id=fac.Clientes_ClientesId
                                        WHERE fcomp.Estado=1 and  facpre.contratoId>0 $idpersonal_whereren_c $fechainicial_reg_whereren_c ";
                $sql.=")";
            $sql.=") as datos";
            //log_message('error',$sql);
            return $sql;
        }
        function getlistadosoloventasgarantias($params){
            $pagosview = $params['pagosview'];
            //log_message('error','pagosview:'.$pagosview);
            if($pagosview==0){
                $subconsulta = $this->sqlgarantias($params);
            }else{
                $subconsulta = $this->sqlgarantias_pagos($params);
            }
            $perfilid=$params['perfilid'];
            $filf=$params['filf'];
            if(isset($params['pre'])){
                $pre=$params['pre'];
            }else{
                $pre='x';    
            }
            $columns = array(
                0=>'id',
                1=>'idCliente',
                2=>'prefactura',
                3=>'id_personal',
                4=>'statuspago',
                5=>'reg',
                6=>'vencimiento',
                7=>'fechaentrega',
                8=>'tipov',
                9=>'total_general',
                10=>'estatus',
                11=>'vendedor',
                12=>'combinada',
                13=>'motivo',
                14=>'activo',
                15=>'empresa',
                16=>'equipo',
                17=>'consumibles',
                18=>'refacciones',
                19=>'facturas',
                20=>'totalcon',
                21=>'totalref',
                22=>'totalequ',
                23=>'totalacc',
                24=>'totalcon2',
                25=>'e_entrega',
                26=>'comentario',
                27=>'garantia',
                28=>'viewcont',
                29=>'tipopagofacman',
                30=>'requ_fac'
                
            );
            $columnsss = array(
                0=>'id',
                1=>'prefactura',
                2=>'reg',
                3=>'vencimiento',
                4=>'fechaentrega',
                5=>'vendedor',  
                6=>'motivo',
                7=>'empresa',
                8=>'facturas',
                9=>'total_general',
                10=>'equipo',
                11=>'consumibles',
                12=>'refacciones',
            );
            if($filf==1){
                $columnsss = array(
                    0=>'folio_venta'
                );
            }
            $select="";
            foreach ($columns as $c) {
                $select.="$c, ";
            }
            $this->db->select($select);
            $this->db->from($subconsulta);
            if($pre!='x'){
                $this->db->where('prefactura',$pre);
            }
                
            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->db->group_start();
                foreach($columnsss as $c){
                    $this->db->or_like($c,$search);
                }
                $this->db->group_end();  
            }            
            $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
            $this->db->limit($params['length'],$params['start']);
            //echo $this->db->get_compiled_select();
            $query=$this->db->get();
            // print_r($query); die;
            return $query;
        }
        function getlistadosoloventasgarantiast($params){
            $pagosview = $params['pagosview'];
            if($pagosview==0){
                $subconsulta = $this->sqlgarantias($params);
            }else{
                $subconsulta = $this->sqlgarantias_pagos($params);
            }
            if(isset($params['pre'])){
                $pre=$params['pre'];
            }else{
                $pre='x';    
            }
            
            $perfilid=$params['perfilid'];
            $filf=$params['filf'];
            $columns = array(
                0=>'id',
                1=>'estatus',
                2=>'prefactura',
                3=>'reg',
                4=>'nombre',
                5=>'motivo',
                6=>'empresa',
                7=>'vencimiento',
                8=>'fechaentrega'
            );
            $columnsss=$columns;
            if($perfilid==1){
                $columnsss = array(
                    0=>'id',
                    1=>'reg',
                    2=>'empresa',
                    3=>'equipo',
                    4=>'consumibles',
                    5=>'refacciones',
                );
            }
            if($filf==1){
                $columnsss = array(
                    0=>'folio_venta',
                    1=>'equipo',
                    2=>'consumibles',
                    3=>'refacciones',
                );
            }
            $this->db->select('COUNT(*) as total');
            $this->db->from($subconsulta);
            if($pre!='x'){
                $this->db->where('prefactura',$pre);
            }
            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->db->group_start();
                foreach($columnsss as $c){
                    $this->db->or_like($c,$search);
                }
                $this->db->group_end();  
            }            
            //$this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
            ///$this->db->limit($params['length'],$params['start']);
            //echo $this->db->get_compiled_select();
            $query=$this->db->get();
            // print_r($query); die;
            return $query->row()->total;
        }


    //======================================================================================

        function validarfactura($idventa,$tipo){
            $strq="SELECT *
                        FROM `factura_venta` as ven 
                        INNER JOIN f_facturas as fac on fac.FacturasId=ven.facturaId 
                        WHERE ven.combinada=$tipo and ven.ventaId=$idventa and fac.Estado=1";
            $query = $this->db->query($strq);
            //$this->db->close();
            return $query;
        }
        function entregasventasnormal($fechahoy){
            $strq="SELECT 
                    v.estatus,
                    v.id,
                    v.prefactura,
                    v.telefono,
                    v.correo,
                    cli.empresa,
                    per.nombre as vendedor,
                    'ventas' as tabla,
                    1 as tipotabla,
                    v.fechaentrega,
                    v.e_entrega,
                    v.e_entrega_p,
                    v.idCliente,
                    v.statuspago,
                    v.activo,
                    0 as id_rh,
                    0 as renta_rh,
                    pre.reg,
                    v.id_personal,
                    concat(per2.nombre,' ',per2.apellido_paterno,' ',per2.apellido_materno) as tecnico
                    FROM ventas as v 
                inner join clientes as cli on cli.id=v.idCliente 
                LEFT join personal as per on per.personalId=v.id_personal 
                left join prefactura as pre on pre.ventaId=v.id and pre.tipo=0

                LEFT join asignacion_ser_venta_a as av on av.tipov=0 and av.ventaId=v.id
                LEFT join personal as per2 on per2.personalId=av.tecnico 
                WHERE 
                    v.activo=1 and 
                    v.combinada=0 and 
                    v.viewed=1 and 
                    v.fechaentrega between '$fechahoy 00:00:00' and  '$fechahoy 23:59:59'";
            $query = $this->db->query($strq);
            return $query;
        }
        function entregasventascombinada($fechahoy){
            $strq="SELECT

                vc.estatus,
                vc.combinadaId id,
                vc.prefactura,
                vc.telefono,
                vc.correo,
                cli.empresa,
                per.nombre vendedor,
                'combinada' tabla,
                4 as tipotabla,
                vc.fechaentrega,
                vc.e_entrega,
                vc.e_entrega_p,
                vc.idCliente,
                vc.statuspago,
                vc.activo,
                0 as id_rh,
                0 as renta_rh,
                pre.reg,
                vc.id_personal,
                vc.equipo,
                vc.consumibles,
                vc.refacciones,
                concat(per2.nombre,' ',per2.apellido_paterno,' ',per2.apellido_materno) as tecnico
                FROM ventacombinada vc
                inner join clientes as cli on cli.id=vc.idCliente
                LEFT join personal as per on per.personalId=vc.id_personal
                left join prefactura as pre on pre.ventaId=vc.combinadaId and pre.tipo=3

                LEFT join asignacion_ser_poliza_a as ap on ap.polizaId=vc.poliza
                LEFT join asignacion_ser_poliza_a_e as apd on apd.asignacionId=ap.asignacionId
                LEFT join personal as per2 on per2.personalId=apd.tecnico 

                WHERE 
                    vc.activo=1 and 
                    vc.viewed=1 and 
                    vc.fechaentrega between '$fechahoy 00:00:00' and  '$fechahoy 23:59:59'
                group by vc.combinadaId
                    ";
            $query = $this->db->query($strq);
            return $query;
        }
        function entregasrentas($fechahoy){
            $strq="SELECT 
                    v.estatus,
                    v.id,
                    v.prefactura,
                    v.telefono,
                    v.correo,
                    cli.empresa,
                    per.nombre vendedor,
                    'rentas' tabla,
                    2 as tipotabla,
                    pre.vencimiento as fechaentrega,
                    v.e_entrega,
                    v.e_entrega_p,
                    v.idCliente,
                    v.statuspago,
                    1 as activo,
                    0 as id_rh,
                    0 as renta_rh,
                    pre.reg,
                    v.id_personal,
                    pre.domicilio_entrega
                    from rentas as v
                    inner join clientes as cli on cli.id=v.idCliente
                    LEFT join personal as per on per.personalId=v.id_personal 
                    left join prefactura as pre on pre.ventaId=v.id and pre.tipo=2
                    WHERE v.estatus !=0 and v.viewed=1 and pre.vencimiento between '$fechahoy' and  '$fechahoy'";
            $query = $this->db->query($strq);
            return $query;
        }
        function entregasrentashistorial($fechahoy){
            $strq="SELECT 
                    0 as estatus,
                    pre.prefacturaId as id,
                    1 prefactura,
                    ren.telefono,
                    ren.correo,
                    cli.empresa,
                    per.nombre vendedor,
                    'rentas historial' as tabla,
                    5 as tipotabla,
                    rh.fechaentrega,
                    rh.e_entrega,
                    rh.e_entrega_p,
                    ren.idCliente,
                    ren.statuspago,
                    rh.activo,
                    rh.id as id_rh,
                    rh.rentas as renta_rh,
                    pre.reg,
                    ren.id_personal,
                    r.id as idconsul_rh
                FROM rentas_historial as rh
                INNER JOIN prefactura as pre on pre.ventaId=rh.id AND pre.tipo=4
                INNER JOIN rentas as ren on ren.id=rh.rentas
                INNER JOIN clientes as cli on cli.id=ren.idCliente
                LEFT join personal as per on per.personalId=ren.id_personal
                
                LEFT join rentas_has_detallesEquipos as r on r.idRenta=rh.id
                LEFT join rentas_historial_equipos as reneqh on reneqh.equipo=r.id and reneqh.historialid=rh.rentas
                
                WHERE 
                    rh.activo=1 and 
                    rh.viewed=1 and 
                    rh.fechaentrega between '$fechahoy 00:00:00' and  '$fechahoy 23:59:59'";
            $query = $this->db->query($strq);
            return $query;
        }
        public  function pres_pendientes_por_com($tipo,$per){
            if($tipo==0){
                $sql="SELECT v.id, v.reg, v.id_personal, DATE_ADD(v.reg, INTERVAL 2 DAY) as reg_mas_dias
                        FROM ventas as v
                        WHERE v.activo=1 AND v.combinada=0 AND v.prefactura=0 AND v.id_personal='$per' and
                        DATE_ADD(v.reg, INTERVAL 2 DAY) < CURDATE()
                        ORDER BY `v`.`id`  DESC";
            }
            if($tipo==1){
                $sql="SELECT v.combinadaId,v.equipo,  v.reg, v.id_personal, DATE_ADD(v.reg, INTERVAL 2 DAY) as reg_mas_dias
                    FROM ventacombinada as v
                    WHERE v.activo=1 AND v.prefactura=0 AND v.id_personal='$per' and
                    DATE_ADD(v.reg, INTERVAL 2 DAY) < CURDATE()";
            }
            if($tipo==2){
                $sql="SELECT v.id,  v.reg, v.id_personal, DATE_ADD(v.reg, INTERVAL 2 DAY) as reg_mas_dias
                    FROM polizasCreadas as v
                    WHERE v.activo=1 AND v.combinada=0 AND v.prefactura=0 AND v.id_personal='$per' and
                    DATE_ADD(v.reg, INTERVAL 2 DAY) < CURDATE()";
            } 
            $query = $this->DB3->query($sql);
            return $query;
        }
}
?>