<?php

class ModeloEstadocuenta extends CI_Model {
    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }
    
    public function sqlventas($idcliente,$idsrow,$eje){
        if($idsrow==''){
            $where=" v.idCliente='$idcliente' ";
        }else{
            $where="(";
                foreach ($idsrow as $item) {
                    $ve=$item->ve;
                    $where.=" v.id = '$ve' OR ";
                }
                    $where.=" v.id = '0'";
            $where.=")";
            
        }
        if($eje>0){
            $where_eje=" and v.id_personal='$eje' ";
        }else{
            $where_eje="";
        }


        $strq="SELECT v.id,v.reg,v.combinada,v.total_general,fac.serie,fac.Folio,fac.FacturasId, fac.FormaPago,fac.fechatimbre , IFNULL(SUM(pagv.pago),0) as pago_manual,IFNULL(SUM(fcompd.ImpPagado),0) as ImpPagado,v.info_factura
                    FROM ventas as v LEFT JOIN factura_venta as facv on facv.ventaId=v.id AND facv.combinada=0 
                    LEFT JOIN f_facturas as fac on fac.FacturasId=facv.facturaId AND fac.Estado=1 
                    LEFT JOIN pagos_ventas as pagv on pagv.idventa=v.id
                    LEFT JOIN f_complementopago_documento as fcompd on fcompd.facturasId=fac.FacturasId and fcompd.Estado=1
                    LEFT JOIN f_complementopago as fcomp on fcomp.complementoId=fcompd.complementoId AND fcomp.Estado=1
                    WHERE v.activo=1 AND v.combinada=0 AND v.total_general>0 AND $where $where_eje
                    GROUP BY v.id";
            return $strq;
    }
    public function sqlventascom($idcliente,$idsrow,$eje){
        if($idsrow==''){
            
            $where=" vc.idCliente='$idcliente' ";
        }else{
            $where="(";
                foreach ($idsrow as $item) {
                    $ve=$item->ve;
                    $where.=" vc.combinadaId = '$ve' OR ";
                }
                    $where.=" vc.combinadaId = '0'";
            $where.=")";
        }
        if($eje>0){
            $where_eje=" and vc.id_personal='$eje' ";
        }else{
            $where_eje="";
        }
        $strq="SELECT vc.combinadaId,vc.equipo,vc.reg,vc.total_general,vc.info_factura,fac.FacturasId,fac.serie,fac.Folio,fac.fechatimbre,fac.FormaPago, IFNULL(SUM(pagc.pago),0) as pago_manual,IFNULL(SUM(fcompd.ImpPagado),0) as ImpPagado 
                        FROM ventacombinada as vc 
                        LEFT JOIN factura_venta as facv on facv.ventaId=vc.combinadaId AND facv.combinada=1 
                        LEFT JOIN f_facturas as fac on fac.FacturasId=facv.facturaId AND fac.Estado=1 
                        LEFT JOIN pagos_combinada as pagc on pagc.idventa=vc.combinadaId 
                        LEFT JOIN f_complementopago_documento as fcompd on fcompd.facturasId=fac.FacturasId and fcompd.Estado=1
                        LEFT JOIN f_complementopago as fcomp on fcomp.complementoId=fcompd.complementoId AND fcomp.Estado=1 
                        WHERE $where AND vc.activo=1 AND vc.total_general>0 $where_eje
                        GROUP BY vc.combinadaId ORDER BY vc.combinadaId DESC";
        return $strq;
    }
    public function sqlrentas($contrato,$periodos,$eje,$group){
        if($periodos==''){
            
            $where_con=" ar.idcontrato='$contrato' ";
            $where_per="";
        }else{
            $where_con="";
            $where_per="(";
                foreach ($periodos as $item) {
                    $per=$item->per;
                    $fac=$item->fac;
                    if($fac>0){
                        $w_fac=" and fac.FacturasId='$fac' ";
                    }else{
                        $w_fac=" and fac.FacturasId is null ";
                    }
                    $where_per.=" (arp.prefId = '$per' $w_fac ) OR ";
                }
                    $where_per.=" arp.prefId = '0'";
            $where_per.=")";
        }
        if($eje>0){
            $where_eje=" and fac.usuario_id='$eje' ";
        }else{
            $where_eje="";
        }
        if($group==1){
            $select_colums=" GROUP_CONCAT(DISTINCT(arp.prefId) SEPARATOR '<br>') as prefId,arp.periodo_inicial,arp.periodo_final,facpre.*,fac.*,facprep.*,sum(facprep.pago) as pagot, max(facprep.fecha) as fechamax,arp.total as total_periodo,facpre.facId as fp_facId,facpre.info_factura as info_factura_b ";
            $select_colums.=", GROUP_CONCAT(DISTINCT(con.folio) SEPARATOR '<br>') as confolio";
            $group_orden='GROUP by fac.FacturasId
                ORDER BY fac.FacturasId  DESC ';
        }else{
            $select_colums=' arp.prefId,arp.periodo_inicial,arp.periodo_final,facpre.*,fac.*,facprep.*,sum(facprep.pago) as pagot, max(facprep.fecha) as fechamax,arp.total as total_periodo,facpre.facId as fp_facId,con.folio as confolio,facpre.info_factura as info_factura_b';
            $group_orden=' GROUP by fac.FacturasId,arp.prefId
                ORDER BY arp.prefId,fac.Estado  DESC ';
        }
        $strq="SELECT $select_colums
                FROM alta_rentas_prefactura as arp
                inner join alta_rentas as ar on ar.id_renta=arp.id_renta
                INNER JOIN contrato as con on con.idcontrato=ar.idcontrato
                left join factura_prefactura as facpre on arp.prefId=facpre.prefacturaId
                left JOIN f_facturas as fac on fac.FacturasId=facpre.facturaId and fac.Estado=1
                left join factura_prefactura_pagos as facprep on facprep.facId=facpre.facId
                WHERE $where_con $where_per  $where_eje
                $group_orden";
        return $strq;
    }
    public function sqlpolizas($idcliente,$idsrow,$eje){
        if($idsrow==''){
            
            $where=" pc.idCliente='$idcliente' ";
        }else{
            $where="(";
                foreach ($idsrow as $item) {
                    $pol=$item->pol;
                    $where.=" pc.id = '$pol' OR ";
                }
                    $where.=" pc.id= '0'";
            $where.=")";
        }
        $strq="SELECT 
            pc.id,
            pc.reg,
            pc.info_factura,
            (SELECT IFNULL(sum(pd.precio_local),0)+ IFNULL(sum(pd.precio_semi),0)+ IFNULL(sum(pd.precio_foraneo),0)+ IFNULL(sum(pd.precio_especial),0) FROM `polizasCreadas_has_detallesPoliza` as pd WHERE pd.idPoliza=pc.id) as subtotalpoliza,
            pc.siniva,
            fac.FacturasId,
            fac.serie,
            fac.Folio,
            fac.fechatimbre,
            fac.FormaPago,
            IFNULL(sum(pagp.pago),0) as pago_manual,
            IFNULL(SUM(fcompd.ImpPagado),0) as ImpPagado
        FROM polizasCreadas as pc
        LEFT JOIN factura_poliza as facp on facp.polizaId=pc.id
        LEFT JOIN f_facturas as fac on fac.FacturasId=facp.facturaId and fac.Estado=1
        LEFT JOIN pagos_poliza as pagp on pagp.idpoliza=pc.id
        LEFT JOIN f_complementopago_documento as fcompd on fcompd.facturasId=fac.FacturasId
        LEFT JOIN f_complementopago as fcomp on fcomp.complementoId=fcompd.complementoId AND fcomp.Estado=1
        WHERE $where and pc.combinada=0
        GROUP BY pc.id";
        return $strq;
    }
    public function sqlfacturassinvinculo($idcliente,$idsrow,$eje){
        if($idsrow==''){
            
            $where=" fac.Clientes_ClientesId='$idcliente' ";
        }else{
            $where="(";
                foreach ($idsrow as $item) {
                    $ve=$item->fac;
                    $where.=" fac.FacturasId= '$ve' OR ";
                }
                    $where.=" fac.FacturasId = '0'";
            $where.=")";
        }
        if($eje>0){
            $where_eje=" and fac.usuario_id='$eje' ";
        }else{
            $where_eje="";
        }
        $strq="SELECT * from(
            SELECT fac.FacturasId,fac.Nombre,fac.Rfc, fac.serie,fac.Folio,fac.Estado,fac.FormaPago,fac.fechatimbre,fac.subtotal,fac.iva,fac.total, facv.facId as facId_v, facpr.facId as facId_pr,facpol.facId as facId_pol,IFNULL(SUM(compd.ImpPagado),0) as ImpPagado,GROUP_CONCAT(DISTINCT(facs.Descripcion2) SEPARATOR '<br>') as Descripcion2,fac.info_factura_x
            FROM f_facturas as fac
            INNER JOIN f_facturas_servicios as facs on facs.FacturasId=fac.FacturasId
            LEFT JOIN factura_venta as facv on facv.facturaId=fac.FacturasId
            LEFT JOIN factura_prefactura as facpr on facpr.facturaId=fac.FacturasId
            LEFT JOIN factura_poliza as facpol on facpol.facturaId=fac.FacturasId
            LEFT JOIN f_complementopago_documento as compd on compd.facturasId =fac.FacturasId
            LEFT JOIN f_complementopago as comp on comp.complementoId=compd.complementoId and comp.Estado=1
            WHERE $where  $where_eje and  fac.Estado=1 AND facv.facId IS null AND facpr.facId IS null AND facpol.facId is null
            GROUP BY fac.FacturasId
            ) as datos where ImpPagado<total";
        $query_tvc = $this->db->query($strq);
        return $query_tvc;
    }
}

?>