<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class Empleados_model extends CI_Model 
{
    public function __construct() 
    {
        parent::__construct();
    }

 	public function getListadoEmpleados(){
        $sql = "SELECT
				per.personalId,
				per.nombre,
				per.sexo,
				per.email,
				per.apellido_materno,
                per.apellido_paterno,
                perf.nombre as perfil,
                usu.UsuarioID,
                per.p_efectivo
				FROM personal as per
                left join usuarios as usu on per.personalId=usu.personalId
                left join perfiles as perf on perf.perfilId=usu.perfilId
				WHERE estatus=1 GROUP BY per.personalId";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function insertaEmpleado($data) 
    {
        $this->db->insert('personal', $data);
        return $this->db->insert_id();
    }

    public function getEmpleadoPorId($idEmpleado)
    {
        $sql = "SELECT
                e.*,
                u.UsuarioID,
                u.perfilId,
                u.Usuario,
                u.contrasena,
                u.texuser
                FROM personal as e
                LEFT JOIN usuarios as u 
                ON e.personalId=u.personalId
                WHERE e.personalId=".$idEmpleado;
        $query = $this->db->query($sql);
        return $query->row();
    }

    public function actualizaEmpleado($data, $id) 
    {
        $this->db->set($data);
        $this->db->where('personalId', $id);
        return $this->db->update('personal');
    }

    public function insertaUsuario($data) 
    {
        $this->db->insert('usuarios', $data);
        return $this->db->insert_id();
    }
   
    public function actualizaUsuario($data, $id) 
    {
        $this->db->set($data);
        $this->db->where('UsuarioID', $id);
        return $this->db->update('usuarios');
    }

    public function getPerfiles()
    {
        $sql = "SELECT * FROM perfiles where activo=1";
        $query = $this->db->query($sql);
        return $query->result();
    }
}