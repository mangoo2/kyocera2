var base_url = $('#base_url').val();
var trserieequipo=0;
var trserieaccesorio=0;
var trserierefaccion=0;
var tablee;
var tablea;
var tabler;
var addtr=0;
$(document).ready(function(){
	$('.chosen-select').chosen({width: "100%"});
	/*
	tablee=$('#tabla_sequipos').dataTable( {
	  "pageLength": 50
	});
	tablea=$('#tabla_saccesorios').dataTable( {
	  "pageLength": 50
	});
	tabler = $('#tabla_srefacciones').dataTable( {
	  "pageLength": 50
	});*/
	aux_verificar=0;
	$('.addbtnequipo').click(function(event) {
		var bodega=$('#bodega1 option:selected').val();
		var bodegat=$('#bodega1 option:selected').text();
		var equipo=$('#equipos option:selected').val();
		var equipot=$('#equipos option:selected').text();
		// Verificar si existe una serie 
		var serie_aux=$('#serie1').val();
		var serie=$('#serie1').val();
		if (serie!=''){
			$.ajax({
		        type:'POST',
		        url: base_url+"index.php/Series/verificar_serie",
		        data: {ser:serie_aux},
		        success: function (response){
		              if(response==1){
		              	aux_verificar=1;
		              	alertfunction('Atención!','El numero de serie ya existe');
		              }else{
		                aux_verificar=0;
		                    var entrarow=1;
							$(".seriev").each(function() {
						        var seriev = $(this).val();
						        if (serie==seriev) {
						        	entrarow=0;
						        }
						    });
						    if (entrarow==1) {
						    	//tablee.fnDestroy();
								//==========================================
								var addtre='<tr class="addtr_'+addtr+'">\
								                <td>\
								                	<input id="bodega" type="hidden"  value="'+bodega+'" readonly>\
								                	'+bodegat+'\
								                </td>\
							                	<td>\
							                		<input id="equipo" type="hidden"  value="'+equipo+'" readonly>\
								                	'+equipot+'\
								                </td>\
							                <td>\
							                	<input id="serie" type="text" class="datostableinput2 seriev" value="'+serie+'" readonly>\
							                </td>\
							                <td><a class="btn-floating waves-effect waves-light red accent-2" onclick="deteletr('+addtr+')"><i class="material-icons">clear</i></a></td>\
							              </tr>';
							    $('.tabla_addsequipos').append(addtre);
							    addtr++;
							    //====================================
							    /*
							    tablee=$('#tabla_sequipos').dataTable( {
								  "pageLength": 50
								});*/

						    }else{
						    	alertfunction('Atención!','El numero de serie duplicado');
						    }
		              }
		        },
		        error: function(response){
		            alertfunction('Atención!','Algo salió mal, intente de nuevo o contacte al administrador del sistema'); 
		        }
		    });
	    ////////////////////////////
		}else{
			alertfunction('Atención!','El numero de serie no debe de estar vacio');
		}
	});
	$('.addbtnaccesorio').click(function(event) {
		var bodega=$('#bodega2 option:selected').val();
		var bodegat=$('#bodega2 option:selected').text();
		var accesorio=$('#accesorio option:selected').val();
		var accesoriot=$('#accesorio option:selected').text();
		var Cantidad=$('#Cantidad_a').val();
		var sinserie=$('#sinserie_a').is(':checked')==true?0:1;

		var serie=$('#serie2').val();
		if (serie!='') {
			$.ajax({
		        type:'POST',
		        url: base_url+"index.php/Series/verificar_serie2",
		        data: {ser:serie},
		        success: function (response){
	                if(response==1){
	                 	aux_verificar=1;
	                	alertfunction('Atención!','El numero de serie ya existe');
	                }else{
	                	aux_verificar=0;
						var entrarow=1;
						$(".seriev").each(function() {
					        var seriev = $(this).val();
					        if(sinserie==1){
						        if (serie==seriev) {
						        	entrarow=0;
						        }
					    	}

					    });
					    if (entrarow==1) {
					    	//tablea.fnDestroy();
							//==========================================
							var addtra='<tr class="addtr_'+addtr+'">\
							                <td>\
							                	<input id="bodega" type="hidden"  value="'+bodega+'" readonly>\
							                	'+bodegat+'\
							                </td>\
						                	<td>\
						                		<input id="accesorio" type="hidden"  value="'+accesorio+'" readonly>\
							                	'+accesoriot+'\
							                </td>\
							                <td>\
							                	<input id="cantidads" type="text" class="datostableinput2 seriev" value="'+Cantidad+'" readonly>\
							                </td>\
						                <td>\
						                	<input id="serie" type="text" class="datostableinput2 seriev" value="'+serie+'" readonly>\
						          			<input id="sinseries" type="hidden" class="datostableinput2 seriev" value="'+sinserie+'" readonly>\
						                </td>\
						                <td><a class="btn-floating waves-effect waves-light red accent-2" onclick="deteletr('+addtr+')"><i class="material-icons">clear</i></a></td>\
						              </tr>';
						    $('.tabla_addsaccesorios').append(addtra);
						    addtr++;
						    //====================================
						    /*
						    tablea=$('#tabla_saccesorios').dataTable( {
							  "pageLength": 50
							});*/
						}else{
					    	alertfunction('Atención!','El numero de serie duplicado');
					    }
					}    
				},
		        error: function(response){
		            alertfunction('Atención!','Algo salió mal, intente de nuevo o contacte al administrador del sistema'); 
		        }    
			});
		}else{
			alertfunction('Atención!','El numero de serie no debe de estar vacio');
		}
	});
	$('.addbtnrefaccion').click(function(event) {
		var bodega=$('#bodega3 option:selected').val();
		var bodegat=$('#bodega3 option:selected').text();
		var refaccion=$('#refaccion option:selected').val();
		var refacciont=$('#refaccion option:selected').text();
		var Cantidad=$('#Cantidad').val();
		var sinserie=$('#sinserie').is(':checked')==true?0:1;

		var serie=$('#serie3').val();
		if (serie!='') {
			$.ajax({
		        type:'POST',
		        url: base_url+"index.php/Series/verificar_serie3",
		        data: {ser:serie},
		        success: function (response){
	                if(response==1){
	                	aux_verificar=1;
	                  	alertfunction('Atención!','El numero de serie ya existe');
	                }else{
						var entrarow=1;
						$(".seriev").each(function() {
					        var seriev = $(this).val();
					        if(sinserie==1){
						        if (serie==seriev) {
						        	entrarow=0;
						        }
					    	}

					    });
					    if (entrarow==1) {
					    	//tabler.fnDestroy();
							//==========================================
							var addtre='<tr class="addtr_'+addtr+'">\
							                <td>\
							                	<input id="bodega" type="hidden"  value="'+bodega+'" readonly>\
							                	'+bodegat+'\
							                </td>\
						                	<td>\
						                		<input id="refaccion" type="hidden"  value="'+refaccion+'" readonly>\
							                	'+refacciont+'\
							                </td>\
							                <td>\
							                	<input id="cantidads" type="text" class="datostableinput2 seriev" value="'+Cantidad+'" readonly>\
							                </td>\
							                <td>\
							                	<input id="serie" type="text" class="datostableinput2 seriev" value="'+serie+'" readonly>\
							                	<input id="sinseries" type="hidden" class="datostableinput2 seriev" value="'+sinserie+'" readonly>\
							                </td>\
						                	<td><a class="btn-floating waves-effect waves-light red accent-2" onclick="deteletr('+addtr+')"><i class="material-icons">clear</i></a></td>\
						              </tr>';
						    $('.tabla_addsrefacciones').append(addtre);
						    addtr++;
						    //====================================
						    /*
						    tabler=$('#tabla_srefacciones').dataTable( {
							  "pageLength": 50
							});*/
					    }else{
					    	alertfunction('Atención!','El numero de serie duplicado');
					    }
					}
				},
		        error: function(response){
		            alertfunction('Atención!','Algo salió mal, intente de nuevo o contacte al administrador del sistema'); 
		        }
		    });
		}else{
			alertfunction('Atención!','El numero de serie no debe de estar vacio');
		}
	});
	$('.saveallseries').click(function(event) {
		$( ".saveallseries" ).prop( "disabled", true );
		$('body').loading({theme: 'dark',message: 'Procesando...'});
	    setTimeout(function(){ 
	         $(".saveallseries" ).prop( "disabled", false );
	    }, 10000);
		$('tr .dataTables_empty').remove();
		var DATAe  = [];
        var TABLA   = $("#tabla_sequipos tbody > tr");
            TABLA.each(function(){         
            item = {};
            item ["bodega"] = $(this).find("input[id*='bodega']").val();
            item ["equipo"]   = $(this).find("input[id*='equipo']").val();
            item ["serie"]  = $(this).find("input[id*='serie']").val();
            DATAe.push(item);
        });
        INFO  = new FormData();
        arrayequipos   = JSON.stringify(DATAe);

        var DATAa  = [];
        var TABLA   = $("#tabla_saccesorios tbody > tr");
            TABLA.each(function(){         
            item = {};
            item ["bodega"] = $(this).find("input[id*='bodega']").val();
            item ["accesorio"]   = $(this).find("input[id*='accesorio']").val();
            item ["serie"]  = $(this).find("input[id*='serie']").val();
            item ["con_serie"]  = $(this).find("input[id*='sinseries']").val();
            item ["cantidad"]  = $(this).find("input[id*='cantidads']").val();
            DATAa.push(item);
        });
        INFO  = new FormData();
        arrayacessorios   = JSON.stringify(DATAa);

        var DATAr  = [];
        var TABLA   = $("#tabla_srefacciones tbody > tr");
            TABLA.each(function(){         
            item = {};
            item ["bodega"] = $(this).find("input[id*='bodega']").val();
            item ["refaccion"]   = $(this).find("input[id*='refaccion']").val();
            item ["serie"]  = $(this).find("input[id*='serie']").val();
            item ["con_serie"]  = $(this).find("input[id*='sinseries']").val();
            item ["cantidad"]  = $(this).find("input[id*='cantidads']").val();
            DATAr.push(item);
        });
        INFO  = new FormData();
        arrayrefacciones  = JSON.stringify(DATAr);



	    var datos = 'arrayequipos='+arrayequipos+'&arrayacessorios='+arrayacessorios+'&arrayrefacciones='+arrayrefacciones;
	    $.ajax({
	        type:'POST',
	        url: base_url+"index.php/Series/addseries",
	        data: datos,
	        success: function (response){
	            idventatabler=response;
	            //modalventasdetalle(response);
	                alertfunction('Éxito!','Se han agregado las series: ');
	                setTimeout(function(){ 
	                    window.location.href = base_url+"index.php/Series";
	                }, 2000);
	             
	            
	            
	            setTimeout(function(){ 
	                //window.location.href = base_url+"index.php/Cotizaciones/listaCotizacionesPorCliente/"+idCliente; 
	            }, 2000);
	            
	        },
	        error: function(response){
	            alertfunction('Atención!','Algo salió mal, intente de nuevo o contacte al administrador del sistema'); 
	            $('body').loading('stop');
	        }
	    });
	});
	$('#sinserie').change(function(event) {
		var sins=$(this).is(':checked');
		console.log(sins);
		if (sins) {
			$('.con_serie').hide( 1000 );
			$('.sin_serie').show( "slow" );
			$('#serie3').val('Sin Serie');
		}else{
			$('.con_serie').show( "slow" );
			$('.sin_serie').hide( 1000 );
			$('#Cantidad').val(1);
			$('#serie3').val('');
		}
	});
	$('#sinserie_a').change(function(event) {
		var sins=$(this).is(':checked');
		console.log(sins);
		if (sins) {
			$('.con_serie_a').hide( 1000 );
			$('.sin_serie_a').show( "slow" );
			$('#serie2').val('Sin Serie');
			$('#con_serie_a').val(1);
		}else{
			$('.con_serie_a').show( "slow" );
			$('.sin_serie_a').hide( 1000 );
			$('#con_serie_a').val(1);
			$('#serie2').val('');
		}
	});
});
function alertfunction(titlea,contenta){
    $.alert({boxWidth: '30%',useBootstrap: false,title: titlea,theme: 'bootstrap',content: contenta});  
}
function deteletr(id){
	$('.addtr_'+id).remove();
}