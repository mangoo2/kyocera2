<style type="text/css">
  .kv-file-upload{
    display: none;
  }
  .error{
    color: red;
  }
</style>
<!-- START CONTENT --> 
<section id="content" width="100%">
  <!--start container-->
  <div class="container" width="100%">
    <div class="section">
      <h4 class="caption">Configuración de facturación.</h4>
      <div class="row">
        <div class="col s12">
          <ul class="collapsible collapsible-accordion" data-collapsible="accordion">
            <li class="active">
              <div class="collapsible-header active">
                Servicios</div>
              <div class="collapsible-body" style="display: block;">
                <div class="row">
                  <div class="col s12">
                    <table id="tabla_servicio"  class="table display" cellspacing="0">
                      <thead>
                        <tr>
                          <th>Clave</th>
                          <th>Concepto</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                  </div>
                </div>
                
              </div>
            </li>
            <li>
              <div class="collapsible-header">
                Unidades</div>
              <div class="collapsible-body">
                <div class="row">
                  <div class="col s12">
                    <table id="tabla_unidades"  class="table display" cellspacing="0" style="width: 100%">
                      <thead>
                        <tr>
                          <th>Clave</th>
                          <th>Concepto</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                  </div>
                </div>
                
              </div>
            </li>
          </ul>
        </div>
       

          
        
      </div>
    </div>
  </div>
</section>