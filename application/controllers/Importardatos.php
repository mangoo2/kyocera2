<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Importardatos extends CI_Controller 
{
    public function __construct() 
    {
        parent::__construct();
        //$this->load->model('ModelGeneral');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fecha_reciente = date('Y-m-d');
    }
    
    function index()     {
            
    }
    public function generatoken(){
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => 'http://semit.vertrou.mx:91/inventarios/api/login/authenticate',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS =>'{"username":"cliente","password":"c0rp0n37"}',
          CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json'
          ),
        ));

        $response = curl_exec($curl);
        $data = json_decode($response);
        $token = json_encode($data->valor);
        $token =str_replace('"', '',$token);
        //echo $token;
        curl_close($curl);
        return $token;
    }
    public function producto(){
        $token=$this->generatoken();
        log_message('error', 'token: '.$token);
        $url="http://semit.vertrou.mx:91/inventarios/api/producto/SEM0206032U7/000001";

            $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => $url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'GET',
          CURLOPT_HTTPHEADER => array(
            'Authorization: Bearer '.$token
          ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return $response;
    }

    

    public function productos($pagina){
        $token=$this->generatoken();
        log_message('error', 'token: '.$token);
        $url="http://semit.vertrou.mx:91/inventarios/api/productos/SEM0206032U7/".$pagina;

            $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => $url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'GET',
          CURLOPT_HTTPHEADER => array(
            'Authorization: Bearer '.$token
          ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        echo $response;
        //return $response;
    }
    
    public function get_productos()
    {
        $result=$this->productos(5);

        echo $result;
        $produ=json_decode($result);
        $resultp=$produ->registros;
        $cont=0;
        var_dump($resultp); 
        $resultf=$this->ModelGeneral->get_select('productos_historial',array('fecha'=>$this->fecha_reciente));
        $sfecha=0;
        foreach ($resultf as $f){
          $sfecha=1;
        }


        if($sfecha==0){
            $this->ModelGeneral->add_record('productos_historial',array('fecha'=>$this->fecha_reciente));
            $this->ModelGeneral->edit_record_tabla(array('estatus'=>0),'productos'); 
            foreach ($resultp as $x){
              $data = array('idProducto'=>$x->idProducto,
              'nombre'=>$x->nombreProducto,
              'marca'=>$x->marca,
              'codigoBarras'=>$x->codigoBarras,
              'unidadMedida'=>$x->unidadMedida,
              'activo'=>$x->activo,
              'activoDesde'=>$x->activoDesde,
              'activoHasta'=>$x->activoHasta,
              'mostrar_pagina'=>$x->pagina,
              'cpaginas'=>$x->cpaginas,
              'listaPrecio'=>$x->listaPrecio,
              'stock'=>$x->existencia,
              'precio_con_iva'=>$x->precio,
              'existenciaAlmacen'=>$x->existenciaAlmacen,
              'comprometido'=>$x->comprometido,
              'pedido'=>$x->pedido,
              'longuitud'=>$x->longuitud,
              'ancho'=>$x->ancho,
              'alto'=>$x->alto,
              'volumen'=>$x->volumen,
              'peso'=>$x->peso,
              'almacen'=>$x->almacen,
              'PrecioV'=>$x->PrecioV,
              'UMLargo'=>$x->UMLargo,
              'UMAncho'=>$x->UMAncho,
              'UMAlto'=>$x->UMAlto,
              'UMPeso'=>$x->UMPeso,
              'porcentajeIva'=>$x->porcentajeIva,
              'descripcion'=>$x->descripcion,
              'categoria'=>$x->idCategoria,
              'Categoriat'=>$x->Categoria,
              'estatus'=>1);
              $result2=$this->ModelGeneral->get_select('productos',array('idProducto'=>$x->idProducto));
              $id=0;
              foreach ($result2 as $p){
                $id=$p->id;
              }
              // agregar cambios de estatus por si no existen 
              if($id==0){
                $this->ModelGeneral->add_record('productos',$data);
              }else{
                $this->ModelGeneral->edit_record('id',$id,$data,'productos');
              }

              $resultc2=$this->ModelGeneral->get_select('categoria',array('categoriaId'=>$x->idCategoria));
              $categoriaId=0;
              foreach ($resultc2 as $c2){
                $categoriaId=$c2->categoriaId;
              }
              
              $datac = array('categoriaId'=>$x->idCategoria,'categoria'=>$x->Categoria); 
              if($categoriaId==0){
                $this->ModelGeneral->add_record('categoria',$datac);
              }else{
                $this->ModelGeneral->edit_record('categoriaId',$categoriaId,$datac,'categoria');
              }

              $cont++;
              echo $cont;
            }
        }
      } 
}