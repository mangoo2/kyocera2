<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class ListadoFolio extends CI_Controller {
    public function __construct()     { 
        parent::__construct();
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fechaactual=date('Y-m-d H:i:s');
        $this->load->model('Clientes_model', 'model');
        $this->load->model('Estados_model', 'estados');
        $this->load->model('ModeloCatalogos');
        $this->load->helper('url');
        $this->idpersonal = $this->session->userdata('idpersonal');
        $this->submenu=49;
    }
    // Listado de Empleados
    function index()     {
        $data['MenusubId']=$this->submenu;
        if($this->session->userdata('logeado')==true){   
            $data['clientes'] = $this->ModeloCatalogos->getlistfoliostatuscli();
            $data['fechainicial_reg'] = date("Y-m-d",strtotime($this->fechaactual."- 5 month")); 
            $data['con_row'] = $this->ModeloCatalogos->getselectwheren('consumibles',array('status'=>1));
            $this->load->view('header');
            $this->load->view('main');
            $this->load->view('almacen/contratofolio/lisfolio',$data);
            $this->load->view('almacen/contratofolio/jslisfolio');
            $this->load->view('footer');
        }else{
            redirect('login');
        }
    }
    function getlistfoliostatus(){
        $params = $this->input->post();
        $productos = $this->ModeloCatalogos->getlistfoliostatus($params);
        $totalRecords=$this->ModeloCatalogos->getlistfoliostatust($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totalRecords),  
            "recordsFiltered" => intval($totalRecords),
            "data"            => $productos->result(),
            "query"           =>$this->db->last_query()   
        );
        
        echo json_encode($json_data);
    }
    function getcodigo(){
        $codigo =  $this->input->post('codigo');
        $result = $this->ModeloCatalogos->getlistfoliocodigo($codigo);
        $aux = 0;
        $html='';
        $idcontratofolio='';
        $modelo='';
        $parte='';
        $folio='';
        $nombre='';
        $reg='';
        $empresa='';
        $idconsumible='';
        foreach ($result as $item) {  
                $aux =1; 
                $idcontratofolio=$item->idcontratofolio;
                $modelo=$item->modelo;
                $parte=$item->parte;
                $folio=$item->foliotext;
                $nombre=$item->nombre;
                $reg=date('d-m-y h:i:s A',strtotime($item->reg));
                $empresa=$item->empresa;  
                $idconsumible=$item->idconsumible; 
        }
        $datarray = array(
                            'aux'=>$aux,
                            'idcontratofolio'=>$idcontratofolio,
                            'modelo'=>$modelo,
                            'parte'=>$parte,
                            'nombre'=>$nombre,
                            'reg'=>$reg,
                            'empresa'=>$empresa,
                            'idconsumible'=>$idconsumible,
                            'folio'=>$folio
                        );
        echo json_encode($datarray);
    }
    function updateretorno(){
        $codigo = $this->input->post('folios');   
        $folio = json_decode($codigo);
        //var_dump($folio);die;
          for ($i=0;$i<count($folio);$i++) { 
              $idcontratofolio = $folio[$i]->idcontratofolio;
              $datac['status'] = 2; 
              $datac['fecharetorno'] = $this->fechahoy;
              $datac['porcentajeretorno'] = $folio[$i]->porcentajeretorno; 
              $comentario = $folio[$i]->comentario; 
              $datac['comentario'] = $comentario; 
              $foliotext = $folio[$i]->folioselected;  
              /// Actualizar retorno         
              $arcIdarray = array('idcontratofolio' => $idcontratofolio);

              $result_vcf=$this->ModeloCatalogos->getselectwheren('contrato_folio',$arcIdarray);
              $status_vcf=0;
              foreach ($result_vcf->result() as $item_vcf) {
                  $status_vcf=$item_vcf->status;
              }

              if($status_vcf<2){
                  $this->ModeloCatalogos->updateCatalogo('contrato_folio',$datac,$arcIdarray);
                  if($folio[$i]->porcentajeretorno==100){
                        $idconsumible=$folio[$i]->idconsumible;
                        //agregar una consulta por si en dado no existe que lo inserte
                        //===================================================================================
                            $r_cb=$this->ModeloCatalogos->getselectwheren('consumibles_bodegas',array('consumiblesId'=>$idconsumible,'bodegaId'=>8));
                            if($r_cb->num_rows()>0){
                                $this->ModeloCatalogos->updatestock2('consumibles_bodegas','total','+',1,'consumiblesId',$idconsumible,'bodegaId',8);//tamaulipas
                            }else{
                                $this->ModeloCatalogos->Insert('consumibles_bodegas',array('total'=>1,'consumiblesId'=>$idconsumible,'bodegaId'=>8));//tamaulipas
                            }
                        //===================================================================================
                        //$this->ModeloCatalogos->updatestock('consumibles_bodegas','stock','+','1','id',$idconsumible);
                       //$this->ModeloCatalogos->updatestock2('consumibles_bodegas','total','+',1,'consumiblesId',$idconsumible,'bodegaId',8);//tamaulipas
                       $this->retornarfolio($foliotext,$comentario);

                       $this->ModeloCatalogos->Insert('bitacora_movimientos',array('id_item'=>0,'cantidad'=>1,'modeloId'=>$idconsumible,'tipo'=>4,'entrada_salida'=>1,'personal'=>$this->idpersonal,'descripcion_evento'=>'Entrada por retorno de consumible '.$foliotext,'serieId'=>0,'clienteId'=>0,'bodega_d'=>8));
                       

                  }elseif($folio[$i]->porcentajeretorno<100 and $folio[$i]->porcentajeretorno>29){
                    $idconsumible=$folio[$i]->idconsumible;
                        //agregar una consulta por si en dado no existe que lo inserte
                        //===================================================================================
                            $r_cb=$this->ModeloCatalogos->getselectwheren('consumibles_bodegas',array('consumiblesId'=>$idconsumible,'bodegaId'=>2));
                            if($r_cb->num_rows()>0){
                                $this->ModeloCatalogos->updatestock2('consumibles_bodegas','total','+',1,'consumiblesId',$idconsumible,'bodegaId',2);//seminuevos
                            }else{
                                $this->ModeloCatalogos->Insert('consumibles_bodegas',array('total'=>1,'consumiblesId'=>$idconsumible,'bodegaId'=>2));//seminuevos
                            }
                        //===================================================================================
                        
                        //this->retornarfolio($foliotext,$comentario);
                        $this->ModeloCatalogos->Insert('bitacora_movimientos',array('id_item'=>0,'cantidad'=>1,'modeloId'=>$idconsumible,'tipo'=>4,'entrada_salida'=>1,'personal'=>$this->idpersonal,'descripcion_evento'=>'Entrada por retorno de consumible '.$foliotext,'serieId'=>0,'clienteId'=>0,'bodega_d'=>2));
                  }
                }
          }  
    }
    function updateretorno_edit(){
        $params = $this->input->post();
        $idc=$params['idc'];
        $por=$params['por'];
        $result_vcf=$this->ModeloCatalogos->getselectwheren('contrato_folio',array('idcontratofolio'=>$idc));
        foreach ($result_vcf->result() as $item_vcf) {
            $idconsumible = $item_vcf->idconsumible;
            $porcentaje = $item_vcf->porcentajeretorno;
            $foliotext = $item_vcf->foliotext;
            if($porcentaje==100){
                $this->ModeloCatalogos->updatestock2('consumibles_bodegas','total','-',1,'consumiblesId',$idconsumible,'bodegaId',8);//tamaulipas
                $this->ModeloCatalogos->Insert('bitacora_movimientos',array('id_item'=>0,'cantidad'=>1,'modeloId'=>$idconsumible,'tipo'=>4,'entrada_salida'=>0,'personal'=>$this->idpersonal,'descripcion_evento'=>'Salida por edicion de retorno de consumible '.$foliotext,'serieId'=>0,'clienteId'=>0,'bodega_o'=>8));
            }elseif($porcentaje<100 and $porcentaje>49){
                $this->ModeloCatalogos->updatestock2('consumibles_bodegas','total','-',1,'consumiblesId',$idconsumible,'bodegaId',2);//seminuevos
                $this->ModeloCatalogos->Insert('bitacora_movimientos',array('id_item'=>0,'cantidad'=>1,'modeloId'=>$idconsumible,'tipo'=>4,'entrada_salida'=>0,'personal'=>$this->idpersonal,'descripcion_evento'=>'Salida por edicion de retorno de consumible '.$foliotext,'serieId'=>0,'clienteId'=>0,'bodega_o'=>2));
            }

            $this->ModeloCatalogos->updateCatalogo('contrato_folio',array('porcentajeretorno'=>$por),array('idcontratofolio'=>$idc));
            //==================================================================
            if($por==100){
                //===================================================================================
                    $r_cb=$this->ModeloCatalogos->getselectwheren('consumibles_bodegas',array('consumiblesId'=>$idconsumible,'bodegaId'=>8));
                    if($r_cb->num_rows()>0){
                        $this->ModeloCatalogos->updatestock2('consumibles_bodegas','total','+',1,'consumiblesId',$idconsumible,'bodegaId',8);//tamaulipas
                    }else{
                        $this->ModeloCatalogos->Insert('consumibles_bodegas',array('total'=>1,'consumiblesId'=>$idconsumible,'bodegaId'=>8));//seminuevos
                    }
                //===================================================================================
                    //$this->ModeloCatalogos->updatestock('consumibles_bodegas','stock','+','1','id',$idconsumible);
                    //$this->ModeloCatalogos->updatestock2('consumibles_bodegas','total','+',1,'consumiblesId',$idconsumible,'bodegaId',8);//tamaulipas
                    //$this->retornarfolio($foliotext,$comentario);

                    $this->ModeloCatalogos->Insert('bitacora_movimientos',array('id_item'=>0,'cantidad'=>1,'modeloId'=>$idconsumible,'tipo'=>4,'entrada_salida'=>1,'personal'=>$this->idpersonal,'descripcion_evento'=>'Entrada por edicion de retorno de consumible '.$foliotext,'serieId'=>0,'clienteId'=>0,'bodega_d'=>8));

            }elseif($por<100 and $por>29){
                //agregar una consulta por si en dado no existe que lo inserte
                //===================================================================================
                    $r_cb=$this->ModeloCatalogos->getselectwheren('consumibles_bodegas',array('consumiblesId'=>$idconsumible,'bodegaId'=>2));
                    if($r_cb->num_rows()>0){
                        $this->ModeloCatalogos->updatestock2('consumibles_bodegas','total','+',1,'consumiblesId',$idconsumible,'bodegaId',2);//seminuevos
                    }else{
                        $this->ModeloCatalogos->Insert('consumibles_bodegas',array('total'=>1,'consumiblesId'=>$idconsumible,'bodegaId'=>2));//seminuevos
                    }
                //===================================================================================
                $this->ModeloCatalogos->Insert('bitacora_movimientos',array('id_item'=>0,'cantidad'=>1,'modeloId'=>$idconsumible,'tipo'=>4,'entrada_salida'=>1,'personal'=>$this->idpersonal,'descripcion_evento'=>'Entrada por edicion de retorno de consumible '.$foliotext,'serieId'=>0,'clienteId'=>0,'bodega_d'=>2));
            }

        }
    }
    function exportar($cliente,$estatus){
        $data['listf'] = $this->ModeloCatalogos->getlistfoliostatus_export($cliente,$estatus);
        $this->load->view('almacen/contratofolio/lisfolio_export',$data);
    }
    function retornarfolio($foliotext,$motivo){
        $result=$this->ModeloCatalogos->getselectwheren('ventas_has_detallesConsumibles',array('foliotext'=>$foliotext));
        $registroretorno=0;
        //================
            $modeloc='';
            $piezasc=0;
            $idconsimible=0;
        //================
        $registro_pre=0;
        foreach ($result->result() as $dataparams) {
            $idrow=$dataparams->id;
            unset($dataparams->id);
            $dataparams->personal_dev=$this->idpersonal;
            $dataparams->motivo_dev=$motivo;
            $this->ModeloCatalogos->Insert('ventas_has_detallesConsumibles_dev',$dataparams);
            $this->ModeloCatalogos->getdeletewheren('ventas_has_detallesConsumibles',array('id'=>$idrow));
            $registroretorno=1;
            $modeloc=$dataparams->modelo;
            $piezasc=$dataparams->piezas;
            $idconsimible=$dataparams->idConsumible;
            $registro_pre=1;
        }
        $resultcf=$this->ModeloCatalogos->getselectwheren('contrato_folio',array('foliotext'=>$foliotext));
        foreach ($resultcf->result() as $itemcf) {
            $idCliente=$itemcf->idCliente;
            //$idrenta=$itemcf->idrenta;
            if($itemcf->idrelacion>0){
                $idrow_cr=$itemcf->idrelacion;
                $resultconsum=$this->ModeloCatalogos->getselectwheren('rentas_has_detallesequipos_consumible',array('id'=>$idrow_cr));
                foreach ($resultconsum->result() as $itemconsu) {
                    $itemconsu->personal_dev=$this->idpersonal;
                    $itemconsu->motivo_dev=$motivo;
                    if($registro_pre==0){//se creeo esta condicion ya que si existe la pre se registra dublicado del registro
                        $this->ModeloCatalogos->Insert('rentas_has_detallesequipos_consumible_dev',$itemconsu);
                    }

                    $this->ModeloCatalogos->getdeletewheren('rentas_has_detallesequipos_consumible',array('id'=>$idrow_cr));

                    //$modeloc='';
                    $piezasc=$itemconsu->cantidad;
                    $idconsimible=$itemconsu->id_consumibles;

                    $registroretorno=1;
                }
            }
            // code...
        }


        if($registroretorno==1){
            //$this->ModeloCatalogos->Insert('bitacora_movimientos',array('entrada_salida'=>1,'descripcion_evento'=>'devolucion de '.$piezasc.' '.$modeloc.' desde ListadoFolio','personal'=>$this->idpersonal,'tipo'=>4,'id_item'=>0,'modeloId'=>$idconsimible,'modelo'=>$modeloc,'cantidad'=>$piezasc,'serieId'=>0,'bodega_d'=>8,'clienteId'=>$idCliente));
        }   
    }
}