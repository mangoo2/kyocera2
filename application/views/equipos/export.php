<?php 
	header("Content-Type: application/vnd.ms-excel");
    header("Content-Disposition: attachment; filename=empleados.xls");
?>
<table border="1">
	<thead>
		<tr>
			<th>Modelo</th>
			<th>Especificaciones</th>
			<th>Especificaciones tecnicas</th>
			<th>Costo venta</th>
			<th>Costo Renta</th>
			<th>Costo Poliza</th>
			<th>Categoria</th>
			<th></th>
			<th></th>
			<th>Costo Revendedor</th>
			<th>Paginas monocromaticas incluidas</th>
			<th>Paginas color incluidas</th>
			<th>Excedente</th>
			<th>Excedente Color</th>
			<th>Numero de parte</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($query->result() as $item) { ?>
			<tr>
				<td><?php echo $item->modelo;?></td>
				<td><?php echo $item->especificaciones;?></td>
				<td><?php echo $item->especificaciones_tecnicas;?></td>
				<td><?php echo $item->costo_venta;?></td>
				<td><?php echo $item->costo_renta;?></td>
				<td><?php echo $item->costo_poliza;?></td>
				<td><?php echo $item->categoriaId;?></td>
				<td><?php echo $item->categoria;?></td>
				<td>--</td>
				<td><?php echo $item->costo_revendedor;?></td>
				<td><?php echo $item->pag_monocromo_incluidos;?></td>
				<td><?php echo $item->pag_color_incluidos;?></td>
				<td><?php echo $item->excedente;?></td>
				<td><?php echo $item->excedentecolor;?></td>
				<td><?php echo $item->noparte;?></td>
			</tr>
		<?php } ?>
	</tbody>
</table>