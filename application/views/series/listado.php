<style type="text/css">
  .chosen-container{    margin-left: 0px;  }
  .seriedisponible,
.serieretorno,
.serierenta,
.seriesalida{text-align: center;  color: #fff;  padding: 1px;border-radius: 6px;box-shadow: 0 2px 2px 0 rgba(0,0,0,0.14), 0 1px 5px 0 rgba(0,0,0,0.12), 0 3px 1px -2px rgba(0,0,0,0.2);}
  .seriedisponible{background-color: #26d79a;  }
  .serieretorno{background-color: #ff9100;  }
  .serierenta{background-color: #26d744;  }
  .seriesalida{background-color: #e31a2f;  }
  td{font-size: 12px;padding: 3px 7px;  }
  .menuaction{width: 170px;  }
  .intermitente{border: 1px solid black;padding: 20px 20px;box-shadow: 0px 0px 20px;animation: infinite resplandorAnimation 2s;  }
@keyframes resplandorAnimation {
  0%,100%{
    box-shadow: 0px 0px 20px red;
  }
  50%{
  box-shadow: 0px 0px 0px red;
  
  }

}
#cant_reguardar{  width: 99%;}
[data-badge].has-badge-danger::after {
  position: inherit;
}
</style>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/bulma-badge.min.css">
<!-- START CONTENT -->
    <section id="content">
      <!--start container-->
        <div class="container">
          <div class="section">

          <input id="idUsuario" type="hidden" name="perfilid" value="<?php echo $_SESSION["usuarioid"]; ?>">
          <input id="perfilid" type="hidden" name="perfilid" value="<?php echo $perfilid; ?>">
          
          <div id="table-datatables">
            <br>
                <h4 class="header">Listado de Series</h4>
                <div class="row">
                    <div class="col s5">
                      <a class="btn waves-effect waves-light gradient-45deg-green-teal gradient-shadow" href="<?php echo base_url(); ?>index.php/Series/add">Nuevo</a>
                    </div>
                    <div class="col s3" align="right">
                      <select id="bodegaId" name="bodegaId" class="browser-default  chosen-select" >
                          <option value="0">Todas las Bodega</option>
                          <?php foreach ($bodegas as $item){ ?>
                              <option value="<?php echo $item->bodegaId ?>"><?php echo $item->bodega ?></option>
                          <?php } ?>
                      </select>
                    </div>
                    <div class="col s3" align="right">
                      <select id="idserie" name="idserie" class="browser-default  chosen-select" onchange="tipo_tabla()">
                          <option value="1">Equipos</option>
                          <option value="2">Accesorios</option>
                          <option value="3">Refacciones</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col s12">
                    <table id="tabla_equipo" class="responsive-table tabla_equipo" cellspacing="0">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Bodega</th>
                          <th>Nombre</th>
                          <th>Serie</th>
                          <th>Fecha</th>
                          <th>Último movimiento</th>
                          <th>Personal</th>
                          <th></th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
          </div>  
        </div>

     </div>
     
    </section>

              
<div class="modal fade" id="modal_traspasos" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Traspasar</h5>
      </div>
      <div class="modal-body">
        <input type="hidden" id="tipo" readonly>
        <input type="hidden" id="trasserieid" readonly>
        <input type="hidden" id="serie_sin_Serie" readonly>
        <input type="hidden" id="trass_cant" readonly>
        <div class="row">   
          <div class="col s12">
            <h2 class="productotras modelotras1"></h2>
          </div>
        </div>
        <div class="row">
          <div class="col s6">
            <label>Bodega Original</label>
            <select class="browser-default chosen-select" id="bodegao" readonly>
              <?php foreach ($bodegas as $item) { ?>
                  <option value="<?php echo $item->bodegaId; ?>" data-bodegatipo="<?php echo $item->tipov; ?>"><?php echo $item->bodega; ?></option>
              <?php } ?> 
            </select>
          </div>
          <div class="col s6">
            <label>Bodega Nueva</label>
            <select id="bodeganew" name="bodega1" class="browser-default chosen-select">
              <option value="0" >Seleccione bodega</option>
              <?php foreach ($bodegas as $item) { ?>
                  <option value="<?php echo $item->bodegaId; ?>" class="bodegatipo_<?php echo $item->tipov; ?>"><?php echo $item->bodega; ?></option>
              <?php } ?> 
            </select>
          </div>
          <div class="col s12 addcantidadtras">
            
          </div>
          <div class="col s12">
            <label>Motivo</label>
            <textarea class="form-control-bmz" id="motivo1"></textarea>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="modal-action modal-close waves-effect waves-red gray btn-flat">Cancelar</button>
        <button type="button" class="btn blue" onclick="aceptar_traspaso()">Aceptar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal_traspasosss" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Traspasar</h5>
      </div>
      <div class="modal-body">
        <input type="hidden" id="cantidadss" readonly>
        <input type="hidden" id="serieIdss" readonly>
        <div class="row">   
          <div class="col s12">
            <h2 class="refaccionesss modelotras2"></h2>
          </div>
        </div>
        <div class="row">
          <div class="col s6">
            <label>Bodega Original</label>
            <select class="browser-default chosen-select" id="bodegaoss" readonly>
              <?php foreach ($bodegas as $item) { ?>
                  <option value="<?php echo $item->bodegaId; ?>" data-bodegatipo="<?php echo $item->tipov; ?>"><?php echo $item->bodega; ?></option>
              <?php } ?> 
            </select>
          </div>
          <div class="col s6">
            <label>Bodega Nueva</label>
            <select id="bodeganewss" name="bodega1" class="browser-default chosen-select">
              <option value="0" >Seleccione bodega</option>
              <?php foreach ($bodegas as $item) { ?>
                  <option value="<?php echo $item->bodegaId; ?>" class="bodegatipo_<?php echo $item->tipov; ?>"><?php echo $item->bodega; ?></option>
              <?php } ?> 
            </select>
          </div>
        </div>
        <div class="row">
          <div class="col s6">
            <label>Cantidad</label>
            <input type="number" id="cantidadnewss" class="form-control" >
            
          </div>
          <div class="col s12">
            <label>Motivo</label>
            <textarea class="form-control-bmz" id="motivo2"></textarea>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="modal-action modal-close waves-effect waves-red gray btn-flat">Cancelar</button>
        <button type="button" class="btn blue" onclick="aceptar_traspasoss()">Aceptar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal_editarserie" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Editar serie</h5>
      </div>
      <div class="modal-body" style="min-height: 250px;">
        <div class="row">   
          <div class="col s6">
            <input type="text" id="serieedit" class="form-control-bmz nombreeditserie">
          </div>
          <div class="col s6">
            <select id="edit_eq_ser" class="browser-default form-control-bmz">
              <?php foreach ($equiposrows->result() as $item) { ?>
                <option value="<?php echo $item->id;?>"><?php echo $item->modelo;?></option>
              <?php } ?>
            </select>
          </div>
        </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="modal-action modal-close waves-effect waves-red gray btn-flat">Cancelar</button>
        <button type="button" class="btn blue" onclick="aceptar_edicionseries()">Aceptar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal_rastrearseries" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Rastrear serie <span class="infoserie"></span></h5>
      </div>
      <div class="modal-body" style="min-height: 250px;">
        <div class="row">   
          <div class="col s12 historialdeseries">
          </div>          
        </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="modal-action modal-close waves-effect waves-red gray btn-flat">Cerrar</button>
      </div>
    </div>
  </div>
</div>