<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inicio extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloCatalogos');
        $this->load->model('Configuraciones_model');
        $this->load->model('ConfiguracionDocumentos_Model');
        $this->load->model('Rentas_model');
    }

	public function index()
    {
        $data['resultzonas']=$this->ModeloCatalogos->view_session_rutas();
        $data['resultstec']=$this->Configuraciones_model->view_session_searchTecnico(); 
        $data['bodegas']=$this->ModeloCatalogos->view_session_bodegas();
        $data['configdoc']=$this->ConfiguracionDocumentos_Model->getConfiguracion(1);

       $this->ModeloCatalogos->view_session_bodegas0();
		$this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('Catalogos/catalogos');
        $this->load->view('templates/footer');
        
	}
    function verificarpagosalperiodo($id){
        $this->ModeloCatalogos->verificarpagosalperiodo($id);
    }
    function pruebafolio($aux,$idRenta){
        $foliotext=$this->Rentas_model->generadorfolio($aux,$idRenta);
        echo $foliotext;
    }
    function pruebafac(){
        $view=1;
        if($view==2){
            $idfactura=5795;//id del periodo
            $tipo=2;//1 individual 2 global
            $mon=1;
            $col=2;
            $equipos=array();
        }else{
            $idfactura=6004;//id del periodo
            $tipo=1;//1 individual 2 global
            $mon=1;
            $col=2;
            $equipos_res = $this->Rentas_model->equiposfactura($idfactura);
            $equipos=array();
            foreach ($equipos_res as $item) {
                //{"equipos":"24610","renta":0,"serie":5145,"tipo":1,"excedente":0}
                $equipos[]=array(
                            'equipos'=>$item->prefdId,
                            'renta'=>$item->statusfacturar,
                            'serie'=>$item->serieId,
                            'tipo'=>$item->tipo,
                            'excedente'=>$item->statusfacturae
                            );

            }
            $equipos =json_encode($equipos);
            echo $equipos;
        }
        
        $html = $this->ModeloCatalogos->factura_requerimientos2($idfactura,$tipo,$mon,$col,$equipos,1);
        var_dump($html);
    }
 
}