<?php

class Modeloentregasdeldia extends CI_Model {
    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }
    
    public function getListaVentasIncompletasfactura($params){
            $tipo=$params['tipo'];
            $cliente=$params['cliente'];
            $status=$params['status'];
            $fecha=$params['fecha'];
            $emp=$params['emp'];
            $tec=$params['tec'];
                $columns = array( 
                    0=>'v.checada',
                    1=>'v.id',
                    2=>'v.empresa',
                    3=>'v.prefactura',
                    4=>"v.tabla",
                    5=>'v.vendedor',  
                    6=>'v.estatus',
                    7=>'v.reg',
                    8=>'v.fechaentrega',
                    9=>'v.e_entrega',
                    10=>'v.id_rh',
                    11=>'v.renta_rh',
                    12=>'v.tipotabla',
                    13=>'ep.nombre',
                    14=>'ep.apellido_paterno',
                    15=>'v.equipo',
                    16=>'v.consumibles',
                    17=>'v.refacciones',
                    18=>'v.poliza',
                    19=>'v.fechaentregah',
                    20=>'v.entregado_fecha',
                    21=>'v.not_dev',
                    22=>'v.tec_ser',
                    23=>'ep2.nombre as nombre2',
                    24=>'ep2.apellido_paterno as apellido_paterno2',
                    25=>'ep2.apellido_materno as apellido_materno2',


                );
                $columns2 = array( 
                    0=>'v.checada',
                    1=>'v.id',
                    2=>'v.empresa',
                    3=>'v.prefactura',
                    4=>"v.tabla",
                    5=>'v.vendedor',  
                    6=>'v.estatus',
                    7=>'v.reg',
                    8=>'v.fechaentrega',
                    9=>'v.e_entrega',
                    10=>'v.id_rh',
                    11=>'v.renta_rh',
                    12=>'v.tipotabla',
                    13=>'ep.nombre',
                    14=>'ep.apellido_paterno',
                    15=>'v.equipo',
                    16=>'v.consumibles',
                    17=>'v.refacciones',
                    18=>'v.poliza',
                    19=>'v.fechaentregah',
                    20=>'v.entregado_fecha',
                    21=>'v.not_dev',
                    22=>'v.tec_ser',
                    

                );
                
                $select="";
                foreach ($columns as $c) {
                    $select.="$c, ";
                }
                $this->db->select($select);
                $this->db->from('vista_entregas_dia_mes v ');
                $this->db->join('personal ep', 'ep.personalId = v.e_entrega_p','left');
                $this->db->join('personal ep2', 'ep2.personalId = v.tec_ser','left');
                $where = array('v.prefactura'=>1);
                if ($cliente>0) {
                    $where['v.idCliente']=$cliente;
                }
                if ($status==1) {
                    $where['v.e_entrega']=1;
                }elseif ($status==2) {
                    $where['v.e_entrega']=0;
                }elseif ($status==3) {
                    $this->db->where(array('v.e_entrega'=>0));
                    //$this->db->where("(v.fechaentregah is null or v.fechaentregah != 'null')");
                    $this->db->where("v.fechaentregah != ''");
                }
                    $where['v.activo']=1;
                if ($tipo>0) {
                    $where['v.tipotabla']=$tipo;
                }
                if ($emp>0) {
                    $where['v.tipov']=$emp;
                }
                if($tec>0){
                    $where['v.tec_ser']=$tec;
                }

                if($fecha!=''){
                    //$where['v.fechaentrega']=$fecha;
                    $this->db->where("(v.fechaentrega='$fecha' or v.fechaentregah like '%$fecha%')");     
                }
                
                $this->db->where($where);
                     
            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->db->group_start();
                foreach($columns2 as $c){
                    $this->db->or_like($c,$search);
                }
                $this->db->group_end();
                
            }
            //==========================================
            $this->db->order_by($columns2[$params['order'][0]['column']], $params['order'][0]['dir']);
            $this->db->limit($params['length'],$params['start']);
            //echo $this->db->get_compiled_select();
            $query=$this->db->get();
            // print_r($query); die;
            return $query;
    }
    public function getListaVentasIncompletasfacturat($params){
            $tipo=$params['tipo'];
            $cliente=$params['cliente'];
            $status=$params['status'];
            $fecha=$params['fecha'];
            $emp=$params['emp'];
            $tec=$params['tec'];
                $columns = array( 
                    0=>'v.id',
                    1=>'v.empresa',
                    2=>'v.prefactura',
                    3=>"v.tabla",
                    4=>'v.vendedor',  
                    5=>'v.estatus',
                    6=>'v.reg',
                    7=>'v.fechaentrega',
                    8=>'v.e_entrega',
                    9=>'v.id_rh',
                    10=>'v.renta_rh',
                    11=>'v.tipotabla',
                    12=>'v.equipo',
                    13=>'v.consumibles',
                    14=>'v.refacciones',
                    15=>'v.poliza',
                    16=>'v.fechaentregah',
                    17=>'v.checada',

                );
                
                $select="";
                foreach ($columns as $c) {
                    $select.="$c, ";
                }
                $this->db->select('COUNT(*) as total');
                $this->db->from('vista_entregas_dia_mes v ');
                $where = array('v.prefactura'=>1);
                if ($cliente>0) {
                    $where['v.idCliente']=$cliente;
                }
                if ($status==1) {
                    $where['v.statuspago']=1;
                }elseif ($status==2) {
                    $where['v.statuspago']=0;
                }elseif ($status==3) {
                    $this->db->where(array('v.e_entrega'=>0));
                    //$this->db->where("(v.fechaentregah is null or v.fechaentregah != 'null')");
                    $this->db->where("v.fechaentregah != ''");
                }
                    $where['v.activo']=1;
                if ($tipo>0) {
                    $where['v.tipotabla']=$tipo;
                }
                if ($emp>0) {
                    $where['v.tipov']=$emp;
                }
                if($tec>0){
                    $where['v.tec_ser']=$tec;
                }
                if($fecha!=''){
                    //$where['v.fechaentrega']=$fecha;  
                    $this->db->where("(v.fechaentrega='$fecha' or v.fechaentregah like '%$fecha%')");  
                }
                $this->db->where($where);
                     
            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->db->group_start();
                foreach($columns as $c){
                    $this->db->or_like($c,$search);
                }
                $this->db->group_end();
                
            }
            //==========================================
            
            $query=$this->db->get();

            return $query->row()->total;
    }
    public function getListaVentas($params){
            $tipo=$params['tipo'];
            $cliente=$params['cliente'];
            $status=$params['status'];
            $fecha=$params['fecha'];
            $fec_ini_reg=$params['fec_ini_reg'];
            $emp=$params['emp'];
            $tec=$params['tec'];
                $columns = array(
                    0=>'v.checada',
                    1=>'v.id',
                    2=>'cli.empresa',
                    3=>'v.prefactura',
                    4=>"'ventas' as tabla",
                    5=>'per.nombre as vendedor',
                    6=>'v.estatus',
                    7=>'pre.reg',
                    8=>'v.fechaentrega',
                    9=>'1 as tipotabla',
                    10=>'v.e_entrega',
                    11=>'v.e_entrega_p',
                    12=>'v.idCliente',
                    13=>'v.statuspago',
                    14=>'0 as id_rh',
                    15=>'0 as renta_rh',
                    16=>'v.id_personal',
                    17=>'ep.nombre',
                    18=>'ep.apellido_paterno',
                    19=>'v.fechaentregah',
                    20=>'v.entregado_fecha',
                    21=>'v.not_dev',
                    22=>'v.tec_ser',
                    23=>'ep2.nombre as nombre2',
                    24=>'ep2.apellido_paterno as apellido_paterno2',
                    25=>'ep2.apellido_materno as apellido_materno2',
                    
                );
                $columnssss = array( 
                    0=>'v.checada',
                    1=>'v.id',
                    2=>'cli.empresa',
                    3=>'v.prefactura',
                    4=>'per.nombre',
                    5=>'per.nombre',
                    6=>'v.estatus',
                    7=>'pre.reg',
                    8=>'v.fechaentrega',
                    9=>'v.e_entrega',
                    10=>'v.e_entrega_p',
                    11=>'v.idCliente',
                    12=>'v.statuspago',
                    13=>'v.id_personal',
                    14=>'ep.nombre',
                    15=>'ep.apellido_paterno',
                );
                
                $select="";
                foreach ($columns as $c) {
                    $select.="$c, ";
                }
                $this->db->select($select);
                $this->db->from('ventas v ');
                $this->db->join('clientes cli', 'cli.id=v.idCliente');
                $this->db->join('personal per', 'per.personalId=v.id_personal');
                $this->db->join('prefactura pre', 'pre.ventaId=v.id and pre.tipo=0','left');
                $this->db->join('personal ep', 'ep.personalId = v.e_entrega_p','left');
                $this->db->join('personal ep2', 'ep2.personalId = v.tec_ser','left');
                $where = array('v.activo'=>1,'v.combinada'=>0,'v.prefactura'=>1);

                if ($cliente>0) {
                    $where['v.idCliente']=$cliente;
                }
                if ($status==1) {
                    $where['v.e_entrega']=1;
                }elseif ($status==2) {
                    $where['v.e_entrega']=0;
                }elseif ($status==3) {
                    $this->db->where(array('v.e_entrega'=>0));
                    //$this->db->where("(v.fechaentregah is null or v.fechaentregah != 'null' or v.fechaentregah != '')");
                    $this->db->where("v.fechaentregah != ''");
                }
                    $where['v.activo']=1;
                if ($emp>0) {
                    $where['v.tipov']=$emp;
                }
                if($tec>0){
                    $where['v.tec_ser']=$tec;
                }
                if($fecha!=''){
                    //$where['v.fechaentrega']=$fecha;  
                    $this->db->where("(v.fechaentrega='$fecha' or v.fechaentregah like '%$fecha%')");  
                }
                if($fec_ini_reg!=''){
                    $where['v.reg >=']=$fec_ini_reg;   
                }
                
                $this->db->where($where);
                     
            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->db->group_start();
                foreach($columnssss as $c){
                    $this->db->or_like($c,$search);
                }
                $this->db->group_end();
                
            }
            //==========================================
            $this->db->order_by($columnssss[$params['order'][0]['column']], $params['order'][0]['dir']);
            $this->db->limit($params['length'],$params['start']);
            //echo $this->db->get_compiled_select();
            $query=$this->db->get();
            // print_r($query); die;
            return $query;
    }
    public function getListaVentast($params){
            $tipo=$params['tipo'];
            $cliente=$params['cliente'];
            $status=$params['status'];
            $fecha=$params['fecha'];
            $fec_ini_reg=$params['fec_ini_reg'];
            $emp=$params['emp'];
            $tec=$params['tec'];
                $columns = array( 
                    0=>'v.id',
                    1=>'cli.empresa',
                    2=>'v.prefactura',
                    3=>'per.nombre',
                    4=>'v.estatus',
                    5=>'pre.reg',
                    6=>'v.fechaentrega',
                    7=>'v.e_entrega',
                    8=>'v.e_entrega_p',
                    9=>'v.idCliente',
                    10=>'v.statuspago',
                    11=>'v.id_personal',
                    12=>'ep.nombre',
                    13=>'ep.apellido_paterno',
                    14=>'v.fechaentregah',
                );
                
                $select="";
                foreach ($columns as $c) {
                    $select.="$c, ";
                }
                $this->db->select('COUNT(*) as total');
                $this->db->from('ventas v ');
                $this->db->join('clientes cli', 'cli.id=v.idCliente');
                $this->db->join('personal per', 'per.personalId=v.id_personal');
                $this->db->join('prefactura pre', 'pre.ventaId=v.id and pre.tipo=0','left');
                $this->db->join('personal ep', 'ep.personalId = v.e_entrega_p','left');

                $where = array('v.prefactura'=>1);
                if ($cliente>0) {
                    $where['v.idCliente']=$cliente;
                }
                if ($status==1) {
                    $where['v.statuspago']=1;
                }elseif ($status==2) {
                    $where['v.statuspago']=0;
                }elseif ($status==3) {
                    $this->db->where(array('v.e_entrega'=>0));
                    //$this->db->where("(v.fechaentregah is null or v.fechaentregah != 'null')");
                    $this->db->where("v.fechaentregah != ''");
                }

                if ($emp>0) {
                    $where['v.tipov']=$emp;
                }
                    $where['v.activo']=1;
               
                if($fecha!=''){
                    //$where['v.fechaentrega']=$fecha;  
                    $this->db->where("(v.fechaentrega='$fecha' or v.fechaentregah like '%$fecha%')");   
                }
                if($fec_ini_reg!=''){
                    $where['v.reg >=']=$fec_ini_reg;   
                }
                if($tec>0){
                    $where['v.tec_ser']=$tec;
                }
                $this->db->where($where);
                     
            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->db->group_start();
                foreach($columns as $c){
                    $this->db->or_like($c,$search);
                }
                $this->db->group_end();
                
            }
            //==========================================
            
            $query=$this->db->get();

            return $query->row()->total;
    }
    public function getListaRentas($params){
            $tipo=$params['tipo'];
            $cliente=$params['cliente'];
            $status=$params['status'];
            $fecha=$params['fecha'];
            $fec_ini_reg=$params['fec_ini_reg'];
            $tec=$params['tec'];
                $columns = array(
                    0=>'v.checada',
                    1=>'v.id',
                    2=>'cli.empresa',
                    3=>'v.prefactura',
                    4=>"'rentas' tabla",
                    5=>'per.nombre vendedor',
                    6=>'v.estatus',
                    7=>'pre.reg',
                    8=>'pre.vencimiento as fechaentrega',
                    9=>'2 as tipotabla',
                    10=>'v.e_entrega',
                    11=>'v.e_entrega_p',
                    12=>'v.idCliente',
                    13=>'v.statuspago',
                    14=>'0 as id_rh',
                    15=>'0 as renta_rh',
                    16=>'v.id_personal',
                    17=>'ep.nombre',
                    18=>'ep.apellido_paterno',
                    19=>'pre.prefacturaId as equipo',
                    20=>'v.entregado_fecha',
                    21=>'0 as not_dev',
                    22=>'v.tec_ser',
                    23=>'ep2.nombre as nombre2',
                    24=>'ep2.apellido_paterno as apellido_paterno2',
                    25=>'ep2.apellido_materno as apellido_materno2',
                );
                $columnssss = array( 
                    0=>'v.checada',
                    1=>'v.id',
                    2=>'cli.empresa',
                    3=>'v.prefactura',
                    4=>'per.nombre',
                    5=>'v.estatus',
                    6=>'pre.reg',
                    7=>'pre.vencimiento',
                    8=>'v.e_entrega',
                    9=>'v.e_entrega_p',
                    10=>'v.idCliente',
                    11=>'v.statuspago',
                    12=>'v.id_personal',
                    13=>'ep.nombre',
                    14=>'ep.apellido_paterno',
                    15=>'pre.prefacturaId'
                );
                
                $select="";
                foreach ($columns as $c) {
                    $select.="$c, ";
                }
                $this->db->select($select);
                $this->db->from('rentas v ');
                $this->db->join('clientes cli', 'cli.id=v.idCliente');
                $this->db->join('personal per', 'per.personalId=v.id_personal');
                $this->db->join('prefactura pre', 'pre.ventaId=v.id and pre.tipo=2','left');
                $this->db->join('personal ep', 'ep.personalId = v.e_entrega_p','left');
                $this->db->join('personal ep2', 'ep2.personalId = v.tec_ser','left');


                $where = array('v.estatus >'=>0,'v.viewed'=>1,'v.prefactura'=>1,'v.cop_par'=>1);
                if ($cliente>0) {
                    $where['v.idCliente']=$cliente;
                }
                if ($status==1) {
                    $where['v.e_entrega']=1;
                }elseif ($status==2) {
                    $where['v.e_entrega']=0;
                }elseif ($status==3) {
                    $where['v.e_entrega']=0;
                }
                
                if($fecha!=''){
                    $where['pre.vencimiento']=$fecha;   
                }
                if($fec_ini_reg!=''){
                    $where['v.reg >=']=$fec_ini_reg;   
                }
                if($tec>0){
                    $where['v.tec_ser']=$tec;
                }
                $this->db->where($where);
                     
            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->db->group_start();
                foreach($columnssss as $c){
                    $this->db->or_like($c,$search);
                }
                $this->db->group_end();
                
            }
            //==========================================
            $this->db->order_by($columnssss[$params['order'][0]['column']], $params['order'][0]['dir']);
            $this->db->limit($params['length'],$params['start']);
            //echo $this->db->get_compiled_select();
            $query=$this->db->get();
            // print_r($query); die;
            return $query;
    }
    public function getListaRentast($params){
            $tipo=$params['tipo'];
            $cliente=$params['cliente'];
            $status=$params['status'];
            $fecha=$params['fecha'];
            $fec_ini_reg=$params['fec_ini_reg'];
            $tec=$params['tec'];
                $columns = array( 
                    0=>'v.id',
                    1=>'cli.empresa',
                    2=>'v.prefactura',
                    3=>'per.nombre',
                    4=>'v.estatus',
                    5=>'pre.reg',
                    6=>'pre.vencimiento',
                    7=>'v.e_entrega',
                    8=>'v.e_entrega_p',
                    9=>'v.idCliente',
                    10=>'v.statuspago',
                    11=>'v.id_personal',
                    12=>'ep.nombre',
                    13=>'ep.apellido_paterno',
                    14=>'pre.prefacturaId'
                );
                
                $select="";
                foreach ($columns as $c) {
                    $select.="$c, ";
                }
                $this->db->select('COUNT(*) as total');
                $this->db->from('rentas v ');
                $this->db->join('clientes cli', 'cli.id=v.idCliente');
                $this->db->join('personal per', 'per.personalId=v.id_personal');
                $this->db->join('prefactura pre', 'pre.ventaId=v.id and pre.tipo=2','left');
                $this->db->join('personal ep', 'ep.personalId = v.e_entrega_p','left');


                $where = array('v.estatus >'=>0,'v.viewed'=>1,'v.prefactura'=>1);
                if ($cliente>0) {
                    $where['v.idCliente']=$cliente;
                }
                if ($status==1) {
                    $where['v.e_entrega']=1;
                }elseif ($status==2) {
                    $where['v.e_entrega']=0;
                }elseif ($status==3) {
                    $where['v.e_entrega']=0;
                }
                
                if($fecha!=''){
                    $where['pre.vencimiento']=$fecha;   
                }
                if($fec_ini_reg!=''){
                    $where['v.reg >=']=$fec_ini_reg;   
                }
                if($tec>0){
                    $where['v.tec_ser']=$tec;
                }
                $this->db->where($where);
                     
            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->db->group_start();
                foreach($columns as $c){
                    $this->db->or_like($c,$search);
                }
                $this->db->group_end();
                
            }
            //==========================================
            
            $query=$this->db->get();

            return $query->row()->total;
    }
    public function getListaRentash($params){
            $tipo=$params['tipo'];
            $cliente=$params['cliente'];
            $status=$params['status'];
            $fecha=$params['fecha'];
            $fec_ini_reg=$params['fec_ini_reg'];
            $tec=$params['tec'];
                $columns = array(
                    0=>'rh.checada',
                    1=>'pre.prefacturaId as id',
                    2=>'cli.empresa',
                    3=>'1 prefactura',
                    4=>"'rentas historial' as tabla",
                    5=>'per.nombre vendedor',
                    6=>'0 as estatus',
                    7=>'pre.reg',
                    8=>'rh.fechaentrega',
                    9=>'5 as tipotabla',
                    10=>'rh.e_entrega',
                    11=>'rh.e_entrega_p',
                    12=>'ren.idCliente',
                    13=>'ren.statuspago',
                    14=>'rh.id as id_rh',
                    15=>'rh.rentas as renta_rh',
                    16=>'ren.id_personal',
                    17=>'ep.nombre',
                    18=>'ep.apellido_paterno',
                    19=>'rh.fechaentregah',
                    20=>'rh.entregado_fecha',
                    21=>'0 as not_dev',
                    22=>'rh.tec_ser',
                    23=>'ep2.nombre as nombre2',
                    24=>'ep2.apellido_paterno as apellido_paterno2',
                    25=>'ep2.apellido_materno as apellido_materno2',
                );
                $columnsss = array(
                    0=>'rh.checada',
                    1=>'pre.prefacturaId',
                    2=>'cli.empresa',
                    3=>'per.nombre',
                    4=>'pre.reg',
                    5=>'rh.fechaentrega',
                    6=>'rh.e_entrega',
                    7=>'rh.e_entrega_p',
                    8=>'ren.idCliente',
                    9=>'ren.statuspago',
                    10=>'rh.id',
                    11=>'rh.rentas',
                    12=>'ren.id_personal',
                    13=>'ep.nombre',
                    14=>'ep.apellido_paterno',
                );
                
                $select="";
                foreach ($columns as $c) {
                    $select.="$c, ";
                }
                $this->db->select($select);
                $this->db->from('rentas_historial rh ');
                $this->db->join('prefactura pre', 'pre.ventaId=rh.id AND pre.tipo=4','left');
                $this->db->join('rentas ren', 'ren.id=rh.rentas'); 
                $this->db->join('clientes cli', 'cli.id=ren.idCliente');
                $this->db->join('personal per', 'per.personalId=ren.id_personal');
                $this->db->join('personal ep', 'ep.personalId = rh.e_entrega_p','left');
                $this->db->join('personal ep2', 'ep2.personalId = rh.tec_ser','left');

                $where = array('rh.activo'=>1,'rh.viewed'=>1);
                if ($cliente>0) {
                    $where['ren.idCliente']=$cliente;
                }
                if ($status==1) {
                    $where['rh.e_entrega']=1;
                }elseif ($status==2) {
                    $where['rh.e_entrega']=0;
                }elseif ($status==3) {
                    $this->db->where(array('rh.e_entrega'=>0));
                    //$this->db->where("(rh.fechaentregah is null or rh.fechaentregah != 'null')");
                    $this->db->where("rh.fechaentregah != ''");
                }
                
                if($fecha!=''){
                    //$where['rh.fechaentrega']=$fecha;  
                    $this->db->where("(rh.fechaentrega='$fecha' or rh.fechaentregah like '%$fecha%')");  
                }
                if($fec_ini_reg!=''){
                    $where['rh.reg >=']=$fec_ini_reg;   
                }
                if($tec>0){
                    $where['rh.tec_ser']=$tec;
                }
                $this->db->where($where);
                     
            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->db->group_start();
                foreach($columnsss as $c){
                    $this->db->or_like($c,$search);
                }
                $this->db->group_end();
                
            }
            //==========================================
            $this->db->order_by($columnsss[$params['order'][0]['column']], $params['order'][0]['dir']);
            $this->db->limit($params['length'],$params['start']);
            //echo $this->db->get_compiled_select();
            $query=$this->db->get();
            // print_r($query); die;
            return $query;
    }
    public function getListaRentasht($params){
            $tipo=$params['tipo'];
            $cliente=$params['cliente'];
            $status=$params['status'];
            $fecha=$params['fecha'];
            $fec_ini_reg=$params['fec_ini_reg'];
            $tec=$params['tec'];
                $columns = array( 
                    0=>'pre.prefacturaId',
                    1=>'cli.empresa',
                    2=>'per.nombre',
                    3=>'pre.reg',
                    4=>'rh.fechaentrega',
                    5=>'rh.e_entrega',
                    6=>'rh.e_entrega_p',
                    7=>'ren.idCliente',
                    8=>'ren.statuspago',
                    9=>'rh.id',
                    10=>'rh.rentas',
                    11=>'ren.id_personal',
                    12=>'ep.nombre',
                    13=>'ep.apellido_paterno',
                );
                
                $select="";
                foreach ($columns as $c) {
                    $select.="$c, ";
                }
                $this->db->select('COUNT(*) as total');
                $this->db->from('rentas_historial rh ');
                $this->db->join('prefactura pre', 'pre.ventaId=rh.id AND pre.tipo=4','left');
                $this->db->join('rentas ren', 'ren.id=rh.rentas'); 
                $this->db->join('clientes cli', 'cli.id=ren.idCliente');
                $this->db->join('personal per', 'per.personalId=ren.id_personal');
                $this->db->join('personal ep', 'ep.personalId = rh.e_entrega_p','left');


                $where = array('rh.activo'=>1,'rh.viewed'=>1);
                if ($cliente>0) {
                    $where['ren.idCliente']=$cliente;
                }
                if ($status==1) {
                    $where['rh.e_entrega']=1;
                }elseif ($status==2) {
                    $where['rh.e_entrega']=0;
                }elseif ($status==3) {
                    $this->db->where(array('rh.e_entrega'=>0));
                    //$this->db->where("(rh.fechaentregah is null or rh.fechaentregah != 'null')");
                    $this->db->where("rh.fechaentregah != ''");
                }
                
                if($fecha!=''){
                    //$where['rh.fechaentrega']=$fecha; 
                    $this->db->where("(rh.fechaentrega='$fecha' or rh.fechaentregah like '%$fecha%')");    
                }
                if($fec_ini_reg!=''){
                    $where['rh.reg >=']=$fec_ini_reg;   
                }
                if($tec>0){
                    $where['rh.tec_ser']=$tec;
                }
                $this->db->where($where);
                     
            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->db->group_start();
                foreach($columns as $c){
                    $this->db->or_like($c,$search);
                }
                $this->db->group_end();
                
            }
            //==========================================
            
            $query=$this->db->get();

            return $query->row()->total;
    }
    public function getListaVentasc($params){
            $tipo=$params['tipo'];
            $cliente=$params['cliente'];
            $status=$params['status'];
            $fecha=$params['fecha'];
            $fec_ini_reg=$params['fec_ini_reg'];
            $emp=$params['emp'];
            $tec=$params['tec'];
                $columns = array(
                    0=>'vc.checada',
                    1=>'vc.combinadaId id',
                    2=>'cli.empresa',
                    3=>'vc.prefactura',
                    4=>"'combinada' tabla",
                    5=>'per.nombre vendedor',
                    6=>'vc.estatus',
                    7=>'pre.reg',
                    8=>'vc.fechaentrega',
                    9=>'4 as tipotabla',
                    10=>'vc.e_entrega',
                    11=>'vc.e_entrega_p',
                    12=>'vc.idCliente',
                    13=>'vc.statuspago',
                    14=>'vc.activo',
                    15=>'0 as id_rh',
                    16=>'0 as renta_rh',
                    17=>'vc.id_personal',
                    18=>'vc.equipo',
                    19=>'vc.consumibles',
                    20=>'vc.refacciones',
                    21=>'vc.poliza',
                    22=>'vc.fechaentregah',
                    23=>'vc.entregado_fecha',
                    24=>'(SELECT pol.not_dev FROM polizasCreadas as pol WHERE pol.id=vc.poliza LIMIT 1) as not_dev',
                    25=>'ep2.nombre as nombre2',
                    26=>'ep2.apellido_paterno as apellido_paterno2',
                    27=>'ep2.apellido_materno as apellido_materno2',

                );

                $columnsss = array(
                    0=>'vc.checada',
                    1=>'vc.combinadaId',
                    2=>'cli.empresa',
                    3=>'vc.prefactura',
                    4=>'per.nombre',
                    5=>'vc.estatus',
                    6=>'pre.reg',
                    7=>'vc.fechaentrega',
                    8=>'vc.e_entrega',
                    9=>'vc.e_entrega_p',
                    10=>'vc.idCliente',
                    11=>'vc.statuspago',
                    12=>'vc.activo',
                    13=>'vc.id_personal',
                    14=>'vc.equipo',
                    15=>'vc.consumibles',
                    16=>'vc.refacciones',
                    17=>'vc.poliza',
                );
                $select="";
                foreach ($columns as $c) {
                    $select.="$c, ";
                }
                $this->db->select($select);
                $this->db->from('ventacombinada vc');
                $this->db->join('clientes cli', 'cli.id=vc.idCliente');
                $this->db->join('personal per', 'per.personalId=vc.id_personal','left');
                $this->db->join('prefactura pre', 'pre.ventaId=vc.combinadaId and pre.tipo=3','left');
                $this->db->join('personal ep', 'ep.personalId = vc.e_entrega_p','left');
                $this->db->join('personal ep2', 'ep2.personalId = vc.tec_ser','left');


                $where = array('vc.activo'=>1,'vc.viewed'=>1,'vc.prefactura'=>1);
                if ($cliente>0) {
                    $where['vc.idCliente']=$cliente;
                }
                if ($status==1) {
                    $where['vc.e_entrega']=1;
                }elseif ($status==2) {
                    $where['vc.e_entrega']=0;
                }elseif ($status==3) {
                    $this->db->where(array('vc.e_entrega'=>0));
                    //$this->db->where("(vc.fechaentregah is null or vc.fechaentregah != 'null')");
                    $this->db->where("vc.fechaentregah != ''");
                }
                if ($emp>0) {
                    $where['vc.tipov']=$emp;
                }
                if($fecha!=''){
                    //$where['vc.fechaentrega']=$fecha;   
                    $this->db->where("(vc.fechaentrega='$fecha' or vc.fechaentregah like '%$fecha%')"); 
                }
                if($fec_ini_reg!=''){
                    $where['vc.reg >=']=$fec_ini_reg;   
                }
                if($tec>0){
                    $where['vc.tec_ser']=$tec;
                }
                $this->db->where($where);
                     
            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->db->group_start();
                foreach($columnsss as $c){
                    $this->db->or_like($c,$search);
                }
                $this->db->group_end();
                
            }
            //==========================================
            $this->db->order_by($columnsss[$params['order'][0]['column']], $params['order'][0]['dir']);
            $this->db->limit($params['length'],$params['start']);
            //echo $this->db->get_compiled_select();
            $query=$this->db->get();
            // print_r($query); die;
            return $query;
    }
    public function getListaVentasct($params){
            $tipo=$params['tipo'];
            $cliente=$params['cliente'];
            $status=$params['status'];
            $fecha=$params['fecha'];
            $fec_ini_reg=$params['fec_ini_reg'];
            $emp=$params['emp'];
            $tec=$params['tec'];
                $columns = array( 
                    0=>'vc.combinadaId',
                    1=>'cli.empresa',
                    2=>'vc.prefactura',
                    4=>'per.nombre',
                    5=>'vc.estatus',
                    6=>'pre.reg',
                    7=>'vc.fechaentrega',
                    9=>'vc.e_entrega',
                    10=>'vc.e_entrega_p',
                    11=>'vc.idCliente',
                    12=>'vc.statuspago',
                    13=>'vc.activo',
                    16=>'vc.id_personal',
                    17=>'vc.equipo',
                    18=>'vc.consumibles',
                    19=>'vc.refacciones',
                    20=>'vc.poliza',
                );
                
                $select="";
                foreach ($columns as $c) {
                    $select.="$c, ";
                }
                $this->db->select('COUNT(*) as total');
                $this->db->from('ventacombinada vc');
                $this->db->join('clientes cli', 'cli.id=vc.idCliente');
                $this->db->join('personal per', 'per.personalId=vc.id_personal','left');
                $this->db->join('prefactura pre', 'pre.ventaId=vc.combinadaId and pre.tipo=3','left');
                $this->db->join('personal ep', 'ep.personalId = vc.e_entrega_p','left');


                $where = array('vc.activo'=>1,'vc.viewed'=>1,'vc.prefactura'=>1);
                if ($cliente>0) {
                    $where['vc.idCliente']=$cliente;
                }
                if ($status==1) {
                    $where['vc.e_entrega']=1;
                }elseif ($status==2) {
                    $where['vc.e_entrega']=0;
                }elseif ($status==3) {
                    $this->db->where(array('vc.e_entrega'=>0));
                    //$this->db->where("(vc.fechaentregah is null or vc.fechaentregah != 'null')");
                    $this->db->where("vc.fechaentregah != ''");
                }
                
                if($fecha!=''){
                    //$where['vc.fechaentrega']=$fecha;
                    $this->db->where("(vc.fechaentrega='$fecha' or vc.fechaentregah like '%$fecha%')");   
                }
                if($fec_ini_reg!=''){
                    $where['vc.reg >=']=$fec_ini_reg;   
                }
                if ($emp>0) {
                    $where['vc.tipov']=$emp;
                }
                if($tec>0){
                    $where['vc.tec_ser']=$tec;
                }
                $this->db->where($where);
                     
            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->db->group_start();
                foreach($columns as $c){
                    $this->db->or_like($c,$search);
                }
                $this->db->group_end();
                
            }
            //==========================================
            
            $query=$this->db->get();

            return $query->row()->total;
    }
    function prefacturaspendientesdeentraga(){
        $strq="SELECT `v`.`id`, `v`.`empresa`, `v`.`prefactura`, `v`.`tabla`, `v`.`vendedor`, `v`.`estatus`, `v`.`reg`, `v`.`fechaentrega`, `v`.`e_entrega`, `v`.`id_rh`, `v`.`renta_rh`, `v`.`tipotabla`, `ep`.`nombre`, `ep`.`apellido_paterno`, `v`.`equipo`, `v`.`consumibles`, `v`.`refacciones`, `v`.`poliza`, `v`.`fechaentregah` 
            FROM `vista_entregas_dia_mes` `v` 
            LEFT JOIN `personal` `ep` ON `ep`.`personalId` = `v`.`e_entrega_p` 
            WHERE `v`.`prefactura` = 1 AND `v`.`activo` = 1 AND v.e_entrega!=1";
        $query = $this->db->query($strq);
        return $query;
    }
}

?>