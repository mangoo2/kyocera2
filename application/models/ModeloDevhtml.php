<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloDevhtml extends CI_Model 
{
    public function __construct() 
    {
        parent::__construct();
    }
    function updateCatalogo($Tabla,$data,$where){
        $this->db->set($data);
        $this->db->where($where);
        $this->db->update($Tabla);
        //return $id;
    }
    function getselectwheren($table,$where){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($where);
        $query=$this->db->get(); 
        return $query;
    }
    function titlehtmlformatocc($tipo,$info){
        if($tipo==1){
            $title='Mono'.$info;
        }else{
            $title='color'.$info;
        }
        $html_title='<table border="1" align="center" cellpadding="2"><thead>';
                  $html_title.='<tr valign="middle" class="styletitles">';
                    $html_title.='<th class="httable" colspan="2" class="styletitles"><b>EQUIPO</b></th>';
                    $html_title.='<th class="httable" colspan="3"><b>PRODUCCIÓN COPIA/IMPRESIÓN '.$title.'</b></th>';
                    $html_title.='<th class="httable" colspan="3"><b>PRODUCCIÓN ESCANEO</b></th>';
                    $html_title.='<th class="httable" rowspan="2"><b>PRODUCCIÓN EQUIPO</b></th>';
                    $html_title.='<th class="httable" rowspan="2"><b>SubTotal</b></th>';
                  $html_title.='</tr>';
                  $html_title.='<tr valign="middle" class="styletitles">';
                    $html_title.='<th class="httable"><b>MODELO</b></th>';
                    $html_title.='<th class="httable"><b>SERIE</b></th>';
                    $html_title.='<th class="httable"><b>CONTADOR INICIAL</b></th>';
                    $html_title.='<th class="httable"><b>CONTADOR FINAL</b></th>';
                    $html_title.='<th class="httable"><b>PRODUCCIÓN</b></th>';
                    $html_title.='<th class="httable"><b>CONTADOR INCIAL</b></th>';
                    $html_title.='<th class="httable"><b>CONTADOR FINAL</b></th>';
                    $html_title.='<th class="httable"><b>PRODUCCIÓN</b></th>';
                    
                  $html_title.='</tr>';
                $html_title.='</thead>';
        return $html_title;
    }
    function result_cceq($idcc,$idpre,$idtipo){
            $strq="SELECT cceq.rowequipo,cceq.modeloid,cceq.serieid,eq.modelo,sp.serie,arpd.tipo,arpd.produccion_total,
                            arpd.c_c_i,arpd.c_c_f,arpd.produccion,arpd.e_c_i,arpd.e_c_f
                    FROM centro_costos_equipos as cceq
                    INNER JOIN equipos as eq on eq.id=cceq.modeloid
                    INNER JOIN series_productos as sp on sp.serieId=cceq.serieid
                    INNER JOIN contrato as con on con.idcontrato=cceq.idcontrato
                    INNER JOIN rentas as ren on ren.id=con.idRenta
                    INNER JOIN rentas_has_detallesEquipos as rde on rde.id=cceq.rowequipo
                    INNER JOIN alta_rentas_prefactura_d as arpd on arpd.productoId=cceq.rowequipo AND arpd.serieId=cceq.serieid
                    WHERE cceq.activo=1 AND cceq.idcentroc=$idcc AND arpd.prefId=$idpre AND arpd.tipo=$idtipo";
            $query = $this->db->query($strq);
            return $query;
    }
    function obtenerproducciontotalcentrocostos($pre,$cce){
            $strq="SELECT rde.id,
                      rde.idEquipo,
                      rde.modelo,
                      sepro.serie,
                      sepro.usado,
                      sepro.serieId,
                      arpd.produccion_total,                      
                      arpd.produccion,
                      arpd.tipo,
                      SUM(arpd.produccion_total) as produccion_total_sum
                FROM `alta_rentas_prefactura_d` as arpd 
                inner JOIN rentas_has_detallesEquipos as rde on rde.id =arpd.productoId
                inner JOIN series_productos as sepro on sepro.serieId=arpd.serieId
                INNER JOIN centro_costos_equipos as cce on cce.serieid=arpd.serieId AND cce.rowequipo=arpd.productoId
                WHERE arpd.prefId='$pre' and cce.idcentroc='$cce' /*AND arpd.tipo=1 */";
            $query = $this->db->query($strq);
            $pro_total=0;
            foreach ($query->result() as $item) {
                $pro_total=$item->produccion_total_sum;
            }
            return $pro_total;
    }

   	public function detalleperiodo($data){
        $actualizardatos=1;//0 no actualiza 1 actualiza
        $html='';
        //===================================
            $idcontrato=$data['idcontrato'];
            $idpre=$data['idpre'];
            $Folio=$data['Folio'];
            $prefacturaId=$data['prefacturaId'];
            $detalleperido=$data['detalleperido'];
            $detalleconsumiblesfolios=$data['detalleconsumiblesfolios'];
            $detallecimages=$data['detallecimages'];
            $rowcentros=$data['rowcentros'];
            $idcondicionextra=$data['idcondicionextra'];
            $descuentogclisck=$data['descuentogclisck'];
            $descuentogscaneo=$data['descuentogscaneo'];
            $periodo=$data['periodo'];
            $periodoante=$data['periodoante'];
            $foliocontrato=$data['foliocontrato'];
            $tiporenta=$data['tiporenta'];
            $grentadeposito=$data['grentadeposito'];
            $grentacolor=$data['grentacolor'];
            $gclicks_mono=$data['gclicks_mono'];
            $gprecio_c_e_mono=$data['gprecio_c_e_mono'];
            $gclicks_color=$data['gclicks_color'];
            $gprecio_c_e_color=$data['gprecio_c_e_color'];
            $nombreclienter=$data['nombreclienter'];
            if($idcondicionextra>0){
              $condiciones = $this->getselectwheren('rentas_condiciones',array('id'=>$idcondicionextra));
              foreach ($condiciones->result() as $itemrc) {
                if($itemrc->tipo>0){
                  $tiporenta = $itemrc->tipo;  
                }
                
              }
            }
        //========================================
            //log_message('error','ordencompra: '.$data['ordencompra']);
            if($data['ordencompra']!=''){
              $oc_la=1;
              $oc_numero_l='<b>Orden de Compra</b>';
              $oc_numero_v=$data['ordencompra'];
              $oc_vigencia_l='<b>Vigencia</b>';
              $oc_vigencia_v=$data['oc_vigencia'];
            }else{
              $oc_la=0;
              $oc_numero_l='';
              $oc_numero_v='';
              $oc_vigencia_l='';
              $oc_vigencia_v='';
            }
          $html.='<table border="1" align="center">';
            $html.='<tr>';
              $html.='<td width="10%" ><b>Contrato</b></td>';
              $html.='<td width="10%" >'.$foliocontrato.'</td>';
              $html.='<td width="60%" ></td>';
              $html.='<td width="10%" >'.$oc_numero_l.'</td>';
              $html.='<td width="10%" >'.$oc_numero_v.'</td>';
            $html.='</tr>';
            if($oc_la==1){
              $html.='<tr>';
                $html.='<td colspan="3"></td>';
                $html.='<td>'.$oc_vigencia_l.'</td>';
                $html.='<td>'.$oc_vigencia_v.'</td>';
              $html.='</tr>';
            }
          $html.='</table>';
        if ($tiporenta==2) {
          //===============
            $valor_g_subtotal=0;
            $valor_g_subtotalcolor=0;
            $valor_g_excedente_m=0;
            $valor_g_excedente_c=0;
          //===============
          if($idcondicionextra>0){
            $condiciones = $this->getselectwheren('rentas_condiciones',array('id'=>$idcondicionextra));
            foreach ($condiciones->result() as $itemrc) {
              $grentadeposito = $itemrc->renta_m;
              $grentacolor = $itemrc->renta_c;
              $gclicks_mono = $itemrc->click_m;
              $gprecio_c_e_mono = $itemrc->excedente_m;
              $gclicks_color = $itemrc->click_c;
              $gprecio_c_e_color = $itemrc->excedente_c;
            }
          }
          $descuentosg=$descuentogclisck+$descuentogscaneo;

          $valor_g_subtotal=$grentadeposito;
          $valor_g_subtotalcolor=$grentacolor;

          $html.='<style type="text/css">
                    .httable{
                      font-size: 8px;
                    }
                  </style>
                  <table border="1" cellpadding="3">
                    <tr>
                      <td align="right" style="background-color: #d8d4d4; color: black">RENTA MENSUAL</td>
                      <td  >$ '.number_format($grentadeposito,2,'.',',').'</td>
                      <td align="right" style="background-color: #d8d4d4; color: black">RENTA MENSUAL</td>
                      <td  >$ '.number_format($grentacolor,2,'.',',').'</td>
                    </tr>
                    <tr>
                      <td colspan="4" align="center" >VOLUMEN INCLUIDO</td>
                    </tr>
                    <tr>
                      <td colspan="2" style="background-color: #d8d4d4; color: black" align="center">MONOCROMÁTICA</td>
                      <td colspan="2" style="background-color: #d8d4d4; color: black" align="center">COLOR</td>
                    </tr>
                    <tr style="background-color: #d8d4d4; color: black">
                      <td align="center">CLICKS</td>
                      <td align="center">EXCEDENTES</td>
                      <td align="center">CLICKS</td>
                      <td align="center">EXCEDENTES</td>
                    </tr>
                    <tr>
                      <td align="center">'.$gclicks_mono.'</td>
                      <td align="center">$ '.number_format($gprecio_c_e_mono,3,'.',',').'</td>
                      <td align="center">'.$gclicks_color.'</td>
                      <td align="center">$ '.number_format($gprecio_c_e_color,3,'.',',').'</td>
                    </tr>
                </table><table><tr><th></th></tr></table>';
                if($descuentosg>0){
                  $html.='<table border="1" cellpadding="3" align="center">
                              <tr>
                                  <td style="background-color: #d8d4d4; color: black">Descuento Clicks</td>
                                  <td>'.$descuentogclisck.'</td>
                                  <td style="background-color: #d8d4d4; color: black">Descuento Escaneo</td>
                                  <td>'.$descuentogscaneo.'</td>
                              </tr>
                            </table>
                            <table><tr><th></th></tr></table>';
                }

                  
            $contadoresmono=0;
            $contadorescolor=0;
            foreach ($detalleperido->result() as $item) {
                if($item->tipo==1){
                  $contadoresmono++;
                }
            }
            $haydespueto=0;$haydespuetoe=0;
            foreach ($detalleperido->result() as $item) {
                if($item->tipo==2){
                  $contadorescolor++;
                }
                if($item->descuento>0){
                  $haydespueto=1;
                }
                if($item->descuentoe>0){
                  $haydespuetoe=1;
                }

            }
            if ($contadoresmono>0) {
              $html.='<table border="1" align="center" cellpadding="2">
                      <thead> 
                        <tr valign="middle" style="background-color: #d8d4d4; color: black">
                          <th class="httable" colspan="2"><b>EQUIPO</b></th>
                          <th class="httable" colspan="3" ><b>PRODUCCIÓN COPIA/IMPRESIÓN MONO</b></th>
                          <th class="httable" colspan="3" ><b>PRODUCCIÓN ESCANEO</b></th>';
                          if($haydespueto==1){
                            $html.='<th class="httable" rowspan="2"><b>DESCUENTO CLICKS</b></th>';  
                          }
                          if($haydespuetoe==1){
                            $html.='<th class="httable" rowspan="2"><b>DESCUENTO ESCANEO</b></th>';  
                          }
                          $html.='<th class="httable" rowspan="2"><b>PRODUCCIÓN EQUIPO</b></th>
                        </tr>
                        <tr valign="middle" style="background-color: #d8d4d4; color: black">
                          <th class="httable"><b>MODELO</b></th>
                          <th class="httable"><b>SERIE</b></th>
                          <th class="httable"><b>CONTADOR INICIAL</b></th>
                          <th class="httable"><b>CONTADOR FINAL</b></th>
                          <th class="httable"><b>PRODUCCIÓN</b></th>
                          <th class="httable"><b>CONTADOR INCIAL</b></th>
                          <th class="httable"><b>CONTADOR FINAL</b></th>
                          <th class="httable"><b>PRODUCCIÓN</b></th>
                        </tr>
                      </thead>
                    ';
                    $producciontotal=0;
                    $trrowcol=$detalleperido->num_rows();
                    foreach ($detalleperido->result() as $item) {
                      if($item->tipo==1){
                        if($item->usado==1){$vusado='-mc';}else{$vusado='';}
                        $html.=' 
                                <tr valign="middle">
                                  <td class="httable">'.$item->modelo.$vusado.'</td>
                                  <td class="httable">'.$item->serie.'</td>
                                  <td class="httable">'.$item->c_c_i.'</td>
                                  <td class="httable">'.$item->c_c_f.'</td>
                                  <td class="httable">'.$tc=$item->c_c_f-$item->c_c_i.'</td>
                                  <td class="httable">'.$item->e_c_i.'</td>
                                  <td class="httable">'.$item->e_c_f.'</td>
                                  <td class="httable">'.$tc=$item->e_c_f-$item->e_c_i.'</td>';
                                  if($haydespueto==1){
                                    $html.='<td class="httable">'.$item->descuento.'</td>';
                                  }
                                  if($haydespuetoe==1){
                                    $html.='<td class="httable">'.$item->descuentoe.'</td>';
                                  }
                                  $html.='<td class="httable">'.$item->produccion.'</td>  
                                </tr>
                              
                            ';
                            $producciontotal=$producciontotal+$item->produccion;
                      }
                    }
                    if($descuentosg>0){
                      $producciontotal=$producciontotal-$descuentosg; 
                    }
                    $volumenexcenetente=$producciontotal-$gclicks_mono;
                    if ($volumenexcenetente<0) {
                      $volumenexcenetente=0;
                    }
                    $totalexcedente=$volumenexcenetente*$gprecio_c_e_mono;
                    $subtotal=$totalexcedente+$grentadeposito;
                    $iva=$subtotal*0.16;
                    $total=$subtotal+$iva;
                    $valor_g_excedente_m=$totalexcedente;
                    $this->updateCatalogo('alta_rentas_prefactura',array('excedente'=>$totalexcedente),array('prefId'=>$Folio));
                      $colspan=8;
                      if($haydespueto==1){
                        $colspan=$colspan+1;
                      }
                      if($haydespuetoe==1){
                        $colspan=$colspan+1;
                      }
                      $html.='
                              <tr valign="middle">
                                  <td class="httable" colspan="'.$colspan.'" align="right">Produccion Total</td>
                                  <td class="httable">'.$producciontotal.'</td>
                                  <td class="httable"></td>
                                </tr>

                      </table><table><tr><th></th></tr></table>
                      <table border="1" align="center" cellpadding="2">
                        <tr>
                          <th>VOLUMEN EXCEDENTE</th>
                          <th>COSTO EXCEDENTE</th>
                          <th>TOTAL EXCEDENTE</th>
                          <th>RENTA FIJA</th>
                          <th>SUBTOTAL</th>
                          <th>IVA</th>
                          <th style="background-color: #d8d4d4; color: black">TOTAL</th>
                        </tr>
                        <tr>
                          <td>'.$volumenexcenetente.'</td>
                          <td>$ '.number_format($gprecio_c_e_mono,3,'.',',').'</td>
                          <td>$ '.number_format($totalexcedente,2,'.',',').'</td>
                          <td>$ '.number_format($grentadeposito,2,'.',',').'</td>
                          <td>$ '.number_format($subtotal,2,'.',',').'</td>
                          <td>$ '.number_format($iva,2,'.',',').'</td>
                          <td style="background-color: #d8d4d4; color: black">$ '.number_format($total,2,'.',',').'</td>
                        </tr>
                      </table>';
                      $this->updateCatalogo('alta_rentas_prefactura',array('cant_ext_mono'=>$volumenexcenetente,'prec_ext_mono'=>$gprecio_c_e_mono),array('prefId'=>$Folio));
            }
            if ($contadorescolor>0) {
              $html.='<table><tr><th></th></tr></table>
                    <table border="1" align="center" cellpadding="2">
                      <thead> 
                        <tr valign="middle" style="background-color: #d8d4d4; color: black">
                          <th class="httable" colspan="2"><b>EQUIPO</b></th>
                          <th class="httable" colspan="3" ><b>PRODUCCIÓN COPIA/IMPRESIÓN COLOR</b></th>
                          <th class="httable" colspan="3" ><b>PRODUCCIÓN ESCANEO</b></th>';
                          if($haydespueto==1){
                            $html.='<th class="httable" rowspan="2"><b>DESCUENTO</b></th>';
                          }
                          $html.='<th class="httable" rowspan="2"><b>PRODUCCIÓN EQUIPO</b></th>
                        </tr>
                        <tr valign="middle" style="background-color: #d8d4d4; color: black">
                          <th class="httable"><b>MODELO</b></th>
                          <th class="httable"><b>SERIE</b></th>
                          <th class="httable"><b>CONTADOR INICIAL</b></th>
                          <th class="httable"><b>CONTADOR FINAL</b></th>
                          <th class="httable"><b>PRODUCCIÓN</b></th>
                          <th class="httable"><b>CONTADOR INCIAL</b></th>
                          <th class="httable"><b>CONTADOR FINAL</b></th>
                          <th class="httable"><b>PRODUCCIÓN</b></th>
                        </tr>
                      </thead>
                    ';
                    $producciontotal=0;
                    $trrowcol=$detalleperido->num_rows();
                    foreach ($detalleperido->result() as $item) {
                      if($item->tipo==2){
                        if($item->usado==1){$vusado='-mc';}else{$vusado='';}
                        $html.=' 
                                <tr valign="middle">
                                  <td class="httable">'.$item->modelo.$vusado.'</td>
                                  <td class="httable">'.$item->serie.'</td>
                                  <td class="httable">'.$item->c_c_i.'</td>
                                  <td class="httable">'.$item->c_c_f.'</td>
                                  <td class="httable">'.$tc=$item->c_c_f-$item->c_c_i.'</td>
                                  <td class="httable">'.$item->e_c_i.'</td>
                                  <td class="httable">'.$item->e_c_f.'</td>
                                  <td class="httable">'.$tc=$item->e_c_f-$item->e_c_i.'</td>';
                                  if($haydespueto==1){
                                    $html.='<td class="httable">'.$item->descuento.'</td> ';
                                  }
                                  if($haydespuetoe==1){
                                    $html.='<td class="httable">'.$item->descuentoe.'</td> ';
                                  }
                                  $html.='<td class="httable">'.$item->produccion.'</td>  
                                </tr>
                              
                            ';
                            $producciontotal=$producciontotal+$item->produccion;
                      }
                    }
                    
                    $volumenexcenetente=$producciontotal-$gclicks_color;
                    if ($volumenexcenetente<0) {
                      $volumenexcenetente=0;
                    }
                    $totalexcedente=$volumenexcenetente*$gprecio_c_e_color;
                    $subtotal=$totalexcedente+$grentacolor;
                    $iva=$subtotal*0.16;
                    $total=$subtotal+$iva;
                    $valor_g_excedente_c=$totalexcedente;
                    $this->ModeloCatalogos->updateCatalogo('alta_rentas_prefactura',array('excedentecolor'=>$totalexcedente),array('prefId'=>$Folio));
                      $colspan=7;
                      if($haydespueto==1){
                        $colspan=$colspan+1;
                      }
                      if($haydespuetoe==1){
                        $colspan=$colspan+1;
                      }
                      $html.='
                              <tr valign="middle">
                                  <td class="httable" colspan="'.$colspan.'" align="right">Produccion Total</td>
                                  <td class="httable">'.$producciontotal.'</td>
                                  <td class="httable"></td>
                                </tr>

                      </table><table><tr><th></th></tr></table>
                      <table border="1" align="center" cellpadding="2">
                        <tr>
                          <th>VOLUMEN EXCEDENTE</th>
                          <th>COSTO EXCEDENTE</th>
                          <th>TOTAL EXCEDENTE</th>
                          <th>RENTA FIJA</th>
                          <th>SUBTOTAL</th>
                          <th>IVA</th>
                          <th style="background-color: #d8d4d4; color: black">TOTAL</th>
                        </tr>
                        <tr>
                          <td>'.$volumenexcenetente.'</td>
                          <td>$ '.number_format($gprecio_c_e_color,3,'.',',').'</td>
                          <td>$ '.number_format($totalexcedente,2,'.',',').'</td>
                          <td>$ '.number_format($grentacolor,2,'.',',').'</td>
                          <td>$ '.number_format($subtotal,2,'.',',').'</td>
                          <td>$ '.number_format($iva,2,'.',',').'</td>
                          <td style="background-color: #d8d4d4; color: black">$ '.number_format($total,2,'.',',').'</td>
                        </tr>
                      </table>
                      ';
                      $this->updateCatalogo('alta_rentas_prefactura',array('cant_ext_color'=>$volumenexcenetente,'prec_ext_color'=>$gprecio_c_e_color),array('prefId'=>$Folio));
            }
                   $valor_g_total=$valor_g_subtotal+$valor_g_subtotalcolor+$valor_g_excedente_m+$valor_g_excedente_c;
            //log_message('error','contrato:'.$idcontrato.' periodo:'.$Folio.' Renta m: '.$valor_g_subtotal.' renta c: '.$valor_g_subtotalcolor.' exmono:'.$valor_g_excedente_m.' excolor:'.$valor_g_excedente_c.' Total:'.$valor_g_total);
            $array_data_u=array(
                                'subtotal'=>$valor_g_subtotal,
                                'subtotalcolor'=>$valor_g_subtotalcolor,
                                'excedente'=>$valor_g_excedente_m,
                                'excedentecolor'=>$valor_g_excedente_c,
                                'total'=>$valor_g_total
                              );
            if($actualizardatos==1){
              $this->updateCatalogo('alta_rentas_prefactura',$array_data_u,array('prefId'=>$Folio));
            }
        }else{
          //===============
            $valor_g_subtotal=0;
            $valor_g_subtotalcolor=0;
            $valor_g_excedente_m=0;
            $valor_g_excedente_c=0;
          //===============
          $haydespueto=0;
          $haydespuetoe=0;
                $html.='<style type="text/css">
                    .httable{
                      font-size: 7.5px;
                    }
                  </style>';
                    foreach ($detalleperido->result() as $item) {
                      if($item->descuento>0){
                        $haydespueto=1;
                      }
                      if($item->descuentoe>0){
                        $haydespuetoe=1;
                      }
                    }
                    foreach ($detalleperido->result() as $item) {

                      if($item->usado==1){$vusado='-mc';}else{$vusado='';}
                      if($item->tipo==2){
                        $title='COLOR';
                        $volumenincluido =  $item->clicks_color;
                        $costorenta=round($item->precio_rc,2);
                        $costoexcedente  =  round($item->precio_c_e_color,2);
                        if($idcondicionextra>0){
                          $condicionesdll = $this->getselectwheren('rentas_condiciones_detalle',array('idc'=>$idcondicionextra,'equiporow'=>$item->id,'serieId'=>$item->serieId));
                          foreach ($condicionesdll->result() as $itemcdll) {
                            $volumenincluido = $itemcdll->click_c;
                            $costorenta = $itemcdll->renta_c;
                            $costoexcedente = $itemcdll->excedente_c;
                          }
                        }
                        $valor_g_subtotalcolor=$valor_g_subtotalcolor+$costorenta;
                      }else{
                        $title='MONO';
                        $volumenincluido =  $item->clicks_mono;
                        $costorenta=round($item->precio_r,2);
                        $costoexcedente  =  round($item->precio_c_e_mono,3);
                        if($idcondicionextra>0){
                          $condicionesdll = $this->getselectwheren('rentas_condiciones_detalle',array('idc'=>$idcondicionextra,'equiporow'=>$item->id,'serieId'=>$item->serieId));
                          foreach ($condicionesdll->result() as $itemcdll) {
                            $volumenincluido = $itemcdll->click_m;
                            $costorenta = $itemcdll->renta_m;
                            $costoexcedente = $itemcdll->excedente_m;
                          }
                        }
                        $valor_g_subtotal=$valor_g_subtotal+$costorenta;
                      }
                      $html.='<table border="1" align="center" cellpadding="2">
                      <thead> 
                        <tr valign="middle" style="background-color: #d8d4d4; color: black">
                          <th class="httable" colspan="2"><b>EQUIPO</b></th>
                          <th class="httable" colspan="3" ><b>PRODUCCIÓN COPIA/IMPRESIÓN '.$title.'</b></th>
                          <th class="httable" colspan="3" ><b>PRODUCCIÓN ESCANEO</b></th>';
                          if($haydespueto==1){
                            $html.='<th class="httable" rowspan="2"><b>DESCUENTO CLICKS</b></th>';  
                          }
                          if($haydespuetoe==1){
                            $html.='<th class="httable" rowspan="2"><b>DESCUENTO ESCANEO</b></th>';  
                          }
                          
                          $html.='<th class="httable" rowspan="2"><b>PRODUCCIÓN EQUIPO</b></th>
                          <th class="httable" rowspan="2"><b>VOLUMEN INCLUIDO</b></th>
                        </tr>
                        <tr valign="middle" style="background-color: #d8d4d4; color: black">
                          <th class="httable"><b>MODELO</b></th>
                          <th class="httable"><b>SERIE</b></th>
                          <th class="httable"><b>CONTADOR INICIAL</b></th>
                          <th class="httable"><b>CONTADOR FINAL</b></th>
                          <th class="httable"><b>PRODUCCIÓN</b></th>
                          <th class="httable"><b>CONTADOR INCIAL</b></th>
                          <th class="httable"><b>CONTADOR FINAL</b></th>
                          <th class="httable"><b>PRODUCCIÓN</b></th>
                        </tr>
                      </thead>';
                        $excedente=$item->produccion-$volumenincluido;
                        if ($excedente<0) {
                          $excedente=0;
                        }
                        if($item->tipo==2){
                          $valor_g_excedente_c=$valor_g_excedente_c+($excedente*$costoexcedente);
                        }else{
                          $valor_g_excedente_m=$valor_g_excedente_m+($excedente*$costoexcedente);
                        }
                        $totalexcedente=($excedente*$costoexcedente);
                        $subtotal=$costorenta+$totalexcedente;
                        $subtotaliva=$subtotal*0.16;
                        $totalgeneral=round($subtotal+$subtotaliva,2);
                        $html.=' 
                                <tr valign="middle">
                                  <td class="httable">'.$item->modelo.$vusado.'</td>
                                  <td class="httable">'.$item->serie.'</td>
                                  <td class="httable">'.$item->c_c_i.'</td>
                                  <td class="httable">'.$item->c_c_f.'</td>
                                  <td class="httable">'.$tc=$item->c_c_f-$item->c_c_i.'</td>
                                  <td class="httable">'.$item->e_c_i.'</td>
                                  <td class="httable">'.$item->e_c_f.'</td>
                                  <td class="httable">'.$tc=$item->e_c_f-$item->e_c_i.'</td>';
                                  if($haydespueto==1){
                                    $html.='<td class="httable">'.$item->descuento.'</td>'; 
                                    $colspan=2;
                                  }else{
                                    $colspan=1;
                                  }
                                  if($haydespuetoe==1){
                                    $html.='<td class="httable">'.$item->descuentoe.'</td>'; 
                                    $colspane=2;
                                  }else{
                                    $colspane=1;
                                  }
                                  $html.='<td class="httable">'.$item->produccion.'</td>  
                                  <td class="httable">'.$volumenincluido.'</td>  
                                </tr>
                                <tr>
                                  <td colspan="10"> </td>
                                </tr>
                                <tr valign="middle">
                                  <td colspan="2">VOLUMEN EXCEDENTE</td> 
                                  <td colspan="2">COSTO EXCEDENTE</td>  
                                  <td colspan="2">RENTA FIJA</td> 
                                  <td colspan="2">SUBTOTAL</td>
                                  <td colspan="'.$colspan.'">IVA</td>  
                                  <td colspan="'.$colspane.'">TOTAL</td>  
                                </tr>
                                <tr valign="middle">
                                  <td colspan="2">'.$excedente.'</td> 
                                  <td colspan="2">$'.$costoexcedente.'</td>  
                                  <td colspan="2">$'.$costorenta.'</td> 
                                  <td colspan="2">$'.$subtotal.'</td>
                                  <td colspan="'.$colspan.'">$'.$subtotaliva.'</td>  
                                  <td colspan="'.$colspane.'">$'.$totalgeneral.'</td>   
                                </tr>

                      </table><table><tr><th></th></tr></table>';
                      $array_data_u_d=array(
                                          'costoexcedente'=>$costoexcedente,
                                          'totalexcedente'=>$totalexcedente
                                            );
                      if($actualizardatos==1){
                        $this->updateCatalogo('alta_rentas_prefactura_d',$array_data_u_d,array('prefdId'=>$item->prefdId));
                      }
                    }
                   $valor_g_total=$valor_g_subtotal+$valor_g_subtotalcolor+$valor_g_excedente_m+$valor_g_excedente_c;
            //log_message('error','contrato:'.$idcontrato.' periodo:'.$Folio.' Renta m: '.$valor_g_subtotal.' renta c: '.$valor_g_subtotalcolor.' exmono:'.$valor_g_excedente_m.' excolor:'.$valor_g_excedente_c.' Total:'.$valor_g_total);
            $array_data_u=array(
                                'subtotal'=>$valor_g_subtotal,
                                'subtotalcolor'=>$valor_g_subtotalcolor,
                                'excedente'=>$valor_g_excedente_m,
                                'excedentecolor'=>$valor_g_excedente_c,
                                'total'=>$valor_g_total
                              );
            if($actualizardatos==1){
              $this->updateCatalogo('alta_rentas_prefactura',$array_data_u,array('prefId'=>$Folio));
            }
        }
        return $html;
   	}
    function detalleperiodo_cc($data){
        $actualizardatos=1;//0 no actualiza 1 actualiza
        //===================================
            $idcontrato=$data['idcontrato'];
            $idpre=$data['idpre'];
            $Folio=$data['Folio'];
            $prefacturaId=$data['prefacturaId'];
            $detalleperido=$data['detalleperido'];
            $detalleconsumiblesfolios=$data['detalleconsumiblesfolios'];
            $detallecimages=$data['detallecimages'];
            $rowcentros=$data['rowcentros'];
            $idcondicionextra=$data['idcondicionextra'];
            $descuentogclisck=$data['descuentogclisck'];
            $descuentogscaneo=$data['descuentogscaneo'];
            $periodo=$data['periodo'];
            $periodoante=$data['periodoante'];
            $foliocontrato=$data['foliocontrato'];
            $tiporenta=$data['tiporenta'];
            $grentadeposito=$data['grentadeposito'];
            $grentacolor=$data['grentacolor'];
            $gclicks_mono=$data['gclicks_mono'];
            $gprecio_c_e_mono=$data['gprecio_c_e_mono'];
            $gclicks_color=$data['gclicks_color'];
            $gprecio_c_e_color=$data['gprecio_c_e_color'];
            $nombreclienter=$data['nombreclienter'];
            $gel_fol = $data['gel_fol'];
            $tipo_cc = $data['tipo_cc'];
        //========================================
            $tipo=$tipo_cc;
            $costoclick_m=0;
                  $costoclick_c=0;
                  $rowcobrar=0;
                  $html='';
                  $precio_excedente_mono=0;
                  $precio_excedente_color=0;
                  $clicks_mono=0;
                  $clicks_color=0;
            //===============================================================
              //=============================================
                    $result_ar=$this->getselectwheren('alta_rentas',array('idcontrato'=>$idcontrato,'activo'=>1));
                    foreach ($result_ar->result() as $item) {
                      if($item->monto>0){
                        $costoclick_m=$item->monto/$item->clicks_mono;
                      }
                      if($item->montoc>0){
                        $costoclick_c=$item->montoc/$item->clicks_color;
                      }
                      if($item->precio_c_e_mono>0){
                        $precio_excedente_mono=$item->precio_c_e_mono;
                      }
                      if($item->precio_c_e_color>0){
                        $precio_excedente_color=$item->precio_c_e_mono;
                      }
                      //$clicks_mono=$item->clicks_mono;
                      //$clicks_color=$item->clicks_color;
                    }
              //======================================================================
                    $result_arp=$this->getselectwheren('alta_rentas_prefactura',array('prefId'=>$idpre,'activo'=>1));
                    $idcondicionextra=0;
                    foreach ($result_arp->result() as $item) {
                      $idcondicionextra=$item->idcondicionextra;
                    }
                    if($idcondicionextra>0){
                      $condiciones = $this->getselectwheren('rentas_condiciones',array('id'=>$idcondicionextra));
                      foreach ($condiciones->result() as $itemrc) {
                        if($itemrc->renta_m>0){
                          $costoclick_m=$itemrc->renta_m/$itemrc->click_m;
                        }
                        if($itemrc->renta_c>0){
                          $costoclick_c=$itemrc->renta_c/$itemrc->click_c;
                        }
                        if($itemrc->excedente_m>0){
                          $precio_excedente_mono=$itemrc->excedente_m;
                        }
                        if($itemrc->excedente_c>0){
                          $precio_excedente_color=$itemrc->excedente_c;
                        }
                        //$clicks_mono=$itemrc->click_m;
                        //$clicks_color=$itemrc->click_c;
                      }
                    }
              //======================================================================

            //===============================================================
            $html=' <style type="text/css">.httable{font-size: 8px;}.styletitles{text-align:center;background-color: #d8d4d4; color: black}</style>
            <table border="1"><tr><td width="10%" align="center">Contrato</td><td width="10%" align="center">'.$foliocontrato.'</td><td width="60%" ></td><td  width="10%" align="center"><b>FACTURA</b></td><td width="10%" ></td></tr>  </table>';
            if($tipo==0){
              $total_general_cc_general=0;
              foreach ($rowcentros->result() as $item_cc) {
                  $idccs=$item_cc->idcentroc;
                  //============================================
                      $result_cc=$this->getselectwheren('centro_costos',array('id'=>$idccs));
                      $clicks_mono=0;
                      $clicks_color=0;
                      $mono_renta_cc=0;
                      $color_renta_cc=0;
                      foreach ($result_cc->result() as $item) {
                        $descripcion=$item->descripcion;
                        $clicks_mono=$item->clicks;
                        $clicks_color=$item->clicks_c;
                        if($clicks_mono>0){
                          $mono_renta_cc=$clicks_mono*$costoclick_m;
                        }
                        if($clicks_color>0){
                          $color_renta_cc=$clicks_color*$costoclick_c;
                        }
                      }
                  //============================================


                $html.='<table border="1"  cellpadding="3"><tr><td class="styletitles" colspan="3"><b>'.$item_cc->descripcion.'</b></td></tr><tr><td colspan="3">';
                  $total_general_cc=0;
                  //===========================================
                      if($tipo==0){//0 detallado 1 monto global
                        
                           $html_title=$this->titlehtmlformatocc(1,'');
                        if($clicks_mono>0){
                          $result_cceq=$this->result_cceq($idccs,$idpre,1);
                          $num_equipos=$result_cceq->num_rows();
                          if($num_equipos>0){
                            $monto_x_equipo=round($mono_renta_cc/$num_equipos,3);
                              $html.=$html_title;
                            foreach ($result_cceq->result() as $item) {
                              $pro_imp=$item->c_c_f-$item->c_c_i;
                              $pro_es=$item->e_c_f-$item->e_c_i;
                              
                              $produccion_total_eq=$pro_imp+$pro_es;
                              $html.='<tr>';
                                      $html.='<td>'.$item->modelo.'</td>';
                                      $html.='<td>'.$item->serie.'</td>';
                                      $html.='<td>'.$item->c_c_i.'</td>';
                                      $html.='<td>'.$item->c_c_f.'</td>';
                                      $html.='<td>'.$pro_imp.'</td>';
                                      $html.='<td>'.$item->e_c_i.'</td>';
                                      $html.='<td>'.$item->e_c_f.'</td>';
                                      $html.='<td>'.$pro_es.'</td>';
                                      $html.='<td>'.$produccion_total_eq.'</td>';
                                      $html.='<td>$'.$monto_x_equipo.'</td>';
                                    $html.='</tr>';
                                    $total_general_cc=$total_general_cc+$monto_x_equipo;

                                      
                            
                                    //$rowcobrar++;
                            }
                            $html.='</table>';
                            $html.='<table><tr><td></td></tr></table>';
                            
                          }
                        }else{
                          $result_cceq=$this->result_cceq($idccs,$idpre,1);
                          $num_equipos=$result_cceq->num_rows();
                          //log_message('error','$num_equipos:'.$num_equipos);
                          if($num_equipos>0){
                            //$monto_x_equipo=round($mono_renta_cc/$num_equipos,3);
                            /* 
                              echo round( 1.55, 1, PHP_ROUND_HALF_UP);   //  1.6
                              echo round( 1.54, 1, PHP_ROUND_HALF_UP);   //  1.5
                            */
                              $html_title=$this->titlehtmlformatocc(1,'');
                              $html.=$html_title;
                            foreach ($result_cceq->result() as $item) {
                              $monto_x_equipo=round($item->produccion_total*$precio_excedente_mono,3);
                              if($monto_x_equipo>0 and $clicks_mono==0){
                              $pro_imp=$item->c_c_f-$item->c_c_i;
                              $pro_es=$item->e_c_f-$item->e_c_i;
                              //$html.=$html_title;
                              $produccion_total_eq=$pro_imp+$pro_es;
                              $html.='<tr>
                                        <td>'.$item->modelo.'</td>
                                        <td>'.$item->serie.'</td>
                                        <td>'.$item->c_c_i.'</td>
                                        <td>'.$item->c_c_f.'</td>
                                        <td>'.$pro_imp.'</td>
                                        <td>'.$item->e_c_i.'</td>
                                        <td>'.$item->e_c_f.'</td>
                                        <td>'.$pro_es.'</td>
                                        <td>'.$produccion_total_eq.'</td>
                                        <td>$'.$monto_x_equipo.'</td>
                                      </tr>';
                                      $total_general_cc=$total_general_cc+$monto_x_equipo;
                                    //$rowcobrar++;
                                }
                            }
                            $html.='</table>';
                            $html.='<table><tr><td></td></tr></table>';
                          }
                        }

                        if($clicks_color>0){
                          $result_cceq=$this->result_cceq($idccs,$idpre,2);
                          $num_equipos=$result_cceq->num_rows();
                          
                          if($num_equipos>0){
                            $monto_x_equipo=round($color_renta_cc/$num_equipos,3);
                            /* 
                              echo round( 1.55, 1, PHP_ROUND_HALF_UP);   //  1.6
                              echo round( 1.54, 1, PHP_ROUND_HALF_UP);   //  1.5
                            */
                              $html_title=$this->titlehtmlformatocc(2,3);
                          $html.=$html_title;
                            foreach ($result_cceq->result() as $item) {
                              $pro_imp=$item->c_c_f-$item->c_c_i;
                              $pro_es=$item->e_c_f-$item->e_c_i;
                              $html.=$html_title;
                              $produccion_total_eq=$pro_imp+$pro_es;
                              $html.='<tr>
                                        <td>'.$item->modelo.'</td>
                                        <td>'.$item->serie.'</td>
                                        <td>'.$item->c_c_i.'</td>
                                        <td>'.$item->c_c_f.'</td>
                                        <td>'.$pro_imp.'</td>
                                        <td>'.$item->e_c_i.'</td>
                                        <td>'.$item->e_c_f.'</td>
                                        <td>'.$pro_es.'</td>
                                        <td>'.$produccion_total_eq.'</td>
                                        <td>$'.$monto_x_equipo.'</td>
                                      </tr>';
                                      $total_general_cc=$total_general_cc+$monto_x_equipo;

                                    
                                    //$rowcobrar++;
                            }
                            $html.='</table>';
                            $html.='<table><tr><td></td></tr></table>';
                          }
                          
                        }else{
                          $result_cceq=$this->result_cceq($idccs,$idpre,2);
                          $num_equipos=$result_cceq->num_rows();
                          if($num_equipos>0 and $clicks_color==0){
                            //$monto_x_equipo=round($color_renta_cc/$num_equipos,3);
                            /* 
                              echo round( 1.55, 1, PHP_ROUND_HALF_UP);   //  1.6
                              echo round( 1.54, 1, PHP_ROUND_HALF_UP);   //  1.5
                            */
                            if($result_cceq->num_rows()>0) {
                              $row_montoxeq=0;
                              foreach ($result_cceq->result() as $item) {
                                  $monto_x_equipo=round($item->produccion_total*$precio_excedente_color,3);
                                  if($monto_x_equipo>0){
                                    $row_montoxeq++;
                                  }
                              }
                              if($row_montoxeq>0){
                                $html_title=$this->titlehtmlformatocc(2,'');
                            
                                $html.=$html_title;
                                  foreach ($result_cceq->result() as $item) {
                                    $monto_x_equipo=round($item->produccion_total*$precio_excedente_color,3);
                                    if($monto_x_equipo>0){
                                      $pro_imp=$item->c_c_f-$item->c_c_i;
                                      $pro_es=$item->e_c_f-$item->e_c_i;
                                      $html.=$html_title;
                                      $produccion_total_eq=$pro_imp+$pro_es;
                                      $html.='<tr>
                                              <td>'.$item->modelo.'</td>
                                              <td>'.$item->serie.'</td>
                                              <td>'.$item->c_c_i.'</td>
                                              <td>'.$item->c_c_f.'</td>
                                              <td>'.$pro_imp.'</td>
                                              <td>'.$item->e_c_i.'</td>
                                              <td>'.$item->e_c_f.'</td>
                                              <td>'.$pro_es.'</td>
                                              <td>'.$produccion_total_eq.'</td>
                                              <td>$'.$monto_x_equipo.'</td>
                                            </tr>';
                                            $total_general_cc=$total_general_cc+$monto_x_equipo;
                                            
                                          //$rowcobrar++;
                                    }
                                  }
                                  $html.='</table>';
                                  $html.='<table><tr><td></td></tr></table>';
                              }
                            }
                          }
                        }
                      }
                      $total_general_cc_general=$total_general_cc_general+$total_general_cc;
                $html.='</td></tr><tr><td width="80%"></td><td width="10%">Total</td><td width="10%">$ '.number_format($total_general_cc,2,'.',',').'</td></tr></table><table><tr><td></td></tr></table>';
              }
                $html.='<table border="1" cellpadding="3"><tr><td width="80%"></td><td width="10%"><b>Total General</b></td><td width="10%">$ '.number_format($total_general_cc_general,2,'.',',').'</td></tr></table>';
            }
            if($tipo==1){//0 detallado 1 monto global
              $html_title='<table border="1" align="center" cellpadding="2">
                            <thead> 
                              <tr valign="middle" class="styletitles">
                                <th class="httable"><b>Centro de costos</b></th>
                                <th class="httable"><b>Periodo</b></th>
                                <th class="httable"><b>Producción</b></th>
                                <th class="httable"><b>Monto</b></th>
                                <th class="httable"><b>Iva</b></th>
                                <th class="httable"><b>Total</b></th>
                              </tr>
                            </thead>';
                $html.=$html_title;
              foreach ($rowcentros->result() as $item_cc) {
                  $idccs=$item_cc->idcentroc;
                  //======================================
                    //=============================================
                      $result_cc=$this->getselectwheren('centro_costos',array('id'=>$idccs));
                      $clicks_mono=0;
                      $clicks_color=0;
                      $mono_renta_cc=0;
                      $color_renta_cc=0;
                      foreach ($result_cc->result() as $item) {
                        $descripcion=$item->descripcion;
                        $clicks_mono=$item->clicks;
                        $clicks_color=$item->clicks_c;
                        if($clicks_mono>0){
                          $mono_renta_cc=$clicks_mono*$costoclick_m;
                        }
                        if($clicks_color>0){
                          $color_renta_cc=$clicks_color*$costoclick_c;
                        }
                      }
                    //=============================================
                  //======================================
                      $r_produccion=$this->obtenerproducciontotalcentrocostos($idpre,$idccs);
                      if($mono_renta_cc>0){
                        $iva=round($mono_renta_cc*0.16,2);
                        $total=round($mono_renta_cc+$iva,2);
                        $html.='<tr>
                                  <td>'.$item_cc->descripcion.'</td>
                                  <td>'.$periodo.'</td>
                                  <td>'.$r_produccion.'</td>
                                  <td>$ '.number_format($mono_renta_cc, 2, '.', ',').'</td>
                                  <td>$ '.number_format($iva, 2, '.', ',').'</td>
                                  <td>$ '.number_format($total, 2, '.', ',').'</td>
                                </tr>';
                        
                        
                      }
                      if($color_renta_cc>0){
                          $iva=round($color_renta_cc*0.16,2);
                          $total=round($color_renta_cc+$iva,2);
                          $html.='<tr>
                                    <td>'.$item_cc->descripcion.'</td>
                                    <td>'.$periodo.'</td>
                                    <td>'.$r_produccion.'</td>
                                    <td>$ '.number_format($color_renta_cc, 2, '.', ',').'</td>
                                    <td>$ '.number_format($iva, 2, '.', ',').'</td>
                                    <td>$ '.number_format($total, 2, '.', ',').'</td>
                                  </tr>';
                         
                      }
                      if($precio_excedente_mono>0 and $clicks_mono==0){
                        $result_cceq=$this->result_cceq($idccs,$idpre,1);
                        $num_equipos=$result_cceq->num_rows();
                        
                        $produccion_total=0;
                        if($num_equipos>0){
                          //$monto_x_equipo=round($mono_renta_cc/$num_equipos,3);
                          /* 
                            echo round( 1.55, 1, PHP_ROUND_HALF_UP);   //  1.6
                            echo round( 1.54, 1, PHP_ROUND_HALF_UP);   //  1.5
                          */
                            
                          foreach ($result_cceq->result() as $item) {
                            $produccion_total=$produccion_total+$item->produccion_total;
                          }
                        }
                        if($produccion_total>0){
                          $mono_renta_cc=$produccion_total*$precio_excedente_mono;
                          
                          $iva=round($mono_renta_cc*0.16,2);
                          $total=round($mono_renta_cc+$iva,2);

                          $html.='<tr>
                                    <td>'.$item_cc->descripcion.'</td>
                                    <td>'.$periodo.'</td>
                                    <td>'.$r_produccion.'</td>
                                    <td>$ '.number_format($mono_renta_cc, 2, '.', ',').'</td>
                                    <td>$ '.number_format($iva, 2, '.', ',').'</td>
                                    <td>$ '.number_format($total, 2, '.', ',').'</td>
                                  </tr>';

                          
                        }
                      }
                      if($precio_excedente_color>0 and $clicks_color==0){
                        $result_cceq=$this->result_cceq($idccs,$idpre,2);
                        $num_equipos=$result_cceq->num_rows();
                        $produccion_total=0;
                        if($num_equipos>0){
                          //$monto_x_equipo=round($color_renta_cc/$num_equipos,3);
                          /* 
                            echo round( 1.55, 1, PHP_ROUND_HALF_UP);   //  1.6
                            echo round( 1.54, 1, PHP_ROUND_HALF_UP);   //  1.5
                          */
                          foreach ($result_cceq->result() as $item) {
                            $produccion_total=$produccion_total+$item->produccion_total;
                          }
                          if($produccion_total>0){
                            $color_renta_cc=$produccion_total*$precio_excedente_color;

                            $iva=round($color_renta_cc*0.16,2);
                            $total=round($color_renta_cc+$iva,2);

                          $html.='<tr>
                                    <td>'.$item_cc->descripcion.'</td>
                                    <td>'.$periodo.'</td>
                                    <td>$ '.number_format($color_renta_cc, 2, '.', ',').'</td>
                                    <td>$ '.number_format($iva, 2, '.', ',').'</td>
                                    <td>$ '.number_format($total, 2, '.', ',').'</td>
                                  </tr>';


                            
                          }
                        }
                      }
              }
              $html.='</table>';
            }

        return $html;

    }
}

?>