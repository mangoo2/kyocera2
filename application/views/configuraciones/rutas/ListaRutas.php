<!-- START CONTENT -->
<section id="content">
  <!--start container-->
  <div class="container">
    <div class="section">
      <div class="card-panel">
          <div id="table-datatables">
            <br>
            <h4 class="header">Formulario de registro de Rutas</h4>
            <div class="row">
              <div class="col s12">

                <form class="form" id="form_rutas" method="post">

                  <div class="row">
                    <div class="input-field col s10">
                      <i class="material-icons prefix">navigation</i>
                      <input id="nombre" name="nombre" type="text" >
                      <label for="nombre">Nombre de Ruta</label>
                    </div>
                    <div class="input-field col s2">
                      <button type="submit" class="btn-floating waves-effect waves-light cyan"><i class="material-icons">add</i></button>
                    </div>
                  </div>

                  <div class="row">

                    <div class="input-field col s12" style="overflow-y: scroll; height: 250px;">
  
                     <table id="tabla_rutas">
                      <thead>
                        <tr>
                            <th> </th>
                            <th>Nombre</th>
                            <th>Acciones</th>
                            <th> </th>
                        </tr>
                      </thead>

                      <tbody >
                        <?php
                        foreach ($rutas as $r) {
                            echo " <tr>
                            <td></td>
                            <td style='display:none'>$r->id</td>
                            <td>$r->nombre</td>
                            <td><a class='btn btn-floating waves-effect waves-light red eliminar'><i class='material-icons'>delete_forever</i></a></td>
                            </tr>";
                        }
                        ?> 
                      </tbody>
                    </table>
                    </div>
                  </div>

                </form>
              </div>
            </div>
          </div>
      </div>
    </div>  
  </div>
</section>
