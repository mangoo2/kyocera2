<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Inventario_productos extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloCatalogos');
        $this->submenu=71;
        if($this->session->userdata('logeado')==true){
            $this->perfilid =$this->session->userdata('perfilid');
            $this->idpersonal = $this->session->userdata('idpersonal');
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,$this->submenu);// 52 es el id del submenu
            if ($permiso==0) {
                redirect('Sistema');
            }
        }else{
            redirect('Sistema'); 
        }
    }

    public function index(){
        $data['MenusubId']=$this->submenu;
        $data['resultbodega']=$this->ModeloCatalogos->getselectwheren('bodegas',array('activo'=>1)); 
    	$this->load->view('header');
        $this->load->view('main');
        $this->load->view('Reportes/inventario_productos',$data);
        $this->load->view('footer');
        $this->load->view('Reportes/inventario_productosjs');
    }
    public function selecte(){
        $search = $this->input->get('search');
        $results=$this->ModeloCatalogos->getseleclike('equipos','modelo',$search);
        echo json_encode($results->result());
    }
    public function selectr(){
        $search = $this->input->get('search');
        $results=$this->ModeloCatalogos->getseleclike2('refacciones','nombre',$search);
        echo json_encode($results->result());
    }
    public function selecta(){
        $search = $this->input->get('search');
        $results=$this->ModeloCatalogos->getseleclike2('catalogo_accesorios','nombre',$search);
        echo json_encode($results->result());
    }
    public function selectc(){
        $search = $this->input->get('search');
        $results=$this->ModeloCatalogos->getseleclike2('consumibles','modelo',$search);
        echo json_encode($results->result());
    }

    function inventario($tipoview=0){
        $params=$this->input->get();
        $tipo=$params['tipo'];
        $producto=$params['producto'];
        $series=$params['series'];
        $bodega=$params['bodega'];
        if($tipoview==0){
            header("Pragma: public");
            header("Expires: 0");
            $filename = "Inventario.xls";
            header("Content-type: application/x-msdownload");
            header("Content-Disposition: attachment; filename=$filename");
            header("Pragma: no-cache");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        }
        




        $html='<table border="1">
        <thead>
                    <tr>
                        <th></th>
                        <th>Bodega</th>
                        <th>Item</th>
                        <th>No. Parte</th>
                        <th>Stock</th>
                        <th>Resguardo</th>';
                        if($series==1){
                            $html.='<th>Series</th>';
                        }
                $html.='</tr></thead><tbody>';
        if($tipo==1 or $tipo==0){//equipo
            if($producto>0){
                $where_pro=" and eq.id='$producto' ";
            }else{
                $where_pro="";
            }
            if($bodega>0){
                $where_bod=" and serpro.bodegaId='$bodega' ";
            }else{
                $where_bod="";
            }
            $strq="SELECT eq.id,bod.bodega, serpro.bodegaId,eq.modelo,eq.noparte, SUM(serpro.cantidad) as cantidad, SUM(serpro.resguardar_cant) as resguardar_cant,  GROUP_CONCAT(serpro.serie SEPARATOR ', ') as series
            FROM equipos as eq
            LEFT JOIN series_productos as serpro ON serpro.productoid=eq.id AND serpro.status<2 AND serpro.activo=1
            LEFT JOIN bodegas as bod on bod.bodegaId=serpro.bodegaId
            WHERE eq.estatus=1 $where_pro $where_bod
            GROUP BY serpro.bodegaId,eq.id  
            ORDER BY `eq`.`id` ASC";

            $query= $this->db->query($strq);

            foreach ($query->result() as $item) {
                $html.='<tr>
                        <td>Equipo</td>
                        <td>'.utf8_decode($item->bodega).'</td>
                        <td>'.$item->modelo.'</td>
                        <td>'.$item->noparte.'</td>
                        <td>'.$item->cantidad.'</td>
                        <td>'.$item->resguardar_cant.'</td>';
                        if($series==1){
                            $html.='<td>'.$item->series.'</td>';
                        }
             $html.='</tr>';
            }
        }
        if($tipo==2 or $tipo==0){//refacciones
            if($producto>0){
                $where_pro=" and ref.id = '$producto' ";
            }else{
                $where_pro="";
            }
            if($bodega>0){
                $where_bod=" and serref.bodegaId='$bodega' ";
            }else{
                $where_bod="";
            }
            $strq="SELECT bod.bodega,serref.bodegaId,ref.id, ref.nombre,ref.codigo,serref.con_serie,
                SUM(IF(serref.con_serie=1,1,serref.cantidad)) as cantidad,
                SUM(serref.resguardar_cant) as resguardar_cant,
                GROUP_CONCAT(serref.serie SEPARATOR ', ') as series
                FROM refacciones as ref
                LEFT JOIN series_refacciones as serref on serref.refaccionid=ref.id AND serref.activo=1 AND serref.status<2
                LEFT JOIN bodegas as bod ON bod.bodegaId=serref.bodegaId
                WHERE ref.status=1  $where_bod $where_pro
                GROUP BY ref.id,serref.bodegaId,serref.con_serie  
                ORDER BY `ref`.`id` ASC";

            $queryr= $this->db->query($strq);

            foreach ($queryr->result() as $item) {
                $html.='<tr>
                        <td>Refacciones</td>
                        <td>'.utf8_decode($item->bodega).'</td>
                        <td>'.$item->nombre.'</td>
                        <td>'.$item->codigo.'</td>
                        <td>'.$item->cantidad.'</td>
                        <td>'.$item->resguardar_cant.'</td>';
                        if($series==1){
                            $html.='<td></td>';
                        }
                $html.='</tr>';
            }
        }
        if($tipo==3 or $tipo==0){//accesorios
            if($producto>0){
                $where_pro=" and caacc.id='$producto' ";
            }else{
                $where_pro="";
            }
            if($bodega>0){
                $where_bod=" and seracc.bodegaId='$bodega' ";
            }else{
                $where_bod="";
            }

            $strq="SELECT bod.bodega,seracc.bodegaId, caacc.nombre,caacc.id,caacc.no_parte,seracc.con_serie,GROUP_CONCAT(seracc.serie SEPARATOR ', ') as series,SUM(seracc.cantidad) as cantidad, SUM(seracc.resguardar_cant) as resguardar_cant
                FROM catalogo_accesorios as caacc
                LEFT JOIN series_accesorios as seracc on seracc.accesoriosid=caacc.id AND seracc.status<2 AND seracc.activo=1
                LEFT JOIN bodegas as bod ON bod.bodegaId=seracc.bodegaId
                WHERE caacc.status=1 $where_bod $where_pro
                GROUP BY seracc.bodegaId,caacc.id,seracc.con_serie";
           
            $querya= $this->db->query($strq);
            foreach ($querya->result() as $item) {
                
                $html.='<tr>
                        <td>Accesorio</td>
                        <td>'.utf8_decode($item->bodega).'</td>
                        <td>'.$item->nombre.'</td>
                        <td>'.$item->no_parte.'</td>
                        <td>'.$item->cantidad.'</td>
                        <td>'.$item->resguardar_cant.'</td>';
                        if($series==1){
                            $html.='<td>'.$item->series.'</td>';
                        }
            $html.='</tr>';
            }
            
        }
        if($tipo==4 or $tipo==0){//consumibles
            $columns=''; 
            $columns.='co.id,';
            $columns.='cobo.bodegaId,';
            $columns.='co.parte,';
            $columns.='co.modelo,';
            $columns.='cobo.total,';
            $columns.='bo.bodega,';
            $columns.='cobo.consubId,';
            $columns.='cobo.resguardar_cant';
            $this->db->select($columns);
            $this->db->from('consumibles co');
            $this->db->join('consumibles_bodegas cobo', 'co.id=cobo.consumiblesId and cobo.status = 1','left');
            $this->db->join('bodegas bo', 'bo.bodegaId=cobo.bodegaId','left');
            $this->db->where(array('co.status'=>1));
            if($producto>0){
                $this->db->where(array('co.id'=>$producto));
            }
            if($bodega>0){
                $this->db->where(array('cobo.bodegaId'=>$bodega));
            }
            $queryc=$this->db->get();
            foreach ($queryc->result() as $item) {
                
                $html.='<tr>
                        <td>Consumible</td>
                        <td>'.$item->bodega.'</td>
                        <td>'.$item->modelo.'</td>
                        <td>'.$item->parte.'</td>
                        <td>'.$item->total.'</td>
                        <td>'.$item->resguardar_cant.'</td>';
                        if($series==1){
                            $html.='<td></td>';
                        }
            $html.='</tr>';
            }
        }





        $html.='</tbody><table>';
        echo $html;
        
    }



    

}
?>