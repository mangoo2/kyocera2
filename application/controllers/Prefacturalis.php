<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Prefacturalis extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->helper('url');
         $this->load->model('ModeloCatalogos');
        //$this->load->model('ModeloGeneral');//insert generales
        //$this->load->model('Cotizaciones_model');
        //$this->load->model('Equipos_model');
        //$this->load->model('Polizas_model');
        //$this->load->model('Clientes_model');
        //$this->load->model('Prospectos_model');
        //$this->load->model('Consumibles_model');
        //$this->load->model('Refacciones_model');
        //$this->load->model('ConfiguracionDocumentos_Model');
        $this->load->model('Ventaspre_model');
        //$this->load->model('Rentas_model');
        //$this->load->model('Personal/ModeloPersonal');
        $this->load->library('menus');
        if ($this->session->userdata('logeado')!=true) {
            redirect('login');
        }else{
            $this->idpersonal=$this->session->userdata('idpersonal');
        }
    }
    public function index(){
        $where = array('personalId' =>$this->idpersonal);  
        $result=$this->ModeloCatalogos->getselectwheren('personal',$where);
        foreach ($result->result() as $item) {
           $data['nombre']=$item->nombre;
           $data['apellido_paterno']=$item->apellido_paterno;
           $data['apellido_materno']=$item->apellido_materno;
        }
        $where_m = array('activo' =>1);  
        $data['get_metodo_pago']=$this->ModeloCatalogos->getselectwheren('f_metodopago',$where_m);
        $menus = $this->menus->getMenus($this->session->userdata('perfilid'));
        $where_p = array('tipo' =>2,'estatus'=>1);  
        $data['get_personal']=$this->ModeloCatalogos->getselectwheren('personal',$where_p);
        $data['menus'] = $menus['menus'];
        $data['submenusArray'] = $menus['submenusArray'];

        $this->load->view('header');
        $this->load->view('main',$data);
        $this->load->view('ventas/pre_listado');
        $this->load->view('ventas/pre_listadojs');
        $this->load->view('footer');     
	}
    public function getListaVentasIncompletas() {
        $params = $this->input->post();
        $params['idpersonal']=$this->idpersonal;
        $productos = $this->Ventaspre_model->getListaVentasIncompletasfactura($params);
        $totalRecords=$this->Ventaspre_model->getListaVentasIncompletasfacturat($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totalRecords),  
            "recordsFiltered" => intval($totalRecords),
            "data"            => $productos->result(),
            "query"           =>$this->db->last_query()   
        );
        
        echo json_encode($json_data);
    }

    function get_excel_prefactura($tipoventa,$cliente,$tipoestatus,$f1,$f2,$idpersonal)
    {
        $data['f1']=$f1;
        $data['f2']=$f2;
        $data['tipoventa']=$tipoventa;
        $data['personal']='';
        $data['cliente']='';
        $where = array('personalId' =>$idpersonal);  
        $result_p=$this->ModeloCatalogos->getselectwheren('personal',$where);
        foreach ($result_p->result() as $x) {
            $data['personal']=$x->nombre;
        }

        $where_c = array('id' =>$cliente);  
        $result_c=$this->ModeloCatalogos->getselectwheren('clientes',$where_c);
        foreach ($result_c->result() as $c) {
            $data['cliente']=$c->empresa;
        }

        $data['get_result'] = $this->Ventaspre_model->get_prefactura($tipoventa,$cliente,$tipoestatus,$f1,$f2,$idpersonal);
        $this->load->view('ventas/prefactura_excel',$data);
    }

    
}

?>