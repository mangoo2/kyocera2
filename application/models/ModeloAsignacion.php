<?php
defined('BASEPATH') OR exit ('No direct script access allowed');
class ModeloAsignacion extends CI_Model {
    public function __construct()     {
        parent::__construct();
        $this->DB8 = $this->load->database('other8_db', TRUE);
        $this->DB9 = $this->load->database('other9_db', TRUE);
        $this->DB10 = $this->load->database('other10_db', TRUE);

        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fechaactual = date('Y-m-d');
        $this->horaactual = date('G:i');
    }
    function Insert($Tabla,$data){
        $this->db->insert($Tabla, $data);
        $id=$this->db->insert_id();
        return $id;
    }
    function updateCatalogo($Tabla,$data,$where){
        $this->db->set($data);
        $this->db->where($where);
        $this->db->update($Tabla);
        //return $id;
    }
    function getselectwheren($table,$where){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($where);
        $query=$this->db->get(); 
        return $query;
    }
    function Totalventas_accesorios($params){

        if(isset($params['fecha'])){
                $fecha=$params['fecha'];
            }else{
                $fecha='';
            }
            if($fecha!=''){
                $w_fecha=" and v.reg >='$fecha' ";
            }else{
                $w_fecha='';
            }
            if(isset($params['tipoc'])){
                $tipoc=$params['tipoc'];
            }else{
                $tipoc=2;
            }
            if($tipoc<2){
                $w_statis_s=" and vd.serie_estatus='$tipoc' ";
            }else{
                $w_statis_s='';
            }
        $strq = "SELECT COUNT(*) as total
                from ventas_has_detallesEquipos_accesorios as vd
                inner JOIN ventas as v on v.id=vd.id_venta
                inner JOIN personal as pr on pr.personalId=v.id_personal
                inner JOIN bodegas as bo on bo.bodegaId=vd.serie_bodega
                inner JOIN clientes as cl on cl.id=v.idCliente
                inner JOIN catalogo_accesorios as cata on cata.id=vd.id_accesorio
                where v.activo=1 and vd.ocultarsolicitudserie=0 $w_fecha $w_statis_s
                ";
        $query = $this->DB10->query($strq);
        $total=0;
        foreach ($query->result() as $row) {
            $total=$row->total;
        }
        return $total;
    }
    function Totalventas_equipos($params){
        if(isset($params['fecha'])){
            $fecha=$params['fecha'];
        }else{
            $fecha='';
        }
        if($fecha!=''){
            $w_fecha=" and v.reg >='$fecha' ";
        }else{
            $w_fecha='';
        }
        if(isset($params['tipoc'])){
            $tipoc=$params['tipoc'];
        }else{
            $tipoc=2;
        }
        if($tipoc<2){
            $w_statis_s=" and vd.serie_estatus='$tipoc' ";
        }else{
            $w_statis_s='';
        }
        $strq = "SELECT COUNT(*) as total
                from ventas_has_detallesEquipos as vd
                inner JOIN ventas as v on v.id=vd.idVenta
                inner JOIN personal as pr on pr.personalId=v.id_personal
                inner JOIN bodegas as bo on bo.bodegaId=vd.serie_bodega
                inner JOIN clientes as cl on cl.id=v.idCliente
                where v.activo=1 and vd.ocultarsolicitudserie=0 $w_fecha $w_statis_s
                ";
        $query = $this->DB10->query($strq);
        $total=0;
        foreach ($query->result() as $row) {
            $total=$row->total;
        }
        return $total;
    }
    function Totalventas_refacciones($params){
        if(isset($params['fecha'])){
            $fecha=$params['fecha'];
        }else{
            $fecha='';
        }
        if($fecha!=''){
            $w_fecha=" and v.reg >='$fecha' ";
        }else{
            $w_fecha='';
        }
        if(isset($params['tipoc'])){
            $tipoc=$params['tipoc'];
        }else{
            $tipoc=2;
        }
        if($tipoc<2){
            $w_statis_s=" and vd.serie_estatus='$tipoc' ";
        }else{
            $w_statis_s='';
        }
        $strq = "SELECT COUNT(*) as total
                from ventas_has_detallesRefacciones as vd
                inner JOIN ventas as v on v.id=vd.idVentas
                inner JOIN personal as pr on pr.personalId=v.id_personal
                inner JOIN bodegas as bo on bo.bodegaId=vd.serie_bodega
                inner JOIN clientes as cl on cl.id=v.idCliente
                where v.activo=1 and vd.ocultarsolicitudserie=0 $w_fecha $w_statis_s
                ";
        $query = $this->DB10->query($strq);
        $total=0;
        foreach ($query->result() as $row) {
            $total=$row->total;
        }
        return $total;
    }
    function Getventas_accesorios($params){
            if(isset($params['fecha'])){
                $fecha=$params['fecha'];
            }else{
                $fecha='';
            }
            if(isset($params['tipoc'])){
                $tipoc=$params['tipoc'];
            }else{
                $tipoc=2;
            }
            $columns0 = array(
                0=>'vd.id_accesoriod',
                1=>'vd.id_venta',
                2=>'pr.nombre as vendedor',
                3=>'vd.cantidad',
                4=>'vd.id_accesorio',
                5=>'cata.nombre',
                6=>'cata.no_parte',
                7=>'vd.serie_bodega',
                8=>'bo.bodega',
                9=>'vd.serie_estatus',
                10=>'v.reg',
                11=>'cl.empresa',
                12=>'v.prefactura',
                13=>'vc.combinadaId',
                14=>'vc.equipo',
                15=>'vc.consumibles',
                16=>'vc.refacciones',
                17=>'v.tipov'
            );
            $columns = array(
                0=>'vd.id_accesoriod',
                1=>'vd.id_venta',
                2=>'pr.nombre',
                3=>'vd.cantidad',
                4=>'vd.id_accesorio',
                5=>'cata.nombre',
                6=>'vd.serie_bodega',
                7=>'bo.bodega',
                8=>'vd.serie_estatus',
                9=>'v.reg',
                10=>'cl.empresa',
                11=>'vc.combinadaId',
                12=>'cata.no_parte',
            );

            $select="";
            foreach ($columns0 as $c) {
                $select.="$c, ";
            }
            $this->db->select($select);
            $this->db->from('ventas_has_detallesEquipos_accesorios vd');
            $this->db->join('ventas v', 'v.id=vd.id_venta');
            $this->db->join('personal pr', 'pr.personalId=v.id_personal');
            $this->db->join('bodegas bo', 'bo.bodegaId=vd.serie_bodega');
            $this->db->join('clientes cl', 'cl.id=v.idCliente');
            $this->db->join('catalogo_accesorios cata', 'cata.id=vd.id_accesorio');
            $this->db->join('ventacombinada vc', 'vc.equipo=v.id','left');
            $this->db->where(array('v.activo'=>1));
            if($fecha!=''){
                $this->db->where(array('v.reg >='=>$fecha));
            }
            if($tipoc<2){
                $this->db->where(array('vd.serie_estatus'=>$tipoc));
            }
            $this->db->where('vd.ocultarsolicitudserie',0);
            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->db->group_start();
                foreach($columns as $c){
                    $this->db->or_like($c,$search);
                }
                $this->db->group_end();
                
            }
           
            $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
            $this->db->limit($params['length'],$params['start']);
            //echo $this->DB10->get_compiled_select();
            $query=$this->db->get();
            // print_r($query); die;
            return $query;
    }
    function Getventas_equipos($params){
        if(isset($params['fecha'])){
            $fecha=$params['fecha'];
        }else{
            $fecha='';
        }
        if(isset($params['tipoc'])){
            $tipoc=$params['tipoc'];
        }else{
            $tipoc=2;
        }
        $columns = array(
                0=>'vd.id',
                1=>'vd.idVenta',
                2=>'pr.nombre',
                3=>'vd.cantidad',
                4=>'vd.idEquipo',
                5=>'eq.modelo',
                6=>'eq.noparte',
                7=>'vd.serie_bodega',
                8=>'bo.bodega',
                9=>'vd.serie_estatus',
                10=>'v.reg',
                11=>'cl.empresa',
                12=>'vd.id',
                13=>'v.prefactura',
                14=>'vc.combinadaId',
                15=>'vc.equipo',
                16=>'vc.consumibles',
                17=>'vc.refacciones',
                18=>'v.tipov'
            );

            $select="";
            foreach ($columns as $c) {
                $select.="$c, ";
            }
            $this->DB10->select($select);
            $this->DB10->from('ventas_has_detallesEquipos vd');
            $this->DB10->join('ventas v', 'v.id=vd.idVenta');
            $this->DB10->join('equipos eq', 'eq.id=vd.idEquipo');
            $this->DB10->join('personal pr', 'pr.personalId=v.id_personal');
            $this->DB10->join('bodegas bo', 'bo.bodegaId=vd.serie_bodega','left');
            $this->DB10->join('clientes cl', 'cl.id=v.idCliente');
            $this->DB10->join('ventacombinada vc', 'vc.equipo=v.id','left');
            $this->DB10->where('v.activo',1);
            $this->DB10->where('vd.ocultarsolicitudserie',0);
            if($fecha!=''){
                $this->DB10->where(array('v.reg >='=>$fecha));
            }
            if($tipoc<2){
                $this->DB10->where(array('vd.serie_estatus'=>$tipoc));
            }
            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->DB10->group_start();
                foreach($columns as $c){
                    $this->DB10->or_like($c,$search);
                }
                $this->DB10->group_end();
                
            }
           
            $this->DB10->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
            $this->DB10->limit($params['length'],$params['start']);
            //echo $this->DB10->get_compiled_select();
            $query=$this->DB10->get();
            // print_r($query); die;
            return $query;
    }
    function Getventas_refacciones($params){
        if(isset($params['fecha'])){
            $fecha=$params['fecha'];
        }else{
            $fecha='';
        }
        if(isset($params['tipoc'])){
            $tipoc=$params['tipoc'];
        }else{
            $tipoc=2;
        }
        $columns = array(
                0=>'vd.id',
                1=>'vd.idVentas',
                2=>'pr.nombre',
                3=>'vd.piezas',
                4=>'vd.idEquipo',
                5=>'re.nombre modelo',
                6=>'re.codigo',
                7=>'vd.serie_bodega',
                8=>'bo.bodega',
                9=>'vd.serie_estatus',
                10=>'v.reg',
                11=>'cl.empresa',
                12=>'vd.idRefaccion',
                13=>'v.prefactura',
                14=>'vc.combinadaId',
                15=>'vc.equipo',
                16=>'vc.consumibles',
                17=>'vc.refacciones',
                18=>'v.tipov'
            );
            $columnsss = array(
                0=>'vd.id',
                1=>'vd.idVentas',
                2=>'pr.nombre',
                3=>'vd.piezas',
                4=>'vd.idEquipo',
                5=>'re.nombre',
                6=>'re.codigo',
                7=>'vd.serie_bodega',
                8=>'bo.bodega',
                9=>'vd.serie_estatus',
                10=>'v.reg',
                11=>'cl.empresa',
                12=>'vd.idRefaccion',
                13=>'v.prefactura',
                14=>'vc.combinadaId',
                15=>'vc.equipo',
                16=>'vc.consumibles',
                17=>'vc.refacciones'
            );

            $select="";
            foreach ($columns as $c) {
                $select.="$c, ";
            }
            $this->db->select($select);
            $this->db->from('ventas_has_detallesRefacciones vd');
            $this->db->join('refacciones re', 're.id=vd.idRefaccion');
            $this->db->join('ventas v', 'v.id=vd.idVentas');
            $this->db->join('personal pr', 'pr.personalId=v.id_personal');
            $this->db->join('bodegas bo', 'bo.bodegaId=vd.serie_bodega');
            $this->db->join('clientes cl', 'cl.id=v.idCliente');
            $this->db->join('ventacombinada vc', 'vc.refacciones=v.id','left');

            $this->db->where(array('v.activo'=>1));
            $this->db->where(array('vd.ocultarsolicitudserie'=>0));
            if($fecha!=''){
                $this->db->where(array('v.reg >='=>$fecha));
            }
            if($tipoc<2){
                $this->db->where(array('vd.serie_estatus'=>$tipoc));
            }
            $this->db->group_by("vd.id");
            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->db->group_start();
                foreach($columnsss as $c){
                    $this->db->or_like($c,$search);
                }
                $this->db->group_end();
                
            }
           
            $this->db->order_by($columnsss[$params['order'][0]['column']], $params['order'][0]['dir']);
            $this->db->limit($params['length'],$params['start']);
            //echo $this->DB10->get_compiled_select();
            $query=$this->db->get();
            // print_r($query); die;
            return $query;
    }
    //======================================
    function Totalventas_accesoriosr($params){
        if(isset($params['fecha'])){
            $fecha=$params['fecha'];
        }else{
            $fecha='';
        }
        if($fecha!=''){
            $w_fecha=" and v.reg >='$fecha' ";
        }else{
            $w_fecha="";
        }
        if(isset($params['tipoc'])){
            $tipoc=$params['tipoc'];
        }else{
            $tipoc=2;
        }
        if($tipoc<2){
            $w_statis_s=" and vd.serie_estatus='$tipoc' ";
        }else{
            $w_statis_s='';
        }
        $strq = "SELECT COUNT(*) as total
                from rentas_has_detallesEquipos_accesorios as vd
                inner JOIN rentas as v on v.id=vd.idrentas
                where v.estatus !=0 and vd.ocultarsolicitudserie=0 $w_fecha $w_statis_s
                ";
        $query = $this->DB10->query($strq);
        $total=0;
        foreach ($query->result() as $row) {
            $total=$row->total;
        }
        return $total;
    }
    function Totalventas_equiposr($params){
        if(isset($params['fecha'])){
            $fecha=$params['fecha'];
        }else{
            $fecha='';
        }
        if($fecha!=''){
            $w_fecha=" and v.reg >='$fecha' ";
        }else{
            $w_fecha="";
        }
        if(isset($params['tipoc'])){
            $tipoc=$params['tipoc'];
        }else{
            $tipoc=2;
        }
        if($tipoc<2){
            $w_statis_s=" and vd.serie_estatus='$tipoc' ";
        }else{
            $w_statis_s='';
        }
        $strq = "SELECT COUNT(*) as total
                from rentas_has_detallesEquipos as vd
                inner JOIN rentas as v on v.id=vd.idRenta
                inner JOIN personal as pr on pr.personalId=v.id_personal
                inner JOIN bodegas as bo on bo.bodegaId=vd.serie_bodega
                inner JOIN clientes as cl on cl.id=v.idCliente  
                where v.estatus !=0 and vd.estatus !=0 and vd.ocultarsolicitudserie=0 $w_fecha $w_statis_s
                ";
        $query = $this->DB10->query($strq);
        $total=0;
        foreach ($query->result() as $row) {
            $total=$row->total;
        }
        return $total;
    }
    function Getventas_accesoriosr($params){
        if(isset($params['fecha'])){
            $fecha=$params['fecha'];
        }else{
            $fecha='';
        }
        if(isset($params['tipoc'])){
            $tipoc=$params['tipoc'];
        }else{
            $tipoc=2;
        }
            $columns0 = array(
                0=>'vd.id_accesoriod',
                1=>'vd.idrentas',
                2=>'c.folio',
                3=>'pr.nombre as vendedor',
                4=>'bo.bodega',
                5=>'vd.cantidad',
                6=>'vd.id_accesorio',
                7=>'cata.no_parte',
                8=>'cl.empresa',
                9=>'v.reg',
                10=>'vd.serie_estatus',
                11=>'cata.nombre',
                12=>'vd.serie_bodega',
                13=>'v.prefactura'
            );
            $columns = array(
                0=>'vd.id_accesoriod',
                1=>'vd.idrentas',
                2=>'c.folio',
                3=>'pr.nombre',
                4=>'bo.bodega',
                5=>'vd.cantidad',
                6=>'vd.id_accesorio',
                7=>'cata.no_parte',
                8=>'cl.empresa',
                9=>'v.reg',
                10=>'vd.serie_estatus',
                11=>'cata.nombre',
                12=>'vd.serie_bodega' 
            );

            $select="";
            foreach ($columns0 as $c) {
                $select.="$c, ";
            }
            $this->DB10->select($select);
            $this->DB10->from('rentas_has_detallesEquipos_accesorios vd');
            $this->DB10->join('rentas v', 'v.id=vd.idrentas');
            $this->DB10->join('personal pr', 'pr.personalId=v.id_personal');
            $this->DB10->join('bodegas bo', 'bo.bodegaId=vd.serie_bodega','left');
            $this->DB10->join('clientes cl', 'cl.id=v.idCliente');
            $this->DB10->join('catalogo_accesorios cata', 'cata.id=vd.id_accesorio');
            $this->DB10->join('contrato c', 'c.idRenta=v.id','left');
            $this->DB10->where('v.estatus !=0');

            $this->DB10->where(array('vd.activo'=>1));
            $this->DB10->where(array('vd.ocultarsolicitudserie'=>0));
            if($fecha!=''){
                $this->DB10->where(array('v.reg >= '=>$fecha));
            }
            if($tipoc<2){
                $this->DB10->where(array('vd.serie_estatus'=>$tipoc));
            }
            $this->DB10->group_by('vd.id_accesoriod');
            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->DB10->group_start();
                foreach($columns as $c){
                    $this->DB10->or_like($c,$search);
                }
                $this->DB10->group_end();
                
            }
           
            $this->DB10->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
            $this->DB10->limit($params['length'],$params['start']);
            //echo $this->DB10->get_compiled_select();
            $query=$this->DB10->get();
            // print_r($query); die;
            return $query;
    }
    function Getventas_equiposr($params){
        if(isset($params['fecha'])){
            $fecha=$params['fecha'];
        }else{
            $fecha='';
        }
        if(isset($params['tipoc'])){
            $tipoc=$params['tipoc'];
        }else{
            $tipoc=2;
        }
        $columns = array(
                0=>'vd.id',
                1=>'vd.idRenta',
                2=>'c.folio',
                3=>'pr.nombre',
                4=>'bo.bodega',
                5=>'vd.cantidad',
                6=>'eq.modelo',
                7=>'eq.noparte',
                8=>'cl.empresa',
                9=>'v.reg',
                10=>'vd.serie_estatus',
                11=>'vd.serie_bodega',
                12=>'v.prefactura',
                13=>'v.idCliente',
                14=>'vd.idEquipo'
            );

            $select="";
            foreach ($columns as $c) {
                $select.="$c, ";
            }
            $this->DB10->select($select);
            $this->DB10->from('rentas_has_detallesEquipos vd');
            $this->DB10->join('equipos eq', 'eq.id=vd.idEquipo');
            $this->DB10->join('rentas v', 'v.id=vd.idRenta');
            $this->DB10->join('personal pr', 'pr.personalId=v.id_personal');
            $this->DB10->join('bodegas bo', 'bo.bodegaId=vd.serie_bodega','left');
            $this->DB10->join('clientes cl', 'cl.id=v.idCliente');
            $this->DB10->join('contrato c', 'c.idRenta=v.id','left');
            $this->DB10->where('v.estatus !=0');
            $this->DB10->where('vd.estatus !=0');
            $this->DB10->where(array('vd.ocultarsolicitudserie'=>0));
            if($fecha!=''){
                $this->DB10->where(array('v.reg >= '=>$fecha));
            }
            if($tipoc<2){
                $this->DB10->where(array('vd.serie_estatus'=>$tipoc));
            }
            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->DB10->group_start();
                foreach($columns as $c){
                    $this->DB10->or_like($c,$search);
                }
                $this->DB10->group_end();
                
            }
           
            $this->DB10->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
            $this->DB10->limit($params['length'],$params['start']);
            //echo $this->DB10->get_compiled_select();
            $query=$this->DB10->get();
            // print_r($query); die;
            return $query;
    }
    function getlistadocalentario($fechaini,$fechafin,$servicio,$tecnico,$cliente,$ciudad,$perfil,$personal,$zona,$soli){
        $strq="SELECT * from (
                    SELECT 
                    `ac`.`asignacionId`,
                    `ac`.`prioridad`,
                    `cli`.`empresa`,
                    `ace`.`fecha`,
                    `ace`.`hora`,
                    `ace`.`horafin`,
                    `ace`.`status`,
                    `ace`.`confirmado`,
                    per.nombre,
                    per.apellido_paterno,
                    per.apellido_materno,
                    1 as tipo,
                    ac.tserviciomotivo,
                    pol.nombre as poliza,
                    ace.nr,
                    concat(per2.nombre,' ',per2.apellido_paterno,' ',per2.apellido_materno) as tecnico2,
                    concat(per3.nombre,' ',per3.apellido_paterno,' ',per3.apellido_materno) as tecnico3,
                    concat(per4.nombre,' ',per4.apellido_paterno,' ',per4.apellido_materno) as tecnico4
                    FROM `asignacion_ser_contrato_a_e` `ace`
                    JOIN `asignacion_ser_contrato_a` `ac` ON `ac`.`asignacionId`=`ace`.`asignacionId`
                    JOIN `personal` `per` ON `per`.`personalId`=`ace`.`tecnico`
                    left JOIN personal as per2 ON per2.personalId=ace.tecnico2
                    left JOIN personal as per3 ON per3.personalId=ace.tecnico3
                    left JOIN personal as per4 ON per4.personalId=ace.tecnico4
                    left JOIN `contrato` `con` ON `con`.`idcontrato`=`ac`.`contratoId`
                    JOIN `rentas` `ren` ON `ren`.`id`=`con`.`idRenta`
                    JOIN `clientes` `cli` ON `cli`.`id`=`ren`.`idCliente`
                    JOIN `rutas` `ru` ON `ru`.`id`=`ac`.`zonaId`
                    JOIN `rentas_has_detallesEquipos` `rde` ON `rde`.`id`=`ace`.`idequipo`
                    JOIN `series_productos` `serp` ON `serp`.`serieId`=`ace`.`serieId`
                    left JOIN `servicio_evento` `sereve` ON `sereve`.`id`=`ac`.`tservicio`
                    left JOIN polizas as pol on pol.id=ac.tpoliza
                    where ace.status<3 and "; 
            if ($cliente>0) {
                $strq.=" ren.idCliente=$cliente and ";
            }
            if ($perfil==5) {
                $strq.=" ace.tecnico=$personal and ";
            }else{
                if ($tecnico>0) {
                    $strq.=" (ace.tecnico='$tecnico' or ace.tecnico2='$tecnico') and ";
                }
            }
            if ($servicio>0) {
                $strq.=" ac.tpoliza=$servicio and ";
            }
            if ($zona>0) {
                $strq.=" ac.zonaId=$zona and";
            }
            if($soli>0){
                $strq.=" con.ejecutivo=$soli and ";
            }
            $strq.=" ace.activo=1 and ac.activo=1 and ace.fecha between '$fechaini' and '$fechafin' GROUP BY `ac`.`asignacionId`,`ace`.`fecha` ";


            $strq.=" UNION ";
            $strq.=" SELECT 
                    `ap`.`asignacionId`,
                    `ap`.`prioridad`,
                    `cli`.`empresa`,
                    `ape`.`fecha`,
                    ape.hora, ape.horafin,
                    `ape`.`status`,
                    `ape`.`confirmado`,
                    per.nombre,per.apellido_paterno,per.apellido_materno,
                    2 as tipo,
                    pocde.comentario as tserviciomotivo,
                    pocde.nombre as poliza,
                    ape.nr,
                    concat(per2.nombre,' ',per2.apellido_paterno,' ',per2.apellido_materno) as tecnico2,
                    concat(per3.nombre,' ',per3.apellido_paterno,' ',per3.apellido_materno) as tecnico3,
                    concat(per4.nombre,' ',per4.apellido_paterno,' ',per4.apellido_materno) as tecnico4
                    FROM `asignacion_ser_poliza_a_e` `ape`
                    JOIN `asignacion_ser_poliza_a` `ap` ON `ap`.`asignacionId`=`ape`.`asignacionId`
                    JOIN `personal` `per` ON `per`.`personalId`=`ape`.`tecnico`
                    left JOIN personal as per2 ON per2.personalId=ape.tecnico2
                    left JOIN personal as per3 ON per3.personalId=ape.tecnico3
                    left JOIN personal as per4 ON per4.personalId=ape.tecnico4
                    JOIN `polizasCreadas` `poc` ON `poc`.`id`=`ap`.`polizaId`
                    JOIN `clientes` `cli` ON `cli`.`id`=`poc`.`idCliente`
                    JOIN `rutas` `ru` ON `ru`.`id`=`ap`.`zonaId`
                    JOIN `polizasCreadas_has_detallesPoliza` `pocde` ON `pocde`.`id`=`ape`.`idequipo`
                    JOIN `equipos` `equ` ON `equ`.`id`=`pocde`.`idEquipo`
                    left JOIN `servicio_evento` `sereve` ON `sereve`.`id`=`ap`.`tservicio`
                    left JOIN polizas as pol on pol.id=ap.tpoliza
                    where ape.status<3 and ";
            if ($cliente>0) {
                $strq.=" poc.idCliente=$cliente and ";
            }
            if ($perfil==5) {
                $strq.=" ape.tecnico=$personal and ";
            }else{
                if ($tecnico>0) {
                    $strq.=" ( ape.tecnico='$tecnico' or ape.tecnico2='$tecnico') and ";
                }
            }
            if ($servicio>0) {
                $strq.=" ap.tpoliza=$servicio and ";
            }
            if ($zona>0) {
                $strq.=" ap.zonaId=$zona and";
            }
            if($soli>0){
                $strq.=" poc.id_personal=$soli and ";
            }
            $strq.=" ape.activo=1 and ap.activo=1 and  ape.fecha between '$fechaini' and '$fechafin' GROUP BY `ap`.`asignacionId`,`ape`.`fecha`";
            //========================================================================
            $strq.=" UNION ";
            $strq.=" SELECT 
                    `acl`.`asignacionId`, 
                    `acl`.`prioridad`,
                    `cli`.`empresa`,
                    `acl`.`fecha`,
                    acl.hora,acl.horafin,
                    `acld`.`status`,
                    `acl`.`confirmado`,
                    per.nombre,per.apellido_paterno,per.apellido_materno,
                    3 as tipo,
                    acl.tserviciomotivo,
                    pol.nombre as poliza,
                    acld.nr,
                    concat(per2.nombre,' ',per2.apellido_paterno,' ',per2.apellido_materno) as tecnico2,
                    concat(per3.nombre,' ',per3.apellido_paterno,' ',per3.apellido_materno) as tecnico3,
                    concat(per4.nombre,' ',per4.apellido_paterno,' ',per4.apellido_materno) as tecnico4
                    FROM `asignacion_ser_cliente_a` `acl`
                    JOIN `asignacion_ser_cliente_a_d` `acld` ON `acld`.`asignacionId` = `acl`.`asignacionId`
                    JOIN `personal` `per` ON `per`.`personalId`=acld.tecnico
                    left JOIN personal as per2 ON per2.personalId=acld.tecnico2
                    left JOIN personal as per3 ON per3.personalId=acld.tecnico3
                    left JOIN personal as per4 ON per4.personalId=acld.tecnico4
                    JOIN `clientes` `cli` ON `cli`.`id`=`acl`.`clienteId`
                    JOIN `rutas` `zo` ON `zo`.`id`=`acl`.`zonaId`
                    
                    left JOIN `servicio_evento` `sereve` ON `sereve`.`id` = `acld`.`tservicio`
                    left JOIN polizas as pol on pol.id=acld.tpoliza
                    where acl.status<3 and ";
            if ($cliente>0) {
                $strq.=" acl.clienteId=$cliente and ";
            }
            if ($perfil==5) {
                $strq.=" acl.tecnico=$personal and ";
            }else{
                if ($tecnico>0) {
                    $strq.=" (acld.tecnico='$tecnico' or acld.tecnico2='$tecnico' ) and ";
                }
            }
            if ($servicio>0) {
                $strq.=" (acl.tpoliza=$servicio or acld.tpoliza=$servicio ) and ";
            }
            if ($zona>0) {
                $strq.=" acl.zonaId=$zona and";
            }
            if($soli>0){
                $strq.=" acl.confirmado_personal=$soli and ";
            }
            $strq.=" acld.activo=1 and acl.activo=1 and `acl`.`fecha` between '$fechaini' and '$fechafin' GROUP BY `acl`.`asignacionId`,`acl`.`fecha` ";
            //==============================================================================
            $strq.=" UNION ";
            $strq.=" SELECT 
                    `acl`.`asignacionId`, 
                    `acl`.`prioridad`,
                    `cli`.`empresa`,
                    `acl`.`fecha`,
                    acl.hora,acl.horafin,
                    `acl`.`status`,
                    `acl`.`confirmado`,
                    per.nombre,per.apellido_paterno,per.apellido_materno,
                    4 as tipo,
                    acl.tserviciomotivo,
                    pol.nombre as poliza,
                    acl.nr,
                    concat(per2.nombre,' ',per2.apellido_paterno,' ',per2.apellido_materno) as tecnico2,
                    concat(per3.nombre,' ',per3.apellido_paterno,' ',per3.apellido_materno) as tecnico3,
                    concat(per4.nombre,' ',per4.apellido_paterno,' ',per4.apellido_materno) as tecnico4
                    FROM `asignacion_ser_venta_a` `acl`
                    JOIN `personal` `per` ON `per`.`personalId`=`acl`.`tecnico`
                    left JOIN personal as per2 ON per2.personalId=acl.tecnico2
                    left JOIN personal as per3 ON per3.personalId=acl.tecnico3
                    left JOIN personal as per4 ON per4.personalId=acl.tecnico4
                    JOIN `clientes` `cli` ON `cli`.`id`=`acl`.`clienteId`
                    JOIN `rutas` `zo` ON `zo`.`id`=`acl`.`zonaId`
                    JOIN `ventas` `ve` ON `ve`.`id`=`acl`.`ventaId`
                    JOIN `asignacion_ser_venta_a_d` `acld` ON `acld`.`asignacionId` = `acl`.`asignacionId`
                    left JOIN `servicio_evento` `sereve` ON `sereve`.`id` = `acld`.`tservicio`
                    left JOIN polizas as pol on pol.id=acld.tpoliza
                    where acl.status<3 and ";
            if ($cliente>0) {
                $strq.=" acl.clienteId=$cliente and ";
            }
            if ($perfil==5) {
                $strq.=" acl.tecnico=$personal and ";
            }else{
                if ($tecnico>0) {
                    $strq.=" ( acl.tecnico='$tecnico' or acl.tecnico2='$tecnico') and ";
                }
            }
            if ($servicio>0) {
                $strq.=" acl.tpoliza=$servicio and ";
            }
            if ($zona>0) {
                $strq.=" acl.zonaId=$zona and";
            }
            if($soli>0){
                $strq.=" ve.id_personal=$soli and ";
            }
            $strq.=" acl.activo=1 and `acl`.`fecha` between '$fechaini' and '$fechafin' GROUP BY `acl`.`asignacionId`,`acl`.`fecha`";
            //==============================================================================
            $strq.=") as datos order by status asc";
            $query = $this->db->query($strq);
            return $query;
    }
    function getlistadocalentariotipo($fechaini,$fechafin,$servicio,$tecnico,$cliente,$ciudad,$perfil,$personal,$tipo,$status,$statu2){
        if($statu2==0){
            $w_sta=' <2 ';
        }else{
            $w_sta=' <=2 ';
        }
        $strq="";
        if ($tipo==1) {
            $strq="
                    SELECT 
                    `ac`.`asignacionId`,
                    `ac`.`prioridad`,`ac`.`prioridad2`,
                    `cli`.`empresa`,
                    `ace`.`fecha`,
                    `ace`.`hora`,`ace`.`horafin`,
                    `ace`.`hora_comida`,`ace`.`horafin_comida`,
                    `ace`.`status`,
                    ace.firma,
                    1 as tipo,
                    ren.idCliente,
                    ac.stock_toner,
                    ac.hora_cri_ser,
                    GROUP_CONCAT(serp.serie SEPARATOR ' ') as series
                    FROM `asignacion_ser_contrato_a_e` `ace`
                    JOIN `asignacion_ser_contrato_a` `ac` ON `ac`.`asignacionId`=`ace`.`asignacionId`
                    /*JOIN `personal` `per` ON `per`.`personalId`=`ace`.`tecnico`*/
                    JOIN `contrato` `con` ON `con`.`idcontrato`=`ac`.`contratoId`
                    JOIN `rentas` `ren` ON `ren`.`id`=`con`.`idRenta`
                    JOIN `clientes` `cli` ON `cli`.`id`=`ren`.`idCliente`
                    JOIN `rentas_has_detallesEquipos` `rde` ON `rde`.`id`=`ace`.`idequipo`
                    JOIN `series_productos` `serp` ON `serp`.`serieId`=`ace`.`serieId`
                    left JOIN `servicio_evento` `sereve` ON `sereve`.`id`=`ac`.`tservicio` 
                    where ace.nr=0 and ace.activo=1 and ace.confirmado=1 and ace.status$w_sta and "; 
                    
                    //where ace.activo=1 and ace.confirmado=1 and (ace.status<2 or ace.firma is null) and
                        if ($cliente>0) {
                            $strq.=" ren.idCliente=$cliente and ";
                        }
                        if ($perfil==5) {
                            $strq.=" (ace.tecnico='$personal' or ace.tecnico2='$personal' or ace.tecnico3='$personal' or ace.tecnico4='$personal')  and ";
                        }else{
                            if ($tecnico>0) {
                                $strq.=" (ace.tecnico='$tecnico' or ace.tecnico2='$tecnico' or ace.tecnico3='$personal' or ace.tecnico4='$personal')  and ";
                            }
                        }
                        if ($servicio>0) {
                            $strq.=" ac.tservicio=$servicio and ";
                        }
                        $strq.="`ace`.`fecha` between '$fechaini' and '$fechafin'";
                        $strq.=" GROUP by `ac`.`asignacionId`,
                                `cli`.`empresa`,
                                `ace`.`fecha`";
        }
        if ($tipo==2) {
            $strq.="SELECT 
                    `ap`.`asignacionId`,
                    `ap`.`prioridad`,`ap`.`prioridad2`,
                    `cli`.`empresa`,
                    `ape`.`fecha`,
                    `ape`.`hora`,`ape`.`horafin`,
                    `ape`.`hora_comida`,`ape`.`horafin_comida`,
                    `ape`.`status`,
                    ape.firma,
                    2 as tipo,
                    poc.idCliente,
                    poc.id as polizaid,
                    ap.hora_cri_ser,
                    poc.combinada,
                    GROUP_CONCAT(ape.serie SEPARATOR ' ') as series
                    FROM `asignacion_ser_poliza_a_e` `ape`
                    JOIN `asignacion_ser_poliza_a` `ap` ON `ap`.`asignacionId`=`ape`.`asignacionId`
                    /*JOIN `personal` `per` ON `per`.`personalId`=`ape`.`tecnico`*/
                    JOIN `polizasCreadas` `poc` ON `poc`.`id`=`ap`.`polizaId`
                    JOIN `clientes` `cli` ON `cli`.`id`=`poc`.`idCliente`
                    JOIN `polizasCreadas_has_detallesPoliza` `pocde` ON `pocde`.`id`=`ape`.`idequipo`
                    JOIN `equipos` `equ` ON `equ`.`id`=`pocde`.`idEquipo`
                    /*JOIN `servicio_evento` `sereve` ON `sereve`.`id`=`ap`.`tservicio`*/
                    where ape.nr=0 and ape.activo=1 and ape.status$w_sta and ";
                    //where ape.activo=1 and (ape.status<2 or ape.firma is null) and
                    if ($cliente>0) {
                        $strq.=" poc.idCliente=$cliente and ";
                    }
                    if ($perfil==5) {
                        $strq.=" (ape.tecnico='$personal' or ape.tecnico2='$personal' or ape.tecnico3='$personal' or ape.tecnico4='$personal') and ";
                    }else{
                        if ($tecnico>0) {
                            $strq.=" (ape.tecnico='$tecnico' or ape.tecnico2='$tecnico' or ape.tecnico3='$personal' or ape.tecnico4='$personal') and ";
                        }
                    }
                    if ($servicio>0) {
                        $strq.=" ap.tservicio=$servicio and ";
                    }
                    $strq.="`ape`.`fecha` between '$fechaini' and '$fechafin'";
                    $strq.=" GROUP by ap.asignacionId,cli.empresa,ape.fecha,ape.hora,ape.horafin,ape.status ";
        }
        if ($tipo==3) {
            $strq.="SELECT 
                    `acl`.`asignacionId`, 
                    `acl`.`prioridad`,`acl`.`prioridad2`,
                    `cli`.`empresa`,
                    `acl`.`fecha`,
                    `acl`.`hora`,`acl`.`horafin`,
                    `acl`.`hora_comida`,`acl`.`horafin_comida`,
                    `acl`.`status`,
                    acl.firma,
                    3 as tipo,
                    acl.clienteId as idCliente,
                    acl.hora_cri_ser,
                    GROUP_CONCAT(acld.serie SEPARATOR ' ') as series
                    FROM `asignacion_ser_cliente_a` `acl`
                    /*JOIN `personal` `per` ON `per`.`personalId`=`acl`.`tecnico`*/
                    JOIN `clientes` `cli` ON `cli`.`id`=`acl`.`clienteId`
                    JOIN `asignacion_ser_cliente_a_d` `acld` ON `acld`.`asignacionId` = `acl`.`asignacionId`
                    left JOIN `servicio_evento` `sereve` ON `sereve`.`id` = `acld`.`tservicio`
                    where acld.nr=0 and acld.activo=1 and acld.status$w_sta  and ";
                    //where acld.activo=1 and (acld.status<2 or acl.firma is null) and
                    if ($cliente>0) {
                        $strq.=" acl.clienteId=$cliente and ";
                    }
                    /*
                    if ($perfil==5) {
                        $strq.=" (acl.tecnico='$personal' or acl.tecnico2='$personal' or acl.tecnico3='$personal' or acl.tecnico4='$personal') and ";
                    }else{
                        if ($tecnico>0) {
                            $strq.=" (acl.tecnico='$tecnico' or acl.tecnico2='$tecnico' or acl.tecnico3='$personal' or acl.tecnico4='$personal') and ";
                        }
                    }
                    */
                    $strq.=" (acld.tecnico='$tecnico' or acld.tecnico2='$tecnico' or acld.tecnico3='$tecnico' or acld.tecnico4='$tecnico') and ";
                    if ($servicio>0) {
                        $strq.=" acl.tservicio=$servicio and ";
                    }
                    $strq.="`acl`.`fecha` between '$fechaini' and '$fechafin'";
                    $strq.=" GROUP by `acl`.`asignacionId`,`cli`.`empresa`,`acl`.`fecha`,`acl`.`hora`,`acl`.`horafin`,`acl`.`status` ";
        }
        if ($tipo==4) {
            $strq.="SELECT 
                    `acl`.`asignacionId`, 
                    `acl`.`prioridad`,`acl`.`prioridad2`,
                    `cli`.`empresa`,
                    `acl`.`fecha`,
                    `acl`.`hora`,`acl`.`horafin`,
                    `acl`.`hora_comida`,`acl`.`horafin_comida`,
                    `acl`.`status`,
                    acl.firma,
                    4 as tipo,
                    acl.clienteId as idCliente,
                    acl.hora_cri_ser,
                    acl.ventaId,
                    acl.tipov as tipoventa,
                    GROUP_CONCAT(acld.serie SEPARATOR ' ') as series
                    FROM `asignacion_ser_venta_a` `acl`
                    /*JOIN `personal` `per` ON `per`.`personalId`=`acl`.`tecnico`*/
                    JOIN `clientes` `cli` ON `cli`.`id`=`acl`.`clienteId`
                    JOIN `asignacion_ser_venta_a_d` `acld` ON `acld`.`asignacionId` = `acl`.`asignacionId`
                    left JOIN `servicio_evento` `sereve` ON `sereve`.`id` = `acld`.`tservicio`
                    where acl.nr=0 and acl.activo=1 and acl.status$w_sta  and ";
                    //where acl.activo=1 and (acl.status<2 or acl.firma) and
                    if ($cliente>0) {
                        $strq.=" acl.clienteId=$cliente and ";
                    }
                    if ($perfil==5) {
                        $strq.=" (acl.tecnico='$personal' or acl.tecnico2='$personal' or acl.tecnico3='$personal' or acl.tecnico4='$personal') and ";
                    }else{
                        if ($tecnico>0) {
                            $strq.=" (acl.tecnico='$tecnico' or acl.tecnico2='$tecnico' or acl.tecnico3='$personal' or acl.tecnico4='$personal') and ";
                        }
                    }
                    if ($servicio>0) {
                        $strq.=" acl.tservicio=$servicio and ";
                    }
                    $strq.="`acl`.`fecha` between '$fechaini' and '$fechafin'";
                    $strq.=" GROUP by `acl`.`asignacionId`, `cli`.`empresa`, `acl`.`fecha`, `acl`.`hora`, `acl`.`horafin`, `acl`.`status`";
        }
        //log_message('error',$strq);
        $query = $this->db->query($strq);            
        //$query = $this->DB8->query($strq);
        return $query;
    }
    function getlistadocalentariotipogroup($fecha,$tecnico,$tipo){
        $strq="";
        if ($tipo==1) {
            $strq="SELECT 
                    ac.asignacionId,ac.prioridad,ac.prioridad2,cli.empresa,ace.fecha,ace.hora,ace.horafin,ace.status,
                    group_concat(ace.status separator ',')  as g_status,
                    ace.hora_comida,ace.horafin_comida,ace.firma,
                    1 as tipo,ren.idCliente,ac.stock_toner,
                    group_concat(ace.nr separator ',')  as g_nr
                    FROM asignacion_ser_contrato_a_e ace
                    JOIN asignacion_ser_contrato_a ac ON ac.asignacionId=ace.asignacionId
                    JOIN personal per ON per.personalId=ace.tecnico
                    JOIN contrato con ON con.idcontrato=ac.contratoId
                    JOIN rentas ren ON ren.id=con.idRenta
                    JOIN clientes cli ON cli.id=ren.idCliente
                    JOIN rutas ru ON ru.id=ac.zonaId
                    JOIN rentas_has_detallesEquipos rde ON rde.id=ace.idequipo
                    JOIN series_productos serp ON serp.serieId=ace.serieId
                    left JOIN servicio_evento sereve ON sereve.id=ac.tservicio 
                    where /*ace.nr=0 and */ ace.activo=1 and ace.confirmado=1 and ace.status<3 and"; 
                    
                    $strq.=" (ace.tecnico='$tecnico' or ace.tecnico2='$tecnico' or ace.tecnico3='$tecnico' or ace.tecnico4='$tecnico')  and ";
                                
                    $strq.="ace.fecha='$fecha'";
                    $strq.=" GROUP by ac.asignacionId,cli.empresa,ace.fecha";
        }
        if ($tipo==2) {
            $strq.="SELECT 
                    ap.asignacionId,ap.prioridad,ap.prioridad2,cli.empresa,ape.fecha,ape.hora,ape.horafin,ape.status,
                    group_concat(ape.status separator ',') as g_status,
                    ape.hora_comida,ape.horafin_comida,ape.firma,
                    2 as tipo,poc.idCliente,
                    group_concat(ape.nr separator ',')  as g_nr
                    FROM `asignacion_ser_poliza_a_e` `ape`
                    JOIN `asignacion_ser_poliza_a` `ap` ON `ap`.`asignacionId`=`ape`.`asignacionId`
                    JOIN `personal` `per` ON `per`.`personalId`=`ape`.`tecnico`
                    JOIN `polizasCreadas` `poc` ON `poc`.`id`=`ap`.`polizaId`
                    JOIN `clientes` `cli` ON `cli`.`id`=`poc`.`idCliente`
                    JOIN `rutas` `ru` ON `ru`.`id`=`ap`.`zonaId`
                    JOIN `polizasCreadas_has_detallesPoliza` `pocde` ON `pocde`.`id`=`ape`.`idequipo`
                    JOIN `equipos` `equ` ON `equ`.`id`=`pocde`.`idEquipo`
                    /*JOIN `servicio_evento` `sereve` ON `sereve`.`id`=`ap`.`tservicio`*/
                    where /*ape.nr=0 and */ape.activo=1 and ape.status<3 and ";
                    //where ape.activo=1 and (ape.status<2 or ape.firma is null) and
                
                    $strq.=" (ape.tecnico='$tecnico' or ape.tecnico2='$tecnico' or ape.tecnico3='$tecnico' or ape.tecnico4='$tecnico') and ";

                    $strq.="ape.fecha='$fecha'";
                    $strq.=" GROUP by ap.asignacionId,cli.empresa,ape.hora,ape.horafin";
        }
        if ($tipo==3) {
            $strq.="SELECT 
                    acl.asignacionId,acl.prioridad,acl.prioridad2,cli.empresa,acl.fecha,acl.hora,acl.horafin,acl.hora_comida,acl.horafin_comida,acl.status,acld.status AS estatus1,
                    group_concat(acld.status separator ',') as g_status1,
                    acl.status AS estatus2,acl.firma,
                    3 as tipo,acl.clienteId as idCliente,
                    group_concat(acld.nr separator ',')  as g_nr,
                    group_concat(IF(acld.status = 1, acld.horaserinicio, '' )separator '') as g_horaserinicio
                    FROM `asignacion_ser_cliente_a` `acl`
                    JOIN `personal` `per` ON `per`.`personalId`=`acl`.`tecnico`
                    JOIN `clientes` `cli` ON `cli`.`id`=`acl`.`clienteId`
                    JOIN `rutas` `zo` ON `zo`.`id`=`acl`.`zonaId`
                    JOIN `asignacion_ser_cliente_a_d` `acld` ON `acld`.`asignacionId` = `acl`.`asignacionId`
                    left JOIN `servicio_evento` `sereve` ON `sereve`.`id` = `acld`.`tservicio`
                    where /*acld.nr=0 and*/ acld.activo=1 and acld.status<3  and ";
                    //where acld.activo=1 and (acld.status<2 or acl.firma is null) and
                    
                    //$strq.=" (acl.tecnico='$tecnico' or acl.tecnico2='$tecnico' or acl.tecnico3='$tecnico' or acl.tecnico4='$tecnico') and ";
                    $strq.=" (acld.tecnico='$tecnico' or acld.tecnico2='$tecnico' or acld.tecnico3='$tecnico' or acld.tecnico4='$tecnico') and ";

                    $strq.=" acl.fecha='$fecha' ";
                    $strq.=" GROUP by `acl`.`asignacionId`,`cli`.`empresa`,`acl`.`hora`,`acl`.`horafin`,`acl`.`status`";
                    //log_message('error',$strq);
        }
        if ($tipo==4) {
            $strq.="SELECT 
                    acl.asignacionId,acl.prioridad,acl.prioridad2,cli.empresa,acl.fecha,acl.hora,acl.horafin,acl.status,
                    group_concat(acl.status separator ',') as g_status,
                    acl.hora_comida,acl.horafin_comida,acl.firma,
                    4 as tipo,acl.clienteId as idCliente,
                    group_concat(acl.nr separator ',')  as g_nr
                    FROM `asignacion_ser_venta_a` `acl`
                    JOIN `personal` `per` ON `per`.`personalId`=`acl`.`tecnico`
                    JOIN `clientes` `cli` ON `cli`.`id`=`acl`.`clienteId`
                    JOIN `rutas` `zo` ON `zo`.`id`=`acl`.`zonaId`
                    JOIN `asignacion_ser_venta_a_d` `acld` ON `acld`.`asignacionId` = `acl`.`asignacionId`
                    left JOIN `servicio_evento` `sereve` ON `sereve`.`id` = `acld`.`tservicio`
                    where /* acl.nr=0  and */ acl.activo=1 and acl.status<3  and ";
                    //where acl.activo=1 and (acl.status<2 or acl.firma) and
                    
                    
                            $strq.=" (acl.tecnico='$tecnico' or acl.tecnico2='$tecnico' or acl.tecnico3='$tecnico' or acl.tecnico4='$tecnico') and ";
                        
                    
                    $strq.="`acl`.`fecha` between '$fecha' and '$fecha'";
                    $strq.=" GROUP by `acl`.`asignacionId`,`cli`.`empresa`,`acl`.`fecha`,`acl`.`hora`,`acl`.`horafin`,`acl`.`status`";
        }
        //log_message('error',$strq);
        $query = $this->db->query($strq);            
        //$query = $this->DB8->query($strq);
        return $query;
    }
    function getdatosrenta($idasignacion,$fecha,$serve,$tecnico){
        if ($serve>0) {
            $whereserve=" and ace.asignacionIde=$serve ";
        }else{
            $whereserve='';
        }
        if ($tecnico>0) {
            $wheretecnico=" (ace.tecnico='$tecnico' or ace.tecnico2='$tecnico') and ";
        }else{
            $wheretecnico="";
        }
        if($fecha==''){
            $w_fecha='';
        }else{
            $w_fecha=" and ace.fecha='$fecha' ";
        }
        $strq="SELECT `ace`.`asignacionIde`, `per`.`personalId`, 
                CONCAT(per.nombre, ' ', `per`.`apellido_paterno`, ' ', per.apellido_materno) as tecnico, `cli`.`id`, `cli`.`empresa`, 
            ace.fecha, 
            ace.hora,ace.horafin, 
            ace.hora_comida,ace.horafin_comida, 
            ace.horaserinicioext,
            ace.horaserfinext,
            `ac`.`zonaId`, 
            `ru`.`nombre` as zona, 
            `rde`.`modelo`, `serp`.`serie`, 
            ace.status, 
            ac.tiposervicio, 
            `ace`.`horafin`, `ac`.`asignacionId`, `sereve`.`nombre` as `servicio`,
            ac.prioridad,
            rde.id as id_equipo,
            serp.serieId,
            con.contacto,
            con.telefono,
            ac.comentariocal,
            con.servicio_equipo,
            con.servicio_doc,
            ac.tservicio_a,
            ace.confirmado,
            pol.nombre as poliza,
            ren.idCliente,
            ac.contacto_text,
            cli.equipo_acceso,
            cli.documentacion_acceso,
            cli.protocolo_acceso,
            ac.equipo_acceso_info,
            ac.documentacion_acceso_info,
            ac.tserviciomotivo,
            ac.tpoliza,
            con.idRenta,
            rde.idEquipo,
            asre.comentario,
            asre.ubicacion,
            con.idRenta,
            asre.contadores,
            ace.nr,
            ace.comentario as comentarioasge,
            CONCAT(per2.nombre, ' ', per2.apellido_paterno, ' ', per2.apellido_materno) as tecnico2,
            per.personalId as personalId1,
            per2.personalId as personalId2,
            IFNULL(ac.pass,'') as pass,
            ac.prioridad2,
            ac.prioridad2 as prioridadser,
            ace.per_fin,
            CONCAT(perf.nombre, ' ', perf.apellido_paterno, ' ', perf.apellido_materno) as tecnicofin,
            pexs.id as idpexs,
            cli.horario_disponible,
            ace.horaserinicio,
            ace.horaserfin,
            ac.historial_hrs,
            IFNULL(ace.cot_refaccion, '') as cot_refaccion,
            IFNULL(ace.cot_refaccion2, '') as cot_refaccion2,
            IFNULL(ace.cot_refaccion3, '') as cot_refaccion3,
            ace.prioridad,
            ace.prioridad2,
            ace.prioridad3,
            ace.qrecibio
            FROM asignacion_ser_contrato_a_e ace
            JOIN asignacion_ser_contrato_a ac ON ac.asignacionId=ace.asignacionId
            LEFT JOIN personal per ON per.personalId=ace.tecnico
            LEFT JOIN personal per2 ON per2.personalId=ace.tecnico2
            LEFT JOIN personal perf ON perf.personalId=ace.per_fin
            JOIN contrato con ON con.idcontrato=ac.contratoId
            JOIN rentas ren ON ren.id=con.idRenta
            JOIN clientes cli ON cli.id=ren.idCliente
            JOIN rutas ru ON ru.id=ac.zonaId
            JOIN rentas_has_detallesEquipos rde ON rde.id=ace.idequipo
            JOIN series_productos serp ON serp.serieId=ace.serieId
            left JOIN polizas as pol on pol.id=ac.tpoliza
            left JOIN servicio_evento sereve ON sereve.id=ac.tservicio
            left JOIN polizas_detalles as pold on pold.id=ac.tservicio_a
            JOIN asignacion_series_r_equipos asre ON rde.id=asre.id_equipo and asre.serieId=serp.serieId
            left join polizascreadas_ext_servisios as pexs on pexs.id_ser_garantia=ac.asignacionId and pexs.id_tipo_garantia=1
            where $wheretecnico  ace.activo=1  and ac.asignacionId=$idasignacion $whereserve $w_fecha and ace.status!=3 ORDER by ace.horaserinicio,asre.orden ASC
            ";
            //log_message('error',$strq);
        $query = $this->DB9->query($strq);
        return $query;
    }
    function getdatospoliza($idasignacion,$serve,$tecnico){
        if ($serve>0) {
            $whereserve="  ape.asignacionIde=$serve and";
        }else{
            $whereserve='';
        }
        if ($tecnico>0) {
            $wheretecnico=" (ape.tecnico='$tecnico' or ape.tecnico2='$tecnico') and ";
        }else{
            $wheretecnico="";
        }
        $strq="SELECT 
            ape.asignacionIde, 
            per.personalId, 
            CONCAT(per.nombre, ' ', per.apellido_paterno, ' ', per.apellido_materno) as tecnico, 
            cli.id, 
            cli.empresa, 
            ape.fecha, 
            ape.hora,ape.horafin,
            ape.hora_comida,ape.horafin_comida, 
            ape.horaserinicioext,
            ape.horaserfinext,
            ap.zonaId, 
            ru.nombre as zona, 
            equ.modelo, ape.serie, 
            ape.status, 
            ap.tiposervicio, 
            ape.horafin, 
            ap.asignacionId, 
            pocde.nombre as servicio,
            ap.prioridad,
            ape.idequipo as id_equipo,
            ape.serie,
            pocde.direccionservicio,
            pocde.iddir,
            poc.atencion,poc.telefono,
            ap.comentariocal,
            pocde.comentario,
            pocde.comeninfo,
            pre.observaciones,
            ape.confirmado,
            ap.polizaId as ventaId,
            poc.idCliente,
            ap.contacto_text,
            cli.equipo_acceso,
            cli.documentacion_acceso,
            cli.protocolo_acceso,
            ap.equipo_acceso_info,
            ap.documentacion_acceso_info,
            pre.formadecobro,
            ape.nr,
            ap.polizaId,
            poc.combinada,
            poc.correo,
            CONCAT(per2.nombre, ' ', per2.apellido_paterno, ' ', per2.apellido_materno) as tecnico2,
            per.personalId as personalId1,
            per2.personalId as personalId2,
            IFNULL(ap.pass,'') as pass,
            ap.prioridad2,
            ap.prioridad2 as prioridadser,
            ape.per_fin,
            CONCAT(perf.nombre, ' ', perf.apellido_paterno, ' ', perf.apellido_materno) as tecnicofin,
            pexs.id as idpexs,
            cli.horario_disponible,
            ape.horaserinicio,
            ape.horaserfin,
            ap.historial_hrs,
            ape.comentario as comentarioasge,
            IFNULL(ape.cot_refaccion, '') as cot_refaccion,
            IFNULL(ape.cot_refaccion2, '') as cot_refaccion2,
            IFNULL(ape.cot_refaccion3, '') as cot_refaccion3,
            ape.prioridad,
            ape.prioridad2,
            ape.prioridad3,
            ape.qrecibio,
            ap.tipo_pre_corr
            FROM `asignacion_ser_poliza_a_e` `ape`
            JOIN `asignacion_ser_poliza_a` `ap` ON ap.asignacionId=ape.asignacionId
            LEFT JOIN personal per ON per.personalId=ape.tecnico
            LEFT JOIN personal per2 ON per2.personalId=ape.tecnico2
            LEFT JOIN personal perf ON perf.personalId=ape.per_fin
            JOIN `polizasCreadas` `poc` ON poc.id=ap.polizaId
            JOIN `clientes` `cli` ON cli.id=poc.idCliente
            JOIN `rutas` `ru` ON ru.id=ap.zonaId
            JOIN `polizasCreadas_has_detallesPoliza` `pocde` ON `pocde`.`id`=`ape`.`idequipo`
            JOIN `equipos` `equ` ON `equ`.`id`=`pocde`.`idEquipo`
            left join prefactura as pre on pre.ventaId=poc.id and (pre.tipo=1 or pre.tipo=3)
            /*JOIN `servicio_evento` `sereve` ON `sereve`.`id`=`ap`.`tservicio`*/
            left join polizascreadas_ext_servisios as pexs on pexs.id_ser_garantia=ap.asignacionId and pexs.id_tipo_garantia=2
            where $wheretecnico $whereserve ape.activo=1 and ap.asignacionId=$idasignacion group by ape.serie,ape.asignacionIde";
        $query = $this->db->query($strq);
        return $query;
    }
    function getdatoscliente($idasignacion,$tecnico=0){
        //log_message('error', 'entra al modelo');
        if ($tecnico>0) {
            $wheretecnico=" (acld.tecnico='$tecnico' or acld.tecnico2='$tecnico' or acld.tecnico3='$tecnico' or acld.tecnico3='$tecnico') and ";
        }else{
            $wheretecnico="";
        }
        $strq = "SELECT 
                acld.asignacionIdd,
                acl.asignacionId, per.personalId, 
                CONCAT(per.nombre, ' ', per.apellido_paterno, ' ', per.apellido_materno) as tecnico, cli.id, cli.empresa, 
                acl.fecha,acl.hora,acl.horafin,
                acl.hora_comida,acl.horafin_comida, 
                acl.horaserinicioext,
                acl.horaserfinext,
                `acl`.`horafin`, `acl`.`zonaId`, 
                acl.prioridad, 
            `zo`.`nombre` as zona, 
            `acld`.`status`, 
            acl.tiposervicio, 
            `sereve`.`nombre` as `servicio`,
            acl.equipostext,
            acl.tserviciomotivo,
            acl.documentaccesorio,
            acld.serie,
            acld.direccion,
            acld.iddir,
            acl.comentariocal,
            pol.nombre as poliza,
            acld.tservicio_a,
            acl.confirmado,
            equ.modelo,
            acl.clienteId,
            acl.contacto_text,
            cli.equipo_acceso,
            cli.documentacion_acceso,
            cli.protocolo_acceso,
            acl.equipo_acceso_info,
            acl.documentacion_acceso_info,
            acld.nr,
            acld.comentario,
            CONCAT(per2.nombre, ' ', `per2`.`apellido_paterno`, ' ', per2.apellido_materno) as tecnico2,
            acld.modelo as modeloretiro,
            acld.retiro,
            per.personalId as personalId1,
            per2.personalId as personalId2,
            IFNULL(acl.pass,'') as pass,
            acl.prioridad2,
            acl.prioridad2 as prioridadser,
            pexs.id as idpexs,
            cli.horario_disponible,
            acld.horaserinicio,
            acld.horaserfin,
            acl.historial_hrs,
            acl.comentario as comentarioasge,
            IFNULL(acld.cot_refaccion, '') as cot_refaccion,
            IFNULL(acld.cot_refaccion2, '') as cot_refaccion2,
            IFNULL(acld.cot_refaccion3, '') as cot_refaccion3,
            acld.prioridad,
            acld.prioridad2,
            acld.prioridad3,
            acl.qrecibio,
            acld.qrecibio as qrecibio2
            FROM `asignacion_ser_cliente_a` `acl`
            JOIN `personal` `per` ON `per`.`personalId`=`acl`.`tecnico`
            left JOIN `personal` `per2` ON `per2`.`personalId`=`acl`.`tecnico2`
            JOIN `clientes` `cli` ON `cli`.`id`=`acl`.`clienteId`
            JOIN `rutas` `zo` ON `zo`.`id`=`acl`.`zonaId`
            JOIN `asignacion_ser_cliente_a_d` `acld` ON `acld`.`asignacionId` = `acl`.`asignacionId`
            inner JOIN polizas as pol on pol.id=acld.tpoliza
            left JOIN `servicio_evento` `sereve` ON `sereve`.`id` = `acld`.`tservicio`
            left JOIN polizas_detalles as pold on pold.id=acld.tservicio_a
            left join equipos as equ on equ.id=pold.modelo
            left join polizascreadas_ext_servisios as pexs on pexs.id_ser_garantia=acl.asignacionId and pexs.id_tipo_garantia=3
            where acld.activo=1 and $wheretecnico acl.asignacionId=$idasignacion";
        $query = $this->db->query($strq);
        return $query;
    }
    function getdatosventas($idasignacion){
        //log_message('error', 'entra al modelo');
        $strq = "SELECT `acl`.`asignacionId`, `per`.`personalId`, 
                CONCAT(per.nombre, ' ', `per`.`apellido_paterno`, ' ', per.apellido_materno) as tecnico, `cli`.`id`, `cli`.`empresa`, 
                acl.fecha, 
                acl.hora,acl.horafin,
                acl.hora_comida,acl.horafin_comida, 
                acl.horaserinicioext,
                acl.horaserfinext,
                `acl`.`horafin`, `acl`.`zonaId`, 
                acl.prioridad, 
            `zo`.`nombre` as zona, 
            `acl`.`status`, 
            acl.tiposervicio, 
            `sereve`.`nombre` as `servicio`,
            acl.equipostext,
            acl.tserviciomotivo,
            acl.documentaccesorio,
            acld.serie,
            acld.direccion,
            acl.comentariocal,
            pre.observaciones,
            pre.ventaId,
            pre.tipo,
            acl.ventaId as ventaIdd,
            pol.nombre as poliza,
            acld.tservicio_a,
            clidc.atencionpara,
            clidc.email,
            clidc.telefono,
            clidc.celular,
            clidc.puesto,
            clidca.persona_contacto,
            acl.confirmado,
            acl.reg,
            acl.tserviciomotivo,
            acl.firma,
            acl.qrecibio,
            acl.comentario,
            acl.tipov,
            acl.contacto_text,
            acl.clienteId,
            cli.equipo_acceso,
            cli.documentacion_acceso,
            acl.equipo_acceso_info,
            acl.documentacion_acceso_info,
            pre.formadecobro,
            acl.nr,
            acl.prioridadr,
            acl.cot_refaccion,
            acl.cot_refaccion2,
            acl.cot_refaccion3,
            acl.prioridad2,
            acl.per_fin,
            CONCAT(perf.nombre, ' ', perf.apellido_paterno, ' ', perf.apellido_materno) as tecnicofin,
            acl.comentario_tec,
            acl.id_contacto_new,
            pexs.id as idpexs,
            cli.horario_disponible,
            acl.horaserinicio,
            acl.horaserfin,
            acl.historial_hrs,
            IFNULL(acl.cot_refaccion, '') as cot_refaccion,
            IFNULL(acl.cot_refaccion2, '') as cot_refaccion2,
            IFNULL(acl.cot_refaccion3, '') as cot_refaccion3,
            acl.prioridadr,
            acl.prioridadr2,
            acl.prioridadr3,
            acl.qrecibio
            FROM `asignacion_ser_venta_a` `acl`
            JOIN `personal` `per` ON `per`.`personalId`=`acl`.`tecnico`
            LEFT JOIN personal perf ON perf.personalId=acl.per_fin
            JOIN `clientes` `cli` ON `cli`.`id`=`acl`.`clienteId`
            JOIN `rutas` `zo` ON `zo`.`id`=`acl`.`zonaId`
            JOIN `asignacion_ser_venta_a_d` `acld` ON `acld`.`asignacionId` = `acl`.`asignacionId`
            left JOIN `servicio_evento` `sereve` ON `sereve`.`id` = `acld`.`tservicio`
            left join prefactura as pre on pre.ventaId=acl.ventaId and pre.tipo=0
            left JOIN polizas as pol on pol.id=acld.tpoliza
            left join cliente_datoscontacto as clidc on clidc.datosId=pre.contactoId
            left join cliente_has_persona_contacto as clidca on clidca.id=pre.contactoId
            left join polizascreadas_ext_servisios as pexs on pexs.id_ser_garantia=acl.asignacionId and pexs.id_tipo_garantia=4
            where acl.activo=1 and `acl`.`asignacionId`=$idasignacion GROUP by acld.asignacionIdd";
        $query = $this->db->query($strq);
        return $query;
    }
    function getdatosrentaft($tecnico,$fecha){
        $strq="SELECT `ace`.`asignacionIde`, `per`.`personalId`, 
                CONCAT(per.nombre, ' ', `per`.`apellido_paterno`, ' ', per.apellido_materno) as tecnico, `cli`.`id`, `cli`.`empresa`, 
            ace.fecha, 
            ace.hora,ace.horafin, 
            `ac`.`zonaId`, 
            `ru`.`nombre` as zona, 
            `rde`.`modelo`, `serp`.`serie`, `ace`.`status`, 
            ac.tiposervicio, 
            `ace`.`horafin`, `ac`.`asignacionId`, `sereve`.`nombre` as `servicio`,
            ac.prioridad
            FROM asignacion_ser_contrato_a_e ace
            JOIN asignacion_ser_contrato_a ac ON ac.asignacionId=ace.asignacionId
            LEFT JOIN personal per ON per.personalId=ace.tecnico
            JOIN contrato con ON con.idcontrato=ac.contratoId
            JOIN rentas ren ON ren.id=con.idRenta
            JOIN clientes cli ON cli.id=ren.idCliente
            JOIN rutas ru ON ru.id=ac.zonaId
            JOIN rentas_has_detallesEquipos rde ON rde.id=ace.idequipo
            JOIN series_productos serp ON serp.serieId=ace.serieId
            left JOIN servicio_evento sereve ON sereve.id=ac.tservicio
            JOIN asignacion_series_r_equipos asre ON rde.id=asre.id_equipo and asre.serieId=serp.serieId
            where asre.estatus_suspendido=0 and ace.activo=1 and ace.confirmado=1 and (ace.tecnico='$tecnico' or ace.tecnico2='$tecnico') and ace.fecha='$fecha' and ace.status<2
            ";
        $query = $this->DB9->query($strq);
        return $query;
    }
    function getdatospolizaft($tecnico,$fecha){
        $strq="SELECT 
            `ape`.`asignacionIde`, 
            `per`.`personalId`, 
                CONCAT(per.nombre, ' ', `per`.`apellido_paterno`, ' ', per.apellido_materno) as tecnico, `cli`.`id`, `cli`.`empresa`, 
            ape.fecha, 
            ape.hora,ape.horafin, 
            `ap`.`zonaId`, 
            `ru`.`nombre` as zona, 
            `equ`.`modelo`, `ape`.`serie`, `ape`.`status`, 
            ap.tiposervicio, `ape`.`horafin`, `ap`.`asignacionId`, 
            `pocde`.`nombre` as `servicio`,
            ap.prioridad
            FROM `asignacion_ser_poliza_a_e` `ape`
            JOIN `asignacion_ser_poliza_a` `ap` ON `ap`.`asignacionId`=`ape`.`asignacionId`
            LEFT JOIN `personal` `per` ON `per`.`personalId`=ape.tecnico
            JOIN `polizasCreadas` `poc` ON `poc`.`id`=`ap`.`polizaId`
            JOIN `clientes` `cli` ON `cli`.`id`=`poc`.`idCliente`
            JOIN `rutas` `ru` ON `ru`.`id`=`ap`.`zonaId`
            JOIN `polizasCreadas_has_detallesPoliza` `pocde` ON `pocde`.`id`=`ape`.`idequipo`
            JOIN `equipos` `equ` ON `equ`.`id`=`pocde`.`idEquipo`
            /*JOIN `servicio_evento` `sereve` ON `sereve`.`id`=`ap`.`tservicio`*/
            where ape.activo=1 and ape.confirmado=1 and (ape.tecnico='$tecnico' or ape.tecnico2='$tecnico') and ape.fecha='$fecha' and ape.status<2";
        $query = $this->DB9->query($strq);
        return $query;
    }
    function getdatosclienteft($tecnico,$fecha){
        //log_message('error', 'entra al modelo');
        $strq = "SELECT `acl`.`asignacionId`, `per`.`personalId`, 
                CONCAT(per.nombre, ' ', `per`.`apellido_paterno`, ' ', per.apellido_materno) as tecnico, `cli`.`id`, `cli`.`empresa`, 
                acl.fecha, 
                acl.hora,
                acl.horafin, `acl`.`horafin`, `acl`.`zonaId`, 
                acl.prioridad, 
            `zo`.`nombre` as zona, 
            `acld`.`status`, 
            acl.tiposervicio, 
            `sereve`.`nombre` as `servicio`,
            acl.equipostext
            
            FROM `asignacion_ser_cliente_a` `acl`
            JOIN `personal` `per` ON `per`.`personalId`=`acl`.`tecnico`
            JOIN `clientes` `cli` ON `cli`.`id`=`acl`.`clienteId`
            JOIN `rutas` `zo` ON `zo`.`id`=`acl`.`zonaId`
            join asignacion_ser_cliente_a_d as acld on acld.asignacionId=acl.asignacionId
            left JOIN `servicio_evento` `sereve` ON `sereve`.`id`=`acld`.`tservicio`
            where  acld.activo=1 and acl.confirmado=1 and (acl.tecnico='$tecnico' or acl.tecnico2='$tecnico') and acl.fecha='$fecha' and acld.status<2";
        $query = $this->DB9->query($strq);
        return $query;
    }
    function getdatosventasft($tecnico,$fecha){
        //log_message('error', 'entra al modelo');
        $strq = "SELECT `acl`.`asignacionId`, `per`.`personalId`, 
                CONCAT(per.nombre, ' ', `per`.`apellido_paterno`, ' ', per.apellido_materno) as tecnico, `cli`.`id`, `cli`.`empresa`, 
                acl.fecha, 
                acl.hora,
                acl.horafin, `acl`.`horafin`, `acl`.`zonaId`, 
                acl.prioridad, 
            `zo`.`nombre` as zona, 
            `acl`.`status`, 
            acl.tiposervicio, 
            `sereve`.`nombre` as `servicio`,
            acl.equipostext
            
            FROM `asignacion_ser_venta_a` `acl`
            JOIN `personal` `per` ON `per`.`personalId`=`acl`.`tecnico`
            JOIN `clientes` `cli` ON `cli`.`id`=`acl`.`clienteId`
            JOIN `rutas` `zo` ON `zo`.`id`=`acl`.`zonaId`
            join asignacion_ser_venta_a_d as acld on acld.asignacionId=acl.asignacionId
            left JOIN `servicio_evento` `sereve` ON `sereve`.`id`=`acld`.`tservicio`
            where acl.activo=1 and  acl.confirmado=1 and (acl.tecnico='$tecnico' or acl.tecnico2='$tecnico') and acl.fecha='$fecha' and acl.status<2";
        $query = $this->DB10->query($strq);
        return $query;
    }
    function getserviciopolizaequipos($servicio){
        //log_message('error', 'entra al modelo');
        $strq = "SELECT polequi.* 
                FROM asignacion_ser_poliza_a_e as serpoliza 
                inner join polizasCreadas_has_detallesPoliza as polequi on polequi.id=serpoliza.idequipo
                where serpoliza.asignacionId=$servicio";
        $query = $this->DB9->query($strq);
        return $query;
    }
    //=================================================================== D impresion
    function Getventas_equipos_vd($params){
        $columns = array(
                0=>'vd.id',
                1=>'vd.idVenta',
                2=>'pr.nombre',
                3=>'vd.cantidad',
                4=>'vd.idEquipo',
                5=>'eq.modelo',
                6=>'eq.noparte',
                7=>'vd.serie_bodega',
                8=>'bo.bodega',
                9=>'vd.serie_estatus',
                10=>'v.reg',
                11=>'cl.empresa',
                12=>'vd.id',
                13=>'v.prefactura'
            );

            $select="";
            foreach ($columns as $c) {
                $select.="$c, ";
            }
            $this->DB10->select($select);
            $this->DB10->from('ventasd_has_detallesequipos vd');
            $this->DB10->join('ventasd v', 'v.id=vd.idVenta');
            $this->DB10->join('equipos eq', 'eq.id=vd.idEquipo');
            $this->DB10->join('personal pr', 'pr.personalId=v.id_personal');
            $this->DB10->join('bodegas bo', 'bo.bodegaId=vd.serie_bodega','left');
            $this->DB10->join('clientes cl', 'cl.id=v.idCliente');
            $this->DB10->where('v.activo',1);
            $this->DB10->where('vd.ocultarsolicitudserie',0);
            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->DB10->group_start();
                foreach($columns as $c){
                    $this->DB10->or_like($c,$search);
                }
                $this->DB10->group_end();
                
            }
           
            $this->DB10->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
            $this->DB10->limit($params['length'],$params['start']);
            //echo $this->DB10->get_compiled_select();
            $query=$this->DB10->get();
            // print_r($query); die;
            return $query;
    }
    function Getventas_equipos_vd_total(){
        $strq = "SELECT COUNT(*) as total
                from ventasd_has_detallesequipos as vd
                inner JOIN ventas as v on v.id=vd.idVenta
                inner JOIN personal as pr on pr.personalId=v.id_personal
                inner JOIN bodegas as bo on bo.bodegaId=vd.serie_bodega
                inner JOIN clientes as cl on cl.id=v.idCliente
                where v.activo=1 and vd.ocultarsolicitudserie=0
                ";
        $query = $this->DB10->query($strq);
        $total=0;
        foreach ($query->result() as $row) {
            $total=$row->total;
        }
        return $total;
    }
    function Getventas_accesorios_vd_total(){
        $strq = "SELECT COUNT(*) as total
                from ventasd_has_detallesequipos_accesorios as vd
                inner JOIN ventasd as v on v.id=vd.id_venta
                inner JOIN personal as pr on pr.personalId=v.id_personal
                inner JOIN bodegas as bo on bo.bodegaId=vd.serie_bodega
                inner JOIN clientes as cl on cl.id=v.idCliente
                inner JOIN catalogo_accesorios as cata on cata.id=vd.id_accesorio
                where v.activo=1 and vd.ocultarsolicitudserie=0";
        $query = $this->DB10->query($strq);
        $total=0;
        foreach ($query->result() as $row) {
            $total=$row->total;
        }
        return $total;
    }
    function Getventas_accesorios_vd($params){
            $columns0 = array(
                0=>'vd.id_accesoriod',
                1=>'vd.id_venta',
                2=>'pr.nombre as vendedor',
                3=>'vd.cantidad',
                4=>'vd.id_accesorio',
                5=>'cata.nombre',
                6=>'cata.no_parte',
                7=>'vd.serie_bodega',
                8=>'bo.bodega',
                9=>'vd.serie_estatus',
                10=>'v.reg',
                11=>'cl.empresa',
                12=>'v.prefactura',
            );
            $columns = array(
                0=>'vd.id_accesoriod',
                1=>'vd.id_venta',
                2=>'pr.nombre',
                3=>'vd.cantidad',
                4=>'vd.id_accesorio',
                5=>'cata.nombre',
                6=>'vd.serie_bodega',
                7=>'bo.bodega',
                8=>'vd.serie_estatus',
                9=>'v.reg',
                10=>'cl.empresa',
                11=>'vc.combinadaId',
                12=>'cata.no_parte',
            );

            $select="";
            foreach ($columns0 as $c) {
                $select.="$c, ";
            }
            $this->DB10->select($select);
            $this->DB10->from('ventasd_has_detallesequipos_accesorios vd');
            $this->DB10->join('ventasd v', 'v.id=vd.id_venta');
            $this->DB10->join('personal pr', 'pr.personalId=v.id_personal');
            $this->DB10->join('bodegas bo', 'bo.bodegaId=vd.serie_bodega');
            $this->DB10->join('clientes cl', 'cl.id=v.idCliente');
            $this->DB10->join('catalogo_accesorios cata', 'cata.id=vd.id_accesorio');
            $this->DB10->where(array('v.activo'=>1));
            $this->DB10->where('vd.ocultarsolicitudserie',0);
            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->DB10->group_start();
                foreach($columns as $c){
                    $this->DB10->or_like($c,$search);
                }
                $this->DB10->group_end();
                
            }
           
            $this->DB10->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
            $this->DB10->limit($params['length'],$params['start']);
            //echo $this->DB10->get_compiled_select();
            $query=$this->DB10->get();
            // print_r($query); die;
            return $query;
    }
    function Totalventas_refacciones_vd(){
        $strq = "SELECT COUNT(*) as total
                from ventasd_has_detallesrefacciones as vd
                inner JOIN ventasd as v on v.id=vd.idVentas
                inner JOIN personal as pr on pr.personalId=v.id_personal
                inner JOIN bodegas as bo on bo.bodegaId=vd.serie_bodega
                inner JOIN clientes as cl on cl.id=v.idCliente
                where v.activo=1 and vd.ocultarsolicitudserie=0
                ";
        $query = $this->DB10->query($strq);
        $total=0;
        foreach ($query->result() as $row) {
            $total=$row->total;
        }
        return $total;
    }
    function Getventas_refacciones_vd($params){
        $columns = array(
                0=>'vd.id',
                1=>'vd.idVentas',
                2=>'pr.nombre',
                3=>'vd.piezas',
                4=>'vd.idEquipo',
                5=>'re.nombre modelo',
                6=>'re.codigo',
                7=>'vd.serie_bodega',
                8=>'bo.bodega',
                9=>'vd.serie_estatus',
                10=>'v.reg',
                11=>'cl.empresa',
                12=>'vd.idRefaccion',
                13=>'v.prefactura'
            );
            $columnsss = array(
                0=>'vd.id',
                1=>'vd.idVentas',
                2=>'pr.nombre',
                3=>'vd.piezas',
                4=>'vd.idEquipo',
                5=>'re.nombre',
                6=>'re.codigo',
                7=>'vd.serie_bodega',
                8=>'bo.bodega',
                9=>'vd.serie_estatus',
                10=>'v.reg',
                11=>'cl.empresa',
                12=>'vd.idRefaccion',
                13=>'v.prefactura'
            );

            $select="";
            foreach ($columns as $c) {
                $select.="$c, ";
            }
            $this->DB10->select($select);
            $this->DB10->from('ventasd_has_detallesrefacciones vd');
            $this->DB10->join('refacciones re', 're.id=vd.idRefaccion');
            $this->DB10->join('ventasd v', 'v.id=vd.idVentas');
            $this->DB10->join('personal pr', 'pr.personalId=v.id_personal');
            $this->DB10->join('bodegas bo', 'bo.bodegaId=vd.serie_bodega');
            $this->DB10->join('clientes cl', 'cl.id=v.idCliente');

            $this->DB10->where(array('v.activo'=>1));
            $this->DB10->where(array('vd.ocultarsolicitudserie'=>0));
            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->DB10->group_start();
                foreach($columnsss as $c){
                    $this->DB10->or_like($c,$search);
                }
                $this->DB10->group_end();
                
            }
           
            $this->DB10->order_by($columnsss[$params['order'][0]['column']], $params['order'][0]['dir']);
            $this->DB10->limit($params['length'],$params['start']);
            //echo $this->DB10->get_compiled_select();
            $query=$this->DB10->get();
            // print_r($query); die;
            return $query;
    }
    
    function otenerdireccion($asig,$tipo){
        $direccion=array('idrow'=>'0','direccion'=>'','longitud'=>'','latitud'=>'');
        if($tipo==1){
            $strq="SELECT * FROM `asignacion_ser_contrato_a_e` WHERE asignacionId='$asig'";
            $rest_asig = $this->db->query($strq);
            foreach ($rest_asig->result() as $item) {

                $resultdir=$this->ModeloCatalogos->db2_getselectwheren('contrato_equipo_direccion',array('equiposrow'=>$item->idequipo,'serieId'=>$item->serieId));
                $iddireccionget='g_0';
                //$direccion='';
                foreach ($resultdir->result() as $itemdir) {
                    if($itemdir->direccion!=''){
                        $iddireccionget=$itemdir->direccion;
                    }
                }
                //log_message('error','iddireccionget '.$iddireccionget);
                if($iddireccionget!='g_0'){
                    $tipodireccion=explode('_',$iddireccionget);

                    $tipodir = $tipodireccion[0];
                    $iddir   = $tipodireccion[1];
                    if ($tipodir=='g') {
                        $resultdirc_where['idclientedirecc']=$iddir;
                        
                        
                        $resultdirc=$this->ModeloCatalogos->db2_getselectwheren('clientes_direccion',$resultdirc_where);
                        foreach ($resultdirc->result() as $item) {
                            $direccion=array('idrow'=>'g_'.$iddir,'direccion'=>$item->direccion,'longitud'=>$item->longitud,'latitud'=>$item->latitud);
                        }
                    }
                    if ($tipodir=='f') {
                        $resultdirc_where['id']=$iddir;
                        
                        
                        $resultdirc=$this->ModeloCatalogos->db2_getselectwheren('cliente_has_datos_fiscales',$resultdirc_where);
                        foreach ($resultdirc->result() as $item) {

                            $direccion=array(
                                            'idrow'=>'f_'.$iddir,
                                            'direccion'=>'Razon social: '.$item->razon_social.' Colonia:'.$item->colonia.' Calle:'.$item->calle.' Numero:'.$item->num_ext,
                                            'longitud'=>$item->longitud,
                                            'latitud'=>$item->latitud
                                        );
                        }
                    }
                }
            }
        }
        if($tipo==2){
            $strq="SELECT * FROM asignacion_ser_poliza_a WHERE asignacionId='$asig'";
            $rest_asig = $this->db->query($strq);
            foreach ($rest_asig->result() as $item) {
                $polizaId=$item->polizaId;
                $strqpe="SELECT * FROM asignacion_ser_poliza_a_e WHERE asignacionId='$asig'";
                $rest_asig_pe = $this->db->query($strqpe);
                foreach ($rest_asig_pe->result() as $itempe) {
                    $idequipo = $itempe->idequipo;
                    //=========================================
                    $strq_p="SELECT * FROM polizasCreadas_has_detallesPoliza WHERE idPoliza='$polizaId' and id='$idequipo' ";
                    $rest_p = $this->db->query($strq_p);
                    foreach ($rest_p->result() as $itemp) {
                        $direccionservicio=$itemp->direccionservicio;
                        $idrow='0';
                        $longitud='';
                        $latitud='';
                        $strq_d="SELECT * FROM clientes_direccion WHERE direccion='$direccionservicio'";// agregar tambien la condicion de cliente ya que cuando es en las oficnas colocan la misma direccion
                        $rest_d = $this->db->query($strq_d);
                        foreach ($rest_d->result() as $itemd) {
                            $idrow='g_'.$itemd->idclientedirecc;
                            $longitud=$itemd->longitud;
                            $latitud=$itemd->latitud;
                        }



                        $direccion=array('idrow'=>$idrow,'direccion'=>$direccionservicio,'longitud'=>$longitud,'latitud'=>$latitud);
                    }
                    //=========================================
                }
                
            }
        }
        if($tipo==3){
            $strq="SELECT * FROM asignacion_ser_cliente_a_d WHERE asignacionId='$asig'";
            $rest_asig = $this->db->query($strq);
            foreach ($rest_asig->result() as $item) {
                $direccionl=$item->direccion;
                $idrow='0';
                $longitud='';
                $latitud='';
                $strq_d="SELECT * FROM clientes_direccion WHERE direccion='$direccionl'";// agregar tambien la condicion de cliente ya que cuando es en las oficnas colocan la misma direccion
                $rest_d = $this->db->query($strq_d);
                foreach ($rest_d->result() as $itemd) {
                    $idrow='g_'.$itemd->idclientedirecc;
                    $longitud=$itemd->longitud;
                    $latitud=$itemd->latitud;
                }


                $direccion=array('idrow'=>$idrow,'direccion'=>$direccionl,'longitud'=>$longitud,'latitud'=>$latitud);
            }
        }
        if($tipo==4){
            $strq="SELECT * FROM asignacion_ser_venta_a_d WHERE asignacionId='$asig'";
            $rest_asig = $this->db->query($strq);
            foreach ($rest_asig->result() as $item) {
                $direccionl=$item->direccion;
                $idrow='0';
                $longitud='';
                $latitud='';
                $strq_d="SELECT * FROM clientes_direccion WHERE direccion='$direccionl'";// agregar tambien la condicion de cliente ya que cuando es en las oficnas colocan la misma direccion
                $rest_d = $this->db->query($strq_d);
                foreach ($rest_d->result() as $itemd) {
                    $idrow='g_'.$itemd->idclientedirecc;
                    $longitud=$itemd->longitud;
                    $latitud=$itemd->latitud;
                }


                $direccion=array('idrow'=>$idrow,'direccion'=>$direccionl,'longitud'=>$longitud,'latitud'=>$latitud);
            }
        }
        //echo json_encode($direccion);
        return $direccion;
    }
    function get_info_itinerario_asignacion($asig,$tipo,$personal){
        $strq = "SELECT * FROM `unidades_bitacora_itinerario` WHERE (personal='$personal' and  asignacion='$asig' AND tipo_asignacion='$tipo' and tipoagregado=0) or (personal='$personal' and  asignacion_gen like '%|$tipo,$asig|%' and tipoagregado=0)";
        $query = $this->db->query($strq);
        return $query;
    }
    function get_info_status($numeros){
        $numeros = explode(",", $numeros);
        $conteo = array_count_values($numeros);
        $status_num=0;
        // Verificar si todos los números son iguales a 2
        if (isset($conteo[2]) && count($numeros) == $conteo[2]) {
            //echo "Todos los números son iguales a 2.";
            $status_num=2;
        } 
        // Verificar si todos los números son ceros
        elseif (isset($conteo[0]) && count($numeros) == $conteo[0]) {
            //echo "Todos los números son ceros.";
            $status_num=0;
        } 
        // Verificar si al menos uno de los números es 2
        elseif (isset($conteo[2])) {
            //echo "Al menos uno de los números es 2.";
            $status_num=1;
        } 
        else {
            //echo "No todos los números son iguales a 2, no todos son ceros y no hay ninguno que sea 2.";
            $status_num=0;
        }
        return $status_num;
    }
    function get_info_status_nr($numeros){
        $numeros = explode(",", $numeros);
        $conteo = array_count_values($numeros);
        $status_num=0;
        // Verificar si todos los números son iguales a 2
        if (isset($conteo[1]) && count($numeros) == $conteo[1]) {
            //echo "Todos los números son iguales a 2.";
            $status_num=1;
        } 
        
        else {
            //echo "No todos los números son iguales a 2, no todos son ceros y no hay ninguno que sea 2.";
            $status_num=0;
        }
        return $status_num;
    }
    function get_detalles_itinerario($idpersonal,$fecha){
        $strq = "SELECT * FROM unidades_bitacora_itinerario WHERE activo=1 AND personal=$idpersonal and fecha='$fecha' "; 
        $query = $this->db->query($strq);
        return $query;
    }
    function get_detalles_itinerario_validar($idpersonal,$fecha,$asignacion_gen){
        $strq = "SELECT * FROM unidades_bitacora_itinerario WHERE activo=1 AND personal=$idpersonal and fecha='$fecha' and asignacion_gen like '%$asignacion_gen%' "; 
        $query = $this->db->query($strq);
        return $query;
    }
    function get_detalles_itinerario_info($fecha,$asignacion_gen){
        $strq = "SELECT * FROM unidades_bitacora_itinerario WHERE activo=1 and fecha='$fecha' and asignacion_gen like '%$asignacion_gen%' "; 
        $query = $this->db->query($strq);
        return $query;
    }
    function agregaraitinerario($asignacion,$tipo){
        // esta funcion estaba en calendario agregaraitinerario($asignacion,$tipo)
        $paramsai['unidadid']=0;
        $paramsai['descripcion']='';
        $paramsai['hora_inicio']='';
        $paramsai['hora_fin']='18:00:00';
        $paramsai['km_inicio']='';
        $paramsai['km_fin']='';
        $paramsai['fecha']='';
        $paramsai['personal']='';
        $paramsai['asignacion']=$asignacion;
        $paramsai['tipo_asignacion']=$tipo;
        $fechan=date('Y-m-d');
        $fecha=date('Y-m-d');
        $personalId1 = 0;
        $personalId2 = 0;
        $empresa = '';
        $hora = '';
        $horafin='';
        $prioridad2=0;$unidadid=0;$unidadid2=0;
        $cli=0;
        if ($tipo==1) {
            $resultado=$this->getdatosrenta($asignacion,$fechan,0,0);
            foreach ($resultado->result() as $item) {
                $fecha = $item->fecha;
                $personalId1 = $item->personalId1;
                $personalId2 = $item->personalId2;
                $empresa = $item->empresa;
                $hora = $item->hora;
                $horafin = $item->horafin;

                $hora_comida=$item->hora_comida;
                $horafin_comida=$item->horafin_comida;
                $prioridad2=$item->prioridadser;
                $cli=$item->idCliente;
            }
        }
        if ($tipo==2) {
            $resultado=$this->getdatospoliza($asignacion,0,0);
            foreach ($resultado->result() as $item) {
                $fecha = $item->fecha;
                $personalId1 = $item->personalId1;
                $personalId2 = $item->personalId2;
                $empresa = $item->empresa;
                $hora = $item->hora;
                $horafin = $item->horafin;

                $hora_comida=$item->hora_comida;
                $horafin_comida=$item->horafin_comida;
                $prioridad2=$item->prioridadser;
                $cli=$item->idCliente;
            }

        }
        if ($tipo==3) {
            $resultado=$this->getdatoscliente($asignacion);
            foreach ($resultado->result() as $item) {
                $fecha = $item->fecha;
                $personalId1 = $item->personalId1;
                $personalId2 = $item->personalId2;
                $empresa = $item->empresa;
                $hora = $item->hora;
                $horafin = $item->horafin;

                $hora_comida=$item->hora_comida;
                $horafin_comida=$item->horafin_comida;
                $prioridad2=$item->prioridadser;
                $cli=$item->clienteId;
            }
        }
        if ($tipo==4) {
            $resultado=$this->getdatosventas($asignacion);
            foreach ($resultado->result() as $item) {
                $fecha = $item->fecha;
                $personalId1 = $item->personalId;
                $personalId2 = 0;
                $empresa = $item->empresa;
                $hora = $item->hora;
                $horafin = $item->horafin;

                $hora_comida=$item->hora_comida;
                $horafin_comida=$item->horafin_comida;
                $prioridad2=$item->prioridad2;
                $cli=$item->clienteId;
            }
        }
        if($horafin=='00:00:00'){
            $horafin='18:00:00';
        }
        $horafin = date('H:i:s', strtotime('-60 minutes', strtotime($horafin)));

        if($fecha==$fechan){
            $res_uni=$this->getselectwheren('unidades',array('tecnico_asignado'=>$personalId1,'activo'=>1));
            foreach ($res_uni->result() as $item_u) {
                $unidadid=$item_u->id;
            }
            $res_uni2=$this->getselectwheren('unidades',array('tecnico_asignado'=>$personalId2,'activo'=>1));
            foreach ($res_uni2->result() as $item_u2) {
                $unidadid2=$item_u2->id;
            }

            if($personalId1>0){
                $resulti1=$this->get_detalles_itinerario($personalId1,$fecha);
                if($resulti1->num_rows()>0){
                    $paramsai['descripcion']=$empresa;
                    //$paramsai['hora_inicio']=$hora;
                    $paramsai['hora_inicio']=$this->horaactual;
                    
                    $paramsai['hora_fin']=$horafin;
                    $paramsai['hora_limite']=$horafin;

                    $paramsai['fecha']=$fecha;
                    $paramsai['personal']=$personalId1;
                    $paramsai['asignacion']=$asignacion;
                    $paramsai['tipo_asignacion']=$tipo;
                    $asignacion_gen="|$tipo,$asignacion|";
                    $paramsai['asignacion_gen']=$asignacion_gen;
                    if($prioridad2<8){
                        $paramsai['tipoagregado']=1;
                    }
                    $paramsai['hora_comida']=$hora_comida;
                    $paramsai['horafin_comida']=$horafin_comida;
                    $paramsai['cli']=$cli;
                    if($unidadid>0){
                        $paramsai['unidadid']=$unidadid;
                    }
                    $paramsai['prioridad']=$prioridad2;
                    $resulti2=$this->get_detalles_itinerario_validar($personalId1,$fecha,$asignacion_gen);
                    if($resulti2->num_rows()==0){
                        $this->Insert('unidades_bitacora_itinerario',$paramsai);
                        $this->updateCatalogo('personal',array('not_new_ser'=>1),array('personalId'=>$personalId1));  
                    }
                    
                }
                
            }
            if($personalId2>0){
                $resulti2=$this->get_detalles_itinerario($personalId2,$fecha);
                if($resulti2->num_rows()>0){
                    $paramsai['descripcion']=$empresa;
                    //$paramsai['hora_inicio']=$hora;
                    $paramsai['hora_inicio']=$this->horaactual;
                    
                    $paramsai['hora_fin']=$horafin;
                    $paramsai['hora_limite']=$horafin;

                    $paramsai['fecha']=$fecha;
                    $paramsai['personal']=$personalId2;
                    $paramsai['asignacion']=$asignacion;
                    $paramsai['tipo_asignacion']=$tipo;
                    $asignacion_gen="|$tipo,$asignacion|";
                    $paramsai['asignacion_gen']=$asignacion_gen;
                    if($prioridad2<8){
                        $paramsai['tipoagregado']=1;
                    }
                    $paramsai['hora_comida']=$hora_comida;
                    $paramsai['horafin_comida']=$horafin_comida;
                    $paramsai['cli']=$cli;
                    if($unidadid2>0){
                        $paramsai['unidadid']=$unidadid2;
                    }
                    $paramsai['prioridad']=$prioridad2;
                    $resulti2=$this->get_detalles_itinerario_validar($personalId2,$fecha,$asignacion_gen);
                    if($resulti2->num_rows()==0){
                        $this->Insert('unidades_bitacora_itinerario',$paramsai);
                        $this->updateCatalogo('personal',array('not_new_ser'=>1),array('personalId'=>$personalId2));
                    }
                }
            }
        }
    }
    function ubicacion_if_ser_info($tipo,$ser){
        $strq = "SELECT * FROM `personal_ubicaciones` WHERE tiposer='$tipo' AND idser='$ser'"; 
        $query = $this->db->query($strq);
        return $query;
    }
    function obtenertecnicodelservicio($idser,$tipo){
        $tecnico='';
        
        if($tipo==1){
            $strq="SELECT asigd.asignacionId,asigd.fecha,per.personalId,concat(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno) as tecnico
                    FROM asignacion_ser_contrato_a_e as asigd 
                    INNER JOIN personal as per on per.personalId=asigd.per_fin
                    WHERE asigd.asignacionId='$idser'
                    GROUP BY asigd.per_fin";
        }
        if($tipo==2){
            $strq="SELECT asigd.asignacionId,asigd.fecha,per.personalId,concat(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno) as tecnico
                    FROM asignacion_ser_poliza_a_e as asigd 
                    INNER JOIN personal as per on per.personalId=asigd.per_fin
                    WHERE asigd.asignacionId='$idser'
                    GROUP BY asigd.per_fin";
        }
        if($tipo==3){
            $strq="SELECT asig.asignacionId, asig.fecha, 
                    per.personalId,concat(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno) as tecnico,
                    per2.personalId as personalId2,concat(per2.nombre,' ',per2.apellido_paterno,' ',per2.apellido_materno) as tecnico2
                    FROM asignacion_ser_cliente_a as asig
                    INNER JOIN asignacion_ser_poliza_a_e as asigd on asigd.asignacionId=asig.asignacionId
                    left JOIN personal as per on per.personalId=asig.per_fin
                    LEFT JOIN personal as per2 on per2.personalId=asigd.per_fin
                    WHERE asig.asignacionId='$idser'
                    GROUP BY per.personalId,per2.personalId";
        }
        if($tipo==4){
            $strq="SELECT asig.asignacionId,asig.fecha,per.personalId,concat(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno) as tecnico
                    FROM asignacion_ser_venta_a as asig
                    INNER JOIN personal as per on per.personalId=asig.per_fin
                    WHERE asig.asignacionId='$idser'";
        }
            $query = $this->db->query($strq);
            foreach ($query->result() as $item) {
                if($tipo==3){
                    if($item->personalId2>0){
                        $tecnico.=$item->tecnico2.'<br>';
                    }else{
                        $tecnico.=$item->tecnico.'<br>';
                    }
                }else{
                    $tecnico.=$item->tecnico.'<br>';
                }
            }
        return $tecnico;
    }
    
}
?>