<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloTicket extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    function gettickets($params){
        $personal=$params['personal'];
        $columns = array( 
            0=>'ti.id',
            1=>'per.personalId',
            2=>'ti.tcontenido',
            3=>'ti.status',
            4=>'ti.tipo',
            5=>'ti.reg',
            6=>'per.nombre',
            7=>'per.apellido_paterno',
            8=>'per.apellido_materno',
            9=>'ti.estatus_resp'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('ticket ti');
        $this->db->join('personal per', 'per.personalId = ti.personalId');
        if($personal>0){
            $this->db->where(array('ti.personalId'=>$personal));
        }
        
        $this->db->where(array('ti.activo'=>1));
        //$this->db->where(array('facturaabierta'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    public function total_tickets($params){
        $personal=$params['personal'];
        $columns = array( 
            0=>'ti.id',
            1=>'per.personalId',
            2=>'ti.contenido',
            3=>'ti.status',
            4=>'ti.tipo',
            5=>'ti.reg',
            6=>'per.nombre',
            7=>'per.apellido_paterno',
            8=>'per.apellido_materno',
            9=>'ti.estatus_resp'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(*) as total');
        $this->db->from('ticket ti');
        $this->db->join('personal per', 'per.personalId = ti.personalId');
        if($personal>0){
            $this->db->where(array('ti.personalId'=>$personal));
        }
        
        $this->db->where(array('ti.activo'=>1));
        //$where = ;
        //$this->db->where(array('facturaabierta'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        //$this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        //$this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query->row()->total;
    }



}