<?php
$a=session_id();
if(empty($a)) session_start();
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloSession extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    function login($usu,$pass) {
        $strq = "CALL SP_GET_SESSION('$usu');";
        $count = 0;
        $passwo =0;
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $passwo =$row->contrasena;
            $id = $row->UsuarioID;
            $nom =$row->nombre;
            $perfil = $row->perfilId; 
            $idpersonal = $row->personalId; 
            $verificar = password_verify($pass,$passwo);
            if ($verificar) {
                $_SESSION['usuarioid']=$id;
                $_SESSION['usuario']=$nom;
                $_SESSION['perfilid']=$perfil;
                $_SESSION['idpersonal']=$idpersonal;
                
                
                $count=1;
                //$count=$passwo.'/'.$id.'/'.$nom.'/'.$perfil.'/'.$idpersonal;
            }
            /*
                 si es cero aqui se agregara los mismos parametros pero para el aseso de los residentes si es que lo piden
            */
        } 
        /*
        if ($count==0) {
            $residentes = $this->db->query("CALL SP_GET_SESSIONRESIDENTES('$usu','$pass')");
            $this->db->close();

            foreach ($residentes->result() as $row) {

                $id = $row->AspiranteId;
                $nom =$row->Nombre;
            
            
                $_SESSION['usuarioid']=$id;
                $_SESSION['usuario']=$nom;
                $_SESSION['perfilid']=3;
                $_SESSION['idpersonal']=0;
                $count=1;
            
           
            }
        }
        */
        
        echo $count;
        //echo $strq;
    }
    public function menus($perfil){
        $strq ="SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.PerfilId='$perfil' ORDER BY men.MenuId ASC";
        //$strq="CALL SP_GET_MENUS($perfil)";
        $query = $this->db->query($strq);
        return $query;

    }
    public function submenus($perfil,$menu){
        $strq ="SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon from menu_sub as menus, perfiles_detalles as perfd WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='$perfil' and menus.MenuId='$menu' ORDER BY menus.MenusubId ASC";
        $query = $this->db->query($strq);
        return $query;

    }

}
