var base_url=$('#base_url').val();
var fichero_generado='';
var m_asi;
var m_cli;
$(document).ready(function($) {
	$('#idcliente').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        
        placeholder: 'Buscar una cliente',
        allowClear: true,
        ajax: {
            url: base_url+'Ventasi/searchclientes',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.id,
                        text: element.empresa

                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function (e) {
        var data = e.params.data;
        contratosclientep(data.id)
        //generar();
    });
});
function contratosclientep(id){
	$.ajax({
        type:'POST',
        url: base_url+"NewFolio/contratos2",
        data: {
        	id:id
        },
        success: function (data){
        	$('#pcontrato').html(data); 
            $('#pcontrato').select2().on('select2:select', function (e) {
          			var data = e.params.data;
                //console.log(data);
                //console.log(data.element.dataset.estatus);
                var estatus = data.element.dataset.estatus;
                var vencido = data.element.dataset.vencido;
                var fechafinal = data.element.dataset.fechafinal;
                
		    });
        }
    });
}
function generar(){
    $('.infocontratos').html('');
	var cli = $('#idcliente option:selected').val();
	var con = $('#pcontrato option:selected').val();
	$.ajax({
        type:'POST',
        url: base_url+"Excedentes/ontenercontratos",
        data: {
        	cli:cli,
			con:con
        },
        success: function (data){
        	$('.infocontratos').append(data);
        }
    });
}
function removercon(id){
	$('.cont_'+id).remove();
}
function generarestado(){
    var facturaslis = $(".tableexcendetes tbody > tr");
    var DATAa  = [];
    var num_facturas_selected=0;
    facturaslis.each(function(){  
        if ($(this).find("input[class*='pred_checked']").is(':checked')) {
            num_facturas_selected++;
            item = {};                    
            item ["preid"]  = $(this).find("input[class*='pred_checked']").data('preid');
            item['tipo']    = $(this).find("input[class*='pred_checked']").data('tipo');
            item['periodo'] = $(this).find("input[class*='pred_checked']").data('periodo');
            item['produ']   = $(this).find("input[class*='pred_checked']").data('produ');
            item['vcontra'] = $(this).find("input[class*='pred_checked']").data('vcontra');
            item['exc']     = $(this).find("input[class*='pred_checked']").data('exc');
            item['pexc']    = $(this).find("input[class*='pred_checked']").data('pexc');
            item['porfac']  = $(this).find("input[class*='pred_checked']").data('porfac');
            item['con']  = $(this).find("input[class*='pred_checked']").data('con');
            item['conf']  = $(this).find("input[class*='pred_checked']").data('conf');
            
            DATAa.push(item);
        }       
        
    });
    INFOa  = new FormData();
    aInfoa   = JSON.stringify(DATAa);
    //console.log(aInfoa);
    if (num_facturas_selected>0) {
        $.ajax({
            type:'POST',
            url: base_url+"index.php/Excedentes/generarreporte",
            data: {
                periodos:aInfoa,
                cliente:$('#idcliente option:selected').val()
            },
            success:function(response){ 
                var array = $.parseJSON(response);
                fichero_generado = array.fichero;
                var url_fichero=base_url+fichero_generado;
                window.open(url_fichero, '_blank');
            }
        });
    }else{
        toastr.error('Seleccion uno o mas periodos');
    }
}
function enviarestado(){
    if(fichero_generado!=''){
        var idcliente=$('#idcliente option:selected').val();
        envio_s_m(fichero_generado,idcliente)
    }else{
        $.alert({
                boxWidth: '30%',
                useBootstrap: false,
                title: 'Atención!',
                content: 'Antes de enviar genere un reporte'});
    } 
}
//==========================envio
function envio_s_m(asi,cliente){
    //$('#firma_m_s').val(54);
    m_asi=asi;
    //m_tipo=tipo;
    m_cli=cliente;
    obtenercontacto(cliente);
    //obtenerbodymail(cliente);
    $('.input_envio_c').html('');
    $('.table_mcr_files_c').html('');
    //$('.input_envio_s').html('<input type="file" id="files" name="files[]" multiple accept="*">');
    $('.input_envio_s').html('<a href="'+base_url+''+asi+'" >Archivo XLSX: '+asi+'</a>');
    $('.table_mcr_files_s').html('<table class="table bordered striped" id="table_mcr_files"><tbody class="tabbodycr_files"></tbody></table>');
    $('.addbtnservicio').html('<button type="button" class="waves-effect btn-bmz  red btn-envioser" onclick="envio_s()">Enviar</button>');
    //inputfileadd();
    $('#ms_asucto2').val('Reporte ');
    $('#modal_servicio').modal();
    $('#modal_servicio').modal('open');
}
function obtenercontacto(cliente){
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Calendario/obtenercontactos",
        data: {
            cliente:cliente
        },
        success: function (response){
            $('.m_s_contac').html('<select class="browser-default form-control-bmz" id="ms_contacto">'+response+'</select>');
                
        },
        error: function(response){
            notificacion(1);   
             
        }
    });

    //=====================================================================
    
}
function envio_s(){
    var email =$('#ms_contacto option:selected').data('email');
    $.ajax({
        type:'POST',
        url: base_url+'index.php/Mailenvio/envio_reporte_excedentes',
        data: {
            asi:fichero_generado,
            email:email,
            idcli:m_cli,
            per:$('#firma_m_s option:selected').val(),
            perbbc:$('#bbc_m_s option:selected').val(),
            asunto:$('#ms_asucto2').val(),
            comen:$('#ms_coment2').val()
        },
        success:function(data){
            console.log(data);
            var array = $.parseJSON(data);
            if(array.envio==1){
                toastr["success"]("Correo enviado");
                $('#modal_servicio').modal('close');
                $('#ms_coment2').val('');
            }else{
                toastr["error"]("No se pudo enviar el correo");
            }
        },
        error: function(response){
            notificacion(1);   
             
        }
    });
}
//=====================================
function calcu(){
    var totalex=0;
    var totalfac=0;
     var facturaslis = $(".tableexcendetes tbody > tr");
    facturaslis.each(function(){  
        if ($(this).find("input[class*='pred_checked']").is(':checked')) {
            
            var exc     = $(this).find("input[class*='pred_checked']").data('exc');
            var porfac  = $(this).find("input[class*='pred_checked']").data('porfac');
            totalex += Number(exc);
            totalfac += Number(porfac);
        }       
    });
    $('.total_excedentes').html(new Intl.NumberFormat('es-MX',{style: "currency", currency: "MXN"}).format(totalex));
    $('.total_facturar').html(new Intl.NumberFormat('es-MX',{style: "currency", currency: "MXN"}).format(totalfac));
}
function reporte_excedentes(periodo,cli){
    window.open(base_url+'index.php/Estadocuenta/reporte_excedente/'+periodo+'/'+cli, '_blank');
}
function generarcotiza(){
    var facturaslis = $(".tableexcendetes tbody > tr");
    var DATAa  = [];
    var num_facturas_selected=0;
    facturaslis.each(function(){  
        if ($(this).find("input[class*='pred_checked']").is(':checked')) {
            num_facturas_selected++;
            item = {};           



            item ["preid"]  = $(this).find("input[class*='pred_checked']").data('preid');
            item['tipo']    = $(this).find("input[class*='pred_checked']").data('tipo');
            item['periodo'] = $(this).find("input[class*='pred_checked']").data('periodo');
            
            //item['produ']   = $(this).find("input[class*='pred_checked']").data('produ');
            //item['vcontra'] = $(this).find("input[class*='pred_checked']").data('vcontra');
            item['exc']     = $(this).find("input[class*='pred_checked']").data('exc');
            item['pexc']    = $(this).find("input[class*='pred_checked']").data('pexc');
            //item['porfac']  = $(this).find("input[class*='pred_checked']").data('porfac');
            item['con']  = $(this).find("input[class*='pred_checked']").data('con');
            item['conf']  = $(this).find("input[class*='pred_checked']").data('conf');
            
            DATAa.push(item);
        }       
        
    });
    INFOa  = new FormData();
    aInfoa   = JSON.stringify(DATAa);
    //console.log(aInfoa);
    if (num_facturas_selected>0) {
        var cli=$('#idcliente option:selected').val();
        var url_fichero=base_url+'index.php/Excedentes/reporte_excedente2?cli='+cli+'&info='+aInfoa;
        window.open(url_fichero, '_blank');


        
    }else{
        toastr.error('Seleccion uno o mas periodos');
    }
}