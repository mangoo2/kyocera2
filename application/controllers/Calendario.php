<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Calendario extends CI_Controller {
    public function __construct()     {
        parent::__construct();
        $this->load->model('ModeloCatalogos');
        $this->load->model('Modeloaccesorios');
        $this->load->model('ModeloAsignacion');
        $this->load->model('ModeloServicio');
        $this->load->model('ModeloGeneral');
        $this->load->model('Rentas_model');
        $this->load->model('Configuraciones_model');
        $this->load->helper('url');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->anioactual = date('Y');
        $this->mesactual = date('m');
        $this->submenu=54;
        //===================================
            $fechamasunmes = strtotime ( '+ 1 month' , strtotime ( $this->fechahoy) ) ;
            $messiguiente = date ( 'm' , $fechamasunmes );
            $this->messiguiente = $messiguiente;
        //===================================

        if($this->session->userdata('logeado')==true){
            $this->perfilid =$this->session->userdata('perfilid');
            $this->idpersonal = $this->session->userdata('idpersonal');
            
        }else{
            redirect('login'); 
        }
    }

    function index(){
        $data['MenusubId']=$this->submenu;
            //$data['resultzonas']=$this->ModeloCatalogos->db13_getselectwheren('rutas',array('status'=>1));
            $data['resultzonas']=$this->ModeloCatalogos->view_session_rutas();
            //$data['resultstec']=$this->Configuraciones_model->searchTecnico();
            $data['resultstec']=$this->Configuraciones_model->view_session_searchTecnico();  
            $data['idpersonal']=$this->idpersonal;
            //$data['personalrows']=$this->ModeloCatalogos->getselectwheren('personal',array('estatus'=>1));
            $data['personalrows']=$this->ModeloGeneral->listadopersonal();
            $data['perfirma']=$this->ModeloCatalogos->seleccionejecutivofirma();
            $this->load->view('header');
            $this->load->view('main');
            $this->load->view('Servicio/calendario',$data);
            $this->load->view('footer');
            $this->load->view('Servicio/calendariojs');
    }   
    public function lisservicios(){
       $data = $this->input->post();
       //$inicio = date('Y-m-d',$data['start']);
       //$fin = date('Y-m-d',$data['end']);
       $inicio = $data['start'];
       $fin = $data['end'];
       //log_message('error', $inicio.'      '.$fin);
       if (isset($data['clienteid'])) {
           if ($data['clienteid']>0) {
            $cliente=$data['clienteid'];
           }else{
            $cliente=0;
           }
       }else{
            $cliente=0;
       }
       if (isset($data['servicioid'])) {
            if ($data['servicioid']>0) {
               $servicio=$data['servicioid'];
            }else{
                $servicio=0;
            }
       }else{
            $servicio=0;
       }
       if (isset($data['zona'])) {
            if ($data['zona']>0) {
               $zona=$data['zona'];
            }else{
                $zona=0;
            }
       }else{
            $zona=0;
       }
       if(isset($data['soli'])){
            if ($data['soli']>0) {
               $soli=$data['soli'];
            }else{
                $soli=0;
            }
       }else{
            $soli=0;
       }
       
       $tecnico=$data['tecnicoid'];
       $ciudad=0;
       
       //$result = $this->ModeloAsignacion->getListcitas($inicio,$fin,$area);
       $result = $this->ModeloAsignacion->getlistadocalentario($inicio,$fin,$servicio,$tecnico,$cliente,$ciudad,$this->perfilid,$this->idpersonal,$zona,$soli);
       echo json_encode($result->result());
    }
    function searchservicio(){
        $pro = $this->input->get('search');
        $results=$this->ModeloCatalogos->getseleclike('polizas','nombre',$pro);
        //echo $results;
        echo json_encode($results->result());
    }
    function obternerdatosservicio(){
        $result = $this->input->post();
        $prioridad=1;
        $botondefinalizar='';
        $asignacion = $result['asignacion'];
        $v_ser_id=$asignacion;
        $tipo = $result['tipo'];//1 contrato 2 poliza 3 cliente
        $v_ser_tipo=$tipo;
        $fecha = $result['fecha'];//1 contrato 2 poliza 3 cliente
        if (isset($result['serve'])) {
            $serve=$result['serve'];
        }else{
            $serve=0;
        }
        if (isset($result['oserier'])) {
            $oserier=$result['oserier'];
        }else{
            $oserier=0;
        }
        $equipos='';$equipos_no_consu='';
        $tserviciomotivo = '';
        $documentaccesorio = '';
        $contacto='';
        $comentariocal='';
        $equipoacceso='';
        $observaciones='';
        $confirmado=0;
        $idCliente=0;
        $pin='';
        if ($tipo==1) {
            $resultado=$this->ModeloAsignacion->getdatosrenta($asignacion,$fecha,$serve,0);
            $row_tr_r=0;
            foreach ($resultado->result() as $item) {
                $v_ufp=$this->Rentas_model->verificar_ultimifolios_periodo($item->id_equipo);
                if($v_ufp['consu_alert']==1){
                    $equipos_no_consu.='<div class="ocultar_tec" style="color:red">El Modelo:'.$item->modelo.' Serie:'.$item->serie.' tiene '.$v_ufp['consu_cant'].' consumibles por resurtir</div>';
                }
                $editcontacto=' <a class="ocultar_tec" onclick="edit_contacto_evento('.$asignacion.','.$item->idCliente.',1)"><i class="fa fa-pencil-alt2"></i></a> <a class="ocultar_tec" onclick="add_contacto_cli('.$item->idCliente.')"><i class="fa fa-user-plus"></i></a>';
                $editequipo=' <a onclick="edit_equipo_doc('.$asignacion.',1,1)"><i class="fa fa-pencil-alt2"></i></a>';
                $editdoc=' <a onclick="edit_equipo_doc('.$asignacion.',2,1)"><i class="fa fa-pencil-alt2"></i></a>';
                $pin=$item->pass;
                $contacto=$item->contacto.' '.$item->telefono.$editcontacto;
                if($item->contacto_text!=null){
                    $contacto=$item->contacto_text.$editcontacto;
                }
                if($item->idpexs>0){
                    $gar='<span class="gar"> Garantia</span>';
                }else{
                    $gar='';
                }
                $idCliente=$item->idCliente;
                $empresa = $item->empresa;
                $servicio = $item->servicio.$gar;
                $zona = $item->zona;
                $tecnico = $item->tecnico.', '.$item->tecnico2;
                if($item->per_fin>0){
                    $tecnico = $item->tecnicofin;
                }
                $prioridad = $item->prioridad;
                $tiposervicio = $item->tiposervicio;
                $fecha = $item->fecha;//log_message('error','$item->id_equipo: '.$item->id_equipo.' $item->serieId: '.$item->serieId);
                $direccionequipo= $this->getdireccionequipos($item->id_equipo,$item->serieId,$fecha);
                
                if(intval($row_tr_r)==0){
                    $hora = $item->hora.'<!--a '.$row_tr_r.'-->';
                    if($item->horaserinicio!=''){
                        $hora_data_i =date('G',strtotime($item->horaserinicio));    
                    }else{
                        $hora_data_i=0;
                    }
                    
                    $hora = $item->hora.'<!--b '.$row_tr_r.' '.$hora_data_i.'-->';
                    if($hora_data_i>0){
                        $hora = $item->horaserinicio.'<!--c '.$row_tr_r.' '.$hora_data_i.' '.$item->horaserinicio.'-->';
                    }else{

                    }
                }
                $horafin = $item->horafin;
                $hora_comida = $item->hora_comida;
                $horafin_comida = $item->horafin_comida;
                $comentariocal=$item->comentariocal;
                $confirmado.=$item->confirmado;
                
                if($item->tservicio_a>0){
                    $servicio = $item->poliza.$gar;
                }
                $equipo_acceso=$item->servicio_equipo;//info contrato
                $documentacion_acceso=$item->servicio_doc;//info contrato
                $equipo_acceso=$item->equipo_acceso.$editequipo;//info clientes
                $documentacion_acceso=$item->documentacion_acceso.$editdoc;//info clientes
                //$protocolo_acceso=$item->protocolo_acceso;
                $protocolo_acceso= $this->getdireccionequipos_pac($item->id_equipo,$item->serieId);
                if($item->equipo_acceso_info!=null){
                    $equipo_acceso=$item->equipo_acceso_info.$editequipo;//info clientes
                }
                if($item->documentacion_acceso_info!=null){
                    $documentacion_acceso=$item->documentacion_acceso_info.$editdoc;//info clientes
                }
                if($item->ubicacion!=''){
                    $ubicacion='<br>Ubicación: '.$item->ubicacion.'';
                }else{
                    $ubicacion='';
                }
                if($item->contadores!=''){
                    $inf_conta='('.$item->contadores.')';
                }else{
                    $inf_conta='';
                }
                if($item->nr==1){
                    $nr='<i class="fas fa-times-circle fa-fw" title="Equipo Rechazado"></i>';
                }else{
                    $nr='';
                }
                $r_sol_cot_r_icon='';
                //$r_sol_cot_r=$this->ModeloCatalogos->db2_getselectwheren('asignacion_ser_contrato_a_e',array('idequipo'=>$item->id_equipo,'serieId'=>$item->serieId,'cot_status'=>1));
                $r_sol_cot_r=$this->ModeloCatalogos->get_info_pendiente_solicitud_refaccion($item->id_equipo,$item->serieId,$item->serie);
                if($r_sol_cot_r->num_rows() >0){
                    foreach ($r_sol_cot_r->result() as $item_as) {
                        /*
                            1 contrato
                            2 poliza
                            3 evento
                            4 venta
                        */
                        $r_sol_cot_r_icon.='<i class="far fa-clipboard fa-fw notificacionsercot" title="Solicitud de refacción pendiente de cotizar" 
                                        data-asignacionide="'.$item_as->asignacionIde.'" data-asignacionid="'.$item_as->asignacionId.'"
                                        data-cotrefaccion="'.$item_as->cot_refaccion.'" data-cotrefaccion2="'.$item_as->cot_refaccion2.'"
                                        data-cotrefaccion3="'.$item_as->cot_refaccion3.'" data-tiposer="'.$item_as->tiposer.'"
                                        ></i>';
                    }
                    
                    
                }
                $estatuseq='';
                if($item->status==1){
                    $estatuseq='<i class="fas fa-sync fa-spin"></i>';
                }
                if($item->status==2){
                    $estatuseq='<i class="fas fa-clipboard-check fa-2x colordeiconofinalizado"></i>';
                }
                if($oserier==1){
                    $serie_eq=$this->ModeloGeneral->ocultar_ult_dig(4,$item->serie);
                }else{
                    $serie_eq=$item->serie;
                }


                $equipos .='<tr>';
                                $equipos .='<td style="font-size:12px; padding-right: 6px;">'.$estatuseq.''.$item->modelo.' </td>';
                                $equipos .='<td style="font-size:12px; padding-right: 6px;"> '.$serie_eq.' '.$inf_conta.' '.$nr.' '.$r_sol_cot_r_icon.'</td>';
                                $equipos .='<td style="font-size:10px;">'.$direccionequipo.' '.$ubicacion.'</td>';

                                $promedio1=$this->Rentas_model->produccionpromedio($item->id_equipo,$item->serieId,1,'','');
                                $promedio2=$this->Rentas_model->produccionpromedio($item->id_equipo,$item->serieId,2,'','');
                                $promedio=$promedio1+$promedio2;
                                $equipos .='<td style="font-size:10px;">'.$promedio.'</td>';
                $equipos .='</tr>';
                    
                if($item->tpoliza==22){
                    if($item->comentario!='' and $item->comentario!='null'){
                        $equipos .='<tr><td colspan="2">'.$item->comentario.'</td><td></td></tr>';
                    }
                    
                    

                    $rentaequipos_accesorios = $this->ModeloCatalogos->rentaequipos_accesorios1($item->idRenta,$item->idEquipo,$item->id_equipo);
                    foreach ($rentaequipos_accesorios->result() as $itemacc) {
                        $equipos .='<tr style="font-size:12px;"><td>'.$itemacc->cantidad.' '.$itemacc->nombre.'</td><td>'.$itemacc->series.'</td><td></td></tr>';
                    }
                    //$idcontratofolio=0;
                    $arrayconsumibles=array();
                    $rentaequipos_consumible = $this->ModeloCatalogos->rentaequipos_consumible1($item->idRenta,$item->idEquipo,$item->id_equipo);
                    foreach ($rentaequipos_consumible->result() as $item0) {
                        $contrato_folio2 = $this->ModeloCatalogos->obtenerfoliosconsumiblesrenta3($item->idRenta,$item0->id_consumibles,$item0->id,$item->serieId); 
                        foreach ($contrato_folio2->result() as $itemx) {
                            //if($idcontratofolio!=$itemx->idcontratofolio){
                                $arrayconsumibles[$itemx->idcontratofolio]=array('modelo'=>$item0->modelo,'parte'=>$item0->parte,'folio'=>$itemx->foliotext);
                                //$equipos .='<tr><td>'.$item0->modelo.' No° parte '.$item0->parte.'</td><td>'.$itemx->foliotext.'</td><td></td></tr>';
                            //}  
                        } 
                    }
                    foreach ($arrayconsumibles as $itemu) {
                        $equipos .='<tr style="font-size:12px;"><td>'.$itemu['modelo'].' No° parte '.$itemu['parte'].'</td><td>'.$itemu['folio'].'</td><td></td></tr>';
                    }
                    
                }
                if($item->comentarioasge!='' and $item->comentarioasge!=null){
                        $equipos .='<tr><td colspan="3" style="font-size:12px; padding-right: 6px; border-bottom: 1px solid #d0d0d0;">'.$item->comentarioasge.'</td><td></td></tr>';
                    }
                $equipoacceso='<div class="row">';
                    $equipoacceso.='<div class="col s4 m5 6 textcolorred">Equipo de acceso</div>';
                    $equipoacceso.='<div class="col s8 m7 6 ">'.$equipo_acceso.'</div>';
                $equipoacceso.='</div>';
                $equipoacceso.=' <div class="row">';
                    $equipoacceso.='<div class="col s4 m5 6 textcolorred">Detalle</div>';
                    $equipoacceso.='<div class="col s8 m7 6 resaltomotivo">'.$item->tserviciomotivo.'</div>';
                $equipoacceso.='</div>';
                if($hora_comida=='00:00:00'){

                }else{
                $equipoacceso.='<div class="row">';
                    $equipoacceso.='<div class="col s4 m5 6 textcolorred ">Comida</div>';
                    $equipoacceso.='<div class="col s8 m7 6 ">'.$hora_comida.' '.$horafin_comida.'</div>';
                $equipoacceso.='</div>';
                }
                $equipoacceso.='<div class="row">';
                                $equipoacceso.='<div class="col s4 m5 6 textcolorred">Documentos de acceso</div>';
                                $equipoacceso.='<div class="col s8 m7 6 ">'.$documentacion_acceso.'</div>';
                $equipoacceso.='</div>';

                $equipoacceso.='<div class="row">';
                                $equipoacceso.='<div class="col s4 m5 6 textcolorred">Protocolo de acceso</div>';
                                  $equipoacceso.='<div class="col s8 m7 6 ">'.$protocolo_acceso.'</div>';
                $equipoacceso.='</div>';
                if($this->idpersonal==57 || $this->idpersonal==54 || $this->idpersonal==35 || $this->idpersonal==1){//id del antonio
                    if($item->status<2){
                        if($fecha<$this->fechahoy){
                            $botondefinalizar='<button type="button" class="waves-effect btn-bmz  red" onclick="cl_ser_statusser('.$item->asignacionId.','.$item->asignacionIde.',1,1)">Finalizar servicio</button>';    
                            //$botondefinalizar.='('.$item->status.')';
                        }
                        
                    }
                }
                $row_tr_r++;
            }
            $confirmado = substr($confirmado, 1);
            //log_message('error','$confirmado: '.$confirmado);
            if (preg_match('/^[1]+$/', $confirmado)) {
               $confirmado=1;
            } else {
                $confirmado=0;
            }
        }
        if ($tipo==2) {
            $resultado=$this->ModeloAsignacion->getdatospoliza($asignacion,$serve,0);
            foreach ($resultado->result() as $item) {
                $editcontacto=' <a class="ocultar_tec" onclick="edit_contacto_evento('.$asignacion.','.$item->idCliente.',2)"><i class="fa fa-pencil-alt2"></i></a> 
                <a class="ocultar_tec" onclick="add_contacto_cli('.$item->idCliente.')"><i class="fa fa-user-plus"></i></a>';
                $editequipo=' <a onclick="edit_equipo_doc('.$asignacion.',1,2)"><i class="fa fa-pencil-alt2"></i></a>';
                $editdoc=' <a onclick="edit_equipo_doc('.$asignacion.',2,2)"><i class="fa fa-pencil-alt2"></i></a>';
                if($item->idpexs>0){
                    $gar='<span class="gar"> Garantia</span>';
                }else{
                    $gar='';
                }
                if($item->tipo_pre_corr==1){
                    $gar.='<span class="gar">(Preventivo)</span>';
                }
                if($item->tipo_pre_corr==2){
                    $gar.='<span class="gar">(Correctivo)</span>';
                }
                $pin=$item->pass;
                $contacto=$item->atencion.' '.$item->telefono.$editcontacto;
                if($item->contacto_text!=null){
                    $contacto=$item->contacto_text.$editcontacto;
                }
                $idCliente=$item->idCliente;
                $empresa = $item->empresa;
                $servicio = $item->servicio.$gar;
                $zona = $item->zona;
                $tecnico = $item->tecnico.', '.$item->tecnico2;
                if($item->per_fin>0){
                    $tecnico = $item->tecnicofin;
                }
                $prioridad = $item->prioridad;
                $tiposervicio = $item->tiposervicio;
                $fecha = $item->fecha;
                $hora = $item->hora;
                $horafin = $item->horafin;
                $hora_comida = $item->hora_comida;
                $horafin_comida = $item->horafin_comida;
                $comentariocal=$item->comentariocal;
                $confirmado=$item->confirmado;
                $equipo_acceso=$item->equipo_acceso.$editequipo;//info clientes
                $documentacion_acceso=$item->documentacion_acceso.$editdoc;//info clientes
                //$protocolo_acceso=$item->protocolo_acceso;
                $protocolo_acceso='';
                if($item->iddir>0){
                    $result_dir = $this->ModeloCatalogos->getselectwheren('clientes_direccion',array('idclientedirecc'=>$item->iddir));
                    foreach ($result_dir->result() as $itempa) {
                        $protocolo_acceso=$itempa->pro_acc;
                    }
                }
                if($item->equipo_acceso_info!=null){
                    $equipo_acceso=$item->equipo_acceso_info.$editequipo;//info clientes
                }
                if($item->documentacion_acceso_info!=null){
                    $documentacion_acceso=$item->documentacion_acceso_info.$editdoc;//info clientes
                }
                if($item->nr==1){
                    $nr='<i class="fas fa-times-circle fa-fw"></i>';
                }else{
                    $nr='';
                }
                $estatuseq='';
                if($item->status==1){
                    $estatuseq='<i class="fas fa-sync fa-spin"></i>';
                }
                if($item->status==2){
                    $estatuseq='<i class="fas fa-clipboard-check fa-2x colordeiconofinalizado"></i>';
                }
                $equipoacceso='';
                $r_sol_cot_r_icon='';
                //$r_sol_cot_r=$this->ModeloCatalogos->db2_getselectwheren('asignacion_ser_contrato_a_e',array('idequipo'=>$item->id_equipo,'serieId'=>$item->serieId,'cot_status'=>1));
                $r_sol_cot_r=$this->ModeloCatalogos->get_info_pendiente_solicitud_refaccion(0,0,$item->serie);
                if($r_sol_cot_r->num_rows() >0){
                    foreach ($r_sol_cot_r->result() as $item_as) {
                        /*
                            1 contrato
                            2 poliza
                            3 evento
                            4 venta
                        */
                        $r_sol_cot_r_icon.='<i class="far fa-clipboard fa-fw notificacionsercot" title="Solicitud de refacción pendiente de cotizar" 
                                        data-asignacionide="'.$item_as->asignacionIde.'" data-asignacionid="'.$item_as->asignacionId.'"
                                        data-cotrefaccion="'.$item_as->cot_refaccion.'" data-cotrefaccion2="'.$item_as->cot_refaccion2.'"
                                        data-cotrefaccion3="'.$item_as->cot_refaccion3.'" data-tiposer="'.$item_as->tiposer.'"
                                        ></i>';
                    }
                    
                    
                }
                $equipos .='<tr>
                                <td>'.$estatuseq.''.$item->modelo.'</td>
                                <td>'.$item->serie.$nr.$r_sol_cot_r_icon.'</td>
                                <td style="font-size:10px;">'.$item->direccionservicio.'</td>
                            </tr>
                            <tr><td colspan="3">'.$item->comentario.' '.$item->comeninfo.'</td></tr>';
                $equipoacceso.='<div class="row">
                                    <div class="col s4 m5 6 textcolorred ">Equipo acceso</div>
                                    <div class="col s8 m7 6 ">'.$equipo_acceso.'</div>
                                </div>';
                $equipoacceso.='<div class="row">
                                    <div class="col s4 m5 6 textcolorred ">Documentación acceso</div>
                                    <div class="col s8 m7 6 ">'.$documentacion_acceso.'</div>
                                </div>';
                $equipoacceso.='<div class="row">
                                    <div class="col s4 m5 6 textcolorred ">Protocolo de acceso</div>
                                    <div class="col s8 m7 6 ">'.$protocolo_acceso.'</div>
                                </div>';
                $equipoacceso.=' <div class="row">
                                    <div class="col s4 m5 6 textcolorred">Observaciones</div>
                                    <div class="col s8 m7 6 ">'.$item->observaciones.'</div>
                                </div>';
                if($hora_comida=='00:00:00'){

                }else{
                $equipoacceso.='<div class="row">
                                    <div class="col s4 m5 6 textcolorred ">Comida</div>
                                    <div class="col s8 m7 6 ">'.$hora_comida.' '.$horafin_comida.'</div>
                                </div>';
                }
                
                $equipoacceso.='<div class="row">
                                <div class="col s12 m5 6 textcolorred">
                                    
                                  </div>
                                  <div class="col s12 m7 6 ">
                                    <a class="btn-floating green tooltipped" data-position="top" data-delay="50" data-tooltip="Prefactura" onclick="detallep('.$item->ventaId.')" data-tooltip-id="cd922c14-68f5-032f-afe1-dea43e9b7f5a">                                    <i class="material-icons">assignment</i>                                  </a>
                                  </div>
                                </div>
                                ';
                if($this->idpersonal==54 || $this->idpersonal==57 || $this->idpersonal==35 || $this->idpersonal==1){//id del antonio
                    if($item->status<2){
                        if($fecha<$this->fechahoy){
                            $botondefinalizar='<button type="button" class="waves-effect btn-bmz  red" onclick="cl_ser_statusser('.$item->asignacionId.','.$item->asignacionIde.',2,1)">Finalizar servicio</button>';    
                        }else{
                            $botondefinalizar='<!--('.$this->idpersonal.')'.'('.$item->status.')'.'('.$fecha.')-->';
                        }
                        
                    }else{
                        $botondefinalizar='<!--('.$this->idpersonal.')'.'('.$item->status.')'.'('.$fecha.')-->';
                    }
                }

            }

        }
        if ($tipo==3) {
            $resultado=$this->ModeloAsignacion->getdatoscliente($asignacion);
            foreach ($resultado->result() as $item) {
                $contacto='<a class="ocultar_tec" onclick="edit_contacto_evento('.$asignacion.','.$item->clienteId.',3)"><i class="fa fa-pencil-alt2"></i></a> 
                <a class="ocultar_tec" onclick="add_contacto_cli('.$item->clienteId.')"><i class="fa fa-user-plus"></i></a>';
                $editequipo=' <a onclick="edit_equipo_doc('.$asignacion.',1,3)"><i class="fa fa-pencil-alt2"></i></a>';
                $editdoc=' <a onclick="edit_equipo_doc('.$asignacion.',2,3)"><i class="fa fa-pencil-alt2"></i></a>';
                if($item->idpexs>0){
                    $gar='<span class="gar"> Garantia</span>';
                }else{
                    $gar='';
                }
                $pin=$item->pass;
                $idCliente=$item->clienteId;
                if($item->contacto_text!=''){
                    $contacto=$item->contacto_text.' '.$contacto;
                }else{
                    $contacto=$contacto;
                }
                $equipoacceso='';
                $empresa = $item->empresa;
                $servicio = $item->servicio.$gar;
                $zona = $item->zona;
                $tecnico = $item->tecnico.', '.$item->tecnico2;
                $prioridad = $item->prioridad;
                $tiposervicio = $item->tiposervicio;
                $fecha = $item->fecha;
                $hora = $item->hora;
                $horafin = $item->horafin;
                $hora_comida = $item->hora_comida;
                $horafin_comida = $item->horafin_comida;
                if($item->nr==1){
                    $nr='<i class="fas fa-times-circle fa-fw"></i>';
                }else{
                    $nr='';
                }
                $r_sol_cot_r_icon='';
                //$r_sol_cot_r=$this->ModeloCatalogos->db2_getselectwheren('asignacion_ser_contrato_a_e',array('idequipo'=>$item->id_equipo,'serieId'=>$item->serieId,'cot_status'=>1));
                $r_sol_cot_r=$this->ModeloCatalogos->get_info_pendiente_solicitud_refaccion(0,0,$item->serie);
                if($r_sol_cot_r->num_rows() >0){
                    foreach ($r_sol_cot_r->result() as $item_as) {
                        /*
                            1 contrato
                            2 poliza
                            3 evento
                            4 venta
                        */
                        $r_sol_cot_r_icon.='<i class="far fa-clipboard fa-fw notificacionsercot" title="Solicitud de refacción pendiente de cotizar" 
                                        data-asignacionide="'.$item_as->asignacionIde.'" data-asignacionid="'.$item_as->asignacionId.'"
                                        data-cotrefaccion="'.$item_as->cot_refaccion.'" data-cotrefaccion2="'.$item_as->cot_refaccion2.'"
                                        data-cotrefaccion3="'.$item_as->cot_refaccion3.'" data-tiposer="'.$item_as->tiposer.'"
                                        ></i>';
                    }
                    
                    
                }
                if($item->comentario!=''){
                    $td_comen='<td style="font-size:10px;">'.$item->comentario.'</td>';
                }else{
                    $td_comen='';
                }
                $estatuseq='';
                if($item->status==1){
                    $estatuseq='<i class="fas fa-sync fa-spin"></i>';
                }
                if($item->status==2){
                    $estatuseq='<i class="fas fa-clipboard-check fa-2x colordeiconofinalizado"></i>';
                }
                if($item->retiro==1){
                    $modelo_eq=$item->modeloretiro;
                }else{
                    $modelo_eq=$item->modelo;
                }
                $equipos .='<tr><td>'.$estatuseq.$modelo_eq.'</td><td>'.$item->serie.$nr.$r_sol_cot_r_icon.'</td><td style="font-size:10px;">'.$item->direccion.'</td>'.$td_comen.'</tr>';
                $tserviciomotivo = $item->tserviciomotivo;
                $documentaccesorio = $item->documentaccesorio;
                $comentariocal=$item->comentariocal;
                $confirmado=$item->confirmado;
                if($item->tservicio_a>0){
                    $servicio = $item->poliza.$gar;
                }
                $equipo_acceso=$item->equipo_acceso.$editequipo;//info clientes
                $documentacion_acceso=$item->documentacion_acceso.$editdoc;//info clientes
                //$protocolo_acceso=$item->protocolo_acceso;
                $protocolo_acceso='';
                if($item->iddir>0){
                    $result_dir = $this->ModeloCatalogos->getselectwheren('clientes_direccion',array('idclientedirecc'=>$item->iddir));
                    foreach ($result_dir->result() as $itempa) {
                        $protocolo_acceso=$itempa->pro_acc;
                    }
                }
                if($item->equipo_acceso_info!=null){
                    $equipo_acceso=$item->equipo_acceso_info.$editequipo;//info clientes
                }
                if($item->documentacion_acceso_info!=null){
                    $documentacion_acceso=$item->documentacion_acceso_info.$editdoc;//info clientes
                }

                $equipoacceso.='<div class="row">
                                    <div class="col s4 m5 6 textcolorred ">Equipo acceso</div>
                                    <div class="col s8 m7 6 ">'.$equipo_acceso.'</div>
                                </div>';
                $equipoacceso.='<div class="row">
                                    <div class="col s4 m5 6 textcolorred ">Documentación acceso</div>
                                    <div class="col s8 m7 6 ">'.$documentacion_acceso.'</div>
                                </div>';
                $equipoacceso.='<div class="row">
                                    <div class="col s4 m5 6 textcolorred ">Protocolo de acceso</div>
                                    <div class="col s8 m7 6 ">'.$protocolo_acceso.'</div>
                                </div>';
                if($hora_comida=='00:00:00'){

                }else{
                $equipoacceso.=' <div class="row">
                                    <div class="col s4 m5 6 textcolorred ">Comida</div>
                                    <div class="col s8 m7 6 ">'.$hora_comida.' '.$horafin_comida.'</div>
                                </div>';
                }
                if($this->idpersonal==54 || $this->idpersonal==57 || $this->idpersonal==35 || $this->idpersonal==1){//id del antonio
                    if($item->status<2){
                        if($fecha<$this->fechahoy){
                            $botondefinalizar='<button type="button" class="waves-effect btn-bmz  red" onclick="cl_ser_statusser('.$item->asignacionId.','.$item->asignacionIdd.',3,1)">Finalizar servicio</button>';    
                        }
                        
                    }
                }
            }
        }
        if ($tipo==4) {
            $resultado=$this->ModeloAsignacion->getdatosventas($asignacion);
            foreach ($resultado->result() as $item) {
                $editcontacto=' <a class="ocultar_tec" onclick="edit_contacto_evento('.$asignacion.','.$item->clienteId.',4)"><i class="fa fa-pencil-alt2"></i></a> 
                <a class="ocultar_tec" onclick="add_contacto_cli('.$item->clienteId.')"><i class="fa fa-user-plus"></i></a>';
                $editequipo=' <a onclick="edit_equipo_doc('.$asignacion.',1,4)"><i class="fa fa-pencil-alt2"></i></a>';
                $editdoc=' <a onclick="edit_equipo_doc('.$asignacion.',2,4)"><i class="fa fa-pencil-alt2"></i></a>';
                if($item->idpexs>0){
                    $gar='<span class="gar"> Garantia</span>';
                }else{
                    $gar='';
                }
                $idCliente=$item->clienteId;
                $equipoacceso='';
                $tipov = $item->tipov;
                $empresa = $item->empresa;
                $servicio = $item->servicio.$gar;
                $zona = $item->zona;
                $tecnico = $item->tecnico;
                if($item->per_fin>0){
                    $tecnico = $item->tecnicofin;
                }
                $prioridad = $item->prioridad;
                $tiposervicio = $item->tiposervicio;
                $fecha = $item->fecha;
                $hora = $item->hora;
                $horafin = $item->horafin;
                $hora_comida = $item->hora_comida;
                $horafin_comida = $item->horafin_comida;
                $direccion = $item->direccion;
                $ventaId = $item->ventaId;
                $ventaIdd = $item->ventaIdd;
                if($item->nr==1){
                    $nr='<i class="fas fa-times-circle fa-fw"></i>';
                }else{
                    $nr='';
                }
                //$equipos .='<tr><td colspan="2">'.$item->serie.'</td><td style="font-size:10px;">'.$item->direccion.'</td></tr>';
                $tserviciomotivo = $item->tserviciomotivo;
                $documentaccesorio = $item->documentaccesorio;
                $comentariocal=$item->comentariocal;
                $confirmado=$item->confirmado;
                $contacto=$item->atencionpara.$editcontacto;
                if($item->contacto_text!=null){
                    $contacto=$item->contacto_text.$editcontacto;
                }
                
                if($item->tservicio_a>0){
                    $servicio = $item->poliza.$gar;
                }
                $equipo_acceso=$item->equipo_acceso.$editequipo;//info clientes
                $documentacion_acceso=$item->documentacion_acceso.$editdoc;//info clientes
                if($item->equipo_acceso_info!=null){
                    $equipo_acceso=$item->equipo_acceso_info.$editequipo;//info clientes
                }
                if($item->documentacion_acceso_info!=null){
                    $documentacion_acceso=$item->documentacion_acceso_info.$editdoc;//info clientes
                }
                $estatuseq='';
                if($item->status==1){
                    $estatuseq='<i class="fas fa-sync fa-spin"></i>';
                }
                if($item->status==2){
                    $estatuseq='<i class="fas fa-clipboard-check fa-2x colordeiconofinalizado"></i>';
                }
                $equipoacceso.='<div class="row">
                                    <div class="col s4 m5 6 textcolorred ">
                                        Equipo acceso
                                    </div>
                                    <div class="col s8 m7 6 ">
                                        '.$equipo_acceso.'
                                    </div>
                                </div>';
                $equipoacceso.='<div class="row">
                                    <div class="col s4 m5 6 textcolorred ">
                                        Documentacion acceso
                                    </div>
                                    <div class="col s8 m7 6 ">
                                        '.$documentacion_acceso.'
                                    </div>
                                </div>';
                $equipoacceso.=' <div class="row">
                                    <div class="col s4 m5 6 textcolorred v_'.$ventaIdd.'">
                                        Observaciones
                                    </div>
                                    <div class="col s8 m7 6 ">
                                        '.$item->observaciones.'
                                    </div>
                                </div>';
                if($hora_comida=='00:00:00'){

                }else{
                $equipoacceso.='<div class="row">
                                    <div class="col s4 m5 6 textcolorred ">
                                        Comida
                                    </div>
                                    <div class="col s8 m7 6 ">
                                        '.$hora_comida.' '.$horafin_comida.'
                                    </div>
                                </div>';
                }
                $equipoacceso.='<div class="row">
                                <div class="col s12 m5 6 textcolorred">
                                    
                                  </div>
                                  <div class="col s12 m7 6">
                                    <a class="btn-floating green tooltipped" data-position="top" data-delay="50" data-tooltip="Prefactura" onclick="detallev('.$item->ventaId.','.$tipov.')"><i class="material-icons">assignment</i></a>
                                  </div>
                                </div>
                                ';
                if($this->idpersonal==54 || $this->idpersonal==57 ||$this->idpersonal==35 || $this->idpersonal==1){//id del antonio
                    if($item->status<2){
                        if($fecha<$this->fechahoy){
                            $botondefinalizar='<button type="button" class="waves-effect btn-bmz  red" onclick="cl_ser_statusser('.$item->asignacionId.',0,4,1)">Finalizar servicio</button>';  
                            //$botondefinalizar.='('.$item->status.')';  
                        }
                        
                    }
                }

            }
            if ($tipov==1) {
                $reesequi = $this->ModeloCatalogos->db13_getselectwheren('ventacombinada',array('combinadaId'=>$ventaIdd));
                foreach ($reesequi->result() as $item) {
                    $ventaIdd=$item->equipo;
                }
            }
            $resultadoequipos = $this->ModeloCatalogos->db13_getselectwheren('ventas_has_detallesEquipos',array('idVenta'=>$ventaIdd));
            $resultadoaccesorios = $this->ModeloCatalogos->accesoriosventa($ventaIdd);
            $resultadoconsumibles = $this->ModeloCatalogos->consumiblesventa($ventaIdd);
            $consumiblesventadetalles = $this->ModeloCatalogos->consumiblesventadetalles($ventaIdd);
            $polizaventadetalles = $this->ModeloCatalogos->polizaventadetalles($ventaIdd);
            $ventadetallesrefacion = $this->ModeloCatalogos->ventas_has_detallesRefacciones($ventaIdd);

            foreach ($resultadoequipos->result() as $item) { 
                $model=$this->ModeloCatalogos->obtenernumeroserieequipo($item->idEquipo);
                $resuleserie=$this->ModeloCatalogos->getequiposerie($item->id);

                foreach ($resuleserie->result() as $itemse) { 
                    $equipos .='<tr><td>'.$estatuseq.''.$item->modelo.'</td><td>'.$itemse->serie.$nr.'</td><td style="font-size:10px;">'.$direccion.'</td></tr>';
                }
                
            }
            foreach ($resultadoaccesorios->result() as $item) { 
                $resuleserie=$this->ModeloCatalogos->getaccesorioserie($item->id_accesoriod);
                foreach ($resuleserie->result() as $itemse) { 
                    $equipos .='<tr><td>'.$estatuseq.''.$item->nombre.'</td><td>'.$itemse->serie.'</td><td style="font-size:10px;">'.$direccion.'</td></tr>';
                }
            }
            foreach ($resultadoconsumibles->result() as $item) { 
                $equipos .='<tr><td>'.$estatuseq.''.$item->modelo.'</td><td></td><td style="font-size:10px;">'.$direccion.'</td></tr>';
            }
            foreach ($consumiblesventadetalles->result() as $item) { 
                $equipos .='<tr><td>'.$estatuseq.''.$item->modelo.'</td><td><table>
                                                                <tr>
                                                                    <td width="50%"></td>
                                                                    <td width="50%" style="color: #625e5e; font-size: 11px;">Equipo referencia<br>'.$item->modeloeq.'</td></tr></table></td><td style="font-size:10px;">'.$direccion.'</td></tr>';
            }
            foreach ($ventadetallesrefacion->result() as $item) {
                $resuleserie=$this->ModeloCatalogos->getrefaccionserie($item->id);
                foreach ($resuleserie->result() as $itemse) {
                    $equipos .='<tr><td>'.$estatuseq.''.$item->modelo.'</td><td><table><tr><td rowspan="2">'.$itemse->serie.'</td><td style="color: #625e5e; font-size: 11px;">Equipo referencia<br>'.$item->modeloeq.'</td></tr><tr><td style="color: #625e5e; font-size: 11px;">'.$item->serieeq.'</td></tr></table> </td><td style="font-size:10px;">'.$direccion.'</td></tr>';
                }
            }

        }
        $prioridadl='';
        if ($prioridad==1) {
            $prioridadl='Servicios Correctivos';
        }elseif ($prioridad==2) {
            $prioridadl='Asesoría - Conexión remota';
        }elseif ($prioridad==3) {
            $prioridadl='Pólizas';
        }elseif ($prioridad==4) {
            $prioridadl='servicio regular';
        }elseif ($prioridad==5) {
            $prioridadl='Mensajería';
        }
        $htmlservicio='';
        if ($tipo==1) {
            foreach ($resultado->result() as $item) {
                $datosconsumiblesfolio = $this->ModeloServicio->getfoliosservicios($asignacion,$item->serieId);
                
                if ($datosconsumiblesfolio->num_rows()>0) {
                    $htmlservicio.=' <p> <b> Consumibles</b></p>';
                }
                    $htmlservicio.='<ul>';
                foreach ($datosconsumiblesfolio->result() as $item) {
                        $htmlservicio.='<li> Modelo: '.$item->modelo.' parte:'.$item->parte.' Folio:'.$item->foliotext.'</li>';
                }
                    $htmlservicio.='    </ul>';
            }
        }
        if ($tipo==2) {
            //foreach ($resultado->result() as $item) {
                $datosconsumiblesfolio = $this->ModeloServicio->getfoliosservicios2($asignacion);
                
                if ($datosconsumiblesfolio->num_rows()>0) {
                    $htmlservicio.=' <p> <b> Consumibles</b></p>';
                }
                    $htmlservicio.='<ul>';
                foreach ($datosconsumiblesfolio->result() as $item) {
                        $htmlservicio.='<li> Modelo: '.$item->modelo.' parte:'.$item->parte.' Folio:'.$item->foliotext.'</li>';
                }
                    $htmlservicio.='    </ul>';
            //}
        }
        if($tipo<4){
            $datosres_equi = $this->ModeloCatalogos->obtenerproducventavinculada($asignacion,$tipo);
            foreach ($datosres_equi->result() as $itemre) {
                $htmlservicio.='<li>Cant: '.$itemre->cantidad.' Modelo: '.$itemre->modelo.' Serie:'.$itemre->serie.' </li>';
            }
        }
        $resultv=$this->ModeloCatalogos->getselectwheren('ventas',array('v_ser_tipo'=>$v_ser_tipo,'v_ser_id'=>$v_ser_id,'activo'=>1));
            foreach ($resultv->result() as $itemv) {
                $btn='<a class="btn btn-primary"  onclick="detallev('.$itemv->id.',0)"><i class="fas fa-file-invoice"></i></a>';

                $equipoacceso.='<div class="row">
                                    <div class="col s12 m5 6 textcolorred">
                                        
                                      </div>
                                      <div class="col s12 m7 6">
                                        <a class="btn-floating green tooltipped" data-position="top" data-delay="50" data-tooltip="Prefactura" onclick="detallev('.$itemv->id.',0)"><i class="material-icons">assignment</i></a>
                                        <button class="b-btn b-btn-danger tooltipped ocultar_tec" data-position="top" data-delay="50" data-tooltip="Desvicular venta" onclick="desv_venta_ser('.$itemv->id.')" style="padding-top: 3px;padding-bottom: 3px;padding-left: 8px;padding-right: 8px;font-size: 11px;"><i class="fas fa-times"></i></button>
                                      </div>
                                    </div>';

            }
        if($v_ser_tipo==1){
            $sql2r="SELECT cf.*,c.modelo,c.parte,p.nombre,r.reg,cli.empresa,vdc.idVentas,ven.total_general,ven.combinada
                      FROM contrato_folio AS cf 
                      INNER JOIN consumibles AS c ON c.id = cf.idconsumible 
                      INNER JOIN rentas AS r ON r.id = cf.idrenta 
                      INNER JOIN personal AS p ON p.personalId = r.id_personal 
                      INNER JOIN clientes AS cli ON cli.id = r.idCliente 
                      INNER JOIN ventas_has_detallesConsumibles as vdc on vdc.foliotext=cf.foliotext
                      INNER JOIN ventas as ven on ven.id=vdc.idVentas
                      where cf.serviciocId='$v_ser_id' and cf.status<=1 and cf.serviciocId_tipo=0
                      group by vdc.idVentas";

                      $query2r = $this->db->query($sql2r);
                      foreach ($query2r->result() as $itemcv) {
                        $equipoacceso.='<div class="row">
                                    <div class="col s12 m5 6 textcolorred">
                                        
                                      </div>
                                      <div class="col s12 m7 6">
                                        <a class="btn-floating green tooltipped" data-position="top" data-delay="50" data-tooltip="Prefactura" onclick="detallev('.$itemcv->idVentas.',0)"><i class="material-icons">assignment</i></a>
                                        
                                      </div>
                                    </div>';
                        
                      }
        }


            $htmlservicio.=$equipos_no_consu;
                                //==============================================================================
        $datosarray = array(
                'cliente'=>$empresa,
                'idcliente'=>$idCliente,
                'contacto'=>$contacto,
                'servicio'=>$servicio,
                'zona'=>$zona,
                'tecnico'=>$tecnico,
                'prioridad'=>$prioridadl,
                'tiposervicio'=>$tiposervicio,
                'fecha' => $fecha,
                'hora' => $hora,
                'horafin' => $horafin,
                'hora_comida' => $hora_comida,
                'horafin_comida' => $horafin_comida,
                'equipos'=>$equipos,
                'm_tipo'=>$tipo,
                'm_servicio'=>$tserviciomotivo,
                'm_doc_acce'=>$documentaccesorio,
                'dconsumibles'=>$htmlservicio,
                'comentariocal'=>$comentariocal,
                'equipoacceso'=>$equipoacceso,
                'confirmado'=>$confirmado,
                'botondefinalizar'=>$botondefinalizar,
                'pin'=>$pin
        );
        echo json_encode($datosarray);
    }
    public function registro_re_agenda(){
        $data = $this->input->post();
        $tipo = $data['tipo'];
        $comentario = $this->input->post('comentariocal');
        $where = array('asignacionId' =>$data['id_asignacion']);
        $datos = array('t_fecha' =>$data['fecha'],'t_horainicio' =>$data['hora'],'t_horafin' =>$data['horafin']);
        if($tipo==1){//renta
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_contrato_a_e',$datos,$where);
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_contrato_a',array('comentariocal'=>$comentario),$where);
        }else if($tipo==2){//poliza
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_poliza_a_e',$datos,$where);
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_poliza_a',array('comentariocal'=>$comentario),$where);
        }else if($tipo==3){//evento
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_cliente_a',$datos,$where);
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_cliente_a',array('comentariocal'=>$comentario),$where);
        }else if($tipo==4){//evento
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_venta_a',$datos,$where);
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_venta_a',array('comentariocal'=>$comentario),$where);
        }
    }
    public function confirmar_asignar(){
        $id_asig = $this->input->post('id_asigna');
        $tipo = $this->input->post('tipo');
        $comentario = $this->input->post('comentario');
        
        if($tipo==1){//renta
            $where = array('asignacionId' =>$id_asig);
            $datos = array('confirmado' =>1,'confirmado_personal'=>$this->idpersonal,'confirmado_fecha'=>$this->fechahoy);
            $this->ModeloCatalogos->db4_updateCatalogo('asignacion_ser_contrato_a_e',$datos,$where);
            $this->ModeloCatalogos->db4_updateCatalogo('asignacion_ser_contrato_a',array('comentariocal'=>$comentario),$where);
        }else if($tipo==2){//poliza
            $where = array('asignacionId' =>$id_asig);
            $datos = array('confirmado' =>1,'confirmado_personal'=>$this->idpersonal,'confirmado_fecha'=>$this->fechahoy);
            $this->ModeloCatalogos->db4_updateCatalogo('asignacion_ser_poliza_a_e',$datos,$where);
            $this->ModeloCatalogos->db4_updateCatalogo('asignacion_ser_poliza_a',array('comentariocal'=>$comentario),$where);
        }else if($tipo==3){//evento
            $where = array('asignacionId' =>$id_asig);
            $datos = array('confirmado' =>1,'comentariocal'=>$comentario,'confirmado_personal'=>$this->idpersonal,'confirmado_fecha'=>$this->fechahoy);
            $this->ModeloCatalogos->db4_updateCatalogo('asignacion_ser_cliente_a',$datos,$where);
        }else if($tipo==4){//evento
            $where = array('asignacionId' =>$id_asig);
            $datos = array('confirmado' =>1,'comentariocal'=>$comentario,'confirmado_personal'=>$this->idpersonal,'confirmado_fecha'=>$this->fechahoy);
            $this->ModeloCatalogos->db4_updateCatalogo('asignacion_ser_venta_a',$datos,$where);
        }
        $this->ModeloAsignacion->agregaraitinerario($id_asig,$tipo);
        $this->ModeloCatalogos->db4_Insert('bitacora_confirmacion',array('tipo'=>$tipo,'personal'=>$this->idpersonal,'fecha'=>$this->fechahoy,'idser'=>$id_asig,'info'=>'confirmacion'));
    }
    function obtenerdireccionescontratoubicacion(){
        $params = $this->input->post();
        $equipo=$params['equipo'];
        $serie=$params['serie'];
        $resultdir=$this->ModeloCatalogos->db2_getselectwheren('alta_rentas_equipos',array('id_equipo'=>$equipo,'serieId'=>$serie));
        $ubicacion='';
        foreach ($resultdir->result() as $item) {
            $ubicacion=$item->ubicacion;
            // code...
        }
        echo $ubicacion;
    }
    function obtenerdireccionescontrato(){
        $params = $this->input->post();
        $equipo=$params['equipo'];
        $serie=$params['serie'];
        $fecha=$this->fechahoy;
        $direccion=$this->getdireccionequipos($equipo,$serie,$fecha);
        echo $direccion;
    }
    function getdireccionequipos($equipo,$serie,$fecha){
        //hay una version de esta funcion directamente en ModeloCatalogos->getdireccionequipos2 por si se va a hacer algun ajuste a este
        $resultdir=$this->ModeloCatalogos->db2_getselectwheren('contrato_equipo_direccion',array('equiposrow'=>$equipo,'serieId'=>$serie));
        $iddireccionget='g_0';
        $direccion='';
        foreach ($resultdir->result() as $itemdir) {
            if($itemdir->direccion!=''){
                $iddireccionget=$itemdir->direccion;
            }
        }
        //log_message('error','iddireccionget '.$iddireccionget);
        if($iddireccionget!='g_0'){
            $tipodireccion=explode('_',$iddireccionget);

            $tipodir = $tipodireccion[0];
            $iddir   = $tipodireccion[1];
            if ($tipodir=='g') {
                $resultdirc_where['idclientedirecc']=$iddir;
                
                if($fecha>=$this->fechahoy){
                    //log_message('error','entra direccion g');
                    $resultdirc_where['status']=1;
                }
                $resultdirc=$this->ModeloCatalogos->db2_getselectwheren('clientes_direccion',$resultdirc_where);
                foreach ($resultdirc->result() as $item) {
                    $direccion=$item->direccion;
                }
            }
            if ($tipodir=='f') {
                $resultdirc_where['id']=$iddir;
                
                if($fecha>=$this->fechahoy){
                    //log_message('error','entra direccion f');
                    $resultdirc_where['activo']=1;
                }
                $resultdirc=$this->ModeloCatalogos->db2_getselectwheren('cliente_has_datos_fiscales',$resultdirc_where);
                foreach ($resultdirc->result() as $item) {
                    $direccion='Razon social: '.$item->razon_social.' Colonia:'.$item->colonia.' Calle:'.$item->calle.' Numero:'.$item->num_ext;
                }
            }
        }
        return $direccion;
    }
    
    function getdireccionequipos_pac($equipo,$serie){
        //hay una version de esta funcion directamente en ModeloCatalogos->getdireccionequipos2 por si se va a hacer algun ajuste a este
        $resultdir=$this->ModeloCatalogos->db2_getselectwheren('contrato_equipo_direccion',array('equiposrow'=>$equipo,'serieId'=>$serie));
        $iddireccionget='g_0';
        $pro_acc='';
        foreach ($resultdir->result() as $itemdir) {
            if($itemdir->direccion!=''){
                $iddireccionget=$itemdir->direccion;
            }
        }
        //log_message('error','iddireccionget '.$iddireccionget);
        if($iddireccionget!='g_0'){
            $tipodireccion=explode('_',$iddireccionget);

            $tipodir = $tipodireccion[0];
            $iddir   = $tipodireccion[1];
            if ($tipodir=='g') {
                $resultdirc_where['idclientedirecc']=$iddir;
                
                
                $resultdirc=$this->ModeloCatalogos->db2_getselectwheren('clientes_direccion',$resultdirc_where);
                foreach ($resultdirc->result() as $item) {
                    $pro_acc=$item->pro_acc;
                }
            }
            /*
            if ($tipodir=='f') {
                $resultdirc_where['id']=$iddir;
                
                if($fecha>=$this->fechahoy){
                    //log_message('error','entra direccion f');
                    $resultdirc_where['activo']=1;
                }
                $resultdirc=$this->ModeloCatalogos->db2_getselectwheren('cliente_has_datos_fiscales',$resultdirc_where);
                foreach ($resultdirc->result() as $item) {
                    $direccion='Razon social: '.$item->razon_social.' Colonia:'.$item->colonia.' Calle:'.$item->calle.' Numero:'.$item->num_ext;
                }
            }
            */
        }
        return $pro_acc;
    }
    function obtenercontactos(){
        $params = $this->input->post();
        $clienteId=$params['cliente'];
        $resultcontactos=$this->ModeloCatalogos->db2_getselectwheren('cliente_datoscontacto',array('clienteId'=>$clienteId,'activo'=>1));
        $html='';
        foreach ($resultcontactos->result() as $item) {
            $telefono='';
            if($item->telefono!=''){
                $telefono =' Tel: '.$item->telefono;
            }
            $celular='';
            if($item->celular!=''){
                $celular =' Cel: '.$item->celular;
            }
            $email='';
            if($item->email!=''){
                $email =' Email: '.$item->email;
            }
            $html.='<option value="'.$item->atencionpara.' '.$telefono.''.$celular.'" data-idcont="'.$item->datosId.'" data-email="'.$item->email.'">'.$item->atencionpara.''.$telefono.''.$celular.' '.$email.'</option>';
        }
        echo $html;
    }
    function obtenercontactos2(){
        $params = $this->input->post();
        $clienteId=$params['cliente'];
        $resultcontactos=$this->ModeloCatalogos->db2_getselectwheren('cliente_datoscontacto',array('clienteId'=>$clienteId,'activo'=>1));
        $html='';
        foreach ($resultcontactos->result() as $item) {
            $telefono='';
            if($item->telefono!=''){
                $telefono =' Telefono: '.$item->telefono;
            }
            $celular='';
            if($item->celular!=''){
                $celular =' Celular: '.$item->celular;
            }
            $email='';
            if($item->email!=''){
                $email =' Email: '.$item->email;
            }
            $html.='<option value="'.$item->email.'">'.$item->atencionpara.''.$telefono.''.$celular.' '.$email.'</option>';
        }
        echo $html;
    }
    function obtenerbodymail(){
        $params = $this->input->post();
        $clienteId=$params['cliente'];
        $resultcontactos=$this->ModeloCatalogos->db2_getselectwheren('clientes',array('id'=>$clienteId));
        $html='';
        foreach ($resultcontactos->result() as $item) {
            $html = strval($item->bodymail);
        }
        //log_message('error',$html);
        echo $html;
    }
    function editar_e_contacto(){
        $params = $this->input->post();
        $contacto = $params['contacto'];
        $ser = $params['ser'];
        $tipo = $params['tipo'];
        $idcont = $params['idcont'];

        if($tipo==1){
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_contrato_a',array('contacto_text'=>$contacto,'id_contacto_new'=>$idcont),array('asignacionId'=>$ser));
        }
        if($tipo==2){
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_poliza_a',array('contacto_text'=>$contacto,'id_contacto_new'=>$idcont),array('asignacionId'=>$ser));
        }
        if($tipo==3){
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_cliente_a',array('contacto_text'=>$contacto,'id_contacto_new'=>$idcont),array('asignacionId'=>$ser));
        }
        if($tipo==4){
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_venta_a',array('contacto_text'=>$contacto,'id_contacto_new'=>$idcont),array('asignacionId'=>$ser));
        }

        
    }
    function editar_e_equipo_documento(){
        $params = $this->input->post();
        $textinfo = $params['textinfo'];
        $col = $params['col'];
        $ser = $params['ser'];
        $tipo = $params['tipo'];
        if($col==1){
            $arrayinfo=array('equipo_acceso_info'=>$textinfo);
        }else{
            $arrayinfo=array('documentacion_acceso_info'=>$textinfo);
        }

        if($tipo==1){
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_contrato_a',$arrayinfo,array('asignacionId'=>$ser));
        }
        if($tipo==2){
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_poliza_a',$arrayinfo,array('asignacionId'=>$ser));
        }
        if($tipo==3){
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_cliente_a',$arrayinfo,array('asignacionId'=>$ser));
        }
        if($tipo==4){
            $this->ModeloCatalogos->updateCatalogo('asignacion_ser_venta_a',$arrayinfo,array('asignacionId'=>$ser));
        }   
    }
    function infoclientecontrato(){
        $params = $this->input->post();
        $asi=$params['asi'];
        $strq = "SELECT 
                            asigc.contratoId,
                            con.idRenta,
                            ren.atencion,
                            ren.correo,
                            ren.telefono,
                            cli.empresa,
                            con.folio,
                            ren.idCliente 
                            FROM asignacion_ser_contrato_a as asigc 
                            INNER JOIN contrato as con on con.idcontrato=asigc.contratoId 
                            INNER JOIN rentas as ren on ren.id=con.idRenta 
                            INNER JOIN clientes as cli on cli.id=ren.idCliente 
                            WHERE asigc.asignacionId='$asi'";
                
        $query_a = $this->db->query($strq);
        $general=$query_a->row();
        //=====================================================
            $strqc = "SELECT con.idcontrato,con.folio 
                    FROM contrato as con 
                    INNER JOIN rentas as ren on ren.id=con.idRenta 
                    WHERE con.estatus=1 AND ren.idCliente='$general->idCliente'";
                
        $query_ac = $this->db->query($strqc);
        //=====================================================
        //=====================================================
            $strqdc = "SELECT * FROM `cliente_datoscontacto` 
                    WHERE activo=1 AND clienteId='$general->idCliente'";
                
            $query_dc = $this->db->query($strqdc);
        //=====================================================

        $array_datos=array(
                        'general'=>$general,
                        'contratos'=>$query_ac->result(),
                        'contactos'=>$query_dc->result()
                            );

        echo json_encode($array_datos);
                
    }
    function file_multiple(){
        $code=$_POST['code'];
          $data = [];
       
          $count = count($_FILES['files']['name']);
          $output = [];
          for($i=0;$i<$count;$i++){
        
            if(!empty($_FILES['files']['name'][$i])){
        
              $_FILES['file']['name'] = $_FILES['files']['name'][$i];
              $_FILES['file']['type'] = $_FILES['files']['type'][$i];
              $_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
              $_FILES['file']['error'] = $_FILES['files']['error'][$i];
              $_FILES['file']['size'] = $_FILES['files']['size'][$i];
              $DIR_SUC=FCPATH.'uploads/filestemp';

              $config['upload_path'] = $DIR_SUC; 
              //$config['allowed_types'] = 'docx|xlsx|pdf|doc|xls|pptx';
              $config['allowed_types'] = '*';

              //$config['allowed_types'] = 'docx|xlsx|pdf|jpg|jpeg|png|gif';
              $config['max_size'] = 5000;
              $file_names=date('Ymd_His').'_'.rand(0, 500).str_replace(' ', '_', $_FILES['files']['name'][$i]);
              //$config['file_name'] = $file_names.'_'.str_replace(' ', '_', $_FILES['files']['name'][$i]);
              $config['file_name'] = $file_names;
              //$config['file_name'] = $file_names;
       
              $this->load->library('upload',$config); 
        
              if($this->upload->do_upload('file')){
                $uploadData = $this->upload->data();
                $filename = $uploadData['file_name'];
                $filenametype = $uploadData['file_ext'];
       
                $data['totalFiles'][] = $filename;
                //$this->ModeloGeneral->tabla_inserta('equipo_imgs',array('idequipo'=>$idequipo,'imagen'=>$filename,'filetype'=>$filenametype,'usuario'=>$this->idpersonal));
                $this->ModeloCatalogos->Insert('img_temp',array('code'=>$code,'file'=>$filename));
              }else{
                $data = array('error' => $this->upload->display_errors());
                //log_message('error', json_encode($data)); 
              }
            }
       
          }
          echo json_encode($output);
    }
    function upload_data_view(){
        $code = $this->input->post('code');
        $result = $this->ModeloCatalogos->getselectwheren('img_temp',array('code'=>$code));
        echo json_encode($result->result());
    }
    /*
    function agregaraitinerario($asignacion,$tipo){
        $paramsai['unidadid']=0;
        $paramsai['descripcion']='';
        $paramsai['hora_inicio']='';
        $paramsai['hora_inicio']='';
        $paramsai['hora_fin']='';
        $paramsai['km_inicio']='';
        $paramsai['km_fin']='';
        $paramsai['fecha']='';
        $paramsai['personal']='';
        $paramsai['asignacion']=$asignacion;
        $paramsai['tipo_asignacion']=$tipo;
        $fechan=date('Y-m-d');
        $fecha=date('Y-m-d');
        $personalId1 = 0;
        $personalId2 = 0;
        $empresa = '';
        $hora = '';
        if ($tipo==1) {
            $resultado=$this->ModeloAsignacion->getdatosrenta($asignacion,$fechan,0,0);
            foreach ($resultado->result() as $item) {
                $fecha = $item->fecha;
                $personalId1 = $item->personalId1;
                $personalId2 = $item->personalId2;
                $empresa = $item->empresa;
                $hora = $item->hora;
            }
        }
        if ($tipo==2) {
            $resultado=$this->ModeloAsignacion->getdatospoliza($asignacion,0,0);
            foreach ($resultado->result() as $item) {
                $fecha = $item->fecha;
                $personalId1 = $item->personalId1;
                $personalId2 = $item->personalId2;
                $empresa = $item->empresa;
                $hora = $item->hora;
            }

        }
        if ($tipo==3) {
            $resultado=$this->ModeloAsignacion->getdatoscliente($asignacion);
            foreach ($resultado->result() as $item) {
                $fecha = $item->fecha;
                $personalId1 = $item->personalId1;
                $personalId2 = $item->personalId2;
                $empresa = $item->empresa;
                $hora = $item->hora;
            }
        }
        if ($tipo==4) {
            $resultado=$this->ModeloAsignacion->getdatosventas($asignacion);
            foreach ($resultado->result() as $item) {
                $fecha = $item->fecha;
                $personalId1 = $item->personalId;
                $personalId2 = 0;
                $empresa = $item->empresa;
                $hora = $item->hora;
            }
        }
        if($fecha==$fechan){
            if($personalId1>0){
                $resulti1=$this->ModeloCatalogos->get_detalles_itinerario($personalId1,$fecha);
                if($resulti1->num_rows()>0){
                    $paramsai['descripcion']=$empresa;
                    $paramsai['hora_inicio']=$hora;
                    $paramsai['fecha']=$fecha;
                    $paramsai['personal']=$personalId1;
                    $paramsai['asignacion']=$asignacion;
                    $paramsai['tipo_asignacion']=$tipo;
                    $paramsai['asignacion_gen']="|$tipo,$asignacion|";
                    $this->ModeloCatalogos->Insert('unidades_bitacora_itinerario',$paramsai);
                }
            }
            if($personalId2>0){
                $resulti2=$this->ModeloCatalogos->get_detalles_itinerario($personalId2,$fecha);
                if($resulti2->num_rows()>0){
                    $paramsai['descripcion']=$empresa;
                    $paramsai['hora_inicio']=$hora;
                    $paramsai['fecha']=$fecha;
                    $paramsai['personal']=$personalId2;
                    $paramsai['asignacion']=$asignacion;
                    $paramsai['tipo_asignacion']=$tipo;
                    $paramsai['asignacion_gen']="|$tipo,$asignacion|";
                    $this->ModeloCatalogos->Insert('unidades_bitacora_itinerario',$paramsai);
                }
            }
        }
    }
    */
    function obtenerdirecciones(){
        $params = $this->input->post();
        $tipo = $params['tipo'];
        $servicio = $params['servicio'];
        $fecha = $params['fecha'];
        $modelos=array();
        if($tipo==1){//contrato
            $resultado=$this->ModeloAsignacion->getdatosrenta($servicio,$fecha,0,0);
            foreach ($resultado->result() as $item) {
                $direccionequipo= $this->getdireccionequipos($item->id_equipo,$item->serieId,$fecha);
                $modelos[]=array(
                                'modelo'=>$item->modelo,
                                'serie'=>$item->serie,
                                'dir'=>$direccionequipo
                            );
            }
        }
        if($tipo==2){//poliza
            $resultado=$this->ModeloAsignacion->getdatospoliza($servicio,0,0);
            foreach ($resultado->result() as $item) {
                $modelos[]=array(
                                'modelo'=>$item->modelo,
                                'serie'=>$item->serie,
                                'dir'=>$item->direccionservicio
                            );
            }
        }
        if($tipo==3){//cliente
            $resultado=$this->ModeloAsignacion->getdatoscliente($servicio);
            foreach ($resultado->result() as $item) {
                if($item->retiro==1){
                    $modelo_eq=$item->modeloretiro;
                }else{
                    $modelo_eq=$item->modelo;
                }
                
                $modelos[]=array(
                                'modelo'=>$modelo_eq,
                                'serie'=>$item->serie,
                                'dir'=>$item->direccion
                            );
            }
        }
        if($tipo==4){//cliente
            $resultado=$this->ModeloAsignacion->getdatosventas($servicio);
            foreach ($resultado->result() as $item) {
                $ventaIdd = $item->ventaIdd;
                $direccion = $item->direccion;
                $resultadoequipos = $this->ModeloCatalogos->getselectwheren('ventas_has_detallesEquipos',array('idVenta'=>$ventaIdd));
                foreach ($resultadoequipos->result() as $item) { 
                    $model=$this->ModeloCatalogos->obtenernumeroserieequipo($item->idEquipo);
                    $resuleserie=$this->ModeloCatalogos->getequiposerie($item->id);

                    foreach ($resuleserie->result() as $itemse) { 
                        $equipos .='<tr><td>'.$estatuseq.''.$item->modelo.'</td><td>'.$itemse->serie.$nr.'</td><td style="font-size:10px;">'.$direccion.'</td></tr>';
                        $modelos[]=array(
                                'modelo'=>$item->modelo,
                                'serie'=>$itemse->serie,
                                'dir'=>$direccion
                            );
                    }
                    
                }
                $consumiblesventadetalles = $this->ModeloCatalogos->consumiblesventadetalles($ventaIdd);
                foreach ($consumiblesventadetalles->result() as $item) { 
                    
                    $modelos[]=array(
                                'modelo'=>$item->modelo.' Equipo referencia',
                                'serie'=>'',
                                'dir'=>$direccion
                            );
                }


                
            }
        }
        echo json_encode($modelos);
    }
    function clonarservicio(){
        $params = $this->input->post();
        $asignacion = $params['asignacion'];
        $tipo = $params['tipo'];
        if($tipo==1){
            $result_ser_padre = $this->ModeloCatalogos->getselectwheren('asignacion_ser_contrato_a',array('asignacionId'=>$asignacion));
            $result_ser_padre = $result_ser_padre->row();
            $result_ser_padre->activo=1;
            $result_ser_padre->reg=$this->fechahoy;
            $result_ser_padre->comentariocal='';
            $result_ser_padre->creado_personal=$this->idpersonal;
            unset($result_ser_padre->asignacionId);

            $asignacionId_new=$this->ModeloCatalogos->Insert('asignacion_ser_contrato_a',$result_ser_padre);
            
            //$result_ser_hijo= $this->ModeloCatalogos->getselectwheren('asignacion_ser_contrato_a_e',array('asignacionId'=>$asignacion));
            $result_ser_hijo= $this->ModeloCatalogos->get_asignacion_ser_contrato_a_e($asignacion);
            $dataeqarrayinsert=array();
            foreach ($result_ser_hijo->result() as $item) {
                
                $item->asignacionId=$asignacionId_new;
                //=======================================
                    $fecha=$item->fecha;
                    $anio_up = date("Y", strtotime($fecha));
                    $mes_up = date("m", strtotime($fecha));
                    $dia_up = date("d", strtotime($fecha));
                    //==========
                        $fecha = new DateTime();
                        $fecha->modify('last day of this month');
                        $ultimo_dia_del_mes = $fecha->format('d');
                        if($dia_up>$ultimo_dia_del_mes){
                            $dia_up=$ultimo_dia_del_mes;
                        }
                    //==========
                    $fecha_new=$this->anioactual.'-'.$this->messiguiente.'-'.$dia_up;
                    $fecha_new=date("Y-m-d", strtotime($fecha_new));
                //======================================
                $item->fecha =$fecha_new;
                $item->status=0;
                
                $item->cot_status=0;
                $item->confirmado=0;
                $item->editado=0;
                $item->notificarerror_estatus=0;
                $item->priorizar_cot=0;
                $item->nr=0;
                unset($item->asignacionIde);
                unset($item->motivosuspencion);
                unset($item->cot_refaccion);
                unset($item->cot_refaccion2);
                unset($item->cot_refaccion3);
                unset($item->cot_tipo);
                unset($item->cot_status_ob);
                unset($item->cot_referencia);
                unset($item->confirmado_personal);
                unset($item->confirmado_fecha);
                unset($item->t_fecha);
                unset($item->t_horainicio);
                unset($item->t_horafin);
                unset($item->horaserinicio);
                unset($item->horaserfin);
                unset($item->horaserfin);
                unset($item->qrecibio);
                unset($item->comentario);
                unset($item->horaserinicioext);
                unset($item->horaserfinext);
                unset($item->infodelete);
                unset($item->tservicio_a);
                unset($item->tserviciomotivo);
                unset($item->comentario_tec);
                unset($item->contadorini);
                unset($item->contadorfin);
                unset($item->prioridad);
                unset($item->notificarerror);
                unset($item->nr_coment);
                unset($item->nr_coment_tipo);

                $dataeqarrayinsert[]=$item;
            }
            if(count($dataeqarrayinsert)>0){
                $this->ModeloCatalogos->insert_batch('asignacion_ser_contrato_a_e',$dataeqarrayinsert);
            }



        }
    }
    function desv_venta_ser(){
        $params = $this->input->post();
        $id=$params['id'];

        $this->ModeloCatalogos->updateCatalogo('ventas',array('v_ser_tipo'=>null,'v_ser_id'=>null),array('id'=>$id));
        $this->ModeloCatalogos->updateCatalogo('ventacombinada',array('v_ser_tipo'=>null,'v_ser_id'=>null),array('equipo'=>$id));
    }
}
?>
