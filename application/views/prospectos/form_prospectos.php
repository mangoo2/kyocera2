<?php 
// Validamos si es creación o edición, para así cargar los datos respectivamente
if(isset($prospecto)){
    $idProspecto = $prospecto->id;
    $empresa = $prospecto->empresa;
  //  $persona_contacto = $prospecto->persona_contacto;
    //$puesto_contacto = $prospecto->puesto_contacto;
    //$email = $prospecto->email;
    $estado = $prospecto->estado;
    $municipio = $prospecto->municipio;
    $giro = $prospecto->giro;
    $direccion = $prospecto->direccion;
    $observaciones = $prospecto->observaciones;
    $fecha_registro= date("Y-m-d", strtotime($prospecto->reg));
    $aaa = $prospecto->aaa;
    $antiguedad = $prospecto->antiguedad;
}else{
    $idProspecto = 0;
    $empresa = '';
  //  $persona_contacto = '';
    //$puesto_contacto = '';
    //$email = '';
    $estado = 0;
    $municipio = '';
    $giro = '';
    $direccion = '';
    $observaciones = '';
    $fecha_registro=date('Y-m-d');
    $aaa = '';
    $antiguedad = '';
} 

if($tipoVista==1){
  $label = 'Formulario de Alta de Prospecto';
}else if($tipoVista==2){
  $label = 'Formulario de Edición de Prospecto';
}else if($tipoVista==3){
  $label = 'Formulario de Visualización de Prospecto';
}
?>

<!-- START CONTENT --> 
        <section id="content" width="100%">
          

          <!--start container-->
          <div class="container" width="100%">
            <div class="section">
                <h4 class="caption"><?php echo $label; ?></h4>
              <div class="row">
                <input id="base_url" type="hidden" name="base_url" value="<?php echo base_url(); ?>">
                <!-- FORMULARIO -->
                <input id="tipoVista" type="hidden" name="tipoVista" value="<?php echo $tipoVista; ?>">

                  <div class="col s12 m12 24">

                    <div class="card-panel">
                      <div class="row">
                        <div class="input-field col s4">
                          <i class="material-icons prefix">date_range</i>
                          <!-- class="datepicker" -->
                          <input id="fecha_registro" name="fecha_registro" type="date" value="<?php echo $fecha_registro; ?>" disabled>
                          <label for="fecha_registro" class="active">Fecha de Registro</label>
                        </div>
                        <div class="col s4">
                          <div class="switch"><br>
                            <label>AAA
                              <input type="checkbox" name="aaa" id="aaa" <?php if($aaa==1) echo 'checked'?>>
                              <span class="lever"></span>
                            </label>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                         <form class="form" id="prospectos_from" method="post">

                          <?php 
                            if(isset($prospecto)) 
                            {
                          ?>
                            <input id="idProspecto" name="idProspecto" type="hidden" value="<?php echo $idProspecto; ?>">
                          <?php
                            }
                          ?>

                          <div class="row">
                            <div class="input-field col s12">
                              <i class="material-icons prefix">location_city</i>
                              <input id="empresa" name="empresa" type="text" placeholder="Empresa" value="<?php echo $empresa; ?>" onchange="verificarexis()">
                            </div>
                          </div>

                          <div class="row">
                            <div class="input-field col s3">
                              <i class="material-icons prefix">local_library</i>
                              <label for="estado" class="active">Estado</label>
                              <select id="estado" name="estado" class="browser-default chosen-select">
                                  <option value="" disabled selected>Selecciona una opción</option>
                                    <?php foreach($estados as $estado_elegido){ ?>
                                      <option value="<?php echo $estado_elegido->EstadoId?>" <?php if($estado_elegido->EstadoId==$estado) echo "selected";   ?> ><?php echo $estado_elegido->Nombre; ?></option>
                                    <?php }?>
                              </select>
                            </div>

                            <div class="input-field col s3">
                              <i class="material-icons prefix">local_library</i>
                              <input id="municipio" name="municipio"  type="text" value="<?php echo $municipio; ?>">
                              <label for="municipio">Municipio</label>
                            </div>

                            <div class="input-field col s3">
                              <i class="material-icons prefix">local_offer</i>
                              <input id="giro" name="giro"  type="text" value="<?php echo $giro; ?>">
                              <label for="giro">Giro de la Empresa</label>
                            </div>
                            <div class="input-field col s3">
                              <i class="material-icons prefix">local_offer</i>
                              <input id="antiguedad" name="antiguedad"  type="date" value="<?php echo $antiguedad; ?>">
                              <label for="antiguedad" class="active">Antiguedad</label>
                            </div>
                          </div>

                          <div class="row">
                            <div class="input-field col s10">
                              <i class="material-icons prefix">directions</i>
                              <input type="text" id="direcc">
                              <label for="direcc">Dirección de entrega/instalaciones</label>
                            </div>
                            <div class="col s2"><br>
                              <a class="btn-floating waves-effect waves-light cyan" onclick="add_address()"><i class="material-icons prefix">add</i></a>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col s12 tabla_direcc">
                                <table class="responsive-table display tabla_dir" cellspacing="0">
                                  <thead>
                                    <tr>
                                      <th>#</th>
                                      <th>Dirección</th>
                                      <th></th>
                                    </tr>
                                  </thead>
                                  <tbody class="tbody_direccion">
                                      <?php foreach ($cliente_dire as $item) {?>
                                        <tr>
                                          <td><input id="idclientedirecc" type="hidden"  value="<?php echo $item->idclientedirecc ?>" readonly=""> </td>
                                          <td><input type="text" id="direccion" value="<?php echo $item->direccion ?>"></td>
                                          <td><a class="btn-floating red tooltipped" onclick="delete_direcc(<?php echo $item->idclientedirecc ?>)" data-position="top" data-delay="50" data-tooltip="Eliminar">
                                                        <i class="material-icons">delete_forever</i>
                                                      </a></td>
                                        </tr>
                                      <?php } ?>
                                  </tbody>
                                </table>
                            </div>
                          </div>

                          <div class="row">
                            <div class="input-field col s12">
                              <i class="material-icons prefix">question_answer</i>
                              <textarea id="observaciones" name="observaciones" class="materialize-textarea"><?php echo $observaciones; ?></textarea>
                              <label for="message">Observaciones</label>
                            </div>
                          </div>
                          </form>
                          <div class="row">
                            
                            <div class="input-field col s2">
                              <input id="atencionpara" name="atencionpara"  type="text" class="inputdatoscontacto">
                              <label for="atencionpara">Atencion para:</label>
                            </div>
                            <div class="input-field col s2">
                              <input id="puesto_contacto" name="puesto_contacto" type="text" class="inputdatoscontacto">
                              <label for="puesto_contacto">Puesto de contacto</label>
                            </div>
                            <div class="input-field col s2">
                              <input id="email" name="email"  type="email" class="inputdatoscontacto">
                              <label for="email">Correo electrónico</label>
                            </div>
                            <div class="input-field col s2">
                              <input id="tel_local" name="tel_local"  type="text" class="inputdatoscontacto">
                              <label for="tel_local">Telefono</label>
                            </div>
                            <div class="input-field col s2">
                              <input id="celular" name="celular"  type="text" class="inputdatoscontacto">
                              <label for="celular">celular</label>
                            </div>
                            <div class="col s1">
                              <a class="waves-effect waves-light btn-bmz modal-trigger" onclick="agregar_pc()">Agregar</a>
                            </div>
                          </div>

                          <div class="row">
                            <div class="col s12">
                              <table class="table table-striped table-condensed table-hover table-responsive" id="tabledatoscontacto">
                                <thead>
                                    <tr>
                                        <th>Atencion para</th>
                                        <th>Puesto</th>
                                        <th>Email</th>
                                        <th>Telefono</th>
                                        <th>Celular</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody class="tabledatoscontactotb"> 
                                  <?php foreach ($datoscontacto->result() as $item) { ?>
                                      <tr class="rowpc_<?php echo $item->datosId;?>_<?php echo $item->datosId;?>">
                                        <td>
                                          <input type="hidden" id="datosId" value="<?php echo $item->datosId;?>" readonly>
                                          <input type="hidden" id="atencionpara" value="<?php echo $item->atencionpara;?>" readonly>
                                          <?php echo $item->atencionpara;?>
                                        </td>
                                        <td>
                                          <input type="hidden" id="puesto" value="<?php echo $item->puesto;?>" readonly>
                                          <?php echo $item->puesto;?>
                                        </td>
                                        <td>
                                          <input type="hidden" id="email" value="<?php echo $item->email;?>" readonly>
                                          <?php echo $item->email;?>
                                        </td>
                                        <td><input type="hidden" id="telefono" value="<?php echo $item->telefono;?>" readonly>
                                          <?php echo $item->telefono;?>
                                        </td>
                                        <td><input type="hidden" id="celular" value="<?php echo $item->celular;?>" readonly>
                                          <?php echo $item->celular;?>
                                        </td>
                                        <td>
                                          <a class="btn-floating red tooltipped" onclick="Eliminar_pc(<?php echo $item->datosId;?>,<?php echo $item->datosId;?>)" 
                                            data-position="top" data-delay="50" data-tooltip="Eliminar">
                                            <i class="material-icons">delete_forever</i>
                                          </a>
                                        </td>
                                    </tr>
                                  <?php } ?>
                                </tbody>
                              </table>
                            </div>
                          </div> 

                          <div class="row">
                            <div class="input-field col s12">
                              <button class="btn cyan waves-effect waves-light right submitform" >Guardar
                                <i class="material-icons right">send</i>
                              </button>
                            </div>
                          </div>
                        

                        
                      </div>

                    </div>

                  </div>
                
              </div>
            </div>
          </div>
        </section>

<div id="modalclientesexit" class="modal" >
  <div class="modal-content" style="padding: 5px">
    <div class="row">
      <div class="col s12 ">
        <table>
          <thead>
            <tr>
              <th>Empresa</th>
              <th></th>
            </tr>
          </thead>
          <tbody class="listadoclientes">
            
          </tbody>
        </table>
      </div>
    </div>
  </div>  
  <div class="modal-footer">
    <a class="modal-action modal-close waves-effect waves-red gray btn-flat " href="javascript:location.reload()">Cerrar</a>
  </div>
</div>