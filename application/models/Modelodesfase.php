<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class Modelodesfase extends CI_Model {
    public function __construct(){
        parent::__construct();
    }

	function obtener_servicios(){
        $sql = "SELECT 
            reg, 
            idpersonal, 
            GROUP_CONCAT(tipo) AS tipo, 
            tiposer, 
            idser, 
            GROUP_CONCAT(reg ORDER BY reg ASC) AS regs,
            IF(
                LOCATE(',', GROUP_CONCAT(reg)) > 0, 
                CONCAT(
                    LPAD(FLOOR(TIMESTAMPDIFF(MINUTE, 
                        SUBSTRING_INDEX(GROUP_CONCAT(reg ORDER BY reg ASC), ',', 1), 
                        SUBSTRING_INDEX(GROUP_CONCAT(reg ORDER BY reg ASC), ',', -1)
                    ) / 60), 2, '0'), ':', 
                    LPAD(MOD(TIMESTAMPDIFF(MINUTE, 
                        SUBSTRING_INDEX(GROUP_CONCAT(reg ORDER BY reg ASC), ',', 1), 
                        SUBSTRING_INDEX(GROUP_CONCAT(reg ORDER BY reg ASC), ',', -1)
                    ), 60), 2, '0')
                ), 
                NULL
            ) AS diferencia_horas_minutos
        FROM (
            SELECT reg, idpersonal, tipo, tiposer, idser 
            FROM `personal_ubicaciones` 
            WHERE idpersonal='27' 
            AND tipo > 0 
            AND reg BETWEEN '2024-09-10 00:00:00' AND '2024-09-10 23:59:59' 
            GROUP BY tipo, tiposer, idser 
            ORDER BY `personal_ubicaciones`.`idser` DESC
        ) AS dat 
        GROUP BY tiposer, idser";
        $query = $this->db->query($sql);
        return $query->result();
    }

    function obtener_servicios_eje_date($eje,$fecha){
        $sql = "SELECT 
                    reg,
                    idpersonal,
                    GROUP_CONCAT(tipo) as tipo,
                    tiposer,
                    idser,
                    GROUP_CONCAT(DATE_FORMAT(reg, '%H:%i:%s') ORDER BY reg ASC) as regs

                FROM (
                SELECT reg,idpersonal,tipo,tiposer,idser
                FROM `personal_ubicaciones` 
                WHERE idpersonal='$eje' AND tipo>0 AND reg BETWEEN '$fecha 00:00:00' AND '$fecha 23:59:59'  
                GROUP BY tipo,tiposer,idser  
                ORDER BY `personal_ubicaciones`.`tipo` ASC
                ) as dat
                GROUP BY tiposer,idser";
        $query = $this->db->query($sql);
        return $query->result();
    }

    


}