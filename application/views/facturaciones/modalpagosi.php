<style type="text/css">
  .error{color: red;}
  .addcomep_info img{width: 300px;}
</style>
<script type="text/javascript" src="<?php echo base_url(); ?>public/js/pagossi.js?v=<?php echo date('YmdGis');?>" ></script>
<div id="modalpagosi" class="modal" >
  <div class="modal-content" style="padding: 5px">
    <div class="row">
      <div class="col s12">
        <h4>Pendientes de aplicar</h4>
      </div>
    </div>
    <div class="row">
      <div class="col s3">Vendedor:</div>
      <div class="col s7"><?php echo($_SESSION['usuario']); ?></div>
    </div>
    <form id="form_psi">
      <div class="row">
        <div class="col s3"><label>Fecha</label><input type="date" class="form-control-bmz" id="pi_fecha" name="pi_fecha" value="<?php echo date('Y-m-d');?>" required></div>
        <div class="col s3"><label>Monto</label><input type="number" class="form-control-bmz" id="pi_monto" name="pi_monto" min="0" required></div>
        <div class="col s3"><label>Metodo</label> <label for="pi_idmetodo" class="active">Método de pago</label><br> 
            <select id="pi_idmetodo" name="pi_idmetodo" class="browser-default form-control-bmz"> 
               <?php foreach ($metodorow->result() as $itemm) {
                echo '<option value="'.$itemm->id.'">'.$itemm->formapago_text.'</option>';
                 // code...
               }

               ?></select>
        </div>
        <div class="col s3">
          <label>Cliente</label>
          <select id="idclientep" class="browser-default form-control-bmz " name="pi_cliente"></select>
        </div>
      </div>
      <div class="row">
        <div class="col s2" style="display: <?php echo $vendedor; ?>;">
          <label>Ejecutivo asignado</label>
          <select id="pi_personal" name="pi_personal" class="browser-default form-control-bmz" >
            <option value="0">Todos</option>
            <?php foreach ($personalrows->result() as $item) { 
              echo '<option value="'.$item->personalId.'" >'.$item->nombre.' '.$item->apellido_paterno.' '.$item->apellido_materno.'</option>';
             } ?>
          </select>
        </div>
        <div class="col s10"><label>Observaciones</label><textarea class="form-control-bmz" id="pi_obser" name="pi_obser" required style="min-height:50px"></textarea> </div>
      </div>
    </form>
    <div class="row">
      <div class="col s12" style="text-align: end;">
        <a class="waves-effect green btn-bmz" title="Registrar pagos no reconocidos" onclick="r_pago_n_ok()">Registrar</a>
      </div>
    </div>
    <div class="row">
      <div class="col s12">
        <table class="table display">
          <thead><tr><th>Fecha</th><th>Cliente</th><th>Método de pago</th><th>Monto</th><th>Personal asignado</th><th>Comentario</th><th>Comentario<br>ejecutivo</th><th></th></tr></thead>
          <tbody class="table_pni"></tbody>
        </table>
      </div>
    </div>
  </div>  
  <div class="modal-footer">
    <a class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
  </div>
</div>
<div id="modal_icomen" class="modal"  >
    <div class="modal-content ">
      <h5>¿Desea agregar un comentario?</h5>
      <div class="row addiframecomep"></div>
        <div class="modal-footer">
            <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
        </div>
    </div>
</div>
<div id="modal_icomen_info" class="modal"  >
    <div class="modal-content ">
        <h5>comentario</h5>
        <div class="row"><div class="col s12 addcomep_info"></div></div>
        <div class="modal-footer"><a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a></div>
    </div>
</div>