<style type="text/css">
  .btn_group_btn{
    min-width: 100px;
  }
  #tabla_bodega td{
    font-size: 13px;
  }
</style>
<!-- START CONTENT -->
    <section id="content">
      <!--start container-->
        <div class="container">
          <div class="section">

          <input id="base_url" type="hidden" name="base_url" value="<?php echo base_url(); ?>">

          <div id="table-datatables">
            <br>
                
                <div class="row">
                  <div class="col s7">
                      <h4 class="header">Listado de bodegas</h4>
                  </div>
                  <div class="col s1"></div>
                  <div class="col s4">
                      <h4 class="header"><samp class="label">Nueva</samp> bodegas</h4>
                  </div>
                </div>    
                <div class="row">
                  <div class="col s7">
                    <table id="tabla_bodega" class="responsive-table display" cellspacing="0">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Bodega</th>
                          <th>Dirección</th>
                          <th></th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                  </div>
                  <div class="col s1"></div>
                  <div class="col s4">
                    <div class="row">
                         <form class="form" id="form_bodega" method="post">
                            <div class="row">
                               <div class="input-field col s10">
                                <i class="material-icons prefix" title="No. de parte">border_all</i>
                                <input type="hidden" id="bodegaId" name="bodegaId">
                                <label>Bodega</label>
                                <input id="bodega" name="bodega" type="text" placeholder="Nombre" class="form-control-bmz">
                               </div>
                            </div>
                            <div class="row">
                               <div class="input-field col s10">
                                <i class="material-icons prefix" title="Dirección">location_on</i>
                                <label>Dirección</label>
                                <input id="direccion" name="direccion" type="text" placeholder="Dirección" class="form-control-bmz">
                               </div>
                            </div> 
                            <div class="row">
                               <div class=" col s10">
                                <input name="tipov" type="radio" class="tipov" id="asign1" value="1">
                                <label for="asign1">Kyocera</label>
                                <input name="tipov" type="radio" class="tipov" id="asign2" value="2">
                                <label for="asign2">D-impresión</label>
                               </div>
                            </div> 
                            <div class="row">
                              <div class="col s3">
                                <button type="submit" class="btn waves-effect waves-light gradient-45deg-green-teal gradient-shadow"><samp class="btn_n">Nuevo</samp></button>
                              </div>
                              <div class="col s6">
                                <div class="btn_c"></div>
                              </div>
                            </div>
                         </form>
                    </div>     
                  </div>
                </div>
              </div>
            </div>  