<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/jquery-ui-1.12.1.custom/jquery-ui.css">
<script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/jquery-ui-1.12.1.custom/jquery-ui.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/toastr/toastr.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/plugins/toastr/toastr.css">

<script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/fileinput/fileinput.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/plugins/fileinput/fileinput.css">

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/bulma-badge.min.css">
<script type="text/javascript" src="<?php echo base_url(); ?>public/js/servicio.js?v=<?php echo $version ?>"></script>
<script type="text/javascript">
	$(document).ready(function($) {
		$( "#calendarinput" ).datepicker({
      		showOn: "button",
        	buttonText: "<?php echo $fechahoy?>",
        	setDate:"<?php echo $fechahoy?>",
        	dateFormat:"yy-mm-dd",
        	onSelect: function (date) {
        		console.log(date);
        		window.location.href = '<?php echo base_url(); ?>index.php/Configuracionrentas/servicios?fech='+date;
        	}
    	});
    	<?php if($idpersonal ==1 || $perfilid==5 || $perfilid==10 || $perfilid==12){ ?>
    		setInterval(saveubi, 600000);//10 min (600000)
    	<?php } ?>
	});
	/*
	const anchoPantalla = window.innerWidth;
  	const altoPantalla = window.innerHeight;
  	alert(`La resolución de tu pantalla es: ${anchoPantalla}x${altoPantalla} píxeles`);
  	*/
</script>