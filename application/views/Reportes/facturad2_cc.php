<?php
require_once dirname(__FILE__) . '/TCPDF4/examples/tcpdf_include.php'; 
require_once dirname(__FILE__) . '/TCPDF4/tcpdf.php';
$actualizardatos=1;//0 no actualiza 1 actualiza
$GLOBALS["Folio"]=$Folio;
$GLOBALS["nombreclienter"]=$nombreclienter;

$GLOBALS["periodo"]=$periodo;
$GLOBALS["periodoante"]=$periodoante;
if($idcondicionextra>0){
    $condiciones = $this->ModeloCatalogos->getselectwheren('rentas_condiciones',array('id'=>$idcondicionextra));
    foreach ($condiciones->result() as $itemrc) {
      $tiporenta = $itemrc->tipo;
    }
}
//====================================================
  class MYPDF extends TCPDF {
    //Page header
    public function Header() {
        $logos = base_url().'public/img/alta.png';
        $logosmds = base_url().'public/img/MDS.png';
        
          $tamano1='30';//30
          $tamano2='40';//40
          $tamano3='30';//30
        $html = '
            <style type="text/css">
              .info_fac{font-size: 9px;}
              .info_facd{font-size: 8px;}
              .httablelinea{vertical-align: center;border-bottom: 1px solid #9e9e9e;}
              .httableleft{border-left: 1px solid #9e9e9e;}
              .httableright{border-right: 1px solid #9e9e9e;}
              .httabletop{border-top: 1px solid #9e9e9e;}
            </style>
            <table width="100%" border="0" cellpadding="4px" class="info_fac">
              <tr>
                <td rowspan="6" width="25%"><img src="'.$logos.'"></td>
                <td width="50%" valign="top"></td>
                <td rowspan="6" width="25%"><img src="'.$logosmds.'"></td>
              </tr>
              <tr><td align="center" style="font-size: 12px;"><b>'.$GLOBALS["nombreclienter"].'</b></td></tr>
              <tr><td align="center" style="font-size: 12px;"><b>RENTA '.$GLOBALS["periodo"].'.</b></td></tr>
              <tr><td></td></tr>
              <tr><td align="center" style="font-size: 12px;"><b>DETALLE DE PRODUCCION:</b></td></tr>
              <tr><td align="center" style="font-size: 12px;"><b>DEL '.$GLOBALS["periodoante"].'.</b></td></tr>
            </table>';
          $this->writeHTML($html, true, false, true, false, '');
    }
      // Page footer
    public function Footer() {
      $html2='';
      $styleQR = array('border' => 0, 
           'vpadding' => '0', 
           'hpadding' => '0', 
           'fgcolor' => array(0, 0, 0), 
           'bgcolor' => false, 
           'module_width' => 1, 
           'module_height' => 1);
      $html='';

      $html2 .= '<style type="text/css">
                .fontFooter10{font-size: 10px;}
                .fontFooterp{font-size: 9px;margin-top:0px;}
                .fontFooter{font-size: 9px;margin-top:0px;}
                .fontFooterp{font-size: 8px;}
                .fontFooterpt{font-size: 7px;}
                .fontFooterpt6{font-size: 6px;}
                p{margin:0px;}
                .valign{vertical-align:middle;}
                .httablelinea{vertical-align: center;border-bottom: 1px solid #9e9e9e;}
                .footerpage{font-size: 9px;color: #9e9e9e;}
            </style>';

      $html2 .= '<table width="100%" border="0" cellpadding="2" class="fontFooterp"><tr><td align="right" class="footerpage">Pagina '.$this->getAliasNumPage().' de '.$this->getAliasNbPages().'</td></tr></table>';
        $this->writeHTML($html2, true, false, true, false, '');
        
    }
  } 
  $pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

  // set document information
  $pdf->SetCreator(PDF_CREATOR);
  $pdf->SetAuthor('Kyocera');
  $pdf->SetTitle('Folio_'.$GLOBALS["Folio"]);
  $pdf->SetSubject('factura');
  $pdf->SetKeywords('factura');

  // set default header data
  $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

  // set header and footer fonts
  $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
  $pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

  // set default monospaced font
  $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

  // set margins
  $pdf->SetMargins('7', '45', '7');
  $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
  $pdf->SetFooterMargin('20');

  // set auto page breaks
  $pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);

  // set image scale factor
  $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

  $pdf->SetFont('dejavusans', '', 7);

//====================================================
//$pdf->setPrintHeader(false);
//$pdf->setPrintFooter(false);
//$pdf->SetMargins('10', '10', '10');
//$pdf->SetFooterMargin('10');
if ($tiporenta==2) {
  $pdf->AddPage('L', 'A4');
}else{
  $pdf->AddPage('P', 'A4');
}
$tipo=$tipo_cc;
$costoclick_m=0;
      $costoclick_c=0;
      $rowcobrar=0;
      $html='';
      $precio_excedente_mono=0;
      $precio_excedente_color=0;
      $clicks_mono=0;
      $clicks_color=0;
//===============================================================
  //=============================================
        $result_ar=$this->ModeloCatalogos->getselectwheren('alta_rentas',array('idcontrato'=>$idcontrato,'activo'=>1));
        foreach ($result_ar->result() as $item) {
          if($item->monto>0){
            $costoclick_m=$item->monto/$item->clicks_mono;
          }
          if($item->montoc>0){
            $costoclick_c=$item->montoc/$item->clicks_color;
          }
          if($item->precio_c_e_mono>0){
            $precio_excedente_mono=$item->precio_c_e_mono;
          }
          if($item->precio_c_e_color>0){
            $precio_excedente_color=$item->precio_c_e_mono;
          }
          //$clicks_mono=$item->clicks_mono;
          //$clicks_color=$item->clicks_color;
        }
  //======================================================================
        $result_arp=$this->ModeloCatalogos->getselectwheren('alta_rentas_prefactura',array('prefId'=>$idpre,'activo'=>1));
        $idcondicionextra=0;
        foreach ($result_arp->result() as $item) {
          $idcondicionextra=$item->idcondicionextra;
        }
        if($idcondicionextra>0){
          $condiciones = $this->ModeloCatalogos->getselectwheren('rentas_condiciones',array('id'=>$idcondicionextra));
          foreach ($condiciones->result() as $itemrc) {
            if($itemrc->renta_m>0){
              $costoclick_m=$itemrc->renta_m/$itemrc->click_m;
            }
            if($itemrc->renta_c>0){
              $costoclick_c=$itemrc->renta_c/$itemrc->click_c;
            }
            if($itemrc->excedente_m>0){
              $precio_excedente_mono=$itemrc->excedente_m;
            }
            if($itemrc->excedente_c>0){
              $precio_excedente_color=$itemrc->excedente_c;
            }
            //$clicks_mono=$itemrc->click_m;
            //$clicks_color=$itemrc->click_c;
          }
        }
  //======================================================================

//===============================================================
$html=' <style type="text/css">.httable{font-size: 8px;}.styletitles{text-align:center;background-color: #d8d4d4; color: black}</style>
<table border="1"><tr><td width="10%" align="center">Contrato</td><td width="10%" align="center">'.$foliocontrato.'</td><td width="60%" ></td><td  width="10%" align="center"><b>FACTURA</b></td><td width="10%" ></td></tr>  </table>';
if($tipo==0){
  $total_general_cc_general=0;
  foreach ($rowcentros->result() as $item_cc) {
      $idccs=$item_cc->idcentroc;
      //============================================
          $result_cc=$this->ModeloCatalogos->getselectwheren('centro_costos',array('id'=>$idccs));
          $clicks_mono=0;
          $clicks_color=0;
          $mono_renta_cc=0;
          $color_renta_cc=0;
          foreach ($result_cc->result() as $item) {
            $descripcion=$item->descripcion;
            $clicks_mono=$item->clicks;
            $clicks_color=$item->clicks_c;
            if($clicks_mono>0){
              $mono_renta_cc=$clicks_mono*$costoclick_m;
            }
            if($clicks_color>0){
              $color_renta_cc=$clicks_color*$costoclick_c;
            }
          }
      //============================================


    $html.='<table border="1"  cellpadding="3"><tr><td class="styletitles" colspan="3"><b>'.$item_cc->descripcion.'</b></td></tr><tr><td colspan="3">';
      $total_general_cc=0;
      //===========================================
          if($tipo==0){//0 detallado 1 monto global
            
               $html_title=$this->Rentas_model->titlehtmlformatocc(1,'');
            if($clicks_mono>0){
              $result_cceq=$this->ModeloCatalogos->result_cceq($idccs,$idpre,1);
              $num_equipos=$result_cceq->num_rows();
              if($num_equipos>0){
                $monto_x_equipo=round($mono_renta_cc/$num_equipos,3);
                  $html.=$html_title;
                foreach ($result_cceq->result() as $item) {
                  $pro_imp=$item->c_c_f-$item->c_c_i;
                  $pro_es=$item->e_c_f-$item->e_c_i;
                  
                  $produccion_total_eq=$pro_imp+$pro_es;
                  $html.='<tr>';
                          $html.='<td>'.$item->modelo.'</td>';
                          $html.='<td>'.$item->serie.'</td>';
                          $html.='<td>'.$item->c_c_i.'</td>';
                          $html.='<td>'.$item->c_c_f.'</td>';
                          $html.='<td>'.$pro_imp.'</td>';
                          $html.='<td>'.$item->e_c_i.'</td>';
                          $html.='<td>'.$item->e_c_f.'</td>';
                          $html.='<td>'.$pro_es.'</td>';
                          $html.='<td>'.$produccion_total_eq.'</td>';
                          $html.='<td>$'.$monto_x_equipo.'</td>';
                        $html.='</tr>';
                        $total_general_cc=$total_general_cc+$monto_x_equipo;

                          
                
                        //$rowcobrar++;
                }
                $html.='</table>';
                $html.='<table><tr><td></td></tr></table>';
                
              }
            }else{
              $result_cceq=$this->ModeloCatalogos->result_cceq($idccs,$idpre,1);
              $num_equipos=$result_cceq->num_rows();
              //log_message('error','$num_equipos:'.$num_equipos);
              if($num_equipos>0){
                //$monto_x_equipo=round($mono_renta_cc/$num_equipos,3);
                /* 
                  echo round( 1.55, 1, PHP_ROUND_HALF_UP);   //  1.6
                  echo round( 1.54, 1, PHP_ROUND_HALF_UP);   //  1.5
                */
                  $html_title=$this->Rentas_model->titlehtmlformatocc(1,'');
                  $html.=$html_title;
                foreach ($result_cceq->result() as $item) {
                  $monto_x_equipo=round($item->produccion_total*$precio_excedente_mono,3);
                  if($monto_x_equipo>0 and $clicks_mono==0){
                  $pro_imp=$item->c_c_f-$item->c_c_i;
                  $pro_es=$item->e_c_f-$item->e_c_i;
                  //$html.=$html_title;
                  $produccion_total_eq=$pro_imp+$pro_es;
                  $html.='<tr>
                            <td>'.$item->modelo.'</td>
                            <td>'.$item->serie.'</td>
                            <td>'.$item->c_c_i.'</td>
                            <td>'.$item->c_c_f.'</td>
                            <td>'.$pro_imp.'</td>
                            <td>'.$item->e_c_i.'</td>
                            <td>'.$item->e_c_f.'</td>
                            <td>'.$pro_es.'</td>
                            <td>'.$produccion_total_eq.'</td>
                            <td>$'.$monto_x_equipo.'</td>
                          </tr>';
                          $total_general_cc=$total_general_cc+$monto_x_equipo;
                        //$rowcobrar++;
                    }
                }
                $html.='</table>';
                $html.='<table><tr><td></td></tr></table>';
              }
            }

            if($clicks_color>0){
              $result_cceq=$this->ModeloCatalogos->result_cceq($idccs,$idpre,2);
              $num_equipos=$result_cceq->num_rows();
              
              if($num_equipos>0){
                $monto_x_equipo=round($color_renta_cc/$num_equipos,3);
                /* 
                  echo round( 1.55, 1, PHP_ROUND_HALF_UP);   //  1.6
                  echo round( 1.54, 1, PHP_ROUND_HALF_UP);   //  1.5
                */
                  $html_title=$this->Rentas_model->titlehtmlformatocc(2,3);
              $html.=$html_title;
                foreach ($result_cceq->result() as $item) {
                  $pro_imp=$item->c_c_f-$item->c_c_i;
                  $pro_es=$item->e_c_f-$item->e_c_i;
                  $html.=$html_title;
                  $produccion_total_eq=$pro_imp+$pro_es;
                  $html.='<tr>
                            <td>'.$item->modelo.'</td>
                            <td>'.$item->serie.'</td>
                            <td>'.$item->c_c_i.'</td>
                            <td>'.$item->c_c_f.'</td>
                            <td>'.$pro_imp.'</td>
                            <td>'.$item->e_c_i.'</td>
                            <td>'.$item->e_c_f.'</td>
                            <td>'.$pro_es.'</td>
                            <td>'.$produccion_total_eq.'</td>
                            <td>$'.$monto_x_equipo.'</td>
                          </tr>';
                          $total_general_cc=$total_general_cc+$monto_x_equipo;

                        
                        //$rowcobrar++;
                }
                $html.='</table>';
                $html.='<table><tr><td></td></tr></table>';
              }
              
            }else{
              $result_cceq=$this->ModeloCatalogos->result_cceq($idccs,$idpre,2);
              $num_equipos=$result_cceq->num_rows();
              if($num_equipos>0 and $clicks_color==0){
                //$monto_x_equipo=round($color_renta_cc/$num_equipos,3);
                /* 
                  echo round( 1.55, 1, PHP_ROUND_HALF_UP);   //  1.6
                  echo round( 1.54, 1, PHP_ROUND_HALF_UP);   //  1.5
                */
                if($result_cceq->num_rows()>0) {
                  $row_montoxeq=0;
                  foreach ($result_cceq->result() as $item) {
                      $monto_x_equipo=round($item->produccion_total*$precio_excedente_color,3);
                      if($monto_x_equipo>0){
                        $row_montoxeq++;
                      }
                  }
                  if($row_montoxeq>0){
                    $html_title=$this->Rentas_model->titlehtmlformatocc(2,'');
                
                    $html.=$html_title;
                      foreach ($result_cceq->result() as $item) {
                        $monto_x_equipo=round($item->produccion_total*$precio_excedente_color,3);
                        if($monto_x_equipo>0){
                          $pro_imp=$item->c_c_f-$item->c_c_i;
                          $pro_es=$item->e_c_f-$item->e_c_i;
                          $html.=$html_title;
                          $produccion_total_eq=$pro_imp+$pro_es;
                          $html.='<tr>
                                  <td>'.$item->modelo.'</td>
                                  <td>'.$item->serie.'</td>
                                  <td>'.$item->c_c_i.'</td>
                                  <td>'.$item->c_c_f.'</td>
                                  <td>'.$pro_imp.'</td>
                                  <td>'.$item->e_c_i.'</td>
                                  <td>'.$item->e_c_f.'</td>
                                  <td>'.$pro_es.'</td>
                                  <td>'.$produccion_total_eq.'</td>
                                  <td>$'.$monto_x_equipo.'</td>
                                </tr>';
                                $total_general_cc=$total_general_cc+$monto_x_equipo;
                                
                              //$rowcobrar++;
                        }
                      }
                      $html.='</table>';
                      $html.='<table><tr><td></td></tr></table>';
                  }
                }
              }
            }
          }
          $total_general_cc_general=$total_general_cc_general+$total_general_cc;
    $html.='</td></tr><tr><td width="80%"></td><td width="10%">Total</td><td width="10%">$ '.number_format($total_general_cc,2,'.',',').'</td></tr></table><table><tr><td></td></tr></table>';
  }
    $html.='<table border="1" cellpadding="3"><tr><td width="80%"></td><td width="10%"><b>Total General</b></td><td width="10%">$ '.number_format($total_general_cc_general,2,'.',',').'</td></tr></table>';
}
if($tipo==1){//0 detallado 1 monto global
  $html_title='<table border="1" align="center" cellpadding="2">
                <thead> 
                  <tr valign="middle" class="styletitles">
                    <th class="httable"><b>Centro de costos</b></th>
                    <th class="httable"><b>Periodo</b></th>
                    <th class="httable"><b>Producción</b></th>
                    <th class="httable"><b>Monto</b></th>
                    <th class="httable"><b>Iva</b></th>
                    <th class="httable"><b>Total</b></th>
                  </tr>
                </thead>';
    $html.=$html_title;
  foreach ($rowcentros->result() as $item_cc) {
      $idccs=$item_cc->idcentroc;
      //======================================
        //=============================================
          $result_cc=$this->ModeloCatalogos->getselectwheren('centro_costos',array('id'=>$idccs));
          $clicks_mono=0;
          $clicks_color=0;
          $mono_renta_cc=0;
          $color_renta_cc=0;
          foreach ($result_cc->result() as $item) {
            $descripcion=$item->descripcion;
            $clicks_mono=$item->clicks;
            $clicks_color=$item->clicks_c;
            if($clicks_mono>0){
              $mono_renta_cc=$clicks_mono*$costoclick_m;
            }
            if($clicks_color>0){
              $color_renta_cc=$clicks_color*$costoclick_c;
            }
          }
        //=============================================
      //======================================
          $r_produccion=$this->ModeloCatalogos->obtenerproducciontotalcentrocostos($idpre,$idccs);
          if($mono_renta_cc>0){
            $iva=round($mono_renta_cc*0.16,2);
            $total=round($mono_renta_cc+$iva,2);
            $html.='<tr>
                      <td>'.$item_cc->descripcion.'</td>
                      <td>'.$periodo.'</td>
                      <td>'.$r_produccion.'</td>
                      <td>$ '.number_format($mono_renta_cc, 2, '.', ',').'</td>
                      <td>$ '.number_format($iva, 2, '.', ',').'</td>
                      <td>$ '.number_format($total, 2, '.', ',').'</td>
                    </tr>';
            
            
          }
          if($color_renta_cc>0){
              $iva=round($color_renta_cc*0.16,2);
              $total=round($color_renta_cc+$iva,2);
              $html.='<tr>
                        <td>'.$item_cc->descripcion.'</td>
                        <td>'.$periodo.'</td>
                        <td>'.$r_produccion.'</td>
                        <td>$ '.number_format($color_renta_cc, 2, '.', ',').'</td>
                        <td>$ '.number_format($iva, 2, '.', ',').'</td>
                        <td>$ '.number_format($total, 2, '.', ',').'</td>
                      </tr>';
             
          }
          if($precio_excedente_mono>0 and $clicks_mono==0){
            $result_cceq=$this->ModeloCatalogos->result_cceq($idccs,$idpre,1);
            $num_equipos=$result_cceq->num_rows();
            
            $produccion_total=0;
            if($num_equipos>0){
              //$monto_x_equipo=round($mono_renta_cc/$num_equipos,3);
              /* 
                echo round( 1.55, 1, PHP_ROUND_HALF_UP);   //  1.6
                echo round( 1.54, 1, PHP_ROUND_HALF_UP);   //  1.5
              */
                
              foreach ($result_cceq->result() as $item) {
                $produccion_total=$produccion_total+$item->produccion_total;
              }
            }
            if($produccion_total>0){
              $mono_renta_cc=$produccion_total*$precio_excedente_mono;
              
              $iva=round($mono_renta_cc*0.16,2);
              $total=round($mono_renta_cc+$iva,2);

              $html.='<tr>
                        <td>'.$item_cc->descripcion.'</td>
                        <td>'.$periodo.'</td>
                        <td>'.$r_produccion.'</td>
                        <td>$ '.number_format($mono_renta_cc, 2, '.', ',').'</td>
                        <td>$ '.number_format($iva, 2, '.', ',').'</td>
                        <td>$ '.number_format($total, 2, '.', ',').'</td>
                      </tr>';

              
            }
          }
          if($precio_excedente_color>0 and $clicks_color==0){
            $result_cceq=$this->ModeloCatalogos->result_cceq($idccs,$idpre,2);
            $num_equipos=$result_cceq->num_rows();
            $produccion_total=0;
            if($num_equipos>0){
              //$monto_x_equipo=round($color_renta_cc/$num_equipos,3);
              /* 
                echo round( 1.55, 1, PHP_ROUND_HALF_UP);   //  1.6
                echo round( 1.54, 1, PHP_ROUND_HALF_UP);   //  1.5
              */
              foreach ($result_cceq->result() as $item) {
                $produccion_total=$produccion_total+$item->produccion_total;
              }
              if($produccion_total>0){
                $color_renta_cc=$produccion_total*$precio_excedente_color;

                $iva=round($color_renta_cc*0.16,2);
                $total=round($color_renta_cc+$iva,2);

              $html.='<tr>
                        <td>'.$item_cc->descripcion.'</td>
                        <td>'.$periodo.'</td>
                        <td>$ '.number_format($color_renta_cc, 2, '.', ',').'</td>
                        <td>$ '.number_format($iva, 2, '.', ',').'</td>
                        <td>$ '.number_format($total, 2, '.', ',').'</td>
                      </tr>';


                
              }
            }
          }
  }
  $html.='</table>';
}


  
//log_message('error',$html); 
$pdf->writeHTML($html, true, false, true, false, '');
//=======================================================================

if($gel_fol==1){
  $html='<br><br><br><br>
          <table border="1" align="center" cellpadding="2" class="styletitles">
            <tr valign="middle">
              <th class="httable" style="font-size:12px"><b>TONER</b></th>
            </tr>
          </table>';
  $html.='<table border="1" align="center" cellpadding="2">
          <thead> 
            <tr valign="middle" style="background-color: #d88888; color: black">
              <th class="httable" ><b>TONER</b></th>
              <th class="httable" ><b>FOLIO</b></th>
              <th class="httable"  ><b>ESTATUS</b></th>
            </tr>
          </thead>
        ';
  foreach ($detalleconsumiblesfolios->result() as $item) {
    if ($item->status==0) {
      $status='';
    }elseif ($item->status==1) {
      $status='stock Cliente';
    }else{
      $status='Retorno';
    }
      $html.='<tr valign="middle"><td class="httable">'.$item->modelo.'</td><td class="httable">'.$item->foliotext.'</td><td class="httable">'.$status.'</td></tr>';  
  }
 $html.='</table>';
 $pdf->writeHTML($html, true, false, true, false, '');
}

  foreach ($detallecimages->result() as $item) {
    $pdf->AddPage('P', 'A4');
    if($item->tipoimg==0){
      $file = fopen(base_url()."uploads/rentas_hojasestado/".$item->nombre, "r");
      $line = fgets($file);
      $html= $line;
      fclose($file);
    }else{
      $html=base_url()."uploads/rentas_hojasestado/".$item->nombre;
    }
    $pdf->Image($html, 'C', 0, 250, 297, '', '', '', true, 200, 'C', false, false, 0, false, false, true);
    //else  
    //log_message('error', base64_decode(base_url()."uploads/rentas_hojaestado/".$line));
    //log_message('error', '<th class="httable"><img src="'.base64_decode(base_url()."uploads/rentas_hojasestado/".$line).'"></th>');
  }//for
//=======================================================================================================================================================
  $facturas=$this->ModeloCatalogos->viewprefacturaslis_general($idcontrato);
  $GLOBALS['facturasvencidas']=0;
  $logos = base_url().'public/img/alta.png';
  $logosmds = base_url().'public/img/MDS.png';
  $htmlf = '<table width="100%" border="0" cellpadding="4px" class="info_fac">
              <tr>
                <td rowspan="6" width="25%"><img src="'.$logos.'"></td>
                <td width="50%" valign="top"></td>
                <td rowspan="6" width="25%"><img src="'.$logosmds.'"></td>
              </tr></table><p></p>
              <p></p>
                <style type="text/css">
                    .stileback{background-color:grey;color: white;font-weight: bold;}
                    .stileback2{background-color:#e0e0e0;font-weight: bold;}
                    .bodertable{border: #0078d4 5px solid;}
                    .table td{font-size:10px;}
                </style>
                <table cellpadding="5" border="1" class="table bodertable" width="100%" align="center">
                    <thead>
                        <tr>
                          <td colspan="4" class="stileback">Facturas pendientes</td>
                        </tr>
                        <tr>
                        <td width="9%" class="stileback bodertable">Folio Factura</td>
                        <td width="10%" class="stileback">Fecha Factura</td>
                        <td width="61%" class="stileback">Concepto</td>
                        <td width="10%" class="stileback">Aplicación de pago</td>
                        <td width="10%" class="stileback">Monto Factura</td>
                        <!--<td width="11%" class="stileback">Monto Pagado</td>
                        <td width="11%" class="stileback">Saldo</td>-->
                      </tr></thead>';
            $montovencido=0;
            foreach ($facturas->result() as $item) {
                $descripcion=$this->ModeloCatalogos->descripcionfactura($item->FacturasId);
                if ($item->fechamax==null) {
                    $fechapago='Pendiente';
                }else{
                    $fechapago=$item->fechamax;
                }
                if( floatval($item->pagot)>0){
                    $pagot='$ '.number_format($item->pagot,2,'.',',');
                    $pagot_num=round($item->pagot,2);
                }else{
                    $pagot='';
                    
                    $pagot_num=0;
                }
                $totalf=$item->total-$pagot_num;

                if($totalf>0){
                    $infocomplemento=$this->ModeloCatalogos->infocomplementofactura($item->FacturasId);
                    $pagot_num=$infocomplemento[0];
                    if($infocomplemento[0]>0){
                        $fechapago=date('Y-m-d',strtotime($infocomplemento[1]));
                        $pagot='$ '.number_format($pagot_num,2,'.',',');
                        $totalf=$item->total-$pagot_num;
                    }    
                }
                
                
                $montovencido=$montovencido+$totalf;
                if($totalf>0){
                    $GLOBALS['facturasvencidas']=$GLOBALS['facturasvencidas']+1;
                }
                if($totalf>0){
                $htmlf .= '<tr nobr="true"><td width="9%">U'.$item->Folio.'</td><td width="10%">'.$item->fechatimbre.'</td><td width="61%">'.$descripcion.'</td><td width="10%">'.$fechapago.'</td><td width="10%">$ '.number_format($item->total,2,'.',',').'</td>
                        <!--<td width="11%">'.$pagot.'</td><td width="11%">$ '.number_format($totalf,2,'.',',').'</td>-->
                      </tr>';
                }
            }


            

        $htmlf .= '</table>';
        if($GLOBALS['facturasvencidas']>0){
          $pdf->setPrintHeader(false);
          $pdf->SetMargins(7, '10', 7);
          $pdf->AddPage('P', 'A4');
          $html=$htmlf;

          $pdf->writeHTML($html, true, false, true, false, '');
        }
//========================================================================================================================================================

//=======================================================================
$pdf->Output('Folios_.pdf', 'I');
//$pdf->Output('../../facturas/'.$GLOBALS["Folio"].'.pdf', 'F');
//$pdf->Output('/facturas/'.$GLOBALS["Folio"].'.pdf', 'F');
?>