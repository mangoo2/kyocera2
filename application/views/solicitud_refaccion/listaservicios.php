<style type="text/css">
  .chosen-container{    margin-left: 0px;  }
  td{    font-size: 11px;  }
  #tabla_asignar td{    padding: 5px 5px;  }
  .tiporefaccion{    font-weight: bold;  }
  .tiporefaccion span{    color: #6a0101;  }
  .text-red{    color: red;    font-weight: bold;  }
  .text-green{    color:green;  }
  .btn_edit_i{
    float: right;
    padding-top: 3.625px !important;
    padding-bottom: 3.625px !important;
    padding-left: 9.25px !important;
    padding-right: 9.25px !important;
    font-size: 11px;
  }
  .optionesservicios .select2-container{width: 100% !important;  }
  .colordeiconofinalizado{color: #4CAF50;  }
  .tbody_info_eq textarea{min-height: 73px;font-size: 12px !important;  }
  .btn_c_font{font-size: 11px;    width: 49px;float: inline-end;  }
  .fc-bmz{
    background-color: white !important;
  }
  .ct_c{
    max-width: 100px;
  }
  .e_ser{
    margin-top: 1px;
  }
  .header{
    margin: 0 0 0 0 !important;
  }
</style>
<input id="perfilid" type="hidden" value="<?php echo $perfilid;?>">
<input id="contadorRefacciones" type="hidden" name="contadorRefacciones" value="1">
<!-- START CONTENT -->
<section id="content">
  <!--start container-->
  <div class="container">
    <div class="section">
      <div class="card-panel">
      		<div id="table-datatables">
            <h4 class="header">Solicitud de refacciones</h4>
            <div class="row">
              <div class="col s4 m2 l2">
                <label>Servicio</label>
                <select  id="serv" class="browser-default form-control-bmz">
                  <option value="0">Todo</option>
                  <option value="1">Contrato</option>
                  <option value="2">Poliza</option>
                  <option value="3">Evento</option>
                  <option value="4">Venta</option>
                </select>
              </div>
              <div class="col s4 m2 l2">
                <label>Tipo de Servicio</label>
                <select  id="tiposer" class="browser-default form-control-bmz">
                  <option value="0">Todos</option>
                  <option value="1">Regular</option>
                  <option value="2">Asignación diaria</option>
                </select>
              </div>
              <div class="col s4 m2 l2">
                <label>Técnico/Especialista</label>
                <select id="tecnico" class="browser-default form-control-bmz chosen-select">
                  <option value="0">Todos</option>
                  <?php 
                    foreach ($resultstec as $item) { ?>
                      <option value="<?php echo $item->personalId;?>"><?php echo $item->tecnico;?></option>
                  <?php } ?>
                </select>
              </div>
              <div class="col s4 m2 l2">
                <label>Finalizados/activos</label>
                <select id="status" class="browser-default form-control-bmz" >
                  <option value="0">Todos</option>
                  <option value="1">Activos</option>
                  <option value="2">Finalizados</option>
                </select>
              </div>
              <div class="col s4 m2 l2">
                <label>Zona</label>
                <select id="zona" class="browser-default  form-control-bmz" >
                  <option value="0">Todos</option>
                    <?php foreach ($resultzonas as $item) { ?>
                      <option value="<?php echo $item->id;?>"><?php echo $item->nombre;?></option>
                    <?php  } ?>
                </select>
              </div>
              <div class="col s4 m2 l2">
                <label>Cliente</label>
                <select id="pcliente" class="browser-default" onchange="load()">
                  <option value="0">Todos</option>
                </select>
              </div>
            </div>
            <div class="row">
              <div class="col s4 m2 l2">
                <label>Fecha inicial</label>
                <input type="date" id="fechainicial" class="form-control-bmz" value="<?php 
                  $fechameses=date('Y-m-d H:i:s');
                echo date("Y-m-d",strtotime($fechameses."- 10 month"));?>" >
              </div>
              <div class="col s4 m2 l2">
                <label>Fecha final</label>
                <input type="date" id="fechafinal"  class="form-control-bmz">
              </div>
              <div class="col s4 m1 l1">
                  <button class="b-btn b-btn-primary" onclick="load()" title="Realizar consulta" style="margin-top: 20px;"><i class="fas fa-filter"></i> <span class="btn-filtrar"></span></button>
              </div>
            </div>
            <div class="row">
              <div class="col s12" style="text-align: end;">
                <button class="b-btn b-btn-primary" onclick="agregareditarser()" title="Realizar consulta" style="margin-top: 20px;">Agregar / Editar cotización</button>
              </div>
            </div>
            
            <div class="row">
              <br>
            </div>
            <div class="row">
              <div class="col s12">
                <table id="tabla_asignar" class="table display" cellspacing="0">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th></th>
                      <th>Técnico</th>
                      <th>Cliente</th>
                      <!--<th>Contacto</th>-->
                      <th>Fecha</th>
                      <!--<th>Hora</th>-->
                      <!--<th>Zona</th>-->
                      <th>Equipo</th>
                      <th>Serie</th>
                      <th>Refacción</th>
                      <!--<th>Tipo</th>-->
                      <th>Status</th>
                      <th>Priorizar</th>
                      <th>Acciones</th>
                    </tr>
                  </thead>
                  <tbody>
                    <!------------------------------------->
                  
                    <!------------------------------------->
                  </tbody>
                </table>
              </div>
            </div>
          </div>
      </div>
    </div>  
  </div>
</section>
<input type="hidden" id="idrow">
<input type="hidden" id="idtable"><!--1 contrato 2 pencion 3 asignacion dia 4 venta-->
<input type="hidden" id="asignacionId">

<!--<div id="modalcotizacion" class="modal" style="width:75%">
    <div class="modal-content ">
      <h4> <i class="material-icons">settings_applications</i>Cotizacion</h4>
      <hr>
      <div class="col s12 m12 l6">
        
          <form id="formulario_cotizacion">
                        <div class="row">
                           <div class="input-field col s6">
                              <i class="material-icons prefix">account_box</i>
                              <label for="cliente" class="active">Cliente / Prospecto</label>
                              <input id="cliente" name="cliente"  type="text" value="" placeholder="cliente" readonly>
                              
                            </div>

                            <div class="input-field col s6">
                              <i class="material-icons prefix">account_box</i>
                              <input id="atencion" name="atencion"  type="text" value="" placeholder="atencion">
                              <label for="atencion" class="active">Atención para:</label>
                            </div>
                        </div> 

                        <div class="row">
                           <div class="input-field col s6">
                              <i class="material-icons prefix">mail_outline</i>
                              <input id="correo" name="correo" value="" type="text" placeholder="Correo" >
                              <label for="cliente" class="active">Correo</label>
                            </div>

                            <div class="input-field col s6">
                              <i class="material-icons prefix">call</i>
                              <input id="telefono" name="telefono" value=""  type="text" placeholder="telefono">
                              <label for="telefono" class="active">Telefono:</label>
                            </div>
                        </div>  
                        
                        <input type="hidden" name="eleccionCotizar" id="eleccionCotizar" value="3">
                        <div class="row">
                          <div class="col s6" id="opcionesConsumiblesRefacciones" style="display: none;">
                            <div class="margenTopDivChosen" >
                              <label for="tipo" class="active">Tipo de Cotización</label>
                              <select id="tipo" name="tipo" class="browser-default chosen-select tipoConsumibles">
                                  <option value="1">Venta</option>
                                  
                              </select>
                            </div>
                          </div>
                          <div class="input-field col s6">
                            <div class="margenTopDivChosen" >
                              <label for="propuesta" class="active">¿Que tipo?</label>
                              <select id="propuesta" name="propuesta" class="browser-default  chosen-select" >
                                  <option value="0">Cotizacion</option>
                                  <option value="1">Propuesta</option>
                              </select>
                            </div>
                          </div>


                          
                          
                        </div>
                        

                      <div class="row">
                        <div class="input-field col s6" id="tipoCotizacion" style="display: none;">
                          <div class="col s5" style="margin-left: -12px;">
                            <i class="material-icons prefix">format_list_bulleted</i>
                            <label for="estado">Cotización</label>
                          </div>
                          <div class="col s7 margenTopDivChosen" >
                            <select id="cotizar" name="cotizar" onchange="selectcotiza()" class="browser-default chosen-select">
                                <option value="" disabled selected>Forma de cotización</option>
                                <option value="1">Manual</option>
                            </select>
                          </div>
                        </div>
                        <div class="input-field col s6 select_familia" style="display: none;" id="select_familia">
                          <div class="col s5" style="margin-left: -12px;">   
                            <i class="material-icons prefix">person</i>
                            <label for="estado">Familiar</label>
                          </div>
                          <div class="col s7 margenTopDivChosen" >
                            <select id="idfamilia" name="idfamilia" class="browser-default chosen-select" onchange="verificaFamilia(this)">
                               <option value="" disabled selected>Selecciona una opción</option>
                                 
                            </select>
                          </div>   
                        </div>
                        <div class="input-field col s6 tiporentaselect" style="display: none">
                              <input type="hidden" name="tiporenta" id="tiporenta" value="0">
                        </div>
                        
                          

                         
                           
                      </div>

                      <div class="row">
                        <div class="input-field col s6" id="selectEquipos" style="display: none;">
                          <div class="col s5" style="margin-left: -12px;">   
                            <i class="material-icons prefix">format_list_bulleted</i>
                            <label for="estado">Equipos</label>
                          </div>
                          <div class="col s7 margenTopDivChosen" >
                            <select id="equipoLista" name="equipoLista" class="browser-default chosen-select" >
                                <option value="" disabled selected></option>
                                <?php foreach($equipos as $equipo){ ?>
                                    <option value="<?php echo $equipo->id;?>" >
                                        <?php echo $equipo->modelo;?>
                                    </option>
                                <?php } ?>
                            </select>
                          </div>
                        </div>
                              
                      </div>
                      <br>
                      
                     
                      <div class="row rowRefacciones" id="rowRefacciones1" name="rowRefacciones1">              
                        <div class="input-field col s3"  id="selectEquiposRefacciones">
                            <div class="col s12 margenTopDivChosen" > 
                              <label for="equipoListaRefacciones" class="active">Equipos</label>
                              <select id="equipoListaRefacciones" name="equipoListaRefacciones" class="browser-default chosen-select equipoListaRefacciones" onchange="funcionRefacciones($(this))">
                                  <option value="" disabled selected></option>
                                  <?php foreach($equipos as $equipo){?>
                                      <option value="<?php echo $equipo->id;?>" >
                                          <?php echo $equipo->modelo;?>
                                      </option>
                                  <?php } ?>
                              </select>
                            </div>
                        </div>
                        <div class="input-field col s2 margenTopDivChosen">
                              <input type="text" name="seriee" id="seriee" class="form-control-bmz seriee" placeholder="Serie" style="margin-top: 13px;">
                        </div>
                                      
                        <div class="input-field col s2" id="selectRefaccion">
                          <div class="col s12 margenTopDivChosen" >
                            <label for="refaccion" class="active">Disponibles</label>
                            <select id="refaccion" name="refaccion" class="browser-default chosen-select refaccion" onchange="selectprecior($(this))">
                              <option value="" disabled selected>Selecciona una opción</option>
                            </select>
                          </div> 
                        </div>
                        <div class="col s2">  
                            <label for="estado">Precios</label> 
                           
                            <div class="refaccionesp_mostrar"></div>
                        </div> 
                        <div class="input-field col s3" id="inputCantidadRefaccion">
                          <div class="col s8" style="margin-left: -12px;">  
                              <input type="number" name="piezasRefacion" min="1" max="5" class="piezasRefacion" id="piezasRefacion">
                            <label for="piezasRefacion" class="active">Cantidad</label>
                          </div>
                        

                          <div class="col s2" >
                            <a class="btn-floating waves-effect waves-light purple lightrn-1 btn tooltipped" data-position="top" data-tooltip="Nuevo" id="botonAgregarRefaccion" name="botonAgregarRefaccion">
                              <i class="material-icons">add</i>
                            </a>
                          </div>  
                          <div class="col s2" >
                            <a class="btn-floating waves-effect waves-light red lightrn-1 btn tooltipped botonEliminarRefaccion" data-position="top" data-tooltip="Eliminar" id="botonEliminarRefaccion" name="botonEliminarRefaccion">
                              <i class="material-icons">delete</i>
                            </a>
                          </div>

                          </div>
                      </div>

                      
                      
                      
                      <input type="hidden" name="idCliente" id="idCliente" value="0">
                    </form>
        
        
      </div> 
      <div class="modal-footer">
          <a class="modal-action modal-close waves-effect waves-red gray btn-flat">Cerrar</a>
          <a class=" waves-effect waves-red gray btn-flat savemodalcotizacion">Generar</a>
      </div>
    </div>

</div>-->
<div id="modalAutorizacionrefacciones" class="modal"  >
        <div class="modal-content modal-lg" >

          <input type="hidden" name="idCotizacionRefaccion" id="idCotizacionRefaccion">
          <h5 >Refacciones Cotizados</h5>
          <h6 >Refacciones</h6>
          <br>
          <div class="row">
            <div class="tabla_cotizarmostrarre"></div>  
          </div>
          <!--Agregar mas detalles de cotizacion-->
          <div class="row">
              <div class="input-field col s3">  
                  <i class="material-icons prefix">format_list_bulleted</i>
                  <label for="estado">Equipos</label> 
                  <br><br>
                  <select id="equipoListarefacciones" name="equipoListarefacciones" class="browser-default chosen-select equipoListarefacciones" onchange="selectequipor()">
                      <option value="" disabled selected></option>
                      <?php foreach($equiposs as $equipo){?>
                          <option value="<?php echo $equipo->id;?>" >
                              <?php echo $equipo->modelo;?>
                          </option>
                      <?php } ?>
                  </select>
              </div>
              <div class="input-field col s3">  
                  <i class="material-icons prefix">person</i>
                  <label for="estado">Disponibles</label> 
                  <br><br>
                  <div class="refacciones_mostrar"></div>
              </div>
              <div class="input-field col s3">  
                  <i class="material-icons prefix">attach_money</i>
                  <label for="estado">Precios</label> 
                  <br><br>
                  <div class="refaccionesp_mostrar"></div>
              </div>
              
              <div class="input-field col s1">
                <br>
                <a class="btn-floating waves-effect waves-light purple lightrn-1 btn tooltipped" data-position="top" data-tooltip="Nuevo" id="botonAgregarConsumible" name="botonAgregarConsumible" onclick="addrefaccion()">
                <i class="material-icons">add</i>
                </a>
              </div>
          </div> 
          <div class="row">
            <div class="input-field col s3">  
                  <i class="material-icons prefix">format_list_bulleted</i>
                  <label for="estado">Cantidad</label>
                  <br>
                  <input type="number" id="piezasr" name="piezasr" min="1" max="5">
            </div>
            <div class="col s3">
              <label for="serie_bodegarrs">Bodega</label>
                <select id="serie_bodegarrs" name="serie_bodegarrs" class="browser-default chosen-select" >
                  <?php foreach ($bodegas as $itemb) { ?> 
                      <option value="<?php echo $itemb->bodegaId; ?>"><?php echo $itemb->bodega; ?></option>
                  <?php } ?> 
                </select>
            </div>
          </div> 
          <div class="row">
             <div class="col s12 m12 12 equipotableconsumicle">
                  <table class="col s12 m12 12 tablerefacciones Highlight striped">
                      <tbody>
                      </tbody>
                  </table>
              </div>
          </div> 
          <!-- --> 
          <div class="modal-footer">
            <a class="btn waves-effect waves-light green "   onclick="checkrefacciones()">
              Aceptar
            </a>
            <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cancelar</a>
          </div>
        </div>
    </div>
<div class="row" style="display: none;">
  <select class="browser-default form-control-bmz" id="selectejecutivas">
    <?php 
      foreach ($rowsejecutivas->result() as $item) { ?>
        <option value="<?php echo $item->personalId;?>"><?php echo $item->nombre;?> <?php echo $item->apellido_paterno;?> <?php echo $item->apellido_materno;?></option>
    <?php } ?>
    
  </select>
</div>









<div id="modalcotizacion" class="modal" style="width:75%">
    <div class="modal-content ">
      <h4> <i class="material-icons">settings_applications</i>Cotización</h4>
      <hr>
      <div class="col s12 m12 l6">
        <!------------------------------->
            <form id="formulario_cotizacion">
              <input type="hidden" name="idcotizacion" id="idcotizacion" value="0">
              <input type="hidden" name="dirinfo" id="dirinfo">
              <input type="hidden" name="prioridad" id="prioridad">
              <div class="row editcotizacion">
                <div class="col s6">
                  <label>Empresa</label>
                  <select name="tipov" id="tipov" class="browser-default form-control-bmz selectweb">
                    <option value="1">Alta Productividad</option>
                    <option value="2">D-Impresión</option>
                  </select>
                </div>
                <div class="col s6">
                  <label for="cliente">Cliente / Prospecto</label>
                  <input id="cliente" name="cliente"  type="text" class="form-control-bmz" readonly>
                </div>
                <div class="col s6">
                  <label for="atencion" class="active">Atención para:</label>
                  <input id="atencion" name="atencion" class="form-control-bmz"  type="text" required>
                </div>
              </div>
              <div class="row editcotizacion">
                <div class="col s6">
                  <label for="cliente" class="active">Correo</label>
                  <input id="correo" name="correo"   type="text" class="form-control-bmz" required>
                  
                </div>
                <div class="col s6">
                  <label for="telefono" class="active">Teléfono:</label>
                  <input id="telefono" name="telefono"  type="text" class="form-control-bmz" required>
                  
                </div>
              </div>
              
              <!-- ELECCION DE COTIZACION -->
              <div class="row editcotizacion">
                <div class="col s8">
                  <div class="col s2" style="margin-left: -12px;">
                    <label for="estado">¿Que desea cotizar?</label>
                  </div>
                  <div class="col s3 margenTopDivChosen">
                    <input type="checkbox" class="filled-in" id="ventae" name="eleccionCotizar" value="1"  onchange="eleccionCotizar2(1)" />
                    <label for="ventae">Equipos</label>
                  </div>
                  <div class="col s3 margenTopDivChosen">
                    <input type="checkbox" class="filled-in" id="ventac" name="eleccionCotizar" value="2"  onchange="eleccionCotizar2(2)" />
                    <label for="ventac">Consumibles</label>
                  </div>
                  <div class="col s3 margenTopDivChosen">
                    <input type="checkbox" class="filled-in" id="ventar" name="eleccionCotizar" value="3"  onchange="eleccionCotizar2(3)" />
                    <label for="ventar">Refacciones</label>
                  </div>
                  <div class="col s3 margenTopDivChosen">
                    <input type="checkbox" class="filled-in" id="ventap" name="eleccionCotizar" value="4"  onchange="eleccionCotizar2(4)" />
                    <label for="ventap">Servicio</label>
                  </div>
                  <div class="col s3 margenTopDivChosen">
                    <input type="checkbox" class="filled-in" id="ventaa" name="eleccionCotizar" value="5"  onchange="eleccionCotizar2(5)" />
                    <label for="ventaa">Accesorios</label>
                  </div>
                  
                </div>
                <div class="col s4" id="opcionesEquipos" style="display:none;">
                  <div class="col s5" style="margin-left: -12px;">
                    <i class="material-icons prefix">monetization_on</i>
                    <label for="estado">Tipo de Cotización</label>
                  </div>
                  <div class="col s7 margenTopDivChosen" >
                    <select id="tipo" name="tipo2" class="browser-default form-control-bmz" required>
                      <!--<option value="" disabled selected>Selecciona tipo de cotización</option>-->
                      <option value="1">Venta</option>
                      <!--<option value="2">Renta</option>
                      <option value="3">Póliza</option>-->
                    </select>
                  </div>
                </div>
                <div class="input-field col s2" style="display: none" id="opcionesConsumiblesRefacciones">
                  <div class="col s5" style="margin-left: -12px;">
                    <label for="estado">Tipo de Cotización</label>
                  </div>
                  <div class="col s7 margenTopDivChosen" >
                    <select id="tipo" name="tipo" class="browser-default chosen-select tipoConsumibles">
                      <option value="1">Venta</option>
                    </select>
                  </div>
                </div>
                <div class="col s4 editcotizacion">
                  <div class="col s5" >
                    <label for="estado">¿Que tipo?</label>
                  </div>
                  <div class="col s7 margenTopDivChosen" >
                    <select id="propuesta" name="propuesta" class="browser-default form-control-bmz" >
                      <option value="0">Cotización</option>
                      <option value="1">Propuesta</option>
                    </select>
                  </div>
                </div>
                
              </div>
              <div class="row">
                <div class="col s8"></div>
                <div class="col s4">
                  <div style="text-align: end;"><a class="btn-bmz blue" onclick="servicioaddcliente()" style="margin-bottom: 10px;">Agregar un servicio existente</a></div>
                  <div>
                    <input type="number" name="v_ser_tipo" id="v_ser_tipo" style="display:none">
                    <input type="number" name="v_ser_id" id="v_ser_id" style="display:none">
                  </div>
                  <div class="addinfoser"></div>
                </div>
              </div>
              <div class="row">
                <div class="input-field col s6" id="tipoCotizacion" style="display: none;">
                  <div class="col s5" style="margin-left: -12px;">
                    <i class="material-icons prefix">format_list_bulleted</i>
                    <label for="estado">Cotización</label>
                  </div>
                  <div class="col s7 margenTopDivChosen" >
                    <select id="cotizar" name="cotizar" onchange="selectcotiza()" class="browser-default chosen-select">
                      <option value="" disabled selected>Forma de cotización</option>
                      <option value="1">Manual</option>
                      <!--<option value="2">Familia</option>-->
                    </select>
                  </div>
                </div>
                <div class="input-field col s6 select_familia" style="display: none;" id="select_familia">
                  <div class="col s5" style="margin-left: -12px;">
                    <i class="material-icons prefix">person</i>
                    <label for="estado">Familiar</label>
                  </div>
                  <div class="col s7 margenTopDivChosen" >
                    <select id="idfamilia" name="idfamilia" class="browser-default chosen-select" onchange="verificaFamilia(this)">
                      <option value="" disabled selected>Selecciona una opción</option>
                      
                    </select>
                  </div>
                </div>
                <div class="col s6 tiporentaselect" style="display: none">
                  <div class="col s5" style="margin-left: -12px;">
                    <i class="material-icons prefix">format_list_bulleted</i>
                    <label for="estado">Tipo de Renta</label>
                  </div>
                  <div class="col s7 margenTopDivChosen" >
                    <select id="tiporenta" name="tiporenta" class="browser-default form-control-bmz tipoConsumibles">
                      <option value="" disabled selected>Selecciona tipo de cotización</option>
                      <option value="0"></option>
                      <option value="1">Individual</option>
                      <option value="2">Global</option>
                    </select>
                  </div>
                  
                </div>
                
                <div class="row col s12 costoglobalvaldiv" style="display: none;">
                  <div class="col s2">
                    <label class="active" for="eg_rm">Renta Monocromática</label>
                    <input type="text" id="eg_rm" name="eg_rm" value="0">
                  </div>
                  <div class="col s2">
                    <label class="active" for="eg_rvm">Volumen incluido monocromático</label>
                    <input type="text" id="eg_rvm" name="eg_rvm" value="0">
                  </div>
                  <div class="col s2">
                    <label class="active" for="eg_rpem">Precio Excedente monocromático</label>
                    <input type="text" id="eg_rpem" name="eg_rpem" value="0">
                  </div>
                  <div class="col s2">
                    <label class="active" for="eg_rc">Renta Color</label>
                    <input type="text" id="eg_rc" name="eg_rc" value="0">
                  </div>
                  <div class="col s2">
                    <label class="active" for="eg_rvc">Volumen incluido color</label>
                    <input type="text" id="eg_rvc" name="eg_rvc" value="0">
                  </div>
                  <div class="col s2">
                    <label class="active" for="eg_rpec">Precio Excedente color</label>
                    <input type="text" id="eg_rpec" name="eg_rpec" value="0">
                  </div>
                  <div class="col s6"></div>
                  <div class="col s6">
                    <input type="checkbox" class="" id="eg_compra_consumible" name="eg_compra_consumible" value="1"/>
                    <label for="eg_compra_consumible">No Compra consumible color</label>
                  </div>
                </div>
                
                
              </div>
              <div class="row" style="display: none;">
                <div class="input-field col s6" id="selectEquipos" style="display: none;">
                  <div class="col s5" style="margin-left: -12px;">
                    <i class="material-icons prefix">format_list_bulleted</i>
                    <label for="estado">Equipos</label>
                  </div>
                  <div class="col s7 margenTopDivChosen" >
                    <select id="equipoLista" name="equipoLista" class="browser-default chosen-select">
                      <option value="" disabled selected></option>
                      <?php foreach($equipos as $equipo)
                      {
                      ?>
                      <option value="<?php echo $equipo->id;?>" >
                        <?php echo $equipo->modelo;?>
                      </option>
                      <?php
                      }
                      ?>
                    </select>
                  </div>
                </div>
                
              </div>
              
              <!-------------------------------------------------------------->
              <!------------------- CONSUMIBLES POR EQUIPO ------------------->
            
              <!-------------------------------------------------------------->
              <!------------------- REFACCIONES POR EQUIPO ------------------->
              
              <div class="row rowPolizas" id="rowPolizas1" name="rowPolizas1" style="display:none;">
                <div class="input-field col s4" id="selectPolizas" style="display: none;">
                  <i class="material-icons prefix">format_list_bulleted</i>
                  <label for="estado">Pólizas Existentes</label>
                  <br>
                  <div class="col s12 margenTopDivChosen" >
                    <select id="polizasLista" name="polizasLista" class="browser-default chosen-select polizasLista" onchange="funcionDetalles($(this))">
                      <option value="" disabled selected></option>
                      <?php foreach($polizas as $poliza)
                      {
                      ?>
                      <option value="<?php echo $poliza->id;?>" >
                        <?php echo $poliza->nombre;?>
                      </option>
                      <?php
                      }
                      ?>
                    </select>
                  </div>
                </div>
                
              </div>
              
              <input type="hidden" name="idCliente" id="idCliente" value="0">
            </form>
            
              <div class="row">
                <ul class="collapsible" data-collapsible="accordion">
                  <li class="equipos_select" style="display: none;">
                    <div class="collapsible-header">Equipos</div>
                    <div class="collapsible-body">
                      <div class="col s6">
                        <div class="col s5" style="margin-left: -12px;">
                          <label>Seleccionar precio</label>
                        </div>
                        <div class="col s7">
                          <select  id="tipoprecioequipo" class="browser-default form-control-bmz">
                            <option value="0">General</option>
                            <option value="1">Reventa local</option>
                            <option value="2">Reventa foráneo</option>
                          </select>
                        </div>
                        
                        
                      </div>
                      <table id="tables_equipos_selected" class="display">
                        <thead>
                          <tr>
                            <th></th>
                            <th></th>
                            <th>Equipo</th>
                            <th>Consumibles</th>
                            <th>Accesorios</th>
                            <th></th>
                            <th></th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php foreach ($equiposimg as $item){
                          switch ($item->categoriaId) {
                          case 14:
                          $tipoimpresora=0;
                          break;
                          case 15:
                          $tipoimpresora=1;
                          break;
                          case 16:
                          $tipoimpresora=0;
                          break;
                          case 17:
                          $tipoimpresora=1;
                          break;
                          case 18:
                          $tipoimpresora=0;
                          break;
                          case 21:
                          $tipoimpresora=1;
                          break;
                          case 22:
                          $tipoimpresora=1;
                          break;
                          case 23:
                          $tipoimpresora=1;
                          break;
                          
                          default:
                          $tipoimpresora=0;
                          break;
                          }
                          ?>
                          <tr>
                            <td>
                              <input type="checkbox" class="equipo_check nombre" id="equipo_check_<?php echo $item->id ?>" value="<?php echo $tipoimpresora;?>"/>
                              <label for="equipo_check_<?php echo $item->id ?>"></label>
                              <input type="hidden" id="equipo_selected_all" value="<?php echo $item->id ?>"/>
                            </td>
                            <td style="width: 150px;">
                              <input type="text" id="equipo_cantidad" class="equipo_cantidad" value="1"  />
                            </td>
                            <td>
                              <label class="btn btn-info btn_agb_w has-badge-rounded has-badge-danger notificacion_tipped notificacion_tipped_<?php echo $item->id ?>" for="equipo_check_<?php echo $item->id ?>" data-badge="<?php echo $item->stockequipo;?>"
                                onmouseover="bodegasequipo(<?php echo $item->id ?>)" 
                                data-equipo="<?php echo $item->id ?>"
                                >
                                <?php echo $item->modelo ?>
                                
                              </label>
                            </td>
                            <td class="equipo_consumibleselected equipo_c_<?php echo $item->id ?>"></td>
                            <td class="equipo_accessoriosselected equipo_s_<?php echo $item->id ?>"></td>
                            <td class="equipo_rendimientoselected equipo_r_<?php echo $item->id ?>">
                              <div class="switch">
                                <label>
                                
                                <input type="checkbox" id="s_equipo_garantia">
                                <span class="lever"></span>
                                Garantia
                                </label>
                              </div>
                            </td>
                            <td><a class="btn btn-info"  style="padding: 0 5px;" onclick="addaccesorios(<?php echo $item->id ?>)"><i class="material-icons">list</i></a></td>
                          </tr>
                          <?php } ?>
                        </tbody>
                      </table>
                    </div>
                  </li>
                  <li class="selectEquiposConsumibles" style="display: none;">
                    <div class="collapsible-header">Consumibles</div>
                    <div class="collapsible-body">
                      <div class="row">
                        <div class="col s2">
                          <label>Equipos</label>
                          <select id="equipoListaConsumibles" name="equipoListaConsumibles" class="browser-default chosen-select equipoListaConsumibles" onchange="funcionConsumibles()">
                            <option value="" disabled selected></option>
                            <?php foreach($equipos as $equipo){?>
                            <option value="<?php echo $equipo->id;?>" ><?php echo $equipo->modelo;?></option>
                          <?php } ?>
                          </select>
                        </div>
                        <div class="col s3">
                          <label>Disponibles</label>
                          <select id="toner" name="toner" class="browser-default chosen-select toner" onchange="funcionConsumiblesprecios()">
                            <option value="" disabled selected>Selecciona una opción</option>
                          </select>
                        </div>
                        <div class="col s3">
                          <label>Precios</label>
                          <select id="tonerp" name="tonerp" class="browser-default form-control-bmz tonerp">
                            <option value="" disabled selected>Selecciona una opción</option>
                          </select>
                        </div>
                        <div class="col s2">
                          <label>Cantidad</label>
                          <input type="number" name="" min="1" max="5" id="piezasc" class="piezas form-control-bmz">
                        </div>
                        <div class="col s2">
                          <a class="btn-floating waves-effect waves-light purple lightrn-1 btn tooltipped" data-position="top" data-tooltip="Agregar" id="botonAgregarConsumible" name="botonAgregarConsumible" onclick="addcosumibles()">
                              <i class="material-icons">system_update_alt</i>
                            </a>
                        </div>
                        <div class="col s12">
                          <table class="table bordered striped" id="table-addconsumibles">
                            <tbody class="tbody-addconsumibles">
                              
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </li>
                  <li class="selectEquiposRefacciones" style="display: none;">
                    <div class="collapsible-header">Refacciones</div>
                    <div class="collapsible-body">
                      <div class="row">
                        <div class="col s1" style="display:none;">
                          <a class="btn-bmz material-icons prefix" onClick="addh_refacciones_cotizacion()" title="Equipos registrados">format_list_bulleted</a>
                        </div>
                        <div class="col s2">
                          <label>Equipos</label>
                          <select id="equipoListaRefacciones" name="equipoListaRefacciones" class="browser-default chosen-select equipoListaRefacciones" onchange="funcionRefacciones()">
                            <option value="" disabled selected></option>
                            <?php foreach($equipos as $equipo){ ?>
                              <option value="<?php echo $equipo->id;?>" ><?php echo $equipo->modelo;?></option>
                            <?php }  ?>
                          </select>
                          <div class="info_compra"></div>
                        </div>
                        <div class="col s2">
                          <label>Serie</label>
                          <input type="text" name="seriee" id="seriee" class="form-control-bmz seriee" placeholder=" " style="margin-bottom: 0px;">
                          <div class="info_servicio"></div>
                        </div>
                        <div class="col s2">
                          <label>Disponibles</label>
                          <select id="refaccion" name="refaccion" class="browser-default chosen-select refaccion" onchange="selectprecior()">
                            <option value="" disabled selected>Selecciona una opción</option>
                          </select>
                        </div>
                        <div class="col s2">
                          <label for="estado">Precios</label>
                          <div class="refaccionesp_mostrar"></div>
                        </div>
                        <div class="col s1">
                          <label>Cantidad</label>
                          <input type="number" name="piezasRefacion" min="1" max="5" class="piezasRefacion form-control-bmz" id="piezasRefacion">
                        </div>
                        <div class="col s2" >
                          <div class="switch">
                            <label>
                            
                            <input type="checkbox" id="s_ref_garantia">
                            <span class="lever"></span>
                            Garantía
                            </label>
                          </div>
                          
                        </div>
                        <div class="col s1">
                          <a class="btn-floating waves-effect waves-light purple lightrn-1 btn tooltipped" data-position="top" data-tooltip="Agregar" onclick="addrefaccion()"><i class="material-icons">system_update_alt</i></a>
                        </div>
                        <div class="col s12">
                          <table class="table bordered striped" id="table-addrefacciones">
                            <tbody class="tbody-addrefacciones">
                              
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </li>
                  <li class="addpolizass" style="display: none;">
                    <div class="collapsible-header">Pólizas</div>
                    <div class="collapsible-body">
                      <div class="row addpolizass" >
                        <a class="btn waves-effect waves-light cyan" id="btn_add_equipo_poliza">Agregar equipo</a>
                      </div>
                      <div class="row addpolizas">
                
                      </div>
                    </div>
                  </li>
                  <li class="accesorios_selected" style="display: none;">
                    <div class="collapsible-header">Accesorios</div>
                    <div class="collapsible-body">
                      <div class="row">
                        <div class="col s12">
                          <table class="table" id="table_venta_accesorios">
                            <thead>
                              <tr>
                                <th>Equipo</th>
                                <th>Accesorio</th>
                                <th>Precio</th>
                                <th>Cantidad</th>
                                <th></th>
                                <th></th>
                              </tr>
                              <tr>
                                <th>
                                  <select id="equipo_acce" class="browser-default chosen-select" onchange="obtenerabsesorios()">
                                      <option value="" disabled selected></option>
                                      <?php foreach($equipos as $equipo) { ?>
                                          <option value="<?php echo $equipo->id;?>" >
                                              <?php echo $equipo->modelo;?>
                                          </option>
                                      <?php } ?>
                                  </select>
                                  <div class="info_compra"></div>
                                  <div class="info_servicio"></div>
                                </th>
                                <th>
                                  <select id="accesorio_acce" class="browser-default chosen-select" onchange="obtenerabsesorioscosto()">
                                      
                                  </select>
                                </th>
                                <th>
                                  <input type="number" id="precio_acce" class="form-control-bmz" readonly>
                                </th>
                                <th>
                                  <input type="number" id="cantidad_acc" class="form-control-bmz">
                                </th>
                                <th>
                                  <div class="col s2" >
                                    <div class="switch">
                                      <label>
                                      
                                      <input type="checkbox" id="s_acce_garantia">
                                      <span class="lever"></span>
                                      Garantía
                                      </label>
                                    </div>
                                    
                                  </div>
                                </th>
                                <th>
                                  <a class="btn-floating waves-effect waves-light purple lightrn-1 btn tooltipped" data-position="top" data-tooltip="Agregar" id="botonAgregarConsumible" name="botonAgregarConsumible" onclick="addaccesoriov()">
                                    <i class="material-icons">system_update_alt</i>
                                  </a>
                                </th>
                              </tr>
                            </thead>
                            <tbody class="tbody_acce"></tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </li>
                </ul>
              </div>
        <!------------------------------------->
        
      </div> 
      <div class="modal-footer">
          <a class="modal-action modal-close waves-effect waves-red gray btn-flat">Cerrar</a>
          <a class=" waves-effect waves-red gray btn-flat savemodalcotizacion">Generar</a>
      </div>
    </div>

</div>




<div id="modalaccesorios" class="modal">
  <div class="modal-content ">
    
    <div class="row">
      <div class="col s12">
        <ul class="tabs">
          <li class="tab col s3"><a class="active" href="#test1">Accesorios</a></li>
          <li class="tab col s3"><a  href="#test2">Consumibles</a></li>
          <li class="tab col s3 solopararentas" style="display: none;"><a  href="#test3">Condiciones</a></li>
        </ul>
      </div>
      <div id="test1" class="col s12 modaltableaccesorios"></div>
      <div id="test2" class="col s12 modaltableconsu"></div>
      <div id="test3" class="col s12 modaltablerendimientos">
        <div class="row">
          <div class="col s3">
            <label>Volumen monocromático</label>
            <input type="text" id="volumen_monocromatico" value="0">
          </div>
          <div class="col s1" style="margin-top: 25px;">
            X
          </div>
          <div class="col s3">
            <label>Costo</label>
            <input type="text" id="costo_monocromatico" class="form-control-b" value="0.25" readonly>
          </div>
          <div class="col s1" style="margin-top: 25px;">
            =
          </div>
          <div class="col s3">
            <label>Total</label>
            <div class="input-group-b">
              <span class="input-group-btn-b">
                <button class="btn-b btn-primary-b bootstrap-touchspin-down" type="button">$</button>
              </span>
              <input type="text" id="total_monocromatico" class="form-control-b" value="0.00" readonly>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col s3">
            <label>Volumen color</label>
            <input type="text" id="volumen_color" value="0">
          </div>
          <div class="col s1" style="margin-top: 25px;">
            X
          </div>
          <div class="col s3">
            <label>Costo</label>
            <input type="text" id="costo_color" class="form-control-b" value="2.5" readonly>
          </div>
          <div class="col s1" style="margin-top: 25px;">
            =
          </div>
          <div class="col s3">
            <label>Total</label>
            <div class="input-group-b">
              <span class="input-group-btn-b">
                <button class="btn-b btn-primary-b bootstrap-touchspin-down" type="button">$</button>
              </span>
              <input type="text" id="total_color" class="form-control-b" value="0.00" readonly>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col s4">
            <input type="checkbox" class="equipo_check nombre" id="compra_consumible" value="0"/>
            <label for="compra_consumible">No Compra consumible color</label>
          </div>
        </div>
        <div class="row">
          <input type="hidden" id="tiporendimientoselectede">
          <input type="hidden" id="tiporendimientoselectedm">
          <input type="hidden" id="tiporendimientoselectedc">
          <button  class="waves-effect waves-green green btn-flat addtabuladorrendimiento">Aceptar</button>
        </div>
      </div>
      
    </div>
  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
  </div>
</div>



<div id="modalservicioadd" class="modal" style="z-index: 1003; display: none; opacity: 0; transform: scaleX(0.7); top: 4%;">
  <div class="modal-content ">
    <h5>Seleccione servicio existente</h5>
    <div class="row">
      <div class="col s12 servicioaddcliente">
        
      </div>
    </div>
  </div>
  <div class="modal-footer">
    <button class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</button>
  </div>
</div>




<div id="modalinfoequiposser" class="modal"  >
    <div class="modal-content" style="padding-top: 0px; padding-left: 10px; padding-right: 10px;">
      <h5 >Información</h5>
      <br>
      <div class="row optionesservicios">
        <div class="col s4 ">
          <label>Cliente</label>
          <select class="form-control-bmz browser-default" id="idclienteser"></select>
        </div>
        <div class="col s4 ">
          <label>Servicios</label>
          <select class="form-control-bmz browser-default" id="servcli" onchange="selected_servicio()"></select>
        </div>
        <div class="col s2 infordelservicio">
          
        </div>
      </div>
      <div class="row">
        <div class="col s12">
          <table class="table bordered" id="table_ser_cot">
            <tbody class="tbody_info_eq">
              
            </tbody>
          </table>
        </div>
      </div>
      
      

      
      <div class="modal-footer">
        <button class=" waves-effect waves-red gray btn-flat" id="saveactualizacioncot">Guardar</button>
        <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
      </div>
    </div>
</div>