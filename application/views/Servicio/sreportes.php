<style type="text/css">
  #table_reporte_2 th{
    font-size: 12px;
    padding-left: 10px;
  }
  #table_reporte_1{
    font-size: 12px;
  }
  #table_reporte_4{
    font-size: 12px;
  }
</style>
<section id="content">
  <div id="breadcrumbs-wrapper">
    <div class="header-search-wrapper grey lighten-2 hide-on-large-only">
      <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
    </div>
    <div class="container">
      <div class="row">
        <div class="col s10 m6 l6">
          <h5 class="breadcrumbs-title" style="margin-bottom: 18px">Reportes</h5>
          
        </div>
        
      </div>
    </div>
  </div>
  <!--start container-->
  <div class="container">
    <div class="section">
      <div class="card-panel">
            <div class="row">
              <form id="form_reporte">
                <div class="col s3">
                  <label for="tiporeporte">Tipo Reporte</label>
                  <select id="tiporeporte" class="form-control-bmz browser-default" onchange="calcu_mes()" >
                    <option value="5">Reporte General</option>
                    <option value="6">Reporte AST</option>
                    <option value="7">Reporte Cliente</option>
                    <option value="8">Reporte Devoluciones</option>
                    <!--<option value="0">Seleccione</option>
                    <option value="1">Reporte de Técnicos</option>
                    <option value="2">Reporte de Servicios</option>
                    <option value="3">Reporte de Atención a Correctivos</option>
                    <option value="4">Reporte de Fallas</option>-->
                  </select>
                </div>
                <div class="col s2">
                  <label for="tiporeporte">Fecha inicio</label>
                  <input type="date" id="fechainicio" name="fechainicio" value="<?php echo $primerdiames;?>" min="<?php echo date("Y-m-d",strtotime(date('Y-m-d')."- 1 month")); ?>" max="<?php echo date('Y-m-d')?>" onchange="calcu_mes()" class="form-control-bmz" required>
                </div>
                <div class="col s2">
                  <label for="tiporeporte">Fecha final</label>
                  <input type="date" id="fechafinal" name="fechafinal" value="<?php echo date('Y-m-d')?>" max="<?php echo date('Y-m-d')?>" onchange="calcu_mes()" class="form-control-bmz" required>
                </div>
                <div class="col s3" style="display: none;">
                  <label for="rtecnico">Tecnico</label>
                  <select id="rtecnico" class="form-control-bmz browser-default">
                    <option value="0">Seleccione</option>
                    <?php 
                      foreach ($resultstec as $item) { ?>
                        <option value="<?php echo $item->personalId;?>"><?php echo $item->tecnico;?></option>
                    <?php }  ?>
                  </select>
                </div>
              </form>
              <div class="col s2">
                <button class="btn-bmz cyan waves-effect waves-light" type="button" onclick="generarreporte()">Generar
                </button>
              </div>
            </div>

            <div class="row reporte_1" style="display: none;">
              <div class="col s12">
                <table id="table_reporte_1" class="responsive-table display" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Tecnico </th>
                      <th>Servicios Totales </th>
                      <th title="servicios correctivos atendidos">Servicios Correctivos</th>
                      <th title="km al llegar a un servicio - km de salida del anterior servicio o de home">KM Recorrido</th>
                      <th title="tiempo del inicio al fin de un servicio">Tiempo hábil</th>
                      <th title="tiempo entre el final de un servicio y el inicio del siguiente">Tiempo muerto</th>
                      <th>Garantías</th>
                      <th title="KM/ServiciosT">Eficiencia del recurso</th>
                      <th title="Garantías/Serv.Totales">Confianza</th>
                      <th title="cant. De cotizaciones solicitadas">Cotizaciones</th>

                    </tr>
                  </thead>
                  <tbody class="tbody_reporte_1">
                  </tbody>
                </table>
              </div>
            </div>
            <div class="row reporte_2" style="display: none;">
              <div class="col s12">
                <table id="table_reporte_2" class="responsive-table display" cellspacing="0">
                  <thead>
                    <tr>
                      <th># </th>
                      <th colspan="4">RENTAS</th>
                      <th colspan="4">PÓLIZAS</th>
                      <th colspan="4">GENERAL</th>
                      <th colspan="6">INFO GLOBAL (SUMATORIAS)</th>
                    </tr>
                    <tr>
                      <th>No. De Servicios</th>
                      <th>Preventivo</th>
                      <th>Correctivo</th>
                      <th>Asesoría</th>
                      <th>Garantía</th>
                      <th>Preventivo</th>
                      <th>Correctivo</th>
                      <th>Asesoría</th>
                      <th>Garantía</th>
                      <th>Preventivo</th>
                      <th>Correctivo</th>
                      <th>Asesoría</th>
                      <th>Garantía</th>
                      <th>KMs Recorridos</th>
                      <th>Tiempo hábil</th>
                      <th>Tiempo muerto</th>
                      <th>Garantías </th>
                      <th title="num. Total de cotizaciones">Cotizaciones</th>
                      <th>Cancelaciones de servicio</th>

                      
                    </tr>
                  </thead>
                  <tbody class="tbody_reporte_2">
                  </tbody>
                </table>
              </div>
            </div>
            <div class="row reporte_3" style="display: none;">
              <div class="col s12">
                <table id="table_reporte_3" class="responsive-table display" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Cliente </th>
                      <th title="cuando se genera el servicio">Hora de recibido el reporte</th>
                      <th>Hora de llegada del técnico </th>
                      <th title="hora de llegada-hora de recibido el reporte">Tiempo de atención</th>
                      <th>Técnico asignado  </th>
                      <th>Tiempo del técnico en servicio</th>

                    </tr>
                  </thead>
                  <tbody class="tbody_reporte_3">
                  </tbody>
                </table>
              </div>
            </div>
            <div class="row reporte_4" style="display: none;">
              <div class="col s12">
                <table id="table_reporte_4" class="responsive-table display" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Cliente </th>
                      <th>Fecha </th>
                      <th>Hora de atención  </th>
                      <th>Modelo  </th>
                      <th>Serie </th>
                      <th>Ubicación </th>
                      <th>Falla </th>
                      <th>Técnico asignado  </th>
                      <th title="tiempo del inicio al fin del servicio">Tiempo invertido</th>
                      <th>Comentarios</th>

                    </tr>
                  </thead>
                  <tbody class="tbody_reporte_4">
                  </tbody>
                </table>
              </div>
            </div>

          
      </div>
    </div>  
  </div>
</section>