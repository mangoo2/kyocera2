<style type="text/css">
  table td{font-size: 12px; padding: 6px 7px;}
  .notvigencia p{
    font-size: 13px;
  }
  .consearch_active{animation-name: parpadeo; animation-duration: 1s; animation-timing-function: linear; animation-iteration-count: infinite; -webkit-animation-name:parpadeo; -webkit-animation-duration: 1s; -webkit-animation-timing-function: linear; -webkit-animation-iteration-count: infinite; background-color: #f7a45b !important; color: white;font-weight: bold;font-size: 15px;  }
  @-moz-keyframes parpadeo{  
    0% { opacity: 1.0; } 50% { opacity: 0.0; } 100% { opacity: 1.0; }
  }
  @-webkit-keyframes parpadeo {  
    0% { opacity: 1.0; } 50% { opacity: 0.0; } 100% { opacity: 1.0; }
  }
  @keyframes parpadeo {  
    0% { opacity: 1.0; } 50% { opacity: 0.0; } 100% { opacity: 1.0; }
  }
</style>
<?php 
  if ($perfilid==6 or  $perfilid==1) {
    $soloadmin='';
    $selectedeje=0;
  }else{
    $soloadmin='style="display:none"';
    $selectedeje=$idpersonal;
  }
  $this->ModeloGeneral->update_status_periodo_fac_comple();
?>
<!-- START CONTENT -->
<section id="content" width="100%">
  
  <!--start container-->
  <div class="container" width="100%">
    <div class="section">
      <div class="row">        
          <div class="col s12 m12 24">
            <div class="card-panel">
              <div class="row">
              <form id="contrato-form" method="post">
                <div class="row">
                  <div class="col s6">
                    <!-------------------------------------->
                      <div class="row" style="margin-bottom: 10px;">
                        <div class="col s4"><h6>Seleccionar Cliente </h6></div>  
                        <div class="col s6">
                          <select id="idcliente" name="idcliente" class="browser-default chosen-select" onchange="clientecontrato()"><option value="" disabled selected>Selecciona una opción</option><?php foreach ($clientes as $item){ ?><option value="<?php echo $item->id?>" ><?php echo $item->empresa?></option><?php } ?></select>
                        </div>
                      </div>
                      <div class="row" style="margin-bottom: 10px;">
                        <div class="col s4">
                          <h6>Seleccionar Ejecutivo</h6>
                        </div>  
                        <div class="col s6">
                          <select id="idejecutivo" name="idejecutivo" class="browser-default chosen-select" onchange="clientecontrato()"><option value="0">Selecciona una opción</option><?php foreach ($ejecutivos->result() as $item){ ?><option value="<?php echo $item->personalId?>" ><?php echo $item->nombre;?> <?php echo $item->apellido_paterno;?> <?php echo $item->apellido_materno;?></option><?php } ?></select>
                        </div>
                      </div>
                      <div class="row" style="margin-bottom: 10px;">
                        <div class="col s4"><h6>Seleccionar Estatus</h6></div>  
                        <div class="col s6">
                          <select id="tipo_estatus" name="tipo_estatus" class="browser-default chosen-select" onchange="clientecontrato()"><option value="0">Selecciona una opción</option><option value="1">Vigentes</option><option value="2">Concluidos</option><option value="3">Finalizados</option></select>
                        </div>
                      </div>
                    <!-------------------------------------->
                  </div>
                  <div class="col s6 notvigencia" style="overflow: auto;    max-height: 140px;"><?php 
                    $result=$this->Rentas_model->get_cont_vigencia();
                    $html2='';
                    foreach ($result->result() as $item) {
                      $html='';
                      $html.='<div id="card-alert" class="card red aplasarnoti_'.$item->idcontrato.'" data-vigencia="'.$item->oc_vigencia.'">';
                          $html.='<div class="card-content white-text">';
                            $html.='<p><b>POR VENCER </b><b>Contrato:</b> '.$item->idcontrato.' <b>Folio: </b>'.$item->folio.' <b>Empresa:</b> '.$item->empresa.' <b>Vigencia: </b>'.$item->oc_vigencia.'</p>';
                          $html.='</div>';
                          $html.='<button type="button" class="close white-text" data-dismiss="alert" aria-label="Close" onclick="aplasarnoti('.$item->idcontrato.')">';
                            $html.='<span aria-hidden="true">×</span>';
                          $html.='</button>';
                        $html.='</div>';
                        if($item->oc_vigencia_new==null){
                          $html2.=$html;  
                        }else{
                            $fecha_comparar = new DateTime($item->oc_vigencia_new); 
                            $fecha_actual = new DateTime();

                            if ($fecha_comparar < $fecha_actual) {
                              $html2.=$html;
                            }else{
                            }
                        }
                        
                        
                    }
                    echo $html2;
                  ?></div>
                </div>
                
                <?php if(isset($cargafiles)){ ?>
                  <style type="text/css">
                    .import_cont_dell{display: block !important;width: 75px;float: left;}
                  </style>
                  <div class="row">
                    <div class="col s10"></div>
                    <div class="col s2" style="text-align: end;"><a class="btn-bmz waves-effect waves-light gradient-45deg-green-teal gradient-shadow" onclick="cargafilecontadores()" >Cargar contadores</a></div>
                  </div>
                <?php } ?>
                <div class="row">
                  <div class="col s9"></div>
                  <div class="col s2">
                    <label>Buscar Serie/Folio</label>
                    <input type="search" id="buscarseriefolio" class="form-control-bmz">
                  </div>
                  <div class="col s1"><label style="color:transparent;">___________</label><a class="btn-bmz waves-effect waves-light gradient-45deg-green-teal gradient-shadow" onclick="buscarseriefolio()" >Buscar</a></div>
                </div>
                <br>
                <div class="row mostrar_tabla">
                  <div class="col s12">
                    <table id="tabla_contratos" class="display" cellspacing="0"><thead><tr><th>#</th><th>vendedor</th><th>Fecha de solicitud</th><th>Vigencia</th><th>Folio contrato</th><th></th></tr></thead><tbody></tbody></table>
                  </div>
                </div>
              </form>   
              </div>
            </div>
          </div>
      </div>
    </div>
  </section>
<?php 
  $resultpagos=$this->Configuraciones_model->obtenerpagoscontrastoscomplementos();
  foreach ($resultpagos->result() as $item) {
    if($item->ImpPagado>=$item->total){
      $this->ModeloCatalogos->updateCatalogo('factura_prefactura',array('statuspago'=>1),array('facId'=>$item->facId));
    }
  }
?>
<div id="modal_file" class="modal"  >
    <div class="modal-content ">
        <h5>Cargar archivo</h5>
        <div class="row">  
          <div class="col s12">
            <input id="filecontadores" name="filecontadores" type="file">
          </div>
        </div>
        <div class="row">  
          <div class="col s12">
            <table class="table"><thead><tr><th>Fecha de carga</th><th></th><th></th></tr></thead><tbody class="table_cont_reg"></tbody></table>
          </div>
        </div>  
    </div>
    <div class="modal-footer">
        <button class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</button>
    </div>
    <!-- Modal finaliza fin-->
</div>
<div id="modal_file_info" class="modal"  >
    <div class="modal-content ">
        <h5>Contadores registrados</h5>
        <div class="row">  
          <div class="col s12">
            <table class="table"><thead><tr><th>Modelo</th><th>Serie</th><th>Ubicacion</th><th>Fecha</th><th>Mono</th><th>Color</th><th>Escaner</th><th>Contrato</th><th>Folio</th><th>Empresa</th></tr></thead><tbody class="table_cont_reg_inf"></tbody></table>
          </div>
        </div>  
    </div>
    <div class="modal-footer">
        <button class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</button>
    </div>
    <!-- Modal finaliza fin-->
</div>