<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require 'vendor/autoload.php';//php 7.1 como minimo
//use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\Border; // Asegúrate de importar la clase Border
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;


class Excedentes extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloEstadocuenta');
        $this->load->model('Rentas_model');
        $this->load->model('login_model');
        $this->load->model('Clientes_model');
        $this->load->model('ConfiguracionDocumentos_Model');
        
        $this->load->helper('url');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoyL = date('Y-m-d_H_i_s');
        $this->submenu=79;
        if($this->session->userdata('logeado')==true){
            $this->perfilid =$this->session->userdata('perfilid');
            $this->idpersonal = $this->session->userdata('idpersonal');
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,$this->submenu);// 40 es el id del submenu
            if ($permiso==0) {
                redirect('Sistema');
            }
        }else{
            redirect('Sistema');
        }
    }

    function index(){
        $data['MenusubId']=$this->submenu;
        $data['perfirma']=$this->ModeloCatalogos->seleccionejecutivofirma();
        $data['personalrows']=$this->ModeloCatalogos->getselectwheren('personal',array('estatus'=>1));
        $data['perfirma']=$this->ModeloCatalogos->seleccionejecutivofirma();
        $this->load->view('header');
        $this->load->view('main');
        $this->load->view('excedentes/index',$data);
        $this->load->view('excedentes/index_js');
        $this->load->view('footer');  
    }
    function ontenercontratos(){
        $params = $this->input->post();
        $cli=$params['cli'];
        $con=$params['con'];

        $contrato = $this->Rentas_model->contratorentaclienteeje($cli,0,0,$con);
        $html='';$ocultos='';
        foreach ($contrato as $itemcon) {
            $idcontrato=$itemcon->idcontrato;
            $con_folio=$itemcon->folio;
            ${'contrato_pre_'.$idcontrato}=0;
            if($itemcon->estatus!=2){
                $btn_remv_con='<button class="b-btn b-btn-danger removercon" onclick="removercon('.$idcontrato.')"><i class="fas fa-times"></i></button>';
                $html.='<li class="cont_'.$idcontrato.'">';
                        $html.='<div class="collapsible-header"><div class="col s6">'.$idcontrato.' '.$itemcon->folio.'</div><div class="col s6">'.$btn_remv_con.'</div></div>';
                        $html.='<div class="collapsible-body" style="display: none;">';
                               //-------------------------------------------------------------
                              $html.='<table class="table tableexcendetes" >';
                                 $html.='<thead>';
                                    $html.='<tr>';
                                       $html.='<th></th>';
                                       $html.='<th></th>';
                                       $html.='<th>Detalle de producción</th>';
                                       $html.='<th>Producción</th>';
                                       $html.='<th>Volumen Contratado</th>';
                                       $html.='<th>Excedentes</th>';
                                       $html.='<th>Costo Excedentes</th>';
                                       $html.='<th>Por facturar</th>';
                                       $html.='<th>Estatus</th>';
                                       $html.='<th></th>';
                                    $html.='</tr>';
                                 $html.='</thead>';
                                 $html.='<tbody class="date-rentas">';
                                    $get_pres = $this->Rentas_model->get_info_prefactura($idcontrato);
                                    foreach ($get_pres->result() as $itempre) {
                                            //========================================
                                                $periodo='Renta '.$itempre->periodo_inicial.' al '.$itempre->periodo_final;

                                                $periodo_inicial_ante =date("d-m-Y",strtotime($itempre->periodo_inicial.' -1 month'));
                                                //$periodo_inicial_ante =date("d-m-Y",strtotime($periodo_inicial_ante.' -1 day'));
                                                if (date('d',strtotime($periodo_inicial_ante))==1) {
                                                    $fecha2 = new DateTime($periodo_inicial_ante);
                                                    $fecha2->modify('last day of this month');
                                                    $periodo_final_ante = $fecha2->format('Y-m-d');
                                                }else{
                                                  $periodo_final_ante =date("d-m-Y",strtotime($itempre->periodo_final.' -1 month'));  
                                                }
                                                
                                                //$periodo_final_ante =date("d-m-Y",strtotime($periodo_final_ante.' -1 day'));


                                                $periodoante=$periodo_inicial_ante.' al '.$periodo_final_ante;
                                            //========================================
                                        //if($itempre->statuspago==0){
                                            $prefId=$itempre->prefId;
                                            $tipo2=$itempre->tipo2;//1 individual 2 global
                                            $idcondicionextra =$itempre->idcondicionextra;

                                            $condiciones = $this->ModeloCatalogos->getselectwheren('rentas_condiciones',array('id'=>$idcondicionextra));
                                            
                                            $clicks_mono = $itempre->clicks_mono;//global
                                            $clicks_color = $itempre->clicks_color;//global
                                            $precio_c_e_mono = $itempre->precio_c_e_mono;//global
                                            $precio_c_e_color = $itempre->precio_c_e_color;//global
                                            $monto = $itempre->monto;
                                            $montoc = $itempre->montoc;
                                            foreach ($condiciones->result() as $itemrc) {
                                                $tipo2=$itemrc->tipo;
                                            }
                                            //===================================
                                                if($tipo2==2){ // global
                                                    $get_pres_m = $this->Rentas_model->get_info_pre_dll_pro_ex_indi($prefId,1);
                                                    $get_pres_c = $this->Rentas_model->get_info_pre_dll_pro_ex_indi($prefId,2);
                                                    $produccionmono=0;
                                                    $produccioncolor=0;
                                                    foreach ($get_pres_m->result() as $item) {
                                                        $produccionmono=$item->produccion_total;
                                                    }
                                                    foreach ($get_pres_c->result() as $item) {
                                                        $produccioncolor=$item->produccion_total;
                                                    }
                                                    if($idcondicionextra>0){
                                                        
                                                        foreach ($condiciones->result() as $itemrc) {
                                                          //$grentadeposito = $itemrc->renta_m;
                                                          //$grentacolor = $itemrc->renta_c;
                                                          $clicks_mono = $itemrc->click_m;
                                                          $precio_c_e_mono = $itemrc->excedente_m;
                                                          $clicks_color = $itemrc->click_c;
                                                          $precio_c_e_color = $itemrc->excedente_c;

                                                          $monto = $itemrc->renta_m;
                                                          $montoc = $itemrc->renta_c;
                                                        }
                                                    }
                                                }
                                                if($tipo2==1){ // individual
                                                    $get_pres_m = $this->Rentas_model->get_info_pre_dll_pro_ex_indi($prefId,1);
                                                    $get_pres_c = $this->Rentas_model->get_info_pre_dll_pro_ex_indi($prefId,2);
                                                    $produccionmono=0;
                                                    $produccioncolor=0;
                                                    foreach ($get_pres_m->result() as $item) {
                                                        $produccionmono=$item->produccion_total;
                                                    }
                                                    foreach ($get_pres_c->result() as $item) {
                                                        $produccioncolor=$item->produccion_total;
                                                    }
                                                }
                                                


                                                //========================================
                                                $excedete_mono=$produccionmono-$clicks_mono;
                                                if($excedete_mono<0){
                                                    $excedete_mono=0;
                                                }
                                                $excedete_color=$produccioncolor-$clicks_color;
                                                if($excedete_color<0){
                                                    $excedete_color=0;
                                                }

                                            //===================================
                                                $costo_exc_mono=$itempre->excedente;
                                                $costo_exc_color=$itempre->excedentecolor;

                                                $subtotal_mono=$itempre->subtotal;
                                                $subtotal_color=$itempre->subtotalcolor;

                                                $subtotal_g_mono=$subtotal_mono+$costo_exc_mono;
                                                //log_message('error'," prefId: $prefId subtotal_mono($subtotal_mono)+costo_exc_mono($costo_exc_mono)=subtotal_g_mono($subtotal_g_mono)");
                                                $subtotal_g_color=$subtotal_color+$costo_exc_color;
                                                //$por_facturar_iva=round($itempre->total*0.16,2);
                                                //$por_facturar_total=round($por_facturar_sub+$por_facturar_iva,2);
                                                if($prefId==4639){
                                                    log_message('error','$idcontrato: '.$idcontrato.' prefId: '.$prefId.' $excedete_mono: '.$excedete_mono);
                                                }
                                                if($idcontrato==504){
                                                    log_message('error','$idcontrato: '.$idcontrato.' prefId: '.$prefId.' $excedete_mono: '.$excedete_mono);
                                                }
                                                if($tipo2==2){ //se dice que este reporte es solo para globales
                                                    if($excedete_mono>0){
                                                        $subtotal_g_mono=$excedete_mono*$precio_c_e_mono;
                                                        $btnfac_status=$this->ModeloCatalogos->viewfacturasstatusbtn($prefId,$idcontrato,1);
                                                        if($btnfac_status<2){
                                                            ${'contrato_pre_'.$idcontrato}=${'contrato_pre_'.$idcontrato}+1;
                                                            $html.='<tr>';
                                                                $html.='<td>';
                                                                  $html.='<div class="switch"><br>';
                                                                    $html.='<label>'.$prefId.'<input type="checkbox" name="bloqueo" class="pred_checked" id="periodo_'.$prefId.'_1" onclick="calcu()" value="'.$prefId.'_1" ';
                                                                                $html.=' data-preid="'.$prefId.'" ';
                                                                                $html.=' data-tipo="1" ';
                                                                                $html.=' data-periodo="'.$periodoante.'" ';
                                                                                $html.=' data-produ="'.$produccionmono.'" ';
                                                                                $html.=' data-vcontra="'.$clicks_mono.'" ';
                                                                                $html.=' data-exc="'.$excedete_mono.'" ';
                                                                                $html.=' data-pexc="'.$precio_c_e_mono.'" ';
                                                                                $html.=' data-porfac="'.$subtotal_g_mono.'" ';
                                                                                $html.=' data-con="'.$idcontrato.'" ';
                                                                                $html.=' data-conf="'.$con_folio.'" ';
                                                                            $html.='>';
                                                                      $html.='<span class="lever"></span>';
                                                                    $html.='</label>';
                                                                  $html.='</div>';
                                                                $html.='</td>';
                                                                 $html.='<td>Mono</td>';
                                                                 $html.='<td>'.$periodoante.'</td>';
                                                                 
                                                                 $html.='<td>'.number_format($produccionmono,0,'.',',').'</td>';

                                                                 $html.='<td>'.number_format($clicks_mono,0,'.',',').'</td>';
                                                                 $html.='<td>'.number_format($excedete_mono,0,'.',',').'</td>';
                                                                 $html.='<td>'.$precio_c_e_mono.'</td>';
                                                                 $html.='<td>$ '.number_format($subtotal_g_mono,2,'.',',').'</td>';
                                                                 //$btn_fac_edit='<a class="b-btn b-btn-primary" onclick="reporte_excedentes('.$prefId.','.$cli.')" title="Reporte de Excedentes"><i class="fas fa-file-alt"></i></a>';
                                                                 $html.='<td><a type="button"  class="b-btn b-btn-warning ">Pendiente</a></td>';
                                                                 $html.='<td></td>';
                                                            $html.='</tr>';
                                                        }
                                                    }
                                                    if($montoc>0 || $precio_c_e_color>0){
                                                        if($excedete_color>0){
                                                            $btnfac_status=$this->ModeloCatalogos->viewfacturasstatusbtn($prefId,$idcontrato,1);
                                                            if($btnfac_status<2){
                                                                ${'contrato_pre_'.$idcontrato}=${'contrato_pre_'.$idcontrato}+1;
                                                                $html.='<tr>';
                                                                    $html.='<td>';
                                                                      $html.='<div class="switch"><br>';
                                                                        $html.='<label>'.$prefId.'<input type="checkbox" name="bloqueo" class="pred_checked" id="periodo_'.$prefId.'_2" onclick="calcu()" value="'.$prefId.'_2" ';
                                                                                $html.=' data-preid="'.$prefId.'" ';
                                                                                $html.=' data-tipo="2" ';
                                                                                $html.=' data-periodo="'.$periodoante.'" ';
                                                                                $html.=' data-produ="'.$produccioncolor.'" ';
                                                                                $html.=' data-vcontra="'.$clicks_color.'" ';
                                                                                $html.=' data-exc="'.$excedete_color.'" ';
                                                                                $html.=' data-pexc="'.$precio_c_e_color.'" ';
                                                                                $html.=' data-porfac="'.$subtotal_g_color.'" ';
                                                                                $html.=' data-con="'.$idcontrato.'" ';
                                                                                $html.=' data-conf="'.$con_folio.'" ';
                                                                            $html.='>';
                                                                          $html.='<span class="lever"></span>';
                                                                        $html.='</label>';
                                                                      $html.='</div>';
                                                                    $html.='</td>';
                                                                    $html.='<td>Color</td>';
                                                                     $html.='<td>'.$periodoante.'</td>';
                                                                     
                                                                     $html.='<td>'.number_format($produccioncolor,0,'.',',').'</td>';

                                                                     $html.='<td>'.number_format($clicks_color,0,'.',',').'</td>';
                                                                     $html.='<td>'.number_format($excedete_color,0,'.',',').'</td>';
                                                                     $html.='<td>'.$precio_c_e_color.'</td>';
                                                                     $html.='<td>$ '.number_format($subtotal_g_color,2,'.',',').'</td>';
                                                                     $html.='<td><a type="button"  class="b-btn b-btn-warning ">Pendiente</a></td>';
                                                                     $html.='<td></td>';
                                                                $html.='</tr>';
                                                            }
                                                        }
                                                    }
                                                }
                                                if($tipo2==1){
                                                    $r_equiposrenta_m = $this->Rentas_model->get_info_pred_contadores_indi($idcontrato,$prefId,1);
                                                    $r_equiposrenta_c = $this->Rentas_model->get_info_pred_contadores_indi($idcontrato,$prefId,2);
                                                    $v_contr_mono=0;
                                                    $v_contr_color=0;
                                                    $ex_mono=0;
                                                    $ex_color=0;
                                                    $renta_m=0;
                                                    $renta_c=0;
                                                    $p_ce_m=0;
                                                    $p_ce_c=0;
                                                    foreach ($r_equiposrenta_m->result() as $item_re_m) {
                                                        $v_contr_mono=$v_contr_mono+$item_re_m->clicks_mono;
                                                        $ex_mono=$ex_mono+$item_re_m->excedentes;
                                                        $renta_m=$item_re_m->renta;
                                                        
                                                        if($item_re_m->precio_c_e_mono>0){
                                                            $p_ce_m=$item_re_m->precio_c_e_mono;
                                                        }
                                                    }
                                                    foreach ($r_equiposrenta_c->result() as $item_re_c) {
                                                        $v_contr_color=$v_contr_color+$item_re_c->clicks_color;
                                                        $ex_color=$ex_color+$item_re_c->excedentes;
                                                        $renta_c=$item_re_c->rentac;
                                                        if($item_re_c->precio_c_e_color>0){
                                                            $p_ce_c=$item_re_c->precio_c_e_color;
                                                        }
                                                        
                                                    }
                                                    if($ex_mono>0){
                                                        $btnfac_status=$this->ModeloCatalogos->viewfacturasstatusbtn($prefId,$idcontrato,1);
                                                        if($btnfac_status<2){
                                                            ${'contrato_pre_'.$idcontrato}=${'contrato_pre_'.$idcontrato}+1;
                                                            $html.='<tr>';
                                                                $html.='<td>';
                                                                  $html.='<div class="switch"><br>';
                                                                    $html.='<label>'.$prefId.'<input type="checkbox" name="bloqueo" class="pred_checked" id="periodo_'.$prefId.'_1" onclick="calcu()" value="'.$prefId.'_1" ';
                                                                                $html.=' data-preid="'.$prefId.'" ';
                                                                                $html.=' data-tipo="1" ';
                                                                                $html.=' data-periodo="'.$itempre->periodo_inicial.' al '.$itempre->periodo_final.'" ';
                                                                                $html.=' data-produ="'.$produccionmono.'" ';
                                                                                $html.=' data-vcontra="'.$v_contr_mono.'" ';
                                                                                $html.=' data-exc="'.$ex_mono.'" ';
                                                                                $html.=' data-pexc="'.$p_ce_m.'" ';
                                                                                $html.=' data-porfac="'.$subtotal_g_mono.'" ';
                                                                                $html.=' data-con="'.$idcontrato.'" ';
                                                                                $html.=' data-conf="'.$con_folio.'" ';
                                                                            $html.='>';
                                                                      $html.='<span class="lever"></span>';
                                                                    $html.='</label>';
                                                                  $html.='</div>';
                                                                $html.='</td>';
                                                                 $html.='<td>Mono</td>';
                                                                 $html.='<td>Renta '.$itempre->periodo_inicial.' al '.$itempre->periodo_final.'</td>';
                                                                 
                                                                 $html.='<td>'.number_format($produccionmono,0,'.',',').'</td>';

                                                                 $html.='<td>'.number_format($v_contr_mono,0,'.',',').'</td>';
                                                                 $html.='<td>'.number_format($ex_mono,0,'.',',').'</td>';
                                                                 $html.='<td>$'.number_format($p_ce_m,2,'.',',').'</td>';
                                                                 $html.='<td>$ '.number_format($subtotal_g_mono,2,'.',',').'</td>';
                                                                 $html.='<td><a type="button"  class="b-btn b-btn-warning ">Pendiente</a></td>';
                                                                 $html.='<td></td>';
                                                            $html.='</tr>';
                                                        }
                                                    }
                                                    //=================================================================
                                                    if($subtotal_g_color>0){
                                                        if($ex_color>0){
                                                            $btnfac_status=$this->ModeloCatalogos->viewfacturasstatusbtn($prefId,$idcontrato,1);
                                                            if($btnfac_status<2){

                                                                ${'contrato_pre_'.$idcontrato}=${'contrato_pre_'.$idcontrato}+1;
                                                                $html.='<tr>';
                                                                    $html.='<td>';
                                                                      $html.='<div class="switch"><br>';
                                                                        $html.='<label>'.$prefId.'<input type="checkbox" name="bloqueo" class="pred_checked" id="periodo_'.$prefId.'_2" onclick="calcu()" value="'.$prefId.'_2" ';
                                                                                $html.=' data-preid="'.$prefId.'" ';
                                                                                $html.=' data-tipo="2" ';
                                                                                $html.=' data-periodo="'.$itempre->periodo_inicial.' al '.$itempre->periodo_final.'" ';
                                                                                $html.=' data-produ="'.$produccioncolor.'" ';
                                                                                $html.=' data-vcontra="'.$v_contr_color.'" ';
                                                                                $html.=' data-exc="'.$ex_color.'" ';
                                                                                $html.=' data-pexc="'.$p_ce_c.'" ';
                                                                                $html.=' data-porfac="'.$subtotal_g_color.'" ';
                                                                                $html.=' data-con="'.$idcontrato.'" ';
                                                                                $html.=' data-conf="'.$con_folio.'" ';
                                                                            $html.='>';
                                                                          $html.='<span class="lever"></span>';
                                                                        $html.='</label>';
                                                                      $html.='</div>';
                                                                    $html.='</td>';
                                                                     $html.='<td>Color</td>';
                                                                     $html.='<td>Renta '.$itempre->periodo_inicial.' al '.$itempre->periodo_final.'</td>';
                                                                     
                                                                     $html.='<td>'.number_format($produccioncolor,0,'.',',').'</td>';

                                                                     $html.='<td>'.number_format($v_contr_color,0,'.',',').'</td>';
                                                                     $html.='<td>'.number_format($ex_color,0,'.',',').'</td>';
                                                                     $html.='<td>$'.number_format($p_ce_c,2,'.',',').'</td>';
                                                                     $html.='<td>$ '.number_format($subtotal_g_color,2,'.',',').'</td>';
                                                                     $html.='<td><a type="button"  class="b-btn b-btn-warning ">Pendiente</a></td>';
                                                                     $html.='<td></td>';
                                                                $html.='</tr>';
                                                            }
                                                        }
                                                    }
                                                }
                                        //}
                                    }
                                    
                                    
                                    
                                 $html.='</tbody>';
                              $html.='</table>';
                            //-------------------------------------------------------------
                        $html.='</div>';
                $html.='</li>';
               
                $contrato_pre=${'contrato_pre_'.$idcontrato};
                if($contrato_pre==0){
                    $ocultos.='.cont_'.$idcontrato.'{ display:none;}';
                }
            }
                
        }
                $html.='<style>';
                $html.=$ocultos;
                $html.='</style>';

        echo $html;
    }
    function generarreporte(){
        $params =$this->input->post();
        $periodos =$params['periodos'];
        $cliente =$params['cliente'];
        $DATAf = json_decode($periodos);
        //=========================================
            //$this->load->view('Reportes/exportarservicios',$data);
            //$this->load->view('Reportes/exportarservicios_xlsx',$data);
            //=====================================================================
                $spreadsheet = new Spreadsheet();
                $row_h=0;//num de pagina

                if($row_h==0){
                    $sheet = $spreadsheet->getActiveSheet();//cuendo solo se tiene una pagina
                }else{
                    $spreadsheet->createSheet();
                    $sheet = $spreadsheet->setActiveSheetIndex($row_h);//a partir de la segunda pagina
                }

                $sheet->setTitle('Excedentes');//titulo de pa pagina
                $rc=1;
                //===========================================
                    $imagePath = FCPATH.'public/img/encabezadoform.jpg'; // Ruta de la imagen que deseas agregar
                    $drawing = new Drawing();
                    $drawing->setName('Imagen');
                    $drawing->setDescription('Descripción de la imagen');
                    $drawing->setPath($imagePath);
                    $drawing->setHeight(100); // Altura de la imagen en píxeles
                    //$drawing->setWidth(100); // Ancho de la imagen en píxeles
                    $drawing->setCoordinates('B1'); // Coordenadas de la celda donde se colocará la imagen
                    $drawing->setWorksheet($sheet);
                    $rc=$rc+5;
                //===========================================
                    $rc++;
                    $sheet->mergeCells('A'.$rc.':I'.$rc);
                    $cellda='A'.$rc;
                    $sheet->setCellValue($cellda, 'DETALLE EXCEDENTES');
                    //=========================================
                        $style = $sheet->getStyle($cellda);
                        $style->applyFromArray([
                            
                            
                            'font' => [
                                'bold' => true, // Aplicar negrita al texto
                                'size' => 14 // Cambiar tamaño del texto
                            ],
                            'alignment' => [
                                'horizontal' => Alignment::HORIZONTAL_CENTER // Centrar el texto horizontalmente
                            ]
                        ]);
                    //=========================================
                //===========================================
                        // Combina las celdas A1 y B1
                        
                        $row_idcontrato=0;
                        $totalgeneral_con=0;
                        for ($i=0;$i<count($DATAf);$i++) {
                            $tipo_mc='';
                            if($DATAf[$i]->tipo==1){
                                $tipo_mc='Monocromatico';
                            }
                            if($DATAf[$i]->tipo==2){
                                $tipo_mc='Color';
                            }
                            if($row_idcontrato>0){
                                if($row_idcontrato!=$DATAf[$i]->con){
                                    $rc++;
                                    //===============================================
                                    // esta misma configuracion esta abajo
                                    $sheet->setCellValue('H'.$rc, 'Total');
                                    $sheet->setCellValue('I'.$rc, '$ '.$totalgeneral_con);
                                        //$celda='A'.$rc.':I'.$rc;
                                        $celda='I'.$rc;
                                        $color_back='FFEB3B';
                                        $style = $sheet->getStyle($celda);
                                        $style->applyFromArray([
                                            'fill' => [
                                                'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                                                'startColor' => ['rgb' => $color_back],

                                            ],
                                            'borders' => [
                                                    'allBorders' => [
                                                        'borderStyle' => Border::BORDER_THIN, // Cambia el estilo de borde según lo necesites
                                                        'color' => ['rgb' => '000000'] // Cambia el color del borde según lo necesites
                                                    ]
                                            ]
                                        ]);
                                    //=============================================
                                }

                            }
                            if($row_idcontrato!=$DATAf[$i]->con){
                                $totalgeneral_con=0;
                                $row_idcontrato=$DATAf[$i]->con;
                                $rc=$rc+3;
                                //================================
                                    $sheet->mergeCells('A'.$rc.':B'.$rc);

                                    $sheet->setCellValue('A'.$rc, 'Contrato');
                                    //$sheet->setCellValue('B1', '');
                                    $sheet->setCellValue('C'.$rc, 'Tipo');
                                    $sheet->setCellValue('D'.$rc, 'Periodo');
                                    $sheet->setCellValue('E'.$rc, 'Produccion');
                                    $sheet->setCellValue('F'.$rc, 'Volumen contratado');
                                    $sheet->setCellValue('G'.$rc, 'Excedentes');
                                    $sheet->setCellValue('H'.$rc, 'Costo Excedentes');
                                    $sheet->setCellValue('I'.$rc, 'Por facturar');
                                //================================
                                    
                                    $celda='A'.$rc.':I'.$rc;
                                    //$celda='A'.$rc;
                                    $color_back='b4c6e7';
                                    $style = $sheet->getStyle($celda);
                                    $style->applyFromArray([
                                        'fill' => [
                                            'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                                            'startColor' => ['rgb' => $color_back],

                                        ],
                                        'borders' => [
                                                'allBorders' => [
                                                    'borderStyle' => Border::BORDER_THIN, // Cambia el estilo de borde según lo necesites
                                                    'color' => ['rgb' => '000000'] // Cambia el color del borde según lo necesites
                                                ]
                                        ]
                                    ]);
                                //================================================
                                    
                            }
                            $rc++;
                            $sheet->mergeCells('A'.$rc.':B'.$rc);
                            $sheet->setCellValue('A'.$rc, $DATAf[$i]->conf);
                            //$sheet->setCellValue('B'.$rc, $emp);
                            $sheet->setCellValue('C'.$rc, $tipo_mc);
                            $sheet->setCellValue('D'.$rc, ''.$DATAf[$i]->periodo);
                            $sheet->setCellValue('E'.$rc, $DATAf[$i]->produ);
                            $sheet->setCellValue('F'.$rc, $DATAf[$i]->vcontra);
                            $sheet->setCellValue('G'.$rc, $DATAf[$i]->exc);
                            $sheet->setCellValue('H'.$rc, $DATAf[$i]->pexc);
                            $sheet->setCellValue('I'.$rc, '$ '.$DATAf[$i]->porfac);
                            $totalgeneral_con=$totalgeneral_con+$DATAf[$i]->porfac;
                            //================================
                                    
                                    $celda='A'.$rc.':I'.$rc;
                                    //$celda='A'.$rc;
                                    //$color_back='007bff';
                                    $style = $sheet->getStyle($celda);
                                    $style->applyFromArray([
                                        'borders' => [
                                                'allBorders' => [
                                                    'borderStyle' => Border::BORDER_THIN, // Cambia el estilo de borde según lo necesites
                                                    'color' => ['rgb' => '000000'] // Cambia el color del borde según lo necesites
                                                ]
                                        ]
                                    ]);
                            //================================================
                            $row_tr=$i+1;
                            //log_message('error','count($DATAf):'.count($DATAf).' $i:'.$row_tr);
                            if(count($DATAf)==$row_tr){
                                $rc++;
                                //===============================================
                                // esta misma configuracion estara arriba
                                $sheet->setCellValue('H'.$rc, 'Total');
                                $sheet->setCellValue('I'.$rc, '$ '.$totalgeneral_con);

                                    //$celda='A'.$rc.':I'.$rc;
                                    $celda='I'.$rc;
                                    $color_back='FFEB3B';
                                    $style = $sheet->getStyle($celda);
                                    $style->applyFromArray([
                                        'fill' => [
                                            'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                                            'startColor' => ['rgb' => $color_back],

                                        ],
                                        'borders' => [
                                                'allBorders' => [
                                                    'borderStyle' => Border::BORDER_THIN, // Cambia el estilo de borde según lo necesites
                                                    'color' => ['rgb' => '000000'] // Cambia el color del borde según lo necesites
                                                ]
                                        ]
                                    ]);
                                //=============================================
                            }
                        }
                        //$sheet->getColumnDimension('A')->setAutoSize(true);
                        $sheet->getColumnDimension('C')->setAutoSize(true);
                        $sheet->getColumnDimension('D')->setAutoSize(true);
                        $sheet->getColumnDimension('E')->setAutoSize(true);
                        $sheet->getColumnDimension('F')->setAutoSize(true);
                        $sheet->getColumnDimension('G')->setAutoSize(true);
                        $sheet->getColumnDimension('H')->setAutoSize(true);
                        $sheet->getColumnDimension('I')->setAutoSize(true);
                        









                        
                // Configurar encabezados para la descarga
                //=================================================
                $tiposave=0;//0 guarde directamente 1 descargue
                if($tiposave==1){
                    /*
                    $writer = new Xlsx($spreadsheet);

                    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                    header('Content-Disposition: attachment;filename="archivo.xlsx"');
                    header('Cache-Control: max-age=0');

                    // Guardar el archivo en la salida del búfer de salida (output buffer)
                    $writer->save('php://output');
                    */
                }else{
                    // Guardar el archivo XLSX
                    $writer = new Xlsx($spreadsheet);
                    $urlarchivo0='fichero_reporte/reporte_excedentes_'.$this->fechahoyL.'.xlsx';
                    $urlarchivo=FCPATH.$urlarchivo0;
                    $writer->save($urlarchivo);

                    //redirect(base_url().$urlarchivo0); 

                }
                $array=array('fichero'=>$urlarchivo0);
                echo json_encode($array);
        //=========================================
    }
    function pruebas($prefId,$idcontrato,$tipo){
        //$tipo 0 normal 1 status
        $btnfac_status=$this->ModeloCatalogos->viewfacturasstatusbtn($prefId,$idcontrato,$tipo);
        echo $btnfac_status;
    }
    function reporte_excedente2(){
        if(isset($_GET['cli'])){
            $idcliente=$_GET['cli'];
        }else{
            $idcliente=0;
        }
        $data['datosCliente'] = $this->Clientes_model->getClientePorId($idcliente);
        $data['configuracionCotizacion'] = $this->ConfiguracionDocumentos_Model->getConfiguracion(1);

        $nombreUsuario = $this->ModeloCatalogos->getselectwheren('personal',array('personalId'=>$this->idpersonal));
        $nombreAux = $nombreUsuario->result();
        $data['nombreUsuario'] = $nombreAux[0]->nombre.' '.$nombreAux[0]->apellido_paterno.' '.$nombreAux[0]->apellido_materno;
        $data['emailUsuario'] = $nombreAux[0]->email;

        $this->load->view('Reportes/reporteexcedente2',$data);
    }
    
}