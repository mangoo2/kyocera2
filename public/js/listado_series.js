var base_url = $('#base_url').val();
var table_equipo;
var editarserieid=0;
$(document).ready(function() {
    $('#edit_eq_ser').chosen({width: "100%"});
    $('.chosen-select').chosen({width: "100%"});
    table_equipo=$(".tabla_equipo").DataTable();
    
    datos_equipos();

    $('#bodegaId').change(function() {
        table_equipo.ajax.reload().state.clear();     
    });
});
function tipo_tabla(){
    var id_tipo = $('#idserie option:selected').val();
    if(id_tipo == 1){
        datos_equipos();
        console.log('equipo');
    }else if(id_tipo == 2){
        datos_accesorios();
        console.log('accesorios');
    }else if(id_tipo == 3){
        datos_refacciones();
        console.log('refacciones');  
    }
    //table_equipo.destroy();
}
function datos_equipos(){
    var perfilid = $('#perfilid').val();
    table_equipo.destroy();
    table_equipo=$(".tabla_equipo").DataTable({
        pagingType: 'input',
        //stateSave: true,
        "bProcessing": true,
        "serverSide": true,
        search: {
                return: true
            }, 
        "ajax": {
           "url": base_url+"index.php/Series/getData_serie_e",
           type: "post",
           "data": function(d){
             d.bodegaId = $('#bodegaId option:selected').val()
            },
            error: function(){
               $("#tabla").css("display","none");
            }
        },
        "columns": [
            {"data": "serieId"},
            {"data": "bodega"},
            {"data": "modelo"},
            {"data": "serie"},
            {"data": "reg"},
            {"data": "fechas_mov",
                render: function(data,type,row){ 
                    var cadena = row.fechas_mov;
                    var html='';
                    if (!row.fechas_mov || row.fechas_mov.trim() === '') {
                        
                    }else{
                        
                        const resultados = cadena.split('<br>');
                        const resultadosFiltrados = resultados.map(item => item.trim()).filter(item => item);
                        const ultimoResultado = resultadosFiltrados[resultadosFiltrados.length - 1];
                        
                        var html=ultimoResultado;
                    }
                    
                    return html;
                }
            },
            {"data": "nombre"},
            {"data": null,
                render: function(data,type,row){ 
                    if(row.status == 0){
                        var html='<p class="seriedisponible ubicarseries_'+row.serieId+'" data-serie="'+row.serie+'" style="cursor:pointer" onclick="ubicarseries('+row.serieId+',1,1)">Disponible</p>';
                    }else if(row.status == 1){
                        var html='<p class="serieretorno ubicarseries_'+row.serieId+'" data-serie="'+row.serie+'" style="cursor:pointer" onclick="ubicarseries('+row.serieId+',1,1)">Retorno</p>';
                    }else if(row.status == 2){
                        var html='<p class="serierenta ubicarseries_'+row.serieId+'" data-serie="'+row.serie+'" style="cursor:pointer" onclick="ubicarseries('+row.serieId+',1,0)">Renta</p>';
                    }else if(row.status == 3){
                        var html='<p class="seriesalida ubicarseries_'+row.serieId+'" data-serie="'+row.serie+'" style="cursor:pointer" onclick="ubicarseries('+row.serieId+',1,0)">Salida</p>';
                    }

                    return html;
                }
            },
            {"data": null,
                render: function(data,type,row){ 
                    var html='';
                        html+='<div class="menuaction">';
                    if(row.status == 0 || row.status == 1){
                        var nombre="'"+row.modelo+"'";
                            html+='<button class="btn-bmz waves-effect waves-light " title="Traspaso"\
                                 onclick="traspasar('+row.serieId+','+row.bodegaId+','+1+','+nombre+',1,1)">\
                                 <i class="fas fa-file-import"></i></button>';
                            if(perfilid==1){
                                html+='<button class="btn-bmz waves-effect waves-light editarserie_'+row.serieId+'"\
                                                data-serienombre="'+row.serie+'"\
                                                title="Editar serie"\
                                                onclick="editarserie('+row.serieId+','+row.productoid+')">\
                                     <i class="far fa-edit"></i></button>';
                                if(row.status == 0){
                                    html+='<button class="btn-bmz waves-effect waves-light editarserie_'+row.serieId+'"\
                                                data-serienombre="'+row.serie+'"\
                                                title="Eliminar serie"\
                                                onclick="eliminarserie('+row.serieId+','+row.productoid+')">\
                                     <i class="fas fa-trash-alt" style="color:red"></i></button>';
                                }
                            }
                            html+='<button class="btn-bmz waves-effect waves-light " title="Resguardar/liberar" onclick="resguardar('+row.serieId+','+row.resguardar_cant+')">';
                                        if(row.resguardar_cant==0){
                                            html+='<i class="fas fa-lock-open"></i>';
                                        }else{
                                            html+='<i class="fas fa-lock"></i>';
                                        }
                            html+='</button>';
                    }
                        html+='</div>';
                    return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
    }).on('draw',function(){
        $('.dataTables_filter').append('<i class="fa fa-search buttonsearch" onclick="searchtable_v()"></i>')

    });

}
function datos_accesorios(){
    table_equipo.destroy();
    table_equipo=$(".tabla_equipo").DataTable({
        pagingType: 'input',
        //stateSave: true,
        "bProcessing": true,
        "serverSide": true,
        search: {
                return: true
            }, 
        "ajax": {
           "url": base_url+"index.php/Series/getData_serie_a",
           type: "post",
           "data": function(d){
             d.bodegaId = $('#bodegaId option:selected').val()
            },
            error: function(){
               $("#tabla").css("display","none");
            }
        },
        "columns": [
            {"data": "serieId"},
            {"data": "bodega"},
            {"data": "nombre"},
            {"data": "serie"},
            {"data": "reg"},
            {"data": "fechas_mov",
                render: function(data,type,row){ 
                    var cadena = row.fechas_mov;
                    var html='';
                    if (!row.fechas_mov || row.fechas_mov.trim() === '') {
                        
                    }else{
                        
                        const resultados = cadena.split('<br>');
                        const resultadosFiltrados = resultados.map(item => item.trim()).filter(item => item);
                        const ultimoResultado = resultadosFiltrados[resultadosFiltrados.length - 1];
                        
                        var html=ultimoResultado;
                    }
                    
                    return html;
                }
            },
            {"data": "persona"},
            {"data": null,
                render: function(data,type,row){ 
                    if(row.status == 0){
                        if(row.con_serie==0){
                            var cantidada=row.cantidad;
                            var html='<p class="seriedisponible">Disponible '+cantidada+'</p>';
                        }else{
                            var cantidada='';
                            var html='<p class="seriedisponible ubicarseries_'+row.serieId+'" data-serie="'+row.serie+'" onclick="ubicarseries('+row.serieId+',2,1)">Disponible</p>';
                        }
                        
                    }else if(row.status == 1){
                        var html='<p class="serieretorno ubicarseries_'+row.serieId+'" data-serie="'+row.serie+'" onclick="ubicarseries('+row.serieId+',2,1)">Retorno</p>';
                    }else if(row.status == 2){
                        var html='<p class="serierenta ubicarseries_'+row.serieId+'" data-serie="'+row.serie+'" onclick="ubicarseries('+row.serieId+',2,0)">Renta</p>';
                    }else if(row.status == 3){
                        var html='<p class="seriesalida ubicarseries_'+row.serieId+'" data-serie="'+row.serie+'" onclick="ubicarseries('+row.serieId+',2,0)">Salida</p>';
                    }
                    return html;
                }
            },
            {"data": null,
                render: function(data,type,row){ 
                    if(row.status == 0 || row.status == 1){
                        var nombre="'"+row.nombre+"'";
                        var html='<button class="btn-bmz waves-effect waves-light" title="Traspaso"\
                                 onclick="traspasar('+row.serieId+','+row.bodegaId+','+2+','+nombre+','+row.con_serie+','+row.cantidad+')">\
                                 <i class="material-icons left">reply</i></button>';

                            html+='<button class="btn-bmz waves-effect waves-light "\
                                                title="Eliminar serie"\
                                                onclick="eliminarserieacc('+row.serieId+')">\
                                     <i class="fas fa-trash-alt" style="color:red"></i></button>';
                        //===============================================
                            if(row.resguardar_cant>0){
                                var resclass='has-badge-rounded has-badge-danger notificacion_tipped';
                                var resclass_data=' data-badge="'+row.resguardar_cant+'" '
                            }else{
                                var resclass='';
                                var resclass_data='';
                            }
                            if(row.con_serie==0){
                                html+='<button class="btn-bmz waves-effect waves-light '+resclass+' " title="Resguardar/liberar" onclick="resguardar_acc('+row.serieId+','+row.resguardar_cant+','+row.con_serie+')" '+resclass_data+' >';
                                            if(row.resguardar_cant==0){
                                                html+='<i class="fas fa-lock-open"></i>';
                                            }else{
                                                html+='<i class="fas fa-lock"></i>';
                                            }
                                html+='</button>';
                            }else{
                                html+='<button class="btn-bmz waves-effect waves-light '+resclass+' " title="Resguardar/liberar" onclick="resguardar_acc_s('+row.serieId+','+row.resguardar_cant+')" '+resclass_data+' >';
                                            if(row.resguardar_cant==0){
                                                html+='<i class="fas fa-lock-open"></i>';
                                            }else{
                                                html+='<i class="fas fa-lock"></i>';
                                            }
                                html+='</button>';
                            }
                        //============================================
                    }else {
                        var html='';
                    }

                    return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
    }).on('draw',function(){
        $('.dataTables_filter').append('<i class="fa fa-search buttonsearch" onclick="searchtable_v()"></i>')

    });
}
function datos_refacciones(){
    table_equipo.destroy();
    table_equipo=$(".tabla_equipo").DataTable({
        pagingType: 'input',
        //stateSave: true,
        "bProcessing": true,
        "serverSide": true,
        search: {
                return: true
            }, 
        "ajax": {
           "url": base_url+"index.php/Series/getData_serie_r",
           type: "post",
           "data": function(d){
             d.bodegaId = $('#bodegaId option:selected').val()
            },
            error: function(){
               $("#tabla").css("display","none");
            }
        },
        "columns": [
            {"data": "serieId"},
            {"data": "bodega"},
            {"data": "nombre",
                render: function(data,type,row){
                    var html=row.nombre+' ('+row.codigo+')';
                    return html; 
                }
            },
            {"data": "serie"},
            {"data": "reg"},
            {"data": "fechas_mov",
                render: function(data,type,row){ 
                    var cadena = row.fechas_mov;
                    var html='';
                    if (!row.fechas_mov || row.fechas_mov.trim() === '') {
                        
                    }else{
                        
                        const resultados = cadena.split('<br>');
                        const resultadosFiltrados = resultados.map(item => item.trim()).filter(item => item);
                        const ultimoResultado = resultadosFiltrados[resultadosFiltrados.length - 1];
                        
                        var html=ultimoResultado;
                    }
                    
                    return html;
                }
            },
            {"data": "persona"},
            {"data": null,
                render: function(data,type,row){ 
                    if (row.con_serie==1) {
                        if(row.status == 0){
                            var html='<p class="seriedisponible">Disponible</p>';
                        }else if(row.status == 1){
                            var html='<p class="serieretorno">Retorno</p>';
                        }else if(row.status == 2){
                            var html='<p class="serierenta">Renta</p>';
                        }else if(row.status == 3){
                            var html='<p class="seriesalida">Salida</p>';
                        }
                    }else{
                        if (row.cantidad==0) {
                            var html='<p class="seriesalida">Disponibles: <b>'+row.cantidad+'</b></p>';
                        }else{
                           var html='<p class="seriedisponible">Disponibles: <b>'+row.cantidad+'</b></p>'; 
                        }
                        
                    }
                    
                    return html;
                }
            },
            {"data": null,
                render: function(data,type,row){ 
                    if (row.con_serie==1) {
                        if(row.status == 0 || row.status == 1){
                            var nombre="'"+row.nombre+" ("+row.codigo+")'";
                            var html='<button class="btn-bmz waves-effect waves-light " title="Traspaso" ';
                                html+='onclick="traspasar('+row.serieId+','+row.bodegaId+','+3+','+nombre+',1,'+row.cantidad+')"> ';// el ultimo valor es 1 por que se dice que hay serie, pero despues se modificara para que nos traspasos sean totales o parciales
                                html+='<i class="material-icons left">reply</i></button>';
                        }else {
                            var html='';
                        }
                    }else{
                        var nombre="'"+row.nombre+" ("+row.codigo+")'";
                        var html='<button class="btn-bmz waves-effect waves-light " title="Traspaso"\
                                 onclick="traspasarss('+row.serieId+','+row.bodegaId+','+row.cantidad+','+nombre+')">\
                                 <i class="material-icons left">reply</i></button>';
                    }
                    //===============================================
                        if(row.resguardar_cant>0){
                            var resclass='has-badge-rounded has-badge-danger notificacion_tipped';
                            var resclass_data=' data-badge="'+row.resguardar_cant+'" '
                        }else{
                            var resclass='';
                            var resclass_data='';
                        }
                        if(row.con_serie==0){
                            html+='<button class="btn-bmz waves-effect waves-light '+resclass+' " title="Resguardar/liberar" onclick="resguardar_ref('+row.serieId+','+row.resguardar_cant+','+row.con_serie+')" '+resclass_data+' >';
                                        if(row.resguardar_cant==0){
                                            html+='<i class="fas fa-lock-open"></i>';
                                        }else{
                                            html+='<i class="fas fa-lock"></i>';
                                        }
                            html+='</button>';
                        }else{
                            html+='<button class="btn-bmz waves-effect waves-light '+resclass+' " title="Resguardar/liberar" onclick="resguardar_ref_s('+row.serieId+','+row.resguardar_cant+')" '+resclass_data+' >';
                                            if(row.resguardar_cant==0){
                                                html+='<i class="fas fa-lock-open"></i>';
                                            }else{
                                                html+='<i class="fas fa-lock"></i>';
                                            }
                                html+='</button>';
                        }
                    //============================================

                    return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
    }).on('draw',function(){
        $('.dataTables_filter').append('<i class="fa fa-search buttonsearch" onclick="searchtable_v()"></i>')

    });
}
function searchtable_v(){
    var contenedor = document.getElementById('tabla_equipo_filter');

    // Selecciona el input dentro del contenedor
    var input = contenedor.querySelector('input.form-control-bmz');

    // Obtén el valor del input
    var searchv = input.value;

    // Muestra el valor en la consola
    console.log(searchv);
    //var searchv =$("input[type=search]").val();
    table_equipo.search(searchv).draw();
}
function traspasar(id,bodega,tipo,nombre,consin,cant){
    $('#serie_sin_Serie').val(consin);
    $('#trass_cant').val(cant);
    if(consin==0){
        var datainput='<label>Cantidad</label>';
            datainput+='<input type="number" id="cantidad_tras" class="form-control-bmz">';
        $('.addcantidadtras').html(datainput);
    }else{
        $('.addcantidadtras').html('');
    }
    $('.productotras').html(nombre);
    $('#tipo').val(tipo);
    $('#trasserieid').val(id);
    $('#bodegao').val(bodega).prop('disabled', false).trigger("chosen:updated");
    $('#modal_traspasos').modal();
    $('#modal_traspasos').modal('open');

    var tipov=$('#bodegao option:selected').data('bodegatipo');
    $('.bodegatipo_1').prop('disabled', true).trigger("chosen:updated");
    $('.bodegatipo_2').prop('disabled', true).trigger("chosen:updated");
    //console.log('tipov_'+tipov);
    if(tipov==1){
        $('.bodegatipo_1').prop('disabled', false).trigger("chosen:updated");
        //console.log('tipov1');
    }
    if(tipov==2){
        $('.bodegatipo_2').prop('disabled', false).trigger("chosen:updated");
        //console.log('tipov2');
    }
    setTimeout(function(){ 
        $('#bodegao').prop('disabled', true).trigger("chosen:updated");
        $('#bodeganew option[value="'+bodega+'"]').prop('disabled', true).trigger("chosen:updated");
    }, 1000);
}
function aceptar_traspaso(){
    var bodeganew = $('#bodeganew option:selected').val();
    var tipos=$('#tipo').val();
    var motivo=$('#motivo1').val();
    var consin = $('#serie_sin_Serie').val();
    var cant_o = $('#trass_cant').val(); //solo para validar
    var cant_tras = $('#cantidad_tras').val()==undefined?0:$('#cantidad_tras').val();
    if(consin==1){
        var validar=true;
    }else{
        if (parseFloat(cant_tras)>0&&parseFloat(cant_tras)<=parseFloat(cant_o)) {
            var validar=true;
        }else{
            var validar=false;
            toastr.error('la Cantidad no es valida');
        }
    }
    if(validar){
        if(bodeganew>0){
            if(motivo!=''){
                $.ajax({
                    type:'POST',
                    url: base_url+'Series/traspasar',
                    data: {
                        serieid: $('#trasserieid').val(),
                        tipo: tipos,
                        bodega:$('#bodeganew option:selected').val(),
                        bodegao:$('#bodegao option:selected').val(),
                        motivo:motivo,
                        modelo:$('.modelotras1').html(),
                        consin:consin,
                        cant_tras:cant_tras
                        },
                        async: false,
                        statusCode:{
                            404: function(data){
                                swal("Error", "404", "error");
                            },
                            500: function(){
                                swal("Error", "500", "error"); 
                            }
                        },
                        success:function(data){
                            $('#modal_traspasos').modal('close');
                            if (tipos==1) {
                                datos_equipos();
                            }
                            if (tipos==2) {
                                datos_accesorios();
                            }
                            if (tipos==3) {
                                datos_refacciones();
                            }
                        }
                });
            }else{
                toastr["warning"]("Agregue un motivo del traspaso", "Alerta!");
            }
        }else{
            toastr["warning"]("Seleccione Bodega valida");
        }
    }
    
}
function traspasarss(id,bodega,cantidad,nombre){
    $('.refaccionesss').html(nombre);
    $('#cantidadss').val(cantidad);
    $('#serieIdss').val(id);
    $('#bodegaoss').val(bodega).prop('disabled', false).trigger("chosen:updated");
    $('#modal_traspasosss').modal();
    $('#modal_traspasosss').modal('open');

    var tipov=$('#bodegaoss option:selected').data('bodegatipo');
    $('.bodegatipo_1').prop('disabled', true).trigger("chosen:updated");
    $('.bodegatipo_2').prop('disabled', true).trigger("chosen:updated");
    console.log('tipov_'+tipov);
    if(tipov==1){
        $('.bodegatipo_1').prop('disabled', false).trigger("chosen:updated");
        console.log('tipov1');
    }
    if(tipov==2){
        $('.bodegatipo_2').prop('disabled', false).trigger("chosen:updated");
        console.log('tipov2');
    }
    setTimeout(function(){ 
        $('#bodegaoss').prop('disabled', true).trigger("chosen:updated");
        $('#bodeganewss option[value="'+bodega+'"]').prop('disabled', true).trigger("chosen:updated");
    }, 1000);
}
function aceptar_traspasoss(){
    var bodeganewss = $('#bodeganewss option:selected').val();
    var cantidad=$('#cantidadss').val();
    var cantidadnew = $('#cantidadnewss').val();
    var motivo=$('#motivo2').val();
    if(bodeganewss>0){
        if(motivo!=''){
            if (parseFloat(cantidadnew)>0&&parseFloat(cantidadnew)<=parseFloat(cantidad)) {
                $.ajax({
                    type:'POST',
                    url: base_url+'Series/traspasarssr',
                    data: {
                        serieid: $('#serieIdss').val(),
                        cantidad: cantidadnew,
                        bodega:$('#bodeganewss option:selected').val(),
                        bodegao:$('#bodegaoss option:selected').val(),
                        modelo:$('.modelotras2').html(),
                        motivo:motivo
                    },
                    async: false,
                    statusCode:{
                        404: function(data){
                            swal("Error", "404", "error");
                        },
                        500: function(){
                            swal("Error", "500", "error"); 
                        }
                    },
                    success:function(data){
                        $('#modal_traspasosss').modal('close');
                        datos_refacciones();
                    }
                });
            }else{
                swal("Error", "No se aceptan números negativos y/o mayor al disponible", "error");
            }
        }else{
            toastr["warning"]("Agregue un motivo del traspaso", "Alerta!");
        }
    }else{
        toastr["warning"]("Seleccione Bodega valida");
    }
}
function editarserie(id,proid){
    editarserieid=id;
    $('#modal_editarserie').modal();
    $('#modal_editarserie').modal('open');
    var serienombre=$('.editarserie_'+id).data('serienombre');
    $('.nombreeditserie').val(serienombre);
    $('#edit_eq_ser').val(proid);
}
function aceptar_edicionseries(){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Desea confirmar la edición?',
        type: 'red',
        typeAnimated: true,
        buttons: {
            confirmar: function (){
                //====================================================
                $.ajax({
                        type:'POST',
                        url: base_url+'Series/editarseries',
                        data: {
                            serieid: editarserieid,
                            serie: $('#serieedit').val(),
                            equipo: $('#edit_eq_ser option:selected').val()
                        },
                        async: false,
                        statusCode:{
                            404: function(data){
                                toastr["error"]("Error 404", "Alerta!");
                            },
                            500: function(){
                                toastr["error"]("Error 500", "Alerta!");
                            }
                        },
                        success:function(data){
                            toastr["success"]("Serie editada", "Correcto!");
                            $('#modal_editarserie').modal('close');
                            tipo_tabla();
                        }
                    });
                //=========================================================
            },
            cancelar: function () 
            {
            }
        }
    });
}
function eliminarserie(serieid,producto){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de Eliminar esta Serie? El cambio será irreversible<br>\
                    Se necesita permisos de administrador<br>'+
                    '<input type="password" placeholder="Contraseña" id="contrasena" class="name form-control" required /><br>',
        type: 'red',
        typeAnimated: true,
        buttons: 
        {
            confirmar: function (){
                var pass=$('#contrasena').val();
                if (pass!='') {
                     $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Sistema/solicitarpermiso",
                        data: {
                            pass:pass
                        },
                        success: function (response){
                                var respuesta = parseInt(response);
                                if (respuesta==1) {
                //==========================================================================
                    $.ajax({
                        url: base_url+"index.php/Series/eliminarserie/"+serieid,
                        success: function (response) {
                            toastr["success"]("Serie Eliminada", "Correcto!");
                                tipo_tabla();
                        },
                        error: function(response){
                            toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema");
                            
                        }
                    });
                //=========================================================================
                                }else{
                                    toastr["warning"]("No tiene permisos"); 
                                }
                        },
                        error: function(response){
                            toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema");
                        }
                    });        
                }else{
                    toastr["warning"]("Ingrese una contraseña"); 
                }

            },
            cancelar: function () 
            {
                
            }
        }
    });
}
function eliminarserieacc(serieid){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de Eliminar esta Serie? El cambio será irreversible<br>\
                    Se necesita permisos de administrador<br>'+
                    '<input type="password" placeholder="Contraseña" id="contrasena" class="name form-control" required /><br>',
        type: 'red',
        typeAnimated: true,
        buttons: 
        {
            confirmar: function (){
                var pass=$('#contrasena').val();
                if (pass!='') {
                     $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Sistema/solicitarpermiso",
                        data: {
                            pass:pass
                        },
                        success: function (response){
                                var respuesta = parseInt(response);
                                if (respuesta==1) {
                //========================================================================
                $.ajax({
                    url: base_url+"index.php/Series/eliminarserieaccesorio/"+serieid,
                    success: function (response) {
                        toastr["success"]("Serie Eliminada", "Correcto!");
                        tipo_tabla();
                    },
                    error: function(response){
                        toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema");             
                    }
                });
                //==================================================================
                            }else{
                                toastr["warning"]("No tiene permisos");                   
                            }
                        },
                        error: function(response){
                            toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema");
                        }
                    });        
                }else{
                    toastr["warning"]("Ingrese una contraseña");
                }
            },
            cancelar: function () 
            {
                
            }
        }
    });
}
function ubicarseries(serie,tipo,disponible){
    var seriename=$('.ubicarseries_'+serie).data('serie');
    $('.infoserie').html(seriename);
    $('#modal_rastrearseries').modal();
    $('#modal_rastrearseries').modal('open');
    setTimeout(function(){
        if(disponible==1){
            $('.liberarserie').hide();
        }else{
            $('.liberarserie').show();
        }
    }, 500);
    
    $.ajax({
        type:'POST',
        url: base_url+'Series/historialseries',
        data: {
            serieid: serie,
            tipo: tipo
        },
        async: false,
        statusCode:{
            404: function(data){
                toastr["error"]("Error 404", "Alerta!");
            },
            500: function(){
                toastr["error"]("Error 500", "Alerta!");
            }
        },
        success:function(data){
            $('.historialdeseries').html(data);
        }
    });
}
function liberarserie(serie){
    if($('.equipoactivo').length==0){
        $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Confirma la liberacion de la serie?<br>Se necesita permisos de administrador<br><input type="password" placeholder="Contraseña" id="contrasenals" class="name form-control" required />',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var pass = $('#contrasenals').val();
                //===========================
                    $.ajax({
                        type:'POST',
                        url: base_url+'Series/liberarserie',
                        data: {
                            serieid: serie,
                            pass:pass
                        },
                        async: false,
                        statusCode:{
                            404: function(data){
                                toastr["error"]("Error 404", "Alerta!");
                            },
                            500: function(){
                                toastr["error"]("Error 500", "Alerta!");
                            }
                        },
                        success:function(data){
                            var permiso=parseInt(data);
                            if(permiso==0){
                                toastr["error"]("No se tiene permiso", "Alerta!");
                            }else{
                                tipo_tabla();
                                $('#modal_rastrearseries').modal('close');  
                            }
                            
                        }
                    });
                //===========================
            },
            cancelar: function (){
                
            }
        }
    });
            
    }else{
        $('.equipoactivo').addClass('intermitente');
        $.alert({
                boxWidth: '30%',
                useBootstrap: false,
                title: 'Atención!',
                content: 'Equipo activo, Finalice contrato o elimine equipo del contrato'}
            );
    }
    
}
function liberarseriea(serie){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Confirma la liberacion de la serie?<br>Se necesita permisos de administrador<br><input type="password" placeholder="Contraseña" id="contrasenals" class="name form-control" required />',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var pass = $('#contrasenals').val();
                //===========================
                    $.ajax({
                        type:'POST',
                        url: base_url+'Series/liberarseriea',
                        data: {
                            serieid: serie,
                            pass:pass
                        },
                        async: false,
                        statusCode:{
                            404: function(data){
                                toastr["error"]("Error 404", "Alerta!");
                            },
                            500: function(){
                                toastr["error"]("Error 500", "Alerta!");
                            }
                        },
                        success:function(data){
                            var permiso=parseInt(data);
                            if(permiso==0){
                                toastr["error"]("No se tiene permiso", "Alerta!");
                            }else{
                                tipo_tabla();
                                $('#modal_rastrearseries').modal('close'); 
                            }
                            
                        }
                    });
                //===========================
            },
            cancelar: function (){
                
            }
        }
    });
    
}
function detallev(id,tipo){
    if (tipo==0) {
        window.open(base_url+"Prefactura/view/"+id, "Prefactura", "width=780, height=612");
    }else{
        window.open(base_url+"Prefactura/viewc/"+id, "Prefactura", "width=780, height=612");
    }
}
toastr.options = {
  "closeButton": false,
  "debug": false,
  "newestOnTop": false,
  "progressBar": true,
  "positionClass": "toast-top-right",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "1000",
  "timeOut": "5000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}
function resguardar(serie,tipo){
    if(tipo==0){
        var res='Resguardar';
    }else{
        var res='Liberar';
    }
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Confirma '+res+' el producto?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                //===========================
                    $.ajax({
                        type:'POST',
                        url: base_url+'Series/resguardareq',
                        data: {
                            serie: serie,
                            tipo:tipo
                        },
                        async: false,
                        statusCode:{
                            404: function(data){
                                toastr["error"]("Error 404", "Alerta!");
                            },
                            500: function(){
                                toastr["error"]("Error 500", "Alerta!");
                            }
                        },
                        success:function(data){
                            tipo_tabla();
                            toastr["success"]("Realizado");
                            
                            
                        }
                    });
                //===========================
            },
            cancelar: function (){
                
            }
        }
    });
}
function resguardar_acc(serie,cant,consin){
    if(consin==0){
        var res='';
    }else{
        var res='readonly';
        cant=1;
    }
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Confirma resguardar stock?<br><label>Cantidad</label><input type="number" value="'+cant+'" class="form-control-bmz " '+res+' id="cant_reguardar">',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var can_r=$('#cant_reguardar').val();
                //===========================
                    $.ajax({
                        type:'POST',
                        url: base_url+'Series/resguardar_acc',
                        data: {
                            serie: serie,
                            cant:can_r
                        },
                        async: false,
                        statusCode:{
                            404: function(data){
                                toastr["error"]("Error 404", "Alerta!");
                            },
                            500: function(){
                                toastr["error"]("Error 500", "Alerta!");
                            }
                        },
                        success:function(data){
                            tipo_tabla();
                            toastr["success"]("Realizado");
                            
                            
                        }
                    });
                //===========================
            },
            cancelar: function (){
                
            }
        }
    });
}
function resguardar_acc_s(serie,reg){
    if(reg>0){
        var label='¿Confirma liberar la serie?';
    }else{
        var label='¿Confirma resguardar la serie?';
    }
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: label,
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var can_r=$('#cant_reguardar').val();
                //===========================
                    $.ajax({
                        type:'POST',
                        url: base_url+'Series/resguardar_acc_s',
                        data: {
                            serie: serie,
                            reg:reg
                        },
                        async: false,
                        statusCode:{
                            404: function(data){
                                toastr["error"]("Error 404", "Alerta!");
                            },
                            500: function(){
                                toastr["error"]("Error 500", "Alerta!");
                            }
                        },
                        success:function(data){
                            tipo_tabla();
                            toastr["success"]("Realizado");
                            
                            
                        }
                    });
                //===========================
            },
            cancelar: function (){
                
            }
        }
    });
}
function resguardar_ref(serie,cant,consin){
    if(consin==0){
        var res='';
    }else{
        //var res='readonly';
        var res='';
        cant=1;
    }
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Confirma resguardar stock?<br><label>Cantidad</label><input type="number" value="'+cant+'" class="form-control-bmz " '+res+' id="cant_reguardar">',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var can_r=$('#cant_reguardar').val();
                //===========================
                    $.ajax({
                        type:'POST',
                        url: base_url+'Series/resguardar_ref',
                        data: {
                            serie: serie,
                            cant:can_r
                        },
                        async: false,
                        statusCode:{
                            404: function(data){
                                toastr["error"]("Error 404", "Alerta!");
                            },
                            500: function(){
                                toastr["error"]("Error 500", "Alerta!");
                            }
                        },
                        success:function(data){
                            tipo_tabla();
                            toastr["success"]("Realizado");
                            
                            
                        }
                    });
                //===========================
            },
            cancelar: function (){
                
            }
        }
    });
}
function resguardar_ref_s(serie,reg){
    if(reg>0){
        var label='¿Confirma liberar la serie?';
    }else{
        var label='¿Confirma resguardar la serie?';
    }
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: label,
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var can_r=$('#cant_reguardar').val();
                //===========================
                    $.ajax({
                        type:'POST',
                        url: base_url+'Series/resguardar_ref_s',
                        data: {
                            serie: serie,
                            reg:reg
                        },
                        async: false,
                        statusCode:{
                            404: function(data){
                                toastr["error"]("Error 404", "Alerta!");
                            },
                            500: function(){
                                toastr["error"]("Error 500", "Alerta!");
                            }
                        },
                        success:function(data){
                            tipo_tabla();
                            toastr["success"]("Realizado");
                            
                            
                        }
                    });
                //===========================
            },
            cancelar: function (){
                
            }
        }
    });
}