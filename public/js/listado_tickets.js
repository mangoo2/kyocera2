var base_url = $('#base_url').val();
var table;
var ticketid;
var perfilid = $('#perfilid').val();
var idpersonal = $('#idpersonal').val();
$(document).ready(function() {	
	table = $('#tabla_facturacion').DataTable();
	loadtable();

    $('.responderticket').click(function(event) {
        $.ajax({
            type:'POST',
            url: base_url+'Generales/ticketresponder',
            data: {
                ticketid: ticketid,
                contenido: $('#respuesta').val()           
                },
                async: false,
                statusCode:{
                    404: function(data){
                        swal("Error", "404", "error");
                    },
                    500: function(){
                        swal("Error", "500", "error"); 
                    }
                },
                success:function(data){
                    $('#respuesta').val('');
                    $.ajax({
                        type:'POST',
                        url: base_url+'Generales/ticketview',
                        data: {
                            ticketid: ticketid            
                            },
                            async: false,
                            statusCode:{
                                404: function(data){
                                    swal("Error", "404", "error");
                                },
                                500: function(){
                                    swal("Error", "500", "error"); 
                                }
                            },
                            success:function(data){
                              $('.descripciongeneral').html(data);
                              $('.descripciongeneral img').addClass('materialboxed');
                                $('.materialboxed').materialbox();
                            }
                    });
                }
        });
    });
});
function loadtable(){
    var personals=$('#personals option:selected').val();
	table.destroy();
	table = $('#tabla_facturacion').DataTable({
        stateSave: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url+"index.php/Tickes/getlisttickets",
            type: "post",
            "data": {
                'personal':personals,
            },
        },
        "columns": [
            {"data": "id"},
            {"data": "personalId",
                render:function(data,type,row){
                    var html ='';
                        html=row.nombre+' '+row.apellido_paterno+' '+row.apellido_materno;
                    return html;
                }
            },
            {"data": "tcontenido"},
            {"data": "status",
                render:function(data,type,row){
                    var html='';
                    if(row.status==0){
                        html='<span class="status_tickets red" ondblclick="respuestapop('+row.id+')">Solicitud</span>';
                    }else if(row.status==1){
                        html='<span class="status_tickets orange" ondblclick="respuestapop('+row.id+')">Revisión</span>';
                    }else if(row.status==2){
                        html='<span class="status_tickets green" ondblclick="respuestapop('+row.id+')">Finalizado</span>';
                    }else{
                    	html='';
                    }
                    return html;
                }
            },
            {"data": "estatus_resp",
                render:function(data,type,row){
                    var html='';
                    if(row.estatus_resp==1){
                        html+='<div class="status_tickets orange" style="width: 110px;">Respuesta Mangoo</div>';
                    }else if(row.estatus_resp==2){
                        html+='<div class="status_tickets red" style="width: 110px;">Respuesta Kyocera</div>';
                    }
                    return html;
                }
            },
            {"data": "tipo",
                render:function(data,type,row){
                    var html='';
                    if(row.tipo==1){
                        html='Error de sistema';
                    }else if(row.tipo==2){
                        html='Error de red';
                    }else if(row.tipo==3){
                        html='Capacitación';
                    }else if(row.tipo==4){
                        html='Olvido de proceso';
                    }else if(row.tipo==5){
                        html='Error de servidor';
                    }else if(row.tipo==6){
                        html='Cambio o agregado';
                    }else{
                        html='';
                    }
                    return html;
                }
            },
            {"data": "reg"},

            {"data": null,
                 render:function(data,type,row){
                    $('.tooltipped').tooltip();
                    var html='<div class="btns-factura">';
                    
                        html+='<a \
		                        class="b-btn b-btn-primary tooltipped" \
		                         onclick="respuesta('+row.id+')"  \
		                        data-position="top" data-delay="50" data-tooltip="Descripcion"\
		                      ><i class="fas fa-file-alt fa-2x"></i>\
                      		</a> ';
                        if(idpersonal==1){
                            html+='<a \
                                class="b-btn b-btn-primary tooltipped" \
                                 onclick="status('+row.id+')" \
                                data-position="top" data-delay="50" data-tooltip="Cambiar status"\
                              ><i class="material-icons">confirmation_number</i>\
                            </a> ';
                            html+='<a \
                                class="b-btn b-btn-primary tooltipped" \
                                 onclick="status2('+row.id+')" \
                                data-position="top" data-delay="50" data-tooltip="Cambiar status"\
                              ><i class="material-icons">check_box</i>\
                            </a> ';
                        }
		                
                    
                    html+='</div>';
                    return html;
                }
            }
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[25, 50, 100], [25, 50,100]],
        "searching": false
        
    });
}
function respuestapop(id){
    window.open(base_url+"Tickes/Tickesiframeresponse/"+id, "Ticket", "width=400, height=612");
}
function respuesta(id){
    ticketid=id;
    $('#modaldescripcion').modal();
    $('#modaldescripcion').modal('open');
    $.ajax({
        type:'POST',
        url: base_url+'Generales/ticketview',
        data: {
            ticketid: id            
            },
            async: false,
            statusCode:{
                404: function(data){
                    swal("Error", "404", "error");
                },
                500: function(){
                    swal("Error", "500", "error"); 
                }
            },
            success:function(data){
              $('.descripciongeneral').html(data);
              $('.descripciongeneral img').addClass('materialboxed');
              $('.materialboxed').materialbox();
            }
    });
}
function status(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Cambio de Estatus!',
        content: '<select class="browser-default form-control-bmz" id="newestatus"><option value="0">Solicitud</option><option value="1">Revisión</option><option value="2">Finalizado</option></select>',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                    var status=$('#newestatus option:selected').val();
                     $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Generales/ticketupdate",
                        data: {
                            ticket:id,
                            status:status
                        },
                        success: function (response){
                            loadtable();
                                $.alert({
                                    boxWidth: '30%',
                                    useBootstrap: false,
                                    title: 'Hecho!',
                                    content: 'Se actualizo estatus'});
                        },
                        error: function(response){
                            $.alert({
                                    boxWidth: '30%',
                                    useBootstrap: false,
                                    title: 'Error!',
                                    content: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema'});   
                             
                        }
                    });
                    
                
                
            },
            cancelar: function () 
            {
                
            }
        }
    });
}
function status2(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Tipo!',
        content: '<select class="browser-default form-control-bmz" id="newestatus2">\
                    <option value="1">Error de sistema</option>\
                    <option value="2">Error de red</option>\
                    <option value="3">Capacitación</option>\
                    <option value="4">Olvido de proceso</option>\
                    <option value="5">Error de servidor</option>\
                    <option value="6">Cambio o agregado</option>\
                    </select>',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                    var status=$('#newestatus2 option:selected').val();
                     $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Generales/ticketupdate2",
                        data: {
                            ticket:id,
                            status:status
                        },
                        success: function (response){
                            loadtable();
                                $.alert({
                                    boxWidth: '30%',
                                    useBootstrap: false,
                                    title: 'Hecho!',
                                    content: 'Se actualizo tipo'});
                        },
                        error: function(response){
                            $.alert({
                                    boxWidth: '30%',
                                    useBootstrap: false,
                                    title: 'Error!',
                                    content: 'Algo salió mal, intente de nuevo o contacte al administrador del sistema'});   
                             
                        }
                    });
                    
                
                
            },
            cancelar: function () 
            {
                
            }
        }
    });
}