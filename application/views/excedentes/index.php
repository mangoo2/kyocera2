<style type="text/css">
  td{
    font-size: 12px;
  }
  .preview_iframe{
    text-align: center;
  }
  .ifrafac{
    border: 0px;
    width: 100%;
    min-height: 350px;
  }
  .removercon{
    padding: 0px;
    float: right;
  }
  .removercon i{
    margin: 0px;
  }
</style>
<!-- START CONTENT -->
<input type="hidden" id="codigoenvio" value="<?php echo date('YmdGis');?>">
<section id="content">
  <!--start container-->
    <div class="container">
      <div class="section">
        <div class="row">
          <div class="col s9"><h4 class="caption">Generación de estado de cuenta</h4></div>
          <div class="col s3"></div>
        </div>
        <div class="card-panel">
          <div class="row">
            <div class="col s3">
              <label>Seleccione Cliente</label>
              <select class="browser-default chosen-select" id="idcliente" ></select>
            </div>
            
            <div class="col s3">
              <label>Contratos</label>
              <select id="pcontrato" class="browser-default form-control-bmz" >
                <option value="0">Todos</option>
                
              </select>
            </div>
            <div class="col s3">
              <button class="b-btn b-btn-primary" onclick="generar()" style="margin-top: 20px;" title="Realizar consulta">Agregar</button>
            </div>
            <div class="col s3" style="text-align: end;">
              <button class="b-btn b-btn-primary" onclick="generarcotiza()">Generar cotización</button>
            </div>
            
          </div>
          <div class="row">
            <div class="col s12">
                  <ul class="collapsible popout collapsible-accordion infocontratos" data-collapsible="expandable">
                     
                  </ul>
            </div>
          </div>
          <div class="row">
            <div class="col s4">
              
            </div>
            <div class="col s4">
              <b>Total excedentes: <span class="total_excedentes" style="color:red">$ 0.00</span></b>
            </div>
            <div class="col s4">
              <b>Total por facturar: <span class="total_facturar" style="color:red">$ 0.00</span></b>
            </div>
          </div>
          <div class="row">
            <div class="col s8 preview_iframe">
              
            </div>
            <div class="col s4">
              <button class="b-btn b-btn-warning" onclick="generarestado()">Generar Reporte</button>
              <button class="b-btn b-btn-warning" onclick="enviarestado()">Enviar por correo electrónico</button>

            </div>
          </div>
        </div>


      
      </div>  
    </div>
</section>




<div id="modal_servicio" class="modal">
    <div class="modal-content">
      <div class="col s12">
        <label>Firma del ejecutivo</label>
        <select class="browser-default form-control-bmz" id="firma_m_s">
          <?php foreach ($perfirma->result() as $itemf) {
            echo '<option value="'.$itemf->personalId.'">'.$itemf->nombre.' '.$itemf->apellido_paterno.' '.$itemf->apellido_materno.' / '.$itemf->email.' / '.$itemf->celular.'</option>';
          }?>
        </select>
      </div>
      <div class="col s12">
        <label>Copia oculta (BBC)</label>
        <select class="browser-default form-control-bmz" id="bbc_m_s"><option value="0"></option>
          <?php foreach ($perfirma->result() as $itemf) {
            echo '<option value="'.$itemf->personalId.'">'.$itemf->nombre.' '.$itemf->apellido_paterno.' '.$itemf->apellido_materno.' / '.$itemf->email.' / '.$itemf->celular.'</option>';
          }?>
        </select>
      </div>
      <div class="col s12">
          <label>Correo envio</label>
          <div class="m_s_contac"></div>
        </div>
      <div class="row">
        <div class="col s12">
          <label>Asunto</label>
          <input class="form-control-bmz" id="ms_asucto2" value="Servicio">
        </div>
        <div class="col s7">
          <label>Comentario</label>
          <textarea class="form-control-bmz" id="ms_coment2" style="min-height: 150px;"></textarea>
        </div>
        <div class="col s5 input_envio_s">
        </div>
        
      </div>
      <div class="row"><div class="col s12 addbtnservicio"></div></div>
 
    </div>
    <div class="row">
      <div class="col m12" align="right">
        <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
      </div>
    </div>        
</div>