var base_url =$('#base_url').val();
var table_reporte_1;
var table_reporte_2;
var table_reporte_3;
var table_reporte_4;
$(document).ready(function($) {
	table_reporte_1=$('#table_reporte_1').DataTable();
	table_reporte_2=$('#table_reporte_2').DataTable();
	table_reporte_3=$('#table_reporte_3').DataTable();
	table_reporte_4=$('#table_reporte_4').DataTable();
});
function generarreporte(){
    var form =$('#form_reporte');

	var tiporeporte =$('#tiporeporte option:selected').val();
    var fechainicio = $('#fechainicio').val();
    var fechafin = $('#fechafinal').val();


    if(form.valid()){
    	if (tiporeporte==0) {
    		$( ".reporte_1" ).hide( "fast");
    		$( ".reporte_2" ).hide( "fast");
    		$( ".reporte_3" ).hide( "fast");
    		$( ".reporte_4" ).hide( "fast");
    	}else if(tiporeporte==1){
    		$(".reporte_1").show( 2000 );
    		$( ".reporte_2" ).hide( "fast");
    		$( ".reporte_3" ).hide( "fast");
    		$( ".reporte_4" ).hide( "fast");
    		tecnicos();
    	}else if(tiporeporte==2){
    		$(".reporte_2").show( 2000 );
    		$( ".reporte_1" ).hide( "fast");
    		$( ".reporte_3" ).hide( "fast");
    		$( ".reporte_4" ).hide( "fast");
    		rservicios();
    	}else if(tiporeporte==3){
    		$(".reporte_3").show( 2000 );
    		$( ".reporte_1" ).hide( "fast");
    		$( ".reporte_2" ).hide( "fast");
    		$( ".reporte_4" ).hide( "fast");
    		atencioncorrectivo();
    	}else if(tiporeporte==4){
    		$(".reporte_4").show( 2000 );
    		$( ".reporte_2" ).hide( "fast");
    		$( ".reporte_3" ).hide( "fast");
    		$( ".reporte_1" ).hide( "fast");
    		fallas();
    	}else if(tiporeporte==5){
            var url=base_url+'Listaservicios/exportar2?tipo=0&tecnico=0&cliente=0&finicio='+fechainicio+'&ffin='+fechafin+'&tiposer=0&status=0&zona=0';
                //url+='&viewexcel=1';
            window.open(url, '_blank');
        }else if(tiporeporte==6){
            window.open(base_url+'Listaservicios/exportar6?finicio='+fechainicio+'&ffin='+fechafin, '_blank');
        }else if(tiporeporte==7){
            window.open(base_url+'Listaservicios/exportar7?finicio='+fechainicio+'&ffin='+fechafin, '_blank');
        }else if(tiporeporte==8){
            var url=base_url+'Listaservicios/ser_devolucion?finicio='+fechainicio+'&ffin='+fechafin;
                //url+='&viewexcel=1';
            window.open(url, '_blank');
        }
    }
}
function tecnicos(){
	var fechainicio = $('#fechainicio').val();
	var fechafinal = $('#fechafinal').val();
	$.ajax({
        type:'POST',
        url: base_url+'Rservicios/tecnicos',
        data: {
            fi: fechainicio,
            ff: fechafinal
            },
            async: false,
            statusCode:{
                404: function(data){
                    swal("Error", "404", "error");
                },
                500: function(){
                    swal("Error", "500", "error"); 
                }
            },
            success:function(data){
            	
            	table_reporte_1.destroy();
            	$('.tbody_reporte_1').html(data);
            	table_reporte_1=$('#table_reporte_1').DataTable();
            }
        });
}
function atencioncorrectivo(){
	var fechainicio = $('#fechainicio').val();
	var fechafinal = $('#fechafinal').val();
	$.ajax({
        type:'POST',
        url: base_url+'Rservicios/reporteatencioncorrectivo',
        data: {
            fi: fechainicio,
            ff: fechafinal
            },
            async: false,
            statusCode:{
                404: function(data){
                    swal("Error", "404", "error");
                },
                500: function(){
                    swal("Error", "500", "error"); 
                }
            },
            success:function(data){
            	
            	table_reporte_3.destroy();
            	$('.tbody_reporte_3').html(data);
            	table_reporte_3=$('#table_reporte_3').DataTable();
            }
        });
}
function fallas(){
	var fechainicio = $('#fechainicio').val();
	var fechafinal = $('#fechafinal').val();
	$.ajax({
        type:'POST',
        url: base_url+'Rservicios/reportefallas',
        data: {
            fi: fechainicio,
            ff: fechafinal
            },
            async: false,
            statusCode:{
                404: function(data){
                    swal("Error", "404", "error");
                },
                500: function(){
                    swal("Error", "500", "error"); 
                }
            },
            success:function(data){
            	
            	table_reporte_4.destroy();
            	$('.tbody_reporte_4').html(data);
            	table_reporte_4=$('#table_reporte_4').DataTable();
            }
        });
}
function rservicios(){
	var fechainicio = $('#fechainicio').val();
	var fechafinal = $('#fechafinal').val();
	$.ajax({
        type:'POST',
        url: base_url+'Rservicios/rservicios',
        data: {
            fi: fechainicio,
            ff: fechafinal
            },
            async: false,
            statusCode:{
                404: function(data){
                    swal("Error", "404", "error");
                },
                500: function(){
                    swal("Error", "500", "error"); 
                }
            },
            success:function(data){
            	
            	table_reporte_2.destroy();
            	$('.tbody_reporte_2').html(data);
            	table_reporte_2=$('#table_reporte_2').DataTable();
            }
        });
}
function calcu_mes(){
    var tiporeporte =$('#tiporeporte option:selected').val();
    var fini=$('#fechafinal').val();
    //var idcliente = $('#idcliente option:selected').val();
    
    if(fini!=''){
        //if(idcliente==0){
            var menosmeses=1;
        //}else{
        //    var menosmeses=12;
        //} 
        if(tiporeporte==8){
            menosmeses=4;
        }
        var fechaFinal = moment(fini, "YYYY-MM-DD").subtract(menosmeses, 'months');

        // Formatear la fecha en formato YYYY-MM-DD
        var fechaInicialFormateada = fechaFinal.format("YYYY-MM-DD");

        // Actualizar el elemento con la fecha inicial
        console.log(fechaInicialFormateada);
        $('#fechainicio').prop('min',fechaInicialFormateada);
        $('#fechainicio').prop('max',fini);
    }
}