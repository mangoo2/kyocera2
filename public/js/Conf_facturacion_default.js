var base_url = $('#base_url').val();
$(document).ready(function($) {
	$('.unidaddefault').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un Unidad',
          ajax: {
            url: base_url+'Configuracionrentas/searchunidadsat',
            dataType: "json",
            data: function (params) {
              var query = {
                search: params.term,
                type: 'public'
              }
              return query;
          },
          processResults: function(data){
            var clientes=data;
            var itemscli = [];
            data.forEach(function(element) {
                    itemscli.push({
                        id: element.Clave,
                        text: element.Clave+' / '+element.nombre
                    });
                });
                return {
                    results: itemscli
                };        
          },  
        }
    });
    $('.conceptodefault').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un Consepto',
          ajax: {
            url: base_url+'Configuracionrentas/searchconceptosat',
            dataType: "json",
            data: function (params) {
              var query = {
                search: params.term,
                type: 'public'
              }
              return query;
          },
          processResults: function(data){
            var clientes=data;
            var itemscli = [];
            data.forEach(function(element) {
                    itemscli.push({
                        id: element.Clave,
                        text: element.Clave+' / '+element.nombre
                    });
                });
                return {
                    results: itemscli
                };        
          },  
        }
    });
    $('.serviciodefault_save').click(function(event) {
    	//==============================================
    		var tableus = $("#tableus tbody > tr");
            var DATAus  = [];
            tableus.each(function(){         
                item = {};                    
                item ["id"]   = $(this).find("input[id*='id']").val();
                item ["unidadsat"]  = $(this).find("select[id*='unidadsat'] option:selected").val();
                item ["unidadsat_text"]  = $(this).find("select[id*='unidadsat'] option:selected").text();
                item ["conceptosat"]  = $(this).find("select[id*='conceptosat'] option:selected").val();
                item ["conceptosat_text"]  = $(this).find("select[id*='conceptosat'] option:selected").text();
                DATAus.push(item);
            });
            INFOa  = new FormData();
            aInfoa   = JSON.stringify(DATAus);
        //========================================
        var datos='unidades='+aInfoa;
        console.log(datos);
        $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Conf_facturacion/updateuni_ser_def",
                    data: datos,
                    success: function (response){
                        
                            swal({
                                title:"Éxito!", 
                                text:'Actualizado con éxito', 
                                type:"success",
                                showCancelButton: false
                               });
                        
                        
                        
                    },
                    error: function(response){
                        alertfunction('Atención!','Algo salió mal, intente de nuevo o contacte al administrador del sistema'); 
                    }
                });
    });
});
function alertfunction(titlea,contenta){
    $.alert({boxWidth: '30%',useBootstrap: false,title: titlea,theme: 'bootstrap',content: contenta});  
}