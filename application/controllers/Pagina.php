<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pagina extends CI_Controller {
    public function __construct()     {
        parent::__construct();

        $this->load->model('ModeloCatalogos');
        $this->load->model('Modeloaccesorios');
        $this->load->helper('url');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->submenu=69;
        if($this->session->userdata('logeado')==true){
            $this->perfilid =$this->session->userdata('perfilid');
            $this->idpersonal = $this->session->userdata('idpersonal');
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,$this->submenu);// 69 es el id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
        }else{
            redirect('login'); 
        }
    }

    function index(){
        $data['MenusubId']=$this->submenu;
        $data['perfilid']=$this->perfilid;
            $this->load->view('header');
            $this->load->view('main');
            $this->load->view('configuraciones/pagina',$data);
            $this->load->view('footer');
            $this->load->view('configuraciones/paginajs');
    }
    function baner_imagenes_multiple(){
        $idequipo=$_POST['idequipo'];
      $data = [];
   
      $count = count($_FILES['files']['name']);
      $output = [];
      for($i=0;$i<$count;$i++){
    
        if(!empty($_FILES['files']['name'][$i])){
    
          $_FILES['file']['name'] = $_FILES['files']['name'][$i];
          $_FILES['file']['type'] = $_FILES['files']['type'][$i];
          $_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
          $_FILES['file']['error'] = $_FILES['files']['error'][$i];
          $_FILES['file']['size'] = $_FILES['files']['size'][$i];
          $DIR_SUC=FCPATH.'uploads/baner';

          $config['upload_path'] = $DIR_SUC; 
          //$config['allowed_types'] = 'docx|xlsx|pdf|doc|xls|pptx';
          $config['allowed_types'] = '*';

          //$config['allowed_types'] = 'docx|xlsx|pdf|jpg|jpeg|png|gif';
          $config['max_size'] = 5000;
          $file_names=date('Ymd_His').'_'.rand(0, 500);
          //$config['file_name'] = $file_names.'_'.str_replace(' ', '_', $_FILES['files']['name'][$i]);
          $config['file_name'] = $file_names;
          //$config['file_name'] = $file_names;
   
          $this->load->library('upload',$config); 
    
          if($this->upload->do_upload('file')){
            $uploadData = $this->upload->data();
            $filename = $uploadData['file_name'];
            $filenametype = $uploadData['file_ext'];
   
            $data['totalFiles'][] = $filename;
            //$this->ModeloGeneral->tabla_inserta('equipo_imgs',array('idequipo'=>$idequipo,'imagen'=>$filename,'filetype'=>$filenametype,'usuario'=>$this->idpersonal));
            $this->ModeloCatalogos->Insert('baner',array('imagen'=>$filename));
          }else{
            $data = array('error' => $this->upload->display_errors());
            log_message('error', json_encode($data)); 
          }
        }
   
      }
      echo json_encode($output);
    }
    function viewimages(){
        $params = $this->input->post();
        $id=$params['idequipo'];
        $resultados=$this->ModeloCatalogos->getselectwheren('baner',array('activo'=>1));
        $html='';
                foreach ($resultados->result() as $item) {
                    $html.='<div class="card_img"><div class="card_img_c"><img class="materialboxed" src="'.base_url().'uploads/baner/'.$item->imagen.'"></div><div class="card_img_a">
                                <a class="btn-floating red tooltipped" data-position="top" data-delay="50" data-tooltip="Eliminar" onclick="deleteimg('.$item->id.')"><i class="material-icons">delete_forever</i></a></div></div>';
                }
        echo $html;
    }
    function deleteimgbaner(){
        $params = $this->input->post();
        $id=$params['idequipo'];
        $this->ModeloCatalogos->updateCatalogo('baner',array('activo'=>0),array('id'=>$id));
    }
    


    
}
?>
