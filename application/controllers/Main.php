<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {
    function __construct()    {
        parent::__construct();
        $this->load->model('login_model');
        $this->load->model('ModeloCatalogos');
        $this->load->model('ConfiguracionDocumentos_Model');
        $this->load->model('Configuraciones_model');
        $this->load->model('ModeloAsignacion');
        $this->load->model('ModeloGeneral');
        $this->load->helper('url');
        date_default_timezone_set('America/Mexico_City');
        $this->fechacompleta = date('Y-m-d H:i:s');
        $this->fecha = date('Y-m-d');
        $this->horac = date('H:i:s');
        $this->fechaactual=date('Y-m-d H:i:s');

        if($this->session->userdata('logeado')==true){
            $this->perfilid =$this->session->userdata('perfilid');
            $this->idpersonal = $this->session->userdata('idpersonal');
        }else{
            redirect('login');
        }
    }
	function inicio(){
        $this->ModeloCatalogos->view_session_bodegas0();
        $this->ModeloCatalogos->save_registro_stock();
        $this->ModeloCatalogos->view_session_f_metodopago();
        $hora=date('G');
        $tiempo='Hola ';
        if($hora>6 and $hora<12){
            $tiempo='Buenos días ';
        }
        if($hora>11 and $hora<19){
            $tiempo='Buenas Tardes ';
        }
        if($hora>18 and $hora<23){
            $tiempo='Buenas Noches ';
        }
        $data['bienvenida']=$tiempo.' '.$this->session->userdata('usuario');
        $data['perfilid'] = $this->perfilid;
        if($this->idpersonal==1){
            $data['idpersonal'] = 0;
        }else{
            $data['idpersonal'] = $this->idpersonal;
        }
        
        
        $data['fecha'] = $this->fecha;

        if($this->perfilid==1 or $this->perfilid==2 or $this->perfilid==4 or $this->perfilid==7){ 
            $data['view_paneles']=1;
        }else{
            $data['view_paneles']=0;
        }
        $data['fecha_pi']=date("Y-m-d",strtotime($this->fechaactual."- 1 days"));
        $data['fecha_pf']=date("Y-m-d",strtotime($this->fechaactual."+ 4 days"));

        $data['fechainicial_reg'] = date("Y-m-d",strtotime($this->fechaactual."- 3 days")); 
        //$data['fechainicial_reg'] = date("Y-m-d",strtotime($this->fechaactual."- 3 month")); 
        $data['metodorow']=$this->ModeloCatalogos->getselectwheren('f_formapago',array('activo'=>1));
        $data['personalrows']=$this->ModeloCatalogos->getselectwheren('personal',array('estatus'=>1));
        $data['list_pre_fe']=$this->ModeloGeneral->obtener_pres_pendientes_fe($this->idpersonal);
        $data['vendedor']='block';
        $this->load->view('header');
        $this->load->view('main');
        $this->load->view('panel',$data);
        $this->load->view('footerl');
        $this->load->view('jspanel');
        $this->load->view('Catalogos/catalogosjs');
        $this->load->view('facturaciones/modalpagosi');
  	}
    public function update_cotizacion(){
        $id = $this->input->post('id');
        $tipo = $this->input->post('tipo');
        $Tabla='';
        if($tipo==1){
            $Tabla='asignacion_ser_avc'; 
        }else{
            $Tabla='asignacion_ser_avr';
        }
        $data = array('notificacion' => 0);
        $where = array('asId' => $id);
        $this->ModeloCatalogos->updateCatalogo($Tabla,$data,$where);
    }
    function inicio2(){
        $data['resultzonas']=$this->ModeloCatalogos->view_session_rutas();
        $data['resultstec']=$this->Configuraciones_model->view_session_searchTecnico(); 
        $data['bodegas']=$this->ModeloCatalogos->view_session_bodegas();
        $data['configdoc']=$this->ConfiguracionDocumentos_Model->getConfiguracion(1);

        $data['metodopago']=$this->ModeloCatalogos->view_session_f_metodopago();
    }
    function darkonly(){
        if(isset($_SESSION['darkonly'])){
            if($_SESSION['darkonly']==1){
                //$_SESSION['darkonly']=0;
                //log_message('error','darkonly 0');
                
                $data = array('darkonly' => 0);
            }else{
                //$_SESSION['darkonly']=1;
                //log_message('error','darkonly 11');
                $data = array('darkonly' => 1);
            }
        }else{
            //$_SESSION['darkonly']=1;
            //log_message('error','darkonly 1');
            $data = array('darkonly' => 1);
        }
        $this->session->set_userdata($data);

    }
    function cambiarperfil($per){
            $_SESSION['perfilid']=$per;
    }
    function cambiarpersonal($per){
            $_SESSION['idpersonal']=$per;
    }
    function prueba($id){
        $result=$this->ModeloCatalogos->getselectwheren('ventas_has_detallesConsumibles',array('id'=>$id));
        $dataparams=$result->row();
        $idVentas=$dataparams->idVentas;
        echo $idVentas;
    }
    function pagosenefectivo(){
        $result = $this->ModeloGeneral->pagosefectivo('2024-11-25');
                $html='';
                $html.='<table class="table" id="table_pagos_e">';
                        $html.='<thead>';
                            $html.='<tr>';
                                $html.='<th>#</th>';
                                $html.='<th>Prefactura</th>';
                                $html.='<th>Cliente</th>';
                                $html.='<th>Pago</th>';
                                $html.='<th>Fecha de pago</th>';
                                $html.='<th>Observaciones</th>';
                                $html.='<th>Forma de pago</th>';
                                $html.='<th>Personal</th>';
                                $html.='<th></th>';
                            $html.='</tr>';
                        $html.='</thead><tbody>';
                foreach ($result->result() as $item) {
                    $btn='<a class="b-btn b-btn-primary" onclick="pago_visto('.$item->tipopago.','.$item->idpago.')"><i class="fas fa-eye"></i></a>';
                    $html.='<tr>';
                        $html.='<td>'.$item->idpago.'</td>';
                        $html.='<td>';
                            if($item->tipopago==0){
                                $html.=$item->idventa;
                            }
                            if($item->tipopago==1){
                                $html.=$item->equipo;
                            }
                            if($item->tipopago==2){
                                $html.=$item->idventa;
                            }
                            if($item->tipopago==3){
                                $html.='Contrato:'.$item->idventa;
                            }
                            if($item->tipopago==4){
                                
                            }
                        $html.='</td>';
                        $html.='<td>'.$item->empresa.'</td>';
                        $html.='<td>$ '.number_format($item->pago,2,'.',',').'</td>';
                        $html.='<td>'.$item->fecha.'</td>';
                        $html.='<td>'.$item->observacion.'</td>';
                        $html.='<td>'.$item->formapago_text.'</td>';
                        $html.='<td>'.$item->persona.'</td>';
                        $html.='<td>'.$btn.'</td>';
                    $html.='</tr>';
                }
                    $html.='</tbody><table>';
        echo $html;
    }
    function confirmarpagos(){
        $params = $this->input->post();
        $tipo = $params['tipo'];
        $id = $params['id'];
        if($tipo==0){
            $this->ModeloCatalogos->updateCatalogo('pagos_ventas',array('visto'=>1),array('idpago'=>$id));
        }
        if($tipo==1){
            $this->ModeloCatalogos->updateCatalogo('pagos_combinada',array('visto'=>1),array('idpago'=>$id));
        }
        if($tipo==2){
            $this->ModeloCatalogos->updateCatalogo('pagos_poliza',array('visto'=>1),array('idpago'=>$id));
        }
        if($tipo==3){
            $this->ModeloCatalogos->updateCatalogo('factura_prefactura_pagos',array('visto'=>1),array('fp_pagosId'=>$id));
        }
        if($tipo==4){
            $this->ModeloCatalogos->updateCatalogo('pagos_factura',array('visto'=>1),array('idpago'=>$id));
        }
    }
}