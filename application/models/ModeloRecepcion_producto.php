<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloRecepcion_producto extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    //========================
        function getListado($params){
            $idpersonal = $params['idpersonal'];
            $columns = array( 
                0=>'cantidad',
                1=>'modelo',
                2=>'noparte',
                3=>'tipo',
                4=>'entregado_status',
                5=>'entregado_fecha',
                6=>'entregado_personal',
                7=>'id'
            );
            $select="";
            foreach ($columns as $c) {
                $select.="$c, ";
            }
            $this->db->select($select);
            $this->db->from('vista_de_entregas');

            if($idpersonal>0){
                $this->db->where(array('entregado_personal'=>$idpersonal));
            }
            
            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->db->group_start();
                foreach($columns as $c){
                    $this->db->or_like($c,$search);
                }
                $this->db->group_end();  
            }            
            $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
            $this->db->limit($params['length'],$params['start']);
            //echo $this->db->get_compiled_select();
            $query=$this->db->get();
            // print_r($query); die;
            return $query;
        }
        function getListadototal($params){
            $idpersonal = $params['idpersonal'];
            $columns = array( 
                0=>'cantidad',
                1=>'modelo',
                2=>'noparte',
                3=>'tipo',
                4=>'entregado_status',
                5=>'entregado_fecha',
                6=>'entregado_personal',
                7=>'id'
            );
            
            $this->db->select('COUNT(*) as total');
            $this->db->from('vista_de_entregas');

            if($idpersonal>0){
                $this->db->where(array('entregado_personal'=>$idpersonal));
            }

            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->db->group_start();
                foreach($columnss as $c){
                    $this->db->or_like($c,$search);
                }
                $this->db->group_end();  
            }            
            
            $query=$this->db->get();
            return $query->row()->total;
        }
    //========================================================================
}