<?php 
  if($perfilid==8){
    $tipoventas='selected';
    $tipoventasdisbled='style="display:none"';
  }else{
    $tipoventas='';
    $tipoventasdisbled='';
  }
?>
<style type="text/css">
  #tabla_ventas_incompletas td,#tabla_facturacion td{
    font-size:12px; 
    padding: 4px 5px;
  }
  #tabla_ventas_incompletas th{
    font-size:12px; 
  }
  .chosen-container{
    margin-left: 0px;
  }
  .procesandoclass{
    font-weight: bold;
    border: 2px solid red;
    border-radius: 4px;
    background: white;
    z-index: 999999;
  }
  table.dataTable{
    margin: 0px !important;
  }
  #tabla_facturacion{
    width: 100% !important;
  }
  .yellowc {
    background-color: #ffeb3b !important;
  }
  .solounosusuarios{
      display: none;
    }
  <?php if($idpersonal==1 or $idpersonal==17 or $idpersonal==18 or $idpersonal==63 or $idpersonal==64){ ?>
    .solounosusuarios{
      width: 17px;
      display: block;
      float: right;
    }
  <?php } ?>
  .solounosusuarios
</style>
<input  type="password" name="password" style="display: none;">
<input  type="text" name="usuario" style="display: none;">
<input  type="date" id="fechahoy" value="<?php echo date('Y-m-d')?>" style="display: none;">
<!-- START CONTENT -->
    <section id="content">
      <!--start container-->
        <div class="container">
          <div class="row">
            <div class="col s10 m6 l6">
              <h5 class="breadcrumbs-title">Listado de Ventas</h5>
            </div>
          </div>
        </div>
        <div class="container">
          <div class="section">
            <div class="card-panel">
            <input id="base_url" type="hidden" name="base_url" value="<?php echo base_url(); ?>">
            <div id="table-datatables">
                  <div class="row">
                    <div class="col s2">
                      <label>Fecha inicio</label>
                      <input type="date" id="fechainicial_reg" class="form-control-bmz" value="<?php echo $fechainicial_reg;?>" onchange="loadtable();">
                    </div>
                  </div>
                  <div class="row">
                      <div class="col s6 m2  l2" <?php echo $tipoventasdisbled;?>>
                        <label>Estatus</label>
                        <select id="tipoventadelete" class="browser-default form-control-bmz" onchange="loadtable();">
                          <option value="1">Activas</option>
                          <option value="0">Eliminadas</option>
                        </select>
                      </div>
                      <div class="col s6 m2  l2 div_servicio" style="display: none">
                        <label>Tipo de servicio</label>
                        <select id="tipo_servicio" class="browser-default chosen-select" onchange="loadservicio();">
                          <option value="0">Todos</option>
                          <option value="1">Contrato</option>
                          <option value="2">Poliza</option>
                          <option value="3">Evento</option>
                        </select>
                      </div>
                      <div class="col s6 m2  l2">
                        <label>Clientes</label>
                        <select id="cliente" class="browser-default" onchange="loadtable();">
                          <option value="0">Seleccione</option>
                        </select>
                      </div>
                      <div class="col s6 m2  l2">
                        <label>Tipo de estatus</label>
                        <select id="tipoestatus" class="browser-default form-control-bmz" onchange="loadtable();">
                          <option value="0">Seleccione</option>
                          <option value="1">Pagadas</option>
                          <option value="2">Pendientes</option>
                        </select>
                      </div>
                      <div class="col s6 m2  l2">
                        <label>Ejecutivo</label>
                        <select id="ejecutivoselect" class="browser-default chosen-select" onchange="loadtable();">
                          <option value="0">Todos</option>
                          <?php foreach ($personalrows->result() as $item) { ?>
                            <option 
                            value="<?php echo $item->personalId;?>"
                            <?php if($item->personalId==$idpersonal){ echo 'selected';}?>
                            ><?php echo $item->nombre.' '.$item->apellido_paterno.' '.$item->apellido_paterno;?></option>
                          <?php } ?>
                        </select>
                      </div>
                      <!--<div class="col s6 m2  l2 form-check form-check-inline">
                        <input type="checkbox" name="checkbox1" id="tipoview" class="form-check-input" checked onchange="loadtableswitch()">
                      </div>-->
                  </div>
                  <div class="col s12">

                      <table id="tabla_ventas_incompletas" class="table display" cellspacing="0">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>Cliente</th>
                            <th>PRE FACTURA</th>
                            <th>Vendedor</th>
                            <th>Estatus</th>
                            <th>Fecha de<br>Creación</th>
                            <th>Fecha de<br>vencimiento</th>
                            <th>Fecha de<br>Entrega</th>
                            <th></th>
                            <th>Factura</th>
                            <th>Acciones</th>
                          </tr>
                        </thead>
                        <tbody>
                        </tbody>
                      </table>
                    </div>
                  </div>
            </div>
          </div>
          </div>  
            <!-- Modal finaliza -->
            <div id="modalFinaliza" class="modal"  >
                <div class="modal-content ">
                  <h4>Confirmación</h4>
                  <div class="col s12 m12 l6">
                        <div class="row" align="center">
                          <h5>Deseas finalizar factura</h5>
                        </div>
                  </div>
                  <input type="hidden" id="id_venta">  
                  <input type="hidden" id="tipoventa">  
                </div>    
                <div class="modal-footer">
                  <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
                  <a href="#!" class="modal-action modal-close waves-effect waves-red red btn-flat" style="color: white;" onclick="finalizar_factura()">Aceptar</a>
                </div>
            </div> 
              <!-- Modal finaliza fin-->
            <!-- Modal 1 -->
            <div id="modalDetalles" class="modal"  >
              <div class="modal-content ">
                <h4> Detalles </h4>
                <div class="col s12 m12 l6">
                      <div class="row" align="center">
                        <div class="detalles_ventas"></div>
                      </div>
                </div> 
              </div> 
              <div class="modal-footer">
                <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
              </div>
            </div>
            <!-- Modal 2 -->
            

            <!-- Modal 2 --> 
            <div id="modal_cfdi" class="modal"  > 
              <div class="modal-content "> 
                <h4 align="center">Captura CFDI</h4> 
                <br> 
                <div class="row"> 
                  <div class="input-field col s2"></div> 
                  <div class="input-field col s8"> 
                    <input type="text" id="cdfi_e"> 
                  </div> 
                  <div class="input-field col s2"></div> 
                  <input type="hidden" id="id_venta_e"> 
                  <input type="hidden" id="tipo_e"> 
                </div>  
              </div>     
              <div class="modal-footer"> 
                <a class="modal-action modal-close waves-effect waves-red gray btn-flat" onclick="update_cfdi()">Guardar</a> 
              </div> 
            </div> 
                   <!-- Modal 2 --> 
            <div id="modal_pago" class="modal"  > 
              <div class="modal-content "> 
                <div class="row"> 
                  <div class="col s6"> 
                    <h5>Registro de nuevo pago</h5> 
                  </div> 
                  <div class="col s6" align="right"> 
                    <div class="row" style="margin-bottom: 0px;">
                      <div class="col s7">
                        Monto de prefactura
                      </div>
                      <div style="color: red" class="col s5 monto_prefactura"></div>
                    </div>
                    <div class="row" style="margin-bottom: 0px;">
                      <div class="col s7">
                        Restante
                      </div>
                      <div class="col s5 restante_prefactura" style="color: red"></div>
                    </div>
                  </div> 
                </div>   
                <h6>Vendedor: <?php echo($_SESSION['usuario']); ?></h6> 
                <form class="form" id="form_pago" method="post">  
                  <input type="hidden" id="id_venta_p" name="idcompra"><!--id que se va aguardar dependiendo si es venta, renta, poliza y combinada --> 
                  <input type="hidden" id="tipo_p" name="tipo"><!-- tipo para verificar en que tabla se va insertar -->  
                  <div class="row" style="margin-bottom:0px;"> 
                    <div class=" col s3"> 
                      <label for="fecha" class="active" >Fecha</label> 
                      <input type="date" name="fecha" id="fecha_p" class="form-control-bmz"> 
                      
                    </div> 
                    <div class="col s4"> 
                      <label for="idmetodo" class="active">Método de pago</label><br> 
                      <select name="idmetodo" id="idmetodo" class="browser-default chosen-select"> 
                         <option value="1">01 Efectivo</option> 
                         <option value="2">02 Cheque nominativo</option> 
                         <option value="3">03 Transferencia electrónica de fondos</option> 
                         <option value="4">04 Tarjetas de Crédito</option> 
                         <option value="5">05 Monedero Electrónico</option> 
                         <option value="6">06 Dinero Electrónico</option> 
                         <option value="7">08 Vales de despensa</option> 
                         <option value="8">12 Dación en pago</option> 
                         <option value="9">13 Pago por subrogación</option> 
                         <option value="10">14 Pago por consignación</option> 
                         <option value="11">15 Condonación</option> 
                         <option value="12">17 Compensación</option> 
                         <option value="13">23 Novación</option> 
                         <option value="14">24 Confusión</option> 
                         <option value="15">25 Remisión de deuda</option> 
                         <option value="16">26 Prescripción o caducidad</option> 
                         <option value="17">27 A satisfacción del acreedor</option> 
                         <option value="18">28 Tarjeta de Débito</option> 
                         <option value="19">29 Tarjeta de Servicio</option> 
                         <option value="20">30 Aplicación de anticipos</option> 
                          <option value="21">99 Por definir</option> 
                      </select>   
                    </div>
                    <div class=" col s4"> 
                      <label for="pago" class="active">Monto</label> 
                      <input type="number" name="pago" id="pago_p"> 
                      
                    </div> 
                  </div> 
                  
                  <div class="row"> 
                    <div class="input-field col s9"> 
                      <textarea name="observacion" id="observacion_p"></textarea> 
                      <label for="observacion_p" class="active">Comentario</label> 
                    </div>
                    <div class="col s3">
                      <a class="waves-effect waves-light btn modal-triggert" onclick="guardar_pago_compras()">Guardar</a> 
                    </div> 
                  </div> 
                </form>
                <div class="row">
                  <div class="col s12"> 
                    <span class="text_tabla_pago"></span> 
                  </div> 
                </div>
              </div>     
            </div> 

            <div id="modal_eliminarpago" class="modal"  >
              <div class="modal-content ">
                  <h4>Eliminar pago</h4> 
                  <p>Ingresa contraseña</p>
                  <div class="row">  
                    <div class="input-field col s12">
                      <input id="ver_pass" type="password">
                      <label for="ver_pass" class="active">Contraseña</label>
                    </div>
                  </div>  
              </div>
              <div class="modal-footer">
                  <button class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</button>
                  <button class="waves-effect waves-red red btn-flat" style="color: white;" onclick="eliminarpago()">Aceptar</button>
              </div>
              <!-- Modal finaliza fin-->
          </div>
        
    </section>

<div id="modaleliminar" class="modal">
  <div class="modal-content">
      <h5>Eliminar Venta </h5>
      <br>
      <div class="row">
        <div class="col s12 m12 12 ">
          <label>Motivo</label>
          <textarea id="motivoeliminacion"></textarea>
          <input type="hidden" id="deleteventa" value="">
          <input type="hidden" id="deletetipo" value="">
        </div>
      </div>
      <br>
    <div class="modal-footer">
      <a class="btn waves-effect waves-light green " onclick="ventadelete()">
        Aceptar
      </a>
      <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cancelar</a>
    </div>
  </div>
</div>
<div id="modaleliminarservicio" class="modal">
  <div class="modal-content">
      <h5>Eliminar Servicio </h5>
      <br>
      <div class="row">
        <div class="col s12 m12 12 ">
          <label>Motivo</label>
          <textarea id="motivoeliminacionservicio"></textarea>
          <input type="hidden" id="deleteservicio" value="">
          <input type="hidden" id="deleteserviciotipo" value="">
        </div>
      </div>
      <br>
    <div class="modal-footer">
      <a class="btn waves-effect waves-light green " onclick="serviciodelete()">
        Aceptar
      </a>
      <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cancelar</a>
    </div>
  </div>
</div>

<div id="modal_cfdi_ser" class="modal"  > 
  <div class="modal-content "> 
    <h4 align="center">Captura CFDI</h4> 
    <br> 
    <div class="row"> 
      <div class="input-field col s2"></div> 
      <div class="input-field col s8"> 
        <input type="text" id="cdfi_ser"> 
      </div> 
      <div class="input-field col s2"></div> 
      <input type="hidden" id="id_cfdi_servicio"> 
      <input type="hidden" id="tipo_cfdi_servicio"> 
    </div>  
  </div>     
  <div class="modal-footer"> 
    <a class="modal-action modal-close waves-effect waves-red gray btn-flat" onclick="update_cfdi_ser()">Guardar</a> 
  </div> 
</div>

<div id="modal_pago_s" class="modal"  > 
    <div class="modal-content "> 
      <div class="row"> 
        <div class="col s6"> 
          <h5>Registro de nuevo pago</h5> 
        </div> 
        <div class="col s6" align="right"> 
          <div class="row" style="margin-bottom: 0px;">
            <div class="col s7">
              Monto de prefactura
            </div>
            <div style="color: red" class="col s5 monto_prefactura_s"></div>
          </div>
          <div class="row" style="margin-bottom: 0px;">
            <div class="col s7">
              Restante
            </div>
            <div class="col s5 restante_prefactura_s" style="color: red"></div>
          </div>
        </div> 
      </div>   
      <h6>Vendedor: <?php echo($_SESSION['usuario']); ?></h6> 
      <form class="form" id="form_pago_servicio" method="post">  
        <input type="hidden" id="idservicio" name="idservicio"><!--id que se va aguardar dependiendo si es venta, renta, poliza y combinada --> 
        <input type="hidden" id="tipos" name="tipos"><!-- tipo para verificar en que tabla se va insertar -->  
        <div class="row"> 
          <div class="input-field col s3"> 
            <input type="date" name="fecha" id="fecha_p_s"> 
            <label for="fecha" class="active" class="form-control-bmz">Fecha</label> 
          </div> 
          <div class="input-field col s4"> 
            <label for="idmetodo" class="active">Método de pago</label><br> 
            <select name="idmetodo" class="browser-default chosen-select"> 
               <option value="1">01 Efectivo</option> 
                         <option value="2">02 Cheque nominativo</option> 
                         <option value="3">03 Transferencia electrónica de fondos</option> 
                         <option value="4">04 Tarjetas de Crédito</option> 
                         <option value="5">05 Monedero Electrónico</option> 
                         <option value="6">06 Dinero Electrónico</option> 
                         <option value="7">08 Vales de despensa</option> 
                         <option value="8">12 Dación en pago</option> 
                         <option value="9">13 Pago por subrogación</option> 
                         <option value="10">14 Pago por consignación</option> 
                         <option value="11">15 Condonación</option> 
                         <option value="12">17 Compensación</option> 
                         <option value="13">23 Novación</option> 
                         <option value="14">24 Confusión</option> 
                         <option value="15">25 Remisión de deuda</option> 
                         <option value="16">26 Prescripción o caducidad</option> 
                         <option value="17">27 A satisfacción del acreedor</option> 
                         <option value="18">28 Tarjeta de Débito</option> 
                         <option value="19">29 Tarjeta de Servicio</option> 
                         <option value="20">30 Aplicación de anticipos</option> 
                          <option value="21">99 Por definir</option> 
            </select>   
          </div> 
          <div class="input-field col s4"> 
            <input type="number" name="pago" id="pago_p_s"> 
            <label for="pago" class="active">Monto</label> 
          </div> 
        </div> 
      
        <div class="row"> 
          <div class="input-field col s9"> 
            <textarea name="observacion" ></textarea> 
            <label for="observacion_p" class="active">Comentario</label> 
          </div>
          <div class="col s3">
            <a class="waves-effect waves-light btn modal-triggert" onclick="guardar_pago_servicios()">Guardar</a> 
          </div> 
        </div> 
      </form>
      <div class="row">
        <div class="col s12"> 
          <span class="text_tabla_pago_servicios"></span> 
        </div> 
      </div>
    </div>     
  </div>  





  <div id="modal_addfactura" class="modal"  style="width: 90%">
    <div class="modal-content ">
        <h4>Agregar una factura</h4>
        <input type="hidden" id="addfactcontrato" value="160">
        <input type="hidden" id="addfactperiodo">
        <input type="hidden" id="addfactpersonal" value="1">
        <input type="hidden" id="totalgeneralvalor" value="0">
        <div class="row">
          <div class="col s12">
            <table id="tabla_facturacion" name="tabla_facturacion_cliente" class="responsive-table display" cellspacing="0">
                            <thead>
                              <tr>
                                <th>#</th>
                                <th>Folio</th>
                                <th>Cliente</th>
                                <th>RFC</th>
                                <th>Monto</th>
                                <th>Estatus</th><!--sin timbrar timbrada cancelada-->
                                <th>Fecha</th>
                                <th></th>
                                
                              </tr>
                            </thead>
                            <tbody>
                            </tbody>
                          </table>
          </div>
        </div>
        
        
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
        <a href="#!" class="modal-action modal-close waves-effect waves-red red btn-flat" onclick="agregarfacturas()" style="color: white;">Aceptar</a>
    </div>
    <!-- Modal finaliza fin-->
</div>         