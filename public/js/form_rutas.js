var base_url = $('#base_url').val();
$(document).ready(function(){
    $('#tabla_rutas tbody').on('click', 'a.eliminar', function () {
      var row=$(this).closest("tr");
      var id = row.find("td").eq(1).html();
      $.ajax({
      type: "POST",
      url: base_url+"index.php/Rutas/eliminar",
      data:{id:id},
      success: function(data){
      row.remove();
      }
      });
    }); 

    var form_rutas = $('#form_rutas');
    form_rutas.validate({
            rules: 
            {
                nombre: {
                    required: true
                }
            },
            // Para mensajes personalizados
            messages: 
            {
                 
                nombre:{
                    required: "Ingrese un nombre"
                }
            },
        errorElement : 'div',
            // Si están correctos los campos, procesamos el formulario mediante Ajax
        submitHandler: function (form) 
        {
            var datos = form_rutas.serialize();
            $.ajax({
                type:'POST',
                url: base_url+'index.php/Rutas/insertar',
                data: datos,
                success:function(data)
                {
                    console.log(data);
                    // Si existe el usuario, mostramos el mensaje de entrada y redirigimos
                    if(data>0)
                    {
                        swal({ title: "Éxito",
                            text: "Se insertó la ruta",
                            type: 'success',
                            showCancelButton: false,
                            allowOutsideClick: false,
                        });
                       $('#form_rutas')[0].reset();
                       setTimeout(function(){
                        location.reload();
                      },1000);
                    }
                   
                    else
                    {
                        swal({ title: "Error",
                            text: "Los datos del son erroneos",
                            type: 'error',
                            showCancelButton: false,
                            allowOutsideClick: false,
                        });
                    }
                }
            });
            
            
        }
    
    });


});

