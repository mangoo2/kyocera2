<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class Usuarios_model extends CI_Model 
{
    public function __construct() 
    {
        parent::__construct();
    }

    public function insertarCategoriaEquipos($dato){
    $this->db->insert('usuario_categoria_equipos',$dato);   
    return $this->db->insert_id();
    } 

    public function insertarCategoriaRefacciones($dato){
    $this->db->insert('usuario_categoria_refacciones',$dato);   
    return $this->db->insert_id();
    } 

    public function insertarCatalogoAccesorios($dato){
    $this->db->insert('usuario_catalogo_accesorios',$dato);   
    return $this->db->insert_id();
    } 

    function getDataEquipo(){
    $sql = "SELECT * FROM usuario_categoria_equipos WHERE status=1";
    $query = $this->db->query($sql);
    return $query->result();
    } 

    function getDataRefacciones(){
    $sql = "SELECT * FROM usuario_categoria_refacciones WHERE status=1";
    $query = $this->db->query($sql);
    return $query->result();
    }

    function getDataAccesorios(){
    $sql = "SELECT * FROM usuario_catalogo_accesorios WHERE status=1";
    $query = $this->db->query($sql);
    return $query->result();
    }

    public function eliminar_equipo($id) {
    $result="UPDATE usuario_categoria_equipos SET status=0 WHERE id=$id";
    $datos = $this->db->query($result);
    return $datos;
    }

    public function eliminar_refaccion($id) {
    $result="UPDATE usuario_categoria_refacciones SET status=0 WHERE id=$id";
    $datos = $this->db->query($result);
    return $datos;
    }

    public function eliminar_accesorio($id) {
    $result="UPDATE usuario_catalogo_accesorios SET status=0 WHERE id=$id";
    $datos = $this->db->query($result);
    return $datos;
    }
   
}

?>