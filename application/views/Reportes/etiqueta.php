<?php
require_once dirname(__FILE__) . '/TCPDF4/examples/tcpdf_include.php'; 
require_once dirname(__FILE__) . '/TCPDF4/tcpdf.php';

$GLOBALS['etiqueta']=$etiqueta;
//
    $style = array(
      'position' => 'L',
      'align' => 'L',
      'stretch' => false,
      'fitwidth' => true,
      'cellfitalign' => '',
      'border' => false,
      'hpadding' => 'auto',
      'vpadding' => 'auto',
      'fgcolor' => array(0,0,0),
      'bgcolor' => false, //array(255,255,255),
      'text' => true,
      'font' => 'helvetica',
      'fontsize' => 10,
      'stretchtext' => 4
  );

class MYPDF extends TCPDF {
  //Page header
  public function Header() {
      /// datos completos
      $html = '';
  
      $this->writeHTML($html, true, false, true, false, '');
  }
    // Page footer
  public function Footer() {
    $html = '';
    /*
    $html = '<p></p>';
    $html .= '<table width="100%" border="0">
                <tr>
                  <td align="right" class="footerpage">Pagina '.$this->getAliasNumPage().' de '.$this->getAliasNbPages().'</td>
                </tr>
              </table>';
    */
    $this->writeHTML($html, true, false, true, false, '');
  }
} 
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT,array(75, 30), true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Cuestionario');
$pdf->SetTitle('Cuestionario');
$pdf->SetSubject('Cuestionario');
$pdf->SetKeywords('Cuestionario');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins('2', '1', '2');
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(0);
$pdf->setPrintFooter(false);
// set auto page breaks
$pdf->SetAutoPageBreak(true,0);
$pdf->SetFont('dejavusans', '', 11);
// set image scale factor
//$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//$pdf->SetFont('dejavusans', '', 9);
// add a page
$pdf->AddPage('L');
  $codigo = '';
  
  foreach ($GLOBALS['etiqueta'] as $item) {
    $codigo = $item->foliotext;
    $pdf->write1DBarcode($item->foliotext, 'C39', '', '', '', 14, 0.4, $style, 'N');
    $html='<table border="0" style="100%">
            <tr>
              <td rowspan="3" width="15%" ><img style="width:40px;" src="'.base_url().'app-assets/images/alta.png" alt="materialize logo"></td>
              <td  width="75%" style="font-size: 7px; align:center">Cliente: '.$item->empresa.'</td>
            </tr>
            <tr>
              <td style="font-size: 6px; align:center">Modelo: '.$item->modelo.'</td>
            </tr>
            <tr>
              <td style="font-size: 5px; align:center">Fecha: '.$item->fechagenera.'</td>
            </tr>
          </table>';

  }

$pdf->writeHTML($html, true, false, true, false, '');
$pdf->Output('Captura.pdf', 'I');
?>